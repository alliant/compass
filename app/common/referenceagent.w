&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* referencagent.w
   Window of AGENTs
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/openwindow.i}
{tt/state.i}

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName=agent}

/* def var hData as handle no-undo.  */

/* {lib/winlaunch.i}  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bConfig bDelete bModify fSearchLabel ~
fStateLabel bNew bExport bRefresh bFilterClear bFilter fSearch fState ~
rFilters RECT-1 
&Scoped-Define DISPLAYED-OBJECTS fSearchLabel fStateLabel fSearch fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgentAction C-Win 
FUNCTION openWindowForAgentAction returns handle
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bConfig  NO-FOCUS
     LABEL "Config" 
     SIZE 7.2 BY 1.71 TOOLTIP "Setup the available columns".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the selected Agent".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bFilterClear 
     LABEL "Clear" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected Agent".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new Agent".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 91 BY 1 NO-UNDO.

DEFINE VARIABLE fSearchLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Search:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE fStateLabel AS CHARACTER FORMAT "X(256)":U INITIAL "State:" 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE RECTANGLE rActions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.2 BY 2.05.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 8.6 BY 2.05.

DEFINE RECTANGLE rFilters
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 146.2 BY 2.05.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bConfig AT ROW 1.38 COL 186.2 WIDGET-ID 76 NO-TAB-STOP 
     bDelete AT ROW 1.38 COL 32.2 WIDGET-ID 14 NO-TAB-STOP 
     bModify AT ROW 1.38 COL 24.8 WIDGET-ID 12 NO-TAB-STOP 
     fSearchLabel AT ROW 1.91 COL 69 COLON-ALIGNED NO-LABEL WIDGET-ID 72
     fStateLabel AT ROW 1.91 COL 40.8 COLON-ALIGNED NO-LABEL WIDGET-ID 70
     bNew AT ROW 1.38 COL 17.4 WIDGET-ID 10 NO-TAB-STOP 
     bExport AT ROW 1.38 COL 10 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.38 COL 2.6 WIDGET-ID 4 NO-TAB-STOP 
     bFilterClear AT ROW 1.38 COL 178 WIDGET-ID 68
     bFilter AT ROW 1.38 COL 170.6 WIDGET-ID 64
     fSearch AT ROW 1.71 COL 77 COLON-ALIGNED NO-LABEL WIDGET-ID 62
     fState AT ROW 1.71 COL 47 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     rActions AT ROW 1.24 COL 2 WIDGET-ID 8
     rFilters AT ROW 1.24 COL 39.8 WIDGET-ID 58
     RECT-1 AT ROW 1.24 COL 185.6 WIDGET-ID 74
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 194 BY 23.19 WIDGET-ID 100.

DEFINE FRAME fBrowse
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 3.38
         SIZE 192 BY 20.57 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agents"
         HEIGHT             = 23.19
         WIDTH              = 194
         MAX-HEIGHT         = 38.24
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 38.24
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fBrowse:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fBrowse
                                                                        */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR RECTANGLE rActions IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agents */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agents */
DO:
  for each openwindow no-lock:
    if valid-handle(openwindow.procHandle)
     then run CloseWindow in openwindow.procHandle no-error.
  end.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agents */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConfig C-Win
ON CHOOSE OF bConfig IN FRAME fMain /* Config */
DO:
  run SetDynamicBrowseColumns.
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  std-ha = frame fBrowse:first-child:first-child.
  if valid-handle(std-ha) and std-ha:type = "BROWSE"
   then
    do:
      std-ha = std-ha:query.
      std-ha = std-ha:get-buffer-handle(1).
      std-ch = std-ha:buffer-field("keyfield"):string-value().
      run ActionAgentDelete in this-procedure (std-ch).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run ExportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* Clear */
DO:
  fSearch:screen-value = "".
  fState:screen-value = "ALL".
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  run ActionAgentModify in this-procedure (dynamic-function("getKeyfield")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  openWindowForAgentAction("", "agentDetails", "dialogagent.w", "N").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  empty temp-table agent.
  run buildFieldList in this-procedure.
  run BuildDynamicBrowse (createQuery()).
  publish "LoadAgents".
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain
DO:
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "ActionAgentDashboard" anywhere.
subscribe to "ActionAgentDelete"    anywhere.
subscribe to "ActionAgentModify"    anywhere.
subscribe to "AddDynamicPopupMenu"  anywhere.
subscribe to "AgentDataChanged"     anywhere.

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/set-button.i &label="Refresh"}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="New"}
{lib/set-button.i &label="Modify"}
{lib/set-button.i &label="Delete"}
{lib/set-button.i &label="Filter"}
{lib/set-button.i &label="FilterClear"}
{lib/set-button.i &label="Config"}
{lib/set-filter.i &label="State"  &id="stateID"      &value="stateName"}
{lib/set-filter.i &label="Search" &id="name:agentID" &populate=false}
{lib/build-dynamic-browse.i &table=agent &entity="'Agent'" &keyfield="'agentID'" &resizeColumn="'name'"}
setButtons().

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* get the states */
{lib/get-state-list.i &combo=fState &addAll=true}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run getData in this-procedure.
  run SetDynamicBrowseColumnWidth ("name").
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAgentDashboard C-Win 
PROCEDURE ActionAgentDashboard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  openWindowForAgent(getKeyfield(), "dashboard", "dialogagentsummary.w").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAgentDelete C-Win 
PROCEDURE ActionAgentDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  {lib/confirm-delete.i "Agent"}
  
  publish "DeleteAgent" (pAgentID, output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAgentModify C-Win 
PROCEDURE ActionAgentModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  openWindowForAgentAction(pAgentID, "detail", "wagent.w", "E").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddDynamicPopupMenu C-Win 
PROCEDURE AddDynamicPopupMenu :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter hPopup as handle no-undo.
  define input parameter pEnable as logical no-undo.
  define variable hItem as handle no-undo.
  
  create menu-item hItem
  assign
    label     = "Agent Summary"
    name      = "m_Agent_Summary"
    sensitive = pEnable
    parent    = hPopup
    triggers:
      on choose persistent run ActionAgentDashboard in this-procedure (getKeyfield()).
    end triggers
    .
  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentDataChanged C-Win 
PROCEDURE AgentDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE buildFieldList C-Win 
PROCEDURE buildFieldList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  run BuildDynamicBrowseColumns.
  for each listfield exclusive-lock
     where listfield.dataType = "character":
    
    case listfield.columnBuffer:
     when "remitType" or
     when "stat" then listfield.columnWidth = 8.
     when "zip" or
     when "mZip" then listfield.columnWidth = 10.
     when "state" or
     when "mState" or
     when "agentID" or
     when "stateID" then listfield.columnWidth = 12.
     when "email" then listfield.columnWidth = 35.
     when "website" or
     when "manager" then listfield.columnWidth = 40.
     when "addr1" or
     when "mAddr1" then listfield.columnWidth = 60.
     when "mName" or
     when "name" or
     when "corporationID" then listfield.columnWidth = 61.
     otherwise listfield.columnWidth = 25.
    end case.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearchLabel fStateLabel fSearch fState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bConfig bDelete bModify fSearchLabel fStateLabel bNew bExport bRefresh 
         bFilterClear bFilter fSearch fState rFilters RECT-1 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME fBrowse IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fBrowse}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.

  hBrowse = frame fBrowse:first-child:first-child.
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      if std-ha:num-results = 0
       then
        do:
          message "No results to export" view-as alert-box warning buttons ok.
          return.
        end.
    end.

  &scoped-define ReportName "Agents"

 std-ch = "C".
 publish "GetExportType" (output std-ch).
 if std-ch = "X" 
  then run util/exporttoexcelbrowse.p (string(hBrowse), {&ReportName}).
  else run util/exporttocsvbrowse.p (string(hBrowse), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table agent.
  publish "GetAgents" (output table agent).
  
  if can-find(first agent)
   then
    do:
      run buildFieldList in this-procedure.
      run BuildDynamicBrowse (createQuery()).
    end.

  setFilterCombos("ALL").
  enableButtons(can-find(first agent)).
  enableFilters(can-find(first agent)).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  /* modify the frame for the dynamic browse */
  frame fBrowse:width-pixels = {&window-name}:width-pixels - 10.
  frame fBrowse:virtual-width-pixels = frame fBrowse:width-pixels.
  frame fBrowse:height-pixels = {&window-name}:height-pixels - frame fBrowse:y - 5.
  frame fBrowse:virtual-height-pixels = frame fBrowse:height-pixels.
  
  run getData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE tQuery as CHARACTER no-undo.

  do with frame {&frame-name}:
    tQuery = "preselect each agent where true".

    IF fState:screen-value <> "ALL" 
     THEN tQuery = addDelimiter(tQuery, " and ") + "agent.stateID = '" + fState:screen-value + "' ".
    IF fSearch:screen-value > ""
     THEN tQuery = addDelimiter(tQuery, " and ") + "(agent.name matches '*" + replace(fSearch:screen-value,"'","''") + "*' or agent.legalname matches '*" + replace(fSearch:screen-value,"'","''") + "*' or agent.agentID matches '*" + replace(fSearch:screen-value,"'","''") + "*')".
    
  end.
  RETURN tQuery.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character ) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .
  publish "OpenAgent" (pAgentID, output hFileDataSrv).
  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      run value(pFile) persistent set hWindow (hFileDataSrv).
      create openwindow.
      assign
        openwindow.procFile = cAgentWindow
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgentAction C-Win 
FUNCTION openWindowForAgentAction returns handle
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow      as handle    no-undo.
  define variable hFileDataSrv as handle    no-undo.
  define variable cAgentWindow as character no-undo.

  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAction <> "N" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  if pAction <> "C" then
  do:
    for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
      hWindow = openwindow.procHandle.
    end.
  end.
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv,pAction).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

