&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fRegion 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  modification
      date         Name           Description
      06/10/2022   SA             Task 93485 - Removed RegionID field and changed
                                  Description field to "Region Name"
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pRegionID as character no-undo.
define output parameter pStateList as character no-undo.
define output parameter pCancel as logical no-undo initial true.

/* Local Variable Definitions ---                                       */
define variable tStateList as character no-undo.
{lib/std-def.i}

/* Functions ---                                                        */
{lib/add-delimiter.i}
{lib/move-item.i}

/* Temp Tables ---                                                      */
{tt/region.i}
{tt/sysprop.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fRegion

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tStateAvailable bAddField tStateRegion ~
bRemoveField tRegionName bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tStateAvailable tStateRegion tRegionName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddField 
     LABEL ">" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add the state to the region".

DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bRemoveField 
     LABEL "<" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove the state from the region".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tRegionName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Region Name" 
     VIEW-AS FILL-IN 
     SIZE 39 BY 1 NO-UNDO.

DEFINE VARIABLE tStateAvailable AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "ALL","ALL" 
     SIZE 23 BY 4.52 NO-UNDO.

DEFINE VARIABLE tStateRegion AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "ALL","ALL" 
     SIZE 23 BY 4.52 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fRegion
     tStateAvailable AT ROW 3.86 COL 3 NO-LABEL WIDGET-ID 2
     bAddField AT ROW 4.57 COL 27 WIDGET-ID 106
     tStateRegion AT ROW 3.86 COL 33 NO-LABEL WIDGET-ID 4
     bRemoveField AT ROW 6 COL 27 WIDGET-ID 108
     tRegionName AT ROW 1.48 COL 15 COLON-ALIGNED WIDGET-ID 116 NO-TAB-STOP 
     bSave AT ROW 8.81 COL 13.4
     bCancel AT ROW 8.81 COL 30.4
     "Available States:" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 3.14 COL 3 WIDGET-ID 110
     "States in Region:" VIEW-AS TEXT
          SIZE 16.6 BY .62 AT ROW 3.14 COL 33 WIDGET-ID 112
     SPACE(8.39) SKIP(6.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify Region"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fRegion
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fRegion:SCROLLABLE       = FALSE
       FRAME fRegion:HIDDEN           = TRUE.

ASSIGN 
       tRegionName:READ-ONLY IN FRAME fRegion        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRegion fRegion
ON WINDOW-CLOSE OF FRAME fRegion /* Modify Region */
DO:
  if pStateList <> tStateList
   then {lib/confirm-close.i "Region"}
  
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddField
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddField fRegion
ON CHOOSE OF bAddField IN FRAME fRegion /* > */
DO:
  do with frame frmBuild:
    MoveItem(tStateAvailable:handle,tStateRegion:handle,",").
  end.
  tStateList = "".
  do std-in = 1 to num-entries(tStateRegion:list-item-pairs in frame {&frame-name}) / 2:
    tStateList = addDelimiter(tStateList, ",") + entry(std-in * 2, tStateRegion:list-item-pairs in frame {&frame-name}).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fRegion
ON CHOOSE OF bCancel IN FRAME fRegion /* Cancel */
DO:
  if pStateList <> tStateList
   then {lib/confirm-close.i "Region"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveField
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveField fRegion
ON CHOOSE OF bRemoveField IN FRAME fRegion /* < */
DO:
  do with frame frmBuild:
    MoveItem(tStateRegion:handle,tStateAvailable:handle,",").
  end.
  tStateList = "".
  do std-in = 1 to num-entries(tStateRegion:list-item-pairs in frame {&frame-name}) / 2:
    tStateList = addDelimiter(tStateList, ",") + entry(std-in * 2, tStateRegion:list-item-pairs in frame {&frame-name}).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateAvailable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateAvailable fRegion
ON DEFAULT-ACTION OF tStateAvailable IN FRAME fRegion
DO:
  APPLY "CHOOSE" TO bAddField.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateRegion fRegion
ON DEFAULT-ACTION OF tStateRegion IN FRAME fRegion
DO:
  APPLY "CHOOSE" TO bRemoveField.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fRegion 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bAddField:load-image-up("images/s-right.bmp").
bAddField:load-image-insensitive("images/s-right-i.bmp").
bRemoveField:load-image-up("images/s-left.bmp").
bRemoveField:load-image-insensitive("images/s-left-i.bmp").

publish "GetStates" (output table state).
publish "GetRegions" (output table region).
publish "GetSysProps" (output table sysprop).
do with frame {&frame-name}:
  for each state no-lock
     where state.active = true:
    
    if not can-find(first region where region.stateID = state.stateID)
     then tStateAvailable:add-last(state.description,state.stateID).
    
    if can-find(first region where region.regionID = pRegionID and region.stateID = state.stateID)
     then
      do:
        tStateList = addDelimiter(tStateList, ",") + state.stateID.
        pStateList = addDelimiter(pStateList, ",") + state.stateID.
        tStateRegion:add-last(state.description,state.stateID).
      end.
  end.
  tStateAvailable:delete("ALL").
  tStateRegion:delete("ALL").
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
REPEAT ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
       ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  for first region no-lock
      where region.regionID = pRegionID:
    
    assign
      tRegionName:screen-value in frame {&frame-name} = region.regionDesc
      .
  end.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  assign
    pCancel = false
    pStateList = tStateList
    .
  leave.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fRegion  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fRegion.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fRegion  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStateAvailable tStateRegion tRegionName 
      WITH FRAME fRegion.
  ENABLE tStateAvailable bAddField tStateRegion bRemoveField tRegionName bSave 
         bCancel 
      WITH FRAME fRegion.
  VIEW FRAME fRegion.
  {&OPEN-BROWSERS-IN-QUERY-fRegion}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

