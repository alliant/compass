&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wpolicy.w
   POLICY Window
   D.Sinclair 3.7.2013
   
   Rahul Sharma
   Note: Window moved from app/ops to app/common because 
         it is used from ARM as well as OPS module
   */

CREATE WIDGET-POOL.

{lib/std-def.i}

{tt/batchform.i}
{tt/policy.i}
{tt/proerr.i &tableAlias="fileErr"}       /* Used to get validation messages from server */

/* Used when creating new policy records to speed up multiple entries */
def var tNewAgentID as char no-undo.
def var tNewFormID as char no-undo.
 /* include file to normalize file number */
{lib/normalizefileid.i}

define variable cErrFile as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwForm

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batchform

/* Definitions for BROWSE brwForm                                       */
&Scoped-define FIELDS-IN-QUERY-brwForm batchform.formType batchform.formID batchform.statcode batchform.effDate batchform.liabilityDelta batchform.grossDelta batchform.netDelta batchform.batchID batchform.seq   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwForm   
&Scoped-define SELF-NAME brwForm
&Scoped-define QUERY-STRING-brwForm FOR EACH batchform
&Scoped-define OPEN-QUERY-brwForm OPEN QUERY {&SELF-NAME} FOR EACH batchform.
&Scoped-define TABLES-IN-QUERY-brwForm batchform
&Scoped-define FIRST-TABLE-IN-QUERY-brwForm batchform


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAgentID tAgentName tEffectiveDate ~
tFileNumber tIssueDate tState tStat tInvoiceDate tCountyID tCountyDesc ~
tLiability tVoidDate tFormID tFormDesc tStatCode tStatCodeDesc tGrossPolicy ~
tGrossEndorsement tGrossTotal tRetentionPolicy tRetentionEndorsement ~
tRetentionTotal tNetPolicy tNetEndorsement tNetTotal brwForm tGrossGLRef ~
tgrossGLDesc tretainedGLRef tretainedGLDesc tnetGLRef tnetGLdesc 
&Scoped-Define DISPLAYED-OBJECTS tAgentID tAgentName tEffectiveDate ~
tFileNumber tIssueDate tState tStat tInvoiceDate tResidential tCountyID ~
tCountyDesc tVoidDate tFormID tFormDesc tStatCode tStatCodeDesc tGrossGLRef ~
tgrossGLDesc tretainedGLRef tretainedGLDesc tnetGLRef tnetGLdesc tHeader 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getData C-Win 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bEditFile 
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify the File Number".

DEFINE BUTTON bViewFile 
     LABEL "View" 
     SIZE 4.8 BY 1.14 TOOLTIP "View Agent File details".

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 64.4 BY 1 NO-UNDO.

DEFINE VARIABLE tCountyDesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 30.4 BY 1 NO-UNDO.

DEFINE VARIABLE tCountyID AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEffectiveDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 38.8 BY 1 NO-UNDO.

DEFINE VARIABLE tFormDesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 91.2 BY 1 NO-UNDO.

DEFINE VARIABLE tFormID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Form" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossEndorsement AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tgrossGLDesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 70.8 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossGLRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPolicy AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossTotal AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tHeader AS CHARACTER FORMAT "X(256)":U INITIAL "General Ledger References for the currently selected Processed Form" 
      VIEW-AS TEXT 
     SIZE 67.4 BY .62
     FGCOLOR 7  NO-UNDO.

DEFINE VARIABLE tInvoiceDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Invoiced" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tIssueDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Issued" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 22.8 BY 1 NO-UNDO.

DEFINE VARIABLE tNetEndorsement AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tnetGLdesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 70.8 BY 1 NO-UNDO.

DEFINE VARIABLE tnetGLRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPolicy AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tNetTotal AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tretainedGLDesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 70.8 BY 1 NO-UNDO.

DEFINE VARIABLE tretainedGLRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Retained" 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE tRetentionEndorsement AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tRetentionPolicy AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Retained" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tRetentionTotal AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "I)ssued, P)rocessed, V)oided" NO-UNDO.

DEFINE VARIABLE tStatCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "STAT" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tStatCodeDesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 91.2 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 6.4 BY 1 NO-UNDO.

DEFINE VARIABLE tVoidDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Voided" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 116 BY 4.48.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 116 BY 12.62.

DEFINE VARIABLE tResidential AS LOGICAL INITIAL no 
     LABEL "Residential" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 NO-UNDO.

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get" 
     SIZE 4.8 BY 1.14 TOOLTIP "Get data".

DEFINE VARIABLE tFindPolicy AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 TOOLTIP "Enter policy number and press <Return> to search"
     FONT 6 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwForm FOR 
      batchform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwForm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwForm C-Win _FREEFORM
  QUERY brwForm DISPLAY
      batchform.formType label "Type" format "x(4)"
batchform.formID label "Form" format "x(15)"
batchform.statcode column-label "STAT!Code" format "x(10)"
batchform.effDate column-label "Effective!Date" format "99/99/9999"
batchform.liabilityDelta column-label "Liability!Amount" format "zzz,zzz,zz9.99-"
batchform.grossDelta column-label "Gross!Premium" format "zzz,zz9.99-"
batchform.netDelta column-label "Net!Premium" format "zzz,zz9.99-"
batchform.batchID label "Batch" format "zzzzzzzzz"
batchform.seq label "Seq" format "zzz" width 4
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 112 BY 7.52 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Double-click to open Batch Details for the selected form".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tAgentID AT ROW 1.24 COL 10 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     tAgentName AT ROW 1.24 COL 24.6 COLON-ALIGNED NO-LABEL WIDGET-ID 70 NO-TAB-STOP 
     tEffectiveDate AT ROW 1.24 COL 101.6 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     tFileNumber AT ROW 2.33 COL 10 COLON-ALIGNED WIDGET-ID 14 NO-TAB-STOP 
     bEditFile AT ROW 2.33 COL 51.4 WIDGET-ID 148
     bViewFile AT ROW 2.33 COL 56 WIDGET-ID 120
     tIssueDate AT ROW 2.33 COL 101.6 COLON-ALIGNED WIDGET-ID 92 NO-TAB-STOP 
     tState AT ROW 3.43 COL 10 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     tStat AT ROW 3.43 COL 66 COLON-ALIGNED WIDGET-ID 68 NO-TAB-STOP 
     tInvoiceDate AT ROW 3.43 COL 101.6 COLON-ALIGNED WIDGET-ID 96 NO-TAB-STOP 
     tResidential AT ROW 3.48 COL 19.4 WIDGET-ID 36 NO-TAB-STOP 
     tCountyID AT ROW 4.52 COL 10 COLON-ALIGNED WIDGET-ID 38 NO-TAB-STOP 
     tCountyDesc AT ROW 4.52 COL 24.6 COLON-ALIGNED NO-LABEL WIDGET-ID 40 NO-TAB-STOP 
     tLiability AT ROW 4.52 COL 66 COLON-ALIGNED WIDGET-ID 32 NO-TAB-STOP 
     tVoidDate AT ROW 4.52 COL 101.6 COLON-ALIGNED WIDGET-ID 94 NO-TAB-STOP 
     tFormID AT ROW 5.62 COL 10 COLON-ALIGNED WIDGET-ID 24 NO-TAB-STOP 
     tFormDesc AT ROW 5.62 COL 24.6 COLON-ALIGNED NO-LABEL WIDGET-ID 26 NO-TAB-STOP 
     tStatCode AT ROW 6.71 COL 10 COLON-ALIGNED WIDGET-ID 30 NO-TAB-STOP 
     tStatCodeDesc AT ROW 6.71 COL 24.6 COLON-ALIGNED NO-LABEL WIDGET-ID 28 NO-TAB-STOP 
     tGrossPolicy AT ROW 9.29 COL 25 COLON-ALIGNED WIDGET-ID 44 NO-TAB-STOP 
     tGrossEndorsement AT ROW 9.33 COL 50.4 COLON-ALIGNED NO-LABEL WIDGET-ID 100 NO-TAB-STOP 
     tGrossTotal AT ROW 9.33 COL 76 COLON-ALIGNED NO-LABEL WIDGET-ID 114 NO-TAB-STOP 
     tRetentionPolicy AT ROW 10.38 COL 25 COLON-ALIGNED WIDGET-ID 48 NO-TAB-STOP 
     tRetentionEndorsement AT ROW 10.43 COL 50.4 COLON-ALIGNED NO-LABEL WIDGET-ID 104 NO-TAB-STOP 
     tRetentionTotal AT ROW 10.43 COL 76 COLON-ALIGNED NO-LABEL WIDGET-ID 118 NO-TAB-STOP 
     tNetPolicy AT ROW 11.48 COL 25 COLON-ALIGNED WIDGET-ID 46 NO-TAB-STOP 
     tNetEndorsement AT ROW 11.52 COL 50.4 COLON-ALIGNED NO-LABEL WIDGET-ID 102 NO-TAB-STOP 
     tNetTotal AT ROW 11.52 COL 76 COLON-ALIGNED NO-LABEL WIDGET-ID 116 NO-TAB-STOP 
     brwForm AT ROW 13.71 COL 4 WIDGET-ID 300
     tGrossGLRef AT ROW 22.38 COL 11 COLON-ALIGNED WIDGET-ID 122 NO-TAB-STOP 
     tgrossGLDesc AT ROW 22.38 COL 38.8 COLON-ALIGNED NO-LABEL WIDGET-ID 128 NO-TAB-STOP 
     tretainedGLRef AT ROW 23.48 COL 11 COLON-ALIGNED WIDGET-ID 126 NO-TAB-STOP 
     tretainedGLDesc AT ROW 23.48 COL 38.8 COLON-ALIGNED NO-LABEL WIDGET-ID 132 NO-TAB-STOP 
     tnetGLRef AT ROW 24.57 COL 11 COLON-ALIGNED WIDGET-ID 124 NO-TAB-STOP 
     tnetGLdesc AT ROW 24.57 COL 38.8 COLON-ALIGNED NO-LABEL WIDGET-ID 130 NO-TAB-STOP 
     tHeader AT ROW 21.57 COL 15.6 COLON-ALIGNED NO-LABEL WIDGET-ID 150 NO-TAB-STOP 
     "Endorsements" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 8.62 COL 61.4 WIDGET-ID 108
     "Total" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 8.62 COL 95.6 WIDGET-ID 112
     "Revenue" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 8.14 COL 3 WIDGET-ID 66
     "Processed Forms" VIEW-AS TEXT
          SIZE 17 BY .62 TOOLTIP "Only (C)omplete batches are displayed" AT ROW 12.91 COL 3.6 WIDGET-ID 60
     "Policy" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 8.62 COL 43.4 WIDGET-ID 106
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 3.19
         SIZE 118.2 BY 24.91 WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fMain
     RECT-5 AT ROW 8.43 COL 2 WIDGET-ID 64
     RECT-6 AT ROW 13.24 COL 2 WIDGET-ID 134
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 3.19
         SIZE 118.2 BY 24.91 WIDGET-ID 100.

DEFINE FRAME framePolicy
     bGet AT ROW 1.86 COL 26.6 WIDGET-ID 148 NO-TAB-STOP 
     tFindPolicy AT ROW 1.95 COL 10 COLON-ALIGNED WIDGET-ID 2
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 1 ROW 1
         SIZE 117.4 BY 2.19 WIDGET-ID 500.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Policy Details"
         HEIGHT             = 27.14
         WIDTH              = 117.4
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwForm tNetTotal fMain */
/* SETTINGS FOR BUTTON bEditFile IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bViewFile IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAgentName:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tCountyDesc:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tCountyID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tEffectiveDate:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tFileNumber:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tFormDesc:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tFormID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tGrossEndorsement IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tGrossEndorsement:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tgrossGLDesc:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tGrossGLRef:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tGrossPolicy IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tGrossPolicy:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tGrossTotal IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tGrossTotal:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tHeader IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tHeader:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tInvoiceDate:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tIssueDate:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tNetEndorsement IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNetEndorsement:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tnetGLdesc:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tnetGLRef:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tNetPolicy IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNetPolicy:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tNetTotal IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNetTotal:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tResidential IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tretainedGLDesc:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tretainedGLRef:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tRetentionEndorsement IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tRetentionEndorsement:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tRetentionPolicy IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tRetentionPolicy:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tRetentionTotal IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tRetentionTotal:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tStat:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tStatCode:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tStatCodeDesc:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tState:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tVoidDate:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME framePolicy
                                                                        */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwForm
/* Query rebuild information for BROWSE brwForm
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchform.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwForm */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME framePolicy
/* Query rebuild information for FRAME framePolicy
     _Query            is NOT OPENED
*/  /* FRAME framePolicy */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Policy Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Policy Details */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Policy Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditFile C-Win
ON CHOOSE OF bEditFile IN FRAME fMain /* Edit */
DO:
  run modifyFileNumber in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME framePolicy
&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME framePolicy /* Get */
OR 'RETURN' of tFindPolicy
DO:       
  getData().           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwForm
&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME brwForm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForm C-Win
ON DEFAULT-ACTION OF brwForm IN FRAME fMain
DO:
  if not available batchform 
   then return.
  publish "SetCurrentValue" ("BatchID", batchform.batchID).
  run wops01-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForm C-Win
ON ROW-DISPLAY OF brwForm IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForm C-Win
ON START-SEARCH OF brwForm IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForm C-Win
ON VALUE-CHANGED OF brwForm IN FRAME fMain
DO:
   run setGLInfo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewFile C-Win
ON CHOOSE OF bViewFile IN FRAME fMain /* View */
DO:
  publish "SetCurrentValue" ("FileNumber", tFileNumber:screen-value).
  publish "SetCurrentValue" ("AgentID", tAgentID:screen-value).
  run wfile.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/win-main.i}
{lib/brw-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

assign
 {&WINDOW-NAME}:min-height-pixels = {&WINDOW-NAME}:height-pixels
 {&WINDOW-NAME}:max-height-pixels = session:height-pixels
 {&WINDOW-NAME}:min-width-pixels = {&WINDOW-NAME}:width-pixels
 {&WINDOW-NAME}:max-width-pixels = {&WINDOW-NAME}:width-pixels
 .
 
bGet:load-image("images/s-completed.bmp"). 
bGet:load-image-insensitive("images/s-completed-i.bmp").
bEditFile:load-image("images/s-update.bmp").
bEditFile:load-image-insensitive("images/s-update-i.bmp").
bViewFile:load-image("images/s-lookup.bmp").
bViewFile:load-image-insensitive("images/s-lookup-i.bmp").

publish "GetCurrentValue" ("PolicyID", output std-ch).
/* this procedurte pulbish as well as subscribe these events because same event can be published from 
other window apolicy.w in that case this window need to subscribe that events*/
subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.

tFindPolicy = int(std-ch) no-error.

status default "" in window {&window-name}.
status input "" in window {&window-name}.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  std-lo = false.
 publish "GetAutoView" (output std-lo).
 if std-lo
    and tFindPolicy:input-value in frame framePolicy > 0 
  then 
   do:
     assign
         tFindPolicy:sensitive = false
         bGet:sensitive        = false
         .
     getData().
   end.
  
 apply "ENTRY" to tFindPolicy.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
define input parameter pOldFileID as character.
define input parameter pNewFileID as character.
do with frame {&frame-name}:
end.
if valid-handle(this-procedure) and
   tAgentID:screen-value = pAgentID and
   normalizeFileID(tFileNumber:screen-value) = pOldFileID
 then
  run closewindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "WindowClosed" (input this-procedure).
   APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFindPolicy 
      WITH FRAME framePolicy IN WINDOW C-Win.
  ENABLE bGet tFindPolicy 
      WITH FRAME framePolicy IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-framePolicy}
  DISPLAY tAgentID tAgentName tEffectiveDate tFileNumber tIssueDate tState tStat 
          tInvoiceDate tResidential tCountyID tCountyDesc tVoidDate tFormID 
          tFormDesc tStatCode tStatCodeDesc tGrossGLRef tgrossGLDesc 
          tretainedGLRef tretainedGLDesc tnetGLRef tnetGLdesc tHeader 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tAgentID tAgentName tEffectiveDate tFileNumber tIssueDate tState tStat 
         tInvoiceDate tCountyID tCountyDesc tLiability tVoidDate tFormID 
         tFormDesc tStatCode tStatCodeDesc tGrossPolicy tGrossEndorsement 
         tGrossTotal tRetentionPolicy tRetentionEndorsement tRetentionTotal 
         tNetPolicy tNetEndorsement tNetTotal brwForm tGrossGLRef tgrossGLDesc 
         tretainedGLRef tretainedGLDesc tnetGLRef tnetGLdesc 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportErrorData C-Win 
PROCEDURE exportErrorData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcCurrent as character no-undo.
  
  if can-find(first fileErr)
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "ErrorsModifyFileNumber(" + ipcCurrent + ")_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table fileErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "fileErr",
                              "for each fileErr",
                              "err,description",
                              "Sequence,Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
                              
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyFileNumber C-Win 
PROCEDURE modifyFileNumber PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tCurrent as char no-undo.
 def var tNew as char no-undo.
 def var tSave as logical init false.
 def var cNewFileID as char.
 def var pChoice as char.
 def var tCurrentFileID as char.
 define variable tFileExist as logical init false no-undo.
 define variable tOption    as logical init false no-undo.
 
 tCurrent = tFileNumber:screen-value in frame fMain.
 tNew = tCurrent.
 tCurrentFileID = normalizeFileID(tCurrent).
 
 REPEAT-LOOP:
 repeat with frame fMain:
   run dialogmovepolicy.w (tCurrent,
                           input-output tNew,
                           output tSave).
   if not tSave 
    then return.

   if tCurrent = tNew 
    then return.

   if tNew = "" or tNew = ? 
    then
     do:
         MESSAGE "File Number cannot be blank or unknown."
            VIEW-AS ALERT-BOX error BUTTONS OK.
         next.
     end.

   if tCurrent <> tNew 
    then
     run server/modifyfilenumberpolicy.p (tAgentID:screen-value,
                                          tFindPolicy:input-value in frame framePolicy,
                                          tFileNumber:screen-value,
                                          tNew,
                                          true,
                                          output cNewFileID,
                                          output tFileExist,
                                          output table fileErr,
                                          output std-lo,
                                          output std-ch).
                                          
   /* Export error table into csv and open it */
   run exportErrorData(tCurrent). 
   
   if tFileExist
    then
     MESSAGE "New File Number already exist. Policy will be merged to the new file number. Continue ?"
         VIEW-AS ALERT-BOX question BUTTONS Yes-No update tOption.
   
   if not tOption 
    then
     return.
     
   run server/modifyfilenumberpolicy.p (tAgentID:screen-value,
                                        tFindPolicy:input-value in frame framePolicy,
                                        tFileNumber:screen-value,
                                        tNew,
                                        false,
                                        output cNewFileID,
                                        output tFileExist,
                                        output table fileErr,
                                        output std-lo,
                                        output std-ch).
                                        
   /* Export error table into csv and open it */
   if can-find(first fileErr)
   then
   do:
    run exportErrorData(tCurrent). 
    next.
   end.

   if not std-lo 
    then
   do:
        MESSAGE std-ch
            VIEW-AS ALERT-BOX error BUTTONS OK.
        next.
   end.
    tFileNumber:screen-value in frame fMain = tNew.
     getdata().
     run dialogModifyFileNumbermsg.w (input  "P",
                                      input  tFindPolicy:input-value in frame framePolicy,
                                      input  tCurrent,
                                      input  tNew,
                                      output pChoice).
     case pChoice:
     when "1" then
     publish "RefreshScreensForFileNumModify"(tAgentID:screen-value,
                                              tCurrentFileID,
                                              cNewFileID).
     when "2" then
     publish "CloseScreensForFileNumModify"(tAgentID:screen-value,
                                            tCurrentFileID,
                                            cNewFileID).

     end case.
   leave REPEAT-LOOP.
 end.


 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
define input parameter pOldFileID as character.
define input parameter pNewFileID as character.
do with frame {&frame-name}:
end.
if tAgentID:screen-value = pAgentID and
    normalizeFileID(tFileNumber:screen-value) = pOldFileID
 then
 do:
    tFileNumber:screen-value = pNewFileID.
     getData().
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setGLInfo C-Win 
PROCEDURE setGLInfo PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame fMain:
   if available batchform 
    then
     assign
           tGrossGLRef:screen-value     = batchform.grossGLRef 
           tgrossGLDesc:screen-value    = batchform.grossGLDesc 
           tretainedGLRef:screen-value  = batchform.retainedGLRef  
           tretainedGLDesc:screen-value = batchform.retainedGLDesc 
           tnetGLRef:screen-value       = batchform.netGLRef 
           tnetGLdesc:screen-value      = batchform.netGLdesc
           .
    else
     assign
           tGrossGLRef:screen-value     = "" 
           tgrossGLDesc:screen-value    = "" 
           tretainedGLRef:screen-value  = ""  
           tretainedGLDesc:screen-value = "" 
           tnetGLRef:screen-value       = "" 
           tnetGLdesc:screen-value      = ""
           .
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels - 38.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels - 38.
 browse brwForm:height-pixels = frame {&frame-name}:height-pixels - 385.
 RECT-6:height-pixels = frame {&frame-name}:height-pixels - 275.
 tHeader:row = frame {&frame-name}:height - 4.3.
 tGrossGLRef:row = frame {&frame-name}:height - 3.3.
 tGrossGLRef:side-label-handle:row = frame {&frame-name}:height - 3.3.
 tgrossGLDesc:row = frame {&frame-name}:height - 3.3.
 tretainedGLRef:row = frame {&frame-name}:height - 2.3.
 tretainedGLRef:side-label-handle:row = frame {&frame-name}:height - 2.3.
 tretainedGLDesc:row = frame {&frame-name}:height - 2.3.
 tnetGLRef:row = frame {&frame-name}:height - 1.3.
 tnetGLRef:side-label-handle:row = frame {&frame-name}:height - 1.3.
 tnetGLdesc:row = frame {&frame-name}:height - 1.3.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  clear frame {&frame-name} no-pause.
  close query brwForm.
  status default "" in window {&window-name}.
  bEditFile:sensitive in frame {&frame-name} = false.
  bViewFile:sensitive in frame {&frame-name} = false.

/*   assign                                          */
/*     bAdd:sensitive in frame framePolicy = true    */
/*     bVoid:sensitive in frame framePolicy = false  */
/*     bSwap:sensitive in frame framePolicy = false  */
/*     .                                             */
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getData C-Win 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def buffer policy for policy.

  clearData().

/*   bAdd:sensitive in frame framePolicy = false. */

  run server/getpolicy.p (input tFindPolicy:input-value in frame framePolicy,
                          output table policy,
                          output table batchform,
                          output std-lo,
                          output std-ch).

  std-ch = tFindPolicy:screen-value in frame framePolicy + " as of " + string(now).
  status default std-ch in window {&window-name}.

  find first policy no-error.
  if not available policy
   then
    do with frame fMain: 
        bViewFile:sensitive = false.
        bEditFile:sensitive = false.
        return false.
    end.

  assign
    tGrossPolicy = 0
    tRetentionPolicy = 0
    tNetPolicy = 0
    tGrossEndorsement = 0
    tRetentionEndorsement = 0
    tNetEndorsement = 0
    tGrossTotal = 0
    tRetentionTotal = 0
    tNetTotal = 0
    .
  for each batchform:
   if batchform.formType = "P"
    then assign
           tGrossPolicy = tGrossPolicy + batchform.grossDelta
           tRetentionPolicy = tRetentionPolicy + batchform.retentionDelta
           tNetPolicy = tNetPolicy + batchform.netDelta
           .
    else 
   if batchform.formType = "E" 
    then assign
           tGrossEndorsement = tGrossEndorsement + batchform.grossDelta
           tRetentionEndorsement = tRetentionEndorsement + batchform.retentionDelta
           tNetEndorsement = tNetEndorsement + batchform.netDelta
           .
   assign
     tGrossTotal = tGrossTotal + batchform.grossDelta
     tRetentionTotal = tRetentionTotal + batchform.retentionDelta
     tNetTotal = tNetTotal + batchform.netDelta
     .
  end.

  assign
    tAgentID:screen-value in frame fMain = policy.agentid
    tAgentName:screen-value in frame fMain = policy.agentname
    tFileNumber:screen-value in frame fMain = policy.fileNumber
    tStat:screen-value in frame fMain = policy.descStat
    tEffectiveDate:screen-value in frame fMain = string(policy.effDate)
    tIssueDate:screen-value in frame fMain = string(policy.issueDate)
    tInvoiceDate:screen-value in frame fMain = string(policy.invoiceDate)
    tVoidDate:screen-value in frame fMain = string(policy.voidDate)
    tLiability:screen-value in frame fMain = string(policy.liabilityAmount)
    tGrossPolicy:screen-value in frame fMain = string(policy.grossPremium)
    tNetPolicy:screen-value in frame fMain = string(policy.netPremium)
    tRetentionPolicy:screen-value in frame fMain = string(policy.retention)
    tState:screen-value in frame fMain = policy.stateID
    tResidential:checked = (policy.residential <> ? and policy.residential = true)
    .

/*   assign                                                       */
/*     bVoid:sensitive in frame framePolicy = (policy.stat = "I") */
/*     bSwap:sensitive in frame framePolicy = (policy.stat = "P") */
/*     .                                                          */

  assign
    tStatCode:screen-value in frame fMain = policy.statCode
    tStatCodeDesc:screen-value in frame fMain = policy.descStatCode
    tCountyID:screen-value in frame fMain = policy.countyID
    tCountyDesc:screen-value in frame fMain = policy.descCountyID
    tFormID:screen-value in frame fMain = policy.formID
    tFormDesc:screen-value in frame fMain = policy.descFormID
    .

  display
    tGrossPolicy
    tRetentionPolicy
    tNetPolicy
    tGrossEndorsement
    tRetentionEndorsement
    tNetEndorsement
    tGrossTotal
    tRetentionTotal
    tNetTotal
   with frame {&frame-name}.

  bViewFile:sensitive = (tAgentID:screen-value > "" and tFileNumber:screen-value > "").
  bEditFile:sensitive = (tAgentID:screen-value > "") and tFindPolicy:sensitive.

  dataSortBy = "".
  run sortData("seq").
  run setGLInfo.

  publish "SetCurrentValue" ("PolicyID", string(policy.policyID)).
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

