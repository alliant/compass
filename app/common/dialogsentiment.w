&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsentiment.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created:07/16/2020
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/sentiment.i}
{tt/sentiment.i &tableAlias=ttsentiment}


/* Parameters Definitions ---                                           */
define input  parameter iphfileDataSrv  as  handle.
define input  parameter ipcObjDesc      as  character  no-undo.
define input  parameter table         for sentiment.
define output parameter opisentimentID  as  integer    no-undo.
define output parameter oplSuccess      as  logical    no-undo.


/* Local Variable Definitions ---                                       */
define variable cTrackPerson     as character no-undo.
define variable iTrackType       as integer   no-undo.
define variable cTrackNotes      as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPrsnLookup edNotes Btn_Cancel tSentiment ~
bSnmtThObj1 bSnmtThObj2 bSnmtThObj3 bSnmtThObj4 bSnmtThObj5 
&Scoped-Define DISPLAYED-OBJECTS edNotes edObj fPerson fCreateDt tSentiment 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bPrsnLookup DEFAULT  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Person lookup".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 56 BY 4.52 NO-UNDO.

DEFINE VARIABLE edObj AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 56 BY 2.38 NO-UNDO.

DEFINE VARIABLE fCreateDt AS DATE FORMAT "99/99/99":U 
     LABEL "Created" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fPerson AS CHARACTER FORMAT "X(256)":U 
     LABEL "Person" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 51.4 BY 1 NO-UNDO.

DEFINE IMAGE bSnmtThObj1
     SIZE 5 BY .95 TOOLTIP "Terrible".

DEFINE IMAGE bSnmtThObj2
     SIZE 5 BY .95 TOOLTIP "Poor".

DEFINE IMAGE bSnmtThObj3
     SIZE 5 BY .95 TOOLTIP "Alright".

DEFINE IMAGE bSnmtThObj4
     SIZE 5 BY .95 TOOLTIP "Good".

DEFINE IMAGE bSnmtThObj5
     SIZE 5 BY .95 TOOLTIP "Great".

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 68 BY 14.76.

DEFINE VARIABLE tSentiment AS INTEGER INITIAL 3 
     VIEW-AS SLIDER MIN-VALUE 1 MAX-VALUE 5 HORIZONTAL NO-CURRENT-VALUE 
     TIC-MARKS TOP FREQUENCY 10
     SIZE 46 BY 1.43 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bPrsnLookup AT ROW 9.81 COL 66.8 WIDGET-ID 292 NO-TAB-STOP 
     edNotes AT ROW 11.24 COL 15 NO-LABEL WIDGET-ID 298
     edObj AT ROW 1.48 COL 15 NO-LABEL WIDGET-ID 300 NO-TAB-STOP 
     fPerson AT ROW 9.91 COL 13 COLON-ALIGNED WIDGET-ID 296 NO-TAB-STOP 
     Btn_OK AT ROW 18 COL 26
     Btn_Cancel AT ROW 18 COL 40.2
     fCreateDt AT ROW 16 COL 13 COLON-ALIGNED WIDGET-ID 308 NO-TAB-STOP 
     tSentiment AT ROW 7.86 COL 17 NO-LABEL WIDGET-ID 4 NO-TAB-STOP 
     "Objective:" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 1.71 COL 3.6 WIDGET-ID 302
     "Notes:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 11.24 COL 8.2 WIDGET-ID 304
     "My Sentiment" VIEW-AS TEXT
          SIZE 15.6 BY .62 AT ROW 4.43 COL 6 WIDGET-ID 18
          FONT 6
     "How do you feel about progress towards the objective?" VIEW-AS TEXT
          SIZE 52.4 BY .62 AT ROW 5.48 COL 15.6 WIDGET-ID 6
     RECT-1 AT ROW 4.81 COL 5 WIDGET-ID 16
     bSnmtThObj1 AT ROW 6.91 COL 18 WIDGET-ID 464
     bSnmtThObj2 AT ROW 6.91 COL 28.4 WIDGET-ID 466
     bSnmtThObj3 AT ROW 6.91 COL 38.4 WIDGET-ID 468
     bSnmtThObj4 AT ROW 6.91 COL 48.4 WIDGET-ID 472
     bSnmtThObj5 AT ROW 6.91 COL 58.6 WIDGET-ID 474
     SPACE(13.19) SKIP(11.99)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Sentiment"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR edObj IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edObj:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fCreateDt IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fCreateDt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fPerson IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fPerson:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Sentiment */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrsnLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrsnLookup Dialog-Frame
ON CHOOSE OF bPrsnLookup IN FRAME Dialog-Frame /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSnmtThObj1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtThObj1 Dialog-Frame
ON MOUSE-SELECT-CLICK OF bSnmtThObj1 IN FRAME Dialog-Frame
DO:
  tSentiment:screen-value = "1".
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSnmtThObj2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtThObj2 Dialog-Frame
ON MOUSE-SELECT-CLICK OF bSnmtThObj2 IN FRAME Dialog-Frame
DO:
  tSentiment:screen-value = "2".
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSnmtThObj3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtThObj3 Dialog-Frame
ON MOUSE-SELECT-CLICK OF bSnmtThObj3 IN FRAME Dialog-Frame
DO:
  tSentiment:screen-value = "3".
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSnmtThObj4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtThObj4 Dialog-Frame
ON MOUSE-SELECT-CLICK OF bSnmtThObj4 IN FRAME Dialog-Frame
DO:
  tSentiment:screen-value = "4".
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSnmtThObj5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSnmtThObj5 Dialog-Frame
ON MOUSE-SELECT-CLICK OF bSnmtThObj5 IN FRAME Dialog-Frame
DO:
  tSentiment:screen-value = "5".
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveSentiment in this-procedure.
  if not oplSuccess 
   then
    return no-apply.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes Dialog-Frame
ON VALUE-CHANGED OF edNotes IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPerson Dialog-Frame
ON VALUE-CHANGED OF fPerson IN FRAME Dialog-Frame /* Person */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSentiment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSentiment Dialog-Frame
ON VALUE-CHANGED OF tSentiment IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run displayData in this-procedure.
  bPrsnLookup:load-image              ("images/s-lookup.bmp").
  bPrsnLookup:load-image-insensitive  ("images/s-lookup-i.bmp").
  bSnmtThObj1:load-image("images/s-too sad-16").
  bSnmtThObj2:load-image("images/s-sad-16").
  bSnmtThObj3:load-image("images/s-neutral-16").
  bSnmtThObj4:load-image("images/s-smiling-16").
  bSnmtThObj5:load-image("images/s-lovingit-16").
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.
  
  find first sentiment no-error.
  if not available sentiment
   then 
    return.

  assign
    edObj:screen-value        = ipcObjDesc
    fPerson:screen-value      = sentiment.personName
    tSentiment:screen-value   = string(sentiment.type)
    edNotes:screen-value      = sentiment.notes
    fCreateDt:screen-value    = string(sentiment.createDate)
    opisentimentID            = sentiment.sentimentID
    fPerson:private-data      = sentiment.personID
    .
  
  if sentiment.sentimentID = 0
   then
    assign
      frame dialog-frame:title = "New sentiment"
      Btn_OK :label            = "Create"
      Btn_OK :tooltip          = "Create"
      fCreateDt:visible        = false
      .
   else
     assign
       frame dialog-frame:title = "Edit sentiment"
       Btn_OK :label            = "Save"
       Btn_OK :tooltip          = "Save"
       fCreateDt:visible        = true
       .

  assign
      cTrackPerson   = fPerson:private-data
      iTrackType     = tSentiment:input-value
      cTrackNotes    = edNotes:input-value
      .
  if sentiment.sentimentID = 0
   then
   apply 'mouse-select-click' to bSnmtThObj1.          

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if sentiment.sentimentID = 0
   then
    Btn_OK:sensitive = (tSentiment:input-value <> 0) no-error.
   else
    Btn_OK:sensitive =  (tSentiment:input-value <> 0) and
                        (fPerson:private-data  <> cTrackPerson  or
                         tSentiment:input-value <> iTrackType   or
                         edNotes:input-value <> cTrackNotes  
                         ) no-error
                         .
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY edNotes edObj fPerson fCreateDt tSentiment 
      WITH FRAME Dialog-Frame.
  ENABLE bPrsnLookup edNotes Btn_Cancel tSentiment bSnmtThObj1 bSnmtThObj2 
         bSnmtThObj3 bSnmtThObj4 bSnmtThObj5 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog Dialog-Frame 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  

  define variable cName         as character no-undo.
  define variable cCurrPersonID as character no-undo.
  define variable cPersonID     as character no-undo.
  
  cPersonID = "".
  if available sentiment
   then
    cCurrPersonID = sentiment.personID.
    
  run dialogpersonlookup.w(input  cCurrPersonID,
                           input  false,        /* True if add "New" */
                           input  false,        /* True if add "Blank" */
                           input  false,        /* True if add "ALL" */
                           output cPersonID,
                           output cName,
                           output std-lo).
   
  if not std-lo 
   then
    return no-apply.
    
  fPerson:screen-value = if cPersonID = "" then "" else cName . 
  fPerson:private-data = cPersonID.
  
  run enableDisableSave in this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSentiment Dialog-Frame 
PROCEDURE saveSentiment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table ttsentiment.  
  for first sentiment:
    create ttsentiment.
    buffer-copy sentiment to ttsentiment.
  end.

  assign
    ttsentiment.personID   = fPerson:private-data
    ttsentiment.notes      = edNotes:input-value
    ttsentiment.type       = tSentiment:input-value
    ttsentiment.personName = fPerson:screen-value
    .

  if sentiment.sentimentID = 0
   then
    run newsentiment in iphFileDataSrv (input table ttsentiment,
                                        output opisentimentID,
                                        output oplSuccess).

   else
    run modifysentiment in iphFileDataSrv (input table ttsentiment,
                                           output oplSuccess).
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

