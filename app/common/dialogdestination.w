&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  
  Modifications:
  Date        Name     Comments
  05/05/22   Sachin C  Task# 80191 modified the code to get actionDesc 
                       and destTypeDesc.
  09/15/23   Sagar K   Modified vendor related issue
  05/14/24   Sachin    Task #112772 Fixed Bug in system destination CRUD 
                        function in AR Contact screen
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Temp Table Definitions ---                                           */
{tt/sysdest.i}
{tt/sysprop.i}
{tt/sysdest.i &tableAlias="action"}
{tt/apvendor.i}
{lib/add-delimiter.i}

/* Parameters Definitions ---                                           */

define input        parameter lAgentContext as logical   no-undo. /* 'true' when called from AR contacts screen */
define input        parameter pNew          as logical   no-undo.
define input        parameter pEntity       as character no-undo.
define input        parameter pEntityID     as character no-undo.
define input        parameter pAgentName    as character no-undo.
define input-output parameter table         for sysdest.
define output       parameter pSave         as logical no-undo initial false.
            
/* Local Variable Definitions ---                                       */
define variable iHeight          as integer   no-undo.
define variable iHeightDialog    as integer   no-undo.
define variable cMsg             as character no-undo.
define variable cDestName        as character no-undo.
define variable cActionDesc      as character no-undo.
define variable cDestTypeDesc    as character no-undo. 
define variable cvendorlist      as character no-undo. 
define variable pVendorID        as character no-undo. 
define variable pAgentID         as character no-undo. 
define variable pAuditorID       as character no-undo.
define variable cAgentActionList as character no-undo.
define variable iCount           as integer   no-undo.

/* Standard Libraries ---                                               */
{lib/std-def.i}
{lib/validEmailAddr.i}
{lib/set-button-def.i}
{lib/text-align.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tDestNameCombo tCombo tDestLabel ~
tDestNameLabel tEntityIDLabel tEntity tAction tEntityID bAgentLookup tDest ~
tDestName bCreate bCancel tvendor rEntity rDest 
&Scoped-Define DISPLAYED-OBJECTS tDestNameCombo tCombo tDestLabel ~
tDestNameLabel tEntityIDLabel tEntity tAction tEntityID tAgentName tDest ~
tDestName rDestLabel tvendor 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel"
     BGCOLOR 8 .

DEFINE BUTTON bCreate AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create"
     BGCOLOR 8 .

DEFINE VARIABLE tAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 57 BY 1 NO-UNDO.

DEFINE VARIABLE tCombo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 84 BY 1 NO-UNDO.

DEFINE VARIABLE tDest AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDestNameCombo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 57 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tvendor AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 84 BY 1 NO-UNDO.

DEFINE VARIABLE rDestLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Destination" 
      VIEW-AS TEXT 
     SIZE 11.2 BY .81 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 64 BY 1 NO-UNDO.

DEFINE VARIABLE tDestLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Type:" 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE VARIABLE tDestName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 57 BY 1 TOOLTIP "Enter semicolon (;) sapearted valid email addresses" NO-UNDO.

DEFINE VARIABLE tDestNameLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Name:" 
      VIEW-AS TEXT 
     SIZE 6.4 BY .62 NO-UNDO.

DEFINE VARIABLE tEntityID AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEntityIDLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Entity ID:" 
      VIEW-AS TEXT 
     SIZE 9.6 BY .62 NO-UNDO.

DEFINE RECTANGLE rDest
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 97 BY 2.62.

DEFINE RECTANGLE rEntity
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 97 BY 3.81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tDestNameCombo AT ROW 6.48 COL 39 COLON-ALIGNED NO-LABEL WIDGET-ID 444
     tCombo AT ROW 3.62 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 442
     tDestLabel AT ROW 6.67 COL 6 COLON-ALIGNED NO-LABEL WIDGET-ID 434
     tDestNameLabel AT ROW 6.67 COL 32.2 COLON-ALIGNED NO-LABEL WIDGET-ID 436
     tEntityIDLabel AT ROW 3.81 COL 2 COLON-ALIGNED NO-LABEL WIDGET-ID 438
     tEntity AT ROW 2.43 COL 12 COLON-ALIGNED WIDGET-ID 6
     tAction AT ROW 2.43 COL 39 COLON-ALIGNED WIDGET-ID 8
     tEntityID AT ROW 3.62 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 102
     bAgentLookup AT ROW 3.52 COL 28.4 WIDGET-ID 350
     tAgentName AT ROW 3.62 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 424 NO-TAB-STOP 
     tDest AT ROW 6.48 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 22
     tDestName AT ROW 6.48 COL 41 NO-LABEL WIDGET-ID 40
     bCreate AT ROW 8.91 COL 35.6 WIDGET-ID 428
     bCancel AT ROW 8.91 COL 52.2 WIDGET-ID 426
     rDestLabel AT ROW 5.39 COL 2.2 COLON-ALIGNED NO-LABEL WIDGET-ID 430 NO-TAB-STOP 
     tvendor AT ROW 3.61 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 446
     "Entity Information" VIEW-AS TEXT
          SIZE 16.8 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 4
     rEntity AT ROW 1.48 COL 3 WIDGET-ID 2
     rDest AT ROW 5.76 COL 3 WIDGET-ID 12
     SPACE(1.79) SKIP(2.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Output Destination" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN rDestLabel IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       rDestLabel:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN tAgentName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tAgentName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN tDestName IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Output Destination */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup Dialog-Frame
ON CHOOSE OF bAgentLookup IN FRAME Dialog-Frame /* agentlookup */
DO:
  define variable cEntityID  as character no-undo.
  define variable cName      as character no-undo.
  
  if tEntity:input-value = "G"
   then
    do:
      run dialogagentlookup.w(input tEntityID:input-value,
                              input "",        /* Selected State ID */
                              input false,     /* Allow 'ALL' */
                              output cEntityID,
                              output std-ch, /* Agent state ID */
                              output cName,
                              output std-lo).
   
      if not std-lo or tEntityID:input-value = cEntityID  
       then
        return no-apply.
     
      assign
        tEntityID:screen-value  = cEntityID
        tAgentName:screen-value = cName
        .
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCreate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCreate Dialog-Frame
ON CHOOSE OF bCreate IN FRAME Dialog-Frame /* Create */
DO:
  pSave = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCombo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCombo Dialog-Frame
ON VALUE-CHANGED OF tCombo IN FRAME Dialog-Frame
DO:
  tEntityID:screen-value = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDest Dialog-Frame
ON VALUE-CHANGED OF tDest IN FRAME Dialog-Frame
DO:
  define variable tProperty as character no-undo.

  case self:screen-value:
   when "S" then
    do:
      {lib/get-sysprop-list.i &combo=tDestNameCombo &appCode="'SYS'" &objAction="'DestType'" &objProperty=self:screen-value}
      assign
        tDestName:hidden      = true
        tDestNameCombo:hidden = false
        .
      apply "VALUE-CHANGED" to tDestNameCombo.
    end.
   otherwise
    assign
      tDestName:screen-value = (if pNew then "" else tDestName:screen-value)
      tDestName:hidden       = false
      tDestNameCombo:hidden  = true
      .
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDestName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDestName Dialog-Frame
ON LEAVE OF tDestName IN FRAME Dialog-Frame
DO:
  assign
   cDestName = "".
  
  if tDest:input-value = "E" and 
     tDestName:input-value <> "" 
   then
    do:
      tDestName:screen-value = replace(replace(trim(tDestName:input-value),",",";")," ",";").
      
      if lookup("", tDestName:screen-value, ";") > 0
       then
        do std-in = 1 to num-entries(tDestName:screen-value, ";"):
         if entry(std-in,tDestName:screen-value, ";") = ""
          then
           next.
         cDestName = cDestName + entry(std-in,tDestName:screen-value, ";") + ";".  
        end.
        cDestName = trim(cDestName, ";").
        
      if cDestName <> ""
       then
        tDestName:screen-value = cDestName. 
      
      if not validEmailAddr(tDestName:screen-value)
       then
        do:
         message "Invalid Email Address" view-as alert-box warning buttons ok.
         return no-apply.
        end.
    end.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDestNameCombo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDestNameCombo Dialog-Frame
ON VALUE-CHANGED OF tDestNameCombo IN FRAME Dialog-Frame
DO:
  tDestName:screen-value = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntity Dialog-Frame
ON VALUE-CHANGED OF tEntity IN FRAME Dialog-Frame /* Entity */
DO:
  std-lo = (self:screen-value = "C").
  if not std-lo
   then frame {&frame-name}:height-pixels = iHeightDialog.

  do with frame {&frame-name}:
    assign
      /* rearrange the widgets the dialog box */
      std-in                      = tEntityID:height-pixels + 4
      rEntity:height-pixels       = iHeight + (if std-lo then std-in * -1 else 0)
      rDestLabel:y                = rEntity:y + rEntity:height-pixels + 8
      rDest:y                     = rEntity:y + rEntity:height-pixels + 15
      tDestLabel:y                = rDest:y + 20
      tDest:y                     = rDest:y + 16
      tDestNameLabel:y            = tDestLabel:y
      tDestName:y                 = tDest:y
      tDestNameCombo:y            = tDest:y
      bCreate:y                   = tDestName:y + 48.5
      bCancel:y                   = tDestName:y + 48.5
      /* change the height */
      frame {&frame-name}:height-pixels = iHeightDialog + (if std-lo then std-in * -1 else 0)
      .
  
    case self:screen-value:
     when "G" then
      assign
        tEntityIDLabel:hidden         = false
        tEntityIDLabel:screen-value   = "Agent ID:"
        tEntityID:hidden              = false
        tEntityID:screen-value        = pAgentID
        bAgentLookup:hidden           = false
        tAgentName:hidden             = false
        tCombo:hidden                 = true
        tvendor:hidden                = true no-error
        .
     when "A" then
      assign
        tEntityIDLabel:hidden       = false
        tEntityIDLabel:screen-value = "Auditor:"
        tEntityID:hidden            = true
        bAgentLookup:hidden         = true
        tAgentName:hidden           = true
        tCombo:hidden               = false
        tCombo:screen-value         = pAuditorID
        tvendor:hidden              = true no-error
        .
     when "V" then
      do:
        run displayvendor in this-procedure.
        assign
          tEntityIDLabel:hidden       = false
          tEntityIDLabel:screen-value = "Vendor:"
          tEntityID:hidden            = true
          bAgentLookup:hidden         = true
          tAgentName:hidden           = true
          tCombo:hidden               = true
          tvendor:hidden              = false
          tvendor:screen-value        = pVendorID  no-error
        .
        
      end.
     otherwise
      assign
        tEntityID:hidden            = true
        tEntityIDLabel:hidden       = true
        bAgentLookup:hidden         = true
        tAgentName:hidden           = true
        tCombo:hidden               = true
        tvendor:hidden              = true 
        tEntityID:screen-value      = "" no-error
        .
    end case.
    rightAlignText(tEntityIDLabel:handle).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEntityID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntityID Dialog-Frame
ON LEAVE OF tEntityID IN FRAME Dialog-Frame
DO:
  define variable cName  as character no-undo.
  
  if tEntity:input-value = "G"
   then
    do:
      if tEntityID:input-value <> ""
       then
        do:
          publish "GetAgentName" (input tEntityID:input-value,
                                  output cName,
                                  output std-lo).
  
          if not std-lo 
           then 
            do:
              assign 
                  tEntityID:screen-value  = "" 
                  tAgentName:screen-value = ""
                  .
              return no-apply.
            end.
        end.
  
      tAgentName:screen-value = cName.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntityID Dialog-Frame
ON VALUE-CHANGED OF tEntityID IN FRAME Dialog-Frame
DO:
  tAgentName:screen-value = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tvendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tvendor Dialog-Frame
ON VALUE-CHANGED OF tvendor IN FRAME Dialog-Frame
DO:
  tEntityID:screen-value = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

if lAgentContext
 then
  do:
     cAgentActionList = 'Agent Statement^arAgentStatementQuery^A/R Invoice^invoiceStatementQuery'.
     
     if not pNew
      then
       for first sysdest no-lock:
         if lookup(sysdest.action, cAgentActionList, '^') = 0
          then
           cAgentActionList = cAgentActionList + '^' + sysdest.actiondesc + '^' + sysdest.action.
       end.
     
     do iCount = 1 to num-entries(cAgentActionList,'^'):
       create action.
       assign
           action.displayName = entry(iCount, cAgentActionList, '^')
           action.action      = entry(iCount + 1, cAgentActionList, '^')
           .
       iCount = iCount + 1.
       
       if iCount > num-entries(cAgentActionList,'^')
        then 
         leave.
     end.
  end.
 else
  publish "GetSysActionDests" (output table action).

tAction:delete(1).

for each action no-lock:
  tAction:add-last(action.displayName, action.action).
end.
tAction:screen-value = entry(2, tAction:list-item-pairs).
{lib/get-sysprop-list.i &combo=tEntity &appCode="'SYS'" &objAction="'Destination'" &objProperty="'Entity'" &d="'C'"}
{lib/get-sysprop-list.i &combo=tDest   &appCode="'SYS'" &objAction="'Destination'" &objProperty="'Type'"}
{lib/get-sysprop-list.i &combo=tCombo  &appCode="'QAR'" &objAction="'Auditor'"}

{lib/set-button.i &label="AgentLookup" &image="images/s-lookup.bmp" &inactive="images/s-lookup-i.bmp"}
setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  if not std-lo
   then bCreate:sensitive = false.
  
  assign
    iHeight                   = rEntity:height-pixels
    iHeightDialog             = frame {&frame-name}:height-pixels
    frame {&frame-name}:title = (if not can-find(first sysdest) then "New Destination" else if pNew then "Copy Destination" else "Modify Destination")
    bCreate:label             = (if pNew then "Create" else "Save")
    .
  for first sysdest no-lock:
    assign
      tEntity:screen-value      = sysdest.entityType
      tEntity:sensitive         = pNew
      tAction:screen-value      = if sysdest.action = "" then "ALL" else sysdest.action
      tAction:sensitive         = pNew
      tEntityID:sensitive       = pNew
      bAgentLookup:sensitive    = pNew
      tDest:screen-value        = sysdest.destType
      tDest:sensitive           = pNew
      tDestName:screen-value    = sysdest.destName
      tvendor:screen-value      = if sysdest.entityName = "Unknown" then "ALL" else sysdest.entityID
      tCombo:sensitive          = pNew
      tvendor:sensitive         = pNew  no-error .
      if tEntity:screen-value = "G"
       then
        tAgentName:screen-value   = if sysdest.entityName = "Unknown"  then "ALL" else sysdest.entityName   
      no-error  
      .
    if sysdest.entityType <> "C" and sysdest.entityID <> ""
     then
      pEntityID = if lAgentContext then pEntityID else sysdest.entityID.

    if sysdest.entityType = "V"
     then
       pVendorID  = sysdest.entityID.
     else if sysdest.entityType = "G"
      then
        pAgentID = sysdest.entityID.
     else if sysdest.entityType = "A"
      then
        pAuditorID = sysdest.entityID.
  end.
  
  if pEntity > ""
   then tEntity:screen-value = pEntity.
  
  apply "VALUE-CHANGED" to tEntity.
  apply "VALUE-CHANGED" to tDest.
  
  if lAgentContext
   then
    do:
      assign
          tEntity:sensitive      = false
          tEntityID:sensitive    = false
          tEntityID:screen-value = pEntityID
          bAgentLookup:sensitive = false
          .
    end.
    
  apply "leave" to tEntityID.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  if pNew and not can-find(first sysdest)
   then create sysdest.
  
  for first action where action.action = tAction:input-value :
    cActionDesc = action.displayName. 
  end.
  
  if lookup(tDest:input-value,tDest:list-item-pairs) > 0
   then
    cdestTypeDesc = entry((lookup(tDest:input-value,tDest:list-item-pairs) - 1),tDest:list-item-pairs).

  for first sysdest exclusive-lock:
    assign
      sysdest.entityType   = tEntity:input-value 
      sysdest.action       = tAction:input-value
      sysdest.destType     = tDest:input-value
      sysdest.destName     = tDestName:input-value  
      sysdest.actionDesc   = cActionDesc
      sysdest.destTypeDesc = cDestTypeDesc  
      sysdest.destNameDesc = "".
      if tEntity:screen-value <> "C" 
       then
        do:
          if tEntity:screen-value = "G" 
           then
            sysdest.entityID     = tEntityID:screen-value .
           else if tEntity:screen-value = "A" 
            then 
             sysdest.entityID     = tCombo:screen-value .
           else if tEntity:screen-value = "V" 
            then 
             sysdest.entityID     = tvendor:screen-value .
        end.
        
       else
        assign
          sysdest.entityID     = "" 
          sysdest.entityName   = "".
      if tEntity:screen-value = "G"
        then
         sysdest.entityName   = tAgentName:input-value.
      if sysdest.entityType = "V"
       then
        sysdest.entityDesc = "Vendor".
       else if sysdest.entityType = "G"
        then
         sysdest.entityDesc = "Agent".
       else if sysdest.entityType = "A"
        then
         sysdest.entityDesc = "Auditor".
       else
        sysdest.entityDesc = "Company".
      
  end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayvendor Dialog-Frame 
PROCEDURE displayvendor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  publish "GetAllVendors" (input "B", output table apvendor).
  do with frame {&frame-name}:
  end.
  
  assign tvendor:delimiter = ";".
         
  for each apvendor no-lock:
     cvendorlist = addDelimiter(cvendorlist, ";") + trim(apvendor.vendorname) + " (" + string(trim(apvendor.vendorid)) + ")" + ";" + string(trim(apvendor.vendorid)).
  end.

  tvendor:list-item-pairs  = "All" + ";"  + "All" + (if cvendorlist  = "" then ""  else (";" + cvendorlist)).
  
  tvendor:screen-value          = "All" .
  
  if not pNew 
   then
    for first sysdest:
    if sysdest.entityName <> "Unknown"
     then
      tvendor:screen-value = sysdest.entityID.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDestNameCombo tCombo tDestLabel tDestNameLabel tEntityIDLabel tEntity 
          tAction tEntityID tAgentName tDest tDestName rDestLabel tvendor 
      WITH FRAME Dialog-Frame.
  ENABLE tDestNameCombo tCombo tDestLabel tDestNameLabel tEntityIDLabel tEntity 
         tAction tEntityID bAgentLookup tDest tDestName bCreate bCancel tvendor 
         rEntity rDest 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

