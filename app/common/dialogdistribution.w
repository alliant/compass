&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDist
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDist 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/distribution.i}
{tt/distribution.i &tableAlias="contact"}

/* Parameters Definitions ---                                           */
define input        parameter pAction as character no-undo.
define input-output parameter table   for distribution.
define       output parameter pCancel as logical   no-undo initial true.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/set-button-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDist

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bContacts tName tDescription bOk bCancel 
&Scoped-Define DISPLAYED-OBJECTS tName tDescription 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bContacts  NO-FOCUS
     LABEL "Contacts" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add Contacts".

DEFINE BUTTON bOk AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 47 BY 4.05
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 41 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDist
     bContacts AT ROW 1.38 COL 57.2 WIDGET-ID 6 NO-TAB-STOP 
     tName AT ROW 1.48 COL 13 COLON-ALIGNED WIDGET-ID 2
     tDescription AT ROW 2.91 COL 15 NO-LABEL WIDGET-ID 8
     bOk AT ROW 7.43 COL 17.2
     bCancel AT ROW 7.43 COL 33.8
     "Description:" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 3.19 COL 3.2 WIDGET-ID 10
     SPACE(49.39) SKIP(5.23)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Distribution"
         DEFAULT-BUTTON bOk CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDist
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fDist:SCROLLABLE       = FALSE
       FRAME fDist:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDist
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDist fDist
ON WINDOW-CLOSE OF FRAME fDist /* Distribution */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContacts fDist
ON CHOOSE OF bContacts IN FRAME fDist /* Contacts */
DO:
  define variable lCancel as logical no-undo initial true.
  
  std-ch = tName:screen-value.
  if std-ch = ""
   then
    do:
      message "Please add a distribution name" view-as alert-box warning.
      apply "ENTRY" to tName.
      return no-apply.
    end.

  empty temp-table contact.
  for each distribution no-lock:
    create contact.
    buffer-copy distribution to contact.
    contact.isSelected = true.
    release contact.
  end.
  
  run dialogcontacts.w (input std-ch, input-output table contact, output lCancel).
  
  empty temp-table distribution.
  for each contact no-lock:
    create distribution.
    buffer-copy contact to distribution.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOk
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOk fDist
ON CHOOSE OF bOk IN FRAME fDist /* Save */
DO:
  std-lo = true.
  publish "IsDistributionCreated" (tName:screen-value, output std-lo).
  if pAction <> "Modify" and std-lo
   then
    do:
      message "There is already a distribution with the name " + tName:screen-value view-as alert-box warning.
      apply "ENTRY" to tName.
      return no-apply.
    end.
    
  pCancel = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName fDist
ON LEAVE OF tName IN FRAME fDist /* Name */
DO:
  std-lo = true.
  publish "IsDistributionCreated" (self:screen-value, output std-lo).
  if pAction <> "Modify" and std-lo
   then
    do:
      message "There is already a distribution with the name " + tName:screen-value view-as alert-box warning.
      apply "ENTRY" to self.
      return no-apply.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName fDist
ON VALUE-CHANGED OF tName IN FRAME fDist /* Name */
DO:
  assign
    std-lo              = self:screen-value = ""
    bContacts:sensitive = not std-lo
    bOk:sensitive       = not std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDist 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/set-button.i &label="Contacts" &image="images/s-useradd.bmp" &inactive="images/s-useradd-i.bmp"}
setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  for first distribution no-lock:
    assign
      std-lo                    = pAction = "Modify"
      tName:screen-value        = distribution.distName
      tName:read-only           = if std-lo then true else false
      tName:tab-stop            = if std-lo then false else true
      tDescription:screen-value = distribution.description
      .
  end.
  frame fDist:title = pAction + " Distribution".
  apply "VALUE-CHANGED" to tName.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if not can-find(first distribution)
   then create distribution.
   
  std-lo = can-find(first distribution where personContactID > 0).
  for each distribution exclusive-lock:
    if std-lo and distribution.personContactID = 0
     then delete distribution.
     else
      assign
        distribution.distName    = tName:screen-value
        distribution.description = tDescription:screen-value
        .
  end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDist  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDist.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDist  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tName tDescription 
      WITH FRAME fDist.
  ENABLE bContacts tName tDescription bOk bCancel 
      WITH FRAME fDist.
  VIEW FRAME fDist.
  {&OPEN-BROWSERS-IN-QUERY-fDist}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

