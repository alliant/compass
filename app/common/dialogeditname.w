&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogeditname.w 

  Description: Dialog to edit name.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Naresh Chopra

  Created:05.05.2018 
  Modified
  Date           Name           Description
  03/04/2019     Shubham        Moified for Phase 2.
  03/25/2022     Sachin C       Task #92363 Bug Fix edited name displaying 
                                in the fill-in(fname).
  08/14/2023     Sagar K        Added server Call to save person name
  04/04/2024     SB             Task# 111689 Modified to remove the reference of title
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
/* Temp-table Definitions ---                                           */

/* Parameters Definitions ---                                           */
define input-output parameter ioPersonID      as character no-undo.
define input-output parameter iopcFirstName   as character no-undo.
define input-output parameter iopcMiddleName  as character no-undo.
define input-output parameter iopcLastName    as character no-undo.
define input-output parameter iopcDisplayName as character no-undo.
define output parameter opsuccess  as logical no-undo.

/* Local Variable Definitions ---                                       */
define variable iCount as integer no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSaveName fFirstName fMiddleName fLastName ~
fDisplayName bCancel 
&Scoped-Define DISPLAYED-OBJECTS fFirstName fMiddleName fLastName ~
fDisplayName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 8.6 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSaveName AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 8.6 BY 1.14 TOOLTIP "Save"
     BGCOLOR 8 .

DEFINE VARIABLE fDisplayName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Display" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fFirstName AS CHARACTER FORMAT "X(256)":U 
     LABEL "First" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fLastName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Last" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fMiddleName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Middle" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bSaveName AT ROW 6.19 COL 12.2 NO-TAB-STOP 
     fFirstName AT ROW 1.57 COL 8.6 COLON-ALIGNED WIDGET-ID 2
     fMiddleName AT ROW 2.67 COL 8.6 COLON-ALIGNED WIDGET-ID 6
     fLastName AT ROW 3.76 COL 8.6 COLON-ALIGNED WIDGET-ID 4
     fDisplayName AT ROW 4.86 COL 8.6 COLON-ALIGNED WIDGET-ID 10
     bCancel AT ROW 6.19 COL 22.8 NO-TAB-STOP 
     SPACE(10.59) SKIP(0.23)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Edit Name"
         DEFAULT-BUTTON bSaveName CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Edit Name */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSaveName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSaveName Dialog-Frame
ON CHOOSE OF bSaveName IN FRAME Dialog-Frame /* Save */
do:
  run editPersonName in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDisplayName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDisplayName Dialog-Frame
ON VALUE-CHANGED OF fDisplayName IN FRAME Dialog-Frame /* Display */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fFirstName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fFirstName Dialog-Frame
ON VALUE-CHANGED OF fFirstName IN FRAME Dialog-Frame /* First */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLastName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLastName Dialog-Frame
ON VALUE-CHANGED OF fLastName IN FRAME Dialog-Frame /* Last */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fMiddleName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMiddleName Dialog-Frame
ON VALUE-CHANGED OF fMiddleName IN FRAME Dialog-Frame /* Middle */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
 THEN 
  FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.
  
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
    
  assign  
        fFirstName :screen-value  = iopcFirstName
        fMiddleName:screen-value  = iopcMiddleName
        fLastName  :screen-value  = iopcLastName
        fDisplayName:screen-value = iopcDisplayName
        .

  run enableDisableSave in this-procedure.
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE editPersonName Dialog-Frame 
PROCEDURE editPersonName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable errmsg  as  character  no-undo.
  do with frame {&frame-name}:
  end.

  assign 
        iopcFirstName   = fFirstName :input-value
        iopcMiddleName  = fMiddleName:input-value
        iopcLastName    = fLastName  :input-value
        iopcDisplayName = fDisplayName:input-value
        .
 publish "editpersonname" (input iopcFirstName,
                           input ioPersonID,
                           input iopcLastName,
                           input iopcMiddleName,
                           input iopcDisplayName,
                           output opsuccess,
                           output errmsg).
 publish "refreshpersonnames" (input opsuccess).   
                                
  if not opsuccess 
   then
    do:
       MESSAGE errmsg
           VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
       return .
    end.
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.

bSaveName:sensitive = not ( fFirstName:input-value = "" or 
                            (fFirstName:input-value   = iopcFirstName or fFirstName:input-value = ?) and 
                             fMiddleName:input-value  = iopcMiddleName and 
                             fLastName:input-value    = iopcLastName   and 
                             fDisplayName:input-value = iopcDisplayName).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fFirstName fMiddleName fLastName fDisplayName 
      WITH FRAME Dialog-Frame.
  ENABLE bSaveName fFirstName fMiddleName fLastName fDisplayName bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

