&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*---------------------------------------------------------------------
@name Link Agent
@description Link Agent to person

@author Sagar Koralli
@version 1.0
@created 07/11/2023
@notes 
@modified
---------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* --Temp-Table Definitions ---                                         */
{tt/personagentdetails.i}

/* Parameters Definitions ---                                           */
define input  parameter ipcActionType       as character no-undo.
define input  parameter ipcPersonID         as character no-undo.
define input  parameter table               for personagentdetails   .
define output parameter chAgentID           as character   no-undo.
define output parameter oplSuccess          as logical   no-undo.
/* Local Variable Definitions ---                                       */
define variable chJobTitle   as character no-undo.
define variable chNotes      as character no-undo.
{lib/std-def.i}
{lib/add-delimiter.i}
{lib/find-widget.i}
{tt/agent.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAgentID fagentID fjobTitle edNotes Btn_Link ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tAgentID fagentID fjobTitle edNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 14 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON Btn_Link AUTO-GO 
     LABEL "Link" 
     SIZE 14 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE tAgentID AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 69.6 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 69.6 BY 2.83 NO-UNDO.

DEFINE VARIABLE fagentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 69.6 BY .96 NO-UNDO.

DEFINE VARIABLE fjobTitle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Title" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tAgentID AT ROW 1.26 COL 9.4 COLON-ALIGNED WIDGET-ID 4
     fagentID AT ROW 1.3 COL 9.4 COLON-ALIGNED WIDGET-ID 12 NO-TAB-STOP 
     fjobTitle AT ROW 2.35 COL 9.4 COLON-ALIGNED WIDGET-ID 6
     edNotes AT ROW 3.44 COL 11.4 NO-LABEL WIDGET-ID 8
     Btn_Link AT ROW 6.78 COL 28.2
     Btn_Cancel AT ROW 6.78 COL 42.8
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .61 AT ROW 3.57 COL 4.6 WIDGET-ID 10
     SPACE(71.99) SKIP(4.11)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Link Agent"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       fagentID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Link Agent */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Link
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Link Dialog-Frame
ON CHOOSE OF Btn_Link IN FRAME Dialog-Frame /* Link */
DO:
  run savepersonlinkagent in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes Dialog-Frame
ON VALUE-CHANGED OF edNotes IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fjobTitle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fjobTitle Dialog-Frame
ON VALUE-CHANGED OF fjobTitle IN FRAME Dialog-Frame /* Job Title */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID Dialog-Frame
ON VALUE-CHANGED OF tAgentID IN FRAME Dialog-Frame /* Agent */
DO:
   run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/win-icon.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
   
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

tAgentID:delimiter = {&msg-dlm}.
if ipcActionType = 'New' 
 then
   do:
     {lib/get-agent-list.i &combo=tAgentID &frame-name=Dialog-Frame}
     
   end.
 
       
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
  end.
  run displayData       in this-procedure.

  run enableDisableSave in this-procedure.
  if ipcActionType = 'New' 
   then
     apply "Entry" to tAgentID in frame dialog-frame  .   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow Dialog-Frame 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    APPLY "END-ERROR":U TO SELF.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if ipcActionType = 'Modify' 
   then
    do:
      fagentID:hidden = false.
      tAgentID:hidden = true.

      find first personagentdetails no-error. 
      if available personagentdetails 
       then
        do:
          
          assign 
              frame Dialog-Frame:title          = "Modify Association"
              Btn_Link          :label          = "Save"
              Btn_Link          :tooltip        = "Save"
              fagentID         :screen-value   = personagentdetails.name + " (" + personagentdetails.agentID + ")"
              fjobTitle        :screen-value   = personagentdetails.jobTitle 
              edNotes          :screen-value   = personagentdetails.notes
              no-error.
        end.  /* if available personcontact */
        chAgentID  = personagentdetails.agentID  .
    end.
  else if ipcActionType = 'New' 
   then
    do:
     tAgentID:hidden = false.
     assign
         frame Dialog-Frame:title          = "Link Agent"
         Btn_Link          :label          = "Link"  
         Btn_Link          :tooltip        = "Link" 
         .  
    end. /* else */
  assign
    
    chJobTitle = fjobTitle        :screen-value   
    chNotes    = edNotes          :screen-value   .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if ipcActionType = 'New'
   then
    do:
      Btn_Link:sensitive = if (tAgentID:screen-value ne "ALL") then true else false. 
    end.
   else
      Btn_Link:sensitive = not(chJobTitle = fjobTitle:screen-value and
                               chNotes = edNotes:screen-value ) no-error.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAgentID fagentID fjobTitle edNotes 
      WITH FRAME Dialog-Frame.
  ENABLE tAgentID fagentID fjobTitle edNotes Btn_Link Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savepersonlinkagent Dialog-Frame 
PROCEDURE savepersonlinkagent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  define  variable iposition  as  integer     no-undo.
  define  variable cValue     as  character   no-undo.
  
  if ipcActionType = 'New'
   then
     do: 
       std-ha = GetWidgetByName(tAgentID:frame, "tAgentIDAgentText").
  
       iposition = r-index(std-ha:screen-value, "(" ).
  
       cValue = substring(std-ha:screen-value,iposition + 1,6). 

       if cValue <> tAgentID:input-value
        then
         return.
       empty temp-table personagentdetails.
       create personagentdetails.
       assign
           personagentdetails.personID = ipcPersonID
           personagentdetails.jobTitle = fjobTitle:input-value
           personagentdetails.Notes    = edNotes:input-value
           personagentdetails.agentID  = tAgentID:input-value .
           chAgentID                   = tAgentID:input-value .
     end. 
   else if ipcActionType = 'Modify'
    then
     do:
       for first personagentdetails no-lock where  personagentdetails.agentID = chAgentID :
         assign
             personagentdetails.personID = ipcPersonID
             personagentdetails.jobTitle = fjobTitle:input-value
             personagentdetails.Notes    = edNotes:input-value.
       end.
     end.  
  
  if ipcActionType = 'New'
   then
    do:
      if tAgentID:input-value = "ALL" or tAgentID:input-value = ? 
       then
        do:
          message "AgentID cannot be blank"
             view-as alert-box error buttons ok.
          return. 
        end.
        publish "newpersonagent"(input table  personagentdetails,
                                 output oplSuccess,
                                 output std-ch ).
        publish "refreshpersonnames" (input oplSuccess).   
    end.
   else if ipcActionType = 'Modify'
    then
      
      publish "modifypersonagent"(input table  personagentdetails,
                                  output oplSuccess,
                                  output std-ch ).
  if oplSuccess
    then run CloseWindow in this-procedure.
  else
    MESSAGE   std-ch
        VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
                             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

