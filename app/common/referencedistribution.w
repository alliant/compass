&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName="contact"}
{lib/brw-multi-def.i}
define variable tGroupName   as character no-undo.
define variable tContactName as character no-undo.
define variable dCol         as decimal   no-undo.

/* Temp Table Definitions ---                                           */
{tt/distribution.i}
{tt/distribution.i &tableAlias="singledist"}
{tt/distribution.i &tableAlias="contact"}
{tt/distribution.i &tableAlias="tempcontact"}

/* Functions ---                                                        */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwContacts

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES contact distribution

/* Definitions for BROWSE brwContacts                                   */
&Scoped-define FIELDS-IN-QUERY-brwContacts contact.isSelected contact.contactName contact.address contact.agentName contact.agentStatDesc contact.agentStateDesc contact.email   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwContacts   
&Scoped-define SELF-NAME brwContacts
&Scoped-define QUERY-STRING-brwContacts FOR EACH contact
&Scoped-define OPEN-QUERY-brwContacts OPEN QUERY {&SELF-NAME} FOR EACH contact.
&Scoped-define TABLES-IN-QUERY-brwContacts contact
&Scoped-define FIRST-TABLE-IN-QUERY-brwContacts contact


/* Definitions for BROWSE brwDistributions                              */
&Scoped-define FIELDS-IN-QUERY-brwDistributions distribution.distName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDistributions   
&Scoped-define SELF-NAME brwDistributions
&Scoped-define QUERY-STRING-brwDistributions FOR EACH distribution
&Scoped-define OPEN-QUERY-brwDistributions OPEN QUERY {&SELF-NAME} FOR EACH distribution.
&Scoped-define TABLES-IN-QUERY-brwDistributions distribution
&Scoped-define FIRST-TABLE-IN-QUERY-brwDistributions distribution


/* Definitions for FRAME fContacts                                      */

/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bDistCopy fDistSearch brwDistributions ~
bDistDelete bDistEmail bDistModify bDistNew bDistRefresh 
&Scoped-Define DISPLAYED-OBJECTS fDistSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD changeButtonState C-Win 
FUNCTION changeButtonState RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getContact C-Win 
FUNCTION getContact RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getDistribution C-Win 
FUNCTION getDistribution RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshContacts C-Win 
FUNCTION refreshContacts RETURNS LOGICAL
  ( input pName       as character,
    input pFromServer as logical  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshDistributions C-Win 
FUNCTION refreshDistributions RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD selectAllFilter C-Win  _DB-REQUIRED
FUNCTION selectAllFilter RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setContact C-Win 
FUNCTION setContact RETURNS CHARACTER
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setDistribution C-Win 
FUNCTION setDistribution RETURNS CHARACTER
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setDistributionRow C-Win 
FUNCTION setDistributionRow RETURNS LOGICAL
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwContacts 
       MENU-ITEM m_Contact_Add  LABEL "Add a new Contact (CTRL-A)"
       MENU-ITEM m_Contact_Delete LABEL "Delete the checked Contacts (CTRL-D)"
       RULE
       MENU-ITEM m_Contact_Export LABEL "Export the checked Contacts (CTRL-X)"
       MENU-ITEM m_Contact_Email LABEL "Email the checked Contacts (CTRL-E)"
       RULE
       MENU-ITEM m_Contact_Person LABEL "View the Person of the selected Contact (CTRL-P)".

DEFINE MENU POPUP-MENU-brwDistributions 
       MENU-ITEM m_Dist_Add     LABEL "Create a new Group (CTRL-A)"
       MENU-ITEM m_Dist_Modify  LABEL "Edit the selected Group (CTRL-M)"
       MENU-ITEM m_Dist_Delete  LABEL "Delete the selected Group (CTRL-D)"
       MENU-ITEM m_Dist_Copy    LABEL "Copy the contacts of the selected Group to a new Group (CTRL-C)"
       RULE
       MENU-ITEM m_Dist_Refresh LABEL "Refresh the Groups (CTRL-R)"
       MENU-ITEM m_Dist_Email   LABEL "Email all contacts in the selected Group (CTRL-E)".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bContactClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 4.8 BY 1.14 TOOLTIP "Clear the Search criteria".

DEFINE BUTTON bContactDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the checked Contacts".

DEFINE BUTTON bContactEmail  NO-FOCUS
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Email the checked Contacts".

DEFINE BUTTON bContactExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export the checked Contacts".

DEFINE BUTTON bContactNew  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a new Contact".

DEFINE BUTTON bContactView  NO-FOCUS
     LABEL "View" 
     SIZE 4.8 BY 1.14 TOOLTIP "View the Person of the selected Contact".

DEFINE VARIABLE fContactStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fContactStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fContactSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 34.8 BY 1 NO-UNDO.

DEFINE BUTTON bDistClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 4.8 BY 1.14 TOOLTIP "Clear the Search criteria".

DEFINE BUTTON bDistCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy the contacts of the selected Group to a new Group".

DEFINE BUTTON bDistDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected Group".

DEFINE BUTTON bDistEmail  NO-FOCUS
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Email all contacts in the selected Group".

DEFINE BUTTON bDistModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Edit the selected Group".

DEFINE BUTTON bDistNew  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new Group".

DEFINE BUTTON bDistRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Refresh the Groups".

DEFINE VARIABLE fDistSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 15.6 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwContacts FOR 
      contact SCROLLING.

DEFINE QUERY brwDistributions FOR 
      distribution SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwContacts C-Win _FREEFORM
  QUERY brwContacts DISPLAY
      contact.isSelected view-as toggle-box
      contact.contactName width 35
      contact.address width 21
      contact.agentName width 21
      contact.agentStatDesc width 10
      contact.agentStateDesc width 12
contact.email width 35
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS SIZE 130.6 BY 13.29 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Contacts marked in Red are marked as ~"Do Not Call~" or have their email deactivated.".

DEFINE BROWSE brwDistributions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDistributions C-Win _FREEFORM
  QUERY brwDistributions DISPLAY
      distribution.distName
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS SIZE 28 BY 13.29 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Contacts marked in Red are marked as ~"Do Not Call~" or have their email deactivated.".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bDistCopy AT ROW 7.91 COL 2 WIDGET-ID 82 NO-TAB-STOP 
     fDistSearch AT ROW 1.24 COL 16 COLON-ALIGNED WIDGET-ID 80
     brwDistributions AT ROW 2.67 COL 9 WIDGET-ID 500
     bDistDelete AT ROW 6.48 COL 2 WIDGET-ID 8 NO-TAB-STOP 
     bDistEmail AT ROW 9.1 COL 2 WIDGET-ID 70 NO-TAB-STOP 
     bDistModify AT ROW 5.05 COL 2 WIDGET-ID 62 NO-TAB-STOP 
     bDistNew AT ROW 2.43 COL 2 WIDGET-ID 44 NO-TAB-STOP 
     bDistRefresh AT ROW 11 COL 3 WIDGET-ID 56 NO-TAB-STOP 
     bDistClear AT ROW 1.24 COL 37 WIDGET-ID 76 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 206.8 BY 22.05 WIDGET-ID 100.

DEFINE FRAME fContacts
     bContactView AT ROW 9.57 COL 2 WIDGET-ID 78 NO-TAB-STOP 
     fContactSearch AT ROW 1.33 COL 16.2 COLON-ALIGNED WIDGET-ID 74
     fContactStat AT ROW 1.33 COL 62 COLON-ALIGNED WIDGET-ID 82
     fContactStateID AT ROW 1.33 COL 97 COLON-ALIGNED WIDGET-ID 84
     brwContacts AT ROW 2.67 COL 7 WIDGET-ID 200
     bContactClear AT ROW 1.24 COL 134 WIDGET-ID 76 NO-TAB-STOP 
     bContactDelete AT ROW 4.1 COL 2 WIDGET-ID 64 NO-TAB-STOP 
     bContactEmail AT ROW 8.14 COL 2 WIDGET-ID 70 NO-TAB-STOP 
     bContactExport AT ROW 6.95 COL 2 WIDGET-ID 58 NO-TAB-STOP 
     bContactNew AT ROW 2.67 COL 2 WIDGET-ID 66 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 53 ROW 1.24
         SIZE 154 BY 21.67
         TITLE "Contacts" WIDGET-ID 400.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Contact Groups"
         HEIGHT             = 22.05
         WIDTH              = 206.8
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fContacts:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fContacts
                                                                        */
/* BROWSE-TAB brwContacts fContactStateID fContacts */
/* SETTINGS FOR BUTTON bContactClear IN FRAME fContacts
   NO-ENABLE                                                            */
ASSIGN 
       bContactClear:PRIVATE-DATA IN FRAME fContacts     = 
                "New".

ASSIGN 
       bContactDelete:PRIVATE-DATA IN FRAME fContacts     = 
                "New".

ASSIGN 
       bContactNew:PRIVATE-DATA IN FRAME fContacts     = 
                "New".

ASSIGN 
       brwContacts:POPUP-MENU IN FRAME fContacts             = MENU POPUP-MENU-brwContacts:HANDLE
       brwContacts:COLUMN-RESIZABLE IN FRAME fContacts       = TRUE
       brwContacts:COLUMN-MOVABLE IN FRAME fContacts         = TRUE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fContacts:MOVE-AFTER-TAB-ITEM (fDistSearch:HANDLE IN FRAME fMain)
       XXTABVALXX = FRAME fContacts:MOVE-BEFORE-TAB-ITEM (brwDistributions:HANDLE IN FRAME fMain)
/* END-ASSIGN-TABS */.

/* BROWSE-TAB brwDistributions fContacts fMain */
/* SETTINGS FOR BUTTON bDistClear IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bDistClear:PRIVATE-DATA IN FRAME fMain     = 
                "New".

ASSIGN 
       bDistNew:PRIVATE-DATA IN FRAME fMain     = 
                "New".

ASSIGN 
       brwDistributions:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwDistributions:HANDLE
       brwDistributions:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwDistributions:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwContacts
/* Query rebuild information for BROWSE brwContacts
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH contact.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwContacts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDistributions
/* Query rebuild information for BROWSE brwDistributions
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH distribution.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDistributions */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Contact Groups */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Contact Groups */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Contact Groups */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fContacts
&Scoped-define SELF-NAME bContactClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactClear C-Win
ON CHOOSE OF bContactClear IN FRAME fContacts /* Clear */
DO:
  fContactSearch:screen-value = "".
  self:sensitive = false.
  apply "RETURN" to fContactSearch.
  apply "ENTRY" to fContactSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactDelete C-Win
ON CHOOSE OF bContactDelete IN FRAME fContacts /* Delete */
DO:
  if getDistribution() = ""
   then
    do:
      message "Please select a distribution list" view-as alert-box warning.
      return.
    end.
    
  if can-find(first contact where isSelected = true)
   then
    do:
      {lib/confirm-delete.i "Selected Contacts"}
        
      empty temp-table tempcontact.
      for each contact no-lock
         where contact.isSelected = true:
         
        create tempcontact.
        buffer-copy contact to tempcontact.
        release tempcontact.
      end.
    end.
   else return no-apply.
  
  run server/deletedistributioncontacts.p (input  getDistribution(),
                                           input  table tempcontact,
                                           output table contact,
                                           output std-lo,
                                           output std-ch).
                                           
  if not std-lo
   then message std-ch view-as alert-box error.
   else refreshContacts(getDistribution(), false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactEmail C-Win
ON CHOOSE OF bContactEmail IN FRAME fContacts /* Email */
DO:
  if num-results("brwContacts") = 0
   then
    do: 
     MESSAGE "There are no contacts to email" VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
    
  define variable pBCC as longchar no-undo initial "".
  for each contact no-lock
     where contact.doNotCall  = false
       and contact.isSelected = true:
       
    pBCC = pBCC + contact.email + ";".
  end.
  
  if pBCC = ""
   then return.
   
  run util/openemail.p ("", "", "", pBCC, "", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactExport C-Win
ON CHOOSE OF bContactExport IN FRAME fContacts /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactNew C-Win
ON CHOOSE OF bContactNew IN FRAME fContacts /* New */
DO:
  if getDistribution() = ""
   then
    do:
      message "Please select a distribution list" view-as alert-box warning.
      return.
    end.
  
  empty temp-table tempcontact.
  std-lo = true.
  run dialogcontacts.w (input getDistribution(), input-output table tempcontact, output std-lo).
  if std-lo
   then return.
   
  run server/adddistributioncontacts.p (input  getDistribution(),
                                        input  table tempcontact,
                                        output table contact,
                                        output std-lo,
                                        output std-ch).
                                       
  if not std-lo
   then message std-ch view-as alert-box error.
   else refreshContacts(getDistribution(), false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactView C-Win
ON CHOOSE OF bContactView IN FRAME fContacts /* View */
DO:
  if not available contact
   then return.

  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  
  if std-ch = "AMD" 
   then
    do:
      publish "OpenWindow" (input "Person",
                            input contact.personID, 
                            input "wpersondetail.w", 
                            input "character|input|" + contact.personID + "^integer|input|6" + "^character|input|",                                   
                            input this-procedure).                                          
    end. 
   else
    do:
      publish "OpenWindow" (input "Person", 
                            input contact.personID, 
                            input "wpersondetail.w", 
                            input "character|input|" + contact.personID + "^integer|input|2" + "^character|input|",                                   
                            input this-procedure).
      
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bDistClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistClear C-Win
ON CHOOSE OF bDistClear IN FRAME fMain /* Clear */
DO:
  fDistSearch:screen-value = "".
  self:sensitive = false.
  apply "RETURN" to fDistSearch.
  apply "ENTRY" to fDistSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDistCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistCopy C-Win
ON CHOOSE OF bDistCopy IN FRAME fMain /* Copy */
DO:
  /* copy the distribution contacts */
  empty temp-table singledist.
  for each contact no-lock:
    create singledist.
    buffer-copy contact to singledist.
    assign
      singledist.distName    = ""
      singledist.description = ""
      .
    release singledist.
  end.

  repeat:
    std-lo = true.
    run dialogdistribution.w (input "Copy", input-output table singledist, output std-lo).
    if std-lo
     then return.

    empty temp-table contact.
    run server/newdistribution.p (input  table singledist,
                                  output table contact,
                                  output std-lo,
                                  output std-ch).

    if not std-lo
     then message std-ch view-as alert-box error.
     else leave.
  end.
  
  for first singledist no-lock:
    create distribution.
    buffer-copy singledist to distribution.
    release distribution.
    refreshDistributions().
    setDistributionRow(setDistribution(singledist.distName)).
    assign
      brwDistributions:tooltip = singledist.description
      .
    refreshContacts(getDistribution(), false).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDistDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistDelete C-Win
ON CHOOSE OF bDistDelete IN FRAME fMain /* Delete */
DO:
  {lib/confirm-delete.i "Distribution List"}
  run server/deletedistribution.p (getDistribution(), output std-lo, output std-ch).
  
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      for first distribution exclusive-lock
          where distribution.distName = getDistribution():
        
        delete distribution.
      end.
      refreshDistributions().
      setDistribution("").
      for first distribution no-lock:
        setDistribution(distribution.distName).
      end.
      run distributionChanged in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDistEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistEmail C-Win
ON CHOOSE OF bDistEmail IN FRAME fMain /* Email */
DO:
  if num-results("brwContacts") = 0
   then
    do: 
     MESSAGE "There are no contacts to email" VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
    
  define variable pBCC as longchar no-undo initial "".
  for each contact no-lock
     where contact.doNotCall = false:
       
    pBCC = pBCC + contact.email + ";".
  end.
  run util/openemail.p ("", "", "", pBCC, "", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDistModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistModify C-Win
ON CHOOSE OF bDistModify IN FRAME fMain /* Modify */
DO:
  if not available distribution
   then return.

  /* copy the distribution contacts if any */
  empty temp-table singledist.
  if can-find(first contact where distName = getDistribution())
   then
    for each contact no-lock
       where contact.distName = getDistribution():
       
      create singledist.
      buffer-copy contact to singledist.
      release singledist.
    end.
   else
    for first distribution no-lock
        where distribution.distName = getDistribution():
        
      create singledist.
      buffer-copy distribution to singledist.
      release singledist.
    end.

  repeat:
    std-lo = true.
    run dialogdistribution.w (input "Modify", input-output table singledist, output std-lo).
    if std-lo
     then return.

    empty temp-table contact.
    run server/modifydistribution.p (input  getDistribution(),
                                     input  table singledist,
                                     output table contact,
                                     output std-lo,
                                     output std-ch).
    
    if not std-lo
     then message std-ch view-as alert-box error.
     else leave.
  end.
   
  for first singledist no-lock:
    for first distribution exclusive-lock
        where distribution.distName = getDistribution():
        
      distribution.description = singledist.description.
    end.
    refreshDistributions().
    setDistributionRow(setDistribution(singledist.distName)).
    assign
      brwDistributions:tooltip = singledist.description
      .
    refreshContacts(getDistribution(), false).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDistNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistNew C-Win
ON CHOOSE OF bDistNew IN FRAME fMain /* New */
DO:
  empty temp-table singledist.
  
  repeat:
    std-lo = true.
    run dialogdistribution.w (input "New", input-output table singledist, output std-lo).
    if std-lo
     then return.

    empty temp-table contact.
    run server/newdistribution.p (input  table singledist,
                                  output table contact,
                                  output std-lo,
                                  output std-ch).
                                  
    if not std-lo
     then message std-ch view-as alert-box error.
     else leave.
  end.
  
  for first singledist no-lock:
    create distribution.
    buffer-copy singledist to distribution.
    release distribution.
    refreshDistributions().
    setDistributionRow(setDistribution(singledist.distName)).
    assign
      brwDistributions:tooltip = singledist.description
      .
    refreshContacts(getDistribution(), false).
  end.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDistRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDistRefresh C-Win
ON CHOOSE OF bDistRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
  setDistributionRow(getDistribution()).
  apply "VALUE-CHANGED" to browse brwDistributions.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwContacts
&Scoped-define FRAME-NAME fContacts
&Scoped-define SELF-NAME brwContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON CTRL-A OF brwContacts IN FRAME fContacts
DO:
  apply "CHOOSE" to bContactNew in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON CTRL-D OF brwContacts IN FRAME fContacts
DO:
  if available contact
   then apply "CHOOSE" to bContactDelete in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON CTRL-E OF brwContacts IN FRAME fContacts
DO:
  if bContactEmail:sensitive
   then apply "CHOOSE" to bContactEmail in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON CTRL-P OF brwContacts IN FRAME fContacts
DO:
  if bContactView:sensitive
   then apply "CHOOSE" to bContactView in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON CTRL-X OF brwContacts IN FRAME fContacts
DO:
  if bContactExport:sensitive
   then apply "CHOOSE" to bContactExport in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON MOUSE-SELECT-UP OF brwContacts IN FRAME fContacts
DO:
  if not available contact
   then return.
   
  if valid-handle(brwContacts:current-column)
   then return.
   
  contact.isSelected = not contact.isSelected.
  contact.isSelected:checked in browse brwContacts = contact.isSelected.
  browse {&browse-name}:refresh().
  
  changeButtonState().
  bContactDelete:sensitive = can-find(first contact where isSelected = true).
  menu-item m_Contact_Delete:sensitive in menu POPUP-MENU-brwContacts = bContactDelete:sensitive.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON ROW-DISPLAY OF brwContacts IN FRAME fContacts
DO:
  {lib/brw-rowDisplay-multi.i}
  
  /* color the doNotCall rows */
  if contact.doNotCall
   then
    do:
      rowColor = 12.
      do std-in = 1 to num-entries(colHandleList):
        colHandle = handle(entry(std-in, colHandleList)).
        if valid-handle(colHandle) 
         then colHandle:bgcolor = rowColor.
      end.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON START-SEARCH OF brwContacts IN FRAME fContacts
DO:
  define buffer contact for contact.
  
  hSortColumn = browse {&browse-name}:current-column.
  if hSortColumn:name = "isSelected" and num-results("{&browse-name}") > 0
   then selectAllFilter().
   else
    do:
      {lib/brw-startSearch-multi.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts C-Win
ON VALUE-CHANGED OF brwContacts IN FRAME fContacts
DO:
  if not available contact
   then return.
   
  setContact(contact.contactName).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDistributions
&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME brwDistributions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON CTRL-A OF brwDistributions IN FRAME fMain
DO:
  apply "CHOOSE" to bDistNew.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON CTRL-C OF brwDistributions IN FRAME fMain
DO:
  if available distribution
   then apply "CHOOSE" to bDistCopy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON CTRL-D OF brwDistributions IN FRAME fMain
DO:
  if available distribution
   then apply "CHOOSE" to bDistDelete.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON CTRL-E OF brwDistributions IN FRAME fMain
DO:
  if bDistEmail:sensitive
   then apply "CHOOSE" to bDistEmail.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON CTRL-M OF brwDistributions IN FRAME fMain
DO:
  if available distribution
   then apply "CHOOSE" to bDistModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON CTRL-R OF brwDistributions IN FRAME fMain
DO:
  apply "CHOOSE" to bDistRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON DEFAULT-ACTION OF brwDistributions IN FRAME fMain
DO:
  apply "CHOOSE" to bDistModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON ROW-DISPLAY OF brwDistributions IN FRAME fMain
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON START-SEARCH OF brwDistributions IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistributions C-Win
ON VALUE-CHANGED OF brwDistributions IN FRAME fMain
DO:
  assign
    std-lo                   = available distribution
    bDistModify:sensitive    = std-lo
    bDistDelete:sensitive    = std-lo
    bDistCopy:sensitive      = std-lo
    bDistEmail:sensitive     = false
    /* menu items */
    menu-item m_Dist_Modify:sensitive in menu POPUP-MENU-brwDistributions = bDistModify:sensitive
    menu-item m_Dist_Delete:sensitive in menu POPUP-MENU-brwDistributions = bDistDelete:sensitive
    menu-item m_Dist_Copy:sensitive in menu POPUP-MENU-brwDistributions   = bDistCopy:sensitive
    menu-item m_Dist_Email:sensitive in menu POPUP-MENU-brwDistributions  = bDistEmail:sensitive
    .
    
  if not available distribution
   then return.
   
  setDistribution(distribution.distName).
  run distributionChanged in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fContacts
&Scoped-define SELF-NAME fContactSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fContactSearch C-Win
ON RETURN OF fContactSearch IN FRAME fContacts /* Search */
DO:
  refreshContacts(getDistribution(), false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fContactSearch C-Win
ON VALUE-CHANGED OF fContactSearch IN FRAME fContacts /* Search */
DO:
  bContactClear:sensitive = self:screen-value <> "".
  if self:screen-value = ""
   then refreshContacts(getDistribution(), false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fContactStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fContactStat C-Win
ON VALUE-CHANGED OF fContactStat IN FRAME fContacts /* Status */
DO:
  refreshContacts(getDistribution(), false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fContactStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fContactStateID C-Win
ON VALUE-CHANGED OF fContactStateID IN FRAME fContacts /* State */
DO:
  refreshContacts(getDistribution(), false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME fDistSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDistSearch C-Win
ON RETURN OF fDistSearch IN FRAME fMain /* Search */
DO:
  refreshDistributions().
  apply "VALUE-CHANGED" to brwDistributions.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDistSearch C-Win
ON VALUE-CHANGED OF fDistSearch IN FRAME fMain /* Search */
DO:
  bDistClear:sensitive = self:screen-value <> "".
  if self:screen-value = ""
   then apply "RETURN" to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Contact_Add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Contact_Add C-Win
ON CHOOSE OF MENU-ITEM m_Contact_Add /* Add a new Contact (CTRL-A) */
DO:
  apply "CHOOSE" to bContactNew in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Contact_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Contact_Delete C-Win
ON CHOOSE OF MENU-ITEM m_Contact_Delete /* Delete the checked Contacts (CTRL-D) */
DO:
  apply "CHOOSE" to bContactDelete in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Contact_Email
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Contact_Email C-Win
ON CHOOSE OF MENU-ITEM m_Contact_Email /* Email the checked Contacts (CTRL-E) */
DO:
  apply "CHOOSE" to bContactEmail in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Contact_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Contact_Export C-Win
ON CHOOSE OF MENU-ITEM m_Contact_Export /* Export the checked Contacts (CTRL-X) */
DO:
  apply "CHOOSE" to bContactExport in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Contact_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Contact_Person C-Win
ON CHOOSE OF MENU-ITEM m_Contact_Person /* View the Person of the selected Contact (CTRL-P) */
DO:
  apply "CHOOSE" to bContactView in frame fContacts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dist_Add
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dist_Add C-Win
ON CHOOSE OF MENU-ITEM m_Dist_Add /* Create a new Group (CTRL-A) */
DO:
  apply "CHOOSE" to bDistNew in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dist_Copy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dist_Copy C-Win
ON CHOOSE OF MENU-ITEM m_Dist_Copy /* Copy the contacts of the selected Group to a new Group (CTRL-C) */
DO:
  apply "CHOOSE" to bDistCopy in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dist_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dist_Delete C-Win
ON CHOOSE OF MENU-ITEM m_Dist_Delete /* Delete the selected Group (CTRL-D) */
DO:
  apply "CHOOSE" to bDistDelete in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dist_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dist_Modify C-Win
ON CHOOSE OF MENU-ITEM m_Dist_Modify /* Edit the selected Group (CTRL-M) */
DO:
  apply "CHOOSE" to bDistModify in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dist_Refresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dist_Refresh C-Win
ON CHOOSE OF MENU-ITEM m_Dist_Refresh /* Refresh the Groups (CTRL-R) */
DO:
  apply "CHOOSE" to bDistRefresh in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwContacts
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "IsDistributionCreated" anywhere.

{lib/win-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/win-status.i &entity="'contact'"}
{lib/brw-main-multi.i &parentFrame=fMain &browse-list="brwDistributions,brwContacts" &noSort="brwDistributions"}
/* main window */
{lib/set-button.i &frame-name=fMain          &label="Refresh"}
{lib/set-button.i &frame-name=fMain          &label="Export"}
{lib/set-button.i &frame-name=fMain          &label="Email"}
/* distributions */
{lib/set-button.i                            &label="DistNew"        &image="images/s-new.bmp"       &inactive="images/s-new-i.bmp"}
{lib/set-button.i                            &label="DistModify"     &image="images/s-update.bmp"    &inactive="images/s-update-i.bmp"}
{lib/set-button.i                            &label="DistDelete"     &image="images/s-delete.bmp"    &inactive="images/s-delete-i.bmp"}
{lib/set-button.i                            &label="DistCopy"       &image="images/s-copy.bmp"      &inactive="images/s-copy-i.bmp"}
{lib/set-button.i                            &label="DistClear"      &image="images/s-erase.bmp"     &inactive="images/s-erase-i.bmp"}
{lib/set-button.i                            &label="DistEmail"      &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
{lib/set-button.i                            &label="DistRefresh"    &image="images/s-sync.bmp"      &inactive="images/s-sync-i.bmp"}
/* contacts */
{lib/set-filter.i &frame-name=fContacts      &label="ContactSearch"  &id="contactName:email:address" &populate=false}
{lib/set-filter.i &frame-name=fContacts      &label="ContactStat"    &id="agentStat"                 &value="agentStatDesc"  &addOption="None,N"}
{lib/set-filter.i &frame-name=fContacts      &label="ContactStateID" &id="agentStateID"              &value="agentStateDesc" &addOption="None,N"}
{lib/set-button.i &frame-name=fContacts      &label="ContactNew"     &image="images/s-new.bmp"       &inactive="images/s-new-i.bmp"}
{lib/set-button.i &frame-name=fContacts      &label="ContactDelete"  &image="images/s-delete.bmp"    &inactive="images/s-delete-i.bmp"}
{lib/set-button.i &frame-name=fContacts      &label="ContactClear"   &image="images/s-erase.bmp"     &inactive="images/s-erase-i.bmp"}
{lib/set-button.i &frame-name=fContacts      &label="ContactExport"  &image="images/s-excel.bmp"     &inactive="images/s-excel-i.bmp"}
{lib/set-button.i &frame-name=fContacts      &label="ContactEmail"   &image="images/s-email.bmp"     &inactive="images/s-email-i.bmp"}
{lib/set-button.i &frame-name=fContacts      &label="ContactView"    &image="images/s-users.bmp"     &inactive="images/s-users-i.bmp"}
setButtons().

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  {lib/get-column-width.i &col="'agentName'" &var=dCol}
  run windowResized in this-procedure.
  run getData in this-procedure.
  for first distribution no-lock:
    setDistribution(distribution.distName).
  end.
  apply "VALUE-CHANGED" to brwDistributions.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE distributionChanged C-Win 
PROCEDURE distributionChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fMain:
    for first distribution no-lock
        where distribution.distName = getDistribution():

      brwDistributions:tooltip = distribution.description.
    end.
    if getDistribution() = ""
     then frame fContacts:title = "Contacts".
     else
      do:
        frame fContacts:title = "Contacts in Group " + getDistribution().
        refreshContacts(getDistribution(), true).
        assign
          fContactStateID:screen-value = "ALL"
          fContactStat:screen-value = "ALL"
          .
      end.
    apply "VALUE-CHANGED" to browse brwContacts.
    apply "ENTRY" to browse brwDistributions.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fDistSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bDistCopy fDistSearch brwDistributions bDistDelete bDistEmail 
         bDistModify bDistNew bDistRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY fContactSearch fContactStat fContactStateID 
      WITH FRAME fContacts IN WINDOW C-Win.
  ENABLE bContactView fContactSearch fContactStat fContactStateID brwContacts 
         bContactDelete bContactEmail bContactExport bContactNew 
      WITH FRAME fContacts IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fContacts}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if num-results("brwContacts") = 0
   then
    do: 
     MESSAGE "There are no contacts to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Distribution_Contacts"

  run util/exporttoexceltable.p (temp-table contact:default-buffer-handle, {&ReportName}, "where doNotCall = no and isSelected = yes by contactName").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table distribution.
  run server/getdistributions.p (output table distribution,
                                 output std-lo,
                                 output std-ch).
                                 
  if not std-lo
   then message std-ch view-as alert-box error.
   else refreshDistributions().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsDistributionCreated C-Win 
PROCEDURE IsDistributionCreated :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pName  as character no-undo.
  define output parameter pFound as logical   no-undo.
  
  pFound = can-find(first distribution where distName = pName).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCntTotal    as integer   no-undo initial 0.
  define variable iCntActive   as integer   no-undo initial 0.
  define variable iCntProspect as integer   no-undo initial 0.
  define variable iCntClose    as integer   no-undo initial 0.
  define variable iCntCancel   as integer   no-undo initial 0.
  define variable iCntWith     as integer   no-undo initial 0.
  define variable iCntNone     as integer   no-undo initial 0.
  define variable tMessage     as character no-undo.
  define variable hBuffer      as handle    no-undo.
  define variable hQuery       as handle    no-undo.
  
  {lib/brw-sortData-multi.i}
  if valid-handle(hSortColumn) and hSortColumn:name = "isSelected"
   then browse brwContacts:clear-sort-arrows().
  
  clearStatus(). 
  create buffer hBuffer for table "contact".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each contact no-lock " + getWhereClause() + " by agentStat").
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    iCntTotal = iCntTotal + 1.
    std-ch = hBuffer:buffer-field("agentStat"):buffer-value().
    case std-ch:
     when "A" then iCntActive   = iCntActive + 1.
     when "P" then iCntProspect = iCntProspect + 1.
     when "X" then iCntCancel   = iCntCancel + 1.
     when "C" then iCntClose    = iCntClose + 1.
     when "W" then iCntWith     = iCntWith + 1.
     when "N" then iCntNone     = iCntNone + 1.
    end case.
    hQuery:get-next().
  end.
  hQuery:query-close().
  if iCntActive > 0
   then tMessage = tMessage + "Act: " + trim(string(iCntActive, ">>>,>>9")) + ",".
  if iCntCancel > 0
   then tMessage = tMessage + " CX: " + trim(string(iCntCancel, ">>>,>>9")) + ",".
  if iCntProspect > 0
   then tMessage = tMessage + " PRSPCT: " + trim(string(iCntProspect, ">>>,>>9")) + ",".
  if iCntClose > 0
   then tMessage = tMessage + " CLSD: " + trim(string(iCntClose, ">>>,>>9")) + ",".
  if iCntWith > 0
   then tMessage = tMessage + " WDN: " + trim(string(iCntWith, ">>>,>>9")) + ",".
  if iCntNone > 0
   then tMessage = tMessage + " None: " + trim(string(iCntNone, ">>>,>>9")) + ",".
  tMessage = substring(tMessage, 1, length(tMessage) - 1).
  tMessage = "Tot Contacts: " + trim(string(iCntTotal, ">>>,>>9")) + " (" + tMessage + ")".
  
  if iCntTotal > 0
   then setStatus(tMessage).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* pixels from top and left */
  &scoped-define Margin  5
  &scoped-define Padding 1
  &scoped-define BSpace  10
  &scoped-define THeight 21

  /* adjust the frames */
  assign
    /* main */
    frame fMain:width-pixels           = {&window-name}:width-pixels
    frame fMain:height-pixels          = {&window-name}:height-pixels
    /* contacts */
    frame fContacts:width-pixels       = frame fMain:width-pixels - frame fContacts:x - 5
    frame fContacts:height-pixels      = frame fMain:height-pixels - frame fContacts:y - 5
    .

  /* adjust widgets */
  assign
    /* distributions */
    fDistSearch:side-label-handle:x     = {&Margin} + bDistNew:width-pixels + {&Padding}
    fDistSearch:side-label-handle:y     = {&Margin} + 4
    fDistSearch:x                       = fDistSearch:side-label-handle:x + fDistSearch:side-label-handle:width-pixels
    fDistSearch:y                       = {&Margin} + 4
    fDistSearch:width-pixels            = (frame fMain:width-pixels - frame fContacts:width-pixels) - fDistSearch:x - (bDistClear:width-pixels + {&Padding} + {&Margin} + {&BSpace})
    bDistClear:x                        = fDistSearch:x + fDistSearch:width-pixels + {&Padding}
    bDistClear:y                        = fDistSearch:y - 1
    bDistRefresh:x                      = {&Margin} - 1
    bDistNew:x                          = bDistRefresh:x
    bDistModify:x                       = bDistRefresh:x
    bDistDelete:x                       = bDistRefresh:x
    bDistCopy:x                         = bDistRefresh:x
    bDistEmail:x                        = bDistRefresh:x
    bDistRefresh:y                      = fDistSearch:y + fDistSearch:height-pixels + ({&Padding} + 1)
    bDistNew:y                          = bDistRefresh:y + bDistRefresh:height-pixels + {&Padding}
    bDistModify:y                       = bDistNew:y + bDistNew:height-pixels + {&Padding}
    bDistDelete:y                       = bDistModify:y + bDistNew:height-pixels + {&Padding}
    bDistCopy:y                         = bDistDelete:y + bDistNew:height-pixels + {&Padding}
    bDistEmail:y                        = bDistCopy:y + bDistNew:height-pixels + {&Padding}
    brwDistributions:x                  = fDistSearch:side-label-handle:x
    brwDistributions:y                  = bDistRefresh:y + 1
    brwDistributions:height-pixels      = frame fMain:height-pixels - brwDistributions:y - {&Margin} - 6
    brwDistributions:width-pixels       = fDistSearch:width-pixels + fDistSearch:side-label-handle:width-pixels + bDistClear:width-pixels
    /* contacts */
    fContactSearch:side-label-handle:x  = {&Margin} + bContactNew:width-pixels + {&Padding}
    fContactSearch:side-label-handle:y  = {&Margin}
    fContactSearch:x                    = fContactSearch:side-label-handle:x + fContactSearch:side-label-handle:width-pixels
    fContactSearch:y                    = {&Margin}
    fContactSearch:width-pixels         = frame fContacts:width-pixels - fContactSearch:x - (bContactClear:width-pixels + fContactStat:side-label-handle:width-pixels + fContactStat:width-pixels + fContactStateID:side-label-handle:width-pixels + fContactStateID:width-pixels + ({&Padding} * 3) + ({&BSpace} * 2) + {&Margin})
    fContactStat:side-label-handle:x    = fContactSearch:x + fContactSearch:width-pixels + {&Padding} + {&BSpace}
    fContactStat:side-label-handle:y    = {&Margin}
    fContactStat:x                      = fContactStat:side-label-handle:x + fContactStat:side-label-handle:width-pixels
    fContactStat:y                      = {&Margin}
    fContactStateID:side-label-handle:x = fContactStat:x + fContactStat:width-pixels + {&Padding} + {&BSpace}
    fContactStateID:side-label-handle:y = {&Margin}
    fContactStateID:x                   = fContactStateID:side-label-handle:x + fContactStateID:side-label-handle:width-pixels
    fContactStateID:y                   = {&Margin}
    bContactClear:x                     = fContactStateID:x + fContactStateID:width-pixels + {&Padding}
    bContactClear:y                     = fContactStateID:y - 1
    bContactNew:x                       = {&Margin} - 1
    bContactDelete:x                    = bContactNew:x
    bContactExport:x                    = bContactNew:x
    bContactEmail:x                     = bContactNew:x
    bContactView:x                      = bContactNew:x
    bContactNew:y                       = fContactSearch:y + fContactSearch:height-pixels + ({&Padding} + 1)
    bContactDelete:y                    = bContactNew:y + bContactNew:height-pixels + {&Padding}
    bContactExport:y                    = bContactDelete:y + bContactDelete:height-pixels + {&Padding}
    bContactEmail:y                     = bContactExport:y + bContactExport:height-pixels + {&Padding}
    bContactView:y                      = bContactEmail:y + bContactEmail:height-pixels + {&Padding}
    brwContacts:x                       = fContactSearch:side-label-handle:x
    brwContacts:y                       = bContactNew:y + 1
    brwContacts:width-pixels            = frame fContacts:width-pixels - brwContacts:x - ({&Margin} + 2)
    brwContacts:height-pixels           = frame fContacts:height-pixels - brwContacts:y - ({&THeight} + {&Margin})
    no-error
    .

  /* adjust the virtual frames */
  assign
    /* main */
    frame fMain:virtual-width-pixels           = {&window-name}:width-pixels
    frame fMain:virtual-height-pixels          = {&window-name}:height-pixels
    /* contacts */
    frame fContacts:virtual-width-pixels       = frame fContacts:width-pixels
    frame fContacts:virtual-height-pixels      = frame fContacts:height-pixels
    .
    
  {lib/resize-column.i &col="'address,agentName'" &var=dCol}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION changeButtonState C-Win 
FUNCTION changeButtonState RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame fMain:
    do with frame fContacts:
      /* change buttons */
      assign
        bContactEmail:sensitive  = can-find(first contact where doNotCall = false and isSelected = true)
        bContactExport:sensitive = can-find(first contact where doNotCall = false and isSelected = true)
        bContactDelete:sensitive = can-find(first contact where isSelected = true)
        bContactView:sensitive   = can-find(first contact)
        bDistEmail:sensitive     = can-find(first contact where doNotCall = false)
        /* menu items */
        menu-item m_Contact_Delete:sensitive in menu POPUP-MENU-brwContacts  = bContactDelete:sensitive
        menu-item m_Contact_Person:sensitive in menu POPUP-MENU-brwContacts  = bContactView:sensitive
        menu-item m_Contact_Export:sensitive in menu POPUP-MENU-brwContacts  = bContactExport:sensitive
        menu-item m_Contact_Email:sensitive in menu POPUP-MENU-brwContacts   = bContactEmail:sensitive
        menu-item m_Dist_Email:sensitive in menu POPUP-MENU-brwDistributions = bDistEmail:sensitive
        .
    end.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getContact C-Win 
FUNCTION getContact RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN tContactName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getDistribution C-Win 
FUNCTION getDistribution RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  RETURN tGroupName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshContacts C-Win 
FUNCTION refreshContacts RETURNS LOGICAL
  ( input pName       as character,
    input pFromServer as logical  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query brwContacts.
  std-lo = true.
  if pFromServer
   then 
    do:
      empty temp-table contact.
      run server/getdistributioncontacts.p (input  pName,
                                            output table contact,
                                            output std-lo,
                                            output std-ch).
    end.
                                 
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do with frame fContacts:
      setFilterCombos("ALL").
      changeButtonState().
      {lib/brw-startSearch-multi.i &browseName=brwContacts &whereClause=getWhereClause() &sortClause="'by contactName'"}
    end.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshDistributions C-Win 
FUNCTION refreshDistributions RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  close query brwDistributions.
  do with frame fMain:
    assign
      std-ch       = fDistSearch:screen-value.
      tWhereClause = "where distName matches '*" + std-ch + "*'"
      .
    {lib/brw-startSearch-multi.i &browseName=brwDistributions &whereClause=tWhereClause &sortClause="'by distName'"}
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION selectAllFilter C-Win 
FUNCTION selectAllFilter RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hBuffer     as handle  no-undo.
  define variable hQuery      as handle  no-undo.
  define variable hField      as handle  no-undo.
  define variable lSelected   as logical no-undo.
  define variable tCount      as integer no-undo initial 0.
  define variable tFiltered   as integer no-undo.
  tFiltered = num-results("brwContacts").
  
  create buffer hBuffer for table "contact".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each contact no-lock " + getWhereClause() + " by agentStat").
  hQuery:query-open().
  
  /* get the count of how many records are selected */
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field("isSelected").
    std-lo = hField:buffer-value().
    if std-lo
     then tCount = tCount + 1.
    hQuery:get-next().
  end.
  
  /* select/deselect the fields */
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hField = hBuffer:buffer-field("isSelected").
    hField:buffer-value() = (tCount <> tFiltered).
    hQuery:get-next().
  end.
  hQuery:query-close().
  browse brwContacts:refresh().
  changeButtonState().

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setContact C-Win 
FUNCTION setContact RETURNS CHARACTER
  ( input pName as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  tContactName = pName.
  RETURN tContactName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setDistribution C-Win 
FUNCTION setDistribution RETURNS CHARACTER
  ( input pName as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  tGroupName = pName.
  RETURN tGroupName.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setDistributionRow C-Win 
FUNCTION setDistributionRow RETURNS LOGICAL
  ( input pName as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tRowID as rowid no-undo.

  for first distribution no-lock
      where distribution.distName = pName:
    
    tRowID = rowid(distribution).
  end.

  brwDistributions:set-repositioned-row(1, "CONDITIONAL") in frame fMain no-error.
  reposition brwDistributions to rowid tRowID no-error.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

