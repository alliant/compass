&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created:
  
  Note: Window moved from app/ops to app/common because 
        it is used from ARM as well as OPS module
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input-output parameter pStateID as char.
def input-output parameter pFormID as char.
def input-output parameter pDescription as char.
def input-output parameter pFormCode as char.
def input-output parameter pType as char.
def input-output parameter pInsuredType as char.
def input-output parameter pActive as logical.
def input-output parameter pRateCheck as char.
def input-output parameter pRateMin as decimal.
def input-output parameter pRateMax as decimal.
def input-output parameter pOrg as char.
def input-output parameter pOrgRev as char.
def input-output parameter pOrgRel as datetime.
def input-output parameter pOrgEff as datetime.
def input-output parameter pRevType as character.

def output parameter pCancel as logical init true.

/* Local Variable Definitions ---                                       */
define variable cCode  as character no-undo.

{lib/std-def.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSysCode tRateCheck tState tFormID ~
tDescription tFormCode tType tInsuredType tActive tOrg tOrgRev tOrgRel ~
tOrgEff Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tRateCheck tState tFormID tDescription ~
tFormCode tType tInsuredType tActive tRateMin tRateMax tOrg tOrgRev tOrgRel ~
tOrgEff tRevType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSysCode  NO-FOCUS
     LABEL "sysCode" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tState AS CHARACTER 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     LIST-ITEM-PAIRS "Policy","P",
                     "Endorsement","E",
                     "CPL","C",
                     "Commitment","T"
     DROP-DOWN-LIST
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 112.6 BY 1 TOOLTIP "Form description" NO-UNDO.

DEFINE VARIABLE tFormCode AS CHARACTER FORMAT "X(30)":U 
     LABEL "Form Code" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 TOOLTIP "Enter the general identifier for the form used on reports" NO-UNDO.

DEFINE VARIABLE tFormID AS CHARACTER FORMAT "X(20)":U 
     LABEL "Form" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 TOOLTIP "Enter the general identifier for the form" NO-UNDO.

DEFINE VARIABLE tOrg AS CHARACTER FORMAT "X(256)":U 
     LABEL "Organization" 
     VIEW-AS FILL-IN 
     SIZE 42.4 BY 1 TOOLTIP "Authoring organization" NO-UNDO.

DEFINE VARIABLE tOrgEff AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Effective as assigned by authoring organization" NO-UNDO.

DEFINE VARIABLE tOrgRel AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Released" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Released by authoring organization" NO-UNDO.

DEFINE VARIABLE tOrgRev AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revision" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Revision assigned by authoring organization" NO-UNDO.

DEFINE VARIABLE tRateMax AS DECIMAL FORMAT ">>>,>>9.99":U INITIAL 0 
     LABEL "Maximum" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Maximum absolute value" NO-UNDO.

DEFINE VARIABLE tRateMin AS DECIMAL FORMAT ">>>,>>9.99":U INITIAL 0 
     LABEL "Minimum" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Minimum absolute value" NO-UNDO.

DEFINE VARIABLE tRevType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Revenue Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tInsuredType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Owner", "O",
"Lender", "L",
"Both", "B",
"None", "N"
     SIZE 19.8 BY 4 NO-UNDO.

DEFINE VARIABLE tRateCheck AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Fixed", "F",
"Liability", "L",
"None", "N"
     SIZE 12 BY 3 TOOLTIP "Check to validate processing data entry of the rate min/max" NO-UNDO.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 34.6 BY 7.38.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57.6 BY 7.38.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 34.6 BY 7.38.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 15.4 BY .81 TOOLTIP "Check to allow processing data entry of this form" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     bSysCode AT ROW 6.1 COL 41 WIDGET-ID 252 NO-TAB-STOP 
     tRateCheck AT ROW 9.38 COL 55.2 NO-LABEL WIDGET-ID 134
     tState AT ROW 1.38 COL 15.6 COLON-ALIGNED WIDGET-ID 104
     tFormID AT ROW 2.57 COL 15.6 COLON-ALIGNED WIDGET-ID 102
     tDescription AT ROW 3.76 COL 15.4 COLON-ALIGNED WIDGET-ID 90
     tFormCode AT ROW 4.95 COL 15.6 COLON-ALIGNED WIDGET-ID 132
     tType AT ROW 8.33 COL 13 COLON-ALIGNED WIDGET-ID 106
     tInsuredType AT ROW 9.57 COL 15 NO-LABEL WIDGET-ID 14
     tActive AT ROW 8.48 COL 40.8 WIDGET-ID 98
     tRateMin AT ROW 12.62 COL 49.6 COLON-ALIGNED WIDGET-ID 114
     tRateMax AT ROW 13.76 COL 49.6 COLON-ALIGNED WIDGET-ID 116
     tOrg AT ROW 8.43 COL 85 COLON-ALIGNED WIDGET-ID 92
     tOrgRev AT ROW 9.57 COL 85 COLON-ALIGNED WIDGET-ID 94
     tOrgRel AT ROW 10.71 COL 85 COLON-ALIGNED WIDGET-ID 108
     tOrgEff AT ROW 11.86 COL 85 COLON-ALIGNED WIDGET-ID 110
     Btn_OK AT ROW 15.86 COL 50.8
     Btn_Cancel AT ROW 15.86 COL 68.8
     tRevType AT ROW 6.19 COL 15.8 COLON-ALIGNED WIDGET-ID 64
     "Insured:" VIEW-AS TEXT
          SIZE 8 BY .62 TOOLTIP "Select the type of insured" AT ROW 9.76 COL 6.8 WIDGET-ID 96
     "Author" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 7.62 COL 74 WIDGET-ID 124
     "Processing" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 7.62 COL 38.6 WIDGET-ID 126
     "Type" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 7.62 COL 3 WIDGET-ID 128
     "Rate Check:" VIEW-AS TEXT
          SIZE 13 BY .62 TOOLTIP "Select the type of insured" AT ROW 9.57 COL 40.8 WIDGET-ID 138
     RECT-34 AT ROW 7.95 COL 37.4 WIDGET-ID 118
     RECT-35 AT ROW 7.95 COL 73 WIDGET-ID 120
     RECT-36 AT ROW 7.95 COL 1.8 WIDGET-ID 122
     SPACE(94.79) SKIP(2.90)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Create Form"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-34 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRateMax IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRateMin IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRevType IN FRAME fNew
   NO-ENABLE                                                            */
ASSIGN 
       tRevType:READ-ONLY IN FRAME fNew        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* Create Form */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSysCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSysCode fNew
ON CHOOSE OF bSysCode IN FRAME fNew /* sysCode */
DO:
  run dialogrevenuelookup.w(input tRevType:screen-value,
                            input tState:screen-value,
                            output cCode,                            
                            output std-lo).
  if not std-lo 
   then
    return no-apply.
  
  tRevType:screen-value = cCode.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActive fNew
ON VALUE-CHANGED OF tActive IN FRAME fNew /* Active */
DO:
  if self:checked = false 
   then tRateCheck:checked in frame fNew = false.

  assign
    tRateCheck:sensitive in frame fNew = self:checked
    tRateMin:sensitive in frame fNew = tRateCheck:checked in frame fNew
    tRateMax:sensitive in frame fNew =  tRateCheck:checked in frame fNew
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRateCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRateCheck fNew
ON VALUE-CHANGED OF tRateCheck IN FRAME fNew
DO:
 assign
  tRateMin:sensitive in frame fNew = lookup(self:screen-value,"F,L") <> 0
  tRateMax:sensitive in frame fNew = lookup(self:screen-value,"F,L") <> 0
  tRateMin:label = if self:screen-value = "L" then "Min/$1000" else "Minimum"
  tRateMax:label = if self:screen-value = "L" then "Max/$1000" else "Maximum"
  tRateMin:tooltip = if self:screen-value = "L" then "Minimum absolute value per $1000 of liability" else "Minimum absolute value"
  tRateMax:tooltip = if self:screen-value = "L" then "Maximum absolute value per $1000 of liability" else "Maximum absolute value".
  
 if self:screen-value = "N" then
 assign
  tRateMin:screen-value = "0"
  tRateMin = 0
  tRateMax:screen-value = "0"
  tRateMax = 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRevType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRevType fNew
ON VALUE-CHANGED OF tRevType IN FRAME fNew /* Revenue Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tType fNew
ON VALUE-CHANGED OF tType IN FRAME fNew /* Category */
DO:
  case self:screen-value:
   when "P" 
    then 
     do: 
         tInsuredType:sensitive in frame fNew = true.
         tInsuredType:enable("Owner").
         tInsuredType:enable("Lender").
         tInsuredType:disable("Both").
         tInsuredType:disable("None").
     end.
   when "E" 
    then 
     do: 
         tInsuredType:sensitive in frame fNew = true.
         tInsuredType:enable("Owner").
         tInsuredType:enable("Lender").
         tInsuredType:enable("Both").
         tInsuredType:enable("None").
     end.
   otherwise
    do: tInsuredType:screen-value in frame fNew = "N".
        tInsuredType:disable("Owner").
        tInsuredType:disable("Lender").
        tInsuredType:disable("Both").
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/get-state-list.i &combo=tState}
/*{lib/get-sysprop-list.i &combo=tRevType &appCode="'OPS'" &objAction="'StateProduct'" &objProperty="'RevenueType'"}*/

bSysCode  :load-image("images/s-magnifier.bmp").
bSysCode  :load-image-insensitive("images/s-magnifier-i.bmp").

if pStateID = "" 
 then
  do:
      publish "GetCurrentValue" ("ViewState", output std-ch).
      if std-ch <> "A" and index(tState:list-item-pairs in frame fNew, std-ch) > 0 
       then pStateID = std-ch.
      pActive = true.
      pRateCheck = "N".
  end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
if lookup(pType, "P,E,C,T") = 0 
 then pType = "P".

assign
  tState = pStateID
  tFormID = pFormID
  tDescription = pDescription
  tFormCode = pFormCode
  tType = pType
  tInsuredType = pInsuredType
  tActive = pActive
  tRateCheck = pRateCheck
  tRateMin = pRateMin
  tRateMax = pRateMax
  tOrg = pOrg
  tOrgRev = pOrgRev
  tOrgRel = pOrgRel
  tOrgEff = pOrgEff
  .

RUN enable_UI.

if lookup(tState, tState:list-item-pairs in frame fNew) > 0
 then tState:screen-value in frame fNew = tState.

/*tRevType:screen-value in frame fNew = "P". /* initially set Revenue Type to Premium */ */
apply "VALUE-CHANGED" to tType in frame fNew.
apply "value-changed" to tRateCheck in frame fNew.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if tFormID:screen-value in frame fNew = "" 
   then
    do:
        MESSAGE "Form ID cannot be blank"
         VIEW-AS ALERT-BOX error BUTTONS OK.
        next.
    end.

  assign
    pStateID = tState:screen-value in frame fNew
    pFormID = tFormID:screen-value in frame fNew
    pDescription = tDescription:screen-value in frame fNew
    pFormCode = tFormCode:screen-value in frame fNew
    pType = tType:screen-value in frame fNew
    pInsuredType = tInsuredType:screen-value in frame fNew
    pActive = tActive:checked in frame fNew
    pRateCheck = tRateCheck:screen-value in frame fNew
    pRateMin = tRateMin:input-value in frame fNew
    pRateMax = tRateMax:input-value in frame fNew
    pOrg = tOrg:screen-value in frame fNew
    pOrgRev = tOrgRev:screen-value in frame fNew
    pOrgRel = tOrgRel:input-value in frame fNew
    pOrgEff = tOrgEff:input-value in frame fNew
    pRevType = tRevType:input-value
    pCancel = false
    .
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tRateCheck tState tFormID tDescription tFormCode tType tInsuredType 
          tActive tRateMin tRateMax tOrg tOrgRev tOrgRel tOrgEff tRevType 
      WITH FRAME fNew.
  ENABLE bSysCode tRateCheck tState tFormID tDescription tFormCode tType 
         tInsuredType tActive tOrg tOrgRev tOrgRel tOrgEff Btn_OK Btn_Cancel 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

