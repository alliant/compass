&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogrevenuelookup.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created:10/10/19
  
  @Modified:
  Date         Name         Description
  08/06/2020   AG           Change Title and removed Cancel button.
  01/21/2021   Shefali      Remove Net GL Ref from the grid.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}
{tt/arrevenue.i}
{tt/arrevenue.i &tableAlias=tarrevenue}


/* Parameters Definitions ---                                           */

define input  parameter ipcRevenueType  as  character no-undo.
define input  parameter ipcStateID      as  character no-undo.
define output parameter opcRevenueType  as  character no-undo.
define output parameter oplSuccess      as  logical   no-undo.

/* Local Variable */
define variable lApplySearchString as logical     no-undo.
define variable cSearchString      as character   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwRevenue

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES arrevenue

/* Definitions for BROWSE brwRevenue                                    */
&Scoped-define FIELDS-IN-QUERY-brwRevenue arrevenue.revenueType "Revenue" arrevenue.grossGLRef "Revenue (Gross) GL Ref" arrevenue.retainedGLRef "Retained GL Ref"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwRevenue   
&Scoped-define SELF-NAME brwRevenue
&Scoped-define QUERY-STRING-brwRevenue for each arrevenue
&Scoped-define OPEN-QUERY-brwRevenue open query {&SELF-NAME} for each arrevenue.
&Scoped-define TABLES-IN-QUERY-brwRevenue arrevenue
&Scoped-define FIRST-TABLE-IN-QUERY-brwRevenue arrevenue


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwRevenue}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fSearch bSearch brwRevenue Btn_OK 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search Locks".

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Select" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 TOOLTIP "Search Criteria (Code Type, Code, Type, Description)" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwRevenue FOR 
      arrevenue SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwRevenue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwRevenue Dialog-Frame _FREEFORM
  QUERY brwRevenue DISPLAY
      arrevenue.revenueType  label     "Revenue"                format "x(25)"           
arrevenue.grossGLRef         label     "Revenue (Gross) GL Ref" format "x(25)" 
arrevenue.retainedGLRef      label     "Retained GL Ref"        format "x(16)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 77 BY 10.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fSearch AT ROW 1.67 COL 8.8 COLON-ALIGNED WIDGET-ID 66
     bSearch AT ROW 1.57 COL 57 WIDGET-ID 314
     brwRevenue AT ROW 3.43 COL 3 WIDGET-ID 300
     Btn_OK AT ROW 15 COL 34.4
     SPACE(31.79) SKIP(0.42)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Revenue Type" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwRevenue bSearch Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwRevenue:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwRevenue:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwRevenue
/* Query rebuild information for BROWSE brwRevenue
     _START_FREEFORM
open query {&SELF-NAME} for each arrevenue.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwRevenue */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Revenue Type */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRevenue
&Scoped-define SELF-NAME brwRevenue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRevenue Dialog-Frame
ON DEFAULT-ACTION OF brwRevenue IN FRAME Dialog-Frame
DO:
  apply "Choose" to Btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRevenue Dialog-Frame
ON ROW-DISPLAY OF brwRevenue IN FRAME Dialog-Frame
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRevenue Dialog-Frame
ON START-SEARCH OF brwRevenue IN FRAME Dialog-Frame
do:    
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch Dialog-Frame
ON CHOOSE OF bSearch IN FRAME Dialog-Frame /* Search */
DO:
  lApplySearchString = true.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Select */
DO:
  run setRevenueType in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON ENTRY OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  /* store the previous value of search string on which search is applied */
  cSearchString = fSearch:input-value.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON RETURN OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  apply 'choose' to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON VALUE-CHANGED OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  /* as soon as we change the search string, we track that string 
  is not applied and change the status in taskbar */
  lApplySearchString = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bSearch:load-image("images/s-magnifier.bmp").
bSearch:load-image-insensitive("images/s-magnifier-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
        
  run getData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME Dialog-Frame.
  ENABLE fSearch bSearch brwRevenue Btn_OK 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData Dialog-Frame 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).
  /* if search string is changed but not applied then restrore the fSearch fill-in
     to previous applied serach string and filter on the basis of what is
     present inside fsearch fill-in */
  else
    fSearch:screen-value = cSearchString.

  empty temp-table arrevenue.
  
  create arrevenue.
  assign
      arrevenue.revenueType   = "None"
      arrevenue.grossGLRef    = {&NotApplicable}  
      arrevenue.retainedGLRef = {&NotApplicable}   
      arrevenue.netGLRef      = {&NotApplicable}
      .
  
  for each tarrevenue where (if cSearchString <> "" then tarrevenue.revenueType   matches ("*" + cSearchString + "*")
                             else tarrevenue.revenueType = tarrevenue.revenueType)     or
                            (if cSearchString <> "" then tarrevenue.grossGLRef    matches ("*" + cSearchString + "*")
                             else tarrevenue.grossGLRef = tarrevenue.grossGLRef)       or
                            (if cSearchString <> "" then tarrevenue.retainedGLRef matches ("*" + cSearchString + "*")
                             else tarrevenue.retainedGLRef = tarrevenue.retainedGLRef) or
                            (if cSearchString <> "" then tarrevenue.netGLRef      matches ("*" + cSearchString + "*")
                             else tarrevenue.netGLRef = tarrevenue.netGLRef):
    
    create arrevenue.
    buffer-copy tarrevenue to arrevenue.

  end.

  open query brwRevenue preselect each arrevenue by arrevenue.revenueType desc.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.

  publish "getRevenues" (output table tarrevenue,
                         output std-lo,
                         output std-ch).

  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box info buttons ok.
      return.
    end.
  
  if ipcStateID <> ""    and 
     ipcStateID <> {&ALL} and 
     can-find(first tarrevenue where tarrevenue.revenueType matches ("*" + ipcStateID + "*"))
   then
    assign
        fSearch:screen-value = ipcStateID
        lApplySearchString   = true
        .
  
  run filterData in this-procedure.
  
  find first arrevenue where arrevenue.revenueType = ipcRevenueType no-error.

  if available arrevenue
   then      
    reposition brwRevenue to rowid rowid(arrevenue) no-error. 
  
  apply 'value-changed' to brwRevenue. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setRevenueType Dialog-Frame 
PROCEDURE setRevenueType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if available arrevenue
 then
  assign
      opcRevenueType = if arrevenue.revenueType = "None" then "" else arrevenue.revenueType      
      oplSuccess     = yes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
 
  tWhereClause = " by arrevenue.grossGLRef ".
  
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

