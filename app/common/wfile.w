&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wfile.w
   agent FILE Window
   D.Sinclair 3.7.2013
   
   Rahul Sharma
   Note: Window moved from app/ops to app/common because 
         it is used from ARM as well as OPS module
   @modified   2.03.2022   SC     Added Origin, FileType, FirstCPLIssueDt and FirstPolicyIssueDt
              06.03.2024   Sachin Task 112908- Changes in Agentfile related to Notes, Source and CPLAddr1, etc.
   */

CREATE WIDGET-POOL.

{lib/std-def.i}
{tt/agentfile.i}
{tt/processedform.i}
{tt/proerr.i &tableAlias="fileErr"}       /* Used to get validation messages from server */

 /* include file to normalize file number */
{lib/normalizefileid.i}

define variable cErrFile as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwPolicy

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES processedform

/* Definitions for BROWSE brwPolicy                                     */
&Scoped-define FIELDS-IN-QUERY-brwPolicy processedform.formUniqueID processedform.formtype processedform.formID processedform.statcode processedform.effDate processedform.invoiceDate processedform.liabilityAmount processedform.grossPremium processedform.retentionPremium processedform.netPremium   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwPolicy   
&Scoped-define SELF-NAME brwPolicy
&Scoped-define QUERY-STRING-brwPolicy FOR EACH processedform
&Scoped-define OPEN-QUERY-brwPolicy OPEN QUERY {&SELF-NAME} FOR EACH processedform.
&Scoped-define TABLES-IN-QUERY-brwPolicy processedform
&Scoped-define FIRST-TABLE-IN-QUERY-brwPolicy processedform


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwPolicy flLiabilityAmt flStat flStage ~
flReportingDate flClosingDate flTranType flInsuredType flInvoiceAmt ~
flPaidAmt flOsAmt edNotes edARNotes edAddress fIFileType flFirstCPLIssueDt ~
flFirstPolicyIssueDt flOrigin RECT-1 RECT-2 RECT-3 
&Scoped-Define DISPLAYED-OBJECTS tPolicyLabel flStat flStage ~
flReportingDate flClosingDate flTranType flInsuredType flInvoiceAmt ~
flPaidAmt flOsAmt edNotes edARNotes edAddress fIFileType flFirstCPLIssueDt ~
flFirstPolicyIssueDt flOrigin 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edAddress AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 57 BY 3.86 NO-UNDO.

DEFINE VARIABLE edARNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 57 BY 2.86 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 57 BY 2.86 NO-UNDO.

DEFINE VARIABLE fIFileType AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY .95 NO-UNDO.

DEFINE VARIABLE flClosingDate AS DATE FORMAT "99/99/99":U 
     LABEL "Closing" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flFirstCPLIssueDt AS DATETIME FORMAT "99/99/99":U 
     LABEL "First CPL Issue" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flFirstPolicyIssueDt AS DATETIME FORMAT "99/99/99":U 
     LABEL "First Policy Issue" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flInsuredType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flInvoiceAmt AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Invoice Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flLiabilityAmt AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flOrigin AS CHARACTER FORMAT "X(256)":U 
     LABEL "Origin" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY .95 NO-UNDO.

DEFINE VARIABLE flOsAmt AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Remaining Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flPaidAmt AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Paid Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flReportingDate AS DATE FORMAT "99/99/99":U 
     LABEL "Reporting" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flStage AS CHARACTER FORMAT "X(256)":U 
     LABEL "Stage" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "I)ssued, P)rocessed, V)oided" NO-UNDO.

DEFINE VARIABLE flStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE flTranType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Transaction Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicyLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Processed Forms" 
      VIEW-AS TEXT 
     SIZE 21 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89 BY 3.86.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89 BY 2.86.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89 BY 2.86.

DEFINE BUTTON bAgentLookup  NO-FOCUS
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bEditFile  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify the File Number".

DEFINE BUTTON bGet  NO-FOCUS
     LABEL "Get" 
     SIZE 4.8 BY 1.14 TOOLTIP "Get data".

DEFINE VARIABLE flAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 64.4 BY 1 NO-UNDO.

DEFINE VARIABLE flFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 44.6 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwPolicy FOR 
      processedform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwPolicy C-Win _FREEFORM
  QUERY brwPolicy DISPLAY
      processedform.formUniqueID label "Policy" format "zzzzzzzzz"                 width 12
      processedform.formtype label "FormType" format " x(2)"   
processedform.formID label "FormID" format " x(11)"                             width 15
processedform.statcode label "STAT" format " x(10)"                             width 14
processedform.effDate label "Effective Date" format "99/99/9999"                width 14
processedform.invoiceDate label "Invoice Date" format "99/99/9999"               width 14

processedform.liabilityAmount label "Liability Amount" format "zzz,zzz,zz9.99"   width 17
processedform.grossPremium label "Gross Premium" format "z,zzz,zz9.99"            width 17
processedform.retentionPremium label "Retained Premium" format "z,zzz,zz9.99"     width 17
processedform.netPremium label "Net Premium" format "z,zzz,zz9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 158.8 BY 10.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Double-click to open Policy Details for the selected policy".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME frameFile
     bEditFile AT ROW 2.52 COL 54.8 WIDGET-ID 148 NO-TAB-STOP 
     tAgentID AT ROW 1.48 COL 8 COLON-ALIGNED WIDGET-ID 16
     flAgentName AT ROW 1.48 COL 27.4 COLON-ALIGNED NO-LABEL WIDGET-ID 70 NO-TAB-STOP 
     flFileNumber AT ROW 2.57 COL 8 COLON-ALIGNED WIDGET-ID 74
     bGet AT ROW 2.52 COL 59.4 WIDGET-ID 150 NO-TAB-STOP 
     bAgentLookup AT ROW 1.38 COL 24.2 WIDGET-ID 350 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER OVERLAY NO-HELP 
         PAGE-TOP SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COLUMN 1.2 ROW 1.05
         SIZE 162.6 BY 3.1 WIDGET-ID 400.

DEFINE FRAME fMain
     brwPolicy AT ROW 13.38 COL 3 WIDGET-ID 300
     tPolicyLabel AT ROW 12.71 COL 3.2 NO-LABEL WIDGET-ID 62
     flLiabilityAmt AT ROW 9.91 COL 64.6 COLON-ALIGNED WIDGET-ID 32 NO-TAB-STOP 
     flStat AT ROW 2.57 COL 19.2 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     flStage AT ROW 1.48 COL 19.2 COLON-ALIGNED WIDGET-ID 68 NO-TAB-STOP 
     flReportingDate AT ROW 6.24 COL 19.2 COLON-ALIGNED WIDGET-ID 126
     flClosingDate AT ROW 6.24 COL 64.6 COLON-ALIGNED WIDGET-ID 90
     flTranType AT ROW 7.33 COL 19.2 COLON-ALIGNED WIDGET-ID 108
     flInsuredType AT ROW 7.33 COL 64.6 COLON-ALIGNED WIDGET-ID 120
     flInvoiceAmt AT ROW 9.91 COL 19.2 COLON-ALIGNED WIDGET-ID 2
     flPaidAmt AT ROW 11 COL 19.2 COLON-ALIGNED WIDGET-ID 124
     flOsAmt AT ROW 11 COL 64.6 COLON-ALIGNED WIDGET-ID 122
     edNotes AT ROW 5.86 COL 104.8 NO-LABEL WIDGET-ID 26
     edARNotes AT ROW 9.57 COL 104.8 NO-LABEL WIDGET-ID 128
     edAddress AT ROW 1.19 COL 104.8 NO-LABEL WIDGET-ID 132
     fIFileType AT ROW 1.48 COL 64.6 COLON-ALIGNED WIDGET-ID 142
     flFirstCPLIssueDt AT ROW 3.67 COL 19.2 COLON-ALIGNED WIDGET-ID 138
     flFirstPolicyIssueDt AT ROW 3.67 COL 64.6 COLON-ALIGNED WIDGET-ID 140
     flOrigin AT ROW 2.57 COL 64.6 COLON-ALIGNED WIDGET-ID 136
     "Notes:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 5.86 COL 98.2 WIDGET-ID 28
     "A/R Notes:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 9.57 COL 93.6 WIDGET-ID 130
     "Property:" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 1.14 COL 96 WIDGET-ID 134
     RECT-1 AT ROW 1.19 COL 3 WIDGET-ID 144
     RECT-2 AT ROW 5.86 COL 3 WIDGET-ID 146
     RECT-3 AT ROW 9.57 COL 3 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 4.14
         SIZE 184.4 BY 24.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent File Details"
         HEIGHT             = 26.43
         WIDTH              = 162.8
         MAX-HEIGHT         = 38.81
         MAX-WIDTH          = 288
         VIRTUAL-HEIGHT     = 38.81
         VIRTUAL-WIDTH      = 288
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwPolicy 1 fMain */
ASSIGN 
       edAddress:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       edARNotes:RETURN-INSERTED IN FRAME fMain  = TRUE
       edARNotes:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME fMain  = TRUE
       edNotes:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       fIFileType:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flClosingDate:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flFirstCPLIssueDt:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flFirstPolicyIssueDt:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flInsuredType:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flInvoiceAmt:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN flLiabilityAmt IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       flLiabilityAmt:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flOrigin:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flOsAmt:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flPaidAmt:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flReportingDate:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flStage:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flStat:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       flTranType:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolicyLabel IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       tPolicyLabel:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME frameFile
                                                                        */
/* SETTINGS FOR BUTTON bEditFile IN FRAME frameFile
   NO-ENABLE                                                            */
ASSIGN 
       flAgentName:READ-ONLY IN FRAME frameFile        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwPolicy
/* Query rebuild information for BROWSE brwPolicy
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH processedform.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwPolicy */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frameFile
/* Query rebuild information for FRAME frameFile
     _Query            is NOT OPENED
*/  /* FRAME frameFile */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent File Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent File Details */
DO:
  /* This event will close the window and terminate the procedure.  */
    run closeWindow.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent File Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frameFile
&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME frameFile /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input tAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      tAgentID:screen-value    = cAgentID
      flAgentName:screen-value = cName
      . 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditFile C-Win
ON CHOOSE OF bEditFile IN FRAME frameFile /* Edit */
DO:
  run modifyFileNumber in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGet C-Win
ON CHOOSE OF bGet IN FRAME frameFile /* Get */
OR 'RETURN' of tAgentID
OR 'RETURN' of flFileNumber
DO:
  if not validAgent()
   then
    return no-apply.
    
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwPolicy
&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME brwPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPolicy C-Win
ON DEFAULT-ACTION OF brwPolicy IN FRAME fMain
DO:
  if not available processedform
   then return.
  if processedform.formtype = "P" or
     processedform.formtype = "E"
   then
    do:
       publish "SetCurrentValue" ("PolicyID", string(processedform.formUniqueID)). 
       run wpolicy.w persistent.
    end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes C-Win
ON VALUE-CHANGED OF edNotes IN FRAME fMain
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

assign
 {&WINDOW-NAME}:min-height-pixels = {&WINDOW-NAME}:height-pixels
 {&WINDOW-NAME}:max-height-pixels = session:height-pixels
 {&WINDOW-NAME}:min-width-pixels = {&WINDOW-NAME}:width-pixels
 {&WINDOW-NAME}:max-width-pixels = {&WINDOW-NAME}:width-pixels
 .
 
bGet:load-image("images/s-completed.bmp"). 
bGet:load-image-insensitive("images/s-completed-i.bmp").
bAgentLookup:load-image("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").
bEditFile:load-image("images/s-update.bmp").
bEditFile:load-image-insensitive("images/s-update-i.bmp").

publish "GetCurrentValue" ("AgentID", output tAgentID).
publish "GetCurrentValue" ("FileNumber", output flFileNumber).

/* this procedurte pulbish as well as subscribe these events because same event can be published from 
other window apolicy.w in that case this window need to subscribe that events*/
subscribe to "RefreshScreensForFileNumModify" anywhere.
subscribe to "CloseScreensForFileNumModify" anywhere.

{lib/win-main.i}

status default "" in window {&window-name}.
status input "" in window {&window-name}.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  publish "GetAutoView" (output std-lo).
  if std-lo
    and tAgentID:screen-value in frame frameFile > ""
    and flFileNumber:screen-value in frame frameFile > "" 
   then 
    do:
      assign
          bGet:sensitive         = false
          bAgentLookup:sensitive = false
          tAgentID:sensitive     = false
          flFileNumber:sensitive = false
          bEditFile:sensitive    = false
          .
      
      if not validAgent()
       then return.
      
      run getData.
    end.
     
  apply "ENTRY" to tAgentID.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseScreensForFileNumModify C-Win 
PROCEDURE CloseScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.
 do with frame frameFile:
 end.
 if valid-handle(this-procedure) and
    tAgentID:screen-value = pAgentID and
    (normalizeFileID(flFileNumber:screen-value) = pOldFileID or 
    normalizeFileID(flFileNumber:screen-value) = pNewFileID )
  then
   run closewindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "WindowClosed" (input this-procedure).
 apply "close":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount          as integer   no-undo.  
  define variable cFormattedNotes as character no-undo.

  find first agentfile no-error.
  if not available agentfile 
   then return.
   
  do with frame fMain:
  end. 
  assign     
      flAgentName:screen-value in frame frameFile  = agentfile.agentname     
      flFileNumber:screen-value in frame frameFile = agentfile.fileNumber    
      flStat:screen-value                          = if agentfile.stat = "O" then "Open" else "Closed"
      flStage:screen-value                         = agentfile.stage
      edNotes:screen-value                         = agentfile.notes    
      flTranType:screen-value                      = agentfile.transactionType
      flInsuredType:screen-value                   = agentfile.insuredType
      flReportingDate:screen-value                 = string(agentfile.reportingDate)
      flClosingDate:screen-value                   = string(agentfile.closingDate)
      flInvoiceAmt:screen-value                    = string(agentfile.invoiceAmt)
      flLiabilityAmt:screen-value                  = string(agentfile.liabilityAmt)
      flPaidAmt:screen-value                       = string(agentfile.paidAmt)
      flOsAmt:screen-value                         = string(agentfile.osAmt)
      flOrigin:screen-value                        = agentfile.origin
      flFirstCPLIssueDt:screen-value               = string(agentfile.firstCPLIssueDt)
      flFirstPolicyIssueDt:screen-value            = string(agentfile.firstPolicyIssueDt)
      fIFileType:screen-value                      = agentfile.fileType  
      .
         
  do iCount = 1 to num-entries(agentfile.arnotes,"["):
    if entry(iCount,agentfile.arnotes,"[") = "" 
     then next.
    cFormattedNotes = (if cFormattedNotes <> "" then cFormattedNotes + chr(10) + chr(10) else "") + "[" + entry(iCount,agentfile.arnotes,"["). 
  end.    
      
  /* Storing Address */
  std-ch = ((if agentfile.cplAddr1    <> "" then agentfile.cplAddr1    + chr(10) else "") +
            (if agentfile.cplCity     <> "" then agentfile.cplCity     + ", "    else "") +
            (if agentfile.cplState    <> "" then agentfile.cplState    + " "     else "") +
            (if agentfile.cplZip      <> "" then agentfile.cplZip      + ", "    else "")).
      
  
  assign
      edAddress:screen-value = trim(std-ch,", ")
      edARNotes:screen-value = cFormattedNotes 
      bEditFile:sensitive    = (tAgentID:screen-value > "") and tAgentID:sensitive
      .  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAgentID flAgentName flFileNumber 
      WITH FRAME frameFile IN WINDOW C-Win.
  ENABLE tAgentID flAgentName flFileNumber bGet bAgentLookup 
      WITH FRAME frameFile IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frameFile}
  DISPLAY tPolicyLabel flStat flStage flReportingDate flClosingDate flTranType 
          flInsuredType flInvoiceAmt flPaidAmt flOsAmt edNotes edARNotes 
          edAddress fIFileType flFirstCPLIssueDt flFirstPolicyIssueDt flOrigin 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwPolicy flLiabilityAmt flStat flStage flReportingDate flClosingDate 
         flTranType flInsuredType flInvoiceAmt flPaidAmt flOsAmt edNotes 
         edARNotes edAddress fIFileType flFirstCPLIssueDt flFirstPolicyIssueDt 
         flOrigin RECT-1 RECT-2 RECT-3 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportErrorData C-Win 
PROCEDURE exportErrorData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcCurrent as character no-undo.
  
  if can-find(first fileErr)
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "ErrorsModifyFileNumber(" + ipcCurrent + ")_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table fileErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "fileErr",
                              "for each fileErr",
                              "err,description",
                              "Sequence,Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
                              
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if tAgentID:screen-value in frame frameFile = ""
   or flFileNumber:screen-value in frame frameFile = ""
  then return.
  
 close query brwPolicy.
 empty temp-table processedform. 
 run server/getagentfile.p (input tAgentID:screen-value in frame frameFile,
                            input flFileNumber:screen-value in frame frameFile,
                            output table agentFile,
                            output table processedform,
                            output std-lo,
                            output std-ch).

 if not std-lo 
  then
   do:
     message std-ch
         view-as alert-box error buttons ok.
     return.
   end.

 std-ch = flFileNumber:screen-value in frame frameFile + " (" + string(now) + ")".
 
 status default std-ch in window {&window-name}.

 dataSortBy = "".
 run sortData("formuniqueid").
 run displayData.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyFileNumber C-Win 
PROCEDURE modifyFileNumber PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tCurrent       as character no-undo.
 define variable tNew           as character no-undo.
 define variable tNewFileID     as character no-undo.
 define variable tSave          as logical   no-undo init false.
 define variable pChoice        as character no-undo.
 define variable tCurrentFileID as character no-undo.
 define variable tMergeFiles    as logical   no-undo init false.
 define variable tNewFileExist  as logical   no-undo init false.
 
  
 if not available agentFile 
  then return.

 tCurrent       = flFileNumber:screen-value in frame frameFile.
 tNew           = tCurrent.
 tCurrentFileID = normalizeFileID(tCurrent).

 REPEAT-LOOP:
 repeat with frame fMain:
   run dialogModifyFileNumber.w (tCurrent,
                                 input-output tNew,
                                 output tSave).
                                  
   if not tSave 
    then leave REPEAT-LOOP.

   if tCurrent = tNew 
    then leave REPEAT-LOOP.

   if tNew = "" or tNew = ? 
    then
     do:
         message "File Number cannot be blank or unknown."
            view-as alert-box error buttons ok.
         next.
     end.

   run server/modifyfilenumber.p (agentFile.agentID,
                                  tCurrent,
                                  tNew,
                                  tMergeFiles,
                                  output tNewFileID,
                                  output tNewFileExist,
                                  output table fileErr,
                                  output std-lo,
                                  output std-ch
                                  ).
                                  
  if tNewFileExist 
   then
   do:
   
    MESSAGE "New File Number already exist. Files will be merged to the new file number. Continue?"
           VIEW-AS ALERT-BOX question BUTTONS Yes-No update tMergeFiles.
    
    if tMergeFiles
     then 
       run server/modifyfilenumber.p (agentFile.agentID,
                                     tCurrent,
                                     tNew,
                                     tMergeFiles,
                                     output tNewFileID,
                                     output tNewFileExist,
                                     output table fileErr,
                                     output std-lo,
                                     output std-ch
                                     ).
    else
     next.
          
   end.
  
  /* Export error table into csv and open it */
  if can-find(first fileErr)
   then
   do:
    run exportErrorData(tCurrent). 
    next.
   end.
  
   if not std-lo 
    then
     do:
        message std-ch
            view-as alert-box error buttons ok.
        next.
     end.
     
   flFileNumber:screen-value = tNew.
   
   run getdata.
   
   run dialogModifyFileNumbermsg.w (input  "F",
                                    input  "",
                                    input  tCurrent,
                                    input  tNew,
                                    output pChoice).
   case pChoice:
     when "1"
      then
       publish "RefreshScreensForFileNumModify"(tAgentID:screen-value,
                                                tCurrentFileID,
                                                tNewFileID).
     when "2" 
      then
       publish "CloseScreensForFileNumModify"(tAgentID:screen-value,
                                              tCurrentFileID,
                                              tNewFileID).

   end case.
   
   leave REPEAT-LOOP.
 end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshScreensForFileNumModify C-Win 
PROCEDURE RefreshScreensForFileNumModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pAgentID as character.
 define input parameter pOldFileID as character.
 define input parameter pNewFileID as character.
 do with frame frameFile:
 end.
 if tAgentID:screen-value = pAgentID and
    (normalizeFileID(flFileNumber:screen-value) = pOldFileID or 
     normalizeFileID(flFileNumber:screen-value) = pNewFileID )
  then
   run getData.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels - 66.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels - 66.
 
 browse brwPolicy:height-pixels = frame {&frame-name}:height-pixels - 263. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwPolicy.
 clear frame fMain no-pause.
 status default "" in window {&window-name}.
 
  bEditFile:sensitive in frame frameFile = false.
 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame frameFile:
  end. 
  
  if tAgentID:input-value = ""
   then return false. /* Function return value. */
  
  else if tAgentID:input-value = "ALL"
   then
    flAgentName:screen-value = "N/A".
       
  else if tAgentID:input-value <> "ALL"
   then
    do:
      publish "getAgentName" (input tAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do: 
          flAgentName:screen-value = "".              .
          return false. /* Function return value. */
        end.
      flAgentName:screen-value = std-ch.
    end.  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

