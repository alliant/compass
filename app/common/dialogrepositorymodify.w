&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/repository.i}

/* Parameters Definitions ---                                           */
define input        parameter table     for repository.
define input        parameter pID       as character no-undo.
define input-output parameter pFile     as character no-undo.
define input-output parameter pCategory as character no-undo.
define input-output parameter pPrivate  as logical   no-undo.
define       output parameter pSave     as logical   no-undo initial false.

/* Local Variable Definitions ---                                       */
define variable tDefaultCategoryList as character no-undo.
define variable tLink                as character no-undo.
{lib/std-def.i}
{lib/set-button-def.i}

/* Functions and Procedures ---                                         */
{lib/validFilename.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tUpload tCategory tPrivate bOK bCancel 
&Scoped-Define DISPLAYED-OBJECTS tUpload tCategory tPrivate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD improveReadability Dialog-Frame 
FUNCTION improveReadability RETURNS CHARACTER
  ( input pText as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bOK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tCategory AS CHARACTER 
     LABEL "Category" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     DROP-DOWN
     SIZE 46 BY 1 TOOLTIP "Select a Category" NO-UNDO.

DEFINE VARIABLE tUpload AS CHARACTER FORMAT "X(256)":U 
     LABEL "Filename" 
     VIEW-AS FILL-IN 
     SIZE 59 BY 1 TOOLTIP "Type in a Filename" NO-UNDO.

DEFINE VARIABLE tPrivate AS LOGICAL INITIAL no 
     LABEL "Private?" 
     VIEW-AS TOGGLE-BOX
     SIZE 12 BY .81 TOOLTIP "Should the file be marked as Private?" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tUpload AT ROW 1.48 COL 10 COLON-ALIGNED WIDGET-ID 2
     tCategory AT ROW 2.67 COL 10 COLON-ALIGNED WIDGET-ID 6
     tPrivate AT ROW 2.76 COL 59.8 WIDGET-ID 8
     bOK AT ROW 4.1 COL 21
     bCancel AT ROW 4.1 COL 37
     SPACE(20.79) SKIP(0.42)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify File"
         DEFAULT-BUTTON bOK CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Modify File */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK Dialog-Frame
ON CHOOSE OF bOK IN FRAME Dialog-Frame /* Save */
DO:
  define variable tBad         as character no-undo.
  if not validFilenameWindows(tUpload:screen-value, output tBad)
   then
    do:
      message "The filename must not contain the following characters: " + improveReadability(tBad) view-as alert-box warning buttons ok.
      tUpload:screen-value = pFile.
      return no-apply.
    end.
  
  publish "CheckDisplayName" (tUpload:screen-value, tCategory:screen-value, pID, output std-lo).
  if not std-lo
   then
    do:
      tUpload:screen-value = "".
      apply "ENTRY" to tUpload.
      return no-apply.
    end.

  pSave = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUpload Dialog-Frame
ON MOUSE-SELECT-DBLCLICK OF tUpload IN FRAME Dialog-Frame /* Filename */
DO:
  if self:screen-value = ""
   then return.
   
  run util/openfile.p (tUpload:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

{lib/set-button.i &label="Upload" &image="images/s-upload.bmp" &inactive="images/s-upload-i.bmp"}
setButtons().

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  tCategory:list-items = " ".
  
  /* add the default entries */
  std-ch = "".
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = ""
   then
    do:
      publish "GetSysPropList" (std-ch, "Repository", "Category", output tDefaultCategoryList).
      do std-in = 1 to num-entries(tDefaultCategoryList, {&msg-dlm}):
        std-ch = entry(std-in, tDefaultCategoryList, {&msg-dlm}).
        if tCategory:list-items = ? or lookup(std-ch, tCategory:list-items, tCategory:delimiter) = 0
         then tCategory:add-last(std-ch).
      end.
    end.
  
  /* add the categories the user has */
  std-lo = true.
  for each repository no-lock:
    if lookup(repository.category, tCategory:list-items) = 0
     then tCategory:add-last(repository.category).
     
    if repository.identifier = pID
     then std-lo = repository.fileSize > 0.
  end.
  tCategory:delete(1).
  
  assign
    tUpload:screen-value      = pFile
    tCategory:screen-value    = pCategory
    tPrivate:checked          = pPrivate
    tPrivate:sensitive        = std-lo
    frame {&frame-name}:title = if std-lo then "Modify Linked Document" else "Modify File"
    .
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if pSave
   then
    assign
      pFile     = tUpload:screen-value
      pCategory = tCategory:screen-value
      pPrivate  = tPrivate:checked
      .
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tUpload tCategory tPrivate 
      WITH FRAME Dialog-Frame.
  ENABLE tUpload tCategory tPrivate bOK bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION improveReadability Dialog-Frame 
FUNCTION improveReadability RETURNS CHARACTER
  ( input pText as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tCounter as integer   no-undo.
  define variable tLength  as integer   no-undo.
  define variable tText    as character no-undo.

  tLength = length(pText).
  do tCounter = 1 to tLength:
    tText = tText + substring(pText, tCounter, 1) + " ".
  end.
  RETURN tText.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

