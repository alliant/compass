&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogagentlookup.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created:12/31/19
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}

{tt/state.i}
{tt/agent.i}
{tt/agent.i &tableAlias=ttagent}

/* Parameters Definitions ---                                           */
define input  parameter ipcAgentID    as  character no-undo.
define input  parameter ipcStateID    as  character no-undo.
define input  parameter iplAllowAll   as  logical   no-undo.
define output parameter opcAgentID    as  character no-undo.
define output parameter opcStateID    as  character no-undo.
define output parameter opcName       as  character no-undo.
define output parameter oplSuccess    as  logical   no-undo.

/* Local Variable */
define variable lApplySearchString as logical     no-undo.
define variable cSearchString      as character   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAgent

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agent

/* Definitions for BROWSE brwAgent                                      */
&Scoped-define FIELDS-IN-QUERY-brwAgent agent.stateID "State ID" agent.agentID "Agent ID" agent.name "Name"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAgent   
&Scoped-define SELF-NAME brwAgent
&Scoped-define QUERY-STRING-brwAgent preselect each agent
&Scoped-define OPEN-QUERY-brwAgent open query brwAgent preselect each agent.
&Scoped-define TABLES-IN-QUERY-brwAgent agent
&Scoped-define FIRST-TABLE-IN-QUERY-brwAgent agent


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAgent}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState fSearch bSearch brwAgent Btn_OK ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cbState fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search Locks".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 44.6 BY 1 TOOLTIP "Search Criteria (Code Type, Code, Type, Description)" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAgent FOR 
      agent SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAgent Dialog-Frame _FREEFORM
  QUERY brwAgent DISPLAY
      agent.stateID     label        "State ID"   format "x(12)"           
agent.agentID           label        "Agent ID"   format "x(16)" 
agent.name              label        "Name"       format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 98 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbState AT ROW 1.86 COL 9 COLON-ALIGNED WIDGET-ID 242
     fSearch AT ROW 1.86 COL 47.6 COLON-ALIGNED WIDGET-ID 66
     bSearch AT ROW 1.76 COL 94.4 WIDGET-ID 314
     brwAgent AT ROW 3.71 COL 3 WIDGET-ID 300
     Btn_OK AT ROW 16.33 COL 35.8
     Btn_Cancel AT ROW 16.33 COL 52.4
     SPACE(34.79) SKIP(0.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Agents"
         DEFAULT-BUTTON bSearch CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAgent bSearch Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwAgent:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwAgent:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAgent
/* Query rebuild information for BROWSE brwAgent
     _START_FREEFORM
open query brwAgent preselect each agent.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAgent */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Agents */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAgent
&Scoped-define SELF-NAME brwAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgent Dialog-Frame
ON DEFAULT-ACTION OF brwAgent IN FRAME Dialog-Frame
DO:
  apply "Choose" to Btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgent Dialog-Frame
ON ROW-DISPLAY OF brwAgent IN FRAME Dialog-Frame
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgent Dialog-Frame
ON START-SEARCH OF brwAgent IN FRAME Dialog-Frame
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch Dialog-Frame
ON CHOOSE OF bSearch IN FRAME Dialog-Frame /* Search */
DO:
  lApplySearchString = true.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run setAgentID in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState Dialog-Frame
ON VALUE-CHANGED OF cbState IN FRAME Dialog-Frame /* State */
DO:
  run filterData in this-procedure.       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON ENTRY OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  /* store the previous value of search string on which search is applied */
  cSearchString = fSearch:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON VALUE-CHANGED OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  /* as soon as we change the search string, we track that string 
  is not applied and change the status in taskbar */
  lApplySearchString = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bSearch:load-image("images/s-magnifier.bmp").
bSearch:load-image-insensitive("images/s-magnifier-i.bmp").
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  /* create state combo */
  {lib/get-state-list.i &combo=cbState &addAll=true}
  
  run getData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState fSearch 
      WITH FRAME Dialog-Frame.
  ENABLE cbState fSearch bSearch brwAgent Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData Dialog-Frame 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttagent for ttagent.
  
  close query brwAgent.  
  empty temp-table agent.
  
  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).
  /* if search string is changed but not applied then restrore the fSearch fill-in
     to previous applied serach string and filter on the basis of what is
     present inside fsearch fill-in */
  else
    fSearch:screen-value = cSearchString.

  if iplAllowAll  
   then
    do:
      create agent.
      assign 
          agent.stateID = {&NotApplicable}
          agent.agentID = {&ALL}
          agent.name    = {&NotApplicable}
          .
    end.
    
  for each ttagent where ttagent.stateID = (if cbState:input-value <> {&ALL} then cbState:input-value
                                            else ttagent.stateID) and 
                                           ((if cSearchString <> "" then ttagent.agentID matches ("*" + cSearchString + "*")
                                             else ttagent.agentID = ttagent.agentID) or
                                            (if cSearchString <> "" then ttagent.name matches ("*" + cSearchString + "*")
                                             else ttagent.name = ttagent.name) or
                                            (if cSearchString <> "" then ttagent.corporationID matches ("*" + cSearchString + "*")
                                             else ttagent.corporationID = ttagent.corporationID)):
    
    create agent.
    buffer-copy ttagent to agent.
  end.

  open query brwAgent for each agent by agent.agentId desc.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.

  if ipcStateID <> "" 
   then
    cbState:screen-value = ipcStateID.
    
  publish "getAgents" (output table ttagent).
    
  run filterData in this-procedure.
  
  find first agent where agent.agentID = ipcAgentID  no-error.

  if available agent
   then      
    reposition brwAgent to rowid rowid(agent) no-error. 
  
  apply 'entry' to fSearch.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAgentID Dialog-Frame 
PROCEDURE setAgentID :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if available agent
   then
    assign
        opcAgentID = agent.agentID
        opcStateID = (if agent.agentID = {&ALL} then cbState:screen-value else agent.stateID)
        opcName    = agent.name
        oplSuccess = yes
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

