&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fContacts 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{tt/distribution.i &tableAlias="out-contact"}
define input        parameter pName   as character no-undo.
define input-output parameter table   for out-contact.
define       output parameter pCancel as logical   no-undo initial true.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/set-button-def.i}

/* Temp Table Definitions ---                                           */
{tt/distribution.i &tableAlias="contact"}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fContacts
&Scoped-define BROWSE-NAME brwContacts

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES contact

/* Definitions for BROWSE brwContacts                                   */
&Scoped-define FIELDS-IN-QUERY-brwContacts contact.isSelected contact.contactName contact.agentName contact.email   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwContacts contact.isSelected   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwContacts contact
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwContacts contact
&Scoped-define SELF-NAME brwContacts
&Scoped-define QUERY-STRING-brwContacts FOR EACH contact
&Scoped-define OPEN-QUERY-brwContacts OPEN QUERY {&SELF-NAME} FOR EACH contact.
&Scoped-define TABLES-IN-QUERY-brwContacts contact
&Scoped-define FIRST-TABLE-IN-QUERY-brwContacts contact


/* Definitions for DIALOG-BOX fContacts                                 */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bFirstA bGo bLastA RECT-63 tSearch tState ~
brwContacts bSave bCancel bLastB bLastC bLastD bLastE bLastF bLastG bLastH ~
bLastI bLastJ bLastK bLastL bLastM bLastN bLastO bLastP bLastQ bLastR ~
bLastS bLastT bFirstB bLastU bLastV bLastW bLastX bFirstC bLastY bFirstD ~
bLastZ bFirstE bFirstF bFirstG bFirstH bFirstI bFirstJ bFirstK bFirstL ~
bFirstM bFirstN bFirstO bFirstP bFirstQ bFirstR bFirstS bFirstT bFirstU ~
bFirstV bFirstW bFirstX bFirstY bFirstZ tCount 
&Scoped-Define DISPLAYED-OBJECTS tSearch tState tCount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bFirstA  NO-FOCUS
     LABEL "A" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstB  NO-FOCUS
     LABEL "B" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstC  NO-FOCUS
     LABEL "C" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstD  NO-FOCUS
     LABEL "D" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstE  NO-FOCUS
     LABEL "E" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstF  NO-FOCUS
     LABEL "F" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstG  NO-FOCUS
     LABEL "G" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstH  NO-FOCUS
     LABEL "H" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstI  NO-FOCUS
     LABEL "I" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstJ  NO-FOCUS
     LABEL "J" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstK  NO-FOCUS
     LABEL "K" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstL  NO-FOCUS
     LABEL "L" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstM  NO-FOCUS
     LABEL "M" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstN  NO-FOCUS
     LABEL "N" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstO  NO-FOCUS
     LABEL "O" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstP  NO-FOCUS
     LABEL "P" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstQ  NO-FOCUS
     LABEL "Q" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstR  NO-FOCUS
     LABEL "R" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstS  NO-FOCUS
     LABEL "S" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstT  NO-FOCUS
     LABEL "T" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstU  NO-FOCUS
     LABEL "U" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstV  NO-FOCUS
     LABEL "V" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstW  NO-FOCUS
     LABEL "W" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstX  NO-FOCUS
     LABEL "X" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstY  NO-FOCUS
     LABEL "Y" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bFirstZ  NO-FOCUS
     LABEL "Z" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bLastA  NO-FOCUS
     LABEL "A" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastB  NO-FOCUS
     LABEL "B" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastC  NO-FOCUS
     LABEL "C" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastD  NO-FOCUS
     LABEL "D" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastE  NO-FOCUS
     LABEL "E" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastF  NO-FOCUS
     LABEL "F" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastG  NO-FOCUS
     LABEL "G" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastH  NO-FOCUS
     LABEL "H" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastI  NO-FOCUS
     LABEL "I" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastJ  NO-FOCUS
     LABEL "J" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastK  NO-FOCUS
     LABEL "K" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastL  NO-FOCUS
     LABEL "L" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastM  NO-FOCUS
     LABEL "M" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastN  NO-FOCUS
     LABEL "N" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastO  NO-FOCUS
     LABEL "O" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastP  NO-FOCUS
     LABEL "P" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastQ  NO-FOCUS
     LABEL "Q" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastR  NO-FOCUS
     LABEL "R" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastS  NO-FOCUS
     LABEL "S" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastT  NO-FOCUS
     LABEL "T" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastU  NO-FOCUS
     LABEL "U" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastV  NO-FOCUS
     LABEL "V" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastW  NO-FOCUS
     LABEL "W" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastX  NO-FOCUS
     LABEL "X" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastY  NO-FOCUS
     LABEL "Y" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bLastZ  NO-FOCUS
     LABEL "Z" 
     SIZE 3.6 BY .86 TOOLTIP "First Name".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21.4 BY 1 NO-UNDO.

DEFINE VARIABLE tCount AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 45 BY .62 NO-UNDO.

DEFINE VARIABLE tSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 108.6 BY 1 TOOLTIP "Enter keywords or #tags separated by spaces" NO-UNDO.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 157.6 BY 2.43.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwContacts FOR 
      contact SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwContacts fContacts _FREEFORM
  QUERY brwContacts DISPLAY
      contact.isSelected view-as toggle-box
      contact.contactName width 41
contact.agentName width 50
contact.email width 40
enable contact.isSelected
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS SIZE 150 BY 20.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fContacts
     bFirstA AT ROW 4.1 COL 1.8 WIDGET-ID 208 NO-TAB-STOP 
     bGo AT ROW 1.57 COL 150.4 WIDGET-ID 74
     bLastA AT ROW 4.1 COL 156.2 WIDGET-ID 538 NO-TAB-STOP 
     tSearch AT ROW 1.95 COL 9.4 COLON-ALIGNED WIDGET-ID 370
     tState AT ROW 1.95 COL 125.8 COLON-ALIGNED WIDGET-ID 372
     brwContacts AT ROW 4.19 COL 5.8 WIDGET-ID 200
     bSave AT ROW 25.52 COL 65.4
     bCancel AT ROW 25.52 COL 81.4
     bLastB AT ROW 4.91 COL 156.2 WIDGET-ID 540 NO-TAB-STOP 
     bLastC AT ROW 5.71 COL 156.2 WIDGET-ID 542 NO-TAB-STOP 
     bLastD AT ROW 6.52 COL 156.2 WIDGET-ID 544 NO-TAB-STOP 
     bLastE AT ROW 7.33 COL 156.2 WIDGET-ID 546 NO-TAB-STOP 
     bLastF AT ROW 8.14 COL 156.2 WIDGET-ID 548 NO-TAB-STOP 
     bLastG AT ROW 8.95 COL 156.2 WIDGET-ID 550 NO-TAB-STOP 
     bLastH AT ROW 9.76 COL 156.2 WIDGET-ID 552 NO-TAB-STOP 
     bLastI AT ROW 10.57 COL 156.2 WIDGET-ID 554 NO-TAB-STOP 
     bLastJ AT ROW 11.38 COL 156.2 WIDGET-ID 556 NO-TAB-STOP 
     bLastK AT ROW 12.19 COL 156.2 WIDGET-ID 558 NO-TAB-STOP 
     bLastL AT ROW 13 COL 156.2 WIDGET-ID 560 NO-TAB-STOP 
     bLastM AT ROW 13.81 COL 156.2 WIDGET-ID 562 NO-TAB-STOP 
     bLastN AT ROW 14.62 COL 156.2 WIDGET-ID 564 NO-TAB-STOP 
     bLastO AT ROW 15.43 COL 156.2 WIDGET-ID 566 NO-TAB-STOP 
     bLastP AT ROW 16.24 COL 156.2 WIDGET-ID 568 NO-TAB-STOP 
     bLastQ AT ROW 17.05 COL 156.2 WIDGET-ID 570 NO-TAB-STOP 
     bLastR AT ROW 17.86 COL 156.2 WIDGET-ID 572 NO-TAB-STOP 
     bLastS AT ROW 18.67 COL 156.2 WIDGET-ID 574 NO-TAB-STOP 
     bLastT AT ROW 19.48 COL 156.2 WIDGET-ID 576 NO-TAB-STOP 
     bFirstB AT ROW 4.91 COL 1.8 WIDGET-ID 382 NO-TAB-STOP 
     bLastU AT ROW 20.29 COL 156.2 WIDGET-ID 578 NO-TAB-STOP 
     bLastV AT ROW 21.1 COL 156.2 WIDGET-ID 580 NO-TAB-STOP 
     bLastW AT ROW 21.91 COL 156.2 WIDGET-ID 582 NO-TAB-STOP 
     bLastX AT ROW 22.71 COL 156.2 WIDGET-ID 584 NO-TAB-STOP 
     bFirstC AT ROW 5.71 COL 1.8 WIDGET-ID 384 NO-TAB-STOP 
     bLastY AT ROW 23.52 COL 156.2 WIDGET-ID 586 NO-TAB-STOP 
     bFirstD AT ROW 6.52 COL 1.8 WIDGET-ID 386 NO-TAB-STOP 
     bLastZ AT ROW 24.33 COL 156.2 WIDGET-ID 588 NO-TAB-STOP 
     bFirstE AT ROW 7.33 COL 1.8 WIDGET-ID 388 NO-TAB-STOP 
     bFirstF AT ROW 8.14 COL 1.8 WIDGET-ID 390 NO-TAB-STOP 
     bFirstG AT ROW 8.95 COL 1.8 WIDGET-ID 392 NO-TAB-STOP 
     bFirstH AT ROW 9.76 COL 1.8 WIDGET-ID 394 NO-TAB-STOP 
     bFirstI AT ROW 10.57 COL 1.8 WIDGET-ID 396 NO-TAB-STOP 
     bFirstJ AT ROW 11.38 COL 1.8 WIDGET-ID 398 NO-TAB-STOP 
     bFirstK AT ROW 12.19 COL 1.8 WIDGET-ID 400 NO-TAB-STOP 
     bFirstL AT ROW 13 COL 1.8 WIDGET-ID 430 NO-TAB-STOP 
     bFirstM AT ROW 13.81 COL 1.8 WIDGET-ID 428 NO-TAB-STOP 
     bFirstN AT ROW 14.62 COL 1.8 WIDGET-ID 426 NO-TAB-STOP 
     bFirstO AT ROW 15.43 COL 1.8 WIDGET-ID 424 NO-TAB-STOP 
     bFirstP AT ROW 16.24 COL 1.8 WIDGET-ID 422 NO-TAB-STOP 
     bFirstQ AT ROW 17.05 COL 1.8 WIDGET-ID 420 NO-TAB-STOP 
     bFirstR AT ROW 17.86 COL 1.8 WIDGET-ID 418 NO-TAB-STOP 
     bFirstS AT ROW 18.67 COL 1.8 WIDGET-ID 416 NO-TAB-STOP 
     bFirstT AT ROW 19.48 COL 1.8 WIDGET-ID 414 NO-TAB-STOP 
     bFirstU AT ROW 20.29 COL 1.8 WIDGET-ID 412 NO-TAB-STOP 
     bFirstV AT ROW 21.1 COL 1.8 WIDGET-ID 410 NO-TAB-STOP 
     bFirstW AT ROW 21.91 COL 1.8 WIDGET-ID 408 NO-TAB-STOP 
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fContacts
     bFirstX AT ROW 22.71 COL 1.8 WIDGET-ID 406 NO-TAB-STOP 
     bFirstY AT ROW 23.52 COL 1.8 WIDGET-ID 404 NO-TAB-STOP 
     bFirstZ AT ROW 24.33 COL 1.8 WIDGET-ID 402 NO-TAB-STOP 
     tCount AT ROW 25.76 COL 4 COLON-ALIGNED NO-LABEL WIDGET-ID 590
     "Parameter" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 1.05 COL 3.6 WIDGET-ID 72
     RECT-63 AT ROW 1.24 COL 2 WIDGET-ID 70
     SPACE(0.99) SKIP(23.27)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "<insert dialog title>"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fContacts
   FRAME-NAME                                                           */
/* BROWSE-TAB brwContacts tState fContacts */
ASSIGN 
       FRAME fContacts:SCROLLABLE       = FALSE
       FRAME fContacts:HIDDEN           = TRUE.

ASSIGN 
       brwContacts:COLUMN-RESIZABLE IN FRAME fContacts       = TRUE
       brwContacts:COLUMN-MOVABLE IN FRAME fContacts         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwContacts
/* Query rebuild information for BROWSE brwContacts
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH contact.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwContacts */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fContacts fContacts
ON WINDOW-CLOSE OF FRAME fContacts /* <insert dialog title> */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFirstA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFirstA fContacts
ON CHOOSE OF bFirstA IN FRAME fContacts /* A */
,bFirstB,bFirstC,bFirstD,bFirstE,bFirstF,bFirstG,bFirstH,bFirstI,bFirstJ,bFirstK,bFirstL,bFirstM,bFirstN,bFirstO,bFirstP,bFirstQ,bFirstR,bFirstS,bFirstT,bFirstU,bFirstV,bFirstW,bFirstX,bFirstY,bFirstZ 
DO:
  assign
    tSearch:screen-value = ""
    tState:screen-value  = "ALL"
    .
  run addContact in this-procedure.
  run getData in this-procedure ("First", self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo fContacts
ON CHOOSE OF bGo IN FRAME fContacts /* Go */
DO:
  run getData in this-procedure ("", tSearch:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLastA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLastA fContacts
ON CHOOSE OF bLastA IN FRAME fContacts /* A */
,bLastB,bLastC,bLastD,bLastE,bLastF,bLastG,bLastH,bLastI,bLastJ,bLastK,bLastL,bLastM,bLastN,bLastO,bLastP,bLastQ,bLastR,bLastS,bLastT,bLastU,bLastV,bLastW,bLastX,bLastY,bLastZ 
DO:
  assign
    tSearch:screen-value = ""
    tState:screen-value  = "ALL"
    .
  run addContact in this-procedure.
  run getData in this-procedure ("Last", self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwContacts
&Scoped-define SELF-NAME brwContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts fContacts
ON DEFAULT-ACTION OF brwContacts IN FRAME fContacts
DO:
  if not available contact
   then return.

  contact.isSelected = not contact.isSelected.
  browse {&browse-name}:refresh().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts fContacts
ON ROW-DISPLAY OF brwContacts IN FRAME fContacts
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts fContacts
ON START-SEARCH OF brwContacts IN FRAME fContacts
DO:
  define buffer contact for contact.
  
  hSortColumn = browse {&browse-name}:current-column.
  if hSortColumn:name = "isSelected" and num-results("{&browse-name}") > 0
   then
    do:
      std-lo = can-find(first contact where isSelected = true).
      for each contact exclusive-lock:
        contact.isSelected = not std-lo.
      end.
      run addContact in this-procedure.
      browse {&browse-name}:refresh().
    end.
   else
    do:
      {lib/brw-startSearch.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fContacts
ON CHOOSE OF bSave IN FRAME fContacts /* Save */
DO:
  pCancel = false.
  run addContact in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSearch fContacts
ON RETURN OF tSearch IN FRAME fContacts /* Search */
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState fContacts
ON RETURN OF tState IN FRAME fContacts /* State */
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fContacts 


/* ***************************  Main Block  *************************** */

ON VALUE-CHANGED OF contact.isSelected IN browse {&browse-name} /* recordChanged */
DO:
  if not available contact
   then return.
   
  contact.isSelected = contact.isSelected:checked in browse {&browse-name}.
  run addContact in this-procedure.
END.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/brw-main.i}
{lib/set-button.i &label="Go" &toggle=false}
setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-state-list.i &combo=tState &addAll=true}
  assign
    frame fContacts:title = "Add Contacts for " + pName
    tState:screen-value   = "ALL"
    tCount:screen-value   = "0 contacts selected"
    .
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addContact fContacts 
PROCEDURE addContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer contact     for contact.
  define buffer out-contact for out-contact.
  
  /* add the selected contacts */
  for each contact no-lock
     where contact.isSelected = true:
     
    if can-find(first out-contact where personContactID = contact.personContactID)
     then next.

    create out-contact.
    buffer-copy contact to out-contact.
    release out-contact.
  end.
  /* remove the unselected contacts */
  std-in = 0.
  for each out-contact exclusive-lock:
    if can-find(first contact where personContactID = out-contact.personContactID and isSelect = false)
     then delete out-contact.
     else std-in = std-in + 1.
  end.
  /* show the count */
  if std-in = 1
   then std-ch = "contact".
   else std-ch = "contacts".
  tCount:screen-value in frame {&frame-name} = string(std-in, ">>>,>>>,>>9") + " " + std-ch + " selected".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fContacts  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fContacts.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fContacts  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSearch tState tCount 
      WITH FRAME fContacts.
  ENABLE bFirstA bGo bLastA RECT-63 tSearch tState brwContacts bSave bCancel 
         bLastB bLastC bLastD bLastE bLastF bLastG bLastH bLastI bLastJ bLastK 
         bLastL bLastM bLastN bLastO bLastP bLastQ bLastR bLastS bLastT bFirstB 
         bLastU bLastV bLastW bLastX bFirstC bLastY bFirstD bLastZ bFirstE 
         bFirstF bFirstG bFirstH bFirstI bFirstJ bFirstK bFirstL bFirstM 
         bFirstN bFirstO bFirstP bFirstQ bFirstR bFirstS bFirstT bFirstU 
         bFirstV bFirstW bFirstX bFirstY bFirstZ tCount 
      WITH FRAME fContacts.
  VIEW FRAME fContacts.
  {&OPEN-BROWSERS-IN-QUERY-fContacts}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData fContacts 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pType   as character no-undo.
  define input parameter pSearch as character no-undo.
  
  close query {&browse-name}.
  empty temp-table contact.

  run server/searchcontacts.p (input  pType,
                               input  pSearch,
                               input  tState:screen-value in frame fContacts,
                               output table contact,
                               output std-lo,
                               output std-ch).
                                 
  if not std-lo
   then message std-ch view-as alert-box error.
   else
    do:
      for each contact exclusive-lock:
        contact.isSelected = can-find(first out-contact where contactName = contact.contactName and personContactID = contact.personContactID).
      end.
      dataSortDesc = not dataSortDesc.
      if dataSortBy = ""
       then dataSortBy = "contactName".
      run sortData in this-procedure (dataSortBy).
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData fContacts 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortdata.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

