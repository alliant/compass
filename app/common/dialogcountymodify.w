&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

define input parameter pStateID as character no-undo.
define input parameter pCounty as character no-undo.
define input-output parameter pDescription as character no-undo.
define output parameter pCancel as logical no-undo initial true.

define variable tTempDescription as character no-undo.

{lib/std-def.i}
{lib/getstatename.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tDescription Btn_OK Btn_Cancel tState ~
tCounty 
&Scoped-Define DISPLAYED-OBJECTS tDescription tState tCounty 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 TOOLTIP "STAT Code description" NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     tDescription AT ROW 3.86 COL 15 COLON-ALIGNED WIDGET-ID 90
     Btn_OK AT ROW 5.91 COL 8.6
     Btn_Cancel AT ROW 5.91 COL 26.6
     tState AT ROW 1.48 COL 15 COLON-ALIGNED WIDGET-ID 100 NO-TAB-STOP 
     tCounty AT ROW 2.67 COL 15 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     SPACE(4.99) SKIP(4.18)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify County"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

ASSIGN 
       tCounty:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tState:READ-ONLY IN FRAME fNew        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* Modify County */
DO:
  IF pDescription <> tTempDescription
   THEN {lib/confirm-close.i "County"}
  
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fNew
ON CHOOSE OF Btn_Cancel IN FRAME fNew /* Cancel */
DO:
  IF pDescription <> tTempDescription
   THEN {lib/confirm-close.i "Code"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDescription fNew
ON LEAVE OF tDescription IN FRAME fNew /* Description */
DO:
  tTempDescription = SELF:SCREEN-VALUE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

assign
  tState = GetStateName(pStateID)
  tCounty = pCounty
  tDescription = pDescription
  .

RUN enable_UI.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  assign
    pDescription = tDescription:screen-value in frame fNew
    pCancel = false
    .
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDescription tState tCounty 
      WITH FRAME fNew.
  ENABLE tDescription Btn_OK Btn_Cancel tState tCounty 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

