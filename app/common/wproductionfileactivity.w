&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wproductionfileactivity.w

  Description: Window for AR posted transactions

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: S Chandu

  Created: 07.12.2024
  Modified:
  Date        Name     Comments
  07/22/2024  S Chandu Browser column name chnaged to 'written-off'.
  12/02/2024  S Chandu Bug fix start and enddate for the period.
  12/03/2024  S Chandu Modified to 'Written-off'.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/ar-def.i}
{lib/winshowscrollbars.i}
{lib/get-column.i}

{lib/getperiodname.i} /* Include function: getPeriodName */

{tt/fileAR.i }
{tt/fileAR.i       &tableAlias="ttFileAR"}


define variable cAgentID          as character no-undo.
define variable lDefaultAgent     as logical   no-undo.
define variable dtFromDate        as date      no-undo.
define variable dtToDate          as date      no-undo.
define variable dColumnWidth      as decimal   no-undo.
define variable iMonth            as integer   no-undo.
define variable iYear             as integer   no-undo.
define variable dtStartDate       as date      no-undo.
define variable dtEndDate         as date      no-undo.
define variable cAppCode          as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwFileAR

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES fileAR

/* Definitions for BROWSE brwFileAR                                     */
&Scoped-define FIELDS-IN-QUERY-brwFileAR fileAR.agent "Agent" fileAR.fileID "File Number" fileAR.trandate "First Transaction" fileAR.Invoicedamount "Invoiced" fileAR.cancelledamount "Written-off" fileAR.paidamount "Paid"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFileAR   
&Scoped-define SELF-NAME brwFileAR
&Scoped-define QUERY-STRING-brwFileAR for each fileAR
&Scoped-define OPEN-QUERY-brwFileAR open query {&SELF-NAME} for each fileAR.
&Scoped-define TABLES-IN-QUERY-brwFileAR fileAR
&Scoped-define FIRST-TABLE-IN-QUERY-brwFileAR fileAR


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwFileAR}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPeriod flAgentID bAgentLookup btGo ~
brwFileAR tgExclude RECT-79 RECT-3 
&Scoped-Define DISPLAYED-OBJECTS flAgentID flName fPeriod tgExclude 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwFileAR 
       MENU-ITEM m_Transaction_Detail LABEL "Transaction Detail".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAgentLookup 
     LABEL "agentlookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Agent lookup".

DEFINE BUTTON bGetCSV  NO-FOCUS
     LABEL "CSV" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Transactions".

DEFINE BUTTON bPeriod  NO-FOCUS
     LABEL "Period" 
     SIZE 5 BY 1.14 TOOLTIP "Select Period".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE flAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE VARIABLE flName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.4 BY 1 NO-UNDO.

DEFINE VARIABLE fPeriod AS CHARACTER FORMAT "X(256)":U INITIAL "Select Period" 
     LABEL "Period" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 18.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.4 BY 3.33.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92.8 BY 3.33.

DEFINE VARIABLE tgExclude AS LOGICAL INITIAL no 
     LABEL "Exclude zero balance files" 
     VIEW-AS TOGGLE-BOX
     SIZE 33 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFileAR FOR 
      fileAR SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFileAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFileAR C-Win _FREEFORM
  QUERY brwFileAR DISPLAY
      fileAR.agent              label         "Agent" format "x(55)"         width 50    
fileAR.fileID             label         "File Number"         format "x(25)" 
fileAR.trandate           label         "First Transaction"   format "99/99/99" width 18
fileAR.Invoicedamount     label         "Invoiced"            format "->>>,>>>,>>>,>>9.99"  width 12
fileAR.cancelledamount    label         "Written-off"         format "->>>,>>>,>>>,>>9.99"  width 12
fileAR.paidamount         label         "Paid"                format "->>>,>>>,>>>,>>9.99"  width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 138 BY 18.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bPeriod AT ROW 3.1 COL 33 WIDGET-ID 402 NO-TAB-STOP 
     bOpen AT ROW 2.1 COL 104.2 WIDGET-ID 452
     flAgentID AT ROW 1.86 COL 12.8 COLON-ALIGNED WIDGET-ID 352
     bAgentLookup AT ROW 1.76 COL 33 WIDGET-ID 350
     flName AT ROW 1.86 COL 35.8 COLON-ALIGNED NO-LABEL WIDGET-ID 424
     btGo AT ROW 2.1 COL 87 WIDGET-ID 262 NO-TAB-STOP 
     brwFileAR AT ROW 4.81 COL 3 WIDGET-ID 200
     bGetCSV AT ROW 2.1 COL 97 WIDGET-ID 420 NO-TAB-STOP 
     fPeriod AT ROW 3.14 COL 12.8 COLON-ALIGNED WIDGET-ID 44
     tgExclude AT ROW 3.24 COL 38.6 WIDGET-ID 460
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.05 COL 4.2 WIDGET-ID 266
     "Actions" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 1.1 COL 97.8 WIDGET-ID 438
     RECT-79 AT ROW 1.29 COL 3.2 WIDGET-ID 270
     RECT-3 AT ROW 1.29 COL 95.6 WIDGET-ID 416
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1
         SIZE 250.8 BY 29.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Production File Activity"
         HEIGHT             = 22.81
         WIDTH              = 142.2
         MAX-HEIGHT         = 37.1
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 37.1
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwFileAR btGo DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bGetCSV IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwFileAR:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwFileAR:HANDLE
       brwFileAR:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwFileAR:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFileAR:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN flName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flName:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN fPeriod IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       fPeriod:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFileAR
/* Query rebuild information for BROWSE brwFileAR
     _START_FREEFORM
open query {&SELF-NAME} for each fileAR.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFileAR */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Production File Activity */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Production File Activity */
DO:  
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Production File Activity */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAgentLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAgentLookup C-Win
ON CHOOSE OF bAgentLookup IN FRAME DEFAULT-FRAME /* agentlookup */
DO:
  define variable cAgentID  as character no-undo.
  define variable cName     as character no-undo.
    
  run dialogagentlookup.w(input flAgentID:input-value,
                          input "",      /* Selected State ID */
                          input true,    /* Allow 'ALL' */
                          output cAgentID,
                          output std-ch, /* Agent state ID */
                          output cName,
                          output std-lo).
   
  if not std-lo  
   then
     return no-apply.
     
  assign
      flAgentID:screen-value = cAgentID
      flName:screen-value    = cName
      . 
  
  resultsChanged(false).
  
  if lDefaultAgent               and
     flAgentID:input-value <> "" and
     flAgentID:input-value <> {&ALL}
   then
    /* Set default AgentID */
    publish "SetDefaultAgent" (input flAgentID:input-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGetCSV
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGetCSV C-Win
ON CHOOSE OF bGetCSV IN FRAME DEFAULT-FRAME /* CSV */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen C-Win
ON CHOOSE OF bOpen IN FRAME DEFAULT-FRAME /* Open */
DO:
  run viewTransactionsDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPeriod C-Win
ON CHOOSE OF bPeriod IN FRAME DEFAULT-FRAME /* Period */
DO:
  run getPeriod in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFileAR
&Scoped-define SELF-NAME brwFileAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileAR C-Win
ON DEFAULT-ACTION OF brwFileAR IN FRAME DEFAULT-FRAME
DO:
  run viewTransactionsDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileAR C-Win
ON ROW-DISPLAY OF brwFileAR IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileAR C-Win
ON START-SEARCH OF brwFileAR IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileAR C-Win
ON VALUE-CHANGED OF brwFileAR IN FRAME DEFAULT-FRAME
DO:
  run setwidgets in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
OR 'RETURN' of flAgentID
DO:
  if not validAgent()
   then
    return no-apply.
        
  run getData in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flAgentID C-Win
ON VALUE-CHANGED OF flAgentID IN FRAME DEFAULT-FRAME /* Agent ID */
DO:
  resultsChanged(false).
  assign
      flName:screen-value     = ""
      /*fVoidDate:screen-value  = "" 
      tSearch:sensitive       = false 
      bSearch:sensitive       = false
      btVoid:sensitive        = false
      bPrelimRpt:sensitive    = false
      fVoidDate:sensitive     = false
      bExport:sensitive       = false
      bTranDetail:sensitive   = false     */
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Transaction_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Transaction_Detail C-Win
ON CHOOSE OF MENU-ITEM m_Transaction_Detail /* Transaction Detail */
DO:
  run viewTransactionsDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgExclude
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgExclude C-Win
ON VALUE-CHANGED OF tgExclude IN FRAME DEFAULT-FRAME /* Exclude zero balance files */
OR 'VALUE-CHANGED' of fPeriod
OR 'VALUE-CHANGED' of flAgentID
DO:
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

subscribe to "closeWindow" anywhere.
 
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


btGo        :load-image            ("images/completed.bmp").
btGo        :load-image-insensitive("images/completed-i.bmp").

bGetCSV     :load-image            ("images/excel.bmp").
bGetCSV     :load-image-insensitive("images/excel-i.bmp").  

bAgentLookup:load-image            ("images/s-lookup.bmp").
bAgentLookup:load-image-insensitive("images/s-lookup-i.bmp").

bOpen      :load-image             ("images/open.bmp").
bOpen      :load-image-insensitive ("images/open-i.bmp").

bPeriod      :load-image            ("images/s-calendar.bmp").
bPeriod      :load-image-insensitive("images/s-calendar-i.bmp").

publish "GetCurrentValue" ("ApplicationCode", output cAppCode). 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized.  
   
  run enable_UI.  
        
  /* Getting date range from first and last open active period */  
  if cAppCode = "TPS"
   then
    do:
      
       publish "loadPeriods".
       publish "GetDefaultPeriodDate"(output iMonth,output iYear).

      fPeriod:screen-value = getPeriodName(iMonth,iYear).
      /* Calculate date range of selected period */
       publish "GetSelectedPeriodDate" (input iMonth,
                                        input iYear,
                                        output dtStartDate,
                                        output dtEndDate).
    end.
  else
   do:
      publish "GetDefaultPeriod"(output iMonth,output iYear).

      fPeriod:screen-value = getPeriodName(iMonth,iYear).
      /* Calculate date range of selected period */
       publish "GetSelectedPeriodDate" (input iMonth,
                                        input iYear,
                                        output dtStartDate,
                                        output dtEndDate).
 
   end.
  
  {lib/get-column-width.i &col="'Agent'" &var=dColumnWidth}  
        
  /* Procedure restores the window and move it to top */
  run showWindow in this-procedure.  
  run setwidgets in this-procedure. 
  
  apply 'entry' to flAgentID.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).  
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flAgentID flName fPeriod tgExclude 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bPeriod flAgentID bAgentLookup btGo brwFileAR tgExclude RECT-79 RECT-3 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwfileAR:num-results = 0 
   then
    do: 
      message "There is nothing to export."
          view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  std-ha = temp-table fileAR:handle.
  run util/exporttable.p (table-handle std-ha,
                          "fileAR",
                          "for each fileAR",
                          "agent,fileid,trandate,invoicedamount,cancelledamount,paidamount",
                          "Agent,File Number,First Transaction,Invoiced,Written-off,Paid", 
                          std-ch,
                          "Production_Files_Activity_" + replace(string(dtStartDate), "/", "") + "_to_" + replace(string(dtEndDate), "/", "") + "_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                                                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.

  define buffer ttFileAR for ttFileAR.

  setStatusCount(query brwfileAR:num-results) no-error.
  
  if can-find(first fileAR) 
   then
    apply 'value-changed' to browse  brwfileAR. 
    
   run setWidgets in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  define buffer ttFileAR for ttFileAR. 
  
  if flAgentID:screen-value = "" /* using date filter only when particular agentId is not entered*/
   then
    do:
        message "AgentID cannot be blank"
             view-as alert-box error buttons ok.
         return. 
    end.        
    
  if fPeriod:input-value = "" or fPeriod:input-value = ?  or fPeriod:input-value = "Unknown Month"
   then
    do:
      message "selection of period is mandatory"
         view-as alert-box error buttons ok.   
      return.
    end.
    
  run server\queryproductionactivity.p(input flAgentID:screen-value,       /* Entity ID */                                
                                         input '',
                                         input  dtStartDate ,        /* from date */
                                         input  dtEndDate,          /* To date   */
                                         input  tgExclude:checked,  
                                         output table ttFileAR,                                
                                         output std-lo,
                                         output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  empty temp-table fileAR.

  for each ttFileAR:
    create fileAR.
    buffer-copy ttFileAR to fileAR. 
  end.
  
  run setwidgets in this-procedure.
  
  open query brwfileAR preselect each fileAR by fileAR.agentID.
  run setBrowse in this-procedure.
  
  setStatusRecords(query brwfileAR:num-results). 
  
  if can-find(first fileAR) 
   then
    apply 'value-changed' to browse brwfileAR .
    
  run setwidgets in this-procedure. 
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPeriod C-Win 
PROCEDURE getPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    do with frame {&frame-name}:
  end.

  run dialogperiod.w (input-output iMonth,
                      input-output iYear,
                      output std-ch).
                     
  fPeriod:screen-value = getPeriodName(iMonth,iYear).
  /* Calculate date range of selected period */
  publish "GetSelectedPeriodDate" (input iMonth,
                                   input iYear,
                                   output dtStartDate,
                                   output dtEndDate).
                                    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setBrowse C-Win 
PROCEDURE setBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/    
  do with frame {&frame-name}:
  end.
  
  define buffer fileAR for fileAR.
  
  find first fileAR no-error.

  if not available fileAR then return.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setwidgets C-Win 
PROCEDURE setwidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame default-frame:
  end.
  
  define buffer fileAR for fileAR.
  if query brwFileAR:num-results > 0 
   then
    assign 
        browse brwFileAR:sensitive                      = true               
               bGetCSV:sensitive in frame {&frame-name} = true 
                bOpen  :sensitive = true
               .      
   else
    assign 
        browse brwFileAR:sensitive = false
               bGetCSV:sensitive = false
               bOpen  :sensitive = false. 
    
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state = window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by filear.agentID by filear.agent ".                                                    
   
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
  
  if can-find(first fileAR) 
   then
    apply 'value-changed' to browse  brwfileAR. 
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewTransactionsDetail C-Win 
PROCEDURE viewTransactionsDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.

if available filear
 then
  publish "OpenWindow" (input "wproductionfiledetail",       /*childtype*/
                        input string(filear.agentfileID),  /*childid*/
                        input "wproductionfiledetail.w",     /*window*/
                        input "character|input|" + string(filear.agentID) + 
                              "^character|input|" + string(filear.agentName) + 
                              "^character|input|" + string(filear.fileID)  +
                              "^date|input|" + string(dtStartDate)   +
                              "^date|input|" + string(dtEndDate),  /*parameters*/                               
                        input this-procedure).            /*currentProcedure handle*/ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 21
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
   
  {lib/resize-column.i &col="'Agent'" &var=dColumnWidth}
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validAgent C-Win 
FUNCTION validAgent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if flAgentID:input-value = ""
   then return false. /* Function return value. */
  
  else if flAgentID:input-value = {&ALL}
   then
    flName:screen-value = {&NotApplicable}.
       
  else if flAgentID:input-value <> {&ALL}
   then
    do:
      publish "getAgentName" (input flAgentID:input-value,
                              output std-ch,
                              output std-lo).                                               
      if not std-lo 
       then 
        do:
          assign 
              flAgentID:screen-value = "" 
              flName:screen-value    = ""
              .
          return false. /* Function return value. */
        end.
      flName:screen-value = std-ch.
    end. 
  
  resultsChanged(false). 
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

