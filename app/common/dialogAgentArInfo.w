&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogAgentArInfo.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created:
  Modified: 
  Date      Name Comments
  07/05/21  SB   Modified to add write-off GL account
  11/29/24  SC   Task#117302 Prepopulate the GL Ref numbers in Accounts Receivable screen
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input-output parameter ipcMethod         as character no-undo.
define input-output parameter iopiDueInDays     as integer   no-undo.
define input-output parameter iopiDueOnDay      as integer   no-undo.
define input-output parameter iopcCashAcc       as character no-undo.
define input-output parameter iopcCashAccDesc   as character no-undo.
define input-output parameter iopcARAcc         as character no-undo.
define input-output parameter iopcARAccDesc     as character no-undo.
define input-output parameter iopcRefARAcc      as character no-undo.
define input-output parameter iopcRefARAccDesc  as character no-undo.
define input-output parameter iopcWroffARAcc      as character no-undo.
define input-output parameter iopcWroffARAccDesc  as character no-undo.
define output       parameter loSuccess         as logical   no-undo.

/* Local Variable Definitions ---                                       */
define variable cARGLRef              as character no-undo.
define variable cPaymentCashGLRef     as character no-undo.
define variable cRefundCashGLRef      as character no-undo.
define variable cARGLRefDesc          as character no-undo.
define variable cPaymentCashGLRefDesc as character no-undo.
define variable cRefundCashGLRefDesc  as character no-undo.
define variable cAppName              as character no-undo.

&scoped-define Invdue        "Invdue"
&scoped-define Invdueindays  "Invdueindays"
&scoped-define Invdueonday   "Invdueonday"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn_Cancel Btn_OK RECT-88 RECT-89 RECT-90 ~
flPaymentRefDR flPaymentRefDescDR flPaymentRefCR flPaymentRefDescCR ~
flRefPaymentRefDR flRefPaymentRefDescDR flRefPaymentRefCR ~
flRefPaymentRefDescCR flWroffARPaymentRef flWroffARPaymentRefDesc ~
flWroffPaymentRef flWroffPaymentRefDesc cbMethod fDueInDays fDueOnDay 
&Scoped-Define DISPLAYED-OBJECTS flPaymentRefDR flPaymentRefDescDR ~
flPaymentRefCR flPaymentRefDescCR flRefPaymentRefDR flRefPaymentRefDescDR ~
flRefPaymentRefCR flRefPaymentRefDescCR flWroffARPaymentRef ~
flWroffARPaymentRefDesc flWroffPaymentRef flWroffPaymentRefDesc cbMethod ~
fDueInDays fDueOnDay 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbMethod AS CHARACTER FORMAT "X(256)":U 
     LABEL "Invoice Due" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "item","item"
     DROP-DOWN-LIST
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE fDueInDays AS INTEGER FORMAT "ZZ9":U INITIAL 0 
     LABEL "Due in Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 6.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDueOnDay AS INTEGER FORMAT "Z9":U INITIAL 0 
     LABEL "Due on Day (1 to 30)" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5.8 BY 1 NO-UNDO.

DEFINE VARIABLE flPaymentRefCR AS CHARACTER FORMAT "X(256)":U 
     LABEL "AR GL Ref.(CR)" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flPaymentRefDescCR AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flPaymentRefDescDR AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flPaymentRefDR AS CHARACTER FORMAT "X(256)":U 
     LABEL "Cash GL Ref.(DR)" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flRefPaymentRefCR AS CHARACTER FORMAT "X(256)":U 
     LABEL "Cash GL Ref.(CR)" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flRefPaymentRefDescCR AS CHARACTER FORMAT "X(256)":U INITIAL "AR Clearing" 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flRefPaymentRefDescDR AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flRefPaymentRefDR AS CHARACTER FORMAT "X(256)":U 
     LABEL "AR GL Ref.(DR)" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flWroffARPaymentRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "AR GL Ref" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flWroffARPaymentRefDesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE flWroffPaymentRef AS CHARACTER FORMAT "X(256)":U 
     LABEL "Write-off GL Ref" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flWroffPaymentRefDesc AS CHARACTER FORMAT "X(256)":U INITIAL "Bad Debt" 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-88
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 101 BY 3.71.

DEFINE RECTANGLE RECT-89
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 101 BY 3.91.

DEFINE RECTANGLE RECT-90
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 101 BY 3.91.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     Btn_Cancel AT ROW 16.29 COL 53.2 NO-TAB-STOP 
     Btn_OK AT ROW 16.29 COL 36.2 NO-TAB-STOP 
     flPaymentRefDR AT ROW 2.14 COL 27 COLON-ALIGNED WIDGET-ID 10
     flPaymentRefDescDR AT ROW 2.14 COL 61.4 COLON-ALIGNED WIDGET-ID 14
     flPaymentRefCR AT ROW 3.24 COL 27 COLON-ALIGNED WIDGET-ID 12
     flPaymentRefDescCR AT ROW 3.24 COL 61.4 COLON-ALIGNED WIDGET-ID 18
     flRefPaymentRefDR AT ROW 5.86 COL 27 COLON-ALIGNED WIDGET-ID 30 NO-TAB-STOP 
     flRefPaymentRefDescDR AT ROW 5.86 COL 61.4 COLON-ALIGNED WIDGET-ID 28 NO-TAB-STOP 
     flRefPaymentRefCR AT ROW 6.95 COL 27 COLON-ALIGNED WIDGET-ID 24
     flRefPaymentRefDescCR AT ROW 6.95 COL 61.4 COLON-ALIGNED WIDGET-ID 26
     flWroffARPaymentRef AT ROW 9.67 COL 27 COLON-ALIGNED WIDGET-ID 42 NO-TAB-STOP 
     flWroffARPaymentRefDesc AT ROW 9.67 COL 61.4 COLON-ALIGNED WIDGET-ID 40 NO-TAB-STOP 
     flWroffPaymentRef AT ROW 10.76 COL 27 COLON-ALIGNED WIDGET-ID 36
     flWroffPaymentRefDesc AT ROW 10.76 COL 61.4 COLON-ALIGNED WIDGET-ID 38
     cbMethod AT ROW 13.14 COL 27 COLON-ALIGNED WIDGET-ID 2
     fDueInDays AT ROW 14.29 COL 27 COLON-ALIGNED WIDGET-ID 6
     fDueOnDay AT ROW 14.29 COL 61 COLON-ALIGNED WIDGET-ID 8
     "Payment Information" VIEW-AS TEXT
          SIZE 20.2 BY .62 AT ROW 1.1 COL 3.8 WIDGET-ID 22
     "Refund Information" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 4.71 COL 3.8 WIDGET-ID 34
     "Write-off Information" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 8.52 COL 3.8 WIDGET-ID 46
     RECT-88 AT ROW 1.38 COL 2 WIDGET-ID 20
     RECT-89 AT ROW 5 COL 2 WIDGET-ID 32
     RECT-90 AT ROW 8.81 COL 2 WIDGET-ID 44
     SPACE(0.59) SKIP(5.32)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Accounts Receivable"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       flRefPaymentRefDescDR:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flRefPaymentRefDR:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flWroffARPaymentRef:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flWroffARPaymentRefDesc:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Accounts Receivable */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
    
  if (cAppName <> "Agents" and cAppName <> "Accounts Receivable")
   then
    return.
    
  if  (iopcARAcc        = '' and 
      iopcCashAcc       = '' and 
      iopcRefARAcc      = '') 
   then
    do:
      message "GL Accounts not saved. Are you sure you want to continue without saving?"                      
          view-as alert-box warning buttons yes-no update std-lo.
      
      if std-lo 
       then
        loSuccess        = false.
       else
        apply 'choose':U to btn_ok. 
    end.     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
DO:

  if  (cAppName = "Agents" or cAppName = "Accounts Receivable")   and
      (
            ((iopcCashAcc <> flPaymentRefDR:input-value) and (flPaymentRefDR:input-value <> cPaymentCashGLRef)) or
            ((iopcARAcc <> flPaymentRefCR:input-value) and (flPaymentRefCR:input-value <> cARGLRef)) or
            ((iopcRefARAcc <> flRefPaymentRefCR:input-value) and (flRefPaymentRefCR:input-value <> cRefundCashGLRef)) or
            ((iopcCashAccDesc <> flPaymentRefDescDR:input-value) and (flPaymentRefDescDR:input-value <> cPaymentCashGLRefDesc)) or
            ((iopcARAccDesc <> flPaymentRefDescCR:input-value) and (flPaymentRefDescCR:input-value <> cARGLRefDesc)) or
            ((iopcRefARAccDesc <> flRefPaymentRefDescCR:input-value) and (flRefPaymentRefDescCR:input-value <> cRefundCashGLRefDesc))
      )
    then
     do:
       message "Default GL accounts changed. Are you sure you want to continue?"                      
          view-as alert-box warning buttons yes-no update std-lo.     
       if not std-lo 
        then
         do:
           loSuccess = false.
           return.
         end. 
     end.
  
  assign
      ipcMethod       = cbMethod:input-value 
      iopiDueInDays   = fDueInDays:input-value
      iopiDueOnDay    = fDueOnDay:input-value
      iopcCashAcc     = flPaymentRefDR:input-value
      iopcCashAccDesc = flPaymentRefDescDR:input-value
      iopcARAcc       = flPaymentRefCR:input-value
      iopcARAccDesc   = flPaymentRefDescCR:input-value
      iopcRefARAcc     = flRefPaymentRefCR:input-value
      iopcRefARAccDesc = flRefPaymentRefDescCR:input-value
    
      iopcWroffARAcc     = flWroffPaymentRef:input-value
      iopcWroffARAccDesc = flWroffPaymentRefDesc:input-value 
      loSuccess        = true
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbMethod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbMethod Dialog-Frame
ON VALUE-CHANGED OF cbMethod IN FRAME Dialog-Frame /* Invoice Due */
DO:
  if cbMethod:input-value = {&Invdue}
   then
    assign
      fDueInDays:sensitive = false
      fDueOnDay:sensitive = false
      fDueInDays:screen-value = "0"
      fDueOnDay:screen-value = "0"    
      .
  else if cbMethod:input-value = {&Invdueindays}
   then
    assign
      fDueInDays:sensitive = true
      fDueOnDay:sensitive = false
      fDueInDays:screen-value = string(iopiDueInDays)
      fDueOnDay:screen-value = "0"     
      .  
  else if cbMethod:input-value = {&Invdueonday}
   then
    assign
      fDueInDays:sensitive = false
      fDueOnDay:sensitive = true
      fDueInDays:screen-value = "0"
      fDueOnDay:screen-value = string(iopiDueOnDay).
      .   
  run enableDisableOK in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDueInDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDueInDays Dialog-Frame
ON VALUE-CHANGED OF fDueInDays IN FRAME Dialog-Frame /* Due in Days */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDueOnDay
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDueOnDay Dialog-Frame
ON VALUE-CHANGED OF fDueOnDay IN FRAME Dialog-Frame /* Due on Day (1 to 30) */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flPaymentRefCR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flPaymentRefCR Dialog-Frame
ON VALUE-CHANGED OF flPaymentRefCR IN FRAME Dialog-Frame /* AR GL Ref.(CR) */
DO:
  run enableDisableOK in this-procedure.
  flRefPaymentRefDR:screen-value = self:screen-value.
  flWroffARPaymentRef:screen-value = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flPaymentRefDescCR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flPaymentRefDescCR Dialog-Frame
ON VALUE-CHANGED OF flPaymentRefDescCR IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableOK in this-procedure.
  flRefPaymentRefDescDR:screen-value = self:screen-value.
  flWroffARPaymentRefDesc:screen-value = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flPaymentRefDescDR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flPaymentRefDescDR Dialog-Frame
ON VALUE-CHANGED OF flPaymentRefDescDR IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flPaymentRefDR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flPaymentRefDR Dialog-Frame
ON VALUE-CHANGED OF flPaymentRefDR IN FRAME Dialog-Frame /* Cash GL Ref.(DR) */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefPaymentRefCR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefPaymentRefCR Dialog-Frame
ON VALUE-CHANGED OF flRefPaymentRefCR IN FRAME Dialog-Frame /* Cash GL Ref.(CR) */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefPaymentRefDescCR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefPaymentRefDescCR Dialog-Frame
ON VALUE-CHANGED OF flRefPaymentRefDescCR IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefPaymentRefDescDR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefPaymentRefDescDR Dialog-Frame
ON VALUE-CHANGED OF flRefPaymentRefDescDR IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flRefPaymentRefDR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flRefPaymentRefDR Dialog-Frame
ON VALUE-CHANGED OF flRefPaymentRefDR IN FRAME Dialog-Frame /* AR GL Ref.(DR) */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flWroffARPaymentRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flWroffARPaymentRef Dialog-Frame
ON VALUE-CHANGED OF flWroffARPaymentRef IN FRAME Dialog-Frame /* AR GL Ref */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flWroffARPaymentRefDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flWroffARPaymentRefDesc Dialog-Frame
ON VALUE-CHANGED OF flWroffARPaymentRefDesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flWroffPaymentRef
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flWroffPaymentRef Dialog-Frame
ON VALUE-CHANGED OF flWroffPaymentRef IN FRAME Dialog-Frame /* Write-off GL Ref */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flWroffPaymentRefDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flWroffPaymentRefDesc Dialog-Frame
ON VALUE-CHANGED OF flWroffPaymentRefDesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/get-sysprop-list.i &combo=cbMethod &appCode="'ARM'" &objAction="'Agent'" &objProperty="'SetInvDueDate'"}.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  publish "GetAppName" (output cAppName).
  if (cAppName = "Agents" or cAppName = "Accounts Receivable")
   then
    run getDefaultGLCodes in this-procedure.
  
  assign
    cbMethod:screen-value              = (if ipcMethod = "" then {&Invdue} else ipcMethod)
    fDueInDays:screen-value            = string(iopiDueInDays)
    fDueOnDay:screen-value             = string(iopiDueOnDay)
    flPaymentRefDR:screen-value        = iopcCashAcc
    flPaymentRefDescDR:screen-value    = iopcCashAccDesc
    flPaymentRefCR:screen-value        = iopcARAcc
    flPaymentRefDescCR:screen-value    = iopcARAccDesc
    flRefPaymentRefDR:screen-value     = iopcARAcc
    flRefPaymentRefDescDR:screen-value = iopcARAccDesc
    flRefPaymentRefCR:screen-value     = iopcRefARAcc
    flRefPaymentRefDescCR:screen-value = iopcRefARAccDesc
    
    flWroffARPaymentRef:screen-value     = iopcARAcc
    flWroffARPaymentRefDesc:screen-value = iopcARAccDesc
    flWroffPaymentRef:screen-value       = iopcWroffARAcc
    flWroffPaymentRefDesc:screen-value   = iopcWroffARAccDesc
    no-error.

  if (cAppName = "Agents" or cAppName = "Accounts Receivable")   and 
     (iopcARAcc        = '' or  
      iopcCashAcc      = '' or 
      iopcRefARAcc     = '' or
      iopcCashAccDesc  = '' or
      iopcARAccDesc    = '' or
      iopcRefARAccDesc = '')
   then
    do:
       assign
           flPaymentRefDR:screen-value        = if flPaymentRefDR:screen-value = '' then cPaymentCashGLRef else flPaymentRefDR:screen-value
           flPaymentRefDescDR:screen-value    = if flPaymentRefDescDR:screen-value = '' then cPaymentCashGLRefDesc else flPaymentRefDescDR:screen-value
           flPaymentRefCR:screen-value        = if flPaymentRefCR:screen-value = '' then cARGLRef  else flPaymentRefCR:screen-value
           flPaymentRefDescCR:screen-value    = if flPaymentRefDescCR:screen-value = '' then cARGLRefDesc else flPaymentRefDescCR:screen-value
           
           flRefPaymentRefDR:screen-value     = if flRefPaymentRefDR:screen-value = '' then cARGLRef else flRefPaymentRefDR:screen-value
           flRefPaymentRefDescDR:screen-value = if flRefPaymentRefDescDR:screen-value = '' then cARGLRefDesc else flRefPaymentRefDescDR:screen-value
           flRefPaymentRefCR:screen-value     = if flRefPaymentRefCR:screen-value = '' then cRefundCashGLRef else flRefPaymentRefCR:screen-value
           flRefPaymentRefDescCR:screen-value = if flRefPaymentRefDescCR:screen-value = '' then cRefundCashGLRefDesc else flRefPaymentRefDescCR:screen-value
           
           flWroffARPaymentRef:screen-value     = if flWroffARPaymentRef:screen-value = '' then cARGLRef else flWroffARPaymentRef:screen-value
           flWroffARPaymentRefDesc:screen-value = if flWroffARPaymentRefDesc:screen-value = '' then cARGLRefDesc else flWroffARPaymentRefDesc:screen-value
           .
    end.
    
  apply 'value-changed' to cbMethod.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableOK Dialog-Frame 
PROCEDURE enableDisableOK :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  Btn_OK:sensitive = (cbMethod:input-value = {&Invdue}
                      and (ipcMethod <> "" and ipcMethod <> {&Invdue}))    or
                     (cbMethod:input-value = {&Invdueindays} 
                      and fDueInDays:input-value > 0
                      and fDueInDays:input-value <> iopiDueInDays)         or
                     (cbMethod:input-value = {&Invdueonday} 
                      and fDueOnDay:input-value > 0                       
                      and fDueOnDay:input-value < 31
                      and fDueOnDay:input-value <> iopiDueOnDay)
                      .
  
  if cbMethod:input-value = {&Invdue} or
     fDueInDays:input-value > 0       or
     (fDueOnDay:input-value > 0 and 
      fDueOnDay:input-value < 31)
   then
    Btn_OK:sensitive = Btn_OK:sensitive                                   or
                       (iopcCashAcc <> flPaymentRefDR:input-value         or    
                        iopcCashAccDesc <> flPaymentRefDescDR:input-value or 
                        iopcARAcc <> flPaymentRefCR:input-value           or
                        iopcARAccDesc <> flPaymentRefDescCR:input-value   or
                        iopcRefARAcc <> flRefPaymentRefCR:input-value     or
                        iopcRefARAccDesc <> flRefPaymentRefDescCR:input-value or
                        iopcWroffARAcc <> flWroffPaymentRef:input-value       or
                        iopcWroffARAccDesc <> flWroffPaymentRefDesc:input-value)
                       .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flPaymentRefDR flPaymentRefDescDR flPaymentRefCR flPaymentRefDescCR 
          flRefPaymentRefDR flRefPaymentRefDescDR flRefPaymentRefCR 
          flRefPaymentRefDescCR flWroffARPaymentRef flWroffARPaymentRefDesc 
          flWroffPaymentRef flWroffPaymentRefDesc cbMethod fDueInDays fDueOnDay 
      WITH FRAME Dialog-Frame.
  ENABLE Btn_Cancel Btn_OK RECT-88 RECT-89 RECT-90 flPaymentRefDR 
         flPaymentRefDescDR flPaymentRefCR flPaymentRefDescCR flRefPaymentRefDR 
         flRefPaymentRefDescDR flRefPaymentRefCR flRefPaymentRefDescCR 
         flWroffARPaymentRef flWroffARPaymentRefDesc flWroffPaymentRef 
         flWroffPaymentRefDesc cbMethod fDueInDays fDueOnDay 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDefaultGLCodes Dialog-Frame 
PROCEDURE getDefaultGLCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "GetGLRef" (output cARGLRef, 
                      output cPaymentCashGLRef,
                      output cRefundCashGLRef,
                      output cARGLRefDesc, 
                      output cPaymentCashGLRefDesc,
                      output cRefundCashGLRefDesc).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

