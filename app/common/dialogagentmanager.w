&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Manager Dialog
@description Sets the managers for an agent

@param FileDataSrv;handle;The handle to the File Data Service
@param AgentName;character;The name of the agent
@return AgentManager;complex;The agent manager temp-table

@author John Oliver
@version 1.0
@created 2017/06/28
@notes Because a user can set managers before the agent is created, we
       need to create the temp table and 
@modified
         Shefali     2020/11/26    Add a combo-box for agent manager type
         Shefali     2020/12/16    Task-86695-Agent Manager Type Implementation.         

---------------------------------------------------------------------*/

/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{tt/agentmanager.i}
{tt/agentmanager.i &tableAlias="ttagentmanager"}
{tt/agentmanager.i &tableAlias="tagentmanager"}

define input parameter hSource      as handle    no-undo.
define input parameter hFileDataSrv as handle    no-undo.
define input parameter pAgentName   as character no-undo.
define input parameter pCanChange   as logical   no-undo.
define input parameter pAction      as character no-undo.
define input parameter table for agentmanager.

/* functions */
{lib/add-delimiter.i}
{lib/get-last-day.i}

/* variables */
define variable tManagerID as integer   no-undo.
define variable tIsChange  as logical   no-undo initial false.
define variable statusvar  as character no-undo.
define variable opcMsg     as character no-undo.
define variable lManager   as logical   no-undo.
{lib/std-def.i}
{lib/amd-def.i}
{lib/set-button-def.i}

/* temp tables */
{tt/sysprop.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fManager
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentmanager

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData agentmanager.isPrimary entry(index("AESHU0", agentmanager.type),"Agency,Executive Team,Sales,Searcher,UnderWriter, ") @ agentmanager.TYPE agentmanager.uidDesc agentmanager.statDesc agentmanager.effDate agentmanager.expDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH agentmanager
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH agentmanager.
&Scoped-define TABLES-IN-QUERY-brwData agentmanager
&Scoped-define FIRST-TABLE-IN-QUERY-brwData agentmanager


/* Definitions for FRAME fManager                                       */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bAdd bCancel bDelete bExport ~
bRefresh bSave tManagerType tManager tIsPrimary tStatus tEffDate tExpDate ~
tType tTypeDenied tAmountDenied lInformation tAmount lIsPrimaryLabel ~
lCommission rInformation rCommssion RECT-12 RECT-69 
&Scoped-Define DISPLAYED-OBJECTS tManagerType tManager tIsPrimary tStatus ~
tEffDate tExpDate tType tTypeDenied tAmountDenied lInformation tAmount ~
lIsPrimaryLabel lCommission fMarkMandatory1 fMarkMandatory2 fMarkMandatory3 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.6 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tManager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 50 BY 1 NO-UNDO.

DEFINE VARIABLE tManagerType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 6
     DROP-DOWN-LIST
     SIZE 50 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "None","A"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE lCommission AS CHARACTER FORMAT "X(256)":U INITIAL "Commission" 
      VIEW-AS TEXT 
     SIZE 13.6 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE lInformation AS CHARACTER FORMAT "X(256)":U INITIAL "Information" 
      VIEW-AS TEXT 
     SIZE 13 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE lIsPrimaryLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Primary:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tAmountDenied AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tEffDate AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tExpDate AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tTypeDenied AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE RECTANGLE rCommssion
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66.2 BY 2.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 5.6 BY 4.76.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 5.6 BY 2.57.

DEFINE RECTANGLE rInformation
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 5.71.

DEFINE VARIABLE tIsPrimary AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      agentmanager SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      agentmanager.isPrimary view-as toggle-box
entry(index("AESHU0", agentmanager.type),"Agency,Executive Team,Sales,Searcher,UnderWriter, ") @ agentmanager.TYPE width 16
agentmanager.uidDesc width 14
agentmanager.statDesc width 10
agentmanager.effDate width 12
agentmanager.expDate width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 77.6 BY 8.19 ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fManager
     brwData AT ROW 1.48 COL 7.8 WIDGET-ID 200
     bAdd AT ROW 3.81 COL 2.4 WIDGET-ID 42 NO-TAB-STOP 
     bCancel AT ROW 2.81 COL 90 WIDGET-ID 52 NO-TAB-STOP 
     bDelete AT ROW 4.95 COL 2.4 WIDGET-ID 82 NO-TAB-STOP 
     bExport AT ROW 2.67 COL 2.4 WIDGET-ID 80 NO-TAB-STOP 
     bRefresh AT ROW 1.52 COL 2.4 WIDGET-ID 76 NO-TAB-STOP 
     bSave AT ROW 1.62 COL 90 WIDGET-ID 40 NO-TAB-STOP 
     tManagerType AT ROW 1.95 COL 106 COLON-ALIGNED WIDGET-ID 68
     tManager AT ROW 3.19 COL 106 COLON-ALIGNED WIDGET-ID 46
     tIsPrimary AT ROW 4.57 COL 108 WIDGET-ID 18
     tStatus AT ROW 4.43 COL 139.8 COLON-ALIGNED WIDGET-ID 24
     tEffDate AT ROW 5.62 COL 106 COLON-ALIGNED WIDGET-ID 16
     tExpDate AT ROW 5.62 COL 139.8 COLON-ALIGNED WIDGET-ID 22
     tType AT ROW 8.14 COL 106 COLON-ALIGNED WIDGET-ID 32
     tTypeDenied AT ROW 8.14 COL 106 COLON-ALIGNED NO-LABEL WIDGET-ID 64
     tAmountDenied AT ROW 8.14 COL 139.8 COLON-ALIGNED NO-LABEL WIDGET-ID 66 DISABLE-AUTO-ZAP 
     lInformation AT ROW 1.24 COL 95.2 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     tAmount AT ROW 8.14 COL 139.8 COLON-ALIGNED WIDGET-ID 38
     lIsPrimaryLabel AT ROW 4.67 COL 98 COLON-ALIGNED NO-LABEL WIDGET-ID 58
     lCommission AT ROW 7.43 COL 95.2 COLON-ALIGNED NO-LABEL WIDGET-ID 62
     fMarkMandatory1 AT ROW 2.19 COL 156 COLON-ALIGNED NO-LABEL WIDGET-ID 86 NO-TAB-STOP 
     fMarkMandatory2 AT ROW 3.43 COL 156 COLON-ALIGNED NO-LABEL WIDGET-ID 88 NO-TAB-STOP 
     fMarkMandatory3 AT ROW 5.86 COL 122 COLON-ALIGNED NO-LABEL WIDGET-ID 90 NO-TAB-STOP 
     rInformation AT ROW 1.52 COL 95.4 WIDGET-ID 14
     rCommssion AT ROW 7.67 COL 95.4 WIDGET-ID 28
     RECT-12 AT ROW 1.43 COL 2 WIDGET-ID 54
     RECT-69 AT ROW 1.52 COL 89.6 WIDGET-ID 84
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 162 BY 8.91 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Manager"
         HEIGHT             = 8.76
         WIDTH              = 161.4
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         TOP-ONLY           = yes
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fManager
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fManager */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fManager       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fManager         = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME fManager
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME fManager
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory3 IN FRAME fManager
   NO-ENABLE                                                            */
ASSIGN 
       tAmountDenied:READ-ONLY IN FRAME fManager        = TRUE.

ASSIGN 
       tTypeDenied:READ-ONLY IN FRAME fManager        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentmanager.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Manager */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Manager */
DO:
  /* This event will close the window and terminate the procedure. */ 
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fManager /* Add */
DO:
  doModify(true).
  do with frame {&frame-name}:
    /* create the manager list by removing the users that are already manager */
    tManager:list-item-pairs = "" + tManager:delimiter + "".
    tManager:delete(1).
    for each sysprop no-lock
          by sysprop.objName:   
      if not can-find(first agentmanager where uid = sysprop.objID and stat = "A")
       then tManager:add-last(sysprop.objName, sysprop.objID).
    end.
    assign
        tManagerType:screen-value = "None"
        tIsPrimary:checked        = not can-find(first agentmanager where isPrimary = true)
        tIsPrimary:sensitive      = true
        tStatus:screen-value      = "A"
        tStatus:sensitive         = false
        tType:screen-value        = ""
        tAmount:screen-value      = "0"
        tEffDate:screen-value     = string(today)
        tEffDate:read-only        = false
        tExpDate:screen-value     = ""
        tExpDate:read-only        = true
        tManagerID                = 0
        bSave:sensitive           = false
        bCancel:sensitive         = false
        tManagerType:sensitive    = true
        tManager:sensitive        = true
        fMarkMandatory1:hidden    = false
        fMarkMandatory2:hidden    = false
        fMarkMandatory3:hidden    = false
        fMarkMandatory1:screen-value  = {&mandatory}
        fMarkMandatory2:screen-value  = {&mandatory}
        fMarkMandatory3:screen-value  = {&mandatory}
        .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fManager /* Cancel */
DO:
  doModify(false).
  if not can-find(first agentmanager) then
   do:
     assign
         tManagerType:sensitive    = false
         tManager:sensitive        = false
         bSave:sensitive           = false
         bCancel:sensitive         = false.
   end.
  {&open-query-brwData}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fManager /* Delete */
DO:
  run deleteAgentManager in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fManager /* Export */
DO:                             
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fManager /* Refresh */
DO:
  run refreshAgentManagers in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fManager
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fManager
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fManager
DO:
  if not available agentmanager
   then return.

  if agentmanager.stat = "A" 
   then
    bDelete:sensitive = false.
   else 
    bDelete:sensitive = true.
  /* assign */
  assign
      tManagerID                        = agentmanager.managerID
      tManagerType:screen-value         = if agentmanager.type = "0" then "None" else agentmanager.type 
      tManagerType:sensitive            = false
      tManager:list-item-pairs          = agentmanager.uidDesc + tManager:delimiter + agentmanager.uid
      tManager:screen-value             = agentmanager.uid
      tManager:sensitive                = false
      tIsPrimary:checked                = agentmanager.isPrimary
      tStatus:screen-value              = agentmanager.stat
      tStatus:sensitive                 = false
      tType:screen-value                = agentmanager.commissionCode
      tAmount:screen-value              = string(agentmanager.commissionRate)
      tEffDate:screen-value             = string(agentmanager.effDate)
      tExpDate:screen-value             = string(agentmanager.expDate)
      tEffDate:read-only                = true
      tAmount:hidden                    = true 
      tAmount:side-label-handle:hidden  = false
      bSave:sensitive                   = false
      bCancel:sensitive                 = false
      /* disable based on status */
      std-lo                            = agentmanager.stat <> "I"
      tIsPrimary:sensitive              = std-lo
      tStatus:sensitive                 = std-lo no-error
      .     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fManager /* Save */
DO:
  run newAgentManager in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define SELF-NAME tIsPrimary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tIsPrimary C-Win
ON VALUE-CHANGED OF tIsPrimary IN FRAME fManager
DO:
  assign 
      bSave:sensitive       = true
      bCancel:sensitive     = true.
  if self:checked and can-find(first agentmanager where isPrimary = true)
   then
    for first agentmanager exclusive-lock
        where agentmanager.isPrimary = true:

      std-lo = true.
      message agentmanager.uidDesc + " is already the primary manager. Would you like to change the primary manager?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then self:checked = false.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tManager C-Win
ON VALUE-CHANGED OF tManager IN FRAME fManager /* Manager */
DO:
  run valueChange in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tManagerType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tManagerType C-Win
ON VALUE-CHANGED OF tManagerType IN FRAME fManager /* Type */
DO:
  run valueChange in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStatus C-Win
ON VALUE-CHANGED OF tStatus IN FRAME fManager /* Status */
DO:
  do with frame {&frame-name}:
    assign 
        bSave:sensitive       = true
        bCancel:sensitive     = true.
    case self:screen-value:
     when "A" then
      assign
        std-lo = true
        tIsPrimary:checked = can-find(first agentmanager where isPrimary = true)
        tIsPrimary:sensitive = std-lo
        tExpDate:screen-value = ""
        tExpDate:read-only = not std-lo
        .
     when "I" then
       assign
         std-lo = false
         tIsPrimary:checked = std-lo
         tIsPrimary:sensitive = std-lo
         tStatus:sensitive = std-lo
         tExpDate:screen-value = string(date(month(today),getLastDay(today),year(today)))
         tExpDate:read-only = std-lo
         .
    end case.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/brw-main.i}
{lib/win-status.i}
{lib/amd-def.i}

subscribe to "CloseWindow" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/get-sysprop-list.i &combo=tStatus &appCode="'AMD'" &objAction="'AgentManager'" &objProperty="'Status'"}
{lib/get-syscode-list.i &combo=tType &codeType="'Commission'"}
{lib/get-syscode-list.i &combo=tManagerType &codeType="'AgentManagerType'"}
publish "GetSysProp" ("AMD", "AgentManager", "", output table sysprop).
tManagerType:add-first("--Select Type--","None").
tManagerType:screen-value = "None".

/* set the title of the window */
{&window-name}:title = pAgentName + "'s Managers".
{lib/set-button.i &label="Refresh" &image="images/s-refresh.bmp" &inactive="images/s-refresh-i.bmp"}
{lib/set-button.i &label="Export" &image="images/s-excel.bmp" &inactive="images/s-excel-i.bmp"}
{lib/set-button.i &label="Add" &image="images/s-add.bmp" &inactive="images/s-add-i.bmp"}
{lib/set-button.i &label="Delete" &image="images/s-delete.bmp" &inactive="images/s-delete-i.bmp"}
{lib/set-button.i &label="Save" &image="images/s-save.bmp" &inactive="images/s-save-i.bmp"}
{lib/set-button.i &label="Cancel" &image="images/s-cancel.bmp" &inactive="images/s-cancel-i.bmp"}
setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  if index(tManagerType:list-item-pairs,"ALL") ne 0 
   then
    tManagerType:delete("ALL").

  if not can-find(first agentmanager) then
  do:
    assign
        tManagerType:sensitive = false
        tManager    :sensitive = false
        bSave       :sensitive = false
        bCancel     :sensitive = false
        tIsPrimary  :sensitive = false
        tStatus     :sensitive = false
        tEffDate    :read-only = true
        tExpDate    :read-only = true
        .
  end.
  doModify(not can-find(first agentmanager where stat = "A")).
  {&OPEN-QUERY-brwData}
  setStatusCount(num-results("{&browse-name}")).
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteAgentManager C-Win 
PROCEDURE deleteAgentManager :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pSuccess as logical no-undo.
  define variable lChoice as logical no-undo.
  
  find first agentmanager 
   where agentmanager.managerID = tManagerID no-error.
  if not available agentmanager
   then
    return.
    
  if agentmanager.stat = 'A' 
   then
    do:
      message string(agentmanager.managerID) + " Manager can't be deleted," skip 
              " Manager is currently active" view-as alert-box information buttons ok.
      return.
    end.
    
  message "Highlighted Agent Manager will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no title "Delete Agent Manager" update lChoice.
 
  if not lChoice 
   then
    return.
    
  if valid-handle(hFileDataSrv) and (agentmanager.managerID ne 0) 
   then
    do:
      run deleteAgentManager in hFileDataSrv(input agentmanager.managerID, output lManager, output opcMsg).
      
     if not lManager 
       then
        do:
          message opcMsg view-as alert-box information buttons ok.
          return.
        end. 
    end.   
  run DeleteManager in hSource (input tManagerID,output pSuccess).
  if pSuccess 
   then
    delete agentmanager.  
  
  close query brwdata.
  {&open-query-brwData}
  doModify(false).
  setStatusCount(num-results("{&browse-name}")).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tManagerType tManager tIsPrimary tStatus tEffDate tExpDate tType 
          tTypeDenied tAmountDenied lInformation tAmount lIsPrimaryLabel 
          lCommission fMarkMandatory1 fMarkMandatory2 fMarkMandatory3 
      WITH FRAME fManager IN WINDOW C-Win.
  ENABLE brwData bAdd bCancel bDelete bExport bRefresh bSave tManagerType 
         tManager tIsPrimary tStatus tEffDate tExpDate tType tTypeDenied 
         tAmountDenied lInformation tAmount lIsPrimaryLabel lCommission 
         rInformation rCommssion RECT-12 RECT-69 
      WITH FRAME fManager IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fManager}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                          
 def var th as handle no-undo.
 if query brwData:num-results = 0
  then
   do:
     message "There is nothing to export"
     view-as alert-box warning buttons ok.
     return.
   end.
  
 for each agentmanager no-lock:
   if agentmanager.type = "A" 
    then agentmanager.typeDesc = "Agency".
    else if agentmanager.type = "E" 
     then agentmanager.typeDesc = "Executive Team".
     else if agentmanager.type = "S" 
      then agentmanager.typeDesc = "Sales".
      else if agentmanager.type = "H" 
       then agentmanager.typeDesc = "Searcher".
        else if agentmanager.type = "U" 
        then agentmanager.typeDesc = "Underwriter".
 end.

 publish "GetReportDir" (output std-ch).
 th = temp-table agentmanager:handle.
 run util/exporttable.p (table-handle th,
                         "agentmanager",
                         "for each agentmanager ",
                         "isPrimary,typeDesc,uidDesc,statDesc,effDate,expDate",
                         "Primary,Type,Name,Status,Effective,Expiration",
                         std-ch,
                         "AgentManagers- " + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newAgentManager C-Win 
PROCEDURE newAgentManager :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pSuccess as logical no-undo.
  empty temp-table ttagentmanager.
  empty temp-table tagentmanager.
  
  do with frame {&frame-name}:
    if tEffDate:input-value = ?
     then
      do:
        message "Effective Date is required" view-as alert-box information buttons ok.
        return no-apply.
      end.
    if tManagerType:screen-value = ? or tManagerType:screen-value = "None"
     then
      do:
        message "Manager Type is required" view-as alert-box information buttons ok.
        return no-apply.
      end.
    if tManager:screen-value = ? or tManager:screen-value = ""
     then
      do:
        message "Manager is required" view-as alert-box information buttons ok.
        return no-apply.
      end.

      create tagentmanager.
      do:
        assign
            tIsChange                    = true
            tagentmanager.type           = tManagerType:screen-value
            tagentmanager.managerID      = tManagerID
            tagentmanager.uid            = tManager:screen-value
            tagentmanager.stat           = tStatus:screen-value
            tagentmanager.effDate        = tEffDate:input-value
            tagentmanager.expDate        = tExpDate:input-value
            tagentmanager.isPrimary      = tIsPrimary:checked
            tagentmanager.commissionCode = (if pCanChange then "" else tType:screen-value)
            tagentmanager.commissionRate = (if pCanChange then 0 else decimal(tAmount:screen-value) )   .
            
       for first agentmanager where agentmanager.managerID = tagentmanager.managerID :
         tagentmanager.createdBy      = agentmanager.createdBy .
         tagentmanager.dateCreated    = agentmanager.dateCreated.
       end.
       
       if tagentmanager.stat = "A"
        then
         tagentmanager.statDesc = "Active".
        else if tagentmanager.stat = "A"
         then
          tagentmanager.statDesc = "Inactive".
        publish "GetSysUserName" (tagentmanager.uid, output tagentmanager.uidDesc).
        dataSortDesc = not dataSortDesc.
      end.
    if tIsChange
     then 
      run SetManager in hSource (table tagentmanager,output pSuccess).
     
    if pSuccess 
     then
      publish "refreshTags".

    run getManagerData in hSource (output table ttagentmanager).
    empty temp-table agentmanager.
    for each ttagentmanager:
      create agentmanager.
      buffer-copy ttagentmanager to agentmanager no-error.
    end.
    close query brwdata.  
    {&open-query-brwData}
    doModify(false).
    setStatusCount(num-results("{&browse-name}")).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAgentManagers C-Win 
PROCEDURE refreshAgentManagers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hFileDataSrv) 
   then
    do:
      run LoadAgentManagers in hFileDataSrv.
      {&open-query-brwData}
    end.
  setStatusCount(num-results("{&browse-name}")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valueChange C-Win 
PROCEDURE valueChange :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign 
        bSave:sensitive       = true
        bCancel:sensitive     = true.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
@description Sets the window to modify or view
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    if not can-find(first agentmanager) 
     then
      assign 
          bExport:sensitive = false
          bDelete:sensitive = false
          tManagerType:screen-value = "None" 
          tManager:list-item-pairs  = "" + tManager:delimiter + ""
          tIsPrimary:checked        = not can-find(first agentmanager where isPrimary = true)
          tStatus:screen-value      = "A"
          tEffDate:screen-value     = ""
          tExpDate:screen-value     = "".
     else 
      assign 
          bExport:sensitive = true
          bDelete:sensitive = true.
    
   if can-find(first agentmanager where managerID ne 0) 
    then
     bRefresh:sensitive = true.
    else 
     bRefresh:sensitive = false.
         
    find first agentmanager where agentmanager.managerID = agentmanager.managerID no-lock no-error.
    if available agentmanager
     then
      do:
        if agentmanager.stat = "A" 
         then bDelete:sensitive = false.
         else bDelete:sensitive = true.
        /* assign */
        assign
            tManagerID                = agentmanager.managerID
            tManagerType:screen-value = if agentmanager.type = "0" then "None" else agentmanager.type
            tManagerType:sensitive    = pmodify
            tManager:list-item-pairs  = agentmanager.uidDesc + tManager:delimiter + agentmanager.uid
            tManager:screen-value     = agentmanager.uid
            tManager:sensitive        = pmodify
            tIsPrimary:checked        = agentmanager.isPrimary
            tStatus:screen-value      = agentmanager.stat
            tType:screen-value        = agentmanager.commissionCode
            tAmount:screen-value      = string(agentmanager.commissionRate)
            tEffDate:screen-value     = string(agentmanager.effDate)
            tEffDate:read-only        = true
            tExpDate:screen-value     = string(agentmanager.expDate)
            tExpDate:read-only        = true
            /* disable based on status */
            std-lo                    = agentmanager.stat <> "I"
            tStatus:sensitive         = std-lo
            tIsPrimary:sensitive      = std-lo
            brwData:hidden            = false
            tTypeDenied:hidden        = false 
            tAmount:hidden            = true
            bSave:sensitive           = pmodify
            bCancel:sensitive         = pModify
            fMarkMandatory1:hidden    = true
            fMarkMandatory2:hidden    = true
            fMarkMandatory3:hidden    = true
            tAmount:side-label-handle:hidden  = false no-error.
      end.
      if pAction = "N" and can-find(first agentmanager where agentmanager.managerID = 0) 
       then
        assign bAdd:sensitive = false.
       else 
        bAdd:sensitive = true.
  end.     
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
  /*------------------------------------------------------------------------------
    Purpose:  
    Notes:  
  ------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.    /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

