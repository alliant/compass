&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wpostedvendortask.w

  Description: Window for posted vendor task

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: sagar

  Created: 08.25.2023
  
  @Modified:
  Date        Name     Description   
  07/02/24    S Chandu Task #:113230 Bug fixes on the screen.
  07/19/24    SRK      Modified  voucherID as integer
  08/01/24    SRK      Modified generate vendor documents
  11/06/24    SRK      Modified to fix bug
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/set-button-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/get-column.i}
{lib/winshowscrollbars.i}
{lib/normalizefileid.i}  /* include file to normalize file number */
{lib/add-delimiter.i}

/* Temp-table Definition */
{tt/fileTask.i}
{tt/fileTask.i &tableAlias="ttFileTask"} 
{tt/fileTask.i &tableAlias="tFileTask"}
{tt/fileTask.i &tableAlias="inputFileTask"}
{tt/apinv.i}
{tt/apinv.i &tableAlias=ttapinv}
{tt/apvendor.i }
{tt/proerr.i  &tableAlias="fileTaskPostErr"}
define temp-table tapinv no-undo like apinv
  field voucherID     as integer.

{lib/brw-multi-def.i}

define variable devendorColumnWidth  as decimal    no-undo.
define variable deuidColumnWidth     as decimal    no-undo.
define variable iCount               as integer    no-undo.
define variable cVendorList          as character  no-undo.
define variable dtstartdate          as date       no-undo.
define variable dtenddate            as date       no-undo.
define variable hRepoWindow          as handle     no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwFileTask

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES fileTask tapinv

/* Definitions for BROWSE brwFileTask                                   */
&Scoped-define FIELDS-IN-QUERY-brwFileTask fileTask.fileNumber filetask.agent fileTask.AssignedDate fileTask.EndDateTime fileTask.AssignedToName fileTask.cost   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFileTask   
&Scoped-define SELF-NAME brwFileTask
&Scoped-define QUERY-STRING-brwFileTask for each fileTask
&Scoped-define OPEN-QUERY-brwFileTask open query {&SELF-NAME} for each fileTask.
&Scoped-define TABLES-IN-QUERY-brwFileTask fileTask
&Scoped-define FIRST-TABLE-IN-QUERY-brwFileTask fileTask


/* Definitions for BROWSE brwpostedvendor                               */
&Scoped-define FIELDS-IN-QUERY-brwpostedvendor tapinv.voucherID tapinv.vendorName tapinv.task tapinv.glaccount tapinv.invoiceNumber tapinv.invoiceDate tapinv.username tapinv.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwpostedvendor   
&Scoped-define SELF-NAME brwpostedvendor
&Scoped-define QUERY-STRING-brwpostedvendor for each tapinv
&Scoped-define OPEN-QUERY-brwpostedvendor open query {&SELF-NAME} for each tapinv.
&Scoped-define TABLES-IN-QUERY-brwpostedvendor tapinv
&Scoped-define FIRST-TABLE-IN-QUERY-brwpostedvendor tapinv


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwFileTask}~
    ~{&OPEN-QUERY-brwpostedvendor}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS breport btSetPeriod bdoc cbVendor bExport ~
fDateStart fDateEnd brwpostedvendor brwFileTask btClear btGo RECT-82 ~
RECT-85 
&Scoped-Define DISPLAYED-OBJECTS cbVendor fDateStart fDateEnd 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bdoc  NO-FOCUS
     LABEL "Document" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON breport  NO-FOCUS
     LABEL "Report" 
     SIZE 7.2 BY 1.71 TOOLTIP "Genarate Report".

DEFINE BUTTON btClear 
     IMAGE-UP FILE "images/s-cross.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Blank out the date range".

DEFINE BUTTON btGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON btSetPeriod 
     IMAGE-UP FILE "images/s-calendar.bmp":U NO-FOCUS
     LABEL "" 
     SIZE 4.8 BY 1.14 TOOLTIP "Set default date range from first and last open period".

DEFINE VARIABLE cbVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 46.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDateEnd AS DATE FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fDateStart AS DATE FORMAT "99/99/99":U 
     LABEL "Completed From" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-82
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 76 BY 3.05.

DEFINE RECTANGLE RECT-85
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.8 BY 3.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFileTask FOR 
      fileTask SCROLLING.

DEFINE QUERY brwpostedvendor FOR 
      tapinv SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFileTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFileTask C-Win _FREEFORM
  QUERY brwFileTask DISPLAY
      fileTask.fileNumber       label "File Number"         format "x(30)"          width 20
      filetask.agent            label "Agent"               format "x(100)"         width 40
fileTask.AssignedDate           column-label "Assigned"     format "99/99/9999"     width 15
fileTask.EndDateTime            column-label "Completed"    format "99/99/9999"     width 15
fileTask.AssignedToName         column-label "Completed By" format "x(30)"          width 35
fileTask.cost                   label "Cost"                format ">>>,>>>,>>9.99" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 153.2 BY 9.76
         TITLE "File Task Details" ROW-HEIGHT-CHARS 1 FIT-LAST-COLUMN.

DEFINE BROWSE brwpostedvendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwpostedvendor C-Win _FREEFORM
  QUERY brwpostedvendor DISPLAY
      tapinv.voucherID     label  "Voucher ID"         width 13
tapinv.vendorName      label  "Vendor"        format "x(60)"          width 32
tapinv.task            label  "Task"               format "x(60)"          width 22
tapinv.glaccount       label  "GL account"         format "x(30)"          width 13
tapinv.invoiceNumber   label  "Invoice Number"     format "x(60)"          width 17
tapinv.invoiceDate     label  "Invoiced"           format "99/99/9999"     width 15
tapinv.username        label  "Posted By"          format "x(50)"          width 18
tapinv.amount          label  "Amount"             format  ">>>,>>>,>>9.99"  width 11
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 153.2 BY 10.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     breport AT ROW 1.95 COL 93.2 WIDGET-ID 482 NO-TAB-STOP 
     btSetPeriod AT ROW 2.76 COL 62.4 WIDGET-ID 450 NO-TAB-STOP 
     bdoc AT ROW 1.95 COL 86.2 WIDGET-ID 478 NO-TAB-STOP 
     cbVendor AT ROW 1.76 COL 18.2 COLON-ALIGNED WIDGET-ID 458
     bExport AT ROW 1.95 COL 79.2 WIDGET-ID 480 NO-TAB-STOP 
     fDateStart AT ROW 2.81 COL 18.2 COLON-ALIGNED WIDGET-ID 444
     fDateEnd AT ROW 2.81 COL 41.4 COLON-ALIGNED WIDGET-ID 462
     brwpostedvendor AT ROW 4.43 COL 1.8 WIDGET-ID 200
     brwFileTask AT ROW 15.57 COL 1.8 WIDGET-ID 300
     btClear AT ROW 2.76 COL 57.6 WIDGET-ID 448 NO-TAB-STOP 
     btGo AT ROW 1.95 COL 68.6 WIDGET-ID 452 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 79.2 WIDGET-ID 194
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.05 COL 3.6 WIDGET-ID 476
     RECT-82 AT ROW 1.29 COL 2 WIDGET-ID 460
     RECT-85 AT ROW 1.29 COL 77.4 WIDGET-ID 484
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1
         SIZE 171.6 BY 25.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Posted Vouchers"
         HEIGHT             = 24.48
         WIDTH              = 155.2
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwpostedvendor fDateEnd DEFAULT-FRAME */
/* BROWSE-TAB brwFileTask brwpostedvendor DEFAULT-FRAME */
ASSIGN 
       brwFileTask:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFileTask:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwpostedvendor:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwpostedvendor:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFileTask
/* Query rebuild information for BROWSE brwFileTask
     _START_FREEFORM
open query {&SELF-NAME} for each fileTask.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFileTask */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwpostedvendor
/* Query rebuild information for BROWSE brwpostedvendor
     _START_FREEFORM
open query {&SELF-NAME} for each tapinv.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwpostedvendor */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Posted Vouchers */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Posted Vouchers */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Posted Vouchers */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdoc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdoc C-Win
ON CHOOSE OF bdoc IN FRAME DEFAULT-FRAME /* Document */
DO:
  if not available tapinv
    then return.
  if not valid-handle(hRepoWindow)
   then run repository.w persistent set hRepoWindow ("APVoucher", string(tapinv.refID)).
   else run ShowWindow in hRepoWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME breport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL breport C-Win
ON CHOOSE OF breport IN FRAME DEFAULT-FRAME /* Report */
DO:
  run server/regeneratevendorremittance.p (input  tapinv.voucherID,     /* voucherID */
                                           input  tapinv.invoiceDate, /* Invoice  Date*/
                                           input  'pdf',   /* Invoice end Date*/
                                           output std-lo,
                                           output std-ch).
                                           
  message    std-ch
      view-as  alert-box  information  buttons  ok .
  if std-lo
   then
    do:
       tapinv.hasDocument = true.
       apply "value-changed" to brwpostedvendor.
    end.
        
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFileTask
&Scoped-define SELF-NAME brwFileTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileTask C-Win
ON ROW-DISPLAY OF brwFileTask IN FRAME DEFAULT-FRAME /* File Task Details */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileTask C-Win
ON START-SEARCH OF brwFileTask IN FRAME DEFAULT-FRAME /* File Task Details */
DO:
   {lib/brw-startSearch-multi.i &browseName=brwFileTask &sortClause="'by fileNumber'"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwpostedvendor
&Scoped-define SELF-NAME brwpostedvendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpostedvendor C-Win
ON ROW-DISPLAY OF brwpostedvendor IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpostedvendor C-Win
ON START-SEARCH OF brwpostedvendor IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch-multi.i &browseName=brwpostedvendor &sortClause="'by voucherID desc'"}.
  apply 'value-changed' to brwpostedvendor. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwpostedvendor C-Win
ON VALUE-CHANGED OF brwpostedvendor IN FRAME DEFAULT-FRAME
DO:
  run fileTaskDetails in this-procedure.
  if not available tapinv
   then
    return.
    
  if tapinv.hasDocument
   then
     breport:sensitive = false.
   else
     breport:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME DEFAULT-FRAME
DO:
 assign 
     fDateStart:screen-value = ""
     fDateEnd:screen-value   = ""
     .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGo C-Win
ON CHOOSE OF btGo IN FRAME DEFAULT-FRAME /* Go */
DO:      
  run getData in this-procedure.           
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSetPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSetPeriod C-Win
ON CHOOSE OF btSetPeriod IN FRAME DEFAULT-FRAME
DO:
  assign 
      fDateStart:screen-value = string(dtstartdate)
      fDateEnd:screen-value   = string(dtenddate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbVendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbVendor C-Win
ON VALUE-CHANGED OF cbVendor IN FRAME DEFAULT-FRAME /* Vendor */
DO:
  /*lFilterData = false.
  apply 'choose':U to bFilter.  */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFileTask
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
/* {lib/brw-main.i} */
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwpostedvendor,brwFileTask"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

/* {lib/set-button.i &frame-name={&FRAME-NAME} &b=bopen   &label="Open"} */
{lib/set-button.i &frame-name={&FRAME-NAME} &b=bopen   &image="images/magnifier.bmp" &inactive="images/magnifier-i.bmp"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=bExport   &label="Export"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=btResetFilter   &label="FilterClear"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=btGo   &label="Go"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=bCancel   &label="Cancel"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=bdoc   &image="images/attach.bmp" &inactive="images/attach-i.bmp"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=breport   &image="images/report.bmp" &inactive="images/report-i.bmp"}

setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  run enable_UI.

  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  
  if std-ch = "TPS" then
  do:
    publish "loadPeriods". 
  end.
  /* Getting date range from first and last open active accounting period */   
  publish "getPeriodForPostedTasks" (output dtstartdate,output dtenddate).
  
  assign 
     fDateStart:screen-value = string(dtstartdate)
     fDateEnd:screen-value   = string(dtenddate).
  
  {lib/get-column-width.i &browse-name=brwpostedvendor &var=devendorColumnWidth &col="'vendorName'"}
  {lib/get-column-width.i &browse-name=brwFileTask &var=deuidColumnWidth &col="'AssignedToName'"}
 /* {lib/get-column-width.i &browse-name=brwpostedvendor &var=deuidColumnWidth &col="'username'"}     */
  breport:sensitive = false  .
                             
  run displaydata in this-procedure.
   
  /* Makes filters enable-disable based on the data */
                               

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  publish "GetApVendors" (input "B", output table apvendor).
  do with frame {&frame-name}:
  end.
  
  assign cbVendor:delimiter = ";".
         
  for each apvendor no-lock by apvendor.vendorname:
     cvendorlist = addDelimiter(cvendorlist, ";") + trim(apvendor.vendorname) + " (" + string(trim(apvendor.vendorid)) + ")" + ";" + string(trim(apvendor.vendorid)).
  end.

  cbVendor:list-item-pairs  = "All" + ";"  + "All" + (if cvendorlist  = "" then ""  else (";" + cvendorlist)).
  
  cbVendor:screen-value          = "All" .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActions C-Win 
PROCEDURE enableActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwpostedvendor:num-results > 0
   then
    assign bExport:sensitive = true.
   else
    assign bExport:sensitive  = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbVendor fDateStart fDateEnd 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE breport btSetPeriod bdoc cbVendor bExport fDateStart fDateEnd 
         brwpostedvendor brwFileTask btClear btGo RECT-82 RECT-85 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwpostedvendor:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
   
  empty temp-table inputFileTask. 
  for each ttfiletask:
    create inputFileTask.
    buffer-copy ttfiletask to inputFileTask.
  end.
  for each ttapinv no-lock:
    if not can-find( first ttfiletask where ttfiletask.apInvID = ttapinv.apinvID)
     then
      do:
        create inputFileTask.
        assign
           inputFileTask.VendorID      = ttapinv.VendorID
           inputFileTask.VendorName    = ttapinv.VendorName
           inputFileTask.refID         = ttapinv.refID
           inputFileTask.invoiceNumber = ttapinv.invoiceNumber
           inputFileTask.invoiceDate   = ttapinv.invoiceDate
           inputFileTask.amount        = ttapinv.amount
           inputFileTask.refID         = ttapinv.refID.
      end.
  end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table inputFileTask:handle.
  run util/exporttable.p (table-handle std-ha,
                          "inputFileTask",
                          "for each inputFileTask",
                          "refID,vendorID,vendorname,topic,invoiceNumber,invoiceDate,username,amount,fileNumber,AssignedDate,EndDateTime,AssignedToName,cost",
                          "Voucher ID,Vendor ID,Vendor,Task,Invoice Number,Invoiced,Posted By,Amount,File Number,Assigned,Completed,Completed By,Cost" ,
                          std-ch,
                          "File Task Details-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fileTaskDetails C-Win 
PROCEDURE fileTaskDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table fileTask.
  for each ttfiletask where ttfiletask.apinvID   = tapinv.apinvID:
    create fileTask.
    buffer-copy ttfiletask to fileTask.
    assign
        filetask.agent = filetask.agent + ' (' + filetask.agentID + ')'.
  end.
  
  open query brwFileTask preselect each fileTask.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer ttfileTask for ttfileTask.
  define variable cvendor    as character no-undo.
  define variable dstartdate as date      no-undo.
  define variable denddate   as date      no-undo.

  do with frame {&frame-name}:
  end.
  assign
  cvendor    = cbVendor:screen-value.
  dstartdate = date(fDateStart:screen-value).
  denddate   = date(fDateEnd:screen-value).

  if cvendor = "All" and (dstartdate = ? or denddate = ?)
   then
    do:
      message  "You must enter date range and/or Vendor"
         view-as alert-box warning buttons ok.
      return.
    end.
  if cvendor = "All" 
   then
    do:
      cvendor = "". 
    end.
  if dstartdate <> ? and denddate = ?
   then
    do:
      denddate = today. 
    end.
  
  empty temp-table filetask.
  empty temp-table tapinv.
   /* call server */
  run server/getpostedvendortasks.p (input  cvendor,     /* VendorID */
                                     input  dstartdate, /* Invoice Start Date*/
                                     input  denddate,   /* Invoice end Date*/
                                     output table ttfiletask,
                                     output table ttapinv,
                                     output std-lo,
                                     output std-ch).

  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      open query brwpostedvendor preselect each tapinv.
      apply "value-changed" to brwpostedvendor.
      return.
    end.

  for each ttapinv:
    create tapinv.
    buffer-copy ttapinv to tapinv.
    tapinv.voucherID =  integer(ttapinv.RefID) no-error.
    
    for first ttfiletask where ttfiletask.refId   = ttapinv.refID
                           and ttfiletask.apinvID = ttapinv.apinvID:
     assign
         tapinv.task      = ttfiletask.topic
         tapinv.glaccount = ttfiletask.glaccount
         tapinv.vendorName = trim(tapinv.vendorName) + ' (' + trim(tapinv.vendorID) + ')'.
         .
    end.
  end.
  
  open query brwpostedvendor preselect each tapinv.
  
  apply "value-changed" to brwpostedvendor.
  apply "start-search" to brwpostedvendor.
  /* This will use the screen-value of the filters which is ALL */
  /*run filterData  in this-procedure.    */
  run enableActions in this-procedure.
      
  setStatusRecords(query brwpostedvendor:num-results).     
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData-multi.i}
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      
      brwfiletask:width-pixels               = frame {&frame-name}:width-pixels - 9.5
      
      brwpostedvendor:width-pixels    = brwfiletask:width-pixels
      brwpostedvendor:height-pixels   = frame {&frame-name}:height-pixels - brwfiletask:height-pixels - 73
      
      brwfiletask:y                          = frame {&frame-name}:height-pixels - brwfiletask:height-pixels - 5
      .

  {lib/resize-column.i &browse-name=brwpostedvendor &col="'vendorName'" &var=devendorColumnWidth}
  {lib/resize-column.i &browse-name=brwFileTask &col="'AssignedToName'" &var=deuidColumnWidth}
 /* {lib/resize-column.i &browse-name=brwpostedvendor &col="'username'" &var=deuidColumnWidth}    */
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

