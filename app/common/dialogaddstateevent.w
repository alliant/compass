&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogverifyqual.w

  Description: Dialog to add or edit a qualification review.

  Input Parameters:
      ipchStateID         :state ID
      ipintQualificationID:Qualification ID
      ipchQualStatus      :Qualification Status
      ipdtEffectiveDate   :Effective Date
      ipdtExpirationDate  :Expiration Date

  Input-Output Parameters:
      review              :review temp-table

  Author:Sachin Chaturvedi 

  Created:05.05.2018 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/stateevent.i}
{lib/Com-def.i}

/* Parameters Definitions ---                                           */
define input parameter ipcStateID   as character no-undo.
define input-output parameter table for stateevent.
/* Local Variable Definitions ---                                       */
define variable cCurrUser as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bCancel flEventDate edDescription 
&Scoped-Define DISPLAYED-OBJECTS flStateId flUserID flEventDate ~
edDescription 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel"
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save"
     BGCOLOR 8 .

DEFINE VARIABLE edDescription AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 59 BY 2.95 NO-UNDO.

DEFINE VARIABLE flEventDate AS DATE FORMAT "99/99/99":U 
     LABEL "Event Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flStateId AS CHARACTER FORMAT "X(256)":U 
     LABEL "State ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flUserID AS CHARACTER FORMAT "X(256)":U 
     LABEL "User ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bCancel AT ROW 8.14 COL 43.8 NO-TAB-STOP 
     bSave AT ROW 8.14 COL 26.6 NO-TAB-STOP 
     flStateId AT ROW 1.38 COL 22.2 COLON-ALIGNED WIDGET-ID 40
     flUserID AT ROW 2.57 COL 22.2 COLON-ALIGNED WIDGET-ID 38
     flEventDate AT ROW 3.76 COL 22.2 COLON-ALIGNED WIDGET-ID 36
     edDescription AT ROW 4.91 COL 24.2 NO-LABEL WIDGET-ID 42
     "Description:" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 5.05 COL 12.4 WIDGET-ID 44
     SPACE(60.79) SKIP(3.99)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New State Event"
         CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flStateId IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flStateId:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flUserID IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flUserID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New State Event */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:
  do with frame {&frame-name}:
  end.
  
  if edDescription:screen-value = ""
   then
    do:
      message "Description cannot be blank."
         view-as alert-box info buttons ok.
      return no-apply.
    end.

  if flEventDate:screen-value = "" or date(flEventDate:screen-value) = ? 
   then
    do:
      message "Event Date cannot be blank."
         view-as alert-box info buttons ok.
      return no-apply.
    end.

  if date(flEventDate:screen-value) > today 
   then
    do:
      message "Event Date cannot be in future."
         view-as alert-box info buttons ok.
      flEventDate:screen-value = string(today).
      return no-apply.
    end.

  find first stateevent no-error.
  if not available stateevent 
   then
    create stateevent.
   
  assign stateevent.stateID           = ipcStateID
         stateevent.eventDate         = datetime(date(flEventDate:screen-value),mtime)
         stateevent.UID               = cCurrUser
         stateevent.description       = edDescription:screen-value no-error.
    
  if stateevent.seq = 0 
   then
    publish "newStateEvent" (input-output table stateevent).
  else
    publish "ModifyStateEvent" (input-output table stateevent).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edDescription Dialog-Frame
ON VALUE-CHANGED OF edDescription IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flEventDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flEventDate Dialog-Frame
ON VALUE-CHANGED OF flEventDate IN FRAME Dialog-Frame /* Event Date */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? THEN
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  publish "GetCredentialsName" (output cCurrUser).
  
  assign flEventDate:screen-value = string(today)
         flStateId:screen-value   = ipcStateID
         flUserID:screen-value    = cCurrUser.

  find first stateevent no-error.
  if available stateevent then
    assign edDescription:screen-value = stateevent.description
           flEventDate:screen-value   = string(stateevent.eventDate) 
           frame Dialog-Frame:title   = "Edit State Event" no-error.
  else
    frame Dialog-Frame:title = "New State Event" no-error.

/*   on value-changed anywhere                        */
/*     bSave:sensitive in frame {&frame-name} = true. */
    
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first stateevent no-error.
  if available stateevent
   then
    do:
      bSave:sensitive = not (flEventDate:input-value  = date(stateevent.eventDate) and
                            (edDescription:screen-value = "" or edDescription:input-value = stateevent.description) ).
    end.
  else
   if edDescription:screen-value ne "" 
    then
     bSave:sensitive = true.
   else
    bSave:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flStateId flUserID flEventDate edDescription 
      WITH FRAME Dialog-Frame.
  ENABLE bCancel flEventDate edDescription 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

