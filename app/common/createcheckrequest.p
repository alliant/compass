&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name createpdf.p
@description Creates a pdf of a payable

@param Claim;complex;The claim temp table

@author John Oliver
@version 1.0
@created 10/28/2016
----------------------------------------------------------------------*/

{lib/rpt-def.i &ID="'Claim'"}
{tt/checkrequest.i &tableAlias="ttPDF"}
{tt/apinva.i &tableAlias="ttApproval"}

define input parameter table for ttPDF.
define input parameter table for ttApproval.
define input parameter pIsComplete as logical no-undo.
define output parameter pFilename as character.
{lib/std-def.i}

define variable iLines as integer no-undo initial 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .

run getFilename in this-procedure (output activeFilename).
if activeFilename = ""
 then return.

{lib/rpt-open.i}
 
run claimDetails in this-procedure.
run paymentDetails in this-procedure.
run paymentApproval in this-procedure.

{lib/rpt-cloz.i &exclude-pdfview=true}
pFilename = activeFilename.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-claimDetails) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE claimDetails Procedure 
PROCEDURE claimDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttPDF for ttPDF.

  blockTitle("CLAIM DETAILS").
  textColor(0.0, 0.0, 0.0).

  for first ttPDF no-lock:
    /* claim number and summary */
    setFont("Helvetica-Bold", 12.0).
    textAt ("File Number", 14).
    textAt ("Summary", 50).
    newLine(1).
    setFont("Helvetica", 12.0).
    textAt (string(ttPDF.claimID), 14).
    iLines = iLines + wordWrap(ttPDF.claimSummary,70,50).
    newLine(1).
    setFont("Helvetica-Bold", 12.0).
    textAt ("Insured Name:", 14).
    setFont("Helvetica", 12.0).
    iLines = iLines + wordWrap(ttPDF.insuredName,70,50).
    newLine(1).
    /* agent number, name, and file number */
    setFont("Helvetica-Bold", 12.0).
    textAt ("Agent Number", 14).
    textAt ("Agent File Number", 50).
    textAt ("Agent Name", 100).
    newLine(1).
    setFont("Helvetica", 12.0).
    textAt (string(ttPDF.agentID), 14).
    textAt (ttPDF.agentFileNumber, 50).
    iLines = iLines + wordWrap(ttPDF.agentName,50,100).
    newLine(1).
    createLine(125 + ((iLines - 4) * 12)).
    /* alta code risk and responsibility */
    setFont("Helvetica-Bold", 12.0).
    textAt ("Alta Code Risk", 14).
    textAt ("Alta Code Responsibility", 70).
    newLine(1).
    setFont("Helvetica", 12.0).
    textAt ((if ttPDF.altaRisk > "" then ttPDF.altaRisk else "Unknown"), 14).
    iLines = iLines + wordWrap((if ttPDF.altaResponsibility > "" then ttPDF.altaResponsibility else "Unknown"),50,70).
    newLine(1).
    createLine(160 + ((iLines - 5) * 12)).
    /* agent number, name, and file number */
    setFont("Helvetica-Bold", 12.0).
    textAt ("Policy Number", 14).
    textAt ("Policy Amount", 50).
    textAt ("Policy Date", 80).
    newLine(1).
    setFont("Helvetica", 12.0).
    textAt (string(ttPDF.policyID), 14).
    textAt (string(ttPDF.policyAmount), 50).
    if ttPDF.policyDate = ?
     then textAt ("Unknown", 80).
     else textAt (string(date(ttPDF.policyDate)), 80).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getFilename Procedure 
PROCEDURE getFilename PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pFilename as character no-undo.
  
  define buffer ttPDF for ttPDF.
  for first ttPDF no-lock:
    publish "GetReportDir" (output pFilename).
    if pFilename = ""
     then pFilename = os-getenv("appdata") + "\Alliant\".
    if pFilename > ""
     then
      do:
        pFilename = pFilename + "file_" + string(ttPDF.claimID) + "_loss_check_request.pdf".
        if search(pFilename) <> ? 
         then os-delete value(pFilename).
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-paymentApproval) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE paymentApproval Procedure 
PROCEDURE paymentApproval PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttApproval for ttApproval.
  
  blockTitle("APPROVALS").
  textColor(0.0, 0.0, 0.0).

  for each ttApproval no-lock:
    case ttApproval.stat:
     when "P" then
      do:
        setFont ("Helvetica-Bold", 12.0).
        textAt ("Pending By:", 14).
        setFont ("Helvetica", 12.0).
        textAt (ttApproval.username, 60).
      end.
     when "D" then
      do:
        setFont ("Helvetica-Bold", 12.0).
        textAt ("Denied By:", 14).
        setFont ("Helvetica", 12.0).
        textAt (ttApproval.username, 60).
        setFont ("Helvetica", 12.0).
        textAt ("Date " + string(ttApproval.dateActed), 100).
      end.
     when "A" then
      do:
        setFont ("Helvetica-Bold", 12.0).
        textAt ("Approved By:", 14).
        setFont ("Helvetica", 12.0).
        textAt (ttApproval.username, 60).
        setFont ("Helvetica", 12.0).
        textAt ("Date " + string(ttApproval.dateActed), 100).
      end.
    end case.
    newLine(1).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-paymentDetails) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE paymentDetails Procedure 
PROCEDURE paymentDetails PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttPDF for ttPDF.
  
  blockTitle("PAYMENT DETAILS").
  textColor(0.0, 0.0, 0.0).

  /* account number and name */
  /* headers */
  setFont("Helvetica-Bold", 12.0).
  textAt ("Account Number", 14).
  textAt ("Account Name", 80).
  newLine(1).
  for each ttPDF no-lock
     where ttPDF.account <> "":
    /* lines */
    iLines = iLines + 1.
    setFont("Helvetica", 12.0).
    textAt (string(ttPDF.account), 14).
    textAt (string(ttPDF.accountName), 80).
    newLine(1).
  end.
  createLine(258 + ((iLines - 6) * 12)).
  newLine(1).
  for first ttPDF no-lock:
    /* vendor name and address */
    setFont("Helvetica-Bold", 12.0).
    textAt ("Vendor ID:", 14).
    setFont("Helvetica", 12.0).
    textAt (ttPDF.vendorID, 60).
    newLine(1).
    
    setFont("Helvetica-Bold", 12.0).
    textAt ("Vendor Name:", 14).
    setFont("Helvetica", 12.0).
    iLines = iLines + wordWrap(ttPDF.vendorName,70,60).
    
    setFont("Helvetica-Bold", 12.0).
    textAt ("Vendor Address:", 14).
    setFont("Helvetica", 12.0).
    if ttPDF.vendorAddress > ""
     then iLines = iLines + wordWrap(ttPDF.vendorAddress,70,60).
     else
      do:
        newLine(1).
        iLines = iLines + 1.
      end.
    newLine(1).
    createLine(293 + ((iLines - 7) * 12)).
    /* amount */
    setFont("Helvetica-Bold", 12.0).
    if pIsComplete
     then textAt ("Completed Amount:", 14).
     else textAt ("Requested Amount:", 14).
    setFont("Helvetica", 12.0).
    textAt ("$" + string(ttPDF.amount, ">>>,>>>,>>>,>>>,>>9.99"), 60).
    newLine(1).
    /* invoice notes */
    setFont("Helvetica-Bold", 12.0).
    textAt ("Invoice Number:", 14).
    setFont("Helvetica", 12.0).
    textAt (ttPDF.invoiceNumber, 60).
    newLine(1).
    setFont("Helvetica-Bold", 12.0).
    textAt ("Invoice Notes:", 14).
    setFont("Helvetica", 12.0).
    iLines = iLines + wordWrap(ttPDF.invoiceNotes,70,60).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/rpt-ftr.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tTitle as character no-undo.
 define variable tTitle2 as character no-undo.
 define variable tXpos as integer no-undo.

 assign 
  tTitle = "Claim Check Request"
  tTitle2 = (if pIsComplete then "Final" else "Preliminary") + " Report"
  tXPos = 300
  .
  
 {lib/rpt-hdr.i &title=tTitle &title2=tTitle2 &title-x=tXpos &title-y=740}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */


