&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*
 dialogagentdashboard.w
 @author J.Oliver
 9.13.2018 das - Added agent to UI as a field
                 Realigned buttons
                 Managed button state (enable/disable) consistently
                 Made data load on open the default behavior (changed my mind)
                 Visualized the window before the ProgressBar displays
*/


CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.
define variable pAgentID as character no-undo.

/* Local Variable Definitions ---                                       */
define variable iTotalSection as integer no-undo initial 0.
{lib/std-def.i}
{lib/brw-multi-def.i}
{lib/set-button-def.i &noEnable=true}

/* column widths */
define variable dClaims as decimal no-undo.
define variable dAlert as decimal no-undo.

&scoped-define months "January,February,March,April,May,June,July,August,September,October,November,December"

/* Table Definitions ---                                                */
{tt/agent.i}
{tt/agentmanager.i}

/* Tables for review sections ---                                       */
{tt/reviewscore.i}
{tt/reviewclaim.i}
{tt/alert.i &tableAlias="reviewalert"}
{tt/cplvolume.i &tableAlias="reviewcpl"}
{tt/policyvolume.i &tableAlias="reviewpolicy"}
{tt/reviewremit.i}
{tt/gpreceivable.i &tableAlias="reviewreceivable"}
{tt/activity.i &tableAlias="reviewactivity"}
{tt/reviewcorrective.i}

define temp-table wt
  field id as integer
  field wButton as handle
  field wLabel as handle
  field wFrame as handle
  field wHeight as decimal
  field isOpen as logical
  field isSensitive as logical
  .
  
/* Function Definitions ---                                             */
{lib/get-column.i}
{lib/find-widget.i}
{lib/add-delimiter.i}
{lib/getstatename.i}
{lib/text-align.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME tActYear1

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES reviewactivity reviewalert reviewclaim ~
reviewcorrective

/* Definitions for BROWSE tActYear1                                     */
&Scoped-define FIELDS-IN-QUERY-tActYear1 reviewactivity.categoryDesc reviewactivity.typeDesc reviewactivity.qtr1 reviewactivity.qtr2 reviewactivity.qtr3 reviewactivity.qtr4 reviewactivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-tActYear1   
&Scoped-define SELF-NAME tActYear1
&Scoped-define QUERY-STRING-tActYear1 FOR EACH reviewactivity where year = year(today) - 2
&Scoped-define OPEN-QUERY-tActYear1 OPEN QUERY {&SELF-NAME} FOR EACH reviewactivity where year = year(today) - 2.
&Scoped-define TABLES-IN-QUERY-tActYear1 reviewactivity
&Scoped-define FIRST-TABLE-IN-QUERY-tActYear1 reviewactivity


/* Definitions for BROWSE tActYear2                                     */
&Scoped-define FIELDS-IN-QUERY-tActYear2 reviewactivity.categoryDesc reviewactivity.typeDesc reviewactivity.qtr1 reviewactivity.qtr2 reviewactivity.qtr3 reviewactivity.qtr4 reviewactivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-tActYear2   
&Scoped-define SELF-NAME tActYear2
&Scoped-define QUERY-STRING-tActYear2 FOR EACH reviewactivity where year = year(today) - 1
&Scoped-define OPEN-QUERY-tActYear2 OPEN QUERY {&SELF-NAME} FOR EACH reviewactivity where year = year(today) - 1.
&Scoped-define TABLES-IN-QUERY-tActYear2 reviewactivity
&Scoped-define FIRST-TABLE-IN-QUERY-tActYear2 reviewactivity


/* Definitions for BROWSE tActYear3                                     */
&Scoped-define FIELDS-IN-QUERY-tActYear3 reviewactivity.categoryDesc reviewactivity.typeDesc reviewactivity.qtr1 reviewactivity.qtr2 reviewactivity.qtr3 reviewactivity.qtr4 reviewactivity.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-tActYear3   
&Scoped-define SELF-NAME tActYear3
&Scoped-define QUERY-STRING-tActYear3 FOR EACH reviewactivity where year = year(today)
&Scoped-define OPEN-QUERY-tActYear3 OPEN QUERY {&SELF-NAME} FOR EACH reviewactivity where year = year(today).
&Scoped-define TABLES-IN-QUERY-tActYear3 reviewactivity
&Scoped-define FIRST-TABLE-IN-QUERY-tActYear3 reviewactivity


/* Definitions for BROWSE tAlert                                        */
&Scoped-define FIELDS-IN-QUERY-tAlert reviewalert.processCodeDesc reviewalert.ownerDesc reviewalert.severityDesc reviewalert.thresholdRange reviewalert.scoreDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-tAlert   
&Scoped-define SELF-NAME tAlert
&Scoped-define QUERY-STRING-tAlert FOR EACH reviewalert where severity > 0
&Scoped-define OPEN-QUERY-tAlert OPEN QUERY {&SELF-NAME} FOR EACH reviewalert where severity > 0.
&Scoped-define TABLES-IN-QUERY-tAlert reviewalert
&Scoped-define FIRST-TABLE-IN-QUERY-tAlert reviewalert


/* Definitions for BROWSE tClaims                                       */
&Scoped-define FIELDS-IN-QUERY-tClaims reviewclaim.claimID reviewclaim.statDesc reviewclaim.agentErrorDesc reviewclaim.description reviewclaim.lossPaid reviewclaim.laePaid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-tClaims   
&Scoped-define SELF-NAME tClaims
&Scoped-define QUERY-STRING-tClaims FOR EACH reviewclaim where laePaid > 0 or lossPaid > 0
&Scoped-define OPEN-QUERY-tClaims OPEN QUERY {&SELF-NAME} FOR EACH reviewclaim where laePaid > 0 or lossPaid > 0.
&Scoped-define TABLES-IN-QUERY-tClaims reviewclaim
&Scoped-define FIRST-TABLE-IN-QUERY-tClaims reviewclaim


/* Definitions for BROWSE tCorrective                                   */
&Scoped-define FIELDS-IN-QUERY-tCorrective reviewcorrective.statDesc reviewcorrective.uidDesc reviewcorrective.dateCreated reviewcorrective.dateDue reviewcorrective.dateComplete reviewcorrective.comment   
&Scoped-define ENABLED-FIELDS-IN-QUERY-tCorrective   
&Scoped-define SELF-NAME tCorrective
&Scoped-define QUERY-STRING-tCorrective FOR EACH reviewcorrective
&Scoped-define OPEN-QUERY-tCorrective OPEN QUERY {&SELF-NAME} FOR EACH reviewcorrective.
&Scoped-define TABLES-IN-QUERY-tCorrective reviewcorrective
&Scoped-define FIRST-TABLE-IN-QUERY-tCorrective reviewcorrective


/* Definitions for FRAME f2Details                                      */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f2Details ~
    ~{&OPEN-QUERY-tClaims}

/* Definitions for FRAME f5Details                                      */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f5Details ~
    ~{&OPEN-QUERY-tAlert}

/* Definitions for FRAME f8Details                                      */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f8Details ~
    ~{&OPEN-QUERY-tActYear1}~
    ~{&OPEN-QUERY-tActYear2}~
    ~{&OPEN-QUERY-tActYear3}

/* Definitions for FRAME f9Details                                      */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f9Details ~
    ~{&OPEN-QUERY-tCorrective}

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD addSection C-Win 
FUNCTION addSection RETURNS LOGICAL
  ( input pSection as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD enableButtons C-Win 
FUNCTION enableButtons RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showError C-Win 
FUNCTION showError RETURNS LOGICAL
  ( input pSuccess as logical, 
    input pMessage as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tERRScore1 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tERRScore2 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tERRScore3 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "ERR Score" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tERRScoreAvg AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tQARDate1 AS DATETIME FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tQARDate2 AS DATETIME FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tQARDate3 AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Audit Date" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tQARScore1 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tQARScore2 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tQARScore3 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "QAR Score" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tQARScoreAvg AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tQARYear1 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tQARYear2 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tQARYear3 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tQARYearAvg AS CHARACTER FORMAT "X(256)":U INITIAL "Average" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-53
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 3.33.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 3.33.

DEFINE RECTANGLE RECT-57
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 3.33.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86 BY .1.

DEFINE VARIABLE tCRExpenses AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     LABEL "Claim Expenses" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tCRNet AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     LABEL "Net Revenue" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tCRRatio AS DECIMAL FORMAT "ZZZZZZZ9.99 %":U INITIAL 0 
     LABEL "Claims Ratio" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitCPL1 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitCPL2 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitCPL3 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     LABEL "CPL Premium" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitCPLTotal AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitGross1 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitGross2 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitGross3 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitGrossTotal AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitNet1 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitNet2 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitNet3 AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitNetTotal AS DECIMAL FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitYear1 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tRemitYear2 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tRemitYear3 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tRemitYearTotal AS CHARACTER FORMAT "X(256)":U INITIAL "Total" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-59
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 3.38.

DEFINE RECTANGLE RECT-60
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86 BY .1.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 3.38.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 3.38.

DEFINE VARIABLE tAR31to60 AS INTEGER FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tAR31to60Label AS CHARACTER FORMAT "X(256)":U INITIAL "31 to 60" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tAR61to90 AS INTEGER FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tAR61to90Label AS CHARACTER FORMAT "X(256)":U INITIAL "61 to 90" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tARCurrent AS INTEGER FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 88888888 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tARCurrentLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Current" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tAROver90 AS INTEGER FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tAROver90Label AS CHARACTER FORMAT "X(256)":U INITIAL "Over 90" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tARTotal AS INTEGER FORMAT "(ZZZ,ZZZ,ZZ9)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tARTotalLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Total" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 108 BY .1.

DEFINE VARIABLE tAlertCritical AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Red" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tAlertTotal AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tAlertWarning AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Yellow" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tPVAvailable AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Available" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tPVAvailableAverage AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Available Average" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tPVLimit AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Policy Limit" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tPVMonth1 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tPVMonth1Label AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tPVMonth2 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tPVMonth2Label AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tPVMonth3 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tPVMonth3Label AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tPVThreeMonthAverage AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Three Month Average" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tPVUnremittedAverage AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Unremitted Average" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tPVUnremmited AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Unremitted" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tCVMonth1 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tCVMonth1Label AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tCVMonth2 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tCVMonth2Label AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tCVMonth3 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tCVMonth3Label AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tCVThreeMonthAverage AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Three Month Average" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tCorrectiveAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 96 BY 2 NO-UNDO.

DEFINE VARIABLE tCorrectiveFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 96 BY 2 NO-UNDO.

DEFINE VARIABLE tCorrectiveQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 96 BY 2 NO-UNDO.

DEFINE VARIABLE tCorrectiveClosed AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tCorrectiveOpen AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Open" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tCorrectivePlanned AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Planned" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE tCorrectiveTotal AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE BUTTON bCollapseAll  NO-FOCUS
     LABEL "Collapse" 
     SIZE 7.2 BY 1.71 TOOLTIP "Collapse all of the sections".

DEFINE BUTTON bExpandAll  NO-FOCUS
     LABEL "Expand" 
     SIZE 7.2 BY 1.71 TOOLTIP "Expand all of the sections".

DEFINE BUTTON bPrint 
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View as PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Update all sections".

DEFINE VARIABLE tAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 94 BY 1 NO-UNDO.

DEFINE VARIABLE tContract AS DATE FORMAT "99/99/9999":U 
     LABEL "Contract Date" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tManager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE BUTTON t1Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t2Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t3Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t4Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t5Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t6Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t7Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t8Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON t9Expand  NO-FOCUS
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE VARIABLE t1Label AS CHARACTER FORMAT "X(256)":U INITIAL "Quality Assurance Review" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t2Label AS CHARACTER FORMAT "X(256)":U INITIAL "Claims" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t3Label AS CHARACTER FORMAT "X(256)":U INITIAL "Remittance" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t4Label AS CHARACTER FORMAT "X(256)":U INITIAL "Open Receivables" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t5Label AS CHARACTER FORMAT "X(256)":U INITIAL "Open Alerts" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t6Label AS CHARACTER FORMAT "X(256)":U INITIAL "Policy Information" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t7Label AS CHARACTER FORMAT "X(256)":U INITIAL "CPL Information" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t8Label AS CHARACTER FORMAT "X(256)":U INITIAL "Activity" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

DEFINE VARIABLE t9Label AS CHARACTER FORMAT "X(256)":U INITIAL "Corrective Actions" 
      VIEW-AS TEXT 
     SIZE 134 BY .76
     FONT 5 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY tActYear1 FOR 
      reviewactivity SCROLLING.

DEFINE QUERY tActYear2 FOR 
      reviewactivity SCROLLING.

DEFINE QUERY tActYear3 FOR 
      reviewactivity SCROLLING.

DEFINE QUERY tAlert FOR 
      reviewalert SCROLLING.

DEFINE QUERY tClaims FOR 
      reviewclaim SCROLLING.

DEFINE QUERY tCorrective FOR 
      reviewcorrective SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE tActYear1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS tActYear1 C-Win _FREEFORM
  QUERY tActYear1 DISPLAY
      reviewactivity.categoryDesc width 20
      reviewactivity.typeDesc width 20
reviewactivity.qtr1 width 16
reviewactivity.qtr2 width 16
reviewactivity.qtr3 width 16
reviewactivity.qtr4  width 16
reviewactivity.yrTotal  width 17
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 131 BY 5.86
         TITLE "Year 1" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE tActYear2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS tActYear2 C-Win _FREEFORM
  QUERY tActYear2 DISPLAY
      reviewactivity.categoryDesc width 20
      reviewactivity.typeDesc width 20
reviewactivity.qtr1 width 16
reviewactivity.qtr2 width 16
reviewactivity.qtr3 width 16
reviewactivity.qtr4  width 16
reviewactivity.yrTotal  width 17
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 131 BY 5.86
         TITLE "Year 2" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE tActYear3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS tActYear3 C-Win _FREEFORM
  QUERY tActYear3 DISPLAY
      reviewactivity.categoryDesc width 20
      reviewactivity.typeDesc width 20
reviewactivity.qtr1 width 16
reviewactivity.qtr2 width 16
reviewactivity.qtr3 width 16
reviewactivity.qtr4  width 16
reviewactivity.yrTotal  width 17
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 131 BY 5.86
         TITLE "Year 3" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE tAlert
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS tAlert C-Win _FREEFORM
  QUERY tAlert DISPLAY
      reviewalert.processCodeDesc width 20
reviewalert.ownerDesc width 20
reviewalert.severityDesc width 20
reviewalert.thresholdRange width 20
reviewalert.scoreDesc width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 106 BY 3.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE tClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS tClaims C-Win _FREEFORM
  QUERY tClaims DISPLAY
      reviewclaim.claimID width 12
reviewclaim.statDesc width 12
reviewclaim.agentErrorDesc width 12
reviewclaim.description width 21
reviewclaim.lossPaid width 14
reviewclaim.laePaid width 14
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 94 BY 3.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE tCorrective
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS tCorrective C-Win _FREEFORM
  QUERY tCorrective DISPLAY
      reviewcorrective.statDesc width 12
reviewcorrective.uidDesc width 23
reviewcorrective.dateCreated width 13
reviewcorrective.dateDue width 13
reviewcorrective.dateComplete width 13
reviewcorrective.comment
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 106 BY 4.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.05
         SIZE 145 BY 27 WIDGET-ID 100.

DEFINE FRAME fDetails
     t1Expand AT ROW 1.19 COL 2.2 WIDGET-ID 8
     t2Expand AT ROW 2.43 COL 2.2 WIDGET-ID 14
     t3Expand AT ROW 3.62 COL 2.2 WIDGET-ID 18
     t4Expand AT ROW 4.81 COL 2.2 WIDGET-ID 22
     t9Expand AT ROW 10.76 COL 2.2 WIDGET-ID 42
     t8Expand AT ROW 9.57 COL 2.2 WIDGET-ID 38
     t7Expand AT ROW 8.38 COL 2.2 WIDGET-ID 34
     t6Expand AT ROW 7.19 COL 2.2 WIDGET-ID 30
     t5Expand AT ROW 6 COL 2.2 WIDGET-ID 26
     t1Label AT ROW 1.38 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     t2Label AT ROW 2.62 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     t3Label AT ROW 3.81 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     t4Label AT ROW 5 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     t5Label AT ROW 6.19 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 28
     t6Label AT ROW 7.38 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 32
     t7Label AT ROW 8.57 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     t8Label AT ROW 9.76 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     t9Label AT ROW 10.95 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 44
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 3.19
         SIZE 145 BY 24.76 WIDGET-ID 200.

DEFINE FRAME f4Details
     tARCurrent AT ROW 2.67 COL 3 NO-LABEL WIDGET-ID 4 NO-TAB-STOP 
     tAR31to60 AT ROW 2.67 COL 23 COLON-ALIGNED NO-LABEL WIDGET-ID 62 NO-TAB-STOP 
     tAR61to90 AT ROW 2.67 COL 45 COLON-ALIGNED NO-LABEL WIDGET-ID 64 NO-TAB-STOP 
     tAROver90 AT ROW 2.67 COL 67 COLON-ALIGNED NO-LABEL WIDGET-ID 66 NO-TAB-STOP 
     tARTotal AT ROW 2.67 COL 89 COLON-ALIGNED NO-LABEL WIDGET-ID 68 NO-TAB-STOP 
     tARCurrentLabel AT ROW 1.48 COL 3 NO-LABEL WIDGET-ID 34
     tAR31to60Label AT ROW 1.48 COL 23 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     tAR61to90Label AT ROW 1.48 COL 45 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     tAROver90Label AT ROW 1.48 COL 67 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     tARTotalLabel AT ROW 1.48 COL 89 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     RECT-64 AT ROW 2.38 COL 3 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 22.14
         SIZE 111 BY 2.91 WIDGET-ID 800.

DEFINE FRAME f9Details
     tCorrectivePlanned AT ROW 1.24 COL 16 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tCorrective AT ROW 1.24 COL 27 WIDGET-ID 600
     tCorrectiveOpen AT ROW 2.57 COL 16 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tCorrectiveClosed AT ROW 3.91 COL 16 COLON-ALIGNED WIDGET-ID 52 NO-TAB-STOP 
     tCorrectiveTotal AT ROW 5.19 COL 16 COLON-ALIGNED WIDGET-ID 34
     tCorrectiveQuestion AT ROW 6.48 COL 36.6 NO-LABEL WIDGET-ID 38
     tCorrectiveFinding AT ROW 8.71 COL 36.6 NO-LABEL WIDGET-ID 46
     tCorrectiveAction AT ROW 10.95 COL 36.6 NO-LABEL WIDGET-ID 48
     "Action:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 11.1 COL 29.2 WIDGET-ID 50
     "Question:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 6.62 COL 27 WIDGET-ID 40
     "Finding:" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 8.86 COL 28.6 WIDGET-ID 42
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 6
         SIZE 133 BY 12.14 WIDGET-ID 1800.

DEFINE FRAME f2Details
     tCRExpenses AT ROW 1.24 COL 16 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tClaims AT ROW 1.24 COL 39 WIDGET-ID 600
     tCRNet AT ROW 2.71 COL 16 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tCRRatio AT ROW 4.19 COL 16 COLON-ALIGNED WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 20
         SIZE 133 BY 4.52 WIDGET-ID 500.

DEFINE FRAME f8Details
     tActYear1 AT ROW 1.24 COL 2 WIDGET-ID 1500
     tActYear2 AT ROW 7.43 COL 2 WIDGET-ID 1600
     tActYear3 AT ROW 13.62 COL 2 WIDGET-ID 1700
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 6
         SIZE 133 BY 18.81 WIDGET-ID 1400.

DEFINE FRAME f6Details
     tPVLimit AT ROW 1.24 COL 16 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tPVMonth1 AT ROW 1.24 COL 44 COLON-ALIGNED NO-LABEL WIDGET-ID 60 NO-TAB-STOP 
     tPVThreeMonthAverage AT ROW 1.24 COL 83 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tPVAvailable AT ROW 2.43 COL 16 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tPVMonth2 AT ROW 2.43 COL 44 COLON-ALIGNED NO-LABEL WIDGET-ID 62 NO-TAB-STOP 
     tPVAvailableAverage AT ROW 2.43 COL 83 COLON-ALIGNED WIDGET-ID 58 DISABLE-AUTO-ZAP  NO-TAB-STOP 
     tPVUnremmited AT ROW 3.62 COL 16 COLON-ALIGNED WIDGET-ID 54 NO-TAB-STOP 
     tPVMonth3 AT ROW 3.62 COL 44 COLON-ALIGNED NO-LABEL WIDGET-ID 64 NO-TAB-STOP 
     tPVUnremittedAverage AT ROW 3.62 COL 83 COLON-ALIGNED WIDGET-ID 56 NO-TAB-STOP 
     tPVMonth1Label AT ROW 1.43 COL 44.6 RIGHT-ALIGNED NO-LABEL WIDGET-ID 66
     tPVMonth2Label AT ROW 2.62 COL 44.6 RIGHT-ALIGNED NO-LABEL WIDGET-ID 68
     tPVMonth3Label AT ROW 3.81 COL 44.6 RIGHT-ALIGNED NO-LABEL WIDGET-ID 70
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 20
         SIZE 104 BY 4 WIDGET-ID 1000.

DEFINE FRAME f7Details
     tCVMonth3 AT ROW 1.24 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 2 NO-TAB-STOP 
     tCVMonth2 AT ROW 1.24 COL 42 COLON-ALIGNED NO-LABEL WIDGET-ID 4 NO-TAB-STOP 
     tCVMonth1 AT ROW 1.24 COL 68 COLON-ALIGNED NO-LABEL WIDGET-ID 54 NO-TAB-STOP 
     tCVThreeMonthAverage AT ROW 2.43 COL 39 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tCVMonth3Label AT ROW 1.38 COL 16.4 RIGHT-ALIGNED NO-LABEL WIDGET-ID 56
     tCVMonth2Label AT ROW 1.38 COL 42.4 RIGHT-ALIGNED NO-LABEL WIDGET-ID 58
     tCVMonth1Label AT ROW 1.38 COL 68.4 RIGHT-ALIGNED NO-LABEL WIDGET-ID 60
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 20
         SIZE 80 BY 2.81 WIDGET-ID 1100.

DEFINE FRAME f3Details
     tRemitNet3 AT ROW 2.43 COL 16 COLON-ALIGNED WIDGET-ID 6
     tRemitNet2 AT ROW 2.43 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     tRemitNet1 AT ROW 2.43 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 30
     tRemitNetTotal AT ROW 2.43 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     tRemitGross3 AT ROW 3.62 COL 16 COLON-ALIGNED WIDGET-ID 8
     tRemitGross2 AT ROW 3.62 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     tRemitGross1 AT ROW 3.62 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 28
     tRemitGrossTotal AT ROW 3.62 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 38
     tRemitCPL3 AT ROW 4.81 COL 16 COLON-ALIGNED WIDGET-ID 10
     tRemitCPL2 AT ROW 4.81 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     tRemitCPL1 AT ROW 4.81 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 26
     tRemitCPLTotal AT ROW 4.81 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     tRemitYear3 AT ROW 1.24 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 4
     tRemitYear2 AT ROW 1.24 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 22
     tRemitYear1 AT ROW 1.24 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 32
     tRemitYearTotal AT ROW 1.24 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 46
     RECT-59 AT ROW 2.43 COL 38.8 WIDGET-ID 12
     RECT-60 AT ROW 2.14 COL 18 WIDGET-ID 14
     RECT-61 AT ROW 2.43 COL 60.8 WIDGET-ID 24
     RECT-62 AT ROW 2.43 COL 82.8 WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 20
         SIZE 104 BY 5.05 WIDGET-ID 700.

DEFINE FRAME f5Details
     tAlertWarning AT ROW 1.24 COL 16 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tAlert AT ROW 1.24 COL 27 WIDGET-ID 600
     tAlertCritical AT ROW 2.71 COL 16 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tAlertTotal AT ROW 4.19 COL 16 COLON-ALIGNED WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 20.52
         SIZE 133 BY 4.52 WIDGET-ID 900.

DEFINE FRAME f1Details
     tQARDate3 AT ROW 2.43 COL 16 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tQARDate2 AT ROW 2.43 COL 38.2 COLON-ALIGNED NO-LABEL WIDGET-ID 22 NO-TAB-STOP 
     tQARDate1 AT ROW 2.43 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 28 NO-TAB-STOP 
     tQARScore3 AT ROW 3.62 COL 16 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tQARScore2 AT ROW 3.62 COL 38.2 COLON-ALIGNED NO-LABEL WIDGET-ID 20 NO-TAB-STOP 
     tQARScore1 AT ROW 3.62 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 32 NO-TAB-STOP 
     tQARScoreAvg AT ROW 3.62 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 40 NO-TAB-STOP 
     tERRScore3 AT ROW 4.81 COL 16 COLON-ALIGNED WIDGET-ID 58 NO-TAB-STOP 
     tERRScore2 AT ROW 4.81 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 56 NO-TAB-STOP 
     tERRScore1 AT ROW 4.81 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 54 NO-TAB-STOP 
     tERRScoreAvg AT ROW 4.81 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 60 NO-TAB-STOP 
     tQARYear3 AT ROW 1.24 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 34
     tQARYear2 AT ROW 1.24 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     tQARYear1 AT ROW 1.24 COL 60 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     tQARYearAvg AT ROW 1.24 COL 82 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     RECT-53 AT ROW 2.43 COL 38.8 WIDGET-ID 14
     RECT-54 AT ROW 2.43 COL 60.6 WIDGET-ID 26
     RECT-57 AT ROW 2.43 COL 82.8 WIDGET-ID 36
     RECT-63 AT ROW 2.14 COL 18 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 20
         SIZE 104 BY 5.05 WIDGET-ID 300.

DEFINE FRAME fAgent
     bCollapseAll AT ROW 1.24 COL 9.2 WIDGET-ID 12 NO-TAB-STOP 
     bExpandAll AT ROW 1.24 COL 2 WIDGET-ID 14 NO-TAB-STOP 
     bRefresh AT ROW 1.24 COL 16.4 WIDGET-ID 8 NO-TAB-STOP 
     tAgent AT ROW 1.1 COL 32 COLON-ALIGNED WIDGET-ID 16
     bPrint AT ROW 1.24 COL 136.6 WIDGET-ID 10
     tStateID AT ROW 2.19 COL 32 COLON-ALIGNED WIDGET-ID 2
     tManager AT ROW 2.19 COL 63 COLON-ALIGNED WIDGET-ID 4
     tContract AT ROW 2.19 COL 109 COLON-ALIGNED WIDGET-ID 6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 144.8 BY 2.38 WIDGET-ID 1300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Summary"
         HEIGHT             = 27.05
         WIDTH              = 145
         MAX-HEIGHT         = 31.1
         MAX-WIDTH          = 145
         VIRTUAL-HEIGHT     = 31.1
         VIRTUAL-WIDTH      = 145
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME f1Details:FRAME = FRAME fDetails:HANDLE
       FRAME f2Details:FRAME = FRAME fDetails:HANDLE
       FRAME f3Details:FRAME = FRAME fDetails:HANDLE
       FRAME f4Details:FRAME = FRAME fDetails:HANDLE
       FRAME f5Details:FRAME = FRAME fDetails:HANDLE
       FRAME f6Details:FRAME = FRAME fDetails:HANDLE
       FRAME f7Details:FRAME = FRAME fDetails:HANDLE
       FRAME f8Details:FRAME = FRAME fDetails:HANDLE
       FRAME f9Details:FRAME = FRAME fDetails:HANDLE
       FRAME fAgent:FRAME = FRAME fMain:HANDLE
       FRAME fDetails:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME f1Details
                                                                        */
ASSIGN 
       tERRScore1:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tERRScore2:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tERRScore3:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tERRScoreAvg:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARDate1:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARDate2:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARDate3:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARScore1:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARScore2:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARScore3:READ-ONLY IN FRAME f1Details        = TRUE.

ASSIGN 
       tQARScoreAvg:READ-ONLY IN FRAME f1Details        = TRUE.

/* SETTINGS FOR FRAME f2Details
                                                                        */
/* BROWSE-TAB tClaims tCRExpenses f2Details */
ASSIGN 
       tClaims:COLUMN-RESIZABLE IN FRAME f2Details       = TRUE.

ASSIGN 
       tCRExpenses:READ-ONLY IN FRAME f2Details        = TRUE.

ASSIGN 
       tCRNet:READ-ONLY IN FRAME f2Details        = TRUE.

ASSIGN 
       tCRRatio:READ-ONLY IN FRAME f2Details        = TRUE.

/* SETTINGS FOR FRAME f3Details
                                                                        */
ASSIGN 
       tRemitCPL1:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitCPL2:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitCPL3:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitCPLTotal:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitGross1:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitGross2:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitGross3:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitGrossTotal:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitNet1:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitNet2:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitNet3:READ-ONLY IN FRAME f3Details        = TRUE.

ASSIGN 
       tRemitNetTotal:READ-ONLY IN FRAME f3Details        = TRUE.

/* SETTINGS FOR FRAME f4Details
                                                                        */
ASSIGN 
       tAR31to60:READ-ONLY IN FRAME f4Details        = TRUE.

ASSIGN 
       tAR61to90:READ-ONLY IN FRAME f4Details        = TRUE.

/* SETTINGS FOR FILL-IN tARCurrent IN FRAME f4Details
   ALIGN-L                                                              */
ASSIGN 
       tARCurrent:READ-ONLY IN FRAME f4Details        = TRUE.

/* SETTINGS FOR FILL-IN tARCurrentLabel IN FRAME f4Details
   ALIGN-L                                                              */
ASSIGN 
       tAROver90:READ-ONLY IN FRAME f4Details        = TRUE.

ASSIGN 
       tARTotal:READ-ONLY IN FRAME f4Details        = TRUE.

/* SETTINGS FOR FRAME f5Details
                                                                        */
/* BROWSE-TAB tAlert tAlertWarning f5Details */
ASSIGN 
       tAlert:COLUMN-RESIZABLE IN FRAME f5Details       = TRUE.

ASSIGN 
       tAlertCritical:READ-ONLY IN FRAME f5Details        = TRUE.

ASSIGN 
       tAlertTotal:READ-ONLY IN FRAME f5Details        = TRUE.

ASSIGN 
       tAlertWarning:READ-ONLY IN FRAME f5Details        = TRUE.

/* SETTINGS FOR FRAME f6Details
                                                                        */
ASSIGN 
       tPVAvailable:READ-ONLY IN FRAME f6Details        = TRUE.

ASSIGN 
       tPVAvailableAverage:READ-ONLY IN FRAME f6Details        = TRUE.

ASSIGN 
       tPVLimit:READ-ONLY IN FRAME f6Details        = TRUE.

ASSIGN 
       tPVMonth1:READ-ONLY IN FRAME f6Details        = TRUE.

/* SETTINGS FOR FILL-IN tPVMonth1Label IN FRAME f6Details
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       tPVMonth2:READ-ONLY IN FRAME f6Details        = TRUE.

/* SETTINGS FOR FILL-IN tPVMonth2Label IN FRAME f6Details
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       tPVMonth3:READ-ONLY IN FRAME f6Details        = TRUE.

/* SETTINGS FOR FILL-IN tPVMonth3Label IN FRAME f6Details
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       tPVThreeMonthAverage:READ-ONLY IN FRAME f6Details        = TRUE.

ASSIGN 
       tPVUnremittedAverage:READ-ONLY IN FRAME f6Details        = TRUE.

ASSIGN 
       tPVUnremmited:READ-ONLY IN FRAME f6Details        = TRUE.

/* SETTINGS FOR FRAME f7Details
                                                                        */
ASSIGN 
       tCVMonth1:READ-ONLY IN FRAME f7Details        = TRUE.

/* SETTINGS FOR FILL-IN tCVMonth1Label IN FRAME f7Details
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       tCVMonth2:READ-ONLY IN FRAME f7Details        = TRUE.

/* SETTINGS FOR FILL-IN tCVMonth2Label IN FRAME f7Details
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       tCVMonth3:READ-ONLY IN FRAME f7Details        = TRUE.

/* SETTINGS FOR FILL-IN tCVMonth3Label IN FRAME f7Details
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       tCVThreeMonthAverage:READ-ONLY IN FRAME f7Details        = TRUE.

/* SETTINGS FOR FRAME f8Details
                                                                        */
/* BROWSE-TAB tActYear1 1 f8Details */
/* BROWSE-TAB tActYear2 tActYear1 f8Details */
/* BROWSE-TAB tActYear3 tActYear2 f8Details */
ASSIGN 
       tActYear1:COLUMN-RESIZABLE IN FRAME f8Details       = TRUE.

ASSIGN 
       tActYear2:COLUMN-RESIZABLE IN FRAME f8Details       = TRUE.

ASSIGN 
       tActYear3:COLUMN-RESIZABLE IN FRAME f8Details       = TRUE.

/* SETTINGS FOR FRAME f9Details
                                                                        */
/* BROWSE-TAB tCorrective tCorrectivePlanned f9Details */
ASSIGN 
       tCorrective:COLUMN-RESIZABLE IN FRAME f9Details       = TRUE.

ASSIGN 
       tCorrectiveAction:READ-ONLY IN FRAME f9Details        = TRUE.

ASSIGN 
       tCorrectiveClosed:READ-ONLY IN FRAME f9Details        = TRUE.

ASSIGN 
       tCorrectiveFinding:READ-ONLY IN FRAME f9Details        = TRUE.

ASSIGN 
       tCorrectiveOpen:READ-ONLY IN FRAME f9Details        = TRUE.

ASSIGN 
       tCorrectivePlanned:READ-ONLY IN FRAME f9Details        = TRUE.

ASSIGN 
       tCorrectiveQuestion:READ-ONLY IN FRAME f9Details        = TRUE.

ASSIGN 
       tCorrectiveTotal:READ-ONLY IN FRAME f9Details        = TRUE.

/* SETTINGS FOR FRAME fAgent
                                                                        */
/* SETTINGS FOR BUTTON bExpandAll IN FRAME fAgent
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrint IN FRAME fAgent
   NO-ENABLE                                                            */
ASSIGN 
       bPrint:HIDDEN IN FRAME fAgent           = TRUE.

/* SETTINGS FOR BUTTON bRefresh IN FRAME fAgent
   NO-ENABLE                                                            */
ASSIGN 
       tAgent:READ-ONLY IN FRAME fAgent        = TRUE.

ASSIGN 
       tContract:READ-ONLY IN FRAME fAgent        = TRUE.

ASSIGN 
       tManager:READ-ONLY IN FRAME fAgent        = TRUE.

ASSIGN 
       tStateID:READ-ONLY IN FRAME fAgent        = TRUE.

/* SETTINGS FOR FRAME fDetails
                                                                        */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME f5Details:MOVE-BEFORE-TAB-ITEM (FRAME f4Details:HANDLE)
       XXTABVALXX = FRAME f6Details:MOVE-BEFORE-TAB-ITEM (FRAME f5Details:HANDLE)
       XXTABVALXX = FRAME f7Details:MOVE-BEFORE-TAB-ITEM (FRAME f6Details:HANDLE)
       XXTABVALXX = FRAME f1Details:MOVE-BEFORE-TAB-ITEM (FRAME f7Details:HANDLE)
       XXTABVALXX = FRAME f3Details:MOVE-BEFORE-TAB-ITEM (FRAME f1Details:HANDLE)
       XXTABVALXX = FRAME f2Details:MOVE-BEFORE-TAB-ITEM (FRAME f3Details:HANDLE)
       XXTABVALXX = FRAME f9Details:MOVE-BEFORE-TAB-ITEM (FRAME f2Details:HANDLE)
       XXTABVALXX = FRAME f8Details:MOVE-BEFORE-TAB-ITEM (FRAME f9Details:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN XXTABVALXX = FRAME fAgent:MOVE-BEFORE-TAB-ITEM (FRAME fDetails:HANDLE)
/* END-ASSIGN-TABS */.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE tActYear1
/* Query rebuild information for BROWSE tActYear1
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reviewactivity where year = year(today) - 2.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE tActYear1 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE tActYear2
/* Query rebuild information for BROWSE tActYear2
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reviewactivity where year = year(today) - 1.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE tActYear2 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE tActYear3
/* Query rebuild information for BROWSE tActYear3
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reviewactivity where year = year(today).
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE tActYear3 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE tAlert
/* Query rebuild information for BROWSE tAlert
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reviewalert where severity > 0.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE tAlert */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE tClaims
/* Query rebuild information for BROWSE tClaims
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reviewclaim where laePaid > 0 or lossPaid > 0.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE tClaims */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE tCorrective
/* Query rebuild information for BROWSE tCorrective
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH reviewcorrective.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE tCorrective */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Summary */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Summary */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Summary */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fAgent
&Scoped-define SELF-NAME bCollapseAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCollapseAll C-Win
ON CHOOSE OF bCollapseAll IN FRAME fAgent /* Collapse */
DO:
  define variable iCnt as integer no-undo.
  do iCnt = 1 to iTotalSection:
    if can-find(first wt where wt.id = iCnt and wt.isOpen)
     then run toggleSection (iCnt, true).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExpandAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExpandAll C-Win
ON CHOOSE OF bExpandAll IN FRAME fAgent /* Expand */
DO:
  define variable iCnt as integer no-undo.
  do iCnt = 1 to iTotalSection:
    if not can-find(first wt where wt.id = iCnt and wt.isOpen)
     then run toggleSection (iCnt, false).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fAgent /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fAgent /* Refresh */
DO:
  clearData().
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME tActYear1
&Scoped-define FRAME-NAME f8Details
&Scoped-define SELF-NAME tActYear1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActYear1 C-Win
ON ROW-DISPLAY OF tActYear1 IN FRAME f8Details /* Year 1 */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActYear1 C-Win
ON START-SEARCH OF tActYear1 IN FRAME f8Details /* Year 1 */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME tActYear2
&Scoped-define SELF-NAME tActYear2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActYear2 C-Win
ON ROW-DISPLAY OF tActYear2 IN FRAME f8Details /* Year 2 */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActYear2 C-Win
ON START-SEARCH OF tActYear2 IN FRAME f8Details /* Year 2 */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME tActYear3
&Scoped-define SELF-NAME tActYear3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActYear3 C-Win
ON ROW-DISPLAY OF tActYear3 IN FRAME f8Details /* Year 3 */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActYear3 C-Win
ON START-SEARCH OF tActYear3 IN FRAME f8Details /* Year 3 */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME tAlert
&Scoped-define FRAME-NAME f5Details
&Scoped-define SELF-NAME tAlert
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAlert C-Win
ON DEFAULT-ACTION OF tAlert IN FRAME f5Details
DO:
  run wamd01-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAlert C-Win
ON ROW-DISPLAY OF tAlert IN FRAME f5Details
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAlert C-Win
ON START-SEARCH OF tAlert IN FRAME f5Details
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME tClaims
&Scoped-define FRAME-NAME f2Details
&Scoped-define SELF-NAME tClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tClaims C-Win
ON DEFAULT-ACTION OF tClaims IN FRAME f2Details
DO:
  run rpt/claimsbyagent.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tClaims C-Win
ON ROW-DISPLAY OF tClaims IN FRAME f2Details
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tClaims C-Win
ON START-SEARCH OF tClaims IN FRAME f2Details
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME tCorrective
&Scoped-define FRAME-NAME f9Details
&Scoped-define SELF-NAME tCorrective
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCorrective C-Win
ON ROW-DISPLAY OF tCorrective IN FRAME f9Details
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCorrective C-Win
ON START-SEARCH OF tCorrective IN FRAME f9Details
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCorrective C-Win
ON VALUE-CHANGED OF tCorrective IN FRAME f9Details
DO:
  if not available reviewcorrective
   then return.
   
  do with frame f9Details:
    assign
      tCorrectiveQuestion = reviewcorrective.q
      tCorrectiveFinding = reviewcorrective.f
      tCorrectiveAction = reviewcorrective.a
      .
    display
      tCorrectiveQuestion
      tCorrectiveFinding
      tCorrectiveAction
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME tActYear1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main-multi.i &browse-list="tClaims,tAlert,tAR,tActYear1,tActYear2,tActYear3,tCorrective" &parentFrame="fDetails"}
{lib/win-main.i}
{lib/win-status.i}
{&window-name}:window-state = 2.

/* the triggers need to be defined here as they mess up ABL */
ON CHOOSE OF t1Expand  IN FRAME fDetails or
   CHOOSE OF t2Expand  IN FRAME fDetails or
   CHOOSE OF t3Expand  IN FRAME fDetails or
   CHOOSE OF t4Expand  IN FRAME fDetails or
   CHOOSE OF t5Expand  IN FRAME fDetails or
   CHOOSE OF t6Expand  IN FRAME fDetails or
   CHOOSE OF t7Expand  IN FRAME fDetails or
   CHOOSE OF t8Expand  IN FRAME fDetails or
   CHOOSE OF t9Expand  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t1Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t2Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t3Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t4Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t5Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t6Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t7Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t8Label  IN FRAME fDetails or
   MOUSE-SELECT-CLICK OF t9Label  IN FRAME fDetails
DO:
  std-in = integer(substring(self:name, 2, 1)).
  std-lo = can-find(first wt where id = std-in and isOpen).
  run toggleSection in this-procedure (std-in, std-lo).
END.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

{lib/set-button.i &label="Refresh"     &frame-name=fAgent &toggle=false}
{lib/set-button.i &label="Print"       &frame-name=fAgent &toggle=false}
{lib/set-button.i &label="ExpandAll"   &frame-name=fAgent &toggle=false &image="images/expand.bmp"   &inactive="images/expand-i.bmp"}
{lib/set-button.i &label="CollapseAll" &frame-name=fAgent &toggle=false &image="images/collapse.bmp" &inactive="images/collapse-i.bmp"}
setButtons().

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* count the sections */
std-lo = true.
std-in = 1.
repeat while std-lo:
  std-ha = getWidgetByName(frame fDetails:handle, "t" + string(std-in) + "Expand").
  if valid-handle(std-ha)
   then iTotalSection = iTotalSection + 1.
   else std-lo = false.
   
  std-in = std-in + 1.
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-column-width.i &col="'description'" &var=dClaims &browse-name=tClaims}
  {lib/get-column-width.i &col="'processCodeDesc'" &var=dAlert &browse-name=tAlert}
  
  /* set the details frame to a rediculous amount so we can build the details */
  frame fDetails:virtual-height-chars = 100.
  /* can't do this all in one go as we need to */
  /* load the table then toggle the sections */
  do std-in = 1 to iTotalSection:
    addSection(std-in).
  end.
  clearData().
  run windowResized in this-procedure.

  /* set the manager, state, and contra */
  if valid-handle(hFileDataSrv)
   then
    do:
      run GetAgent in hFileDataSrv (output table agent).
      run GetAgentManagers in hFileDataSrv (output table agentmanager).
      for first agent no-lock:
        std-ch = "None".
        for first agentmanager no-lock
            where agentmanager.agentID = agent.agentID
              and agentmanager.isPrimary:
              
          publish "GetSysUserName" (agentmanager.uid, output std-ch).
        end.
        assign
          {&window-name}:TITLE = "Agent Summary for " + agent.name
          tAgent:SCREEN-VALUE IN FRAME fAgent = agent.NAME
          tStateID:screen-value in frame fAgent = getStateName(agent.stateID)
          tManager:screen-value in frame fAgent = std-ch
          tContract:screen-value in frame fAgent = string(agent.contractDate)
          pAgentID = agent.agentID
          .
      end.
    end.
  
  /* das: incomplete implementation of GetAutoView configuration option--to be done
  std-lo = false.
  publish "GetAutoView" (output std-lo).
  if valid-handle(hFileDataSrv) and std-lo
   then run getData in this-procedure.
    */
  {&window-name}:window-state = 3.

  if valid-handle(hFileDataSrv)
     then run getData in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAgent tStateID tManager tContract 
      WITH FRAME fAgent IN WINDOW C-Win.
  ENABLE bCollapseAll tAgent tStateID tManager tContract 
      WITH FRAME fAgent IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fAgent}
  VIEW FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY t1Label t2Label t3Label t4Label t5Label t6Label t7Label t8Label 
          t9Label 
      WITH FRAME fDetails IN WINDOW C-Win.
  ENABLE t1Expand t2Expand t3Expand t4Expand t9Expand t8Expand t7Expand 
         t6Expand t5Expand t1Label t2Label t3Label t4Label t5Label t6Label 
         t7Label t8Label t9Label 
      WITH FRAME fDetails IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDetails}
  DISPLAY tCorrectivePlanned tCorrectiveOpen tCorrectiveClosed tCorrectiveTotal 
          tCorrectiveQuestion tCorrectiveFinding tCorrectiveAction 
      WITH FRAME f9Details IN WINDOW C-Win.
  ENABLE tCorrectivePlanned tCorrective tCorrectiveOpen tCorrectiveClosed 
         tCorrectiveTotal tCorrectiveQuestion tCorrectiveFinding 
         tCorrectiveAction 
      WITH FRAME f9Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f9Details}
  ENABLE tActYear1 tActYear2 tActYear3 
      WITH FRAME f8Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f8Details}
  DISPLAY tPVLimit tPVMonth1 tPVThreeMonthAverage tPVAvailable tPVMonth2 
          tPVAvailableAverage tPVUnremmited tPVMonth3 tPVUnremittedAverage 
          tPVMonth1Label tPVMonth2Label tPVMonth3Label 
      WITH FRAME f6Details IN WINDOW C-Win.
  ENABLE tPVLimit tPVMonth1 tPVThreeMonthAverage tPVAvailable tPVMonth2 
         tPVAvailableAverage tPVUnremmited tPVMonth3 tPVUnremittedAverage 
      WITH FRAME f6Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f6Details}
  DISPLAY tCVMonth3 tCVMonth2 tCVMonth1 tCVThreeMonthAverage tCVMonth3Label 
          tCVMonth2Label tCVMonth1Label 
      WITH FRAME f7Details IN WINDOW C-Win.
  ENABLE tCVMonth3 tCVMonth2 tCVMonth1 tCVThreeMonthAverage 
      WITH FRAME f7Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f7Details}
  DISPLAY tRemitNet3 tRemitNet2 tRemitNet1 tRemitNetTotal tRemitGross3 
          tRemitGross2 tRemitGross1 tRemitGrossTotal tRemitCPL3 tRemitCPL2 
          tRemitCPL1 tRemitCPLTotal tRemitYear3 tRemitYear2 tRemitYear1 
          tRemitYearTotal 
      WITH FRAME f3Details IN WINDOW C-Win.
  ENABLE RECT-59 RECT-60 RECT-61 RECT-62 tRemitNet3 tRemitNet2 tRemitNet1 
         tRemitNetTotal tRemitGross3 tRemitGross2 tRemitGross1 tRemitGrossTotal 
         tRemitCPL3 tRemitCPL2 tRemitCPL1 tRemitCPLTotal tRemitYear3 
         tRemitYear2 tRemitYear1 tRemitYearTotal 
      WITH FRAME f3Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f3Details}
  DISPLAY tCRExpenses tCRNet tCRRatio 
      WITH FRAME f2Details IN WINDOW C-Win.
  ENABLE tCRExpenses tClaims tCRNet tCRRatio 
      WITH FRAME f2Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f2Details}
  DISPLAY tQARDate3 tQARDate2 tQARDate1 tQARScore3 tQARScore2 tQARScore1 
          tQARScoreAvg tERRScore3 tERRScore2 tERRScore1 tERRScoreAvg tQARYear3 
          tQARYear2 tQARYear1 tQARYearAvg 
      WITH FRAME f1Details IN WINDOW C-Win.
  ENABLE RECT-53 RECT-54 RECT-57 RECT-63 tQARDate3 tQARDate2 tQARDate1 
         tQARScore3 tQARScore2 tQARScore1 tQARScoreAvg tERRScore3 tERRScore2 
         tERRScore1 tERRScoreAvg tQARYear3 tQARYear2 tQARYear1 tQARYearAvg 
      WITH FRAME f1Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f1Details}
  DISPLAY tAlertWarning tAlertCritical tAlertTotal 
      WITH FRAME f5Details IN WINDOW C-Win.
  ENABLE tAlertWarning tAlert tAlertCritical tAlertTotal 
      WITH FRAME f5Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f5Details}
  DISPLAY tARCurrent tAR31to60 tAR61to90 tAROver90 tARTotal tARCurrentLabel 
          tAR31to60Label tAR61to90Label tAROver90Label tARTotalLabel 
      WITH FRAME f4Details IN WINDOW C-Win.
  ENABLE RECT-64 tARCurrent tAR31to60 tAR61to90 tAROver90 tARTotal 
         tARCurrentLabel tAR31to60Label tAR61to90Label tAROver90Label 
         tARTotalLabel 
      WITH FRAME f4Details IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-f4Details}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  setStatus("").
  enableButtons(FALSE).
  
  DisableNotResponding().
 
  {lib/pbshow.i "''"}
  run getDataQAR in this-procedure (pAgentID, 9).
   
  run getDataClaims in this-procedure (pAgentID, 18).
   
  run getDataRemittances in this-procedure (pAgentID, 27).
   
  run getDataReceivables in this-procedure (pAgentID, 36).
   
  run getDataAlerts in this-procedure (pAgentID, 45).
   
  run getDataCPL in this-procedure (pAgentID, 54).
   
  run getDataPolicy in this-procedure (pAgentID, 63).
  
  run getDataActivity in this-procedure (pAgentID, 72).
   
  run getDataCorrective in this-procedure (pAgentID, 81).
   
  run getDataLicensing in this-procedure (pAgentID, 90).
   
  {lib/pbupdate.i "'Completing Agent Review'" 100}
  enableButtons(true).

  setStatus("Information as of " + string(now,"99/99/9999 HH:MM:SS")).
  displayStatus().
  
  {lib/pbhide.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataActivity C-Win 
PROCEDURE getDataActivity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  define variable hBrowse as handle no-undo.
  define variable iYear1 as integer no-undo.
  iYear1 = year(today) - 2.
  
  for first wt exclusive-lock
      where wt.id = 8:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/reportagentactivity.p (input 0,
                                      input "ALL",
                                      input pAgentID,
                                      input "ALL",
                                      output table reviewactivity,
                                      output std-lo,
                                      output std-ch).
                                     
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f8Details:
        std-ha = GetWidgetByName(frame fDetails:handle, "f" + string(wt.id) + "Details").
        for each reviewactivity exclusive-lock
           where reviewactivity.agentID = pAgentID
        break by reviewactivity.year:
          
          if first-of(reviewactivity.year)
           then std-in = 0.
          
          assign
            /* set the data to 0 if past the current month */
            std-lo = (reviewactivity.year = year(today))
            reviewactivity.month1  = (if std-lo and 1  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month1)
            reviewactivity.month2  = (if std-lo and 2  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month2)
            reviewactivity.month3  = (if std-lo and 3  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month3)
            reviewactivity.month4  = (if std-lo and 4  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month4)
            reviewactivity.month5  = (if std-lo and 5  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month5)
            reviewactivity.month6  = (if std-lo and 6  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month6)
            reviewactivity.month7  = (if std-lo and 7  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month7)
            reviewactivity.month8  = (if std-lo and 8  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month8)
            reviewactivity.month9  = (if std-lo and 9  > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month9)
            reviewactivity.month10 = (if std-lo and 10 > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month10)
            reviewactivity.month11 = (if std-lo and 11 > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month11)
            reviewactivity.month12 = (if std-lo and 12 > month(today) and reviewactivity.type <> "P" then 0 else reviewactivity.month12)
            /* calculate the totals */
            reviewactivity.qtr1 = truncate(reviewactivity.month1,0) + truncate(reviewactivity.month2,0) + truncate(reviewactivity.month3,0)
            reviewactivity.qtr2 = truncate(reviewactivity.month4,0) + truncate(reviewactivity.month5,0) + truncate(reviewactivity.month6,0)
            reviewactivity.qtr3 = truncate(reviewactivity.month7,0) + truncate(reviewactivity.month8,0) + truncate(reviewactivity.month9,0)
            reviewactivity.qtr4 = truncate(reviewactivity.month10,0) + truncate(reviewactivity.month11,0) + truncate(reviewactivity.month12,0)
            reviewactivity.yrTotal = reviewactivity.qtr1 + reviewactivity.qtr2 + reviewactivity.qtr3 + reviewactivity.qtr4
            .
          /* set the category */
          publish "GetSysPropDesc" ("AMD", "Activity", "Category", reviewactivity.category, output reviewactivity.categoryDesc).
          /* set the type */
          publish "GetSysPropDesc" ("AMD", "Activity", "Type", reviewactivity.type, output reviewactivity.typeDesc).
          if reviewactivity.categoryDesc = ""
           then delete reviewactivity.
           else std-in = std-in + 1.
          
          if last-of(reviewactivity.year)
           then
            do:
              hBrowse = GetWidgetByName(std-ha, "tActYear" + string(reviewactivity.year - iYear1 + 1)).
              if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
               then hBrowse:title = string(reviewactivity.year) + " (" + string(std-in) + " Found)".
            end.
        end.
        {&OPEN-BROWSERS-IN-QUERY-f8Details}
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataAlerts C-Win 
PROCEDURE getDataAlerts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 5:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentalerts.p (input 0,
                                 input "",
                                 input pAgentID,
                                 input "",
                                 input "O",
                                 output table reviewalert,
                                 output std-lo,
                                 output std-ch).
    
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f5Details:
        assign
          tAlertWarning = 0
          tAlertCritical = 0
          tAlertTotal = 0
          .
        for each reviewalert exclusive-lock
           where reviewalert.agentID = pAgentID
             and reviewalert.stat = "O":
          
          if reviewalert.severity = 1
           then tAlertWarning = tAlertWarning + 1.
          
          if reviewalert.severity = 2
           then tAlertCritical = tAlertCritical + 1.
          
          if reviewalert.severity > 0
           then tAlertTotal = tAlertTotal + 1.
           
          /* set the alert type */
          publish "GetSysCodeDesc" ("Alert", reviewalert.processCode, output reviewalert.processCodeDesc).
          /* set the severity */
          publish "GetSysPropDesc" ("AMD", "Alert", "Severity", reviewalert.severity, output reviewalert.severityDesc).
          /* set the owner */
          publish "GetSysPropDesc" ("AMD", "Alert", "Owner", reviewalert.owner, output reviewalert.ownerDesc).
        end.
        display
          tAlertWarning
          tAlertCritical
          tAlertTotal
          .
        {&OPEN-BROWSERS-IN-QUERY-f5Details}
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataClaims C-Win 
PROCEDURE getDataClaims :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 2:
      
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentreviewclaim.p (input pAgentID,
                                      output table reviewclaim,
                                      output std-lo,
                                      output std-ch).
                                      
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f2Details:
        assign
          std-in = 0
          tCRNet = 0
          tCRExpenses = 0
          tCRRatio = 0
          .
        for each reviewclaim exclusive-lock
           where reviewclaim.agentID = pAgentID:
          
          /* get the status */
          publish "GetSysPropDesc" ("CLM", "ClaimDescription", "Status", reviewclaim.stat, output reviewclaim.statDesc).
          /* get the agent error */
          publish "GetSysPropDesc" ("CLM", "ClaimDescription", "AgentError", reviewclaim.agentError, output reviewclaim.agentErrorDesc).
          assign
            std-in = std-in + 1
            /* get the premium */
            tCRNet = reviewclaim.netPremium
            /* sum the expenses */
            tCRExpenses = tCRExpenses +
                         (reviewclaim.laePaid +
                          reviewclaim.laeReserve +
                          reviewclaim.lossPaid +
                          reviewclaim.lossReserve -
                          reviewclaim.recoveries)
            .
        end.
        if tCRNet <= 0 or tCRExpenses <= 0
         then tCRRatio = 0.
         else tCRRatio = (tCRExpenses / tCRNet) * 100.
        display
          tCRExpenses
          tCRNet
          tCRRatio
          .
        std-ha = GetWidgetByName(frame fDetails:handle, "t" + string(wt.id) + "Label").
        std-ha:screen-value = std-ha:screen-value + " (" + string(std-in) + " Found)".
        {&OPEN-BROWSERS-IN-QUERY-f2Details}
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataCorrective C-Win 
PROCEDURE getDataCorrective :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 9:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentreviewcorrective.p (input pAgentID,
                                           output table reviewcorrective,
                                           output std-lo,
                                           output std-ch).
                                           
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f9Details:
        assign
          tCorrectivePlanned = 0
          tCorrectiveOpen = 0
          tCorrectiveClosed = 0
          tCorrectiveTotal = 0
          .
        for each reviewcorrective exclusive-lock
           where reviewcorrective.agentID = pAgentID:
          
          tCorrectiveTotal = tCorrectiveTotal + 1.
          case reviewcorrective.stat:
           when "P" then tCorrectivePlanned = tCorrectivePlanned + 1.
           when "O" then tCorrectiveOpen = tCorrectiveOpen + 1.
           when "C" then tCorrectiveClosed = tCorrectiveClosed + 1.
          end case.
          
          /* set the status */
          publish "GetSysPropDesc" ("CAM", "Action", "Status", reviewcorrective.stat, output reviewcorrective.statDesc).
          /* set the owner */
          publish "GetSysUserName" (reviewcorrective.uid, output reviewcorrective.uidDesc).
        end.
        display
          tCorrectivePlanned
          tCorrectiveOpen
          tCorrectiveClosed
          tCorrectiveTotal
          .
        {&OPEN-BROWSERS-IN-QUERY-f9Details}
        apply "VALUE-CHANGED" to tCorrective.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataCPL C-Win 
PROCEDURE getDataCPL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 7:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentcplvolume.p (input "",
                                    input pAgentID,
                                    output table reviewcpl,
                                    output std-lo,
                                    output std-ch).
                                    
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f7Details:
        std-ha = GetWidgetByName(frame fDetails:handle, "f" + string(wt.id) + "Details").
        for first reviewcpl no-lock
            where reviewcpl.agentID = pAgentID:
          
          assign
            std-da = today
            tCVMonth1 = reviewcpl.month1
            tCVMonth2 = reviewcpl.month2
            tCVMonth3 = reviewcpl.month3
            tCVThreeMonthAverage = reviewcpl.threeMonthAverage
            tCVMonth1Label = entry(month(add-interval(std-da, -1, "month")), {&months}) + ":"
            tCVMonth2Label = entry(month(add-interval(std-da, -2, "month")), {&months}) + ":"
            tCVMonth3Label = entry(month(add-interval(std-da, -3, "month")), {&months}) + ":"
            .
        end.
        display
          tCVMonth1
          tCVMonth2
          tCVMonth3
          tCVThreeMonthAverage
          tCVMonth1Label
          tCVMonth2Label
          tCVMonth3Label
          .
        rightAlignText(tCVMonth1Label:handle).
        rightAlignText(tCVMonth2Label:handle).
        rightAlignText(tCVMonth3Label:handle).
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataLicensing C-Win 
PROCEDURE getDataLicensing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 10:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do:
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataPolicy C-Win 
PROCEDURE getDataPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 6:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentpolicyvolume.p (input "",
                                       input pAgentID,
                                       output table reviewpolicy,
                                       output std-lo,
                                       output std-ch).
                                       
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f6Details:
        for first reviewpolicy no-lock
            where reviewpolicy.agentID = pAgentID:
          
            assign
              std-da = today
              tPVLimit = reviewpolicy.policyLimit
              tPVUnremmited = reviewpolicy.unremitted
              tPVAvailable = reviewpolicy.policiesAvailable
              tPVMonth1 = reviewpolicy.month1
              tPVMonth2 = reviewpolicy.month2
              tPVMonth3 = reviewpolicy.month3
              tPVThreeMonthAverage = reviewpolicy.threeMonthAverage
              tPVUnremittedAverage = reviewpolicy.unremittedAvgRatio
              tPVAvailableAverage = reviewpolicy.availableAvgRatio
              tPVMonth1Label = entry(month(add-interval(std-da, -1, "month")), {&months}) + ":"
              tPVMonth2Label = entry(month(add-interval(std-da, -2, "month")), {&months}) + ":"
              tPVMonth3Label = entry(month(add-interval(std-da, -3, "month")), {&months}) + ":"
              .
        end.
        display
          tPVLimit
          tPVUnremmited
          tPVAvailable
          tPVMonth1
          tPVMonth2
          tPVMonth3
          tPVThreeMonthAverage
          tPVUnremittedAverage
          tPVAvailableAverage
          tPVMonth1Label
          tPVMonth2Label
          tPVMonth3Label
          .
        rightAlignText(tPVMonth1Label:handle).
        rightAlignText(tPVMonth2Label:handle).
        rightAlignText(tPVMonth3Label:handle).
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataQAR C-Win 
PROCEDURE getDataQAR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  define variable hYear as handle no-undo.
  define variable hDate as handle no-undo.
  define variable hQAR as handle no-undo.
  define variable hERR as handle no-undo.
  define variable dERRAvg as decimal no-undo initial 0.
  define variable dQARAvg as decimal no-undo initial 0.
  
  for first wt exclusive-lock
      where wt.id = 1:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentreviewscore.p (input pAgentID,
                                      output table reviewscore,
                                      output std-lo,
                                      output std-ch).
                                      
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f1Details:
        std-ha = GetWidgetByName(frame fDetails:handle, "f" + string(wt.id) + "Details").
        do std-in = 1 to 3:
          hYear = GetWidgetByName(std-ha, "tQARYear" + string(std-in)).
          if valid-handle(hYear)
           then hYear:hidden = true.
        end.
        std-in = 0.
        for each reviewscore no-lock
           where reviewscore.agentID = pAgentID:
          
          assign
            hYear = GetWidgetByName(std-ha, "tQARYear" + string(reviewscore.seq))
            hDate = GetWidgetByName(std-ha, "tQARDate" + string(reviewscore.seq))
            hQAR = GetWidgetByName(std-ha, "tQARScore" + string(reviewscore.seq))
            hERR = GetWidgetByName(std-ha, "tERRScore" + string(reviewscore.seq))
            .
          if valid-handle(hYear) and valid-handle(hDate) and valid-handle(hQAR) and valid-handle(hERR)
           then
            do:
              assign
                std-in = std-in + 1
                hYear:hidden = false
                hYear:screen-value = string(year(reviewscore.auditDate))
                hDate:screen-value = string(reviewscore.auditDate)
                hQAR:screen-value = string(reviewscore.qarScore)
                hERR:screen-value = string(reviewscore.errScore)
                dQARAvg = dQARAvg + reviewscore.qarScore
                dERRAvg = dERRAvg + reviewscore.errScore
                .
              centerText(hYear).
            end.
        end.
        assign
          tQARScoreAvg = dQARAvg / (if std-in = 0 then 1 else std-in)
          tERRScoreAvg = dERRAvg / (if std-in = 0 then 1 else std-in)
          .
        display
          tQARScoreAvg
          tERRScoreAvg
          .
        centerText(tQARYearAvg:handle).
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataReceivables C-Win 
PROCEDURE getDataReceivables :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  for first wt exclusive-lock
      where wt.id = 4:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentreceivables.p (input "ALL",
                                      input pAgentID,
                                      output table reviewreceivable,
                                      output std-lo,
                                      output std-ch).
                                            
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f4Details:
        assign
          tARCurrent = 0
          tAR31to60 = 0
          tAR61to90 = 0
          tAROver90 = 0
          tARTotal = 0
          .
        for first reviewreceivable exclusive-lock
            where reviewreceivable.agentID = pAgentID:
          
          assign
            tARCurrent = reviewreceivable.daysCurrent
            tAR31to60 = reviewreceivable.days31to60
            tAR61to90 = reviewreceivable.days61to90
            tAROver90 = reviewreceivable.daysOver90
            tARTotal = tARCurrent + tAR31to60 + tAR61to90 + tAROver90
            .
        end.
        display
          tARCurrent
          tAR31to60
          tAR61to90
          tAROver90
          tARTotal
          .
        centerText(tARCurrentLabel:handle).
        centerText(tAR31to60Label:handle).
        centerText(tAR61to90Label:handle).
        centerText(tAROver90Label:handle).
        centerText(tARTotalLabel:handle).
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getDataRemittances C-Win 
PROCEDURE getDataRemittances :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pPercent as integer no-undo.
  
  define variable hYear as handle no-undo.
  define variable hNet as handle no-undo.
  define variable hGross as handle no-undo.
  define variable hCPL as handle no-undo.
  define variable dNetTotal as decimal no-undo initial 0.
  define variable dGrossTotal as decimal no-undo initial 0.
  define variable dCPLTotal as decimal no-undo initial 0.
  
  for first wt exclusive-lock
      where wt.id = 3:
    
    {lib/pbupdate.i "'Retrieving ' + wt.wLabel:screen-value" pPercent}
    run server/getagentreviewremit.p (input pAgentID,
                                      output table reviewremit,
                                      output std-lo,
                                      output std-ch).
                                      
    wt.isSensitive = std-lo.
    if showError(std-lo, std-ch)
     then
      do with frame f3Details:
        std-ha = GetWidgetByName(frame fDetails:handle, "f" + string(wt.id) + "Details").
        do std-in = 1 to 3:
          hYear = GetWidgetByName(std-ha, "tRemitYear" + string(std-in)).
          if valid-handle(hYear)
           then hYear:hidden = true.
        end.
        std-in = 0.
        for each reviewremit no-lock
           where reviewremit.agentID = pAgentID:
          
          assign
            hYear = GetWidgetByName(std-ha, "tRemitYear" + string(reviewremit.seq))
            hNet = GetWidgetByName(std-ha, "tRemitNet" + string(reviewremit.seq))
            hGross = GetWidgetByName(std-ha, "tRemitGross" + string(reviewremit.seq))
            hCPL = GetWidgetByName(std-ha, "tRemitCPL" + string(reviewremit.seq))
            .
          
          if valid-handle(hYear) and valid-handle(hNet) and valid-handle(hGross) and valid-handle(hCPL)
           then
            do:
              assign
                std-in = std-in + 1
                hYear:hidden = false
                hYear:screen-value = string(reviewremit.periodYear) + (if reviewremit.periodYear = year(today) then " (YTD)" else "")
                hNet:screen-value = string(reviewremit.netPremium)
                hGross:screen-value = string(reviewremit.grossPremium)
                hCPL:screen-value = string(reviewremit.cplPremium)
                dNetTotal = dNetTotal + reviewremit.netPremium
                dGrossTotal = dGrossTotal + reviewremit.grossPremium
                dCPLTotal = dCPLTotal + reviewremit.cplPremium
                .
              centerText(hYear).
            end.
        end.
        assign
          tRemitNetTotal = dNetTotal
          tRemitGrossTotal = dGrossTotal
          tRemitCPLTotal = dCPLTotal
          .
        display
          tRemitNetTotal
          tRemitGrossTotal
          tRemitCPLTotal
          .
        centerText(tRemitYearTotal:handle).
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE toggleSection C-Win 
PROCEDURE toggleSection :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pSection as integer no-undo.
  define input parameter pClose as logical no-undo.
  
  define variable dRow as decimal no-undo.
  
  frame fDetails:virtual-height-chars = 100.
  for each wt exclusive-lock
     where wt.id >= pSection:
    
    if valid-handle(wt.wFrame)
     then wt.wFrame:column = 7.
    /* hide/show the section */
    if wt.id = pSection
     then
      do:
        assign
          dRow = wt.wButton:row
          wt.wButton:row = dRow
          wt.wLabel:row = dRow + 0.2
          wt.isOpen = not pClose
          .
        if valid-handle(wt.wFrame)
         then
          assign
            wt.wFrame:row = dRow + 1.1
            wt.wFrame:hidden = pClose
            .
      end.
     else
      do:
        assign
          wt.wButton:row = dRow
          wt.wLabel:row = dRow + 0.2
          .
        if valid-handle(wt.wFrame)
         then
          assign
            wt.wFrame:row = dRow + 1.1
            .
      end.
    
    dRow = dRow + wt.wButton:height-chars + 0.1.
    if wt.isOpen and wt.isSensitive
     then
      do:
        dRow = dRow + wt.wHeight.
        wt.wButton:load-image("images/s-collapse.bmp").
        wt.wButton:load-image-insensitive("images/s-collapse-i.bmp").
        wt.wButton:tooltip = "Hide the " + wt.wLabel:screen-value + " section".
        wt.wLabel:tooltip = "Hide the " + wt.wLabel:screen-value + " section".
      end.
     else
      do:
        wt.wButton:load-image("images/s-expand.bmp").
        wt.wButton:load-image-insensitive("images/s-expand-i.bmp").
        wt.wButton:tooltip = "Show the " + wt.wLabel:screen-value + " section".
        wt.wLabel:tooltip = "Show the " + wt.wLabel:screen-value + " section".
      end.
  end.
  if dRow > frame fDetails:height-chars
   then frame fDetails:virtual-height-chars = dRow.
   else frame fDetails:virtual-height-chars = frame fDetails:height-chars.
  run util/RemoveHScrollbar.p (frame fDetails:handle).
  setFocus(frame fDetails:handle).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dRow as decimal no-undo.

  
  assign
    /* resize the main frame */
    frame {&frame-name}:width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels = frame {&frame-name}:width-pixels
    frame {&frame-name}:height-pixels = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = frame {&frame-name}:height-pixels
    /* resize the details frame */
    frame fDetails:width-pixels = frame {&frame-name}:width-pixels
    frame fDetails:virtual-width-pixels = frame fDetails:width-pixels
    frame fDetails:height-pixels = frame {&frame-name}:height-pixels - 44
    /* center the agent frame */
    frame fAgent:x = (frame {&frame-name}:width-pixels - frame fAgent:width-pixels) / 2
    /* expand the frames */
    frame f2Details:width-pixels = frame {&frame-name}:width-pixels - 55
    frame f2Details:virtual-width-pixels = frame f2Details:width-pixels
    frame f5Details:width-pixels = frame {&frame-name}:width-pixels - 55
    frame f5Details:virtual-width-pixels = frame f5Details:width-pixels
    frame f9Details:width-pixels = frame {&frame-name}:width-pixels - 55
    frame f9Details:virtual-width-pixels = frame f9Details:width-pixels
    /* resize the browse */
    browse tClaims:width-pixels = frame f2Details:width-pixels - browse tClaims:x - 10
    browse tAlert:width-pixels = frame f5Details:width-pixels - browse tAlert:x - 10
    browse tCorrective:width-pixels = frame f9Details:width-pixels - browse tCorrective:x - 10 
    /* resize other widgets */
    tCorrectiveQuestion:width-pixels in frame f9Details = frame f9Details:width-pixels - tCorrectiveQuestion:x in frame f9Details - 10
    tCorrectiveFinding:width-pixels in frame f9Details = frame f9Details:width-pixels - tCorrectiveFinding:x in frame f9Details - 10
    tCorrectiveAction:width-pixels in frame f9Details = frame f9Details:width-pixels - tCorrectiveAction:x in frame f9Details - 10
    .
  
  /* resize the description column in the claims browse */
  {lib/resize-column.i &col="'description'" &var=dClaims &browse-name=tClaims}
  {lib/resize-column.i &col="'processCodeDesc,ownerDesc,severityDesc,thresholdRange'" &var=dAlert &browse-name=tAlert}
  
  /* adjust the labels */
  for each wt no-lock:
    wt.wLabel:width-pixels = frame {&frame-name}:width-pixels - 55.
  end.
  
  /* redo the virtual height */
  frame fDetails:virtual-height-chars = 100.
  for each wt no-lock:
    dRow = dRow + wt.wButton:height-chars + 0.1.
    if wt.isOpen
     then dRow = dRow + wt.wHeight.
  end.
  if dRow > frame fDetails:height-chars
   then frame fDetails:virtual-height-chars = dRow.
   else frame fDetails:virtual-height-chars = frame fDetails:height-chars.
  run util/RemoveHScrollbar.p (frame fDetails:handle).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION addSection C-Win 
FUNCTION addSection RETURNS LOGICAL
  ( input pSection as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  create wt.
  assign
    wt.id = pSection
    wt.wButton = getWidgetByName(frame fDetails:handle, "t" + string(pSection) + "Expand")
    wt.wLabel = getWidgetByName(frame fDetails:handle, "t" + string(pSection) + "Label")
    wt.wFrame = getWidgetByName(frame fDetails:handle, "f" + string(pSection) + "Details")
    wt.wHeight = (if valid-handle(wt.wFrame) then wt.wFrame:height-chars + 0.1 else 0)
    wt.isOpen = true
    wt.isSensitive = true
    .
  wt.wButton:load-image("images/s-expand.bmp").
  wt.wButton:load-image-insensitive("images/s-expand-i.bmp").
  release wt.
  RETURN can-find(first wt where wt.id = pSection and wt.wHeight > 0).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iCnt as integer no-undo.
  
  /* clear the sections */
  do iCnt = 1 to iTotalSection:
    run toggleSection in this-procedure (iCnt, true).
  end.
  /* make all labels insensitive */
  for each wt exclusive-lock:
    wt.isSensitive = false.
  end.
  enableButtons(false).
  /* das: don't clear the manager and contact date 
  do with frame fAgent:
    assign
      tStateID:screen-value = ""
      tManager:screen-value = ""
      tContract:screen-value = ""
      .
  end.
  */

  /* reset the labels */
  do with frame fDetails:
    assign
      t1Label:screen-value = "Quality Assurance Reviews"
      t2Label:screen-value = "Claims"
      t3Label:screen-value = "Remittances"
      t4Label:screen-value = "Accounts Receivable"
      t5Label:screen-value = "Open Alerts"
      t6Label:screen-value = "Policy Information"
      t7Label:screen-value = "CPL Information"
      t8Label:screen-value = "Activity"
      t9Label:screen-value = "Corrective Actions"
      .
  end.
  clearStatus().
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableButtons C-Win 
FUNCTION enableButtons RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* disable the buttons */
  do with frame fAgent:
    assign
      /*bPrint:sensitive = pEnable*/
      bRefresh:SENSITIVE = pEnable
      bExpandAll:sensitive = pEnable
      bCollapseAll:sensitive = pEnable
      .
  end.
  /* color the labels */
  for each wt no-lock:
    std-ha = wt.wLabel.
    if valid-handle(std-ha)
     then std-ha:fgcolor = (if pEnable and wt.isSensitive then ? else 7).
    
    std-ha = wt.wButton.
    if valid-handle(std-ha)
     then
      do:
        if wt.isSensitive = ?
         then wt.isSensitive = false.
        std-ha:sensitive = (pEnable and wt.isSensitive).
      end.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showError C-Win 
FUNCTION showError RETURNS LOGICAL
  ( input pSuccess as logical, 
    input pMessage as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if not pSuccess
   then message pMessage view-as alert-box error.
    
  RETURN pSuccess.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

