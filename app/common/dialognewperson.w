&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File:dialognewperson.w 

  Description:Dialog to create new Person record. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Sachin Chaturvedi 

  Created:05.12.2018 
  @Modified
  Date         Name            Description
  11/21/18     Gurvindar       Changed IP call for person duplicacy
  04/11/19     Rahul           Add mandatory flag '*' in firstName fillIn
  10/28/22     SA              Task #96812 Modified to add logic according to personagent.
  12/21/22  Sachin Chaturvedi  Task #100776 Validation and Format on Phone number
  03/02/23     SB              Task #102649 Capitalize first, middle and last name
  02/23/24     SB              Task #110389 Modified to validate email format.
  04/03/24     SB              Task #111689 Upgrade the UI layout to create a new person or link existing person to an agent
  04/19/24     SChandu         Task #111689 Modified to change active one first and inactives in grey color.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Temp-table Definitions ---                                           */
{tt/person.i}
{tt/person.i &tableAlias=tempPerson}
{tt/person.i &tableAlias=duplicatePerson}
{tt/personagentdetails.i}
{tt/state.i}

define temp-table tPerson no-undo like duplicatePerson
  index idxSeq seq.

/* Parameters Definitions ---                                           */
define input  parameter ipcAgentId as character no-undo.
define output parameter opcPersonId as character no-undo.
define output parameter oplSuccess  as logical   no-undo.

/* Library Files  */
{lib/com-def.i}
{lib/std-def.i}
{lib/validEmailList.i}
{lib/formatcontactnumber.i}

/* Local Variable Definitions ---                                       */
define variable cValuePair        as character            no-undo.
define variable iCount            as integer initial 1    no-undo.
define variable iRecord           as integer              no-undo.
define variable cAppCode          as character            no-undo.
define variable lCheck            as logical initial true no-undo.
define variable cFullName         as character            no-undo.
define variable cEmail            as character            no-undo.
define variable cMobile           as character            no-undo.
define variable unsavedChanges    as logical              no-undo.
define variable cAgentName        as character            no-undo.
define variable lDupPerson        as logical              no-undo.
define variable lCanLinkAgent     as logical              no-undo.
define variable exitOnClose       as logical              no-undo.
define variable cFormatNumber     as character            no-undo .
define variable  lError           as logical              no-undo .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAffiliation

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES personagentdetails

/* Definitions for BROWSE brwAffiliation                                */
&Scoped-define FIELDS-IN-QUERY-brwAffiliation personagentdetails.Notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAffiliation   
&Scoped-define SELF-NAME brwAffiliation
&Scoped-define QUERY-STRING-brwAffiliation FOR EACH personagentdetails
&Scoped-define OPEN-QUERY-brwAffiliation OPEN QUERY {&SELF-NAME} FOR EACH personagentdetails.
&Scoped-define TABLES-IN-QUERY-brwAffiliation personagentdetails
&Scoped-define FIRST-TABLE-IN-QUERY-brwAffiliation personagentdetails


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAffiliation}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fFullName fEmail fMobile btnCheckDuplicate ~
fAddress fAddress2 fCity fZip fPhone fExt tglEmp fJobTitle brwAffiliation ~
edNotes fNPN fAltaUID flMessage 
&Scoped-Define DISPLAYED-OBJECTS fFullName fMarkMandatory1 fEmail fMobile ~
fMarkMandatory3 fMarkMandatory2 fAddress fAddress2 fCity cbState fZip ~
fPhone fExt tgldnc tglEmp fJobTitle edNotes fNPN fAltaUID flMessage 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkUnsavedChanges Dialog-Frame 
FUNCTION checkUnsavedChanges RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bFirst  NO-FOCUS
     LABEL "First" 
     SIZE 4.8 BY 1.14 TOOLTIP "First".

DEFINE BUTTON bLast  NO-FOCUS
     LABEL "Last" 
     SIZE 4.8 BY 1.14 TOOLTIP "Last".

DEFINE BUTTON bNext  NO-FOCUS
     LABEL "Next" 
     SIZE 4.8 BY 1.14 TOOLTIP "Next".

DEFINE BUTTON bPervious  NO-FOCUS
     LABEL "Pervious" 
     SIZE 4.8 BY 1.14 TOOLTIP "Pervious".

DEFINE BUTTON bSaveClose AUTO-GO 
     LABEL "Save" 
     SIZE 14.8 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON btnCheckDuplicate 
     LABEL "Check for Duplicates" 
     SIZE 27 BY 1.14.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10.2 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 83 BY 4 NO-UNDO.

DEFINE VARIABLE fAddress AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 83 BY 1 NO-UNDO.

DEFINE VARIABLE fAddress2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 83 BY 1 NO-UNDO.

DEFINE VARIABLE fAltaUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "ALTA UID" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE fCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE fEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 41.6 BY 1 TOOLTIP "Email or Mobile is required" NO-UNDO.

DEFINE VARIABLE fExt AS CHARACTER FORMAT "X(256)":U 
     LABEL "Ext" 
     VIEW-AS FILL-IN 
     SIZE 8.6 BY 1 NO-UNDO.

DEFINE VARIABLE fFullName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 83 BY 1 TOOLTIP "Name is required" NO-UNDO.

DEFINE VARIABLE fJobTitle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Title" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE flMessage AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 18.6 BY .62 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3.2 BY .62 TOOLTIP "Name is required"
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3.2 BY .62 TOOLTIP "Email or Mobile is required"
     FGCOLOR 9 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory3 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62 TOOLTIP "Email or Mobile is required"
     FGCOLOR 9 FONT 10 NO-UNDO.

DEFINE VARIABLE fMobile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Mobile" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "Email or Mobile is required" NO-UNDO.

DEFINE VARIABLE fNPN AS CHARACTER FORMAT "X(256)":U 
     LABEL "NPN" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE fPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE fZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 NO-UNDO.

DEFINE VARIABLE tgldnc AS LOGICAL INITIAL no 
     LABEL "Do Not Contact" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .81 NO-UNDO.

DEFINE VARIABLE tglEmp AS LOGICAL INITIAL no 
     LABEL "Our Employee" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 TOOLTIP "Our Employee" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAffiliation FOR 
      personagentdetails SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAffiliation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAffiliation Dialog-Frame _FREEFORM
  QUERY brwAffiliation DISPLAY
      personagentdetails.Notes                  format "x(100)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-LABELS NO-ASSIGN NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-TAB-STOP SIZE 83 BY 4.1
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bFirst AT ROW 3.71 COL 26 WIDGET-ID 392 NO-TAB-STOP 
     fFullName AT ROW 1.14 COL 8.8 COLON-ALIGNED WIDGET-ID 8
     fMarkMandatory1 AT ROW 1.43 COL 91.8 COLON-ALIGNED NO-LABEL WIDGET-ID 388 NO-TAB-STOP 
     fEmail AT ROW 2.24 COL 8.8 COLON-ALIGNED WIDGET-ID 10
     fMobile AT ROW 2.24 COL 61.8 COLON-ALIGNED WIDGET-ID 12
     fMarkMandatory3 AT ROW 2.57 COL 50.4 COLON-ALIGNED NO-LABEL WIDGET-ID 384 NO-TAB-STOP 
     fMarkMandatory2 AT ROW 2.57 COL 91.8 COLON-ALIGNED NO-LABEL WIDGET-ID 382 NO-TAB-STOP 
     btnCheckDuplicate AT ROW 3.71 COL 36 WIDGET-ID 14
     fAddress AT ROW 5.67 COL 9 COLON-ALIGNED WIDGET-ID 20
     fAddress2 AT ROW 6.76 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 22
     fCity AT ROW 7.86 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 26
     cbState AT ROW 7.86 COL 67.6 COLON-ALIGNED NO-LABEL WIDGET-ID 282
     fZip AT ROW 7.86 COL 78.4 COLON-ALIGNED NO-LABEL WIDGET-ID 28
     fPhone AT ROW 8.95 COL 9 COLON-ALIGNED WIDGET-ID 32
     fExt AT ROW 8.95 COL 43.4 COLON-ALIGNED WIDGET-ID 34
     tgldnc AT ROW 9 COL 57 WIDGET-ID 52
     tglEmp AT ROW 9 COL 77.6 WIDGET-ID 380
     fJobTitle AT ROW 10.05 COL 9 COLON-ALIGNED WIDGET-ID 66
     brwAffiliation AT ROW 12.1 COL 11 WIDGET-ID 200
     edNotes AT ROW 17.14 COL 11 NO-LABEL WIDGET-ID 36
     fNPN AT ROW 21.24 COL 9 COLON-ALIGNED WIDGET-ID 16
     fAltaUID AT ROW 21.24 COL 66 COLON-ALIGNED WIDGET-ID 18
     bSaveClose AT ROW 23.29 COL 42.6 WIDGET-ID 58
     bLast AT ROW 3.71 COL 68.2 WIDGET-ID 390 NO-TAB-STOP 
     bNext AT ROW 3.71 COL 63.2 WIDGET-ID 386 NO-TAB-STOP 
     bPervious AT ROW 3.71 COL 31 WIDGET-ID 158 NO-TAB-STOP 
     flMessage AT ROW 4 COL 61.6 COLON-ALIGNED NO-LABEL WIDGET-ID 394 NO-TAB-STOP 
     "Affiliations:" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 11.38 COL 11.2 WIDGET-ID 64
     "Notes:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 16.43 COL 11 WIDGET-ID 38
     SPACE(78.19) SKIP(7.65)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Person" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAffiliation fJobTitle Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bFirst IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bFirst:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* SETTINGS FOR BUTTON bLast IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bLast:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* SETTINGS FOR BUTTON bNext IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bNext:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* SETTINGS FOR BUTTON bPervious IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bPervious:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* SETTINGS FOR BUTTON bSaveClose IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbState IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edNotes:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fAddress:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fAddress2:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fAltaUID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fCity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fExt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fJobTitle:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flMessage:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fNPN:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fPhone:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fZip:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tgldnc IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAffiliation
/* Query rebuild information for BROWSE brwAffiliation
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH personagentdetails.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAffiliation */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON END-ERROR OF FRAME Dialog-Frame /* New Person */
DO:
  if not exitOnClose then
   do:
    if checkUnsavedChanges()
     then
      return no-apply.
   end.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Person */
DO:    
  if not exitOnClose then
   do:
     if checkUnsavedChanges() 
      then return no-apply.
      else
       do:
         exitOnClose = true.
         run windowClose in this-procedure. 
       end.
   end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFirst
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFirst Dialog-Frame
ON CHOOSE OF bFirst IN FRAME Dialog-Frame /* First */
do:
  find first tPerson use-index idxseq no-error.
  run displayData in this-procedure.
  if available tPerson
   then
    iCount = tPerson.seq - 1 no-error.
    
  run setNavigationState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLast
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLast Dialog-Frame
ON CHOOSE OF bLast IN FRAME Dialog-Frame /* Last */
do:
  find last tPerson use-index idxseq no-error.
  run displayData in this-procedure.
  if available tPerson
   then
     iCount = tPerson.seq - 1 no-error.
  run setNavigationState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNext
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNext Dialog-Frame
ON CHOOSE OF bNext IN FRAME Dialog-Frame /* Next */
do:
  find next tPerson use-index idxSeq no-error.
  run displayData in this-procedure.
  if available tPerson 
   then
    iCount = (iCount + 1).
  run setNavigationState in this-procedure.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPervious
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPervious Dialog-Frame
ON CHOOSE OF bPervious IN FRAME Dialog-Frame /* Pervious */
do:
  find prev tPerson use-index idxSeq no-error.
  run displayData in this-procedure.
  if available tPerson 
   then
    iCount = (iCount - 1).
  run setNavigationState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAffiliation
&Scoped-define SELF-NAME brwAffiliation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliation Dialog-Frame
ON ROW-DISPLAY OF brwAffiliation IN FRAME Dialog-Frame
DO:
  assign
      personagentdetails.notes:fgcolor in browse brwAffiliation = if (personagentdetails.stat = "InActive")  then 7
                                                                   else ?                                                                                                                   
      . 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSaveClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSaveClose Dialog-Frame
ON CHOOSE OF bSaveClose IN FRAME Dialog-Frame /* Save */
DO:
  run savePerson in this-procedure.

  if not oplSuccess
   then
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnCheckDuplicate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnCheckDuplicate Dialog-Frame
ON CHOOSE OF btnCheckDuplicate IN FRAME Dialog-Frame /* Check for Duplicates */
DO:
   run clearDisableLowerPortion in this-procedure.
   run validationCheck in this-procedure(output lCheck).
   if lCheck then
   run getduplicatepersons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fFullName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fFullName Dialog-Frame
ON VALUE-CHANGED OF fFullName IN FRAME Dialog-Frame /* Name */
,fEmail 
DO:
  run clearDisableLowerPortion in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fMobile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMobile Dialog-Frame
ON VALUE-CHANGED OF fMobile IN FRAME Dialog-Frame /* Mobile */
DO:
  if fMobile:input-value = "" then
   assign
       fMobile:screen-value  = ""
       fMobile:handle:format = 'X(256)'.
       
  run clearDisableLowerPortion in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNPN
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNPN Dialog-Frame
ON VALUE-CHANGED OF fNPN IN FRAME Dialog-Frame /* NPN */
, fNPN, fAltaUID, fAddress, fAddress2, fCity, cbState, fZip, tgldnc, tglEmp, fPhone, fExt, fJobTitle, edNotes  
DO:
  assign flMessage:screen-value = "".
  run cacheNewPersonData in this-procedure(self:handle).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAffiliation
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
bPervious:load-image("images/s-previous.bmp").
bPervious:load-image-insensitive("images/s-previous-i.bmp").
bNext:load-image("images/s-next.bmp").
bNext:load-image-insensitive("images/s-next-i.bmp").
bFirst:load-image("images/s-previouspg.bmp").
bFirst:load-image-insensitive("images/s-previouspg-i.bmp").
bLast:load-image("images/s-nextpg.bmp").
bLast:load-image-insensitive("images/s-nextpg-i.bmp").

publish "GetCurrentValue" ("ApplicationCode", output cAppCode).

{lib/get-full-state-list.i &combo=cbState &useSingle=true &ALL=false}
cbState:delete("ALL").
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  cbState:add-first("None").
  assign cbState:screen-value = "None".

  apply 'entry' to fFullName.
  assign
      fMarkMandatory1:screen-value = {&mandatory}
      fMarkMandatory2:screen-value = {&mandatory}
      fMarkMandatory3:screen-value = {&mandatory}
      .
 
  if ipcAgentID <> ""
   then
    do:
      publish "getAgentName" (input ipcAgentID,
                              output std-ch,
                              output std-lo).                                               
      if std-lo 
       then 
        assign cAgentName = std-ch.   
    end.
    
  if ipcAgentId ne "" then
   do:
     frame Dialog-Frame:Title = "New Person for Agent - " + cAgentName + " (" + ipcAgentId + ")".
     fJobTitle:sensitive = true.
     tglEmp:hidden       = true.
   end.
   else
     assign 
         fJobTitle:hidden = true
         tglEmp:sensitive = false.
    
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cacheNewPersonData Dialog-Frame 
PROCEDURE cacheNewPersonData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter wHandle as handle no-undo.
  
  do with frame {&frame-name}:
  end.
  
  find first tPerson where tPerson.seq = 1 no-error.
  if avail tPerson then
   do:
     assign
         tPerson.dispName      = cFullName
         tPerson.contactEmail  = cEmail
         tPerson.contactMobile = cMobile.
  
     if wHandle:name = "fNPN" then tPerson.NPN = wHandle:screen-value.
     if wHandle:name = "fAltaUID" then tPerson.AltaUID = wHandle:screen-value.
     if wHandle:name = "fAddress" then tPerson.address1 = wHandle:screen-value.
     if wHandle:name = "fAddress2" then tPerson.address2 = wHandle:screen-value.
     if wHandle:name = "fCity" then tPerson.city = wHandle:screen-value.
     if wHandle:name = "cbState" then tPerson.state = wHandle:screen-value.
     if wHandle:name = "fZip" then tPerson.zip = wHandle:screen-value.
     if (ipcAgentId ne "" and wHandle:name = "fJobTitle") then tPerson.jobTitle = wHandle:screen-value.
     if wHandle:name = "edNotes" then tPerson.notes = wHandle:screen-value.
     if wHandle:name = "fphone" and wHandle:input-value = "" then
     assign
         wHandle:screen-value = ""
         wHandle:format = 'X(256)'.   
     if wHandle:name = "fphone" then tPerson.contactPhone = wHandle:screen-value.
     if (ipcAgentId = "" and wHandle:name = "tglEmp") then tPerson.internal = wHandle:checked.
     if wHandle:name = "tgldnc" then tPerson.doNotCall = wHandle:checked.
     if wHandle:name = "fExt" then assign tPerson.phExtension = if fExt:input-value = '0' then '' else fExt:input-value.
   end.
  
   if fAddress:input-value  ne "" or
      fAddress2:input-value ne "" or
      fCity:input-value     ne "" or
      cbState:input-value   ne "None" or
      fZip:input-value      ne "" or
      fPhone:input-value    ne "" or
      fExt:input-value      ne "" or
      fJobTitle:input-value ne "" or
      edNotes:input-value   ne "" or
      fNPN:input-value      ne "" or
      fAltaUID:input-value  ne "" or
      tglDNC:input-value    ne 0 or
      tglEmp:input-value    ne 0
   then 
    assign unsavedChanges = yes.
   else assign unsavedChanges = no.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearDisableLowerPortion Dialog-Frame 
PROCEDURE clearDisableLowerPortion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table tPerson.
  empty temp-table personagentdetails.
  
  /*Clear everything*/
  assign
       cFullName               = ""
       cEmail                  = ""
       cMobile                 = ""
       flMessage:screen-value  = ""
       fJobTitle:screen-value  = "" 
       fNPN:screen-value       = ""
       fAltaUid:screen-value   = "" 
       fAddress:screen-value   = ""  
       fAddress2:screen-value  = ""  
       fCity:screen-value      = ""      
       cbState:screen-value    = ""     
       fZip:screen-value       = ""       
       fPhone:screen-value     = ""
       fExt:screen-value       = ""   
       edNotes:screen-value    = ""
       tgldnc:checked          = false
       tglEmp:checked          = false
       iRecord                 = 0
       iCount                  = 1 
       unsavedChanges          = false no-error.
  
  /*Disable lower Portion*/
  assign
       fJobTitle:read-only  = true 
       fNPN:read-only       = true
       fAltaUid:read-only   = true 
       fAddress:read-only   = true  
       fAddress2:read-only  = true  
       fCity:read-only      = true      
       cbState:sensitive    = false     
       fZip:read-only       = true       
       fPhone:read-only     = true
       fExt:read-only       = true   
       edNotes:read-only    = true
       tgldnc:sensitive     = false
       tglEmp:sensitive     = false 
       bSaveClose:sensitive = false no-error.
 
  run setNavigationState in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayAffiliations Dialog-Frame 
PROCEDURE displayAffiliations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 if avail tPerson  then
  do:
    for each personagentdetails:
      if personagentdetails.jobTitle ne "" 
       then
        assign personagentdetails.notes = personagentdetails.name + ' ('+ personagentdetails.jobTitle + ")".
       else
        assign personagentdetails.notes = personagentdetails.name.
    end.
    open query brwAffiliation for each personagentdetails where personagentdetails.personID = tPerson.personID by personagentdetails.stat.
    if num-results("brwAffiliation") ne 0
     then brwAffiliation:Deselect-focused-row().
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  
  define variable fPh-handle as handle no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if avail tPerson then
   do:
     assign
         fFullName:screen-value  = tPerson.dispName
         fEmail:screen-value     = tPerson.contactEmail
         fMobile:screen-value    = tPerson.contactMobile
         fJobTitle:screen-value  = tPerson.jobTitle 
         fNPN:screen-value       = tPerson.NPN
         fAltaUid:screen-value   = tPerson.AltaUID
         fFullName:screen-value  = tPerson.dispName  
         fAddress:screen-value   = tPerson.address1  
         fAddress2:screen-value  = tPerson.address2  
         fCity:screen-value      = tPerson.city      
         cbState:screen-value    = if tPerson.state = "" then "None" else tPerson.state     
         fZip:screen-value       = tPerson.zip       
         fPhone:screen-value     = entry(1,tPerson.contactPhone)
         fExt:screen-value       = if num-entries(tPerson.contactPhone) > 1 then  entry(2,tPerson.contactPhone) else ''   
         tgldnc:checked          = tPerson.doNotCall
         tglEmp:checked          = tPerson.internal
         edNotes:screen-value    = tPerson.notes
         no-error
         .
         
     run displayAffiliations in this-procedure.

     if avail tPerson and tPerson.seq ne 1 
      then
       assign btnCheckDuplicate:label = string(tPerson.seq - 1) + " of " + string(iRecord - 1).
     else
       assign btnCheckDuplicate:label = "Check for Duplicates".
        
     run enableDisableLowerWidgets in this-procedure.
  
     fPh-handle = fPhone:handle. 
     if entry(1,tPerson.contactPhone) ne "" 
      then 
       do:
         Validatenumber(input fPhone:input-value, output cFormatNumber,output std-lo).
         if not std-lo 
          then
           fPhone:screen-value= cFormatNumber.
       end.
      else 
       assign
           fPh-handle:screen-value = ''  
           fPh-handle:format       = 'X(256)'.
     
     fPh-handle = fMobile:handle. 
     if tPerson.contactMobile ne "" 
      then 
       do:
         cFormatNumber = "".
         Validatenumber(input fMobile:input-value, output cFormatNumber,output std-lo).
         if not std-lo
          then
           fMobile:screen-value= cFormatNumber.
       end.
      else
       assign
           fPh-handle:screen-value = ''  
           fPh-handle:format       = 'X(256)'.
                
   end.
  else
   do:
     find first tPerson use-index idxSeq no-error.
     run displayData in this-procedure.
     iCount = tPerson.seq - 1.
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableLowerWidgets Dialog-Frame 
PROCEDURE enableDisableLowerWidgets :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign bSaveClose:label = if avail tPerson and tPerson.seq ne 1 then "Select" else "Save".
  
  if bSaveClose:label = "select" 
   then
    do:
      assign
          fFullName:read-only         = true
          fEmail:read-only            = true
          fMobile:read-only           = true
          btnCheckDuplicate:sensitive = false
          fNPN:read-only              = false
          fAltaUid:read-only          = false
          fAddress:read-only          = false
          fAddress2:read-only         = false
          fCity:read-only             = false
          cbState:sensitive           = true
          fZip:read-only              = false
          fPhone:read-only            = false
          fExt:read-only              = false
          tgldnc:sensitive            = true
          tglEmp:sensitive            = true
          fJobTitle:read-only         = false
          edNotes:read-only           = false
          . 
    end.
   else
    do:
      assign
          fFullName:read-only         = false
          fEmail:read-only            = false
          fMobile:read-only           = false
          btnCheckDuplicate:sensitive = true
          fNPN:read-only              = true
          fAltaUid:read-only          = true
          fAddress:read-only          = true
          fAddress2:read-only         = true
          fCity:read-only             = true
          cbState:sensitive           = false
          fZip:read-only              = true
          fPhone:read-only            = true
          fExt:read-only              = true
          tgldnc:sensitive            = false
          tglEmp:sensitive            = false
          fJobTitle:read-only         = true
          edNotes:read-only           = true.
    end.
  
  run enableDisableSave in this-procedure.
   
  if (lDupPerson and not lCanLinkAgent) or (not lDupPerson and lCanLinkAgent) or (lDupPerson and lCanLinkAgent)
   then
    do:
      if bSaveClose:sensitive and bSaveClose:label = "Save" 
       then
         assign
             fNPN:read-only             = false  
             fAltaUid:read-only         = false  
             fAddress:read-only         = false 
             fAddress2:read-only        = false 
             fCity:read-only            = false 
             cbState:sensitive          = true
             fZip:read-only             = false 
             fPhone:read-only           = false 
             fExt:read-only             = false
             tgldnc:sensitive           = true
             tglEmp:sensitive           = true
             fJobTitle:read-only        = false
             edNotes:read-only          = false 
             . 
      else 
       assign
           fNPN:read-only             = true 
           fAltaUid:read-only         = true 
           fAddress:read-only         = true
           fAddress2:read-only        = true
           fCity:read-only            = true
           cbState:sensitive          = false
           fZip:read-only             = true
           fPhone:read-only           = true
           fExt:read-only             = true
           tgldnc:sensitive           = false
           tglEmp:sensitive           = false
           fJobTitle:read-only        = true
           edNotes:read-only          = true
           .
            
      assign bSaveClose:hidden = if (not bSaveClose:sensitive and bSaveClose:label = "Select") then true else false.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   do with frame {&frame-name}:
   end.
  
   if (lDupPerson and not lCanLinkAgent) then
      assign bSaveClose:sensitive = false.
   else if (lDupPerson and lCanLinkAgent) then
      assign bSaveClose:sensitive = if (ipcAgentId ne "" and bSaveClose:label = "Select") then true else false.
   else if not lDupPerson and lCanLinkAgent then
      assign bSaveClose:sensitive = if (bSaveClose:label = "Select" and ipcAgentID ne "") or
                                        bSaveClose:label = "Save" then true else false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fFullName fMarkMandatory1 fEmail fMobile fMarkMandatory3 
          fMarkMandatory2 fAddress fAddress2 fCity cbState fZip fPhone fExt 
          tgldnc tglEmp fJobTitle edNotes fNPN fAltaUID flMessage 
      WITH FRAME Dialog-Frame.
  ENABLE fFullName fEmail fMobile btnCheckDuplicate fAddress fAddress2 fCity 
         fZip fPhone fExt tglEmp fJobTitle brwAffiliation edNotes fNPN fAltaUID 
         flMessage 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getduplicatepersons Dialog-Frame 
PROCEDURE getduplicatepersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign 
       cFullName = trim(fFullName:input-value)
       cEmail    = trim(fEmail:input-value)
       cMobile   = trim(fMobile:input-value).
  
  empty temp-table duplicatePerson.
  empty temp-table tPerson.
  empty temp-table personagentdetails.
  
  publish "getduplicatepersons" (input cFullName,
                                 input cEmail,
                                 input cMobile,
                                 output table duplicatePerson,
                                 output table personagentdetails,
                                 output lDupPerson,
                                 output lCanLinkAgent,
                                 output std-lo,
                                 output std-ch).

  if not std-lo
   then
    message std-ch
      view-as alert-box error buttons ok.
  else 
   do: 
     if not can-find(first duplicatePerson) 
      then
       assign flMessage:screen-value = "No duplicate found.".
      else 
       assign flMessage:screen-value = "".
      
     for each duplicatePerson:
       assign 
           iRecord             = iRecord + 1
           duplicatePerson.seq = iRecord + 1.
     end.
     
     create duplicatePerson.
     assign
         duplicatePerson.seq             = 1
         duplicatePerson.dispName        = cFullName
         duplicatePerson.contactEmail    = cEmail
         duplicatePerson.contactMobile   = cMobile
         iRecord                         = iRecord + 1
         .
     
     for each duplicatePerson:
       create tPerson.
       buffer-copy duplicatePerson to tPerson.
     end.
     
     /*Show data of duplicate persons*/
     find first tPerson where tPerson.seq = 2 no-error.
     
     run displayData in this-procedure.
     run setNavigationState in this-procedure.
   end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savePerson Dialog-Frame 
PROCEDURE savePerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if not avail tPerson then 
  return.
  
  empty temp-table tempPerson.
  
  run validationCheck in this-procedure(output lCheck).
  if not lCheck then
   return.

  create tempPerson.
  
  if bSaveClose:label = "Save"
  then
    buffer-copy tPerson except agentID linkedToAgent to tempPerson  no-error.
   
 assign 
     tempPerson.personID      = tPerson.personID
     tempPerson.agentID       = ipcAgentId
     tempPerson.linkedToAgent = if ipcAgentId ne "" then true else false
     tempPerson.createArTag   = if cAppCode = "ARM" then true else false.

  if cAppcode = 'ARM'
   then
    do:
      publish "newagentperson" (input table tempPerson,
                                output opcPersonID,
                                output std-lo,
                                output std-ch).


     /* If there is any error during the server call, then show error, set flag and return. */
     if not std-lo
      then
       do:
         message std-ch
           view-as alert-box error buttons ok.
         oplSuccess = false.
         return.
      end.
     else
       oplSuccess = true.

     assign opcPersonID  =  if tempPerson.personID <> "" then tempPerson.personID else opcPersonID.

    end.
  else
   do:
     publish "newperson" (input table tempPerson,
                          output opcPersonID,
                          output std-lo,
                          output std-ch).

    /* If there is any error during the server call, then show error, set flag and return. */
    if not std-lo
     then
      do:
        message std-ch
          view-as alert-box error buttons ok.
        oplSuccess = false.
       return.
      end.
    else
     do:
       oplSuccess = true.
       if (opcPersonID <> "" and opcPersonID <> "0" and opcPersonID <> ?) then
       do:
         message "New Person created with ID: " + opcPersonID
         view-as alert-box info buttons ok.
       end.
     end.
   end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNavigationState Dialog-Frame 
PROCEDURE setNavigationState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      bNext:hidden        = (not (not(iRecord <= 1) and not(iCount + 1 = iRecord)))
      bLast:hidden        = bNext:hidden
      bPervious:hidden    = (not (not(iRecord <= 0) and iCount > 0))
      bFirst:hidden       = bPervious:hidden
      bNext:sensitive     = not bNext:hidden
      bLast:sensitive     = bNext:sensitive
      bPervious:sensitive = not bPervious:hidden
      bFirst:sensitive    = bPervious:sensitive
      .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validationCheck Dialog-Frame 
PROCEDURE validationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter lCheck as logical  initial true   no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if fFullName:input-value = ""
   then
    do:
      message "Name is required" view-as alert-box information buttons ok.
      assign lCheck = false.
      apply "entry" to fFullName.
      return.
    end.
    
  if fEmail:input-value = "" and fMobile:input-value = "" 
   then
    do:
      message "Email or Mobile is required" view-as alert-box information buttons ok.
      assign lCheck = false.
      apply "entry" to fEmail.
      return.
    end.
    
  if not validEmailAddr(fEmail:screen-value)
   then
    do:
      message "Invalid Email Address" view-as alert-box information buttons ok.
      assign lCheck = false.
      apply "entry" to fEmail.
      return.
    end.

  if fMobile:input-value ne ""
   then
    do:
      assign lCheck = false.
      apply "entry" to fMobile.
      Validatenumber(input fMobile:input-value, output cFormatNumber,output lError).

      if not lError 
       then
        fMobile:screen-value = string(cFormatNumber).
       else
        do:
          message "Invalid Mobile Number" view-as alert-box information buttons ok.
          lCheck = false.
          return.
        end.
     
    end.
   else 
    do:
     assign 
        fMobile:format = 'X(256)'
        fMobile:screen-value = ""
        lCheck = true.
    end.
  
  if fPhone:input-value ne ""
   then
    do:
      assign
         lCheck = false
         cFormatNumber = ""
         lError        = false.
      apply "entry" to fPhone.
      Validatenumber(input fPhone:input-value, output cFormatNumber,output lError).

      if not lError 
       then
        do: 
          fPhone:screen-value = string(cFormatNumber).
          if bSaveClose:label = "Save" then
           run cacheNewPersonData in this-procedure(fPhone:handle).
        end.
       else
        do:
          message "Invalid Phone Number" view-as alert-box information buttons ok.
          lCheck = false.
          return.
        end.    
    end.
   else 
    do:
     assign 
        fPhone:format = 'X(256)'
        fPhone:screen-value = ""
        lCheck = true.
    end.
  
  assign lCheck = true.
  apply "entry" to btnCheckDuplicate.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowClose Dialog-Frame 
PROCEDURE windowClose :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  apply "end-error":U to frame dialog-frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkUnsavedChanges Dialog-Frame 
FUNCTION checkUnsavedChanges RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if unsavedChanges 
   then
    do:
      message "There are unsaved changes. Continue?"
         view-as alert-box question buttons yes-no update lupdate as logical.      
      if not lupdate 
       then return true.
      else return false.
    end.
   
  return false.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

