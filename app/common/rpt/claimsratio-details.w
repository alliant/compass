&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pAgentID as character no-undo.
define input parameter pAgentName as character no-undo.
define input parameter pStartDate as datetime no-undo.
define input parameter pEndDate as datetime no-undo.
define input parameter pAgentError as logical no-undo.

/* Local Variable Definitions ---                                       */
define variable dColumnDescription as decimal no-undo.
define variable dColumnAdministrator as decimal no-undo.
define variable cParamList as character no-undo.

{tt/reportclaimsummary.i &tableAlias="data"}
{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.claimID data.statDesc data.assignedTo data.stageDesc data.description data.lastNote data.laeReserve data.laeLTD data.lossReserve data.lossLTD data.recoveries data.pendingRecoveries   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh RECT-42 rParam bExport tDateRange ~
tAgentError brwData 
&Scoped-Define DISPLAYED-OBJECTS tDateRange tAgentError 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE tAgentError AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDateRange AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 25.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-42
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 2.38.

DEFINE RECTANGLE rParam
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 172 BY 2.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.claimID column-label "Claim ID" format "99999999"
data.statDesc column-label "Status" format "x(10)" width 10
data.assignedTo column-label "Administrator" width 20 format "x(100)"
data.stageDesc column-label "Stage" format "x(50)" width 18
data.description column-label "Description" format "x(200)" width 20
data.lastNote column-label "Activity" format "99/99/9999" width 13
data.laeReserve column-label "LAE Reserve!Change" format "(>>,>>>,>>>,>>Z)" width 14
data.laeLTD column-label "LAE Paid" format "(>>,>>>,>>>,>>Z)" width 14
data.lossReserve column-label "Loss Reserve!Change" format "(>>,>>>,>>>,>>Z)" width 14
data.lossLTD column-label "Losses Paid" format "(>>,>>>,>>>,>>Z)" width 14
data.recoveries column-label "Recoveries" format "(>>,>>>,>>>,>>Z)" width 14
data.pendingRecoveries column-label "Pending!Recoveries" format "(>>,>>>,>>>,>>Z)" width 14
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189 BY 24.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bRefresh AT ROW 1.95 COL 10 WIDGET-ID 8 NO-TAB-STOP 
     bExport AT ROW 1.95 COL 2.6 WIDGET-ID 2
     tDateRange AT ROW 2.33 COL 34.6 COLON-ALIGNED NO-LABEL WIDGET-ID 62
     tAgentError AT ROW 2.33 COL 75.8 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     brwData AT ROW 4.1 COL 2 WIDGET-ID 200
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 14
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 20 WIDGET-ID 56
     "Date Range:" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 2.48 COL 22 WIDGET-ID 60
          FONT 6
     "Agent Error:" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 2.48 COL 63.8 WIDGET-ID 64
          FONT 6
     RECT-42 AT ROW 1.48 COL 2 WIDGET-ID 4
     rParam AT ROW 1.48 COL 19 WIDGET-ID 10
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 190.8 BY 27.91 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Claims for Agent"
         HEIGHT             = 27.91
         WIDTH              = 190.8
         MAX-HEIGHT         = 32.19
         MAX-WIDTH          = 216.4
         VIRTUAL-HEIGHT     = 32.19
         VIRTUAL-WIDTH      = 216.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData tAgentError fMain */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Claims for Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Claims for Agent */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Claims for Agent */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run GetData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  /* set the assigned to user */
  std-ch = "".
  publish "GetSysUserName" (data.assignedTo, output std-ch).
  if std-ch = "NOT FOUND"
   then std-ch = data.assignedTo.
  data.assignedTo:screen-value in browse {&browse-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  bExport:sensitive in frame {&frame-name} = available data.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&WINDOW-NAME}:title = "Claims for " + pAgentName.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/sync.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  assign
    tDateRange:screen-value = string(date(pStartDate)) + " to " + string(date(pEndDate))
    tAgentError:screen-value = (if pAgentError then "Yes" else "ALL")
    tDateRange:sensitive = false
    tAgentError:sensitive = false
    .
  
  std-ha = getColumn({&browse-name}:handle, "description").
  IF VALID-HANDLE(std-ha)
   THEN dColumnDescription = std-ha:width * session:pixels-per-column.
  
  std-ha = getColumn({&browse-name}:handle, "assignedTo").
  IF VALID-HANDLE(std-ha)
   THEN dColumnAdministrator = std-ha:width * session:pixels-per-column.
   
  run WindowResized in this-procedure.
  apply "VALUE-CHANGED" to {&browse-name}.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDateRange tAgentError 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh RECT-42 rParam bExport tDateRange tAgentError brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cReportName as character no-undo.
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  cReportName = "Claims_Ratio_Details_" + pAgentID + ".csv".
  std-ch = "".
  publish "GetReportDir" (output std-ch).
  if std-ch = ""
   then std-ch = os-getenv("TEMP") + "\".
  cReportName = std-ch + cReportName.
  
  /* write the headers of the report */
  output to value(cReportName).
  if index(pAgentName,",") > 0
   then put unformatted "~"" + pAgentName + "~"" skip.
   else put unformatted pAgentName skip.
  put unformatted "Date Range: " + string(date(pStartDate)) + " to " + string(date(pEndDate)) skip.
  put unformatted "Agent Error: " + (if pAgentError then "Yes" else "ALL") skip.
  put unformatted " " skip.
  output close.

  run util/exporttocsvbrowseappend.p (string(browse {&browse-name}:handle), cReportName).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
@description Get the data for claims by agent
------------------------------------------------------------------------------*/
  clearData().
  run server/getclaimsbyagent.p (input "",
                                 input pAgentID,
                                 input 0,
                                 input "",
                                 input "",
                                 input pStartDate,
                                 input pEndDate,
                                 output table data,
                                 output std-lo,
                                 output std-ch).
                                 
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else
    do with frame {&frame-name}:
      {&OPEN-QUERY-brwData}
      run sortData in this-procedure (dataSortBy).
      apply "VALUE-CHANGED" to {&browse-name}.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount as integer no-undo.
  define variable hBuffer as handle no-undo.
  define variable tWhereClause as character no-undo initial "".
  
  create buffer hBuffer for table "data".
  
  if pAgentError
   then tWhereClause = "agentError = yes".
   
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  std-ch = string(num-results("{&browse-name}")) + " claim(s) shown".
  status input std-ch in window {&window-name}.
  status default std-ch in window {&window-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define  variable  dDiffWidth as decimal no-undo.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
 
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.
  
  rParam:width-pixels = frame {&frame-name}:width-pixels - rParam:x - 5.
  
  std-ha = getColumn({&browse-name}:handle, "description").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumnDescription + dDiffWidth / 2.
   
  std-ha = getColumn({&browse-name}:handle, "assignedTo").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumnAdministrator + dDiffWidth / 2.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query brwData.
  status input "".
  status default "".
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

