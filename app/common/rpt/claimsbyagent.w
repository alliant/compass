&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Claims by Agent (wcrm06-r.w)
@description The browse shows all of the claims for the agent.

@author John Oliver
@version 1.0
@created 2017/03/01
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/state.i}
{tt/reportclaimsummary.i &tableAlias="data"}
{tt/sysprop.i}

{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/brw-totalData-def.i}
{lib/set-status.i}

define variable dColumnWidth as decimal no-undo.

define variable currAdmin as character no-undo.
define variable currStatus as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.claimID data.statDesc data.agentErrorDesc data.assignedToDesc data.descriptionDesc data.dateOpened data.lastNote data.laeReserve data.laeLTD data.lossReserve data.lossLTD data.recoveries data.costsPaid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh tStateID tAgentID fAdmin RECT-36 ~
fStat RECT-37 brwData 
&Scoped-Define DISPLAYED-OBJECTS tStateID tAgentID fAdmin fStat 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterAdminCombo C-Win 
FUNCTION getFilterAdminCombo RETURNS CHARACTER
  ( input pStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pAdmin as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE fAdmin AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Administrator" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE fStat AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN AUTO-COMPLETION
     SIZE 68.4 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 97 BY 3.57.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.claimID column-label "Claim ID" format "99999999"
data.statDesc column-label "Status" format "x(10)" width 10
data.agentErrorDesc COLUMN-LABEL "Error" FORMAT "x(20)" WIDTH 12
data.assignedToDesc column-label "Assigned To" format "x(100)" width 24

data.descriptionDesc column-label "Description" format "x(500)" width 24
data.dateOpened column-label "Opened" format "99/99/9999" width 13
data.lastNote column-label "Note Date" format "99/99/9999" width 13

data.laeReserve column-label "LAE Reserve" format "(>>,>>>,>>>,>>Z)" width 16
data.laeLTD column-label "LAE LTD" format "(>>,>>>,>>>,>>Z)" width 16
data.lossReserve column-label "Loss Reserve" format "(>>,>>>,>>>,>>Z)" width 16
data.lossLTD column-label "Losses LTD" format "(>>,>>>,>>>,>>Z)" width 16
data.recoveries column-label "Recoveries" format "(>>,>>>,>>>,>>Z)" width 16

data.costsPaid column-label "Costs Paid" format "(>>,>>>,>>>,>>Z)" width 16
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 216 BY 21.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.1 COL 89.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 2.1 COL 82 WIDGET-ID 4 NO-TAB-STOP 
     tStateID AT ROW 2.19 COL 10 COLON-ALIGNED WIDGET-ID 166
     tAgentID AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 84
     fAdmin AT ROW 2.19 COL 114 COLON-ALIGNED WIDGET-ID 300
     fStat AT ROW 3.38 COL 114 COLON-ALIGNED WIDGET-ID 168
     brwData AT ROW 5.29 COL 2 WIDGET-ID 200
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 100.4 WIDGET-ID 60
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3.6 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 98.6 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 218 BY 26.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Claims By Agent"
         HEIGHT             = 26.33
         WIDTH              = 218
         MAX-HEIGHT         = 29.43
         MAX-WIDTH          = 218.2
         VIRTUAL-HEIGHT     = 29.43
         VIRTUAL-WIDTH      = 218.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData RECT-37 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.91
       COLUMN          = 82
       HEIGHT          = .48
       WIDTH           = 14.6
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Claims By Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Claims By Agent */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Claims By Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAdmin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAdmin C-Win
ON VALUE-CHANGED OF fAdmin IN FRAME fMain /* Administrator */
DO:
  setFilterCombos(self:label).
  displayStatus().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStat C-Win
ON VALUE-CHANGED OF fStat IN FRAME fMain /* Status */
DO:
  setFilterCombos(self:label).
  displayStatus().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/report-progress-bar.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
initializeStatusWindow({&window-name}:handle).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}
    
    /* set the value */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'assignedTo'" &var=dColumnWidth}
  
  clearData().
  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "claimsbyagent.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "claimsbyagent.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tStateID tAgentID fAdmin fStat 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh tStateID tAgentID fAdmin RECT-36 fStat RECT-37 brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Claims_By_Agent"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tStartTime as datetime no-undo.
  define buffer data for data.

  /* cleanup before getting data */
  clearData().
  tStartTime = now.
  run server/getclaimsbyagent.p (input tStateID:screen-value in frame {&frame-name},
                                 input tAgentID:screen-value in frame {&frame-name},
                                 input 0,
                                 input "",
                                 input "",
                                 input ?,
                                 input ?,
                                 output table data,
                                 output std-lo,
                                 output std-ch).
                           
  if not std-lo
   then message "ClaimsByAgent failed: " std-ch view-as alert-box warning.
   else
    do:
      run SetProgressStatus.
      for each data exclusive-lock:
        /* get the status */
        publish "GetSysPropDesc" ("CLM", "ClaimDescription", "Status", data.stat, output data.statDesc).
        /* get the name */
        publish "GetSysUserName" (data.assignedTo, output data.assignedToDesc).
        data.costsPaid = data.laeLTD + data.lossLTD - data.recoveries.
      end.
      setFilterCombos("ALL").
      run SetProgressEnd.
      appendStatus("in " + trim(string(interval(now, tStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
      displayStatus().
      assign
        std-lo = can-find(first data)
        /* enable the buttons and filters */
        fAdmin:sensitive = std-lo
        fStat:sensitive = std-lo
        bExport:sensitive = std-lo
        .
    end.
      
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStatus as character no-undo.
  define variable cAdmin as character no-undo.
  
  define variable tWhereClause as character no-undo.
  
  define variable hQuery as handle no-undo.
  
  do with frame {&frame-name}:
    assign
      cStatus = fStat:screen-value
      cAdmin = fAdmin:screen-value
      .
  end.
  
  /* build the query */
  if cStatus <> "ALL" and cStatus <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stat='" + cStatus + "'".
  if cAdmin <> "ALL" and cAdmin <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "assignedTo='" + cAdmin + "'".
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  setStatus(string(num-results("{&browse-name}")) + " claim(s) shown").
  {lib/brw-totalData.i &integerOnly=true}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  if totalAdded
   then iOffset = 23.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - iOffset.
  
  /* resize the column */
  {lib/resize-column.i &col="'assignedTo,descriptionDesc'" &var=dColumnWidth}
   
  {lib/brw-totalData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with FRAME {&frame-name}:
    assign
      /* default the filter */
      fAdmin:screen-value = "ALL"
      fStat:screen-value = "ALL"
      /* disable the buttons and filters */
      fAdmin:sensitive = false
      fStat:sensitive = false
      bExport:sensitive = false
      .
  end.
  clearStatus().
  close query brwData.
  empty temp-table data.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterAdminCombo C-Win 
FUNCTION getFilterAdminCombo RETURNS CHARACTER
  ( input pStatus as character ) :
/*------------------------------------------------------------------------------
@description Get a list of states given the status
------------------------------------------------------------------------------*/
  define variable cList as character no-undo.
  define variable cName as character no-undo.
  define buffer data for data.
 
  cList = "ALL,ALL".
  for each data no-lock:
     
    if pStatus <> "ALL" and pStatus <> data.stat
     then next.
   
    if lookup(data.stateID,cList) = 0
     then 
      do:
        cName = "".
        publish "GetSysUserName" (data.assignedTo, output cName).
        if cName = ""
         then cName = data.assignedTo.
        if lookup(data.assignedTo,cList) = 0
         then cList = addDelimiter(cList,",") + cName + "," + data.assignedTo.
      end.
  end.
  {lib/brw-totalData.i}
  RETURN cList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pAdmin as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent statuses for the state
------------------------------------------------------------------------------*/
  define variable cList as character no-undo.
  define buffer data for data.
 
  cList = "ALL,ALL".
  for each data no-lock:
  
    if pAdmin <> "ALL" and pAdmin <> data.assignedTo
     then next.
   
    if lookup(data.stat,cList) = 0
     then cList = addDelimiter(cList,",") + data.statDesc + "," + data.stat.
  end.
  RETURN cList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character ) :
/*------------------------------------------------------------------------------
@description Sets the filter combo boxes
------------------------------------------------------------------------------*/
  /* get the manager and status based on the state */
  do with frame {&frame-name}:
    assign
      currAdmin = fAdmin:screen-value
      currStatus = fStat:screen-value
      .
    case pName:
     when "ALL" then
      assign
        fAdmin:list-item-pairs = getFilterAdminCombo(currStatus)
        fStat:list-item-pairs = getFilterStatusCombo(currAdmin)
        .
     when "Administrator" then
      assign
        fStat:list-item-pairs = getFilterStatusCombo(currAdmin)
        .
     when "Status" then
      assign
        fAdmin:list-item-pairs = getFilterAdminCombo(currStatus)
        .
    end case.
    /* set the state */
    if lookup(currAdmin,fAdmin:list-item-pairs) > 0
     then fAdmin:screen-value = currAdmin.
     else fAdmin:screen-value = "ALL".
    /* set the status */
    if lookup(currStatus,fStat:list-item-pairs) > 0
     then fStat:screen-value = currStatus.
     else fStat:screen-value = "ALL".
  end.
  run sortData in this-procedure ("").
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

