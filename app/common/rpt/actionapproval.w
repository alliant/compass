&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{tt/agent.i}
{tt/attorney.i}
{tt/findingaction.i}
{tt/findingaction.i &tableAlias="sendaction"}
{tt/period.i}
/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{lib/std-def.i} 
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName="findingaction"}

define variable dColumnWidth as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwNotify

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES findingaction

/* Definitions for BROWSE brwNotify                                     */
&Scoped-define FIELDS-IN-QUERY-brwNotify findingaction.actionID findingaction.statDesc findingaction.severityDesc findingaction.actionTypeDesc findingaction.entity findingaction.entityID findingaction.ownerDesc findingaction.actionCreatedDate findingaction.dueDate findingaction.approvalStatDesc findingaction.sentDate findingaction.approvalDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwNotify   
&Scoped-define SELF-NAME brwNotify
&Scoped-define QUERY-STRING-brwNotify for each findingaction
&Scoped-define OPEN-QUERY-brwNotify open query {&SELF-NAME} for each findingaction.
&Scoped-define TABLES-IN-QUERY-brwNotify findingaction
&Scoped-define FIRST-TABLE-IN-QUERY-brwNotify findingaction


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwNotify}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-4 RECT-5 RECT-2 bGo bExport tMonth ~
fEntity cbSubject bSend tYear fStatus fOwner brwNotify 
&Scoped-Define DISPLAYED-OBJECTS tMonth fEntity cbSubject tYear fStatus ~
fOwner 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bGo 
     LABEL "OK" 
     SIZE 7.2 BY 1.71 TOOLTIP "Click to get actions".

DEFINE BUTTON bSend 
     LABEL "Send" 
     SIZE 7.2 BY 1.71 TOOLTIP "Send for Approval".

DEFINE VARIABLE cbSubject AS CHARACTER INITIAL "ALL" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN AUTO-COMPLETION
     SIZE 42 BY 1 NO-UNDO.

DEFINE VARIABLE fEntity AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE fOwner AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 34.6 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Planned","P",
                     "Opened","O"
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1",0
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 15 BY 3.57.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43 BY 3.57.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.6 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwNotify FOR 
      findingaction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwNotify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwNotify C-Win _FREEFORM
  QUERY brwNotify DISPLAY
      findingaction.actionID width 12
findingaction.statDesc width 12
findingaction.severityDesc width 12
findingaction.actionTypeDesc width 15
findingaction.entity width 15
findingaction.entityID width 12
findingaction.ownerDesc width 20
findingaction.actionCreatedDate width 15
findingaction.dueDate width 15
findingaction.approvalStatDesc width 15
findingaction.sentDate width 15
findingaction.approvalDate width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 187 BY 15.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bGo AT ROW 2.1 COL 28 WIDGET-ID 224
     bExport AT ROW 2.1 COL 35.4 WIDGET-ID 236
     tMonth AT ROW 2.19 COL 9 COLON-ALIGNED WIDGET-ID 244
     fEntity AT ROW 2.19 COL 52 COLON-ALIGNED WIDGET-ID 26
     cbSubject AT ROW 2.19 COL 72 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     bSend AT ROW 2.43 COL 122.4 WIDGET-ID 4
     tYear AT ROW 3.38 COL 9 COLON-ALIGNED WIDGET-ID 248
     fStatus AT ROW 3.38 COL 52 COLON-ALIGNED WIDGET-ID 242
     fOwner AT ROW 3.38 COL 79.4 COLON-ALIGNED WIDGET-ID 220
     brwNotify AT ROW 5.29 COL 2 WIDGET-ID 200
     "Parameter" VIEW-AS TEXT
          SIZE 10.2 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 250
     "Approval" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 1.24 COL 119.6 WIDGET-ID 32
     "Filter" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 45.4 WIDGET-ID 152
     RECT-4 AT ROW 1.48 COL 2 WIDGET-ID 226
     RECT-5 AT ROW 1.48 COL 44.4 WIDGET-ID 234
     RECT-2 AT ROW 1.48 COL 118.6 WIDGET-ID 30
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 189 BY 20.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Actions By Month"
         HEIGHT             = 20.1
         WIDTH              = 189
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwNotify fOwner DEFAULT-FRAME */
ASSIGN 
       brwNotify:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwNotify
/* Query rebuild information for BROWSE brwNotify
     _START_FREEFORM
open query {&SELF-NAME} for each findingaction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwNotify */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME DEFAULT-FRAME:HANDLE
       ROW             = 3.91
       COLUMN          = 28
       HEIGHT          = .48
       WIDTH           = 14.6
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(fOwner:HANDLE IN FRAME DEFAULT-FRAME).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Actions By Month */
or endkey of {&WINDOW-NAME} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Actions By Month */
do:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Actions By Month */
DO:
  run windowresized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME DEFAULT-FRAME /* OK */
do:   
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwNotify
&Scoped-define SELF-NAME brwNotify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON DEFAULT-ACTION OF brwNotify IN FRAME DEFAULT-FRAME
do:
  if not available findingaction or findingaction.actionID = 0
   then return.
   
  publish "OpenWindowForAction" (findingaction.findingID, findingaction.actionID, "dialogactionmodify.w").
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON ROW-DISPLAY OF brwNotify IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON START-SEARCH OF brwNotify IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSend C-Win
ON CHOOSE OF bSend IN FRAME DEFAULT-FRAME /* Send */
do:
  run sendData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSubject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSubject C-Win
ON VALUE-CHANGED OF cbSubject IN FRAME DEFAULT-FRAME
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEntity C-Win
ON VALUE-CHANGED OF fEntity IN FRAME DEFAULT-FRAME /* Entity */
DO:
  cbSubject:hidden = false.
  cbSubject:sensitive = can-find(first findingaction).
  cbSubject:delimiter = ",".
  cbSubject:inner-lines = 15.
  run AgentComboHide in this-procedure (true).
  run AttorneyComboHide in this-procedure (true).
  case self:screen-value:
   when "ALL" then
    do:
      cbSubject:inner-lines = 1.
      cbsubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
   when "Agent" then
    do:
      cbsubject:delimiter = {&msg-dlm}.
      run AgentComboHide in this-procedure (false).
    end.
   when "Attorney" then
    do:
      cbsubject:delimiter = {&msg-dlm}.
      run AttorneyComboHide in this-procedure (false).
    end.
   when "Company" then
    do:
      cbsubject:hidden = true. 
    end.
   when "Department" then
    do:
      std-ch = "".
      publish "GetDeptCombo" (output std-ch).
      if std-ch > ""
       then cbSubject:list-item-pairs = "ALL,ALL," + std-ch.
       else cbSubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
  end case.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOwner
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOwner C-Win
ON VALUE-CHANGED OF fOwner IN FRAME DEFAULT-FRAME /* Owner */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME DEFAULT-FRAME /* Status */
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth C-Win
ON VALUE-CHANGED OF tMonth IN FRAME DEFAULT-FRAME /* Month */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME DEFAULT-FRAME /* Year */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i}
{lib/brw-main.i}
{lib/set-filter.i &label="Entity"  &id="entity"   &value="entity"}
{lib/set-filter.i &label="Status"  &id="stat"     &value="statDesc"}
{lib/set-filter.i &label="Owner"   &id="ownerID"  &value="ownerDesc"}
{lib/set-filter.i &label="Subject" &id="entityID" &value="entityID" &f=cbSubject &populate=false}
{lib/set-button.i &label="Go"   &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="Send" &image="images/bell.bmp" &inactive="images/bell-i.bmp"}
setButtons().

initializeStatusWindow({&window-name}:handle).

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  {lib/get-attorney-list.i &combo=cbsubject &addAll=true}
  {lib/get-agent-list.i &combo=cbsubject &addAll=true}
  {lib/get-period-list.i &mth=tMonth &yr=tYear}
  {lib/set-current-value.i &mth=tMonth &yr=tYear}
  clearData().
  bSend:sensitive = false.
  apply "VALUE-CHANGED" to fEntity.

  {lib/get-column-width.i &col="'ownerDesc'" &var=dColumnWidth}
  run windowResized in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "actionapproval.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "actionapproval.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tMonth fEntity cbSubject tYear fStatus fOwner 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-4 RECT-5 RECT-2 bGo bExport tMonth fEntity cbSubject bSend tYear 
         fStatus fOwner brwNotify 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwNotify:num-results = 0 
   then
    do: 
      MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.

  &scoped-define ReportName "Actions_By_Month"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(brwNotify), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(brwNotify), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  setFilterCombos("ALL").
  if dataSortBy = ""
   then
    assign
      dataSortBy = "actionID"
      dataSortDesc = false
      .
  run sortData (dataSortBy).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  
  empty temp-table findingaction.
  dStartTime = now.
  clearData().
  do with frame {&frame-name}:
    publish "SetProgressStatus".
    run server/getfindingactions.p (input "P,O",
                                    input "A",
                                    input tMonth:input-value,
                                    input tYear:input-value,
                                    input "", /*entity*/
                                    input "", /*entityID*/
                                    input "",
                                    input 0,
                                    input 0,
                                    output table findingaction,
                                    output std-lo,
                                    output std-ch).
  end.
  if not std-lo
   then message std-ch view-as alert-box warning.
   else
    do with frame {&frame-name}:
      publish "SetProgressStatus".
      for each findingaction exclusive-lock:
        publish "GetSysUserName" (findingaction.ownerID, output findingaction.ownerDesc).
        publish "GetSysUserName" (findingaction.findingUID, output findingaction.findingUIDDesc).
        publish "GetSysUserName" (findingaction.actionUID, output findingaction.actionUIDDesc).
        publish "GetSysPropDesc" ("CAM", "Finding", "Status", findingaction.stage, output findingaction.stageDesc).
        publish "GetSysPropDesc" ("CAM", "Finding", "Severity", findingaction.severity, output findingaction.severityDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "ActionType", findingaction.actionType, output findingaction.actionTypeDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "Status", findingaction.stat, output findingaction.statDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "CompleteStatus", findingaction.completeStatus, output findingaction.completeStatusDesc).
        publish "GetSysPropDesc" ("CAM", "ActionApproval", "Status", findingaction.approvalStat, output findingaction.approvalStatDesc).
      end.
      run filterData in this-procedure.
      appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
      publish "SetProgressEnd".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sendData C-Win 
PROCEDURE sendData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hQuery  as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hAction as handle no-undo.

  empty temp-table sendaction.
  create buffer hBuffer for table "findingaction".
  create buffer hAction for table "sendaction".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each findingaction no-lock " + getWhereClause()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hAction:buffer-create().
    hAction:buffer-copy(hBuffer).
    hQuery:get-next().
  end.
  hQuery:query-close().
  
  run server/sendactionapproval.p (input tMonth:input-value in frame {&frame-name},
                                   input tYear:input-value in frame {&frame-name},
                                   input table sendaction,
                                   output std-lo,
                                   output std-ch).

  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else
    do:
      run getData in this-procedure.
      bSend:sensitive in frame {&frame-name} = false.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.

  tWhereClause = getWhereClause().
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}

  enableButtons(can-find(first findingaction)).
  bSend:sensitive in frame {&frame-name} = true.
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowresized C-Win 
PROCEDURE windowresized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10. 
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  {lib/resize-column.i &col="'ownerDesc'" &var=dColumnWidth}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  clearStatus().
  close query {&browse-name}.
  empty temp-table findingaction.
  enableButtons(false).
  enableFilters(false).
  cbSubject:sensitive in frame {&frame-name} = false.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

