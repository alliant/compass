&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agents for Period (wcrm02-r.w)
@description The user chooses an agent and will retrieve the batch 
             figures for the period selected

@author John Oliver
@version 1.0
@created 2017/03/01
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/batch.i}
{tt/state.i}
{tt/batchaction.i}
{tt/period.i}

define temp-table agentperiod
  field agentID as char
  field name as char
  field stat as char
  field stateID as char
  field numBatches as int
  field numFiles as int
  field numPolicies as int
  field numEndorsements as int
  field liabilityAmount as dec
  field grossPremium as dec
  field netPremium as dec
  field retainedPremium as decimal
  /* for client */
  field statDesc as character
  .

{lib/std-def.i}

define variable hData as handle no-undo.

/* for the progress bar */
define variable iCounterMax as integer no-undo.
define variable iCounter as integer no-undo.

define variable tStatus as char no-undo.
define variable dColumnName as decimal no-undo.

define variable currState as character no-undo.
define variable currStatus as character no-undo.

define variable periodDate as datetime no-undo.
{lib/winlaunch.i}

{lib/getstatename.i}
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/do-wait.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentperiod

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData agentperiod.stateID agentperiod.agentID agentperiod.name agentperiod.statDesc agentperiod.numBatches agentperiod.numFiles agentperiod.numPolicies agentperiod.numEndorsements agentperiod.liabilityAmount agentperiod.grossPremium agentperiod.netPremium agentperiod.retainedPremium   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH agentperiod
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH agentperiod.
&Scoped-define TABLES-IN-QUERY-brwData agentperiod
&Scoped-define FIRST-TABLE-IN-QUERY-brwData agentperiod


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh tMonth tYear fState fStat fAgents ~
fBatch tBatches brwData tGrossPremium tAgents tNetPremium 
&Scoped-Define DISPLAYED-OBJECTS tMonth tYear fState fStat fAgents fBatch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitizeReportButtons C-Win 
FUNCTION sensitizeReportButtons RETURNS LOGICAL PRIVATE
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE fStat AS CHARACTER FORMAT "X(256)" INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE tAgents AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Agents" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tBatches AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Batches" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE fAgents AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Yes", 1,
"No", 2,
"Both", 3
     SIZE 23.4 BY .95 NO-UNDO.

DEFINE VARIABLE fBatch AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Complete", "C",
"Incomplete", "I",
"Both", "B"
     SIZE 36 BY .95 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65 BY 3.57.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 81 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      agentperiod SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      agentperiod.stateID column-label "State"
agentperiod.agentID column-label "AgentID" format "x(12)"
agentperiod.name column-label "Agent" format "x(250)" width 40
agentperiod.statDesc column-label "Status" format "x(20)" width 12
agentperiod.numBatches column-label "Batches"
agentperiod.numFiles column-label "Files"
agentperiod.numPolicies column-label "Policies"
agentperiod.numEndorsements column-label "Endorse."
agentperiod.liabilityAmount column-label "Total!Liability" format "-zzz,zzz,zzz,zz9.99"
agentperiod.grossPremium column-label "Gross!Premium" format "-zzz,zzz,zzz,zz9.99"
agentperiod.netPremium column-label "Net!Premium" format "-zzz,zzz,zzz,zz9.99"
agentperiod.retainedPremium column-label "Retained" format "-zzz,zzz,zzz,zz9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 208.6 BY 21.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.19 COL 49.4 WIDGET-ID 2 NO-TAB-STOP 
     bPrint AT ROW 2.19 COL 56.8 WIDGET-ID 14 NO-TAB-STOP 
     bRefresh AT ROW 2.19 COL 42 WIDGET-ID 4 NO-TAB-STOP 
     tMonth AT ROW 2.91 COL 10 COLON-ALIGNED WIDGET-ID 16
     tYear AT ROW 2.91 COL 27 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     fState AT ROW 2.19 COL 73.6 COLON-ALIGNED WIDGET-ID 34
     fStat AT ROW 3.38 COL 73.6 COLON-ALIGNED WIDGET-ID 160
     fAgents AT ROW 2.29 COL 110.6 NO-LABEL WIDGET-ID 28
     fBatch AT ROW 3.48 COL 110.6 NO-LABEL WIDGET-ID 164
     tBatches AT ROW 2.19 COL 157 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     brwData AT ROW 5.29 COL 2 WIDGET-ID 200
     tGrossPremium AT ROW 2.19 COL 185.8 COLON-ALIGNED WIDGET-ID 24 NO-TAB-STOP 
     tAgents AT ROW 3.33 COL 157 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tNetPremium AT ROW 3.33 COL 185.8 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 67.6 WIDGET-ID 60
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 56
     "Batch:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 3.57 COL 103.8 WIDGET-ID 162
     "Reported:" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 2.43 COL 100.6 WIDGET-ID 32
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 66.6 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 210.6 BY 26 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Remittances by Agent"
         HEIGHT             = 26
         WIDTH              = 210.6
         MAX-HEIGHT         = 27.62
         MAX-WIDTH          = 212.4
         VIRTUAL-HEIGHT     = 27.62
         VIRTUAL-WIDTH      = 212.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tBatches fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrint IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgents IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tAgents:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tBatches IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tBatches:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tGrossPremium:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tNetPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNetPremium:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentperiod.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4
       COLUMN          = 42
       HEIGHT          = .48
       WIDTH           = 22
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Remittances by Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Remittances by Agent */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Remittances by Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  sensitizeReportButtons(false).
  run getData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgents C-Win
ON VALUE-CHANGED OF fAgents IN FRAME fMain
DO:
  if self:input-value = 2
   then fBatch:sensitive in frame {&frame-name} = false.
   else fBatch:sensitive in frame {&frame-name} = true.
  run filterData in this-procedure.
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fBatch C-Win
ON VALUE-CHANGED OF fBatch IN FRAME fMain
DO:
  run filterData in this-procedure.
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStat C-Win
ON VALUE-CHANGED OF fStat IN FRAME fMain /* Status */
DO:
  run filterData.
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  run filterData in this-procedure.
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth C-Win
ON VALUE-CHANGED OF tMonth IN FRAME fMain /* Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bPrint:load-image("images/pdf.bmp").
bPrint:load-image-insensitive("images/pdf-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

publish "GetAgents" (output table agent).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=tMonth &yr=tYear}
    {lib/get-state-list.i &combo=fState &addAll=true}
    /* set the values */
    {lib/set-current-value.i &mth=tMonth &yr=tYear}
  
    fState:screen-value = "ALL".
    fStat:screen-value = "ALL".
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'name'" &var=dColumnName}
   
  run windowResized.
  clearData().
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "agentforperiod.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "agentforperiod.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tMonth tYear fState fStat fAgents fBatch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh tMonth tYear fState fStat fAgents fBatch tBatches brwData 
         tGrossPremium tAgents tNetPremium 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Agents_For_Period"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table agentperiod.
  
  doWait(true).
  do with frame {&frame-name}:
    for each agent no-lock:
     
      if fState:screen-value <> "ALL" and agent.stateID <> fState:screen-value
       then next.
      
      if fStat:screen-value <> "ALL" and agent.stat <> fStat:screen-value
       then next.
     
      create agentperiod.
      assign
        agentperiod.agentID = agent.agentID
        agentperiod.name = agent.name
        agentperiod.stat = agent.stat
        agentperiod.stateID = agent.stateID
        .
      publish "GetSysPropDesc" ("AMD", "Agent", "Status", agent.stat, output std-ch).
      agentperiod.statDesc = std-ch.
     
      for each batch no-lock
         where batch.agentID = agent.agentID:
        
        if fBatch:screen-value = "C" and batch.stat <> "C"
         then next.
        
        if fBatch:screen-value = "I" and batch.stat = "C"
         then next.
        
        assign
          agentperiod.numBatches = agentperiod.numBatches + 1
          agentperiod.numFiles = agentperiod.numFiles + batch.fileCount
          agentperiod.numPolicies = agentperiod.numPolicies + batch.policyCount
          agentperiod.numEndorsements = agentperiod.numEndorsements + batch.endorsementCount
          agentperiod.liability = agentperiod.liability + batch.liabilityDelta
          agentperiod.grossPremium = agentperiod.grossPremium + batch.grossPremiumDelta
          agentperiod.netPremium = agentperiod.netPremium + batch.netPremiumDelta
          agentperiod.retainedPremium = agentperiod.retainedPremium + batch.retainedPremiumDelta.
      end.
     
      if fAgents:input-value = 1 and agentperiod.numBatches = 0
       then delete agentperiod.
      
      if fAgents:input-value = 2 and agentperiod.numBatches <> 0
       then delete agentperiod.
    end.
  end.
  
  setFilterCombos("ALL").
  displayTotals().
  doWait(false).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer period for period.
  define buffer batch for batch.

  clearData().
  run SetProgressCounter (2).
  pbMinStatus().
  empty temp-table batch.
  run SetProgressStatus.

  do with frame {&frame-name}:
    for each period no-lock
       where period.periodMonth = tMonth:input-value
         and period.periodYear = tYear:input-value:
         
      run server/getagentsforperiod.p (period.periodID,
                                       output table batch,
                                       output table batchaction,
                                       output std-lo,
                                       output std-ch).
                               
      if not std-lo
       then message "AgentsForPeriod failed: " std-ch view-as alert-box warning.
       else 
        do:
          run SetProgressStatus.
          run filterData in this-procedure.
          run SetProgressEnd.
        end.
    end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tTitle as char no-undo.
 def var tFilename as char no-undo.


 if query brwData:num-results = 0
  then
   do: 
    MESSAGE "There is nothing to print"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 if fAgents:input-value in frame fMain = 1 
  then tTitle = "Reported ".
  else
 if fAgents:input-value in frame fMain = 2 
  then tTitle = "Unreported ".
  else tTitle = "All ".

 if fState:input-value in frame fMain <> "ALL" 
  then tTitle = tTitle + getStateName(fState:input-value in frame fMain) + " ".

 tTitle = tTitle + "Agents in " 
    + getMonthName(tMonth:input-value in frame fMain) 
    + " " + string(tYear:input-value in frame fMain, "9999").


 publish "IsPeriodOpen" (tMonth:input-value in frame fMain,
                         tYear:input-value in frame fMain,
                         output std-lo).

 tTitle = tTitle + " (" + (if std-lo then "Open" else "Closed") + ")".

 tFilename = "AgentsForPeriod_" 
    + string(tYear:input-value in frame fMain, "9999")
    + string(tMonth:input-value in frame fMain, "99")
    + ".pdf".

 run rpt/agentforperiod-pdf.p (input table agentperiod,
                               input tTitle,
                               input tFilename).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressCounter C-Win 
PROCEDURE SetProgressCounter :
/*------------------------------------------------------------------------------
@description Sets the progress bar count
------------------------------------------------------------------------------*/
  def input parameter pCounter as int no-undo.
  assign
    iCounterMax = pCounter
    iCounter = 0
    .
  pbMinStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressEnd C-Win 
PROCEDURE SetProgressEnd :
/*------------------------------------------------------------------------------
@description Makes the progress bar minimum after setting to 100
------------------------------------------------------------------------------*/
  pbUpdateStatus(100, 1).
  pbMinStatus().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressStatus C-Win 
PROCEDURE SetProgressStatus :
/*------------------------------------------------------------------------------
@description Updates the progress bar
------------------------------------------------------------------------------*/
  iCounter = iCounter + 1.
  pbUpdateStatus(int(iCounter / iCounterMax * 100), 0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStatus as character no-undo.
  define variable cState as character no-undo.
  
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
    assign
      cStatus = fStat:screen-value
      cState = fState:screen-value
      .
  end.
  
  /* build the query */
  if cStatus <> "ALL" and cStatus <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stat='" + cStatus + "'".
  if cState <> "ALL" and cState <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stateID='" + cState + "'".
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  
  setStatus(string(num-results("{&browse-name}")) + " agent(s) found for " + tMonth:screen-value in frame {&frame-name} + "/" + tYear:screen-value in frame {&frame-name} + "  (" + string(now,"99/99/99 HH:MM:SS") + ")").
  displayStatus().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dDiffWidth as decimal no-undo.
  
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* get the difference between the old and new width\height */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
 
  /* the browse column */
  {lib/resize-column.i &col="'name'" &var=dColumnName}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      /* default the filter */
      fState:screen-value = "ALL"
      fStat:screen-value = "ALL"
      fAgents:screen-value = "1"
      fBatch:screen-value = "C"
      /* disable the buttons and filters */
      bPrint:sensitive = false
      bExport:sensitive = false
      fState:sensitive = false
      fStat:sensitive = false
      fAgents:sensitive = false
      fBatch:sensitive = false
      .
  end.
  close query brwData.
  clearStatus().
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer agentperiod for agentperiod.

 assign
   tBatches = 0
   tAgents = 0
   tGrossPremium = 0
   tNetPremium = 0
   .
 for each agentperiod
  break by agentperiod.agentID:
  
  if fAgents:input-value in frame {&frame-name} = 1
   and agentperiod.numBatches = 0 then next.

  if fAgents:input-value in frame {&frame-name} = 2
   and agentperiod.numBatches > 0 then next.

  tBatches = tBatches + agentperiod.numBatches.
  if first-of(agentperiod.agentID) 
   then tAgents = tAgents + 1.
  tGrossPremium = tGrossPremium + agentperiod.grossPremium.
  tNetPremium = tNetPremium + agentperiod.netPremium.
 end.

 display
   tBatches
   tAgents
   tGrossPremium
   tNetPremium
  with frame fMain.
 pause 0.

 RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pStatus as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cStateList as character no-undo.
  define variable cName as character no-undo.
  define buffer agentperiod for agentperiod.
 
  cStateList = "ALL,ALL".
  for each agentperiod no-lock:
     
    if pStatus <> "ALL" and pStatus <> agentperiod.stat
     then next.
   
    if lookup(agentperiod.stateID,cStateList) = 0
     then 
      assign
        cStateList = addDelimiter(cStateList,",") + getStateName(agentperiod.stateID) + "," + agentperiod.stateID
        .
  end.
  RETURN cStateList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent statuses for the state
------------------------------------------------------------------------------*/
  define variable cStatusList as character no-undo.
  define variable cStatus as character no-undo.
  define buffer agentperiod for agentperiod.
  
  cStatusList = "ALL,ALL".
  for each agentperiod no-lock:
    
    if pState <> "ALL" and pState <> agentperiod.stateID
     then next.
    
    if lookup(agentperiod.stat,cStatusList) = 0
     then 
      do:
        cStatus = "".
        publish "GetSysPropDesc" ("AMD", "Agent", "Status", agentperiod.stat, output cStatus).
        if cStatus = ""
         then cStatus = agentperiod.stat.
        cStatusList = addDelimiter(cStatusList,",") + cStatus + "," + agentperiod.stat.
      end.
  end.
  RETURN cStatusList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Sets the progress bar back to the minimum value
------------------------------------------------------------------------------*/
  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT) :
/*------------------------------------------------------------------------------
@description: Sets the progress bar to pPercentage
------------------------------------------------------------------------------*/
  {&WINDOW-NAME}:move-to-top().
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0
     then
      do:
        session:set-wait-state("general").
        pause pPauseSeconds no-message.
        session:set-wait-state("").
      end.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitizeReportButtons C-Win 
FUNCTION sensitizeReportButtons RETURNS LOGICAL PRIVATE
  ( input pEnable as logical ) :

  do with frame {&frame-name}:
    assign
      std-lo = can-find(first agentperiod)
      tYear:sensitive = pEnable
      tMonth:sensitive = pEnable
      bRefresh:sensitive = pEnable
      bPrint:sensitive = pEnable and std-lo
      bExport:sensitive = pEnable and std-lo
      fState:sensitive = pEnable and std-lo
      fStat:sensitive = pEnable and std-lo
      fAgents:sensitive = pEnable and std-lo
      fBatch:sensitive = pEnable and std-lo
      .
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character ) :
/*------------------------------------------------------------------------------
@description Sets the filter combo boxes
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      currStatus = fStat:screen-value
      currState = fState:screen-value
      .
    case pName:
     when "ALL" then
      assign
        fStat:list-item-pairs = getFilterStatusCombo(currState)
        .
     when "State" then
      assign
        fStat:list-item-pairs = getFilterStatusCombo(currState)
        .
    end case.
    /* set the status */
    if lookup(currStatus,fStat:list-item-pairs) > 0
     then fStat:screen-value = currStatus.
     else fStat:screen-value = "ALL".
  end.
  run sortData in this-procedure (dataSortBy).
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

