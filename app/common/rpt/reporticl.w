&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name ICL Report
@description Gets the data for the ICL report

@author John Oliver
@version 1.0
@created 2018/11/06
@notes 
@modification
   date         Name           Description
   06/10/2022   SA             Task 93485 - Modified logic in 'getData'
                               to get regionDesc from 'GetSysCodeDesc'.
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/reporticl.i &tableAlias="data"}
{tt/reporticl.i &tableAlias="tempdata"}
{tt/reporticl.i &tableAlias="exportdata"}

{lib/open-window.i}
{lib/add-delimiter.i}
{lib/std-def.i}

define variable cYearList as character no-undo.
define variable hExport   as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bConfig tYear fRegion bGo fStatus fState ~
fAgent fManager RECT-36 RECT-37 
&Scoped-Define DISPLAYED-OBJECTS tYear fRegion fStatus fState fAgent ~
fManager 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( input pSortColumn as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doFilterSort C-Win 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getTotal C-Win 
FUNCTION getTotal RETURNS DECIMAL PRIVATE
  ( input table for tempdata,
    input pType as character  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bConfig  NO-FOCUS
     LABEL "Config" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE fAgent AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fRegion AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "         0" 
     DROP-DOWN-LIST
     SIZE 13 BY 1
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 3.1.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 94.6 BY 3.1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bConfig AT ROW 1.91 COL 40.8 WIDGET-ID 336 NO-TAB-STOP 
     tYear AT ROW 2.52 COL 9 COLON-ALIGNED WIDGET-ID 310
     bExport AT ROW 1.91 COL 33.4 WIDGET-ID 2 NO-TAB-STOP 
     fRegion AT ROW 1.95 COL 58.2 COLON-ALIGNED WIDGET-ID 316
     bGo AT ROW 1.91 COL 26 WIDGET-ID 4 NO-TAB-STOP 
     fStatus AT ROW 3.14 COL 58.2 COLON-ALIGNED WIDGET-ID 166
     fState AT ROW 1.95 COL 85 COLON-ALIGNED WIDGET-ID 320
     fAgent AT ROW 3.14 COL 85.2 COLON-ALIGNED WIDGET-ID 84
     fManager AT ROW 1.95 COL 115.2 COLON-ALIGNED WIDGET-ID 318
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3.6 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 51.6 WIDGET-ID 314
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 49.6 WIDGET-ID 312
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 175.8 BY 21.19 WIDGET-ID 100.

DEFINE FRAME fBrowse
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 4.81
         SIZE 174 BY 17 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "CPL Monthly Report"
         HEIGHT             = 21.1
         WIDTH              = 176.8
         MAX-HEIGHT         = 29.43
         MAX-WIDTH          = 212.2
         VIRTUAL-HEIGHT     = 29.43
         VIRTUAL-WIDTH      = 212.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fBrowse:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fBrowse
                                                                        */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fBrowse:MOVE-AFTER-TAB-ITEM (fManager:HANDLE IN FRAME fMain)
/* END-ASSIGN-TABS */.

ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.71
       COLUMN          = 26.2
       HEIGHT          = .48
       WIDTH           = 21.8
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* CPL Monthly Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* CPL Monthly Report */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* CPL Monthly Report */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConfig C-Win
ON CHOOSE OF bConfig IN FRAME fMain /* Config */
DO:
  run SetDynamicBrowseColumns.
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run ExportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRegion C-Win
ON VALUE-CHANGED OF fRegion IN FRAME fMain /* Region */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain /* Year */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

dataSortBy = "".
subscribe to "ICLSelected" anywhere.

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i &entity="'ICL'"}
{lib/report-progress-bar.i}
{lib/set-buttons.i}
{lib/set-filters.i &tableName="'tempdata'" &labelName="'State,Manager,Status,Region'" &columnName="'stateID,manager,stat,regionID'" &noSort=true}
{lib/build-dynamic-browse.i &table=data &entity="'ICL'" &keyfield="'agentID'" &useInteger=true  &resizeColumn="'agentName,managerDesc'"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

initializeStatusWindow({&window-name}:handle).

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  {&window-name}:window-state = 2.
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    publish "GetActivityYearList" (output cYearList).
    if cYearList = ""
     then run server/getagentactivityyears.p (output cYearList, output std-lo, output std-ch).
    if cYearList > ""
     then tYear:list-items = cYearList.
     else tYear:list-items = string(year(today)).
    {lib/get-agent-list.i &combo=fAgent &addAll=true &t=tempdata &n=agentName &noPublish=true}
    /* set the values */
    {lib/set-current-value.i &yr=tYear}
  end.
  
  clearData().
  run SetDynamicBrowseColumnWidth ("agentName").
  run windowResized in this-procedure.
  setFilterCombos("ALL").
  
  {&window-name}:window-state = 3.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE buildFieldList C-Win 
PROCEDURE buildFieldList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  run BuildDynamicBrowseColumns.
  for each listfield exclusive-lock:
    case listfield.datatype:
     when "decimal" then listfield.columnWidth = 15.
     when "character" then
      case listfield.columnBuffer:
       when "stateID" then listfield.columnWidth = 8.
       when "regionDesc" or
       when "agentID" then listfield.columnWidth = 12.
       when "agentName" then listfield.columnWidth = 27.
       when "managerDesc" then listfield.columnWidth = 27.
       otherwise listfield.columnWidth = 15.
      end case.
    end case.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "reporticl.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "reporticl.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tYear fRegion fStatus fState fAgent fManager 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bConfig tYear fRegion bGo fStatus fState fAgent fManager RECT-36 
         RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME fBrowse IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fBrowse}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hQuery  as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hTable  as handle no-undo.

  define variable hBrowse as handle no-undo.
  define variable cReport as character no-undo.

  hBrowse = getBrowseHandle().
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      if std-ha:num-results = 0
       then
        do:
          message "No results to export" view-as alert-box warning buttons ok.
          return.
        end.
    end.
    
  /* create the export data temp table */
  empty temp-table exportdata.
  create buffer hBuffer for table "data".
  create buffer hTable for table "exportdata".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each data no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hTable:buffer-create.
    hTable:buffer-copy(hBuffer).
    hQuery:get-next().
  end.
  hQuery:query-close().
  
  if valid-handle(hExport)
   then run ShowWindow in hExport.
   else run rpt/reporticl-export.w persistent set hExport (tYear:input-value in frame {&frame-name}, table exportdata).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.
  define variable dStartTime as datetime no-undo.
    
  /* cleanup before getting data */
  clearData().
  empty temp-table data.
  do with frame {&frame-name}:
    dStartTime = now.
    std-lo = false.
    run server/reporticl.p (input tYear:input-value,
                            input "",
                            output table data,
                            output std-lo,
                            output std-ch).
  end.
  if not std-lo
   then message "ICLReport failed: " + std-ch view-as alert-box warning.
   else
    do:
      run SetProgressStatus.
      for each data exclusive-lock:
        /* get the descriptions */
        publish "GetSysUserName" (data.manager, output data.managerDesc).
        publish "GetSysCodeDesc" ("Region", data.regionID, output data.regionDesc).
        publish "GetSysPropDesc" ("AMD", "Agent", "Status", data.stat, output data.statDesc).
        /* get the totals */
        empty temp-table tempdata.
        create tempdata.
        buffer-copy data to tempdata.
        data.qtr1 = getTotal(table tempdata, "Qtr1").
        data.qtr2 = getTotal(table tempdata, "Qtr2").
        data.qtr3 = getTotal(table tempdata, "Qtr3").
        data.qtr4 = getTotal(table tempdata, "Qtr4").
        data.yrTotal = getTotal(table tempdata, "Year").
      end.
      dataSortDesc = false.
      run sortData in this-procedure ("claimID").
      std-lo = can-find(first data).
      
      run AgentComboEnable in this-procedure (std-lo).
      
      hBrowse = frame fBrowse:first-child:first-child.
      if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
       then
        do:
          std-ha = hBrowse:query.
          appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
          displayStatus().
        end.
    end.
  RUN SetProgressEnd.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ICLSelected C-Win 
PROCEDURE ICLSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  std-ch = "".
  publish "GetDoubleClick" (output std-ch).
  case std-ch:
   when "M" then openWindowForAgent(pAgentID, "agentDetails", "dialogagentmodify.w").
   when "S" then openWindowForAgent(pAgentID, "review", "dialogagentsummary.w").
  end case.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pSortColumn as character no-undo.
  define variable hBrowse as handle no-undo.
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hTable as handle no-undo.

  run buildFieldList in this-procedure.
  run BuildDynamicBrowse (createQuery(dataSortBy)).
  run BuildDynamicBrowseTotalRow (createQuery(dataSortBy)).
  enableButtons(can-find(first data)).
  enableFilters(can-find(first data)).
  
  hBrowse = getBrowseHandle().
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      setStatusCount(std-ha:num-results).
    end.
  
  empty temp-table tempdata.
  create buffer hBuffer for table "data".
  create buffer hTable for table "tempdata".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each data no-lock " + doFilterSort()).
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    hTable:buffer-create.
    hTable:buffer-copy(hBuffer).
    hQuery:get-next().
  end.
  hQuery:query-close().
  setFilterCombos("ALL").
  
  delete object hQuery.
  delete object hBuffer.
  delete object hTable.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  /* modify the frame for the dynamic browse */
  frame fBrowse:width-pixels = {&window-name}:width-pixels - 10.
  frame fBrowse:virtual-width-pixels = frame fBrowse:width-pixels.
  frame fBrowse:height-pixels = {&window-name}:height-pixels - frame fBrowse:y - 5.
  frame fBrowse:virtual-height-pixels = frame fBrowse:height-pixels.
  
  run buildFieldList in this-procedure.
  dataSortDesc = not dataSortDesc.
  run BuildDynamicBrowse (createQuery(dataSortBy)).
  run BuildDynamicBrowseTotalRow (createQuery(dataSortBy)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  enableButtons(false).
  enableFilters(false).
  clearStatus().
  empty temp-table data.
  run windowResized in this-procedure.
  run AgentComboEnable in this-procedure (false).
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( input pSortColumn as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pSortColumn = dataSortBy
   then dataSortDesc = not dataSortDesc.
  
  if pSortColumn = ""
   then return "preselect each data no-lock " + doFilterSort().
   else return "preselect each data no-lock " + doFilterSort() + " by " + pSortColumn + (if dataSortDesc then " descending" else "").

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doFilterSort C-Win 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo initial "".
  define variable tAgentID as character no-undo.
  define variable tRegionID as character no-undo.
  define variable tStatus as character no-undo.
  define variable tManager as character no-undo.
  define variable tState as character no-undo.
  
  do with frame {&frame-name}:
    assign
      tAgentID = fAgent:screen-value
      tAgentID = (if tAgentID = ? or tAgentID = "" then "ALL" else tAgentID)
      tRegionID = fRegion:screen-value
      tRegionID = (if tRegionID = ? or tRegionID = "" then "ALL" else tRegionID)
      tStatus = fStatus:screen-value
      tStatus = (if tStatus = ? or tStatus = "" then "ALL" else tStatus)
      tManager = fManager:screen-value
      tManager = (if tManager = ? or tManager = "" then "ALL" else tManager)
      tState = fState:screen-value
      tState = (if tState = ? or tState = "" then "ALL" else tState)
      .
  end.
  /* build the query */
  tWhereClause = "where category='I'".
  if tAgentID <> "ALL" 
   then tWhereClause = addDelimiter(tWhereClause, " and ") + "agentID = '" + tAgentID + "'".
  if tRegionID <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause, " and ") + "regionID = '" + tRegionID + "'".
  if tStatus <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause, " and ") + "stat = '" + tStatus + "'".
  if tManager <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause, " and ") + "manager = '" + tManager + "'".
  if tState <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause, " and ") + "stateID = '" + tState + "'".
  
  RETURN tWhereClause.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getTotal C-Win 
FUNCTION getTotal RETURNS DECIMAL PRIVATE
  ( input table for tempdata,
    input pType as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dReturn as decimal no-undo.
  define variable iMonth as integer no-undo.
  define buffer tempdata for tempdata.
  
  iMonth = 12.
  for first tempdata no-lock:
    case pType:
     when "Qtr1" then dReturn = tempdata.month1 + tempdata.month2 + tempdata.month3.
     when "Qtr2" then dReturn = tempdata.month4 + tempdata.month5 + tempdata.month6.
     when "Qtr3" then dReturn = tempdata.month7 + tempdata.month8 + tempdata.month9.
     when "Qtr4" then dReturn = tempdata.month10 + tempdata.month11 + tempdata.month12.
     when "Year" then
      do:
        dReturn = (if iMonth >= 1  then tempdata.month1  else 0)
                + (if iMonth >= 2  then tempdata.month2  else 0)
                + (if iMonth >= 3  then tempdata.month3  else 0)
                + (if iMonth >= 4  then tempdata.month4  else 0)
                + (if iMonth >= 5  then tempdata.month5  else 0)
                + (if iMonth >= 6  then tempdata.month6  else 0)
                + (if iMonth >= 7  then tempdata.month7  else 0)
                + (if iMonth >= 8  then tempdata.month8  else 0)
                + (if iMonth >= 9  then tempdata.month9  else 0)
                + (if iMonth >= 10 then tempdata.month10 else 0)
                + (if iMonth >= 11 then tempdata.month11 else 0)
                + (if iMonth >= 12 then tempdata.month12 else 0).
      end.
    end case.
  end.
  return dReturn.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

