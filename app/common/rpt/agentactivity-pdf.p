&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file agentactivity.p
@description Print the agent activity report in a PDF

@param Activity;complex;The activity table
------------------------------------------------------------------------*/

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/rpt-def.i &ID="'Activity'" &lines=80}

/* Temp Table Definitions ---                                           */
{tt/activity.i &tableAlias="data"}

define temp-table showdata like data
  field showLevel as integer   {&nodeType}
  field showID    as character {&nodeType} column-label "Agent"      format "x(500)"
  field showName  as character {&nodeType} column-label "Agent Name" format "x(500)"
  field seq       as integer   {&nodeType}
  .

/* Function Definitions ---                                             */
{lib/add-delimiter.i}
{lib/getstatename.i}

/* Parameters ---                                                       */
define input parameter table for showdata.
define input parameter pGroupBy as character no-undo.
define input parameter pFormat as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .

if not can-find(first showdata)
 then 
  do: 
    MESSAGE "There is nothing to print" VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
  end.

run getFilename in this-procedure no-error.

{lib/rpt-open.i}

run details in this-procedure.

{lib/rpt-cloz.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getFilename Procedure 
PROCEDURE getFilename PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "GetReportDir" (output activeFilename).
  if activeFilename > "" 
   then 
    if lookup(substring(activeFilename, length(activeFilename), 1), "/,\") = 0 
     then activeFilename = activeFilename + "/".
  activeFilename = activeFilename + "AgentActivity.pdf".
  if search(activeFilename) <> ? 
   then
    do:
      os-delete value(activeFilename).
      do while os-error <> 0:
        message "Please close the open Agent Activity report" view-as alert-box warning buttons ok.
        os-delete value(activeFilename).
      end.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE details Procedure 
PROCEDURE details :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  setFont("Courier", 8.0).
  textColor(0.0, 0.0, 0.0).
  
  define buffer showdata for showdata.
  for each showdata no-lock break by showdata.showID by seq:
    /* the name */
    if can-find(first showdata where showdata.type = "C") and (showdata.type = "P" or showdata.type = "I")
     then
      case pGroupBy:
       when "S" then std-ch = showdata.showName. /* State */
       when "C" then std-ch = substring(showdata.showName, 1,  r-index(showdata.showName, " ", 25) - 1).
       when "A" then std-ch = showdata.agentID + " - " + substring(showdata.name, 1,  r-index(showdata.name, " ", 17) - 1).
      end case.
     else std-ch = "".
    paragraph(std-ch, 25, 9, 12, false).
    /* the type */
    textAt(showdata.typeDesc, 37).
    /* the totals */
    textTo(string(showdata.qtr1,(if showdata.type = "C" then pFormat else "(>>>,>>>,>>9)")), 62).
    textTo(string(showdata.qtr2,(if showdata.type = "C" then pFormat else "(>>>,>>>,>>9)")), 75).
    textTo(string(showdata.qtr3,(if showdata.type = "C" then pFormat else "(>>>,>>>,>>9)")), 88).
    textTo(string(showdata.qtr4,(if showdata.type = "C" then pFormat else "(>>>,>>>,>>9)")), 101).
    textTo(string(showdata.yrTotal,(if showdata.type = "C" then pFormat else "(>>>,>>>,>>>,>>9)")), 116).
    newLine(1).
    if last-of(showdata.showID)
     then newLine(1).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/rpt-ftr.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTitle as character no-undo.
  define variable tXpos as integer no-undo.
  define variable cName as character no-undo.
  
  std-in = 0.
  for each showdata break by showdata.stateID:
    if first-of(showdata.stateID)
     then 
      assign
        std-in = std-in + 1
        cName = showdata.name
        .
  end.

  tTitle = "AGENT ACTIVITIES - ".
  case pGroupBy:
   when "S" then /* States */
    do:
      if std-in = 1
       then tTitle = tTitle + cName.
       else tTitle = tTitle + "ALL STATES".
    end.
   when "C" then /* Corporations */
    do:
      if std-in = 1
       then tTitle = tTitle + cName.
       else tTitle = tTitle + "ALL COMPANIES".
    end.
   when "M" then /* Managers */
    do:
      if std-in = 1
       then tTitle = tTitle + cName.
       else tTitle = tTitle + "ALL MANAGERS".
    end.
   when "A" then tTitle = tTitle + "DETAILS".
  end case.
  tXpos = 400 - (length(tTitle) / 2 * 6). /* there are 6 points in one character and 400 points is the middle of the header */
  {lib/rpt-hdr.i &title=tTitle &title-x=tXpos &title-y=740}
  
  setFont("Courier", 8.0).
  textColor(0.0, 0.0, 0.0).
  textAt("Name", 9).
  textAt("Type", 37).
  textTo("Quarter 1", 60).
  textTo("Quarter 2", 73).
  textTo("Quarter 3", 86).
  textTo("Quarter 4", 99).
  textTo("Year Total", 116).
  newLine(1).
  createLine(-3).
  newLine(2).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
