&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Batch Activity (wcrm01-r.w)
@description The user chooses an agent and will retrieve the batch 
             figures for the month/year

@author John Oliver
@version 1.0
@created 2017/03/01
@notes 
---------------------------------------------------------------------*/


CREATE WIDGET-POOL.

{tt/agent.i}
{tt/period.i}
{tt/state.i}
{tt/ops18.i}

def temp-table data
 field agentID as char
 field name as char
 field periodYear as int format "9999"
 field stateID as char
 field janValue as dec format "(zzz,zzz,zzz,zzz)"
 field febValue as dec format "(zzz,zzz,zzz,zzz)"
 field marValue as dec format "(zzz,zzz,zzz,zzz)"
 field aprValue as dec format "(zzz,zzz,zzz,zzz)"
 field mayValue as dec format "(zzz,zzz,zzz,zzz)"
 field junValue as dec format "(zzz,zzz,zzz,zzz)"
 field julValue as dec format "(zzz,zzz,zzz,zzz)"
 field augValue as dec format "(zzz,zzz,zzz,zzz)"
 field sepValue as dec format "(zzz,zzz,zzz,zzz)"
 field octValue as dec format "(zzz,zzz,zzz,zzz)"
 field novValue as dec format "(zzz,zzz,zzz,zzz)"
 field decValue as dec format "(zzz,zzz,zzz,zzz)"
 field ytdValue as dec format "(zzz,zzz,zzz,zzz)"
 field avgValue as dec format "(zzz,zzz,zzz,zzz)"
 .

{lib/std-def.i}
{lib/find-widget.i}
{lib/count-rows.i}
{lib/get-column.i}
{lib/getstatename.i}
{lib/add-delimiter.i}
{lib/set-status.i}
/*---------------------------------------------------------------------
@name Compass
@action 
@description 

@param 

@author John Oliver
@version 1.0
@created 2017/04/04
@notes 
---------------------------------------------------------------------*/

def var hData as handle no-undo.

def var tGettingData as logical.
def var tKeepGettingData as logical.

/* variables needed for the browse total */
def var totalCol as decimal extent no-undo.
def var totalAdded as logical no-undo init false.

/* column width for the year column */
def var dBrwColWidth as decimal no-undo.

define menu brwpopmenu title "Actions"
 menu-item m_PopViewBatch label "View Batch"
 .

ON 'choose':U OF menu-item m_PopViewBatch in menu brwpopmenu
DO:
 run viewBatch in this-procedure.
 RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.periodYear data.janValue data.febValue data.marValue data.aprValue data.mayValue data.junValue data.julValue data.augValue data.sepValue data.octValue data.novValue data.decValue data.ytdValue data.avgValue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo tStateID tPolCount brwData tPolLiability ~
tPolGross tPolNet tFileCount tBatchCount 
&Scoped-Define DISPLAYED-OBJECTS tStateID tAgentID fView tPolCount ~
tBatchCount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitizeReportButtons C-Win 
FUNCTION sensitizeReportButtons RETURNS LOGICAL PRIVATE
  ( input pCmd as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN AUTO-COMPLETION
     SIZE 68.4 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tBatchCount AS INTEGER FORMAT " -zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Batch Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tFileCount AS INTEGER FORMAT " -zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "File Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolCount AS INTEGER FORMAT " -zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Policy Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolGross AS DECIMAL FORMAT "$-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolLiability AS DECIMAL FORMAT "$-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolNet AS DECIMAL FORMAT "$-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 83 BY 6.67.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 49.2 BY 6.67.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25.6 BY 6.67.

DEFINE VARIABLE fView AS CHARACTER INITIAL "N" 
     VIEW-AS SELECTION-LIST SINGLE 
     LIST-ITEM-PAIRS "Net Premium","N",
                     "Gross Premium","G",
                     "Retained Premium","R",
                     "Liability Amount","L",
                     "Number of Batches","B",
                     "Number of Files","F",
                     "Number of Policies","P",
                     "Report Gap","T" 
     SIZE 22 BY 5.24 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.periodYear label "Year":C width 10
      data.janValue label "Jan":C width 16
      data.febValue label "Feb":C width 16
      data.marValue label "Mar":C width 16
      data.aprValue label "Apr":C width 16
      data.mayValue label "May":C width 16
      data.junValue label "Jun":C width 16
      data.julValue label "Jul":C width 16
      data.augValue label "Aug":C width 16
      data.sepValue label "Sep":C width 16
      data.octValue label "Oct":C width 16
      data.novValue label "Nov":C width 16
      data.decValue label "Dec":C width 16
      data.ytdValue label "YTD":C width 16
      data.avgValue LABEL "AVG":C WIDTH 16
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 249.8 BY 16.05 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 5.19 COL 19.4 WIDGET-ID 2 NO-TAB-STOP 
     bGo AT ROW 5.19 COL 12 WIDGET-ID 4 NO-TAB-STOP 
     tStateID AT ROW 2.67 COL 10 COLON-ALIGNED WIDGET-ID 82
     tAgentID AT ROW 3.86 COL 10.2 COLON-ALIGNED WIDGET-ID 84
     fView AT ROW 2.19 COL 86.4 NO-LABEL WIDGET-ID 124
     tPolCount AT ROW 2.86 COL 127 COLON-ALIGNED WIDGET-ID 86 NO-TAB-STOP 
     brwData AT ROW 8.62 COL 2 WIDGET-ID 200
     tPolLiability AT ROW 6.67 COL 127 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tPolGross AT ROW 4.76 COL 127 COLON-ALIGNED WIDGET-ID 88 NO-TAB-STOP 
     tPolNet AT ROW 5.71 COL 127 COLON-ALIGNED WIDGET-ID 90 NO-TAB-STOP 
     tFileCount AT ROW 3.81 COL 127 COLON-ALIGNED WIDGET-ID 92 NO-TAB-STOP 
     tBatchCount AT ROW 1.91 COL 127 COLON-ALIGNED WIDGET-ID 122 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     "Totals" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 111.6 WIDGET-ID 112
     "View" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 85.6 WIDGET-ID 130
     RECT-36 AT ROW 1.52 COL 2 WIDGET-ID 54
     RECT-38 AT ROW 1.52 COL 109.8 WIDGET-ID 110
     RECT-39 AT ROW 1.52 COL 84.6 WIDGET-ID 128
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 251.8 BY 23.91 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Batch Activity"
         HEIGHT             = 23.91
         WIDTH              = 251.8
         MAX-HEIGHT         = 47.48
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.48
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tPolCount fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR SELECTION-LIST fView IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-38 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-39 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tAgentID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tBatchCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFileCount IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFileCount:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPolCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolNet:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 6.95
       COLUMN          = 12
       HEIGHT          = .48
       WIDTH           = 15
       TAB-STOP        = no
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Batch Activity */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Batch Activity */
DO:
  if tGettingData 
   then return no-apply.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Batch Activity */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  sensitizeReportButtons(false).
  run getData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fView C-Win
ON VALUE-CHANGED OF fView IN FRAME fMain
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  run AgentComboState in this-procedure.
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/set-buttons.i}
{lib/report-progress-bar.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

browse brwData:POPUP-MENU = MENU brwpopmenu:HANDLE.

subscribe to "DataError" anywhere.

session:immediate-display = yes.

fView = "N". /* Net Premium */
sensitizeReportButtons(true).
initializeStatusWindow({&window-name}:handle).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID}
    /* set the values */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'year'" &var=dBrwColWidth}
     
  clearData().
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "agentbatchactivity.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "agentbatchactivity.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input param pMsg as char.

  /* Since Progress is single-threaded, this works */
  if not tGettingData 
   then return.

  tKeepGettingData = false.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the System Administrator if the problem persists."
    view-as alert-box error.

  clearData().
  
  sensitizeReportButtons(true).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tStateID tAgentID fView tPolCount tBatchCount 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo tStateID tPolCount brwData tPolLiability tPolGross tPolNet 
         tFileCount tBatchCount 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Agent_Batch_Activity"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def buffer ops18 for ops18.
  def buffer agent for agent.

  def var cFilter as char no-undo.
  def var iAverage as int no-undo.
  def var hBuffer as handle no-undo.

  hBuffer = buffer data:handle.
    
  setStatus("Filtering data...").
  displayStatus().

  do with frame {&frame-name}:
    
    assign
      tPolCount = 0
      tPolLiability = 0
      tPolGross = 0
      tPolNet = 0
      tFileCount = 0
      tBatchCount = 0
      .

    close query brwData.
    empty temp-table data.

    cFilter = fView:input-value.

    for first agent no-lock
        where agent.agentID = tAgentID:input-value:
        
      for each ops18:
        process events.
        
        create data.
        assign
          data.agentID = ops18.agentID
          data.stateID = ops18.stateID
          data.periodYear = ops18.periodYear
          data.name = agent.name
          .
        
        case cFilter:
         when "G" then assign
                         data.janValue = ops18.janGross
                         data.febValue = ops18.febGross
                         data.marValue = ops18.marGross
                         data.aprValue = ops18.aprGross
                         data.mayValue = ops18.mayGross
                         data.junValue = ops18.junGross
                         data.julValue = ops18.julGross
                         data.augValue = ops18.augGross
                         data.sepValue = ops18.sepGross
                         data.octValue = ops18.octGross
                         data.novValue = ops18.novGross
                         data.decValue = ops18.decGross
                         .
         when "N" then assign 
                         data.janValue = ops18.janNet
                         data.febValue = ops18.febNet
                         data.marValue = ops18.marNet
                         data.aprValue = ops18.aprNet
                         data.mayValue = ops18.mayNet
                         data.junValue = ops18.junNet
                         data.julValue = ops18.julNet
                         data.augValue = ops18.augNet
                         data.sepValue = ops18.sepNet
                         data.octValue = ops18.octNet
                         data.novValue = ops18.novNet
                         data.decValue = ops18.decNet
                         .
         when "R" then assign 
                         data.janValue = ops18.janRetain
                         data.febValue = ops18.febRetain
                         data.marValue = ops18.marRetain
                         data.aprValue = ops18.aprRetain
                         data.mayValue = ops18.mayRetain
                         data.junValue = ops18.junRetain
                         data.julValue = ops18.julRetain
                         data.augValue = ops18.augRetain
                         data.sepValue = ops18.sepRetain
                         data.octValue = ops18.octRetain
                         data.novValue = ops18.novRetain
                         data.decValue = ops18.decRetain
                         .
         when "L" then assign 
                         data.janValue = ops18.janLiab
                         data.febValue = ops18.febLiab
                         data.marValue = ops18.marLiab
                         data.aprValue = ops18.aprLiab
                         data.mayValue = ops18.mayLiab
                         data.junValue = ops18.junLiab
                         data.julValue = ops18.julLiab
                         data.augValue = ops18.augLiab
                         data.sepValue = ops18.sepLiab
                         data.octValue = ops18.octLiab
                         data.novValue = ops18.novLiab
                         data.decValue = ops18.decLiab
                         .
         when "P" then assign 
                         data.janValue = ops18.janPolicies
                         data.febValue = ops18.febPolicies
                         data.marValue = ops18.marPolicies
                         data.aprValue = ops18.aprPolicies
                         data.mayValue = ops18.mayPolicies
                         data.junValue = ops18.junPolicies
                         data.julValue = ops18.julPolicies
                         data.augValue = ops18.augPolicies
                         data.sepValue = ops18.sepPolicies
                         data.octValue = ops18.octPolicies
                         data.novValue = ops18.novPolicies
                         data.decValue = ops18.decPolicies
                         .
         when "F" then assign 
                         data.janValue = ops18.janFiles
                         data.febValue = ops18.febFiles
                         data.marValue = ops18.marFiles
                         data.aprValue = ops18.aprFiles
                         data.mayValue = ops18.mayFiles
                         data.junValue = ops18.junFiles
                         data.julValue = ops18.julFiles
                         data.augValue = ops18.augFiles
                         data.sepValue = ops18.sepFiles
                         data.octValue = ops18.octFiles
                         data.novValue = ops18.novFiles
                         data.decValue = ops18.decFiles
                         .
         when "B" then assign 
                         data.janValue = ops18.janBatches
                         data.febValue = ops18.febBatches
                         data.marValue = ops18.marBatches
                         data.aprValue = ops18.aprBatches
                         data.mayValue = ops18.mayBatches
                         data.junValue = ops18.junBatches
                         data.julValue = ops18.julBatches
                         data.augValue = ops18.augBatches
                         data.sepValue = ops18.sepBatches
                         data.octValue = ops18.octBatches
                         data.novValue = ops18.novBatches
                         data.decValue = ops18.decBatches
                         .
         when "T" then assign 
                         data.janValue = ops18.janGap
                         data.febValue = ops18.febGap
                         data.marValue = ops18.marGap
                         data.aprValue = ops18.aprGap
                         data.mayValue = ops18.mayGap
                         data.junValue = ops18.junGap
                         data.julValue = ops18.julGap
                         data.augValue = ops18.augGap
                         data.sepValue = ops18.sepGap
                         data.octValue = ops18.octGap
                         data.novValue = ops18.novGap
                         data.decValue = ops18.decGap
                         .
        end case.

        data.ytdValue = data.janValue + data.febValue + data.marValue
                      + data.aprValue + data.mayValue + data.junValue
                      + data.julValue + data.augValue + data.sepValue
                      + data.octValue + data.novValue + data.decValue.
         
        /* average */
        iAverage = 0.
        do std-in = 1 to hBuffer:num-fields:
          std-ha = hBuffer:buffer-field(std-in).
          case std-ha:name:
           when "janValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "febValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "marValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "aprValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "mayValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "junValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "julValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "augValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "sepValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "octValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "novValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
           when "decValue" then if decimal(std-ha:buffer-value()) > 0 then iAverage = iAverage + 1.
          end case.
        end.
        
        if iAverage > 0
         then data.avgValue = integer(data.ytdValue / iAverage) no-error.
         else data.avgValue = 0.
         
        /* set the YTD values to 0 for Report Gap */
        if cFilter = "T"
         then data.ytdValue = 0.
             
        /* das: figure out how to increment these... */
        assign
          tPolCount = tPolCount 
                                + (ops18.janPolicies
                                + ops18.febPolicies
                                + ops18.marPolicies
                                + ops18.aprPolicies
                                + ops18.mayPolicies
                                + ops18.junPolicies
                                + ops18.julPolicies
                                + ops18.augPolicies
                                + ops18.sepPolicies
                                + ops18.octPolicies
                                + ops18.novPolicies
                                + ops18.decPolicies)
       
          tPolLiability = tPolLiability
                                + (ops18.janLiab
                                + ops18.febLiab
                                + ops18.marLiab
                                + ops18.aprLiab
                                + ops18.mayLiab
                                + ops18.junLiab
                                + ops18.julLiab
                                + ops18.augLiab
                                + ops18.sepLiab
                                + ops18.octLiab
                                + ops18.novLiab
                                + ops18.decLiab)
       
          tPolGross = tPolGross
                                + (ops18.janGross
                                + ops18.febGross
                                + ops18.marGross
                                + ops18.aprGross
                                + ops18.mayGross
                                + ops18.junGross
                                + ops18.julGross
                                + ops18.augGross
                                + ops18.sepGross
                                + ops18.octGross
                                + ops18.novGross
                                + ops18.decGross)
       
          tPolNet = tPolNet
                                + (ops18.janNet
                                + ops18.febNet
                                + ops18.marNet
                                + ops18.aprNet
                                + ops18.mayNet
                                + ops18.junNet
                                + ops18.julNet
                                + ops18.augNet
                                + ops18.sepNet
                                + ops18.octNet
                                + ops18.novNet
                                + ops18.decNet)
       
          tFileCount = tFileCount
                                + (ops18.janFiles
                                + ops18.febFiles
                                + ops18.marFiles
                                + ops18.aprFiles
                                + ops18.mayFiles
                                + ops18.junFiles
                                + ops18.julFiles
                                + ops18.augFiles
                                + ops18.sepFiles
                                + ops18.octFiles
                                + ops18.novFiles
                                + ops18.decFiles)
       
          tBatchCount = tBatchCount
                                + (ops18.janBatches
                                + ops18.febBatches
                                + ops18.marBatches
                                + ops18.aprBatches
                                + ops18.mayBatches
                                + ops18.junBatches
                                + ops18.julBatches
                                + ops18.augBatches
                                + ops18.sepBatches
                                + ops18.octBatches
                                + ops18.novBatches
                                + ops18.decBatches)
          .
      end.
    end.
  end.
   
  displayTotals().
   
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).

  if cFilter = "T"
   then
    do:
      {lib/brw-totalData.i &noShow=true}
    end.

  std-in = lookup(tAgentID:screen-value, tAgentID:list-item-pairs, {&msg-dlm}) - 1.
  setStatus("Report Complete for Agent: " + entry(std-in, tAgentID:list-item-pairs, {&msg-dlm})).
  appendStatus("(" + string(now,"99/99/99 HH:MM:SS") + ")").
  displayStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tSuccess as logical init false no-undo.
  def var tMsg as char no-undo.
  
  clearData().
  
  tGettingData = true.
  tkeepGettingData = true.
  
  setStatus("Getting data...").
  displayStatus().

  
  GETTING-DATA:
  do with frame {&frame-name}:
    if tAgentID:input-value = ? or tAgentID:input-value = "ALL"
     then 
      do:
        message "Please select an agent" view-as alert-box information buttons ok.
        tKeepGettingData = false.
      end.
     else 
      do:
        run server/getagentbatchytdvolume.p (tAgentID:input-value,
                                             output table ops18, 
                                             output tSuccess,
                                             output tMsg).
         if not tSuccess
          then message "GetAgentBatchYTDVolume failed: " tMsg view-as alert-box warning.
         process events.
      end.
  end. /* GETTING-DATA */
  
  if not tKeepGettingData 
   then
    do:
      clearData().
      tGettingData = false.
      return.
    end.
  
  run SetProgressStatus.
  run filterData.
  run SetProgressEnd.
  
  do with frame {&frame-name}:
    ASSIGN
      CtrlFrame:HIDDEN = TRUE
      .
  END.
  tGettingData = false.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i &post-by-clause="+ ' by data.periodYear '"}
{lib/brw-totalData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewBatch C-Win 
PROCEDURE viewBatch PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available data and data.agentID <> ""
   then
    do:
        publish "SetCurrentValue" ("AgentID", data.agentID).
        /* das: what should we run?  run wops01-r.w persistent. */
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var iOffset as int no-undo init 5.
  
  def var dDiffWidth as decimal no-undo.

  if totalAdded
   then iOffset = 28.
  
  assign
    /* resize the frame */
    frame {&frame-name}:width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
    /* resize the browse */
    {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10
    {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - iOffset
    .
  /* resize the column */
  {lib/resize-column.i &col="'year'" &var=dBrwColWidth}
  
  {lib/brw-totalData.i}
  apply "VALUE-CHANGED":U to {&browse-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    
    close query brwData.
    empty temp-table data. 
    empty temp-table ops18.

    hide message no-pause.
    
    assign
      tPolCount = 0
      tPolLiability = 0
      tPolGross = 0
      tPolNet = 0
      tFileCount = 0
      tBatchCount = 0
      CtrlFrame:HIDDEN = FALSE
      .
      
    display
      tPolCount
      tPolLiability
      tPolGross
      tPolNet
      tFileCount
      tBatchCount
      .
      
    bExport:sensitive = false.
    
  end.
 {lib/brw-totalData.i}
 clearStatus().
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
do with frame {&frame-name}:
  
  display
    tPolCount
    tPolLiability
    tPolGross
    tPolNet
    tFilecount
    tBatchCount
    .
  
  pause 0.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitizeReportButtons C-Win 
FUNCTION sensitizeReportButtons RETURNS LOGICAL PRIVATE
  ( input pCmd as logical ) :

  do with frame {&frame-name}:
    assign
      std-lo = can-find(first data)
      tStateID:sensitive = pCmd
      tAgentID:sensitive = pCmd
      bGo:sensitive = pCmd
      fView:sensitive = pCmd and std-lo
      bExport:sensitive  = pCmd and std-lo
      .
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

