&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Activity
@description Report to show all of the agent activities

@author John Oliver
@version 1.0
@created 02/15/2018
@notes 
---------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */
CREATE WIDGET-POOL.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/brw-totalData-def.i}

/* for the column width on resize */
define variable dColumnWidth as decimal no-undo.

/* the years list */
define variable cYearList as character no-undo initial "".

/* used for the formatting */
define variable cFormat as character no-undo.

/* boolean to hold if comparison */
define variable lIsComparison as logical no-undo initial false.

/* Temp Table Definitions ---                                           */
{tt/agent.i}
{tt/state.i}
{tt/period.i}
{tt/activity.i &tableAlias="data"}

define temp-table showdata like data
  field showLevel as integer   {&nodeType}
  field showID    as character {&nodeType} column-label "Agent"      format "x(500)"
  field showName  as character {&nodeType} column-label "Agent Name" format "x(500)"
  field seq       as integer   {&nodeType}
  .
  
/* temp table for exporting and building the comparison row */
define temp-table tempdata   like showdata.
define temp-table exportdata like showdata.

/* Function Definitions ---                                             */
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/getstatename.i}
{lib/groupby-def.i &group-column-fields="stateID" &lowestDetailName="name"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES showdata

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData showdata.stateID showdata.showID showdata.showName showdata.typeDesc showdata.month1 showdata.month2 showdata.month3 showdata.month4 showdata.month5 showdata.month6 showdata.month7 showdata.month8 showdata.month9 showdata.month10 showdata.month11 showdata.month12 showdata.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH showdata
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH showdata.
&Scoped-define TABLES-IN-QUERY-brwData showdata
&Scoped-define FIRST-TABLE-IN-QUERY-brwData showdata


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo tYear tCategory fTypes fState fAgent ~
fCompare fCompareOperator fGroupBy fMonth fCompareValue RECT-36 RECT-39 ~
RECT-40 
&Scoped-Define DISPLAYED-OBJECTS tYear tCategory fTypes fState fAgent ~
fCompare fCompareOperator fGroupBy fMonth fCompareValue 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD addComparisonRow C-Win 
FUNCTION addComparisonRow RETURNS LOGICAL
  ( input pType as character,
    input pshowID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createShowRow C-Win 
FUNCTION createShowRow RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input table for tempdata )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doFilterSort C-Win 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD enableButtonsAndFilters C-Win 
FUNCTION enableButtonsAndFilters RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getTotal C-Win 
FUNCTION getTotal RETURNS DECIMAL PRIVATE
  ( input table for tempdata,
    input pType as character  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setYTDColumn C-Win 
FUNCTION setYTDColumn RETURNS LOGICAL
  ( input pMonth as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "Generate PDF".

DEFINE VARIABLE fAgent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 71 BY 1 NO-UNDO.

DEFINE VARIABLE fCompareOperator AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Comparison" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "None","ALL",
                     "Greater Than","gt",
                     "Less Than","lt"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE fGroupBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Group By" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "State","S",
                     "Company","C",
                     "Agent","A"
     DROP-DOWN-LIST
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fMonth AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "Evaluate Thru" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 24 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE fCompareValue AS INTEGER FORMAT "->>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE fCompare AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Percent", "P",
"Value", "D"
     SIZE 11 BY 1.81 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62 BY 3.57.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 122 BY 3.57.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 31.6 BY 3.57.

DEFINE VARIABLE fTypes AS CHARACTER INITIAL "C" 
     VIEW-AS SELECTION-LIST SINGLE 
     LIST-ITEM-PAIRS "Comparison","C" 
     SIZE 13 BY 2.14 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      showdata SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      showdata.stateID width 8
      showdata.showID width 10
      showdata.showName width 16
      showdata.typeDesc width 14
showdata.month1 width 14
showdata.month2 width 14
showdata.month3 width 14
showdata.month4 width 14
showdata.month5 width 14
showdata.month6 width 14
showdata.month7 width 14
showdata.month8 width 14
showdata.month9 width 14
showdata.month10 width 14
showdata.month11 width 14
showdata.month12 width 14
showdata.yrTotal width 17
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 251 BY 20 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.1 COL 46.4 WIDGET-ID 2 NO-TAB-STOP 
     bGo AT ROW 2.1 COL 39 WIDGET-ID 4 NO-TAB-STOP 
     bPrint AT ROW 2.1 COL 53.8 WIDGET-ID 328 NO-TAB-STOP 
     tYear AT ROW 2.19 COL 12 COLON-ALIGNED WIDGET-ID 168
     tCategory AT ROW 3.38 COL 12 COLON-ALIGNED WIDGET-ID 300
     fTypes AT ROW 2.19 COL 67 NO-LABEL WIDGET-ID 354
     fState AT ROW 2.19 COL 104.4 COLON-ALIGNED WIDGET-ID 166
     fAgent AT ROW 3.38 COL 104.4 COLON-ALIGNED WIDGET-ID 84
     fCompare AT ROW 2.33 COL 81.8 NO-LABEL WIDGET-ID 334
     fCompareOperator AT ROW 2.19 COL 139.4 COLON-ALIGNED WIDGET-ID 342
     fGroupBy AT ROW 2.14 COL 192.4 COLON-ALIGNED WIDGET-ID 338
     fMonth AT ROW 3.33 COL 192.4 COLON-ALIGNED WIDGET-ID 360
     brwData AT ROW 5.29 COL 2 WIDGET-ID 200
     fCompareValue AT ROW 2.19 COL 162.4 COLON-ALIGNED NO-LABEL WIDGET-ID 340 NO-TAB-STOP 
     "View" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.24 COL 64.6 WIDGET-ID 350
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 96 WIDGET-ID 346
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3.4 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-39 AT ROW 1.48 COL 95 WIDGET-ID 344
     RECT-40 AT ROW 1.48 COL 63.6 WIDGET-ID 348
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 253 BY 24.57 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Activity - Year"
         HEIGHT             = 24.57
         WIDTH              = 253
         MAX-HEIGHT         = 47.29
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.29
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fMonth fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrint IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwData IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH showdata.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.91
       COLUMN          = 39
       HEIGHT          = .48
       WIDTH           = 22
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Activity - Year */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Activity - Year */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Activity - Year */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run rpt/agentactivity-pdf.p (table showdata, fGroupBy:input-value in frame {&frame-name}, cFormat).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  if lIsComparison
   then
    do std-in = 1 to num-entries(colHandleList):
      colHandle = handle(entry(std-in, colHandleList)).
      if valid-handle(colHandle)
       then colHandle:bgcolor = showdata.rowColor.
    end.
   else
    do:
      {lib/brw-rowDisplay.i}
    end.
  /* change the column format */
  if showdata.type = "C"
   then
    assign
      showdata.month1:format in browse {&browse-name} = cFormat
      showdata.month2:format in browse {&browse-name} = cFormat
      showdata.month3:format in browse {&browse-name} = cFormat
      showdata.month4:format in browse {&browse-name} = cFormat
      showdata.month5:format in browse {&browse-name} = cFormat
      showdata.month6:format in browse {&browse-name} = cFormat
      showdata.month7:format in browse {&browse-name} = cFormat
      showdata.month8:format in browse {&browse-name} = cFormat
      showdata.month9:format in browse {&browse-name} = cFormat
      showdata.month10:format in browse {&browse-name} = cFormat
      showdata.month11:format in browse {&browse-name} = cFormat
      showdata.month12:format in browse {&browse-name} = cFormat
      showdata.yrTotal:format in browse {&browse-name} = cFormat
      .
  /* if company grouping, make the name the agent name if no company */
  if fGroupBy:screen-value in FRAME {&frame-name} = "C"
   then
    if showdata.showName = showdata.agentID
     then
      assign
        showdata.showID:screen-value in browse {&browse-name} = showdata.agentID
        showdata.showName:screen-value in browse {&browse-name} = showdata.name
        .
     else showdata.showID:screen-value in browse {&browse-name} = "".
  /* don't show the name or agentID for the actual or comparison rows */
  if lIsComparison and showdata.type <> "P" and showdata.type <> "I"
   then
    assign
      showdata.showID:screen-value in browse {&browse-name} = ""
      showdata.showName:screen-value in browse {&browse-name} = ""
      showdata.stateID:screen-value in browse {&browse-name} = ""
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  if fTypes:screen-value in FRAME {&frame-name} <> "C"
   then 
    do:
      {lib/brw-startsearch.i}.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCompare
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCompare C-Win
ON VALUE-CHANGED OF fCompare IN FRAME fMain
DO:
  case self:screen-value:
   when "D" then cFormat = "(>>>,>>>,>>>,>>9)".
   when "P" then cFormat = "(>,>>>,>>9)%".
  end case.
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCompareOperator
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCompareOperator C-Win
ON VALUE-CHANGED OF fCompareOperator IN FRAME fMain /* Comparison */
DO:
  assign
    std-lo = (self:screen-value = "ALL")
    /* hide the values for ALL */
    fCompareValue:hidden = std-lo
    .
  if std-lo
   then apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCompareValue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCompareValue C-Win
ON LEAVE OF fCompareValue IN FRAME fMain
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCompareValue C-Win
ON RETURN OF fCompareValue IN FRAME fMain
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fGroupBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fGroupBy C-Win
ON VALUE-CHANGED OF fGroupBy IN FRAME fMain /* Group By */
DO:
  define variable tWhereClause as character no-undo.
  
  tWhereClause = doFilterSort().
  case self:screen-value:
   when "S" then {lib/groupby.i &firstField=stateID &secondField=type &groupName=stateName &columnLabel="'State Name'" &whereClause=tWhereClause &noSort=true}
   when "C" then {lib/groupby.i &firstField=corporationID &secondField=type &groupName=corporationID &columnLabel="'Company'" &whereClause=tWhereClause &noSort=true}
   when "A" then {lib/groupby.i &firstField=agentID &secondField=type &groupName=name &columnLabel="'Agent Name'" &whereClause=tWhereClause &noSort=true}
  end case.
  /* add the comparison row */
  if lIsComparison
   then
    for each showdata no-lock break by showdata.showID:
      if first-of(showdata.showID)
       then addComparisonRow(fCompare:screen-value, showdata.showID).
    end.
  /* get the data for the year column */
  for each showdata exclusive-lock:
    if lIsComparison and showdata.seq = 3
     then next.
    
    empty temp-table tempdata.
    create tempdata.
    buffer-copy showdata to tempdata.
    showdata.yrTotal = getTotal(table tempdata, "Year").
  end.
  /* set the column name */
  setYTDColumn(fMonth:input-value).
  /* enable filters */
  enableButtonsAndFilters(can-find(first data)).
  /* sort */
  run sortData in this-procedure ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMonth C-Win
ON VALUE-CHANGED OF fMonth IN FRAME fMain /* Evaluate Thru */
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fTypes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fTypes C-Win
ON VALUE-CHANGED OF fTypes IN FRAME fMain
DO:
  lIsComparison = false.
  if fTypes:screen-value = "C"
   then lIsComparison = true.
  do with FRAME {&frame-name}:
    assign
      fCompare:sensitive = lIsComparison
      fCompareOperator:sensitive = lIsComparison
      .
    apply "VALUE-CHANGED" to fCompareOperator.
  end.
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCategory C-Win
ON VALUE-CHANGED OF tCategory IN FRAME fMain /* Category */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain /* Year */
DO:
  if self:input-value = year(today)
   then fMonth:screen-value in frame {&frame-name} = string(month(today)).
   else fMonth:screen-value in frame {&frame-name} = string(12).
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i}
{lib/brw-main.i}
{lib/report-progress-bar.i}
{lib/set-buttons.i}
{lib/set-filters.i &tableName="'showdata'" &labelName="'State'" &columnName="'stateID'" &include="'Agent,GroupBy,Month,Types'" &noSort=true}

{&window-name}:window-state = 2.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


/* fill the year list */
publish "GetActivityYearList" (output cYearList).
if cYearList = ""
 then run server/getagentactivityyears.p (output cYearList, output std-lo, output std-ch).
if cYearList > ""
 then tYear:list-items = cYearList.
 else tYear:list-items = string(year(today)).
 
/* get the categories */
{lib/get-sysprop-list.i &combo=tCategory &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Category'"}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-agent-list.i &combo=fAgent &state=fState &addAll=true}
    {lib/get-period-list.i &mth=fMonth &noYear=true}
    /* set the values */
    run AgentComboState in this-procedure.
  
    /* get the column width */
    {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
    
    std-in = year(today).
    if lookup(string(std-in), tYear:list-items) > 0
     then tYear:screen-value = string(std-in).
     else tYear:screen-value = entry(1, tYear:list-items).
    apply "VALUE-CHANGED" to tYear.
    
    assign
      lIsComparison                 = true
      tCategory:screen-value        = entry(2, tCategory:list-item-pairs)
      fCompareOperator:screen-value = "ALL"
      fGroupBy:screen-value         = "A"
      fCompare:screen-value         = "P"
      cFormat                       = "(>,>>>,>>9)%"
      .
  end.
  
  clearData().
  run windowResized in this-procedure.
  {&window-name}:window-state = 3.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "agentactivity.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "agentactivity.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tYear tCategory fTypes fState fAgent fCompare fCompareOperator 
          fGroupBy fMonth fCompareValue 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo tYear tCategory fTypes fState fAgent fCompare fCompareOperator 
         fGroupBy fMonth fCompareValue RECT-36 RECT-39 RECT-40 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufferdata for showdata.
  define buffer exportdata for exportdata.

  empty temp-table exportdata.
  for each bufferdata no-lock:
    create exportdata.
    buffer-copy bufferdata to exportdata.
  end.

  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "agent_activity"
  
  /* replace the sequence numbers with blanks */
  close query {&browse-name}.
  for each bufferdata exclusive-lock:
    if fGroupBy:screen-value in frame {&frame-name} = "C"
     then bufferdata.showID = "".
    if bufferdata.showName = bufferdata.agentID
     then bufferdata.showName = bufferdata.name.
  end.
  open query {&browse-name} for each showdata.

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
  /* set the original data */
  close query {&browse-name}.
  empty temp-table bufferdata.
  for each exportdata exclusive-lock:
    create bufferdata.
    buffer-copy exportdata to bufferdata.
  end.
  open query {&browse-name} for each showdata.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  define variable cMsg as character no-undo.
  
  define buffer data for data.
  /* cleanup before getting showdata */
  clearData().
  empty temp-table data.
  dStartTime = now.
  run server/reportagentactivity.p (input tYear:screen-value in frame {&frame-name},
                                    input tCategory:screen-value in frame {&frame-name},
                                    input "ALL",
                                    input "ALL",
                                    output table data,
                                    output std-lo,
                                    output std-ch).
  
  if not std-lo
   then message "ReportAgentActivities failed: " + std-ch view-as alert-box warning.
   else
    do with frame {&frame-name}:
      run SetProgressStatus.
      std-in = 0.
      for each data exclusive-lock:
        /* set the type */
        data.typeDesc = data.type.
        publish "GetSysPropDesc" ("AMD", "Activity", "Type", data.type, output data.typeDesc).
        /* get the state */
        data.stateName = data.stateID.
        data.stateName = getStateName(data.stateID).
        /* get totals */
        empty temp-table tempdata.
        create tempdata.
        buffer-copy data to tempdata.
        assign
          data.qtr1 = getTotal(table tempdata, "Qtr1")
          data.qtr2 = getTotal(table tempdata, "Qtr2")
          data.qtr3 = getTotal(table tempdata, "Qtr3")
          data.qtr4 = getTotal(table tempdata, "Qtr4")
          data.yrTotal = getTotal(table tempdata, "Year")
          .
      end.
      run SetProgressStatus.
      /* add the types */
      for each data break by data.type by data.type:
        if first-of(data.type)
         then fTypes:add-first(data.typeDesc, data.type).
      end.
      /* sorting */
      if can-find(first data)
       then
        do:
          fTypes:screen-value = "C".
          lIsComparison = true.
          apply "VALUE-CHANGED" to fCompare.
          setFilterCombos("ALL").
          enableButtonsAndFilters(can-find(first showdata)).
          apply "VALUE-CHANGED" to browse {&browse-name}.
          fCompareValue:hidden = true.
        end.
      appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
    end.
  run SetProgressEnd.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  rowColor = ?.
  do with frame {&frame-name}:
    {lib/brw-sortData.i &post-by-clause="+ ' by showID by seq'" &ExcludeOpenQuery=true}
    /* remove the rows that don't match the criteria */
    std-ch = "". /* used to store the good keys */
    for each showdata no-lock
       where showdata.type = "C":
      
      std-lo = false.
      std-in = fCompareValue:input-value.
      case fCompareOperator:screen-value:
       when "gt" then 
        if showdata.yrTotal > std-in
         then std-lo = true.
       when "lt" then 
        if showdata.yrTotal < std-in
         then std-lo = true.
       when "ALL" then std-lo = true.
      end case.
      if std-lo
       then std-ch = addDelimiter(std-ch, ",") + showdata.showID.
    end.
    /* delete the rows that aren't in the variable */
    if lIsComparison
     then
      for each showdata exclusive-lock:
        if lookup(showdata.showID, std-ch) = 0
         then delete showdata.
      end.
    /* put the row color */
    for each showdata exclusive-lock break by showdata.showID:
      if first-of(showdata.showID)
       then rowColor = (if rowColor = {&oddColor} then {&evenColor} else {&oddColor}).
      
      showdata.rowColor = rowColor.
    end.
    /* remove the rows that don't match the criteria */
    browse {&browse-name}:sensitive = true.
    hQueryHandle:query-prepare(tQueryString).
    hQueryHandle:query-open().
    std-in = num-results("{&browse-name}").
    if lIsComparison
     then std-ch  = string(std-in / 3) + " comparison(s) shown".
     else std-ch = string(std-in) + " activity(s) shown".
    setStatus(std-ch).
    {lib/brw-totalData.i &noShow=lIsComparison &integerOnly=true &excludeColumn="2"}
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  if totalAdded
   then iOffset = 23.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5 - iOffset.
  
  /* resize the column width */
  {lib/resize-column.i &col="'name'" &var=dColumnWidth}
  
  {lib/brw-totalData.i &noShow=lIsComparison &integerOnly=true}
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION addComparisonRow C-Win 
FUNCTION addComparisonRow RETURNS LOGICAL
  ( input pType as character,
    input pshowID as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dMonthPlan as decimal extent 17 no-undo.
  define variable dMonthActual as decimal extent 17 no-undo.
  define buffer showdata for showdata.
  
  if lIsComparison
   then
    do:
      for each showdata exclusive-lock
         where showdata.showID = pshowID
      break by showdata.showID
            by showdata.type desc:
        
        empty temp-table tempdata.
        create tempdata.
        buffer-copy showdata to tempdata.
        case showdata.type:
         when "P" or
         when "I" then
          do:
            showdata.seq = 1.
            assign
              dMonthPlan[1] = showdata.month1
              dMonthPlan[2] = showdata.month2
              dMonthPlan[3] = showdata.month3
              dMonthPlan[4] = showdata.month4
              dMonthPlan[5] = showdata.month5
              dMonthPlan[6] = showdata.month6
              dMonthPlan[7] = showdata.month7
              dMonthPlan[8] = showdata.month8
              dMonthPlan[9] = showdata.month9
              dMonthPlan[10] = showdata.month10
              dMonthPlan[11] = showdata.month11
              dMonthPlan[12] = showdata.month12
              dMonthPlan[13] = showdata.qtr1
              dMonthPlan[14] = showdata.qtr2
              dMonthPlan[15] = showdata.qtr3
              dMonthPlan[16] = showdata.qtr4
              dMonthPlan[17] = getTotal(table tempdata, "Year")
              .
          end.
         when "A" or
         when "R" then
          do:
            showdata.seq = 2.
            assign
              dMonthActual[1] = showdata.month1
              dMonthActual[2] = showdata.month2
              dMonthActual[3] = showdata.month3
              dMonthActual[4] = showdata.month4
              dMonthActual[5] = showdata.month5
              dMonthActual[6] = showdata.month6
              dMonthActual[7] = showdata.month7
              dMonthActual[8] = showdata.month8
              dMonthActual[9] = showdata.month9
              dMonthActual[10] = showdata.month10
              dMonthActual[11] = showdata.month11
              dMonthActual[12] = showdata.month12
              dMonthActual[13] = showdata.qtr1
              dMonthActual[14] = showdata.qtr2
              dMonthActual[15] = showdata.qtr3
              dMonthActual[16] = showdata.qtr4
              dMonthActual[17] = getTotal(table tempdata, "Year")
              .
          end.
        end case.
      end.
      if can-find(first showdata where showdata.showID = pshowID and (showdata.type = "A" or showdata.type = "P"))
       then
        do:
          if not can-find(first showdata where showdata.showID = pshowID and showdata.type = "P")
           then createShowRow(1, "P", table tempdata).
          if not can-find(first showdata where showdata.showID = pshowID and showdata.type = "A")
           then createShowRow(2, "A", table tempdata).
        end.
      if can-find(first showdata where showdata.showID = pshowID and (showdata.type = "I" or showdata.type = "R"))
       then
        do:
          if not can-find(first showdata where showdata.showID = pshowID and showdata.type = "I")
           then createShowRow(1, "I", table tempdata).
          if not can-find(first showdata where showdata.showID = pshowID and showdata.type = "R")
           then createShowRow(2, "R", table tempdata).
        end.
      /* build the comparison row */
      create showdata.
      case pType:
       when "P" then assign showdata.month1 = ((dMonthActual[1] / dMonthPlan[1])) * 100
                            showdata.month2 = ((dMonthActual[2] / dMonthPlan[2])) * 100
                            showdata.month3 = ((dMonthActual[3] / dMonthPlan[3])) * 100
                            showdata.month4 = ((dMonthActual[4] / dMonthPlan[4])) * 100
                            showdata.month5 = ((dMonthActual[5] / dMonthPlan[5])) * 100
                            showdata.month6 = ((dMonthActual[6] / dMonthPlan[6])) * 100
                            showdata.month7 = ((dMonthActual[7] / dMonthPlan[7])) * 100
                            showdata.month8 = ((dMonthActual[8] / dMonthPlan[8])) * 100
                            showdata.month9 = ((dMonthActual[9] / dMonthPlan[9])) * 100
                            showdata.month10 = ((dMonthActual[10] / dMonthPlan[10])) * 100
                            showdata.month11 = ((dMonthActual[11] / dMonthPlan[11])) * 100
                            showdata.month12 = ((dMonthActual[12] / dMonthPlan[12])) * 100
                            showdata.qtr1 = ((dMonthActual[13] / dMonthPlan[13])) * 100
                            showdata.qtr2 = ((dMonthActual[14] / dMonthPlan[14])) * 100
                            showdata.qtr3 = ((dMonthActual[15] / dMonthPlan[15])) * 100
                            showdata.qtr4 = ((dMonthActual[16] / dMonthPlan[16])) * 100
                            showdata.yrTotal = ((dMonthActual[17] / dMonthPlan[17])) * 100
                            .
       when "D" then assign showdata.month1 = dMonthActual[1] - dMonthPlan[1]
                            showdata.month2 = dMonthActual[2] - dMonthPlan[2]
                            showdata.month3 = dMonthActual[3] - dMonthPlan[3]
                            showdata.month4 = dMonthActual[4] - dMonthPlan[4]
                            showdata.month5 = dMonthActual[5] - dMonthPlan[5]
                            showdata.month6 = dMonthActual[6] - dMonthPlan[6]
                            showdata.month7 = dMonthActual[7] - dMonthPlan[7]
                            showdata.month8 = dMonthActual[8] - dMonthPlan[8]
                            showdata.month9 = dMonthActual[9] - dMonthPlan[9]
                            showdata.month10 = dMonthActual[10] - dMonthPlan[10]
                            showdata.month11 = dMonthActual[11] - dMonthPlan[11]
                            showdata.month12 = dMonthActual[12] - dMonthPlan[12]
                            showdata.qtr1 = dMonthActual[13] - dMonthPlan[13]
                            showdata.qtr2 = dMonthActual[14] - dMonthPlan[14]
                            showdata.qtr3 = dMonthActual[15] - dMonthPlan[15]
                            showdata.qtr4 = dMonthActual[16] - dMonthPlan[16]
                            showdata.yrTotal = dMonthActual[17] - dMonthPlan[17]
                            .
      end case.
      for first tempdata no-lock:
        assign
          showdata.seq = 3
          showdata.agentID = tempdata.agentID
          showdata.showID = tempdata.showID
          showdata.stateID = tempdata.stateID
          showdata.corporationID = tempdata.corporationID
          showdata.name = tempdata.name
          showdata.showName = tempdata.showName
          showdata.type = "C"
          showdata.typeDesc = "Comparison"
          showdata.category = tempdata.category
          showdata.month1 = if showdata.month1 = ? then 0 else showdata.month1
          showdata.month2 = if showdata.month2 = ? then 0 else showdata.month2
          showdata.month3 = if showdata.month3 = ? then 0 else showdata.month3
          showdata.month4 = if showdata.month4 = ? then 0 else showdata.month4
          showdata.month5 = if showdata.month5 = ? then 0 else showdata.month5
          showdata.month6 = if showdata.month6 = ? then 0 else showdata.month6
          showdata.month7 = if showdata.month7 = ? then 0 else showdata.month7
          showdata.month8 = if showdata.month8 = ? then 0 else showdata.month8
          showdata.month9 = if showdata.month9 = ? then 0 else showdata.month9
          showdata.month10 = if showdata.month10 = ? then 0 else showdata.month10
          showdata.month11 = if showdata.month11 = ? then 0 else showdata.month11
          showdata.month12 = if showdata.month12 = ? then 0 else showdata.month12
          showdata.qtr1 = if showdata.qtr1 = ? then 0 else showdata.qtr1
          showdata.qtr2 = if showdata.qtr2 = ? then 0 else showdata.qtr2
          showdata.qtr3 = if showdata.qtr3 = ? then 0 else showdata.qtr3
          showdata.qtr4 = if showdata.qtr4 = ? then 0 else showdata.qtr4
          showdata.yrTotal = if showdata.yrTotal = ? then 0 else showdata.yrTotal
          .
      end.
    end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query {&browse-name}.
  empty temp-table data.
  empty temp-table showdata.
  hide message no-pause.
  enableButtonsAndFilters(false).
  fGroupBy:screen-value in frame {&frame-name} = "A".
  fTypes:list-item-pairs in frame {&frame-name} = "Comparison,C".
  fState:screen-value = "ALL".
  {lib/brw-totalData.i &noShow=true}
  setYTDColumn(fMonth:input-value).
  clearStatus().
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createShowRow C-Win 
FUNCTION createShowRow RETURNS LOGICAL
  ( input pSeq as integer,
    input pType as character,
    input table for tempdata ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  for first tempdata no-lock:
  create showdata.
    assign
      showdata.seq = pSeq
      showdata.agentID = tempdata.agentID
      showdata.showID = tempdata.showID
      showdata.stateID = tempdata.stateID
      showdata.corporationID = tempdata.corporationID
      showdata.name = tempdata.name
      showdata.showName = tempdata.showName
      showdata.type = pType
      showdata.category = tempdata.category
      showdata.month1 = 0
      showdata.month2 = 0
      showdata.month3 = 0
      showdata.month4 = 0
      showdata.month5 = 0
      showdata.month6 = 0
      showdata.month7 = 0
      showdata.month8 = 0
      showdata.month9 = 0
      showdata.month10 = 0
      showdata.month11 = 0
      showdata.month12 = 0
      showdata.qtr1 = 0
      showdata.qtr2 = 0
      showdata.qtr3 = 0
      showdata.qtr4 = 0
      showdata.yrTotal = 0
      .
    publish "GetSysPropDesc" ("AMD", "Activity", "Type", pType, output showdata.typeDesc).
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doFilterSort C-Win 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo initial "".
  
  define variable cStateID as character no-undo.
  define variable cAgentID as character no-undo.
  define variable cView as character no-undo.
  do with frame {&frame-name}:
    assign
      cStateID = fState:screen-value
      cAgentID = fAgent:screen-value
      cView = fTypes:screen-value
      .
  end.
  
  /* build the query */
  if cStateID <> "ALL" and cStateID <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stateID = '" + cStateID + "'".
  if cAgentID <> "ALL" and cAgentID <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "agentID = '" + cAgentID + "'".
  if cView <> "C"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "type = '" + cView + "'".
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
   
  RETURN tWhereClause.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableButtonsAndFilters C-Win 
FUNCTION enableButtonsAndFilters RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable lCompare as logical no-undo.
  do with frame {&frame-name}:
    run AgentComboEnable in this-procedure (pEnable).
    enableButtons(pEnable).
    enableFilters(pEnable).
    assign
      /* disable the browse */
      browse {&browse-name}:sensitive = pEnable
      /* disable the filters */
      fCompare:sensitive = (pEnable and lIsComparison)
      fCompareOperator:sensitive = (pEnable and lIsComparison)
      lCompare = (fCompareOperator:screen-value = "ALL")
      fCompareValue:hidden = not pEnable and lCompare
      .
    if lIsComparison and fCompare:screen-value = "P"
     then bExport:sensitive = false.
     else bExport:sensitive = true.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getTotal C-Win 
FUNCTION getTotal RETURNS DECIMAL PRIVATE
  ( input table for tempdata,
    input pType as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable dReturn as decimal no-undo.
  define variable iMonth as integer no-undo.
  define buffer tempdata for tempdata.
  
  iMonth = fMonth:input-value in frame {&frame-name}.
  for first tempdata no-lock:
    case pType:
     when "Qtr1" then dReturn = tempdata.month1 + tempdata.month2 + tempdata.month3.
     when "Qtr2" then dReturn = tempdata.month4 + tempdata.month5 + tempdata.month6.
     when "Qtr3" then dReturn = tempdata.month7 + tempdata.month8 + tempdata.month9.
     when "Qtr4" then dReturn = tempdata.month10 + tempdata.month11 + tempdata.month12.
     when "Year" then
      do:
        iMonth = fMonth:input-value in frame {&frame-name}.
        dReturn = (if iMonth >= 1  then tempdata.month1  else 0)
                + (if iMonth >= 2  then tempdata.month2  else 0)
                + (if iMonth >= 3  then tempdata.month3  else 0)
                + (if iMonth >= 4  then tempdata.month4  else 0)
                + (if iMonth >= 5  then tempdata.month5  else 0)
                + (if iMonth >= 6  then tempdata.month6  else 0)
                + (if iMonth >= 7  then tempdata.month7  else 0)
                + (if iMonth >= 8  then tempdata.month8  else 0)
                + (if iMonth >= 9  then tempdata.month9  else 0)
                + (if iMonth >= 10 then tempdata.month10 else 0)
                + (if iMonth >= 11 then tempdata.month11 else 0)
                + (if iMonth >= 12 then tempdata.month12 else 0).
      end.
    end case.
  end.
  return dReturn.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setYTDColumn C-Win 
FUNCTION setYTDColumn RETURNS LOGICAL
  ( input pMonth as integer ) :
/*------------------------------------------------------------------------------
@description Sets the YTD column
------------------------------------------------------------------------------*/
  define variable hYTD as handle no-undo.
  
  do with FRAME {&frame-name}:
    hYTD = getColumn(browse {&browse-name}:handle, "yrTotal").
    if valid-handle(hYTD)
     then hYTD:label = "Thru " + substring(entry(lookup(string(pMonth, "99"), fMonth:list-item-pairs) - 1, fMonth:list-item-pairs), 1, 3).
  end.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

