&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* businessmix.w
   Data Call report
   B.Johnson 11.24.2015
   */

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/get-column.i}

def var tLiabRange as char no-undo.
def var tGettingData as logical.
def var tKeepGettingData as logical.
def var sPeriodID as int no-undo.
def var ePeriodID as int no-undo.
def var dColumnWidth as decimal no-undo.

{tt/period.i}
{tt/statcode.i}
{tt/stateform.i}
{tt/agent.i}
{tt/state.i}

/* The following table holds summary data based on a set of two keys:
    StateID or AgentID 
    and 
    STATCode or FormID

   Each record in the data table is for a range of data values.

   Type     ID       codeType Code      Meaning
   ----     -------- -------- ----      -------
    S       StateID   S       STAT      Values for the state/STATcode combo
    S       StateID   S       <blank>   Subtotal for all STATcodes in the state
    S       StateID   F       FormID    Values for the state/formID combo
    S       StateID   F       <blank>   Subtotal for all FormIDs in the state
    A       AgentID   S       STAT      Values for the agent/STATcode combo
    A       AgentID   F       FormID    Values for the agent/FormID combo
    <blank> ALL       <blank> <blank>   Totals for all records in all states

   Conceptually, we also should have subtotals for all STATcodes and FormIDs
   for an agent.  We may need to implement this later.

   ---

   The seq and *Calc fields are used in export to CSV for use in Excel.
 */

{tt/periodmix.i &tableAlias="tempData"}
def temp-table data like tempData
 field seq as int
 field grossRateCalc as char
 field netRateCalc as char
 field retainedRateCalc as char
 field grossRangeCalc as char
 field netRangeCalc as char
 field retainedRangeCalc as char
 field liabilityRangeCalc as char
 index pCodeLiab code minLiab maxLiab
 .

def temp-table viewData like data.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES viewData

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData viewData.code viewData.description viewData.numForms viewData.numPolicies viewData.grossPremium viewData.endorsementPremium viewData.otherPremium viewData.totalPremium viewData.retainedPremium viewData.netPremium viewData.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH viewData
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH viewData.
&Scoped-define TABLES-IN-QUERY-brwData viewData
&Scoped-define FIRST-TABLE-IN-QUERY-brwData viewData


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh sMonth sYear eMonth eYear fState ~
fAgent bRanges brwData 
&Scoped-Define DISPLAYED-OBJECTS sMonth sYear eMonth eYear fExportAll ~
fState fAgent fDetailTotal fCode 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayCodeTotals C-Win 
FUNCTION displayCodeTotals RETURNS LOGICAL
  ( pType as char,
    pID as char,
    pCodeType as char,
    pCode as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayDataTotals C-Win 
FUNCTION displayDataTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL PRIVATE
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bRanges  NO-FOCUS
     LABEL "Ranges..." 
     SIZE 7.2 BY 1.71 TOOLTIP "Set Liability Distribution Ranges".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE eMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE eYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE fAgent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 92 BY 1 NO-UNDO.

DEFINE VARIABLE fCode AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Code" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 26.2 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE sMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "From" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDetailTotal AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Detail", "D",
"Total", "T"
     SIZE 20 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 101.8 BY 4.52.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 4.52.

DEFINE VARIABLE fExportAll AS LOGICAL INITIAL no 
     LABEL "Include subtotals" 
     VIEW-AS TOGGLE-BOX
     SIZE 20 BY .81 NO-UNDO.

DEFINE VARIABLE iCodeForms AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Endorsements" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE iCodeGrossPremium AS DECIMAL FORMAT "-z,zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total Premium" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE iCodeNetPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE iCodePolicies AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Policies" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE iCodeRetainedPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Retained Premium" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE iCodeTotalLiability AS DECIMAL FORMAT "-z,zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total Liability" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE iStatCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "STAT Code or FormID" NO-UNDO.

DEFINE VARIABLE iStatCodeDesc AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.4 BY 1 NO-UNDO.

DEFINE VARIABLE tForms AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Endorsements" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "-z,zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total Premium" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicies AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Policies" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRetainedPremium AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Retained Premium" 
     VIEW-AS FILL-IN 
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalLiability AS DECIMAL FORMAT "-z,zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total Liability" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      viewData SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      viewData.code label "Code" format "x(20)" width 8
viewData.description label "Range" format "x(60)" width 45
viewData.numForms column-label "No. of!Endsmts" format "zzz,zzz"
viewData.numPolicies column-label "No. of!Policies" format "zzz,zzz"
viewData.grossPremium column-label "Gross!Premium" format "-zzz,zzz,zz9.99"
viewData.endorsementPremium column-label "Endorsement!Premium" format "-zzz,zzz,zz9.99"
viewData.otherPremium column-label "Other!Premium" format "-zzz,zzz,zz9.99"
viewData.totalPremium column-label "Total!Premium" format "-zzz,zzz,zz9.99"
viewData.retainedPremium column-label "Retained!Premium" format "-zzz,zzz,zz9.99"
viewData.netPremium column-label "Net!Premium" format "-zzz,zzz,zz9.99"
viewData.amount column-label "Total!Liability" format "-zzz,zzz,zzz,zz9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 195 BY 16.43 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.71 COL 95.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.71 COL 80.6 WIDGET-ID 4 NO-TAB-STOP 
     sMonth AT ROW 1.95 COL 8 COLON-ALIGNED WIDGET-ID 16
     sYear AT ROW 1.95 COL 26.2 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     eMonth AT ROW 1.95 COL 44 COLON-ALIGNED WIDGET-ID 96
     eYear AT ROW 1.95 COL 62.2 COLON-ALIGNED NO-LABEL WIDGET-ID 98
     fExportAll AT ROW 4.33 COL 117 WIDGET-ID 78 NO-TAB-STOP 
     fState AT ROW 3.14 COL 8 COLON-ALIGNED WIDGET-ID 82
     fAgent AT ROW 4.33 COL 8 COLON-ALIGNED WIDGET-ID 84
     fDetailTotal AT ROW 1.86 COL 117 NO-LABEL WIDGET-ID 90
     fCode AT ROW 3.14 COL 115 COLON-ALIGNED WIDGET-ID 76
     bRanges AT ROW 1.71 COL 88 WIDGET-ID 46 NO-TAB-STOP 
     brwData AT ROW 5.95 COL 2 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1 COL 3.4 WIDGET-ID 66
     "Show:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 2 COL 110.2 WIDGET-ID 94
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 1 COL 104.8 WIDGET-ID 70
     RECT-35 AT ROW 1.24 COL 103.6 WIDGET-ID 68
     RECT-34 AT ROW 1.24 COL 2 WIDGET-ID 44
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 196.8 BY 27.71 WIDGET-ID 100.

DEFINE FRAME fTotals
     tNetPremium AT ROW 1.24 COL 111 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tGrossPremium AT ROW 1.24 COL 161 COLON-ALIGNED WIDGET-ID 24 NO-TAB-STOP 
     tForms AT ROW 2.43 COL 22 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     tPolicies AT ROW 2.43 COL 46 COLON-ALIGNED WIDGET-ID 30 NO-TAB-STOP 
     tRetainedPremium AT ROW 2.43 COL 111 COLON-ALIGNED WIDGET-ID 32 NO-TAB-STOP 
     tTotalLiability AT ROW 2.43 COL 161 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     "Totals" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.24 COL 2 WIDGET-ID 80
          FONT 6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 25.67
         SIZE 195 BY 2.76 WIDGET-ID 500.

DEFINE FRAME fStatCode
     iStatCode AT ROW 1.19 COL 17.8 WIDGET-ID 2 NO-TAB-STOP 
     iStatCodeDesc AT ROW 1.19 COL 36.6 COLON-ALIGNED NO-LABEL WIDGET-ID 4 NO-TAB-STOP 
     iCodeNetPremium AT ROW 1.24 COL 117 COLON-ALIGNED WIDGET-ID 10 NO-TAB-STOP 
     iCodeGrossPremium AT ROW 1.24 COL 161 COLON-ALIGNED WIDGET-ID 8 NO-TAB-STOP 
     iCodeForms AT ROW 2.43 COL 22 COLON-ALIGNED WIDGET-ID 6 NO-TAB-STOP 
     iCodePolicies AT ROW 2.43 COL 46 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     iCodeRetainedPremium AT ROW 2.43 COL 117 COLON-ALIGNED WIDGET-ID 12 NO-TAB-STOP 
     iCodeTotalLiability AT ROW 2.43 COL 161 COLON-ALIGNED WIDGET-ID 14 NO-TAB-STOP 
     "Subtotals" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.24 COL 2 WIDGET-ID 30
          FONT 6
    WITH 1 DOWN NO-HIDE KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2 ROW 22.67
         SIZE 195 BY 2.76
         BGCOLOR 15  WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Business Mix"
         HEIGHT             = 27.71
         WIDTH              = 196.8
         MAX-HEIGHT         = 35.57
         MAX-WIDTH          = 228.6
         VIRTUAL-HEIGHT     = 35.57
         VIRTUAL-WIDTH      = 228.6
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fStatCode:FRAME = FRAME fMain:HANDLE
       FRAME fTotals:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fTotals:MOVE-AFTER-TAB-ITEM (fCode:HANDLE IN FRAME fMain)
       XXTABVALXX = FRAME fTotals:MOVE-BEFORE-TAB-ITEM (brwData:HANDLE IN FRAME fMain)
       XXTABVALXX = FRAME fStatCode:MOVE-AFTER-TAB-ITEM (brwData:HANDLE IN FRAME fMain)
/* END-ASSIGN-TABS */.

/* BROWSE-TAB brwData bRanges fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX fCode IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET fDetailTotal IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX fExportAll IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-34 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME fStatCode
                                                                        */
/* SETTINGS FOR FILL-IN iCodeForms IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iCodeForms:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iCodeGrossPremium IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iCodeGrossPremium:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iCodeNetPremium IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iCodeNetPremium:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iCodePolicies IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iCodePolicies:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iCodeRetainedPremium IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iCodeRetainedPremium:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iCodeTotalLiability IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iCodeTotalLiability:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iStatCode IN FRAME fStatCode
   NO-DISPLAY ALIGN-L                                                   */
ASSIGN 
       iStatCode:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FILL-IN iStatCodeDesc IN FRAME fStatCode
   NO-DISPLAY                                                           */
ASSIGN 
       iStatCodeDesc:READ-ONLY IN FRAME fStatCode        = TRUE.

/* SETTINGS FOR FRAME fTotals
                                                                        */
/* SETTINGS FOR FILL-IN tForms IN FRAME fTotals
   NO-DISPLAY                                                           */
ASSIGN 
       tForms:READ-ONLY IN FRAME fTotals        = TRUE.

/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fTotals
   NO-DISPLAY                                                           */
ASSIGN 
       tGrossPremium:READ-ONLY IN FRAME fTotals        = TRUE.

/* SETTINGS FOR FILL-IN tNetPremium IN FRAME fTotals
   NO-DISPLAY                                                           */
ASSIGN 
       tNetPremium:READ-ONLY IN FRAME fTotals        = TRUE.

/* SETTINGS FOR FILL-IN tPolicies IN FRAME fTotals
   NO-DISPLAY                                                           */
ASSIGN 
       tPolicies:READ-ONLY IN FRAME fTotals        = TRUE.

/* SETTINGS FOR FILL-IN tRetainedPremium IN FRAME fTotals
   NO-DISPLAY                                                           */
ASSIGN 
       tRetainedPremium:READ-ONLY IN FRAME fTotals        = TRUE.

/* SETTINGS FOR FILL-IN tTotalLiability IN FRAME fTotals
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalLiability:READ-ONLY IN FRAME fTotals        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH viewData.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fStatCode
/* Query rebuild information for FRAME fStatCode
     _Query            is NOT OPENED
*/  /* FRAME fStatCode */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.52
       COLUMN          = 80.2
       HEIGHT          = .48
       WIDTH           = 21.8
       TAB-STOP        = no
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Business Mix */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Business Mix */
DO:
  if tGettingData 
   then return no-apply.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Business Mix */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRanges
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRanges C-Win
ON CHOOSE OF bRanges IN FRAME fMain /* Ranges... */
DO:
  publish "GetLiabilityDistribution" (output std-ch).
  run dialogliabrange.w (std-ch, output tLiabRange).
  if std-ch <> tLiabRange 
     and tLiabRange > ""
   then publish "SetLiabilityDistribution" (tLiabRange).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  assign
    sMonth 
    sYear
    eMonth 
    eYear
    fState
    fAgent.
  
  assign
    sPeriodID = 0
    ePeriodID = 0.
  
  find first period where period.periodMonth = sMonth:input-value
                    and period.periodYear = sYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Starting period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  sPeriodID = period.periodID.
  
  find first period where period.periodMonth = eMonth:input-value
                    and period.periodYear = eYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Ending period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  ePeriodID = period.periodID.

  if sPeriodID = 0 or ePeriodID = 0 or ePeriodID < sPeriodID then
  do:
    message "Ending period must be equal or later than starting period."
      view-as alert-box error.
    return no-apply.
  end.
  
  if fState:input-value = "" or fState:input-value = ? or fState:input-value = "ALL"
  or fAgent:input-value = "" or fAgent:input-value = ? then
  do:
    message
      "You must select a single state or agent."
      view-as alert-box.
    return no-apply.
  end.
  
  assign
    bRefresh:sensitive in frame {&frame-name} = false
    bExport:sensitive in frame {&frame-name} = false
    sMonth:sensitive in frame {&frame-name} = false
    sYear:sensitive in frame {&frame-name} = false
    eMonth:sensitive in frame {&frame-name} = false
    eYear:sensitive in frame {&frame-name} = false
    bRanges:sensitive in frame {&frame-name} = false
    fState:sensitive in frame {&frame-name} = false
    fAgent:sensitive in frame {&frame-name} = false
    .

  clearData().
  run getData in this-procedure.

  assign
    bRefresh:sensitive in frame {&frame-name} = true
    bExport:sensitive in frame {&frame-name} = true
    sMonth:sensitive in frame {&frame-name} = true
    sYear:sensitive in frame {&frame-name} = true
    eMonth:sensitive in frame {&frame-name} = true
    eYear:sensitive in frame {&frame-name} = true
    bRanges:sensitive in frame {&frame-name} = true
    fState:sensitive in frame {&frame-name} = true
    fAgent:sensitive in frame {&frame-name} = true
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available viewData 
    or (available viewData and viewData.numForms = 0)
   then return.

  MESSAGE "Gross: " viewData.lowGross " to " viewData.highGross skip(2)
          "Retention: " viewData.lowRetained " to " viewData.highRetained skip(2)
          "Net: " viewData.lowNet " to " viewData.highNet skip(2)
          "data: " viewData.lowLiability " to " viewData.highLiability skip
      VIEW-AS ALERT-BOX INFO BUTTONS OK title "Details".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  
  if not available viewData 
   then 
    do: clear frame fStatCode.
        pause 0.
        return.
    end.
  displayCodeTotals(viewData.type, viewData.ID, viewData.codeType, viewData.code).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eMonth C-Win
ON VALUE-CHANGED OF eMonth IN FRAME fMain /* To */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eYear C-Win
ON VALUE-CHANGED OF eYear IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCode C-Win
ON VALUE-CHANGED OF fCode IN FRAME fMain /* Code */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDetailTotal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDetailTotal C-Win
ON VALUE-CHANGED OF fDetailTotal IN FRAME fMain
DO:
  if self:input-value = "D" then
  assign
    fCode:screen-value = "ALL"
    fCode:sensitive = yes.
  else
  assign
    fCode:screen-value = "ALL"
    fCode:sensitive = no.
    
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sMonth C-Win
ON VALUE-CHANGED OF sMonth IN FRAME fMain /* From */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

status default "" in window {&window-name}.
status input off.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRanges:load-image("images/docs.bmp").
bRanges:load-image-insensitive("images/docs-i.bmp").

subscribe to "DataError" anywhere.

publish "GetLiabilityDistribution" (output tLiabRange).
publish "GetStatCodes" (output table statcode).
publish "GetStateForms" (output table stateform).

session:immediate-display = yes.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=sMonth &yr=sYear}
    {lib/get-period-list.i &mth=eMonth &yr=eYear}
    {lib/get-state-list.i &combo=fState &addAll=true}
    {lib/get-agent-list.i &combo=fAgent &state=fState &addAll=true}
    /* set the values */
    {lib/set-current-value.i &state=fState &agent=fAgent}
    {lib/set-current-value.i &mth=sMonth &yr=sYear}
    {lib/set-current-value.i &mth=eMonth &yr=eYear}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'description'" &var=dColumnWidth}

  run windowResized.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "businessmix.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "businessmix.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input param pMsg as char.

  /* Since Progress is single-threaded, this works */
  if not tGettingData 
   then return.
  
  tKeepGettingData = false.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the systems administrator if the problem persists."
    view-as alert-box error.

  clearData().
  
  assign
    bRefresh:sensitive in frame {&frame-name} = true
    bExport:sensitive in frame {&frame-name} = true
    sMonth:sensitive in frame {&frame-name} = true
    sYear:sensitive in frame {&frame-name} = true
    eMonth:sensitive in frame {&frame-name} = true
    eYear:sensitive in frame {&frame-name} = true
    bRanges:sensitive in frame {&frame-name} = true
    fState:sensitive in frame {&frame-name} = true
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY sMonth sYear eMonth eYear fExportAll fState fAgent fDetailTotal fCode 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh sMonth sYear eMonth eYear fState fAgent bRanges brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  ENABLE iStatCode iStatCodeDesc iCodeNetPremium iCodeGrossPremium iCodeForms 
         iCodePolicies iCodeRetainedPremium iCodeTotalLiability 
      WITH FRAME fStatCode IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fStatCode}
  ENABLE tNetPremium tGrossPremium tForms tPolicies tRetainedPremium 
         tTotalLiability 
      WITH FRAME fTotals IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fTotals}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var th as handle no-undo.
  def var tSeq as int.
  def buffer dataCalc for viewData.
  def buffer dataAll for viewData.
 
  th = temp-table viewData:handle.
 
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
 
  if fExportAll:checked in frame {&frame-name} then
  do:
     for each dataCalc 
         where not dataCalc.description matches "*TOTAL*"
         break by dataCalc.code:
        
       if first-of(dataCalc.code) then
       do: 
         create dataAll.
         buffer-copy dataCalc to dataAll.
   
         assign
           dataAll.lowGross = ?
           dataAll.highGross = ?
           dataAll.grossRangeCalc = ?
           dataAll.lowNet = ?
           dataAll.highNet = ?
           dataAll.netRangeCalc = ?
           dataAll.lowRetained = ?
           dataAll.highRetained = ?
           dataAll.retainedRangeCalc = ?
           dataAll.lowLiability = ?
           dataAll.highLiability = ?
           dataAll.liabilityRangeCalc = ?
           dataAll.grossRateCalc = ?
           dataAll.netRateCalc = ?
           dataAll.retainedRateCalc = ?
           dataAll.minLiab = 9999999999
           dataAll.maxLiab = 999999999.99
           dataAll.description = if fDetailTotal:input-value = "D" 
                                 then "$ TOTAL (" + dataCalc.code + ")"
                                 else "$ TOTAL"
           .
         next.
       end.
         
       find dataAll where dataAll.type = dataCalc.type
                    and dataAll.ID = dataCalc.ID
                    and dataAll.codeType = dataCalc.codeType
                    and dataAll.code = dataCalc.code
                    and dataAll.description begins "$ TOTAL".
                  
       assign
         dataAll.numForms = dataAll.numForms + dataCalc.numForms
         dataAll.numPolicies = dataAll.numPolicies + dataCalc.numPolicies
         dataAll.grossPremium = dataAll.grossPremium + dataCalc.grossPremium
         dataAll.endorsementPremium = dataAll.endorsementPremium + dataCalc.endorsementPremium
         dataAll.otherPremium = dataAll.otherPremium + dataCalc.otherPremium
         dataAll.totalPremium = dataAll.totalPremium + dataCalc.totalPremium
         dataAll.amount = dataAll.amount + dataCalc.amount
         dataAll.retainedPremium = dataAll.retainedPremium + dataCalc.retainedPremium
         dataAll.netPremium = dataAll.netPremium + dataCalc.netPremium
         .
     end.
 
     if fDetailTotal:input-value = "D" 
     and fCODE:screen-value in frame {&frame-name} = "ALL" then
     for each data
         where data.code = "TOTAL"
         break by data.code:
             
       if first(data.code) then
       do: 
         create dataAll.
         buffer-copy data to dataAll.
     
         assign
           dataAll.lowGross = ?
           dataAll.highGross = ?
           dataAll.grossRangeCalc = ?
           dataAll.lowNet = ?
           dataAll.highNet = ?
           dataAll.netRangeCalc = ?
           dataAll.lowRetained = ?
           dataAll.highRetained = ?
           dataAll.retainedRangeCalc = ?
           dataAll.lowLiability = ?
           dataAll.highLiability = ?
           dataAll.liabilityRangeCalc = ?
           dataAll.grossRateCalc = ?
           dataAll.netRateCalc = ?
           dataAll.retainedRateCalc = ?
           dataAll.code = "GRAND TOTAL"
           dataAll.codeType = "TOTAL"
           dataAll.minLiab = 0
           dataAll.maxLiab = 999999999.99
           dataAll.description = "$ GRAND TOTAL"
           .
         next.
       end.
             
       find dataAll where dataAll.codeType = "TOTAL".
       
       assign
         dataAll.numForms = dataAll.numForms + data.numForms
         dataAll.numPolicies = dataAll.numPolicies + data.numPolicies
         dataAll.grossPremium = dataAll.grossPremium + data.grossPremium
         dataAll.endorsementPremium = dataAll.endorsementPremium + data.endorsementPremium
         dataAll.otherPremium = dataAll.otherPremium + data.otherPremium
         dataAll.totalPremium = dataAll.totalPremium + data.totalPremium
         dataAll.amount = dataAll.amount + data.amount
         dataAll.retainedPremium = dataAll.retainedPremium + data.retainedPremium
         dataAll.netPremium = dataAll.netPremium + data.netPremium
         .
     end.
  end.
 
  tSeq = 1. /* The first row is labels */
  
  for each dataCalc
    by dataCalc.type
    by dataCalc.ID
    by dataCalc.codeType
    by dataCalc.code
    by dataCalc.minLiab:
     
     tSeq = tSeq + 1.
     dataCalc.seq = tSeq.
     std-ch = string(tSeq).
 
     if dataCalc.amount <> 0 
      then assign
             dataCalc.grossRateCalc = '=G' + std-ch + '/(J' + std-ch + '/1000)'
             dataCalc.netRateCalc = '=M' + std-ch + '/(J' + std-ch + '/1000)'
             dataCalc.retainedRateCalc = '=L' + std-ch + '/(J' + std-ch + '/1000)'
             .
     if dataCalc.lowGross <> ? and dataCalc.highGross <> ?
      then dataCalc.grossRangeCalc = '=U' + std-ch + '-T' + std-ch.
     if dataCalc.lowNet <> ? and dataCalc.highNet <> ? 
      then dataCalc.netRangeCalc = '=X' + std-ch + '-W' + std-ch.
     if dataCalc.lowRetained <> ? and dataCalc.highRetained <> ? 
      then dataCalc.retainedRangeCalc = '=AA' + std-ch + '-Z' + std-ch.
     if dataCalc.lowLiability <> ? and dataCalc.highLiability <> ? 
      then dataCalc.liabilityRangeCalc = '=AD' + std-ch + '-AC' + std-ch.
  end.
 
  /* It's a hack to get the subtotal below the code...*/
  for each dataCalc
    where dataAll.minLiab = 9999999999:
   dataCalc.minLiab = 0.
  end.
 
  publish "GetReportDir" (output std-ch).
  run util/exporttable.p (table-handle th,
                          "viewData",
                          "for each viewData by viewData.seq ",
                          "ID,code,codeDesc,description,numForms,numPolicies,grossPremium,endorsementPremium,otherPremium,totalPremium,amount,retainedPremium,netPremium,type,codeType,sPeriodID,ePeriodID,minLiab,maxLiab,lowGross,highGross,grossRangeCalc,lowNet,highNet,netRangeCalc,lowRetained,highRetained,retainedRangeCalc,lowLiability,highLiability,liabilityRangeCalc,grossRateCalc,netRateCalc,retainedRateCalc",
                          "RefID,Transaction Code,Transaction Desc,Liability Category,No of Endsmts,No of Policies,Gross Premium,Endorsement Premium,Other Premium,Total Premium,Total Liability,Retained Premium,Net Premium,Type,Code Type,Starting Period,Ending Period,Minimum Liability,Maximum Liability,Low Gross,High Gross,Gross Range,Low Net,High Net,Net Range,Low Retained,High Retained,Retained Range,Low Liability,High Liability,Liability Range,Gross Rate,Net Rate,Retained Rate",
                          /* Since using the letters "ID" at the very beginning of the file makes 
                             Excel think it's a SYLK file, we're using the label 'RefID' instead.
                             See http://support.microsoft.com/en-us/kb/215591 for more info */
                          std-ch,
                          "DataCall_" + fState:screen-value in frame fMain 
                           + "_" + string(sPeriodID)
                           + "_" + string(ePeriodID)
                           + ".csv",
                          true,
                          output std-ch,
                          output std-in).
 
  if fExportAll:checked in frame {&frame-name} 
   then
    for each dataAll
      where dataAll.description begins "$ TOTAL"
         or dataAll.description begins "$ GRAND":
     delete dataAll.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer data for data.
 def buffer viewData for viewData.

 hide message no-pause.
 clear frame fMain.
 clear frame fStatCode.
 close query brwData.
 empty temp-table viewData.

 if fDetailTotal:input-value = "D" then
 for each data where data.code <> "TOTAL":
  if fCode:screen-value in frame {&frame-name} <> "ALL" 
     and data.code <> fCode:screen-value 
   then next.
  create viewData.
  buffer-copy data to viewData.
 end.
 else
 if fDetailTotal:input-value = "T" then
 for each data where data.code = "TOTAL":
  create viewData.
  buffer-copy data to viewData.
 end.
 
 if dataSortBy = "code"
  then dataSortDesc = not dataSortDesc. /* Reverse the default behavior */
 run sortData ("code").

 apply "ENTRY" TO brwData in frame {&frame-name}.
 if available viewData 
  then 
   do: apply "VALUE-CHANGED" to brwData.
       bExport:sensitive in frame {&frame-name} = true.
       fExportAll:sensitive = true.
   end.

 hide message no-pause.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer data for data.

 def var tNumTasks as int.
 def var tCurTask as int.
 def var tStartTime as datetime.


 clearData().
 pbMinStatus().

 fDetailTotal:sensitive in frame {&frame-name} = false.
 fCode:sensitive in frame {&frame-name} = false.
 bExport:sensitive = false.
 fExportAll:sensitive = false.

 /* In case of invalid UI input */
 empty temp-table data.
 empty temp-table tempData.

 tGettingData = true.
 tkeepGettingData = true.

 tStartTime = now.
 hide message no-pause.
 message "Started " + string(tStartTime).
 message "Getting Data".

 tNumTasks = 0.

 GETTING-DATA:
 do:

      tCurTask = 1.
      pbUpdateStatus(tCurTask, 0).
    
      process events.
      if not tKeepGettingData 
       then leave GETTING-DATA.

      hide message no-pause.
      message " ".
      message "Getting Data".
      pause 0.
    
      run server/getDataCallMix.p (input sPeriodID,
                                   input ePeriodID,
                                   input fState:input-value in frame fMain,
                                   input fAgent:input-value in frame fMain,
                                   input tLiabRange,
                                   output table tempData,
                                   output std-lo,
                                   output std-ch).

      if not std-lo 
       then
        do: hide message no-pause.
            tKeepGettingData = false.
            MESSAGE "Unable to get report data." skip(1) std-ch
                VIEW-AS ALERT-BOX warning BUTTONS OK.
            leave GETTING-DATA.
        end.

      hide message no-pause.
      message " ".
      message "Receiving Data " + " (" + string(interval(now, tStartTime, "seconds")) + " seconds)".
      pause 0.

      pbUpdateStatus(50, 0). /* 50% done */
    
      process events.
      if not tKeepGettingData 
       then leave GETTING-DATA.

      for each tempData:
       create data.
       buffer-copy tempData to data.
      end.

      process events.
      if not tKeepGettingData 
       then leave GETTING-DATA.

     pbUpdateStatus(99, 0). /* Don't hit 100%...yet */

 end. /* GETTING-DATA block */

 if not tKeepGettingData 
  then
   do:
       empty temp-table data.
       empty temp-table tempData.

       hide message no-pause.
       message " ".
       message "Cancelled (" + string(now) + ")"
            + "  " + "Duration of " + string(interval(now, tStartTime, "seconds")) + " seconds".
       pause 0.

       pbUpdateStatus(100, 1).
       pbMinStatus().
       tGettingData = false.
       return.
   end.


 for each data:
  if data.amount <> 0
   then assign
          data.grossRate = data.totalPremium / (data.amount / 1000)
          data.netRate = data.netPremium / (data.amount / 1000).
  if data.lowGross = 999999999 
   then data.lowGross = ?.
  if data.lowNet = 999999999 
   then data.lowNet = ?.
  if data.lowRetained = 999999999 
   then data.lowRetained = ?.
  if data.lowLiability = 999999999 
   then data.lowLiability = ?.
  if data.highGross = -999999999 
   then data.highGross = ?.
  if data.highNet = -999999999 
   then data.highNet = ?.
  if data.highRetained = -999999999 
   then data.highRetained = ?.
  if data.highLiability = -999999999 
   then data.highLiability = ?.
 end.

 empty temp-table tempData.

 hide message no-pause.
 message "Completed".
 message sMonth:screen-value in frame fMain 
    + "/" + sYear:screen-value in frame fMain
    + "-" + eMonth:screen-value in frame fMain 
    + "/" + eYear:screen-value in frame fMain
    + "  (" + string(now) + ")"
    + "  " + "Duration of " + string(interval(now, tStartTime, "seconds")) + " seconds".

 /* Determine Code Filters */
 std-ch = "ALL".
 for each data where data.code <> "TOTAL"
   break by data.code:
  if first-of(data.code) 
   then std-ch = std-ch + "," + data.code.
 end.
 fCode:list-items in frame {&frame-name} = std-ch.
 fCode:screen-value = "ALL".
 fDetailTotal:screen-value = "D".

 run filterData.

 displayDataTotals().

 fDetailTotal:sensitive = true.
 fCode:sensitive = true.

 pbUpdateStatus(100, 1).
 pbMinStatus().
 tGettingData = false.
 hide message no-pause.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i &post-by-clause="+ ' by viewData.minLiab '"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 130.

 frame fStatCode:y = frame {&frame-name}:height-pixels - 126.
 frame fTotals:y = frame {&frame-name}:height-pixels - 63.
 
  /* resize the column */
  {lib/resize-column.i &col="'description'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  hide message no-pause.
  close query brwData.
  clear frame fStatCode.
  clear frame fTotals.
  do with FRAME {&frame-name}:
    fDetailTotal:screen-value = "D".
    fDetailTotal:sensitive = false.
    fCode:screen-value in frame {&frame-name} = "ALL".
    fCode:sensitive = false.
    bExport:sensitive = false.
    fExportAll:sensitive = false.
  end.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayCodeTotals C-Win 
FUNCTION displayCodeTotals RETURNS LOGICAL
  ( pType as char,
    pID as char,
    pCodeType as char,
    pCode as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  def buffer x-data for data.
  def buffer agent for agent.

  assign
    iCodePolicies = 0
    iCodeForms = 0
    iCodeGrossPremium = 0
    iCodeNetPremium = 0
    iCodeRetainedPremium = 0
    iCodeTotalLiability = 0
    .

  for each x-data
    where x-data.type = pType
      and x-data.ID = pID
      and x-data.codeType = pCodeType
      and x-data.code = pCode:
   assign
     iCodePolicies = iCodePolicies + x-data.numPolicies
     iCodeForms = iCodeForms + x-data.numForms
     iCodeGrossPremium = iCodeGrossPremium + x-data.totalPremium
     iCodeNetPremium = iCodeNetPremium + x-data.netPremium
     iCodeRetainedPremium = iCodeRetainedPremium + x-data.retainedPremium
     iCodeTotalLiability = iCodeTotalLiability + x-data.amount
     iStatCodeDesc = x-data.codeDesc
     .
  end.

  
  find first agent where agent.agentID = pID no-lock no-error.
  if avail agent 
  then pID = agent.stateID.

  if pCodeType = "" 
   then
    do: clear frame fStatCode.
        pause 0.
    end.
   else display
        pCode @ iStatCode
        iStatCodeDesc
        iCodeForms
        iCodePolicies
        iCodeGrossPremium
        iCodeNetPremium
        iCodeRetainedPremium
        iCodeTotalLiability
       with frame fStatCode.

  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayDataTotals C-Win 
FUNCTION displayDataTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer data for data.

 assign
   tForms = 0
   tPolicies = 0
   tGrossPremium = 0
   tNetPremium = 0
   tRetainedPremium = 0
   tTotalLiability = 0
   .

 for each data where data.code = "TOTAL":
  assign
     tForms = tForms + data.numForms
     tPolicies = tPolicies + data.numPolicies
     tGrossPremium = tGrossPremium + data.totalPremium
     tNetPremium = tNetPremium + data.netPremium
     tRetainedPremium = tRetainedPremium + data.retainedPremium
     tTotalLiability = tTotalLiability + data.amount
     .
 end.

 display
   tForms
   tPolicies
   tGrossPremium
   tNetPremium
   tRetainedPremium
   tTotalLiability
  with frame fTotals.
 pause 0.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL PRIVATE
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

  {&WINDOW-NAME}:move-to-top().
  
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

