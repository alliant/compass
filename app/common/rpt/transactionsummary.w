&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* transactionsummary.w
   Window of Transaction Summary report.
Modification:
 Date        Name             Description
 04-25-2022   VR			  Task 93486 - Modified to add 'County'
							  field in Transaction Summary Report.
*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/state.i}
{tt/period.i}
{tt/trxsummary.i}
{tt/trxsummary.i &tableAlias=ttData}

{lib/std-def.i}

def var hData as handle no-undo.

def var tStatus as char no-undo.
def var tNumTasks as int no-undo.
def var tCurTask as int no-undo.

def var tGettingData as logical.
def var tKeepGettingData as logical.

def var sPeriodID as int no-undo.
def var ePeriodID as int no-undo.

/* to get the column to resize */
{lib/get-column.i}
define variable dColumnWidth as decimal no-undo.

{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttData

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttData.formType ttData.stateID ttData.formID ttData.formCode ttData.statCode ttData.formCount ttData.liabilityDelta ttData.grossDelta ttData.netDelta ttData.retentionDelta   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH ttData
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH ttData.
&Scoped-define TABLES-IN-QUERY-brwData ttData
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttData


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bPrint bRefresh sMonth tPolCount ~
sYear eMonth eYear tStateID tAgentID brwData tPolLiability tPolGross ~
tPolNet tPolRetention tEndCount tEndGross tEndNet tEndRetention ~
tEndLiability tTotalCount tTotalGross tTotalNet tTotalRetention ~
tTotalLiability 
&Scoped-Define DISPLAYED-OBJECTS sMonth tPolCount sYear eMonth eYear ~
tStateID tAgentID tEndCount tTotalCount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setReportButtons C-Win 
FUNCTION setReportButtons RETURNS LOGICAL
  ( input pCmd as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE eMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Ending Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE eYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE sMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Starting Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 80 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tEndCount AS INTEGER FORMAT "-zzz,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndGross AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndLiability AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndNet AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndRetention AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolCount AS INTEGER FORMAT "-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Form Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolGross AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolLiability AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolNet AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolRetention AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Retention Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalCount AS INTEGER FORMAT "-zzz,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalGross AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalLiability AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalNet AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalRetention AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 99 BY 5.71.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttData SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttData.formType column-label "Form!Type" format "x(8)" 
ttData.stateID column-label "State" format "x(8)" 
ttData.countyID column-label "County" format "x(40)" width 15
ttData.formID column-label "Form ID" format "x(25)"
ttData.formCode column-label "Form Code" format "x(40)"
ttData.statCode column-label "STAT Code" format "x(25)" width 13
ttData.formCount column-label "Form!Count" format ">,>>>,>>9"
ttData.liabilityDelta column-label "Liability" format "->>>,>>>,>>>,>>9.99"
ttData.grossDelta column-label "Gross!Premium" format "->>>,>>>,>>>,>>9.99"
ttData.netDelta column-label "Net!Premium" format "->>>,>>>,>>>,>>9.99"
ttData.retentionDelta column-label "Retention!Premium" format "->>>,>>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 207 BY 20.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.14 COL 85.2 WIDGET-ID 2 NO-TAB-STOP 
     bPrint AT ROW 2.14 COL 92.6 WIDGET-ID 14 NO-TAB-STOP 
     bRefresh AT ROW 2.14 COL 77.8 WIDGET-ID 4 NO-TAB-STOP 
     sMonth AT ROW 2.19 COL 17 COLON-ALIGNED WIDGET-ID 64
     tPolCount AT ROW 2.19 COL 120.8 COLON-ALIGNED WIDGET-ID 86 NO-TAB-STOP 
     sYear AT ROW 2.19 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     eMonth AT ROW 3.38 COL 17 COLON-ALIGNED WIDGET-ID 70
     eYear AT ROW 3.38 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 72
     tStateID AT ROW 4.57 COL 17 COLON-ALIGNED WIDGET-ID 82
     tAgentID AT ROW 5.76 COL 17 COLON-ALIGNED WIDGET-ID 84
     brwData AT ROW 7.43 COL 2 WIDGET-ID 200
     tPolLiability AT ROW 3.14 COL 120.8 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tPolGross AT ROW 4.1 COL 120.8 COLON-ALIGNED WIDGET-ID 88 NO-TAB-STOP 
     tPolNet AT ROW 5.05 COL 120.8 COLON-ALIGNED WIDGET-ID 90 NO-TAB-STOP 
     tPolRetention AT ROW 6 COL 120.8 COLON-ALIGNED WIDGET-ID 92 NO-TAB-STOP 
     tEndCount AT ROW 2.19 COL 149.4 COLON-ALIGNED NO-LABEL WIDGET-ID 94 NO-TAB-STOP 
     tEndGross AT ROW 4.1 COL 149.4 COLON-ALIGNED NO-LABEL WIDGET-ID 96 NO-TAB-STOP 
     tEndNet AT ROW 5.05 COL 149.4 COLON-ALIGNED NO-LABEL WIDGET-ID 98 NO-TAB-STOP 
     tEndRetention AT ROW 6 COL 149.4 COLON-ALIGNED NO-LABEL WIDGET-ID 100 NO-TAB-STOP 
     tEndLiability AT ROW 3.14 COL 149.4 COLON-ALIGNED NO-LABEL WIDGET-ID 108 NO-TAB-STOP 
     tTotalCount AT ROW 2.19 COL 178 COLON-ALIGNED NO-LABEL WIDGET-ID 114 NO-TAB-STOP 
     tTotalGross AT ROW 4.1 COL 178 COLON-ALIGNED NO-LABEL WIDGET-ID 116 NO-TAB-STOP 
     tTotalNet AT ROW 5.05 COL 178 COLON-ALIGNED NO-LABEL WIDGET-ID 120 NO-TAB-STOP 
     tTotalRetention AT ROW 6 COL 178 COLON-ALIGNED NO-LABEL WIDGET-ID 122 NO-TAB-STOP 
     tTotalLiability AT ROW 3.14 COL 178 COLON-ALIGNED NO-LABEL WIDGET-ID 118 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.19 COL 3 WIDGET-ID 56
     "Policies" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.57 COL 132.8 WIDGET-ID 104
     "Endorsements/Other" VIEW-AS TEXT
          SIZE 21 BY .62 AT ROW 1.57 COL 155 WIDGET-ID 106
     "Total" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.57 COL 190.2 WIDGET-ID 124
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 209 BY 27.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Transaction Summary"
         HEIGHT             = 27.24
         WIDTH              = 209
         MAX-HEIGHT         = 47.48
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.48
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tAgentID fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tEndCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndNet:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndRetention IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndRetention:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPolCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolNet:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolRetention IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolRetention:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tTotalCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalNet:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalRetention IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalRetention:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttData.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4.05
       COLUMN          = 78
       HEIGHT          = .48
       WIDTH           = 21.6
       TAB-STOP        = no
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Transaction Summary */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Transaction Summary */
DO:
  if tGettingData 
   then return no-apply.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Transaction Summary */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  assign
    sMonth 
    sYear
    eMonth 
    eYear
    tStateID
    tAgentID.
  
  assign
    sPeriodID = 0
    ePeriodID = 0.
  
  find first period where period.periodMonth = sMonth:input-value
                    and period.periodYear = sYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Starting period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  sPeriodID = period.periodID.
  
  find first period where period.periodMonth = eMonth:input-value
                    and period.periodYear = eYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Ending period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  ePeriodID = period.periodID.

  if sPeriodID = 0 or ePeriodID = 0 or ePeriodID < sPeriodID then
  do:
    message "Ending period must be equal or later than starting period."
      view-as alert-box error.
    return no-apply.
  end.
  
  setReportButtons("Disable").
  run getData in this-procedure.
  setReportButtons("Enable").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eMonth C-Win
ON VALUE-CHANGED OF eMonth IN FRAME fMain /* Ending Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eYear C-Win
ON VALUE-CHANGED OF eYear IN FRAME fMain
DO:
 clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sMonth C-Win
ON VALUE-CHANGED OF sMonth IN FRAME fMain /* Starting Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain
DO:
 clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bPrint:load-image("images/pdf.bmp").
bPrint:load-image-insensitive("images/pdf-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

subscribe to "DataError" anywhere.

/* run getData in this-procedure. */

session:immediate-display = yes.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=sMonth &yr=sYear}
    {lib/get-period-list.i &mth=eMonth &yr=eYear}
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}
    /* set the values */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
    {lib/set-current-value.i &mth=sMonth &yr=sYear}
    {lib/set-current-value.i &mth=eMonth &yr=eYear}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'formcode'" &var=dColumnWidth}
  run windowResized.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "transactionsummary.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "transactionsummary.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input param pMsg as char.

  /* Since Progress is single-threaded, this works */
  if not tGettingData 
   then return.

  tKeepGettingData = false.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the systems administrator if the problem persists."
    view-as alert-box error.

  clearData().
  
  setReportButtons("Enable").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY sMonth tPolCount sYear eMonth eYear tStateID tAgentID tEndCount 
          tTotalCount 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport bPrint bRefresh sMonth tPolCount sYear eMonth eYear tStateID 
         tAgentID brwData tPolLiability tPolGross tPolNet tPolRetention 
         tEndCount tEndGross tEndNet tEndRetention tEndLiability tTotalCount 
         tTotalGross tTotalNet tTotalRetention tTotalLiability 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tFile as character no-undo initial "transaction_summary_header".

  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "transaction_summary"
  
  do with frame {&frame-name}:
    output to value(tFile).
    put unformatted "Starting Period: " + GetMonthName(sMonth:input-value) + " " + sYear:screen-value skip.
    put unformatted "Ending Period: " + GetMonthName(eMonth:input-value) + " " + eYear:screen-value skip.
    output close.
  end.

  run rpt/transactionsummary-export.p (string(browse {&browse-name}:handle), {&ReportName}, search(tFile)).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def buffer trxsummary for trxsummary.
def buffer ttData for ttData.
  
empty temp-table ttData no-error.

hide message no-pause.
message "Filtering data...".

do with frame {&frame-name}:
  
  assign
    tPolCount = 0
    tPolLiability = 0
    tPolGross = 0
    tPolNet = 0
    tPolRetention = 0
    tEndCount = 0
    tEndLiability = 0
    tEndGross = 0
    tEndNet = 0
    tEndRetention = 0
    tTotalCount = 0
    tTotalLiability = 0
    tTotalGross = 0
    tTotalNet = 0
    tTotalRetention = 0.
  
  for each trxsummary no-lock:
   
    process events.
    
    create ttData.
    buffer-copy trxsummary to ttData.

    if ttData.formType = "P" then
    assign
      tPolCount = tPolCount + ttData.formCount
      tPolLiability = tPolLiability + ttData.liabilityDelta
      tPolGross = tPolGross + ttData.grossDelta
      tPolNet = tPolNet + ttData.netDelta
      tPolRetention = tPolRetention + ttData.retentionDelta.
    else
    assign
      tEndCount = tEndCount + ttData.formCount
      tEndLiability = tEndLiability + ttData.liabilityDelta
      tEndGross = tEndGross + ttData.grossDelta
      tEndNet = tEndNet + ttData.netDelta
      tEndRetention = tEndRetention + ttData.retentionDelta.

    assign
      tTotalCount = tTotalCount + ttData.formCount
      tTotalLiability = tTotalLiability + ttData.liabilityDelta
      tTotalGross = tTotalGross + ttData.grossDelta
      tTotalNet = tTotalNet + ttData.netDelta
      tTotalRetention = tTotalRetention + ttData.retentionDelta.
  end.
end.
 
displayTotals().
 
dataSortBy = "".
dataSortDesc = yes.
run sortData ("formType").

hide message no-pause.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer trxsummary for trxsummary.
 def buffer ttData for ttData.

 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
 
 close query brwData.
 empty temp-table trxsummary.
 empty temp-table ttData.
 clearData().

 tNumTasks = 2.
 tCurTask = 1.
 pbMinStatus().

 tGettingData = true.
 tkeepGettingData = true.

 hide message no-pause.
 message "Getting data...".

 GETTING-DATA:
 do with frame {&frame-name}:
   pbUpdateStatus(tCurTask, 0).
   
   run server/gettrxsummary.p (sPeriodID,
                               ePeriodID,
                               tStateID:screen-value,
                               tAgentID:screen-value,
                               output table trxsummary,
                               output tSuccess,
                               output tMsg).
    if not tSuccess
     then 
      do: std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "GetTrxSummary failed: " tMsg view-as alert-box warning.
      end.
  
   process events.
      
   if not tKeepGettingData 
    then leave GETTING-DATA.
   
   pbUpdateStatus(int((tCurTask / tNumTasks) * 100), 0).
 end. /* GETTING-DATA */
 
 if not tKeepGettingData 
  then
   do:
       close query brwData.
       empty temp-table trxsummary.
       empty temp-table ttData.
       clearData().

       hide message no-pause.
       message "Cancelled (" + string(now,"99/99/99 HH:MM:SS") + ")".
       pbUpdateStatus(100, 1).
       pbMinStatus().
       tGettingData = false.
       return.
   end.
 
 run filterData.

 pbUpdateStatus(100, 1).
 pbMinStatus().
 tGettingData = false. 
 hide message no-pause.
 message "Report Complete for Period " + 
         sMonth:screen-value in frame fMain + "/" +
         sYear:screen-value in frame fMain + "  " +
         "(" + string(now,"99/99/99 HH:MM:SS") + ")".
 message.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwData:num-results = 0 /* or not available batch */
  then
   do: 
    MESSAGE "There is nothing to print"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 do with frame {&frame-name}:
   run rpt/transactionsummary-pdf.p (input sMonth:input-value,
                                     input sYear:input-value,
                                     input eMonth:input-value,
                                     input eYear:input-value,
                                     input tStateID:screen-value,
                                     input tAgentID:screen-value,
                                     input tPolCount,
                                     input tPolLiability,
                                     input tPolGross,
                                     input tPolNet,
                                     input tPolRetention,
                                     input tEndCount,
                                     input tEndLiability,
                                     input tEndGross,
                                     input tEndNet,
                                     input tEndRetention,
                                     input table ttData).
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i &post-by-clause="+ ' by ttData.seq '" }

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
 
  /* resize the column */
  {lib/resize-column.i &col="'formcode'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    
    close query {&browse-name}.
    hide message no-pause.
    
    assign
      tPolCount = 0
      tPolLiability = 0
      tPolGross = 0
      tPolNet = 0
      tPolRetention = 0
      tEndCount = 0
      tEndLiability = 0
      tEndGross = 0
      tEndNet = 0
      tEndRetention = 0.
      
    display
      tPolCount
      tPolLiability
      tPolGross
      tPolNet
      tPolRetention
      tEndCount
      tEndLiability
      tEndGross
      tEndNet
      tEndRetention
      .
  end.
                  
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
do with frame {&frame-name}:
  
  display
    tPolCount
    tPolLiability
    tPolGross
    tPolNet
    tPolRetention
    tEndCount
    tEndLiability
    tEndGross
    tEndNet
    tEndRetention
    tTotalCount
    tTotalLiability
    tTotalGross
    tTotalNet
    tTotalRetention
    .
  
  pause 0.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

  {&WINDOW-NAME}:move-to-top().
  
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setReportButtons C-Win 
FUNCTION setReportButtons RETURNS LOGICAL
  ( input pCmd as char ) :

  do with frame {&frame-name}:
    case pCmd:
      when "Enable" then
      assign
        bRefresh:sensitive = yes
        bPrint:sensitive   = yes
        bExport:sensitive  = yes.
      when "Disable" then
      assign
        bRefresh:sensitive = no
        bPrint:sensitive   = no
        bExport:sensitive  = no.
      otherwise
        .
    end case.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

