&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Claims Ratio (wcrm05-r.w)
@description The browse shows the claims ratio for the agent. The claims
             ratio is the costs incurred for all claims for the agent
             divided by the net premiums for all completed batches for
             the agent.

@author John Oliver
@version 1.0
@created 2017/03/24
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/reportclaimsratio.i}
{tt/state.i}
{tt/period.i}

define temp-table agentratio
  field agentID as character
  field stateID as character
  field stat as character
  field name as character
  field manager as character
  field netPremium as decimal
  field laePaidDelta as decimal
  field laeReserveDelta as decimal
  field lossPaidDelta as decimal
  field lossReserveDelta as decimal
  field recoveries as decimal
  field pendingRecoveries as decimal
  field costsIncurred as decimal
  field costsPaid as decimal
  field incurredClaimsRatio as decimal
  field paidClaimsRatio as decimal
  .

{lib/getstatename.i}
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/std-def.i}

define variable iCounterMax as integer no-undo.
define variable iCounter as integer no-undo.

define variable dColumnWidth as decimal no-undo.

define variable currState as character no-undo.
define variable currStatus as character no-undo.
define variable currManager as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentratio

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData agentratio.agentID agentratio.name agentratio.stateID agentratio.netPremium agentratio.laePaidDelta agentratio.laeReserveDelta agentratio.lossPaidDelta agentratio.lossReserveDelta agentratio.recoveries agentratio.pendingRecoveries agentratio.costsIncurred agentratio.costsPaid agentratio.incurredClaimsRatio agentratio.paidClaimsRatio   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH agentratio by agentratio.agentID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH agentratio by agentratio.agentID.
&Scoped-define TABLES-IN-QUERY-brwData agentratio
&Scoped-define FIRST-TABLE-IN-QUERY-brwData agentratio


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh brwData sMonth sYear eMonth eYear ~
radAgentError fState fManager tAgent fStat fNoData fNegative 
&Scoped-Define DISPLAYED-OBJECTS sMonth sYear eMonth eYear radAgentError ~
fState fManager tAgent fStat fNoData fNegative 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterManagerCombo C-Win 
FUNCTION getFilterManagerCombo RETURNS CHARACTER
  ( input pState as character,
    input pStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pManager as character,
    input pStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character,
    input pManager as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getLastDay C-Win 
FUNCTION getLastDay RETURNS DATE
  ( input pMonth as integer,
    input pYear as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE eMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Ending" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE eYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)" INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE fStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE sMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Starting" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 83 BY 1 NO-UNDO.

DEFINE VARIABLE radAgentError AS LOGICAL 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Agent Error Only", yes,
"ALL", no
     SIZE 20 BY 1.91 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 81 BY 4.76.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 99 BY 4.76.

DEFINE VARIABLE fNegative AS LOGICAL INITIAL yes 
     LABEL "Agents With Negative Ratio" 
     VIEW-AS TOGGLE-BOX
     SIZE 30 BY .81 NO-UNDO.

DEFINE VARIABLE fNoData AS LOGICAL INITIAL yes 
     LABEL "Agents With No Data" 
     VIEW-AS TOGGLE-BOX
     SIZE 25 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      agentratio SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      agentratio.agentID column-label "Agent ID" format "x(12)" width 10
agentratio.name column-label "Agent" format "x(250)" width 25
agentratio.stateID column-label "State" width 7
agentratio.netPremium column-label "Net Premium" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.laePaidDelta column-label "LAE Paid" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.laeReserveDelta column-label "LAE Reserve!Change" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.lossPaidDelta column-label "Loss Paid" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.lossReserveDelta column-label "Loss Reserve!Change" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.recoveries column-label "Recoveries!Received" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.pendingRecoveries column-label "Pending!Recoveries" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.costsIncurred column-label "Total Costs!Incurred" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.costsPaid column-label "Total Costs!Paid" format "(>>,>>>,>>>,>>Z)" width 14
agentratio.incurredClaimsRatio column-label "Incurred!Claims Ratio" format "(>>>,>>9.99) %" width 14
agentratio.paidClaimsRatio column-label "Paid!Claims Ratio" format "(>>>,>>9.99) %" width 14
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 212 BY 21.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.91 COL 71.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 2.91 COL 64 WIDGET-ID 4 NO-TAB-STOP 
     brwData AT ROW 6.48 COL 2 WIDGET-ID 200
     sMonth AT ROW 2.91 COL 11 COLON-ALIGNED WIDGET-ID 16
     sYear AT ROW 2.91 COL 28 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     eMonth AT ROW 4.1 COL 11 COLON-ALIGNED WIDGET-ID 162
     eYear AT ROW 4.1 COL 28 COLON-ALIGNED NO-LABEL WIDGET-ID 164
     radAgentError AT ROW 3.05 COL 42 NO-LABEL WIDGET-ID 166
     fState AT ROW 2.19 COL 94 COLON-ALIGNED WIDGET-ID 34
     fManager AT ROW 3.38 COL 94 COLON-ALIGNED WIDGET-ID 160
     tAgent AT ROW 4.57 COL 94 COLON-ALIGNED WIDGET-ID 176
     fStat AT ROW 2.19 COL 126 COLON-ALIGNED WIDGET-ID 170
     fNoData AT ROW 2.33 COL 148 WIDGET-ID 172
     fNegative AT ROW 3.48 COL 148 WIDGET-ID 174
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 83.8 WIDGET-ID 60
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 82.6 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 214 BY 27.29 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Claims Ratio"
         HEIGHT             = 27.29
         WIDTH              = 214
         MAX-HEIGHT         = 28.91
         MAX-WIDTH          = 221.2
         VIRTUAL-HEIGHT     = 28.91
         VIRTUAL-WIDTH      = 221.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentratio by agentratio.agentID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4.71
       COLUMN          = 64
       HEIGHT          = .48
       WIDTH           = 15
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Claims Ratio */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Claims Ratio */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Claims Ratio */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available agentratio
   then return.
   
  std-ch = "".
  for first agent no-lock
      where agent.agentID = agentratio.agentID:
    
    std-ch = agent.name.
  end.
   
  do with frame {&frame-name}:
    run rpt/claimsratio-details.w persistent set std-ha (input agentratio.agentID,
                                                         input std-ch,
                                                         input date(sMonth:input-value,1,sYear:input-value),
                                                         input getLastDay(eMonth:input-value,eYear:input-value),
                                                         input radAgentError:input-value
                                                         ).
  end.
  run GetData in std-ha.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
  /* change the ratio format if the ratio is 0 */
  if agentratio.incurredClaimsRatio = 0
   then agentratio.incurredClaimsRatio:format in browse {&browse-name} = "Z".
  /* change the ratio format if the ratio is 0 */
  if agentratio.paidClaimsRatio = 0
   then agentratio.paidClaimsRatio:format in browse {&browse-name} = "Z".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eMonth C-Win
ON VALUE-CHANGED OF eMonth IN FRAME fMain /* Ending */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eYear C-Win
ON VALUE-CHANGED OF eYear IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNegative
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNegative C-Win
ON VALUE-CHANGED OF fNegative IN FRAME fMain /* Agents With Negative Ratio */
DO:
  run sortData in this-procedure ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNoData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNoData C-Win
ON VALUE-CHANGED OF fNoData IN FRAME fMain /* Agents With No Data */
DO:
  run sortData in this-procedure ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStat C-Win
ON VALUE-CHANGED OF fStat IN FRAME fMain /* Status */
DO:
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME radAgentError
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL radAgentError C-Win
ON VALUE-CHANGED OF radAgentError IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sMonth C-Win
ON VALUE-CHANGED OF sMonth IN FRAME fMain /* Starting */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgent C-Win
ON LEAVE OF tAgent IN FRAME fMain /* Agent */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgent C-Win
ON RETURN OF tAgent IN FRAME fMain /* Agent */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

publish "GetAgents" (output table agent).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=sMonth &yr=sYear}
    {lib/get-period-list.i &mth=eMonth &yr=eYear}
    /* set the values */
    {lib/set-current-value.i &state=fState}
    {lib/set-current-value.i &mth=sMonth &yr=sYear}
    {lib/set-current-value.i &mth=eMonth &yr=eYear}
    
    /* get the last closed period */
    for last period no-lock
       where period.active = false
         and period.activeAccounting = false:
        
      assign
        sMonth:screen-value = string(month(add-interval(period.startDate,-3,"year")))
        eMonth:screen-value = string(month(period.endDate))
        sYear:screen-value = string(year(add-interval(period.startDate,-3,"year")))
        eYear:screen-value = string(year(period.endDate))
        .
    end.
    
    assign
      fStat:screen-value = "ALL"
      fState:screen-value = "ALL"
      fManager:screen-value = "ALL"
      .

    {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
  end.
   
  run windowResized.
  clearData().
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "claimsratio.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "claimsratio.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY sMonth sYear eMonth eYear radAgentError fState fManager tAgent fStat 
          fNoData fNegative 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh brwData sMonth sYear eMonth eYear radAgentError fState 
         fManager tAgent fStat fNoData fNegative 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Claims_Ratio"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable startDate as datetime no-undo.
  define variable endDate as datetime no-undo.
  define variable agentError as logical no-undo.
  
  define buffer period for period.
  define buffer agent for agent.
  define buffer reportclaimsratio for reportclaimsratio.

  /* cleanup before getting data */
  run SetProgressCounter (3).
  pbMinStatus().
  clearData().
  do with frame {&frame-name}:
    std-ch = "".
    for each period no-lock
       where period.periodMonth = sMonth:input-value
         and period.periodYear = sYear:input-value:

      startDate = period.startDate.
    end.
    for each period no-lock
       where period.periodMonth = eMonth:input-value
         and period.periodYear = eYear:input-value:

      endDate = period.endDate.
    end.
    agentError = radAgentError:input-value.
  end. 
  if startDate = ?
   then std-ch = addDelimiter(std-ch,"~n") + "The starting period is invalid.".
   
  if endDate = ?
   then std-ch = addDelimiter(std-ch,"~n") + "The ending period is invalid.".
  
  if startDate <> ? and endDate <> ? and endDate < startDate 
   then std-ch = addDelimiter(std-ch,"~n") + "The ending period must be equal or later than starting period.". 
   
  /* show the error if any */
  if std-ch > ""
   then 
    do:
      message std-ch view-as alert-box error buttons ok.
      return.
    end.
     
  run SetProgressStatus.
  empty temp-table reportclaimsratio.
  run server/getclaimsratio.p (input startDate,
                               input endDate,
                               input agentError,
                               output table reportclaimsratio,
                               output std-lo,
                               output std-ch).
                         
  if not std-lo
   then 
    do: 
      message std-ch view-as alert-box warning.
      run SetProgressEnd.
      return.
    end.
  
  std-in = 0.
  run SetProgressStatus.
  empty temp-table agentratio.
  for each agent no-lock:
    if can-find(first reportclaimsratio where agentID = agent.agentID)
     then
      for first reportclaimsratio no-lock
          where reportclaimsratio.agentID = agent.agentID:
          
        create agentratio.
        assign
          agentratio.agentID = agent.agentID
          agentratio.name = agent.name
          agentratio.stat = agent.stat
          agentratio.stateID = agent.stateID
          agentratio.manager = agent.manager
          agentratio.netPremium = reportclaimsratio.netPremium
          agentratio.laePaidDelta = reportclaimsratio.laePaidDelta
          agentratio.laeReserveDelta = reportclaimsratio.laeReserveDelta
          agentratio.lossPaidDelta = reportclaimsratio.lossPaidDelta
          agentratio.lossReserveDelta = reportclaimsratio.lossReserveDelta
          agentratio.recoveries = reportclaimsratio.recoveries
          agentratio.pendingRecoveries = reportclaimsratio.pendingRecoveries
          agentratio.costsIncurred = agentratio.laePaidDelta + 
                                     agentratio.laeReserveDelta + 
                                     agentratio.lossPaidDelta + 
                                     agentratio.lossReserveDelta - 
                                     agentratio.recoveries
          agentratio.costsPaid = agentratio.laePaidDelta + 
                                 agentratio.lossPaidDelta -
                                 agentratio.recoveries
          .
        if agentratio.netPremium = 0
         then agentratio.incurredClaimsRatio = 0.
         else agentratio.incurredClaimsRatio = agentratio.costsIncurred / agentratio.netPremium * 100.
        if agentratio.netPremium = 0
         then agentratio.paidClaimsRatio = 0.
         else agentratio.paidClaimsRatio = agentratio.costsPaid / agentratio.netPremium * 100.
      end.
     else
      do:
        create agentratio.
        assign
          agentratio.agentID = agent.agentID
          agentratio.name = agent.name
          agentratio.stat = agent.stat
          agentratio.stateID = agent.stateID
          agentratio.manager = agent.manager
          agentratio.netPremium = 0
          agentratio.laePaidDelta = 0
          agentratio.laeReserveDelta = 0
          agentratio.lossPaidDelta = 0
          agentratio.lossReserveDelta = 0
          agentratio.recoveries = 0
          agentratio.pendingRecoveries = 0
          agentratio.costsIncurred = 0
          agentratio.costsPaid = 0
          agentratio.incurredClaimsRatio = 0
          agentratio.paidClaimsRatio = 0
          .
      end.
    std-in = std-in + 1.
  end.
  {&OPEN-QUERY-brwData}
  setFilterCombos("ALL").
  run SetProgressEnd.
  do with FRAME {&frame-name}:
    assign
      std-lo = can-find(first agentratio)
      /* disable the buttons and filters */
      bExport:sensitive = std-lo
      fState:sensitive = std-lo
      fManager:sensitive = std-lo
      fStat:sensitive = std-lo
      fNegative:sensitive = std-lo
      fNoData:sensitive = std-lo
      tAgent:read-only = not std-lo
      .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressCounter C-Win 
PROCEDURE SetProgressCounter :
/*------------------------------------------------------------------------------
@description Sets the progress bar count
------------------------------------------------------------------------------*/
  def input parameter pCounter as int no-undo.
  assign
    iCounterMax = pCounter
    iCounter = 0
    .
  pbMinStatus().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressEnd C-Win 
PROCEDURE SetProgressEnd :
/*------------------------------------------------------------------------------
@description Makes the progress bar minimum after setting to 100
------------------------------------------------------------------------------*/
  pbUpdateStatus(100, 1).
  pbMinStatus().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressStatus C-Win 
PROCEDURE SetProgressStatus :
/*------------------------------------------------------------------------------
@description Updates the progress bar
------------------------------------------------------------------------------*/
  iCounter = iCounter + 1.
  pbUpdateStatus(int(iCounter / iCounterMax * 100), 0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStatus as character no-undo.
  define variable cState as character no-undo.
  define variable cManager as character no-undo.
  define variable cAgent as character no-undo.
  define variable lNoData as logical no-undo.
  define variable lNegative as logical no-undo.
  
  define variable tWhereClause as character no-undo.
  
  define variable hQuery as handle no-undo.
  
  do with frame {&frame-name}:
    assign
      cStatus = fStat:screen-value
      cState = fState:screen-value
      cManager = fManager:screen-value
      cAgent = tAgent:screen-value
      lNoData = fNoData:checked
      lNegative = fNegative:checked
      .
  end.
  
  /* build the query */
  if cStatus <> "ALL" and cStatus <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stat='" + cStatus + "'".
  if cState <> "ALL" and cState <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stateID='" + cState + "'".
  if cManager <> "ALL" and cManager <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "manager='" + cManager + "'".
  if cAgent > ""
   then tWhereClause = addDelimiter(tWhereClause," and ") + "(agentID matches '*" + cAgent + "*' or name matches '*" + cAgent + "*')".
  if not lNoData
   then tWhereClause = addDelimiter(tWhereClause," and ") + "(costsIncurred <> 0 or costsPaid <> 0)".
  if not lNegative
   then tWhereClause = addDelimiter(tWhereClause," and ") + "incurredClaimsRatio >= 0 and paidClaimsRatio >= 0".
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  apply "VALUE-CHANGED" to brwData in frame {&frame-name}.
  std-in = 0.
  hide message no-pause.
  std-ch = string(num-results("{&browse-name}")) + " agent(s) shown".
  message std-ch.
  message.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  
  {lib/resize-column.i &col="'name'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with FRAME {&frame-name}:
    assign
      /* default the filter */
      fState:screen-value = "ALL"
      fManager:screen-value = "ALL"
      fStat:screen-value = "ALL"
      tAgent:screen-value = ""
      fNegative:checked = true
      fNoData:checked = true
      /* disable the buttons and filters */
      bExport:sensitive = false
      fState:sensitive = false
      fManager:sensitive = false
      fStat:sensitive = false
      fNegative:sensitive = false
      fNoData:sensitive = false
      tAgent:read-only = true
      .
  end.
  close query brwData.
  empty temp-table agentratio.
  hide message no-pause.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterManagerCombo C-Win 
FUNCTION getFilterManagerCombo RETURNS CHARACTER
  ( input pState as character,
    input pStatus as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cManagerList as character no-undo.
  define variable cName as character no-undo.
  define buffer agent for agent.
 
  cManagerList = "ALL,ALL".
  for each agent no-lock:
  
    if pState <> "ALL" and pState <> agent.stateID
     then next.
     
    if pStatus <> "ALL" and pStatus <> agent.stat
     then next.
   
    if agent.manager <> "" and lookup(agent.manager,cManagerList) = 0
     then 
      do:
        publish "GetSysUserName" (agent.manager, output cName).
        cManagerList = addDelimiter(cManagerList,",") + cName + "," + agent.manager.
      end.
  end.
  RETURN cManagerList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pManager as character,
    input pStatus as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cStateList as character no-undo.
  define variable cName as character no-undo.
  define buffer agent for agent.
 
  cStateList = "ALL,ALL".
  for each agent no-lock:
  
    if pManager <> "ALL" and pManager <> agent.manager
     then next.
     
    if pStatus <> "ALL" and pStatus <> agent.stat
     then next.
   
    if lookup(agent.stateID,cStateList) = 0
     then 
      assign
        cStateList = addDelimiter(cStateList,",") + getStateName(agent.stateID) + "," + agent.stateID
        .
  end.
  RETURN cStateList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character,
    input pManager as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent statuses for the state
------------------------------------------------------------------------------*/
  define variable cStatusList as character no-undo.
  define variable cStatus as character no-undo.
  define buffer agent for agent.
 
  cStatusList = "ALL,ALL".
  for each agent no-lock:
    
    if pState <> "ALL" and pState <> agent.stateID
     then next.
  
    if pManager <> "ALL" and pManager <> agent.manager
     then next.
     
    if lookup(agent.stat,cStatusList) = 0
     then 
      do:
        cStatus = "".
        publish "GetSysPropDesc" ("AMD", "Agent", "Status", agent.stat, output cStatus).
        if cStatus = ""
         then cStatus = agent.stat.
        cStatusList = addDelimiter(cStatusList,",") + cStatus + "," + agent.stat.
      end.
  end.
  RETURN cStatusList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getLastDay C-Win 
FUNCTION getLastDay RETURNS DATE
  ( input pMonth as integer,
    input pYear as integer ) :
/*------------------------------------------------------------------------------
@description Get the last day of the month
------------------------------------------------------------------------------*/
  define variable pLastDate as date no-undo.
  if pMonth = 12
   then pLastDate = date(pMonth, 31, pYear).
   else pLastDate = date(pMonth + 1, 1, pYear) - 1.

  return pLastDate.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Sets the progress bar back to the minimum value
------------------------------------------------------------------------------*/
  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT) :
/*------------------------------------------------------------------------------
@description: Sets the progress bar to pPercentage
------------------------------------------------------------------------------*/
  {&WINDOW-NAME}:move-to-top().
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0
     then pause pPauseSeconds no-message.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character ) :
/*------------------------------------------------------------------------------
@description Sets the filter combo boxes
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      currState = fState:screen-value
      currManager = fManager:screen-value
      currStatus = fStat:screen-value
      .
    case pName:
     when "ALL" then
      assign
        fState:list-item-pairs = getFilterStateCombo(currManager, currStatus)
        fManager:list-item-pairs = getFilterManagerCombo(currState, currStatus)
        fStat:list-item-pairs = getFilterStatusCombo(currState, currManager)
        .
     when "State" then
      assign
        fManager:list-item-pairs = getFilterManagerCombo(currState, currStatus)
        fStat:list-item-pairs = getFilterStatusCombo(currState, currManager)
        .
     when "Manager" then
      assign
        fState:list-item-pairs = getFilterStateCombo(currManager, currStatus)
        fStat:list-item-pairs = getFilterStatusCombo(currState, currManager)
        .
     when "Status" then
      assign
        fState:list-item-pairs = getFilterStateCombo(currManager, currStatus)
        fManager:list-item-pairs = getFilterManagerCombo(currState, currStatus)
        .
    end case.
    /* set the state */
    if lookup(currState,fState:list-item-pairs) > 0
     then fState:screen-value = currState.
     else fState:screen-value = "ALL".
    /* set the manager */
    if lookup(currManager,fManager:list-item-pairs) > 0
     then fManager:screen-value = currManager.
     else fManager:screen-value = "ALL".
    /* set the status */
    if lookup(currStatus,fStat:list-item-pairs) > 0
     then fStat:screen-value = currStatus.
     else fStat:screen-value = "ALL".
  end.
  run sortData in this-procedure (dataSortBy).
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

