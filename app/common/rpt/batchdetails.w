&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wbatchform.w
   Window of STAT codes
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/batchdetail.i &tableAlias="data"}

{lib/std-def.i}
{lib/winlaunch.i}
{lib/set-button-def.i}
{lib/set-filter-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bConfig bExport bFilter bFilterClear bPrint ~
tBatchID fSearch bGo RECT-39 
&Scoped-Define DISPLAYED-OBJECTS tBatchID fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bConfig  NO-FOCUS
     LABEL "Config" 
     SIZE 7.2 BY 1.71 TOOLTIP "Config Columns".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Apply search filter".

DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear filter".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get batch details".

DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View batch details as PDF".

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 42.4 BY 1 TOOLTIP "Criteria for File or Policy; press <Return> to apply the filter" NO-UNDO.

DEFINE VARIABLE tBatchID AS INTEGER FORMAT ">>>>>>>>":U INITIAL 0 
     LABEL "Batch" 
     VIEW-AS FILL-IN 
     SIZE 20.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 55 BY 2.62.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71.4 BY 2.62.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.4 BY 2.62.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bConfig AT ROW 1.95 COL 130 WIDGET-ID 76 NO-TAB-STOP 
     bExport AT ROW 1.95 COL 47.4 WIDGET-ID 2 NO-TAB-STOP 
     bFilter AT ROW 1.95 COL 111 WIDGET-ID 72 NO-TAB-STOP 
     bFilterClear AT ROW 1.95 COL 118.4 WIDGET-ID 70 NO-TAB-STOP 
     bPrint AT ROW 1.95 COL 40 WIDGET-ID 14 NO-TAB-STOP 
     tBatchID AT ROW 2.33 COL 9 COLON-ALIGNED WIDGET-ID 10
     fSearch AT ROW 2.33 COL 65.6 COLON-ALIGNED WIDGET-ID 66
     bGo AT ROW 1.95 COL 32.6 WIDGET-ID 4 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 57.6 WIDGET-ID 60
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 56.6 WIDGET-ID 58
     RECT-39 AT ROW 1.48 COL 127.6 WIDGET-ID 74
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 140 BY 20.43 WIDGET-ID 100.

DEFINE FRAME fBrowse
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 4.24
         SIZE 138 BY 17 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Batch Details"
         HEIGHT             = 20.43
         WIDTH              = 140
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fBrowse:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fBrowse
                                                                        */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       fSearch:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Batch Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Batch Details */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Batch Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConfig C-Win
ON CHOOSE OF bConfig IN FRAME fMain /* Config */
DO:
  run SetDynamicBrowseColumns.
  run buildFieldList in this-procedure.
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  run BuildDynamicBrowse (createQuery()).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* Clear */
DO:
  fSearch:screen-value = "".
  run BuildDynamicBrowse (createQuery()).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tBatchID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tBatchID C-Win
ON RETURN OF tBatchID IN FRAME fMain /* Batch */
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/win-status.i &entity="'Batch Form'"}
{lib/build-dynamic-browse.i &table=data &entity="'Batch Form'" &keyfield="'policyID'"}
{lib/set-filter.i &label="Search" &id="policyChar:fileNumber:cplID" &populate=false}
{lib/set-button.i &label="Go"     &toggle=false}
{lib/set-button.i &label="Config" &toggle=false}
{lib/set-button.i &label="Print"}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="Filter"}
{lib/set-button.i &label="FilterClear"}
setButtons().

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

std-ch = "".
publish "GetCurrentValue" ("BatchID", output std-ch).
assign tBatchID = int(std-ch) no-error.
if error-status:error or std-ch = "" or std-ch = ?
 then assign tBatchID = 0.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  enableFilters(false).
  enableButtons(false).
  run buildFieldList in this-procedure.
  run windowResized in this-procedure.

  publish "GetAutoView" (output std-lo).
  if std-lo and tBatchID:screen-value > "" 
   then run getData.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE buildFieldList C-Win 
PROCEDURE buildFieldList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run BuildDynamicBrowseColumns.
  for each listfield exclusive-lock
     where listfield.dataType = "character":
    
    case listfield.columnBuffer:
      when "formTypeDesc"     or
      when "formIDDesc"       or
      when "statCode"         then listfield.columnWidth = 12.
      when "countyID"         or
      when "insuredTypeDesc"  or
      when "muniID"           then listfield.columnWidth = 20.
      when "cplID"            then listfield.columnWidth = 15.
      when "validMsg"         then listfield.columnWidth = 30.
      when "validForm"        then listfield.columnWidth = 5.
      when "fileNumber"       or
      when "fileID"           then listfield.columnWidth = 20.
      otherwise listfield.columnWidth = 25.
    end case.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tBatchID fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bConfig bExport bFilter bFilterClear bPrint tBatchID fSearch bGo 
         RECT-39 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME fBrowse IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fBrowse}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.

  hBrowse = getBrowseHandle().
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      if std-ha:num-results = 0
       then
        do:
          message "No results to export" view-as alert-box warning buttons ok.
          return.
        end.
    end.

  &scoped-define ReportName "Agents"

 std-ch = "C".
 publish "GetExportType" (output std-ch).
 if std-ch = "X" 
  then run util/exporttoexcelbrowse.p (string(hBrowse), {&ReportName}).
  else run util/exporttocsvbrowse.p (string(hBrowse), {&ReportName}).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    if tBatchID:screen-value = "" or tBatchID:input-value = 0
     then
      do:
        MESSAGE "Batch cannot be blank or zero" VIEW-AS ALERT-BOX INFO BUTTONS OK.
        return.
      end.
    
    empty temp-table data.
    publish "GetBatchForms" (input tBatchID:screen-value, output table data).
    if not can-find(first data)
     then
      do:
        run server/getbatchforms.p (input  tBatchID:input-value,
                                    input  0,
                                    output table data,
                                    output std-lo,
                                    output std-ch).

        if not std-lo
         then message std-ch view-as alert-box error buttons ok.
      end.
    
    if can-find(first data)
     then
      do:
        for each data exclusive-lock:
          data.policyChar = string(data.policyID).
        end.
        run buildFieldList in this-procedure.
        run BuildDynamicBrowse (createQuery()).
        
        std-lo = can-find(first data).
        enableFilters(std-lo).
        enableButtons(std-lo).
        
        std-ha = getBrowseHandle().
        if valid-handle(std-ha) and std-ha:type = "BROWSE"
         then
          do:
            std-ha = std-ha:query.
            setStatusCount(std-ha:num-results).
          end.
        publish "SetCurrentValue" ("BatchID", tBatchID:screen-value).
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.

  hBrowse = getBrowseHandle().
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      if std-ha:num-results = 0
       then
        do:
          message "No results to print" view-as alert-box warning buttons ok.
          return.
        end.
    end.
 
  run rpt/batchdetails-pdf.p (table data).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* modify the frame for the dynamic browse */
  assign
    frame fBrowse:width-pixels          = {&window-name}:width-pixels - 10
    frame fBrowse:virtual-width-pixels  = frame fBrowse:width-pixels
    frame fBrowse:height-pixels         = {&window-name}:height-pixels - frame fBrowse:y - 5
    frame fBrowse:virtual-height-pixels = frame fBrowse:height-pixels
    .

  run BuildDynamicBrowse (createQuery()).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createQuery C-Win 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE tQuery       as CHARACTER no-undo.

  tQuery = "preselect each data " + getWhereClause() + " by seq".
  RETURN tQuery.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

