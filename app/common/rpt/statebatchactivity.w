&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wcrm07-r.w
   State Batch Activity report
   Created 6.18.2015 D.Sinclair
*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/period.i}
{tt/state.i}

{tt/ops18.i}

define temp-table data
 field agentID as character
 field name as character
 field stat as character
 field statDesc as character
 field manager as character
 field managerDesc as character
 field janValue as decimal format "(zzz,zzz,zzz,zzz)"
 field febValue as decimal format "(zzz,zzz,zzz,zzz)"
 field marValue as decimal format "(zzz,zzz,zzz,zzz)"
 field aprValue as decimal format "(zzz,zzz,zzz,zzz)"
 field mayValue as decimal format "(zzz,zzz,zzz,zzz)"
 field junValue as decimal format "(zzz,zzz,zzz,zzz)"
 field julValue as decimal format "(zzz,zzz,zzz,zzz)"
 field augValue as decimal format "(zzz,zzz,zzz,zzz)"
 field sepValue as decimal format "(zzz,zzz,zzz,zzz)"
 field octValue as decimal format "(zzz,zzz,zzz,zzz)"
 field novValue as decimal format "(zzz,zzz,zzz,zzz)"
 field decValue as decimal format "(zzz,zzz,zzz,zzz)"
 field ytdValue as decimal format "(zzz,zzz,zzz,zzz)"
 field stateID as character
 .

{lib/std-def.i}
{lib/find-widget.i}
{lib/count-rows.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/set-status.i}
{lib/set-button-def.i}
{lib/set-filter-def.i}

/* needed for the filter values */
define variable currAgent as character no-undo.
define variable currStatus as character no-undo.
define variable currManager as character no-undo.

def var hData as handle no-undo.

def var tStatus as char no-undo.

def var tGettingData as logical.
def var tKeepGettingData as logical.

/* variables needed for the browse total */
def var totalCol as decimal extent no-undo.
def var totalAdded as logical no-undo init false.

/* resize column width */
def var dBrwColWidth as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.agentID data.name data.stat data.janValue data.febValue data.marValue data.aprValue data.mayValue data.junValue data.julValue data.augValue data.sepValue data.octValue data.novValue data.decValue data.ytdValue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo fAgent fStatus fManager tPolCount ~
brwData tPolLiability tPolGross tPolNet tFileCount tBatchCount 
&Scoped-Define DISPLAYED-OBJECTS sYear tStateID fView fAgent fStatus ~
fManager tPolCount tBatchCount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitizeReportButtons C-Win 
FUNCTION sensitizeReportButtons RETURNS LOGICAL PRIVATE
  ( input pCmd as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE fAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 84 BY 1 NO-UNDO.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 84 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 84 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS INTEGER FORMAT ">>>9":U INITIAL 2010 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 12 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tBatchCount AS INTEGER FORMAT " -zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Batch Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tFileCount AS INTEGER FORMAT " -zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "File Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolCount AS INTEGER FORMAT " -zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Policy Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolGross AS DECIMAL FORMAT "$-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolLiability AS DECIMAL FORMAT "$-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolNet AS DECIMAL FORMAT "$-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 59 BY 6.43.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25.6 BY 6.43.

DEFINE RECTANGLE RECT-41
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100 BY 6.43.

DEFINE VARIABLE fView AS CHARACTER INITIAL "N" 
     VIEW-AS SELECTION-LIST SINGLE 
     LIST-ITEM-PAIRS "Net Premium","N",
                     "Gross Premium","G",
                     "Retained Premium","R",
                     "Liability Amount","L",
                     "Number of Batches","B",
                     "Number of Files","F",
                     "Number of Policies","P",
                     "Report Gap","T" 
     SIZE 22 BY 5.24 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.agentID label "AgentID":C width 10
      data.name label "Name":C format "x(200)" width 24
      data.stat label "Status":c format "x(20)" width 12
      data.janValue label "Jan":C width 15
      data.febValue label "Feb":C width 15
      data.marValue label "Mar":C width 15
      data.aprValue label "Apr":C width 15
      data.mayValue label "May":C width 15
      data.junValue label "Jun":C width 15
      data.julValue label "Jul":C width 15
      data.augValue label "Aug":C width 15
      data.sepValue label "Sep":C width 15
      data.octValue label "Oct":C width 15
      data.novValue label "Nov":C width 15
      data.decValue label "Dec":C width 15
      data.ytdValue label "YTD":C width 18
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 261 BY 20 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 3.38 COL 51.4 WIDGET-ID 2 NO-TAB-STOP 
     bGo AT ROW 3.38 COL 44 WIDGET-ID 4 NO-TAB-STOP 
     sYear AT ROW 3.62 COL 10 COLON-ALIGNED WIDGET-ID 66
     tStateID AT ROW 4.76 COL 10 COLON-ALIGNED WIDGET-ID 82
     fView AT ROW 2.1 COL 62 NO-LABEL WIDGET-ID 124
     fAgent AT ROW 2.86 COL 96.2 COLON-ALIGNED WIDGET-ID 138
     fStatus AT ROW 4.05 COL 96.2 COLON-ALIGNED WIDGET-ID 140
     fManager AT ROW 5.24 COL 96.2 COLON-ALIGNED WIDGET-ID 142
     tPolCount AT ROW 2.86 COL 202.6 COLON-ALIGNED WIDGET-ID 86 NO-TAB-STOP 
     brwData AT ROW 8.14 COL 2 WIDGET-ID 200
     tPolLiability AT ROW 6.67 COL 202.6 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tPolGross AT ROW 4.76 COL 202.6 COLON-ALIGNED WIDGET-ID 88 NO-TAB-STOP 
     tPolNet AT ROW 5.71 COL 202.6 COLON-ALIGNED WIDGET-ID 90 NO-TAB-STOP 
     tFileCount AT ROW 3.81 COL 202.6 COLON-ALIGNED WIDGET-ID 92 NO-TAB-STOP 
     tBatchCount AT ROW 1.91 COL 202.6 COLON-ALIGNED WIDGET-ID 122 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3.4 WIDGET-ID 56
     "Total" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 216.2 WIDGET-ID 112
     "View" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.14 COL 62.2 WIDGET-ID 130
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 1.19 COL 87.2 WIDGET-ID 136
     RECT-36 AT ROW 1.43 COL 2 WIDGET-ID 54
     RECT-39 AT ROW 1.43 COL 60.6 WIDGET-ID 128
     RECT-41 AT ROW 1.43 COL 85.8 WIDGET-ID 134
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 263.6 BY 27.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Remittances by State"
         HEIGHT             = 27.38
         WIDTH              = 263.6
         MAX-HEIGHT         = 47.48
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.48
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tPolCount fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR SELECTION-LIST fView IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-39 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-41 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX sYear IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tBatchCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFileCount IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFileCount:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPolCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolNet:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX tStateID IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 5.29
       COLUMN          = 44.4
       HEIGHT          = .48
       WIDTH           = 14
       TAB-STOP        = no
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Remittances by State */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Remittances by State */
DO:
  if tGettingData 
   then return no-apply.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Remittances by State */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO: 
  sensitizeReportButtons(false).
  run getData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  /* set the status' description */
  std-ch = "".
  publish "GetSysPropDesc" ("AMD", "agent", "Status", data.stat, output std-ch).
  data.stat:screen-value in browse {&browse-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  /* when the row is highlighted, display the name in the tooltip */
  if available data
   then
    do:
      /* determine if the column is big enough for the value */
      assign
        std-ha = getColumn(self:handle, "name")
        std-lo = (std-ha:width-chars < length(std-ha:screen-value))
        self:tooltip = (if std-lo then std-ha:screen-value else "")
        .
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  setFilterCombos(self:label).
  displayStatus().
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  setFilterCombos(self:label).
  displayStatus().
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  setFilterCombos(self:label).
  displayStatus().
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fView C-Win
ON VALUE-CHANGED OF fView IN FRAME fMain
DO:
  run FilterData in this-procedure.
  displayStatus().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain /* Year */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/report-progress-bar.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/set-filter.i &label="Status"  &id="stat"    &value="statDesc"}
{lib/set-filter.i &label="Manager" &id="manager" &value="managerDesc"}
{lib/set-filter.i &label="Agent"   &id="agentID" &value="name"        &populate=false}
{lib/set-button.i &label="Go"}
{lib/set-button.i &label="Export"}
setButtons().

subscribe to "DataError" anywhere.

session:immediate-display = yes.

fView = "N". /* Net Premium */
sensitizeReportButtons(false).
bGo:sensitive = true.
initializeStatusWindow({&window-name}:handle).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &yr=sYear}
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list-filter.i &combo=fAgent &t=data &state=tStateID &addAll=true &stat=fStatus &manager=fManager}
    /* set the values */
    {lib/set-current-value.i &state=tStateID}
    {lib/set-current-value.i &yr=sYear}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'name'" &var=dBrwColWidth}
  clearData().
  
  run WindowResized in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "statebatchactivity.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "statebatchactivity.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input param pMsg as char.

  /* Since Progress is single-threaded, this works */
  if not tGettingData 
   then return.

  tKeepGettingData = false.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the System Administrator if the problem persists."
    view-as alert-box error.

  clearData().
  
  sensitizeReportButtons(true).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY sYear tStateID fView fAgent fStatus fManager tPolCount tBatchCount 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo fAgent fStatus fManager tPolCount brwData tPolLiability 
         tPolGross tPolNet tFileCount tBatchCount 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "State_Batch_Activity"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterData C-Win 
PROCEDURE FilterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def buffer ops18 for ops18.
  def buffer agent for agent.
  def var cFilter as char no-undo.
  
  do with frame {&frame-name}:
    
    assign
      tPolCount = 0
      tPolLiability = 0
      tPolGross = 0
      tPolNet = 0
      tFileCount = 0
      tBatchCount = 0
      .
  
    close query brwData.
    empty temp-table data.
  
    cFilter = fView:input-value.
  
    for each ops18:
      process events.
     
      create data.
      assign
        data.agentID     = ops18.agentID
        data.stateID     = ops18.stateID
        data.name        = ops18.name
        data.stat        = ops18.stat
        data.statDesc    = ops18.statDesc
        data.manager     = ops18.manager
        data.managerDesc = ops18.managerDesc
        .
     
      /* any null values should be put as blank space */
      if data.manager = "?" or data.manager = ?
       then data.manager = "".
  
      case cFilter:
       when "G" then 
        assign
          data.janValue = ops18.janGross
          data.febValue = ops18.febGross
          data.marValue = ops18.marGross
          data.aprValue = ops18.aprGross
          data.mayValue = ops18.mayGross
          data.junValue = ops18.junGross
          data.julValue = ops18.julGross
          data.augValue = ops18.augGross
          data.sepValue = ops18.sepGross
          data.octValue = ops18.octGross
          data.novValue = ops18.novGross
          data.decValue = ops18.decGross
          .
       when "N" then 
        assign 
          data.janValue = ops18.janNet
          data.febValue = ops18.febNet
          data.marValue = ops18.marNet
          data.aprValue = ops18.aprNet
          data.mayValue = ops18.mayNet
          data.junValue = ops18.junNet
          data.julValue = ops18.julNet
          data.augValue = ops18.augNet
          data.sepValue = ops18.sepNet
          data.octValue = ops18.octNet
          data.novValue = ops18.novNet
          data.decValue = ops18.decNet
          .
       when "R" then 
        assign 
          data.janValue = ops18.janRetain
          data.febValue = ops18.febRetain
          data.marValue = ops18.marRetain
          data.aprValue = ops18.aprRetain
          data.mayValue = ops18.mayRetain
          data.junValue = ops18.junRetain
          data.julValue = ops18.julRetain
          data.augValue = ops18.augRetain
          data.sepValue = ops18.sepRetain
          data.octValue = ops18.octRetain
          data.novValue = ops18.novRetain
          data.decValue = ops18.decRetain
          .
       when "L" then
        assign 
          data.janValue = ops18.janLiab
          data.febValue = ops18.febLiab
          data.marValue = ops18.marLiab
          data.aprValue = ops18.aprLiab
          data.mayValue = ops18.mayLiab
          data.junValue = ops18.junLiab
          data.julValue = ops18.julLiab
          data.augValue = ops18.augLiab
          data.sepValue = ops18.sepLiab
          data.octValue = ops18.octLiab
          data.novValue = ops18.novLiab
          data.decValue = ops18.decLiab
          .
       when "P" then 
        assign 
          data.janValue = ops18.janPolicies
          data.febValue = ops18.febPolicies
          data.marValue = ops18.marPolicies
          data.aprValue = ops18.aprPolicies
          data.mayValue = ops18.mayPolicies
          data.junValue = ops18.junPolicies
          data.julValue = ops18.julPolicies
          data.augValue = ops18.augPolicies
          data.sepValue = ops18.sepPolicies
          data.octValue = ops18.octPolicies
          data.novValue = ops18.novPolicies
          data.decValue = ops18.decPolicies
          .
       when "F" then 
        assign 
          data.janValue = ops18.janFiles
          data.febValue = ops18.febFiles
          data.marValue = ops18.marFiles
          data.aprValue = ops18.aprFiles
          data.mayValue = ops18.mayFiles
          data.junValue = ops18.junFiles
          data.julValue = ops18.julFiles
          data.augValue = ops18.augFiles
          data.sepValue = ops18.sepFiles
          data.octValue = ops18.octFiles
          data.novValue = ops18.novFiles
          data.decValue = ops18.decFiles
          .
       when "B" then 
        assign 
          data.janValue = ops18.janBatches
          data.febValue = ops18.febBatches
          data.marValue = ops18.marBatches
          data.aprValue = ops18.aprBatches
          data.mayValue = ops18.mayBatches
          data.junValue = ops18.junBatches
          data.julValue = ops18.julBatches
          data.augValue = ops18.augBatches
          data.sepValue = ops18.sepBatches
          data.octValue = ops18.octBatches
          data.novValue = ops18.novBatches
          data.decValue = ops18.decBatches
          .
       when "T" then
        assign
          data.janValue = ops18.janGap
          data.febValue = ops18.febGap
          data.marValue = ops18.marGap
          data.aprValue = ops18.aprGap
          data.mayValue = ops18.mayGap
          data.junValue = ops18.junGap
          data.julValue = ops18.julGap
          data.augValue = ops18.augGap
          data.sepValue = ops18.sepGap
          data.octValue = ops18.octGap
          data.novValue = ops18.novGap
          data.decValue = ops18.decGap
          .
      end case.
    
      data.ytdValue = data.janValue + data.febValue + data.marValue
                    + data.aprValue + data.mayValue + data.junValue
                    + data.julValue + data.augValue + data.sepValue
                    + data.octValue + data.novValue + data.decValue.
  
      /* set the YTD values to 0 for Report Gap */
      if cFilter = "T"
       then data.ytdValue = 0.
       
      /* das: figure out how to increment these... */
      assign
        tPolCount = tPolCount + (ops18.janPolicies
                              + ops18.febPolicies
                              + ops18.marPolicies
                              + ops18.aprPolicies
                              + ops18.mayPolicies
                              + ops18.junPolicies
                              + ops18.julPolicies
                              + ops18.augPolicies
                              + ops18.sepPolicies
                              + ops18.octPolicies
                              + ops18.novPolicies
                              + ops18.decPolicies)
      
        tPolLiability = tPolLiability + (ops18.janLiab
                                      + ops18.febLiab
                                      + ops18.marLiab
                                      + ops18.aprLiab
                                      + ops18.mayLiab
                                      + ops18.junLiab
                                      + ops18.julLiab
                                      + ops18.augLiab
                                      + ops18.sepLiab
                                      + ops18.octLiab
                                      + ops18.novLiab
                                      + ops18.decLiab)
      
        tPolGross = tPolGross + (ops18.janGross
                              + ops18.febGross
                              + ops18.marGross
                              + ops18.aprGross
                              + ops18.mayGross
                              + ops18.junGross
                              + ops18.julGross
                              + ops18.augGross
                              + ops18.sepGross
                              + ops18.octGross
                              + ops18.novGross
                              + ops18.decGross)
        
        tPolNet = tPolNet + (ops18.janNet
                          + ops18.febNet
                          + ops18.marNet
                          + ops18.aprNet
                          + ops18.mayNet
                          + ops18.junNet
                          + ops18.julNet
                          + ops18.augNet
                          + ops18.sepNet
                          + ops18.octNet
                          + ops18.novNet
                          + ops18.decNet)
        
        tFileCount = tFileCount + (ops18.janFiles
                                + ops18.febFiles
                                + ops18.marFiles
                                + ops18.aprFiles
                                + ops18.mayFiles
                                + ops18.junFiles
                                + ops18.julFiles
                                + ops18.augFiles
                                + ops18.sepFiles
                                + ops18.octFiles
                                + ops18.novFiles
                                + ops18.decFiles)
        
        tBatchCount = tBatchCount + (ops18.janBatches
                                  + ops18.febBatches
                                  + ops18.marBatches
                                  + ops18.aprBatches
                                  + ops18.mayBatches
                                  + ops18.junBatches
                                  + ops18.julBatches
                                  + ops18.augBatches
                                  + ops18.sepBatches
                                  + ops18.octBatches
                                  + ops18.novBatches
                                  + ops18.decBatches)
       .
    end.
  end.
  
  
  displayTotals().
   
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).

  if cFilter = "T"
   then
    do:
      {lib/brw-totalData.i &noShow=true}
    end.
  
  hide message no-pause.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEF VAR tStartTime AS DATETIME NO-UNDO.
  def var tSuccess as logical init false no-undo.
  def var tMsg as char no-undo.
 
  clearData().
 
  tGettingData = true.
  tkeepGettingData = true.
 
  GETTING-DATA:
  do with frame {&frame-name}:
    
    tStartTime = NOW.
    run server/getstatebatchytdvolume.p (sYear:input-value,
                                         tStateID:input-value,
                                         "",
                                         output table ops18,
                                         output tSuccess,
                                         output tMsg).
     if not tSuccess
      then message "GetStateBatchYTDVolume failed: " tMsg view-as alert-box warning.
   
    process events.
       
    if not tKeepGettingData 
     then leave GETTING-DATA.
    
    RUN SetProgressStatus.
  
    if not tKeepGettingData 
     then
      do:
          clearData().
          tGettingData = false.
          return.
      end.
   
    run FilterData.
    
    tGettingData = false.

    apply "VALUE-CHANGED":U to {&browse-name}.
   
    /* show the filters */
    setFilterCombos("ALL").
    appendStatus("in " + trim(string(interval(now, tStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
    displayStatus().
  end. /* GETTING-DATA */
  run SetProgressEnd.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  tWhereClause = getWhereClause().

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +" &post-by-clause="+ ' by data.agentID '"}
  setStatus(string(num-results("{&browse-name}")) + " agent(s) shown").
  {lib/brw-totalData.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var iOffset as int no-undo init 5.
  def var dNewColWidth as decimal no-undo.
  dNewColWidth = dBrwColWidth * session:pixels-per-column.
  
  def var dDiffWidth as decimal no-undo.

 if totalAdded
  then iOffset = 28.
  
  assign
    /* resize the frame */
    frame {&frame-name}:width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
    /* resize the browse */
    {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10
    {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - iOffset
    .
  
  /* resize the column width */
  {lib/resize-column.i &col="'name'" &var=dBrwColWidth}

  {lib/brw-totalData.i}
  apply "VALUE-CHANGED":U to {&browse-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    
    close query brwData.
    empty temp-table data. 
    empty temp-table ops18.

    hide message no-pause.
    
    assign
      tPolCount = 0
      tPolLiability = 0
      tPolGross = 0
      tPolNet = 0
      tFileCount = 0
      tBatchCount = 0
      .
      
    display
      tPolCount
      tPolLiability
      tPolGross
      tPolNet
      tFileCount
      tBatchCount
      .
    assign
      /* default the values */
      fStatus:screen-value = "ALL"
      fManager:screen-value = "ALL"
      /* disable the filters */
      bExport:sensitive = false
      fAgent:sensitive = false
      fStatus:sensitive = false
      fManager:sensitive = false
      .
    run AgentComboFilterEnable in this-procedure (fAgent:name, false).
    clearStatus().
  end.
 {lib/brw-totalData.i}
                  
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
do with frame {&frame-name}:
  
  display
    tPolCount
    tPolLiability
    tPolGross
    tPolNet
    tFilecount
    tBatchCount
    .
  
  pause 0.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitizeReportButtons C-Win 
FUNCTION sensitizeReportButtons RETURNS LOGICAL PRIVATE
  ( input pCmd as logical ) :

  do with frame {&frame-name}:
    assign
      std-lo = can-find(first data)
      sYear:sensitive = pCmd
      tStateID:sensitive = pCmd
      fView:sensitive = pCmd and std-lo
      bExport:sensitive  = pCmd and std-lo
      fAgent:sensitive = pCmd and std-lo
      fStatus:sensitive = pCmd and std-lo
      fManager:sensitive = pCmd and std-lo
      .
    run AgentComboFilterEnable in this-procedure (fAgent:name, pCmd and std-lo).
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


