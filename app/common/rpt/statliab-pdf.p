&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* statliab.p
  ----------------------------------------------------------------------*/

{tt/state.i}
{tt/statliab.i &tableAlias=ttData}

def input param pMonth as int.
def input param pYear as int.
def input param pState as char.
def input param pAgents as int.
def input param pBatches as int.
def input param pFiles as int.
def input param pPolicies as int.
def input param pOwnerOnly as dec.
def input param pLenderOnly as dec.
def input param pOwnerLender as dec.
def input param pLenderOwner as dec.
def input param pSubjectToReserve as dec.

def input parameter table for ttData.

def var tMonthList as char init "January,February,March,April,May,June,July,August,September,October,November,December".
def var tMonthName as char.
def var tStateDesc as char.

{lib/rpt-def.i &ID="'OPS05'" &lines=100}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

assign
  activeFont = "Helvetica"
  activeSize = 10.0
  .

find first ttData no-error.
if not available ttData
 then 
  do: 
      MESSAGE "There is nothing to print"
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.

run getFilename in this-procedure no-error.
if error-status:error 
 then return.

tMonthName = entry(pMonth, tMonthList).
 
publish "GetStates" (output table state). 
if pState = "" then
tStateDesc = "All States".
else
do:
  find first state where state.stateID = pState no-lock no-error.
  tStateDesc = if avail state then state.description else "UNKNOWN".
end.

{lib/rpt-open.i}

run details in this-procedure.
/* run summary in this-procedure.  */

{lib/rpt-cloz.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE details Procedure 
PROCEDURE details PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  setFont("Helvetica-BoldOblique", 12.0). */
/*  setFont("Helvetica-Bold", 12.0).        */
/*  setFont("Helvetica", 12.0).             */

 def var totOwnerNum as int.
 def var totLenderNum as int.
 def var totOwnerLiability as dec.
 def var totLenderLiability as dec.
 def var totReservableLiability as dec.

 setFont("Courier", 7.0).
 textColor(0.0, 0.0, 0.0).

 /* Report detail */
 
 for each ttData
   break by ttData.agentID
         by ttData.fileNumber:
  
  totOwnerNum = totOwnerNum + ttData.ownerNum.
  totLenderNum = totLenderNum + ttData.lenderNum.
  totOwnerLiability = totOwnerLiability + ttData.ownerLiability.
  totLenderLiability = totLenderLiability + ttData.lenderLiability.
  totReservableLiability = totReservableLiability + ttData.reservableLiability.
  
  textAt(ttData.agentID, 1).
  textAt(substring(ttData.name,1,30), 10).
  textAt(ttData.stateID, 42).
  textAt(ttData.fileNumber, 45).
  textTo(string(ttData.ownerNum), 66).
  textTo(string(ttData.lenderNum), 72).
  textTo(string(ttData.ownerLiability, "->>,>>>,>>>,>>9"), 90).
  textTo(string(ttData.lenderLiability, "->>,>>>,>>>,>>9"), 108).
  textTo(string(ttData.reservableLiability, "->>,>>>,>>>,>>9"), 126).
  textAt(ttData.grouped, 128).
  newLine(1).
  
 end.

 newLine(1).
 
 setFont("Courier-Bold", 7.0).
 textAt("TOTALS:", 1).
 textTo(string(totOwnerNum), 66).
 textTo(string(totLenderNum), 72).
 textTo(string(round(totOwnerLiability, 0), "->>,>>>,>>>,>>9"), 90).
 textTo(string(round(totLenderLiability, 0), "->>,>>>,>>>,>>9"), 108).
 textTo(string(round(totReservableLiability, 0), "->>,>>>,>>>,>>9"), 126).

 newLine(1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getFilename Procedure 
PROCEDURE getFilename PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "GetReportDir" (output activeFilename).
 if activeFilename > "" 
  then if lookup(substring(activeFilename, length(activeFilename), 1), "/,\") = 0
        then activeFilename = activeFilename + "/".
 activeFilename = activeFilename + "StatutoryLiability.pdf".
 if search(activeFilename) <> ? 
  then
   do: 
       os-delete value(activeFilename).
       if os-error <> 0
        then
         do:
             MESSAGE "Please close the open details report"
              VIEW-AS ALERT-BOX warning BUTTONS OK.
             return error.
         end.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/rpt-ftr.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tTitle as char no-undo.

 tTitle = "STATUTORY LIABILITY REPORT".
 {lib/rpt-hdr.i &title=tTitle &title-x=10 &title-y=750 &no-logo=true 
                &title2="tMonthName + ' ' + string(pYear) + ' - ' + tStateDesc"}

 setFont("Courier-Bold", 7.0).
 textColor(0.0, 0.0, 0.0).
 
 textAt("Agents:", 1).              textTo(string(pAgents), 16).  
 textAt("Owner Only:", 20).         textTo(string(pOwnerOnly, "->>,>>>,>>>,>>9"), 54).
 textAt("Subject to Reserve:", 58). textTo(string(pSubjectToReserve, "->>,>>>,>>>,>>9"), 96).
 newLine(1).
 textAt("Batches:", 1).         textTo(string(pBatches), 16).  
 textAt("Lender Only:", 20).    textTo(string(pLenderOnly, "->>,>>>,>>>,>>9"), 54).
 newLine(1).
 textAt("Files:", 1).           textTo(string(pFiles), 16).  
 textAt("Owner > Lender:", 20). textTo(string(pOwnerLender, "->>,>>>,>>>,>>9"), 54).
 newLine(1).
 textAt("Policies:", 1).        textTo(string(pPolicies), 16).  
 textAt("Lender > Owner:", 20). textTo(string(pLenderOwner, "->>,>>>,>>>,>>9"), 54).
 
 newLine(2).

 textAt("File", 45).
 textTo("Owner", 90).
 textTo("Lender", 108).
 textTo("Liability Subject", 126).
 
 newLine(1).

 textAt("AgentID", 1).
 textAt("Name", 10).
 textAt("ST", 42).
 textAt("Number", 45).
 textTo("#Own", 66).
 textTo("#Lnd", 72).
 textTo("Liability", 90).
 textTo("Liability", 108).
 textTo("to Reserve", 126).
 textAt("Group", 128).

 newLine(2).

 setFont("Courier", 7.0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-summary) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE summary Procedure 
PROCEDURE summary PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 textColor(0.0, 0.0, 0.0).
 setFont("Helvetica-Bold", 8.0).
 textAt("SUMMARY", 2).
 newLine(2).

 setFont("Helvetica", 8.0).
 textAt("Not Implemented yet...", 10).
 newLine(1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

