&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wAccounting
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wAccounting 
/*---------------------------------------------------------------------
@name Agent Accounting (wcrm04-r.w)
@description The user selects an agent and period. The Payments browse 
             shows the checks for the period. The Invoices browse 
             shows the invoices that the selected check was applied to.

@author John Oliver
@version 1.0
@created 2017/03/01
@notes 
@Modified
Date        Name    Description
05/25/21    AG      Task #: 82304 - Pull data from compass AR instead of GP
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/brw-multi-def.i}
{lib/get-last-day.i}
{tt/period.i}
{tt/agentinvoice.i}
{tt/agentinvoice.i &tableAlias="agentpayment"}
{tt/artran.i       &tableAlias="ttArPostedTran"}   /* contain data recived from server */
{tt/agent.i}
{tt/state.i}

define variable currPaymentNumber as character no-undo.
define variable dtDateTo as datetime no-undo.
define variable dtDateFrom as datetime no-undo.
define variable iCounter as int no-undo initial 0.
define variable iCounterMax as int no-undo.
define variable hDocs as handle no-undo.

/* used for the resize */
define variable dParamRectWidth as decimal no-undo.
define variable dBrwInvHeight as decimal no-undo.
define variable dBrwInvWidth as decimal no-undo.
define variable dBrwInvColumn as decimal no-undo.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fAccounting
&Scoped-define BROWSE-NAME brwInvoices

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentinvoice agentpayment

/* Definitions for BROWSE brwInvoices                                   */
&Scoped-define FIELDS-IN-QUERY-brwInvoices agentinvoice.fileNumber agentinvoice.batchID agentinvoice.invoiceDate agentinvoice.invoiceAmount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwInvoices   
&Scoped-define SELF-NAME brwInvoices
&Scoped-define QUERY-STRING-brwInvoices FOR EACH agentinvoice NO-LOCK BY agentinvoice.invoiceNumber INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwInvoices OPEN QUERY {&SELF-NAME} FOR EACH agentinvoice NO-LOCK BY agentinvoice.invoiceNumber INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwInvoices agentinvoice
&Scoped-define FIRST-TABLE-IN-QUERY-brwInvoices agentinvoice


/* Definitions for BROWSE brwPayments                                   */
&Scoped-define FIELDS-IN-QUERY-brwPayments agentpayment.paymentDate agentpayment.checkNumber agentpayment.amountTotal agentpayment.amountApplied agentpayment.amountUnapplied   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwPayments   
&Scoped-define SELF-NAME brwPayments
&Scoped-define QUERY-STRING-brwPayments FOR EACH agentpayment NO-LOCK BY agentpayment.paymentDate INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwPayments OPEN QUERY {&SELF-NAME} FOR EACH agentpayment NO-LOCK BY agentpayment.paymentDate INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwPayments agentpayment
&Scoped-define FIRST-TABLE-IN-QUERY-brwPayments agentpayment


/* Definitions for FRAME fAccounting                                    */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fAccounting ~
    ~{&OPEN-QUERY-brwInvoices}~
    ~{&OPEN-QUERY-brwPayments}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bGo tFromMonth tFromYear tToMonth ~
rParam tToYear tStateID rPayments tAgentID rTotalPayments rInvoices ~
chkUnapplied brwPayments tFilterInvoices brwInvoices tTotalAmount ~
tTotalCount tTotalUnapplied tTotalApplied tTotalText tTotalAmountText ~
tTotalCountText tTotalUnappliedText tTotalAppliedText 
&Scoped-Define DISPLAYED-OBJECTS tFromMonth tFromYear tToMonth tToYear ~
tStateID tAgentID chkUnapplied tFilterInvoices tTotalAmount tTotalCount ~
tTotalUnapplied tTotalApplied tTotalText tTotalAmountText tTotalCountText ~
tTotalUnappliedText tTotalAppliedText 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData wAccounting 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD closeDocsWindow wAccounting 
FUNCTION closeDocsWindow RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getYearCombo wAccounting 
FUNCTION getYearCombo RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus wAccounting 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus wAccounting 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitizeReportButtons wAccounting 
FUNCTION sensitizeReportButtons RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wAccounting AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwInvoices 
       MENU-ITEM m_Batch_Details LABEL "Batch Details". 


/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDocs  NO-FOCUS
     LABEL "Docs" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bFilterInvoicesClear 
     LABEL "Clear" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bFilterInvoicesGo 
     LABEL "Go" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE VARIABLE tAgentID AS CHARACTER INITIAL "None" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN
     SIZE 86 BY 1 NO-UNDO.

DEFINE VARIABLE tFromMonth AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "From" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tFromYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 8
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE tToMonth AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "To" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tToYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 8
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 16 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tFilterInvoices AS CHARACTER FORMAT "X(256)":U 
     LABEL "Filter" 
     VIEW-AS FILL-IN 
     SIZE 41 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalAmount AS DECIMAL FORMAT "$ (>,>>>,>>>,>>9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE tTotalAmountText AS CHARACTER FORMAT "X(256)":U INITIAL "Amount:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE tTotalApplied AS DECIMAL FORMAT "$ (>,>>>,>>>,>>9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE tTotalAppliedText AS CHARACTER FORMAT "X(256)":U INITIAL "Applied:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE tTotalCount AS INTEGER FORMAT ">>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE tTotalCountText AS CHARACTER FORMAT "X(256)":U INITIAL "Count:" 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE VARIABLE tTotalText AS CHARACTER FORMAT "X(256)":U INITIAL "Total" 
      VIEW-AS TEXT 
     SIZE 5.2 BY .62 NO-UNDO.

DEFINE VARIABLE tTotalUnapplied AS DECIMAL FORMAT "$ (>,>>>,>>>,>>9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE tTotalUnappliedText AS CHARACTER FORMAT "X(256)":U INITIAL "Unapplied:" 
      VIEW-AS TEXT 
     SIZE 10 BY .62 NO-UNDO.

DEFINE RECTANGLE rInvoices
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 118 BY 17.95.

DEFINE RECTANGLE rParam
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 203 BY 3.57.

DEFINE RECTANGLE rPayments
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 83 BY 17.95.

DEFINE RECTANGLE rTotalPayments
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 3.33.

DEFINE VARIABLE chkUnapplied AS LOGICAL INITIAL no 
     LABEL "Hide Fully Applied Payments" 
     VIEW-AS TOGGLE-BOX
     SIZE 31 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwInvoices FOR 
      agentinvoice SCROLLING.

DEFINE QUERY brwPayments FOR 
      agentpayment SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwInvoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwInvoices wAccounting _FREEFORM
  QUERY brwInvoices NO-LOCK DISPLAY
     
agentinvoice.fileNumber FORMAT "x(50)" WIDTH 30.5 column-label "File!Number"
agentinvoice.batchID FORMAT "x(50)" WIDTH 30.5 column-label "Batch!Number"
agentinvoice.invoiceDate FORMAT "99/99/99" WIDTH 15 column-label "Invoice!Date"
agentinvoice.invoiceAmount FORMAT "(>>>,>>>,>>9.99)" WIDTH 15 LABEL "Amount"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 114 BY 15.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwPayments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwPayments wAccounting _FREEFORM
  QUERY brwPayments NO-LOCK DISPLAY
      agentpayment.paymentDate FORMAT "99/99/99" WIDTH 20 LABEL "Check Date"
agentpayment.checkNumber FORMAT "x(10)" WIDTH 15 LABEL "Check ID"
agentpayment.amountTotal FORMAT "(>>>,>>>,>>9.99)" WIDTH 12 LABEL "Amount"
agentpayment.amountApplied FORMAT "(>>>,>>>,>>9.99)" WIDTH 12 column-label "Applied!Amount"
agentpayment.amountUnapplied FORMAT "(>>>,>>>,>>9.99)" WIDTH 12 column-label "Unapplied!Amount"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 79 BY 11.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fAccounting
     bDocs AT ROW 2.19 COL 157.4 WIDGET-ID 148 NO-TAB-STOP 
     bExport AT ROW 2.19 COL 150 WIDGET-ID 104 NO-TAB-STOP 
     bGo AT ROW 2.19 COL 143 WIDGET-ID 86 NO-TAB-STOP 
     tFromMonth AT ROW 2.19 COL 8 COLON-ALIGNED WIDGET-ID 126
     tFromYear AT ROW 2.19 COL 28 COLON-ALIGNED NO-LABEL WIDGET-ID 128
     tToMonth AT ROW 3.38 COL 8 COLON-ALIGNED WIDGET-ID 130
     tToYear AT ROW 3.38 COL 28 COLON-ALIGNED NO-LABEL WIDGET-ID 132
     tStateID AT ROW 2.19 COL 53 COLON-ALIGNED WIDGET-ID 82
     tAgentID AT ROW 3.38 COL 53 COLON-ALIGNED WIDGET-ID 84
     chkUnapplied AT ROW 6.48 COL 4 WIDGET-ID 134
     brwPayments AT ROW 7.43 COL 4 WIDGET-ID 200
     tFilterInvoices AT ROW 6.19 COL 93 COLON-ALIGNED WIDGET-ID 120
     bFilterInvoicesGo AT ROW 6.14 COL 136.6 WIDGET-ID 122
     bFilterInvoicesClear AT ROW 6.14 COL 141.6 WIDGET-ID 124
     brwInvoices AT ROW 7.43 COL 89 WIDGET-ID 300
     tTotalAmount AT ROW 20.33 COL 15 COLON-ALIGNED NO-LABEL WIDGET-ID 140 NO-TAB-STOP 
     tTotalCount AT ROW 20.33 COL 52 COLON-ALIGNED NO-LABEL WIDGET-ID 146 NO-TAB-STOP 
     tTotalUnapplied AT ROW 21.52 COL 15 COLON-ALIGNED NO-LABEL WIDGET-ID 144 NO-TAB-STOP 
     tTotalApplied AT ROW 21.52 COL 52 COLON-ALIGNED NO-LABEL WIDGET-ID 142 NO-TAB-STOP 
     tTotalText AT ROW 19.33 COL 3 COLON-ALIGNED NO-LABEL WIDGET-ID 154
     tTotalAmountText AT ROW 20.48 COL 6.6 COLON-ALIGNED NO-LABEL WIDGET-ID 156
     tTotalCountText AT ROW 20.52 COL 45.4 COLON-ALIGNED NO-LABEL WIDGET-ID 158
     tTotalUnappliedText AT ROW 21.71 COL 4.2 COLON-ALIGNED NO-LABEL WIDGET-ID 160
     tTotalAppliedText AT ROW 21.71 COL 43.8 COLON-ALIGNED NO-LABEL WIDGET-ID 162
     "Invoices" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 5.29 COL 88 WIDGET-ID 152
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 4
     "Payments" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 5.29 COL 3 WIDGET-ID 108
     rParam AT ROW 1.48 COL 2 WIDGET-ID 2
     rPayments AT ROW 5.52 COL 2 WIDGET-ID 106
     rTotalPayments AT ROW 19.62 COL 4 WIDGET-ID 136
     rInvoices AT ROW 5.52 COL 87 WIDGET-ID 150
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 204.8 BY 22.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wAccounting ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Accounting"
         HEIGHT             = 22.62
         WIDTH              = 204.8
         MAX-HEIGHT         = 36.71
         MAX-WIDTH          = 285.4
         VIRTUAL-HEIGHT     = 36.71
         VIRTUAL-WIDTH      = 285.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wAccounting
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fAccounting
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwPayments chkUnapplied fAccounting */
/* BROWSE-TAB brwInvoices bFilterInvoicesClear fAccounting */
/* SETTINGS FOR BUTTON bDocs IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bFilterInvoicesClear IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bFilterInvoicesGo IN FRAME fAccounting
   NO-ENABLE                                                            */
ASSIGN 
       brwInvoices:POPUP-MENU IN FRAME fAccounting             = MENU POPUP-MENU-brwInvoices:HANDLE.

ASSIGN 
       tFilterInvoices:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalAmount:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalApplied:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalCount:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalUnapplied:READ-ONLY IN FRAME fAccounting        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wAccounting)
THEN wAccounting:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwInvoices
/* Query rebuild information for BROWSE brwInvoices
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentinvoice NO-LOCK BY agentinvoice.invoiceNumber INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwInvoices */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwPayments
/* Query rebuild information for BROWSE brwPayments
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentpayment NO-LOCK BY agentpayment.paymentDate INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwPayments */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fAccounting:HANDLE
       ROW             = 4
       COLUMN          = 143
       HEIGHT          = .48
       WIDTH           = 22
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wAccounting
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wAccounting wAccounting
ON END-ERROR OF wAccounting /* Agent Accounting */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wAccounting wAccounting
ON WINDOW-CLOSE OF wAccounting /* Agent Accounting */
DO:
  /* This event will close the window and terminate the procedure.  */
  closeDocsWindow().
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wAccounting wAccounting
ON WINDOW-RESIZED OF wAccounting /* Agent Accounting */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocs wAccounting
ON CHOOSE OF bDocs IN FRAME fAccounting /* Docs */
DO:
  if valid-handle(hDocs)
   then run ShowWindow in hDocs.
   else run rpt/agentaccounting-docs.w persistent set hDocs (tAgentID:screen-value in FRAME {&frame-name}).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wAccounting
ON CHOOSE OF bExport IN FRAME fAccounting /* Export */
DO:
  sensitizeReportButtons(false).
  run exportData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterInvoicesClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterInvoicesClear wAccounting
ON CHOOSE OF bFilterInvoicesClear IN FRAME fAccounting /* Clear */
DO:
  apply "CHOOSE":U to bFilterInvoicesGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterInvoicesGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterInvoicesGo wAccounting
ON CHOOSE OF bFilterInvoicesGo IN FRAME fAccounting /* Go */
DO:
  RUN filterInvoices IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo wAccounting
ON CHOOSE OF bGo IN FRAME fAccounting /* Go */
DO:
  sensitizeReportButtons(false).
  run getData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwInvoices
&Scoped-define SELF-NAME brwInvoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwInvoices wAccounting
ON ROW-DISPLAY OF brwInvoices IN FRAME fAccounting
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwInvoices wAccounting
ON START-SEARCH OF brwInvoices IN FRAME fAccounting
DO:
  RUN filterInvoices IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwPayments
&Scoped-define SELF-NAME brwPayments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayments wAccounting
ON ROW-DISPLAY OF brwPayments IN FRAME fAccounting
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayments wAccounting
ON START-SEARCH OF brwPayments IN FRAME fAccounting
DO:
  {lib/brw-startSearch-multi.i}
  /* assign the dataSortBy */
  def var hColumn as handle no-undo.
  hColumn = self:current-column.
  /* correct the invoice browse */
  if available agentinvoice
   then run filterInvoices IN this-procedure.
  /* save the dataSortBy column */
  dataSortBy = hColumn:name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayments wAccounting
ON VALUE-CHANGED OF brwPayments IN FRAME fAccounting
DO:
  if available agentpayment
   then 
    do:
      currPaymentNumber = agentpayment.paymentNumber.
      RUN filterInvoices IN this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwInvoices wAccounting
ON VALUE-CHANGED OF brwInvoices IN FRAME fAccounting
DO:
  if available agentInvoice
   then 
    publish "SetCurrentValue" ("BatchID", string(agentInvoice.batchID)).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




&Scoped-define SELF-NAME chkUnapplied
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL chkUnapplied wAccounting
ON VALUE-CHANGED OF chkUnapplied IN FRAME fAccounting /* Hide Fully Applied Payments */
DO:
  RUN filterPayments IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define SELF-NAME m_Batch_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Batch_Details wWin
ON CHOOSE OF MENU-ITEM m_Batch_Details /* Batch Details */
DO:
  run wops01-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID wAccounting
ON VALUE-CHANGED OF tAgentID IN FRAME fAccounting /* Agent */
DO:
  if self:screen-value <> "None"
   then bDocs:sensitive in frame {&frame-name} = true.
   else bDocs:sensitive in frame {&frame-name} = false.
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFilterInvoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFilterInvoices wAccounting
ON RETURN OF tFilterInvoices IN FRAME fAccounting /* Filter */
DO:
  APPLY "CHOOSE":U TO bFilterInvoicesGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFromMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFromMonth wAccounting
ON VALUE-CHANGED OF tFromMonth IN FRAME fAccounting /* From */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFromYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFromYear wAccounting
ON VALUE-CHANGED OF tFromYear IN FRAME fAccounting
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID wAccounting
ON VALUE-CHANGED OF tStateID IN FRAME fAccounting /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tToMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tToMonth wAccounting
ON VALUE-CHANGED OF tToMonth IN FRAME fAccounting /* To */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tToYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tToYear wAccounting
ON VALUE-CHANGED OF tToYear IN FRAME fAccounting
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwInvoices
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wAccounting 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}

subscribe to "SetProgressCounter" anywhere.
subscribe to "SetProgressStatus" anywhere.
subscribe to "SetProgressEnd" anywhere.

assign
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels = session:width-pixels
  .

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bGo:load-image("images/completed.bmp").
bGo:load-image-insensitive("images/completed-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bDocs:load-image("images/book.bmp").
bDocs:load-image-insensitive("images/book-i.bmp").
bFilterInvoicesGo:load-image("images/s-completed.bmp").
bFilterInvoicesGo:load-image-insensitive("images/s-completed-i.bmp").
bFilterInvoicesClear:load-image("images/s-erase.bmp").
bFilterInvoicesClear:load-image-insensitive("images/s-erase-i.bmp").

/* setup the browse widgets */
{lib/brw-main-multi.i &browse-list="brwPayments,brwInvoices"}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID}
    {lib/get-period-list.i &mth=tFromMonth &yr=tFromYear}
    {lib/get-period-list.i &mth=tToMonth &yr=tToYear}
    /* set the values */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
    {lib/set-current-value.i &mth=tFromMonth &yr=tFromYear}
    {lib/set-current-value.i &mth=tToMonth &yr=tToYear}
    
    assign
      /* set the dates */
      dtDateFrom = datetime(tFromMonth:input-value,1,tFromYear:input-value,0,0)
      dtDateTo = datetime(tToMonth:input-value,getLastDay(date(tToMonth:input-value,1,tToYear:input-value)),tToYear:input-value,0,0)
      /* set the width and heights for the rectangles/browses */
      dParamRectWidth = rParam:width-pixels
      dBrwInvHeight = brwInvoices:height-pixels
      dBrwInvWidth = brwInvoices:width-pixels
      .
    
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'fileNumber'" &var=dBrwInvColumn}
    
  run windowResized in this-procedure.
  clearData().
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load wAccounting  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "agentaccounting.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "agentaccounting.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wAccounting  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wAccounting)
  THEN DELETE WIDGET wAccounting.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wAccounting  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tFromMonth tFromYear tToMonth tToYear tStateID tAgentID chkUnapplied 
          tFilterInvoices tTotalAmount tTotalCount tTotalUnapplied tTotalApplied 
          tTotalText tTotalAmountText tTotalCountText tTotalUnappliedText 
          tTotalAppliedText 
      WITH FRAME fAccounting IN WINDOW wAccounting.
  ENABLE bExport bGo tFromMonth tFromYear tToMonth rParam tToYear tStateID 
         rPayments tAgentID rTotalPayments rInvoices chkUnapplied brwPayments 
         tFilterInvoices brwInvoices tTotalAmount tTotalCount tTotalUnapplied 
         tTotalApplied tTotalText tTotalAmountText tTotalCountText 
         tTotalUnappliedText tTotalAppliedText 
      WITH FRAME fAccounting IN WINDOW wAccounting.
  {&OPEN-BROWSERS-IN-QUERY-fAccounting}
  VIEW wAccounting.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData wAccounting 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
@description Exports the payments and invoices to Excel
------------------------------------------------------------------------------*/
  define buffer agentinvoice for agentinvoice.

  if query brwInvoices:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  publish "GetReportDir" (output std-ch).

  std-ha = temp-table agentinvoice:handle.

  run util/exporttable.p (table-handle std-ha,
                          "agentinvoice",
                          "for each agentinvoice by agentinvoice.invoiceNumber",
                          "invoiceNumber,fileNumber,batchID,invoiceDate,invoiceAmount",    
                          "Transaction ID,File Number,Batch ID,Invoice Date,Invoice Amount",
                          std-ch,
                          "Agent_" + tAgentID:screen-value in frame {&frame-name} + "_Accounting_Report" +
                          replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterInvoices wAccounting 
PROCEDURE filterInvoices :
/*------------------------------------------------------------------------------
@description Filters the invoices browse widget
------------------------------------------------------------------------------*/
  def var cFilterValue as char no-undo.
  def var cWhere as char no-undo init "".
  def var cSort as char no-undo init "by invoiceNumber".
  
  do with frame {&frame-name}:
    cFilterValue = tFilterInvoices:screen-value.
  end.
  
  if cFilterValue <> ""
   then cWhere = " where (batchID begins '" + cFilterValue + "' OR fileNumber begins '" + cFilterValue + "')".
  
  {lib/brw-startSearch-multi.i &browseName="brwInvoices" &whereClause="cWhere" &sortClause="cSort"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterPayments wAccounting 
PROCEDURE filterPayments :
/*------------------------------------------------------------------------------
@description Filters the payments browse widget
------------------------------------------------------------------------------*/
  define variable cWhere as character no-undo initial "".
  define variable cSort as character no-undo initial "by paymentDate".
  DEFINE VARIABLE dAmount AS DECIMAL NO-UNDO INITIAL 0.0.
  DEFINE VARIABLE dApplied AS DECIMAL NO-UNDO INITIAL 0.0.
  DEFINE VARIABLE dUnapplied AS DECIMAL NO-UNDO INITIAL 0.0.
  DEFINE VARIABLE iCount AS INTEGER NO-UNDO INITIAL 0.
  
  do with frame {&frame-name}:
    std-lo = logical(chkUnapplied:screen-value) NO-ERROR.
    /* set the totals */
    IF std-lo
     then cWhere = "where amountUnapplied > 0".
     
    FOR EACH agentpayment:
      ASSIGN
        dAmount = dAmount + agentpayment.amountTotal
        dApplied = dApplied + agentpayment.amountApplied
        dUnapplied = dUnapplied + agentpayment.amountUnapplied
        iCount = iCount + 1
        .
    END.
        
    ASSIGN
      tTotalAmount:SCREEN-VALUE = STRING(dAmount)
      tTotalApplied:SCREEN-VALUE = STRING(dApplied)
      tTotalUnapplied:SCREEN-VALUE = STRING(dUnapplied)
      tTotalCount:SCREEN-VALUE = STRING(iCount)
      .
  end.
  
  {lib/brw-startSearch-multi.i &browseName="brwPayments" &whereClause="cWhere" &sortClause="cSort"}
  apply "VALUE-CHANGED" to brwPayments.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData wAccounting 
PROCEDURE getData :
/*------------------------------------------------------------------------------
@description Gets the report data from the server
------------------------------------------------------------------------------*/
  define variable cAgentID as character no-undo.
  define variable iCurTask as int no-undo initial 0.
  
  /* validate */
  run validation in this-procedure (output std-lo).
  if not std-lo
   then return.
   
  assign
    /* get the agent ID */
    cAgentID = tAgentID:screen-value in frame {&frame-name}
    .

  /* cleanup before getting data */
  publish "SetProgressCounter" (2).
  pbMinStatus().
  clearData().
  
 
  if dtDateTo > datetime(today) then
        dtDateTo = datetime(today).
  
  run server\gettransactions.p( input 0,          /* ArTranID */
                                input "A",        /* Entity */
                                input cAgentID,   /* Entity ID */                                
                                input yes,        /* Include Files */
                                input yes,        /* Include Fully Paid Files */
                                input no,         /* Include Invoices */
                                input no,         /* Include Void Invoices */
                                input no,         /* Include Fully Paid Invoices */
                                input no,         /* Include Credits */
                                input no,         /* Include Void Credits */
                                input no,         /* Include Fully Applied Credits */                                 
                                input yes,        /* Include Payments */
                                input no,         /* Include Void Payments */
                                input yes,        /* Include Fully Applied Payments */                                
                                input no,         /* Include Refunds */
                                input no,         /* Include Void Refunds */ 
                                input dtDateFrom, /* from date */
                                input dtDateTo,   /* To date   */
                                output table ttArPostedTran,                                
                                output std-lo,
                                output std-ch).                              

  if not std-lo
   then
    do:
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      publish "SetProgressCounter" (0).
      return.
    end.
  publish "SetProgressStatus".
  publish "SetProgressEnd".

  /* create the agentpayment table */
  empty temp-table agentpayment.
  empty temp-table agentinvoice.

  for each ttArPostedTran no-lock:
  
    if ttArPostedTran.type = 'P'
     then
      do:
        create agentpayment.
        assign
          agentpayment.agentID         = ttArPostedTran.entityID
          agentpayment.paymentNumber   = ttArPostedTran.tranID
          agentpayment.paymentDate     = ttArPostedTran.trandate
          agentpayment.checkNumber     = ttArPostedTran.reference
          agentpayment.batchID         = ""
          agentpayment.invoiceNumber   = ""
          agentpayment.invoiceDate     = ttArPostedTran.trandate
          agentpayment.fileNumber      = ttArPostedTran.filenumber
          agentpayment.amountTotal     = ttArPostedTran.tranamt
          agentpayment.amountUnapplied = ttArPostedTran.remainingAmt
          agentpayment.invoiceAmount   = 0
          agentpayment.amountApplied   = agentpayment.amountTotal - ttArPostedTran.remainingAmt
          .
      end.
    else if ttArPostedTran.type = 'F'
     then
      do:
        create agentinvoice.
        assign
          agentinvoice.invoiceNumber = string(ttArPostedTran.artranID)
          agentinvoice.fileNumber    = ttArPostedTran.filenumber
          agentinvoice.batchID       = ttArPostedTran.sourceID
          agentinvoice.invoiceDate   = ttArPostedTran.tranDate
          agentinvoice.invoiceAmount = ttArPostedTran.tranAmt
         .
    end.
  end.
  /* display the browse */
  run filterPayments in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressCounter wAccounting 
PROCEDURE SetProgressCounter :
/*------------------------------------------------------------------------------
@description Sets the progress bar count
------------------------------------------------------------------------------*/
  def input parameter pCounter as int no-undo.
  assign
    iCounterMax = pCounter
    iCounter = 0
    .
  pbMinStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressEnd wAccounting 
PROCEDURE SetProgressEnd :
/*------------------------------------------------------------------------------
@description Makes the progress bar minimum after setting to 100
------------------------------------------------------------------------------*/
  pbUpdateStatus(100, 1).
  pbMinStatus().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressStatus wAccounting 
PROCEDURE SetProgressStatus :
/*------------------------------------------------------------------------------
@description Updates the progress bar
------------------------------------------------------------------------------*/
  iCounter = iCounter + 1.
  pbUpdateStatus(int(iCounter / iCounterMax * 100), 0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow wAccounting 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wAccounting 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
@description Sorts the data in the browse widgets
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i} 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validation wAccounting 
PROCEDURE validation :
/*------------------------------------------------------------------------------
@description Validates the criteria
------------------------------------------------------------------------------*/
  def output parameter pSuccess as logical no-undo init false.
  
  do with frame {&frame-name}:
    std-ch = "".
    /* make sure an agent is selected */
    if tAgentID:screen-value = "" or
       tAgentID:screen-value = ?  or
       tAgentID:screen-value = '?'
     then std-ch = "Please select an agent".
    /* make sure the From date is valid */
    dtDateFrom = DATETIME(tFromMonth:INPUT-VALUE,1,tFromYear:INPUT-VALUE,0,0) NO-ERROR.
    if error-status:error
     then std-ch = "The From: date is invalid".
    /* make sure the To date is valid */
      dtDateTo = datetime(tToMonth:input-value,getLastDay(date(tToMonth:input-value,1,tToYear:input-value)),tToYear:input-value,0,0).
    IF dtDateTo = ?
     then std-ch = "The To: date is invalid".
    /* check if the from date is after the to date */
    if interval(dtDateFrom,dtDateTo,"days") > 0
     then std-ch = "The From: date cannot be after the To: date".
    if std-ch > ""
     then MESSAGE std-ch VIEW-AS ALERT-BOX INFO BUTTONS OK.
     else pSuccess = true.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wAccounting 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var dDiffHeight as decimal no-undo.
  def var dDiffWidth as decimal no-undo.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* get the difference between the old and new width\height */
  assign
    dDiffHeight = frame {&frame-name}:height-pixels - {&window-name}:min-height-pixels
    dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels
    .
    
  do with frame {&frame-name}:
    assign
      /* parameter rectangle */
      rParam:width-pixels = dParamRectWidth + dDiffWidth
      /* invoices browse */
      brwInvoices:height-pixels = dBrwInvHeight + dDiffHeight
      brwInvoices:width-pixels = dBrwInvWidth + dDiffWidth
      rInvoices:height-pixels = brwInvoices:height-pixels + 50
      rInvoices:width-pixels = brwInvoices:width-pixels + 20
      /* payments browse */
      brwPayments:height-pixels = brwInvoices:height-pixels - 84
      rPayments:height-pixels = rInvoices:height-pixels
      /* payments total */
      std-de = brwPayments:y + brwPayments:height-pixels
      tTotalText:y = std-de + 7
      rTotalPayments:y = std-de + 13
      tTotalAmount:y = std-de + 27
      tTotalAmountText:y = std-de + 31
      tTotalCount:y = std-de + 27
      tTotalCountText:y = std-de + 31
      tTotalUnapplied:y = std-de + 53
      tTotalUnappliedText:y = std-de + 57
      tTotalApplied:y = std-de + 53
      tTotalAppliedText:y = std-de + 57
      .
      
    /* resize the column */
    {lib/resize-column.i &col="'fileNumber,batchID'" &var=dBrwInvColumn}
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData wAccounting 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Closes the queries for the browsers and resets the filters 
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      bExport:sensitive = false
      tFilterInvoices:read-only = true
      bFilterInvoicesGo:sensitive = false
      bFilterInvoicesClear:sensitive = false
      chkUnapplied:sensitive = false
      .
    /* close the docs window */
    closeDocsWindow().
    /* browse for checks */
    close query brwPayments.
    empty temp-table agentpayment. 
    /* browse for invoices */
    close query brwInvoices.
    empty temp-table agentinvoice.
  end.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION closeDocsWindow wAccounting 
FUNCTION closeDocsWindow RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if valid-handle(hDocs)
   then
    do:
      run CloseWindow in hDocs.
      hDocs = ?.
    end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getYearCombo wAccounting 
FUNCTION getYearCombo RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Sets the year combo
------------------------------------------------------------------------------*/
  def buffer period for period.
  def var cYearList as char.

  for each period 
  break by period.periodYear descending:
    if first-of(period.periodYear)
     then cYearList = addDelimiter(cYearList,",") + string(period.periodYear).
  end.
  if cYearList = ""
   then cYearList = string(year(today)).
  RETURN cYearList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus wAccounting 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Sets the progress bar back to the minimum value
------------------------------------------------------------------------------*/
  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus wAccounting 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT) :
/*------------------------------------------------------------------------------
@description: Sets the progress bar to pPercentage
------------------------------------------------------------------------------*/
  {&WINDOW-NAME}:move-to-top().
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0
     then pause pPauseSeconds no-message.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitizeReportButtons wAccounting 
FUNCTION sensitizeReportButtons RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
@description Enables or disables the buttons
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
    assign
      std-lo = can-find(first agentinvoice)
      tStateID:sensitive = pEnable
      tAgentID:sensitive = pEnable
      tFromMonth:sensitive = pEnable
      tFromYear:sensitive = pEnable
      tToMonth:sensitive = pEnable
      tToYear:sensitive = pEnable
      bGo:sensitive = pEnable
      bDocs:sensitive = pEnable and tAgentID:screen-value <> "None"
      bExport:sensitive = (pEnable and std-lo)
      tFilterInvoices:read-only = not (pEnable and std-lo)
      bFilterInvoicesGo:sensitive = (pEnable and std-lo)
      bFilterInvoicesClear:sensitive = (pEnable and std-lo)
      chkUnapplied:sensitive = (pEnable and std-lo)
      .
  end.

  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

