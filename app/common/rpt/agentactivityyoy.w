&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Activity
@description Report to show all of the agent activities

@author John Oliver
@version 1.0
@created 02/15/2018
@notes
@modification
date         Name           Description
06/09/2022   SA             Task 93485 - Changed logic to get regionDesc
                            from "sysprop" table to "SysCode"table.
---------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */
CREATE WIDGET-POOL.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/brw-totalData-def.i}

/* for the column width on resize */
define variable dColumnWidth as decimal no-undo.

/* the years list */
define variable cYearList as character no-undo initial "".

/* Temp Table Definitions ---                                           */
{tt/agent.i}
{tt/state.i}
{tt/period.i}
{tt/activityyoy.i &tableAlias="data"}
{tt/activityyoy.i &tableAlias="initdata"}

define temp-table showdata like data
  field showLevel as integer   {&nodeType}
  field showID    as character {&nodeType} column-label "Agent"      format "x(500)"
  field showName  as character {&nodeType} column-label "Agent Name" format "x(500)"
  .
  
/* temp table for exporting and the details */
define temp-table exportdata like showdata.
define temp-table tempdata   like showdata.

/* Function Definitions ---                                             */
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/count-rows.i}
{lib/getstatename.i}
{lib/groupby-def.i &group-column-fields="stateID,statDesc,managerDesc" &lowestDetailName="name"}
{lib/groupby-formula.i &type="percent" &numerator="actualDifference" &denominator="actualThruMonthPrev" &outcome="actualPercentage"}
{lib/groupby-formula.i &type="percent" &numerator="actualThruMonthCurr" &denominator="plannedThruMonthCurr" &outcome="actualToPlanPercent" &onError=100}
{lib/groupby-formula.i &type="percent" &numerator="actualThruMonthCurr" &denominator="plannedYearTotalCurr" &outcome="actualThruMonthCurrToPlannedYear" &onError=100}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES showdata

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData showdata.showID showdata.showName showdata.stateID showdata.statDesc showdata.managerDesc showdata.regionDesc showdata.actualThruMonthPrev showdata.actualThruMonthCurr showdata.actualDifference showdata.actualPercentage showdata.plannedThruMonthCurr showdata.actualToPlanDiff showdata.actualToPlanPercent showdata.plannedYearTotalCurr showdata.actualThruMonthCurrToPlannedYear   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH showdata
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH showdata.
&Scoped-define TABLES-IN-QUERY-brwData showdata
&Scoped-define FIRST-TABLE-IN-QUERY-brwData showdata


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData tStateID tCategory tAgentID ~
tStartYear tEndYear fGroupBy fMonth fStatus fManager bGo RECT-36 RECT-38 ~
fExpandAll fCollapseAll fExpandLabel 
&Scoped-Define DISPLAYED-OBJECTS tStateID tCategory tAgentID tStartYear ~
tEndYear fGroupBy fMonth fStatus fManager fExpandLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setHeaders C-Win 
FUNCTION setHeaders RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh agents for the selected criteria".

DEFINE BUTTON fCollapseAll 
     LABEL "Collapse" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON fExpandAll 
     LABEL "Expand" 
     SIZE 7.2 BY 1.71.

DEFINE VARIABLE fGroupBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Group By" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "State","S",
                     "Company","C",
                     "Manager","M",
                     "Agent","A"
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE fMonth AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "Evaluate Thru" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Compare To" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 11 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tStartYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Base Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 11 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fExpandLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Expand:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 116 BY 3.57.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 106.4 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      showdata SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      showdata.showID width 10
      showdata.showName width 20
      showdata.stateID width 8
      showdata.statDesc width 12
      showdata.managerDesc width 20
      showdata.regionDesc width 15
      showdata.actualThruMonthPrev width 18
      showdata.actualThruMonthCurr width 18
      showdata.actualDifference width 18
      showdata.actualPercentage width 18
      showdata.plannedThruMonthCurr width 18
      showdata.actualToPlanDiff width 18
      showdata.actualToPlanPercent width 18
      showdata.plannedYearTotalCurr width 18
      showdata.actualThruMonthCurrToPlannedYear width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 266 BY 20.71 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 5.29 COL 2 WIDGET-ID 200
     bExport AT ROW 2.19 COL 107.6 WIDGET-ID 2 NO-TAB-STOP 
     tStateID AT ROW 2.19 COL 10 COLON-ALIGNED WIDGET-ID 166
     tCategory AT ROW 2.19 COL 42 COLON-ALIGNED WIDGET-ID 300
     tAgentID AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 84
     tStartYear AT ROW 2.19 COL 84.6 COLON-ALIGNED WIDGET-ID 168
     tEndYear AT ROW 3.38 COL 84.6 COLON-ALIGNED WIDGET-ID 356
     fGroupBy AT ROW 2.19 COL 131.8 COLON-ALIGNED WIDGET-ID 338
     fMonth AT ROW 3.38 COL 131.8 COLON-ALIGNED WIDGET-ID 360
     fStatus AT ROW 2.19 COL 163.8 COLON-ALIGNED WIDGET-ID 362
     fManager AT ROW 3.38 COL 163.8 COLON-ALIGNED WIDGET-ID 368
     bGo AT ROW 2.19 COL 100.2 WIDGET-ID 4 NO-TAB-STOP 
     fExpandAll AT ROW 2.43 COL 205 WIDGET-ID 316
     fCollapseAll AT ROW 2.43 COL 213 WIDGET-ID 318
     fExpandLabel AT ROW 2.95 COL 194.8 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 119 WIDGET-ID 60
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-38 AT ROW 1.48 COL 117.6 WIDGET-ID 326
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 267.8 BY 25.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Activity - Year Over Year"
         HEIGHT             = 25.19
         WIDTH              = 267.8
         MAX-HEIGHT         = 47.29
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.29
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH showdata.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4
       COLUMN          = 100.2
       HEIGHT          = .48
       WIDTH           = 14.4
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Activity - Year Over Year */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Activity - Year Over Year */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Activity - Year Over Year */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  do with frame {&frame-name}:
    if tStartYear:input-value = tEndYear:input-value
     then 
      do:
        message "Start Year and End Year must be different" view-as alert-box.
        return.
    end.
    
    if tStartYear:input-value > tEndYear:input-value
     then 
      do:
        message "Start Year must be before End Year" view-as alert-box.
        return.
    end.
    
    run getData in this-procedure.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available showdata
   then return.
  
  define variable tShowID      as character no-undo.
  define variable tRowID       as rowid     no-undo.
  define variable tPosition    as integer   no-undo.
  define variable tWhereClause as character no-undo.
  
  define buffer browsedata     for showdata.
  
  /* get row data and position */
  tShowID = showdata.showID.
  do tPosition = 1 to {&browse-name}:num-iterations:
     if {&browse-name}:is-row-selected(tPosition) 
      then leave.
  end.
  
  run filterData in this-procedure (output tWhereClause).
  {lib/groupby-detail.i &groupID=showdata.showID &whereClause=tWhereClause &useClick=true}
  case fGroupBy:screen-value:
   when "S" then {lib/groupby-column-value.i &dataField=stateID &whereClause=tWhereClause}
   when "C" then {lib/groupby-column-value.i &dataField=corporationID &whereClause=tWhereClause}
   when "M" then {lib/groupby-column-value.i &dataField=manager &whereClause=tWhereClause}
   when "A" then {lib/groupby-column-value.i &dataField=agentID &whereClause=tWhereClause}
  end case.
  
  /* reposition row */
  for first browsedata no-lock
      where browsedata.showID = tShowID:
    
    tRowID = rowid(browsedata).
  end.
  if tRowID <> ? and tPosition > 0
   then
    do:
      {&browse-name}:set-repositioned-row(tPosition, "ALWAYS") no-error.
      reposition {&browse-name} to rowid tRowID no-error.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/groupby-detail-rowDisplay.i}
  /* set group name if company and group is agent ID */
  if fGroupBy:screen-value in frame {&frame-name} = "C" and showdata.showName = showdata.agentID
   then showdata.showName:screen-value = showdata.name. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/groupby-detail-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCollapseAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCollapseAll C-Win
ON CHOOSE OF fCollapseAll IN FRAME fMain /* Collapse */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = dynamic-function("doFilterSort").
  for each tempdata no-lock:
    {lib/groupby-detail.i &groupID=tempdata.showID &whereClause=tWhereClause &expandAll=false}
  end.
  run sortData in this-procedure ("").
  case fGroupBy:screen-value:
   when "S" then {lib/groupby-column-value.i &dataField=stateID &whereClause=tWhereClause}
   when "C" then {lib/groupby-column-value.i &dataField=corporationID &whereClause=tWhereClause}
   when "M" then {lib/groupby-column-value.i &dataField=manager &whereClause=tWhereClause}
   when "A" then {lib/groupby-column-value.i &dataField=agentID &whereClause=tWhereClause}
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fExpandAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fExpandAll C-Win
ON CHOOSE OF fExpandAll IN FRAME fMain /* Expand */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = dynamic-function("doFilterSort").
  for each tempdata no-lock:
    {lib/groupby-detail.i &groupID=tempdata.showID &whereClause=tWhereClause &expandAll=true}
  end.
  run sortData in this-procedure ("").
  case fGroupBy:screen-value:
   when "S" then {lib/groupby-column-value.i &dataField=stateID &whereClause=tWhereClause}
   when "C" then {lib/groupby-column-value.i &dataField=corporationID &whereClause=tWhereClause}
   when "M" then {lib/groupby-column-value.i &dataField=manager &whereClause=tWhereClause}
   when "A" then {lib/groupby-column-value.i &dataField=agentID &whereClause=tWhereClause}
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fGroupBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fGroupBy C-Win
ON VALUE-CHANGED OF fGroupBy IN FRAME fMain /* Group By */
DO:
  dynamic-function("clearStatus").
  define variable tWhereClause as character no-undo.
  run filterData in this-procedure (output tWhereClause).
  case self:screen-value:
   when "S" then {lib/groupby.i &firstField=stateID &groupName=stateName &columnLabel="'State Name'" &whereClause=tWhereClause}
   when "C" then {lib/groupby.i &firstField=corporationID &groupName=corporationID &columnLabel="'Company'" &whereClause=tWhereClause}
   when "M" then {lib/groupby.i &firstField=manager &groupName=managerDesc &columnLabel="'Manager'" &whereClause=tWhereClause}
   when "A" then {lib/groupby.i &firstField=agentID &groupName=name &columnLabel="'Agent Name'" &whereClause=tWhereClause}
  end case.
  /* copy the showdata into tempdata */
  empty temp-table tempdata.
  for each showdata no-lock:
    create tempdata.
    buffer-copy showdata to tempdata.
  end.
  /* disable the expand/collapse buttons */
  if self:screen-value = "A"
   then std-lo = false.
   else std-lo = true.
   
  assign
    fExpandAll:sensitive   = std-lo
    fCollapseAll:sensitive = std-lo
    lGroupDetailSortAllow  = not std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMonth C-Win
ON VALUE-CHANGED OF fMonth IN FRAME fMain /* Evaluate Thru */
DO:
  setHeaders().
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  apply "VALUE-CHANGED" to fGroupBy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCategory C-Win
ON VALUE-CHANGED OF tCategory IN FRAME fMain /* Category */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEndYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEndYear C-Win
ON VALUE-CHANGED OF tEndYear IN FRAME fMain /* Compare To */
DO:
  if self:input-value = year(today)
   then fMonth:screen-value in frame {&frame-name} = string(month(today)).
   else fMonth:screen-value in frame {&frame-name} = string(12).
  setHeaders().
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStartYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStartYear C-Win
ON VALUE-CHANGED OF tStartYear IN FRAME fMain /* Base Year */
DO:
  setHeaders().
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "ActivityChanged" anywhere.

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i &entity="'Agent'"}
{lib/brw-main.i}
{lib/set-buttons.i}
{lib/set-filters.i &tableName="'showdata'" &labelName="'Status,Manager'" &columnName="'stat,manager'" &include="'GroupBy,Month,CollapseAll,ExpandAll'"}
{lib/report-progress-bar.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

fExpandAll:load-image("images/expand.bmp").
fExpandAll:load-image-insensitive("images/expand-i.bmp").
fCollapseAll:load-image("images/collapse.bmp").
fCollapseAll:load-image-insensitive("images/collapse-i.bmp").

/* fill the year list */
publish "GetActivityYearList" (output cYearList).
if cYearList = ""
 then run server/getagentactivityyears.p (output cYearList, output std-lo, output std-ch).
if cYearList = ""
 then cYearList = string(year(today)).
tStartYear:list-items = cYearList.
tEndYear:list-items = cYearList.
 
/* get the categories */
{lib/get-sysprop-list.i &combo=tCategory &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Category'"}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=fMonth &noYear=true}
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}
    /* set the values */
  
    /* get the column width */
    {lib/get-column-width.i &col="'showName'" &var=dColumnWidth}
    
    std-in = year(today) - 1.
    if lookup(string(std-in), tStartYear:list-items) > 0
     then tStartYear:screen-value = string(std-in).
     else tStartYear:screen-value = entry(1, tStartYear:list-items).
    
    std-in = year(today).
    if lookup(string(std-in), tEndYear:list-items) > 0
     then tEndYear:screen-value = string(std-in).
     else tEndYear:screen-value = entry(1, tEndYear:list-items).
    apply "VALUE-CHANGED" to tEndYear.
    
    fGroupBy:screen-value = "A".
  end.
  
  clearData().
  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "agentactivityyoy.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "agentactivityyoy.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tStateID tCategory tAgentID tStartYear tEndYear fGroupBy fMonth 
          fStatus fManager fExpandLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData tStateID tCategory tAgentID tStartYear tEndYear fGroupBy 
         fMonth fStatus fManager bGo RECT-36 RECT-38 fExpandAll fCollapseAll 
         fExpandLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufferdata for showdata.
  define buffer exportdata for exportdata.
  
  apply "CHOOSE" to fCollapseAll in frame {&frame-name}.

  empty temp-table exportdata.
  for each bufferdata no-lock:
    create exportdata.
    buffer-copy bufferdata to exportdata.
  end.

  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "agent_ltd"
  
  /* replace the sequence numbers with blanks */
  close query {&browse-name}.
  for each bufferdata exclusive-lock:
    if fGroupBy:screen-value = "C" or fGroupBy:screen-value = "M"
     then bufferdata.showID = "".
    if bufferdata.showName = bufferdata.agentID
     then bufferdata.showName = bufferdata.name.
  end.
  open query {&browse-name} for each showdata.

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
  /* set the original data */
  close query {&browse-name}.
  empty temp-table bufferdata.
  for each exportdata exclusive-lock:
    create bufferdata.
    buffer-copy exportdata to bufferdata.
  end.
  open query {&browse-name} for each showdata.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pWhereClause as character no-undo.
  
  define variable iMonth as integer no-undo.
  define buffer initdata for initdata.
  define buffer data for data.
  
  pWhereClause = doFilterSort().
  
  /* get the first columns we need to calculate the other columns */
  empty temp-table data.
  iMonth = fMonth:input-value in frame {&frame-name}.
  for each initdata exclusive-lock
  break by initdata.agentID:
    
    if first-of(initdata.agentID)
     then
      do:
        create data.
        buffer-copy initdata to data.
      end.

    for first data exclusive-lock
        where data.agentID = initdata.agentID:
      
      case initdata.type:
       when "P" or when "I" then
        assign
          data.plannedYearTotalCurr = initdata.currMonth1
                                    + initdata.currMonth2
                                    + initdata.currMonth3
                                    + initdata.currMonth4
                                    + initdata.currMonth5
                                    + initdata.currMonth6
                                    + initdata.currMonth7
                                    + initdata.currMonth8
                                    + initdata.currMonth9
                                    + initdata.currMonth10
                                    + initdata.currMonth11
                                    + initdata.currMonth12
          data.plannedThruMonthCurr = (if iMonth >= 1  then initdata.currMonth1  else 0)
                                    + (if iMonth >= 2  then initdata.currMonth2  else 0)
                                    + (if iMonth >= 3  then initdata.currMonth3  else 0)
                                    + (if iMonth >= 4  then initdata.currMonth4  else 0)
                                    + (if iMonth >= 5  then initdata.currMonth5  else 0)
                                    + (if iMonth >= 6  then initdata.currMonth6  else 0)
                                    + (if iMonth >= 7  then initdata.currMonth7  else 0)
                                    + (if iMonth >= 8  then initdata.currMonth8  else 0)
                                    + (if iMonth >= 9  then initdata.currMonth9  else 0)
                                    + (if iMonth >= 10 then initdata.currMonth10 else 0)
                                    + (if iMonth >= 11 then initdata.currMonth11 else 0)
                                    + (if iMonth >= 12 then initdata.currMonth12 else 0)
                                    .
       when "A" or when "R" then
        assign
          data.actualThruMonthPrev  = (if iMonth >= 1  then initdata.prevMonth1  else 0)
                                    + (if iMonth >= 2  then initdata.prevMonth2  else 0)
                                    + (if iMonth >= 3  then initdata.prevMonth3  else 0)
                                    + (if iMonth >= 4  then initdata.prevMonth4  else 0)
                                    + (if iMonth >= 5  then initdata.prevMonth5  else 0)
                                    + (if iMonth >= 6  then initdata.prevMonth6  else 0)
                                    + (if iMonth >= 7  then initdata.prevMonth7  else 0)
                                    + (if iMonth >= 8  then initdata.prevMonth8  else 0)
                                    + (if iMonth >= 9  then initdata.prevMonth9  else 0)
                                    + (if iMonth >= 10 then initdata.prevMonth10 else 0)
                                    + (if iMonth >= 11 then initdata.prevMonth11 else 0)
                                    + (if iMonth >= 12 then initdata.prevMonth12 else 0)
          data.actualThruMonthCurr  = (if iMonth >= 1  then initdata.currMonth1  else 0)
                                    + (if iMonth >= 2  then initdata.currMonth2  else 0)
                                    + (if iMonth >= 3  then initdata.currMonth3  else 0)
                                    + (if iMonth >= 4  then initdata.currMonth4  else 0)
                                    + (if iMonth >= 5  then initdata.currMonth5  else 0)
                                    + (if iMonth >= 6  then initdata.currMonth6  else 0)
                                    + (if iMonth >= 7  then initdata.currMonth7  else 0)
                                    + (if iMonth >= 8  then initdata.currMonth8  else 0)
                                    + (if iMonth >= 9  then initdata.currMonth9  else 0)
                                    + (if iMonth >= 10 then initdata.currMonth10 else 0)
                                    + (if iMonth >= 11 then initdata.currMonth11 else 0)
                                    + (if iMonth >= 12 then initdata.currMonth12 else 0)
                                    .
      end case.
    end.
  end.
  /* calcuate the other columns */
  for each data exclusive-lock:
    assign
      data.actualDifference = data.actualThruMonthCurr - data.actualThruMonthPrev
      data.actualPercentage = (if data.actualThruMonthPrev = 0 then 0 else data.actualDifference / data.actualThruMonthPrev) * 100
      data.actualToPlanDiff = data.actualThruMonthCurr - data.plannedThruMonthCurr
      data.actualToPlanPercent = (if data.plannedThruMonthCurr = 0 then 1 else data.actualThruMonthCurr / data.plannedThruMonthCurr) * 100
      data.actualThruMonthCurrToPlannedYear = (if data.plannedYearTotalCurr = 0 then 1 else data.actualThruMonthCurr / data.plannedYearTotalCurr) * 100
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  
  define buffer initdata for initdata.
  /* cleanup before getting showactivity */
  clearData().
  empty temp-table initdata.
  dStartTime = now.
  run server/reportagentactivityyoy.p (input tAgentID:input-value in frame {&frame-name},
                                       input tStateID:input-value in frame {&frame-name},
                                       input tStartYear:input-value in frame {&frame-name},
                                       input tEndYear:input-value in frame {&frame-name},
                                       input tCategory:input-value in frame {&frame-name},
                                       output table initdata,
                                       output std-lo,
                                       output std-ch).
  
  if not std-lo
   then message "ReportAgentActivitiesYearOverYear failed: " + std-ch view-as alert-box warning.
   else
    do with frame {&frame-name}:
      run SetProgressStatus.
      for each initdata exclusive-lock:
        /* get the region */
        publish "GetRegion" (initdata.stateID, output initdata.region).
        publish "GetSysCodeDesc" ("Region", initdata.region, output initdata.regionDesc).
        /* get the manager */
        publish "GetSysUserName" (initdata.manager, output initdata.managerDesc).
        /* get the status */
        publish "GetSysPropDesc" ("AMD", "Agent", "Status", initdata.stat, output initdata.statDesc).
        /* get the state */
        initdata.stateName = initdata.stateID.
        initdata.stateName = getStateName(initdata.stateID).
        for first agent no-lock
            where agent.agentID = initdata.agentID:
          
          initdata.name = agent.name.
        end.
      end.
      run SetProgressStatus.
      /* sorting */
      if can-find(first initdata)
       then
        do:
          apply "VALUE-CHANGED" to fGroupBy.
          setFilterCombos("ALL").
        end.
      enableButtons(can-find(first showdata)).
      enableFilters(can-find(first showdata)).
      appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
      displayStatus().
      run SetProgressEnd.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    {lib/brw-sortData.i}
    /* remove the rows that don't match the criteria */
    setStatusCount(num-results("{&browse-name}")).
    if fGroupBy:screen-value in frame {&frame-name} = "A"
     then
      do:
        {lib/brw-totalData.i &excludeColumn="10,13,15" &integerOnly=true}
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  if totalAdded
   then iOffset = 23.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5 - iOffset.
  
  /* resize the column width */
  {lib/resize-column.i &col="'showName,managerDesc'" &var=dColumnWidth}
  
  if fGroupBy:screen-value in frame {&frame-name} = "A"
   then
    do:
      {lib/brw-totalData.i &excludeColumn="10,13,15" &integerOnly=true}
    end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query {&browse-name}.
  empty temp-table data.
  empty temp-table showdata.
  hide message no-pause.
  enableButtons(false).
  enableFilters(false).
  fStatus:screen-value in frame {&frame-name} = "ALL".
  fManager:screen-value in frame {&frame-name} = "ALL".
  fGroupBy:screen-value in frame {&frame-name} = "A".
  {lib/brw-totalData.i &noShow=true}
  clearStatus().
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setHeaders C-Win 
FUNCTION setHeaders RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iStartYear as integer no-undo.
  define variable iEndYear as integer no-undo.
  define variable cCategory as character no-undo.
  define variable iMonth as integer no-undo.
  define variable cLabel as character no-undo.
  
  define variable hBuffer as handle no-undo.
  define variable hField as handle no-undo.
  define variable i as integer no-undo.
  define variable hColumn as handle no-undo.
  
  do with frame {&frame-name}:
    assign
      iStartYear = tStartYear:input-value
      iEndYear = tEndYear:input-value
      cCategory = tCategory:input-value
      iMonth = fMonth:input-value
      .
  end.
  /* set the columns */
  create buffer hBuffer for table "data".
  do i = 1 to hBuffer:num-fields:
    cLabel = "".
    hField = hBuffer:buffer-field(i).
    if valid-handle(hField)
     then
      do:
        case hField:name:
         when "actualThruMonthPrev" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "actualThruMonthCurr" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "actualDifference" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "actualPercentage" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "plannedThruMonthCurr" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "actualToPlanDiff" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "actualToPlanPercent" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "plannedYearTotalCurr" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
         when "actualThruMonthCurrToPlannedYear" then cLabel = substitute(hField:column-label, string(iStartYear), string(iEndYear), substring(GetMonthName(iMonth), 1, 3)).
        end case.
        /* set the column */
        hColumn = getColumn(browse {&browse-name}:handle, hField:name).
        if valid-handle(hColumn) and cLabel > ""
         then hColumn:label = cLabel.
      end.
  end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

