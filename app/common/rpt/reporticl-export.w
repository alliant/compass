&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{tt/reporticl.i &tableAlias="data"}
define input parameter pYear as integer no-undo.
define input parameter table for data.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* Temp Table ---                                                       */
{tt/period.i}

/* Functions ---                                                        */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-41 tYear tMonth tChartRankings ~
tChartNet tChartStateRankings tChartDaily tChartStateByState tChartPurchase ~
tChartAll bExport bCancel 
&Scoped-Define DISPLAYED-OBJECTS tYear tMonth tChartRankings tChartNet ~
tChartStateRankings tChartDaily tChartStateByState tChartPurchase tChartAll 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1",0
     DROP-DOWN-LIST
     SIZE 22.8 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-41
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58 BY 5.71.

DEFINE VARIABLE tChartAll AS LOGICAL INITIAL no 
     LABEL "ALL" 
     VIEW-AS TOGGLE-BOX
     SIZE 9 BY .81 NO-UNDO.

DEFINE VARIABLE tChartDaily AS LOGICAL INITIAL no 
     LABEL "Daily Average" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.

DEFINE VARIABLE tChartNet AS LOGICAL INITIAL no 
     LABEL "Net Year Over Year" 
     VIEW-AS TOGGLE-BOX
     SIZE 23 BY .81 NO-UNDO.

DEFINE VARIABLE tChartPurchase AS LOGICAL INITIAL no 
     LABEL "Purchase Vs. Refinance" 
     VIEW-AS TOGGLE-BOX
     SIZE 27 BY .81 NO-UNDO.

DEFINE VARIABLE tChartRankings AS LOGICAL INITIAL no 
     LABEL "Rankings" 
     VIEW-AS TOGGLE-BOX
     SIZE 12 BY .81 NO-UNDO.

DEFINE VARIABLE tChartStateByState AS LOGICAL INITIAL no 
     LABEL "State By State" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81 NO-UNDO.

DEFINE VARIABLE tChartStateRankings AS LOGICAL INITIAL no 
     LABEL "State Rankings" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tYear AT ROW 1.48 COL 7 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tMonth AT ROW 1.48 COL 36 COLON-ALIGNED WIDGET-ID 28
     tChartRankings AT ROW 3.86 COL 6 WIDGET-ID 6
     tChartNet AT ROW 3.86 COL 31 WIDGET-ID 14
     tChartStateRankings AT ROW 5.05 COL 6 WIDGET-ID 8
     tChartDaily AT ROW 5.05 COL 31 WIDGET-ID 30
     tChartStateByState AT ROW 6.24 COL 6 WIDGET-ID 10
     tChartPurchase AT ROW 6.24 COL 31 WIDGET-ID 16
     tChartAll AT ROW 7.43 COL 6 WIDGET-ID 18
     bExport AT ROW 9.1 COL 16 WIDGET-ID 22
     bCancel AT ROW 9.1 COL 33 WIDGET-ID 24
     "Select Charts" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 2.91 COL 4 WIDGET-ID 4
     RECT-41 AT ROW 3.14 COL 3 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 62 BY 9.91 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Select Charts"
         HEIGHT             = 9.91
         WIDTH              = 62
         MAX-HEIGHT         = 16
         MAX-WIDTH          = 112.4
         VIRTUAL-HEIGHT     = 16
         VIRTUAL-WIDTH      = 112.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */

ASSIGN 
       tYear:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Select Charts */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Select Charts */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME DEFAULT-FRAME /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  std-ch = "".
  if tChartRankings:checked
   then std-ch = addDelimiter(std-ch, ",") + "2".
  if tChartStateRankings:checked
   then std-ch = addDelimiter(std-ch, ",") + "3".
  if tChartStateByState:checked
   then std-ch = addDelimiter(std-ch, ",") + "4".
  if tChartNet:checked
   then std-ch = addDelimiter(std-ch, ",") + "5".
  if tChartDaily:checked
   then std-ch = addDelimiter(std-ch, ",") + "6".
  if tChartPurchase:checked
   then std-ch = addDelimiter(std-ch, ",") + "7".
  if tChartALL:checked
   then std-ch = "ALL".

  run rpt/reporticl-export-proc.p (table data, std-ch, tYear:input-value, tMonth:input-value).
  apply "CHOOSE" to bCancel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tChartAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tChartAll C-Win
ON VALUE-CHANGED OF tChartAll IN FRAME DEFAULT-FRAME /* ALL */
DO:
  assign
    std-lo = self:checked
    tChartRankings:checked = std-lo
    tChartStateRankings:checked = std-lo
    tChartStateByState:checked = std-lo
    tChartDaily:checked = std-lo
    tChartNet:checked = std-lo
    tChartPurchase:checked = std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-period-list.i &mth=tMonth &noYear=true}
  assign
    tYear:screen-value = string(pYear)
    tMonth:screen-value = if pYear = year(today) then string(month(add-interval(today, -1, "month"))) else "12"
    .

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tYear tMonth tChartRankings tChartNet tChartStateRankings tChartDaily 
          tChartStateByState tChartPurchase tChartAll 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-41 tYear tMonth tChartRankings tChartNet tChartStateRankings 
         tChartDaily tChartStateByState tChartPurchase tChartAll bExport 
         bCancel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

