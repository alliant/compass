&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Temp Table Definitions ---                                           */
{tt/batchforperiod.i &tableAlias="data"}
{tt/agent.i}
{tt/period.i}

/* Local Variable Definitions ---                                       */
define variable dColumnWidth as decimal no-undo.
{lib/std-def.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName=data}
{lib/brw-totalData-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.stateDesc data.agentID data.agentName data.agentStatDesc data.agentActiveDate data.batchID data.statDesc data.netPremiumReported data.netPremiumDelta data.grossPremiumDelta data.retainedPremiumDelta   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each data
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 bGo bExport bFilterClear ~
tPeriodMonth tPeriodYear fStateID fBatchStatus fAgentStatus brwData 
&Scoped-Define DISPLAYED-OBJECTS tPeriodMonth tPeriodYear fStateID ~
fBatchStatus fAgentStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bFilterClear 
     LABEL "FilterClear" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bGo 
     LABEL "Go" 
     SIZE 7.2 BY 1.71.

DEFINE VARIABLE fAgentStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Agent Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fBatchStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Batch Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tPeriodMonth AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1",0
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tPeriodYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "   0" 
     DROP-DOWN-LIST
     SIZE 12.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62 BY 3.81.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 126.4 BY 3.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.stateDesc width 15
data.agentID width 10
data.agentName width 24
data.agentStatDesc width 14
data.agentActiveDate width 15
data.batchID width 12
data.statDesc width 21
  data.netPremiumReported width 16
  data.netPremiumDelta width 16
  data.grossPremiumDelta width 16
  data.retainedPremiumDelta width 16
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 188 BY 17.71 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bGo AT ROW 2.43 COL 44 WIDGET-ID 12
     bExport AT ROW 2.43 COL 51.4 WIDGET-ID 92
     bFilterClear AT ROW 2.43 COL 179 WIDGET-ID 104
     tPeriodMonth AT ROW 2.81 COL 10 COLON-ALIGNED WIDGET-ID 6
     tPeriodYear AT ROW 2.81 COL 28 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     fStateID AT ROW 2.81 COL 71 COLON-ALIGNED WIDGET-ID 10
     fBatchStatus AT ROW 2.81 COL 108 COLON-ALIGNED WIDGET-ID 100
     fAgentStatus AT ROW 2.81 COL 153 COLON-ALIGNED WIDGET-ID 106
     brwData AT ROW 5.43 COL 2 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 4
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 64.6 WIDGET-ID 96
     RECT-1 AT ROW 1.48 COL 2 WIDGET-ID 2
     RECT-2 AT ROW 1.48 COL 63.6 WIDGET-ID 94
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 190 BY 22.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Batches for Period"
         HEIGHT             = 22.33
         WIDTH              = 190
         MAX-HEIGHT         = 24.57
         MAX-WIDTH          = 194.4
         VIRTUAL-HEIGHT     = 24.57
         VIRTUAL-WIDTH      = 194.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fAgentStatus fMain */
ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Batches for Period */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Batches for Period */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Batches for Period */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* FilterClear */
DO:
  assign
    fStateID:screen-value     = "ALL"
    fBatchStatus:screen-value = "ALL"
    fAgentStatus:screen-value = "ALL"
    .
  setFilterCombos("ALL").
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available data or data.batchID = 0
   then return.
    
  publish "SetCurrentValue" ("BatchID", string(data.batchID)).
  run rpt/batchdetails.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgentStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgentStatus C-Win
ON VALUE-CHANGED OF fAgentStatus IN FRAME fMain /* Agent Status */
DO:
  setFilterCombos(self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fBatchStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fBatchStatus C-Win
ON VALUE-CHANGED OF fBatchStatus IN FRAME fMain /* Batch Status */
DO:
  setFilterCombos(self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStateID C-Win
ON VALUE-CHANGED OF fStateID IN FRAME fMain /* State */
DO:
  setFilterCombos(self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPeriodMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPeriodMonth C-Win
ON VALUE-CHANGED OF tPeriodMonth IN FRAME fMain /* Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPeriodYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPeriodYear C-Win
ON VALUE-CHANGED OF tPeriodYear IN FRAME fMain
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/win-status.i &entity="'Agent Batch'" &entityPlural="'Agent Batches'"}
{lib/set-filter.i &label="StateID"      &id="stateID"   &value="stateDesc"}
{lib/set-filter.i &label="Batch Status" &id="stat"      &value="statDesc"}
{lib/set-filter.i &label="Agent Status" &id="agentStat" &value="agentStatDesc"}
{lib/set-button.i &label="Go"           &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="FilterClear"}
setButtons().

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.


{&window-name}:window-state = 2.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

publish "GetPeriods" (output table period).
temp-table period:write-xml("file","period.xml").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  {lib/get-period-list.i &mth=tPeriodMonth &yr=tPeriodYear}

  {lib/get-column-width.i &col="'agentName'" &var=dColumnWidth}
  run windowResized in this-procedure.
  clearData().
  {&window-name}:window-state = 3.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tPeriodMonth tPeriodYear fStateID fBatchStatus fAgentStatus 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 bGo bExport bFilterClear tPeriodMonth tPeriodYear 
         fStateID fBatchStatus fAgentStatus brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query {&browse-name}:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Batches for Period"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tPeriodID  as integer  no-undo.
  define variable tStartTime as datetime no-undo.

  clearData().
  empty temp-table data.
  do with frame {&frame-name}:
    for first period no-lock
        where period.periodMonth = tPeriodMonth:input-value
          and period.periodYear = tPeriodYear:input-value:

      tPeriodID = period.periodID.
    end.
  end.
  

  tStartTime = now.
  run server/getbatchforperiod.p (input  tPeriodID,
                                  output table data,
                                  output std-lo,
                                  output std-ch
                                  ).

  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   else
    do:
      setFilterCombos("ALL").
      apply "CHOOSE" to bFilterClear.
      dataSortBy = "".
      run sortData in this-procedure("agentName").
      setStatusTime(num-results("{&browse-name}"), tStartTime).
    end.

  std-lo = can-find(first data).
  enableFilters(std-lo).
  enableButtons(std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  tWhereClause = getWhereClause().

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +" &post-by-clause="+ ' by agentName'"}
  {lib/brw-totalData.i &excludeColumn="2,6"}
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as integer no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  if totalAdded
   then iOffset = 23.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - (5 + iOffset).
  /* resize the column */
  {lib/resize-column.i &col="'agentName'" &var=dColumnWidth}
  
  {lib/brw-totalData.i &excludeColumn="2,5"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table data.
  close query {&browse-name}.
  clearStatus().
  setFilterCombos("ALL").
  enableFilters(false).
  enableButtons(false).
  {lib/brw-totalData.i &noShow=true}
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

