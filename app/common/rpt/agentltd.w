&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name LTD Metrics
@description Displays metrics on the life to date of an agent

@author John Oliver
@version 1.0
@created 2017/09/26
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* variables */
{lib/std-def.i}
{lib/brw-totalData-def.i}

/* define the column width */
define variable dColumnWidth as decimal no-undo.

/* temp tables */
{tt/agent.i}
{tt/state.i}
{tt/reportagentltd.i &tableAlias="data"}

/* temp table for showing the data once grouped */
define temp-table showdata like data
  field showLevel as integer   {&nodeType}
  field showID    as character {&nodeType} column-label "Agent"      format "x(500)"
  field showName  as character {&nodeType} column-label "Agent Name" format "x(500)"
  .
  
/* temp table for exporting and showing the details */
define temp-table tempdata   like showdata.
define temp-table exportdata like showdata.

/* functions */
{lib/getstatename.i}
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/groupby-def.i &group-column-fields="stateID,statDesc,managerDesc,agentDateShow" &lowestDetailName="name"}
{lib/groupby-formula.i &type="percent" &numerator="costsIncurred" &denominator="netPremium" &outcome="netPercent"}

 /*&group-column-fields="'stateID,statDesc,managerDesc,agentDateShow'"*/
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES showdata

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData showdata.showID showdata.showName showdata.statDesc showdata.managerDesc showdata.agentDateShow showdata.numPolicies showdata.numForms showdata.numClaims showdata.netPremium showdata.costsIncurred showdata.netPercent showdata.agentLTD   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH showdata
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH showdata.
&Scoped-define TABLES-IN-QUERY-brwData showdata
&Scoped-define FIRST-TABLE-IN-QUERY-brwData showdata


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo RECT-36 RECT-37 fStatus fManager ~
fGroupBy tStateID fExpandAll fCollapseAll fAgentID brwData fExpandLabel 
&Scoped-Define DISPLAYED-OBJECTS fStatus fManager fGroupBy tStateID ~
fAgentID fExpandLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fetch data".

DEFINE BUTTON fCollapseAll 
     LABEL "Collapse" 
     SIZE 4.6 BY 1.1.

DEFINE BUTTON fExpandAll 
     LABEL "Expand" 
     SIZE 4.6 BY 1.1.

DEFINE VARIABLE fAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN AUTO-COMPLETION
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE fGroupBy AS CHARACTER FORMAT "X(256)":U INITIAL "A" 
     LABEL "Group By" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "State","S",
                     "Company","C",
                     "Manager","M",
                     "Agent","A"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fExpandLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Expand:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57 BY 3.57.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 105.4 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      showdata SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      showdata.showID width 10
showdata.showName width 26
showdata.statDesc width 12
showdata.managerDesc width 25
showdata.agentDateShow width 15
showdata.numPolicies width 12
showdata.numForms width 12
showdata.numClaims width 12
showdata.netPremium width 15
showdata.costsIncurred width 15
showdata.netPercent width 15
showdata.agentLTD width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 200 BY 20.71 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.29 COL 45.6 WIDGET-ID 2 NO-TAB-STOP 
     bGo AT ROW 2.29 COL 38.2 WIDGET-ID 4 NO-TAB-STOP 
     fStatus AT ROW 2.19 COL 67.6 COLON-ALIGNED WIDGET-ID 320
     fManager AT ROW 2.19 COL 103.6 COLON-ALIGNED WIDGET-ID 300
     fGroupBy AT ROW 2.19 COL 144 COLON-ALIGNED WIDGET-ID 312
     tStateID AT ROW 2.91 COL 10 COLON-ALIGNED WIDGET-ID 166
     fExpandAll AT ROW 3.33 COL 145.8 WIDGET-ID 316
     fCollapseAll AT ROW 3.33 COL 150.8 WIDGET-ID 318
     fAgentID AT ROW 3.38 COL 67.6 COLON-ALIGNED WIDGET-ID 84
     brwData AT ROW 5.29 COL 2 WIDGET-ID 200
     fExpandLabel AT ROW 3.57 COL 135.6 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 59.6 WIDGET-ID 310
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 58.6 WIDGET-ID 308
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 202 BY 25.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Activity by Agent - Life to Date"
         HEIGHT             = 25.19
         WIDTH              = 202
         MAX-HEIGHT         = 29.43
         MAX-WIDTH          = 218.2
         VIRTUAL-HEIGHT     = 29.43
         VIRTUAL-WIDTH      = 218.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fAgentID fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH showdata.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4.1
       COLUMN          = 38.2
       HEIGHT          = .48
       WIDTH           = 14.6
       TAB-STOP        = no
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = no.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Activity by Agent - Life to Date */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Activity by Agent - Life to Date */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Activity by Agent - Life to Date */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  clearData().
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available showdata
   then return.
   
  define variable tShowID      as character no-undo.
  define variable tRowID       as rowid     no-undo.
  define variable tPosition    as integer   no-undo.
  define variable tWhereClause as character no-undo.
  
  define buffer browsedata     for showdata.
  
  /* get row data and position */
  tShowID = showdata.showID.
  do tPosition = 1 to {&browse-name}:num-iterations:
     if {&browse-name}:is-row-selected(tPosition) 
      then leave.
  end.
  
  DoWait(true).
  tWhereClause = dynamic-function("doFilterSort").
  {lib/groupby-detail.i &groupID=showdata.showID &whereClause=tWhereClause &useClick=true}
  case fGroupBy:screen-value:
   when "S" then {lib/groupby-column-value.i &dataField=stateID &whereClause=tWhereClause}
   when "C" then {lib/groupby-column-value.i &dataField=corporationID &whereClause=tWhereClause}
   when "M" then {lib/groupby-column-value.i &dataField=manager &whereClause=tWhereClause}
   when "A" then {lib/groupby-column-value.i &dataField=agentID &whereClause=tWhereClause}
  end case.
  DoWait(false).
  
  /* reposition row */
  for first browsedata no-lock
      where browsedata.showID = tShowID:
    
    tRowID = rowid(browsedata).
  end.
  if tRowID <> ? and tPosition > 0
   then
    do:
      {&browse-name}:set-repositioned-row(tPosition, "ALWAYS") no-error.
      reposition {&browse-name} to rowid tRowID no-error.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/groupby-detail-rowDisplay.i}
  if showdata.showName = showdata.agentID
   then showdata.showName:screen-value = showdata.name.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/groupby-detail-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgentID C-Win
ON VALUE-CHANGED OF fAgentID IN FRAME fMain /* Agent */
DO:
  apply "VALUE-CHANGED" to fGroupBy in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCollapseAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCollapseAll C-Win
ON CHOOSE OF fCollapseAll IN FRAME fMain /* Collapse */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = dynamic-function("doFilterSort").
  for each tempdata no-lock:
    {lib/groupby-detail.i &groupID=tempdata.showID &whereClause=tWhereClause &expandAll=false}
  end.
  run sortData in this-procedure ("").
  case fGroupBy:screen-value:
   when "S" then {lib/groupby-column-value.i &dataField=stateID &whereClause=tWhereClause}
   when "C" then {lib/groupby-column-value.i &dataField=corporationID &whereClause=tWhereClause}
   when "M" then {lib/groupby-column-value.i &dataField=manager &whereClause=tWhereClause}
   when "A" then {lib/groupby-column-value.i &dataField=agentID &whereClause=tWhereClause}
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fExpandAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fExpandAll C-Win
ON CHOOSE OF fExpandAll IN FRAME fMain /* Expand */
DO:
  define variable tWhereClause as character no-undo.
  tWhereClause = dynamic-function("doFilterSort").
  for each tempdata no-lock:
    {lib/groupby-detail.i &groupID=tempdata.showID &whereClause=tWhereClause &expandAll=true}
  end.
  run sortData in this-procedure ("").
  case fGroupBy:screen-value:
   when "S" then {lib/groupby-column-value.i &dataField=stateID &whereClause=tWhereClause}
   when "C" then {lib/groupby-column-value.i &dataField=corporationID &whereClause=tWhereClause}
   when "M" then {lib/groupby-column-value.i &dataField=manager &whereClause=tWhereClause}
   when "A" then {lib/groupby-column-value.i &dataField=agentID &whereClause=tWhereClause}
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fGroupBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fGroupBy C-Win
ON VALUE-CHANGED OF fGroupBy IN FRAME fMain /* Group By */
DO:
  dynamic-function("clearStatus").
  define variable tWhereClause as character no-undo.
  tWhereClause = dynamic-function("doFilterSort").
  case self:screen-value:
   when "S" then {lib/groupby.i &firstField=stateID &groupName=stateName &columnLabel="'State Name'" &whereClause=tWhereClause}
   when "C" then {lib/groupby.i &firstField=corporationID &groupName=corporationID &columnLabel="'Company'" &whereClause=tWhereClause}
   when "M" then {lib/groupby.i &firstField=manager &groupName=managerDesc &columnLabel="'Manager'" &whereClause=tWhereClause}
   when "A" then {lib/groupby.i &firstField=agentID &groupName=name &columnLabel="'Agent Name'" &whereClause=tWhereClause}
  end case.
  /* copy the showdata into tempdata */
  empty temp-table tempdata.
  for each showdata no-lock:
    create tempdata.
    buffer-copy showdata to tempdata.
  end.
  /* disable the expand/collapse buttons */
  if self:screen-value = "A"
   then std-lo = false.
   else std-lo = true.
   
  assign
    fExpandAll:sensitive   = std-lo
    fCollapseAll:sensitive = std-lo
    lGroupDetailSortAllow  = not std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  dynamic-function("setFilterCombos", self:label).
  apply "VALUE-CHANGED" to fGroupBy in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  dynamic-function("setFilterCombos", self:label).
  apply "VALUE-CHANGED" to fGroupBy in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-show.i}
{lib/win-close.i}
{lib/win-status.i}
{lib/brw-main.i}
{lib/report-progress-bar.i}
{lib/set-buttons.i}
{lib/set-filters.i &tableName="'showdata'" &labelName="'Manager,Status,State'" &columnName="'manager,stat,stateID'" &include="'Agent,GroupBy,CollapseAll,ExpandAll'"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

fExpandAll:load-image("images/s-expand.bmp").
fExpandAll:load-image-insensitive("images/s-expand-i.bmp").
fCollapseAll:load-image("images/s-collapse.bmp").
fCollapseAll:load-image-insensitive("images/s-collapse-i.bmp").

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-sysprop-list.i &combo=fManager &appCode="'AMD'" &objAction="'AgentManager'" &objProperty="''" &addAll=true}
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=fAgentID &state=tStateID &stat=fStatus &manager=fManager &addAll=true}
    {lib/set-current-value.i &state=tStateID}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
  
  clearData().
  run windowResized in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "agentltd.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "agentltd.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY fStatus fManager fGroupBy tStateID fAgentID fExpandLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo RECT-36 RECT-37 fStatus fManager fGroupBy tStateID fExpandAll 
         fCollapseAll fAgentID brwData fExpandLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufferdata for showdata.
  define buffer exportdata for exportdata.
  
  apply "CHOOSE" to fCollapseAll in frame {&frame-name}.

  empty temp-table exportdata.
  for each bufferdata no-lock:
    create exportdata.
    buffer-copy bufferdata to exportdata.
  end.

  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "agent_ltd"
  
  /* replace the sequence numbers with blanks */
  close query {&browse-name}.
  for each bufferdata exclusive-lock:
    if fGroupBy:screen-value = "C" or fGroupBy:screen-value = "M"
     then bufferdata.showID = "".
    if bufferdata.showName = bufferdata.agentID
     then bufferdata.showName = bufferdata.name.
  end.
  open query {&browse-name} for each showdata.

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
  /* set the original data */
  close query {&browse-name}.
  empty temp-table bufferdata.
  for each exportdata exclusive-lock:
    create bufferdata.
    buffer-copy exportdata to bufferdata.
  end.
  open query {&browse-name} for each showdata.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  
  /* cleanup before getting data */
  clearData().
  empty temp-table data.
  do with frame {&frame-name}:
    dStartTime = now.
    run server/reportagentltd.p (input tStateID:screen-value,
                                 input "ALL",
                                 input "ALL",
                                 output table data,
                                 output std-lo,
                                 output std-ch).
  end.
  if not std-lo
   then message "AgentManagerReport failed: " std-ch view-as alert-box warning.
   else
    do with frame {&frame-name}:
      run SetProgressStatus.
      std-in = 0.
      for each data exclusive-lock
            by data.stat:
            
        assign
          /* set the agent date */
          data.agentDateShow    = string(date(data.agentDate))
          data.agentDateShow    = (if data.agentDateShow = "?" then "" else data.agentDateShow)
          .
        /* calculate the percentage */
        if data.netPremium = 0
         then data.netPercent = data.costsIncurred / -1.
         else data.netPercent = ((if data.costsIncurred < 0 then 0 else data.costsIncurred) / data.netPremium) * 100.
        /* get the user */
        data.managerDesc = data.manager.
        publish "GetSysUserName" (data.manager, output data.managerDesc).
        data.managerDesc = (if data.managerDesc = "UNKNOWN" then "" else data.managerDesc).
        /* get the status */
        data.statDesc = data.stat.
        publish "GetSysPropDesc" ("AMD", "Agent", "Status", data.stat, output data.statDesc).
        /* get the state */
        data.stateName = data.stateID.
        data.stateName = getStateName(data.stateID).
      end.
      run SetProgressStatus.
      if can-find(first data)
       then
        do:
          if lookup("A",fStatus:list-item-pairs) > 0
           then fStatus:screen-value = "A".
           else fStatus:screen-value = "ALL".
          apply "VALUE-CHANGED" to fGroupBy.
          setFilterCombos("ALL").
        end.
      enableButtons(can-find(first data)).
      enableFilters(can-find(first data)).
      run AgentComboEnable (can-find(first data)).
      appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
      displayStatus().
      run SetProgressEnd.
    end.
      
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cGroup as character no-undo.
  cGroup = fGroupBy:screen-value in frame {&frame-name}.
  
  {lib/brw-sortData.i}
  if cGroup = "A"
   then
    do:
      {lib/brw-totalData.i &excludeColumn="11" &integerOnly=true}
    end.
  
  std-in = 0.
  for each showdata no-lock:
    std-in = std-in + 1.
  end.
  
  std-ch = "agent".
  case cGroup:
   when "S" then std-ch = "state".
   when "C" then std-ch = "company".
   when "M" then std-ch = "manager".
  end case.
  if (cGroup <> "A" and dataSortBy = "agentID") or (cGroup = "M" and dataSortBy = "managerDesc")
   then browse {&browse-name}:clear-sort-arrow().
  setStatus(string(std-in) + " " + std-ch + "(s) found").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  if totalAdded
   then iOffset = 23.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - (5 + iOffset).
  /* resize the column */
  {lib/resize-column.i &col="'name,managerDesc'" &var=dColumnWidth}
  
  if fGroupBy:screen-value in frame {&frame-name} = "A"
   then
    do:
      {lib/brw-totalData.i &excludeColumn="11" &integerOnly=true}
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hColumn as handle no-undo.
  do with FRAME {&frame-name}:
    run AgentComboEnable in this-procedure (false).
    hColumn = getColumn(browse {&browse-name}:handle, "showName").
    IF VALID-HANDLE(hColumn)
     THEN hColumn:label = "Agent Name".
  end.
  enableButtons(false).
  enableFilters(false).
  empty temp-table data.
  empty temp-table showdata.
  close query brwData.
  clearStatus().
  {lib/brw-totalData.i &noShow=true}
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

