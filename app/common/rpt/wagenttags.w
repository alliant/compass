&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Tags By Entity
@description Displays the Tags By Entity

@author Sagar R K
@version 1.0
@created 12/08/2023
@notes 
@modified
NAME       Date         Comments
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/tagbyentity.i &tableAlias="data"}
{tt/tagbyentity.i &tableAlias="ttdata"}
{tt/tagbyentity.i &tableAlias="tdata"}
{tt/tag.i}
{tt/tag.i &tableAlias="temptag"}
{tt/tag.i &tableAlias="tagdata"}
{tt/tag.i &tableAlias="tagall"}
{tt/openwindow.i}

{lib/amd-def.i}
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/std-def.i}
{lib/brw-multi-def.i}

define variable dColumnWidth       as decimal   no-undo.
define variable dEntityColumnWidth as decimal   no-undo.
define variable hTagDetail         as handle    no-undo.
define variable cnamelist          as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data tdata tag

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.selectrecord data.name data.tagscount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData data.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData data
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for BROWSE brwtag                                        */
&Scoped-define FIELDS-IN-QUERY-brwtag tdata.selectrecord tdata.name   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwtag tdata.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwtag tdata
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwtag tdata
&Scoped-define SELF-NAME brwtag
&Scoped-define QUERY-STRING-brwtag FOR EACH tdata
&Scoped-define OPEN-QUERY-brwtag OPEN QUERY {&SELF-NAME} FOR EACH tdata.
&Scoped-define TABLES-IN-QUERY-brwtag tdata
&Scoped-define FIRST-TABLE-IN-QUERY-brwtag tdata


/* Definitions for BROWSE brwTagDetail                                  */
&Scoped-define FIELDS-IN-QUERY-brwTagDetail tag.entityName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTagDetail   
&Scoped-define SELF-NAME brwTagDetail
&Scoped-define QUERY-STRING-brwTagDetail FOR EACH tag
&Scoped-define OPEN-QUERY-brwTagDetail OPEN QUERY {&SELF-NAME} FOR EACH tag.
&Scoped-define TABLES-IN-QUERY-brwTagDetail tag
&Scoped-define FIRST-TABLE-IN-QUERY-brwTagDetail tag


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwtag}~
    ~{&OPEN-QUERY-brwTagDetail}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brefresh brwTagDetail brwData brwtag 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgentAction C-Win 
FUNCTION openWindowForAgentAction RETURNS HANDLE
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export".

DEFINE BUTTON brefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Refresh".

DEFINE BUTTON bTranDetail  NO-FOCUS
     LABEL "TagDetail" 
     SIZE 4.8 BY 1.14 TOOLTIP "Tag Detail".

DEFINE RECTANGLE rAction
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.8 BY 1.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.

DEFINE QUERY brwtag FOR 
      tdata SCROLLING.

DEFINE QUERY brwTagDetail FOR 
      tag SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.selectrecord              width 8        column-label  "Select" view-as toggle-box    
data.name         label "Tag"        format "x(20)"                                                  
data.tagscount    label "#Agents" format ">>9"
enable data.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 47 BY 18 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwtag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwtag C-Win _FREEFORM
  QUERY brwtag DISPLAY
      tdata.selectrecord              width 8        column-label  "Linked" view-as toggle-box    
tdata.name         label "Tags"        format "x(20)"                                                  
enable tdata.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 111 BY 5.95 ROW-HEIGHT-CHARS .83 FIT-LAST-COLUMN.

DEFINE BROWSE brwTagDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTagDetail C-Win _FREEFORM
  QUERY brwTagDetail DISPLAY
      tag.entityName  label "Agents"     format "x(200)"      width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 111 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brefresh AT ROW 2.57 COL 2 WIDGET-ID 406 NO-TAB-STOP 
     bExport AT ROW 1.24 COL 156 WIDGET-ID 318 NO-TAB-STOP 
     bTranDetail AT ROW 1.24 COL 160.8 WIDGET-ID 396 NO-TAB-STOP 
     brwTagDetail AT ROW 2.62 COL 55.2 WIDGET-ID 300
     brwData AT ROW 2.62 COL 7.2 WIDGET-ID 200
     brwtag AT ROW 14.67 COL 55.2 WIDGET-ID 400
     rAction AT ROW 1.14 COL 155.6 WIDGET-ID 50
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 166.2 BY 20.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Tags"
         HEIGHT             = 19.81
         WIDTH              = 166.2
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwTagDetail bTranDetail fMain */
/* BROWSE-TAB brwData brwTagDetail fMain */
/* BROWSE-TAB brwtag brwData fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       brwtag:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwtag:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       brwTagDetail:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwTagDetail:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bTranDetail IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rAction IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwtag
/* Query rebuild information for BROWSE brwtag
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tdata.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwtag */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTagDetail
/* Query rebuild information for BROWSE brwTagDetail
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tag.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwTagDetail */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Tags */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Tags */
DO:  
  for each openwindow no-lock:
    if valid-handle(openwindow.procHandle)
     then run CloseWindow in openwindow.procHandle no-error.
  end.
  
  publish "closeWindow".
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Tags */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME brefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brefresh C-Win
ON CHOOSE OF brefresh IN FRAME fMain /* Refresh */
do:
  cnamelist = "".
  for each ttdata where ttdata.selectrecord :
    if cnamelist = ""
     then
      cnamelist = ttdata.name.
     else
     cnamelist = cnamelist + "," + ttdata.name.
  end.
  run getData       in this-procedure.
  run filterData in this-procedure("",false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON MOUSE-SELECT-DBLCLICK OF brwData IN FRAME fMain
DO:
  cnamelist = data.name.
  data.selectrecord = true  .
  data.selectrecord:checked in browse brwData = true.

  for each ttdata exclusive-lock :
   if ttdata.name = cnamelist
    then
     assign 
       ttdata.selectrecord = true.
    else
     ttdata.selectrecord = false.
  end.
  for each data exclusive-lock where data.selectrecord and data.name <> cnamelist: 
    assign 
       data.selectrecord = false
       data.selectrecord:checked in browse brwData = false.
  end.
    
  BROWSE brwData:REFRESH() .   
  run filterData in this-procedure(data.name,data.selectrecord).    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwData}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwtag
&Scoped-define SELF-NAME brwtag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwtag C-Win
ON ROW-DISPLAY OF brwtag IN FRAME fMain
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwtag C-Win
ON START-SEARCH OF brwtag IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwtag}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTagDetail
&Scoped-define SELF-NAME brwTagDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTagDetail C-Win
ON DEFAULT-ACTION OF brwTagDetail IN FRAME fMain
DO:
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = "AMD" 
   then
    do:
      if tag.entity = {&PersonCode} then
       publish "OpenWindow" (input {&PersonCode},
                             input tag.entityID,
                             input "wpersondetail.w",
                             input "character|input|" + tag.entityID + "^integer|input|6" + "^character|input|",
                             input this-procedure).
      else 
       run ActionAgentModify in this-procedure (tag.entityID).
    end.
   else
    do:
      if tag.entity = {&PersonCode} then
       publish "OpenWindow" (input {&PersonCode},
                             input tag.entityID,
                             input "wpersondetail.w",
                             input "character|input|" + tag.entityID + "^integer|input|2" + "^character|input|",
                             input this-procedure).
      else 
       run ActionAgentModify in this-procedure (tag.entityID).
    end.
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTagDetail C-Win
ON MOUSE-SELECT-DBLCLICK OF brwTagDetail IN FRAME fMain
DO:
  run  openTranDetail in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTagDetail C-Win
ON ROW-DISPLAY OF brwTagDetail IN FRAME fMain
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTagDetail C-Win
ON START-SEARCH OF brwTagDetail IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwTagDetail}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTagDetail C-Win
ON VALUE-CHANGED OF brwTagDetail IN FRAME fMain
DO:
  for each tdata:
    tdata.selectrecord = false.
  end.
    for each tagall fields(entityID name)  where tagall.entityID = tag.entityID:
      for each tdata where tdata.name = tagall.name :
        tdata.selectrecord = true.
      end.
    end.
  open query brwtag preselect each tdata.
  tdata.selectrecord:read-only in browse  brwtag   = true .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTranDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTranDetail C-Win
ON CHOOSE OF bTranDetail IN FRAME fMain /* TagDetail */
do:
  run  openTranDetail in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-status.i}

bTranDetail   :load-image            ("images/s-open.bmp").
bTranDetail   :load-image-insensitive("images/s-open-i.bmp").
{lib/set-buttons.i}

{lib/brw-main-multi.i &browse-list="brwData,brwTagDetail,brwtag"} 

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

setStatusMessage("").

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* initializeStatusWindow({&window-name}:handle). */

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  /* get the column width */
  {lib/get-column-width.i &col="'name'" &var=dColumnWidth &browse-name=brwTagDetail}
  /* get the column width */
  {lib/get-column-width.i &col="'entityName'" &var=dEntityColumnWidth &browse-name=brwTagDetail}
  
  run getData       in this-procedure.
  on 'value-changed':U of  data.selectrecord in browse  brwData  
   do:
     do with frame {&frame-name}:
     end.
     if available data 
      then
       do:
         data.selectrecord  = data.selectrecord:checked in browse brwData.
         for first ttdata where ttdata.name = data.name:
           ttdata.selectrecord = data.selectrecord.
         end.
       end.
     run filterData in this-procedure(data.name,data.selectrecord).
   end.
  
  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAgentModify C-Win 
PROCEDURE ActionAgentModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
   
  openWindowForAgentAction(pAgentID, "agentDetails", "wagent.w", "E").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brefresh brwTagDetail brwData brwtag 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htableHandle as handle no-undo.

  if query brwTagDetail:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).
 
  htableHandle = temp-table tag:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "tag",
                          "for each tag",
                          "entityName",
                          "Agents",
                          std-ch,
                          "Tags.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportQueryTagData C-Win 
PROCEDURE exportQueryTagData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hHandle as handle no-undo.

  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).
 
  hHandle = temp-table data:handle.
  run util/exporttable.p (table-handle hHandle,
                          "data",
                          "for each data",
                          "name,tagscount",
                          "Tag,Agent",
                          std-ch,
                          "QueryTag.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cname as character  no-undo.
  define input parameter lget as logical  no-undo.
  define variable cstatusBar as  character   no-undo.
  define variable icount     as  integer     no-undo.
  define variable ivalue     as  integer     no-undo.
  
  
  do with frame {&frame-name}:
  end.
  
  close query brwTagDetail.
  ivalue = 0.
  empty temp-table tag.
  empty temp-table tdata.
  open query brwtag preselect each tdata by tdata.selectrecord .
  cnamelist = "".
  
  for each ttdata where ttdata.selectrecord:
    create tdata.
    tdata.name = ttdata.name.
    ivalue = ivalue + 1.
    if cnamelist = ""
     then
      cnamelist = ttdata.name.
     else
     cnamelist = cnamelist + "," + ttdata.name.
  end. 
 
  if cnamelist = "" then
   do:
     empty temp-table tag.
     open query brwTagDetail preselect each tag.
   end.
  if cnamelist <> "" and ivalue = 0 
   then
    ivalue = 1.
  
  if lget
   then
    do:
      run server/querytagbyentitydetail.p(input "A",
                                          input cname,
                                          output table tagdata,
                                          output std-lo,
                                          output std-ch
                                         ).
      for each tagdata fields(entityID entityName name):
        create tagall.
        buffer-copy tagdata to tagall.
      end.
    end.
   else
    do:
      for each tagall where tagall.name = cname :
        delete tagall .
      end.
    end.

  do icount = 1 to num-entries(cnamelist):
   for each tagall fields(entityID entityName name) where tagall.name = entry(icount,cnamelist,"," ):                                      
     if not can-find(tag where tag.entityID = tagall.entityID)
      then
       do:
         create tag.
         assign
           tag.entityID    = tagall.entityID
           tag.entityName  = tagall.entityName
           tag.name        = tagall.name.              
           tag.percentageMatch = 1.
        end.
       else
        do:
          for first tag where tag.entityID = tagall.entityID:
            tag.percentageMatch =  tag.percentageMatch + 1.
          end.
        end.
   end. 
  end.  
  for each tag fields(percentageMatch):
    tag.percentageMatch = (tag.percentageMatch / num-entries(cnamelist,',')) * 100.
  end.
  
  
  open query brwTagDetail preselect each tag by tag.percentageMatch desc by tag.entityname .
  
  if (query brwTagDetail:num-results) > 0
   then
    apply "VALUE-CHANGED":U to brwTagDetail.

  assign 
      bExport:sensitive     = can-find(first tag)
      bTranDetail:sensitive = can-find(first tag).   

  if ivalue = 0 or (query brwTagDetail:num-results) = 0                                                   
   then
    cstatusBar = "".
   else
    cstatusBar = string(query brwTagDetail:num-results) + " record(s) found for " + string(ivalue) + " selected tag(s) ".

  setStatusMessage(cstatusBar).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable icount     as  integer     no-undo.
 do with frame {&FRAME-NAME}:
 end.
 setStatusMessage("").
 
 close query brwData.
 empty temp-table data.
 
 close query brwTagDetail.
 empty temp-table tagdata.
 empty temp-table ttdata.
 
 empty temp-table tag.

 run server/querytagbyentity.p(input "A",
                               output table data,
                               output std-lo,
                               output std-ch
                               ).
                               
 do icount = 1 to num-entries(cnamelist):
   for first data where data.name = entry(icount,cnamelist,"," ): 
     data.selectrecord = true.
     if icount = 1
      then
       std-ro = rowid(data).
   end.
 end.
  
  for each data:
    create ttdata.
    buffer-copy data to ttdata.
  end.
  
  open query brwData preselect each data by data.tagscount desc by data.name.

  if cnamelist <> ""
   then
    reposition brwData to rowid std-ro.
    
  assign 
      bExport:sensitive     = can-find(first tag)
      bTranDetail:sensitive = can-find(first tag).   

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openTranDetail C-Win 
PROCEDURE openTranDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  if std-ch = "AMD" 
   then
    do:
      if tag.entity = {&PersonCode} then
       publish "OpenWindow" (input {&PersonCode},
                             input tag.entityID,
                             input "wpersondetail.w",
                             input "character|input|" + tag.entityID + "^integer|input|6" + "^character|input|",
                             input this-procedure).
      else 
       run ActionAgentModify in this-procedure (tag.entityID).
    end.
   else
    do:
      if tag.entity = {&PersonCode} then
       publish "OpenWindow" (input {&PersonCode},
                             input tag.entityID,
                             input "wpersondetail.w",
                             input "character|input|" + tag.entityID + "^integer|input|2" + "^character|input|",
                             input this-procedure).
      else 
       run ActionAgentModify in this-procedure (tag.entityID).
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resultChanged C-Win 
PROCEDURE resultChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  setStatusMessage("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* {&frame-name} components */
  brwData:height-pixels      = frame {&frame-name}:height-pixels - brwData:y - (6.5 + iOffset).
  
  brwTagDetail:width-pixels  = frame {&frame-name}:width-pixels - brwData:width-pixels - 41.
  brwTagDetail:height-pixels = frame {&frame-name}:height-pixels - brwTagDetail:y - (132 + iOffset).
  
  brwtag:width-pixels  = frame {&frame-name}:width-pixels - brwData:width-pixels - 41 .
  
  brwtag:height-pixels = frame {&frame-name}:height-pixels - brwtag:y - (5.5 + iOffset).

  bExport:x       = frame {&frame-name}:width-pixels - 56.
  bTranDetail:x   = frame {&frame-name}:width-pixels - 32.
  rAction:x       = frame {&frame-name}:width-pixels - 58.                                                                                         
  
  /* resize the column */
  {lib/resize-column.i &col="'name'"       &var=dColumnWidth       &browse-name=brwTagDetail}
  {lib/resize-column.i &col="'entityName'" &var=dEntityColumnWidth &browse-name=brwTagDetail}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgentAction C-Win 
FUNCTION openWindowForAgentAction RETURNS HANDLE
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow      as handle    no-undo.
  define variable hFileDataSrv as handle    no-undo.
  define variable cAgentWindow as character no-undo.

  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAction <> "N" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).
  
  if pAction <> "C" then
  do:
    for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
      hWindow = openwindow.procHandle.
    end.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv,pAction).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

