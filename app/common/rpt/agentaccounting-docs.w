&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Accounting Documents (wcrm04-docs.w)
@description The browse shows all the documents for the selected agent.

@author John Oliver
@version 1.0
@created 2017/03/01
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/doc.i}             
{lib/std-def.i}
{lib/add-delimiter.i}
{lib/winlaunch.i}

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

define input parameter pAgentID as character no-undo.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDocs
&Scoped-define BROWSE-NAME brwDocs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES document

/* Definitions for BROWSE brwDocs                                       */
&Scoped-define FIELDS-IN-QUERY-brwDocs document.subdir document.displayName document.fileSize   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocs   
&Scoped-define SELF-NAME brwDocs
&Scoped-define QUERY-STRING-brwDocs FOR EACH document NO-LOCK BY document.displayName INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwDocs OPEN QUERY {&SELF-NAME} FOR EACH document NO-LOCK BY document.displayName INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwDocs document
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocs document


/* Definitions for FRAME fDocs                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fDocs ~
    ~{&OPEN-QUERY-brwDocs}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwDocs bClose 
&Scoped-Define DISPLAYED-OBJECTS cmbCategory 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClose 
     LABEL "Close" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cmbCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocs FOR 
      document SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocs C-Win _FREEFORM
  QUERY brwDocs NO-LOCK DISPLAY
      document.subdir FORMAT "x(30)" width 17 LABEL "Category"
document.displayName FORMAT "x(100)" WIDTH 46 LABEL "Name"
document.fileSize FORMAT "ZZ,ZZZ,ZZ9" WIDTH 13 LABEL "File Size (KB)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 85 BY 20.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDocs
     cmbCategory AT ROW 1.57 COL 10 COLON-ALIGNED WIDGET-ID 118
     brwDocs AT ROW 3.14 COL 2 WIDGET-ID 400
     bClose AT ROW 24.33 COL 37 WIDGET-ID 120
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 87 BY 24.91 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Documents"
         HEIGHT             = 24.91
         WIDTH              = 87
         MAX-HEIGHT         = 47.38
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.38
         VIRTUAL-WIDTH      = 384
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fDocs
   FRAME-NAME                                                           */
/* BROWSE-TAB brwDocs cmbCategory fDocs */
/* SETTINGS FOR COMBO-BOX cmbCategory IN FRAME fDocs
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocs
/* Query rebuild information for BROWSE brwDocs
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH document NO-LOCK BY document.displayName INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwDocs */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Documents */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Documents */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose C-Win
ON CHOOSE OF bClose IN FRAME fDocs /* Close */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&Scoped-define SELF-NAME brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DEFAULT-ACTION OF brwDocs IN FRAME fDocs
DO:
  if available document
   then run DownloadFile in this-procedure (input document.identifier).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON ROW-DISPLAY OF brwDocs IN FRAME fDocs
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON START-SEARCH OF brwDocs IN FRAME fDocs
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbCategory C-Win
ON VALUE-CHANGED OF cmbCategory IN FRAME fDocs /* Category */
DO:
  define variable hQuery as handle.
  define variable cFilterValue as character no-undo.
  define variable cWhere as character no-undo init "preselect each document".
  
  do with frame {&frame-name}:
    cFilterValue = cmbCategory:screen-value.
  
    if cFilterValue <> "ALL"
     then cWhere = cWhere + " where subdir = '" + cFilterValue + "'".
  
    hQuery = query brwDocs:handle.
    if hQuery:is-open
     then hQuery:query-close().
  
    hQuery:query-prepare(cWhere).
    hQuery:query-open().
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/brw-main.i}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run GetData in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
@description Closes the window
------------------------------------------------------------------------------*/
  apply "WINDOW-CLOSE" to {&window-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DownloadFile C-Win 
PROCEDURE DownloadFile :
/*------------------------------------------------------------------------------
@description Downloads one of the agent files from the server   
------------------------------------------------------------------------------*/
  def input parameter pIdentifier as char no-undo.
  def var cInputFile as char no-undo.
  def var cSubDir as char no-undo.
  def var cOutputFile as char no-undo.
  
  for first document no-lock
      where document.identifier = pIdentifier:
    
    assign
      cInputFile = document.fullpath
      cSubDir = document.subdir
      .
  end.
  run server/getagentaccountingfile.p (input cInputFile,
                                       input cSubDir,
                                       output cOutputFile,
                                       output std-lo,
                                       output std-ch
                                       ).
  if std-lo
   then
    do:
      RUN ShellExecuteA in this-procedure (0,
                                           "open",
                                           cOutputFile,
                                           "",
                                           "",
                                           1,
                                           OUTPUT std-in).
    end.
  
  if not std-lo and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX INFO BUTTONS OK.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cmbCategory 
      WITH FRAME fDocs IN WINDOW C-Win.
  ENABLE brwDocs bClose 
      WITH FRAME fDocs IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDocs}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer doc for document.
  /* get the documents */
  run server/getagentaccountingdocuments.p (input pAgentID,
                                            output table document,
                                            output std-lo,
                                            output std-ch
                                            ).
  if not std-lo
   then
    do:
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      return.
    end.
  {&OPEN-QUERY-brwDocs}
    
  /* populate the Category drop down for the documents */
  std-ch = "ALL".
  for each doc no-lock 
  break by doc.subdir:
    if first-of(doc.subdir) and doc.subdir > ""
     then 
      assign
        std-ch = addDelimiter(std-ch,",") + doc.subdir
        .
  end.
  do with frame {&frame-name}:
    if std-ch > ""
     then
      assign
        cmbCategory:list-items = std-ch
        cmbCategory:sensitive = true
        cmbCategory:screen-value = "ALL"
        .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
@description Shows the window
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
@description Sorts the data in the browse widget
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i} 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

