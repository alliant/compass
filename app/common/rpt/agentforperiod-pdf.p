&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* agentforperiod.p
   PDF version of Agents for Period
   9.24.2014 D.Sinclair
  ----------------------------------------------------------------------*/

define temp-table agentperiod
  field agentID as char
  field name as char
  field stat as char
  field stateID as char
  field numBatches as int
  field numFiles as int
  field numPolicies as int
  field numEndorsements as int
  field liabilityAmount as dec
  field grossPremium as dec
  field netPremium as dec
  field retainedPremium as decimal
  /* for client */
  field statDesc as character
  .

def input parameter table for agentperiod.
def input parameter pTitle as char.
def input parameter pFilename as char.

{lib/rpt-def.i &ID="'OPS03'" &lines=100}

def var tTotalBatches as int.
def var tTotalFiles as int.
def var tTotalPolicies as int.
def var tTotalAgents as int.
def var tTotalGross as deci.
def var tTotalNet as deci.
def var tTotalRet as deci.
def var tTotalLiab as deci.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

assign
  activeFont = "Courier"
  activeSize = 10.0
  activeFilename = pFilename
  .

if not can-find(first agentperiod)
 then 
  do: 
      MESSAGE "There is nothing to print" 
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.

{lib/rpt-open.i}

run details in this-procedure.
/* run summary in this-procedure.  */

{lib/rpt-cloz.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-details) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE details Procedure 
PROCEDURE details PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  setFont("Helvetica-BoldOblique", 12.0). */
/*  setFont("Helvetica-Bold", 12.0).        */
/*  setFont("Helvetica", 12.0).             */
def var tLastState as char no-undo.
DEFINE VARIABLE tSubNumBatches AS int     NO-UNDO.
DEFINE VARIABLE tSubNumFiles AS INTEGER     NO-UNDO.
DEFINE VARIABLE tSubNumPolicies AS INTEGER     NO-UNDO.
DEFINE VARIABLE tSubLiability AS DECIMAL     NO-UNDO.
DEFINE VARIABLE tSubGrossPremium AS DECIMAL     NO-UNDO.
DEFINE VARIABLE tSubNetPremium AS DECIMAL     NO-UNDO.
DEFINE VARIABLE tSubRetainedPremium AS DECIMAL     NO-UNDO.

 setFont("Courier", 7.0).
 textColor(0.0, 0.0, 0.0).
 
 assign
   tTotalBatches = 0
   tTotalFiles = 0
   tTotalPolicies = 0
   tTotalAgents = 0
   tTotalGross = 0
   tTotalNet = 0
   tTotalRet = 0
   tTotalLiab = 0
   .

 for each agentperiod
   break by agentperiod.stateID
         by agentperiod.name
         by agentperiod.agentID:
  if first-of(agentperiod.state) 
   then
    do:
        if not first(agentperiod.stateID)
         then newLine(2).
        setFont("Courier-Bold", 8.0).
        textAt(agentperiod.stateID, 1).
        setFont("Courier", 7.0).
        newLine(2).
    end.

  textAt(agentperiod.agentID, 1).
  textAt(substring(agentperiod.name, 1, 35), 10).
  textTo(string(agentperiod.numBatches, "zzz,zz9"), 53).
  textTo(string(agentperiod.numFiles, "zzz,zz9"), 62).
  textTo(string(agentperiod.numPolicies, "zzz,zz9"), 71).
  textTo(string(agentperiod.liability, "-zzz,zzz,zzz,zz9"), 88).
  textTo(string(agentperiod.grossPremium, "-zzz,zzz,zzz,zz9"), 105).
  textTo(string(agentperiod.netPremium, "-zzz,zzz,zzz,zz9"), 122).
  textTo(string(agentperiod.retainedPremium, "-zzz,zzz,zzz,zz9"), 139).
  newLine(1).

  assign
    tSubNumBatches = tSubNumBatches + agentperiod.numBatches
    tSubNumFiles = tSubNumFiles + agentperiod.numFiles
    tSubNumPolicies = tSubNumPolicies + agentperiod.numPolicies
    tSubLiability = tSubLiability + agentperiod.liability
    tSubGrossPremium = tSubGrossPremium + agentperiod.grossPremium
    tSubNetPremium = tSubNetPremium + agentperiod.netPremium
    tSubRetainedPremium = tSubRetainedPremium + agentperiod.retainedPremium
    .

  assign
    tTotalBatches = tTotalBatches + agentperiod.numBatches
    tTotalFiles = tTotalFiles + agentperiod.numFiles
    tTotalPolicies = tTotalPolicies + agentperiod.numPolicies
    tTotalLiab = tTotalLiab + agentperiod.liability
    tTotalGross = tTotalGross + agentperiod.grossPremium
    tTotalNet = tTotalNet + agentperiod.netPremium
    tTotalRet = tTotalRet + agentperiod.retainedPremium
    .

  if last-of(agentperiod.state) 
   then
    do:
        newLine(1).
        setFont("Courier-Bold", 7.0).
        textAt("Total for " + agentperiod.state, 10).
        textTo(string(tSubNumBatches, "zzz,zz9"), 53).
        textTo(string(tSubNumFiles, "zzz,zz9"), 62).
        textTo(string(tSubNumPolicies, "zzz,zz9"), 71).
        textTo(string(tSubLiability, "-zzz,zzz,zzz,zz9"), 88).
        textTo(string(tSubGrossPremium, "-zzz,zzz,zzz,zz9"), 105).
        textTo(string(tSubNetPremium, "-zzz,zzz,zzz,zz9"), 122).
        textTo(string(tSubRetainedPremium, "-zzz,zzz,zzz,zz9"), 139).
        
        assign
          tSubNumBatches = 0
          tSubNumFiles = 0
          tSubNumPolicies = 0
          tSubLiability = 0
          tSubGrossPremium = 0
          tSubNetPremium = 0
          tSubRetainedPremium = 0
          .
        
        setFont("Courier", 7.0).
    end.
 end.

 newLine(2).
 setFont("Courier-Bold", 7.0).
 textAt("Report Totals", 10).
 textTo(string(tTotalBatches, "zzz,zz9"), 53).
 textTo(string(tTotalFiles, "zzz,zz9"), 62).
 textTo(string(tTotalPolicies, "zzz,zz9"), 71).
 textTo(string(tTotalLiab, "-zzz,zzz,zzz,zz9"), 88).
 textTo(string(tTotalGross, "-zzz,zzz,zzz,zz9"), 105).
 textTo(string(tTotalNet, "-zzz,zzz,zzz,zz9"), 122).
 textTo(string(tTotalRet, "-zzz,zzz,zzz,zz9"), 139).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/rpt-ftr.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/rpt-hdr.i &title=PTitle &title-x=10 &title-y=750 &no-logo=true}

 setFont("Courier-Bold", 7.0).
 textColor(0.0, 0.0, 0.0).
 
 textTo("Number", 53).
 textTo("Number", 62).
 textTo("Number", 71).

 textTo("Liability", 88).
 textTo("Gross", 105).
 textTo("Net", 122).
 textTo("Retained", 139).
 newLine(1).

 textAt("AgentID", 1).
 textAt("Name", 10).

 textTo("Batches", 53).
 textTo("Files", 62).
 textTo("Policies", 71).

 textTo("Amount", 88).
 textTo("Premium", 105).
 textTo("Premium", 122).
 textTo("Premium", 139).
 newLine(2).

 setFont("Courier", 7.0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-summary) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE summary Procedure 
PROCEDURE summary PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 textColor(0.0, 0.0, 0.0).
 setFont("Helvetica-Bold", 8.0).
 textAt("SUMMARY", 2).
 newLine(2).

 setFont("Helvetica", 8.0).
 textAt("Not Implemented yet...", 10).
 newLine(1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

