&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialoginvoicedue.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input-output parameter ipcMethod     as character no-undo.
define input-output parameter iopiDueInDays as integer   no-undo.
define input-output parameter iopiDueOnDay  as integer   no-undo.
define output       parameter loSuccess     as logical   no-undo.

/* Local Variable Definitions ---                                       */


&scoped-define Invdue        "Invdue"
&scoped-define Invdueindays  "Invdueindays"
&scoped-define Invdueonday   "Invdueonday"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbMethod fDueInDays fDueOnDay Btn_OK ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cbMethod fDueInDays fDueOnDay 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbMethod AS CHARACTER FORMAT "X(256)":U 
     LABEL "Invoice Due" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "item","item"
     DROP-DOWN-LIST
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE fDueInDays AS INTEGER FORMAT "ZZ9":U INITIAL 0 
     LABEL "Due in Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 6.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDueOnDay AS INTEGER FORMAT "Z9":U INITIAL 0 
     LABEL "Due on Day (1 to 30)" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5.8 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbMethod AT ROW 1.62 COL 20 COLON-ALIGNED WIDGET-ID 2
     fDueInDays AT ROW 3.05 COL 20 COLON-ALIGNED WIDGET-ID 6
     fDueOnDay AT ROW 3.05 COL 54.2 COLON-ALIGNED WIDGET-ID 8
     Btn_OK AT ROW 4.67 COL 24
     Btn_Cancel AT ROW 4.67 COL 41
     SPACE(19.79) SKIP(0.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Set Invoice Due Date"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Set Invoice Due Date */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:  
  assign
    ipcMethod     = cbMethod:input-value 
    iopiDueInDays = fDueInDays:input-value
    iopiDueOnDay  = fDueOnDay:input-value
    loSuccess     = true.
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbMethod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbMethod Dialog-Frame
ON VALUE-CHANGED OF cbMethod IN FRAME Dialog-Frame /* Invoice Due */
DO:
  if cbMethod:input-value = {&Invdue}
   then
    assign
      fDueInDays:sensitive = false
      fDueOnDay:sensitive = false
      fDueInDays:screen-value = "0"
      fDueOnDay:screen-value = "0"    
      .
  else if cbMethod:input-value = {&Invdueindays}
   then
    assign
      fDueInDays:sensitive = true
      fDueOnDay:sensitive = false
      fDueInDays:screen-value = string(iopiDueInDays)
      fDueOnDay:screen-value = "0"     
      .  
  else if cbMethod:input-value = {&Invdueonday}
   then
    assign
      fDueInDays:sensitive = false
      fDueOnDay:sensitive = true
      fDueInDays:screen-value = "0"
      fDueOnDay:screen-value = string(iopiDueOnDay).
      .   
  run enableDisableOK in this-procedure.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDueInDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDueInDays Dialog-Frame
ON VALUE-CHANGED OF fDueInDays IN FRAME Dialog-Frame /* Due in Days */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDueOnDay
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDueOnDay Dialog-Frame
ON VALUE-CHANGED OF fDueOnDay IN FRAME Dialog-Frame /* Due on Day (1 to 30) */
DO:
  run enableDisableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/get-sysprop-list.i &combo=cbMethod &appCode="'ARM'" &objAction="'Agent'" &objProperty="'SetInvDueDate'"}.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  cbMethod:screen-value = (if ipcMethod = "" then {&Invdue} else ipcMethod) no-error.
  fDueInDays:screen-value = string(iopiDueInDays).
  fDueOnDay:screen-value = string(iopiDueOnDay).
  apply 'value-changed' to cbMethod.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableOK Dialog-Frame 
PROCEDURE enableDisableOK :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  Btn_OK:sensitive = (cbMethod:input-value = {&Invdue})            or
                     (cbMethod:input-value = {&Invdueindays} 
                      and fDueInDays:input-value > 0
                      and fDueInDays:input-value <> iopiDueInDays) or
                     (cbMethod:input-value = {&Invdueonday} 
                      and fDueOnDay:input-value > 0                       
                      and fDueOnDay:input-value < 31
                      and fDueOnDay:input-value <> iopiDueOnDay).
                        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbMethod fDueInDays fDueOnDay 
      WITH FRAME Dialog-Frame.
  ENABLE cbMethod fDueInDays fDueOnDay Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

