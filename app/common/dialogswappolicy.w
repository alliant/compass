&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*
 dialogswappolicy.w
 DIALOG to ADD a POLICY outside of a Batch
 4.25.2012 D.Sinclair
 
 Rahul Sharma
 Note: Window moved from app/ops to app/common because 
       it is used from ARM as well as OPS module
 */

def input parameter pFromPolicy as int.
def input-output parameter pPolicy as int.
def output parameter pAction as char init "UNDO".

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tPolicy bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tPolicy tFromPolicy 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Apply" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tFromPolicy AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "From Policy" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "To Policy" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tPolicy AT ROW 3.43 COL 22.4 COLON-ALIGNED WIDGET-ID 4
     bSave AT ROW 7.52 COL 19
     bCancel AT ROW 7.52 COL 36.4
     tFromPolicy AT ROW 2.29 COL 22.4 COLON-ALIGNED WIDGET-ID 42
     "The From Policy will be changed to status Voided." VIEW-AS TEXT
          SIZE 49 BY .62 AT ROW 5.19 COL 11.4 WIDGET-ID 44
     "The To Policy must be in status Issued to the same Agent." VIEW-AS TEXT
          SIZE 55.4 BY .62 AT ROW 5.86 COL 11.4 WIDGET-ID 46
     SPACE(3.79) SKIP(2.94)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Swap Policy"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tFromPolicy IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Swap Policy */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fMain
ON CHOOSE OF bSave IN FRAME fMain /* Apply */
DO:

  if tPolicy:input-value in frame fMain = 0 
   then
    do:
     MESSAGE "Please enter a non-zero Policy number."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return no-apply.
    end.

  pAction = "SAVE".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

ASSIGN
  tFromPolicy = pFromPolicy
  tPolicy = pPolicy
  .

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
Do ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if pAction = "SAVE" 
   then 
    do: assign
          pPolicy = tPolicy:input-value in frame fMain
          .
    end.
  
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tPolicy tFromPolicy 
      WITH FRAME fMain.
  ENABLE tPolicy bSave bCancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

