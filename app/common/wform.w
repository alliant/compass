&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wdocument.w
   Window of DOCUMENTs
   4.23.2012
   
   Rahul Sharma
   Note: Window moved from app/ops to app/common because 
         it is used from ARM as well as OPS module
   */

CREATE WIDGET-POOL.

{tt/stateform.i}
{tt/state.i}

{lib/std-def.i}

def var hData as handle no-undo.

{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwDocuments

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES stateform

/* Definitions for BROWSE brwDocuments                                  */
&Scoped-define FIELDS-IN-QUERY-brwDocuments stateform.stateID stateform.formID stateform.formType stateform.revenueType stateform.description stateform.formCode stateform.active stateform.insuredType stateform.rateCheck stateform.rateMin stateform.rateMax stateform.orgName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocuments   
&Scoped-define SELF-NAME brwDocuments
&Scoped-define QUERY-STRING-brwDocuments FOR EACH stateform
&Scoped-define OPEN-QUERY-brwDocuments OPEN QUERY {&SELF-NAME} FOR EACH stateform.
&Scoped-define TABLES-IN-QUERY-brwDocuments stateform
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocuments stateform


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwDocuments bClear bFilter fSearch bDelete ~
bModify bNew bExport bRefresh fState RECT-37 
&Scoped-Define DISPLAYED-OBJECTS fSearch fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the selected form".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected form".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new form".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Colorado","CO",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 80 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36.8 BY 1.91.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 154 BY 1.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocuments FOR 
      stateform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocuments C-Win _FREEFORM
  QUERY brwDocuments DISPLAY
      stateform.stateID label "StateID" width 10 
 stateform.formID column-label "FormID" width 18 format "x(20)"
 stateform.formType label "Type"
 stateform.revenueType label "Revenue Type" format "x(12)"
 stateform.description format "x(250)" width 60 label "Description"
 stateform.formCode format "x(100)" width 25 label "Form Code"
 stateform.active label "Active" format "Yes/No"
 stateform.insuredType column-label "Insured!Type"
 stateform.rateCheck column-label "Rate!Check" format "x(6)"
 stateform.rateMin column-label "Min!Value" format "zzz,zz9.99"
 stateform.rateMax column-label "Max!Value" format "zzz,zz9.99"
 stateform.orgName column-label "Org Name" format "x(100)" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 209 BY 19.52 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwDocuments AT ROW 3.38 COL 2 WIDGET-ID 200
     bClear AT ROW 1.62 COL 182.6 WIDGET-ID 68
     bFilter AT ROW 1.62 COL 172.4 WIDGET-ID 64
     fSearch AT ROW 1.71 COL 90 COLON-ALIGNED WIDGET-ID 62
     bDelete AT ROW 1.29 COL 31 WIDGET-ID 14 NO-TAB-STOP 
     bModify AT ROW 1.29 COL 23.8 WIDGET-ID 12 NO-TAB-STOP 
     bNew AT ROW 1.29 COL 16.6 WIDGET-ID 10 NO-TAB-STOP 
     bExport AT ROW 1.29 COL 9.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.29 COL 2.2 WIDGET-ID 4 NO-TAB-STOP 
     fState AT ROW 1.71 COL 49 COLON-ALIGNED WIDGET-ID 42
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1 COL 41 WIDGET-ID 60
     RECT-2 AT ROW 1.24 COL 2 WIDGET-ID 8
     RECT-37 AT ROW 1.33 COL 40 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 211 BY 22.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "State Products"
         HEIGHT             = 22.19
         WIDTH              = 211
         MAX-HEIGHT         = 29.29
         MAX-WIDTH          = 211
         VIRTUAL-HEIGHT     = 29.29
         VIRTUAL-WIDTH      = 211
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwDocuments 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwDocuments:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwDocuments:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocuments
/* Query rebuild information for BROWSE brwDocuments
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH stateform
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDocuments */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* State Products */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* State Products */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* State Products */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  fSearch:screen-value = "".
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  run actionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  run actionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  run actionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocuments
&Scoped-define SELF-NAME brwDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocuments C-Win
ON DEFAULT-ACTION OF brwDocuments IN FRAME fMain
DO:
  run actionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocuments C-Win
ON ROW-DISPLAY OF brwDocuments IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocuments C-Win
ON START-SEARCH OF brwDocuments IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
 assign {&SELF-NAME}.
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("stateID").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
 {&window-name}:window-state = window-minimized. 
  
{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bNew:load-image("images/new.bmp").
bModify:load-image("images/update.bmp").
bDelete:load-image("images/delete.bmp").

{lib/get-state-list.i &combo=fState &addAll=true}


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
   run getData in this-procedure.  
   
  {&window-name}:window-state = WINDOW-NORMAL . 
  run ShowWindow in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available stateform 
  then return.

 std-lo = false.
 MESSAGE "Form will be permanently removed.  Continue?"
  VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
 if not std-lo 
  then return.

 publish "DeleteStateForm" (stateform.stateID,
                            stateform.formID,
                            output std-lo,
                            output std-ch).
 if not std-lo 
  then 
   do: MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.
 delete stateform.
 browse brwDocuments:delete-selected-rows().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tStateID as char no-undo.
 def var tFormID as char no-undo.
 def var tDescription as char no-undo.
 def var tFormCode as char no-undo.
 def var tType as char no-undo.
 def var tInsuredType as char no-undo.
 def var tActive as logical no-undo.
 def var tRateCheck as char no-undo.
 def var tRateMin as decimal no-undo.
 def var tRateMax as decimal no-undo.
 def var tOrgName as char no-undo.
 def var tOrgRev as char no-undo.
 def var tOrgRel as datetime no-undo.
 def var tOrgEff as datetime no-undo.
 def var tRevType as char no-undo.

 def var tCancel as logical no-undo.

 if not available stateform 
  then return.
 assign
   tDescription = stateform.description
   tFormCode = stateform.formCode
   tActive = stateform.active
   tRateCheck = stateform.rateCheck
   tRateMin = stateform.rateMin
   tRateMax = stateform.rateMax
   tOrgName = stateform.orgName
   tOrgRev = stateform.orgRev
   tOrgRel = stateform.orgRel
   tOrgEff = stateform.orgEff
   tRevType = stateform.revenueType
   .

 UPDATE-BLOCK:
 repeat:
  run dialogmodifysform.w (stateform.stateID,
                           stateform.formID,
                           stateform.formType,
                           stateform.insuredType,
                           input-output tDescription,
                           input-output tFormCode,
                           input-output tActive,
                           input-output tRateCheck,
                           input-output tRateMin,
                           input-output tRateMax,
                           input-output tOrgName,
                           input-output tOrgRev,
                           input-output tOrgRel,
                           input-output tOrgEff,
                           input-output tRevType,
                           output tCancel).
  if tCancel 
   then leave UPDATE-BLOCK.
  
  std-lo = false.
  publish "ModifyStateForm" (stateform.stateID,
                             stateform.formID,
                             tDescription,
                             tFormCode,
                             tActive,
                             tRateCheck,
                             tRateMin,
                             tRateMax,
                             tOrgName,
                             tOrgRev,
                             tOrgRel,
                             tOrgEff,
                             tRevType,
                             output std-lo,
                             output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        next UPDATE-BLOCK.
    end.
  assign
    stateform.description = tDescription
    stateform.formCode = tFormCode
    stateform.active = tActive
    stateform.rateCheck = tRateCheck
    stateform.rateMin = tRateMin
    stateform.rateMax = tRateMax
    stateform.orgName = tOrgName
    stateform.orgRev = tOrgRev
    stateform.orgRel = tOrgRel
    stateform.orgEff = tOrgEff
    stateForm.revenueType = tRevType
    .
  display
    stateForm.revenueType
    stateform.description
    stateform.formCode
    stateform.active
    stateform.rateCheck
    stateform.rateMin
    stateform.rateMax
    stateform.orgName
   with browse brwDocuments.
  leave.
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tStateID as char no-undo.
 def var tFormID as char no-undo.
 def var tDescription as char no-undo.
 def var tFormCode as char no-undo.
 def var tType as char no-undo.
 def var tInsuredType as char no-undo.
 def var tActive as logical no-undo.
 def var tRateCheck as char no-undo.
 def var tRateMin as decimal no-undo.
 def var tRateMax as decimal no-undo.
 def var tOrg as char no-undo.
 def var tOrgRev as char no-undo.
 def var tOrgRel as datetime no-undo.
 def var tOrgEff as datetime no-undo.
 def var tRevType as character no-undo.

 def var tCancel as logical no-undo.

 UPDATE-BLOCK:
 repeat:
  run dialognewsform.w (input-output tStateID,
                        input-output tFormID,
                        input-output tDescription,
                        input-output tFormCode,
                        input-output tType,
                        input-output tInsuredType,
                        input-output tActive,
                        input-output tRateCheck,
                        input-output tRateMin,
                        input-output tRateMax,
                        input-output tOrg,
                        input-output tOrgRev,
                        input-output tOrgRel,
                        input-output tOrgEff,
                        input-output tRevType,
                        output tCancel).
  if tCancel 
   then leave UPDATE-BLOCK.
   
  std-lo = false.
  publish "NewStateForm" (tStateID,
                          tFormID,
                          tDescription,
                          tFormCode,
                          tType,
                          tInsuredType,
                          tActive,
                          tRateCheck,
                          tRateMin,
                          tRateMax,
                          tOrg,
                          tOrgRev,
                          tOrgRel,
                          tOrgEff,
                          tRevType,
                          output std-lo,
                          output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        next UPDATE-BLOCK.
    end.
  run getData in this-procedure.
  leave.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch fState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwDocuments bClear bFilter fSearch bDelete bModify bNew bExport 
         bRefresh fState RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.

 if query brwDocuments:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).
 
 th = temp-table stateform:handle.
 
 run util/exporttable.p (table-handle th,
                         "stateform",
                         "for each stateform ",
                         "stateID,revenueType,formID,formType,description,formCode,active,insuredType,rateCheck,rateMin,rateMax,orgName,orgRev,orgRel,orgEff",
                         "State,Revenue Type,FormID,Type,Description,Form Code,Active,Insured Type,Rate Check,Min Premium,Max Premium,Organization,Revision,Released,Effective",
                         std-ch,
                         "StateForms.csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("stateID").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer stateform for stateform.
 def var tFile as char no-undo.
 
 close query brwDocuments.
 empty temp-table stateform.

 publish "GetStateForms" (output table stateform).
 
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("stateID").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tWhereClause as char no-undo.

do with frame {&frame-name}:

  tWhereClause = "where " +
                 (if fState:screen-value <> "ALL" 
                 then "stateform.stateID = '" + fState:screen-value + "' "
                 else "true ") +
                 "and " + 
                 (if fSearch:screen-value <> ""
                 then "stateform.description matches '*" + fSearch:screen-value + "*' "
                 else "true ").
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

