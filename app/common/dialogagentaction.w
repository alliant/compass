&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogTag.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas Jain

  Created:04/15/2020
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/action.i}
{tt/finding.i}


/* Parameters Definitions ---                                           */
define input  parameter table for action .
define input  parameter table for finding .

/* Local Variable Definitions ---                                       */
define variable cTrackTag      as character no-undo.
define variable lTrackPrivate   as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tEntity tEntityName tSeverity tSource ~
tQuestion tFinding tActionID tCreatedDate tFollowupDate tType tStartDate ~
tDueDate tOwner tOpenedDate tOriginalDueDate tStatus tCompletedDate ~
tExtendedDate tCompleteStatus tStartGap tAction Btn_Cancel RECT-55 iIcon ~
RECT-58 
&Scoped-Define DISPLAYED-OBJECTS tEntity tEntityName tSeverity tSource ~
tQuestion tFinding tActionID tCreatedDate tFollowupDate tType tStartDate ~
tDueDate tOwner tOpenedDate tOriginalDueDate tStatus tCompletedDate ~
tExtendedDate tCompleteStatus tStartGap tAction 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActionStatus Dialog-Frame 
FUNCTION getActionStatus RETURNS CHARACTER
  ( input ipcStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getActionType Dialog-Frame 
FUNCTION getActionType RETURNS CHARACTER
  ( input ipcType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 12 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2.14 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2.14 NO-UNDO.

DEFINE VARIABLE tQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2.14 NO-UNDO.

DEFINE VARIABLE tActionID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action ID" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tCompletedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Completed" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tCompleteStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Complete" 
     VIEW-AS FILL-IN 
     SIZE 63 BY 1 NO-UNDO.

DEFINE VARIABLE tCreatedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Created" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tEntityName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 89.8 BY 1 NO-UNDO.

DEFINE VARIABLE tExtendedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Extended" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tFollowupDate AS DATE FORMAT "99/99/99":U 
     LABEL "Followup" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOpenedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Opened" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOriginalDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Originally Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Severity" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 TOOLTIP "What business process found the issue?" NO-UNDO.

DEFINE VARIABLE tStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Started" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tStartGap AS CHARACTER FORMAT "X(256)":U 
     LABEL "Start Gap" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 DROP-TARGET NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22 BY 1 NO-UNDO.

DEFINE IMAGE iIcon
     SIZE 3.6 BY .86.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 9.29.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 8.33.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tEntity AT ROW 2.95 COL 14 COLON-ALIGNED WIDGET-ID 328
     tEntityName AT ROW 3 COL 37.2 COLON-ALIGNED NO-LABEL WIDGET-ID 334
     tSeverity AT ROW 4.14 COL 14 COLON-ALIGNED WIDGET-ID 332
     tSource AT ROW 4.14 COL 45 COLON-ALIGNED WIDGET-ID 312
     tQuestion AT ROW 5.33 COL 16 NO-LABEL WIDGET-ID 308
     tFinding AT ROW 7.71 COL 16 NO-LABEL WIDGET-ID 306
     tActionID AT ROW 11.52 COL 14 COLON-ALIGNED WIDGET-ID 228
     tCreatedDate AT ROW 11.52 COL 57 COLON-ALIGNED WIDGET-ID 220
     tFollowupDate AT ROW 11.52 COL 107 COLON-ALIGNED WIDGET-ID 232
     tType AT ROW 12.71 COL 14 COLON-ALIGNED WIDGET-ID 336
     tStartDate AT ROW 12.71 COL 57 COLON-ALIGNED WIDGET-ID 326
     tDueDate AT ROW 12.71 COL 107 COLON-ALIGNED WIDGET-ID 324
     tOwner AT ROW 13.91 COL 14 COLON-ALIGNED WIDGET-ID 322
     tOpenedDate AT ROW 13.91 COL 57 COLON-ALIGNED WIDGET-ID 82
     tOriginalDueDate AT ROW 13.91 COL 107 COLON-ALIGNED WIDGET-ID 222
     tStatus AT ROW 15.1 COL 14 COLON-ALIGNED WIDGET-ID 318
     tCompletedDate AT ROW 15.1 COL 57 COLON-ALIGNED WIDGET-ID 218
     tExtendedDate AT ROW 15.1 COL 107 COLON-ALIGNED WIDGET-ID 86
     tCompleteStatus AT ROW 16.29 COL 14 COLON-ALIGNED WIDGET-ID 320
     tStartGap AT ROW 16.29 COL 107 COLON-ALIGNED WIDGET-ID 280
     tAction AT ROW 17.48 COL 16 NO-LABEL WIDGET-ID 192
     Btn_OK AT ROW 20.52 COL 50
     Btn_Cancel AT ROW 20.52 COL 63.6
     "Action:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 17.57 COL 8.8 WIDGET-ID 216
     "Action" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 10.57 COL 4 WIDGET-ID 224
     "Finding:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 7.76 COL 8 WIDGET-ID 302
     "Question:" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 5.43 COL 6.4 WIDGET-ID 304
     "Finding" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.76 COL 4 WIDGET-ID 300
     RECT-55 AT ROW 10.81 COL 3 WIDGET-ID 76
     iIcon AT ROW 11.62 COL 27 WIDGET-ID 268
     RECT-58 AT ROW 2 COL 3 WIDGET-ID 296
     SPACE(1.79) SKIP(11.90)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Action"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tAction:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tActionID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tCompletedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tCompleteStatus:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tCreatedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tDueDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tEntity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tEntityName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tExtendedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tFinding:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tFollowupDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tOpenedDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tOriginalDueDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tOwner:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tQuestion:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tSeverity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tSource:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tStartDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tStartGap:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tStatus:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tType:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Action */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
          
  run displayData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  for first finding:
    publish "GetSysPropDesc" ("CAM", "Finding", "Severity", finding.severity, output finding.severityDesc).
    assign
        tEntity    :screen-value = finding.entityID
        tEntityName:screen-value = finding.entityName 
        tSeverity  :screen-value = finding.severityDesc
        tSource    :screen-value = finding.source + (if finding.sourceID > "" then " (" + finding.sourceID + ")" else "")
        tQuestion  :screen-value = if finding.sourceQuestion = "?" then "" else finding.sourceQuestion
        tFinding   :screen-value = finding.description
        .
  end.      
  for first action exclusive-lock:
    assign
        tActionID       :screen-value = string(action.actionID)
        tType           :screen-value = getActionType(action.actionType)
        tOwner          :screen-value = action.ownerDesc
        tStatus         :screen-value = getActionStatus(action.stat)
        tCompleteStatus :screen-value = action.completeStatusDesc
        tCreatedDate    :screen-value = string(action.createdDate)
        tStartDate      :screen-value = string(action.startDate)
        tOpenedDate     :screen-value = string(action.openedDate)
        tCompletedDate  :screen-value = string(action.completedDate)
        tFollowupDate   :screen-value = string(action.followupDate)
        tOriginalDueDate:screen-value = string(action.originalDueDate)
        tDueDate        :screen-value = string(action.dueDate)
        tExtendedDate   :screen-value = string(action.extendedDate)
        tAction         :screen-value = action.comments
        tStartGap       :screen-value = (if action.startDate = ? then "" else string(interval(action.startDate, action.openedDate, "days") + 1))
     /*   tType           :read-only    = yes /* not (action.stat = "X" or action.stat = "C") */
        tAction         :read-only    = (action.stat = "X" or action.stat = "C")
       */ .
        run setImage in this-procedure (action.iconSeverity, action.iconMessages).        
      end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEntity tEntityName tSeverity tSource tQuestion tFinding tActionID 
          tCreatedDate tFollowupDate tType tStartDate tDueDate tOwner 
          tOpenedDate tOriginalDueDate tStatus tCompletedDate tExtendedDate 
          tCompleteStatus tStartGap tAction 
      WITH FRAME Dialog-Frame.
  ENABLE tEntity tEntityName tSeverity tSource tQuestion tFinding tActionID 
         tCreatedDate tFollowupDate tType tStartDate tDueDate tOwner 
         tOpenedDate tOriginalDueDate tStatus tCompletedDate tExtendedDate 
         tCompleteStatus tStartGap tAction Btn_Cancel RECT-55 iIcon RECT-58 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSeverity Dialog-Frame 
PROCEDURE getSeverity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setImage Dialog-Frame 
PROCEDURE setImage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pSeverity as integer no-undo.
  define input parameter pMessage  as character no-undo.

  do with frame {&frame-name}:
    if pSeverity = 3 then
      iIcon:load-image ("images/smiley-sqare-red-18.gif").
    else if pSeverity = 2 then
      iIcon:load-image ("images/smiley-sqare-yellow-18.gif").
    else if pSeverity = 1 then
      iIcon:load-image ("images/smiley-sqare-green-18.gif").
      
    iIcon:tooltip = replace(trim(pMessage,","),",",chr(10)).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActionStatus Dialog-Frame 
FUNCTION getActionStatus RETURNS CHARACTER
  ( input ipcStatus as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("CAM", "Action", "Status", ipcStatus, output std-ch).
  return std-ch.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getActionType Dialog-Frame 
FUNCTION getActionType RETURNS CHARACTER
  ( input ipcType as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("CAM", "Action", "ActionType", ipcType, output std-ch).
  return std-ch.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

