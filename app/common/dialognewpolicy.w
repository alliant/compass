&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*
 dialognewpolicy.w
 DIALOG to ADD a POLICY outside of a Batch
 4.25.2012 D.Sinclair
 
 Rahul Sharma
 Note: Window moved from app/ops to app/common because 
       it is used from ARM as well as OPS module
 */

def input-output parameter pPolicy as int.
def input-output parameter pAgentID as char.
def input-output parameter pForm as char.

def input-output parameter pFile as char.
def input-output parameter pResidential as logical.
def input-output parameter pLiability as dec.
def input-output parameter pGrossPremium as dec.

def output parameter pAction as char init "UNDO".

{tt/agent.i}
{tt/stateform.i}

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFile tPolicy tForm tLiability tResidential ~
tGrossPremium bSave bCancel tAgent 
&Scoped-Define DISPLAYED-OBJECTS tFile tPolicy tForm tLiability ~
tResidential tGrossPremium tAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setForms fMain 
FUNCTION setForms RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tAgent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","N"
     DROP-DOWN AUTO-COMPLETION
     SIZE 113.4 BY 1 NO-UNDO.

DEFINE VARIABLE tForm AS CHARACTER INITIAL "N" 
     LABEL "Form" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "None","N"
     DROP-DOWN AUTO-COMPLETION
     SIZE 113.4 BY 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 22.2 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 22.2 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tResidential AS LOGICAL INITIAL no 
     LABEL "Residential" 
     VIEW-AS TOGGLE-BOX
     SIZE 15.2 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tFile AT ROW 5.62 COL 16.8 COLON-ALIGNED WIDGET-ID 2
     tPolicy AT ROW 1.43 COL 16.8 COLON-ALIGNED WIDGET-ID 4
     tForm AT ROW 3.71 COL 16.8 COLON-ALIGNED WIDGET-ID 30
     tLiability AT ROW 6.76 COL 16.8 COLON-ALIGNED WIDGET-ID 12
     tResidential AT ROW 5.71 COL 43 WIDGET-ID 38
     tGrossPremium AT ROW 7.91 COL 16.8 COLON-ALIGNED WIDGET-ID 16
     bSave AT ROW 9.43 COL 49.8
     bCancel AT ROW 9.43 COL 67.2
     tAgent AT ROW 2.57 COL 16.8 COLON-ALIGNED WIDGET-ID 40
     SPACE(3.39) SKIP(8.21)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add Policy"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Add Policy */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fMain
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  if tAgent:screen-value in frame fMain = "N" 
   then
    do:
     MESSAGE "Please select an Agent."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return no-apply.
    end.

  if tForm:screen-value in frame fMain = "N" 
   then
    do:
     MESSAGE "Please select a Policy Form."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return no-apply.
    end.

  if tPolicy:input-value in frame fMain = 0 
   then
    do:
     MESSAGE "Please enter a non-zero Policy number."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return no-apply.
    end.

  pAction = "SAVE".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgent fMain
ON VALUE-CHANGED OF tAgent IN FRAME fMain /* Agent */
DO:
  setForms().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/get-agent-list.i &combo=tAgent}

publish "GetStateForms" (output table stateform).

ASSIGN
  tFile = pFile
  tPolicy = pPolicy
  tForm = pForm
  tLiability = pLiability
  tResidential = pResidential
  tGrossPremium = pGrossPremium
  .

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
Do ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/set-current-value.i &agent=tAgent}
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if pAction = "SAVE" 
   then 
    do: assign
          pPolicy = tPolicy:input-value in frame fMain
          pAgentID = tAgent:screen-value in frame fMain
          pFile = tFile:screen-value in frame fMain
          pForm = tForm:screen-value in frame fMain
          pLiability = tLiability:input-value in frame fMain
          pResidential = tResidential:checked in frame fMain
          pGrossPremium = tGrossPremium:input-value in frame fMain
          .
    end.
  
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFile tPolicy tForm tLiability tResidential tGrossPremium tAgent 
      WITH FRAME fMain.
  ENABLE tFile tPolicy tForm tLiability tResidential tGrossPremium bSave 
         bCancel tAgent 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setForms fMain 
FUNCTION setForms RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 std-ch = "".
 find agent
   where agent.agentID = tAgent:screen-value in frame fMain no-error.
 MESSAGE tAgent:screen-value in frame fMain skip
         available agent
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
 if not available agent
  then
   do: tForm:list-item-pairs = "None,N".
       tForm:screen-value = "N".
       return false.
   end.

 for each stateform
   where stateform.stateID = agent.stateID
     and stateform.formType = "P"
   by stateform.formID:
  std-ch = std-ch + "," + replace(stateform.description, ",", " ")
                        + " (" + stateform.formID + ")"
                        + "," + stateform.formID.
 end.
 std-ch = trim(std-ch, ",").
 if num-entries(std-ch) <= 1 
  then std-ch = "None,N".
 tForm:list-item-pairs in frame fMain = std-ch.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

