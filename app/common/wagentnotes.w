&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Notes (wagentnotes.w)
@description The agent notes window to show the notes as a scrollable
             list
             
@param FileDataSrv;handle;The handle to the agent's file data procedure

@author John Oliver
@version 1.0
@created 2017/04/05
@notes 
---------------------------------------------------------------------*/


/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.
define input parameter hFileDataSrv as handle no-undo.

/* ***************************  Definitions  ************************** */

{tt/agent.i}
{tt/agentnote.i}
{lib/std-def.i}
{lib/add-delimiter.i}

define variable hModifyWindow as handle no-undo.
define variable cAgentID as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rFilter bModifyNote tDescription bFilter ~
bClear tViewNoteCategory tViewNoteType tViewNoteOrder fSearch editorNotes ~
bRefresh bAddNote 
&Scoped-Define DISPLAYED-OBJECTS tDescription tViewNoteCategory ~
tViewNoteType tViewNoteOrder fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SearchNote C-Win 
FUNCTION SearchNote RETURNS LOGICAL
  ( input hBuffer as handle,
    input pSearch as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddNote  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new note".

DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 10.2 BY 1.14.

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 10.2 BY 1.14.

DEFINE BUTTON bModifyNote  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify notes".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh notes".

DEFINE VARIABLE tViewNoteCategory AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 21.6 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE tViewNoteType AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE editorNotes AS LONGCHAR 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 160 BY 25
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL " Agent" 
     VIEW-AS FILL-IN 
     SIZE 130 BY 1 NO-UNDO.

DEFINE VARIABLE tViewNoteOrder AS CHARACTER INITIAL "D" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Asc", "A",
"Desc", "D"
     SIZE 16.6 BY 1 TOOLTIP "Select to display notes in date oldest first (Asc) or newest first (Des) order" NO-UNDO.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 160.2 BY 1.91.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bModifyNote AT ROW 1.24 COL 147.6 WIDGET-ID 306 NO-TAB-STOP 
     tDescription AT ROW 1.62 COL 7 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     bFilter AT ROW 3.71 COL 140.2 WIDGET-ID 64
     bClear AT ROW 3.71 COL 150.6 WIDGET-ID 68
     tViewNoteCategory AT ROW 3.81 COL 11.4 COLON-ALIGNED WIDGET-ID 240 NO-TAB-STOP 
     tViewNoteType AT ROW 3.81 COL 42 COLON-ALIGNED WIDGET-ID 310 NO-TAB-STOP 
     tViewNoteOrder AT ROW 3.81 COL 71 NO-LABEL WIDGET-ID 242 NO-TAB-STOP 
     fSearch AT ROW 3.81 COL 95 COLON-ALIGNED WIDGET-ID 62
     editorNotes AT ROW 5.19 COL 2 NO-LABEL WIDGET-ID 200
     bRefresh AT ROW 1.24 COL 155 WIDGET-ID 308 NO-TAB-STOP 
     bAddNote AT ROW 1.24 COL 140.2 WIDGET-ID 224 NO-TAB-STOP 
     "Sort:" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 4 COL 66 WIDGET-ID 300
     rFilter AT ROW 3.38 COL 2 WIDGET-ID 298
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 162 BY 29.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Notes"
         HEIGHT             = 29.43
         WIDTH              = 162
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR EDITOR editorNotes IN FRAME DEFAULT-FRAME
   NO-DISPLAY                                                           */
ASSIGN 
       editorNotes:RETURN-INSERTED IN FRAME DEFAULT-FRAME  = TRUE
       editorNotes:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDescription:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* File Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* File Notes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* File Notes */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddNote C-Win
ON CHOOSE OF bAddNote IN FRAME DEFAULT-FRAME /* Add */
DO:
  publish "ActionWindowForAgent" (cAgentID,"notesAdd","dialogagentnoteadd.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME DEFAULT-FRAME /* Clear */
DO:
  assign
    fSearch:screen-value = ""
    tViewNoteCategory:screen-value = "ALL"
    tViewNoteOrder:screen-value = "D"
    .
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME DEFAULT-FRAME /* Filter */
DO:
  run FilterNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyNote C-Win
ON CHOOSE OF bModifyNote IN FRAME DEFAULT-FRAME /* Modify */
DO:
  publish "ActionWindowForAgent" (cAgentID,"notesModify","dialogagentnotemodify.w").
   
  run FilterNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
 run LoadAgentNotes in hFileDataSrv.
 apply "CHOOSE" to bClear. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewNoteCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewNoteCategory C-Win
ON VALUE-CHANGED OF tViewNoteCategory IN FRAME DEFAULT-FRAME /* Category */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewNoteOrder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewNoteOrder C-Win
ON VALUE-CHANGED OF tViewNoteOrder IN FRAME DEFAULT-FRAME
DO:
  run FilterNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewNoteType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewNoteType C-Win
ON VALUE-CHANGED OF tViewNoteType IN FRAME DEFAULT-FRAME /* Status */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "NoteChanged" anywhere.
subscribe to "CloseWindow" anywhere.

if valid-handle(hFileDataSrv)
 then run GetAgent in hFileDataSrv (output table agent).

{lib/win-main.i}
  
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       

assign
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels = session:width-pixels
  .
  
bAddNote:load-image-up("images/blank-add.bmp").
bAddNote:load-image-insensitive("images/blank-i.bmp").
bModifyNote:load-image-up("images/update.bmp").
bModifyNote:load-image-insensitive("images/update-i.bmp").
bRefresh:load-image-up("images/sync.bmp").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* note category */
{lib/get-sysprop-list.i &combo=tViewNoteCategory &appCode="'AMD'" &objAction="'AgentNote'" &objProperty="'Category'" &addAll=true}

/* note type */
{lib/get-sysprop-list.i &combo=tViewNoteType &appCode="'AMD'" &objAction="'AgentNote'" &objProperty="'Type'" &addAll=true}

for first agent no-lock:
  assign
    cAgentID = agent.agentID
    {&window-name}:title = "Notes for " + agent.name
    .
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  for first agent no-lock:
    tDescription:screen-value = agent.name + " (" + cAgentID + ")".
  end.
  
  run FilterNotes in this-procedure.
  run WindowResized in this-procedure.
  run ShowWindow in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "WINDOW-CLOSE" to {&window-name}.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDescription tViewNoteCategory tViewNoteType tViewNoteOrder fSearch 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE rFilter bModifyNote tDescription bFilter bClear tViewNoteCategory 
         tViewNoteType tViewNoteOrder fSearch editorNotes bRefresh bAddNote 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterNotes C-Win 
PROCEDURE FilterNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable tQuery as character no-undo.
  define variable cCategoryDesc as character no-undo.
  define variable cTypeDesc as character no-undo.
  define variable cUsername as character no-undo.
  define variable cSubject as character no-undo.
  define variable tNote as character no-undo.
  define variable tNotes as longchar no-undo.
  define variable isValid as logical no-undo.
  
  if valid-handle(hFileDataSrv)
   then run GetAgentNotes in hFileDataSrv (output table agentnote).
  
  /* get the category */
  do with frame {&frame-name}:
    /* create the query */
    tQuery = "for each agentnote by noteDate".
    if tViewNoteOrder:screen-value = "D"
     then tQuery = tQuery + " descending".
    
    create buffer hBuffer for table "agentnote".
    create query hQuery.
    hQuery:set-buffers(hBuffer).
    hQuery:query-prepare(tQuery).
    hQuery:query-open().
    hQuery:get-first().
    repeat while not hQuery:query-off-end:
      assign
        isValid = true
        .
      /* test if the record is the category */
      std-ch = hBuffer:buffer-field("category"):buffer-value().
      if tViewNoteCategory:screen-value <> "ALL" and std-ch <> tViewNoteCategory:screen-value
       then isValid = false.
       else publish "GetNoteCategoryDesc" (std-ch, output cCategoryDesc).
      /* test is the record is the type */
      std-ch = hBuffer:buffer-field("noteType"):buffer-value().
      if tViewNoteType:screen-value <> "ALL" and std-ch <> tViewNoteType:screen-value
       then isValid = false.
       else publish "GetNoteTypeDesc" (std-ch, output cTypeDesc).
      /* test if the record contains the search text */
      if fSearch:screen-value > "" and not SearchNote(hBuffer, fSearch:screen-value)
       then isValid = false.
 
      if isValid
       then
        do:
          /* get the user's name */
          std-ch = hBuffer:buffer-field("uid"):buffer-value().
          publish "GetSysUserName" (std-ch, output cUsername).
          if cUsername = "NOT FOUND"
           then cUsername = std-ch.
          /* get the subject */
          std-ch = hBuffer:buffer-field("subject"):buffer-value().
          if std-ch > "" or std-ch = ?
           then cSubject = "" + std-ch.
           else cSubject = "".
          assign
            /* build the note */
            tNote = ""
            tNote = "[  " + string(hBuffer:buffer-field("noteDate"):buffer-value(), "99/99/9999 HH:MM AM  ")
            tNote = tNote + "|  " + caps(cUsername)
                          + (if cTypeDesc > "" then "  |  " else "") 
                          + caps(cTypeDesc)
                          + (if cCategoryDesc > "" then ": " else "") 
                          + caps(cCategoryDesc)
                          + (if cSubject > "" then "  |  " else "")  + caps(cSubject) + "  ]" 
                          + chr(10) + chr(10)
            tNote = tNote + hBuffer:buffer-field("notes"):buffer-value() + chr(10) + chr(10)
            tNotes = tNotes + tNote
            .
        end.
        hQuery:get-next().
    end.
    hQuery:query-close().
    delete object hQuery no-error.
    
    editorNotes:screen-value = tNotes.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NoteChanged C-Win 
PROCEDURE NoteChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  if can-find(first agent where agentID = pAgentID)
   then run FilterNotes in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      frame {&frame-name}:width-pixels = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* note resize */
      editorNotes:height-pixels = frame {&frame-name}:height-pixels - editorNotes:y - 5
      editorNotes:width-pixels = frame {&frame-name}:width-pixels - 10
      /* filter resize */
      rFilter:width-pixels = frame {&frame-name}:width-pixels - 10
      bClear:x = frame {&frame-name}:width-pixels - bClear:width-pixels - 12
      bFilter:x = bClear:x - bFilter:width-pixels - 1
      fSearch:width-pixels = (bFilter:x - fSearch:x) - 5
      /* top buttons and agent name */
      bRefresh:x = frame {&frame-name}:width-pixels - bRefresh:width-pixels - 5
      bModifyNote:x = bRefresh:x - bModifyNote:width-pixels - 1
      bAddNote:x = bModifyNote:x - bAddNote:width-pixels - 1
      tDescription:width-pixels = (bAddNote:x - tDescription:x) - 5
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SearchNote C-Win 
FUNCTION SearchNote RETURNS LOGICAL
  ( input hBuffer as handle,
    input pSearch as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  return hBuffer:buffer-field("notes"):buffer-value() matches "*" + pSearch + "*" or
         hBuffer:buffer-field("subject"):buffer-value() matches "*" + pSearch + "*".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

