&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wbatchform.w
   Window of STAT codes
   4.23.2012
   
   Rahul Sharma
   Note: Window moved from app/ops to app/common because 
         it is used from ARM as well as OPS module
   */

CREATE WIDGET-POOL.

{tt/batchform.i}
{tt/batchform.i &tableAlias=allBatchForm}

{lib/std-def.i}

def var hData as handle no-undo.

def var tBatchID as char no-undo.

{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batchform

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData batchform.seq batchform.fileNumber batchform.zipcode batchform.policyID batchform.formType batchform.formID batchform.formCount batchform.statCode batchform.effDate batchform.liabilityAmount batchform.grossPremium batchform.netPremium batchform.retentionPremium batchform.grossDelta batchform.netDelta batchform.retentionDelta batchform.reprocess batchform.validform batchform.validMsg   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH batchform
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH batchform.
&Scoped-define TABLES-IN-QUERY-brwData batchform
&Scoped-define FIRST-TABLE-IN-QUERY-brwData batchform


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fBatchID fSearch brwData bRefresh 
&Scoped-Define DISPLAYED-OBJECTS fBatchID fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear filter".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View batch details as PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get batch details".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Apply search filter".

DEFINE VARIABLE fBatchID AS INTEGER FORMAT ">>>>>>>>":U INITIAL 0 
     LABEL "Batch" 
     VIEW-AS FILL-IN 
     SIZE 20.6 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 30.4 BY 1 TOOLTIP "Criteria for File or Policy; press <Return> to apply the filter" NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40 BY 2.62.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58.4 BY 2.62.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 18.2 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      batchform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      batchform.seq column-label "Seq" format "zzzz9"
batchform.fileNumber column-label "File" format "x(30)" width 18 
batchform.zipcode COLUMN-LABEL "Zip Code"
batchform.policyID column-label "Policy" format "zzzzzzzzz"
batchform.formType column-label "Type" format "X(2)"
batchform.formID column-label "Form" format "x(15)"
batchform.formCount column-label "Count" format "Z"
batchform.statCode column-label "STAT!Code" format "x(10)"
batchform.effDate column-label "Effective!Date" format "99/99/9999"
batchform.liabilityAmount column-label "Liability" format "-zzz,zzz,zz9.99"
batchform.grossPremium column-label "Processed!Gross" format "-z,zzz,zz9.99"
batchform.netPremium column-label "Processed!Net" format "-z,zzz,zz9.99"
batchform.retentionPremium column-label "Processed!Retention" format "-z,zzz,zz9.99"
batchform.grossDelta column-label "Accounting!Gross" format "-z,zzz,zz9.99"
batchform.netDelta column-label "Accounting!Net" format "-z,zzz,zz9.99"
batchform.retentionDelta column-label "Accounting!Retention" format "-z,zzz,zz9.99"
batchform.reprocess column-label "Redo" format "Yes/No"
batchform.validform column-label "Verified" format "x(2)"
batchform.validMsg column-label "Messages" format "x(100)" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 235 BY 17.29 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bSearch AT ROW 1.95 COL 104.8 WIDGET-ID 72 NO-TAB-STOP 
     fBatchID AT ROW 2.29 COL 9 COLON-ALIGNED WIDGET-ID 10
     bClear AT ROW 1.95 COL 112 WIDGET-ID 70 NO-TAB-STOP 
     fSearch AT ROW 2.29 COL 71.6 COLON-ALIGNED WIDGET-ID 66
     brwData AT ROW 4.29 COL 2 WIDGET-ID 200
     bExport AT ROW 1.91 COL 52.4 WIDGET-ID 2 NO-TAB-STOP 
     bPrint AT ROW 1.91 COL 45.2 WIDGET-ID 14 NO-TAB-STOP 
     bRefresh AT ROW 1.91 COL 32.6 WIDGET-ID 4 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 63.6 WIDGET-ID 60
     "Actions" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 1.14 COL 44.2 WIDGET-ID 64
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 62.6 WIDGET-ID 58
     RECT-38 AT ROW 1.48 COL 43.2 WIDGET-ID 62
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 237.2 BY 20.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Batch Details"
         HEIGHT             = 20.81
         WIDTH              = 237.2
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bClear IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrint IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSearch IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fSearch:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-38 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchform.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Batch Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Batch Details */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Batch Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
 fSearch:screen-value in frame {&frame-name} = "".

 run filterData ("").

 dataSortDesc = not dataSortDesc.
 run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if available batchform and batchform.policyID <> 0
   then
    do: 
        publish "SetCurrentValue" ("PolicyID", string(batchform.policyID)).
        run wpolicy.w persistent.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Filter */
DO:
 run filterData (fSearch:screen-value in frame {&frame-name}).

 dataSortDesc = not dataSortDesc.
 run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fBatchID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fBatchID C-Win
ON ANY-PRINTABLE OF fBatchID IN FRAME fMain /* Batch */
DO:
  close query brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fBatchID C-Win
ON RETURN OF fBatchID IN FRAME fMain /* Batch */
DO:
  apply "CHOOSE" to bRefresh in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:

 run filterData (self:screen-value).

 dataSortDesc = not dataSortDesc.
 run sortData (dataSortBy).
 return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bPrint:load-image("images/pdf.bmp").
bPrint:load-image-insensitive("images/pdf-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

publish "GetCurrentValue" ("BatchID", output tBatchID).
assign fBatchID = int(tBatchID) no-error.
if error-status:error or tBatchID = "" or tBatchID = ? then
assign fBatchID = 0.

/* run getData in this-procedure. */

run windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

publish "GetAutoView" (output std-lo).
if std-lo
  and fBatchID:screen-value in frame fMain > "" 
 then run getData.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fBatchID fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fBatchID fSearch brwData bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.

 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).
 
 th = temp-table batchform:handle.
 
 run util/exporttable.p (table-handle th,
                         "batchform",
                         "for each batchform by batchform.seq",
                         "batchID,seq,fileNumber,policyID,formType,formID,statCode,effDate,countyID,liabilityAmount,grossPremium,netPremium,retentionPremium,liabilityDelta,grossDelta,netDelta,retentionDelta,reprocess,validform,validmsg",
                         "Batch ID,Seq,File,Policy,Type,Form,STAT,Eff Date,County,Liability,Processed Gross,Processed Net,Processed Retention,Acct Liability,Acct Gross,Acct Net,Acct Retention,Redo,Verified,Messages",
                         std-ch,
                         "BatchDetails_" + fBatchID:screen-value in frame fMain 
                          + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pSearch as char.

 close query brwData.
 empty temp-table batchform.
 hide message no-pause.

 if pSearch = "" 
  then std-ch = "*".
  else std-ch = "*" + pSearch + "*".

 for each allbatchform:
  if allbatchform.fileNumber matches std-ch
   then .
   else
  if string(allbatchform.policyID) matches std-ch
   then .
   else next.

  create batchform.
  buffer-copy allbatchform to batchform.
 end.

 if pSearch = ""
  then std-ch = "".
  else std-ch = "Filtered where File or Policy matches " + pSearch.
 message std-ch.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer batchform for batchform.
 def var tFile as char no-undo.
 
 hide message no-pause.
 message "Getting data...".
 
 if fBatchID:screen-value in frame fMain = ""
 or fBatchID:screen-value in frame fMain = "0"
  then
   do:
       MESSAGE "Batch cannot be blank or zero"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 close query brwData.
 empty temp-table batchform.
 empty temp-table allbatchform.

 assign
   bPrint:sensitive in frame {&frame-name} = false
   bExport:sensitive in frame {&frame-name} = false
   fSearch:screen-value = ""
   fSearch:read-only = true
   bClear:sensitive in frame {&frame-name} = false
   bSearch:sensitive in frame {&frame-name} = false
   .

 publish "GetBatchForms" (input fBatchID:screen-value in frame fMain,
                          output table allbatchform).

 run filterData ("").
  
 std-lo = can-find(first batchForm where formCount > 1). 
 std-ha = brwData:first-column.  

 BLK:
 do while valid-handle(std-ha):                   
     if std-ha:name = "FormCount" 
      then 
       do:
         std-ha:visible = std-lo.
         leave BLK.
       end.
                 
     std-ha = std-ha:next-column.                   
 end.
  
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("seq").
 
 std-ch = string(query brwData:num-results) 
    + " records for batch " 
    + fBatchID:screen-value in frame fMain
    + "  (" + string(now,"99/99/99 HH:MM:SS") + ")".
 
 hide message no-pause.
 message std-ch.

 assign
   bPrint:sensitive in frame {&frame-name} = true
   bExport:sensitive in frame {&frame-name} = true
   fSearch:read-only = false
   bClear:sensitive in frame {&frame-name} = true
   bSearch:sensitive in frame {&frame-name} = true
   .

 publish "SetCurrentValue" ("BatchID", fBatchID:screen-value in frame fMain).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwData:num-results = 0 or not available batchform
  then
   do: 
    MESSAGE "There is nothing to print"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 run ops01-r.p (table batchform).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

