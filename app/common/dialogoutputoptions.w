&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: common\dialogOutputOptions.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define output parameter opcOutputOptions as character no-undo.
/* define output parameter opcOutputFormat  as character no-undo. */

/* Local Variable Definitions ---                                       */
define variable cCurrentUID as character no-undo.

{lib/std-def.i}
{lib/ar-def.i}
{lib/validEmailAddr.i}
 /*opcOutputOptions =
  'I8=Q^I8a=Y^I9=C^I9E=agupta@alliantnational.com^I9P=something'*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rsOutputActionType tgNotify btDone 
&Scoped-Define DISPLAYED-OBJECTS rbReportFromat rsOutputActionType fEmailID ~
cbPrinter tgNotify 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON btDone AUTO-GO 
     LABEL "Done" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbPrinter AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 46.8 BY 1 NO-UNDO.

DEFINE VARIABLE fEmailID AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.8 BY 1 TOOLTIP "Email address" NO-UNDO.

DEFINE VARIABLE rbReportFromat AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "PDF", "P",
"CSV", "C"
     SIZE 8.8 BY 2.14 NO-UNDO.

DEFINE VARIABLE rsOutputActionType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Agent", "A",
"Email", "E",
"Printer", "P"
     SIZE 11 BY 3.19 NO-UNDO.

DEFINE VARIABLE tgNotify AS LOGICAL INITIAL no 
     LABEL "Notify once done" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     rbReportFromat AT ROW 1.95 COL 9 NO-LABEL WIDGET-ID 424
     rsOutputActionType AT ROW 1.91 COL 33 NO-LABEL WIDGET-ID 446
     fEmailID AT ROW 3.05 COL 42 COLON-ALIGNED NO-LABEL WIDGET-ID 454
     cbPrinter AT ROW 4.24 COL 42 COLON-ALIGNED NO-LABEL WIDGET-ID 472
     tgNotify AT ROW 4.43 COL 4 WIDGET-ID 474
     btDone AT ROW 6.71 COL 39
     "Format" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 428
     "Destination" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.24 COL 25 WIDGET-ID 452
     "Destination is based on preferences setup" VIEW-AS TEXT
          SIZE 41 BY .62 AT ROW 2.1 COL 44 WIDGET-ID 468
     SPACE(8.39) SKIP(5.70)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Output Options" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cbPrinter IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fEmailID IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET rbReportFromat IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Output Options */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btDone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btDone Dialog-Frame
ON CHOOSE OF btDone IN FRAME Dialog-Frame /* Done */
DO:
  run setOutputOptions in this-procedure  (output std-ch).
  if std-ch <> ''
   then
    return no-apply.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsOutputActionType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsOutputActionType Dialog-Frame
ON VALUE-CHANGED OF rsOutputActionType IN FRAME Dialog-Frame
DO:
  run setDestinationButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetCredentialsID"   (output cCurrentUID).
publish "GetSysPropList"     ({&printappcode},
                              {&printobjAction},
                              {&printProperty},
                              output std-ch).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
 
  RUN enable_UI.
  if std-ch > ""
   then 
    cbPrinter:list-item-pairs = trim(replace(std-ch,"^",","),",").  
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY rbReportFromat rsOutputActionType fEmailID cbPrinter tgNotify 
      WITH FRAME Dialog-Frame.
  ENABLE rsOutputActionType tgNotify btDone 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setDestinationButtons Dialog-Frame 
PROCEDURE setDestinationButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      fEmailID:sensitive       = false
      fEmailID:screen-value    = ""
      cbPrinter:sensitive      = false
      rbReportFromat:sensitive = true
      .     
  
  case rsOutputActionType:screen-value:
    when {&agent} 
     then
      do:
        assign
            rbReportFromat:screen-value = "P"
            rbReportFromat:sensitive    = false
            .
      end.
    when {&email} 
     then
      do:
        assign
/*             bQueue:tooltip        = "Send email" */
            fEmailID:sensitive    = true
            fEmailID:screen-value = cCurrentUID
            .  
      end.
    when {&printer} 
     then 
      do:
        assign
/*             bQueue:tooltip              = "Print" */
            cbPrinter:sensitive         = true
            rbReportFromat:screen-value = "P"
            rbReportFromat:sensitive    = false
            .
      end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setOutputOptions Dialog-Frame 
PROCEDURE setOutputOptions PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE OUTPUT PARAMETER opError  AS CHARACTER   NO-UNDO.

  do with frame dialog-frame:
  end.

  if rsOutputActionType:input-value = {&email} 
   then   
    do:
     if fEmailID:screen-value = ""
      then
       do:
         opError = "Email ID cannot be blank.".
         return.
       end.
      
     if fEmailID:screen-value <> "" and not validEmailAddr(fEmailID:screen-value)
      then 
       do:   
         opError = "Email ID is Invalid.".
         return. 
       end.  
    end.
    
  if rsOutputActionType:input-value = {&printer} 
   then   
    do:
     if cbPrinter:screen-value = ""
      then
       do:
         opError = "Printer name cannot be blank.".
         return.
       end.      
    end.    

  if rbReportFromat:input-value = 'C' and 
     (rsOutputActionType:input-value = 'A' or rsOutputActionType:input-value = 'P') 
   then 
      message "A CSV file can only be emailed." view-as alert-box error buttons ok.
   
   assign
     opcOutputOptions = 
       'I8=Q&I8a=' + 
       (if tgNotify:input-value then 'Yes' else 'No') +
       '&I9='  +
       (if rsOutputActionType:input-value = 'A' then 'C' else 'S') +
       '&I9E=' +
       (if fEmailID:input-value <> '' then fEmailID:input-value else '') +
       '&I9P=' +
       (if cbPrinter:input-value <> '' then cbPrinter:input-value else '') +
       '&I10=' +
        rbReportFromat:input-value.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

