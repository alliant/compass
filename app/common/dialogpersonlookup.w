&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogpersonlookup.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created:12/31/19
  
  @Modified:
  Date         Name         Description
  08/06/2020   AG           Change Title and removed Cancel button.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/ar-def.i}

{tt/state.i}
{tt/person.i}
{tt/person.i &tableAlias=ttperson}

/* Parameters Definitions ---                                           */
define input  parameter ipcpersonID   as  character no-undo.
define input  parameter lAddNew       as  logical   no-undo.
define input  parameter lAddBlank     as  logical   no-undo.
define input  parameter lAddAll       as  logical   no-undo.
define output parameter opcpersonID   as  character no-undo.
define output parameter opcName       as  character no-undo.
define output parameter oplSuccess    as  logical   no-undo.

define variable stateList as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwPerson

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES person

/* Definitions for BROWSE brwPerson                                     */
&Scoped-define FIELDS-IN-QUERY-brwPerson person.state "State" person.personID "Person ID" person.dispname "Name"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwPerson   
&Scoped-define SELF-NAME brwPerson
&Scoped-define QUERY-STRING-brwPerson preselect each person
&Scoped-define OPEN-QUERY-brwPerson open query brwperson preselect each person.
&Scoped-define TABLES-IN-QUERY-brwPerson person
&Scoped-define FIRST-TABLE-IN-QUERY-brwPerson person


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwPerson}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState fSearch bSearch brwPerson Btn_OK 
&Scoped-Define DISPLAYED-OBJECTS cbState fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search Locks".

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Select" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 59.2 BY 1 TOOLTIP "Search Criteria (Code Type, Code, Type, Description)" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwPerson FOR 
      person SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwPerson Dialog-Frame _FREEFORM
  QUERY brwPerson DISPLAY
      person.state       label        "State"             format "x(12)"      width 8     
person.personID          label        "Person ID"         format "x(5)"       width 12
person.dispname          label        "Name"              format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 98 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbState AT ROW 1.86 COL 9 COLON-ALIGNED WIDGET-ID 242
     fSearch AT ROW 1.86 COL 33 COLON-ALIGNED WIDGET-ID 66
     bSearch AT ROW 1.76 COL 94.4 WIDGET-ID 314 NO-TAB-STOP 
     brwPerson AT ROW 3.71 COL 3 WIDGET-ID 300
     Btn_OK AT ROW 16.33 COL 44
     SPACE(42.99) SKIP(0.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Person"
         DEFAULT-BUTTON Btn_OK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwPerson bSearch Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwPerson:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE
       brwPerson:COLUMN-MOVABLE IN FRAME Dialog-Frame         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwPerson
/* Query rebuild information for BROWSE brwPerson
     _START_FREEFORM
open query brwperson preselect each person.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwPerson */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Select Person */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwPerson
&Scoped-define SELF-NAME brwPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPerson Dialog-Frame
ON DEFAULT-ACTION OF brwPerson IN FRAME Dialog-Frame
do:
  apply "Choose" to Btn_OK.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPerson Dialog-Frame
ON ROW-DISPLAY OF brwPerson IN FRAME Dialog-Frame
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPerson Dialog-Frame
ON START-SEARCH OF brwPerson IN FRAME Dialog-Frame
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch Dialog-Frame
ON CHOOSE OF bSearch IN FRAME Dialog-Frame /* Search */
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Select */
do:
  run setpersonID in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState Dialog-Frame
ON VALUE-CHANGED OF cbState IN FRAME Dialog-Frame /* State */
do:
  run filterData in this-procedure.       
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch Dialog-Frame
ON return OF fSearch IN FRAME Dialog-Frame /* Search */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&FRAME-NAME}:parent eq ?
 then
  frame {&FRAME-NAME}:parent = active-window.

bSearch:load-image("images/s-magnifier.bmp").
bSearch:load-image-insensitive("images/s-magnifier-i.bmp").
 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.
  
  run getData in this-procedure.
  
  apply "VALUE-CHANGED":U to brwPerson.

  wait-for go of frame {&FRAME-NAME}.
END.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState fSearch 
      WITH FRAME Dialog-Frame.
  ENABLE cbState fSearch bSearch brwPerson Btn_OK 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData Dialog-Frame 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer ttperson for ttperson.
  
  close query brwperson.  
  empty temp-table person.
  
  if ipcpersonID = "" and lAddNew
   then
    do:  
      create person.
      assign 
          person.state      = "N/A"
          person.personID   = "-99"
          person.dispname   = "New person"
          .   
    end.  
  if (ipcpersonID <> "" and ipcpersonID <> {&ALL} and lAddBlank )
   then
    do:
      create person.
      assign 
          person.state     = "N/A"
          person.personID  = ""
          person.dispname  = "None"
          .
    end.
    
  if lAddAll
   then
    do:
      create person.
      assign 
          person.state       = "N/A"
          person.personID    = ""
          person.dispname    = "ALL"
          . 
    end.
  
  for each ttperson where ttperson.state = (if cbState:input-value <> {&ALL} then cbState:input-value
                                            else ttperson.state) and
                                            ((if fSearch:input-value <> "" then ttperson.dispname matches ("*" + fSearch:input-value + "*")
                                             else ttperson.dispname = ttperson.dispname) or 
                                            (if fSearch:input-value <> "" then ttperson.personID matches ("*" + fSearch:screen-value + "*")
                                             else ttperson.personID = ttperson.personID)):
    
    create person.
    buffer-copy ttperson to person.
  end.
  
  open query brwperson for each person by person.personId.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.
    
  publish "getpersons" (output table ttperson).
  
  stateList = "ALL".
  for each ttperson break by ttPerson.state:
    if first-of(ttPerson.state) 
     then
      stateList = if stateList = "" then ttperson.state else stateList + "," + ttperson.state.  
  end.
  
  cbState:list-items = stateList.
  cbState:screen-value = "ALL".
    
  run filterData in this-procedure.
  
  find first person where person.personID = ipcpersonID no-error.

  if available person
   then      
    reposition brwperson to rowid rowid(person) no-error. 
  
  apply 'entry' to fSearch.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setPersonID Dialog-Frame 
PROCEDURE setPersonID :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if available person
   then
    assign
        opcpersonID   = person.personID
        opcName    = person.dispname
        oplSuccess = yes
        .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = " by person.personID ".
  
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

