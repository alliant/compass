&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Notes (wagentnotesbrowse.w)
@description The agent notes window to show the notes as a browsable
             list
             
@param FileDataSrv;handle;The handle to the agent's file data procedure

@author John Oliver
@version 1.0
@created 2017/04/05
@notes 
---------------------------------------------------------------------*/


/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.
define input parameter hFileDataSrv as handle no-undo.

/* ***************************  Definitions  ************************** */
define variable cAgentID as character no-undo.

{tt/agent.i}
{tt/agentnote.i}
{tt/agentnote.i &tableAlias="tempagentnote"}
{lib/std-def.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentnote

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData agentnote.categoryDesc agentnote.typeDesc agentnote.subject agentnote.noteDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH agentnote by noteDate
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH agentnote by noteDate.
&Scoped-define TABLES-IN-QUERY-brwData agentnote
&Scoped-define FIRST-TABLE-IN-QUERY-brwData agentnote


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData tSubject tNoteText bAddNote ~
tDescription bRefresh 
&Scoped-Define DISPLAYED-OBJECTS tSubject tDescription cmbCategory cmbType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD isModified C-Win 
FUNCTION isModified RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddNote  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new note".

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel changes to the note".

DEFINE BUTTON bModifyNote  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the note".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh notes".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save changes to the note".

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE cmbCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 30 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE cmbType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29.2 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE tNoteText AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 86 BY 13.24
     FONT 5 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 157 BY 1 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Summary" 
     VIEW-AS FILL-IN 
     SIZE 67.8 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      agentnote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      agentnote.categoryDesc column-label "Category" format "x(100)" width 20
agentnote.typeDesc column-label "Type" format "x(100)" width 20
agentnote.subject column-label "Summary" format "x(200)" width 37
agentnote.noteDate column-label "Date" format "99/99/9999 HH:MM:SS" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 115 BY 15.62 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 3.38 COL 2 WIDGET-ID 200
     bSpellCheck AT ROW 3.62 COL 197 WIDGET-ID 326 NO-TAB-STOP 
     bModifyNote AT ROW 1.24 COL 174.8 WIDGET-ID 306 NO-TAB-STOP 
     tSubject AT ROW 4.57 COL 126 COLON-ALIGNED WIDGET-ID 320 NO-TAB-STOP 
     bCancel AT ROW 1.24 COL 189.6 WIDGET-ID 318 NO-TAB-STOP 
     bSave AT ROW 1.24 COL 182.2 WIDGET-ID 316 NO-TAB-STOP 
     tNoteText AT ROW 5.76 COL 118 NO-LABEL WIDGET-ID 312
     bAddNote AT ROW 1.24 COL 167.4 WIDGET-ID 224 NO-TAB-STOP 
     tDescription AT ROW 1.62 COL 7 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     cmbCategory AT ROW 3.38 COL 126 COLON-ALIGNED WIDGET-ID 322 NO-TAB-STOP 
     cmbType AT ROW 3.38 COL 164.6 COLON-ALIGNED WIDGET-ID 328 NO-TAB-STOP 
     bRefresh AT ROW 1.24 COL 197 WIDGET-ID 334 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 204 BY 18.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Notes"
         HEIGHT             = 18.24
         WIDTH              = 204
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModifyNote IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSpellCheck IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cmbCategory IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cmbType IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tDescription:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR EDITOR tNoteText IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNoteText:RETURN-INSERTED IN FRAME fMain  = TRUE
       tNoteText:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tSubject:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentnote by noteDate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Notes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Notes */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddNote C-Win
ON CHOOSE OF bAddNote IN FRAME fMain /* Add */
DO:
  publish "ActionWindowForAgent" (cAgentID,"notesAdd","dialogagentnoteadd.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  doModify(false).
  apply "VALUE-CHANGED" to browse {&browse-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyNote C-Win
ON CHOOSE OF bModifyNote IN FRAME fMain /* Modify */
DO:
  doModify(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run LoadAgentNotes in hFileDataSrv.
  run GetData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available agentnote
   then return.
   
  doModify(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if not available agentnote 
   then
    do with frame {&frame-name}:
      assign
        tNoteText:screen-value = ""
        tSubject:screen-value = ""
        cmbCategory:screen-value = ""
        cmbCategory:sensitive = false
        cmbType:screen-value = ""
        cmbType:sensitive = false
        bModifyNote:sensitive = false
        .
      return.
    end.
   
  do with frame {&frame-name}:
    if lookup(agentnote.category, cmbCategory:list-item-pairs) > 0 
     then cmbCategory:screen-value = agentnote.category.
     else cmbCategory:screen-value = "".
     
    if lookup(agentnote.category, cmbType:list-item-pairs) > 0 
     then cmbType:screen-value = agentnote.noteType.
     else cmbType:screen-value = "".
  
    std-ch = "".
    publish "GetCredentialsID" (output std-ch).
    assign
      tNoteText:screen-value = agentnote.notes
      tSubject:screen-value = agentnote.subject
      bModifyNote:sensitive = (agentnote.uid = std-ch and agentnote.category <> "0")
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  if not available agentnote
   then return.
  
  run SaveNote in this-procedure (agentnote.agentID,agentnote.seq).
  doModify(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck C-Win
ON CHOOSE OF bSpellCheck IN FRAME fMain /* Spell Check */
DO:
  ASSIGN tNoteText.
  run util/spellcheck.p ({&window-name}:handle, input-output tNoteText, output std-lo).
  display tNoteText with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "NoteChanged" anywhere.
subscribe to "CloseWindow" anywhere.

if valid-handle(hFileDataSrv)
 then run GetAgent in hFileDataSrv (output table agent).
 
{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* bRefresh:load-image("images/completed.bmp").               */
/* bRefresh:load-image-insensitive("images/completed-i.bmp"). */
bAddNote:load-image-up("images/blank-add.bmp").
bAddNote:load-image-insensitive("images/blank-i.bmp").
bModifyNote:load-image-up("images/update.bmp").
bModifyNote:load-image-insensitive("images/update-i.bmp").
bSave:load-image-up("images/save.bmp").
bSave:load-image-insensitive("images/save-i.bmp").
bCancel:load-image-up("images/cancel.bmp").
bCancel:load-image-insensitive("images/cancel-i.bmp").
bSpellCheck:load-image-up("images/spellcheck.bmp").
bSpellCheck:load-image-insensitive("images/spellcheck-i.bmp").
bRefresh:load-image-up("images/sync.bmp").

/* note category */
{lib/get-sysprop-list.i &combo=cmbCategory &appCode="'AMD'" &objAction="'AgentNote'" &objProperty="'Category'"}

/* note type */
{lib/get-sysprop-list.i &combo=cmbType &appCode="'AMD'" &objAction="'AgentNote'" &objProperty="'Type'"}

session:immediate-display = yes.

for first agent no-lock:
  assign
    cAgentID = agent.agentID
    {&window-name}:title = "Notes for " + agent.name
    .
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  for first agent no-lock:
    tDescription:screen-value = agent.name + " (" + cAgentID + ")".
  end.
  
  run GetData in this-procedure.
  run WindowResized in this-procedure.
  run ShowWindow in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "WINDOW-CLOSE" to {&window-name}.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSubject tDescription cmbCategory cmbType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData tSubject tNoteText bAddNote tDescription bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def buffer agentnote for agentnote.
  
  empty temp-table agentnote.
  if valid-handle(hFileDataSrv)
   then run GetAgentNotes in hFileDataSrv (output table agentnote).
  
  dataSortBy = "".
  dataSortDesc = no.
  run sortData ("noteDate").
  
  apply "value-changed" to browse {&browse-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NoteChanged C-Win 
PROCEDURE NoteChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  if can-find(first agent where agentID = pAgentID)
   then run GetData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveNote C-Win 
PROCEDURE SaveNote PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pSeq as integer no-undo.

  do with frame {&frame-name}:
    for first agentnote no-lock
        where agentnote.agentID = pAgentID
          and agentnote.seq = pSeq:
      
      create tempagentnote.
      assign
        tempagentnote.agentID = pAgentID
        tempagentnote.seq = pSeq
        tempagentnote.category = cmbCategory:screen-value
        tempagentnote.subject = tSubject:screen-value
        tempagentnote.notes = tNoteText:screen-value
        tempagentnote.noteType = cmbType:screen-value
        tempagentnote.uid = agentnote.uid
        tempagentnote.noteDate = agentnote.noteDate
        .
    end.
    run ModifyAgentNote in hFileDataSrv (table tempagentnote).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
apply "value-changed" to brwData in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      frame {&frame-name}:width-pixels = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* browse resize*/
      browse {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - browse {&browse-name}:y - 5
      /* note resize */
      tNoteText:height-pixels = frame {&frame-name}:height-pixels - tNoteText:y - 5
      tNoteText:width-pixels = frame {&frame-name}:width-pixels - tNoteText:x - 5
      bSpellCheck:x = frame {&frame-name}:width-pixels - bSpellCheck:width-pixels - 5
      tSubject:width-pixels = (bSpellCheck:x - tSubject:x) - 5
      cmbType:width-pixels = (bSpellCheck:x - cmbType:x) - 5
      /* top buttons and agent name */
      bRefresh:x = frame {&frame-name}:width-pixels - bRefresh:width-pixels - 5
      bCancel:x = bRefresh:x - bCancel:width-pixels - 1
      bSave:x = bCancel:x - bSave:width-pixels - 1
      bModifyNote:x = bSave:x - bModifyNote:width-pixels - 1
      bAddNote:x = bModifyNote:x - bAddNote:width-pixels - 1
      tDescription:width-pixels = (bAddNote:x - tDescription:x) - 5
      .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
@description Enables or disables the controls for modification of the note
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
    assign
      cmbCategory:sensitive = pEnable
      cmbType:sensitive = pEnable
      tSubject:read-only = not pEnable
      tNoteText:read-only = not pEnable
      bSave:sensitive = pEnable
      bCancel:sensitive = pEnable
      bSpellCheck:sensitive = pEnable
      bModifyNote:sensitive = not pEnable
      bRefresh:sensitive = not pEnable
      browse {&browse-name}:sensitive = not pEnable
      .
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION isModified C-Win 
FUNCTION isModified RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Tests to see if the note is in modification
------------------------------------------------------------------------------*/
  RETURN bSave:sensitive in frame {&frame-name}.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

