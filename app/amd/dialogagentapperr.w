&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
define variable pNewUID as character no-undo.
define variable pReason as character no-undo.
define variable hDocs as handle no-undo.
define variable lSave as logical no-undo initial false.
{lib/std-def.i}

/* Functions ---                                                        */
{lib/encrypt.i}

/* Temp Tables ---                                                      */
{tt/agent.i}
{tt/attorney.i}
{tt/agentapp.i &tableAlias="agentapp"}
{tt/qaraudit.i &tableAlias="data"}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.qarID data.statDesc data.auditor data.schedStartDate data.auditFinishDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data by data.qarID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data by data.qarID.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tYear tAuditor tPhone tEmail brwData tName ~
tAddress tAddress2 tCity tState tZip bSave bClose bNew bCancel bReassign ~
bDocs 
&Scoped-Define DISPLAYED-OBJECTS tERR tYear tAuditor tPhone tEmail tName ~
tAddress tAddress2 tCity tState tZip 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doNew C-Win 
FUNCTION doNew RETURNS LOGICAL
  ( input pNew as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel the creation of a new ERR".

DEFINE BUTTON bClose 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bDocs 
     LABEL "Docs" 
     SIZE 4.8 BY 1.14 TOOLTIP "View the doucments that the auditors need to use for them to complete the ERR".

DEFINE BUTTON bNew 
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new ERR".

DEFINE BUTTON bReassign 
     LABEL "Reassign" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reassign the ERR to another auditor".

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tAddress AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 62 BY 1 NO-UNDO.

DEFINE VARIABLE tAddress2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 62 BY 1 NO-UNDO.

DEFINE VARIABLE tCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE tEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE tERR AS INTEGER FORMAT ">>>>>>>9":U INITIAL 0 
     LABEL "Current ERR" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 62 BY 1 NO-UNDO.

DEFINE VARIABLE tPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Years Active" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "Zipcode" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.qarID label "QAR ID" format "ZZ999999" width 12
 data.statDesc label "Status" format "x(20)" width 12
 data.auditor  label "Auditor" format "x(100)" width 18
 data.schedStartDate label "Start Date" format "99/99/9999" width 13
 data.auditFinishDate label "End Date" format "99/99/9999" width 13
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 75 BY 6.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tERR AT ROW 1.48 COL 14 COLON-ALIGNED WIDGET-ID 2
     tYear AT ROW 2.67 COL 14 COLON-ALIGNED WIDGET-ID 178
     tAuditor AT ROW 2.67 COL 42 COLON-ALIGNED WIDGET-ID 180
     tPhone AT ROW 3.86 COL 14 COLON-ALIGNED WIDGET-ID 14
     tEmail AT ROW 3.86 COL 42 COLON-ALIGNED WIDGET-ID 18
     brwData AT ROW 2.67 COL 3 WIDGET-ID 200
     tName AT ROW 5.05 COL 14 COLON-ALIGNED WIDGET-ID 172
     tAddress AT ROW 6.24 COL 14 COLON-ALIGNED WIDGET-ID 20
     tAddress2 AT ROW 7.43 COL 14 COLON-ALIGNED NO-LABEL WIDGET-ID 22
     tCity AT ROW 8.62 COL 14 COLON-ALIGNED NO-LABEL WIDGET-ID 170
     tState AT ROW 8.62 COL 39 COLON-ALIGNED NO-LABEL WIDGET-ID 174
     tZip AT ROW 8.62 COL 63 COLON-ALIGNED NO-LABEL WIDGET-ID 176
     bSave AT ROW 9.81 COL 25 WIDGET-ID 6
     bClose AT ROW 9.81 COL 41 WIDGET-ID 184
     bNew AT ROW 1.43 COL 58.4 WIDGET-ID 8
     bCancel AT ROW 1.43 COL 63.4 WIDGET-ID 10
     bReassign AT ROW 1.43 COL 68.4 WIDGET-ID 186
     bDocs AT ROW 1.43 COL 73.4 WIDGET-ID 182
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 78.8 BY 10.29 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "ERR for Agent"
         HEIGHT             = 10.29
         WIDTH              = 78.8
         MAX-HEIGHT         = 29.71
         MAX-WIDTH          = 80
         VIRTUAL-HEIGHT     = 29.71
         VIRTUAL-WIDTH      = 80
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tEmail fMain */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN tERR IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tERR:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data by data.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* ERR for Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* ERR for Agent */
DO:
  if not lSave
   then {lib/confirm-close.i "Agent Application ERR"}
   
  if valid-handle(hDocs)
   then run CloseWindow in hDocs no-error.
  
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  doNew(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose C-Win
ON CHOOSE OF bClose IN FRAME fMain /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocs C-Win
ON CHOOSE OF bDocs IN FRAME fMain /* Docs */
DO:
  if not can-find(first agentapp)
   then return.
    

  if valid-handle(hDocs)
   then run ShowWindow in hDocs no-error.
   else 
    for first agentapp no-lock:
      run documents.w persistent set hDocs ("AMD",
                                            "/PreSign/" + agentapp.agentID,
                                            "Agent " + agentapp.agentID + " ERR Presign",
                                            "new,delete,open,modify,share,send,request",   /* permissions */
                                            "",
                                            "",
                                            "").
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  doNew(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReassign
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReassign C-Win
ON CHOOSE OF bReassign IN FRAME fMain /* Reassign */
DO:
  if not available data
   then return.

  for first agentapp no-lock:  
    run dialogqarreassign.w (data.qarID, agentapp.agentID, agentapp.agentName, data.uid, output pNewUID, output pReason, output std-lo).
    if std-lo
     then 
      do with frame {&frame-name}:
        assign
          /* set the err */
          tERR:screen-value = string(data.qarID)
          /* the buttons at top */
          bNew:sensitive = false
          bCancel:sensitive = false
          bReassign:sensitive = false
          /* show the widgets */
          tYear:visible = true
          tAuditor:visible = true
          tAuditor:screen-value = pNewUID
          tAuditor:sensitive = false
          tPhone:visible = true
          tEmail:visible = true
          tName:visible = true
          tAddress:visible = true
          tAddress2:visible = true
          tCity:visible = true
          tState:visible = true
          tZip:visible = true
          /* hide the browse */
          brwData:visible = false
          /* change the name of the button */
          bSave:label = "Reassign"
          .
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available data
   then return.

  do with frame {&frame-name}:
    assign
      /* set the err */
      tERR:screen-value = string(data.qarID)
      /* the buttons at top */
      bNew:sensitive = false
      bCancel:sensitive = false
      bReassign:sensitive = false
      /* show the widgets */
      tYear:visible = true
      tAuditor:visible = true
      tAuditor:screen-value = data.uid
      tPhone:visible = true
      tEmail:visible = true
      tName:visible = true
      tAddress:visible = true
      tAddress2:visible = true
      tCity:visible = true
      tState:visible = true
      tZip:visible = true
      /* hide the browse */
      brwData:visible = false
      /* change the name of the button */
      bSave:label = "Notify"
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if not available data
   then return.

  if data.stat = "A" or data.stat = "C" or data.stat = "X"
   then bReassign:sensitive in frame {&frame-name} = false.
   else bReassign:sensitive in frame {&frame-name} = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  case self:label:
   when "Create" then
    do:
      run ERRSave in hFileDataSrv (input tERR:input-value,
                                   input tYear:input-value,
                                   input tAuditor:input-value,
                                   input tPhone:input-value,
                                   input tEmail:input-value,
                                   input tName:input-value,
                                   input tAddress:input-value,
                                   input tAddress2:input-value,
                                   input tCity:input-value,
                                   input tState:input-value,
                                   input tZip:input-value,
                                   output lSave).
      if lSave
       then
        do:
          run ERRCreate in hFileDataSrv (tAuditor:input-value, output lSave).
          apply "CHOOSE" to bClose.
        end.
    end.
   when "Link" then apply "DEFAULT-ACTION" to brwData.
   when "Notify" then
    do:
      run ERRSave in hFileDataSrv (input tERR:input-value,
                                   input tYear:input-value,
                                   input tAuditor:input-value,
                                   input tPhone:input-value,
                                   input tEmail:input-value,
                                   input tName:input-value,
                                   input tAddress:input-value,
                                   input tAddress2:input-value,
                                   input tCity:input-value,
                                   input tState:input-value,
                                   input tZip:input-value,
                                   output lSave).
      if lSave
       then
        do:
          run ERRNotify in hFileDataSrv.
          apply "CHOOSE" to bClose.
        end.
    end.
   when "Reassign" then
    do:
      run ERRSave in hFileDataSrv (input tERR:input-value,
                                   input tYear:input-value,
                                   input tAuditor:input-value,
                                   input tPhone:input-value,
                                   input tEmail:input-value,
                                   input tName:input-value,
                                   input tAddress:input-value,
                                   input tAddress2:input-value,
                                   input tCity:input-value,
                                   input tState:input-value,
                                   input tZip:input-value,
                                   output lSave).
      if lSave
       then
        do:
          run ERRReassign in hFileDataSrv (tERR:input-value, pNewUID, pReason, output lSave).
          apply "CHOOSE" to bClose.
        end.
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

bNew:load-image("images/s-add.bmp").
bNew:load-image-insensitive("images/s-add-i.bmp").
bCancel:load-image("images/s-cancel.bmp").
bCancel:load-image-insensitive("images/s-cancel-i.bmp").
bReassign:load-image("images/s-usergo.bmp").
bReassign:load-image-insensitive("images/s-usergo-i.bmp").
bDocs:load-image("images/s-docs.bmp").
bDocs:load-image-insensitive("images/s-docs-i.bmp").

{lib/get-sysprop-list.i &combo=tAuditor &appCode="'QAR'" &objAction="'Auditor'"}
{lib/get-full-state-list.i &combo=tState}

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

if valid-handle(hFileDataSrv)
 then 
  do:
    run GetAgentApp in hFileDataSrv (output table agentapp).
    run GetERR in hFileDataSrv (output table data).
    for first agentapp no-lock:
      case agentapp.type:
       when "G" then
        do:
          std-ha = ?.
          publish "OpenAgent" (agentapp.agentID, output std-ha).
          {&window-name}:title = "ERR for agent " + agentapp.agentID.
          if valid-handle(std-ha)
           then run GetAgent in std-ha (output table agent).
        end.
       when "A" then
        do:
          std-ha = ?.
          publish "OpenAttorney" (agentapp.agentID, output std-ha).
          {&window-name}:title = "ERR for attorney " + agentapp.agentID.
          if valid-handle(std-ha)
           then run GetAttorney in std-ha (output table attorney).
        end.
       end case.
    end.
  end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  for first agentapp no-lock:
    assign
      tERR:screen-value = (if can-find(first data) and agentapp.qarID <> ? then string(agentapp.qarID) else "0")
      tYear:screen-value = string(agentapp.yearsActive)
      .
    for first agent no-lock:
      assign
        tPhone:screen-value = (if agentapp.phone = "" then agent.phone else agentapp.phone)
        tEmail:screen-value = (if agentapp.email = "" then agent.email else agentapp.email)
        tName:screen-value = (if agentapp.name = "" then agent.contact else agentapp.name)
        tAddress:screen-value = (if agentapp.addr1 = "" then agent.addr1 else agentapp.addr1)
        tAddress2:screen-value = (if agentapp.addr2 = "" then agent.addr2 else agentapp.addr2)
        tCity:screen-value = (if agentapp.city = "" then agent.city else agentapp.city)
        tState:screen-value = (if agentapp.state = "" then agent.state else agentapp.state)
        tZip:screen-value = (if agentapp.zip = "" then agent.zip else agentapp.zip)
        .
    end.
    for first data no-lock
        where data.qarID = agentapp.qarID:
      
      publish "GetSysPropDesc" ("QAR", "Audit", "Status", data.stat, output data.statDesc).
      tAuditor:screen-value = data.uid.
    end.
  end.
  if not can-find(first data) 
   then doNew(true).
   else doNew(false).

  {&OPEN-BROWSERS-IN-QUERY-fMain}
  apply "VALUE-CHANGED" to {&browse-name}.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tERR tYear tAuditor tPhone tEmail tName tAddress tAddress2 tCity 
          tState tZip 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tYear tAuditor tPhone tEmail brwData tName tAddress tAddress2 tCity 
         tState tZip bSave bClose bNew bCancel bReassign bDocs 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doNew C-Win 
FUNCTION doNew RETURNS LOGICAL
  ( input pNew as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      /* the buttons at top */
      bNew:sensitive = not pNew
      bCancel:sensitive = pNew and can-find(first data)
      bReassign:sensitive = not pNew
      /* show the widgets */
      tYear:visible = pNew
      tAuditor:visible = pNew
      tPhone:visible = pNew
      tEmail:visible = pNew
      tName:visible = pNew
      tAddress:visible = pNew
      tAddress2:visible = pNew
      tCity:visible = pNew
      tState:visible = pNew
      tZip:visible = pNew
      /* hide the browse */
      brwData:visible = not pNew
      /* rename the button */
      bSave:label = (if pNew then "Create" else "Link")
      .
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

