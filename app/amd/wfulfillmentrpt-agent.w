&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wfulfillmentrpt-agent.w

  Description: Screen for Agent's Fulfillment Report.
  
  Author: Shefali
  
  Created: 11.28.2024
  
  @Modified:
  Date        Name     Description
  
  ----------------------------------------------------------------------*/   

create widget-pool.

/* Include Files Definition */
{tt/reqqual.i}
{tt/reqqual.i &tableAlias=ttresult}
{tt/staterequirement.i &getactiverequirements=true}

{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}

/* variables Definition */
define variable dColumnWidth     as decimal   no-undo.
define variable chAction         as character no-undo.
define variable trackMetText     as character  init "" no-undo.
define variable trackUnMetText   as character  init "" no-undo.
define variable trackAllText     as character  init "" no-undo.
  
define variable iMetCount        as integer    init 0  no-undo.
define variable iUnMetCount      as integer    init 0  no-undo.
define variable iAllCount        as integer    init 0  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQualification

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttresult

/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification ttresult.stateID ttresult.entityId ttresult.entityName ttresult.roleID ttresult.roleName ttresult.agentmanager ttresult.qualificationDesc ttresult.service ttresult.stat "Status" ttresult.effDate "Effective" ttresult.expDate ttresult.reviewDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification   
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification for each ttresult
&Scoped-define OPEN-QUERY-brwQualification open query {&SELF-NAME} for each ttresult.
&Scoped-define TABLES-IN-QUERY-brwQualification ttresult
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification ttresult


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQualification}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState cbrequirementparam brwQualification ~
btGet RECT-55 RECT-56 RECT-57 RECT-58 
&Scoped-Define DISPLAYED-OBJECTS cbState cbrequirementparam fOrganization ~
cbManager cbMet fUnmetcount fMetcount fTotalcount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData returns logical private ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged returns logical private
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bOrgLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset Filter".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cbManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE cbrequirementparam AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE fMetcount AS INTEGER FORMAT "ZZ,ZZ9":U INITIAL 0 
     LABEL "Met" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE fOrganization AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Organization" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 61.6 BY 1.

DEFINE VARIABLE fTotalcount AS INTEGER FORMAT "ZZ,ZZ9":U INITIAL 0 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE fUnmetcount AS INTEGER FORMAT "ZZ,ZZ9":U INITIAL 0 
     LABEL "Unmet" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE cbMet AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Met", "M",
"Unmet", "U",
"Both", "B"
     SIZE 10 BY 3.52 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 115.6 BY 4.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 83.6 BY 4.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 13.4 BY 4.

DEFINE RECTANGLE RECT-57
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25 BY 4.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 8 BY .1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQualification FOR 
      ttresult SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification C-Win _FREEFORM
  QUERY brwQualification DISPLAY
      ttresult.stateID           column-label "State ID"         format "x(8)"       width 8      
ttresult.entityId          column-label "Organization ID"   format "x(10)"      width 15         
ttresult.entityName        column-label "Organization"      format "x(80)"      width 30
ttresult.roleID            column-label "Agent ID"          format "x(10)"      width 9
ttresult.roleName          column-label "Agent"             format "x(80)"      width 42
ttresult.agentmanager      column-label "Manager"           format "x(30)"      width 20
ttresult.qualificationDesc column-label "Qualification"     format "x(50)"      width 30
ttresult.service           column-label "Number/Service"    format "x(50)"      width 17
ttresult.stat              label        "Status"            format "x(16)"      width 16
ttresult.effDate           label        "Effective"         format "99/99/9999" width 12         
ttresult.expDate           column-label "Expiration"        format "99/99/9999" width 12
ttresult.reviewDate        column-label "Review"            format "99/99/9999" width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 236.2 BY 15.67
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bOrgLookup AT ROW 2.24 COL 163.2 WIDGET-ID 282 NO-TAB-STOP 
     cbState AT ROW 2.33 COL 16.6 COLON-ALIGNED WIDGET-ID 260
     btFilter AT ROW 2.57 COL 190.4 WIDGET-ID 274 NO-TAB-STOP 
     cbrequirementparam AT ROW 3.48 COL 16.6 COLON-ALIGNED WIDGET-ID 284
     brwQualification AT ROW 5.62 COL 2 WIDGET-ID 200
     btExport AT ROW 2.57 COL 203.6 WIDGET-ID 2 NO-TAB-STOP 
     fOrganization AT ROW 2.33 COL 99.4 COLON-ALIGNED WIDGET-ID 280 NO-TAB-STOP 
     btGet AT ROW 2.57 COL 75.4 WIDGET-ID 262 NO-TAB-STOP 
     cbManager AT ROW 3.48 COL 99.4 COLON-ALIGNED WIDGET-ID 296 NO-TAB-STOP 
     cbMet AT ROW 1.62 COL 178.6 NO-LABEL WIDGET-ID 364 NO-TAB-STOP 
     fUnmetcount AT ROW 2.81 COL 223 COLON-ALIGNED WIDGET-ID 370 NO-TAB-STOP 
     fMetcount AT ROW 2 COL 223 COLON-ALIGNED WIDGET-ID 372 NO-TAB-STOP 
     fTotalcount AT ROW 3.91 COL 223 COLON-ALIGNED WIDGET-ID 374 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.19 COL 86.4 WIDGET-ID 242
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.19 COL 3 WIDGET-ID 252
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.19 COL 201.6 WIDGET-ID 266
     "Status:" VIEW-AS TEXT
          SIZE 7 BY 1 AT ROW 1.62 COL 171.4 WIDGET-ID 368
     "Fulfillment" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 1.19 COL 214.6 WIDGET-ID 378
     RECT-2 AT ROW 1.43 COL 85.2 WIDGET-ID 8
     RECT-55 AT ROW 1.43 COL 2 WIDGET-ID 250
     RECT-56 AT ROW 1.43 COL 200.4 WIDGET-ID 264
     RECT-57 AT ROW 1.43 COL 213.4 WIDGET-ID 376
     RECT-58 AT ROW 3.81 COL 226.4 WIDGET-ID 380
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1 ROW 1
         SIZE 238.4 BY 22.24
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Fulfillment Report"
         HEIGHT             = 18.62
         WIDTH              = 238.2
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwQualification cbrequirementparam fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bOrgLookup IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwQualification:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwQualification:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON btExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFilter IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbManager IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET cbMet IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMetcount IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fMetcount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fOrganization IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fOrganization:READ-ONLY IN FRAME fMain        = TRUE
       fOrganization:PRIVATE-DATA IN FRAME fMain     = 
                "ALL".

/* SETTINGS FOR FILL-IN fTotalcount IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fTotalcount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fUnmetcount IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fUnmetcount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query {&SELF-NAME} for each ttresult.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Fulfillment Report */
or endkey of {&window-name} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Fulfillment Report */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Fulfillment Report */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup C-Win
ON CHOOSE OF bOrgLookup IN FRAME fMain /* Lookup */
do:
  run openDialog in this-procedure.
  apply "value-changed":U to fOrganization.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON ROW-DISPLAY OF brwQualification IN FRAME fMain
do:    
   {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON START-SEARCH OF brwQualification IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter C-Win
ON CHOOSE OF btFilter IN FRAME fMain /* Filter */
do:
  /* Reset filter widgets */
  assign 
      fOrganization:private-data = {&ALL}
      fOrganization:screen-value = {&ALL}
      cbManager:screen-value     = {&ALL}
      cbmet:screen-value         = {&RadioBoth}.

  run filterData in this-procedure.
  run RefreshFulfillmentCount in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* Get */
do:
  clearData().
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbManager C-Win
ON VALUE-CHANGED OF cbManager IN FRAME fMain /* Manager */
DO:
  run filterData in this-procedure.
  run RefreshFulfillmentCount in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbMet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbMet C-Win
ON VALUE-CHANGED OF cbMet IN FRAME fMain
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbrequirementparam
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbrequirementparam C-Win
ON VALUE-CHANGED OF cbrequirementparam IN FRAME fMain /* Requirement */
DO:
  clearData().
  resultsChanged(false).
  if self:screen-value = "0" or self:screen-value = "?" then
   assign btGet:sensitive = false.
  else 
   do:
     btGet:sensitive = true.
     if lookup("0",cbrequirementparam:list-item-pairs,",") ne 0 then
      cbrequirementparam:delete("0").
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  clearData().
  resultsChanged(false).
  run changestate in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOrganization
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOrganization C-Win
ON VALUE-CHANGED OF fOrganization IN FRAME fMain /* Organization */
DO:
  run filterData in this-procedure.
  run RefreshFulfillmentCount in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
   run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

btExport:load-image("images/excel.bmp").
btExport:load-image-insensitive("images/excel-i.bmp").
btGet:load-image("images/Completed.bmp").
btGet:load-image-insensitive("images/Completed-i.bmp").
btFilter:load-image("images/filtererase.bmp").
btFilter:load-image-insensitive("images/filtererase-i.bmp").
bOrgLookup:load-image("images/s-lookup.bmp").
bOrgLookup:load-image-insensitive("images/s-lookup-i.bmp").

{lib/win-main.i}
{lib/win-status.i}

C-Win:title = "Agent Fulfillment Report" .
 
/* Function to reset default values and clear filters */
clearData().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  {lib/get-column-width.i &col="'entityName'"        &var=dColumnWidth} 
  {lib/get-column-width.i &col="'roleName'"          &var=dColumnWidth}
  {lib/get-column-width.i &col="'qualificationDesc'" &var=dColumnWidth}

  empty temp-table staterequirement. 
  publish "getactiverequirements" (input "ALL", output table staterequirement).
  
  cbState:list-item-pairs = "ALL" + "," + "ALL" .

  for each staterequirement
    break by staterequirement.stateID:
    if not first-of(staterequirement.stateID) or staterequirement.stateID = "00"
     then next.
    cbState:list-item-pairs = cbState:list-item-pairs + "," + staterequirement.stateDesc + "," + staterequirement.stateID.
  end.
  cbState:screen-value = {&ALL}.
  
  run changestate in this-procedure.
  assign 
      fTotalcount:screen-value = string(iAllCount)
      fMetcount:screen-value   = string(iMetCount)
      fUnmetcount:screen-value = string(iUnMetCount).
 
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  run windowResized in this-procedure.

  if not this-procedure:persistent
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changestate C-Win 
PROCEDURE changestate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.

  std-lo = false.
  std-ch = "".
  
  empty temp-table staterequirement.
  
  publish "getactiverequirements" (input cbState:input-value, output table staterequirement).
  
  cbrequirementparam:list-item-pairs = "-- Select Requirement --" + "," + "0" .
  
  /* Set the reuirement parameter with ALL as the first option*/
  for each staterequirement where staterequirement.stateID = (if cbState:input-value = "ALL" then staterequirement.stateID else cbState:input-value)
    break by staterequirement.description:
    if not first-of(staterequirement.description) 
     then next.
    cbrequirementparam:list-item-pairs = cbrequirementparam:list-item-pairs + "," + staterequirement.description + "," + string(staterequirement.requirementID).
  end.
  cbrequirementparam:screen-value = "0".
  
  apply "value-changed" to cbrequirementparam.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState cbrequirementparam fOrganization cbManager cbMet fUnmetcount 
          fMetcount fTotalcount 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE cbState cbrequirementparam brwQualification btGet RECT-55 RECT-56 
         RECT-57 RECT-58 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable      as handle    no-undo.
  define variable cReportName as character no-undo.

  if query brwQualification:num-results = 0
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  if std-ch = ""
   then std-ch = os-getenv("TEMP") + "\".
  
  cReportName = "AgentFulfillment" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
  cReportName = std-ch + cReportName.
  
  /* write the requirement header of the report */
  output to value(cReportName).
  for first ttresult:
    put unformatted "Requirement: " + ttresult.requirementDesc skip.
  end.
  output close.
  
  run util/exporttocsvbrowseappend.p (string(browse brwQualification:handle), cReportName, "").                           
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  close query brwQualification.
  empty temp-table ttresult.

  for each reqQual where reqQual.entityId  = (if (fOrganization:private-data = "ALL" or fOrganization:private-data = ? ) then reqQual.entityId else fOrganization:private-data) and
                         reqQual.agentManager = (if (cbManager:screen-value = "ALL" or cbManager:screen-value = ? ) then reqQual.agentManager else cbManager:screen-value):
    if cbmet:screen-value = {&RadioMet} 
     then
       do:
         if reqQual.qualificationId <> 0 
          then
           do:
             create ttresult.
             buffer-copy reqQual to ttresult.
           end.
       end.
    else if cbmet:screen-value = {&RadioUnmet} 
     then
      do:
        if reqQual.qualificationId = 0 
         then
          do:
            create ttresult.
            buffer-copy reqQual to ttresult.
          end.
      end.
    else
     do:
       create ttresult.
       buffer-copy reqQual to ttresult.
     end.
  end.
   
  for each ttresult:
    ttresult.stat = getQualificationDesc(ttresult.stat).
  end.
  
  open query brwqualification preselect each ttresult.

  /* Display no. of records on status bar */
  setStatusCount(query brwqualification:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define variable cRequirementID   as character no-undo.
  define variable cRequirementDesc as character no-undo.
  
  define buffer reqQual for reqQual. 
  do with frame {&FRAME-NAME} :
  end.

  cRequirementID = "".
  
  /* Set the reuirement parameter with ALL as the first option*/
  if cbState:input-value = "ALL" then
   do:
     for first staterequirement where staterequirement.requirementID = cbrequirementparam:input-value:
      assign cRequirementDesc = staterequirement.description.
     end.
     for each staterequirement where staterequirement.description = cRequirementDesc:
       if cRequirementID = "" then assign cRequirementID = string(staterequirement.requirementID).
        else assign cRequirementID = cRequirementID +  "," + string(staterequirement.requirementID).
     end.
     cRequirementID = trim(cRequirementID,",").
   end.
  
  /* client Server Call */
  run server/queryrequirementsqualifications.p (input cbState:input-value,
                                                input "O",
                                                input "Agent",
                                                input (if cbState:input-value = "ALL" then cRequirementID else string(cbrequirementparam:input-value)),
                                                output table reqQual,
                                                output std-lo,
                                                output std-ch).

  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
  
  cbManager:list-items = "".
  cbManager:add-first({&ALL}). 
  
  for each reqQual
    break by reqQual.agentManager:
    if not first-of(reqQual.agentManager) or reqQual.agentManager = ""
     then next.   
    cbManager:add-last(reqQual.agentManager) in frame {&frame-name}.  
  end. 
  
  cbManager:screen-value = {&ALL}.

  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure.
  run RefreshFulfillmentCount in this-procedure.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwQualification:num-results > 0 
   then
    assign 
        browse brwQualification:sensitive = true
               btExport:sensitive         = true
               fOrganization:sensitive    = true
               bOrgLookup:sensitive       = true
               cbmet:sensitive            = true
               cbManager:sensitive        = true
               btFilter:sensitive         = true
               .
  else
   assign 
       browse brwQualification:sensitive = false
       btExport:sensitive                = false
       fOrganization:sensitive           = false
       bOrgLookup:sensitive              = false
       cbmet:sensitive                   = false
       cbManager:sensitive               = false
       btFilter:sensitive                = false
       .
      
  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwqualification:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog C-Win 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable cPrevOrgID as character              no-undo.
  define variable cOrgID     as character              no-undo.
  define variable cName      as character              no-undo.
  
  if fOrganization:private-data <> ""
   then
    cPrevOrgID = fOrganization:private-data. 
    
  run dialogorganizationlookup.w(input  cPrevOrgID,
                                 input  false,        /* True if add "New" */
                                 input  false,        /* True if add "Blank" */
                                 input  true,         /* True if add "ALL" */
                                 output cOrgID,
                                 output cName,
                                 output std-lo).
   
  if not std-lo 
   then
    return no-apply.

  if cName = {&ALL}
   then
    do:
      fOrganization:screen-value = cName.
      fOrganization:private-data = cName.
    end.
  else
   do:
     fOrganization:screen-value = if cOrgID = "" then "" else cName.
     fOrganization:private-data = if cOrgID = "" then "" else cOrgID.
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshFulfillmentCount C-Win 
PROCEDURE RefreshFulfillmentCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cRadioSelected as character no-undo.
  
  do with frame {&FRAME-NAME}:
  end.
  
  cRadioSelected = cbMet:screen-value.

  assign 
      iMetCount   = 0
      iUnMetCount = 0
      iAllCount   = 0.
  
  
  for each reqQual where reqQual.entityId  = (if (fOrganization:private-data = "ALL" or fOrganization:private-data = ? ) then reqQual.entityId else fOrganization:private-data) and
                         reqQual.agentManager = (if (cbManager:screen-value = "ALL" or cbManager:screen-value = ? ) then reqQual.agentManager else cbManager:screen-value):
   iAllCount = iAllCount + 1.
    if reqQual.qualificationId <> 0 
     then iMetCount = iMetCount + 1.
    else if reqQual.qualificationId = 0 
     then iUnMetCount = iUnMetCount + 1.   
  end.
  
  assign 
      fTotalcount:screen-value = string(iAllCount)
      fMetcount:screen-value   = string(iMetCount)
      fUnmetcount:screen-value = string(iUnMetCount).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  if chAction = "entityName" then
    tWhereClause = tWhereClause + " by ttresult.entityId ".
  else 
    tWhereClause = "".
 
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.
 
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 7
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 2
      .
  
  {lib/resize-column.i &col="'entityName,qualificationDesc'" &var=dColumnWidth}
  run ShowScrollBars(browse brwQualification:handle, no, yes). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData returns logical private ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  /* Set default values for the client side filters */
  assign 
      cbmet:screen-value       = {&RadioBoth}
      cbManager:screen-value   = {&ALL}
      fOrganization:private-data = {&ALL}
      fOrganization:screen-value = {&ALL}
      iAllCount                = 0
      iMetCount                = 0
      iUnMetCount              = 0
      fTotalcount:screen-value = string(iAllCount)
      fMetcount:screen-value   = string(iMetCount)
      fUnmetcount:screen-value = string(iUnMetCount).
      .

  /* disable widgets on launch of report and everytime different state is selected */
  disable fOrganization bOrgLookup cbmet cbManager
          btExport btFilter with frame {&frame-name}.

  setStatusMessage("").
  return true. /* Function return value */
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  RETURN std-ch.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged returns logical private
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 return true.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

