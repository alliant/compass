&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Application
@description Tool to manage the agent applications

@author Yoke Sam Chin (modified by John Oliver)
@version 1.1
@created 07.07.2017
@notes 
@modified
Date          Name            Description
09-09-2022    SC              Task#97318 AgentApp modified from Window not reflected 
                              in ModifyAgentApp dialog.
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* Temp Table Definitions ---                                        */
{tt/agentapp.i &tableAlias="data"}
{tt/agentapp.i &tableAlias="origdata"}
{tt/agentapp.i &tableAlias="tempdata"}
{tt/agent.i}
{tt/openwindow.i}
define temp-table openApp
  field appID as character
  field appHandle as handle
  .

{lib/set-button-def.i}
{lib/set-filter-def.i &tableName=data}
{lib/std-def.i}
{lib/get-column.i}

define variable hNewWindow as handle no-undo.
define variable hDocWindow as handle no-undo.
define variable hReasonWindow as handle no-undo.

define variable dColumnAgent as decimal no-undo.
define variable cReasonStatus as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAgentApp

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwAgentApp                                   */
&Scoped-define FIELDS-IN-QUERY-brwAgentApp data.agentID data.statDesc data.typeDesc data.stateID data.agentName data.managerDesc data.dateCreated data.dateUnderReview data.dateSigned   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAgentApp   
&Scoped-define SELF-NAME brwAgentApp
&Scoped-define QUERY-STRING-brwAgentApp FOR EACH data by data.agentID.  apply "value-changed" to brwAgentApp
&Scoped-define OPEN-QUERY-brwAgentApp OPEN QUERY {&SELF-NAME} FOR EACH data by data.agentID.  apply "value-changed" to brwAgentApp.
&Scoped-define TABLES-IN-QUERY-brwAgentApp data
&Scoped-define FIRST-TABLE-IN-QUERY-brwAgentApp data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwAgentApp}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bNew rFilter RECT-38 bSave RECT-50 rDetails ~
fRegion fState bFilter bFilterClear fSearch fManager fStatus brwAgentApp ~
tNotes bDocs bExport bRefresh tNotesLabel 
&Scoped-Define DISPLAYED-OBJECTS fRegion fState fSearch fManager fStatus ~
tNotes tNotesLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForApplication C-Win 
FUNCTION openWindowForApplication RETURNS HANDLE
  ( input pAgentID as character,
    input pApplicationID as integer,
    input pType as character,
    input pFile as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwAgentApp 
       MENU-ITEM m_View_Documents LABEL "View Documents".


/* Definitions of the field level widgets                               */
DEFINE VARIABLE tDateAgreeSent AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateApproved AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateCompleted AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateCreated AS CHARACTER FORMAT "X(256)":U 
     LABEL "Application" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateERRFinish AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateERROrdered AS CHARACTER FORMAT "X(256)":U 
     LABEL "ERR" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateERRStart AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateReview AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateReviewSent AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateStart AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDateSubmitted AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tDecisionStatus AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 13 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE tMetricApplication AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Application Created to Submitted" NO-UNDO.

DEFINE VARIABLE tMetricCreatedToDecision AS CHARACTER FORMAT "X(256)":U 
     LABEL "Created to Decision" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE tMetricCreatedToSigned AS CHARACTER FORMAT "X(256)":U 
     LABEL "Created to Complete" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE tMetricCreatedToUnderReview AS CHARACTER FORMAT "X(256)":U 
     LABEL "Created to Processed" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE tMetricERRStartedToFinish AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Escrow Reconciliation Review Ordered to Finished" NO-UNDO.

DEFINE VARIABLE tMetricSentAgencyToSigned AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE tMetricSentReviewToApproved AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Review Sent to Deicision Made" NO-UNDO.

DEFINE VARIABLE tMetricUnderReviewToSentAgency AS CHARACTER FORMAT "X(256)":U 
     LABEL "Process Started to Agreement Sent" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Process Started to Agreement Sent" NO-UNDO.

DEFINE VARIABLE tMetricUnderReviewToSentReview AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Process Started to Finished" NO-UNDO.

DEFINE RECTANGLE RECT-52
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 49.6 BY .1.

DEFINE BUTTON bApprove  NO-FOCUS
     LABEL "Approve" 
     SIZE 7.2 BY 1.71 TOOLTIP "Approve Review".

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel Application".

DEFINE BUTTON bComplete  NO-FOCUS
     LABEL "Complete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Complete Application".

DEFINE BUTTON bDeny  NO-FOCUS
     LABEL "Deny" 
     SIZE 7.2 BY 1.71 TOOLTIP "Deny Review".

DEFINE BUTTON bDocs  NO-FOCUS
     LABEL "Docs" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bFilterClear 
     LABEL "Clear" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Update" 
     SIZE 7.2 BY 1.71 TOOLTIP "Update".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save Notes".

DEFINE BUTTON bStart  NO-FOCUS
     LABEL "Start" 
     SIZE 7.2 BY 1.71 TOOLTIP "Start Processing".

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fRegion AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62 BY 6.19 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE tNotesLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Notes:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE RECTANGLE rDetails
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 21.57.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 3.1.

DEFINE RECTANGLE RECT-50
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 41 BY 3.1.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 141.8 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAgentApp FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAgentApp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAgentApp C-Win _FREEFORM
  QUERY brwAgentApp DISPLAY
      data.agentID width 12
data.statDesc width 15
data.typeDesc width 15
data.stateID width 8
data.agentName width 30
data.managerDesc WIDTH 27
data.dateCreated width 15
data.dateUnderReview WIDTH 15
data.dateSigned WIDTH 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 163 BY 21.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bNew AT ROW 2.24 COL 18.8 WIDGET-ID 10 NO-TAB-STOP 
     bSave AT ROW 2.19 COL 41 WIDGET-ID 306 NO-TAB-STOP 
     bApprove AT ROW 2.19 COL 73.8 WIDGET-ID 78 NO-TAB-STOP 
     fRegion AT ROW 1.95 COL 101 COLON-ALIGNED WIDGET-ID 316
     fState AT ROW 1.95 COL 135 COLON-ALIGNED WIDGET-ID 64
     bCancel AT ROW 2.19 COL 59 WIDGET-ID 74 NO-TAB-STOP 
     bFilter AT ROW 2.19 COL 215 WIDGET-ID 66
     bFilterClear AT ROW 2.19 COL 222.4 WIDGET-ID 86
     bComplete AT ROW 2.19 COL 81.2 WIDGET-ID 80 NO-TAB-STOP 
     fSearch AT ROW 2.57 COL 166 COLON-ALIGNED WIDGET-ID 84
     fManager AT ROW 3.14 COL 101 COLON-ALIGNED WIDGET-ID 318
     fStatus AT ROW 3.14 COL 135 COLON-ALIGNED WIDGET-ID 62
     bDeny AT ROW 2.19 COL 66.4 WIDGET-ID 76 NO-TAB-STOP 
     brwAgentApp AT ROW 4.81 COL 2 WIDGET-ID 200
     tNotes AT ROW 5.76 COL 168 NO-LABEL WIDGET-ID 310
     bStart AT ROW 2.19 COL 51.6 WIDGET-ID 72 NO-TAB-STOP 
     bDocs AT ROW 2.19 COL 33.6 WIDGET-ID 298 NO-TAB-STOP 
     bExport AT ROW 2.19 COL 11.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 2.19 COL 4 WIDGET-ID 4 NO-TAB-STOP 
     bModify AT ROW 2.19 COL 26.2 WIDGET-ID 88 NO-TAB-STOP 
     tNotesLabel AT ROW 5.14 COL 166 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 91.2 WIDGET-ID 60
     "Actions" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 70
     "Status Changes" VIEW-AS TEXT
          SIZE 15.8 BY .62 AT ROW 1.24 COL 50.6 WIDGET-ID 304
     rFilter AT ROW 1.48 COL 90.2 WIDGET-ID 58
     RECT-38 AT ROW 1.48 COL 2 WIDGET-ID 68
     RECT-50 AT ROW 1.48 COL 49.6 WIDGET-ID 302
     rDetails AT ROW 4.81 COL 166 WIDGET-ID 308
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 231.8 BY 25.62 WIDGET-ID 100.

DEFINE FRAME fDetails
     tDateCreated AT ROW 2.43 COL 1.8 WIDGET-ID 174 NO-TAB-STOP 
     tDateStart AT ROW 2.43 COL 28 NO-LABEL WIDGET-ID 10
     tDateSubmitted AT ROW 2.43 COL 42.4 NO-LABEL WIDGET-ID 14
     tMetricApplication AT ROW 2.43 COL 55 COLON-ALIGNED NO-LABEL WIDGET-ID 292
     tDateERROrdered AT ROW 3.86 COL 11.4 COLON-ALIGNED WIDGET-ID 276
     tDateERRStart AT ROW 3.86 COL 28 NO-LABEL WIDGET-ID 24
     tDateERRFinish AT ROW 3.86 COL 40.4 COLON-ALIGNED NO-LABEL WIDGET-ID 26
     tMetricERRStartedToFinish AT ROW 3.86 COL 55 COLON-ALIGNED NO-LABEL WIDGET-ID 274 DISABLE-AUTO-ZAP 
     tDateReview AT ROW 5.29 COL 28 NO-LABEL WIDGET-ID 234 NO-TAB-STOP 
     tDateReviewSent AT ROW 5.29 COL 40.4 COLON-ALIGNED NO-LABEL WIDGET-ID 236
     tMetricUnderReviewToSentReview AT ROW 5.29 COL 55 COLON-ALIGNED NO-LABEL WIDGET-ID 272 DISABLE-AUTO-ZAP 
     tDateApproved AT ROW 6.71 COL 28 NO-LABEL WIDGET-ID 242 NO-TAB-STOP 
     tMetricSentReviewToApproved AT ROW 6.71 COL 55 COLON-ALIGNED NO-LABEL WIDGET-ID 270 DISABLE-AUTO-ZAP 
     tDateAgreeSent AT ROW 8.14 COL 28 NO-LABEL WIDGET-ID 246
     tDateCompleted AT ROW 8.14 COL 40.4 COLON-ALIGNED NO-LABEL WIDGET-ID 248 NO-TAB-STOP 
     tMetricSentAgencyToSigned AT ROW 8.14 COL 55 COLON-ALIGNED NO-LABEL WIDGET-ID 266 DISABLE-AUTO-ZAP 
     tMetricCreatedToUnderReview AT ROW 9.57 COL 55 COLON-ALIGNED WIDGET-ID 260 DISABLE-AUTO-ZAP 
     tMetricCreatedToDecision AT ROW 11 COL 55 COLON-ALIGNED WIDGET-ID 300 DISABLE-AUTO-ZAP 
     tMetricUnderReviewToSentAgency AT ROW 12.43 COL 55 COLON-ALIGNED WIDGET-ID 268 DISABLE-AUTO-ZAP 
     tMetricCreatedToSigned AT ROW 13.86 COL 55 COLON-ALIGNED WIDGET-ID 264 DISABLE-AUTO-ZAP 
     tDecisionStatus AT ROW 6.91 COL 40.4 COLON-ALIGNED NO-LABEL WIDGET-ID 296
     "Agreement:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 8.33 COL 2 WIDGET-ID 254
     "Days" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.48 COL 57.6 WIDGET-ID 290
     "Process:" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 5.48 COL 4.6 WIDGET-ID 286
     "Created" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.48 COL 15.8 WIDGET-ID 298
     "Ended" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.48 COL 45.4 WIDGET-ID 282
     "Started" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.48 COL 30.2 WIDGET-ID 280
     "Decision:" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 6.91 COL 4 WIDGET-ID 284
     RECT-52 AT ROW 2.19 COL 13.4 WIDGET-ID 288
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 168 ROW 11.95
         SIZE 62 BY 14.05 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Applications"
         HEIGHT             = 25.62
         WIDTH              = 231.8
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fDetails:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fDetails
                                                                        */
/* SETTINGS FOR FILL-IN tDateAgreeSent IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateAgreeSent:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FILL-IN tDateApproved IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateApproved:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDateCompleted:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FILL-IN tDateCreated IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateCreated:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDateERRFinish:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDateERROrdered:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FILL-IN tDateERRStart IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateERRStart:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FILL-IN tDateReview IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateReview:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDateReviewSent:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FILL-IN tDateStart IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateStart:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FILL-IN tDateSubmitted IN FRAME fDetails
   ALIGN-L                                                              */
ASSIGN 
       tDateSubmitted:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDecisionStatus:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricApplication:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricCreatedToDecision:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricCreatedToSigned:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricCreatedToUnderReview:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricERRStartedToFinish:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricSentAgencyToSigned:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricSentReviewToApproved:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricUnderReviewToSentAgency:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tMetricUnderReviewToSentReview:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAgentApp bDeny fMain */
/* SETTINGS FOR BUTTON bApprove IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bComplete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeny IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwAgentApp:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwAgentApp:HANDLE
       brwAgentApp:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwAgentApp:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bStart IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAgentApp
/* Query rebuild information for BROWSE brwAgentApp
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data by data.agentID.

apply "value-changed" to brwAgentApp.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAgentApp */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Applications */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Applications */
DO:
  /* save the notes */
  apply "CHOOSE" to bSave in frame {&frame-name}.
  
  if valid-handle(hNewWindow)
   then run CloseWindow in hNewWindow no-error.
  
  if valid-handle(hDocWindow)
   then run CloseWindow in hDocWindow no-error.
  
  for each openwindow:
    if valid-handle(openwindow.procHandle)
     then run CloseWindow in openwindow.procHandle no-error.
  end.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Applications */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApprove C-Win
ON CHOOSE OF bApprove IN FRAME fMain /* Approve */
DO:
  if not available data
   then return.
  
  run statusApprove in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  if not available data
   then return.
  
  run statusCancel in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bComplete C-Win
ON CHOOSE OF bComplete IN FRAME fMain /* Complete */
DO:
  if not available data
   then return.
  
  run statusComplete in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeny
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeny C-Win
ON CHOOSE OF bDeny IN FRAME fMain /* Deny */
DO:
  if not available data
   then return.
  
  run statusDeny in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocs C-Win
ON CHOOSE OF bDocs IN FRAME fMain /* Docs */
DO:
  if not available data
   then return.
   
  run actionDocs in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* Clear */
DO:
  fManager:screen-value = "ALL".
  fSearch:screen-value = "".
  fStatus:screen-value = "ALL".
  fState:screen-value = "ALL".
  fRegion:screen-value = "ALL".
  setFilterCombos("ALL").
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Update */
DO:
  if not available data
   then return.
  
  run actionModify in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  if not valid-handle(hNewWindow)
   then run dialogagentappnew.w persistent set hNewWindow.
   else run ShowWindow in hNewWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "LoadAgentApps".
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAgentApp
&Scoped-define SELF-NAME brwAgentApp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgentApp C-Win
ON DEFAULT-ACTION OF brwAgentApp IN FRAME fMain
DO:
  apply "CHOOSE" to bModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgentApp C-Win
ON ROW-DISPLAY OF brwAgentApp IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgentApp C-Win
ON START-SEARCH OF brwAgentApp IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgentApp C-Win
ON VALUE-CHANGED OF brwAgentApp IN FRAME fMain
DO:
  if not available data
   then return.

  do with frame {&frame-name}:
    assign
      bStart:sensitive    = (data.stat = "N")
      bCancel:sensitive   = (lookup(data.stat,"C,D,X") = 0)
      bDeny:sensitive     = bCancel:sensitive
      bApprove:sensitive  = (data.stat = "R")
      bComplete:sensitive = (data.stat = "A")
      tNotes:screen-value = data.notes
      .
    do with frame fDetails:
      if data.stat <> "N" and data.stat <> "R"
       then
        do:
          publish "GetSysPropDesc" ("AMD", "Application", "Status", "A", output std-ch).
          if data.stat = "A" or data.stat = "C"
           then tDecisionStatus:screen-value = std-ch.
           else tDecisionStatus:screen-value = data.statDesc.
        end.
       else tDecisionStatus:screen-value = "".
       
      /* dates */
      assign 
        tDateCreated:screen-value    = string(data.dateCreated, "99/99/99")
        tDateStart:screen-value      = (if data.dateStarted = ? then "" else string(data.dateStarted, "99/99/99"))
        tDateSubmitted:screen-value  = (if data.dateSubmitted = ? then "" else string(data.dateSubmitted, "99/99/99"))
        tDateERROrdered:screen-value = (if data.dateOrderedERR = ? then "" else string(data.dateOrderedERR, "99/99/99"))
        tDateERRStart:screen-value   = (if data.dateStartedERR = ? then "" else string(data.dateStartedERR, "99/99/99"))
        tDateERRFinish:screen-value  = (if data.dateReceviedERR = ? then "" else string(data.dateReceviedERR, "99/99/99"))
        tDateReview:screen-value     = (if data.dateUnderReview = ? then "" else string(data.dateUnderReview, "99/99/99"))
        tDateReviewSent:screen-value = (if data.dateSentReview = ? then "" else string(data.dateSentReview, "99/99/99"))
        std-da                       = (if data.stat = "D" or data.stat = "X" then data.dateStopped else data.dateApproved)
        tDateApproved:screen-value   = (if std-da = ? then "" else string(std-da, "99/99/99"))
        tDateAgreeSent:screen-value  = (if data.dateSentAgency = ? then "" else string(data.dateSentAgency, "99/99/99"))
        tDateCompleted:screen-value  = (if data.dateSigned = ? then "" else string(data.dateSigned, "99/99/99"))
        .
      /* metrics */
      if data.dateCreated <> ? and data.dateSubmitted <> ?
       then tMetricApplication:screen-value = string(data.metricCreatedToSubmitted).
       else tMetricApplication:screen-value = "".
      if data.dateOrderedERR <> ? and data.dateReceviedERR <> ?
       then tMetricERRStartedToFinish:screen-value = string(data.metricERRStartedToFinish).
       else tMetricERRStartedToFinish:screen-value = "".
      if data.dateCreated <> ? and data.dateUnderReview <> ?
       then tMetricCreatedToUnderReview:screen-value = string(data.metricCreatedToUnderReview).
       else tMetricCreatedToUnderReview:screen-value = "".
      if data.dateUnderReview <> ? and data.dateSentAgency <> ?
       then tMetricUnderReviewToSentAgency:screen-value = string(data.metricUnderReviewToSentAgency).
       else tMetricUnderReviewToSentAgency:screen-value = "".
      if data.dateSentAgency <> ? and data.dateSigned <> ?
       then tMetricSentAgencyToSigned:screen-value = string(data.metricSentAgencyToSigned).
       else tMetricSentAgencyToSigned:screen-value = "".
      if data.dateSentReview <> ? and (data.dateApproved <> ? or data.dateStopped <> ?)
       then tMetricSentReviewToApproved:screen-value = string(data.metricSentReviewToApproved).
       else tMetricSentReviewToApproved:screen-value = "".
      if data.dateCreated <> ? and (data.dateApproved <> ? or data.dateStopped <> ?)
       then tMetricCreatedToDecision:screen-value = string(data.metricCreatedToApproved).
       else tMetricCreatedToDecision:screen-value = "".
      if data.dateUnderReview <> ? and data.dateSentReview <> ?
       then tMetricUnderReviewToSentReview:screen-value = string(data.metricUnderReviewToSentReview).
       else tMetricUnderReviewToSentReview:screen-value = "".
      if data.dateCreated <> ? and data.dateSigned <> ?
       then tMetricCreatedToSigned:screen-value = string(data.metricCreatedToSigned).
       else tMetricCreatedToSigned:screen-value = "".
    end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  run actionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStart C-Win
ON CHOOSE OF bStart IN FRAME fMain /* Start */
DO:
  if not available data
   then return.
  
  run statusStart in this-procedure (data.agentID, data.applicationID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRegion C-Win
ON VALUE-CHANGED OF fRegion IN FRAME fMain /* Region */
DO:
  setFilterCombos(self:label).
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON LEAVE OF fSearch IN FRAME fMain /* Search */
DO:
  apply "RETURN" to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  setFilterCombos(self:label).
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  setFilterCombos(self:label).
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  setFilterCombos(self:label).
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Documents C-Win
ON CHOOSE OF MENU-ITEM m_View_Documents /* View Documents */
DO:
  apply "choose" to bDocs in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes C-Win
ON VALUE-CHANGED OF tNotes IN FRAME fMain
DO:
  if not available data
   then return.
   
  bSave:sensitive = true.
  if can-find(first origdata where notes = self:screen-value)
   then bSave:sensitive = false.
   
  data.notes = self:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "AgentAppDataChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/brw-main.i}
{lib/win-main.i}
{lib/win-status.i &Entity="'Agent Application'"}
{lib/win-close.i}
{lib/win-show.i}
/* action buttons */
{lib/set-button.i &label="Refresh" &toggle=false}
{lib/set-button.i &label="New"     &toggle=false}
{lib/set-button.i &label="Modify"}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="Docs"}
{lib/set-button.i &label="Save"}
/* status buttons */
{lib/set-button.i &label="Start"    &image="images/start.bmp"     &inactive="images/start-i.bmp"     &toggle=false}
{lib/set-button.i &label="Cancel"   &image="images/cancel.bmp"    &inactive="images/cancel-i.bmp"    &toggle=false}
{lib/set-button.i &label="Deny"     &image="images/rejected.bmp"  &inactive="images/rejected-i.bmp"  &toggle=false}
{lib/set-button.i &label="Approve"  &image="images/check.bmp"     &inactive="images/check-i.bmp"     &toggle=false}
{lib/set-button.i &label="Complete" &image="images/completed.bmp" &inactive="images/completed-i.bmp" &toggle=false}
/* filter buttons */
{lib/set-button.i &label="Filter"}
{lib/set-button.i &label="FilterClear"}
setButtons().
/* filters */
{lib/set-filter.i &label="Status"  &id="stat"              &value="statDesc"}
{lib/set-filter.i &label="State"   &id="stateID"           &value="stateName"}
{lib/set-filter.i &label="Search"  &id="agentID:agentName" &populate=false}
{lib/set-filter.i &label="Region"  &id="regionID"          &value="regionDesc"}
{lib/set-filter.i &label="Manager" &id="manager"           &value="managerDesc"}

{&window-name}:window-state = 2.

{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.

{lib/get-column-width.i &col="'agentName'" &var=dColumnAgent}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  
  clearData().
  run getData in this-procedure.
  run windowResized in this-procedure.
  
  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDocs C-Win 
PROCEDURE actionDocs :
/*------------------------------------------------------------------------------
@description View the documents for the agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as character no-undo.
  
  if valid-handle(hDocWindow)
   then run ShowWindow in hDocWindow no-error.
   else run documents.w persistent set hDocWindow ("AMD",
                                                   "/Agent/" + pAgentID + "/App" + pApplicationID,
                                                   "Agent " + pAgentID + ", Application " + pApplicationID,
                                                   "new,delete,open,modify,share,send,request",   /* permissions */
                                                   "Agent",
                                                   pAgentID,
                                                   "" /* Seq */).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as integer no-undo.
  openWindowForApplication(pAgentID, pApplicationID, "Modify", "dialogagentappmodify.w").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionSave C-Win 
PROCEDURE actionSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer data for data.
  
  empty temp-table tempdata.
  for each origdata no-lock:
    for each data no-lock
       where data.applicationID = origdata.applicationID
         and data.agentID = origdata.agentID:
      
      std-ch = "".
      buffer-compare origdata to data save result in std-ch.
      if std-ch > ""
       then
        do:
          create tempdata.
          buffer-copy data to tempdata.
        end.
    end.
  end.
  
  if can-find(first tempdata)
   then
    do:
      run server/modifyagentapp.p(input table tempdata,
                                  output std-lo,
                                  output std-ch).

      if not std-lo
       then message std-ch view-as alert-box error.
       else
        do:
          bSave:sensitive in frame {&frame-name} = false.
          for each origdata exclusive-lock:
            for each data no-lock
               where data.applicationID = origdata.applicationID
                 and data.agentID = origdata.agentID:
                 
              buffer-compare origdata to data save result in std-ch.
              if std-ch > ""
               then buffer-copy data to origdata.
            end.
          end.
          
          /* Update the data in datasrv */
          publish "updateAgentAppsData"(input table tempdata).
        end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentAppDataChanged C-Win 
PROCEDURE AgentAppDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* variables */
  define variable tAgentAppID as character no-undo.
  define variable tRowID      as rowid     no-undo.
  define variable tPosition   as integer   no-undo.
  
  /* buffers */
  define buffer tempdata for data.
  
  /* get the data */
  run getData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fRegion fState fSearch fManager fStatus tNotes tNotesLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bNew rFilter RECT-38 bSave RECT-50 rDetails fRegion fState bFilter 
         bFilterClear fSearch fManager fStatus brwAgentApp tNotes bDocs bExport 
         bRefresh tNotesLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY tDateCreated tDateStart tDateSubmitted tMetricApplication 
          tDateERROrdered tDateERRStart tDateERRFinish tMetricERRStartedToFinish 
          tDateReview tDateReviewSent tMetricUnderReviewToSentReview 
          tDateApproved tMetricSentReviewToApproved tDateAgreeSent 
          tDateCompleted tMetricSentAgencyToSigned tMetricCreatedToUnderReview 
          tMetricCreatedToDecision tMetricUnderReviewToSentAgency 
          tMetricCreatedToSigned tDecisionStatus 
      WITH FRAME fDetails IN WINDOW C-Win.
  ENABLE RECT-52 tDateCreated tDateStart tDateSubmitted tMetricApplication 
         tDateERROrdered tDateERRStart tDateERRFinish tMetricERRStartedToFinish 
         tDateReview tDateReviewSent tMetricUnderReviewToSentReview 
         tDateApproved tMetricSentReviewToApproved tDateAgreeSent 
         tDateCompleted tMetricSentAgencyToSigned tMetricCreatedToUnderReview 
         tMetricCreatedToDecision tMetricUnderReviewToSentAgency 
         tMetricCreatedToSigned tDecisionStatus 
      WITH FRAME fDetails IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDetails}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if query brwAgentApp:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "AgentApplications"

  run util/exporttoexceltable.p (temp-table data:default-buffer-handle, {&ReportName}, getWhereClause()).


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  define buffer origdata for origdata.

  empty temp-table data.
  empty temp-table origdata.
  publish "GetAgentApps" (output table origdata).

  for each origdata no-lock:
    create data.
    buffer-copy origdata to data.
  end.
  
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  setFilterCombos("ALL").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &pre-by-clause="getWhereClause() +"}
  do with frame {&frame-name}:
    apply "VALUE-CHANGED" to {&browse-name}.
  end.

  enableButtons(can-find(first data)).
  enableFilters(can-find(first data)).
  bSave:sensitive = false.
  setStatusCount(num-results("{&browse-name}")).
  apply "VALUE-CHANGED" to {&browse-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE statusApprove C-Win 
PROCEDURE statusApprove :
/*------------------------------------------------------------------------------
@description Change the agent application to Approved
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as integer no-undo.
  define variable cStatus as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Application", "Status", "A", output cStatus).
  {lib/confirm-status-change.i "Application" cStatus}
  
  publish "OpenAgentApp" (pAgentID, pApplicationID, output std-ha).
  if valid-handle(std-ha)
   then run SetStatusApprove in std-ha (output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE statusCancel C-Win 
PROCEDURE statusCancel :
/*------------------------------------------------------------------------------
@description Change the agent application to Cancelled
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as integer no-undo.
  define variable cStatus as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Application", "Status", "X", output cStatus).
  {lib/confirm-status-change.i "Application" cStatus}
  
  publish "OpenAgentApp" (pAgentID, pApplicationID, output std-ha).
  if valid-handle(std-ha)
   then run dialogagentappreason.w ("Cancel", std-ha).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE statusComplete C-Win 
PROCEDURE statusComplete :
/*------------------------------------------------------------------------------
@description Change the agent application to Complete
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as integer no-undo.
  define variable cStatus as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Application", "Status", "C", output cStatus).
  {lib/confirm-status-change.i "Application" cStatus}
  
  publish "OpenAgentApp" (pAgentID, pApplicationID, output std-ha).
  if valid-handle(std-ha)
   then run SetStatusComplete in std-ha (output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE statusDeny C-Win 
PROCEDURE statusDeny :
/*------------------------------------------------------------------------------
@description Change the agent application to Denied
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as integer no-undo.
  define variable cStatus as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Application", "Status", "D", output cStatus).
  {lib/confirm-status-change.i "Application" cStatus}
  
  publish "OpenAgentApp" (pAgentID, pApplicationID, output std-ha).
  if valid-handle(std-ha)
   then run dialogagentappreason.w ("Deny", std-ha).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE statusStart C-Win 
PROCEDURE statusStart :
/*------------------------------------------------------------------------------
@description Change the agent application to Started
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pApplicationID as integer no-undo.
  define variable cStatus as character no-undo.
  
  publish "GetSysPropDesc" ("AMD", "Application", "Status", "R", output cStatus).
  {lib/confirm-status-change.i "Application" cStatus}
  
  publish "OpenAgentApp" (pAgentID, pApplicationID, output std-ha).
  if valid-handle(std-ha)
   then run SetStatusStart in std-ha (output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dDiff as decimal no-undo.
  
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 
  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 344.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  
  /* resize the details frame */
  dDiff = frame {&frame-name}:height-pixels - {&window-name}:min-height-pixels.
  rDetails:height-pixels = {&browse-name}:height-pixels + 1.
  rDetails:x = {&browse-name}:x + {&browse-name}:width-pixels + 5.
  tNotes:height-pixels = 130 + dDiff.
  tNotes:x = rDetails:x + 10.
  tNotesLabel:x = rDetails:x + 10.
  frame fDetails:y = tNotes:y + tNotes:height-pixels.
  frame fDetails:x = rDetails:x + 10.
  
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  {lib/resize-column.i &col="'agentName'" &var=dColumnAgent}


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table data.
  empty temp-table tempdata.
  empty temp-table origdata.
  close query {&browse-name}.
  /* disable the filters */
  enableButtons(false).
  enableFilters(false).
  clearStatus().
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForApplication C-Win 
FUNCTION openWindowForApplication RETURNS HANDLE
  ( input pAgentID as character,
    input pApplicationID as integer,
    input pType as character,
    input pFile as character ) :
/*------------------------------------------------------------------------------
@description Opens a window for an agent application
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable cWindow as character no-undo.
  define variable hFileDataSrv as handle no-undo.
  define buffer openwindow for openwindow.

  assign
    hWindow = ?
    hFileDataSrv = ?
    .
  publish "GetAgentAppID" (pAgentID, pApplicationID, output cWindow).
  for first openwindow no-lock
      where openwindow.procFile = cWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      publish "OpenAgentApp" (pAgentID, pApplicationID, output hFileDataSrv).
      run value(pFile) persistent set hWindow (hFileDataSrv).
      create openwindow.
      assign
        openwindow.procFile = cWindow
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

