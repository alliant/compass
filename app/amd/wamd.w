&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wamd.w
   main Window for the Agent Management Dashboard client application
   created 11.30.2014 D.Sinclair
 @modified
 08/16/2022  SA      Task #96812 Modified to add functionality for person.
 04/20/2023  Sagar K Task #104070 AMD main screen needs to be modified as per new framework
 06/26/2023  SB      Task #104008 Modified to implement the tag report
 07/28/2023  Sagar K Task #106230 AMD main screen UI Changes.
 09/04/2023  Sagar K Format change in UI
 09/27/2023  Sagar K Task #107457 Fixed error message
 11/08/2023  SR      Modified Status And Manager combos to sort in Alphabetical order.
 11/17/2023  SRK     Task #108492 AMD Agent Communication related changes
 04/04/2024  Sachin  Task #111928 Fixed issue where labels not visible in windows 11
 11/28/2024  Shefali Task #117467 Added Agent Fulfillment Report in Reports Menu
 */

CREATE WIDGET-POOL.
          
{tt/nprdetailsolap.i &tableAlias="data"}
{tt/nprdetailsolap.i &tableAlias="tempdata"}
{tt/openwindow.i}
{tt/sysprop.i}

{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/getstatename.i}
{lib/set-filter-def.i &tableName="data"}
{lib/set-button-def.i}

/* handles to the various windows */
define variable hQLSave      as handle no-undo.
define variable hDocWindow   as handle no-undo.
define variable hCopy        as handle no-undo.

/* variables used to store original values for resize */
define variable dColumnWidth as decimal no-undo.

/* used for the filters */
define variable currState       as character no-undo.
define variable currManager     as character no-undo.
define variable currStatus      as character no-undo.
define variable currAlert       as character no-undo.
define variable cAgentIDsList   as character no-undo initial "".
define variable lReturnApplied  as logical   no-undo.
define variable lSearchApplied  as logical   no-undo.
define variable piMonth         as integer   no-undo.
define variable piyear          as integer   no-undo.

/* used for the views */
define variable currDate     as date no-undo.
define variable currFilter   as character no-undo.

define variable hPerson as handle no-undo.

&scoped-define decFormat "$-ZZ,ZZZ,ZZ9.99"
&scoped-define intformat "$-ZZ,ZZZ,ZZZ,ZZ9"
&scoped-define pctformat "ZZ,ZZZ,ZZ9.99 %"
&scoped-define monthAbbr "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec"

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData /* data.alertShow NO-LABEL */ /* data.hasDocument */ data.Favorite data.agentID data.Name data.statDesc data.StateID data.Manager data.Netpremium2monthprev data.Netpremium1monthprev data.Netpremium0monthprev data.NetPlan data.NetpremiumdiffNetPlan data.NetpremiumYTD data.NetPlanYTD data.NetpremiumDiffplanYTD   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data. APPLY "VALUE-CHANGED" TO BROWSE brwData
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data. APPLY "VALUE-CHANGED" TO BROWSE brwData.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bFilterClear fState fStatus fManager ~
YTDvariance fAgent tView bNextMonth monthvariance bPrevMonth ~
tPlanActualMonth tActualMonth tPlanMonth tTotalLabel Planned tYTDMonth ~
tActualYTD tPlanActualYTD Actual ActualToPlan rTotal 
&Scoped-Define DISPLAYED-OBJECTS fState fStatus fManager YTDvariance fAgent ~
tView monthvariance tPlanActualMonth tActualMonth tPlanMonth tTotalLabel ~
Planned tYTDMonth tActualYTD tPlanActualYTD Actual ActualToPlan 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD closeWindow C-Win 
FUNCTION closeWindow RETURNS HANDLE
  ( input pType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD closeWindowForAgent C-Win 
FUNCTION closeWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD closeWindowForAgentAll C-Win 
FUNCTION closeWindowForAgentAll RETURNS HANDLE
  ( input pAgentID as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getTotal C-Win 
FUNCTION getTotal RETURNS decimal
  ( input pBufferField as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getViewColumn C-Win 
FUNCTION getViewColumn RETURNS CHARACTER
  ( INPUT pID AS CHARACTER,
    input pQuery as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindow C-Win 
FUNCTION openWindow RETURNS handle PRIVATE
  ( input pFile as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgentAction C-Win 
FUNCTION openWindowForAgentAction returns handle
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForCodes C-Win 
FUNCTION openWindowForCodes RETURNS handle
  ( input pCodeType as character,
    input pTitle    as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setManagerCombo C-Win 
FUNCTION setManagerCombo RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Module_Configure LABEL "Configure..."  
       RULE
       MENU-ITEM m_Module_About LABEL "About..."      
       RULE
       MENU-ITEM m_Module_Exit  LABEL "Exit"          .

DEFINE SUB-MENU m_Action 
       MENU-ITEM m_Action_View_Details LABEL "View Details"  
       MENU-ITEM m_Action_Documents LABEL "View Documents"
       MENU-ITEM m_Action_Notes LABEL "View Notes"    
       RULE
       MENU-ITEM m_Action_New_Note LABEL "New Note"      
       RULE
       MENU-ITEM m_Action_New_Agent LABEL "New Agent"     
       MENU-ITEM m_Action_Copy_Details LABEL "Copy Agent Details"
       MENU-ITEM m_Action_Delete_Agent LABEL "Delete Agent"  .

DEFINE SUB-MENU m_References 
       MENU-ITEM m_References_Regions LABEL "Regions"       
       MENU-ITEM m_References_States LABEL "States"        
       MENU-ITEM m_Reference_Counties LABEL "Counties"      
       MENU-ITEM m_References_Periods LABEL "Periods"       
       MENU-ITEM m_References_Offices LABEL "Offices"       
       MENU-ITEM m_Amd_Person   LABEL "People"        
       MENU-ITEM m_References_Distributions LABEL "Contact Groups"
       RULE
       MENU-ITEM m_References_Reason_Codes LABEL "Reason Codes"  
       MENU-ITEM m_Reference_Alert_Codes LABEL "Alert Codes"   .

DEFINE SUB-MENU m_State_Reports 
       MENU-ITEM m_Report_CPL_Volume LABEL "CPL for Preceding 3 Months"
       MENU-ITEM m_Report_ICL   LABEL "CPL Monthly"   
       MENU-ITEM m_Report_Policy_Volume LABEL "Policy Inventory by Agent"
       MENU-ITEM m_Reports_Unreported_Policies LABEL "Unreported Policies"
       RULE
       MENU-ITEM m_Reports_Claim_Costs_Incurred LABEL "Claim Costs Incurred"
       MENU-ITEM m_Report_Claims_Ratio LABEL "Claims Ratio"  
       MENU-ITEM m_Claims_By_Agent LABEL "Claims By Agent"
       RULE
       MENU-ITEM m_Reports_Agents_for_Period LABEL "Remittances by Agent for a Period"
       MENU-ITEM m_Report_Agent_Manager LABEL "Remittances by Manager for a Period"
       MENU-ITEM m_Report_State_Batch_Acitivity LABEL "Remittances by State across Periods"
       MENU-ITEM m_Batches_for_Period LABEL "Batches for Period"
       RULE
       MENU-ITEM m_Report_Data_Call LABEL "Business Mix (Data Call)"
       MENU-ITEM m_Reports_Agent_Data_Call LABEL "Agent Data Call"
       MENU-ITEM m_Report_Transaction_Summary LABEL "Transaction Summary"
       RULE
       MENU-ITEM m_Activity_-_Year LABEL "Activity - Year"
       MENU-ITEM m_Activity_-_Year_over_Year LABEL "Activity - Year over Year"
       MENU-ITEM m_Agent_Worth  LABEL "Activity - Agent Life to Date"
       RULE
       MENU-ITEM m_Agent_Summary LABEL "Agent Summary" 
       MENU-ITEM m_Agent_Tags   LABEL "Agent Tags"    
       MENU-ITEM m_People_Tags  LABEL "People Tags"   
       RULE
       MENU-ITEM m_Agent_Communication LABEL "Agent Communication"
       MENU-ITEM m_Agent_Fulfillment_Report LABEL "Agent Fulfillment Report".

DEFINE SUB-MENU m_Tools 
       MENU-ITEM m_Tools_Applications LABEL "Applications"  
       RULE
       MENU-ITEM m_Tools_Activities LABEL "Maintain Planned Activity"
       MENU-ITEM m_QL_Save_Menu LABEL "Quick Lists"   
       RULE
       MENU-ITEM m_View_Saved_Consolidations LABEL "Consolidations".

DEFINE SUB-MENU m_Alerts 
       MENU-ITEM m_Tools_Alerts LABEL "View Open Alerts"
       MENU-ITEM m_Tools_Alerts_Historical LABEL "View Historical Alerts"
       RULE
       MENU-ITEM m_Action_Run_Alerts LABEL "Evaluate Automated Alerts"
       MENU-ITEM m_Tools_GP_Receivables LABEL "Evaluate Account Receivables Alerts".

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_Action       LABEL "Action"        
       SUB-MENU  m_References   LABEL "References"    
       SUB-MENU  m_State_Reports LABEL "Reports"       
       SUB-MENU  m_Tools        LABEL "Tools"         
       SUB-MENU  m_Alerts       LABEL "Alerts"        .

DEFINE MENU POPUP-MENU-brwData 
       MENU-ITEM m_Popup_View_Details LABEL "View Details"  
       MENU-ITEM m_Popup_Agent_Review LABEL "Agent Summary" 
       MENU-ITEM m_Following    LABEL "Following"     
       MENU-ITEM m_Export       LABEL "Export"        
       MENU-ITEM m_Refresh      LABEL "Refresh"       
       RULE
       MENU-ITEM m_Popup_Open_Alerts LABEL "View Open Alerts"
       MENU-ITEM m_Popup_Run_Alerts LABEL "Evaluate Automated Alerts"
       RULE
       MENU-ITEM m_Popup_Documents LABEL "View Documents"
       MENU-ITEM m_Popup_Notes  LABEL "View Notes"    
       MENU-ITEM m_Popup_New_Note LABEL "Add New Note"  .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear all filters".

DEFINE BUTTON bNextMonth 
     LABEL "Next" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bPrevMonth 
     LABEL "Prev" 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE fManager AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE Actual AS CHARACTER FORMAT "X(256)":U INITIAL "Actual" 
      VIEW-AS TEXT 
     SIZE 10.6 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE ActualToPlan AS CHARACTER FORMAT "X(256)":U INITIAL "Variance" 
      VIEW-AS TEXT 
     SIZE 11.4 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE fAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 90.6 BY 1 TOOLTIP "Enter keywords or #tags separated by spaces" NO-UNDO.

DEFINE VARIABLE monthvariance AS INTEGER FORMAT "-ZZ9%":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 7 BY .62 NO-UNDO.

DEFINE VARIABLE Planned AS CHARACTER FORMAT "X(256)":U INITIAL "Planned" 
      VIEW-AS TEXT 
     SIZE 12.4 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE tActualMonth AS INTEGER FORMAT "$ >>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tActualYTD AS INTEGER FORMAT "$>>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tPlanActualMonth AS INTEGER FORMAT "$ >>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tPlanActualYTD AS INTEGER FORMAT "$ >>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE tPlanMonth AS INTEGER FORMAT "$ >>>,>>>,>>9":U INITIAL 0 
     LABEL "Month" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Totals" 
      VIEW-AS TEXT 
     SIZE 6.8 BY .52 NO-UNDO.

DEFINE VARIABLE tYTDMonth AS INTEGER FORMAT "$ >>>,>>>,>>9":U INITIAL 0 
     LABEL "YTD" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE YTDvariance AS INTEGER FORMAT "-ZZ9%":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE tView AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "P", "P",
"A", "A"
     SIZE 3 BY 2.29 NO-UNDO.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 113 BY 3.24.

DEFINE RECTANGLE rTotal
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 114 BY 3.24.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      /* data.alertShow NO-LABEL WIDTH 1.5 */
/*  data.hasDocument view-as toggle-box */
 data.Favorite width 4 label ""
 data.agentID format "x(11)" width 11 label "Agent ID"
 data.Name format "x(200)" width 31 label "Name"
 data.statDesc format "x(11)" width 11
 data.StateID  width 8 label "State ID"
 data.Manager format "x(32)" width 16
 data.Netpremium2monthprev   width 14
 data.Netpremium1monthprev width 14
 data.Netpremium0monthprev  width 14
 data.NetPlan        width 14
 data.NetpremiumdiffNetPlan    width 15
 data.NetpremiumYTD   width 14     label "YTD Actual"
 data.NetPlanYTD     width 14   label "YTD Plan"
 data.NetpremiumDiffplanYTD    width 19
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 226 BY 18 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 4.67 COL 2 WIDGET-ID 300
     bFilterClear AT ROW 2 COL 104.6 WIDGET-ID 344 NO-TAB-STOP 
     fState AT ROW 1.81 COL 9.2 COLON-ALIGNED WIDGET-ID 294
     fStatus AT ROW 1.81 COL 43.4 COLON-ALIGNED WIDGET-ID 140
     fManager AT ROW 1.81 COL 74.6 COLON-ALIGNED WIDGET-ID 296
     YTDvariance AT ROW 3.38 COL 187 NO-LABEL WIDGET-ID 348 NO-TAB-STOP 
     fAgent AT ROW 2.91 COL 9.2 COLON-ALIGNED WIDGET-ID 322
     tView AT ROW 2.1 COL 216 NO-LABEL WIDGET-ID 340
     bNextMonth AT ROW 1 COL 224 WIDGET-ID 320
     monthvariance AT ROW 2.24 COL 187 NO-LABEL WIDGET-ID 350 NO-TAB-STOP 
     bPrevMonth AT ROW 2.29 COL 224 WIDGET-ID 318
     tPlanActualMonth AT ROW 2.05 COL 194.6 COLON-ALIGNED NO-LABEL WIDGET-ID 330 NO-TAB-STOP 
     tActualMonth AT ROW 2.05 COL 159.6 COLON-ALIGNED NO-LABEL WIDGET-ID 328 NO-TAB-STOP 
     tPlanMonth AT ROW 2.05 COL 126.6 COLON-ALIGNED WIDGET-ID 326 NO-TAB-STOP 
     tTotalLabel AT ROW 1 COL 114 COLON-ALIGNED NO-LABEL WIDGET-ID 308
     Planned AT ROW 1.38 COL 126.8 COLON-ALIGNED NO-LABEL WIDGET-ID 312
     tYTDMonth AT ROW 3.14 COL 126.6 COLON-ALIGNED WIDGET-ID 302 NO-TAB-STOP 
     tActualYTD AT ROW 3.14 COL 159.6 COLON-ALIGNED NO-LABEL WIDGET-ID 330 NO-TAB-STOP 
     tPlanActualYTD AT ROW 3.14 COL 194.6 COLON-ALIGNED NO-LABEL WIDGET-ID 306 NO-TAB-STOP 
     Actual AT ROW 1.38 COL 159.8 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     ActualToPlan AT ROW 1.38 COL 194.6 COLON-ALIGNED NO-LABEL WIDGET-ID 316
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .52 AT ROW 1 COL 3.4 WIDGET-ID 290
     "" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.52 COL 214 WIDGET-ID 346
     rFilter AT ROW 1.24 COL 2 WIDGET-ID 288
     rTotal AT ROW 1.24 COL 114.6 WIDGET-ID 298
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 232.6 BY 24.44 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 22.48
         WIDTH              = 228.8
         MAX-HEIGHT         = 46.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 46.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       brwData:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwData:HANDLE
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN monthvariance IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       monthvariance:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE rFilter IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tActualMonth:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tActualYTD:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPlanActualMonth:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPlanActualYTD:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPlanMonth:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tYTDMonth:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN YTDvariance IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       YTDvariance:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
APPLY "VALUE-CHANGED" TO BROWSE brwData.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
  {lib/confirm-exit.i}
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME fMain /* Clear */
DO:
  assign
    fState:screen-value   = "ALL"
    fStatus:screen-value  = "ALL"
    /*fAlert:screen-value   = "ALL"  */
    fManager:screen-value = "ALL"
    fAgent:screen-value   = ""
    .
  dynamic-function("setFilterCombos", "ALL").
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  run setResetFilterBtn in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNextMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNextMonth C-Win
ON CHOOSE OF bNextMonth IN FRAME fMain /* Next */
DO:
  
/*  publish "LoadAgentSummaries" (currDate).
  run ActionRefresh in this-procedure ().  */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrevMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrevMonth C-Win
ON CHOOSE OF bPrevMonth IN FRAME fMain /* Prev */
DO:
 /* currDate = add-interval(currDate,-1,"months").
  publish "LoadAgentSummaries" (currDate).
  run ActionRefresh in this-procedure (currDate).    */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available data
   then return.
   
  run ActionModify in this-procedure (data.agentID). 
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  /* colorize the screen */
  std-in = ?.
/*  case data.alertShow:
   when "0" then data.alertNoneCnt:BGCOLOR IN BROWSE {&browse-name} = 2.
   when "1" then data.alertWarnCnt:BGCOLOR IN BROWSE {&browse-name} = 14.
   when "2" then assign
                   data.alertCritCnt:BGCOLOR IN BROWSE {&browse-name} = 12
                   data.alertCritCnt:FGCOLOR IN BROWSE {&browse-name} = 15.
  end case.    */
  
  /*
  data.alertShow:bgcolor in browse {&browse-name} = std-in.
  /* Get only the first letter of the name */
  std-ch = "".
  publish "GetSysPropDesc" ("AMD", "Alert", "Severity", data.alertShow, output std-ch).
  data.alertShow:screen-value in browse {&browse-name} = substring(std-ch, 1, 1).
  */
  if data.NetpremiumdiffNetPlan < 0 then
   do:
    data.NetpremiumdiffNetPlan:fgcolor in browse brwData = 12.
   end.
  if data.NetpremiumDiffplanYTD < 0 then
   do:
    data.NetpremiumDiffplanYTD:fgcolor in browse brwData = 12.
   end.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  hSortColumn = browse {&browse-name}:current-column.
  if hSortColumn:label = ""
   then 
    hSortColumn:label = "    isFavorite".
    
  {lib/brw-startSearch.i}
  
  if not available data
   then return.
   else
    do:
      publish "SetCurrentValue" ("AgentID", data.agentID).
      publish "SetCurrentValue" ("StateID", data.StateID).
      publish "SetCurrentValue" ("AgentStatus", data.stat).
    end.
    
  if hSortColumn:label = "    isFavorite"
   then 
    hSortColumn:label = "".  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  define variable cToolTip as character no-undo initial "".
  
  do with frame {&frame-name}:
    assign
     /* bNotes:sensitive = available data
      bNewNote:sensitive = available data   */
      .
  end.
  
  ASSIGN
     MENU-ITEM m_Popup_View_Details:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Following:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Popup_Agent_Review:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Popup_Open_Alerts:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Popup_Run_Alerts:sensitive IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Popup_Documents:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Popup_Notes:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Popup_New_Note:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Export:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     MENU-ITEM m_Refresh:SENSITIVE IN MENU POPUP-MENU-brwData = AVAILABLE data
     
     MENU-ITEM m_Action_View_Details:SENSITIVE IN MENU m_Action = AVAILABLE data
     MENU-ITEM m_Action_Documents:SENSITIVE IN MENU m_Action = AVAILABLE data
     MENU-ITEM m_Action_Notes:SENSITIVE IN MENU m_Action = AVAILABLE data
     MENU-ITEM m_Action_New_Note:SENSITIVE IN MENU m_Action = AVAILABLE data
     MENU-ITEM m_Action_Copy_Details:SENSITIVE IN MENU m_Action = AVAILABLE data
     MENU-ITEM m_Action_Delete_Agent:SENSITIVE IN MENU m_Action = AVAILABLE data
      .
   
  if available data and data.isFavorite <> ""
   then
    menu-item m_Following:label in menu POPUP-MENU-brwData = (if logical(data.isFavorite) then "Stop Following" else "Start Following").  
    
  if not available data
   then return.
   else
    do:
      publish "SetCurrentValue" ("AgentID", data.agentID).
      publish "SetCurrentValue" ("StateID", data.StateID).
      publish "SetCurrentValue" ("AgentStatus", data.stat).

      if data.isFavorite <> "" 
       then
      /* Following tooltip */
      cToolTip = addDelimiter(cToolTip, ", ") + if logical(data.isFavorite) then "Following" else "".
      /* critical alerts tooltip */
 /*     do std-in = 1 to num-entries(data.alertCrit):
        std-ch = "".
        publish "GetSysCodeDesc" ("Alert", trim(entry(std-in, data.alertCrit)), output std-ch).
        cToolTip = addDelimiter(cToolTip, ", ") + std-ch.
      end. 
      /* warning alerts tooltip */
      do std-in = 1 to num-entries(data.alertWarn):
        std-ch = "".
        publish "GetSysCodeDesc" ("Alert", trim(entry(std-in, data.alertWarn)), output std-ch).
        cToolTip = addDelimiter(cToolTip, ", ") + std-ch.
      end.
      
      self:tooltip = cToolTip.   */
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON LEAVE OF fAgent IN FRAME fMain /* Search */
DO:

  if lReturnApplied = true 
   then
    return.

  run getSearchedAgents in this-procedure.  
     
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  
  if not available data
   then return.
   else
    do:
      publish "SetCurrentValue" ("AgentID", data.agentID).
      publish "SetCurrentValue" ("StateID", data.StateID).
      publish "SetCurrentValue" ("AgentStatus", data.stat).
    end.
    
  lSearchApplied = true.  
  run setResetFilterBtn in this-procedure.
  lSearchApplied = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON RETURN OF fAgent IN FRAME fMain /* Search */
DO:
  lReturnApplied = true. /* when return key is hit */
  
  run getSearchedAgents in this-procedure. 
  
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  
  if not available data
   then return.
   else
    do:
      publish "SetCurrentValue" ("AgentID", data.agentID).
      publish "SetCurrentValue" ("StateID", data.StateID).
      publish "SetCurrentValue" ("AgentStatus", data.stat).
    end.

  lSearchApplied = true.  
  run setResetFilterBtn in this-procedure.
  assign
      lSearchApplied = true
      lReturnApplied = false.
  return no-apply.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fManager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fManager C-Win
ON VALUE-CHANGED OF fManager IN FRAME fMain /* Manager */
DO:
  setFilterCombos(self:label).
  setManagerCombo().
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  run setResetFilterBtn in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  setFilterCombos(self:label).
  setManagerCombo().
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  run setResetFilterBtn in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  setFilterCombos(self:label).
  setManagerCombo().
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
  run setResetFilterBtn in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_Copy_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_Copy_Details C-Win
ON CHOOSE OF MENU-ITEM m_Action_Copy_Details /* Copy Agent Details */
DO:
  if not available data then
    return.

  run ActionCopy in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_Delete_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_Delete_Agent C-Win
ON CHOOSE OF MENU-ITEM m_Action_Delete_Agent /* Delete Agent */
DO:
  if not available data
   then return.
  
  run ActionDelete in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_Documents C-Win
ON CHOOSE OF MENU-ITEM m_Action_Documents /* View Documents */
DO:
  if not available data
   then return.
   
  run ActionDocuments in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_New_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_New_Agent C-Win
ON CHOOSE OF MENU-ITEM m_Action_New_Agent /* New Agent */
do:
  openWindowForAgentAction("", "agentDetails", "dialogagent.w", "N").
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_New_Note
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_New_Note C-Win
ON CHOOSE OF MENU-ITEM m_Action_New_Note /* New Note */
DO:
if not available data
   then return.
   
  openWindowForAgent(data.agentID, "notesAdd", "dialogagentnoteadd.w"). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_Notes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_Notes C-Win
ON CHOOSE OF MENU-ITEM m_Action_Notes /* View Notes */
DO:
if not available data
   then return.
   
  run ActionNotes in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_Run_Alerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_Run_Alerts C-Win
ON CHOOSE OF MENU-ITEM m_Action_Run_Alerts /* Evaluate Automated Alerts */
DO:
  if not available data
   then return.
   
  define buffer openwindow for openwindow.
  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.

  std-ha = ?.
  std-ch = data.agentID + " alert preview".
  for first openwindow no-lock
      where openwindow.procFile = std-ch:
      
    std-ha = openwindow.procHandle.
  end.
  
  if not valid-handle(std-ha) then
  do:
    run dialogalertpreview.w persistent set std-ha (data.agentID).

    create openwindow.
    assign openwindow.procFile   = std-ch
           openwindow.procHandle = std-ha.
  end.

  run ShowWindow in std-ha no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_View_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_View_Details C-Win
ON CHOOSE OF MENU-ITEM m_Action_View_Details /* View Details */
DO:
  run ActionModify in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Activity_-_Year
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Activity_-_Year C-Win
ON CHOOSE OF MENU-ITEM m_Activity_-_Year /* Activity - Year */
DO:
  run rpt/agentactivity.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Activity_-_Year_over_Year
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Activity_-_Year_over_Year C-Win
ON CHOOSE OF MENU-ITEM m_Activity_-_Year_over_Year /* Activity - Year over Year */
DO:
  run rpt/agentactivityyoy.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Communication
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Communication C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Communication /* Agent Communication */
DO:
  openWindow("wagentcommunication.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Fulfillment_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Fulfillment_Report C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Fulfillment_Report /* Agent Fulfillment Report */
DO:
   openWindow("wfulfillmentrpt-agent.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Summary C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Summary /* Agent Summary */
DO:
  if not available data 
   then 
    return.
  
  publish "openAgentPDF" (input "wagentsummarypdf",
                        input string(data.agentID),
                        input string(data.name),                        
                        input "wagentsummarypdf.w",
                        input "character|input|" + data.agentID + "^character|input|" + data.name,                                   
                        input this-procedure).   

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Tags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Tags C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Tags /* Agent Tags */
DO:
  run rpt/wagenttags.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Worth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Worth C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Worth /* Activity - Agent Life to Date */
DO:
  run rpt/agentltd.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Amd_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Amd_Person C-Win
ON CHOOSE OF MENU-ITEM m_Amd_Person /* People */
DO:
  if valid-handle(hPerson) 
   then 
    run ShowWindow in hPerson no-error.
  else
   run wPersons.w persistent set hPerson. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Batches_for_Period
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Batches_for_Period C-Win
ON CHOOSE OF MENU-ITEM m_Batches_for_Period /* Batches for Period */
DO:
  run rpt/batchforperiod.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Claims_By_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Claims_By_Agent C-Win
ON CHOOSE OF MENU-ITEM m_Claims_By_Agent /* Claims By Agent */
DO:
  run rpt/claimsbyagent.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export C-Win
ON CHOOSE OF MENU-ITEM m_Export /* Export */
DO:
  run ExportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Following
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Following C-Win
ON CHOOSE OF MENU-ITEM m_Following /* Following */
DO:
  if not available data
   then
    return.
    

  publish "ActionFollow" (input data.agentID,
                          input data.isFavorite).
                          
  run ActionRefresh in this-procedure.
                          
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module_About C-Win
ON CHOOSE OF MENU-ITEM m_Module_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Module_Configure /* Configure... */
DO:
  openWindow("dialogamdconfig.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Module_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_People_Tags
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_People_Tags C-Win
ON CHOOSE OF MENU-ITEM m_People_Tags /* People Tags */
DO:
  run rpt/wpeopletags.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Agent_Review
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Agent_Review C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Agent_Review /* Agent Summary */
DO:
  if not available data
   then return.
   
 /* run ActionReview in this-procedure (data.agentID).      */
 publish "openAgentPDF" (input "wagentsummarypdf",
                        input string(data.agentID),
                        input string(data.name),                        
                        input "wagentsummarypdf.w",
                        input "character|input|" + data.agentID + "^character|input|" + data.name,                                   
                        input this-procedure).   
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Documents C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Documents /* View Documents */
DO:
  if not available data
   then return.
   
  run ActionDocuments in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_New_Note
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_New_Note C-Win
ON CHOOSE OF MENU-ITEM m_Popup_New_Note /* Add New Note */
DO:
 if not available data
   then return.
   
  openWindowForAgent(data.agentID, "notesAdd", "dialogagentnoteadd.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Notes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Notes C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Notes /* View Notes */
DO:
 if not available data
   then return.
   
  run ActionNotes in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Open_Alerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Open_Alerts C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Open_Alerts /* View Open Alerts */
DO:
  std-ha = openWindow("wamd01-t.w").
  if valid-handle(std-ha)
   then run GetDataForAgent in std-ha.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Run_Alerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Run_Alerts C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Run_Alerts /* Evaluate Automated Alerts */
DO:
  apply "CHOOSE" to menu-item m_Action_Run_Alerts in menu m_Alerts.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_View_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_View_Details C-Win
ON CHOOSE OF MENU-ITEM m_Popup_View_Details /* View Details */
DO:
  run ActionModify in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_QL_Save_Menu
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_QL_Save_Menu C-Win
ON CHOOSE OF MENU-ITEM m_QL_Save_Menu /* Quick Lists */
DO:
  if valid-handle(hQLSave)
   then run ShowWindow in hQLSave.
   else run sys/wlistsave.w persistent set hQLSave ("agent").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Distributions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Distributions C-Win
ON CHOOSE OF MENU-ITEM m_References_Distributions /* Distribution Groups */
DO:
  openWindow("referencedistribution.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Offices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Offices C-Win
ON CHOOSE OF MENU-ITEM m_References_Offices /* Offices */
DO:
  openWindow("referenceoffice.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Periods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Periods C-Win
ON CHOOSE OF MENU-ITEM m_References_Periods /* Periods */
DO:
  openWindow("referenceperiod.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Reason_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Reason_Codes C-Win
ON CHOOSE OF MENU-ITEM m_References_Reason_Codes /* Reason Codes */
DO:
  openWindowForCodes("AgentAppReasonCode", "Agent Application Reason").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Regions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Regions C-Win
ON CHOOSE OF MENU-ITEM m_References_Regions /* Regions */
DO:
  openWindow("referenceregion.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_States
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_States C-Win
ON CHOOSE OF MENU-ITEM m_References_States /* States */
DO:
  openWindow("referencestate.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reference_Alert_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reference_Alert_Codes C-Win
ON CHOOSE OF MENU-ITEM m_Reference_Alert_Codes /* Alert Codes */
DO:
  openWindowForCodes("Alert", "Alert").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reference_Counties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reference_Counties C-Win
ON CHOOSE OF MENU-ITEM m_Reference_Counties /* Counties */
DO:
  openWindow("referencecounty.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Refresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Refresh C-Win
ON CHOOSE OF MENU-ITEM m_Refresh /* Refresh */
DO:
/*  publish "LoadAgentSummaries" (4/1/2022).    */

   publish "LoadAgentnpr"(output table data,output piMonth, output piYear).
   
   for each data where logical(data.isFavorite) = true no-lock:
     assign
       data.Favorite = "  * " .
    end.       
   OPEN QUERY brwData FOR EACH data.
   
   setFilterCombos("ALL").
   setManagerCombo().
   dataSortDesc = not dataSortDesc.
   run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reports_Agents_for_Period
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reports_Agents_for_Period C-Win
ON CHOOSE OF MENU-ITEM m_Reports_Agents_for_Period /* Remittances by Agent for a Period */
DO:
  run rpt/agentforperiod.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reports_Agent_Data_Call
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reports_Agent_Data_Call C-Win
ON CHOOSE OF MENU-ITEM m_Reports_Agent_Data_Call /* Agent Data Call */
DO:
  run rpt/agentdatacall.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reports_Claim_Costs_Incurred
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reports_Claim_Costs_Incurred C-Win
ON CHOOSE OF MENU-ITEM m_Reports_Claim_Costs_Incurred /* Claim Costs Incurred */
DO:
  run rpt/claimscostincurred.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reports_Unreported_Policies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reports_Unreported_Policies C-Win
ON CHOOSE OF MENU-ITEM m_Reports_Unreported_Policies /* Unreported Policies */
DO:
  run rpt/unreportedpolicies.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Agent_Manager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Agent_Manager C-Win
ON CHOOSE OF MENU-ITEM m_Report_Agent_Manager /* Remittances by Manager for a Period */
DO:
  run rpt/agentmanager.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Claims_Ratio
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Claims_Ratio C-Win
ON CHOOSE OF MENU-ITEM m_Report_Claims_Ratio /* Claims Ratio */
DO:
  run rpt/claimsratio.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_CPL_Volume
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_CPL_Volume C-Win
ON CHOOSE OF MENU-ITEM m_Report_CPL_Volume /* CPL for Preceding 3 Months */
DO:
  run rpt/cplvolume.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Data_Call
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Data_Call C-Win
ON CHOOSE OF MENU-ITEM m_Report_Data_Call /* Business Mix (Data Call) */
DO:
  run rpt/businessmix.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_ICL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_ICL C-Win
ON CHOOSE OF MENU-ITEM m_Report_ICL /* CPL Monthly */
DO:
  run rpt/reporticl.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Policy_Volume
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Policy_Volume C-Win
ON CHOOSE OF MENU-ITEM m_Report_Policy_Volume /* Policy Inventory by Agent */
DO:
  run rpt/policyvolume.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_State_Batch_Acitivity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_State_Batch_Acitivity C-Win
ON CHOOSE OF MENU-ITEM m_Report_State_Batch_Acitivity /* Remittances by State across Periods */
DO:
  run rpt/statebatchactivity.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Transaction_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Transaction_Summary C-Win
ON CHOOSE OF MENU-ITEM m_Report_Transaction_Summary /* Transaction Summary */
DO:
  run rpt/transactionsummary.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Tools_Activities
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Tools_Activities C-Win
ON CHOOSE OF MENU-ITEM m_Tools_Activities /* Maintain Planned Activity */
DO:
  openWindow("wamd03-t.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Tools_Alerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Tools_Alerts C-Win
ON CHOOSE OF MENU-ITEM m_Tools_Alerts /* View Open Alerts */
DO:
  openWindow("wamd01-t.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Tools_Alerts_Historical
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Tools_Alerts_Historical C-Win
ON CHOOSE OF MENU-ITEM m_Tools_Alerts_Historical /* View Historical Alerts */
DO:
  openWindow("wamd02-t.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Tools_Applications
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Tools_Applications C-Win
ON CHOOSE OF MENU-ITEM m_Tools_Applications /* Applications */
DO:
  openWindow("wamd04-t.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Tools_GP_Receivables
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Tools_GP_Receivables C-Win
ON CHOOSE OF MENU-ITEM m_Tools_GP_Receivables /* Evaluate Account Receivables Alerts */
DO:
  run rpt/gpreceivables.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}
{lib/win-status.i &entity="'Agent'"}
{lib/set-filter.i &label="State"  &id="StateID"   &value="stateName"}
/*{lib/set-filter.i &label="Alert"   &id="alertShow" &value="alertDesc"}*/
{lib/set-filter.i &label="Status"  &id="statDesc" &value="statDesc" }
{lib/set-filter.i &label="Manager" &id="manager" &populate=false}
{lib/set-button.i &label="Refresh" &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="Notes"}
{lib/set-button.i &label="NewNote"}
{lib/set-button.i &label="FilterClear" &toggle=false}
{lib/set-button.i &label="PrevMonth" &image="images/s-previous.bmp" &inactive="images/s-previous-i.bmp"}
{lib/set-button.i &label="NextMonth" &image="images/s-next.bmp" &inactive="images/s-next-i.bmp"}
setButtons().

{&window-name}:window-state = 2.

subscribe to "ActionWindow" anywhere.
subscribe to "ActionWindowClose" anywhere.
subscribe to "ActionWindowForAgent" anywhere.

/* ActionWindowForAgentAction is copy of ActionWindowForAgent
   with one additional input parameter for dialogagent.w. */
subscribe to "ActionWindowForAgentAction" anywhere.

subscribe to "ActionWindowForAgentClose" anywhere.
subscribe to "ActionWindowForAgentCloseAll" anywhere.
subscribe to "AgentDataChanged" anywhere.
subscribe to "AgentSelected" anywhere.
subscribe to "AlertDataChanged" anywhere.
subscribe to "UserDataChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

publish "GetSysProps" (output table sysprop).

clearStatus().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  
  /* set the original column and width */
  assign
    fState:screen-value   = "ALL"
    fManager:screen-value = "ALL"
    fStatus:screen-value  = "ALL"
   /* fAlert:screen-value   = "ALL"   */
  /*  tView:screen-value    = "A"  */
    .
  
  {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
   
/*  publish "GetDefaultView" (output currFilter).
  {lib/validate-sysprop.i &appCode="'AMD'" &objAction="'Main'" &objProperty="'View'" &objID=currFilter}
  if not std-lo
   then currFilter = "NP".
  
  case currFilter:
   when "NP" then menu-item m_View_Net_Premium:checked in menu m_View = true.
   when "GP" then menu-item m_View_Gross_Premium:checked in menu m_View = true.
   when "RP" then menu-item m_View_Retained_Premium:checked in menu m_View = true.
   when "BC" then menu-item m_View_Number_Batches:checked in menu m_View = true.
   when "FC" then menu-item m_View_Number_Files:checked in menu m_View = true.
   when "PC" then menu-item m_View_Number_Policies:checked in menu m_View = true.
   when "RG" then menu-item m_View_Report_Gap:checked in menu m_View = true.
  end case.     */
  
  run ActionRefresh in this-procedure.
  run WindowResized in this-procedure.
  
   currDate = date(string(piMonth) + "/" + "01" + "/" + string(piYear)).

  
  /* change the status to what the user set */
  std-ch = "".
  publish "GetDefaultStatus" (output std-ch).
  if lookup(std-ch,fStatus:list-item-pairs) > 0
   then
    do:
      fStatus:screen-value = std-ch.
      setFilterCombos("Status").
      run sortData in this-procedure ("NetpremiumDiffplanYTD").
    end.
  
  run setResetFilterBtn in this-procedure.
  
  {&window-name}:window-state = 3.
  
 /* assign
    menu-item m_View_Next_Three_Months:sensitive in menu m_View = false
    bNextMonth:sensitive = false     */
    .
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
    
  /* as per new requirement hidden the view part */
  assign
   bNextMonth:hidden = true
   bPrevMonth:hidden = true
   tView:hidden      = true
   /*fAlert:hidden     = true */.
   
 
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionCopy C-Win 
PROCEDURE ActionCopy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  openWindowForAgentAction(pAgentID, "agentDetails", "dialogagent.w", "C").

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete C-Win 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  std-lo = true.
  publish "GetConfirmDelete" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      MESSAGE "Agent will be permanently removed. Continue?" VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
      if not std-lo 
       then return.
    end.
  
  publish "DeleteAgent" (pAgentID, output std-lo).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDocuments C-Win 
PROCEDURE ActionDocuments :
/*------------------------------------------------------------------------------
@description View the documents for the agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  if valid-handle(hDocWindow)
   then run ShowWindow in hDocWindow no-error.
   else run documents.w persistent set hDocWindow ("AMD",
                                                   "/Agent/" + pAgentID,
                                                   "Agent " + pAgentID,
                                                   "new,delete,open,modify,share,send,request",   /* permissions */
                                                   "Agent",
                                                   pAgentID,
                                                   "" /* Seq */).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify C-Win 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
@description View the agent's details
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
   
  openWindowForAgentAction(pAgentID, "agentDetails", "wagent.w", "E").
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNotes C-Win 
PROCEDURE ActionNotes :
/*------------------------------------------------------------------------------
@description View the notes for the agent     
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  publish "GetNoteWindow" (output std-ch).
  if std-ch = "B" 
   then openWindowForAgent(pAgentID, "notes", "wagentnotesbrowse.w").
   else openWindowForAgent(pAgentID, "notes", "wagentnotes.w").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRefresh C-Win 
PROCEDURE ActionRefresh :
/*------------------------------------------------------------------------------
@description Refreshing the agent data
------------------------------------------------------------------------------*/
  

  empty temp-table data.
  /* change the column labels */
  do with frame {&frame-name}:
  /*  publish "GetAgentSummaries" (pDate, output table data). */
    
    publish "GetAgentnpr" (output table data,output piMonth, output piYear).
    
    currDate = date(string(piMonth) + "/" + "01" + "/" + string(piYear)).
    
    for each data where logical(data.isFavorite) = true no-lock:
     assign
     data.Favorite = "  * " .
     end.  

    
    for first data no-lock:
    end.
    
    OPEN QUERY brwData FOR EACH data.
    
    /* add the data for the alertShow column */
  /*  for each data exclusive-lock:
      data.alertWarnCnt = NUM-ENTRIES(data.alertWarn).
      data.alertCritCnt = NUM-ENTRIES(data.alertCrit).
      data.alertScore = data.alertWarnCnt + (IF data.alertCritCnt > 0 THEN 100 ELSE 0) + (data.alertCritCnt * 2).
    end.  */
    
    setFilterCombos("ALL").
    setManagerCombo().
    dataSortDesc = not dataSortDesc.
    run sortData in this-procedure (dataSortBy).
    
   /* assign
      std-da = today
      menu-item m_View_Next_Three_Months:sensitive in menu m_View = (interval(currDate,std-da,"days") < 0)
      menu-item m_View_Previous_Three_Months:sensitive in menu m_View = (interval(currDate,add-interval(std-da,-3,"years"),"days") > 0)
      bNextMonth:sensitive = (interval(currDate,std-da,"days") < 0)
      bPrevMonth:sensitive = (interval(currDate,add-interval(std-da,-3,"years"),"days") > 0)        */
     .
     
  end.      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionReview C-Win 
PROCEDURE ActionReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
   
  openWindowForAgent(pAgentID, "review", "dialogagentsummary.w").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindow C-Win 
PROCEDURE ActionWindow :
/*------------------------------------------------------------------------------
@description Opens a window
------------------------------------------------------------------------------*/
  define input parameter pWindow as character no-undo.
  openWindow(pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowClose C-Win 
PROCEDURE ActionWindowClose :
/*------------------------------------------------------------------------------
@description Closes the window of a certain type
------------------------------------------------------------------------------*/
  define input parameter pType as character no-undo.
  closeWindow(pType).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent C-Win 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgentAction C-Win 
PROCEDURE ActionWindowForAgentAction :
/*------------------------------------------------------------------------------
@description Opens a dialog window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType    as character no-undo.
  define input parameter pWindow  as character no-undo.
  define input parameter pAction  as character no-undo.
  
  openWindowForAgentAction(pAgentID, pType, pWindow, pAction).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgentClose C-Win 
PROCEDURE ActionWindowForAgentClose :
/*------------------------------------------------------------------------------
@description Closes a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  closeWindowForAgent(pAgentID, pType).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgentCloseAll C-Win 
PROCEDURE ActionWindowForAgentCloseAll :
/*------------------------------------------------------------------------------
@description Closes a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  closeWindowForAgentAll(pAgentID).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentDataChanged C-Win 
PROCEDURE AgentDataChanged :
/*------------------------------------------------------------------------------
@description A change was made in the application
------------------------------------------------------------------------------*/
  run ActionRefresh in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentSelected C-Win 
PROCEDURE AgentSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  openWindowForAgentAction(pAgentID, "agentDetails", "wagent.w", "E").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertDataChanged C-Win 
PROCEDURE AlertDataChanged :
/*------------------------------------------------------------------------------
@description A change was made in the application
------------------------------------------------------------------------------*/
 /* apply "CHOOSE" to bRefresh in frame {&frame-name} */.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fState fStatus fManager YTDvariance fAgent tView monthvariance 
          tPlanActualMonth tActualMonth tPlanMonth tTotalLabel Planned tYTDMonth 
          tActualYTD tPlanActualYTD Actual ActualToPlan 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData bFilterClear fState fStatus fManager YTDvariance fAgent tView 
         bNextMonth monthvariance bPrevMonth tPlanActualMonth tActualMonth 
         tPlanMonth tTotalLabel Planned tYTDMonth tActualYTD tPlanActualYTD 
         Actual ActualToPlan rTotal 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "AgentResults"

  run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSearchedAgents C-Win 
PROCEDURE getSearchedAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cMsg            as character no-undo.
  define variable lSuccess        as logical no-undo.

  do with frame {&frame-name}:
  end.

  if fAgent:input-value ne "" 
   then
    do:  
      run server/searchentityprofiles.p(input "",   /* for ALL states */
                                        input 'A',  /* (A)gent */
                                        input 'AMD',
                                        input fAgent:input-value,
                                        output cAgentIDsList,
                                        output lSuccess,
                                        output cMsg
                                       ).
      if not lSuccess
       then
        message cMsg 
          view-as alert-box.
    end. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setResetFilterBtn C-Win 
PROCEDURE setResetFilterBtn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if fStatus:screen-value in frame {&frame-name}  ne "ALL" or
     fState:screen-value                          ne "ALL" or
     /*fAlert:screen-value                          ne "ALL" or  */
     fManager:screen-value                        ne "ALL" or
     (fAgent:screen-value                         ne "" and lSearchApplied)
   then
    bFilterClear:sensitive = true.
   else
    bFilterClear:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cMonth       as character no-undo.
  define variable cLabel       as character no-undo.
  
  define variable itmonthplanned as integer no-undo initial 0.
  define variable itmonthactual  as integer no-undo initial 0.
  define variable itytdplan      as integer no-undo initial 0.
  define variable itytdactual    as integer no-undo initial 0.
  
  define variable tAlertSort   as character no-undo.
  define variable tWhereClause as character no-undo.
  
  enableButtons(can-find(first data)).
  enableFilters(can-find(first data)).

  do with frame {&frame-name}:
  end.

  tWhereClause = getWhereClause().
      
  if tWhereClause ne "" 
   then
    tWhereClause = tWhereClause + if (fAgent:screen-value ne "") then " and lookup(agentID,'" + cAgentIDsList + "')>0 " else "".
  else
    tWhereClause = if (fAgent:screen-value ne "") then " where lookup(agentID,'" + cAgentIDsList + "')>0 " else "". 
     
  {lib/brw-sortData.i 
      &pre-by-clause="tWhereClause +" 
      &post-by-clause="+ tAlertSort + (if pName <> 'NetpremiumDiffplanYTD' then ' by NetpremiumDiffplanYTD' else '') + (if pName <> 'agentID' then ' by agentID' else '')"
      &pre-querystring="IF lookup(pName, 'alertCritCnt,alertWarnCnt,alertNoneCnt') > 0 
                  THEN tAlertSort = ' by alertScore ' + (IF dataSortDesc THEN 'descending ' ELSE '').
                  ELSE tAlertSort = ''."
      &post-querystring="IF lookup(pName, 'alertCritCnt,alertWarnCnt,alertNoneCnt,hasDocument') = 0 then "
      &ExcludeOpenQuery=true}

  /* cLabel = getViewColumn(currFilter, tQueryString).   */
  hQueryHandle:query-prepare(tQueryString).
  hQueryHandle:query-open().
  cLabel = "Net Premium".
  
  do with frame {&frame-name}:

    std-da = currDate.
    assign
      tTotalLabel:screen-value = cLabel
      tTotalLabel:width        = FONT-TABLE:get-text-width-chars(cLabel, tTotalLabel:FONT) + 0.25
      .
   /* tPlanMonth fillin label */
    assign
      std-da = add-interval(std-da,0,"months")
      cMonth = string(year(std-da))
      cMonth = substring(cMonth, LENGTH(cMonth) - 1).
      cMonth = entry(month(std-da),{&monthAbbr}) .
                                                                
      tPlanMonth:label = cMonth.
      
     /* Net Plan */
    assign
      std-da = add-interval(std-da,0,"months").
      std-ha = getColumn({&browse-name}:handle, "NetPlan")   .
      
    IF VALID-HANDLE(std-ha)
     then
      assign
        cMonth = entry(month(std-da),{&monthAbbr}) 
        std-ha:label = cMonth + "!"  + "Plan". 
        
     /* NetpremiumYTD */
    assign
      std-ha = getColumn({&browse-name}:handle, "NetpremiumYTD")   .
      
    IF VALID-HANDLE(std-ha)
     then
      assign
        std-ha:label = "YTD" + "!"  + "Actual". 
        
     /* Net Plan diff */
    assign
      std-ha = getColumn({&browse-name}:handle, "NetPlanYTD")   .
      
    IF VALID-HANDLE(std-ha)
     then
      assign
        std-ha:label = "YTD" + "!"  + "Plan".
        
     /* Net Plan diff */
    assign
      std-da = add-interval(std-da,0,"months").
      std-ha = getColumn({&browse-name}:handle, "NetpremiumdiffNetPlan")   .
      
    IF VALID-HANDLE(std-ha)
     then
      assign
        cMonth = entry(month(std-da),{&monthAbbr}) 
        std-ha:label = cMonth + "!"  + "Actual to Plan".  
     /* Net Plan YTD diff */
    assign
      std-ha = getColumn({&browse-name}:handle, "NetpremiumDiffplanYTD")   .
      
    IF VALID-HANDLE(std-ha)
     then
      assign
        std-ha:label = "YTD" + "!"  + "Actual to Plan".  
        
          /* month 3 */
    assign
      std-da = add-interval(std-da,-2,"months")
      std-ha = getColumn({&browse-name}:handle, "Netpremium2monthprev") .
      .
    IF VALID-HANDLE(std-ha)
     THEN
      assign
        cMonth = entry(month(std-da),{&monthAbbr})
        std-ha:label = cMonth + "!" + "Actual". 
        .
 
     /* month 2 */
    assign
      std-da = add-interval(std-da,1,"months")
      std-ha = getColumn({&browse-name}:handle, "Netpremium1monthprev")   .

      
    IF VALID-HANDLE(std-ha)
     then
      assign
        cMonth = entry(month(std-da),{&monthAbbr}) 
        std-ha:label = cMonth + "!" + "Actual". 
        
               /* Month 1 */
    assign
      std-da = add-interval(std-da,1,"months").
      std-ha = getColumn({&browse-name}:handle, "Netpremium0monthprev")   .
      

      
    IF VALID-HANDLE(std-ha)
     then
      assign
        cMonth = entry(month(std-da),{&monthAbbr})
        std-ha:label = cMonth + "!" + "Actual". 
      
      
    std-ch = {&intFormat}.
  /*  if cLabel begins "Report" or cLabel begins "No."
     then std-ch = {&intFormat}.
    if cLabel matches "*%*"
     then std-ch = {&pctFormat}.      */
    monthvariance = 0.
    YTDvariance = 0.
    assign
      tYTDMonth                = getTotal("NetPlanYTD")     
      tActualYTD               = getTotal("NetpremiumYTD")
      tPlanActualYTD           = getTotal("NetpremiumDiffplanYTD")
      tPlanMonth               = getTotal("NetPlan") 
      tActualMonth             = getTotal("Netpremium0monthprev")
      tPlanActualMonth         = getTotal("NetpremiumdiffNetPlan")   .
      if (tActualMonth <> ? and tActualMonth <> 0) or (tPlanMonth <> ? and tPlanMonth <> 0)
       then
        do:
           monthvariance            = (tActualMonth * 100)/ tPlanMonth .   
           
        end.
      if (tActualYTD <> ? and tActualYTD <> 0) or (tYTDMonth <> ? and tYTDMonth <> 0)
       then
        do:
           YTDvariance              = (tActualYTD * 100)/ tYTDMonth . 
        end.
      assign
        tPlanActualYTD:format    = std-ch
        tYTDMonth:format         = std-ch
        tActualYTD:format        = std-ch
        tPlanActualMonth:format  = std-ch
        tActualMonth:format      = std-ch
        tPlanMonth:format        = std-ch.
     
      
      
  /*  if cLabel = "Report Gap"
     then
      do:
        for each data no-lock:
          if data.month1Data > 0
           then iMonth1Count = iMonth1Count + 1.
          if data.month2Data > 0
           then iMonth2Count = iMonth2Count + 1.
          if data.month3Data > 0
           then iMonth3Count = iMonth3Count + 1.
        end.
        assign
          tTotalMonth3 = tTotalMonth3 / iMonth3Count
          tTotalMonth2 = tTotalMonth2 / iMonth2Count
          tTotalMonth1 = tTotalMonth1 / iMonth1Count
          .                
      end.      */
    display
      tYTDMonth
      tActualYTD
      tPlanActualYTD
      tPlanMonth
      tActualMonth
      tPlanActualMonth
      YTDvariance
      monthvariance
      .

    tPlanActualMonth:fgcolor = if tPlanActualMonth < 0 then 12 else 0  .
    
    tPlanActualYTD:fgcolor   = if tPlanActualYTD   < 0 then 12 else 0  .
    
    monthvariance:fgcolor    = if monthvariance    < 0 then 12 else 0  .
    
    YTDvariance:fgcolor      = if YTDvariance      < 0 then 12 else 0  .
 /*  end.  */
   
    setStatusCount(num-results("{&browse-name}")).
    apply "VALUE-CHANGED" to brwData.         
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UserDataChanged C-Win 
PROCEDURE UserDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* apply "CHOOSE" to bRefresh in frame {&frame-name}.   */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define  variable  dDiffWidth as decimal no-undo.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* get the difference between the old and new width\height */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
    
  {lib/resize-column.i &col="'name,manager'" &var=dColumnWidth}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION closeWindow C-Win 
FUNCTION closeWindow RETURNS HANDLE
  ( input pType as character ) :
/*------------------------------------------------------------------------------
@description Closes the window if open
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo. 
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  hWindow = ?.
  for each openwindow no-lock
     where openwindow.procFile matches "*" + pType + "*":
      
    hWindow = openwindow.procHandle.
    run CloseWindow in hWindow no-error.
  end.
   
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION closeWindowForAgent C-Win 
FUNCTION closeWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character ) :
/*------------------------------------------------------------------------------
@description Closes the window if open
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable cAgentWindow as character no-undo.
  
  define buffer openwindow for openwindow.

  for each openwindow exclusive-lock:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  hWindow = ?.
  cAgentWindow = pAgentID + " " + pType.
  for each openwindow no-lock
     where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
    run CloseWindow in hWindow no-error.
  end.
   
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION closeWindowForAgentAll C-Win 
FUNCTION closeWindowForAgentAll RETURNS HANDLE
  ( input pAgentID as character ) :
/*------------------------------------------------------------------------------
@description Closes the window if open
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  hWindow = ?.
  for each openwindow no-lock
     where openwindow.procFile begins pAgentID:
      
    hWindow = openwindow.procHandle.
    run CloseWindow in hWindow no-error.
  end.
   
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getTotal C-Win 
FUNCTION getTotal RETURNS decimal
  ( input pBufferField as character ) :
/*------------------------------------------------------------------------------
@description Display the totals of the 3 custom columns
------------------------------------------------------------------------------*/
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable hField as handle no-undo.
  define variable dSum as decimal no-undo.
  define variable iCounter as integer no-undo.

  /* query the table */
  if query {&browse-name}:handle:prepare-string <> ?
   then
    do:
      create buffer hBuffer for table "data".
      create query hQuery.
      hQuery:set-buffers(hBuffer).
      hQuery:query-prepare(query {&browse-name}:handle:prepare-string).
      hQuery:query-open().
      hQuery:get-first().
      repeat while not hQuery:query-off-end:
        hField = hBuffer:buffer-field(pBufferField) no-error.
        if valid-handle(hField)
         then dSum = dSum + hField:buffer-value().
         else dSum = dSum + 0.
        hQuery:get-next().
      end.
      hQuery:query-close().
    end.

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.
  
  RETURN dSum.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getViewColumn C-Win 
FUNCTION getViewColumn RETURNS CHARACTER
  ( INPUT pID AS CHARACTER,
    input pQuery as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo initial "".
  define variable hBuffer as handle no-undo.
  define variable hQuery  as handle no-undo.
  define variable hField  as handle no-undo.
  define variable cType   as character no-undo initial "".
  define variable i       as integer no-undo.
  
  /* decide on the type */
  do with frame {&frame-name}:
    assign
      cType = tView:screen-value
      cType = if cType = "A" then "" else cType
      .    
  end.
  
  /* query the table */
/*  if pQuery <> ?
   then
    do:
      create buffer hBuffer for table "data".
      create query hQuery.
      hQuery:set-buffers(hBuffer).
      hQuery:query-prepare(pQuery).
      hQuery:query-open().
      hQuery:get-first().
      repeat while not hQuery:query-off-end:
        
           hField = hBuffer:buffer-field("month" + pID + cType + "_" + string(i)) no-error.
          if valid-handle(hField)
           then hBuffer:buffer-field("NetpremiumYTD"):buffer-value() = hField:buffer-value().
           else hBuffer:buffer-field("NetpremiumYTD"):buffer-value() = 0.
          hField = hBuffer:buffer-field("month" + pID + "P_" + string(i)) no-error.
          if valid-handle(hField)
           then hBuffer:buffer-field("NetPlanYTD"):buffer-value() = hField:buffer-value().
           else hBuffer:buffer-field("NetPlanYTD"):buffer-value() = 0.
          hField = hBuffer:buffer-field("month" + pID + "_" + string(i)) no-error.
          if valid-handle(hField)
           then hBuffer:buffer-field("NetpremiumDiffplanYTD"):buffer-value() = hField:buffer-value().
           else hBuffer:buffer-field("NetpremiumDiffplanYTD"):buffer-value() = 0.

        hQuery:get-next().
      end.
      hQuery:query-close().
    end.

  delete object hQuery no-error.
  delete object hBuffer no-error.
  delete object hField no-error.      */

  /* set the column label */
  publish "GetSysPropDesc" ("AMD", "Main", "View", pID, output pReturn).
  pReturn = replace(pReturn, "Number", "No.").
  pReturn = replace(pReturn, "Retained", "Ret.").

  RETURN pReturn.   /* Function return value. */      

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindow C-Win 
FUNCTION openWindow RETURNS handle PRIVATE
  ( input pFile as character ) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo. 
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  hWindow = ?.
  for first openwindow no-lock
      where openwindow.procFile = pFile:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      run value(pFile) persistent set hWindow.
      create openwindow.
      assign
        openwindow.procFile = pFile
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.
  return hWindow.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgentAction C-Win 
FUNCTION openWindowForAgentAction returns handle
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow      as handle    no-undo.
  define variable hFileDataSrv as handle    no-undo.
  define variable cAgentWindow as character no-undo.

  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAction <> "N" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).
  
  if pAction <> "C" then
  do:
    for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
      hWindow = openwindow.procHandle.
    end.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv,pAction).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForCodes C-Win 
FUNCTION openWindowForCodes RETURNS handle
  ( input pCodeType as character,
    input pTitle    as character ) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo. 
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  hWindow = ?.
  for first openwindow no-lock
      where openwindow.procFile = pCodeType:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      run referencecodes.w persistent set hWindow (pCodeType, pTitle).
      create openwindow.
      assign
        openwindow.procFile = pCodeType
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setManagerCombo C-Win 
FUNCTION setManagerCombo RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* handles */
  define variable hBuffer as handle    no-undo.
  define variable hQuery  as handle    no-undo.
  define variable hFilter as handle    no-undo.
  hFilter = fManager:handle in frame {&frame-name}.
  
  /* used to hold the values from the data */
  define variable cID     as character no-undo.
  define variable cUser   as character no-undo.
  define variable cValue  as character no-undo.
  
  define variable i       as integer   no-undo.

  /* query the data */
  create buffer hBuffer for table "data".
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("for each data no-lock " + getWhereClause() + " by managerdesc").
  hQuery:query-open().
  hQuery:get-first().
  repeat while not hQuery:query-off-end:
    /* primary manager */
    assign
      cID    = hBuffer:buffer-field("manager"):buffer-value()
      cValue = hBuffer:buffer-field("managerDesc"):buffer-value()
      .
    if cID <> "" and lookup(cID, hFilter:list-item-pairs, hFilter:delimiter) = 0
     then hFilter:add-last(cValue,cID).
    /* secondary manager */
    cID = hBuffer:buffer-field("secondManager"):buffer-value().
    if cID <> ""
     then 
      do i = 1 to num-entries(cID):
        cUser = entry(i, cID).
        if lookup(cUser, hFilter:list-item-pairs, hFilter:delimiter) = 0
         then
          do:
          /*  publish "GetSysUserName" (cUser, output cValue).   */
            hFilter:add-last(cValue,cUser).
          end.
      end. 
    hQuery:get-next().
  end.
  hQuery:query-close().
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

