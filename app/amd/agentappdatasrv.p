&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* 
@name agentappdatasrv.p
@description Data model for opening a agent application
@created 07/13/2017 
@author Yoke Sam Chin (modified by John Oliver)
@Modified

11/20/24  SC    Task #116844    Add last completed audit note while creating/scheduling an audit
 */
define input parameter pAgentID as character no-undo.
define input parameter pApplicationID as integer no-undo.

/* variables */
{lib/std-def.i}
define variable tLoadedAgentApp as logical no-undo.
define variable tLoadedERR as logical no-undo.

/* functions */
{lib/encrypt.i}

/* temp tables */
{tt/agent.i}
{tt/agentapp.i}
{tt/agentapp.i &tableAlias="tempagentapp"}
{tt/qaraudit.i &tableAlias="qar"}
{tt/qaraudit.i &tableAlias="tempqar"}
{tt/qarbkgquestion.i &tableAlias="qarbkg"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-createBackground) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createBackground Procedure 
FUNCTION createBackground RETURNS LOGICAL
  ( input pQarID as integer,
    input pSeq as integer,
    input pAnswer as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.57
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run LoadAgentApp in this-procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-ERRCreate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ERRCreate Procedure 
PROCEDURE ERRCreate :
/*------------------------------------------------------------------------------
@description Create an ERR for the agent application
------------------------------------------------------------------------------*/
  define input parameter pAuditor as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  define variable cLastAuditNote as character no-undo.
  
  for first agentapp exclusive-lock:
    run server/newaudit.p(input year(now),
                          input agentapp.agentID,
                          input now,
                          input pAuditor,
                          input "E",
                          input "P",
                          input "",
                          output std-in,
                          output cLastAuditNote,
                          output pSuccess,
                          output std-ch).
    
    if not pSuccess
     then message std-ch view-as alert-box error buttons ok.
     else
      do:
        /* save the data */
        empty temp-table tempagentapp.
        create tempagentapp.
        buffer-copy agentapp to tempagentapp.
        tempagentapp.qarID = std-in.
        run ModifyAgentApp in this-procedure (table tempagentapp, false, output pSuccess).
        if pSuccess
         then 
          do:
            buffer-copy tempagentapp to agentapp.
            /* queue the audit */
            std-ha = ?.
            publish "OpenAgent" (agentapp.agentID, output std-ha).
            if valid-handle(std-ha)
             then
              do:
                empty temp-table tempqar.
                run GetERR in this-procedure (output table tempqar).
                for each tempqar exclusive-lock
                   where tempqar.qarID <> std-in:
                  
                  delete tempqar.
                end.
                run GetAgent in std-ha (output table agent).
                for first agent no-lock:
                  createBackground(std-in, 20000, "0").
                  createBackground(std-in, 20005, "0").
                  createBackground(std-in, 20007, string(year(now))).
                  createBackground(std-in, 20010, "n").
                  createBackground(std-in, 20020, "").
                  createBackground(std-in, 20035, "N/A").
                  createBackground(std-in, 20040, "NA").
                  createBackground(std-in, 20045, "").
                  createBackground(std-in, 20055, "N/A").
                  createBackground(std-in, 20060, "N/A").
                  createBackground(std-in, 20070, string(agent.liabilityLimit)).
                  createBackground(std-in, 20090, "0").
                  createBackground(std-in, 20105, string(agent.eoRequired)).
                end.
                run server/agentappqueueaudit.p (input std-in,
                                                 input table qar,
                                                 input table qarbkg,
                                                 output pSuccess,
                                                 output std-ch).
                
                if not pSuccess
                 then message std-ch view-as alert-box error buttons ok.
                 else run ERRNotify in this-procedure.
              end.
          end.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ERRNotify) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ERRNotify Procedure 
PROCEDURE ERRNotify :
/*------------------------------------------------------------------------------
@description Notify the auditor that an ERR is assigned to them.
------------------------------------------------------------------------------*/
  run server/notifyagentapp.p (input pAgentID,
                               input pApplicationID,
                               output std-lo,
                               output std-ch).
  
  if not std-lo
   then message std-ch view-as alert-box information buttons ok.
   else
    do:
      tLoadedERR = false.
      publish "AgentAppChanged" (pAgentID).
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ERRReassign) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ERRReassign Procedure 
PROCEDURE ERRReassign :
/*------------------------------------------------------------------------------
@description Reassign an err to someone else
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define input parameter pAuditor as character no-undo.
  define input parameter pReason as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  run server/reassignAudit.p (input pQarID,
                              input pAuditor,
                              input pReason,
                              output pSuccess,
                              output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error buttons ok.
   else run ERRNotify in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ERRSave) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ERRSave Procedure 
PROCEDURE ERRSave :
/*------------------------------------------------------------------------------
@description Save an ERR information for the agent application
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define input parameter pYears as integer no-undo.
  define input parameter pAuditor as character no-undo.
  define input parameter pPhone as character no-undo.
  define input parameter pEmail as character no-undo.
  define input parameter pName as character no-undo.
  define input parameter pAddress as character no-undo.
  define input parameter pAddress2 as character no-undo.
  define input parameter pCity as character no-undo.
  define input parameter pState as character no-undo.
  define input parameter pZip as character no-undo.
  define output parameter pSuccess as logical no-undo.
  
  /* save the data */
  empty temp-table tempagentapp.
  for first agentapp no-lock:
    create tempagentapp.
    buffer-copy agentapp to tempagentapp.
    assign
      tempagentapp.qarID = pQarID
      tempagentapp.yearsActive = pYears
      tempagentapp.phone = pPhone
      tempagentapp.email = pEmail
      tempagentapp.name = pName
      tempagentapp.addr1 = pAddress
      tempagentapp.addr2 = pAddress2
      tempagentapp.city = pCity
      tempagentapp.state = pState
      tempagentapp.zip = pZip
      .
    temp-table tempagentapp:write-xml("file","tempagentapp.xml").
    run ModifyAgentApp in this-procedure (table tempagentapp, false, output pSuccess).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetAgentApp) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentApp Procedure 
PROCEDURE GetAgentApp :
/*------------------------------------------------------------------------------
@description Get the agent application
------------------------------------------------------------------------------*/
  define output parameter table for agentapp.
  
  run LoadAgentApp in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetERR) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetERR Procedure 
PROCEDURE GetERR :
/*------------------------------------------------------------------------------
@description Get the agent application
------------------------------------------------------------------------------*/
  define output parameter table for qar.
  
  run LoadERR in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadAgentApp) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAgentApp Procedure 
PROCEDURE LoadAgentApp :
/*------------------------------------------------------------------------------
@description Load the agent application
------------------------------------------------------------------------------*/
  define buffer agentapp for agentapp.
  
  empty temp-table agentapp.
  publish "GetAgentApp" (pAgentID, pApplicationID, output table agentapp).
  
  for each agentapp exclusive-lock
     where agentapp.applicationID <> pApplicationID:
    
    delete agentapp.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadERR) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadERR Procedure 
PROCEDURE LoadERR :
/*------------------------------------------------------------------------------
@description Load the agent application
------------------------------------------------------------------------------*/
  empty temp-table qar.
  run server/getaudits.p (input 0,
                          input 0,
                          input "",
                          input pAgentID,
                          input "",
                          input "",
                          input "E",
                          output table qar,
                          output tLoadedERR,
                          output std-ch).
                         
  if not tLoadedERR
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "LoadERR failed: " + std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyAgentApp) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyAgentApp Procedure 
PROCEDURE ModifyAgentApp :
/*------------------------------------------------------------------------------
@description Modify the agent application
------------------------------------------------------------------------------*/
  define input parameter table for tempagentapp.
  define input parameter pPublish as logical no-undo.
  define output parameter pSuccess as logical no-undo.

  run server/modifyagentapp.p(input table tempagentapp,
                              output pSuccess,
                              output std-ch).

  if not pSuccess
   then message std-ch view-as alert-box error.
   else
    for first tempagentapp no-lock:
      for first agentapp exclusive-lock:
        buffer-copy tempagentapp to agentapp.
      end.
      if pPublish
       then publish "AgentAppChanged" (tempagentapp.agentID).
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetStatusApprove) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetStatusApprove Procedure 
PROCEDURE SetStatusApprove :
/*------------------------------------------------------------------------------
@description Set the status of the agent application to Approved
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical no-undo.

  run server/approveagentapp.p (input pAgentID,
                                input pApplicationID,
                                output pSuccess,
                                output std-ch).
  
  if not pSuccess
   then message std-ch view-as alert-box error.
   else publish "AgentAppChanged" (pAgentID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetStatusCancel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetStatusCancel Procedure 
PROCEDURE SetStatusCancel :
/*------------------------------------------------------------------------------
@description Set the status of the agent application to Cancelled
------------------------------------------------------------------------------*/
  define input parameter pReasonCode as character no-undo.
  define output parameter pSuccess as logical no-undo.

  run server/cancelagentapp.p (input pAgentID,
                               input pApplicationID,
                               input pReasonCode,
                               output pSuccess,
                               output std-ch).
  
  if not pSuccess
   then message std-ch view-as alert-box error.
   else publish "AgentAppChanged" (pAgentID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetStatusComplete) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetStatusComplete Procedure 
PROCEDURE SetStatusComplete :
/*------------------------------------------------------------------------------
@description Set the status of the agent application to Completed
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical no-undo.

  run server/completeagentapp.p (input pAgentID,
                                 input pApplicationID,
                                 output pSuccess,
                                 output std-ch).
  
  if not pSuccess
   then message std-ch view-as alert-box error.
   else publish "AgentAppChanged" (pAgentID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetStatusDeny) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetStatusDeny Procedure 
PROCEDURE SetStatusDeny :
/*------------------------------------------------------------------------------
@description Set the status of the agent application to Denied
------------------------------------------------------------------------------*/
  define input parameter pReasonCode as character no-undo.
  define output parameter pSuccess as logical no-undo.

  run server/denyagentapp.p (input pAgentID,
                             input pApplicationID,
                             input pReasonCode,
                             output pSuccess,
                             output std-ch).
      
  if not pSuccess
   then message std-ch view-as alert-box error.
   else publish "AgentAppChanged" (pAgentID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetStatusStart) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetStatusStart Procedure 
PROCEDURE SetStatusStart :
/*------------------------------------------------------------------------------
@description Set the status of the agent application to Completed
------------------------------------------------------------------------------*/
  define output parameter pSuccess as logical no-undo.

  run server/startagentapp.p (input pAgentID,
                              input pApplicationID,
                              output pSuccess,
                              output std-ch).
  
  if not pSuccess
   then message std-ch view-as alert-box error.
   else publish "AgentAppChanged" (pAgentID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-createBackground) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createBackground Procedure 
FUNCTION createBackground RETURNS LOGICAL
  ( input pQarID as integer,
    input pSeq as integer,
    input pAnswer as character ) :
/*------------------------------------------------------------------------------
@description Create a background question and answer
------------------------------------------------------------------------------*/
  create qarbkg.
  assign
    qarbkg.qarID = pQarID
    qarbkg.seq = pSeq
    qarbkg.answer = pAnswer
    .
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

