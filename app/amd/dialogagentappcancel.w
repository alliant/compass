&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* dialogagentappcancel.w
   Window for adding the new agent application
   Yoke Sam 08.09.2017

 */
 
 

def input parameter pAgentID as char no-undo.
def output parameter pCancel as logical init true.

{lib/std-def.i}

def var reasoncodelist as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tReasonCode bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tReasonCode 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidateAgentApp fMain 
FUNCTION ValidateAgentApp RETURNS logical
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tReasonCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Select Reason Code" 
     VIEW-AS COMBO-BOX INNER-LINES 17
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 95 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tReasonCode AT ROW 1.95 COL 23 COLON-ALIGNED WIDGET-ID 158
     bSave AT ROW 3.86 COL 47
     bCancel AT ROW 3.86 COL 65
     SPACE(45.99) SKIP(0.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Cancel Agent Application"
         DEFAULT-BUTTON bCancel CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Cancel Agent Application */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fMain
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  if  validateAgentApp() then 
  do:
      run CancelAgentApp in this-procedure.
      apply "window-close" to frame {&frame-name}
      .
  end.
  

  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

ON 'VALUE-CHANGED':U anywhere 
DO:
    bSave:sensitive in frame {&frame-name} = true.
END.

frame {&frame-name}:title = "Cancel Agent Application (" + pAgentID + ")".

std-ch = "".
  publish "GetReasonCodeList" (output std-ch).
  if std-ch > "" 
      then
      do:
        tReasonCode:list-item-pairs = std-ch.
        reasoncodelist = std-ch.
      end.

RUN enable_UI.

MAIN-BLOCK:

repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   WAIT-FOR GO OF FRAME {&FRAME-NAME}.

END.



/* repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK */
/*    ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:  */
/*   WAIT-FOR GO OF FRAME {&FRAME-NAME}.             */
/*                                                   */
/*   if tCode:input-value in frame fMain = ""        */
/*   or tCode:input-value in frame fMain = ? then    */
/*   do:                                             */
/*     message                                       */
/*       "Code cannot be blank"                      */
/*       view-as alert-box error.                    */
/*     next.                                         */
/*   end.                                            */
/*                                                   */
/*   assign                                          */
/*     pCode = tCode:input-value in frame fMain      */
/*     pCancel = false                               */
/*     .                                             */
/*   leave MAIN-BLOCK.                               */
/* END.                                              */
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CancelAgentApp fMain 
PROCEDURE CancelAgentApp PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:

/*       message "reason code:" tReasonCode:input-value view-as alert-box. */
/*       message "AGENTID: " pAgentID view-as alert-box.                   */
      bSave:sensitive = false.
      publish "CancelAgentApp" (pAgentID, 
                              tReasonCode:input-value,
                              output std-lo).


      if std-lo 
          then
           assign
            tReasonCode:screen-value = ?
            .
      

 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tReasonCode 
      WITH FRAME fMain.
  ENABLE tReasonCode bSave bCancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidateAgentApp fMain 
FUNCTION ValidateAgentApp RETURNS logical
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var errMsg as char no-undo.

errMsg = "".
std-lo = true.

 do with frame {&frame-name}:

   if tReasonCode:screen-value = ? 
      then
        do:
             if errMsg > "" then 
                 errMsg = errMsg + chr(10) + chr(13).
             else errMsg = "".

              errMsg = errMsg + "Please select a reason code.".
             std-lo = false.
             
        end.
  
  if errMsg > "" 
      then  message errMsg view-as alert-box warning.

  end.
            
  RETURN std-lo.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

