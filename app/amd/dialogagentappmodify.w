&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* Functions ---                                                        */

/* Temp Tables ---                                                      */
{tt/agentapp.i}
{tt/qaraudit.i &tableAlias="qar"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-8 RECT-7 tStatus tType tSubtype ~
tAgentID tNotes tUsername tPassword bERR tDateCreated tDateStarted ~
tDateSubmitted tDateERRStarted tDateERRFinished tDateReview tDateReviewSent ~
tDateApproved tDateDenied tDateAgreeSent tDateCompleted tReason bSave 
&Scoped-Define DISPLAYED-OBJECTS tStatus tType tSubtype tAgentID tNotes ~
tUsername tPassword tDateCreated tDateStarted tDateSubmitted ~
tDateERRStarted tDateERRFinished tDateReview tDateReviewSent tDateApproved ~
tDateDenied tDateAgreeSent tDateCompleted tReason 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bERR 
     LABEL "Link ERR" 
     SIZE 17.4 BY 1.14.

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tSubtype AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subtype" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 52.2 BY 6.43 NO-UNDO.

DEFINE VARIABLE tReason AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 52 BY 1.67 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 NO-UNDO.

DEFINE VARIABLE tDateAgreeSent AS DATETIME FORMAT "99/99/99":U 
     LABEL "Agree. Sent" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateApproved AS DATETIME FORMAT "99/99/99":U 
     LABEL "Approved" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateCompleted AS DATETIME FORMAT "99/99/99":U 
     LABEL "Completed" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateCreated AS DATETIME FORMAT "99/99/99":U 
     LABEL "Created" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateDenied AS DATETIME FORMAT "99/99/99":U 
     LABEL "Denied" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateERRFinished AS DATETIME FORMAT "99/99/99":U 
     LABEL "ERR Finished" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateERRStarted AS DATETIME FORMAT "99/99/99":U 
     LABEL "ERR Ordered" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateReview AS DATETIME FORMAT "99/99/99":U 
     LABEL "Processed" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateReviewSent AS DATETIME FORMAT "99/99/99":U 
     LABEL "Review Sent" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateStarted AS DATETIME FORMAT "99/99/99":U 
     LABEL "Started" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tDateSubmitted AS DATETIME FORMAT "99/99/99":U 
     LABEL "Submitted" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tPassword AS CHARACTER FORMAT "X(256)":U 
     LABEL "Password" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tUsername AS CHARACTER FORMAT "X(256)":U 
     LABEL "Username" 
     VIEW-AS FILL-IN 
     SIZE 52 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 3.57.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tStatus AT ROW 1.48 COL 17 COLON-ALIGNED WIDGET-ID 226
     tType AT ROW 2.67 COL 17 COLON-ALIGNED WIDGET-ID 230
     tSubtype AT ROW 2.67 COL 51 COLON-ALIGNED WIDGET-ID 220
     tAgentID AT ROW 3.86 COL 17 COLON-ALIGNED WIDGET-ID 228
     tNotes AT ROW 5.05 COL 18.8 NO-LABEL WIDGET-ID 212
     tUsername AT ROW 12.71 COL 17 COLON-ALIGNED WIDGET-ID 188
     tPassword AT ROW 13.91 COL 17 COLON-ALIGNED WIDGET-ID 186
     bERR AT ROW 16.62 COL 53.8 WIDGET-ID 232
     tDateCreated AT ROW 16.71 COL 17 COLON-ALIGNED WIDGET-ID 174 NO-TAB-STOP 
     tDateStarted AT ROW 17.91 COL 17 COLON-ALIGNED WIDGET-ID 136 NO-TAB-STOP 
     tDateSubmitted AT ROW 17.91 COL 52 COLON-ALIGNED WIDGET-ID 172 NO-TAB-STOP 
     tDateERRStarted AT ROW 19.1 COL 17 COLON-ALIGNED WIDGET-ID 198
     tDateERRFinished AT ROW 19.1 COL 52 COLON-ALIGNED WIDGET-ID 200
     tDateReview AT ROW 20.29 COL 17 COLON-ALIGNED WIDGET-ID 166 NO-TAB-STOP 
     tDateReviewSent AT ROW 20.29 COL 52 COLON-ALIGNED WIDGET-ID 202
     tDateApproved AT ROW 21.48 COL 17 COLON-ALIGNED WIDGET-ID 146 NO-TAB-STOP 
     tDateDenied AT ROW 21.48 COL 52 COLON-ALIGNED WIDGET-ID 144 NO-TAB-STOP 
     tDateAgreeSent AT ROW 22.67 COL 17 COLON-ALIGNED WIDGET-ID 204
     tDateCompleted AT ROW 22.67 COL 52 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     tReason AT ROW 23.86 COL 19 NO-LABEL WIDGET-ID 234
     bSave AT ROW 26.24 COL 31 WIDGET-ID 224
     "Reason:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 23.95 COL 10.4 WIDGET-ID 236
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 5.14 COL 12 WIDGET-ID 216
     "Activity" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 15.76 COL 4 WIDGET-ID 152
     "Credentials" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 11.71 COL 4 WIDGET-ID 184
     RECT-8 AT ROW 16 COL 3 WIDGET-ID 150
     RECT-7 AT ROW 11.95 COL 3 WIDGET-ID 182
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 74.8 BY 26.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Modify Agent Application"
         HEIGHT             = 26.76
         WIDTH              = 74.8
         MAX-HEIGHT         = 28.52
         MAX-WIDTH          = 116.8
         VIRTUAL-HEIGHT     = 28.52
         VIRTUAL-WIDTH      = 116.8
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateApproved:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateCompleted:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateCreated:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateDenied:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateReview:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateStarted:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDateSubmitted:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tReason:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tStatus:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tType:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Modify Agent Application */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Modify Agent Application */
DO:
  {lib/confirm-close.i "Agent Application"}
  
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bERR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bERR C-Win
ON CHOOSE OF bERR IN FRAME DEFAULT-FRAME /* Link ERR */
DO:
  run dialogagentapperr.w (hFileDataSrv).

  if valid-handle(hFileDataSrv)
   then
    do:
      run GetAgentApp in hFileDataSrv (output table agentapp).
      run setAgentApp in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME DEFAULT-FRAME /* Save */
DO:
  if self:label = "Save"
   then run saveAgentApp in this-procedure.
   else apply "WINDOW-CLOSE" to {&WINDOW-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

if valid-handle(hFileDataSrv)
 then
  do:
    run GetAgentApp in hFileDataSrv (output table agentapp).
    run GetERR in hFileDataSrv (output table qar).
  end.
 
for first agentapp no-lock:
  {&window-name}:title = agentapp.agentName + "'s Agent Application".
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run setAgentApp in this-procedure.
  temp-table agentapp:write-xml("file","agentapp.xml").
  doModify(not can-find(first agentapp where stat = "D" or stat = "X" or stat = "C")).
  bERR:label = (if can-find(first qar) then "Link ERR" else "Create ERR").

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStatus tType tSubtype tAgentID tNotes tUsername tPassword 
          tDateCreated tDateStarted tDateSubmitted tDateERRStarted 
          tDateERRFinished tDateReview tDateReviewSent tDateApproved tDateDenied 
          tDateAgreeSent tDateCompleted tReason 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-8 RECT-7 tStatus tType tSubtype tAgentID tNotes tUsername 
         tPassword bERR tDateCreated tDateStarted tDateSubmitted 
         tDateERRStarted tDateERRFinished tDateReview tDateReviewSent 
         tDateApproved tDateDenied tDateAgreeSent tDateCompleted tReason bSave 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveAgentApp C-Win 
PROCEDURE saveAgentApp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    for first agentapp exclusive-lock:
      assign
        agentapp.subtype = tSubtype:screen-value
        agentapp.notes = tNotes:screen-value
        /* credentials */
        agentapp.username = tUsername:screen-value
        agentapp.password = tPassword:screen-value
        /* activity */
        /*agentapp.dateUnderReview = tDateReview:input-value*/
        agentapp.dateSentReview = tDateReviewSent:input-value
        /*agentapp.dateApproved = tDateApproved:input-value*/
        /*agentapp.dateStopped = tDateDenied:input-value*/
        agentapp.dateSentAgency = tDateAgreeSent:input-value
        /*agentapp.dateSigned = tDateCompleted:input-value*/
        .
    end.
    run ModifyAgentApp in hFileDataSrv (table agentapp, true, output std-lo).
    if std-lo
     then
      do:
        empty temp-table agentapp.
        run GetAgentApp in hFileDataSrv (output table agentapp).
        run GetERR in hFileDataSrv (output table qar).
        bERR:label = (if can-find(first qar) then "Link ERR" else "Create ERR").
        run setAgentApp in this-procedure.
        run ShowWindow in this-procedure.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAgentApp C-Win 
PROCEDURE setAgentApp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    for first agentapp no-lock:
      std-ch = "".
      publish "GetSysCodeList" (agentapp.typeDesc + "Type", output std-ch).
      if std-ch > ""
       then
        do:
          tSubtype:side-label-handle:screen-value = agentapp.typeDesc + " Type".
          {lib/get-syscode-list.i &combo=tSubtype &codeType="agentapp.typeDesc + 'Type'" &d="'T01'"}
          tSubtype:hidden = false.
        end.
       else tSubtype:hidden = true.

      assign
        tStatus:screen-value = agentapp.statDesc
        tType:screen-value = agentapp.typeDesc
        tSubtype:screen-value = agentapp.subtype
        tAgentID:screen-value = agentapp.agentName + " (" + agentapp.agentID + ")"
        tNotes:screen-value = agentapp.notes
        /* credentials */
        tUsername:screen-value = agentapp.username
        tPassword:screen-value = agentapp.password
        /* activity */
        tDateCreated:screen-value = string(agentapp.dateCreated)
        tDateStarted:screen-value = string(agentapp.dateStarted)
        tDateSubmitted:screen-value = string(agentapp.dateSubmitted)
        tDateERRStarted:screen-value = string(agentapp.dateOrderedERR)
        tDateERRFinished:screen-value = string(agentapp.dateReceviedERR)
        tDateReview:screen-value = string(agentapp.dateUnderReview)
        tDateReviewSent:screen-value = string(agentapp.dateSentReview)
        tDateApproved:screen-value = string(agentapp.dateApproved)
        tDateDenied:screen-value = string(agentapp.dateStopped)
        tDateAgreeSent:screen-value = string(agentapp.dateSentAgency)
        tDateCompleted:screen-value = string(agentapp.dateSigned)
        tReason:screen-value = agentapp.reasonCodeDesc
        .
    end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    if pModify
     then bSave:label = "Save".
     else bSave:label = "Close".

    assign
      tSubtype:sensitive = pModify
      tNotes:read-only = not pModify
      /* credentials */
      tUsername:read-only = not pModify
      tPassword:read-only = not pModify
      /* activity */
      tDateCreated:read-only = not pModify
      bERR:sensitive = pModify
      tDateStarted:read-only = not pModify
      tDateSubmitted:read-only = not pModify
      tDateERRStarted:read-only = not pModify
      tDateERRFinished:read-only = not pModify
      tDateReview:read-only = not pModify
      tDateReviewSent:read-only = not pModify
      tDateApproved:read-only = not pModify
      tDateDenied:read-only = not pModify
      tDateAgreeSent:read-only = not pModify
      tDateCompleted:read-only = not pModify
      /* these dates should not be modified - comment out to allow */
      tDateCreated:read-only = true
      tDateStarted:read-only = true
      tDateSubmitted:read-only = true
      tDateERRStarted:read-only = true
      tDateERRFinished:read-only = true
      tDateReview:read-only = true
      tDateApproved:read-only = true
      tDateDenied:read-only = true
      tDateCompleted:read-only = true
      .
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

