&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wsavedconsolidationparams.w

  Description: Displays list of saved consolidation parameters

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created: 11.04.2020

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

{lib/std-def.i}  
{lib/winlaunch.i} 
{lib/winshowscrollbars.i}
{lib/set-button-def.i }

/* Temp-table Definition */
{tt/sysuserconfig.i &tablealias=ttParamList}
{tt/consolidate.i &tableAlias=tConsolidate}
{tt/agent.i &tablealias = "tAgent"}
{tt/openwindow.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwParamList

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttParamList

/* Definitions for BROWSE brwParamList                                  */
&Scoped-define FIELDS-IN-QUERY-brwParamList ttParamList.configType   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwParamList   
&Scoped-define SELF-NAME brwParamList
&Scoped-define QUERY-STRING-brwParamList for each ttParamList
&Scoped-define OPEN-QUERY-brwParamList open query {&SELF-NAME} for each ttParamList.
&Scoped-define TABLES-IN-QUERY-brwParamList ttParamList
&Scoped-define FIRST-TABLE-IN-QUERY-brwParamList ttParamList


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwParamList}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwParamList bDelete edParam bModify bRun 
&Scoped-Define DISPLAYED-OBJECTS flParameters edParam 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Rename".

DEFINE BUTTON bRun  NO-FOCUS
     LABEL "Run" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run Query".

DEFINE VARIABLE edParam AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 107.8 BY 5.14 NO-UNDO.

DEFINE VARIABLE flParameters AS CHARACTER FORMAT "X(256)":U INITIAL "Parameters:" 
     VIEW-AS FILL-IN 
     SIZE 12.4 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwParamList FOR 
      ttParamList SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwParamList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwParamList C-Win _FREEFORM
  QUERY brwParamList DISPLAY
      ttParamList.configType            format "x(50)"             label ""
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 107.8 BY 13.81
         TITLE "Consolidations" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     brwParamList AT ROW 3.05 COL 2.6 WIDGET-ID 200
     flParameters AT ROW 17.24 COL 2 NO-LABEL WIDGET-ID 426
     bDelete AT ROW 1.19 COL 16.8 WIDGET-ID 46
     edParam AT ROW 18.1 COL 2.6 NO-LABEL WIDGET-ID 54
     bModify AT ROW 1.19 COL 9.6 WIDGET-ID 52
     bRun AT ROW 1.19 COL 2.4 WIDGET-ID 44
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 111.2 BY 22.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Consolidations"
         HEIGHT             = 22.67
         WIDTH              = 111.4
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwParamList 1 DEFAULT-FRAME */
ASSIGN 
       bDelete:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "Delete".

ASSIGN 
       bModify:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "Email".

ASSIGN 
       bRun:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "New".

ASSIGN 
       brwParamList:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwParamList:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       edParam:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flParameters IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       flParameters:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwParamList
/* Query rebuild information for BROWSE brwParamList
     _START_FREEFORM
open query {&SELF-NAME} for each ttParamList.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwParamList */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Consolidations */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Consolidations */
DO:  
  /* This event will close the window and terminate the procedure.  */ 
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Consolidations */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  if available  ttParamList
   then
    run deleteConsolidation in this-procedure (input ttParamList.sysUserConfigID) .

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Modify */
DO:
  if available ttParamList
   then
    run modifyConsolidation in this-procedure (input ttParamList.sysUserConfigID, 
                                               input ttParamList.configType)
                                               .   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRun
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRun C-Win
ON CHOOSE OF bRun IN FRAME DEFAULT-FRAME /* Run */
DO:
  define variable cProcFile as character no-undo.
  define buffer openwindow for openwindow.
  
  if not available ttParamList
   then return.
   
  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.

  std-ha = ?.
  cProcFile = ttParamList.configType.
  for first openwindow no-lock
      where openwindow.procFile = cProcFile:
    std-ha = openwindow.procHandle.
  end.
  
  if not valid-handle(std-ha) 
   then
    do:
      run server/getConsolidatedAgents.p(input table tConsolidate,
                                         output std-in,
                                         output table tagent,
                                         output std-lo,
                                         output std-ch
                                         ).
       
      run wagentsgroup.w persistent set std-ha(input table tConsolidate,
                                               input table tagent). 

      create openwindow.
      assign openwindow.procFile   = cProcFile
             openwindow.procHandle = std-ha.
    end.

  run ShowWindow in std-ha no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwParamList
&Scoped-define SELF-NAME brwParamList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParamList C-Win
ON DEFAULT-ACTION OF brwParamList IN FRAME DEFAULT-FRAME /* Consolidations */
DO:
  apply 'choose' to bRun.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParamList C-Win
ON ROW-DISPLAY OF brwParamList IN FRAME DEFAULT-FRAME /* Consolidations */
do:
  {lib/brw-rowdisplay.i}
  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwParamList C-Win
ON VALUE-CHANGED OF brwParamList IN FRAME DEFAULT-FRAME /* Consolidations */
DO:
  run generateConsolidationTable in this-procedure.
  run displayEditorValue in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.


/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow" anywhere.

setStatusMessage("").
   
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  {lib/set-button.i &frame-name=DEFAULT-FRAME &b=bRun    &image="images/start.bmp"     &inactive="images/start-i.bmp"}
  {lib/set-button.i &frame-name=DEFAULT-FRAME &b=bModify &image="images/update.bmp"    &inactive="images/update-i.bmp"}
  {lib/set-button.i &frame-name=DEFAULT-FRAME &b=bDelete &image="images/delete.bmp"    &inactive="images/delete-i.bmp"}    
  
  setButtons().

  run getParamsList in this-procedure.
  
  run enable_UI. 
  run enableDisableBtn in this-procedure.
  
  apply 'value-changed' to brwParamList in frame {&frame-name}.
  /* Set Status count with date and time from the server */
  setStatusRecords(query brwparamList:num-results).
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteConsolidation C-Win 
PROCEDURE deleteConsolidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipisysUserConfigID as integer no-undo.
  
  define variable lcheck as logical no-undo.
       
  message "Highlighted consolidation will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no title "Delete Consolidation" update lcheck.
  
  if not lCheck 
   then
    return.
    
  run server/deleteconsolidationparams.p(input ipisysUserConfigID,
                                         output std-lo,
                                         output std-ch
                                         ).
                                         
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box.
      return.
    end.      
  else
   do:
      find first ttParamList where ttParamList.sysUserConfigID = ipisysUserConfigID no-error.
      if available ttParamList
       then
        delete ttParamList.
      
      open query brwParamList for each ttParamList.
      
      apply "value-changed" to brwParamList in frame {&frame-name}.
  
   end.
   
  run enableDisableBtn in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayEditorValue C-Win 
PROCEDURE displayEditorValue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cAffiliationList as character no-undo.
  define variable cTagList         as character no-undo.
  define variable cParameterList   as character no-undo.
  define variable cDeleimiter      as character no-undo.

  cDeleimiter = chr(10).

  do with frame {&frame-name}:
  
    for each tConsolidate where tConsolidate.isChecked = true:

      case tConsolidate.fldKey:
        when "State"
         then
          cParameterList = cParameterList + cDeleimiter + "State = " + tConsolidate.fldDesc.
          
        when "Year of Signing"
         then
          cParameterList = cParameterList + cDeleimiter + "Year of Signing = " +  tConsolidate.fldDesc.
          
        when "Software"
         then
          cParameterList = cParameterList + cDeleimiter + "Software = " + tConsolidate.fldDesc.
          
        when "Organization"
         then
          cParameterList = cParameterList + cDeleimiter + "Organization = " + tConsolidate.fldDesc.
          
        when "Company"
         then
          cParameterList = cParameterList + cDeleimiter + "Company = " +  tConsolidate.fldDesc.
          
        when "Manager"
         then
          cParameterList = cParameterList + cDeleimiter + "Manager = " +  tConsolidate.fldDesc.
          
        when "Tag"
         then
          do:
            cTagList = if cTagList = "" 
                        then 
                         tConsolidate.fldValue 
                        else   
                         cTagList + "," + tConsolidate.fldDesc. 
          end.
          
        when "Affiliation"
         then
          do:
            cAffiliationList = if cAffiliationList = "" 
                                then 
                                 tConsolidate.fldDesc 
                                else   
                                 cAffiliationList + "," + tConsolidate.fldDesc. 
          end.      
      end case.  
    end.
  
    assign 
        cParameterList = cParameterList +  (if cTagList ne "" then (cDeleimiter + "Tag = " + cTagList) else "")  
                                        +  (if cAffiliationList ne "" then (cDeleimiter + "Affiliation = " + cAffiliationList) else "")
        cParameterList = trim(cParameterList,cDeleimiter)
        cParameterList = trim(cParameterList)
        .
        
    edParam:screen-value = cParameterList.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableBtn C-Win 
PROCEDURE enableDisableBtn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first ttParamList no-error.
  if not available ttParamList
   then
    enablebuttons(false).
    
  setStatusRecords(query brwparamList:num-results).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flParameters edParam 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE brwParamList bDelete edParam bModify bRun 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generateConsolidationTable C-Win 
PROCEDURE generateConsolidationTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cKeyValPair      as character no-undo.
  define variable cKey             as character no-undo.
/*   define variable cVal             as character no-undo. */
/*   define variable cDesc            as character no-undo. */
  define variable cDescVal         as character no-undo.
  define variable cTagVal          as character no-undo.
  define variable cAffVal          as character no-undo.
  define variable iNumParams       as integer   no-undo.
  define variable iNumVals       as integer     no-undo.
  
  empty temp-table tConsolidate.

  find current ttParamList no-error.
  if available ttParamList
   then
    do iNumParams = 1 to num-entries(ttParamList.configuration,"|"):
    
      assign
          cKeyValPair = entry(iNumParams,ttParamList.configuration,"|")
          cKey     = entry(1,cKeyValPair,"=")
          cDescVal = entry(2,cKeyValPair,"=")
          .
      case cKey:
       when "Tag"  
        then
         do iNumVals = 1 to num-entries(cDescVal,"^"):
           cTagVal = entry(iNumVals,cDescVal,"^").
           create tConsolidate.
           assign 
               tConsolidate.fldKey    = "Tag"
               tConsolidate.fldValue  = cTagVal
               tConsolidate.fldDesc   = cTagVal
               tConsolidate.isChecked = true
               .  
         end.
         
       when "Affiliation"  
        then
         do iNumVals = 1 to num-entries(cDescVal,"^"):
           cAffVal = entry(iNumVals,cDescVal,"^").
           create tConsolidate.
           assign 
               tConsolidate.fldKey    = "Affiliation"
               tConsolidate.fldValue  = entry(2,cAffVal,";")
               tConsolidate.fldDesc   = entry(1,cAffVal,";")
               tConsolidate.isChecked = true
               .  
         end.
         
       otherwise
        do:
          create tConsolidate.
           assign 
               tConsolidate.fldKey    = cKey
               tConsolidate.fldDesc   = entry(1,cDescVal,";")
               tConsolidate.fldValue  = if index(cDescVal,";") = 0 then tConsolidate.fldDesc else entry(2,cDescVal,";")
               tConsolidate.isChecked = true
               .
        end.
      end case. 
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getParamsList C-Win 
PROCEDURE getParamsList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cPrefix as character no-undo initial "cp_".
  run server/getconsolidationparams.p(input cPrefix,
                                      output table ttParamList,
                                      output std-lo,
                                      output std-ch)
                                      .
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box information buttons ok.
      return.    
    end.
    
  for each ttParamList:
    ttParamList.configType = trim(ttParamList.configType,cPrefix).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyConsolidation C-Win 
PROCEDURE modifyConsolidation :
/*------------------------------------------------------------------------------
  Purpose: this ip modifies the consolidation      
  Parameters: ipiConfigID
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiConfigID   as integer no-undo.
  define input parameter ipcconfigType as character no-undo.
  
  define variable opUpdateName as character no-undo.

  run dialogsaveconsolidationparams.w (input "M",
                                       input ipiConfigID ,
                                       input ipcconfigType,
                                       input table tConsolidate,
                                       output opUpdateName
                                       ).
  
  if opUpdateName = ""
   then
    return.
     
  find first ttParamList where ttParamList.sysUserConfigID = ipiConfigID no-error.
  if available ttParamList
   then
    do:
      assign
          ttParamList.configType = opUpdateName
          std-ro = rowid(ttParamList)
          . 
    end.
  
  open query brwParamList for each ttParamList.
  reposition brwParamList to rowid std-ro.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      /* fMain Components */
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 14
      edParam:width-pixels                      = frame {&frame-name}:width-pixels - 14
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 143
      edParam:y =  frame {&frame-name}:height-pixels - edParam:height-pixels - 5
      flParameters:y =  frame {&frame-name}:height-pixels - edParam:height-pixels - flParameters:height-pixels - 7
      .
  run ShowScrollBars(frame {&frame-name}:handle, no, no).  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

