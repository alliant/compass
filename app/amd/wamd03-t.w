&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Agent Activity
@description Report to show all of the agent activities

@author John Oliver
@version 1.0
@created 02/15/2018
@notes 
---------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */
CREATE WIDGET-POOL.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* for the column width on resize */
define variable dColumnWidth as decimal no-undo.

/* for the filters */
define variable currCategory as character no-undo.
define variable currType as character no-undo.

/* new activity handle */
define variable hNewActivity as handle no-undo.

/* for the import screen */
define variable pImportYear as integer no-undo.
define variable pImportType as character no-undo initial "P".
define variable pImportCategory as character no-undo initial "N".

/* the years list */
define variable cYearList as character no-undo initial "".

/* Temp Table Definitions ---                                           */
{tt/agent.i}
{tt/state.i}
{tt/activity.i &tableAlias="data"}
{tt/activity.i &tableAlias="tempdata"}
{tt/openwindow.i}
{tt/importdata.i}
define temp-table openActivity
  field activityID as integer
  field activityHandle as handle
  .

/* Function Definitions ---                                             */
{lib/add-delimiter.i}
{lib/get-column.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.agentID data.name data.stateID data.categoryDesc data.typeDesc data.statDesc data.qtr1 data.qtr2 data.qtr3 data.qtr4 data.yrTotal   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-36 bGo RECT-37 RECT-38 tYear ~
tStateID fCategory tDecide bNew bImport tAgentID tName fType brwData 
&Scoped-Define DISPLAYED-OBJECTS tYear tStateID fCategory tDecide tAgentID ~
tName fType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createCopy C-Win 
FUNCTION createCopy RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showOpenWindowForActivity C-Win 
FUNCTION showOpenWindowForActivity RETURNS HANDLE
  ( input pType as character,
    input table for tempdata,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClose 
     LABEL "Finalize" 
     SIZE 7.2 BY 1.71 TOOLTIP "Close the shown activities".

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the agent activity".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bImport 
     LABEL "Import" 
     SIZE 7.2 BY 1.71 TOOLTIP "Import the agent activity".

DEFINE BUTTON bModify 
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "View the agent activity".

DEFINE BUTTON bNew 
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new agent activity".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh agents for the selected criteria".

DEFINE VARIABLE fCategory AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 73 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 11 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 73 BY 1 NO-UNDO.

DEFINE VARIABLE tDecide AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Agents", 1,
"Targets", 2,
"ALL", 0
     SIZE 30 BY .71 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 102 BY 3.57.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40 BY 3.57.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43 BY 3.57.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.agentID width 10
data.name width 34
data.stateID width 10
data.categoryDesc width 20
data.typeDesc width 15
data.statDesc width 12
data.qtr1 width 15
data.qtr2 width 15
data.qtr3 width 15
data.qtr4 width 15
data.yrTotal width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189 BY 19.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.14 COL 94.4 WIDGET-ID 2 NO-TAB-STOP 
     bGo AT ROW 2.14 COL 87 WIDGET-ID 4 NO-TAB-STOP 
     tYear AT ROW 2.19 COL 10 COLON-ALIGNED WIDGET-ID 168
     tStateID AT ROW 2.19 COL 30 COLON-ALIGNED WIDGET-ID 166
     fCategory AT ROW 2.19 COL 159 COLON-ALIGNED WIDGET-ID 300
     tDecide AT ROW 2.33 COL 55 NO-LABEL WIDGET-ID 318
     bNew AT ROW 2.43 COL 108 WIDGET-ID 306
     bImport AT ROW 2.43 COL 115.4 WIDGET-ID 314
     bModify AT ROW 2.43 COL 122.8 WIDGET-ID 308
     bDelete AT ROW 2.43 COL 130.2 WIDGET-ID 322
     bClose AT ROW 2.43 COL 137.6 WIDGET-ID 324
     tAgentID AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 84
     tName AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 316
     fType AT ROW 3.38 COL 159 COLON-ALIGNED WIDGET-ID 312
     brwData AT ROW 5.29 COL 2 WIDGET-ID 200
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 150 WIDGET-ID 60
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 106 WIDGET-ID 304
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 149 WIDGET-ID 58
     RECT-38 AT ROW 1.48 COL 105 WIDGET-ID 302
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 191 BY 24.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Activity Maintenance"
         HEIGHT             = 24.48
         WIDTH              = 191
         MAX-HEIGHT         = 29.43
         MAX-WIDTH          = 256
         VIRTUAL-HEIGHT     = 29.43
         VIRTUAL-WIDTH      = 256
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fType fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bClose IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       tName:HIDDEN IN FRAME fMain           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.91
       COLUMN          = 87
       HEIGHT          = .48
       WIDTH           = 14.6
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(fType:HANDLE IN FRAME fMain).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Activity Maintenance */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Activity Maintenance */
DO:
  /* close the new activity dialog */
  if valid-handle(hNewActivity)
   then run CloseWindow in hNewActivity.
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Activity Maintenance */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose C-Win
ON CHOOSE OF bClose IN FRAME fMain /* Finalize */
DO:
  define variable cQuery as character no-undo.
  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.
  
  std-ch = dynamic-function("doFilterSort", self:label).
  
  createCopy().
  hBuffer = temp-table tempdata:default-buffer-handle.
  cQuery = "for each tempdata no-lock " + std-ch.
  
  /* build a new query */
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare(cQuery).
  hQuery:query-open().
  hQuery:get-first().

  /* loop through the buffer fields to get the activity ids */
  std-ch = "".
  repeat while not hQuery:query-off-end:
    std-ch = addDelimiter(std-ch, ",") + string(hBuffer:buffer-field("activityID"):buffer-value()).
    hQuery:get-next().
  end.
  hQuery:query-close().
  
  publish "FinalizeActivities" (std-ch, output std-lo).
  if std-lo
   then run getData in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  if not available data
   then return.
  
  {lib/confirm-delete.i "Activity"}
  
  publish "DeleteActivity" (data.activityID, output std-lo).
  
  if std-lo
   then run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bImport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bImport C-Win
ON CHOOSE OF bImport IN FRAME fMain /* Import */
DO:
  std-lo = true.
  pImportYear = year(today).
  run dialogactivityimport.w (input-output pImportYear, input-output pImportType, input-output pImportCategory, output std-lo).
  
  if not std-lo
   then run util/importdialognew.w
            ("Agent ID,Name,State ID,January,February,March,April,May,June,July,August,September,October,November,December",
             "CH,CH,CH,DE,DE,DE,DE,DE,DE,DE,DE,DE,DE,DE,DE",
             "Column 1 is Agent ID: If blank, Name must be present||Column 2 is Name: If blank, Agent ID must be present||Column 3 is State ID: Should be an active state||Column 4 is January's Value: Must be a decimal||Column 5 is February's Value: Must be a decimal||Column 6 is March's Value: Must be a decimal||Column 7 is April's Value: Must be a decimal||Column 8 is May's Value: Must be a decimal||Column 9 is June's Value: Must be a decimal||Column 10 is July's Value: Must be a decimal||Column 11 is August's Value: Must be a decimal||Column 12 is September's Value: Must be a decimal||Column 13 is October's Value: Must be a decimal||Column 14 is November's Value: Must be a decimal||Column 15 is December's Value: Must be a decimal",
             "ImportActivity").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  if not available data
   then return.
   
  empty temp-table tempdata.
  create tempdata.
  buffer-copy data to tempdata.
  showOpenWindowForActivity("modify", table tempdata, "dialogactivitymodify.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  if valid-handle(hNewActivity)
   then run ShowWindow in hNewActivity.
   else run dialogactivitynew.w persistent set hNewActivity.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  apply "CHOOSE" to bModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ENTRY OF brwData IN FRAME fMain
DO:
  if available data
   then
    do:
      assign
        std-ch = (if data.stat = "C" then "View" else "Modify")
        bModify:tooltip = std-ch + " the agent activity"
        bDelete:sensitive = (data.stat = "O")
        .
      std-ch = (if data.stat = "C" then "magnifier" else "update").
      bModify:load-image("images/" + std-ch + ".bmp").
      bModify:load-image-insensitive("images/" + std-ch + "-i.bmp").
    end.
  std-ch = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if available data
   then
    do:
      assign
        std-ch = (if data.stat = "C" then "View" else "Modify")
        bModify:tooltip = std-ch + " the agent activity"
        bDelete:sensitive = (data.stat = "O")
        .
      std-ch = (if data.stat = "C" then "magnifier" else "update").
      bModify:load-image("images/" + std-ch + ".bmp").
      bModify:load-image-insensitive("images/" + std-ch + "-i.bmp").
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCategory C-Win
ON VALUE-CHANGED OF fCategory IN FRAME fMain /* Category */
DO:
  dynamic-function("setFilterCombos", self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fType C-Win
ON VALUE-CHANGED OF fType IN FRAME fMain /* Type */
DO:
  dynamic-function("setFilterCombos", self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDecide
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDecide C-Win
ON VALUE-CHANGED OF tDecide IN FRAME fMain
DO:
  do with frame {&frame-name}:
    run AgentComboEnable in this-procedure (true).
    run AgentComboHide in this-procedure (false).
    run AgentComboSet in this-procedure ("").
    assign
      tName:hidden = true
      tName:screen-value = ""
      tStateID:hidden = false
      tStateID:sensitive = true
      tStateID:screen-value = "ALL"
      .
    case self:screen-value:
     when "0" then
      do:
        run AgentComboEnable in this-procedure (false).
        tStateID:sensitive = false.
      end.
     when "1" then
      do:
        tAgentID:sensitive = true.
      end.
     when "2" then
      do:
        run AgentComboHide in this-procedure (true).
        tName:hidden = false.
      end.
    end case.
  end.
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
  if tDecide:input-value in frame {&frame-name} = 1
   then run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain /* Year */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "ActivityDataChanged" anywhere.

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i &entity="'Activity'" &entityPlural="'Activities'"}
{lib/report-progress-bar.i}
{lib/set-buttons.i}
{lib/set-filters.i &tableName="'data'" &labelName="'Category,Type'" &columnName="'category,type'" &entity="'Activity'"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bImport:load-image("images/import.bmp").
bImport:load-image-insensitive("images/import-i.bmp").

/* fill the year list */
publish "GetActivityYearList" (output cYearList).
if cYearList > ""
 then tYear:list-items = cYearList.
 else tYear:list-items = string(year(today)).

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}
  
    {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
     
    /* set the agent list first */
    tDecide:screen-value = "1".
    apply "VALUE-CHANGED" to tDecide.
    /* set the values */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
    
    std-in = year(today).
    if lookup(string(std-in), tYear:list-items) > 0
     then tYear:screen-value = string(std-in).
     else tYear:screen-value = entry(1, tYear:list-items).
  end.
     
  clearData().
  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActivityDataChanged C-Win 
PROCEDURE ActivityDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* variables */
  define variable tActivityID as integer no-undo.
  define variable tRowID      as rowid   no-undo.
  define variable tPosition   as integer no-undo.
  
  /* buffers */
  define buffer tempdata for data.
  
  /* find the row selected */
  if available data
   then
    do with frame {&frame-name}:
      tActivityID = data.activityID.
      do tPosition = 1 to {&browse-name}:num-iterations:
         if {&browse-name}:is-row-selected(tPosition) 
          then leave.
      end.
    end.
  
  /* get the data */
  run getData in this-procedure.
  
  /* reposition to the correct row */
  for first tempdata no-lock
      where tempdata.activityID = tActivityID:
    
    tRowID = rowid(tempdata).
  end.
  if tRowID <> ? and tPosition > 0
   then
    do with frame {&frame-name}:
      {&browse-name}:set-repositioned-row(tPosition, "ALWAYS") no-error.
      reposition {&browse-name} to rowid tRowID no-error.
    end.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wamd03-t.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wamd03-t.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tYear tStateID fCategory tDecide tAgentID tName fType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-36 bGo RECT-37 RECT-38 tYear tStateID fCategory tDecide bNew 
         bImport tAgentID tName fType brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query {&browse-name}:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Agent_Activity"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* cleanup before getting data */  
  clearData().
  empty temp-table data.
  empty temp-table tempdata.
  do with frame {&frame-name}:
    publish "GetActivities" (input tYear:input-value,
                             input tName:input-value,
                             input tAgentID:input-value,
                             input tStateID:input-value,
                             input tDecide:input-value,
                             output table tempdata).
                           
    run SetProgressStatus.
    for each tempdata no-lock:
      create data.
      buffer-copy tempdata to data.
    end.
    setFilterCombos("ALL").
    dataSortDesc = not dataSortDesc.
    run sortData in this-procedure (dataSortBy).
    assign
      std-lo = enableButtons(can-find(first data))
      std-lo = enableFilters(can-find(first data))
      /* enable the buttons and filters */
      fCategory:sensitive = std-lo
      fType:sensitive = std-lo
      bExport:sensitive = std-lo
      bModify:sensitive = std-lo
      .
    apply "VALUE-CHANGED" to browse {&browse-name}.
    run SetProgressEnd.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ImportActivity C-Win 
PROCEDURE ImportActivity :
/*------------------------------------------------------------------------------
@description Imports the data from the spreadsheet
------------------------------------------------------------------------------*/
  define input parameter table for importdata.
  define output parameter pSuccess as logical no-undo.
  define output parameter pMsg as character no-undo.
  
  run server/importagentactivity.p (input pImportYear,
                                    input pImportType,
                                    input pImportCategory,
                                    input table importdata,
                                    output std-ch,
                                    output pSuccess,
                                    output pMsg).
  
  if not pSuccess and search(std-ch) <> ?
   then
    do:
      std-lo = true.
      message "There were errors with the import. Would you like to view the file?" view-as alert-box question buttons yes-no update std-lo.
      if std-lo
       then publish "OpenTempFile" ("ActivityErrors").
    end.
   else message "Agent Activity File Imported" view-as alert-box information buttons ok.
   
  pSuccess = true. /* prevents alert box from displaying when returning to the import dialog */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cQuery as character no-undo.
  define variable hBuffer as handle no-undo.
  define variable hQuery as handle no-undo.

  std-ch = doFilterSort().
  {lib/brw-sortData.i &pre-by-clause="std-ch +" &post-by-clause="+ ' by agentID by name'"}

  setStatusCount(num-results("{&browse-name}")).
  bClose:sensitive in frame {&frame-name} = can-find(first data where stat = "O").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /*if totalAdded
   then iOffset = 23.*/

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
 
  /* resize the column width */
  {lib/resize-column.i &col="'name'" &var=dColumnWidth}
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with FRAME {&frame-name}:
    assign
      /* disable the filters */
      std-lo = enableButtons(false)
      std-lo = enableFilters(false)
      /* default the filter */
      fCategory:screen-value = "ALL"
      fType:screen-value = "ALL"
      /* disable the buttons and filters */
      fCategory:sensitive = false
      fType:sensitive = false
      bExport:sensitive = false
      bModify:sensitive = false
      bDelete:sensitive = false
      bClose:sensitive = false
      .
  end.
  empty temp-table data.
  close query {&browse-name}.
  clearStatus().
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createCopy C-Win 
FUNCTION createCopy RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  empty temp-table tempdata.
  for each data no-lock:
    create tempdata.
    buffer-copy data to tempdata.
  end.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showOpenWindowForActivity C-Win 
FUNCTION showOpenWindowForActivity RETURNS HANDLE
  ( input pType as character,
    input table for tempdata,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    .
  for first tempdata no-lock:
    publish "OpenActivity" (tempdata.activityID, output hFileDataSrv).
    for first openwindow no-lock
        where openwindow.procFile = string(tempdata.activityID) + "-" + pType:
        
      hWindow = openwindow.procHandle.
    end.
    
    if not valid-handle(hWindow)
     then
      do:
        run value(pFile) persistent set hWindow (hFileDataSrv).
        create openwindow.
        assign
          openwindow.procFile = string(tempdata.activityID) + "-" + pType
          openwindow.procHandle = hWindow
          .
      end.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

