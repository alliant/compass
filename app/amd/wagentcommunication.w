&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wagentcommunication.w

  Description: Window for agent communication

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: sagar

  Created: 11.07.2023
  
  @Modified:
  Date        Name     Description   

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/set-button-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/get-column.i}
{lib/winshowscrollbars.i}
{lib/normalizefileid.i}  /* include file to normalize file number */
{lib/add-delimiter.i}

/* Temp-table Definition */
{tt/agentcommunication.i}
{tt/agentcommunication.i &tableAlias = "ttagentcommunication"}


define variable cyear              as character no-undo.
define variable cstatus            as character no-undo.
define variable cmonth             as character no-undo.
define variable currManager        as character no-undo.
define variable currregion         as character no-undo.
define variable curragent          as character no-undo.
define variable cYearList          as character no-undo.
define variable cMonthList         as character no-undo.
define variable cStatusList        as character no-undo.
define variable imaxyear           as integer   no-undo.
define variable iminyear           as integer   no-undo.
define variable iAgents            as integer   no-undo.
define variable cAgent             as character no-undo.         
define variable deagentColumnWidth as decimal   no-undo.
define variable lsortcolumn        as logical   no-undo initial false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwagentcmmns

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttagentcommunication

/* Definitions for BROWSE brwagentcmmns                                 */
&Scoped-define FIELDS-IN-QUERY-brwagentcmmns ttagentcommunication.agent ttagentcommunication.year ttagentcommunication.month ttagentcommunication.stateName ttagentcommunication.Netpremium ttagentcommunication.NetPlan ttagentcommunication.AgentCommunication ttagentcommunication.uwCommunication ttagentcommunication.TotalFiles   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwagentcmmns   
&Scoped-define SELF-NAME brwagentcmmns
&Scoped-define QUERY-STRING-brwagentcmmns for each ttagentcommunication
&Scoped-define OPEN-QUERY-brwagentcmmns open query {&SELF-NAME} for each ttagentcommunication.
&Scoped-define TABLES-IN-QUERY-brwagentcmmns ttagentcommunication
&Scoped-define FIRST-TABLE-IN-QUERY-brwagentcmmns ttagentcommunication


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwagentcmmns}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bFilterClear cbregion cbagent brefresh ~
cbstatus bExport cbmanager cbyear cbmonth brwagentcmmns RECT-82 RECT-84 
&Scoped-Define DISPLAYED-OBJECTS cbregion cbagent cbstatus cbmanager cbyear ~
cbmonth 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAgentFilterCombo C-Win 
FUNCTION getAgentFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
    output pagentlist as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterAgents C-Win 
FUNCTION getFilterAgents RETURNS CHARACTER
( input pfilter as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterData C-Win 
FUNCTION getFilterData RETURNS CHARACTER
  ( input tWhereClause as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilteredAgents C-Win 
FUNCTION getFilteredAgents RETURNS CHARACTER
( input pfilter as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getManagerFilterCombo C-Win 
FUNCTION getManagerFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
   output pManagerlist as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getMonthFilterCombo C-Win 
FUNCTION getMonthFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
   output cMonthList as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getRegionFilterCombo C-Win 
FUNCTION getRegionFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
     output pRegionlist as character  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatusFilterCombo C-Win 
FUNCTION getStatusFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
    output cStatusList as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getYearFilterCombo C-Win 
FUNCTION getYearFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
    output pyearlist as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.7 TOOLTIP "Export data".

DEFINE BUTTON bFilterClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 7.2 BY 1.7 TOOLTIP "Clear all filters".

DEFINE BUTTON brefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.7 TOOLTIP "Refresh".

DEFINE VARIABLE cbagent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN
     SIZE 46.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbmanager AS CHARACTER FORMAT "X(256)":U 
     LABEL "Manager" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE cbmonth AS CHARACTER FORMAT "X(256)":U 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE cbregion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbstatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 25.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbyear AS CHARACTER FORMAT "x(8)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 13 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-82
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 151 BY 3.04.

DEFINE RECTANGLE RECT-84
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.8 BY 3.04.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwagentcmmns FOR 
      ttagentcommunication SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwagentcmmns
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwagentcmmns C-Win _FREEFORM
  QUERY brwagentcmmns DISPLAY
      ttagentcommunication.agent     label  "Agent"  format "x(200)"       width 36
ttagentcommunication.year     label  "Year"         width 10
ttagentcommunication.month     label  "Month"         width 10
ttagentcommunication.stateName     label  "State"  format "x(50)"        width 15
ttagentcommunication.Netpremium     label  "Netpremium"         width 16
ttagentcommunication.NetPlan        label  "NetPlan"          width 16
ttagentcommunication.AgentCommunication      label  "Agent Communication"        width 22
ttagentcommunication.uwCommunication   label  "UW Communication"              width 20
ttagentcommunication.TotalFiles     label  "Total Files"                                  width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 168.2 BY 17.9 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bFilterClear AT ROW 2 COL 144 widget-id 344 NO-TAB-STOP 
     cbregion AT ROW 1.78 COL 12.6 COLON-ALIGNED WIDGET-ID 482
     cbagent AT ROW 2.87 COL 12.6 COLON-ALIGNED WIDGET-ID 488
     brefresh AT ROW 1.96 COL 154.2 WIDGET-ID 478 NO-TAB-STOP 
     cbstatus AT ROW 1.78 COL 74.6 COLON-ALIGNED WIDGET-ID 504
     bExport AT ROW 1.96 COL 161.2 WIDGET-ID 480 NO-TAB-STOP 
     cbmanager AT ROW 2.87 COL 74.6 COLON-ALIGNED WIDGET-ID 490
     cbyear AT ROW 1.78 COL 124 COLON-ALIGNED WIDGET-ID 484
     cbmonth AT ROW 2.87 COL 124 COLON-ALIGNED WIDGET-ID 486
     brwagentcmmns AT ROW 4.44 COL 1.8 WIDGET-ID 200
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .61 AT ROW 1.04 COL 154.2 WIDGET-ID 194
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .61 AT ROW 1 COL 4 WIDGET-ID 476
     RECT-82 AT ROW 1.3 COL 2 WIDGET-ID 460
     RECT-84 AT ROW 1.3 COL 152.6 WIDGET-ID 470
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 178.6 BY 25.09 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Communication"
         HEIGHT             = 21.44
         WIDTH              = 170.6
         MAX-HEIGHT         = 27.57
         MAX-WIDTH          = 256
         VIRTUAL-HEIGHT     = 27.57
         VIRTUAL-WIDTH      = 256
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwagentcmmns cbmonth DEFAULT-FRAME */
ASSIGN 
       brwagentcmmns:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwagentcmmns:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwagentcmmns
/* Query rebuild information for BROWSE brwagentcmmns
     _START_FREEFORM
open query {&SELF-NAME} for each ttagentcommunication.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwagentcmmns */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Communication */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Communication */
DO:
  /* This event will close the window and terminate the procedure.  */
  apply "CLOSE":U to this-procedure.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Communication */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterClear C-Win
ON CHOOSE OF bFilterClear IN FRAME DEFAULT-FRAME /* Clear */
DO:
  if lookup("ALL",cbagent:list-item-pairs) = 0
   then 
    do:
      cbagent:list-item-pairs = "ALL^ALL" .
    end.
  assign
    cbregion:screen-value  = "ALL"
    cbmanager:screen-value = "ALL"
    cbyear:screen-value    = "ALL"
    cbmonth:screen-value   = "ALL"
    cbagent:screen-value   = "ALL"
    cbstatus:screen-value  = "ALL"
      .
   setFilterCombos("ALL").   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME brefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brefresh C-Win
ON CHOOSE OF brefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  run ActionRefresh     in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwagentcmmns
&Scoped-define SELF-NAME brwagentcmmns
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwagentcmmns C-Win
ON ROW-DISPLAY OF brwagentcmmns IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
  
  if ttagentcommunication.Netpremium < 0 then
   do:
     ttagentcommunication.Netpremium:fgcolor in browse brwagentcmmns = 12.
   end.
  
  if ttagentcommunication.NetPlan < 0 then
   do:
     ttagentcommunication.NetPlan:fgcolor in browse brwagentcmmns = 12.
   end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwagentcmmns C-Win
ON START-SEARCH OF brwagentcmmns IN FRAME DEFAULT-FRAME
do:
   lsortcolumn = true.
   {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbagent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbagent C-Win
ON RETURN OF cbagent IN FRAME DEFAULT-FRAME /* Agent */
DO:
      if cbregion:screen-value = "ALL" and cbmanager:screen-value = "ALL" and
       cbstatus:screen-value = "ALL" and 
       cbyear:screen-value   = "ALL" and cbmonth:screen-value   = "ALL" 
     then
      lsortcolumn = true.
     else
      lsortcolumn = false.
    
  if (cbagent:screen-value <> "" and cbagent:screen-value <> ?  and cbagent:screen-value <> "ALL"  )
   then
    do:
      if lsortcolumn = true
       then
        getFilterAgents(cbagent:screen-value).
       else
        getFilteredAgents(cbagent:screen-value).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbagent C-Win
ON VALUE-CHANGED OF cbagent IN FRAME DEFAULT-FRAME /* Agent */
DO:
  if (can-find (first agentcommunication where agentcommunication.name = cbagent:screen-value)) or cbagent:screen-value = "ALL"
   then
    do:
      setFilterCombos(self:label).   
    end.
     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbmanager
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbmanager C-Win
ON VALUE-CHANGED OF cbmanager IN FRAME DEFAULT-FRAME /* Manager */
DO:
  if cbmanager:screen-value <> "" or cbmanager:screen-value <> ?
   then
    setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbmonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbmonth C-Win
ON VALUE-CHANGED OF cbmonth IN FRAME DEFAULT-FRAME /* Month */
DO:
  if cbmonth:screen-value <> "" or cbmonth:screen-value <> ?
   then
    setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbregion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbregion C-Win
ON VALUE-CHANGED OF cbregion IN FRAME DEFAULT-FRAME /* Region */
DO:
  if cbregion:screen-value <> "" or cbregion:screen-value <> ?
   then
    setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbstatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbstatus C-Win
ON VALUE-CHANGED OF cbstatus IN FRAME DEFAULT-FRAME /* Status */
DO:
  if cbstatus:screen-value <> "" or cbstatus:screen-value <> ?
   then
    setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbyear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbyear C-Win
ON VALUE-CHANGED OF cbyear IN FRAME DEFAULT-FRAME /* Year */
DO:
  if cbyear:screen-value <> "" or cbyear:screen-value <> ?
   then
    setFilterCombos(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i} 
{lib/win-status.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

{lib/set-button.i &frame-name={&FRAME-NAME} &b=bExport      &label="Export"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=bFilterClear &label="FilterClear"}
{lib/set-button.i &frame-name={&FRAME-NAME} &b=brefresh     &label="Refresh"}

setButtons().
cbagent:delimiter         = "^"  .

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  run enable_UI.
  
  {lib/get-column-width.i &browse-name=brwagentcmmns &var=deagentColumnWidth &col="'Agent'"}
  
    assign
    cbregion:screen-value  = "ALL"
    cbmanager:screen-value = "ALL"
    cbyear:screen-value    = "ALL"
    cbmonth:screen-value   = "ALL"
    cbagent:screen-value   = "ALL"
    cbstatus:screen-value  = "ALL"
      . 
    bFilterClear:sensitive = false.  
    run ActionRefresh in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRefresh C-Win 
PROCEDURE ActionRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.
  
  empty temp-table agentcommunication.
  close query brwagentcmmns.

   /* call server */
  publish "LoadAgentCommunication"  (output table agentcommunication).
  
  /* This will use the screen-value of the filters which is ALL */

  setFilterCombos("ALL"). 
  setStatusRecords(query brwagentcmmns:num-results).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActions C-Win 
PROCEDURE enableActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwagentcmmns:num-results > 0
   then
    bExport:sensitive = true.
   else
    bExport:sensitive  = false.
    
  if cbregion:screen-value  = "ALL" and cbmanager:screen-value = "ALL" and
     cbyear:screen-value = "ALL" and cbmonth:screen-value = "ALL" and
     cbagent:screen-value = "ALL" and cbstatus:screen-value  = "ALL"
   then
    bFilterClear:sensitive = false.
   else
    bFilterClear:sensitive = true.
    
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbregion cbagent cbstatus cbmanager cbyear cbmonth 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bFilterClear cbregion cbagent brefresh cbstatus bExport cbmanager 
         cbyear cbmonth brwagentcmmns RECT-82 RECT-84 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwagentcmmns:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
   

  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table ttagentcommunication:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttagentcommunication",
                          "for each ttagentcommunication",
                          "agentID,name,year,month,Manager,stateName,Region,Netpremium,NetPlan,AgentCommunication,uwCommunication,LenderpolicyReported,OwnerPolicyReported,CPLReported,LenderFiles,OwnerFiles,OwnerLenderFiles,TotalFiles",
                          "Agent ID,Name,Year,Month,Manager,State,Region,Netpremium,NetPlan,Agent Communication,UW Communication,Lender Policy Reported,Owner Policy Reported,CPL Reported,Lender Files,Owner Files,Owner Lender Files,TotalFiles" ,
                          std-ch,
                          "Agent Communication-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).      
                           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cRegion  as character no-undo.
  define variable cManager as character no-undo.
  define variable cMonth   as character no-undo.
  define variable cYear    as character no-undo.
  define variable cStatus  as character no-undo.
  
  define variable tWhereClause as character no-undo.
  
  define variable hQuery as handle no-undo.
  
  do with frame {&frame-name}:
    assign
      cRegion  = cbregion:screen-value
      cManager = cbmanager:screen-value
      cYear    = cbyear:screen-value
      cMonth   = cbmonth:screen-value
      cStatus  = cbstatus:screen-value
      .
      if cAgent = ""
       then
        cAgent   = cbagent:screen-value.
  end.
  
  /* build the query */
  if cRegion <> "ALL" and cRegion <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "region='" + cRegion + "'".
  if cManager <> "ALL" and cManager <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "manager='" + cManager + "'".
  if cYear <> "ALL" and cYear <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "year='" + cYear + "'".
  if cMonth <> "ALL" and cMonth <> ?
   then tWhereClause = addDelimiter(tWhereClause," and ") + "month='" + cMonth + "'".
  if cAgent <> "ALL" and cAgent <> ? 
   then tWhereClause = addDelimiter(tWhereClause," and ") + "name='" + cAgent + "'".
  if cStatus <> "ALL" and cStatus <> ? 
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stat='" + cStatus + "'".
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
 
  getFilterData(tWhereClause) .
  open query brwagentcmmns preselect each ttagentcommunication.
  
  if lsortcolumn 
   then
    {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}

  lsortcolumn = false.
  cAgent = "" .
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels

      /* {&frame-name} components */
     {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
     {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.

     {lib/resize-column.i &browse-name=brwagentcmmns &col="'Agent'" &var=deagentColumnWidth}
  
  run ShowScrollBars(frame {&frame-name}:handle, no, no).      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAgentFilterCombo C-Win 
FUNCTION getAgentFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
    output pagentlist as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pmanager = ?
   then
    pmanager = "ALL"  .
  if pregion = ?
   then
    pregion = "ALL"  .
  if pyear = ?
   then
    pyear = "ALL"  .
  if pmonth = ?
   then
    pmonth = "ALL"  .
  if pstatus = ?
   then
    pstatus = "ALL"  .
  if pagent = ?
   then
    pagent = "ALL"  .
    
  assign
    iAgents = 0.
    pagentlist = "" .

  for each agentcommunication fields(manager region year month stat name agentID) no-lock  by name:
    
    if pmanager <> "ALL" and pmanager <> agentcommunication.manager
     then next.
    if pregion <> "ALL" and pregion <> agentcommunication.region
     then next.
    if pyear <> "ALL" and pyear <> agentcommunication.year
     then next.
    if pmonth <> "ALL" and pmonth <> agentcommunication.month
     then next.
    if pstatus <> "ALL" and pstatus <> agentcommunication.stat
     then next.
    if pagent <> "ALL" and pagent <> agentcommunication.name
     then next.
     
    if agentcommunication.name <> "" and lookup(agentcommunication.name,pagentlist,"^") = 0
     then 
      do:
        iAgents = iAgents + 1.
        if iAgents > 100
         then next.
          pagentlist = addDelimiter(pagentlist,"^") + agentcommunication.name + " (" + agentcommunication.agentID + " )"  + "^" + agentcommunication.name.
      end.
  end.
  if pagentlist = ""
   then
    pagentlist = "ALL^ALL".
   else
    pagentlist = "ALL^ALL" + "^" + pagentlist.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterAgents C-Win 
FUNCTION getFilterAgents RETURNS CHARACTER
( input pfilter as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cList as char no-undo.
  iAgents = 0.
  cList = "".

  do with frame {&frame-name}:
  end.

  for each agentcommunication fields(name agentID) no-lock :
    
    if pfilter > "" and not (agentcommunication.name matches "*" + pfilter + "*" or agentcommunication.agentID begins pfilter  ) 
     then next.

    if lookup(agentcommunication.name,cList,"^") = 0
     then 
      do:
         iAgents = iAgents + 1.
         if iAgents > 100
          then next.
         cList = addDelimiter(cList,"^") + agentcommunication.name + " (" + agentcommunication.agentID + " )"  + "^" + agentcommunication.name.
      end.
  end.
  if cList = "" 
   then
    cList = "ALL^All".
   else
    cbagent:list-item-pairs   = cList.   
  return "".   /* Function return value. */


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterData C-Win 
FUNCTION getFilterData RETURNS CHARACTER
  ( input tWhereClause as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable qh       as handle    no-undo.
 
  empty temp-table  ttagentcommunication.
  create  query  qh.
  qh:set-buffers(buffer agentcommunication:handle).
  qh:QUERY-PREPARE ("FOR EACH agentcommunication " + tWhereClause).

   qh:query-open.
   do while  qh:get-next():
   create ttagentcommunication.
   assign
     ttagentcommunication.agentID              = agentcommunication.agentID
     ttagentcommunication.name                 = agentcommunication.name
     ttagentcommunication.year                 = agentcommunication.year
     ttagentcommunication.month                = agentcommunication.month
     ttagentcommunication.stat                 = agentcommunication.stat
     ttagentcommunication.stateName            = agentcommunication.stateName
     ttagentcommunication.Netpremium           = agentcommunication.Netpremium
     ttagentcommunication.NetPlan              = agentcommunication.NetPlan
     ttagentcommunication.AgentCommunication   = agentcommunication.AgentCommunication
     ttagentcommunication.uwCommunication      = agentcommunication.uwCommunication
     ttagentcommunication.TotalFiles           = agentcommunication.TotalFiles 
     ttagentcommunication.Manager              = agentcommunication.Manager
     ttagentcommunication.Region               = agentcommunication.Region
     ttagentcommunication.LenderpolicyReported = agentcommunication.LenderpolicyReported
     ttagentcommunication.OwnerPolicyReported  = agentcommunication.OwnerPolicyReported
     ttagentcommunication.CPLReported          = agentcommunication.CPLReported
     ttagentcommunication.LenderFiles          = agentcommunication.LenderFiles
     ttagentcommunication.OwnerFiles           = agentcommunication.OwnerFiles
     ttagentcommunication.OwnerLenderFiles     = agentcommunication.OwnerLenderFiles
     ttagentcommunication.agent                = agentcommunication.name + "( " + agentcommunication.agentID + " )".
     .          
   end.
 
 
   qh:query-close().
   delete object qh.
  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilteredAgents C-Win 
FUNCTION getFilteredAgents RETURNS CHARACTER
( input pfilter as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cList as char no-undo.
  iAgents = 0.
  cList = "".

  do with frame {&frame-name}:
  end.
  
  for each ttagentcommunication fields(name agentID) no-lock :
    
    if pfilter > "" and not (ttagentcommunication.name matches "*" + pfilter + "*" or ttagentcommunication.agentID begins pfilter  ) 
     then next.

    if lookup(ttagentcommunication.name,cList,"^") = 0
     then 
      do:
         iAgents = iAgents + 1.
         if iAgents > 100
          then next.
         cList = addDelimiter(cList,"^") + ttagentcommunication.name + " (" + ttagentcommunication.agentID + " )"  + "^" + ttagentcommunication.name.
      end.
  end.
  if cList = "" 
   then
    cList = "ALL^All".
   else
    cbagent:list-item-pairs   = cList.
    
  return "".   /* Function return value. */


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getManagerFilterCombo C-Win 
FUNCTION getManagerFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
   output pManagerlist as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pmanager = ?
   then
    pmanager = "ALL"  .
  if pregion = ?
   then
    pregion = "ALL"  .
  if pyear = ?
   then
    pyear = "ALL"  .
  if pmonth = ?
   then
    pmonth = "ALL"  .
  if pstatus = ?
   then
    pstatus = "ALL"  .
  if pagent = ?
   then
    pagent = "ALL"  .
    
  assign
    iAgents = 0
    pManagerlist = ""     .

  for each agentcommunication fields(manager region year month stat name) no-lock by manager :
    
    if pmanager <> "ALL" and pmanager <> agentcommunication.manager
     then next.
    if pregion <> "ALL" and pregion <> agentcommunication.region
     then next.
    if pyear <> "ALL" and pyear <> agentcommunication.year
     then next.
    if pmonth <> "ALL" and pmonth <> agentcommunication.month
     then next.
    if pstatus <> "ALL" and pstatus <> agentcommunication.stat
     then next.
    if pagent <> "ALL" and pagent <> agentcommunication.name
     then next.
   
    if agentcommunication.manager <> "" and lookup(agentcommunication.manager,pManagerlist) = 0
     then 
      pManagerlist = addDelimiter(pManagerlist,",") + agentcommunication.manager + "," + agentcommunication.manager.

  end.
  if pManagerlist = ""
   then
    pManagerlist = "ALL,ALL".
   else
    pManagerlist = "ALL,ALL" + "," + pManagerlist.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getMonthFilterCombo C-Win 
FUNCTION getMonthFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
   output cMonthList as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cmonthdata  as  character    no-undo.
  define variable cAllmonth   as  character    no-undo.
  define variable icount      as  integer      no-undo.
  define variable cmonth      as  character    no-undo.
  
  
  if pmanager = ?
   then
    pmanager = "ALL"  .
  if pregion = ?
   then
    pregion = "ALL"  .
  if pyear = ?
   then
    pyear = "ALL"  .
  if pmonth = ?
   then
    pmonth = "ALL"  .
  if pstatus = ?
   then
    pstatus = "ALL"  .
  if pagent = ?
   then
    pagent = "ALL"  .
  
  cAllmonth = "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC".
  
  cmonthdata = "".
  for each agentcommunication fields(manager region year month stat name) no-lock by month :
    
    if pmanager <> "ALL" and pmanager <> agentcommunication.manager
     then next.
    if pregion <> "ALL" and pregion <> agentcommunication.region
     then next.
    if pyear <> "ALL" and pyear <> agentcommunication.year
     then next.
    if pmonth <> "ALL" and pmonth <> agentcommunication.month
     then next.
    if pstatus <> "ALL" and pstatus <> agentcommunication.stat
     then next.
    if pagent <> "ALL" and pagent <> agentcommunication.name
     then next.
     
    if agentcommunication.month <> "" and lookup(agentcommunication.month,cmonthdata) = 0
     then 
      cmonthdata = addDelimiter(cmonthdata,",") + agentcommunication.month.

  end.
  
  do icount = 1 to NUM-ENTRIES(cAllmonth):
    cmonth = entry(icount,cAllmonth).
    if lookup(cmonth,cmonthdata) > 0 
     then
      cMonthList = addDelimiter(cMonthList,",") + cmonth.
  end.
  
  if cMonthList = ""
   then
    cMonthList = "ALL".
   else
    cMonthList = "ALL" + "," + cMonthList.     
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getRegionFilterCombo C-Win 
FUNCTION getRegionFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
     output pRegionlist as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pmanager = ?
   then
    pmanager = "ALL"  .
  if pregion = ?
   then
    pregion = "ALL"  .
  if pyear = ?
   then
    pyear = "ALL"  .
  if pmonth = ?
   then
    pmonth = "ALL"  .
  if pstatus = ?
   then
    pstatus = "ALL"  .
  if pagent = ?
   then
    pagent = "ALL"  .
    
  pRegionlist = "".

  for each agentcommunication fields(manager region year month stat name) no-lock by region:
    
    if pmanager <> "ALL" and pmanager <> agentcommunication.manager
     then next.
    if pregion <> "ALL" and pregion <> agentcommunication.region
     then next.
    if pyear <> "ALL" and pyear <> agentcommunication.year
     then next.
    if pmonth <> "ALL" and pmonth <> agentcommunication.month
     then next.
    if pstatus <> "ALL" and pstatus <> agentcommunication.stat
     then next.
    if pagent <> "ALL" and pagent <> agentcommunication.name
     then next.
    
    if agentcommunication.region <> "" and lookup(agentcommunication.region,pRegionlist) = 0
     then 
      pRegionlist = addDelimiter(pRegionlist,",") + agentcommunication.region + "," + agentcommunication.region.

  end.
  if pRegionlist = ""
   then
    pRegionlist = "ALL,ALL".
   else
    pRegionlist = "ALL,ALL" + "," + pRegionlist.
   
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatusFilterCombo C-Win 
FUNCTION getStatusFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
    output cStatusList as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pmanager = ?
   then
    pmanager = "ALL"  .
  if pregion = ?
   then
    pregion = "ALL"  .
  if pyear = ?
   then
    pyear = "ALL"  .
  if pmonth = ?
   then
    pmonth = "ALL"  .
  if pstatus = ?
   then
    pstatus = "ALL"  .
  if pagent = ?
   then
    pagent = "ALL"  .
    
  assign
    cStatusList = ""  .

  for each agentcommunication fields(manager region year month stat name statDesc) no-lock by statDesc :
    
    if pmanager <> "ALL" and pmanager <> agentcommunication.manager
     then next.
    if pregion <> "ALL" and pregion <> agentcommunication.region
     then next.
    if pyear <> "ALL" and pyear <> agentcommunication.year
     then next.
    if pmonth <> "ALL" and pmonth <> agentcommunication.month
     then next.
    if pstatus <> "ALL" and pstatus <> agentcommunication.stat
     then next.
    if pagent <> "ALL" and pagent <> agentcommunication.name
     then next.
     
    if lookup(agentcommunication.Stat,cStatusList) = 0
     then 
      do:
        cStatusList = addDelimiter(cStatusList,",") + statDesc + "," + agentcommunication.Stat.
      end.
   
  end.
  if cStatusList = ""
   then
    cStatusList = "ALL,ALL".
   else
    cStatusList = "ALL,ALL" + "," + cStatusList.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getYearFilterCombo C-Win 
FUNCTION getYearFilterCombo returns character
  ( input pmanager as character , input pregion as character ,input pagent as character , input pyear as character , input pmonth as character , input pstatus as character ,
    output pyearlist as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pmanager = ?
   then
    pmanager = "ALL"  .
  if pregion = ?
   then
    pregion = "ALL"  .
  if pyear = ?
   then
    pyear = "ALL"  .
  if pmonth = ?
   then
    pmonth = "ALL"  .
  if pstatus = ?
   then
    pstatus = "ALL"  .
  if pagent = ?
   then
    pagent = "ALL"  .
    
  assign
    pyearlist = "".

  for each agentcommunication fields(manager region year month stat name) no-lock by year:
    
    if pmanager <> "ALL" and pmanager <> agentcommunication.manager
     then next.
    if pregion <> "ALL" and pregion <> agentcommunication.region
     then next.
    if pyear <> "ALL" and pyear <> agentcommunication.year
     then next.
    if pmonth <> "ALL" and pmonth <> agentcommunication.month
     then next.
    if pstatus <> "ALL" and pstatus <> agentcommunication.stat
     then next.
    if pagent <> "ALL" and pagent <> agentcommunication.name
     then next.
     
    if agentcommunication.year <> "" and lookup(agentcommunication.year,pyearlist) = 0
     then 
      pyearlist = addDelimiter(pyearlist,",") + agentcommunication.year.                    

  end.
  if pyearlist = ""
   then
    pyearlist = "ALL".
   else
    pyearlist = "ALL" + ","  + pyearlist.
  return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombos C-Win 
FUNCTION setFilterCombos RETURNS LOGICAL
  ( input pName as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/ 
  define variable cregion  as character no-undo.
  define variable cagent   as character no-undo.
  define variable cmanager as character no-undo.
  define variable cyear    as character no-undo.
  define variable cmonth   as character no-undo.
  define variable cstatus  as character no-undo.
  
  do with frame {&frame-name}:
   
   assign 
    cregion  = cbregion:screen-value
    cagent   = cbagent:screen-value
    cmanager = cbmanager:screen-value
    cyear    = cbyear:screen-value
    cmonth   = cbmonth:screen-value
    cstatus  = cbstatus:screen-value
    curragent   = "".
    
    if cbregion:screen-value = "ALL" and cbmanager:screen-value = "ALL" and
       cbstatus:screen-value = "ALL" and cbagent:screen-value   = "ALL" and
       cbyear:screen-value   = "ALL" and cbmonth:screen-value   = "ALL" 
     then
      pName = "ALL".

    case pName:
     when "ALL" 
      then
       do:
         getManagerFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currManager).
         getStatusFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cStatusList).
         getAgentFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             curragent) .
         getRegionFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currregion).
         getMonthFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cMonthList).
         getYearFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cYearList) .
       end.
     when "Manager"
      then
       do:
         getStatusFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cStatusList).
         getAgentFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             curragent) .
         getRegionFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currregion).
         getMonthFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cMonthList).
         getYearFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cYearList) .
       end.

     when "Region"
      then
       do:
         getStatusFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cStatusList).
         getAgentFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             curragent) .
         getManagerFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currManager).
         getMonthFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cMonthList).
         getYearFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cYearList) .
       end.
        
     when "Agent" 
      then
       do:
         getManagerFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currManager).
         getStatusFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cStatusList).
         getRegionFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currregion).
         getMonthFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cMonthList).
         getYearFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cYearList) .
       end.
           
     when "Year"
      then
       do:
         getManagerFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currManager).
         getStatusFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cStatusList).
         getAgentFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             curragent) .
         getRegionFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currregion).
         getMonthFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cMonthList).
       end.
     when "Month"
      then
       do:
         getManagerFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currManager).
         getStatusFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cStatusList).
         getAgentFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             curragent) .
         getRegionFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currregion).
         getYearFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cYearList) .
       end.
     when "Status"
      then
       do:
         getManagerFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currManager).
         getMonthFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cMonthList).
         getAgentFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             curragent) .
         getRegionFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             currregion).
         getYearFilterCombo(cmanager,cregion,cagent,cyear,cmonth,cstatus,
                             cYearList) .
       end.

        
    end case.   

    
    assign
      cbmanager:list-item-pairs = currManager
      cbregion:list-item-pairs  = currregion
      cbyear:list-items         = cYearList
      cbmonth:list-items        = cMonthList 
      cbstatus:list-item-pairs  = cStatusList 
     no-error .
   
   if pName <> "Agent" 
    then
      cbagent:list-item-pairs   = curragent no-error .
      
            
    if cmanager <> "ALL"
     then cbmanager:screen-value = cmanager.
     else cbmanager:screen-value = "ALL". 
    if cbregion <> "ALL"
     then cbregion:screen-value = cregion.
     else cbregion:screen-value = "ALL".
    if cbyear <> "ALL"
     then cbyear:screen-value = cyear.
     else cbyear:screen-value = "ALL".
    if cbmonth <> "ALL"
     then cbmonth:screen-value = cmonth.
     else cbmonth:screen-value = "ALL".
    if cbagent <> "ALL"
     then cbagent:screen-value = cagent.
     else cbagent:screen-value = "ALL".
    if cstatus <> "ALL"
     then cbstatus:screen-value = cstatus.
     else cbstatus:screen-value = "ALL".     
  end.
                
  run sortData in this-procedure("") .
  run enableActions in this-procedure.
      
  setStatusCount(query brwagentcmmns:num-results).  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

