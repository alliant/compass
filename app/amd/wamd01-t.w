&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Open Alerts
@description Displays the open alerts

@author John Oliver
@version 1.0
@created 2017/09/26
@notes 
---------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/state.i}
{tt/alert.i &tableAlias="data"}
{tt/alert.i &tableAlias="tempdata"}
{tt/openwindow.i}

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/get-column.i}

define variable dColumnWidth as decimal no-undo.

define variable currCode as character no-undo.
define variable currOwner as character no-undo.

define variable hNewAlert as handle no-undo.

define temp-table openAlert
  field alertID as integer
  field alertHandle as handle
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.dateCreated data.processCodeDesc data.thresholdRange data.scoreDesc data.severityDesc data.statDesc data.ownerDesc data.description /* data.createdByDesc */   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bNew RECT-36 RECT-37 RECT-38 tStateID fCode ~
tAgentID fOwner bGo brwData 
&Scoped-Define DISPLAYED-OBJECTS tStateID fCode tAgentID fOwner 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showOpenWindowForAlert C-Win 
FUNCTION showOpenWindowForAlert RETURNS HANDLE
  ( input pType as character,
    input table for tempdata,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwData 
       MENU-ITEM m_View_Historical_Alerts LABEL "View Historical Alerts".


/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh agents for the selected criteria".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the agent alert".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new agent alert".

DEFINE BUTTON bNewNote  NO-FOCUS
     LABEL "Note" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new note for the agent alert".

DEFINE VARIABLE fCode AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Alert Type" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE fOwner AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 44 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN AUTO-COMPLETION
     SIZE 68 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 96 BY 3.1.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60 BY 3.1.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.dateCreated column-label "Date Created" format "99/99/9999" width 15
data.processCodeDesc column-label "Alert Type" format "x(100)" width 50
data.thresholdRange column-label "Threshold" format "x(100)" width 25
data.scoreDesc column-label "Measure" format "x(20)" width 15
data.severityDesc column-label "Severity" format "x(50)" width 12
data.statDesc column-label "Stat" format "x(20)" width 10
data.ownerDesc column-label "Owner" format "x(100)" width 18
data.description column-label "Description" format "x(500)" width 33
/* data.createdByDesc column-label "Created By" format "x(100)" width 25 */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 185 BY 20.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bModify AT ROW 2.24 COL 109 WIDGET-ID 308 NO-TAB-STOP 
     bNew AT ROW 2.24 COL 101.8 WIDGET-ID 306 NO-TAB-STOP 
     bNewNote AT ROW 2.24 COL 116.2 WIDGET-ID 310 NO-TAB-STOP 
     bExport AT ROW 1.95 COL 89.4 WIDGET-ID 2 NO-TAB-STOP 
     tStateID AT ROW 2 COL 10 COLON-ALIGNED WIDGET-ID 166
     fCode AT ROW 2 COL 138 COLON-ALIGNED WIDGET-ID 300
     tAgentID AT ROW 3.19 COL 10 COLON-ALIGNED WIDGET-ID 84
     fOwner AT ROW 3.19 COL 138 COLON-ALIGNED WIDGET-ID 168
     bGo AT ROW 1.95 COL 82 WIDGET-ID 4 NO-TAB-STOP 
     brwData AT ROW 4.76 COL 2 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 128 WIDGET-ID 60
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 100 WIDGET-ID 304
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 127 WIDGET-ID 58
     RECT-38 AT ROW 1.48 COL 99 WIDGET-ID 302
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 187 BY 24.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Open Alerts"
         HEIGHT             = 24.86
         WIDTH              = 187
         MAX-HEIGHT         = 29.43
         MAX-WIDTH          = 256
         VIRTUAL-HEIGHT     = 29.43
         VIRTUAL-WIDTH      = 256
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData bGo fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bNewNote IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwData:HANDLE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.71
       COLUMN          = 82.2
       HEIGHT          = .48
       WIDTH           = 14.6
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(fOwner:HANDLE IN FRAME fMain).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Open Alerts */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Open Alerts */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Open Alerts */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  if not available data
   then return.
   
  empty temp-table tempdata.
  create tempdata.
  buffer-copy data to tempdata.
  showOpenWindowForAlert("modify", table tempdata, "dialogalertmodify.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  if valid-handle(hNewAlert)
   then run ShowWindow in hNewAlert.
   else run dialogalertnew.w persistent set hNewAlert.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewNote C-Win
ON CHOOSE OF bNewNote IN FRAME fMain /* Note */
DO:
  if available data
   then 
    do:
      empty temp-table tempdata.
      create tempdata.
      buffer-copy data to tempdata.
      showOpenWindowForAlert("note", table tempdata, "dialogalertnoteadd.w").
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  apply "CHOOSE" to bModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if available data
   then
    do:
      assign
        std-lo = (not data.active or stat = "C")
        bNewNote:sensitive = not std-lo
        std-ch = (if std-lo then "magnifier" else "update")
        bModify:tooltip = (if std-lo then "View the agent alert" else "Modify the agent alert")
        .
      bModify:load-image("images/" + std-ch + ".bmp").
      bModify:load-image-insensitive("images/" + std-ch + "-i.bmp").
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCode C-Win
ON VALUE-CHANGED OF fCode IN FRAME fMain /* Alert Type */
DO:
  dynamic-function("setFilterCombos", self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOwner
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOwner C-Win
ON VALUE-CHANGED OF fOwner IN FRAME fMain /* Owner */
DO:
  dynamic-function("setFilterCombos", self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Historical_Alerts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Historical_Alerts C-Win
ON CHOOSE OF MENU-ITEM m_View_Historical_Alerts /* View Historical Alerts */
DO:
  if not available data
   then return.
  
  /* save the state and agent before overriding the value */
  define variable cCurrStateID as character no-undo.
  define variable cCurrAgentID as character no-undo.
  publish "GetCurrentValue" ("StateID", output cCurrStateID).
  publish "GetCurrentValue" ("AgentID", output cCurrAgentID).
  
  publish "SetCurrentValue" ("StateID", data.stateID).
  publish "SetCurrentValue" ("AgentID", data.agentID).
  publish "SetCurrentValue" ("AlertType", data.processCode).
  run wamd02-r.w persistent set std-ha.
  if valid-handle(std-ha)
   then run GetData in std-ha.
  
  /* reset to old values */
  publish "SetCurrentValue" ("StateID", cCurrStateID).
  publish "SetCurrentValue" ("AgentID", cCurrAgentID).
  publish "SetCurrentValue" ("AlertType", "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON RETURN OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
  apply "CHOOSE" to bGo in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "AlertDataChanged" anywhere.

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/win-status.i &entity="'Open Alert'"}
{lib/brw-main.i}
{lib/report-progress-bar.i}
{lib/set-filters.i &entity="'Alert'" &tableName="'data'" &labelName="'Owner,Code'" &columnName="'owner,processCode'"}
{lib/set-buttons.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bNewNote:load-image("images/blank-add.bmp").
bNewNote:load-image-insensitive("images/blank-add-i.bmp").

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID &addAll=true}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}
    /* set the values */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
  end.
  
  /* get the column width */
  {lib/get-column-width.i &col="'description'" &var=dColumnWidth}
     
  clearData().
  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AlertDataChanged C-Win 
PROCEDURE AlertDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wamd01-t.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wamd01-t.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tStateID fCode tAgentID fOwner 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bNew RECT-36 RECT-37 RECT-38 tStateID fCode tAgentID fOwner bGo 
         brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Policy_Volume"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  
  /* cleanup before getting data */
  clearData().
  empty temp-table data.
  dStartTime = now.
  do with frame {&frame-name}:
    publish "LoadAlerts" (tStateID:screen-value, tAgentID:screen-value, "", "O").
    publish "GetAlerts" (output table data).
           
    run SetProgressStatus.
    setFilterCombos("ALL").
    run sortData in this-procedure ("").
    assign
      std-lo = enableButtons(can-find(first data))
      std-lo = enableFilters(can-find(first data))
      /* enable the buttons and filters */
      bNewNote:sensitive = std-lo
      .
    apply "ENTRY" to browse brwData.
    appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
    displayStatus().
    run SetProgressEnd.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDataForAgent C-Win 
PROCEDURE GetDataForAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  /* change the window title */
    std-ch = tAgentID:screen-value in frame {&frame-name}.
    for first agent no-lock
        where agent.agentID = tAgentID:screen-value:
      {&window-name}:title = "Open Alerts for " + agent.name.
    end.
    /* disable the new alert button, state drop down, and agent drop down */
    assign
      bNew:sensitive = false
      tStateID:sensitive = false
      tAgentID:sensitive = false
      .
    /* get the data */
    run GetData in this-procedure.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
   
  tWhereClause = doFilterSort().

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +" &post-by-clause="+ ' by agentID by stat desc by dateCreated by processCodeDesc'"}
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iOffset as int no-undo initial 0.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /*if totalAdded
   then iOffset = 23.*/

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
 
  /* resize the column */
  {lib/resize-column.i &col="'description'" &var=dColumnWidth}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with FRAME {&frame-name}:
    assign
      /* disable the filters */
      std-lo = enableButtons(false)
      std-lo = enableFilters(false)
      /* default the filter */
      fCode:screen-value = "ALL"
      fOwner:screen-value = "ALL"
      /* disable the buttons and filters */
      fCode:sensitive = false
      fOwner:sensitive = false
      bExport:sensitive = false
      bNewNote:sensitive = false
      bModify:sensitive = false
      .
  end.
  empty temp-table data.
  close query brwData.
  clearStatus().
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showOpenWindowForAlert C-Win 
FUNCTION showOpenWindowForAlert RETURNS HANDLE
  ( input pType as character,
    input table for tempdata,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    .
  for first tempdata no-lock:
    publish "OpenAlert" (tempdata.alertID, output hFileDataSrv).
    for first openwindow no-lock
        where openwindow.procFile = string(tempdata.alertID) + "-" + pType:
        
      hWindow = openwindow.procHandle.
    end.
    
    if not valid-handle(hWindow)
     then
      do:
        run value(pFile) persistent set hWindow (hFileDataSrv).
        create openwindow.
        assign
          openwindow.procFile = string(tempdata.alertID) + "-" + pType
          openwindow.procHandle = hWindow
          .
      end.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

