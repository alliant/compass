&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* Temp Table Definitions ---                                           */
{tt/agent.i}
{tt/state.i}
{tt/activity.i}

/* Functions ---                                                        */
{lib/add-delimiter.i}
{lib/find-widget.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-39 RECT-40 tStateID tName tAgentID ~
tType tCategory tStatus tYear tQuarter1Total tMonth1 tMonth2 tMonth3 ~
tQuarter2Total tMonth4 tMonth5 tMonth6 tQuarter3Total tMonth7 tMonth8 ~
tMonth9 tQuarter4Total tMonth10 tMonth11 tMonth12 tYearTotal bSave 
&Scoped-Define DISPLAYED-OBJECTS tStateID tName tAgentID tType tCategory ~
tStatus tYear tQuarter1Total tMonth1 tMonth2 tMonth3 tQuarter2Total tMonth4 ~
tMonth5 tMonth6 tQuarter3Total tMonth7 tMonth8 tMonth9 tQuarter4Total ~
tMonth10 tMonth11 tMonth12 tYearTotal 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calcQuarter C-Win 
FUNCTION calcQuarter RETURNS LOGICAL
  ( input pQuarter as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calcTotals C-Win 
FUNCTION calcTotals RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 76 BY 1 NO-UNDO.

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 10 BY 1 TOOLTIP "Please select the year"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tMonth1 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth10 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth11 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth12 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth2 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth3 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth4 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth5 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth6 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth7 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth8 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth9 AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 76 BY 1 NO-UNDO.

DEFINE VARIABLE tQuarter1Total AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Quarter 1" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tQuarter2Total AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Quarter 2" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tQuarter3Total AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Quarter 3" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tQuarter4Total AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Quarter 4" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tYearTotal AS INTEGER FORMAT "->>>,>>>,>>9":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89 BY 4.76.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89 BY 10.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tStateID AT ROW 2.19 COL 10 COLON-ALIGNED WIDGET-ID 166
     tName AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 316
     tAgentID AT ROW 3.38 COL 10 COLON-ALIGNED WIDGET-ID 84
     tType AT ROW 4.57 COL 10 COLON-ALIGNED WIDGET-ID 332
     tCategory AT ROW 4.57 COL 42 COLON-ALIGNED WIDGET-ID 330
     tStatus AT ROW 4.57 COL 72 COLON-ALIGNED WIDGET-ID 426
     tYear AT ROW 7.43 COL 41.4 NO-LABEL WIDGET-ID 384
     tQuarter1Total AT ROW 9.81 COL 12 COLON-ALIGNED WIDGET-ID 340
     tMonth1 AT ROW 9.81 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 354
     tMonth2 AT ROW 9.81 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 358
     tMonth3 AT ROW 9.81 COL 70 COLON-ALIGNED NO-LABEL WIDGET-ID 360
     tQuarter2Total AT ROW 11 COL 12 COLON-ALIGNED WIDGET-ID 344
     tMonth4 AT ROW 11 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 362
     tMonth5 AT ROW 11 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 364
     tMonth6 AT ROW 11 COL 70 COLON-ALIGNED NO-LABEL WIDGET-ID 366
     tQuarter3Total AT ROW 12.19 COL 12 COLON-ALIGNED WIDGET-ID 346
     tMonth7 AT ROW 12.19 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 368
     tMonth8 AT ROW 12.19 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 370
     tMonth9 AT ROW 12.19 COL 70 COLON-ALIGNED NO-LABEL WIDGET-ID 372
     tQuarter4Total AT ROW 13.38 COL 12 COLON-ALIGNED WIDGET-ID 348
     tMonth10 AT ROW 13.38 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 374
     tMonth11 AT ROW 13.38 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 376
     tMonth12 AT ROW 13.38 COL 70 COLON-ALIGNED NO-LABEL WIDGET-ID 378
     tYearTotal AT ROW 14.57 COL 12 COLON-ALIGNED WIDGET-ID 350
     bSave AT ROW 17.1 COL 39 WIDGET-ID 336
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 10 COL 50.8 WIDGET-ID 400
     "Yearly Details" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 6.48 COL 3 WIDGET-ID 338
     "Totals" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 9.1 COL 20 WIDGET-ID 342
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 11.19 COL 50.8 WIDGET-ID 402
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 12.38 COL 50.8 WIDGET-ID 404
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 13.57 COL 50.8 WIDGET-ID 406
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 10 COL 69.8 WIDGET-ID 408
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 11.19 COL 69.8 WIDGET-ID 410
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 12.38 COL 69.8 WIDGET-ID 412
     "+" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 13.57 COL 69.8 WIDGET-ID 414
     "=" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 10 COL 31.8 WIDGET-ID 416
     "=" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 11.19 COL 31.8 WIDGET-ID 418
     "=" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 13.57 COL 31.8 WIDGET-ID 420
     "=" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 12.38 COL 31.8 WIDGET-ID 422
     "Activity" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 318
     "Month 1" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 9.1 COL 38 WIDGET-ID 352
     "Month 2" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 9.1 COL 57 WIDGET-ID 380
     "Month 3" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 9.1 COL 76 WIDGET-ID 382
     RECT-39 AT ROW 1.48 COL 2 WIDGET-ID 168
     RECT-40 AT ROW 6.71 COL 2 WIDGET-ID 326
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 90.8 BY 17.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Modify Agent Activity"
         HEIGHT             = 17.81
         WIDTH              = 90.8
         MAX-HEIGHT         = 19.86
         MAX-WIDTH          = 101
         VIRTUAL-HEIGHT     = 19.86
         VIRTUAL-WIDTH      = 101
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN 
       tName:HIDDEN IN FRAME fMain           = TRUE.

ASSIGN 
       tStatus:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX tYear IN FRAME fMain
   ALIGN-L                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Modify Agent Activity */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Modify Agent Activity */
DO:
  /* This event will close the window and terminate the procedure.  */
  {lib/confirm-close.i "'Agent Activity'"}
   
  APPLY "CLOSE":U TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  if self:label = "Save"
   then run SaveActivity in this-procedure.
   else apply "WINDOW-CLOSE" to {&WINDOW-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth1 C-Win
ON LEAVE OF tMonth1 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth10 C-Win
ON LEAVE OF tMonth10 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth11 C-Win
ON LEAVE OF tMonth11 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth12
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth12 C-Win
ON LEAVE OF tMonth12 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth2 C-Win
ON LEAVE OF tMonth2 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth3 C-Win
ON LEAVE OF tMonth3 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth4 C-Win
ON LEAVE OF tMonth4 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth5 C-Win
ON LEAVE OF tMonth5 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth6 C-Win
ON LEAVE OF tMonth6 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth7 C-Win
ON LEAVE OF tMonth7 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth8 C-Win
ON LEAVE OF tMonth8 IN FRAME fMain
DO:
  do with frame {&frame-name}:
    tQuarter3Total = tMonth7:input-value +
                     tMonth8:input-value +
                     tMonth9:input-value
                     .
    calcTotals().
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMonth9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMonth9 C-Win
ON LEAVE OF tMonth9 IN FRAME fMain
DO:
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tQuarter1Total
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tQuarter1Total C-Win
ON LEAVE OF tQuarter1Total IN FRAME fMain /* Quarter 1 */
DO:
  calcQuarter(2).
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tQuarter2Total
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tQuarter2Total C-Win
ON LEAVE OF tQuarter2Total IN FRAME fMain /* Quarter 2 */
DO:
  calcQuarter(2).
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tQuarter3Total
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tQuarter3Total C-Win
ON LEAVE OF tQuarter3Total IN FRAME fMain /* Quarter 3 */
DO:
  calcQuarter(3).
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tQuarter4Total
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tQuarter4Total C-Win
ON LEAVE OF tQuarter4Total IN FRAME fMain /* Quarter 4 */
DO:
  calcQuarter(4).
  calcTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain
DO:
  doModify(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYearTotal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYearTotal C-Win
ON LEAVE OF tYearTotal IN FRAME fMain /* Year */
DO:
  define variable iNumToDivide as integer no-undo initial 4.
  define variable iEnabled as integer no-undo initial 0.
  define variable iModulo as integer no-undo initial 0.
  do std-in = 1 to iNumToDivide:
    std-ha = GetWidgetByName(frame {&frame-name}:handle, "tQuarter" + string(std-in) + "Total").
    if valid-handle(std-ha) and not std-ha:read-only
     then iEnabled = iEnabled + 1.
  end.
  if iEnabled > 0
   then
    do:
      std-de = truncate(self:input-value / iEnabled, 0).
      iModulo = self:input-value modulo iEnabled.
      do std-in = 1 to iEnabled:
        std-ha = GetWidgetByName(frame {&frame-name}:handle, "tQuarter" + string((iNumToDivide - iEnabled) + std-in) + "Total").
        if valid-handle(std-ha)
         then std-ha:screen-value = string(std-de + (if iModulo >= std-in then 1 else 0)).
      end.
      calcQuarter(1).
      calcQuarter(2).
      calcQuarter(3).
      calcQuarter(4).
      calcTotals().
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-icon.i}
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
 
/* set the activity type */
{lib/get-sysprop-list.i &combo=tType &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Type'"}
 
/* set the activity category */
{lib/get-sysprop-list.i &combo=tCategory &appCode="'AMD'" &objAction="'Activity'" &objProperty="'Category'"}

/* fill the year list */
do std-in = 1 to 5:
  /* since the counter is 1 based, subtract 1 from the current year */
  tYear:add-last(string(year(add-interval(today, -1, "year")) + std-in)).
end.
  
/* get the fields */
if valid-handle(hFileDataSrv)
 then run GetActivity in hFileDataSrv (output table activity).
 
{&window-name}:title = "Modify Activity".
for first activity no-lock:
  {&window-name}:title = {&window-name}:title + " for " + activity.name.
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID}
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID}
  
    /* set the status */
    std-ch = "".
    tStatus:screen-value = std-ch.
    
    tYear:delete(1).
    /* there may be a posibility the year isn't in the list so we need to add it */
    std-in = 0.
    for first activity no-lock:
      std-in = activity.year.
    end.
    if std-in > 0 and lookup(string(std-in), tYear:list-items) = 0
     then tYear:add-first(string(std-in)).
     else tYear:screen-value = string(year(today)).
     
    run SetFields in this-procedure.
    doModify(not can-find(first activity where stat = "C")).
  end.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  APPLY "WINDOW-CLOSE":U TO {&window-name}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStateID tName tAgentID tType tCategory tStatus tYear tQuarter1Total 
          tMonth1 tMonth2 tMonth3 tQuarter2Total tMonth4 tMonth5 tMonth6 
          tQuarter3Total tMonth7 tMonth8 tMonth9 tQuarter4Total tMonth10 
          tMonth11 tMonth12 tYearTotal 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-39 RECT-40 tStateID tName tAgentID tType tCategory tStatus tYear 
         tQuarter1Total tMonth1 tMonth2 tMonth3 tQuarter2Total tMonth4 tMonth5 
         tMonth6 tQuarter3Total tMonth7 tMonth8 tMonth9 tQuarter4Total tMonth10 
         tMonth11 tMonth12 tYearTotal bSave 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveActivity C-Win 
PROCEDURE SaveActivity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      /* activity */
      activity.name = (if activity.agentID = "" then tName:input-value else "")
      activity.agentID = (if activity.agentID > "" then tAgentID:input-value else "")
      activity.stateID = tStateID:input-value
      activity.year = tYear:input-value
      activity.category = tCategory:input-value
      activity.type = tType:input-value
      activity.stat = tStatus:input-value
      /* the month data */
      activity.month1 = tMonth1:input-value
      activity.month2 = tMonth2:input-value
      activity.month3 = tMonth3:input-value
      activity.month4 = tMonth4:input-value
      activity.month5 = tMonth5:input-value
      activity.month6 = tMonth6:input-value
      activity.month7 = tMonth7:input-value
      activity.month8 = tMonth8:input-value
      activity.month9 = tMonth9:input-value
      activity.month10 = tMonth10:input-value
      activity.month11 = tMonth11:input-value
      activity.month12 = tMonth12:input-value
      .
    run ModifyActivity in hFileDataSrv (table activity, output std-lo).
    if std-lo
     then
      do:
        doModify(false).
        run SetFields in this-procedure.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetFields C-Win 
PROCEDURE SetFields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    for first activity no-lock:
      if activity.agentID = ""
       then /* target */
        do:
          run AgentComboHide in this-procedure (true).
          assign
            tName:hidden = false
            tAgentID:hidden = true
            tName:screen-value = activity.name
            .
        end.
       else /* agent */
        do:
          run AgentComboHide in this-procedure (false).
          assign
            tName:hidden = true
            tAgentID:hidden = false
            .
          run AgentComboSet in this-procedure (activity.agentID).
        end.
      
      std-ch = "".
      publish "GetSysPropDesc" ("AMD", "Activity", "Status", activity.stat, output std-ch).
      assign
       /* Activity */
        tStateID:screen-value = activity.stateID
        tType:screen-value = activity.type
        tCategory:screen-value = activity.category
        tStatus:screen-value = std-ch
        /* Calculate Totals */
        activity.qtr1 = activity.month1 +
                        activity.month2 +
                        activity.month3
        activity.qtr2 = activity.month4 +
                        activity.month5 +
                        activity.month6
        activity.qtr3 = activity.month7 +
                        activity.month8 +
                        activity.month9
        activity.qtr4 = activity.month10 +
                        activity.month11 +
                        activity.month12
        activity.yrTotal = activity.month1 +
                           activity.month2 +
                           activity.month3 +
                           activity.month4 +
                           activity.month5 +
                           activity.month6 +
                           activity.month7 +
                           activity.month8 +
                           activity.month9 +
                           activity.month10 +
                           activity.month11 +
                           activity.month12
        /* Yearly Details */
        tYear:screen-value = string(activity.year)
        tYearTotal:screen-value = string(activity.yrTotal)
        tQuarter1Total:screen-value = string(activity.qtr1)
        tQuarter2Total:screen-value = string(activity.qtr2)
        tQuarter3Total:screen-value = string(activity.qtr3)
        tQuarter4Total:screen-value = string(activity.qtr4)
        tMonth1:screen-value = string(activity.month1)
        tMonth2:screen-value = string(activity.month2)
        tMonth3:screen-value = string(activity.month3)
        tMonth4:screen-value = string(activity.month4)
        tMonth5:screen-value = string(activity.month5)
        tMonth6:screen-value = string(activity.month6)
        tMonth7:screen-value = string(activity.month7)
        tMonth8:screen-value = string(activity.month8)
        tMonth9:screen-value = string(activity.month9)
        tMonth10:screen-value = string(activity.month10)
        tMonth11:screen-value = string(activity.month11)
        tMonth12:screen-value = string(activity.month12)
        .
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calcQuarter C-Win 
FUNCTION calcQuarter RETURNS LOGICAL
  ( input pQuarter as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable i as integer no-undo.
  define variable hMonth as handle no-undo.
  define variable hQuarter as handle no-undo.
  define variable iQuarterStart as integer no-undo.
  define variable iNumToDivide as integer no-undo initial 3.
  define variable iEnabled as integer no-undo initial 0.
  define variable iAmount as integer no-undo.
  define variable iModulo as integer no-undo.
  
  /* get the cound of haw many months are available */
  iQuarterStart = iNumToDivide * (pQuarter - 1).
  do i = 1 to iNumToDivide:
    hMonth = GetWidgetByName(frame {&frame-name}:handle, "tMonth" + string(i + iQuarterStart)).
    if valid-handle(hMonth) and not hMonth:read-only
     then iEnabled = iEnabled + 1.
  end.
  /* set the months that are enabled only */
  if iEnabled > 0
   then
    do:
      /* get the quarter amount and  */
      hQuarter = GetWidgetByName(frame {&frame-name}:handle, "tQuarter" + string(pQuarter) + "Total").
      if valid-handle(hQuarter)
       then
        do:
          iAmount = truncate(hQuarter:input-value / iEnabled, 0).
          iModulo = hQuarter:input-value modulo iEnabled.
          do i = 1 to iEnabled:
            hMonth = GetWidgetByName(frame {&frame-name}:handle, "tMonth" + string((iNumToDivide - iEnabled) + i + iQuarterStart)).
            if valid-handle(hMonth)
             then hMonth:screen-value = string(iAmount + (if iModulo >= i then 1 else 0)).
          end.
        end.
    end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calcTotals C-Win 
FUNCTION calcTotals RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      tQuarter1Total = tMonth1:input-value +
                       tMonth2:input-value +
                       tMonth3:input-value
      tQuarter2Total = tMonth4:input-value +
                       tMonth5:input-value +
                       tMonth6:input-value
      tQuarter3Total = tMonth7:input-value +
                       tMonth8:input-value +
                       tMonth9:input-value
      tQuarter4Total = tMonth10:input-value +
                       tMonth11:input-value +
                       tMonth12:input-value
      tYearTotal = tMonth1:input-value +
                   tMonth2:input-value +
                   tMonth3:input-value +
                   tMonth4:input-value +
                   tMonth5:input-value +
                   tMonth6:input-value +
                   tMonth7:input-value +
                   tMonth8:input-value +
                   tMonth9:input-value +
                   tMonth10:input-value +
                   tMonth11:input-value +
                   tMonth12:input-value
                   .
    display
      tQuarter1Total
      tQuarter2Total
      tQuarter3Total
      tQuarter4Total
      tYearTotal
      .
  end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doModify C-Win 
FUNCTION doModify RETURNS LOGICAL
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
@description Sets the window to modify or view
------------------------------------------------------------------------------*/
  define variable lIsAdmin as logical no-undo initial false.
  do with frame {&frame-name}:
    if pModify
     then bSave:label = "Save".
     else bSave:label = "Close".
    
    run AgentComboEnable in this-procedure (pModify).
    publish "IsAdmin" (output lIsAdmin).
    assign
      /* Activity */
      tName:read-only = not pModify
      tStateID:sensitive = pModify
      tAgentID:sensitive = pModify
      tType:sensitive = pModify
      tCategory:sensitive = pModify
      /* Yearly Details */
      tYear:sensitive = false
      tMonth1:read-only = (tYear:input-value <= year(today) and 1 <= month(today) and not lIsAdmin) or not pModify
      tMonth2:read-only = (tYear:input-value <= year(today) and 2 <= month(today) and not lIsAdmin) or not pModify
      tMonth3:read-only = (tYear:input-value <= year(today) and 3 <= month(today) and not lIsAdmin) or not pModify
      tMonth4:read-only = (tYear:input-value <= year(today) and 4 <= month(today) and not lIsAdmin) or not pModify
      tMonth5:read-only = (tYear:input-value <= year(today) and 5 <= month(today) and not lIsAdmin) or not pModify
      tMonth6:read-only = (tYear:input-value <= year(today) and 6 <= month(today) and not lIsAdmin) or not pModify
      tMonth7:read-only = (tYear:input-value <= year(today) and 7 <= month(today) and not lIsAdmin) or not pModify
      tMonth8:read-only = (tYear:input-value <= year(today) and 8 <= month(today) and not lIsAdmin) or not pModify
      tMonth9:read-only = (tYear:input-value <= year(today) and 9 <= month(today) and not lIsAdmin) or not pModify
      tMonth10:read-only = (tYear:input-value <= year(today) and 10 <= month(today) and not lIsAdmin) or not pModify
      tMonth11:read-only = (tYear:input-value <= year(today) and 11 <= month(today) and not lIsAdmin) or not pModify
      tMonth12:read-only = (tYear:input-value <= year(today) and 12 <= month(today) and not lIsAdmin) or not pModify
      tQuarter1Total:read-only = (tYear:input-value <= year(today) and 3 <= month(today) and not lIsAdmin) or not pModify
      tQuarter2Total:read-only = (tYear:input-value <= year(today) and 6 <= month(today) and not lIsAdmin) or not pModify
      tQuarter3Total:read-only = (tYear:input-value <= year(today) and 9 <= month(today) and not lIsAdmin) or not pModify
      tQuarter4Total:read-only = (tYear:input-value <= year(today) and 12 <= month(today) and not lIsAdmin) or not pModify
      tYearTotal:read-only = (tYear:input-value <= year(today) and 12 <= month(today) and not lIsAdmin) or not pModify
      .
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

