&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Alert Modify
@description View the details of the alert

@author John Oliver
@version 1.0
@created 2017/10/09
@notes 
---------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/alert.i}
{tt/agent.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fAlert

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-38 tStateID tAgentID tCode tScore ~
tOwner tSeverity tDescription bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tStateID tAgentID tCode tScore tOwner ~
tSeverity tDescription 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tAgentID AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 69 BY 1 NO-UNDO.

DEFINE VARIABLE tOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 42 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "x(10)":U
     LABEL "Severity" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 69 BY 4.05 NO-UNDO.

DEFINE VARIABLE tCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Alert Type" 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1 NO-UNDO.

DEFINE VARIABLE tScore AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "Measure" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 85 BY 7.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fAlert
     tStateID AT ROW 1.48 COL 14.8 COLON-ALIGNED WIDGET-ID 166
     tAgentID AT ROW 2.67 COL 14.8 COLON-ALIGNED WIDGET-ID 84
     tCode AT ROW 4.81 COL 15 COLON-ALIGNED WIDGET-ID 258
     tScore AT ROW 4.81 COL 68 COLON-ALIGNED WIDGET-ID 214
     tOwner AT ROW 6 COL 15 COLON-ALIGNED WIDGET-ID 210
     tSeverity AT ROW 6 COL 68 COLON-ALIGNED WIDGET-ID 248
     tDescription AT ROW 7.19 COL 16.8 NO-LABEL WIDGET-ID 254
     bSave AT ROW 12.43 COL 30 WIDGET-ID 134
     bCancel AT ROW 12.43 COL 47 WIDGET-ID 250
     "Scores" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 3.86 COL 4 WIDGET-ID 206
          FONT 6
     "Description:" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 7.29 COL 5 WIDGET-ID 256
     RECT-38 AT ROW 4.1 COL 3 WIDGET-ID 204
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 88.6 BY 13.05
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "New Alert"
         HEIGHT             = 13.05
         WIDTH              = 88.6
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fAlert
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Alert */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Alert */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fAlert /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&WINDOW-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fAlert /* Save */
DO:
  run SaveAlert in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fAlert /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-icon.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

tAgentID:delimiter = {&msg-dlm}.

/* set the alert owner */
{lib/get-sysprop-list.i &combo=tOwner &appCode="'AMD'" &objAction="'Alert'" &objProperty="'Owner'"}

/* set the alert severity */
{lib/get-sysprop-list.i &combo=tSeverity &appCode="'AMD'" &objAction="'Alert'" &objProperty="'Severity'"}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
   
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=tStateID }
    {lib/get-agent-list.i &combo=tAgentID &state=tStateID}
    /* set the values */
    {lib/set-current-value.i &state=tStateID &agent=tAgentID}
  end.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
@description Closes the window
------------------------------------------------------------------------------*/
  APPLY "WINDOW-CLOSE":U TO {&window-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStateID tAgentID tCode tScore tOwner tSeverity tDescription 
      WITH FRAME fAlert IN WINDOW C-Win.
  ENABLE RECT-38 tStateID tAgentID tCode tScore tOwner tSeverity tDescription 
         bSave bCancel 
      WITH FRAME fAlert IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fAlert}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveAlert C-Win 
PROCEDURE SaveAlert :
/*------------------------------------------------------------------------------
@description Save the alert
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    create alert.
    assign
      /* scores */
      alert.agentID = tAgentID:input-value
      alert.processCode = tCode:input-value
      alert.threshold = 0.0
      alert.score = tScore:input-value
      alert.stat = "O"
      alert.owner = tOwner:input-value
      alert.severity = tSeverity:input-value
      alert.effDate = now
      alert.description = tDescription:input-value
      .
    publish "NewAlert" (table alert, output std-lo).
    if std-lo
     then run CloseWindow in this-procedure.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
@description Shows the window
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

