&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{lib/configttdef.i &exclude-subscriptions=true}
{tt/sysconfig.i &tableAlias=data}

def var tDataChanged as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.f data.v   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData data.f data.v   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwData data
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data by data.f
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data by data.f.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFileType tFile bSelectFile bLoad bSave ~
tField tValue bAdd bDefaults bDelete bEmpty brwData 
&Scoped-Define DISPLAYED-OBJECTS tFileType tFile tField tValue 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openDataQuery C-Win 
FUNCTION openDataQuery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setModified C-Win 
FUNCTION setModified RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Application 
       MENU-ITEM m_About        LABEL "About..."      
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Application  LABEL "Application"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add" 
     SIZE 7.4 BY 1.14 TOOLTIP "Create a new setting".

DEFINE BUTTON bDefaults 
     LABEL "Add Defaults" 
     SIZE 16.4 BY 1.14 TOOLTIP "Create default settings".

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 9.2 BY 1.14 TOOLTIP "Delete the selected setting".

DEFINE BUTTON bEmpty 
     LABEL "Empty" 
     SIZE 9.2 BY 1.14 TOOLTIP "Delete all settings".

DEFINE BUTTON bLoad 
     LABEL "Load" 
     SIZE 8.4 BY 1.14 TOOLTIP "Clear settings and load from file".

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 8.4 BY 1.14 TOOLTIP "Save existing settings to the file".

DEFINE BUTTON bSelectFile 
     LABEL "..." 
     SIZE 4.6 BY 1.14.

DEFINE VARIABLE tField AS CHARACTER FORMAT "X(256)":U 
     LABEL "Add Setting" 
     VIEW-AS FILL-IN 
     SIZE 30.2 BY 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 43.8 BY 1 NO-UNDO.

DEFINE VARIABLE tModified AS CHARACTER FORMAT "X(256)":U INITIAL "Modified" 
     VIEW-AS FILL-IN 
     SIZE 9.6 BY 1
     FGCOLOR 12  NO-UNDO.

DEFINE VARIABLE tValue AS CHARACTER FORMAT "X(256)":U 
     LABEL "Value" 
     VIEW-AS FILL-IN 
     SIZE 50.8 BY 1 NO-UNDO.

DEFINE VARIABLE tFileType AS INTEGER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Settings", 1,
"Options", 2
     SIZE 12 BY 1.62 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 166 BY 16.95.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      data.f label "Setting" format "x(25)"
 data.v label "Value" format "x(255)" width 90
  enable data.f data.v
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 163.8 BY 13.95 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tModified AT ROW 19 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     tFileType AT ROW 1.29 COL 64.8 NO-LABEL WIDGET-ID 36
     tFile AT ROW 1.62 COL 12 COLON-ALIGNED WIDGET-ID 4
     bSelectFile AT ROW 1.52 COL 57.6 WIDGET-ID 26
     bLoad AT ROW 1.57 COL 78 WIDGET-ID 12
     bSave AT ROW 1.57 COL 86.4 WIDGET-ID 2
     tField AT ROW 3.62 COL 16.6 COLON-ALIGNED WIDGET-ID 16
     tValue AT ROW 3.62 COL 58.4 COLON-ALIGNED WIDGET-ID 34
     bAdd AT ROW 3.57 COL 112 WIDGET-ID 18
     bDefaults AT ROW 3.57 COL 120.6 WIDGET-ID 28
     bDelete AT ROW 3.57 COL 138.2 WIDGET-ID 30
     bEmpty AT ROW 3.57 COL 156.8 WIDGET-ID 32
     brwData AT ROW 4.95 COL 2.6 WIDGET-ID 200
     "Working Data" VIEW-AS TEXT
          SIZE 16.2 BY .62 AT ROW 2.95 COL 3.2 WIDGET-ID 10
          FONT 6
     RECT-3 AT ROW 3.24 COL 1.6 WIDGET-ID 20
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 167.2 BY 19.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Configuration File Management"
         HEIGHT             = 19.33
         WIDTH              = 167.2
         MAX-HEIGHT         = 35.14
         MAX-WIDTH          = 256
         VIRTUAL-HEIGHT     = 35.14
         VIRTUAL-WIDTH      = 256
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData bEmpty fMain */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tModified IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tModified:HIDDEN IN FRAME fMain           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data by data.f.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Configuration File Management */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Configuration File Management */
DO:
  /* This event will close the window and terminate the procedure.  */
  if tDataChanged 
   then
    do:
        std-lo = false.
        MESSAGE "All changes will be lost.  Continue?"
         VIEW-AS ALERT-BOX INFO BUTTONS Yes-No update std-lo.
        if not std-lo 
         then return no-apply.
    end.

  apply "CLOSE" to this-procedure.
  publish "ExitApplication".
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add */
DO:
  if tField:screen-value in frame fMain = ""
   then return.
  tDataChanged = true.
  create data.
  data.f = tField:screen-value.
  data.v = tValue:screen-value.
  tField:screen-value = "".
  tValue:screen-value = "".
  openDataQuery().
  setModified().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDefaults
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDefaults C-Win
ON CHOOSE OF bDefaults IN FRAME fMain /* Add Defaults */
DO:
  run createDefaults in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  if not available data 
   then return.
  tDataChanged = true.
  delete data.
  openDataQuery().
  setModified().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEmpty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEmpty C-Win
ON CHOOSE OF bEmpty IN FRAME fMain /* Empty */
DO:
  empty temp-table data.
  close query brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLoad
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLoad C-Win
ON CHOOSE OF bLoad IN FRAME fMain /* Load */
DO:
  if tFile:screen-value in frame fMain = "" 
   then return.

  tDataChanged = false.
  empty temp-table data.

  if tFileType:input-value in frame fMain = 1 
   then /* Settings */
    do:
        std-ha = temp-table setting:handle.
        std-ha:read-xml("file", tFile:screen-value in frame fMain, "empty", "", ?).
      
        for each setting:
         create data.
         buffer-copy setting to data.
        end.
    end.
   else /* Options */
    do:
        std-ha = temp-table config:handle.
        std-ha:read-xml("file", tFile:screen-value in frame fMain, "empty", "", ?).
    
        for each config:
         create data.
         buffer-copy config to data.
        end.
    end.

  openDataQuery().
  setModified().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-LEAVE OF brwData IN FRAME fMain
DO:
  if brwData:current-row-modified 
   then 
    do: tDataChanged = true.
        setModified().
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  if tFile:screen-value in frame fMain = "" 
   then return.
  file-info:file-name = tFile:screen-value.
  if file-info:full-pathname <> ? 
   then
    do:
        MESSAGE tFile:screen-value " exists.  Overwrite?"
         VIEW-AS ALERT-BOX warning BUTTONS YES-NO update std-lo.
        if not std-lo 
         then return.
    end.

  if tFileType:input-value in frame fMain = 1 
   then /* Settings */
    do:
        empty temp-table setting.
        for each data:
         create setting.
         buffer-copy data to setting.
        end.
        std-ha = temp-table setting:handle.
    end.
   else /* Options */
    do:
        empty temp-table config.
        for each data:
         create config.
         buffer-copy data to config.
        end.
      
        std-ha = temp-table config:handle.
    end.
  std-ha:write-xml("file", tFile:screen-value in frame fMain).
  empty temp-table data.
  openDataQuery().
  tDataChanged = false.
  setModified().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSelectFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSelectFile C-Win
ON CHOOSE OF bSelectFile IN FRAME fMain /* ... */
DO:
  system-dialog get-file std-ch
    filters "XML" "*.xml"
    update std-lo.
  if not std-lo 
   then return.
  tFile:screen-value in frame fMain = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createDefaults C-Win 
PROCEDURE createDefaults PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 empty temp-table setting.
 publish "GetSettings" (output table setting).
 for each setting
   where setting.f = "Default":
  tDataChanged = true.
  create data.
  data.f = entry(1, setting.v, "&").
  if num-entries(setting.v, "&") >= 2 
   then data.v = entry(2, setting.v, "&").
 end.
 empty temp-table setting.
 
 openDataQuery().
 setModified().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFileType tFile tField tValue 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tFileType tFile bSelectFile bLoad bSave tField tValue bAdd bDefaults 
         bDelete bEmpty brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openDataQuery C-Win 
FUNCTION openDataQuery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwData.
 open query brwData for each data by data.f by data.v.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setModified C-Win 
FUNCTION setModified RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if tDataChanged 
  then assign
         tModified:visible in frame fMain = true
         tModified:screen-value in frame fMain = "Modified".
  else tModified:screen-value in frame fMain = "".
 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

