&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        :   wbkgnoc.w
    Modification:
    Date          Name      Description
    01/12/2017    AG        Fixed resizing of widgets and data persist
    02/13/2017    AC        Implement local inspect functionality.
  ----------------------------------------------------------------------*/
CREATE WIDGET-POOL.


{lib/std-def.i}
{lib/winshowscrollbars.i}

def input parameter pAuditID as char.
def input parameter pAuditor as char.
def input parameter pAgentName as char.
def input parameter pAgentId as char.
def output parameter pSuccess as logical.
def output parameter pMsg as char.

{lib/questiondef.i &seq=20000}
{lib/questiondef.i &seq=20010}
{lib/questiondef.i &seq=20015}
{lib/questiondef.i &seq=20025}
{lib/questiondef.i &seq=20035}
{lib/questiondef.i &seq=20040}
{lib/questiondef.i &seq=20050}
{lib/questiondef.i &seq=20055}
{lib/questiondef.i &seq=20060}
{lib/questiondef.i &seq=20065}
{lib/questiondef.i &seq=20070}
{lib/questiondef.i &seq=20075}
{lib/questiondef.i &seq=20080}
{lib/questiondef.i &seq=20085}
{lib/questiondef.i &seq=20090}
{lib/questiondef.i &seq=20095}
{lib/questiondef.i &seq=20100}
{lib/questiondef.i &seq=20105}

def var tmodefied as logical.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tComments bSave bRefresh bQueue bCancle 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancle 
     LABEL "Close" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bQueue 
     LABEL "Queue" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Update current data from server".

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save pre-queue changes".

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 137.2 BY 3
     BGCOLOR 15 FONT 18 NO-UNDO.

DEFINE VARIABLE qCA AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 100 BY 2.14
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qClaims AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 101 BY 1.91
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qContract AS INTEGER FORMAT "->>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qDays AS CHARACTER FORMAT "X(256)":U 
     LABEL "Days" 
      VIEW-AS TEXT 
     SIZE 1 BY .24 NO-UNDO.

DEFINE VARIABLE qDrop AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qGap AS INTEGER FORMAT ">>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 TOOLTIP "Enter your best guess; suggested values are 30, 45, 60, 90, etc."
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE qGrossRemit AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE qNetRemit AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE qScore AS CHARACTER FORMAT "X(10)":U 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Enter actual audit score or enter ~"N/A~" if this will be the agent's first audit" NO-UNDO.

DEFINE VARIABLE qY AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
      VIEW-AS TEXT 
     SIZE .8 BY .24 NO-UNDO.

DEFINE VARIABLE qYear AS INTEGER FORMAT ">>>>":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tComments AT ROW 1.29 COL 19.6 NO-LABEL WIDGET-ID 26
     bSave AT ROW 27.48 COL 3.4 WIDGET-ID 32
     bRefresh AT ROW 27.48 COL 11 WIDGET-ID 34
     bQueue AT ROW 27.76 COL 126.2 WIDGET-ID 28
     bCancle AT ROW 27.76 COL 141.8 WIDGET-ID 30
     "Comments:" VIEW-AS TEXT
          SIZE 14.4 BY .62 AT ROW 1.24 COL 3.8 WIDGET-ID 24
          FONT 18
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158 BY 28.52
         CANCEL-BUTTON bCancle WIDGET-ID 100.

DEFINE FRAME fSection
     qGrossRemit AT ROW 1.14 COL 136 COLON-ALIGNED WIDGET-ID 212
     qYear AT ROW 2 COL 117.2 COLON-ALIGNED NO-LABEL WIDGET-ID 232
     qNetRemit AT ROW 2.24 COL 136 COLON-ALIGNED WIDGET-ID 228
     qClaims AT ROW 8.62 COL 11 NO-LABEL WIDGET-ID 214
     qScore AT ROW 11.95 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 218
     qCA AT ROW 16.24 COL 11 NO-LABEL WIDGET-ID 220
     qDrop AT ROW 24.33 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 222
     qContract AT ROW 29.33 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 224
     qGap AT ROW 34.33 COL 117 COLON-ALIGNED NO-LABEL WIDGET-ID 226
     qY AT ROW 1.24 COL 125.6 COLON-ALIGNED WIDGET-ID 238
     qDays AT ROW 34.57 COL 131 COLON-ALIGNED WIDGET-ID 242
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 4.52
         SCROLLABLE SIZE 158 BY 42.86 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Queue Audit"
         HEIGHT             = 28.52
         WIDTH              = 158
         MAX-HEIGHT         = 33.62
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.62
         VIRTUAL-WIDTH      = 273.2
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSection:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR EDITOR tComments IN FRAME fMain
   NO-DISPLAY                                                           */
/* SETTINGS FOR FRAME fSection
                                                                        */
ASSIGN 
       FRAME fSection:HEIGHT           = 22.71
       FRAME fSection:WIDTH            = 158.

/* SETTINGS FOR EDITOR qCA IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR EDITOR qClaims IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qContract IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qDrop IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qGap IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qGrossRemit IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qNetRemit IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qScore IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN qYear IN FRAME fSection
   NO-DISPLAY NO-ENABLE                                                 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Queue Audit */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Queue Audit */
DO:
  if valid-handle(focus) /* maybe not if the window was just closed */
    and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
    and focus:modified /* inconsistently set... */
  then apply "leave" to focus.
  
  /* This event will close the window and terminate the procedure.  */
  if tmodefied = true then
  do: 
    MESSAGE "Audit has been modified. All changes will be discarded."
      VIEW-AS ALERT-BOX question BUTTONS ok-cancel UPDATE lChoice AS LOGICAL.
    if lChoice then
    do:
      run ResetScreen in this-procedure.
      publish "MainWindowSensitive".
      publish "EmptyBackgroundTable".
      APPLY "CLOSE":U TO THIS-PROCEDURE.
      RETURN NO-APPLY.
    end.
    publish "MainWindowSensitive".
  end.
  else
  do:
    run ResetScreen in this-procedure.
    publish "MainWindowSensitive".
    publish "EmptyBackgroundTable".
    APPLY "CLOSE":U TO THIS-PROCEDURE.
    RETURN NO-APPLY.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Queue Audit */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancle C-Win
ON CHOOSE OF bCancle IN FRAME fMain /* Close */
DO:
  APPLY "WINDOW-CLOSE":U TO c-win.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQueue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQueue C-Win
ON CHOOSE OF bQueue IN FRAME fMain /* Queue */
DO:
  session:set-wait-state("general").
  publish "QueueAudit"(input pAuditID,
                       input pAuditor,
                       output pSuccess,
                       output pMsg).
  session:set-wait-state("").
  if not pSuccess then
  do:
    publish "MainWindowSensitive".
    APPLY "CLOSE":U TO THIS-PROCEDURE.
    return .
  end.
  else
  do:
    MESSAGE "Audit successfully queued"
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
    publish "MainWindowSensitive".
    APPLY "CLOSE":U TO THIS-PROCEDURE.
    return .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
publish "GetQueuedAnswers" (input yes,
                            input pAuditID) .
run ReadQueuedAnswers.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
 if valid-handle(focus) /* maybe not if the window was just closed */
    and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
    and focus:modified /* inconsistently set... */
  then apply "leave" to focus.
 publish "SaveQueuedAnswer"(input integer(pAuditID)).
 bSave:sensitive in frame fMain = false. 
 tmodefied = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSection
&Scoped-define SELF-NAME qCA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qCA C-Win
ON LEAVE OF qCA IN FRAME fSection
DO:
  if self:modified then
    run doFillin(20045, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qClaims C-Win
ON LEAVE OF qClaims IN FRAME fSection
DO:
  if self:modified then
   run doFillin (20020, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qContract
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qContract C-Win
ON LEAVE OF qContract IN FRAME fSection
DO:
  if self:modified then
     run doFillin(20070, self:screen-value).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qDrop
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qDrop C-Win
ON LEAVE OF qDrop IN FRAME fSection
DO:
  if self:modified then
    run doFillin(20060, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qGap
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qGap C-Win
ON LEAVE OF qGap IN FRAME fSection
DO:
  if self:modified then
    run doFillin(20090, self:screen-value).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qGrossRemit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qGrossRemit C-Win
ON LEAVE OF qGrossRemit IN FRAME fSection /* Gross */
DO:
  if self:modified then
    run doFillin (20000, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qNetRemit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qNetRemit C-Win
ON LEAVE OF qNetRemit IN FRAME fSection /* Net */
DO:
  if self:modified then
    run doFillin (20005, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qScore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qScore C-Win
ON LEAVE OF qScore IN FRAME fSection
DO:
  if self:modified then
    run doFillin (20035, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME qYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL qYear C-Win
ON LEAVE OF qYear IN FRAME fSection
DO:
  /* Don't do self:modified as we default the value */
  run doFillin (20007, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME tComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tComments C-Win
ON LEAVE OF tComments IN FRAME fMain
DO:
  run doFillin (20110, self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.
bSave:load-image("images/save.bmp").
bSave:load-image-insensitive("images/save-i.bmp").
bRefresh:load-image("images/refresh.bmp").

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
subscribe to "ResetScreen" anywhere.

C-Win:title = C-Win:title + " - " + pAuditId + " - " + pAgentName + " (" + pAgentId + ")" .

on 'value-changed':u anywhere
do:
  bSave:sensitive in frame fMain = true.
  tmodefied = true.
end.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run initializeFrame in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doButton C-Win 
PROCEDURE doButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dobutton.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/docheckbox.i &questionType="Background"}
  tmodefied = true.   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doFillin C-Win 
PROCEDURE doFillin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dofillin.i &questionType="Background" screenvalue}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE tComments bSave bRefresh bQueue bCancle 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY qY qDays 
      WITH FRAME fSection IN WINDOW C-Win.
  ENABLE qY qDays 
      WITH FRAME fSection IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSection}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrame C-Win 
PROCEDURE initializeFrame PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  &scoped-define ex-frameresize true

  &scoped-define ex-u true
  &scoped-define ex-b true
  &scoped-define questionType Background
  
  {lib/question.i &seq=20000 &r="1.72" &ex-y=true &ex-n=true &ex-a=true}
  {lib/question.i &seq=20010 &r="1.72 + 2.5"}
  {lib/question.i &seq=20015 &r="1.72 + 5.0" &ex-a=true &n-label=None &r-row-adj="+ 2.8" &questionHeight=106} 
  {lib/question.i &seq=20035 &r="1.72 + 10.0" &ex-y=true &ex-n=true &ex-a=true}
  {lib/question.i &seq=20040 &r="1.72 + 12.5" &r-row-adj="+ 2.8" &questionHeight=106}
  {lib/question.i &seq=20050 &r="1.72 + 17.5"}
  {lib/question.i &seq=20055 &r="1.72 + 20"}
  {lib/question.i &seq=20060 &r="1.72 + 22.5" &ex-y=true &ex-n=true &ex-a=true}
  {lib/question.i &seq=20065 &r="1.72 + 25"}

  {lib/question.i &seq=20070 &r="1.72 + 27.5" &ex-y=true &ex-n=true &ex-a=true}

  {lib/question.i &seq=20085 &r="1.72 + 30"} 
  {lib/question.i &seq=20090 &r="1.72 + 32.5" &ex-y=true &ex-n=true &ex-a=true}
  {lib/question.i &seq=20095 &r="1.72 + 35"}
  {lib/question.i &seq=20100 &r="1.72 + 37.5" &ex-a=true}
  {lib/question.i &seq=20105 &r="1.72 + 40" &ex-a=true &ex-r=true}

  RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
  run ResetScreen in this-procedure.
  run ReadQueuedAnswers in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReadQueuedAnswers C-Win 
PROCEDURE ReadQueuedAnswers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "GetBackgroundAnswer" (20007, output std-ch).
 qYear:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20000, output std-ch).
 qGrossRemit:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20005, output std-ch).
 qNetRemit:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20020, output std-ch).
 qClaims:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20035, output std-ch).
 qScore:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20045, output std-ch).
 qCA:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20060, output std-ch).
 qDrop:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20070, output std-ch).
 qContract:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20090, output std-ch).
 qGap:screen-value in frame fSection = std-ch.
 publish "GetBackgroundAnswer" (20110, output std-ch).
 tComments:screen-value in frame fMain = std-ch.

 &scoped-define questionType Background
 
 {lib/dispanswer.i &seq=20010}
 {lib/dispanswer.i &seq=20015}
 {lib/dispanswer.i &seq=20040}
 {lib/dispanswer.i &seq=20050}
 {lib/dispanswer.i &seq=20055}
 {lib/dispanswer.i &seq=20065}
 {lib/dispanswer.i &seq=20085}
 {lib/dispanswer.i &seq=20095}
 {lib/dispanswer.i &seq=20100}
 {lib/dispanswer.i &seq=20105}
 
 assign
   qYear:sensitive in frame fSection = true
   qGrossRemit:sensitive = true
   qNetRemit:sensitive = true
   qClaims:sensitive = true
   qScore:sensitive = true
   qCA:sensitive = true
   qDrop:sensitive = true
   qContract:sensitive = true
   qGap:sensitive = true
   tComments:sensitive in frame fMain = true
   .

 frame fMain:sensitive = true.
 frame fSection:sensitive = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResetScreen C-Win 
PROCEDURE ResetScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 assign
   qYear:screen-value in frame fSection = ""
   qGrossRemit:screen-value = ""
   qNetRemit:screen-value = ""
   qClaims:screen-value = ""
   qScore:screen-value = ""
   qCA:screen-value = ""
   qDrop:screen-value = ""
   qContract:screen-value = ""
   qGap:screen-value = ""
   tComments:screen-value in frame fMain = ""
   .

 {lib/unsetquestion.i &seq=20010}
 {lib/unsetquestion.i &seq=20015}
 {lib/unsetquestion.i &seq=20040}
 {lib/unsetquestion.i &seq=20050}
 {lib/unsetquestion.i &seq=20055}
 {lib/unsetquestion.i &seq=20065}
 {lib/unsetquestion.i &seq=20085}
 {lib/unsetquestion.i &seq=20095}
 {lib/unsetquestion.i &seq=20100}
 {lib/unsetquestion.i &seq=20105}

 assign
   qYear:sensitive in frame fSection = false
   qGrossRemit:sensitive = false
   qNetRemit:sensitive = false
   qClaims:sensitive = false
   qScore:sensitive = false
   qCA:sensitive = false
   qDrop:sensitive = false
   qContract:sensitive = false
   qGap:sensitive = false
   tComments:sensitive in frame fMain = false
   .

 frame fMain:sensitive = false.
 frame fSection:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var fMainOrigWidth as int.

 fMainOrigWidth = frame fMain:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.

 tComments:width-pixels = frame fMain:width-pixels - 115.

 if {&window-name}:width-pixels > frame fSection:width-pixels 
  then
   do:
         frame fSection:width-pixels = {&window-name}:width-pixels.
         frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
   end.
  else
   do:
         frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
         frame fSection:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
         RUN ShowScrollBars(FRAME fSection:HANDLE, NO, YES).
   end.

 frame fSection:height-pixels = {&window-name}:height-pixels - 124. /*132 to 76 changed frame fSection AG*/

 bSave:x       =   bSave:x +  0.01 * (frame fMain:width-pixels - fMainOrigWidth) no-error. /*469*/
 bSave:y       =   frame fMain:height-pixels - 42.
 bRefresh:x    =   bRefresh:x + 0.01 * (frame fMain:width-pixels - fMainOrigWidth) no-error. /*469*/
 bRefresh:y    =   frame fMain:height-pixels - 42.
 bQueue:x      =   bQueue:x +  (frame fMain:width-pixels - fMainOrigWidth) no-error. /*469*/
 bQueue:y      =   frame fMain:height-pixels - 36.
 bCancle:x     =   bCancle:x +  (frame fMain:width-pixels - fMainOrigWidth) no-error. /*386*/
 bCancle:y     =   frame fMain:height-pixels - 36.
 
 {lib/resizequestion.i &seq=20000}
 {lib/resizequestion.i &seq=20010}
 {lib/resizequestion.i &seq=20015}
 {lib/resizequestion.i &seq=20035}
 {lib/resizequestion.i &seq=20040}
 {lib/resizequestion.i &seq=20050}
 {lib/resizequestion.i &seq=20055}
 {lib/resizequestion.i &seq=20060}
 {lib/resizequestion.i &seq=20065}
 {lib/resizequestion.i &seq=20070}
 {lib/resizequestion.i &seq=20085}
 {lib/resizequestion.i &seq=20090}
 {lib/resizequestion.i &seq=20095}
 {lib/resizequestion.i &seq=20100}
 {lib/resizequestion.i &seq=20105}
 assign
   qGap:x        = frame fSection:width-pixels  - 200
   qScore:x      = frame fSection:width-pixels  - 200
   qDrop:x       = frame fSection:width-pixels  - 200
   qContract:x   = frame fSection:width-pixels  - 200
   qGrossRemit:x = frame fSection:width-pixels  - 175
   qYear:x       = frame fSection:width-pixels  - 275
   qY:x          = frame fsection:width-pixels  - 275
   qNetRemit:x   = frame fSection:width-pixels  - 175
   qY:side-label-handle:x          =  frame fSection:width-pixels - 270
   qGrossRemit:side-label-handle:x =  frame fSection:width-pixels - 205
   qDays:side-label-handle:x       =  frame fSection:width-pixels - 165
   qNetRemit:side-label-handle:x   =  frame fSection:width-pixels - 195.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

