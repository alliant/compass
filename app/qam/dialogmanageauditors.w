&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pQarID as integer no-undo.

/* Local Variable Definitions ---                                       */
define variable lAdd as logical no-undo initial false.

{tt/qarauditor.i &tableAlias="data"}
{tt/auditor.i}

{lib/std-def.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fAuditor
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData data.uid data.role data.dateCreated   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH data no-lock where isPrimary = false INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH data no-lock where isPrimary = false INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwData data
&Scoped-define FIRST-TABLE-IN-QUERY-brwData data


/* Definitions for FRAME fAuditor                                       */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fAuditor ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 bAdd bDelete tPrimary brwData tUser ~
tUserText tRole bSave 
&Scoped-Define DISPLAYED-OBJECTS tPrimary tUser tUserText tRole 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doAdd C-Win 
FUNCTION doAdd RETURNS LOGICAL
  ( input pCancel as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a new Auditor".

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the Auditor".

DEFINE BUTTON bSave 
     LABEL "Close" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "User" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 73 BY 1 NO-UNDO.

DEFINE VARIABLE tPrimary AS CHARACTER FORMAT "X(256)":U 
     LABEL "Primary Auditor" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE tRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS FILL-IN 
     SIZE 73 BY 1 NO-UNDO.

DEFINE VARIABLE tUserText AS CHARACTER FORMAT "X(256)":U 
     LABEL "User" 
     VIEW-AS FILL-IN 
     SIZE 73 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 3.43.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData NO-LOCK DISPLAY
      data.uid label "User" width 27 format "x(100)"
data.role label "Role" width 35 format "x(500)"
data.dateCreated label "Date Added" width 15 format "99/99/9999"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 84 BY 8.57 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fAuditor
     bAdd AT ROW 1.29 COL 77 WIDGET-ID 8
     bDelete AT ROW 1.29 COL 82.4 WIDGET-ID 10
     tPrimary AT ROW 1.38 COL 16 COLON-ALIGNED WIDGET-ID 2
     brwData AT ROW 2.57 COL 3 WIDGET-ID 200
     tUser AT ROW 12.43 COL 10 COLON-ALIGNED WIDGET-ID 14
     tUserText AT ROW 12.43 COL 10 COLON-ALIGNED WIDGET-ID 18
     tRole AT ROW 13.62 COL 10 COLON-ALIGNED WIDGET-ID 16
     bSave AT ROW 15.29 COL 37 WIDGET-ID 12
     "Details" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 11.38 COL 4 WIDGET-ID 6
     RECT-1 AT ROW 11.62 COL 3 WIDGET-ID 4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 87.8 BY 15.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Manage Auditors"
         HEIGHT             = 15.86
         WIDTH              = 87.8
         MAX-HEIGHT         = 18.33
         MAX-WIDTH          = 87.8
         VIRTUAL-HEIGHT     = 18.33
         VIRTUAL-WIDTH      = 87.8
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fAuditor
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData tPrimary fAuditor */
ASSIGN 
       tPrimary:READ-ONLY IN FRAME fAuditor        = TRUE.

ASSIGN 
       tRole:READ-ONLY IN FRAME fAuditor        = TRUE.

ASSIGN 
       tUserText:READ-ONLY IN FRAME fAuditor        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data no-lock where isPrimary = false INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Manage Auditors */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Manage Auditors */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fAuditor /* Add */
DO:
  doAdd(lAdd).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fAuditor /* Delete */
DO:
  if available data
   then run DeleteAuditor in this-procedure (data.uid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fAuditor
DO:
  {lib/brw-rowDisplay.i}
  std-ch = "".
  publish "GetSysUserName" (data.uid, output std-ch).
  data.uid:screen-value in browse {&browse-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fAuditor
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fAuditor
DO:
  if not available data
   then return.
   
  /* show the user */
  std-ch = "".
  publish "GetSysUserName" (data.uid, output std-ch).
  tUserText:screen-value = std-ch.
  /* show the role */
  tRole:screen-value = data.role.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fAuditor /* Close */
DO:
  if self:label = "Save"
   then run SaveAuditor in this-procedure.
   else apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}                   
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

subscribe to "RefreshQarAuditors" anywhere run-procedure "RefreshAuditors".

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bAdd:load-image("images/s-add.bmp").
bAdd:load-image-insensitive("images/s-add-i.bmp").
bDelete:load-image("images/s-delete.bmp").
bDelete:load-image-insensitive("images/s-delete-i.bmp").

run RefreshAuditors in this-procedure.
{&window-name}:title = "Manage Auditors for Audit " + string(pQarID).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  for first data no-lock
      where data.isPrimary = true:
    
    std-ch = "".
    publish "GetSysUserName" (data.uid, output std-ch).
    tPrimary:screen-value in frame {&frame-name} = std-ch.
  end.
  
  std-lo = false.
  publish "IsQarActive" (pQarID, output std-lo).
  if not std-lo
   then
    assign
      bAdd:sensitive = false
      bDelete:sensitive = false
      .
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteAuditor C-Win 
PROCEDURE DeleteAuditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pUser as character no-undo.
  publish "DeleteQARAuditor" (pQarID, pUser).
  run RefreshAuditors in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tPrimary tUser tUserText tRole 
      WITH FRAME fAuditor IN WINDOW C-Win.
  ENABLE RECT-1 bAdd bDelete tPrimary brwData tUser tUserText tRole bSave 
      WITH FRAME fAuditor IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fAuditor}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshAuditors C-Win 
PROCEDURE RefreshAuditors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table data.
  publish "GetQarAuditors" (pQarID, output table data).
  for first data no-lock
      where data.isPrimary = true:
    
    std-ch = "".
    publish "GetSysUserName" (data.uid, output std-ch).
    tPrimary:screen-value in frame {&frame-name} = std-ch.
  end.
  {&OPEN-BROWSERS-IN-QUERY-fAuditor}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveAuditor C-Win 
PROCEDURE SaveAuditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pUser as character no-undo.
  define variable pRole as character no-undo.
  do with frame {&frame-name}:
    assign
      pUser = tUser:screen-value
      pRole = tRole:screen-value
      .
  end.
  publish "NewQARAuditor" (pQarID, pUser, pRole).
  doAdd(lAdd).
  run RefreshAuditors in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &pre-by-clause="'where isPrimary = false' +" }
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doAdd C-Win 
FUNCTION doAdd RETURNS LOGICAL
  ( input pCancel as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer auditor for auditor.

  do with frame {&frame-name}:
    /* get the auditors */
    tUser:list-item-pairs = ",".
    tUser:delete(1).
    publish "GetAuditors"(output table auditor).
    for each auditor no-lock:
      if not can-find(first data where uid = auditor.uid)
       then tUser:add-last(auditor.name, auditor.uid).
    end.
    /* make the widgets sensitive */
    assign
      browse {&browse-name}:sensitive = pCancel
      bDelete:sensitive = pCancel
      tUser:sensitive = not pCancel
      tUser:hidden = pCancel
      tUserText:read-only = pCancel
      tUserText:screen-value = ""
      tUserText:hidden = not pCancel
      tRole:read-only = pCancel
      tRole:screen-value = ""
      lAdd = not pCancel
      .
    /* change the buttons */
    if not pCancel
     then
      do:
        bAdd:load-image("images/s-cancel.bmp").
        bAdd:load-image-insensitive("images/s-cancel-i.bmp").
        bSave:label = "Save".
      end.
     else
      do:
        bAdd:load-image("images/s-add.bmp").
        bAdd:load-image-insensitive("images/s-add-i.bmp").
        bSave:label = "Close".
      end.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

