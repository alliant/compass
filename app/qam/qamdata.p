&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* qamdata.p
  This procedure is the "data model" for Quality Assurance Mgmt (QAM).
  It is responsible for saving, loading, and managing all requests for data
  elements.  It is the data master.  There are no visual components within
  this procedure.  It is not responsible for making sure there is no data
  loss.  In other words, a controller is responsible to make sure the
  appropriate methods are called on this data model to save its state.
  @Modified  07/15/2021 SA  Modification in version from l to m
             02/02/2022 SD  Task #91131: Added new parameters in "GetQuestionFindings" procedure
                            to fetch data according to them. 
             02/07/2022 SA  Task#:90943 refresh issue on QAM main screen
             03/30/2022 SA  Task #86699 Modified to update linking of agent and org.
             09/13/2022 SC  Task #97039 Added lib/perioddatasrv.i to populate 'From' and 'To' parameters.
             11/28/2022 SA  Task#:99694 Enhancement in qar
             03/17/2023 K.R Task #103305 Enhacement in qam
             08/11/2023 AG  Task#:106419 Modified to show agent legal name instead of short name.
             11/20/2024 SC  Task #116844 Add last completed audit note while creating/scheduling an audit
  ----------------------------------------------------------------------*/

{lib/std-def.i}
{lib/add-delimiter.i}

{tt/agentbyyear.i &tableAlias="Agents"}
{tt/auditor.i}
{tt/scoreanalysis.i} 
{tt/syscode.i}
{tt/qaraudit.i &tableAlias="tempaudit"}
{tt/qar.i &tableAlias="qaraudit"}
{tt/qar.i &tableAlias="qarsummary"}

{tt/qaraudit.i &tableAlias="allaudit"}
{tt/qaraudit.i &tableAlias="updatedAudit"}
{tt/qaraudit.i &tableAlias="qaragent"}
{tt/qaraudit.i &tableAlias="audit"}
{tt/qaraudit.i &tableAlias="qarresults"}
{tt/qaraudit.i &tableAlias="allplannedaudit"}

{tt/qarauditor.i}
{tt/qarauditor.i &tableAlias="tempauditor"}

{tt/qarfinding.i}
{tt/qarfinding.i &tableAlias="findingsummary"}
{tt/qarfinding.i &tableAlias="findingresults"}
{tt/qarfinding.i &tableAlias="questionfindings"}

{tt/qarbestpractice.i &tableAlias="bpsummary"}
{tt/qarbestpractice.i &tableAlias="bpresults"}

{tt/bppareto.i}
{tt/findingpareto.i}
{tt/severitychanges.i}

{tt/bestpracticereport.i &tableAlias="qarbp"} 
{tt/findingreport.i &tableAlias="ttFindingReport"}

{tt/qaraction.i &tableAlias="qaraction"}
{tt/qarnote.i &tableAlias="qarsnote"}
{tt/qaractionnote.i &tableAlias="qarnotegot"}
{tt/qaractionnote.i &tableAlias="qarnote"}

{tt/qardbstate.i}
{tt/qardbstate.i &tableAlias="dbstateresults"}
{tt/qardbauditor.i}
{tt/qardbauditor.i &tableAlias="dbauditorresults"}

{tt/qarbkgquestion.i}
{tt/scheduleQARdata.i}

{tt/organization.i}
{tt/organization.i &tableAlias=tOrganization}

{lib/perioddatasrv.i &period="'Accounting'"}

def dataset activeAuditData
 for audit, 
     bkgQuestion.

def temp-table qarfinding
 field qarID as char
 field agentID as char
 field name as char
 field state as char
 field auditStartDate as date
 field auditor as char
 field questionID as char
 field type as char
 .
def temp-table agentbyyear like Agents.
def temp-table openQars
 field qarid as int
 field hInstance as handle.

def var hServer as handle.
def var pYear as char.
def var activeUID as char no-undo.
def var pAuditID as char no-undo.
def var tFile as char no-undo.
def var NewAuditId as char.
def var NewAuditAgent as char.
def var NewAuditReviewDate as char.
def var NewAuditAuditor as char.
def var lModifyOrganization  as log init false no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-emptyAll) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD emptyAll Procedure 
FUNCTION emptyAll RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-emptyCurrent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD emptyCurrent Procedure 
FUNCTION emptyCurrent RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-loadDefaults) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD loadDefaults Procedure 
FUNCTION loadDefaults RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15.48
         WIDTH              = 72.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

{lib/agentdatasrv.i}
{lib/statedatasrv.i}
{lib/syspropdatasrv.i &sysprop="'SYS'"}

loadDefaults().
run qamserver.p persistent set hServer.
publish "GetSearchYear" (output std-ch).

subscribe to "loadServerAudits" anywhere.
subscribe to "newaudit" anywhere.
subscribe to "PreQueueAudit" anywhere.
subscribe to "QueueAudit" anywhere.
subscribe to "SetBackgroundAnswer" anywhere.
subscribe to "SaveQueuedAnswer" anywhere.
subscribe to "GetQueuedAnswers" anywhere.
subscribe to "GetBackgroundAnswer" anywhere.
subscribe to "EmptyBackgroundTable" anywhere.
subscribe to "DequeueAudit" anywhere.
subscribe to "ReassignAudit" anywhere.
subscribe to "RefreshData" anywhere.
subscribe to "CancelAudit" anywhere.
subscribe to "RestoreAudit" anywhere.
subscribe to "RescheduleAudit" anywhere.
subscribe to "OpenMaintainQar" anywhere.
subscribe to "checkOpenQar" anywhere.

subscribe to "SearchAudits" anywhere.
subscribe to "GetSearchQars" anywhere.

subscribe to "GetAgentandAuiditorcombo" anywhere.
subscribe to "GetBackgroundAttributes" anywhere.
subscribe to "GetQarAudits" anywhere.
subscribe to "GetQarNotes" anywhere.

subscribe to "QarByAgent" anywhere.
subscribe to "GetFindingData" anywhere.
subscribe to "GetDashboardData" anywhere.
subscribe to "GetFindingParetoData" anywhere.
subscribe to "GetAgentbyYearData" anywhere.
subscribe to "GetBestPracticeData" anywhere.
subscribe to "GetBPParetoData" anywhere.
subscribe to "GetSeveritychangesData" anywhere.
subscribe to "GetScoreAnalysis" anywhere.
subscribe to "GetScheduleAgents" anywhere.
subscribe to "GetReasonCode" anywhere.
subscribe to "GetQuestionFindings" anywhere.

subscribe to "FocusUpdatedRecord" anywhere.
subscribe to "EditAuditType"  anywhere.
subscribe to "SetScheduledAgents" anywhere.

subscribe to "NewQarAuditor" anywhere.
subscribe to "GetQarAuditors" anywhere.
subscribe to "DeleteQarAuditor" anywhere.

subscribe to "IsQarActive" anywhere.
subscribe to "IsQarPlanned" anywhere.

subscribe to "AuditDetailReport" anywhere.
subscribe to "InternalReviewReport" anywhere.
subscribe to "QuestionnaireReport" anywhere.

subscribe to "getOrganizations" anywhere.
subscribe to "newOrganizationAgent" anywhere.
subscribe to "modifyOrganizationCheck" anywhere.
subscribe to "getOrganizationName" anywhere.

publish "SetSplashStatus".
run loadServerAudits in this-procedure (input std-ch).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-AuditDetailReport) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditDetailReport Procedure 
PROCEDURE AuditDetailReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter pFile as char no-undo.
def output parameter pSuccess as logical no-undo.
define variable tmsg as character   no-undo.
run GetAuditDetailReportService in hServer ( input pAuditID,
                                             output pFile,
                                             output pSuccess,
                                             output tMsg).
if not pSuccess
 then
 do:
   if tMsg = "" then
     tMsg = "Report not saved.".
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
   run util/openfile.p (pFile).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CancelAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CancelAudit Procedure 
PROCEDURE CancelAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define variable tmsg as character   no-undo.

run GetQarCancel in hServer(input pAuditID,
                            input pReason,
                            output pSuccess,
                            output tMsg ).

if not pSuccess
 then
 do:
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
   do:
   for first allaudit exclusive-lock where allaudit.qarId = pAuditID:
     assign
         allaudit.stat = 'X'
         allaudit.auditStartDate = ?
         allaudit.auditFinishDate = ?
         . 
   end.
      
    MESSAGE "Audit Cancelled Successfully."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
 end.
 
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ChangePrimaryQarAuditor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ChangePrimaryQarAuditor Procedure 
PROCEDURE ChangePrimaryQarAuditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define input parameter pUid as character no-undo.
  
  define buffer qarauditor for qarauditor.
  for first qarauditor exclusive-lock
      where qarauditor.qarID = pQarID
        and qarauditor.isPrimary = true:
    
    qarauditor.uid = pUid.
  end.
  publish "RefreshQarAuditors".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-checkOpenQar) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenQar Procedure 
PROCEDURE checkOpenQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as character.
def output parameter qaropen as logical.
def output parameter qarsummaryhandle as handle.
find first openQars
  where openQars.QarId = integer(pAuditID) no-error.
 if available openQars and valid-handle(openQars.hInstance)
  then
 do:
qaropen = yes.
qarsummaryhandle = openQars.hInstance.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteQarAuditor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteQarAuditor Procedure 
PROCEDURE DeleteQarAuditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define input parameter pUid as character no-undo.
  
  define buffer qarauditor for qarauditor.
  for first qarauditor exclusive-lock
      where qarauditor.qarID = pQarID
        and qarauditor.uid = pUid:
        
    run server/deleteqarauditor.p (input pQarID,
                                   input pUid,
                                   output std-lo,
                                   output std-ch
                                   ).
  
    if not std-lo
     then
      do: std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "DeleteQarAuditor failed: " + std-ch view-as alert-box warning.
      end.
     else delete qarauditor.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DequeueAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DequeueAudit Procedure 
PROCEDURE DequeueAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define variable tmsg as character   no-undo.

run GetQarDequeued in hServer(input pAuditID,
                              input pReason,
                              output pSuccess,
                              output tMsg ).

if not pSuccess
 then
 do: 
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
  do:
    for first allaudit no-lock where allaudit.qarId = int(pAuditID):
      allaudit.stat = 'P'.
    end. 
    
    MESSAGE "Audit Dequeued Successfully."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
  end.     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-EditAuditType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EditAuditType Procedure 
PROCEDURE EditAuditType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pAuditType as char no-undo.
def input parameter pErrType as char no-undo.
def input parameter pReason as char no-undo.
def input parameter pAppType as char no-undo.
define output parameter pSuccess as logical .
define variable tmsg as character   no-undo.

run EditAuditTypeService in hserver( input pAuditID,
                                     input pAuditType,
                                     input pErrType,
                                     input pReason,
                                     input pApptype,
                                     output pSuccess,
                                     output tMsg).

if not pSuccess
  then
do:
  MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  return error .
end.
else
 do:
   for first allaudit no-lock where allaudit.qarId = pAuditID:
     assign
         allaudit.auditType = pAuditType 
         allaudit.errType   = if pAuditType = "E" then pErrType else "" 
         .
   end.
      
    MESSAGE "Audit Type Changed ."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-EmptyBackgroundTable) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EmptyBackgroundTable Procedure 
PROCEDURE EmptyBackgroundTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
for each bkgquestion:
  bkgquestion.answer = "" .
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-FocusUpdatedRecord) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FocusUpdatedRecord Procedure 
PROCEDURE FocusUpdatedRecord :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pYear as integer.
def var pAuditID as char.
empty temp-table updatedAudit.
run GetQarsService in hServer(input pYear,
                              output table updatedAudit ).
Publish "GetCurrentValue" ("QARIDUpdated", output pAuditID).
Publish "SetCurrentValue" ("QARIDUpdated", "0").
for first updatedAudit where updatedAudit.qarid = integer(pAuditID):
  find allaudit where allaudit.qarid = integer(pAuditID) no-error.
  if available(allaudit) then
    buffer-copy updatedAudit to allaudit.
  else
  do:
    create allaudit.
    buffer-copy updatedAudit to allaudit.
  end.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetAgentandAuiditorcombo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentandAuiditorcombo Procedure 
PROCEDURE GetAgentandAuiditorcombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter auditorpair as char.
def output parameter agentpair as char.
def var states as char.

publish "GetAuditors"(output table auditor).
publish "GetSearchStates" (output std-ch).

for each auditor:
  auditorpair  = auditorpair + "|" + string(auditor.name) + "|" + string(auditor.UID) .
end.
auditorpair = trim(auditorpair , " |" ).

publish "GetAgents" (output table agent).

do std-in = 1 to num-entries(std-ch):
  states = entry(std-in + 1 , std-ch, ",").
  std-in = std-in + 1.
  for each agent where agent.stateid = states:
    agentpair  = agentpair + "|" + string(agent.name) + " (" + string(agent.agentID) + ")" +  "|" + string(agent.agentID) .
  end.
end.

agentpair = trim(agentpair , " |" ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetAgentbyYearData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentbyYearData Procedure 
PROCEDURE GetAgentbyYearData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def output parameter table for agentbyyear.

def var pSuccess as logical no-undo.
def var pMsg as char no-undo.

run GetAgentByYearService in hserver (input piYear,
                                     output table agentbyyear,
                                     output pSuccess,
                                     output pMsg ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetallauditTable) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetallauditTable Procedure 
PROCEDURE GetallauditTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for allaudit.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBackgroundAnswer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBackgroundAnswer Procedure 
PROCEDURE GetBackgroundAnswer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pQuestionSeq as int.
def output parameter pAnswer as char.
def buffer bkgQuestion for bkgQuestion.
find bkgQuestion 
  where (bkgQuestion.seq = pQuestionSeq) no-error.
if not available bkgQuestion then return.
  pAnswer = replace(bkgQuestion.answer, "&br;", chr(10)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBackgroundAttributes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBackgroundAttributes Procedure 
PROCEDURE GetBackgroundAttributes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pQuestionSeq as int.
def output parameter pQuestionID as char.
def output parameter pDescription as char.
def output parameter pPriority as int init 1.
def output parameter pCanChangePriority as logical init false.
def output parameter pWarningAnswer as char.
def output parameter pWarningMsg as char.
def output parameter pWarningTip as char.

def buffer bkgQuestion for bkgQuestion.

find bkgQuestion 
  where bkgQuestion.seq = pQuestionSeq no-error.
if available bkgQuestion 
 then assign
        pQuestionID = bkgQuestion.questionID
        pDescription = bkgQuestion.description
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBestPracticeData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBestPracticeData Procedure 
PROCEDURE GetBestPracticeData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as char no-undo.
def input parameter pyear as char.
def input parameter pAgentID as char.
def input parameter pAuditor as char.
def input parameter pState as char no-undo.
def output parameter table for qarbp.
def var  pSuccess as logical.
def var  pMsg as char.

empty temp-table bpresults.
empty temp-table qarbp.

run GetBestPracticeService in hServer (input pAuditID,
                                       input pyear,
                                       input pAgentID,
                                       input pAuditor,
                                       input pState,
                                       output table qarbp, 
                                       output pSuccess,
                                       output pMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBPParetoData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBPParetoData Procedure 
PROCEDURE GetBPParetoData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pyear as char.
def input parameter pAgentID as char.
def input parameter pAuditor as char.
def input parameter pState as char no-undo.
def output parameter table for bppareto.
def var pSuccess as logical.
def var pMsg as char.

empty temp-table bppareto.

run GetBPParetoService in hServer (input pyear,
                                   input pAgentID,
                                   input pAuditor,
                                   input pState,
                                   output table bppareto, 
                                   output pSuccess,
                                   output pMsg).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetDashboardData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDashboardData Procedure 
PROCEDURE GetDashboardData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pcYear as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcAuditor as char no-undo.
def input parameter pcState as char no-undo.
def output parameter table for dbstate .

def var pSuccess as logical no-undo.
def var pMsg as char no-undo.
 empty temp-table qarfinding.
 empty temp-table finding.

run GetStateDashboardService in hserver (input pcYear,
                                  input pcAgentID,
                                  input pcAuditor,
                                  input pcState,
                                  output table dbstate,
                                  output pSuccess,
                                  output pMsg ).
for each dbstate:
  dbstate.numFindings = dbstate.numMinor 
                         + dbstate.numInter
                         + dbstate.numMajor.
  dbstate.avgScore = round(dbstate.totalScore / dbstate.numAudits, 1).
  std-in = std-in + 1.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetFindingData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetFindingData Procedure 
PROCEDURE GetFindingData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def input parameter piAuditID as char no-undo.
def input parameter pcState as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcUid as char no-undo.
def output parameter table for ttFindingReport .

def var pSuccess as logical no-undo.
def var pMsg as char no-undo.
empty temp-table ttFindingReport.

run GetFindingService in hserver (input piYear,
                                  input piAuditID,
                                  input pcState,
                                  input pcAgentID,
                                  input pcUid,
                                  output table ttFindingReport,
                                  output pSuccess,
                                  output pMsg ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetFindingParetoData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetFindingParetoData Procedure 
PROCEDURE GetFindingParetoData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def input parameter piFindingType as int no-undo.
def input parameter pcState as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcUid as char no-undo.
def output parameter table for findingpareto .

def var pSuccess as logical no-undo.
def var pMsg as char no-undo.

run GetfFindingParetoService in hserver (input piYear,
                                         input piFindingType,
                                         input pcState,
                                         input pcAgentID,
                                         input pcUid,
                                         output table findingpareto,
                                         output pSuccess,
                                         output pMsg ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getOrganizationName) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getOrganizationName Procedure 
PROCEDURE getOrganizationName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcOrgID    as character no-undo.
  define output parameter opcName     as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.

  define buffer organization for organization.
  
  /* Client Server Call */
  if not can-find(first organization)
   then
    do:
      run server/getorganizations.p (input "ALL",  /* organizationStatus */
                                     output table organization,
                                     output std-lo,
                                     output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Loadorganizations failed: " std-ch
                 view-as alert-box error.
    end.


  find first organization where organization.orgID = ipcOrgID no-error.
  if available organization
   then
    do:
      opcName = organization.name.
      oplSuccess   = true.
    end.    
  else
   oplSuccess = false.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getOrganizations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getOrganizations Procedure 
PROCEDURE getOrganizations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for organization.
 
  /* Buffer */
  define buffer organization for organization.

  /* Client Server Call */
  if not can-find(first organization) or lModifyOrganization
   then
    do:
      run server/getorganizations.p ("ALL",  /* organizationStatus */
                              output table organization,
                              output std-lo,
                              output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Loadorganizations failed: " std-ch
                 view-as alert-box error.
                 
      IF lModifyOrganization 
       THEN
        lModifyOrganization = FALSE.
    end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarAuditors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarAuditors Procedure 
PROCEDURE GetQarAuditors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define output parameter table for tempauditor.
  
  define buffer qarauditor for qarauditor.
  
  empty temp-table tempauditor.
  for each qarauditor no-lock
     where qarauditor.qarID = pQarID:
     
    create tempauditor.
    buffer-copy qarauditor to tempauditor.
  end.
  if not can-find(first tempauditor)
   then
    do:
      run server/getqarauditors.p (input pQarID,
                                   output table tempauditor,
                                   output std-lo,
                                   output std-ch).
      
      if not std-lo
       then 
        do: std-lo = false.
            publish "GetAppDebug" (output std-lo).
            if std-lo 
             then message "GetQarAuditors failed: " + std-ch view-as alert-box warning.
        end.
       else
        for each tempauditor no-lock:
          create qarauditor.
          buffer-copy tempauditor to qarauditor.
        end.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarAudits) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarAudits Procedure 
PROCEDURE GetQarAudits :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pYear as integer no-undo.
  define input  parameter pQarID as integer no-undo.
  define input  parameter pStateID as character no-undo.
  define input  parameter pAgentID as character no-undo.
  define output parameter table for tempaudit.

  empty temp-table tempaudit.
  run server/getaudits.p (input pYear ,     /* year */
                          input pQarID,     /* QAR ID */
                          input pStateID,   /* state */
                          input pAgentID,   /* agent */
                          input "",         /* user */
                          input "",         /* status */
                          input "",         /* audit types */
                          output table tempaudit,
                          output std-lo,
                          output std-ch
                          ).
                           
  if not std-lo
   then
    do:
      message std-ch view-as alert-box error buttons ok.
      return error.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarNotes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarNotes Procedure 
PROCEDURE GetQarNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter table for qarsnote.

run GetQarNotesService in hServer (input pAuditId,
                                   output table qarsnote).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQuestionFindings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQuestionFindings Procedure 
PROCEDURE GetQuestionFindings :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter ipiquestionseq as int no-undo.
def input parameter ipcyear        as char NO-UNDO.
def input parameter ipcauditor     as char NO-UNDO.
def input parameter ipiFindingType  as int  no-undo.
def input parameter ipcState        as char no-undo.
def input parameter ipcAgentID      as char no-undo.
def output parameter table for questionfindings .

def var pSuccess as logical no-undo.
def var pMsg as char no-undo.

run GetfQuestionFindings in hserver (input ipiquestionseq,
                                     input ipcyear,
                                     input ipcauditor,
                                     input ipiFindingType,
                                     input ipcState,
                                     input ipcAgentID,
                                     output table questionfindings,
                                     output pSuccess,
                                     output pMsg ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQueuedAnswers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQueuedAnswers Procedure 
PROCEDURE GetQueuedAnswers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pRefresh as logical initial false.
def input parameter pAuditid as char.
def var tdir as char.
def var tFile as char.
tDir = os-getenv("appdata") + "\Alliant\qarn\active".
tFile = tDir + "\" + trim(string(pAuditid)) + ".qarn".
if search(tFile) = ? or pRefresh = true then
  run PreQueueAudit in this-procedure (input pAuditid ).
else
  std-lo = dataset activeAuditData:read-xml("FILE", tFile, "EMPTY", "", ?, ?) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetReasonCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReasonCode Procedure 
PROCEDURE GetReasonCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pType as character no-undo.
  define output parameter pList as character.

  define buffer syscode for syscode.

  if not can-find(first syscode where codeType = pType)
   then run getReasonCodeService in hServer (input pType,
                                             output table syscode).
                 
  pList = "".
  for each syscode no-lock
     where syscode.codeType = pType:
  
    pList = addDelimiter(pList, ",") + syscode.description + "," + syscode.code.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetScheduleAgents) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetScheduleAgents Procedure 
PROCEDURE GetScheduleAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 def input parameter piYear as int no-undo.
 def input parameter pcaudittype as char no-undo.
 def output parameter table for scheduleQARdata .
 
 def var pSuccess as logical no-undo.
 def var pMsg as char no-undo.

 run GetScheduleAgentsService in hserver (input piYear,
                                         input pcaudittype,
                                         output table scheduleQARdata ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetScoreAnalysis) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetScoreAnalysis Procedure 
PROCEDURE GetScoreAnalysis :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter piYear as int no-undo.
 def output parameter table for scoreanalysis .

 def var pSuccess as logical no-undo.
 def var pMsg as char no-undo.

 run GetScoreanalysisService in hserver (input piYear,
                                         output table scoreanalysis ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetSearchQars) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSearchQars Procedure 
PROCEDURE GetSearchQars :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for qarresults.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetSeveritychangesData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSeveritychangesData Procedure 
PROCEDURE GetSeveritychangesData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def input parameter piAuditID as char no-undo.
def input parameter pcState as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcUid as char no-undo.
def output parameter table for severitychanges .

def var pSuccess as logical no-undo.
def var pMsg as char no-undo.
empty temp-table severitychanges.

run GetSeveritychangesService in hserver (input piYear,
                                  input piAuditID,
                                  input pcState,
                                  input pcAgentID,
                                  input pcUid,
                                  output table severitychanges,
                                  output pSuccess,
                                  output pMsg ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-InternalReviewReport) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InternalReviewReport Procedure 
PROCEDURE InternalReviewReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter pFile as char no-undo.
def output parameter pSuccess as logical no-undo.
define variable tmsg as character   no-undo.
run GetInternalReviewReportService in hServer ( input pAuditID,
                                                output pFile,
                                                output pSuccess,
                                                output tMsg).
if not pSuccess
 then
 do:
   if tMsg = "" then
     tMsg = "Report not saved.".
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
   run util/openfile.p (pFile).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IsQarActive) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsQarActive Procedure 
PROCEDURE IsQarActive :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define output parameter pIsActive as logical no-undo.
  define buffer allaudit for allaudit.
  
  for first allaudit no-lock
      where allaudit.qarID = pQarID:
    
    pIsActive = not (allaudit.stat = "C" or allaudit.stat = "X").
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IsQarPlanned) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsQarPlanned Procedure 
PROCEDURE IsQarPlanned :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define output parameter pIsActive as logical no-undo.
  define buffer allaudit for allaudit.
  
  for first allaudit no-lock
      where allaudit.qarID = pQarID:
    
    pIsActive = (allaudit.stat = "P").
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-loadServerAudits) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loadServerAudits Procedure 
PROCEDURE loadServerAudits :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter syear as integer no-undo.

empty temp-table allaudit.

run GetQarsService in hServer(input sYear,
                              output table allaudit ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-modifyOrganizationCheck) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyOrganizationCheck Procedure 
PROCEDURE modifyOrganizationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter plModifyOrganization as logical no-undo.

lModifyOrganization = plModifyOrganization.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAudit Procedure 
PROCEDURE NewAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pYear as char.
def input parameter pAgent as char.
def input parameter pReviewDate as char.
def input parameter pAuditor as char.
def input parameter pAuditType as char.
def input parameter pErrType as char.
def input parameter pResncode as char.
def output parameter pAuditID as char.
def output parameter pLastAuditNote as char no-undo.
def output parameter pSuccess as logical.
def output parameter pMsg as char.

run NewAuditService in hServer( input pYear,
                                input pAgent,
                                input pReviewDate,
                                input pAuditor,
                                input pAuditType,
                                input pErrType,
                                input pResncode,
                                output pAuditID,
                                output pLastAuditNote,
                                output pSuccess,
                                output pMsg).
                               
assign
    NewAuditId = pAuditID
    NewAuditAgent = pAgent
    NewAuditReviewDate = pReviewDate
    NewAuditAuditor = pAuditor.
    
if pSuccess
 then
  do:
    create allaudit.
    assign
        allaudit.qarID =  integer(pAuditID)
        allaudit.stat = "P"
        allaudit.agentId = pAgent
        allaudit.auditType = pAuditType
        allaudit.errType  = pErrType
        allaudit.version = (if pAuditType = "U" then "U1" else "M")
        allaudit.score = 0
        allaudit.grade = 0
        allaudit.schedstartdate = date(pReviewDate)
        allaudit.schedFinishDate = date(pReviewDate)
        allaudit.auditYear = integer(pYear)
        allaudit.contactName = ""
        allaudit.contactPhone = ""
        allaudit.contactFax = ""
        allaudit.contactEmail = ""
        allaudit.deliveredTo = ""
        allaudit.comments = pLastAuditNote
        allaudit.createDate = now
        allaudit.uid = pAuditor
        .

    find agent where agent.agentID = pAgent no-error.
    if avail agent 
     then
      assign
          allaudit.name  = agent.legalName 
          allaudit.stateID = agent.stateId 
          allaudit.city = agent.city 
          allaudit.state = agent.state.
          
    publish "GetAuditors"(output table auditor).
    for first auditor no-lock where auditor.uid = pAuditor:
      allaudit.auditor = auditor.name .
    end.
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newOrganizationAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newOrganizationAgent Procedure 
PROCEDURE newOrganizationAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tOrganization.
  
  for first tOrganization:
    find first organization 
      where organization.OrgId = torganization.orgID no-error.
    if not available organization
     then
      do:
        create organization.
        buffer-copy torganization to organization.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewQarAuditor) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewQarAuditor Procedure 
PROCEDURE NewQarAuditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.
  define input parameter pUid as character no-undo.
  define input parameter pRole as character no-undo.
  
  run server/newqarauditor.p (input pQarID,
                              input pUid,
                              input pRole,
                              output std-lo,
                              output std-ch
                              ).
  
  if not std-lo
   then
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "NewQarAuditor failed: " + std-ch view-as alert-box warning.
    end.
   else
    do:
      std-ch = "".
      publish "GetCredentialsID" (output std-ch).
      create qarauditor.
      assign
        qarauditor.qarID = pQarID
        qarauditor.uid = pUid
        qarauditor.role = pRole
        qarauditor.createdBy = std-ch
        qarauditor.dateCreated = now
        qarauditor.isPrimary = false
        .
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-OpenMaintainQar) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainQar Procedure 
PROCEDURE OpenMaintainQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter pAuditId as int.
  def input parameter pWindow as handle.
  
  find openQars where openQars.QarID = pAuditId no-error.
  if not available openQars 
   then
    do:
      publish "SetCurrentValue" ("QARIDUpdated", pAuditId).
      create openQars.
      openQars.QarID = pAuditId.
      openQars.hInstance = pWindow.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PreQueueAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PreQueueAudit Procedure 
PROCEDURE PreQueueAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as char.
def var audityear as integer no-undo.
def var eorequired as log no-undo.
def var maxCoverage as decimal no-undo.
def var lastauditscore as int no-undo.
def var opencorrectact as char no-undo.
def var ytdnetpremium as decimal no-undo.
def var ytdgrosspremium as decimal no-undo.
def var avggap as decimal no-undo.
def var isminamtremitted as logical no-undo.
def var twentypercentdrop as char no-undo.
def var causetwentypercentdrop as char no-undo.
def var opencorrquestionsid as char no-undo.
def var claimprocess as char no-undo.
def var pSuccess as logical no-undo.
def var pMsg as char no-undo.

run PreQueueAuditService in hServer(input integer(pAuditId),
                                    output audityear,
                                    output eorequired,
                                    output maxCoverage,
                                    output lastauditscore,  
                                    output opencorrectact, 
                                    output ytdnetpremium ,
                                    output ytdgrosspremium, 
                                    output avggap, 
                                    output isminamtremitted, 
                                    output twentypercentdrop, 
                                    output causetwentypercentdrop,
                                    output opencorrquestionsid, 
                                    output claimprocess, 
                                    output pSuccess,
                                    output pMsg).
if not pSuccess then return.
find bkgQuestion where bkgQuestion.seq = 20000 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(ytdgrosspremium) .
find bkgQuestion where bkgQuestion.seq = 20005 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(ytdnetpremium).
find bkgQuestion where bkgQuestion.seq = 20007 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(audityear).
find bkgQuestion where bkgQuestion.seq = 20010 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(isminamtremitted, "y/n").
find bkgQuestion where bkgQuestion.seq = 20020 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = claimprocess.
find bkgQuestion where bkgQuestion.seq = 20035 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = (if lastauditscore = ? then "N/A" else string(lastauditscore)).
find bkgQuestion where bkgQuestion.seq = 20040 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = opencorrectact.
find bkgQuestion where bkgQuestion.seq = 20045 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = opencorrquestionsid.
find bkgQuestion where bkgQuestion.seq = 20055 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = twentypercentdrop.
find bkgQuestion where bkgQuestion.seq = 20060 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = causetwentypercentdrop.
find bkgQuestion where bkgQuestion.seq = 20070 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(maxCoverage).
find bkgQuestion where bkgQuestion.seq = 20090 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(avggap).
find bkgQuestion where bkgQuestion.seq = 20105 no-error.
if available bkgQuestion then
  assign bkgQuestion.answer = string(eorequired, "y/n").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-QarByAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QarByAgent Procedure 
PROCEDURE QarByAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pyear as int.
def input parameter pAgentID as char.
def input parameter pAuditor as char.
def output parameter  table for qaragent.
def var pSuccess as logical.

run GetQarByAgentService in hserver (input pyear,
                                     input pAgentID,
                                     input pAuditor,
                                     output table qaragent,
                                     output pSuccess
                                      ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-QuestionnaireReport) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QuestionnaireReport Procedure 
PROCEDURE QuestionnaireReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter pFile as char no-undo.
def output parameter pSuccess as logical no-undo.
define variable tmsg as character   no-undo.
run GetQuestionnaireReportService in hServer ( input pAuditID,
                                                output pFile,
                                                output pSuccess,
                                                output tMsg).
if not pSuccess
 then
 do:
   if tMsg = "" then
     tMsg = "Report not saved.".
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
   run util/openfile.p (pFile).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-QueueAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QueueAudit Procedure 
PROCEDURE QueueAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as char.
def input parameter pAuditor as char.
def output parameter pSuccess as logical.
def output parameter pMsg as char.

find allaudit where allaudit.qarid  = integer(pAuditID) no-error.
if available allaudit then
  buffer-copy allaudit to audit.
for each bkgQuestion:
  assign bkgQuestion.qarid = integer(pAuditID).
end.
run QueueAuditService in hServer (input pAuditId,
                                  input pAuditor,
                                  input table audit,
                                  input table bkgQuestion,
                                  output pSuccess,
                                  output pMsg).
                                  
if pSuccess
 then
  for first allaudit no-lock where allaudit.qarId = int(pAuditID):
    allaudit.stat = 'Q'.
  end.                                  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ReassignAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReassignAudit Procedure 
PROCEDURE ReassignAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pNewAuditor as char no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical.
define variable tmsg as character   no-undo.

  run GetQarReassigned in hServer(input pAuditID,
                                  input pNewAuditor,
                                  input pReason,
                                  output pSuccess,
                                  output tMsg
                                  ).

  if not pSuccess
   then
    do:
      MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      return error .
    end.
   else
    do:
      publish "GetAuditors"(output table auditor).
      for first allaudit no-lock where allaudit.qarId = pAuditID:
        for first auditor no-lock where auditor.uid = pNewAuditor:
          assign
              allaudit.uid = auditor.uid
              allaudit.auditor = auditor.name 
              .
        end.
      end.
      MESSAGE "Audit Reassigned Successfully." VIEW-AS ALERT-BOX INFO BUTTONS OK.
      run ChangePrimaryQarAuditor in this-procedure (pAuditID, pNewAuditor).
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RefreshData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshData Procedure 
PROCEDURE RefreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAction as logical.
def var currentYear as char no-undo.
publish "GetSearchYear" (output std-ch).
publish "SearchYear" (output currentYear).
currentYear = substring(currentyear,7).
if pAction then
  run FocusUpdatedRecord(integer(std-ch)) .
else
  run loadServerAudits  in this-procedure (input currentYear).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RescheduleAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RescheduleAudit Procedure 
PROCEDURE RescheduleAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter rescheduledate as datetime no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define variable tmsg as character   no-undo.

run RescheduleAuditService in hServer(input pAuditID,
                                      input rescheduledate,
                                      input pReason,
                                      output pSuccess,
                                      output tMsg ).

if not pSuccess
 then
 do:
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
  do:
    for first allaudit no-lock where allaudit.qarId = pAuditID:
      allaudit.auditstartdate = rescheduledate .
    end.
    MESSAGE "Audit Rescheduled Successfully."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.

  end. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RestoreAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RestoreAudit Procedure 
PROCEDURE RestoreAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditId as int no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define variable tmsg as character   no-undo.


run GetQarRestoreAudit in hServer(input pAuditID,
                                  input pReason,
                                  output pSuccess,
                                  output tMsg ).

if not pSuccess
then
 do:
   MESSAGE tMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   return error .
 end.
 else
  do:
    for first allaudit no-lock where allaudit.qarId = int(pAuditID):
      assign
          allaudit.stat = 'P'
          allaudit.auditStartDate = now
          .
    end. 
    
    MESSAGE "Audit Restored Successfully."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
  end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SaveQueuedAnswer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveQueuedAnswer Procedure 
PROCEDURE SaveQueuedAnswer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def var tDir as char no-undo.

for each bkgQuestion:
  assign bkgQuestion.qarid = integer(pAuditID).
end. 

def buffer allaudit for allaudit.
for first allaudit no-lock
   where allaudit.qarID = pAuditID:
  buffer-copy allaudit to audit.
  tDir = os-getenv("appdata") + "\Alliant\qarn\active".
  if search(tDir) = ?
    then os-create-dir value(tDir).
  std-ch = tDir + "\" + trim(string(allaudit.qarID)) + ".qarn".
  std-lo = dataset activeAuditData:write-xml("FILE", std-ch).
  if not std-lo 
    then return.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SearchAudits) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SearchAudits Procedure 
PROCEDURE SearchAudits :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter cStatus  as char.
def input parameter cState   as char.
def input parameter cAuditor as char.
def input parameter cAuditType as char.
def output parameter table for qarresults.

define variable  tStat    as character no-undo.
define variable  tState   as character no-undo.
define variable  tAuditor as character no-undo.
define variable  tAuditType as character no-undo.
empty temp-table qarresults.

assign
  tStat    = if cStatus = "ALL" then "*" else cStatus
  tState   = if cState = "ALL"  then "*" else cState
  tAuditor = if cAuditor = "ALL" then "*" else cAuditor
  tAuditType = if cAuditType = "ALL" then "*" else cAuditType
  .

for each allaudit
  where can-do(tAuditor , allaudit.UID)
    and can-do(tState , allaudit.stateID)
    and can-do(tStat , allaudit.stat)
    and can-do(tAuditType , allaudit.audittype):
    
  create qarresults.
  buffer-copy allaudit to qarresults.
  qarresults.cityState = allaudit.city + ", " + allaudit.state.
  
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetBackgroundAnswer) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetBackgroundAnswer Procedure 
PROCEDURE SetBackgroundAnswer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pQuestionSeq as int.
def input parameter pAnswer as char.
def buffer bkgQuestion for bkgQuestion.
find bkgQuestion
  where bkgQuestion.seq = pQuestionSeq no-error.
if not available bkgQuestion
 then 
return error. 
/* Can happen on leave of field after an audit was closed */

pAnswer = replace(pAnswer, chr(10), "&br;").

if bkgQuestion.answer = pAnswer 
 then return. /* no change */

bkgQuestion.answer = pAnswer.
bkgQuestion.uid = activeUID.
bkgQuestion.dateAnswered = now.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetScheduledAgents) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetScheduledAgents Procedure 
PROCEDURE SetScheduledAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter table for tempaudit.

  run SetScheduleagentsService in hServer (table tempaudit).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-emptyAll) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION emptyAll Procedure 
FUNCTION emptyAll RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 empty temp-table qarsummary. 
 empty temp-table qarresults.
 empty temp-table findingsummary.
 empty temp-table findingresults.

 emptyCurrent().
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-emptyCurrent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION emptyCurrent Procedure 
FUNCTION emptyCurrent RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 empty temp-table qaraudit.
 empty temp-table qaraction.
 empty temp-table qarnote.
 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-loadDefaults) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION loadDefaults Procedure 
FUNCTION loadDefaults RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 tFile = search("data\background.d").
 input from value(tFile).
 repeat:
   create bkgQuestion.
   import bkgQuestion.seq
          bkgQuestion.questionID
          bkgQuestion.description
          .
 end.
 input close.
 for each bkgQuestion
   where bkgQuestion.questionID = "":
  delete bkgQuestion.
 end.

 return true.


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

