&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wdashboardstate.w
   Window of DASHBOARD by STATE across qar records
   3.23.3012
   */

CREATE WIDGET-POOL.

{tt/qardbstate.i}
{tt/state.i}

{lib/std-def.i}
def var tSortBy as char init "period".
def var tSortDesc as logical init false.

{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES dbstate

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData string(string(month(dbstate.period), "z9") + "/" + string(year(dbstate.period), "9999")) @ dbstate.period dbstate.numAudits dbstate.numFindings dbstate.numMajor dbstate.numInter dbstate.numMinor dbstate.numSuggestions dbstate.numRecommendations dbstate.numCorrective dbstate.numOpen dbstate.numInProcess dbstate.numComplete dbstate.numRejected dbstate.minScore dbstate.maxScore dbstate.avgScore   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH dbstate by dbstate.state by dbstate.period
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH dbstate by dbstate.state by dbstate.period.
&Scoped-define TABLES-IN-QUERY-brwData dbstate
&Scoped-define FIRST-TABLE-IN-QUERY-brwData dbstate


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData 
&Scoped-Define DISPLAYED-OBJECTS fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export aggregations to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh aggregations".

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      dbstate SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      string(string(month(dbstate.period), "z9") + "/" + string(year(dbstate.period), "9999")) @ dbstate.period label "Period"
 dbstate.numAudits format "zzz" label "Audits" width 8
 dbstate.numFindings format "z,zzz" column-label "Total!Findings" width 12
 dbstate.numMajor format "z,zzz" column-label "Major!Findings" width 12
 dbstate.numInter format "z,zzz" column-label "Intermediate!Findings" width 12
 dbstate.numMinor format "z,zzz" column-label "Minor!Findings" width 12
 dbstate.numSuggestions format "z,zzz" label "Suggestions"
 dbstate.numRecommendations format "z,zzz" label "Recommendations"
 dbstate.numCorrective format "z,zzz" column-label "Total!Corrective" width 15
 dbstate.numOpen format "z,zzz" column-label "Open!Corrective" width 15
 dbstate.numInProcess format "z,zzz" column-label "In Process!Corrective" width 15
 dbstate.numComplete format "z,zzz" column-label "Completed!Corrective" width 15
 dbstate.numRejected format "z,zzz" column-label "Rejected!Corrective" width 15
 dbstate.minScore format "zzz" column-label "Minimum!Score"
 dbstate.maxScore format "zzz" column-label "Maximum!Score"
 dbstate.avgScore format "zzz" column-label "Average!Score"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 136 BY 13 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.52 COL 32.2 WIDGET-ID 2
     bRefresh AT ROW 1.52 COL 39.6 WIDGET-ID 8
     fState AT ROW 1.91 COL 6.6 COLON-ALIGNED WIDGET-ID 6
     brwData AT ROW 3.62 COL 3 WIDGET-ID 200
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 140 BY 16 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "State Dashboard"
         HEIGHT             = 16
         WIDTH              = 140
         MAX-HEIGHT         = 16.05
         MAX-WIDTH          = 140
         VIRTUAL-HEIGHT     = 16.05
         VIRTUAL-WIDTH      = 140
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fState fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRefresh IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX fState IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH dbstate by dbstate.state by dbstate.period.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* State Dashboard */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* State Dashboard */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* State Dashboard */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "EmptyDashboardStateData".
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run doSelect in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
ON 'mouse-select-down':U OF dbstate.period in browse brwData
/*  or 'mouse-select-down' of dbstate.numAudits in browse brwData          */
/*  or 'mouse-select-down' of dbstate.numMajor in browse brwData           */
/*  or 'mouse-select-down' of dbstate.numSuggestions in browse brwData     */
/*  or 'mouse-select-down' of dbstate.numRecommendations in browse brwData */
/*  or 'mouse-select-down' of dbstate.numCorrective in browse brwData      */
DO:
 run sortData in this-procedure (self:name).
 RETURN.
END.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

{lib/get-state-list.i &combo=fState &addAll=true}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

run getData in this-procedure.
run windowResized in this-procedure.

assign
  fState:sensitive in frame fMain = true
  bExport:sensitive in frame fMain = true
  bRefresh:sensitive in frame fMain = true
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSelect C-Win 
PROCEDURE doSelect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tEndDate as date no-undo.
                                 
 if not available dbstate 
  then return.

 if month(dbstate.period) = 12 
  then tEndDate = date(12, 31, year(dbstate.period)).
  else tEndDate = date(month(dbstate.period) + 1, 1, year(dbstate.period)) - 1.
 
 publish "AutoSearch" (if dbstate.state = "all" then "" else dbstate.state,
                       dbstate.period,
                       tEndDate).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer x-dbstate for dbstate.
 def var exportFilename as char no-undo.
 def var doSave as logical no-undo.

 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There are no records to export."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).

 system-dialog get-file exportFilename
  filters "CSV Files" "*.csv"
  initial-dir std-ch
  ask-overwrite
  create-test-file
  default-extension ".csv"
  use-filename
  save-as
 update doSave.

 if not doSave 
  then return.

 std-in = 0.
 output to value(exportFilename).
 export delimiter ","
   "State"
   "Period Start"
   "Num Audits"
   "Num Major"
   "Num Inter"
   "Num Minor"
   "Num Suggestions"
   "Num Recommendations"
   "Num Corrective"
   "Num CA Open"
   "Num CA InProcess" 
   "Num CA Complete"
   "Num CA Rejected"
   "Min Score"
   "Max Score"
   "Avg Score"
   .

 for each x-dbstate:
  export delimiter "," 
    x-dbstate.state
    x-dbstate.period
    x-dbstate.numAudits
    x-dbstate.numMajor
    x-dbstate.numInter
    x-dbstate.numMinor
    x-dbstate.numSuggestions
    x-dbstate.numRecommendations
    x-dbstate.numCorrective
    x-dbstate.numOpen
    x-dbstate.numInProcess
    x-dbstate.numComplete
    x-dbstate.numRejected
    x-dbstate.minScore
    x-dbstate.maxScore
    round(x-dbstate.totalScore / x-dbstate.numAudits, 1)
    .
  std-in = std-in + 1.
 end.
 output close.

 RUN ShellExecuteA in this-procedure (0,
                             "open",
                             exportFilename,
                             "",
                             "",
                             1,
                             OUTPUT std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query brwData.
 empty temp-table dbstate.

 session:set-wait-state("general").
 publish "GetDashboardStateData" (input fState:input-value in frame fMain,
                                  output table dbstate,
                                  output std-lo).
 session:set-wait-state("").

 tSortDesc = not tSortDesc.
 run sortData (tSortBy).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pName as char.

 def var hQuery as handle.
 def var tWhere as char.

 hQuery = query brwData:handle.
 hQuery:query-close().
 browse brwData:clear-sort-arrows().

 if pName = tSortBy 
  then tSortDesc = not tSortDesc.


 case pName:
  when "numAudits" then tWhere = "preselect each dbstate by dbstate.numAudits " 
                                    + (if tSortDesc then "descending" else "")
                                    + " by dbstate.state by dbstate.period ".
  when "numFindings" then tWhere = "preselect each dbstate by dbstate.numFindings " 
                                    + (if tSortDesc then "descending" else "")
                                    + " by dbstate.state by dbstate.period ".
 otherwise 
   do: tWhere = "preselect each dbstate by dbstate.period " 
                    + (if tSortDesc then "descending" else "")
                    + " by dbstate.state ".
       pName = "period".
   end.
 end case.
 
 do std-in = 1 to browse brwData:num-columns:
  std-ha = browse brwData:get-browse-column(std-in).
  if std-ha:name = pName 
   then browse brwData:set-sort-arrow(std-in, not tSortDesc).
 end.

 hQuery:query-prepare(tWhere).
 hQuery:query-open().
 tSortBy = pName.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwData:width-pixels = frame fmain:width-pixels - 20.
 brwData:height-pixels = frame fMain:height-pixels - browse {&browse-name}:y - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

