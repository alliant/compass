&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* wqam.w
   main Window for Quality Assurance Management
   D.Sinclair
   3.8.2012
 */
 
CREATE WIDGET-POOL.

{src/adm2/widgetprto.i}

{lib/std-def.i}

/* Temp-table definitions */
{tt/qar.i &tableAlias="results" } /* set of all audit */
/* {tt/qaraction.i &tableAlias="resultsaction"} /* set of all actions for all audits */   */
/* {tt/qaractionnote.i &tableAlias="resultsnote"} /* set of all notes for all actions */  */

{tt/qar.i} /* only contains a single current audit */
{tt/qaraction.i} /* contains all actions for the current audit */
{tt/qaractionnote.i} /* contains all notes for all actions of the current audit */
{tt/qaraudit.i &tableAlias="allaudit"}
{tt/state.i}
/*
{tt/qaraction.i &tableAlias="actionUpd"} /* contains a single updated action */
{tt/qaractionnote.i &tableAlias="noteUpd"} /* contains all updated notes for the updated action */
 */

def temp-table openActions
 field actionID as int
 field hInstance as handle.



def var minSearchFrameWidth as int no-undo.

def var activeUser as char no-undo.
def var activeAudit as char no-undo.
def var activeRowid as rowid no-undo.

def var auditSortBy as char no-undo init "name".
def var auditSortDesc as logical no-undo init false.
def var actionSortBy as char no-undo init "actionType".
def var actionSortDesc as logical no-undo init false.

def var hFindings as handle no-undo.
def var hPareto as handle no-undo.

def var hBestPractices as handle no-undo.
def var hBpPareto as handle no-undo.
def var hDashboardState as handle no-undo.

{lib/winlaunch.i}

def input parameter table for allaudit.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fSearch
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES action allaudit

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction entry(index("CRS", action.actionType), "Corrective,Recommendation,Suggestion") @ action.actionType entry(index("OICR", action.stat), "Open,In Process,Complete,Rejected") @ action.stat action.dueDate action.followupDate action.questionID action.actionType   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction FOR EACH action by action.actionID
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} FOR EACH action by action.actionID.
&Scoped-define TABLES-IN-QUERY-brwAction action
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction action


/* Definitions for BROWSE brwQAR                                        */
&Scoped-define FIELDS-IN-QUERY-brwQAR /* results.name */ /* results.state */ /* results.auditDate */ /* results.score */ /* results.section6score */ /* /* results.nextDueDate */ */ /* results.auditor */ /* results.numCorrective */ /* results.numRecommendations */ /* results.numSuggestions */ /* results.qarID */ /* results.agentID */ allaudit.qarId allaudit.auditYear allaudit.agentID allaudit.name allaudit.state allaudit.score allaudit.auditScore allaudit.auditDate allaudit.stat   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQAR   
&Scoped-define SELF-NAME brwQAR
&Scoped-define QUERY-STRING-brwQAR FOR EACH allaudit
&Scoped-define OPEN-QUERY-brwQAR OPEN QUERY {&SELF-NAME} FOR EACH allaudit.
&Scoped-define TABLES-IN-QUERY-brwQAR allaudit
&Scoped-define FIRST-TABLE-IN-QUERY-brwQAR allaudit


/* Definitions for FRAME fSearch                                        */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fLatestOnly fName fAuditor fReviewStart ~
fReviewEnd fQAR fAgentID fStates fAll fNone fOpen fInProcess fDueIn ~
fCompleted fRejected fMaxTotalScore fMaxSection6Score bSearch brwQAR ~
brwAction 
&Scoped-Define DISPLAYED-OBJECTS fLatestOnly fName fAuditor fReviewStart ~
fReviewEnd fQAR fAgentID fStates fAll fNone fOpen fInProcess fDueIn ~
fCompleted fRejected fMaxTotalScore fMaxSection6Score txtResultCount ~
txtResultTime tAuditID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearSearchConditions wWin 
FUNCTION clearSearchConditions RETURNS logical
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD loadTestData wWin 
FUNCTION loadTestData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD reloadAudit wWin 
FUNCTION reloadAudit RETURNS LOGICAL PRIVATE
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD repositionToCurrent wWin 
FUNCTION repositionToCurrent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setSearchWidgetState wWin 
FUNCTION setSearchWidgetState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_File 
       MENU-ITEM m_Refresh      LABEL "Refresh"       
       RULE
       MENU-ITEM m_Configuration LABEL "Configure..."  
       MENU-ITEM m_About        LABEL "About..."       ACCELERATOR "ALT-A"
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"           ACCELERATOR "ALT-X".

DEFINE SUB-MENU m_Review 
       MENU-ITEM m_New          LABEL "New"           
       MENU-ITEM m_Queue        LABEL "Queue"         .

DEFINE SUB-MENU m_Search 
       MENU-ITEM m_Do_Search    LABEL "Do Search Query" ACCELERATOR "ALT-S"
       MENU-ITEM m_Go_to_Current LABEL "Go to Current Audit" ACCELERATOR "ALT-G"
       RULE
       MENU-ITEM m_Filters_Clear LABEL "Clear Filters" 
       MENU-ITEM m_Filters_View LABEL "View Default Filters"
       MENU-ITEM m_Filters_Save LABEL "Save as Default Filters".

DEFINE SUB-MENU m_View 
       MENU-ITEM m_Findings     LABEL "Findings"       ACCELERATOR "ALT-F"
       MENU-ITEM m_Findings_Pareto LABEL "Findings Pareto"
       RULE
       MENU-ITEM m_Best_Practices LABEL "Best Practices" ACCELERATOR "ALT-B"
       MENU-ITEM m_Best_Practices_Pareto LABEL "Best Practices Pareto"
       RULE
       MENU-ITEM m_State_Aggregation LABEL "State Dashboard".

DEFINE SUB-MENU m_Report 
       MENU-ITEM m_Export_Search_Results LABEL "Export Search Results..." ACCELERATOR "ALT-E"
       RULE
       MENU-ITEM m_Current_Action_Status LABEL "Current Action Status Report"
       MENU-ITEM m_Action_Status_Report LABEL "Actions Status Report" ACCELERATOR "ALT-R".

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_File         LABEL "File"          
       SUB-MENU  m_Review       LABEL "Review"        
       SUB-MENU  m_Search       LABEL "Search"        
       SUB-MENU  m_View         LABEL "View"          
       SUB-MENU  m_Report       LABEL "Report"        .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch 
     LABEL "Search" 
     SIZE 12 BY 1.24 TOOLTIP "Do Search (Alt-S)".

DEFINE BUTTON bSearchResultsOutdated  NO-FOCUS FLAT-BUTTON
     LABEL "..." 
     SIZE 4.8 BY 1.14 TOOLTIP "Search results may no longer match filters".

DEFINE VARIABLE fAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Identifier" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "ID" NO-UNDO.

DEFINE VARIABLE fAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewer" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "Quality Assurance Reviewer" NO-UNDO.

DEFINE VARIABLE fDueIn AS INTEGER FORMAT "->9":U INITIAL 30 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1 TOOLTIP "For Open or In Process actions, the number of days the follow-up is due within" NO-UNDO.

DEFINE VARIABLE fMaxSection6Score AS INTEGER FORMAT ">>":U INITIAL 10 
     LABEL "Section 6" 
     VIEW-AS FILL-IN 
     SIZE 5.2 BY 1 TOOLTIP "Enter the cutoff Section 6 (ERR) score (to show low results)" NO-UNDO.

DEFINE VARIABLE fMaxTotalScore AS INTEGER FORMAT ">>>":U INITIAL 150 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 5.2 BY 1 TOOLTIP "Enter the cutoff total score (to show low results)" NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "Agent Name" NO-UNDO.

DEFINE VARIABLE fQAR AS CHARACTER FORMAT "X(256)":U 
     LABEL "Audit" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "Quality Assurance Review ID" NO-UNDO.

DEFINE VARIABLE fReviewEnd AS DATE FORMAT "99/99/99":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Latest Review Date (? to blank for all)" NO-UNDO.

DEFINE VARIABLE fReviewStart AS DATE FORMAT "99/99/99":U 
     LABEL "Reviewed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Earliest Review Date (? to blank for all)" NO-UNDO.

DEFINE VARIABLE tAuditID AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 70 BY .62 NO-UNDO.

DEFINE VARIABLE txtResultCount AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE txtResultTime AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 28.8 BY 6.67.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 18.8 BY 3.57.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128.8 BY 7.76.

DEFINE VARIABLE fStates AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "One","1" 
     SIZE 30 BY 6.67 NO-UNDO.

DEFINE VARIABLE fAll AS LOGICAL INITIAL no 
     LABEL "All" 
     VIEW-AS TOGGLE-BOX
     SIZE 7 BY .81 TOOLTIP "Select to include all QAR records regardless of action status or existence" NO-UNDO.

DEFINE VARIABLE fCompleted AS LOGICAL INITIAL no 
     LABEL "Completed" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE fInProcess AS LOGICAL INITIAL no 
     LABEL "In Process" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE fLatestOnly AS LOGICAL INITIAL no 
     LABEL "Latest Only" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 TOOLTIP "Only return the latest audit for an agent" NO-UNDO.

DEFINE VARIABLE fNone AS LOGICAL INITIAL no 
     LABEL "None" 
     VIEW-AS TOGGLE-BOX
     SIZE 9.4 BY .81 NO-UNDO.

DEFINE VARIABLE fOpen AS LOGICAL INITIAL no 
     LABEL "Open" 
     VIEW-AS TOGGLE-BOX
     SIZE 9 BY .81 NO-UNDO.

DEFINE VARIABLE fRejected AS LOGICAL INITIAL no 
     LABEL "Rejected" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      action SCROLLING.

DEFINE QUERY brwQAR FOR 
      allaudit SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction wWin _FREEFORM
  QUERY brwAction DISPLAY
      entry(index("CRS", action.actionType), "Corrective,Recommendation,Suggestion") @ action.actionType label "Type" format "x(18)"
 entry(index("OICR", action.stat), "Open,In Process,Complete,Rejected") @ action.stat label "Status" format "x(10)"  
 action.dueDate label "Due Date" format "99/99/9999"
 action.followupDate label "Followup" format "99/99/9999"
 action.questionID label "Question" format "x(15)"
 action.actionType label "Action" format "x(255)" width 50
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 128 BY 5.71 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.

DEFINE BROWSE brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQAR wWin _FREEFORM
  QUERY brwQAR DISPLAY
      /*  results.name label "Name" format "x(40)"                                  */
/*  results.state label "State" format "x(2)"                                 */
/*  results.auditDate label "Review Date" format "99/99/9999"                 */
/*  results.score label "Score" format "zz9 " width 8                         */
/*  results.section6score label "ERR" format "z9 " width 8                    */
/* /*  results.nextDueDate label "Next Due Date" format "99/99/9999" */       */
/*  results.auditor label "Reviewed By" format "x(40)"                        */
/*  results.numCorrective label "Corrective" format "zzz " width 11           */
/*  results.numRecommendations label "Recommendations" format "zzz " width 19 */
/*  results.numSuggestions label "Suggestions" format "zzz " width 13         */
/*  results.qarID label "Audit" format "x(20)"                                */
/*  results.agentID label "ID" format "x(10)"                                 */
allaudit.qarId
allaudit.auditYear
allaudit.agentID
allaudit.name
allaudit.state
allaudit.score
allaudit.auditScore
allaudit.auditDate
allaudit.stat
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 128 BY 10.91 ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fSearch
     fLatestOnly AT ROW 8.19 COL 14.6 WIDGET-ID 140
     bSearchResultsOutdated AT ROW 9.38 COL 9.8 WIDGET-ID 48 NO-TAB-STOP 
     fName AT ROW 2.29 COL 12.2 COLON-ALIGNED WIDGET-ID 2
     fAuditor AT ROW 3.48 COL 12.2 COLON-ALIGNED WIDGET-ID 36
     fReviewStart AT ROW 4.67 COL 12.2 COLON-ALIGNED WIDGET-ID 6
     fReviewEnd AT ROW 4.67 COL 28.4 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     fQAR AT ROW 5.91 COL 12.2 COLON-ALIGNED WIDGET-ID 4
     fAgentID AT ROW 7.05 COL 12.4 COLON-ALIGNED WIDGET-ID 44
     fStates AT ROW 2.19 COL 50.4 NO-LABEL WIDGET-ID 40
     fAll AT ROW 2.81 COL 83.2 WIDGET-ID 110
     fNone AT ROW 3.52 COL 83.2 WIDGET-ID 134
     fOpen AT ROW 4.86 COL 83.4 WIDGET-ID 10
     fInProcess AT ROW 5.57 COL 83.4 WIDGET-ID 12
     fDueIn AT ROW 5.48 COL 98 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     fCompleted AT ROW 7.05 COL 83.6 WIDGET-ID 14
     fRejected AT ROW 7.76 COL 83.6 WIDGET-ID 16
     fMaxTotalScore AT ROW 2.91 COL 121.2 COLON-ALIGNED WIDGET-ID 120
     fMaxSection6Score AT ROW 4.1 COL 121.2 COLON-ALIGNED WIDGET-ID 118
     bSearch AT ROW 7.62 COL 117.2 WIDGET-ID 18
     brwQAR AT ROW 10.52 COL 3 WIDGET-ID 400
     brwAction AT ROW 22.38 COL 3.2 WIDGET-ID 500
     txtResultCount AT ROW 9.71 COL 12.8 COLON-ALIGNED NO-LABEL WIDGET-ID 28
     txtResultTime AT ROW 9.71 COL 27.2 COLON-ALIGNED NO-LABEL WIDGET-ID 30
     tAuditID AT ROW 21.71 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 132
     "Actions" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 21.71 COL 2 WIDGET-ID 130
          FONT 6
     "Filters" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.19 COL 4.2 WIDGET-ID 24
          FONT 6
     "State:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 2.24 COL 44.4 WIDGET-ID 42
     "Days" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 5.67 COL 104 WIDGET-ID 112
     "Corrective Actions" VIEW-AS TEXT
          SIZE 21.2 BY .62 TOOLTIP "There must be at least one CA in one of the checked statuses" AT ROW 1.95 COL 82.4 WIDGET-ID 46
          FONT 6
     "Score" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.95 COL 112.4 WIDGET-ID 124
          FONT 6
     "Audits" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 9.81 COL 2 WIDGET-ID 22
          FONT 6
     "Due In" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 4.76 COL 98.6 WIDGET-ID 136
     RECT-2 AT ROW 2.19 COL 81.4 WIDGET-ID 116
     RECT-39 AT ROW 2.19 COL 111.2 WIDGET-ID 122
     RECT-40 AT ROW 1.48 COL 2.4 WIDGET-ID 138
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 1 ROW 1
         SIZE 132 BY 27.57 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 27.57
         WIDTH              = 132
         MAX-HEIGHT         = 34.19
         MAX-WIDTH          = 256
         VIRTUAL-HEIGHT     = 34.19
         VIRTUAL-WIDTH      = 256
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fSearch
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwQAR bSearch fSearch */
/* BROWSE-TAB brwAction brwQAR fSearch */
ASSIGN 
       brwAction:COLUMN-RESIZABLE IN FRAME fSearch       = TRUE
       brwAction:COLUMN-MOVABLE IN FRAME fSearch         = TRUE.

ASSIGN 
       brwQAR:COLUMN-RESIZABLE IN FRAME fSearch       = TRUE
       brwQAR:COLUMN-MOVABLE IN FRAME fSearch         = TRUE.

/* SETTINGS FOR BUTTON bSearchResultsOutdated IN FRAME fSearch
   NO-ENABLE                                                            */
ASSIGN 
       bSearchResultsOutdated:HIDDEN IN FRAME fSearch           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fSearch
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-39 IN FRAME fSearch
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-40 IN FRAME fSearch
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAuditID IN FRAME fSearch
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN txtResultCount IN FRAME fSearch
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN txtResultTime IN FRAME fSearch
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH action by action.actionID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQAR
/* Query rebuild information for BROWSE brwQAR
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH allaudit.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwQAR */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fSearch
/* Query rebuild information for FRAME fSearch
     _Query            is NOT OPENED
*/  /* FRAME fSearch */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  apply "CLOSE" to this-procedure.
  publish "ExitApplication".
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&Scoped-define SELF-NAME brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction wWin
ON DEFAULT-ACTION OF brwAction IN FRAME fSearch
DO:
  run doSelectAction in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQAR
&Scoped-define SELF-NAME brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON DEFAULT-ACTION OF brwQAR IN FRAME fSearch
DO:
  run doSelect in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch wWin
ON CHOOSE OF bSearch IN FRAME fSearch /* Search */
DO:
  run doSearch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgentID wWin
ON LEAVE OF fAgentID IN FRAME fSearch /* Identifier */
DO:
  if self:modified 
   then bSearchResultsOutdated:visible in frame fSearch 
           = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAll wWin
ON VALUE-CHANGED OF fAll IN FRAME fSearch /* All */
DO:
 bSearchResultsOutdated:visible in frame fSearch 
   = txtResultCount:screen-value in frame fSearch > "".
if self:checked 
 then assign 
        fOpen:checked in frame fSearch = false
        fInProcess:checked in frame fSearch = false
        fCompleted:checked in frame fSearch = false
        fRejected:checked in frame fSearch = false
        fNone:checked in frame fSearch = false
        
/*         fNone:sensitive in frame fSearch = false      */
/*         fOpen:sensitive in frame fSearch = false      */
/*         fInProcess:sensitive in frame fSearch = false */
/*         fCompleted:sensitive in frame fSearch = false */
/*         fRejected:sensitive in frame fSearch = false  */
/*         fDueIn:sensitive in frame fSearch = false     */
        
        .
 else assign 
/*         fOpen:sensitive in frame fSearch = true      */
/*         fInProcess:sensitive in frame fSearch = true */
/*         fDueIn:sensitive in frame fSearch = true     */
/*         fCompleted:sensitive in frame fSearch = true */
/*         fRejected:sensitive in frame fSearch = true  */
/*         fNone:sensitive in frame fSearch = true      */
        .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAuditor wWin
ON LEAVE OF fAuditor IN FRAME fSearch /* Reviewer */
DO:
 if self:modified 
  then bSearchResultsOutdated:visible in frame fSearch 
          = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCompleted
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCompleted wWin
ON VALUE-CHANGED OF fCompleted IN FRAME fSearch /* Completed */
DO:
 bSearchResultsOutdated:visible in frame fSearch 
     = txtResultCount:screen-value in frame fSearch > "".
 if self:checked
  then fAll:checked in frame fSearch = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDueIn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDueIn wWin
ON LEAVE OF fDueIn IN FRAME fSearch
DO:
 if self:modified
  then bSearchResultsOutdated:visible in frame fSearch 
     = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fInProcess
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fInProcess wWin
ON VALUE-CHANGED OF fInProcess IN FRAME fSearch /* In Process */
DO:
 bSearchResultsOutdated:visible in frame fSearch 
     = txtResultCount:screen-value in frame fSearch > "".
 if self:checked
  then fAll:checked in frame fSearch = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fName wWin
ON LEAVE OF fName IN FRAME fSearch /* Name */
DO:
 if self:modified 
  then bSearchResultsOutdated:visible in frame fSearch 
          = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNone wWin
ON VALUE-CHANGED OF fNone IN FRAME fSearch /* None */
DO:
 bSearchResultsOutdated:visible in frame fSearch 
     = txtResultCount:screen-value in frame fSearch > "".
 if self:checked
  then fAll:checked in frame fSearch = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOpen wWin
ON VALUE-CHANGED OF fOpen IN FRAME fSearch /* Open */
DO:
 bSearchResultsOutdated:visible in frame fSearch 
     = txtResultCount:screen-value in frame fSearch > "".
 if self:checked
  then fAll:checked in frame fSearch = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fQAR wWin
ON LEAVE OF fQAR IN FRAME fSearch /* Audit */
DO:
 if self:modified 
  then bSearchResultsOutdated:visible in frame fSearch 
          = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRejected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRejected wWin
ON VALUE-CHANGED OF fRejected IN FRAME fSearch /* Rejected */
DO:
 bSearchResultsOutdated:visible in frame fSearch 
     = txtResultCount:screen-value in frame fSearch > "".
 if self:checked
  then fAll:checked in frame fSearch = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fReviewEnd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fReviewEnd wWin
ON LEAVE OF fReviewEnd IN FRAME fSearch
DO:
 if self:modified 
  then bSearchResultsOutdated:visible in frame fSearch 
          = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fReviewStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fReviewStart wWin
ON LEAVE OF fReviewStart IN FRAME fSearch /* Reviewed */
DO:
 if self:modified 
  then bSearchResultsOutdated:visible in frame fSearch 
          = txtResultCount:screen-value in frame fSearch > "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStates wWin
ON VALUE-CHANGED OF fStates IN FRAME fSearch
DO:
    bSearchResultsOutdated:visible in frame fSearch 
        = txtResultCount:screen-value in frame fSearch > "".
/*     fStateCnt:screen-value in frame fSearch = "("                              */
/*       + trim(string(num-entries(fStates:screen-value in frame fSearch), ">9")) */
/*       + ")".                                                                   */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About wWin
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
 publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Action_Status_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Action_Status_Report wWin
ON CHOOSE OF MENU-ITEM m_Action_Status_Report /* Actions Status Report */
DO:
 run ActionReport in this-procedure (0).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Best_Practices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Best_Practices wWin
ON CHOOSE OF MENU-ITEM m_Best_Practices /* Best Practices */
DO:
 if not valid-handle(hBestPractices) 
  then run wbp.w persistent set hBestPractices.
  else run "ShowWindow" in hBestPractices no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Best_Practices_Pareto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Best_Practices_Pareto wWin
ON CHOOSE OF MENU-ITEM m_Best_Practices_Pareto /* Best Practices Pareto */
DO:
 if not valid-handle(hBpPareto) 
  then run wbppareto.w persistent set hBpPareto.
  else run "ShowWindow" in hBpPareto no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configuration
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configuration wWin
ON CHOOSE OF MENU-ITEM m_Configuration /* Configure... */
DO:
  run ActionConfig in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Current_Action_Status
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Current_Action_Status wWin
ON CHOOSE OF MENU-ITEM m_Current_Action_Status /* Current Action Status Report */
DO:
  if available action 
   then run ActionReport in this-procedure (action.actionID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Do_Search
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Do_Search wWin
ON CHOOSE OF MENU-ITEM m_Do_Search /* Do Search Query */
DO:
 run doSearch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit wWin
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export_Search_Results
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export_Search_Results wWin
ON CHOOSE OF MENU-ITEM m_Export_Search_Results /* Export Search Results... */
DO:
 run ActionExport in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Filters_Clear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Filters_Clear wWin
ON CHOOSE OF MENU-ITEM m_Filters_Clear /* Clear Filters */
DO:
  clearSearchConditions().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Filters_Save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Filters_Save wWin
ON CHOOSE OF MENU-ITEM m_Filters_Save /* Save as Default Filters */
DO:
  run saveSearchConditions in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Filters_View
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Filters_View wWin
ON CHOOSE OF MENU-ITEM m_Filters_View /* View Default Filters */
DO:
  run loadSearchConditions in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Findings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Findings wWin
ON CHOOSE OF MENU-ITEM m_Findings /* Findings */
DO:
  if not valid-handle(hFindings) 
   then run wfinding.w persistent set hFindings.
   else run "ShowWindow" in hFindings no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Findings_Pareto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Findings_Pareto wWin
ON CHOOSE OF MENU-ITEM m_Findings_Pareto /* Findings Pareto */
DO:
 if not valid-handle(hPareto) 
  then run wpareto.w persistent set hPareto.
  else run "ShowWindow" in hPareto no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Go_to_Current
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Go_to_Current wWin
ON CHOOSE OF MENU-ITEM m_Go_to_Current /* Go to Current Audit */
DO:
  repositionToCurrent().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_New wWin
ON CHOOSE OF MENU-ITEM m_New /* New */
DO:
  RUN dialognewaudit.W.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Queue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Queue wWin
ON CHOOSE OF MENU-ITEM m_Queue /* Queue */
DO:
 RUN dialogqueueaudit.w .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Refresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Refresh wWin
ON CHOOSE OF MENU-ITEM m_Refresh /* Refresh */
DO:
  run ActionRefresh in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_State_Aggregation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_State_Aggregation wWin
ON CHOOSE OF MENU-ITEM m_State_Aggregation /* State Dashboard */
DO:
 if not valid-handle(hDashboardState) 
  then run wdashboardstate.w persistent set hDashboardState.
  else run "ShowWindow" in hDashboardState no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ON 'mouse-select-down':U OF results.name in browse brwQar              */
/*  or 'mouse-select-down' of results.state in browse brwQar              */
/*  or 'mouse-select-down' of results.auditDate in browse brwQar          */
/* /*  or 'mouse-select-down' of results.nextDueDate in browse brwQar */  */
/*  or 'mouse-select-down' of results.auditor in browse brwQar            */
/*  or 'mouse-select-down' of results.score in browse brwQar              */
/*  or 'mouse-select-down' of results.section6score in browse brwQar      */
/*  or 'mouse-select-down' of results.numCorrective in browse brwQar      */
/*  or 'mouse-select-down' of results.numRecommendations in browse brwQar */
/*  or 'mouse-select-down' of results.numSuggestions in browse brwQar     */
/* /*  or 'mouse-select-down' of results.qarID in browse brwQar */        */
/* DO:                                                                    */
/*  run sortAudits in this-procedure (self:name).                         */
/*  RETURN.                                                               */
/* END.                                                                   */

ON 'mouse-select-down':U OF action.actionType in browse brwAction
 or 'mouse-select-down' of action.stat in browse brwAction
 or 'mouse-select-down' of action.dueDate in browse brwAction
 or 'mouse-select-down' of action.followupDate in browse brwAction
 or 'mouse-select-down' of action.questionID in browse brwAction
DO:
 run sortActions in this-procedure (self:name).
 RETURN.
END.


/* ***************************  Main Block  *************************** */
/* wWin:window-state = 2. /* minimized */ */

{lib/win-main.i}

{src/adm2/windowmn.i}

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
&ENDIF
    /* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
    MAIN-BLOCK:
    DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
       ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
 
    /* Now enable the interface and wait for the exit condition.            */
       RUN initializeObject.
       IF NOT THIS-PROCEDURE:PERSISTENT THEN
           WAIT-FOR CLOSE OF THIS-PROCEDURE.
    END.
&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN
END.
&ENDIF

/* Set default widget state/images */
bSearchResultsOutdated:load-image-insensitive("images/s-error.bmp").
bSearchResultsOutdated:load-image("images/s-error.bmp").

assign
  wWin:min-height-pixels = wWin:height-pixels
  wWin:min-width-pixels = wWin:width-pixels
  wWin:max-height-pixels = session:height-pixels
  wWin:max-width-pixels = session:width-pixels
  minSearchFrameWidth = frame fSearch:width-pixels
  .

{lib/get-state-list.i &combo=fStates}

run loadSearchConditions in this-procedure.

wWin:window-state = 3. /* restored */

/* loadTestData().  */
/* open query brwQAR for each results where false. */
OPEN QUERY brwQAR FOR EACH allaudit.   
subscribe to "ActionChanged" anywhere.
subscribe to "AutoSearch" anywhere.

run windowResized in this-procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionChanged wWin 
PROCEDURE ActionChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pActionID as int.

 def var tOpen as int no-undo.
 def var tInProcess as int no-undo.
 def var tComplete as int no-undo.
 def var tRejected as int no-undo.

 def buffer qaction for action.

 reloadAudit().

 for each qaction:
  case qaction.stat:
   when "O" then tOpen = tOpen + 1.
   when "I" then tInProcess = tInProcess + 1.
   when "C" then tComplete = tComplete + 1.
   when "R" then tRejected = tRejected + 1.
  end case.
 end.

 publish "AuditChanged" (activeAudit,
                         tOpen,
                         tInProcess,
                         tComplete,
                         tRejected).

 find qaction
   where qaction.actionID = pActionID no-error.
 if not available qaction
  then return.

 reposition brwAction to rowid (rowid(qaction)) no-error.
 get next brwAction.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionConfig wWin 
PROCEDURE ActionConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.
 def var tSelectedStates as char no-undo.

 run dialogconfig.w.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionExport wWin 
PROCEDURE ActionExport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 def buffer x-results for results.
 def var exportFilename as char no-undo.
 def var doSave as logical no-undo.

 if query brwQAR:num-results = 0 
  then
   do: 
    MESSAGE "There are no search results to export."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
    return.
   end.
   
 publish "GetReportDir" (output std-ch).

 system-dialog get-file exportFilename
  filters "CSV Files" "*.csv"
  initial-dir std-ch
  ask-overwrite
  create-test-file
  default-extension ".csv"
  use-filename
  save-as
 update doSave.

 if not doSave 
  then return.

 std-in = 0.
 output to value(exportFilename).
 export delimiter ","
   "QAR ID"
   "Agent ID"
   "Name"
   "Addr1"
   "Addr2"
   "City"
   "State"
   "Zip"
   "QAR Date"
   "QAR Reviewer"
   "QAR Contact"
   "QAR Delivered To"
   "QAR Score"
   "Major Findings"
   "Total CA"
   "Open CA"
   "In Process CA"
   "Completed CA"
   "Rejected CA"
   "Intermediate Findings"
   "Recommendations"
   "Minor Findings"
   "Suggestions"
   "Section 1"
   "Section 2"
   "Section 3"
   "Section 4"
   "Section 5"
   "Section 6"
   "Section 7"
   "Section 8"
    .

 for each x-results:
  export delimiter "," 
    x-results.qarID
    x-results.agentID
    x-results.name
    x-results.addr1
    x-results.addr2
    x-results.city
    x-results.state
    x-results.zip
    x-results.auditDate
    x-results.auditor
    x-results.contactName
    x-results.deliveredTo
    x-results.score
    x-results.numMajor
    x-results.numCorrective
    x-results.numOpen
    x-results.numInProcess
    x-results.numCompleted
    x-results.numRejected
    x-results.numInter
    x-results.numRecommendations
    x-results.numMinor
    x-results.numSuggestions
    x-results.section1score
    x-results.section2score
    x-results.section3score
    x-results.section4score
    x-results.section5score
    x-results.section6score
    x-results.section7score
    x-results.section8score
     .
  std-in = std-in + 1.
 end.
 output close.

 RUN ShellExecuteA in this-procedure (0,
                             "open",
                             exportFilename,
                             "",
                             "",
                             1,
                             OUTPUT std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRefresh wWin 
PROCEDURE ActionRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 publish "RefreshData".
 run doSearch in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionReport wWin 
PROCEDURE ActionReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter rActionID as int.

 def buffer qar for qar.
 def buffer action for action.
 def buffer actionnote for actionnote.

 run qam01-r.p (rActionId,
                table qar,
                table action,
                table actionnote).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AutoSearch wWin 
PROCEDURE AutoSearch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pState as char.
 def input parameter pStartDate as date.
 def input parameter pEndDate as date.


 if bSearch:sensitive in frame fSearch = false 
  then
   do: 
       MESSAGE "Please complete the open audit."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 clearSearchConditions().

 if pState = "" 
  then
   do std-in = 2 to num-entries(fStates:list-item-pairs in frame fSearch) by 2:
    pState = pState + (if pState = "" then "" else ",") + entry(std-in, fStates:list-item-pairs in frame fSearch).
   end.
  else 
   do: if lookup(pState, fStates:list-item-pairs in frame fSearch) = 0
        then pState = "".
   end.
 assign
   fReviewStart:screen-value in frame fSearch = string(pStartDate)
   fReviewEnd:screen-value in frame fSearch = string(pEndDate)
   fStates:screen-value in frame fSearch = pState
   fAll:checked in frame fSearch = true
   .
 run doSearch.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseMaintainAction wWin 
PROCEDURE CloseMaintainAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pActionID as int.

 find openActions 
   where openActions.actionID = pActionID no-error.
 if available openActions 
  then delete openActions.
 find first openActions no-error.
 if available openActions 
  then return.
 
 /* Enable functionality that should be disabled while maintaining actions */
/*  brwQar:sensitive in frame fSearch = true.  */
 bSearch:sensitive in frame fSearch = true.
 menu-item m_do_search:sensitive in menu m_Search = true.
 menu-item m_refresh:sensitive in menu m_File = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSearch wWin 
PROCEDURE doSearch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query brwAction.
 tAuditID:screen-value in frame fSearch = "".
 close query brwQAR.
 publish "SearchAudits" (fQar:screen-value in frame fSearch,
                         fAgentID:screen-value in frame fSearch,
                         fName:screen-value in frame fSearch,
                         fAuditor:screen-value in frame fSearch,
                         fReviewStart:input-value in frame fSearch,
                         fReviewEnd:input-value in frame fSearch,
                         fStates:screen-value in frame fSearch,
                         fOpen:checked in frame fSearch,
                         fInProcess:checked in frame fSearch,
                         fCompleted:checked in frame fSearch,
                         fRejected:checked in frame fSearch,
                         fNone:checked in frame fSearch,
                         fAll:checked in frame fSearch,
                         fDueIn:input-value in frame fSearch,
                         fMaxTotalScore:input-value in frame fSearch,
                         fMaxSection6Score:input-value in frame fSearch,
                         fLatestOnly:checked in frame fSearch,
                         output table results).

 auditSortDesc = not auditSortDesc. /* So it doesn't end up flipped */
 run sortAudits in this-procedure (auditSortBy).
 setSearchWidgetState().

 publish "NewSearchResults".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSelect wWin 
PROCEDURE doSelect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if can-find(first openActions)
  then
   do: 
    MESSAGE "Please complete any changes to the current audit." skip(1)
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
    return.
   end.

 if not available results 
  then return.

 tAuditID:screen-value in frame fSearch = "for " + results.name + " on audit " + results.qarID.
 activeAudit = results.qarID.
 activeRowid = rowid(results).
 reloadAudit().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSelectAction wWin 
PROCEDURE doSelectAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available action 
  then return.

 find openActions 
   where openActions.actionID = action.actionID no-error.
 if available openActions and valid-handle(openActions.hInstance)
  then 
   do: run ViewWindow in openActions.hInstance no-error.
       return.
   end.
 run waction.w persistent 
   (input action.actionID,
    input results.agentID,
    input results.name,
    input results.state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fLatestOnly fName fAuditor fReviewStart fReviewEnd fQAR fAgentID 
          fStates fAll fNone fOpen fInProcess fDueIn fCompleted fRejected 
          fMaxTotalScore fMaxSection6Score txtResultCount txtResultTime tAuditID 
      WITH FRAME fSearch IN WINDOW wWin.
  ENABLE fLatestOnly fName fAuditor fReviewStart fReviewEnd fQAR fAgentID 
         fStates fAll fNone fOpen fInProcess fDueIn fCompleted fRejected 
         fMaxTotalScore fMaxSection6Score bSearch brwQAR brwAction 
      WITH FRAME fSearch IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fSearch}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/
  publish "ExitApplication".
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loadSearchConditions wWin 
PROCEDURE loadSearchConditions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 publish "GetSearchCondition" ("agentID", output std-ch).
 fAgentID:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("agentName", output std-ch).
 fName:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("qar", output std-ch).
 fQar:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("auditor", output std-ch).
 fAuditor:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("startingDate", output std-ch).
 fReviewStart:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("endingDate", output std-ch).
 fReviewEnd:screen-value in frame fSearch = std-ch.
 
 publish "GetSearchCondition" ("all", output std-ch).
 fAll:checked in frame fSearch = if std-ch = "Y" then true else false.
 publish "GetSearchCondition" ("none", output std-ch).
 fNone:checked in frame fSearch = if std-ch = "Y" then true else false.
 
 publish "GetSearchCondition" ("open", output std-ch).
 fOpen:checked in frame fSearch = if std-ch = "Y" then true else false.
 publish "GetSearchCondition" ("inProcess", output std-ch).
 fInProcess:checked in frame fSearch = if std-ch = "Y" then true else false.
 publish "GetSearchCondition" ("completed", output std-ch).
 fCompleted:checked in frame fSearch = if std-ch = "Y" then true else false.
 publish "GetSearchCondition" ("rejected", output std-ch).
 fRejected:checked in frame fSearch = if std-ch = "Y" then true else false.
 
 publish "GetSearchCondition" ("dueIn", output std-ch).
 fDueIn:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("states", output std-ch).

 def var tSelected as char no-undo.
 /* Check that the condition is valid */
 do std-in = 1 to num-entries(std-ch):
  if lookup(entry(std-in, std-ch), fStates:list-item-pairs) > 0 
   then tSelected = tSelected + (if tSelected > "" then "," else "")
           + entry(std-in, std-ch).
 end.
 
 assign
  fStates:screen-value in frame fSearch = tSelected
  bSearchResultsOutdated:visible in frame fSearch =
      txtResultCount:screen-value in frame fSearch > ""
/*   fStateCnt:screen-value in frame fSearch = "("                              */
/*     + trim(string(num-entries(fStates:screen-value in frame fSearch), ">9")) */
/*     + ")"                                                                    */
  .

 publish "GetSearchCondition" ("maxTotal", output std-ch).
 fMaxTotalScore:screen-value in frame fSearch = std-ch.
 publish "GetSearchCondition" ("maxSection6", output std-ch).
 fMaxSection6Score:screen-value in frame fSearch = std-ch.

 assign
   fQAR:modified in frame fSearch = false
   fAgentID:modified in frame fSearch = false
   fName:modified in frame fSearch = false
   fAuditor:modified in frame fSearch = false
   fReviewStart:modified in frame fSearch = false
   fReviewEnd:modified in frame fSearch = false
   .
 apply "value-changed" to fAll.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainAction wWin 
PROCEDURE OpenMaintainAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pActionID as int.
 def input parameter pWindow as handle.

 find openActions 
   where openActions.actionID = pActionID no-error.
 if not available openActions 
  then
   do: create openActions.
       openActions.actionID = action.actionID.
   end.
 openActions.hInstance = pWindow.

 /* Disable functionality that should be disabled while maintaining actions */
/*  brwQar:sensitive in frame fSearch = false. */
 bSearch:sensitive in frame fSearch = false.
 menu-item m_do_search:sensitive in menu m_Search = false.
 menu-item m_refresh:sensitive in menu m_File = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSearchConditions wWin 
PROCEDURE saveSearchConditions PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "SetSearchCondition" ("agentID", fAgentID:screen-value in frame fSearch).
 publish "SetSearchCondition" ("agentName", fName:screen-value in frame fSearch).
 publish "SetSearchCondition" ("qar", fQar:screen-value in frame fSearch).
 publish "SetSearchCondition" ("auditor", fAuditor:screen-value in frame fSearch).
 publish "SetSearchCondition" ("startingDate", fReviewStart:screen-value in frame fSearch).
 publish "SetSearchCondition" ("endingDate", fReviewEnd:screen-value in frame fSearch).
 publish "SetSearchCondition" ("all", if fAll:checked in frame fSearch then "Y" else "N").
 publish "SetSearchCondition" ("none", if fNone:checked in frame fSearch then "Y" else "N").
 publish "SetSearchCondition" ("open", if fOpen:checked in frame fSearch then "Y" else "N").
 publish "SetSearchCondition" ("inProcess", if fInProcess:checked in frame fSearch then "Y" else "N").
 publish "SetSearchCondition" ("completed", if fCompleted:checked in frame fSearch then "Y" else "N").
 publish "SetSearchCondition" ("rejected", if fRejected:checked in frame fSearch then "Y" else "N").
 publish "SetSearchCondition" ("dueIn", fDueIn:screen-value in frame fSearch).
 publish "SetSearchCondition" ("states", fStates:screen-value in frame fSearch).
 publish "SetSearchCondition" ("maxTotal", fMaxTotalScore:screen-value in frame fSearch).
 publish "SetSearchCondition" ("maxSection6", fMaxSection6Score:screen-value in frame fSearch).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortActions wWin 
PROCEDURE sortActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pName as char.

 def var hQuery as handle.
 def var tWhere as char.

 hQuery = query brwAction:handle.
 hQuery:query-close().
 browse brwAction:clear-sort-arrows().

 if pName = actionSortBy 
  then actionSortDesc = not actionSortDesc.


 tWhere = "preselect each action by ".
 case pName:
  when "stat" then tWhere = tWhere + "action.stat " 
                                    + (if actionSortDesc then "descending" else "") 
                                    + " by action.actionType by action.questionID ".
  when "dueDate" then tWhere = tWhere + "action.dueDate " 
                                        + (if actionSortDesc then "descending" else "")
                                        + " by action.actionType by action.questionID ".
  when "followupDate" then tWhere = tWhere + "action.followupDate " 
                                        + (if actionSortDesc then "descending" else "")
                                        + " by action.actionType by action.questionID ".
  when "questionID" then tWhere = tWhere + "action.questionID " 
                                      + (if actionSortDesc then "descending" else "").
  otherwise 
   do: tWhere = tWhere + "actionType " + (if actionSortDesc then "descending" else "") + " by action.questionID ".
       pName = "actionType".
   end.
 end case.
 
 do std-in = 1 to browse brwAction:num-columns:
  std-ha = browse brwAction:get-browse-column(std-in).
  if std-ha:name = pName 
   then browse brwAction:set-sort-arrow(std-in, not actionSortDesc).
 end.

 hQuery:query-prepare(tWhere).
 hQuery:query-open().
 actionSortBy = pName.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortAudits wWin 
PROCEDURE sortAudits PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pName as char.

 def var hQuery as handle.
 def var tWhere as char.

 hQuery = query brwQar:handle.
 hQuery:query-close().
 browse brwQar:clear-sort-arrows().

 if pName = auditSortBy 
  then auditSortDesc = not auditSortDesc.


 tWhere = "preselect each results by ".
 case pName:
  when "qarID" then tWhere = tWhere + "qarID " + (if auditSortDesc then "descending" else "").
  when "state" then tWhere = tWhere + "results.state " 
                                    + (if auditSortDesc then "descending" else "") 
                                    + " by results.name by results.auditDate ".
  when "auditDate" then tWhere = tWhere + "results.auditDate " 
                                        + (if auditSortDesc then "descending" else "")
                                        + " by results.name by results.auditDate ".
  when "nextDueDate" then tWhere = tWhere + "results.nextDueDate " 
                                        + (if auditSortDesc then "descending" else "")
                                        + " by results.name by results.auditDate".
  when "auditor" then tWhere = tWhere + "results.auditor " 
                                      + (if auditSortDesc then "descending" else "")
                                      + " by results.name by results.auditDate".
  when "score" then tWhere = tWhere + "results.score " 
                                      + (if auditSortDesc then "descending" else "")
                                      + " by results.name by results.auditDate".
  when "section6score" then tWhere = tWhere + "results.section6score " 
                                      + (if auditSortDesc then "descending" else "")
                                      + " by results.name by results.auditDate".
  when "numCorrective" then tWhere = tWhere + "results.numCorrective " 
                                      + (if auditSortDesc then "descending" else "")
                                      + " by results.name by results.auditDate".
  when "numRecommendations" then tWhere = tWhere + "results.numRecommendations " 
                                      + (if auditSortDesc then "descending" else "")
                                      + " by results.name by results.auditDate".
  when "numSuggestions" then tWhere = tWhere + "results.numSuggestions " 
                                      + (if auditSortDesc then "descending" else "")
                                      + " by results.name by results.auditDate".
  otherwise 
   do: tWhere = tWhere + "name " + (if auditSortDesc then "descending" else "").
       pName = "name".
   end.
 end case.
 
 do std-in = 1 to browse brwQar:num-columns:
  std-ha = browse brwQar:get-browse-column(std-in).
  if std-ha:name = pName 
   then browse brwQar:set-sort-arrow(std-in, not auditSortDesc).
 end.

 hQuery:query-prepare(tWhere).
 hQuery:query-open().
 auditSortBy = pName.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fSearch:width-pixels = wWin:width-pixels.
 frame fSearch:virtual-width-pixels = wWin:width-pixels.
 frame fSearch:height-pixels = wWin:height-pixels.
 frame fSearch:virtual-height-pixels = wWin:height-pixels.

 /* fSearch components */
 brwQAR:width-pixels = frame fSearch:width-pixels - 20.
/*  brwQAR:height-pixels = frame fSearch:height-pixels - 240. */
 
/*  bClear:y = frame fSearch:height-pixels - 35. */
/*  bSearchResultsOutdated:y = frame fSearch:height-pixels - 35.  */
/*  txtResultCount:y = frame fSearch:height-pixels - 36. */
/*  txtResultTime:y = frame fSearch:height-pixels - 21.  */

 brwAction:width-pixels = frame fSearch:width-pixels - 20.
 brwAction:height-pixels = frame fSearch:height-pixels - browse brwAction:y - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearSearchConditions wWin 
FUNCTION clearSearchConditions RETURNS logical
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  assign
    fAgentID:screen-value in frame fSearch = ""
    fName:screen-value in frame fSearch = ""
    fQAR:screen-value in frame fSearch = ""
    fAuditor:screen-value in frame fSearch = ""
    fReviewStart:screen-value in frame fSearch = ""
    fReviewEnd:screen-value in frame fSearch = ""
    fAll:checked in frame fSearch = false
    fNone:checked in frame fSearch = false
    fOpen:checked in frame fSearch = false
    fInProcess:checked in frame fSearch = false
    fCompleted:checked in frame fSearch = false
    fRejected:checked in frame fSearch = false
    fDueIn:screen-value in frame fSearch = "0"
    fStates:screen-value in frame fSearch = ?
/*     fStateCnt:screen-value in frame fSearch = "(0)" */

    bSearchResultsOutdated:visible in frame fSearch 
      = txtResultCount:screen-value in frame fSearch > ""

    fMaxTotalScore:screen-value in frame fSearch = "150"
    fMaxSection6Score:screen-value in frame fSearch = "10"

    fAgentID:modified in frame fSearch = false
    fName:modified in frame fSearch = false
    fQAR:modified in frame fSearch = false
    fAuditor:modified in frame fSearch = false
    fReviewStart:modified in frame fSearch = false
    fReviewEnd:modified in frame fSearch = false
    .

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION loadTestData wWin 
FUNCTION loadTestData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
/*def buffer x for results.

input from c:\users\david\desktop\qar\summary.csv.
repeat:
 create x.
 import delimiter ","
   x.qarID 
   x.agentID 
   x.name 
   x.addr1 
   x.city 
   x.state 
   x.zip 
   x.auditDate 
   x.auditor 
   x.contactName 
   x.deliveredTo 
   x.score 
   x.comments 
   x.nextFollowupDate 
   x.nextDueDate 
   x.section1score 
   x.section2score 
   x.section3score 
   x.section4score 
   x.section5score 
   x.section6score 
   x.section7score 
   x.section8score 
   x.numMajor 
   x.numInter 
   x.numMinor 
   x.numOpen 
   x.numInProcess 
   x.numCompleted 
   x.numRejected 
   .
 if x.nextFollowupDate = 1/1/2500 
  then x.nextFollowupDate = ?.
 if x.nextDueDate = 1/1/2500 
  then x.nextDueDate = ?.
end.
input close.

for each x
  where x.qarID = "":
 delete x.
end.

RETURN FALSE.   /* Function return value. */*/

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION reloadAudit wWin 
FUNCTION reloadAudit RETURNS LOGICAL PRIVATE
  ( ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer testqar for results.
 def buffer testaction for action.

 close query brwAction.

 empty temp-table qar.
 empty temp-table action.
 empty temp-table actionnote.
 
 publish "GetAudit" (input activeAudit,
                      output table qar,
                      output table action,
                      output table actionnote,
                      output std-lo).
 actionSortDesc = not actionSortDesc.
 run sortActions in this-procedure (actionSortBy).

 if can-find(first testaction) 
  then assign
        menu-item m_Current_Action_Status:sensitive in menu m_Report = true
        menu-item m_Action_Status_Report:sensitive in menu m_Report = true
        .
  else assign
        menu-item m_Current_Action_Status:sensitive in menu m_Report = false
        menu-item m_Action_Status_Report:sensitive in menu m_Report = false
        .

 find testqar 
   where testqar.qarID = activeAudit no-error.
 if not available testqar 
  then return true.
 create qar.
 buffer-copy testqar to qar.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION repositionToCurrent wWin 
FUNCTION repositionToCurrent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if available results and rowid(results) = activeRowid 
  then return true.
 reposition brwQar to rowid activeRowid no-error.
 get next brwQar.
 return available results.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setSearchWidgetState wWin 
FUNCTION setSearchWidgetState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 assign
/*    bClear:sensitive in frame fSearch =                                     */
/*      (brwQAR:num-selected-rows in frame fSearch > 0 and activeAction = "") */
   
   txtResultCount:screen-value in frame fSearch = 
      string(query brwQAR:num-results, ">>>,>>9") + " records"
   txtResultTime:screen-value in frame fSearch = string(now, "99/99/99 HH:MM:SS")
   bSearchResultsOutdated:visible in frame fSearch = false
   .
 menu-item m_Export_Search_Results:sensitive in menu m_Report = query brwQar:num-results > 0.
 menu-item m_Current_Action_Status:sensitive in menu m_Report = false.
 menu-item m_Action_Status_Report:sensitive in menu m_Report = false.
 
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

