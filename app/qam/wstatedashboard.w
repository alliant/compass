&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wstatedashboard.w
   Window of statedashboard across qar records
   @author AC 
   @modification
   Date          Name           Description
   10/06/2020    Anjly Chanana  Modified to include TOR audits  
   09/24/2021    SA             Task 86696 Defects raised by david
   */             

CREATE WIDGET-POOL.
{lib/winlaunch.i}
{tt/agent.i}
{tt/auditor.i}
{tt/qardbstate.i}
{tt/qardbstate.i &tableAlias="ttstatedb"}
{tt/state.i}
 def temp-table ttresults like ttstatedb.
{lib/std-def.i}

def var auditorpair as char no-undo.
def var agentpair as char no-undo.
def var yearpair as char no-undo.
def var tstate as char no-undo.
def var tagent as char no-undo.
def var tauditor as char no-undo.
def var tyear as char no-undo.
def var hBrw as handle no-undo.
def var hColumn as handle extent 18 no-undo.
def var dbdata as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwStateDashboard

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES dbstate

/* Definitions for BROWSE brwStateDashboard                             */
&Scoped-define FIELDS-IN-QUERY-brwStateDashboard dbstate.state entry(index("EQUT0", dbstate.audittype),"ERR,QAR,UnderWriter,TOR, ") @ dbstate.audittype dbstate.period dbstate.maxScore dbstate.minScore dbstate.avgScore dbstate.totalScore dbstate.numRejected dbstate.numOpen dbstate.numInProcess dbstate.numComplete dbstate.numSuggestions dbstate.numRecommendations dbstate.numCorrective dbstate.numMajor dbstate.numInter dbstate.numMinor dbstate.numFindings dbstate.numAudits   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwStateDashboard   
&Scoped-define SELF-NAME brwStateDashboard
&Scoped-define QUERY-STRING-brwStateDashboard FOR EACH dbstate by dbstate.state
&Scoped-define OPEN-QUERY-brwStateDashboard OPEN QUERY {&SELF-NAME} FOR EACH dbstate by dbstate.state.
&Scoped-define TABLES-IN-QUERY-brwStateDashboard dbstate
&Scoped-define FIRST-TABLE-IN-QUERY-brwStateDashboard dbstate


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwStateDashboard}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-45 RECT-46 cbyear cState cAuditor ~
bRefresh bExport Agents brwStateDashboard 
&Scoped-Define DISPLAYED-OBJECTS cbyear cState cAuditor Agents 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE Agents AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN AUTO-COMPLETION
     SIZE 84.2 BY 1 NO-UNDO.

DEFINE VARIABLE cAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbyear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "IItem 1","Item 1"
     DROP-DOWN-LIST
     SIZE 13.2 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 107.6 BY 3.1.

DEFINE RECTANGLE RECT-46
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.2 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwStateDashboard FOR 
      dbstate SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwStateDashboard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwStateDashboard C-Win _FREEFORM
  QUERY brwStateDashboard DISPLAY
      dbstate.state              label " State"             format "x(2)"
   entry(index("EQUT0", dbstate.audittype),"ERR,QAR,UnderWriter,TOR, ") @ dbstate.audittype label "Audit Type" format "x(12)"
 dbstate.period             label "Period"             format "99/99/99"
 dbstate.maxScore           label "Max Score"          format "zzzz"
 dbstate.minScore           label "Min Score"          format "zzzz" 
 dbstate.avgScore           label "Avg Score"          format "zzzz" 
 dbstate.totalScore         label "Total Score"        format "zzzz" 
 dbstate.numRejected        label "Rejected"           format "zzzz" 
 dbstate.numOpen            label "Open"               format "zzzz" 
 dbstate.numInProcess       label "InProcess"          format "zzzz" 
 dbstate.numComplete        label "Complete"           format "zzzz" 
 dbstate.numSuggestions     label "Suggestions"        format "zzzz" 
 dbstate.numRecommendations label "Recommendations"    format "zzzz" 
 dbstate.numCorrective      label "Corrective"         format "zzzz" 
 dbstate.numMajor           label "Major"              format "zzzz" 
 dbstate.numInter           label "Intermediate"       format "zzzz" 
 dbstate.numMinor           label "Minor"              format "zzzz"
 dbstate.numFindings        label "Total Findings"     format "zzzz"
 dbstate.numAudits          label "Total Audits"       format "zzzz"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 206 BY 21.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbyear AT ROW 1.86 COL 10.8 COLON-ALIGNED WIDGET-ID 10
     cState AT ROW 1.86 COL 33.8 COLON-ALIGNED WIDGET-ID 150
     cAuditor AT ROW 1.86 COL 70 COLON-ALIGNED WIDGET-ID 152
     bRefresh AT ROW 2.1 COL 100.8 WIDGET-ID 4
     bExport AT ROW 2.1 COL 112.6 WIDGET-ID 156
     Agents AT ROW 3.05 COL 10.8 COLON-ALIGNED WIDGET-ID 22
     brwStateDashboard AT ROW 4.81 COL 3 WIDGET-ID 200
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.14 COL 111 WIDGET-ID 164
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 3.6 WIDGET-ID 162
     RECT-45 AT ROW 1.43 COL 3 WIDGET-ID 158
     RECT-46 AT ROW 1.43 COL 110.2 WIDGET-ID 160
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 210.2 BY 26.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "State Analysis"
         HEIGHT             = 26.24
         WIDTH              = 210.2
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwStateDashboard Agents fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwStateDashboard:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwStateDashboard:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwStateDashboard
/* Query rebuild information for BROWSE brwStateDashboard
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH dbstate by dbstate.state.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwStateDashboard */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* State Analysis */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* State Analysis */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* State Analysis */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Agents C-Win
ON VALUE-CHANGED OF Agents IN FRAME fMain /* Agent */
DO:
  
  if agents:screen-value in frame fmain = ? then
       agents:screen-value in frame fmain = " ".

  if cAuditor:screen-value in frame fmain =  tAuditor and 
     cbyear:screen-value in frame fMain   = tYear    and
     agents:screen-value in frame fmain   = tagent   and 
     cstate:screen-value in frame fMain   = tstate   then
   OPEN QUERY brwStateDashboard FOR EACH dbstate by dbstate.state.
  else
   close query brwStateDashboard.
   run Setcount.

  /* commented to fix the error "DITEM is not large enough to hold string." */
/*   Agents:tooltip = entry( lookup(Agents:screen-value,Agents:list-item-pairs, "|") - 1, Agents:list-item-pairs, "|"). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  empty temp-table dbstate.
  empty temp-table ttstatedb.

  std-ch = " record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") .

 
  publish "GetDashboardData" (input cbyear:screen-value,
                              input Agents:screen-value,
                              input cAuditor:screen-value,
                              input cState:screen-value,
                              output table ttstatedb).

  for each ttstatedb:
    create dbstate.
    assign
      dbstate.state               = if ttstatedb.state              = "?" then ""       else  ttstatedb.state             
      dbstate.period              = if ttstatedb.period             = ?   then date("") else  ttstatedb.period            
      dbstate.maxScore            = if ttstatedb.maxScore           = ?   then 0        else  ttstatedb.maxScore          
      dbstate.minScore            = if ttstatedb.minScore           = ?   then 0        else  ttstatedb.minScore          
      dbstate.avgScore            = if ttstatedb.avgScore           = ?   then 0        else  ttstatedb.avgScore          
      dbstate.totalScore          = if ttstatedb.totalScore         = ?   then 0        else  ttstatedb.totalScore        
      dbstate.numRejected         = if ttstatedb.numRejected        = ?   then 0        else  ttstatedb.numRejected       
      dbstate.numOpen             = if ttstatedb.numOpen            = ?   then 0        else  ttstatedb.numOpen           
      dbstate.numInProcess        = if ttstatedb.numInProcess       = ?   then 0        else  ttstatedb.numInProcess      
      dbstate.numComplete         = if ttstatedb.numComplete        = ?   then 0        else  ttstatedb.numComplete       
      dbstate.numSuggestions      = if ttstatedb.numSuggestions     = ?   then 0        else  ttstatedb.numSuggestions    
      dbstate.numRecommendations  = if ttstatedb.numRecommendations = ?   then 0        else  ttstatedb.numRecommendations
      dbstate.numCorrective       = if ttstatedb.numCorrective      = ?   then 0        else  ttstatedb.numCorrective     
      dbstate.numMajor            = if ttstatedb.numMajor           = ?   then 0        else  ttstatedb.numMajor          
      dbstate.numInter            = if ttstatedb.numInter           = ?   then 0        else  ttstatedb.numInter          
      dbstate.numMinor            = if ttstatedb.numMinor           = ?   then 0        else  ttstatedb.numMinor          
      dbstate.numFindings         = if ttstatedb.numFindings        = ?   then 0        else  ttstatedb.numFindings       
      dbstate.numAudits           = if ttstatedb.numAudits          = ?   then 0        else  ttstatedb.numAudits
      dbstate.audittype           = if ttstatedb.audittype = ""  or ttstatedb.audittype = "?" then "0" else  ttstatedb.audittype
      dbdata = dbdata + 1.
  end.

  status default string(dbdata) + std-ch in window {&window-name}. 
  status input string( dbdata) + std-ch in window {&window-name}.
  dbdata = 0.
     
    run sortdata("state").
/*     OPEN QUERY brwStateDashboard FOR EACH dbstate by dbstate.state. */
/*     status input string( dbdata)+ " records(s) found as of "  + " " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") in window {&window-name}. */
/*     dbdata = 0.                                                                                                                                               */

  if query brwStateDashboard:num-results = 0
  then bExport:sensitive = false.
  else 
 bExport:sensitive = true.
 tAgent = Agents:screen-value in frame fMain.
 tYear = cbYear:screen-value in frame fMain.
 tAuditor = cAuditor:screen-value in frame fmain.
 tstate = cState:screen-value in frame fMain.
/*  run setCount. */
/*  apply "value-changed" to brwStateDashboard. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwStateDashboard
&Scoped-define SELF-NAME brwStateDashboard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwStateDashboard C-Win
ON ROW-DISPLAY OF brwStateDashboard IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwStateDashboard C-Win
ON START-SEARCH OF brwStateDashboard IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAuditor C-Win
ON VALUE-CHANGED OF cAuditor IN FRAME fMain /* Auditor */
DO:
  close query brwStateDashboard.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbyear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbyear C-Win
ON VALUE-CHANGED OF cbyear IN FRAME fMain /* Year */
DO:
  close query brwStateDashboard.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
  /* get the agents */
  close query brwStateDashboard.
  run SetCount.
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
{lib/win-main.i}
{lib/brw-main.i}
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

do std-in = 0 to 9:
  yearpair = yearpair + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in) + "," .
end.
yearpair = trim(yearpair, ",").
yearpair = "ALL,0," + yearpair.
cbyear:list-item-pairs = yearpair.


publish "GetAuditors"(output table auditor).
cAuditor:delimiter = "|" .
for each auditor no-lock:
  auditorpair  =  auditorpair + "|" + string(auditor.name) + "|" + string(auditor.UID) .
end.
auditorpair = trim(auditorpair , "|" ).
cAuditor:list-item-pairs =  "ALL|ALL|" + auditorpair.
cAuditor:screen-value = "ALL".

/* get the states */
{lib/get-state-list.i &combo=cState &addAll=true}
{lib/get-agent-list.i &combo=Agents &state=cState &addAll=true}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  assign
    tYear    = cbyear:screen-value
    tState   = cState:screen-value 
    tAgent   = Agents:screen-value
    tAuditor = cAuditor:screen-value.

  RUN enable_UI.
  STATUS input " " in window {&window-name}. 
  if query brwStateDashboard:num-results = 0
  then bExport:sensitive = false.
  assign
    cbyear:screen-value = "0".
  do std-in = 1 to 18:
    hBrw = browse brwStateDashboard:handle.
    hColumn[std-in] = hBrw:get-browse-column(std-in).
  end.
  apply "value-changed" to cState.
  run windowResized in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbyear cState cAuditor Agents 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-45 RECT-46 cbyear cState cAuditor bRefresh bExport Agents 
         brwStateDashboard 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 if query brwStateDashboard:num-results = 0
 then
  do:
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
  end.
  for each dbstate:
      create ttresults.
      buffer-copy dbstate to ttresults.
      if ttresults.audittype = "E" then
         ttresults.audittype = "ERR".
     if ttresults.audittype = "Q" then
         ttresults.audittype = "QAR".
     if ttresults.audittype = "U" then
         ttresults.audittype = "UnderWriter".
     if ttresults.audittype = "T" then
         ttresults.audittype = "TOR".
  end.
  publish "GetReportDir" (output std-ch).
  th = temp-table ttresults:handle.
run util/exporttable.p (table-handle th,
                         "ttresults",
                         "for each ttresults ",
                         "state,audittype,period,maxScore,minScore,avgScore,totalScore,numRejected,numOpen,numInProcess,numComplete,numSuggestions,numRecommendations,numCorrective,numMajor,numInter,numMinor,numFindings,numAudits",
                         "State,Audit Type,Period,Max Score,Min Score,Avg Score,Total Score,Rejected,Open,InProcess,Complete,Suggestions,Recommendations,Corrective,Major,Intermediate,Minor,Total Findings,Total Audits",
                         std-ch,
                         "StateDashboard- "  + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
std-in = 0.
for each dbstate no-lock:
  std-in = std-in + 1.
end.
 std-ch = string(std-in) + " " + "record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") .
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.

 if brwStateDashboard:num-iterations in frame fMain = 0 then
 do:
   std-ch = "".
   status default std-ch in window {&window-name}.
   status input std-ch in window {&window-name}.
   bExport:sensitive in frame fMain = false.
 end.
 else
 bExport:sensitive in frame fMain = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i &post-by-clause=" + ' by dbstate.state' "}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwStateDashboard:width-pixels = frame fmain:width-pixels - 20.
 brwStateDashboard:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

