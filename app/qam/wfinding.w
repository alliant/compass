&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wfindings.w
   Window of FINDINGS across qar records
   08.11.2017
   @author AC
   @modification
   Date          Name           Description
   10/14/2020    Anjly Chanana  Modified for TOR audits exports
   07/15/2021    SA             Task 83510 modified UI according to 
                                 new field grade
   09/24/2021    SA             Task#:86696 Defects raised by david
   */

CREATE WIDGET-POOL.


{lib/std-def.i}
{tt/agent.i}
{tt/auditor.i}
{tt/qaraudit.i &tableAlias="allaudit"}
{tt/state.i}
{lib/std-def.i}
{tt/findingreport.i &tableAlias="ttqarfinding"}
def temp-table qarfinding like ttqarfinding 
  fields cPriority as char
  fields cqarId as char 
  fields taudittype as char
  fields terrtype as char     .

def var tqarID as int no-undo.
def var tstate as char no-undo.
def var tagent as char no-undo.
def var agent as char no-undo.
def var tauditor as char no-undo.
def var tyear as char no-undo.
def var tmpqarID as char no-undo.

def var auditorpair as char no-undo.
def var agentpair as char no-undo.
def var yearpair as char no-undo.
def var piYear as int no-undo.
def var piAuditID as int no-undo.
def var pcState as char no-undo.
def var pcAgentID as char no-undo.
def var pcUid as char no-undo.
def var hserver as handle no-undo.
def var totalfinding as int.
def var tagentid as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwFindings

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES qarfinding

/* Definitions for BROWSE brwFindings                                   */
&Scoped-define FIELDS-IN-QUERY-brwFindings qarfinding.cqarID entry(index("EQUT0", qarfinding.audittype),"ERR,QAR,UnderWriter,TOR, ") @ qarfinding.audittype entry(index("IPCM0", qarfinding.errtype), "Interval,Presign,Corrective,Monthly, ") @ qarfinding.errtype qarfinding.state qarfinding.score qarfinding.grade qarfinding.questionID entry(index("123", qarfinding.cpriority ), "Minor,Intermediate,Major") @ qarfinding.cpriority qarfinding.auditStartDate qarfinding.auditor qarfinding.agentID qarfinding.name   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFindings   
&Scoped-define SELF-NAME brwFindings
&Scoped-define QUERY-STRING-brwFindings FOR EACH qarfinding by qarfinding.qarID
&Scoped-define OPEN-QUERY-brwFindings OPEN QUERY {&SELF-NAME} FOR EACH qarfinding by qarfinding.qarID.
&Scoped-define TABLES-IN-QUERY-brwFindings qarfinding
&Scoped-define FIRST-TABLE-IN-QUERY-brwFindings qarfinding


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwFindings}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-45 RECT-46 cbyear cState cAuditor ~
bRefresh bExport fQarID Agents brwFindings tFinding 
&Scoped-Define DISPLAYED-OBJECTS cbyear cState cAuditor fQarID Agents ~
tFinding 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE Agents AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN AUTO-COMPLETION
     SIZE 84.2 BY 1 NO-UNDO.

DEFINE VARIABLE cAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbyear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "IItem 1","Item 1"
     DROP-DOWN-LIST
     SIZE 13.2 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 184 BY 5.05 NO-UNDO.

DEFINE VARIABLE fQarID AS INTEGER FORMAT ">>>>>>>>":U INITIAL 0 
     LABEL "QAR ID" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 141 BY 3.1.

DEFINE RECTANGLE RECT-46
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.2 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFindings FOR 
      qarfinding SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFindings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFindings C-Win _FREEFORM
  QUERY brwFindings DISPLAY
      qarfinding.cqarID          label "QAR ID" format "x(15)"
 entry(index("EQUT0", qarfinding.audittype),"ERR,QAR,UnderWriter,TOR, ") @ qarfinding.audittype label "Audit Type"      format "x(12)" 
 entry(index("IPCM0", qarfinding.errtype), "Interval,Presign,Corrective,Monthly, ") @ qarfinding.errtype label "ERR Type"      format "x(12)" 
 qarfinding.state          label "State" format "xx"
 qarfinding.score label "Points" format "zzz"  width 8
 qarfinding.grade label "Score%" format "zzz"  width 9
 qarfinding.questionID     label "Question" format "x(12)"
 entry(index("123", qarfinding.cpriority ), "Minor,Intermediate,Major") @  qarfinding.cpriority  label "Severity" format "x(14)"
 qarfinding.auditStartDate label "Audit Date" format "99/99/9999"
 qarfinding.auditor        label "Auditor" format "x(40)"
 qarfinding.agentID        label "Agent ID" format "x(12)" 
 qarfinding.name           label "Agent Name" format "x(40)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 184 BY 16.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbyear AT ROW 1.86 COL 10.8 COLON-ALIGNED WIDGET-ID 10
     cState AT ROW 1.86 COL 33.8 COLON-ALIGNED WIDGET-ID 150
     cAuditor AT ROW 1.86 COL 70 COLON-ALIGNED WIDGET-ID 152
     bRefresh AT ROW 2.1 COL 134 WIDGET-ID 4
     bExport AT ROW 2.1 COL 146 WIDGET-ID 156
     fQarID AT ROW 2.43 COL 107.2 COLON-ALIGNED WIDGET-ID 166
     Agents AT ROW 3.05 COL 10.8 COLON-ALIGNED WIDGET-ID 22
     brwFindings AT ROW 4.81 COL 3 WIDGET-ID 200
     tFinding AT ROW 22.29 COL 3 NO-LABEL WIDGET-ID 168
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.1 COL 144.6 WIDGET-ID 164
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 3.6 WIDGET-ID 162
     RECT-45 AT ROW 1.43 COL 3 WIDGET-ID 158
     RECT-46 AT ROW 1.43 COL 143.6 WIDGET-ID 160
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 187.8 BY 26.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Findings"
         HEIGHT             = 26.76
         WIDTH              = 187.8
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFindings Agents fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwFindings:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwFindings:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

ASSIGN 
       tFinding:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFindings
/* Query rebuild information for BROWSE brwFindings
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH qarfinding by qarfinding.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFindings */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Findings */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Findings */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Findings */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Agents C-Win
ON VALUE-CHANGED OF Agents IN FRAME fMain /* Agent */
DO:
  close query brwFindings.
  run Setcount.

/* commented to fix the error "DITEM is not large enough to hold string." */
/*  Agents:tooltip = entry(lookup(Agents:screen-value,Agents:list-item-pairs, "|") - 1, Agents:list-item-pairs, "|"). */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  empty temp-table qarfinding.
  empty temp-table ttqarfinding.

  publish "GetFindingData" (input cbyear:screen-value,
                            input fQarID:screen-value,
                            input cState:screen-value,
                            input Agents:screen-value,
                            input cAuditor:screen-value,
                            output table ttqarFinding).
  totalfinding = 0.
  for each ttqarFinding:
  create qarfinding.
  assign 
     qarfinding.qarID           = if ttqarFinding.qarID          = ?   then 0            else ttqarFinding.qarID         
     qarfinding.auditStartDate  = if ttqarFinding.auditStartDate = ?   then datetime("") else ttqarFinding.auditStartDate
     qarfinding.auditor         = if ttqarFinding.auditor        = "?" then ""           else ttqarFinding.auditor       
     qarfinding.agentID         = if ttqarFinding.agentID        = "?" then ""           else ttqarFinding.agentID       
     qarfinding.state           = if ttqarFinding.state          = "?" then ""           else ttqarFinding.state         
     qarfinding.questionID      = if ttqarFinding.questionID     = "?" then ""           else ttqarFinding.questionID    
     qarfinding.priority        = if ttqarFinding.priority       = ?   then 0            else ttqarFinding.priority      
     qarfinding.name            = if ttqarFinding.name           = "?" then ""           else ttqarFinding.name      
     qarfinding.comments        = if ttqarFinding.comments       = "?" then ""           else ttqarFinding.comments  
     qarfinding.cpriority       = if ttqarFinding.priority       = ?   then "0"          else string(ttqarFinding.priority) 
     qarfinding.cqarID          = if ttqarFinding.qarID          = ?   then ""            else string(ttqarFinding.qarID) 
     qarfinding.audittype        = if ttqarFinding.audittype = ""  or ttqarFinding.audittype = "?" then "0" else  ttqarFinding.audittype
     qarfinding.errtype          = if ttqarFinding.errtype = "" or ttqarFinding.errtype = "?" then "0" else  ttqarFinding.errtype  
     qarfinding.taudittype        = if ttqarFinding.audittype = ""  or ttqarFinding.audittype = "?" then "0" else  ttqarFinding.audittype
     qarfinding.terrtype          = if ttqarFinding.errtype = "" or ttqarFinding.errtype = "?" then "0" else  ttqarFinding.errtype    
     qarfinding.score           = if ttqarFinding.score          = ?   then 0            else ttqarFinding.score         
     qarfinding.grade           = if ttqarFinding.grade          = ?   then 0            else ttqarFinding.grade         
     .              
     
     totalfinding = totalfinding + 1.
      if  qarfinding.audittype = "Q" or 
              qarfinding.audittype = "T" then
           qarfinding.errtype   = "0".
  end.
/*     open query brwfindings for each qarfinding by qarfinding.qarID. */
  run SortData("qarID").
  status default string(totalfinding) + std-ch in window {&window-name}.
  status input string(totalfinding)+ std-ch  in window {&window-name}.

  if query brwFindings:num-results = 0
    then bExport:sensitive = false.
  else 
    bExport:sensitive = true.
 
tAgent = Agents:screen-value in frame fMain.
tYear = cbYear:screen-value in frame fMain.
tAuditor = cAuditor:screen-value in frame fmain.
tstate = cState:screen-value in frame fMain.

/* run setCount. */
apply "value-changed" to brwfindings.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFindings
&Scoped-define SELF-NAME brwFindings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON ROW-DISPLAY OF brwFindings IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON START-SEARCH OF brwFindings IN FRAME fMain
DO: 
{lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON VALUE-CHANGED OF brwFindings IN FRAME fMain
DO:
 find current qarfinding no-error.
    if available qarfinding then
  tFinding:screen-value = qarfinding.comments.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAuditor C-Win
ON VALUE-CHANGED OF cAuditor IN FRAME fMain /* Auditor */
DO:
  close query brwFindings.
  run Setcount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbyear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbyear C-Win
ON VALUE-CHANGED OF cbyear IN FRAME fMain /* Year */
DO:
  close query brwFindings.
  run Setcount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
  close query brwFindings.
  run Setcount.
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

publish "OpenMaintainFindings" (string(tqarID), this-procedure) .

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

do std-in = 0 to 9:
  yearpair =  yearpair + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in) + "," .
end.
assign
  yearpair               = "ALL,0," + trim(yearpair, ",").
  cbyear:list-item-pairs = yearpair.
  cbyear:screen-value    = "0".

publish "GetAuditors"(output table auditor).
cAuditor:delimiter = "|" .
for each auditor no-lock:
  auditorpair  =  auditorpair + "|" + string(auditor.name) + "|" + string(auditor.UID) .
end.
cAuditor:list-item-pairs =  "ALL|ALL|" + trim(auditorpair , "|" ).
cAuditor:screen-value = "ALL".

{lib/get-state-list.i &combo=cState &addAll=true}
{lib/get-agent-list.i &combo=Agents &state=cState &addAll=true}

publish "GetCurrentValue" ("QARID", output tqarID).
publish "GetCurrentValue" ("Agent", output agent).
publish "GetCurrentValue" ("AgentID", output tagentid).
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

on 'value-changed' of fQarID 
do: 
  close query brwfindings.
  C-Win:title = entry(1,C-Win:title,"-").
  run setcount.
  
  if fQarID:screen-value gt "" then
  assign
   cbyear:sensitive = false
   cState:sensitive = false
   cAuditor:sensitive = false
   Agents:sensitive = false.
  else 
  assign
   cbyear:sensitive = true
   cState:sensitive = true
   cAuditor:sensitive = true
   Agents:sensitive = true.
   tfinding:screen-value = "".
 end.
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

assign
  tYear    = cbyear:screen-value
  tState   = cState:screen-value 
  tAgent   = Agents:screen-value
  tAuditor = cAuditor:screen-value
  tmpqarid = fQarID:screen-value.

  RUN enable_UI.

  status input " " in window {&window-name}. 
  if query brwFindings:num-results = 0
  then bExport:sensitive = false.

  if tqarID > 0 then
  do:
    assign
      fQarID:screen-value in frame fMain = string(tqarID)
      C-Win:title = C-Win:title + " - " + string(tqarID) + " - " + string(agent) + " (" + string(tagentid) + ")" .
      apply 'choose' to bRefresh.
  end.
  else
  do:
    apply "value-changed" to cState.
    run windowResized in this-procedure.
    if fQarID:screen-value gt "" then
    assign
      cbyear:sensitive = false
      cState:sensitive = false
      cAuditor:sensitive = false
      Agents:sensitive = false.
  end.

   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbyear cState cAuditor fQarID Agents tFinding 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-45 RECT-46 cbyear cState cAuditor bRefresh bExport fQarID Agents 
         brwFindings tFinding 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 if query brwFindings:num-results = 0
  then
  do:
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
  end.
  

  for each qarfinding:
    if qarfinding.tauditType = "E" then
       qarfinding.taudittype = "ERR".
    if qarfinding.tauditType = "Q" then
       qarfinding.taudittype = "QAR".
    if qarfinding.tauditType = "U" then
       qarfinding.taudittype = "UnderWriter".
    if qarfinding.tauditType = "T" then
       qarfinding.taudittype = "TOR".
    if qarfinding.terrType = "I" then
       qarfinding.terrtype = "Interval".
    if qarfinding.terrType = "P" then
       qarfinding.terrType = "Presign".
    if qarfinding.terrType = "C" then
       qarfinding.terrType = "Corrective".
    if qarfinding.terrtype = "0" then
       qarfinding.terrtype = "" .
 end.
 
 publish "GetReportDir" (output std-ch).
 th = temp-table qarfinding:handle.
run util/exporttable.p (table-handle th,
                         "qarfinding",
                         "for each qarfinding ",
                         "QARID,tauditType,terrtype,State,score,grade,QuestionID,Priority,auditStartDate,Auditor,AgentID,Name",
                         "QAR ID,Audit Type, Type,stateID,Points,Score,Question ID,Type,AuditStartDate,Auditor,Agent ID,Agent Name",
                         std-ch,
                         "Findings- " + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
std-in = 0.
for each qarfinding no-lock:
  std-in = std-in + 1.
end.
 std-ch = string(std-in) + " " + "record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") .
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.

 if brwFindings:num-iterations in frame fMain = 0 then
 do:
   std-ch = "".
   status default std-ch in window {&window-name}.
   status input std-ch in window {&window-name}.
   bExport:sensitive in frame fMain = false.
   tfinding:screen-value = "".
 end.
 else
 bExport:sensitive in frame fMain = true.
 apply 'value-changed' to brwfindings.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewWindow C-Win 
PROCEDURE ViewWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized  then
    {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var fMainOrigWidth as int.

 fMainOrigWidth = frame fMain:width-pixels.
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */

 brwFindings:width-pixels = frame fmain:width-pixels - 20 no-error.
 brwFindings:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 126 no-error.
 tFinding:x       =   tFinding:x +  0.002 * (frame fMain:width-pixels - fMainOrigWidth) no-error.
 tFinding:y       =   frame fMain:height-pixels - 116 no-error.
 tFinding:width-pixel = frame fMain:width-pixels - 20 no-error .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

