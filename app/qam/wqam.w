&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------
 File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Archana Gupta

  Created: 
  Modified : 10/22/20  AC  Changed QAR ID to Audit ID
             07/15/21  SA  Task 83510 modified UI according to 
                           new field grade
             09/24/21  SA  Task 86696 Defects raised by david
             02/07/22  SA  Task 90943 modified Refresh issue on screen
             03/17/23  K.R Task 103305 Enhancment in qam
             08/11/23  AG  Task#:106419 Modified to show agent legal name instead of short name.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/add-delimiter.i}
{lib/set-button-def.i}
{tt/agent.i}
{tt/auditor.i}
{tt/qaraudit.i &tableAlias="allaudit"}
{tt/qaraudit.i &tableAlias="ttresults"}
{tt/qaraudit.i &tableAlias="ttaudit"}
{tt/openwindow.i}
{tt/state.i}

define temp-table results like ttresults
    fields startdate as date  
    fields enddate as date .

define temp-table qarresults like results.

def temp-table openQars
 field qarid as int
 field hInstance as handle.

def temp-table openDocs
 field qarid as int
 field hInstance as handle.

def var minSearchFrameWidth as int no-undo.
def var iBgColor as integer no-undo.
def var activeUser as char no-undo.
def var activeAudit as char no-undo.
def var activeRowid as rowid no-undo.

def var auditSortBy as char no-undo init "name".
def var auditSortDesc as logical no-undo init false.
def var actionSortBy as char no-undo init "actionType".
def var actionSortDesc as logical no-undo init false.

def var hDocWindow as handle no-undo.
def var hRepoWindow as handle no-undo.
def var hintervalerrWindow as handle no-undo.
def var hregularqarWindow as handle no-undo.
def var hAgentWindow as handle no-undo.
def var hDestination as handle no-undo.
def var auditorpair as char.
def var pYear as char.
def var pStatus as char.
def var sortcolum as char.
def var openqar as logical.
def var qarsummaryhandle as handle.
{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fSearch
&Scoped-define BROWSE-NAME brwQAR

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES results

/* Definitions for BROWSE brwQAR                                        */
&Scoped-define FIELDS-IN-QUERY-brwQAR results.stat results.cqarID results.audittype results.agentId results.name results.cityState results.priorAuditDate results.priorAuditScore results.priorAuditType results.StartDate results.endDate results.grade results.auditor results.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQAR   
&Scoped-define SELF-NAME brwQAR
&Scoped-define QUERY-STRING-brwQAR for each results no-lock by results.stat by results.qarID
&Scoped-define OPEN-QUERY-brwQAR open query {&SELF-NAME} for each results no-lock by results.stat by results.qarID.
&Scoped-define TABLES-IN-QUERY-brwQAR results
&Scoped-define FIRST-TABLE-IN-QUERY-brwQAR results


/* Definitions for FRAME fSearch                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fSearch ~
    ~{&OPEN-QUERY-brwQAR}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS txtYear cAuditor cState cStatus brwQAR ~
auditType 
&Scoped-Define DISPLAYED-OBJECTS txtYear cAuditor cState cStatus auditType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse wWin 
FUNCTION refreshBrowse RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD repositionToCurrent wWin 
FUNCTION repositionToCurrent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_File 
       MENU-ITEM m_Configuration LABEL "Configure..."  
       MENU-ITEM m_About        LABEL "About..."      
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_Action 
       MENU-ITEM m_Refresh      LABEL "Refresh"        ACCELERATOR "ALT-R"
       MENU-ITEM m_Select_Year  LABEL "Select Year"    ACCELERATOR "ALT-Y"
       MENU-ITEM m_Export       LABEL "Export"         ACCELERATOR "ALT-E"
       MENU-ITEM m_Documents    LABEL "Documents"      ACCELERATOR "ALT-D"
       MENU-ITEM m_Open         LABEL "Open"           ACCELERATOR "ALT-O"
       MENU-ITEM m_Manage_Auditors LABEL "Manage Auditors" ACCELERATOR "ALT-M"
       RULE
       MENU-ITEM m_New          LABEL "New"            ACCELERATOR "ALT-N"
       MENU-ITEM m_Reschedule   LABEL "Reschedule"     ACCELERATOR "ALT-T"
       MENU-ITEM m_Queued       LABEL "Queue"          ACCELERATOR "ALT-Q"
       MENU-ITEM m_Dequeue      LABEL "Dequeue"        ACCELERATOR "ALT-U"
       MENU-ITEM m_Reassign     LABEL "Reassign"       ACCELERATOR "ALT-A"
       MENU-ITEM m_Edit_Audit_Type LABEL "Edit Audit Type" ACCELERATOR "ALT-P"
       MENU-ITEM m_Cancel       LABEL "Cancel"         ACCELERATOR "ALT-C"
       MENU-ITEM m_Restore      LABEL "Restore"        ACCELERATOR "ALT-S".

DEFINE SUB-MENU m_References 
       MENU-ITEM m_Agents       LABEL "Agents"        .

DEFINE SUB-MENU m_Report 
       MENU-ITEM m_Severity_Changes2 LABEL "Severity Changes"
       MENU-ITEM m_Start_Gap    LABEL "Start Gap"     
       MENU-ITEM m_Audit_Gap    LABEL "Audit Gap"     
       MENU-ITEM m_Score_Analysis LABEL "Score Analysis"
       RULE
       MENU-ITEM m_QAR_by_Agent LABEL "Audit by Agent"
       MENU-ITEM m_Agent_by_Year LABEL "Agent by Year" 
       MENU-ITEM m_Audit_by_Auditor LABEL "Audit by Auditor"
       MENU-ITEM m_Auditor_Scheduling LABEL "Auditor Schedule"
       RULE
       MENU-ITEM m_Best_Practices LABEL "Best Practices"
       MENU-ITEM m_Best_Practices_Pareto LABEL "Best Practices Pareto"
       RULE
       MENU-ITEM m_Findings     LABEL "Findings"      
       MENU-ITEM m_Findings_Pareto LABEL "Findings Pareto"
       RULE
       MENU-ITEM m_State_Aggregation LABEL "State Analysis".

DEFINE SUB-MENU m_Tools 
       MENU-ITEM m_Schedule     LABEL "Scheduling"    .

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_File         LABEL "Module"        
       SUB-MENU  m_Action       LABEL "Ac&tion"       
       SUB-MENU  m_References   LABEL "References"    
       SUB-MENU  m_Report       LABEL "Reports"       
       SUB-MENU  m_Tools        LABEL "Tools"         .

DEFINE MENU POPUP-MENU-brwQAR 
       MENU-ITEM m_View_Documents LABEL "View Documents"
       MENU-ITEM m_Open2        LABEL "Open"          
       MENU-ITEM m_Manage_Auditors2 LABEL "Manage Auditors"
       RULE
       MENU-ITEM m_Reschedule2  LABEL "Reschedule"    
       MENU-ITEM m_Queue        LABEL "Queue"         
       MENU-ITEM m_Dequeue2     LABEL "Dequeue"       
       MENU-ITEM m_Reassign2    LABEL "Reassign"      
       MENU-ITEM m_Edit_Audit_Type2 LABEL "Edit Audit Type"
       MENU-ITEM m_Cancel2      LABEL "Cancel"        
       MENU-ITEM m_Restore2     LABEL "Restore"       
       RULE
       MENU-ITEM m_View_Findings LABEL "View Findings" 
       MENU-ITEM m_View_Best_Practices LABEL "View Best Practices"
       MENU-ITEM m_Severity_Changes LABEL "Severity Changes".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bauditType  NO-FOCUS
     LABEL "Edit Audit Type" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit Audit Type".

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel".

DEFINE BUTTON bDequeue  NO-FOCUS
     LABEL "Dequeue" 
     SIZE 7.2 BY 1.71 TOOLTIP "Dequeue Audit".

DEFINE BUTTON bDocuments  NO-FOCUS
     LABEL "Documents" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bManage  NO-FOCUS
     LABEL "Manage" 
     SIZE 7.2 BY 1.71 TOOLTIP "Manage Auditors".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New Audit".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "View QAR Summary".

DEFINE BUTTON bQueued  NO-FOCUS
     LABEL "Queued" 
     SIZE 7.2 BY 1.71 TOOLTIP "Queue Audit".

DEFINE BUTTON bReassign  NO-FOCUS
     LABEL "Reassign" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reassign".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh".

DEFINE BUTTON bRescheduled  NO-FOCUS
     LABEL "Rescheduled" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reschedule Audit".

DEFINE BUTTON bRestore  NO-FOCUS
     LABEL "Restore" 
     SIZE 7.2 BY 1.71 TOOLTIP "Restore".

DEFINE BUTTON bYear  NO-FOCUS
     LABEL "Year" 
     SIZE 7.2 BY 1.71 TOOLTIP "Select Year".

DEFINE RECTANGLE RECT-42
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 45.6 BY 1.95.

DEFINE RECTANGLE RECT-49
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60.4 BY 1.95.

DEFINE VARIABLE auditType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Audit Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Planned","P",
                     "Queued","Q",
                     "Active","A",
                     "Completed","C",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE txtYear AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14.4 BY .62
     FONT 6 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQAR FOR 
      results SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQAR wWin _FREEFORM
  QUERY brwQAR DISPLAY
      results.stat label "Status" format "x(12)"
 results.cqarID label "Audit ID" width 10
 results.audittype column-label "Audit!Type" format "x(10)" width 7
 results.agentId label "Agent ID" format "x(15)" width 10
 results.name column-label "Agent Name" format "x(100)" width 40
 results.cityState label "City, State" format "x(20)" width 18
 results.priorAuditDate column-label "Prior Audit!Date" format "99/99/9999" width 13
 results.priorAuditScore column-label "Prior Audit!Score%" format "zzz " width 10 
 results.priorAuditType column-label "Prior Audit!Type" format "x(5)"    width 10
 results.StartDate column-label "Scheduled!Audit Date" format "99/99/9999" width 13
 results.endDate column-label "Completed!Audit Date" format "99/99/9999" width 13
 results.grade column-label "Audit!Score%":R format "zzz "       width 11
 results.auditor  label "Auditor" format "x(100)" width 18
 results.comments label 'Notes'   format "x(100)" width 29
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 237.6 BY 22.57
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fSearch
     txtYear AT ROW 1.86 COL 2.6 COLON-ALIGNED NO-LABEL WIDGET-ID 162
     cAuditor AT ROW 2.86 COL 56.6 COLON-ALIGNED WIDGET-ID 152
     cState AT ROW 1.67 COL 56.6 COLON-ALIGNED WIDGET-ID 150
     cStatus AT ROW 2.86 COL 9 COLON-ALIGNED WIDGET-ID 148
     brwQAR AT ROW 4.19 COL 2.8 WIDGET-ID 400
     auditType AT ROW 1.71 COL 30 COLON-ALIGNED WIDGET-ID 26
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.05
         SIZE 241 BY 26.52 WIDGET-ID 100.

DEFINE FRAME fButtons
     bauditType AT ROW 1.57 COL 91.6 WIDGET-ID 216 NO-TAB-STOP 
     bManage AT ROW 1.57 COL 46.4 WIDGET-ID 220 NO-TAB-STOP 
     bCancel AT ROW 1.57 COL 99 WIDGET-ID 190 NO-TAB-STOP 
     bRestore AT ROW 1.57 COL 106.4 WIDGET-ID 202 NO-TAB-STOP 
     bDequeue AT ROW 1.57 COL 76.8 WIDGET-ID 208 NO-TAB-STOP 
     bNew AT ROW 1.57 COL 54.6 WIDGET-ID 204 NO-TAB-STOP 
     bQueued AT ROW 1.57 COL 69.4 WIDGET-ID 206 NO-TAB-STOP 
     bRescheduled AT ROW 1.57 COL 62 WIDGET-ID 214 NO-TAB-STOP 
     bDocuments AT ROW 1.57 COL 31.6 WIDGET-ID 188 NO-TAB-STOP 
     bOpen AT ROW 1.57 COL 39 WIDGET-ID 210 NO-TAB-STOP 
     bExport AT ROW 1.57 COL 24.2 WIDGET-ID 194 NO-TAB-STOP 
     bReassign AT ROW 1.57 COL 84.2 WIDGET-ID 184 NO-TAB-STOP 
     bRefresh AT ROW 1.57 COL 9.4 WIDGET-ID 186 NO-TAB-STOP 
     bYear AT ROW 1.57 COL 16.8 WIDGET-ID 192 NO-TAB-STOP 
     RECT-42 AT ROW 1.48 COL 8.8 WIDGET-ID 2
     RECT-49 AT ROW 1.48 COL 54 WIDGET-ID 218
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER OVERLAY NO-HELP 
         NO-LABELS SIDE-LABELS NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 127 ROW 1.43
         SIZE 113.4 BY 2.67 WIDGET-ID 500.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 26.1
         WIDTH              = 240.8
         MAX-HEIGHT         = 46.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 46.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fButtons:FRAME = FRAME fSearch:HANDLE.

/* SETTINGS FOR FRAME fButtons
   UNDERLINE                                                            */
ASSIGN 
       bCancel:PRIVATE-DATA IN FRAME fButtons     = 
                "X".

ASSIGN 
       bDequeue:PRIVATE-DATA IN FRAME fButtons     = 
                "P".

ASSIGN 
       bNew:PRIVATE-DATA IN FRAME fButtons     = 
                "A".

ASSIGN 
       bQueued:PRIVATE-DATA IN FRAME fButtons     = 
                "Q".

ASSIGN 
       bRescheduled:PRIVATE-DATA IN FRAME fButtons     = 
                "P".

ASSIGN 
       bRestore:PRIVATE-DATA IN FRAME fButtons     = 
                "P".

/* SETTINGS FOR FRAME fSearch
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fButtons:MOVE-BEFORE-TAB-ITEM (cAuditor:HANDLE IN FRAME fSearch)
/* END-ASSIGN-TABS */.

/* BROWSE-TAB brwQAR cStatus fSearch */
ASSIGN 
       brwQAR:POPUP-MENU IN FRAME fSearch             = MENU POPUP-MENU-brwQAR:HANDLE
       brwQAR:ALLOW-COLUMN-SEARCHING IN FRAME fSearch = TRUE
       brwQAR:COLUMN-RESIZABLE IN FRAME fSearch       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQAR
/* Query rebuild information for BROWSE brwQAR
     _START_FREEFORM
open query {&SELF-NAME} for each results no-lock by results.stat by results.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQAR */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin
DO:
  {lib/confirm-exit.i}
  
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  publish "SetCurrentValue" ("QARIDUpdated", "0").
  publish "closewindow".
  publish "ExitApplication".
  RETURN NO-APPLY.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin
DO:
  run windowresized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME auditType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL auditType wWin
ON VALUE-CHANGED OF auditType IN FRAME fSearch /* Audit Type */
DO:
  run doSearch in this-procedure. 
  run DoButtons in this-procedure.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bauditType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bauditType wWin
ON CHOOSE OF bauditType IN FRAME fButtons /* Edit Audit Type */
DO:
  run EditAuditType in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel wWin
ON CHOOSE OF bCancel IN FRAME fButtons /* Cancel */
DO:
 run cancelAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDequeue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDequeue wWin
ON CHOOSE OF bDequeue IN FRAME fButtons /* Dequeue */
DO:
  run DeQueueAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocuments wWin
ON CHOOSE OF bDocuments IN FRAME fButtons /* Documents */
DO:
  if not available results
    then return.

  if not valid-handle(hRepoWindow)
   then run repository.w persistent set hRepoWindow ("Audit", string(results.qarID)).
   else run ShowWindow in hRepoWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wWin
ON CHOOSE OF bExport IN FRAME fButtons /* Export */
DO:
  run ActionExport in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bManage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bManage wWin
ON CHOOSE OF bManage IN FRAME fButtons /* Manage */
DO:
  if available results
   then run dialogmanageauditors.w persistent (results.qarID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew wWin
ON CHOOSE OF bNew IN FRAME fButtons /* New */
DO:
  run NewAudit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen wWin
ON CHOOSE OF bOpen IN FRAME fButtons /* Open */
DO:
  run doSelect in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQueued
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQueued wWin
ON CHOOSE OF bQueued IN FRAME fButtons /* Queued */
DO:
  run QueueAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReassign
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReassign wWin
ON CHOOSE OF bReassign IN FRAME fButtons /* Reassign */
DO:
  run ReAssignAudit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh wWin
ON CHOOSE OF bRefresh IN FRAME fButtons /* Refresh */
DO:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  run ActionRefresh(no,output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRescheduled
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRescheduled wWin
ON CHOOSE OF bRescheduled IN FRAME fButtons /* Rescheduled */
DO:
  run Reschedule.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRestore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRestore wWin
ON CHOOSE OF bRestore IN FRAME fButtons /* Restore */
DO:
  run RestoreAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQAR
&Scoped-define FRAME-NAME fSearch
&Scoped-define SELF-NAME brwQAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON DEFAULT-ACTION OF brwQAR IN FRAME fSearch
DO:
  run doSelect in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON ROW-DISPLAY OF brwQAR IN FRAME fSearch
DO:
  {lib/brw-rowDisplay.i}
 
  if results.score = ?
   then results.score = 0.
   
  /* set the audit's status */
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Status", results.stat, output std-ch).
  results.stat:screen-value in browse {&browse-name} = std-ch.
  /* set the audit's type */
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Type", results.audittype, output std-ch).
  results.audittype:screen-value in browse {&browse-name} = std-ch.
  /* set the audit's err type */
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Type", results.priorAuditType, output std-ch).
  results.priorAuditType:screen-value in browse {&browse-name} = std-ch.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON START-SEARCH OF brwQAR IN FRAME fSearch
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQAR wWin
ON VALUE-CHANGED OF brwQAR IN FRAME fSearch
DO:
 find current results no-error.
 if available results then
   activeAudit = string(results.qarid).
 run DoButtons in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bYear wWin
ON CHOOSE OF bYear IN FRAME fButtons /* Year */
DO:
 Publish "SetCurrentValue" ("QARIDUpdated", "0").
 run dialogYear.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSearch
&Scoped-define SELF-NAME cAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAuditor wWin
ON VALUE-CHANGED OF cAuditor IN FRAME fSearch /* Auditor */
DO:
  run doSearch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState wWin
ON VALUE-CHANGED OF cState IN FRAME fSearch /* State */
DO:
  run doSearch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cStatus wWin
ON VALUE-CHANGED OF cStatus IN FRAME fSearch /* Status */
DO:
  run doSearch in this-procedure. 
  run DoButtons in this-procedure.                                                                                                                                                    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About wWin
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents wWin
ON CHOOSE OF MENU-ITEM m_Agents /* Agents */
DO:
  if valid-handle(hAgentWindow)
   then run ShowWindow in hagentWindow no-error.
   else run referenceagent.w persistent set hAgentWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_by_Year
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_by_Year wWin
ON CHOOSE OF MENU-ITEM m_Agent_by_Year /* Agent by Year */
DO:
  run wagentreport.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Auditor_Scheduling
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Auditor_Scheduling wWin
ON CHOOSE OF MENU-ITEM m_Auditor_Scheduling /* Auditor Schedule */
DO:
  run wauditorscheduling.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Audit_by_Auditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Audit_by_Auditor wWin
ON CHOOSE OF MENU-ITEM m_Audit_by_Auditor /* Audit by Auditor */
DO:
  run wauditsbyauditor.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Audit_Gap
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Audit_Gap wWin
ON CHOOSE OF MENU-ITEM m_Audit_Gap /* Audit Gap */
DO:
  run wauditgapreport.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Best_Practices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Best_Practices wWin
ON CHOOSE OF MENU-ITEM m_Best_Practices /* Best Practices */
DO:
  find current results no-error.
  if available results then
  do:
    Publish "SetCurrentValue" ("QARID", 0).
    Publish "SetCurrentValue" ("State", "").
    Publish "SetCurrentValue" ("Auditor", "").
    Publish "SetCurrentValue" ("Agent", "").
    Publish "SetCurrentValue" ("Year",0).
    publish "SetCurrentValue" ("AgentName" , "").
  end.
  run wbp.w persistent .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Best_Practices_Pareto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Best_Practices_Pareto wWin
ON CHOOSE OF MENU-ITEM m_Best_Practices_Pareto /* Best Practices Pareto */
DO:
  run wbppareto.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Cancel wWin
ON CHOOSE OF MENU-ITEM m_Cancel /* Cancel */
DO:
  run cancelAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Cancel2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Cancel2 wWin
ON CHOOSE OF MENU-ITEM m_Cancel2 /* Cancel */
DO:
  run cancelAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configuration
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configuration wWin
ON CHOOSE OF MENU-ITEM m_Configuration /* Configure... */
DO:
  run dialogqamconfig.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dequeue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dequeue wWin
ON CHOOSE OF MENU-ITEM m_Dequeue /* Dequeue */
DO:
  run DeQueueAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dequeue2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dequeue2 wWin
ON CHOOSE OF MENU-ITEM m_Dequeue2 /* Dequeue */
DO:
  run DeQueueAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Documents wWin
ON CHOOSE OF MENU-ITEM m_Documents /* Documents */
DO:
  apply "CHOOSE" to bDocuments in frame fButtons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_Audit_Type
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_Audit_Type wWin
ON CHOOSE OF MENU-ITEM m_Edit_Audit_Type /* Edit Audit Type */
DO:
  run EditAuditType in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit_Audit_Type2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit_Audit_Type2 wWin
ON CHOOSE OF MENU-ITEM m_Edit_Audit_Type2 /* Edit Audit Type */
DO:
  run EditAuditType in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit wWin
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export wWin
ON CHOOSE OF MENU-ITEM m_Export /* Export */
DO:
  run ActionExport in this-procedure .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Findings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Findings wWin
ON CHOOSE OF MENU-ITEM m_Findings /* Findings */
DO:
  Publish "SetCurrentValue" ("QARID", 0).
  Publish "SetCurrentValue" ("State", "").
  Publish "SetCurrentValue" ("Auditor", "").
  Publish "SetCurrentValue" ("Agent", "").
  Publish "SetCurrentValue" ("Year",0).

  run wfinding.w persistent .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Findings_Pareto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Findings_Pareto wWin
ON CHOOSE OF MENU-ITEM m_Findings_Pareto /* Findings Pareto */
DO:
  run wfindingpareto.w persistent .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Manage_Auditors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Manage_Auditors wWin
ON CHOOSE OF MENU-ITEM m_Manage_Auditors /* Manage Auditors */
DO:
  if available results
   then run dialogmanageauditors.w persistent (results.qarID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Manage_Auditors2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Manage_Auditors2 wWin
ON CHOOSE OF MENU-ITEM m_Manage_Auditors2 /* Manage Auditors */
DO:
  if available results
   then run dialogmanageauditors.w persistent (results.qarID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_New wWin
ON CHOOSE OF MENU-ITEM m_New /* New */
DO:
  run NewAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open wWin
ON CHOOSE OF MENU-ITEM m_Open /* Open */
DO:
  run doSelect in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open2 wWin
ON CHOOSE OF MENU-ITEM m_Open2 /* Open */
DO:
  run doSelect in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_QAR_by_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_QAR_by_Agent wWin
ON CHOOSE OF MENU-ITEM m_QAR_by_Agent /* Audit by Agent */
DO:
  run QarbyAgent in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Queue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Queue wWin
ON CHOOSE OF MENU-ITEM m_Queue /* Queue */
DO:
  run QueueAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Queued
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Queued wWin
ON CHOOSE OF MENU-ITEM m_Queued /* Queue */
DO:
  run QueueAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reassign
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reassign wWin
ON CHOOSE OF MENU-ITEM m_Reassign /* Reassign */
DO:
  run ReAssignAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reassign2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reassign2 wWin
ON CHOOSE OF MENU-ITEM m_Reassign2 /* Reassign */
DO:
  run ReAssignAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Refresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Refresh wWin
ON CHOOSE OF MENU-ITEM m_Refresh /* Refresh */
DO:
  run ActionRefresh in this-procedure (no,output std-lo). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reschedule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reschedule wWin
ON CHOOSE OF MENU-ITEM m_Reschedule /* Reschedule */
DO:
  run Reschedule.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reschedule2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reschedule2 wWin
ON CHOOSE OF MENU-ITEM m_Reschedule2 /* Reschedule */
DO:
  run Reschedule.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Restore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Restore wWin
ON CHOOSE OF MENU-ITEM m_Restore /* Restore */
DO:
  run RestoreAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Restore2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Restore2 wWin
ON CHOOSE OF MENU-ITEM m_Restore2 /* Restore */
DO:
  run RestoreAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Schedule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Schedule wWin
ON CHOOSE OF MENU-ITEM m_Schedule /* Scheduling */
DO:
  if valid-handle(hregularqarWindow)
   then run ShowWindow in hregularqarWindow no-error.
   else run wscheduling.w persistent set hregularqarWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Score_Analysis
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Score_Analysis wWin
ON CHOOSE OF MENU-ITEM m_Score_Analysis /* Score Analysis */
DO:
  run wscoreanalysis.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Select_Year
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Select_Year wWin
ON CHOOSE OF MENU-ITEM m_Select_Year /* Select Year */
DO:
  run dialogyear.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Severity_Changes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Severity_Changes wWin
ON CHOOSE OF MENU-ITEM m_Severity_Changes /* Severity Changes */
DO:
  find current results no-error.
  if available results then
  do:
    Publish "SetCurrentValue" ("QARID", results.qarid).
    Publish "SetCurrentValue" ("State", results.stateID).
    Publish "SetCurrentValue" ("Auditor", results.uid).
    Publish "SetCurrentValue" ("Agent", results.name).
    Publish "SetCurrentValue" ("AgentID", results.agentID).
    Publish "SetCurrentValue" ("Year",integer(entry(2, txtYear:screen-value in frame fsearch , ":"))).
  end.
  run wseveritychanges.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Severity_Changes2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Severity_Changes2 wWin
ON CHOOSE OF MENU-ITEM m_Severity_Changes2 /* Severity Changes */
DO:
  Publish "SetCurrentValue" ("QARID", 0).
  Publish "SetCurrentValue" ("State", "").
  Publish "SetCurrentValue" ("Auditor", "").
  Publish "SetCurrentValue" ("Agent", "").
  Publish "SetCurrentValue" ("Year",0).
  run wseveritychanges.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Start_Gap
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Start_Gap wWin
ON CHOOSE OF MENU-ITEM m_Start_Gap /* Start Gap */
DO:
  run wstartgapreport.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_State_Aggregation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_State_Aggregation wWin
ON CHOOSE OF MENU-ITEM m_State_Aggregation /* State Analysis */
DO:
  run wStateDashboard.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Best_Practices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Best_Practices wWin
ON CHOOSE OF MENU-ITEM m_View_Best_Practices /* View Best Practices */
DO:
  find current results no-error.
  if available results then
  do:
    Publish "SetCurrentValue" ("QARID", results.qarid).
    Publish "SetCurrentValue" ("State", results.stateID).
    Publish "SetCurrentValue" ("Auditor", results.uid).
    Publish "SetCurrentValue" ("Agent", results.agentID).
    Publish "SetCurrentValue" ("Year",integer(entry(2, txtYear:screen-value in frame fsearch , ":"))).
    publish "SetCurrentValue" ("AgentName" , results.name).
  end.
 run wbp.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Documents wWin
ON CHOOSE OF MENU-ITEM m_View_Documents /* View Documents */
DO:
  apply "CHOOSE" to bDocuments in frame fButtons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Findings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Findings wWin
ON CHOOSE OF MENU-ITEM m_View_Findings /* View Findings */
DO:
  find current results no-error.
  if available results then
  do:
    Publish "SetCurrentValue" ("QARID", results.qarid).
    Publish "SetCurrentValue" ("State", results.stateID).
    Publish "SetCurrentValue" ("Auditor", results.uid).
    Publish "SetCurrentValue" ("Agent", results.name).
    Publish "SetCurrentValue" ("AgentID", results.agentID).
    Publish "SetCurrentValue" ("Year",integer(entry(2, txtYear:screen-value in frame fsearch , ":"))).
  end.
  run wfinding.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
subscribe to "OpenMaintainDocs" anywhere.
subscribe to "MainWindowSensitive" anywhere.
subscribe to "ActionChanged" anywhere.
subscribe to "doYear" anywhere.
subscribe to "SearchYear" anywhere.
subscribe to "ActionWindowForAgent" anywhere.
/*subscribe to "checkOpenQar" anywhere.*/

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/set-button.i &frame-name=fButtons &label="Documents"   &image="images/attach.bmp" &inactive="images/attach-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Year"        &image="images/cal.bmp"    &inactive="images/cal-i.bmp"    &toggle=false}
{lib/set-button.i &frame-name=fButtons &label="Refresh"     &toggle=false}
{lib/set-button.i &frame-name=fButtons &label="Cancel"      &image="images/cancel.bmp" &inactive="images/cancel-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Export"}
{lib/set-button.i &frame-name=fButtons &label="Restore"     &image="images/undo.bmp"   &inactive="images/undo-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Reassign"    &image="images/usergo.bmp" &inactive="images/usergo-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="New"         &toggle=false}
{lib/set-button.i &frame-name=fButtons &label="Queued"      &image="images/start.bmp"  &inactive="images/start-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="DeQueue"     &image="images/stop.bmp"   &inactive="images/stop-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Open"        &image="images/open.bmp"   &inactive="images/open-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Manage"      &image="images/users.bmp"  &inactive="images/users-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Rescheduled" &image="images/clock.bmp"  &inactive="images/clock-i.bmp"}
{lib/set-button.i &frame-name=fButtons &label="Audittype"   &image="images/update.bmp" &inactive="images/update-i.bmp"}
setButtons().

assign
  wWin:min-height-pixels = wWin:height-pixels
  wWin:min-width-pixels = wWin:width-pixels
  wWin:max-height-pixels = session:height-pixels
  wWin:max-width-pixels = session:width-pixels
  minSearchFrameWidth = frame fSearch:width-pixels
  .

publish "GetSearchYear" (output pYear).
txtYear = "Year:" + " " + pYear.

publish "GetAuditors"(output table auditor).
cAuditor:delimiter = "," .
for each auditor no-lock:
  auditorpair  =  auditorpair + "," + string(auditor.name) + "," + string(auditor.UID) .
end.
auditorpair = trim(auditorpair , "|" ).
cAuditor:list-item-pairs =  "ALL,ALL" + auditorpair.

{lib/get-state-list.i &combo=cState &addAll=true}

/* set the audit type */
std-ch = "".
publish "GetSysPropList" ("QAR", "Audit", "Type", output std-ch).
if std-ch > ""
 then std-ch = "ALL,ALL," + replace(std-ch, {&msg-dlm}, ",").
 else std-ch = "ALL,ALL".
audittype:list-item-pairs = std-ch.

assign
 cAuditor:inner-lines = 10
 cStatus:inner-lines  = 10.

publish "GetStatus" (output pStatus).

open query brwQAR for each results where false.

{lib/win-main.i}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  assign cStatus:screen-value = pStatus
         cState:screen-value = "ALL"
         cAuditor:screen-value = "ALL"
         auditType:screen-value = audittype:entry(1).
         .
  run doSearch in this-procedure.
  apply 'value-changed' to brwQAR.
  run DoButtons in this-procedure.
  run windowResized in this-procedure.

  wWin:window-state = 3. /* restored */
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionExport wWin 
PROCEDURE ActionExport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwQAR:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
   
  define variable cReportName as character no-undo.
  cReportName = "QualityManagementAssurance-"  + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","").
    
  std-ch = "".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), cReportName).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), cReportName).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRefresh wWin 
PROCEDURE ActionRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAction as logical.
 def output parameter pError as logical init true.

 publish "RefreshData"(pAction).
 run doSearch in this-procedure.
  {&OPEN-BROWSERS-IN-QUERY-fButtons}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent wWin 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditDetailReport wWin 
PROCEDURE AuditDetailReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pFile as char no-undo.
def var pSuccess as logical no-undo.

publish "AuditDetailReport"(input results.qarId,
                            output pfile,
                            output pSuccess).

if not pSuccess then
  return.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelAudit wWin 
PROCEDURE cancelAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pReason as char .
def var pCancel as logical.
def var pSuccess as logical.

run dialogcancel.w (input results.qarId,
                    input results.agentId,
                    input results.name, 
                    output pReason,
                    output pCancel).
if pCancel then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.
publish "cancelAudit" (input results.qarId,
                       input pReason,
                       output pSuccess).
if not pSuccess then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.
Publish "SetCurrentValue" ("QARIDUpdated", string(results.qarId)).
run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}
run DoButtons.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenQar wWin 
PROCEDURE checkOpenQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as character.
def output parameter qaropen as logical.
def output parameter qarsummaryhandle as handle.

 find first openQars
  where openQars.QarId = integer(pAuditID) no-error.
 if available openQars and valid-handle(openQars.hInstance)
  then
 do:
qaropen = yes.
qarsummaryhandle = openQars.hInstance.
 end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeQueueAudit wWin 
PROCEDURE DeQueueAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pReason as char .
def var pCancel as logical.
def var pSuccess as logical.

run dialogdequeueaudit.w (input results.qarId,
                          input results.agentId,
                          input results.name, 
                          output pReason,
                          output pCancel).
if pCancel then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.
publish "DequeueAudit" (input results.qarId,
                        input preason,
                        output pSuccess).
if not pSuccess then
do:
 Publish "SetCurrentValue" ("QARIDUpdated", "0").
 return.
end.
Publish "SetCurrentValue" ("QARIDUpdated", string(results.qarId)).
run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}
run DoButtons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DoButtons wWin 
PROCEDURE DoButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find current results no-error.
  
  if available results
   then
    assign
      /* menu items */
      menu-item m_Export:sensitive in menu m_action = true
      menu-item m_Documents:sensitive in menu m_action = true
      menu-item m_Open:sensitive in menu m_action = true
      menu-item m_Manage_Auditors:sensitive in menu m_action = true
      /* ----- */
      menu-item m_Reschedule:sensitive in menu m_action = (results.stat = "P")
      menu-item m_queued:sensitive in menu m_action = (results.stat = "P")
      menu-item m_Dequeue:sensitive in menu m_action = (results.stat = "Q")
      menu-item m_reassign:sensitive in menu m_action = (results.stat = "P" or results.stat = "Q")
      menu-item m_edit_audit_type:sensitive in menu m_action = (results.stat = "P")
      menu-item m_cancel:sensitive in menu m_action = (results.stat <> "C" and results.stat <> "X")
      menu-item m_restore:sensitive in menu m_action = (results.stat = "X")
      /* popup menu */
      menu-item m_View_Documents:sensitive in menu POPUP-MENU-brwQAR = true
      menu-item m_View_Findings:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "A" or results.stat = "C")
      menu-item m_View_Best_Practices:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "A" or results.stat = "C")
      menu-item m_Severity_Changes:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "A" or results.stat = "C")
      
      menu-item m_Open2:sensitive in menu POPUP-MENU-brwQAR = true
      menu-item m_Manage_Auditors2:sensitive in menu POPUP-MENU-brwQAR = true
      /* ----- */
      menu-item m_Reschedule2:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "P")
      menu-item m_queue:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "P")
      menu-item m_Dequeue2:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "Q")
      menu-item m_reassign2:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "P" or results.stat = "Q")
      menu-item m_edit_audit_type2:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "P")
      menu-item m_cancel2:sensitive in menu POPUP-MENU-brwQAR = (results.stat <> "C" and results.stat <> "X")
      menu-item m_restore2:sensitive in menu POPUP-MENU-brwQAR = (results.stat = "X")
     
     
      /* buttons */
      bExport:sensitive in frame fButtons = true
      bDocuments:sensitive in frame fButtons = true
      bOpen:sensitive in frame fButtons = true
      bManage:sensitive in frame fButtons = true
      /* ----- */
      bRescheduled:sensitive in frame fButtons = (results.stat = "P")
      bQueued:sensitive in frame fButtons = (results.stat = "P")
      bDequeue:sensitive in frame fButtons = (results.stat = "Q")
      bReassign:sensitive in frame fButtons = (results.stat = "P" or results.stat = "Q")
      bAudittype:sensitive in frame fButtons = (results.stat = "P")
      bCancel:sensitive in frame fButtons = (results.stat <> "C" and results.stat <> "X")
      bRestore:sensitive in frame fButtons = (results.stat = "X")
      .
   else
    assign
      /* menu items */
      menu-item m_Export:sensitive in menu m_action = false
      menu-item m_Documents:sensitive in menu m_action = true
      menu-item m_Open:sensitive in menu m_action = false
      menu-item m_Manage_Auditors:sensitive in menu m_action = false
      /* ----- */
      menu-item m_Reschedule:sensitive in menu m_action = false
      menu-item m_queued:sensitive in menu m_action = false
      menu-item m_Dequeue:sensitive in menu m_action = false
      menu-item m_reassign:sensitive in menu m_action = false
      menu-item m_edit_audit_type:sensitive in menu m_action = false
      menu-item m_cancel:sensitive in menu m_action = false
      menu-item m_restore:sensitive in menu m_action = false
      /* popup menu */
      menu-item m_View_Documents:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_View_Findings:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_View_Best_Practices:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_Severity_Changes:sensitive in menu POPUP-MENU-brwQAR = false
      
      menu-item m_Open2:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_Manage_Auditors2:sensitive in menu POPUP-MENU-brwQAR = false
      /* ----- */
      menu-item m_Reschedule2:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_queue:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_Dequeue2:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_reassign2:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_edit_audit_type2:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_cancel2:sensitive in menu POPUP-MENU-brwQAR = false
      menu-item m_restore2:sensitive in menu POPUP-MENU-brwQAR = false           
      
      /* buttons */
      bExport:sensitive in frame fButtons = false
      bDocuments:sensitive in frame fButtons = false
      bOpen:sensitive in frame fButtons = false
      bManage:sensitive in frame fButtons = false
      /* ----- */
      bRescheduled:sensitive in frame fButtons = false
      bQueued:sensitive in frame fButtons = false    
      bDequeue:sensitive in frame fButtons = false 
      bReassign:sensitive in frame fButtons = false
      bAudittype:sensitive in frame fButtons = false
      bCancel:sensitive in frame fButtons = false
      bRestore:sensitive in frame fButtons = false
      .
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSearch wWin 
PROCEDURE doSearch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwQAR.
  do with frame fSearch:
    publish "SearchAudits" (input  cStatus:screen-value,
                            input  cState:screen-value,
                            input  cAuditor:screen-value,
                            input auditType:screen-value,
                            output table ttresults).
                            
    empty temp-table results.
    for each ttResults:
        create results.
        buffer-copy ttResults to results.
        assign results.cqarid = string (ttresults.qarid).
         
       
        if ttresults.stat = "P" or ttresults.stat = "Q" or ttresults.stat = "X" then
            assign
               results.startdate = if ttresults.auditstartdate <> ? then date (ttresults.auditstartdate) else date (ttresults.schedstartdate)
               results.enddate = date ("")
               .   
      else if ttresults.stat = "A" then
       assign
          results.startdate = date (ttresults.auditstartdate)
          results.enddate = date ("")
          . 
      else if ttresults.stat = "C" then
       assign
         results.startdate = date (ttresults.auditstartdate)
         results.enddate = date (ttresults.auditfinishdate)
         . 
    end.

    assign
      std-ch = dataSortBy
      dataSortBy = ""
      .
    run sortData in this-procedure (std-ch).
    run DoButtons in this-procedure.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSelect wWin 
PROCEDURE doSelect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
activeAudit = string(results.qarID).
activeRowid = rowid(results).

if not available results 
 then return.

find first results where results.QarId = integer(activeAudit) no-error.
if available results then
do:              
  create ttaudit.
  assign
    ttaudit.QarID  =      results.QarID  
    ttaudit.agentID =     results.agentID  
    ttaudit.stat =        results.stat
    ttaudit.auditScore =  results.auditScore
    ttaudit.name =        results.name 
    ttaudit.addr =        results.addr 
    ttaudit.city =        results.city 
    ttaudit.state =       results.state 
    ttaudit.zip =         results.zip 
    .
end.
publish "checkOpenQar" (input results.QarId,
                        output openqar,
                        output qarsummaryhandle ).
 /*find first openQars
 where openQars.QarId = results.QarId no-error.
 if available openQars and valid-handle(openQars.hInstance)*/
if openqar
 then
  do:
     run ViewWindow in qarsummaryhandle no-error.
    empty temp-table ttaudit.
    return.
  end.
else
  do:
    {lib/pbshow.i "''"}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
    if results.audittype = "U" then
      run QURSummary.w persistent ( input table ttaudit ).
    else
      run QARSummary.w persistent ( input table ttaudit ).
     run ViewWindow in qarsummaryhandle no-error.   
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
    {lib/pbhide.i}
    empty temp-table ttaudit.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doYear wWin 
PROCEDURE doYear :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter sYear as int.
def output parameter table for ttresults.

publish "loadServerAudits" (input integer(sYear)).
run doSearch in this-procedure.
txtYear:screen-value in frame fSearch = "Year:" + " " + string(sYear).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EditAuditType wWin 
PROCEDURE EditAuditType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def var pReason as char .
def var pCancel as logical.
def var pSuccess as logical.
def var pAuditType as char.
def var pErrType as char.
def var pAuditID as char.
def var pNewAuditor as char.

pAuditType = string(results.auditType).
pErrType =  string(results.errtype).
pAuditID = string(results.qarID).
do with frame fSearch:
end.

run dialogedittype.w  (input pAuditID,
                       input results.agentID,
                       input-output pAuditType,
                       input-output pErrType,
                       output pReason,
                       output pCancel).

if pCancel then
do:
  publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.

publish "EditAuditType" (input int(pAuditID) ,
                         input pAuditType,
                         input perrtype,
                         input pReason,
                         input "QAM",
                         output pSuccess).
                         
Publish "SetCurrentValue" ("QARIDUpdated", string(results.qarId)).

if not pSuccess then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.

run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY txtYear cAuditor cState cStatus auditType 
      WITH FRAME fSearch IN WINDOW wWin.
  ENABLE txtYear cAuditor cState cStatus brwQAR auditType 
      WITH FRAME fSearch IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fSearch}
  ENABLE bauditType RECT-42 bManage RECT-49 bCancel bRestore bDequeue bNew 
         bQueued bRescheduled bDocuments bOpen bExport bReassign bRefresh bYear 
      WITH FRAME fButtons IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fButtons}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MainWindowSensitive wWin 
PROCEDURE MainWindowSensitive :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{&window-name}:sensitive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAudit wWin 
PROCEDURE NewAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pSuccess as logical.
def var pRowId as char.
def var selectedrow as int.
def var pAuditId as int.
def var totalrows as int.
def var myrow as int.

run dialognewaudit.w(output pAuditId,
                     output pSuccess).

if pSuccess then
do:

  run doSearch in this-procedure.
  {&OPEN-BROWSERS-IN-QUERY-fButtons}
 
end.
else
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainDocs wWin 
PROCEDURE OpenMaintainDocs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditId as int.
def input parameter pWindow as handle.
find openDocs 
  where openDocs.QarID = pAuditId no-error.
if not available openDocs 
 then
  do:
    create openDocs.
    openDocs.QarID = pAuditId.
  end.
openDocs.hInstance = pWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainQar wWin 
PROCEDURE OpenMaintainQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditId as int.
def input parameter pWindow as handle.

find openQars 
  where openQars.QarID = pAuditId no-error.
if not available openQars 
 then
  do:
    create openQars.
    openQars.QarID = results.QarID.
  end.
openQars.hInstance = pWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QarbyAgent wWin 
PROCEDURE QarbyAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
Publish "SetCurrentValue" ("ID", "").
run wqarbyagent.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QueueAudit wWin 
PROCEDURE QueueAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pSuccess as logical.
def var pMsg as char.
def var updatedrecordid as char.
def var pRowId as char.
def var selectedrow as int.
def var lChoice as logical.
      
message "The audit will be queued. " skip
    "Continue?"
   view-as alert-box question buttons ok-cancel update lChoice.

if not lChoice    
 then    
  return. 
 
{&window-name}:sensitive = false.

find current results no-error.
if available results then
do:
  publish "GetQueuedAnswers" (input no,
                              input string(results.qarid)).
  publish "QueueAudit"(input string(results.qarid),
                       input results.uid,
                       output pSuccess,
                       output pMsg).
                       
  if not pSuccess then
  do:
    {&window-name}:sensitive = true.
    return .
  end.
  else
  do:
    MESSAGE "Audit successfully queued"
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
    {&window-name}:sensitive = true.
  end.
  Publish "SetCurrentValue" ("QARIDUpdated", string(results.qarid)).
end.
if not pSuccess then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.
 
run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}
publish "EmptyBackgroundTable". 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReAssignAudit wWin 
PROCEDURE ReAssignAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pReason as char .
def var pCancel as logical.
def var pSuccess as logical.
def var pNewAuditor as char.
def var updatedrecordid as char.
def var pRowId as char.
def var selectedrow as int.

if available results then
run dialogreassignaudit.w (input results.qarId,
                          input results.agentId,
                          input results.name,
                          input (results.UID + "," + results.auditor),
                          output pNewAuditor,
                          output pReason,
                          output pCancel).

if pCancel then
do:
  publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end. 

updatedrecordid = string(results.qarId) .
Publish "SetCurrentValue" ("QARIDUpdated", string(updatedrecordid)).

do selectedrow = 1 to brwQar:num-iterations in frame fsearch:
  if brwQar:is-row-selected(selectedrow) then leave.
end.

publish "ReassignAudit" (input results.qarId,
                         input pNewAuditor,
                         input preason,
                         output pSuccess).
if not pSuccess then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.

run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Reschedule wWin 
PROCEDURE Reschedule :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pReason as char .
def var pCancel as logical.
def var pSuccess as logical.
def var pNewAuditor as char.
def var updatedrecordid as char.
def var pRowId as char.
def var selectedrow as int.
def var rescheduledate as datetime.

if available results then
run dialogrescheduleaudit.w (input results.qarId,
                             input results.agentId,
                             input results.name,
                             input results.startDate,
                             output rescheduledate,
                             output pReason,
                             output pCancel).

if pCancel then
do:
  publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end. 

updatedrecordid = string(results.qarId) .
Publish "SetCurrentValue" ("QARIDUpdated", string(updatedrecordid)).

do selectedrow = 1 to brwQar:num-iterations in frame fsearch:
  if brwQar:is-row-selected(selectedrow) then leave.
end.
publish "RescheduleAudit" (input results.qarId,
                           input rescheduledate,
                           input preason,
                           output pSuccess).
if not pSuccess then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.

run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RestoreAudit wWin 
PROCEDURE RestoreAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pReason as char .
def var pCancel as logical.
def var pSuccess as logical.

do with frame fSearch:
end.
run dialogrestore.w (input results.qarId,
                    input results.agentId,
                    input results.name, 
                    output pReason,
                    output pCancel).

if pCancel then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.
  
publish "RestoreAudit" (input results.qarId,
                        input pReason,
                        output pSuccess).
Publish "SetCurrentValue" ("QARIDUpdated", string(results.qarId)).
if not pSuccess then
do:
  Publish "SetCurrentValue" ("QARIDUpdated", "0").
  return.
end.

run doSearch in this-procedure.
{&OPEN-BROWSERS-IN-QUERY-fButtons}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SearchYear wWin 
PROCEDURE SearchYear :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pYear as char.
pYear = txtYear:screen-value in frame fSearch.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &post-by-clause=" + ' by results.qarid' "}
  std-ch = string(num-results("{&browse-name}")) + " record(s) found as of " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM").
  status default std-ch in window {&window-name}.
  status input std-ch in window {&window-name}.
  run DoButtons.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
frame fSearch:width-pixels = wWin:width-pixels.
frame fSearch:virtual-width-pixels = wWin:width-pixels.
frame fSearch:height-pixels = wWin:height-pixels.
frame fSearch:virtual-height-pixels = wWin:height-pixels.
/* fSearch components */
browse brwQAR:width-pixels = frame fSearch:width-pixels - 16.

if {&window-name}:width-pixels > frame fSearch:width-pixels 
 then
  do: 
    frame fSearch:width-pixels = {&window-name}:width-pixels.
    frame fSearch:virtual-width-pixels = {&window-name}:width-pixels.        
  end.
else
 do:
   frame fSearch:virtual-width-pixels = {&window-name}:width-pixels.
   frame fSearch:width-pixels = {&window-name}:width-pixels.
        /* das: For some reason, shrinking a window size MAY cause the horizontal
           scroll bar.  The above sequence of widget setting should resolve it,
           but it doesn't every time.  So... */  
 end.
browse brwQAR:height-pixels = frame fSearch:height-pixels - 74.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse wWin 
FUNCTION refreshBrowse RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
close query brwQAR.
open query brwQAR for each results by results.auditStartDate.

RETURN "".   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION repositionToCurrent wWin 
FUNCTION repositionToCurrent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
if available results and rowid(results) = activeRowid 
 then return true.
reposition brwQar to rowid activeRowid no-error.
get next brwQar.
return available results.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

