&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogedittype.w

  Description: Create and start new audit .

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: archana gupta

  Created: 10/03/17
  
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input parameter pAuditID as char no-undo.
def input parameter pAgentID as char no-undo.
def input-output parameter pAuditType as char no-undo.
def input-output parameter pErrType as char no-undo.
def output parameter pReason as char no-undo.
def output parameter pCancel as logical no-undo.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/agent.i}

def var agentpair as char.
def var auditorpair as char.
def var pAuditor as char.
def var pReviewdate as date.
def var pYear as char.
def var states as char.
def var tErrtype as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAgent tqarID auditType errType tReason ~
bCreate BtnCancel 
&Scoped-Define DISPLAYED-OBJECTS tAgent tqarID auditType errType tReason 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCreate AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE auditType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Audit Type" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE errType AS CHARACTER FORMAT "X(256)" 
     LABEL "Type" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tReason AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 99 BY 3.24 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 99 BY 1 NO-UNDO.

DEFINE VARIABLE tqarID AS CHARACTER FORMAT "X(256)":U 
     LABEL "QAR ID" 
     VIEW-AS FILL-IN 
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-41
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 115.4 BY 7.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tAgent AT ROW 3.14 COL 15 COLON-ALIGNED WIDGET-ID 30
     tqarID AT ROW 1.95 COL 15 COLON-ALIGNED WIDGET-ID 6
     auditType AT ROW 4.33 COL 15 COLON-ALIGNED WIDGET-ID 26
     errType AT ROW 4.33 COL 43 COLON-ALIGNED WIDGET-ID 28
     tReason AT ROW 5.52 COL 17 NO-LABEL WIDGET-ID 2
     bCreate AT ROW 9.57 COL 45 WIDGET-ID 10
     BtnCancel AT ROW 9.57 COL 61 WIDGET-ID 24
     "Reason:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 5.52 COL 8.4 WIDGET-ID 20
     RECT-41 AT ROW 1.48 COL 2.6 WIDGET-ID 12
     SPACE(1.39) SKIP(1.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Edit Audit Type"
         CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-41 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tAgent:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tqarID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tReason:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Edit Audit Type */
DO:
  pcancel = true.
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME auditType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL auditType Dialog-Frame
ON VALUE-CHANGED OF auditType IN FRAME Dialog-Frame /* Audit Type */
DO:
  if audittype:screen-value = "E" then
  do:
    errtype:visible = true.
    errtype:screen-value = if pErrType = "" then entry(2, errtype:list-item-pairs, errType:delimiter) else pErrType.
  end.
  else
  do:
    errtype:visible = false.
    errtype:screen-value = entry(2,errtype:list-item-pairs).
  end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCreate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCreate Dialog-Frame
ON CHOOSE OF bCreate IN FRAME Dialog-Frame /* OK */
DO:
  pAuditType = auditType:screen-value in frame {&frame-name}.
  
  IF pAuditType = "E"
   THEN
    DO:
      find agent where agent.agentID = pAgentID no-error.
      if avail agent
       then
        do:
          if agent.stat <> "P"
           then
            do:
              if errtype:screen-value in frame  dialog-frame = "P" 
               then
                do: 
                  MESSAGE "ERR type can only be presign for a prospective agent."
                        VIEW-AS ALERT-BOX INFO BUTTONS OK.
                  apply "entry" to errtype in frame dialog-frame.
                  return no-apply.
                end.
            end.
           else
            DO:
              if errtype:screen-value in frame  dialog-frame <> "P"
               then
                do: 
                   MESSAGE "ERR Type can only be presign for a prospective agent."
                        VIEW-AS ALERT-BOX INFO BUTTONS OK.
                   apply "entry" to errtype in frame dialog-frame.
                   return no-apply.
                end.
            end.
        end.
    END.
  
 pErrType  = errType:screen-value in frame {&frame-name}.
 pReason   = tReason:screen-value in frame {&frame-name}.

 if pAuditType = "Q" then
   pErrType = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  pCancel = true.
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* set the audit type */
std-ch = "".
publish "GetSysPropList" ("QAR", "Audit", "Type", output std-ch).
if std-ch = ""
 then std-ch = "ALL,ALL".
audittype:list-item-pairs = replace(std-ch, {&msg-dlm}, ",").
/* set the err type */
std-ch = "".
publish "GetSysPropList" ("QAR", "ERR", "Type", output std-ch).
if std-ch = ""
 then std-ch = "ALL,ALL".
errtype:list-item-pairs = replace(std-ch, {&msg-dlm}, ",").


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  publish "GetAgents" (output table agent).

  RUN enable_UI.

  for first agent no-lock
      where agent.agentID = pAgentID:
      
    tAgent:screen-value = agent.name + " (" + agent.agentID + ")".
  end.
  tQarID:screen-value = pAuditID.
  auditType:screen-value = pAuditType.

  if pErrtype = ""
   then errtype:visible = false.

  if pErrtype > ""
   then
    do:
     errType:screen-value = pErrType.
     errtype:visible = true.
    end.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAgent tqarID auditType errType tReason 
      WITH FRAME Dialog-Frame.
  ENABLE tAgent tqarID auditType errType tReason bCreate BtnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

