&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wfindings.w
   Window of FINDINGS across qar records
   @author anjly
   @modified : 09/24/21  SA  Task 86696 Defects raised by david
               11/12/21  SA  Task#:86696 added logic to open 
	                      findings report 	
   @modified : 02/02/2022  SD  Task #91131   Added logic and new parameters "tyear,tauditor,cauditorname,
                                             tstate,tstatedesc,tAgent,tagentname,tFinding" to fetch data according 
                                             to the parameters.
 */

CREATE WIDGET-POOL.
{lib/winlaunch.i}
{tt/findingpareto.i}
{tt/findingpareto.i &tableAlias="ttfindingpareto"}
{tt/agent.i}
{tt/auditor.i}
{tt/state.i}
{lib/std-def.i}

def var tqarID as int no-undo.
def var tstate as char no-undo.
def var tagent as char no-undo.
def var tauditor as char no-undo.
def var tyear as char no-undo.
def var tfinding as char no-undo.
def var auditorpair as char no-undo.
def var agentpair as char no-undo.
def var yearpair as char no-undo.
def var findingparetorecord as int no-undo.

def var cauditorname as char no-undo.
def var tstatedesc as char no-undo.
def var tagentname as char no-undo. 
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwFindingsPareto

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES findingpareto

/* Definitions for BROWSE brwFindingsPareto                             */
&Scoped-define FIELDS-IN-QUERY-brwFindingsPareto findingpareto.cnt findingpareto.pct findingpareto.Priority findingpareto.type findingpareto.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFindingsPareto   
&Scoped-define SELF-NAME brwFindingsPareto
&Scoped-define QUERY-STRING-brwFindingsPareto FOR EACH findingpareto by findingpareto.cnt descending
&Scoped-define OPEN-QUERY-brwFindingsPareto OPEN QUERY {&SELF-NAME} FOR EACH findingpareto by findingpareto.cnt descending.
&Scoped-define TABLES-IN-QUERY-brwFindingsPareto findingpareto
&Scoped-define FIRST-TABLE-IN-QUERY-brwFindingsPareto findingpareto


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwFindingsPareto}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bQuestionFind cbyear cState bRefresh ~
cAuditor Agents findingType brwFindingsPareto RECT-45 RECT-46 
&Scoped-Define DISPLAYED-OBJECTS cbyear cState cAuditor Agents findingType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwFindingsPareto 
       MENU-ITEM m_View_Questions_Findings LABEL "View Question's Findings".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bQuestionFind  NO-FOCUS
     LABEL "Question's Findings" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Question's Findings".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE Agents AS CHARACTER  FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN AUTO-COMPLETION
     SIZE 84.2 BY 1 NO-UNDO.

DEFINE VARIABLE cAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbyear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "IItem 1","Item 1"
     DROP-DOWN-LIST
     SIZE 13.2 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE findingType AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "ALL", 0,
"Minor", 1,
"Intermediate", 2,
"Major", 3
     SIZE 43.4 BY 1.67 NO-UNDO.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 166 BY 3.1.

DEFINE RECTANGLE RECT-46
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.4 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFindingsPareto FOR 
      findingpareto SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFindingsPareto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFindingsPareto C-Win _FREEFORM
  QUERY brwFindingsPareto DISPLAY
      findingpareto.cnt         label "Count" format "zzz9" width 8
 findingpareto.pct         label "Percentage" width 14
 findingpareto.Priority    label "Priority" width 10
 findingpareto.type        label "Type" format "x(15)" width 15
 findingpareto.description label "Description" format "x(240)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 186 BY 23.62 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 2.1 COL 170 WIDGET-ID 156 NO-TAB-STOP 
     bQuestionFind AT ROW 2.1 COL 177.2 WIDGET-ID 172 NO-TAB-STOP 
     cbyear AT ROW 1.86 COL 10.8 COLON-ALIGNED WIDGET-ID 10
     cState AT ROW 1.86 COL 33.8 COLON-ALIGNED WIDGET-ID 150
     bRefresh AT ROW 2.1 COL 160 WIDGET-ID 4 NO-TAB-STOP 
     cAuditor AT ROW 1.86 COL 70 COLON-ALIGNED WIDGET-ID 152
     Agents AT ROW 3.05 COL 10.8 COLON-ALIGNED WIDGET-ID 22
     findingType AT ROW 2.1 COL 114.8 NO-LABEL WIDGET-ID 62
     brwFindingsPareto AT ROW 4.81 COL 3 WIDGET-ID 200
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.1 COL 169.4 WIDGET-ID 164
     "Finding Type:" VIEW-AS TEXT
          SIZE 13.4 BY .62 AT ROW 2.57 COL 100.8 WIDGET-ID 170
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 3.6 WIDGET-ID 162
     RECT-45 AT ROW 1.43 COL 3 WIDGET-ID 158
     RECT-46 AT ROW 1.43 COL 168.6 WIDGET-ID 160
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 190 BY 28.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Finding Pareto"
         HEIGHT             = 28.19
         WIDTH              = 190
         MAX-HEIGHT         = 32.19
         MAX-WIDTH          = 202.2
         VIRTUAL-HEIGHT     = 32.19
         VIRTUAL-WIDTH      = 202.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwFindingsPareto findingType fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwFindingsPareto:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwFindingsPareto:HANDLE
       brwFindingsPareto:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwFindingsPareto:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFindingsPareto
/* Query rebuild information for BROWSE brwFindingsPareto
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH findingpareto by findingpareto.cnt descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFindingsPareto */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Finding Pareto */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Finding Pareto */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Finding Pareto */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Agents C-Win
ON VALUE-CHANGED OF Agents IN FRAME fMain /* Agent */
DO:

 if agents:screen-value in frame fmain = ? then
       agents:screen-value in frame fmain = " ".
  
 if cAuditor:screen-value in frame fmain =  tAuditor and 
     cbyear:screen-value in frame fMain   = tYear    and
     agents:screen-value in frame fmain   = tagent   and 
     cstate:screen-value in frame fMain   = tstate   and
     findingtype:screen-value            = tfinding then
   OPEN QUERY brwFindingsPareto FOR EACH findingpareto by findingpareto.cnt.
  else
   close query brwFindingsPareto.
   run Setcount.
/* commented to fix the error "DITEM is not large enough to hold string." */
/*   Agents:tooltip = ENTRY( LOOKUP(Agents:SCREEN-VALUE,Agents:LIST-ITEM-PAIRS, "|") - 1, Agents:LIST-ITEM-PAIRS, "|"). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQuestionFind
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQuestionFind C-Win
ON CHOOSE OF bQuestionFind IN FRAME fMain /* Question's Findings */
DO:
  run openFinding in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
    empty temp-table findingpareto.
    empty temp-table ttfindingpareto.

    std-ch = " records(s) found as of "  + " " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM").

    publish "GetFindingParetoData" (input cbyear:screen-value,
                                    input findingType:screen-value,
                                    input cState:screen-value,
                                    input Agents:screen-value,
                                    input cAuditor:screen-value,
                                    output table ttfindingpareto).

      for each ttfindingpareto:
        findingparetorecord = findingparetorecord + 1.
        create findingpareto.
        assign 
         findingpareto.cnt         = if ttfindingpareto.cnt         = ?   then 0 else ttfindingpareto.cnt         
         findingpareto.pct         = if ttfindingpareto.pct         = ?   then 0 else ttfindingpareto.pct
         findingpareto.ver         = if ttfindingpareto.ver         = "?" then "" else ttfindingpareto.ver  
         findingpareto.questionID  = if ttfindingpareto.questionID  = "?" then "" else ttfindingpareto.questionID       
         findingpareto.questionSeq = if ttfindingpareto.questionSeq = ?   then 0 else ttfindingpareto.questionSeq
         findingpareto.description = if ttfindingpareto.description = "?" then "" else ttfindingpareto.description       
         findingpareto.priority    = if ttfindingpareto.priority    = ?   then 0 else ttfindingpareto.priority         
         findingpareto.type        = if ttfindingpareto.type        = "?" then "" else ttfindingpareto.type             
        .   
      end.
      status default string( findingparetorecord) + std-ch in window {&window-name}.
      status input string( findingparetorecord) + std-ch in window {&window-name}. 
      findingparetorecord = 0.
      run SortData("").
/*       open query brwfindingspareto for each findingpareto by findingpareto.cnt descending. */


  if query brwFindingsPareto:num-results = 0
  then 
   assign
       bExport:sensitive = false
       bQuestionFind:sensitive = false
       .
  else 
   assign
       bExport:sensitive = true
       bQuestionFind:sensitive = true
       .
  
  
tAgent = Agents:screen-value in frame fMain.
tYear = cbYear:screen-value in frame fMain.
tAuditor = cAuditor:screen-value in frame fmain.
tstate = cState:screen-value in frame fMain.
tfinding = findingtype:screen-value in frame fmain.
/* run setCount. */
apply "value-changed" to brwFindingsPareto.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFindingsPareto
&Scoped-define SELF-NAME brwFindingsPareto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindingsPareto C-Win
ON DEFAULT-ACTION OF brwFindingsPareto IN FRAME fMain
DO:
  apply "CHOOSE" to bQuestionFind in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindingsPareto C-Win
ON ROW-DISPLAY OF brwFindingsPareto IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindingsPareto C-Win
ON START-SEARCH OF brwFindingsPareto IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAuditor C-Win
ON VALUE-CHANGED OF cAuditor IN FRAME fMain /* Auditor */
DO:
  close query brwFindingsPareto.
  run Setcount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbyear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbyear C-Win
ON VALUE-CHANGED OF cbyear IN FRAME fMain /* Year */
DO:
  close query brwFindingsPareto.
  run Setcount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
  close query brwFindingsPareto.
  run Setcount.
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME findingType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL findingType C-Win
ON VALUE-CHANGED OF findingType IN FRAME fMain
DO:
  close query brwFindingsPareto.
  run Setcount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Questions_Findings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Questions_Findings C-Win
ON CHOOSE OF MENU-ITEM m_View_Questions_Findings /* View Question's Findings */
DO:
  apply "CHOOSE" to bQuestionFind in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
{lib/win-main.i}
{lib/brw-main.i}
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.     

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bQuestionFind:load-image("images/open.bmp").
bQuestionFind:load-image-insensitive("images/open-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

do std-in = 0 to 9:
  yearpair =  yearpair + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in) + ",".
end.
yearpair = trim(yearpair, ",").
yearpair = "ALL,0," + yearpair.
cbyear:list-item-pairs = yearpair.
cbyear:screen-value = "0".


publish "GetAuditors"(output table auditor).
cAuditor:delimiter = "|" .
for each auditor no-lock:
  auditorpair  =  auditorpair + "|" + string(auditor.name) + "|" + string(auditor.UID) .
end.
auditorpair = trim(auditorpair , "|" ).
cAuditor:list-item-pairs =  "ALL|ALL|" + auditorpair.
cAuditor:screen-value = "ALL".

{lib/get-state-list.i &combo=cState &addAll=true}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  assign
    tYear    = cbyear:screen-value
    tState   = cState:screen-value 
    tAgent   = Agents:screen-value
    tAuditor = cAuditor:screen-value
    tFinding = findingType:screen-value.
    RUN enable_UI.
    {lib/get-agent-list.i &combo=Agents &state=cState &addAll=true}
   apply "value-changed" to cState.
    
  run windowResized in this-procedure.
  
  if query brwFindingsPareto:num-results = 0
  then 
   assign
       bExport:sensitive = false
       bQuestionFind:sensitive = false
       .
       
  status input " " in window {&window-name}. 
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbyear cState cAuditor Agents findingType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport bQuestionFind cbyear cState bRefresh cAuditor Agents 
         findingType brwFindingsPareto RECT-45 RECT-46 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 def var th as handle no-undo.
 if query brwFindingsPareto:num-results = 0
 then
 do:
   MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
 end.
 
 publish "GetReportDir" (output std-ch).
 th = temp-table findingpareto:handle.
 run util/exporttable.p (table-handle th,
                         "findingpareto",
                         "for each findingpareto ",
                         "cnt,pct,Priority,type,description",
                         "Count,Percentage,Priority,Type,Description",
                         std-ch,
                         "FindingsPareto- "  + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openFinding C-Win 
PROCEDURE openFinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   
 find current findingpareto no-error.
 if not available findingpareto
  then
   return.
 
/* if condition: to get the auditorname,state description,agentname as the matching is done with uid,stateid and agenID respectively.
  else condition - old value coming in wfindings screen incase of of ALL */
 find first auditor where auditor.UID = tauditor no-lock no-error.
 if available auditor then 
   cauditorname = auditor.name.
 else cauditorname = "".
 
 find first state where state.stateID = tstate no-lock no-error.
 if available state then 
   tstatedesc = state.description.
 else tstatedesc = "".        
 
 find first agent where agent.agentID = tagent no-lock no-error.
 if available agent then 
   tagentname = agent.name.
 else tagentname = "" .
 
 run wfindings.w persistent (input findingpareto.questionSeq,
                             input findingpareto.description,
                             input tyear,
                             input tauditor,
                             input cauditorname,
                             input tstate,
                             input tstatedesc,
                             input tAgent,
                             input tagentname,
                             input tFinding).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  std-in = 0.
  for each findingpareto no-lock:
    std-in = std-in + 1.
  end.
   std-ch = string(std-in) + " " + "record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM").
   status default std-ch in window {&window-name}.
   status input std-ch in window {&window-name}.
  
   if brwFindingspareto:num-iterations in frame fMain = 0 then
   do:
     std-ch = "".
     status default std-ch in window {&window-name}.
     status input std-ch in window {&window-name}.
     assign
          bExport:sensitive in frame fMain = false
          bQuestionFind:sensitive in frame fMain = false
          .
   end.
   else
    assign
        bExport:sensitive in frame fMain = true
        bQuestionFind:sensitive in frame fMain = true
        .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &post-by-clause=" + ' by  findingpareto.cnt desc' "}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* fMain components */
  brwFindingsPareto:width-pixels = frame fmain:width-pixels - 20.
  brwFindingsPareto:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

