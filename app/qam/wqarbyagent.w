&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wqarbyagent.w
   Window of qarbyagent across qar records
   9.6.2017
   @ Archana Gupta
   @Modified 07/15/2021 SA Task 83510 modified UI according to 
                           new field grade
             09/24/2021 SA Task 86696 Defects raised by david
*/

CREATE WIDGET-POOL.

{tt/auditor.i}
{tt/agent.i}
{tt/qaraudit.i &TableAlias="data"}
{tt/qaraudit.i &TableAlias="tempdata"}
{tt/state.i}
def temp-table openQars
 field qarid as int
 field hInstance as handle.

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/get-column.i}

define variable lOpenQar as logical.
define variable hQarSummary as handle.

define variable dColumn as decimal no-undo.

{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQar

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwQar                                        */
&Scoped-define FIELDS-IN-QUERY-brwQar data.stateID data.agentId data.name data.qarID data.statDesc data.auditTypeDesc data.errTypeDesc data.score data.grade data.auditor data.draftReportDate data.auditStartDate data.auditFinishDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQar   
&Scoped-define SELF-NAME brwQar
&Scoped-define QUERY-STRING-brwQar FOR EACH data by data.stat by data.qarid
&Scoped-define OPEN-QUERY-brwQar OPEN QUERY {&SELF-NAME} FOR EACH data by data.stat by data.qarid.
&Scoped-define TABLES-IN-QUERY-brwQar data
&Scoped-define FIRST-TABLE-IN-QUERY-brwQar data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQar}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-47 tStateID tYear ~
bRefresh bExport tAgentID tAuditor brwQar 
&Scoped-Define DISPLAYED-OBJECTS tStateID tYear tAgentID tAuditor 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE tAgentID AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 88 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL",0
     DROP-DOWN-LIST
     SIZE 13 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 113.4 BY 3.1.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 13.2 BY 3.1.

DEFINE RECTANGLE RECT-47
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 39.4 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQar FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQar C-Win _FREEFORM
  QUERY brwQar DISPLAY
      data.stateID label "State" format "x(15)" width 7
data.agentId label "Agent ID" format "x(15)" width 10
data.name label "Agent Name" format "x(200)" width 26
data.qarID label "QAR ID" format ">9999999" width 10
data.statDesc label "Status" format "x(12)" width 12
data.auditTypeDesc label "Audit Type" format "x(20)" width 12
data.errTypeDesc label "ERR Type" format "x(20)" width 12 
data.score label "Points" format "zzz " width 8
data.grade label "Score%" format "zzz"  width 9
data.auditor label "Auditor" format "x(30)" width 22
data.draftReportDate column-label "Preliminary!Report Date" format "99/99/9999" width 15
data.auditStartDate label "Start Date" format "99/99/9999" width 15
data.auditFinishDate label "End Date" format "99/99/9999" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189.6 BY 21.71 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tStateID AT ROW 1.95 COL 12 COLON-ALIGNED WIDGET-ID 150
     tYear AT ROW 1.95 COL 125 COLON-ALIGNED WIDGET-ID 10
     bRefresh AT ROW 2.19 COL 105 WIDGET-ID 4
     bExport AT ROW 2.19 COL 158 WIDGET-ID 2
     tAgentID AT ROW 3.14 COL 7.2 WIDGET-ID 18
     tAuditor AT ROW 3.14 COL 125 COLON-ALIGNED WIDGET-ID 158
     brwQar AT ROW 4.81 COL 3 WIDGET-ID 200
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 156 WIDGET-ID 32
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 28
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 117 WIDGET-ID 154
     RECT-1 AT ROW 1.48 COL 3 WIDGET-ID 26
     RECT-2 AT ROW 1.48 COL 155 WIDGET-ID 30
     RECT-47 AT ROW 1.48 COL 116 WIDGET-ID 152
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 193.2 BY 25.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Audit by Agent"
         HEIGHT             = 25.86
         WIDTH              = 193.2
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQar tAuditor fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwQar:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQar:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR COMBO-BOX tAgentID IN FRAME fMain
   ALIGN-L                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQar
/* Query rebuild information for BROWSE brwQar
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data by data.stat by data.qarid.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQar */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Audit by Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Audit by Agent */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Audit by Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQar
&Scoped-define SELF-NAME brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON DEFAULT-ACTION OF brwQar IN FRAME fMain
DO:
 if not available data 
  then return.

  publish "checkOpenQar" (input data.QarId,
                          output lOpenQar,
                          output hQarSummary ).
  if lOpenQar
   then
    do:
      run ViewWindow in hQarSummary no-error.
      return.
    end.
   else
    do:
      empty temp-table tempdata.
      buffer-copy data to tempdata.
      {lib/pbshow.i "''"}
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
      run QARSummary.w persistent (input table tempdata).
      run ViewWindow in hQarSummary no-error.
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
      {lib/pbhide.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON ROW-DISPLAY OF brwQar IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON START-SEARCH OF brwQar IN FRAME fMain
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditor C-Win
ON VALUE-CHANGED OF tAuditor IN FRAME fMain /* Auditor */
DO:
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain /* Year */
DO:
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/win-main.i}
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

/* get the years */
std-ch = "ALL,0".
do std-in = 0 to 9:
  std-ch =  addDelimiter(std-ch,",") + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in).
end.
tYear:list-item-pairs = std-ch.
tYear:screen-value = "0".

/* get the auditors */
publish "GetAuditors"(output table auditor).
tAuditor:delimiter = {&msg-dlm}.
std-ch = "ALL" {&msg-add} "ALL".
for each auditor no-lock:
  std-ch = addDelimiter(std-ch,{&msg-dlm}) + auditor.name {&msg-add} auditor.uid.
end.
tAuditor:list-item-pairs = std-ch.
tAuditor:screen-value = "ALL".

/* get the states */
{lib/get-state-list.i &combo=tStateID &addAll=true}
{lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}

session:immediate-display = yes.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  bExport:sensitive = false.
  
  {lib/get-column-width.i &col="'name'" &var=dColumn}

  tStateID:screen-value = "ALL".
  apply "VALUE-CHANGED" to tStateID.
  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.

IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStateID tYear tAgentID tAuditor 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-47 tStateID tYear bRefresh bExport tAgentID 
         tAuditor brwQar 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwQar:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
  
  &scoped-define ReportName "QAR_By_Agent"
  
  std-ch = "".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStateID as character no-undo.
  define variable cAgentID as character no-undo.

  do with frame {&frame-name}:
    assign
      cStateID = tStateID:input-value
      cAgentID = tAgentID:input-value
      .
    
    if cStateID = "ALL" and cAgentID = "ALL"
     then
      do:
        std-lo = true.
        MESSAGE "By selecting all states, the report could be slow. Are you sure you want to continue?" VIEW-AS ALERT-BOX question BUTTONS ok-cancel UPDATE std-lo AS LOGICAL.
        if not std-lo
         then return.
      end.
    
    empty temp-table data.
    run server/getallaudits.p (input 0,         /* year */
                               input cStateID,  /* state */
                               input cAgentID,  /* agent */
                               input "ALL",     /* auditor */
                               input "ALL",     /* status */
                               input 0,         /* start gap */
                               input 0,         /* audit gap */
                               output table data,
                               output std-lo,
                               output std-ch
                               ).
                            
    if not std-lo
     then message std-ch view-as alert-box error buttons ok.
     else
      do:
        for each data exclusive-lock:
          publish "GetSysPropDesc" ("QAR", "Audit", "Status", data.stat, output data.statDesc).
          publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType, output data.auditTypeDesc).
          publish "GetSysPropDesc" ("QAR", "ERR", "Type", data.errType, output data.errTypeDesc).
          publish "GetSysUserName" (data.uid, output data.username).
        end.
        run SortData("").
        
        if query brwQar:num-results = 0
         then bExport:sensitive = false.
         else bExport:sensitive = true.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 if {&window-name}:window-state eq window-minimized  then
 {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iYear as integer no-undo.
  define variable cAuditor as character no-undo.
  
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
    assign
      iYear = tYear:input-value
      cAuditor = tAuditor:input-value
      .
  end.
  
  /* build the query */
  if iYear > 0
   then tWhereClause = addDelimiter(tWhereClause," and ") + "auditYear = " + string(iYear).
  if cAuditor <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "uid = '" + cAuditor + "'".
  
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +" &post-by-clause=" + ' by  data.qarID' "}
  std-ch =  " audit(s) found as of " + string(now).
  status default string(num-results("{&browse-name}")) + std-ch in window {&window-name}.
  status input string(num-results("{&browse-name}")) + std-ch  in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dDiffWidth as decimal no-undo.
  
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* fSearch components */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.
  browse brwQAR:width-pixels = frame fMain:width-pixels - 20.
  
  if {&window-name}:width-pixels > frame fMain:width-pixels 
  then
  do: 
    frame fMain:width-pixels = {&window-name}:width-pixels.
    frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  end.
  else
  do:
    frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
    frame fMain:width-pixels = {&window-name}:width-pixels.
          /* das: For some reason, shrinking a window size MAY cause the horizontal
             scroll bar.  The above sequence of widget setting should resolve it,
             but it doesn't every time.  So... */
  end.
  browse brwQAR:height-pixels = frame fMain:height-pixels - browse brwQAR:y - 10.
   
  std-ha = getColumn({&browse-name}:handle, "name").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + (dDiffWidth * 0.75).
   
  std-ha = getColumn({&browse-name}:handle, "auditor").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + (dDiffWidth * 0.25).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

