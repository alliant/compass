&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File: dialogReassignAudit.w
  Description: Reassign audit to new Auditor
  Author: AC
  Created: 05.31.2017
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
define input parameter pQarId as character no-undo.
define input parameter pAgentId as character no-undo.
define input parameter pName as character no-undo.
define input parameter pAuditor as character no-undo.
define output parameter pNewAuditor as character no-undo.
define output parameter pReason as character no-undo .
define output parameter pCancel as logical init false.
define variable auditorpair as character no-undo.
{tt/auditor.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbNewAuditor tReason Btn_OK Btn_Cancel ~
tQarId tName tOldAuditor 
&Scoped-Define DISPLAYED-OBJECTS cbNewAuditor tReason tQarId tName ~
tOldAuditor 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY  NO-CONVERT-3D-COLORS
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbNewAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "New Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE tReason AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 67 BY 4 NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
      VIEW-AS TEXT 
     SIZE 54 BY .62 NO-UNDO.

DEFINE VARIABLE tOldAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Current Auditor" 
      VIEW-AS TEXT 
     SIZE 38 BY .62 NO-UNDO.

DEFINE VARIABLE tQarId AS CHARACTER FORMAT "X(256)":U 
     LABEL "QAR" 
      VIEW-AS TEXT 
     SIZE 30 BY .62 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbNewAuditor AT ROW 4 COL 17.2 COLON-ALIGNED WIDGET-ID 18
     tReason AT ROW 6 COL 4 NO-LABEL WIDGET-ID 2
     Btn_OK AT ROW 10.38 COL 21.4
     Btn_Cancel AT ROW 10.38 COL 38.4
     tQarId AT ROW 1.43 COL 17.2 COLON-ALIGNED WIDGET-ID 6
     tName AT ROW 2.29 COL 17.2 COLON-ALIGNED WIDGET-ID 10
     tOldAuditor AT ROW 3.14 COL 17.2 COLON-ALIGNED WIDGET-ID 16
     "Reason:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 5.33 COL 4 WIDGET-ID 20
     SPACE(59.99) SKIP(5.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Reassign Audit"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       tReason:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Reassign Audit */
DO:
  pCancel = true .
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  pCancel = true .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  publish "IsQarPlanned" (integer(pQarId), output std-lo).
  pReason = tReason:screen-value .
  pNewAuditor = cbNewAuditor:screen-value.
  if pReason = "" and not std-lo then
  do:
    message "Reason cannot be blank. "
      view-as alert-box info buttons ok.
    apply "entry" to tReason.
    return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbNewAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbNewAuditor Dialog-Frame
ON VALUE-CHANGED OF cbNewAuditor IN FRAME Dialog-Frame /* New Auditor */
DO:    
  run AuditorCheck.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReason
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReason Dialog-Frame
ON VALUE-CHANGED OF tReason IN FRAME Dialog-Frame
DO:
    run AuditorCheck.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  publish "GetAuditors"(output table auditor).
  cbNewAuditor:delimiter = "|" .
  for each auditor:
    auditorpair  = auditorpair + "|" + string(auditor.name) + "|" + string(auditor.UID) .
  end.
  auditorpair = trim(auditorpair , " |" ).
  cbNewAuditor:list-item-pairs = auditorpair.
  cbNewAuditor:screen-value = entry(2, auditorpair, "|").
  tQarId:screen-value = pQarId.
  tName:screen-value = pName.
  if length(pname)> 40 then
  tName:screen-value = substring(pName,1,40) + " " + "..." + " " + "(" + pAgentId + ")".
  else
  tName:screen-value = substring(pName,1,40) + " " + "(" + pAgentId + ")".
  tname:tooltip = pname + " " + pAgentID.
  tOldAuditor:screen-value = entry(2,pAuditor).
  run AuditorCheck.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditorCheck Dialog-Frame 
PROCEDURE AuditorCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign pReason = tReason:screen-value in frame dialog-frame.
  if entry(1,pAuditor) eq cbNewAuditor:screen-value then
    tReason:sensitive = false.
  else
    tReason:sensitive = true.

  publish "IsQarPlanned" (integer(pQarId), output std-lo).
  Btn_OK:sensitive in frame {&frame-name} = false.
  if entry(1,pAuditor) ne cbNewAuditor:screen-value
   then Btn_OK:sensitive in frame {&frame-name} = (length(pReason) > 0 or std-lo).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbNewAuditor tReason tQarId tName tOldAuditor 
      WITH FRAME Dialog-Frame.
  ENABLE cbNewAuditor tReason Btn_OK Btn_Cancel tQarId tName tOldAuditor 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

