&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* qardatasrv.p
   DATA model for the QAR module that interacts with the applicationSeRVer
   Created 3.8.2017 D.Sinclair
   
   Modified: 11/02/20  Shefali     add internal procedures instead include 
                                   syspropdatasrv.i to reduce number of hits 
                                   to server. 
 */
 
{tt/sysprop.i}
{tt/sysprop.i &tableAlias="tempsysprop"} 
{tt/auditor.i}

{lib/std-def.i}

{lib/datasource.i &name="SERVER"}

{lib/sysuserdatasrv.i}
{tt/person.i   &tableAlias=tcachePerson}
{tt/organization.i}
 
def var tLoadedAuditors as logical init false no-undo.
define variable tLoadedSysProps      as logical  initial false no-undo.
define variable lModifyOrganization  as logical  initial false no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 22.62
         WIDTH              = 59.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

publish "SetSplashStatus".
run LoadAuditors in this-procedure.

{lib/alertdatasrv.i}
{lib/syscodedatasrv.i &syscode="'Alert,AgentAppReasonCode,AgentType,AttorneyType'"}

subscribe to "GetSysPropDesc" anywhere.
subscribe to "GetSysPropList" anywhere.

subscribe to "LoadAuditors" anywhere.
subscribe to "GetAuditors" anywhere.

/* Get status of Agent*/
subscribe to "getStatusList" anywhere.


/* Persons*/
subscribe to "getPersons" anywhere.

/* Get organizations*/
subscribe to "getOrganizations" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetAuditors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAuditors Procedure 
PROCEDURE GetAuditors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for auditor.
if not tLoadedAuditors 
 then run LoadAuditors in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getOrganizations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getOrganizations Procedure 
PROCEDURE getOrganizations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for organization.
 
  /* Buffer */
  define buffer organization for organization.

  /* Client Server Call */
  if not can-find(first organization) or lModifyOrganization
   then
    do:
      run server/getorganizations.p ("ALL",  /* organizationStatus */
                              output table organization,
                              output std-lo,
                              output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Loadorganizations failed: " std-ch
                 view-as alert-box error.
                 
      IF lModifyOrganization 
       THEN
        lModifyOrganization = FALSE.
    end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getPersons) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPersons Procedure 
PROCEDURE getPersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for tcachePerson.
 
  /* Buffer */
  define buffer tcachePerson for tcachePerson.

  /* Client Server Call */
  if not can-find(first tcachePerson)
   then
    do:    
      run server/getPersons.p (output table tcachePerson,
                               output std-lo,
                               output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Load Persons failed: " std-ch
                 view-as alert-box error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getStatusList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getStatusList Procedure 
PROCEDURE getStatusList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pAppCode  as character no-undo.
  define input  parameter pAction   as character no-undo.
  define input  parameter pProperty as character no-undo.
  define input  parameter pStat     as character no-undo.
  define output parameter pList     as character no-undo.

  define  variable chExcludeList as character   no-undo.
  
  /* buffers */
  define buffer tempsysprop for tempsysprop.

  /* get the system property */
  empty temp-table tempsysprop.
  run GetSysProp (pAppCode, pAction, pProperty, output table tempsysprop).

  case pStat:
    when "A" 
     then
      chExcludeList = "A,P,W".
    when "C" 
     then
      chExcludeList = "P,W,C,X".
    when "P" 
     then
      chExcludeList = "C,X,P".
    when "W" 
     then
      chExcludeList = "A,W,C,X".  
    when "X" 
     then
      chExcludeList = "P,W,C,X".       
    otherwise   
      chExcludeList = "A,W,C,X".    
  end case.
 
  
  /* create a list of system properties */
  pList = "".
  for each tempsysprop no-lock
     where tempsysprop.appCode = pAppCode
       and tempsysprop.objAction = pAction
       and tempsysprop.objProperty = pProperty
       and not can-do(chExcludeList,tempsysProp.objID)
        by tempsysprop.objRef
        by tempsysprop.objID:
    
    pList = addDelimiter(pList, {&msg-dlm}) + tempsysprop.objValue {&msg-add} tempsysprop.objID.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadAuditors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadAuditors Procedure 
PROCEDURE LoadAuditors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer auditor for auditor.
 def var tMsg as char no-undo.
 
 empty temp-table auditor.
 
 run server/getauditors.p ("", /* auditorID, blank=All */
                         output table auditor,
                         output tLoadedAuditors,
                         output tMsg).
 if not tLoadedAuditors
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadAuditors failed: " tMsg view-as alert-box warning.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadSysProps) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSysProps Procedure 
PROCEDURE LoadSysProps :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* buffers */
  define buffer sysprop for sysprop.
  define buffer tempsysprop for tempsysprop.

  /* load the system properties from the preprocessor */
  empty temp-table sysprop.
  empty temp-table tempsysprop.

  /* call the server */
  run server/getsysprops.p (input "QAR",
                            input "",
                            input "",
                            input "",
                            input "",
                            output table tempsysprop,
                            output std-lo,
                            output std-ch
                            ).
    
  /* if unsuccessful, show a message */
  if not std-lo 
   then
    do:
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
  
      if std-lo 
       then message "LoadSysProps (QAR) failed: " + std-ch view-as alert-box warning.
    end.
   else
    /* otherwise, add the newly retrieved system property to the system property records */
    for each tempsysprop:
      create sysprop.
      buffer-copy tempsysprop to sysprop.
    end.
   
  /*** LOAD SYSPROPS NEEDED FOR COMMON ELEMENTS ***/
  /* agent status */
  if not can-find(first sysprop where appCode = "AMD" and objAction = "Agent" and objProperty = "Status")
   then
    do:
      empty temp-table tempsysprop.
      run server/getsysprops.p (input "AMD",
                                input "Agent",
                                input "Status",
                                input "",
                                input "",
                                output table tempsysprop,
                                output std-lo,
                                output std-ch
                                ).
    
      if std-lo 
       THEN 
        for each tempsysprop:
          create sysprop.
          buffer-copy tempsysprop to sysprop.
        end.
       else
        do: 
          std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "LoadSysProps (AMD, Agent, Status) failed: " + std-ch view-as alert-box warning.
        end.
    end.
  tLoadedSysProps = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

