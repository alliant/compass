&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wscheduleQARdata.w
   Window of scheduleQARdata across qar records
   08.22.2017
   @ Anjly Chanana
   @Modified
   07/15/2021 SA   Task 83510 modified UI according to 
                                     new field grade
   09/24/21   SA   Task 86696 Defects raised by david
   01/19/22   SA   Task#:90223 modify to add functionality for fault message view
   02/07/22   SA   Task#:90943 modified logic for data payload 
   02/23/2022 SA   Task#:91615 modified logic for UID and audit type 
*/

CREATE WIDGET-POOL.
{tt/qaraudit.i &tableAlias="qar"}
{tt/scheduleQARdata.i &tableAlias="data"}
{tt/scheduleQARdata.i &tableAlias="tempdata"}

{lib/std-def.i}
{tt/auditor.i}
{tt/proerr.i  &tableAlias="scheduleErr"}       /* Used to get validation messages from server */

define variable lOpenQAR as logical.
define variable hQarSummary as handle.

define variable currState as character no-undo.
define variable currType as character no-undo.
define variable currStatus as character no-undo.
define variable cErrMsg as character no-undo.

define variable dColumn as decimal no-undo.
 
define variable cQarType as character no-undo.
define variable cQarDate as character no-undo.
define variable cQarAuditor as character no-undo.

{lib/winlaunch.i}
{lib/getstatename.i}
{lib/add-delimiter.i}
{lib/get-column.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQar

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwQar                                        */
&Scoped-define FIELDS-IN-QUERY-brwQar data.agentState data.agentID data.agentName data.agentStatusDesc data.year3AuditGrade data.year2AuditGrade data.year1AuditGrade getstatus(data.auditType1) @ data.auditType1 data.lastAuditDate data.lastAuditor data.proposedAuditType data.proposedAuditDate data.proposedAuditor data.recordChanged   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQar data.proposedAuditDate data.proposedAuditor data.proposedAuditType data.recordChanged   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwQar data
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwQar data
&Scoped-define SELF-NAME brwQar
&Scoped-define QUERY-STRING-brwQar FOR EACH data
&Scoped-define OPEN-QUERY-brwQar OPEN QUERY {&SELF-NAME} FOR EACH data.
&Scoped-define TABLES-IN-QUERY-brwQar data
&Scoped-define FIRST-TABLE-IN-QUERY-brwQar data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQar}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAgentAddress tAgentName tAgentNameLabel ~
tAgentState cSelectionYear bRefresh cmbState cmbStatus cmbType tAuditors ~
tApplyERR tScoreERR tScoreAudit brwQar tAuditDetails RECT-1 RECT-2 RECT-3 ~
rIntervalERR rDetails 
&Scoped-Define DISPLAYED-OBJECTS tAgentAddress tAgentName tAgentNameLabel ~
tAgentState cSelectionYear cmbState cmbStatus cmbType tAuditors tApplyERR ~
tScoreERR tScoreAudit tAuditDetails 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD changeButtonSensitive C-Win 
FUNCTION changeButtonSensitive RETURNS logical
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearDetails C-Win 
FUNCTION clearDetails RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD dataChanged C-Win 
FUNCTION dataChanged RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pManager as character,
    input pStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character,
    input pManager as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterTypeCombo C-Win 
FUNCTION getFilterTypeCombo RETURNS CHARACTER
  ( input pState as character,
    input pStatus as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getstatus C-Win 
FUNCTION getstatus RETURNS CHARACTER
  (input pstat as char /* parameter-definitions */ )  FORWARD.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombo C-Win 
FUNCTION setFilterCombo RETURNS LOGICAL
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQar 
       MENU-ITEM m_Discard_Change LABEL "Discard Changes"
              DISABLED
       RULE
       MENU-ITEM m_View_3_QAR   LABEL "View Three Year Ago QAR"
       MENU-ITEM m_View_2_QAR   LABEL "View Two Year Ago QAR"
       MENU-ITEM m_View_1_QAR   LABEL "View Last Year's QAR".


/* Definitions of the field level widgets                               */
DEFINE VARIABLE tDetailsDate1 AS DATETIME FORMAT "99/99/99":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsDate2 AS DATETIME FORMAT "99/99/99":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsDate3 AS DATETIME FORMAT "99/99/99":U 
     LABEL "Audit Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsDatePlanned AS DATETIME FORMAT "99/99/99":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsERR1 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsERR2 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsERR3 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     LABEL "Section 6  Points" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsERRPlanned AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.
DEFINE VARIABLE tDetailsGrade1 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.
DEFINE VARIABLE tDetailsGrade2 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.
DEFINE VARIABLE tDetailsGrade3 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     LABEL "Audit Score" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.
DEFINE VARIABLE tDetailsGradePlanned AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsScore1 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsScore2 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsScore3 AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     LABEL "Total Points" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsScorePlanned AS CHARACTER FORMAT "x(8)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsType1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsType2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsType3 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Audit Type" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDetailsTypePlanned AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-52
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 62.4 BY .1.

DEFINE RECTANGLE RECT-53
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 5.91.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 5.91.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .6 BY 5.91.

DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel the changes made to the schedule".

DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE BUTTON bSchedule 
     LABEL "Schedule" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save the changes made to the schedule".

DEFINE VARIABLE cmbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cmbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cmbType AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Audit Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentAddress AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 80 BY .62 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 90 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentNameLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Agent Name:" 
      VIEW-AS TEXT 
     SIZE 13 BY .62 NO-UNDO.

DEFINE VARIABLE tAgentState AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 80 BY .62 NO-UNDO.

DEFINE VARIABLE tAuditDetails AS CHARACTER FORMAT "X(256)":U INITIAL "Audit Details" 
      VIEW-AS TEXT 
     SIZE 12 BY .62 NO-UNDO.

DEFINE VARIABLE tScoreAudit AS INTEGER FORMAT ">>>>":U INITIAL 130 
     LABEL "QAR Points Minimum" 
     VIEW-AS FILL-IN 
     SIZE 6.2 BY 1 TOOLTIP "Minimum audit score of previous audits" NO-UNDO.

DEFINE VARIABLE tScoreERR AS INTEGER FORMAT ">>>>>":U INITIAL 7 
     LABEL "ERR Points Minimum" 
     VIEW-AS FILL-IN 
     SIZE 6.2 BY 1 TOOLTIP "Minimum ERR score of previous audits" NO-UNDO.

DEFINE RECTANGLE rDetails
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 203 BY 8.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 76.4 BY 5.24.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 20.4 BY 5.24.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26.2 BY 5.24.

DEFINE RECTANGLE rIntervalERR
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 5.24.

DEFINE VARIABLE cSelectionYear AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE 
     SIZE 7 BY 1.57 NO-UNDO.

DEFINE VARIABLE tAuditors AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "ALL","ALL",
                     "UNASSIGNED","UNASSIGNED",
                     "ASSIGNED","ASSIGNED" 
     SIZE 27 BY 3.91 NO-UNDO.

DEFINE VARIABLE tApplyERR AS LOGICAL INITIAL no 
     LABEL "Apply Interval ERR Parameter" 
     VIEW-AS TOGGLE-BOX
     SIZE 32 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQar FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQar C-Win _FREEFORM
  QUERY brwQar DISPLAY
      data.agentState        column-label "State"  width 7 format "x(15)"
data.agentID           column-label "Agent!ID"  width 10 format "x(15)" 
data.agentName         column-label "Agent!Name" width 25 format "x(200)"
data.agentStatusDesc   column-label "Agent!Status"  width 10 format "x(50)"
data.year3AuditGrade   column-label "Third to Last!Score%" format "zzz"  width 15
data.year2AuditGrade   column-label "Second to Last!Score%" format "zzz" width 15
data.year1AuditGrade   column-label "Last! Scores%" format "zzz"  width 15
getstatus(data.auditType1) @  data.auditType1  column-label "Last!Audit Type" width 12 format "x(50)"
data.lastAuditDate     column-label "Last!Audit Date" format "99/99/99"  width 15
data.lastAuditor       column-label "Last Auditor" width 18 format "x(50)"
data.proposedAuditType column-label "Planned!Audit Type" format "x(50)" width 12 view-as combo-box 
data.proposedAuditDate column-label "Planned!Audit Date" format "99/99/99" width 12
data.proposedAuditor   column-label "Planned!Auditor"  width 15 format "x(50)" view-as combo-box 
data.recordChanged     column-label "Chg" view-as toggle-box
enable data.proposedAuditDate data.proposedAuditor data.proposedAuditType data.recordChanged
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 203 BY 16.52 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tAgentAddress AT ROW 27.24 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 190
     tAgentName AT ROW 26 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 188
     tAgentNameLabel AT ROW 26.14 COL 3 COLON-ALIGNED NO-LABEL WIDGET-ID 196
     tAgentState AT ROW 27.95 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 192
     cSelectionYear AT ROW 3.38 COL 11 NO-LABEL WIDGET-ID 16
     bRefresh AT ROW 3.29 COL 19 WIDGET-ID 40
     cmbState AT ROW 2.19 COL 43 COLON-ALIGNED WIDGET-ID 150
     cmbStatus AT ROW 3.62 COL 43 COLON-ALIGNED WIDGET-ID 160
     cmbType AT ROW 5.1 COL 43 COLON-ALIGNED WIDGET-ID 172
     tAuditors AT ROW 2.19 COL 77 NO-LABEL WIDGET-ID 170
     bExport AT ROW 2.24 COL 111 WIDGET-ID 2
     bSchedule AT ROW 2.24 COL 120 WIDGET-ID 158
     tApplyERR AT ROW 2.52 COL 135.2 WIDGET-ID 178
     tScoreERR AT ROW 3.48 COL 154.2 COLON-ALIGNED WIDGET-ID 34
     tScoreAudit AT ROW 4.67 COL 154.2 COLON-ALIGNED WIDGET-ID 36
     brwQar AT ROW 6.95 COL 3 WIDGET-ID 200
     tAuditDetails AT ROW 23.86 COL 2 COLON-ALIGNED NO-LABEL WIDGET-ID 186
     bCancel AT ROW 4.29 COL 111 WIDGET-ID 198
     "Interval ERR" VIEW-AS TEXT
          SIZE 12.6 BY .62 AT ROW 1.24 COL 133.2 WIDGET-ID 176
     "Action" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 1.24 COL 110 WIDGET-ID 32
     "Year:" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 3.91 COL 5 WIDGET-ID 18
     "Parameters" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 28
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 1.24 COL 32 WIDGET-ID 42
     RECT-1 AT ROW 1.48 COL 31 WIDGET-ID 26
     RECT-2 AT ROW 1.48 COL 109 WIDGET-ID 30
     RECT-3 AT ROW 1.48 COL 3 WIDGET-ID 38
     rIntervalERR AT ROW 1.48 COL 131.2 WIDGET-ID 174
     rDetails AT ROW 24.14 COL 3 WIDGET-ID 180
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 205.8 BY 31.95 WIDGET-ID 100.

DEFINE FRAME fDetails
     tDetailsDate3 AT ROW 2.38 COL 16.2 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tDetailsDate2 AT ROW 2.38 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 22 NO-TAB-STOP 
     tDetailsDate1 AT ROW 2.38 COL 48.2 COLON-ALIGNED NO-LABEL WIDGET-ID 28 NO-TAB-STOP 
     tDetailsDatePlanned AT ROW 2.38 COL 64.2 COLON-ALIGNED NO-LABEL WIDGET-ID 38 NO-TAB-STOP 
     tDetailsType3 AT ROW 3.57 COL 16.2 COLON-ALIGNED WIDGET-ID 8 NO-TAB-STOP 
     tDetailsType2 AT ROW 3.57 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 16 NO-TAB-STOP 
     tDetailsType1 AT ROW 3.57 COL 48.2 COLON-ALIGNED NO-LABEL WIDGET-ID 34 NO-TAB-STOP 
     tDetailsTypePlanned AT ROW 3.57 COL 64.2 COLON-ALIGNED NO-LABEL WIDGET-ID 44 NO-TAB-STOP 
     tDetailsGrade3 AT ROW 4.76 COL 16.2 COLON-ALIGNED WIDGET-ID 56 NO-TAB-STOP 
     tDetailsGrade2 AT ROW 4.76 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 54 NO-TAB-STOP 
     tDetailsGrade1 AT ROW 4.76 COL 48.2 COLON-ALIGNED NO-LABEL WIDGET-ID 52 NO-TAB-STOP 
     tDetailsGradePlanned AT ROW 4.76 COL 64.2 COLON-ALIGNED NO-LABEL WIDGET-ID 58 NO-TAB-STOP 
     tDetailsScore3 AT ROW 5.95 COL 16.2 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tDetailsScore2 AT ROW 5.95 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 20 NO-TAB-STOP 
     tDetailsScore1 AT ROW 5.95 COL 48.2 COLON-ALIGNED NO-LABEL WIDGET-ID 32 NO-TAB-STOP 
     tDetailsScorePlanned AT ROW 5.95 COL 64.2 COLON-ALIGNED NO-LABEL WIDGET-ID 42 NO-TAB-STOP 
     tDetailsERR3 AT ROW 7.14 COL 16.2 COLON-ALIGNED WIDGET-ID 6 NO-TAB-STOP 
     tDetailsERR2 AT ROW 7.14 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 24 NO-TAB-STOP 
     tDetailsERR1 AT ROW 7.14 COL 48.2 COLON-ALIGNED NO-LABEL WIDGET-ID 30 NO-TAB-STOP 
     tDetailsERRPlanned AT ROW 7.14 COL 64.2 COLON-ALIGNED NO-LABEL WIDGET-ID 40 NO-TAB-STOP 
     "Third to Last Scores" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.33 COL 18.6 WIDGET-ID 12
     "Planned" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.29 COL 68.8 WIDGET-ID 50
     "Last" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.33 COL 54.8 WIDGET-ID 48
     "Second to Last Scores" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 1.33 COL 34.2 WIDGET-ID 46
     RECT-52 AT ROW 2.14 COL 18 WIDGET-ID 10
     RECT-53 AT ROW 2.33 COL 33 WIDGET-ID 14
     RECT-54 AT ROW 2.33 COL 49 WIDGET-ID 26
     RECT-55 AT ROW 2.38 COL 65 WIDGET-ID 36
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 112 ROW 24.67
         SIZE 81.8 BY 7.43 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Audit Scheduling"
         HEIGHT             = 31.95
         WIDTH              = 207
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 293
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 293
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fDetails:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fDetails
                                                                        */
ASSIGN 
       tDetailsDate1:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsDate2:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsDate3:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsDatePlanned:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsERR1:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsERR2:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsERR3:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsERRPlanned:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsGrade1:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsGrade2:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsGrade3:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsGradePlanned:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsScore1:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsScore2:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsScore3:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsScorePlanned:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsType1:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsType2:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsType3:READ-ONLY IN FRAME fDetails        = TRUE.

ASSIGN 
       tDetailsTypePlanned:READ-ONLY IN FRAME fDetails        = TRUE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwQar tScoreAudit fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwQar:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwQar:HANDLE
       brwQar:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQar:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR BUTTON bSchedule IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAgentName:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQar
/* Query rebuild information for BROWSE brwQar
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQar */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Audit Scheduling */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Audit Scheduling */
DO:
  /* This event will close the window and terminate the procedure.  */
  std-lo = true.
  if dataChanged()
   then message "There are unsaved changes. Continue?" view-as alert-box question buttons yes-no update std-lo.
   
  if std-lo
   then
    do:
      APPLY "CLOSE":U TO THIS-PROCEDURE.
      RETURN NO-APPLY.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent Audit Scheduling */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  for each data no-lock
     where data.recordChanged = true:
    
    run CancelRow in this-procedure (data.agentID).
  end.
  bSchedule:sensitive in frame {&frame-name} = false.
  bCancel:sensitive in frame {&frame-name} = false.
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
  run setbutton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQar
&Scoped-define SELF-NAME brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON ROW-DISPLAY OF brwQar IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  /* set the last auditor's name */
  std-ch = "".
  publish "GetSysUserName" (data.lastAuditor, output std-ch).
  data.lastAuditor:screen-value in browse {&browse-name} = std-ch.
  /* set the audit's type */
  std-ch = "".
  publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.proposedAuditType, output std-ch).
  data.proposedAuditType:screen-value in browse {&browse-name} = std-ch.
  /* set the last auditor's name */
  std-ch = "".
  publish "GetSysUserName" (data.proposedAuditor, output std-ch).
  data.proposedAuditor:screen-value in browse {&browse-name} = std-ch.
  /* color the last four columns white */
  assign
    data.proposedAuditType:bgcolor = 15
    data.proposedAuditDate:bgcolor = 15
    data.proposedAuditor:bgcolor = 15
    data.recordChanged:bgcolor = 15
    .
/*     NO-ASSIGN */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON START-SEARCH OF brwQar IN FRAME fMain
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON VALUE-CHANGED OF brwQar IN FRAME fMain
DO:
  clearDetails().
  if not available data
   then return.
   
  do with frame {&frame-name}:
    assign
      /* the menu */
      menu-item m_Discard_Change:sensitive in menu POPUP-MENU-brwQAR = data.recordChange
      menu-item m_View_3_QAR:sensitive in menu POPUP-MENU-brwQAR = (data.auditFinishDate3 <> ?)
      menu-item m_View_2_QAR:sensitive in menu POPUP-MENU-brwQAR = (data.auditFinishDate2 <> ?)
      menu-item m_View_1_QAR:sensitive in menu POPUP-MENU-brwQAR = (data.auditFinishDate1 <> ?)
      /* agent details */
      tAgentName = data.agentName + " (" + data.agentID + ")"
      tAgentAddress = data.agentAddress
      tAgentState = data.agentCity + ", " + data.agentState + " " + data.agentZip
      .
    display
      /* agent details */
      tAgentName
      tAgentAddress
      tAgentState
      .
    tAgentName:tooltip = (if length(tAgentName:screen-value) > 90 then data.agentName + " (" + data.agentID + ")" else "").
  end.
  
  do with frame fDetails:
    /* types */
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType3, output tDetailsType3).
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType2, output tDetailsType2).
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType1, output tDetailsType1).
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.proposedAuditType, output tDetailsTypePlanned).
  
    assign
      /* dates */
      tDetailsDate3 = data.auditFinishDate3
      tDetailsDate2 = data.auditFinishDate2
      tDetailsDate1 = data.auditFinishDate1
      tDetailsDatePlanned = data.proposedAuditDate
      /* scores */
      tDetailsScore3 = (if data.year3AuditScore = 0 then "" else string(data.year3AuditScore))
      tDetailsScore2 = (if data.year2AuditScore = 0 then "" else string(data.year2AuditScore))
      tDetailsScore1 = (if data.year1AuditScore = 0 then "" else string(data.year1AuditScore))
      tDetailsScorePlanned = ""
      /* grades */
      tDetailsGrade3 = (if data.year3AuditGrade = 0 then "" else string(data.year3AuditGrade))
      tDetailsGrade2 = (if data.year2AuditGrade = 0 then "" else string(data.year2AuditGrade))
      tDetailsGrade1 = (if data.year1AuditGrade = 0 then "" else string(data.year1AuditGrade))
      tDetailsGradePlanned = ""
      /* err scores */
      tDetailsERR3 = (if data.year3ERRScore = 0 then "" else string(data.year3ERRScore))
      tDetailsERR2 = (if data.year2ERRScore = 0 then "" else string(data.year2ERRScore))
      tDetailsERR1 = (if data.year1ERRScore = 0 then "" else string(data.year1ERRScore))
      tDetailsERRPlanned = ""
      .
    display
      /* types */
      tDetailsType3
      tDetailsType2
      tDetailsType1
      tDetailsTypePlanned
      /* dates */
      tDetailsDate3
      tDetailsDate2
      tDetailsDate1
      tDetailsDatePlanned
      /* scores */
      tDetailsScore3
      tDetailsScore2
      tDetailsScore1
      tDetailsScorePlanned
      /* grade */
      tDetailsGrade3 
      tDetailsGrade2 
      tDetailsGrade1 
      tDetailsGradePlanned 
      /* err scores */
      tDetailsERR3
      tDetailsERR2
      tDetailsERR1
      tDetailsERRPlanned
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSchedule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSchedule C-Win
ON CHOOSE OF bSchedule IN FRAME fMain /* Schedule */
DO:
  run ScheduleAgents in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbState C-Win
ON VALUE-CHANGED OF cmbState IN FRAME fMain /* State */
DO:
  setFilterCombo(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbStatus C-Win
ON VALUE-CHANGED OF cmbStatus IN FRAME fMain /* Status */
DO:
  setFilterCombo(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbType C-Win
ON VALUE-CHANGED OF cmbType IN FRAME fMain /* Audit Type */
DO:
  setFilterCombo(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cSelectionYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cSelectionYear C-Win
ON VALUE-CHANGED OF cSelectionYear IN FRAME fMain
DO:
  if cSelectionYear:screen-value = std-ch then.
  else
    std-ch = cSelectionYear:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Discard_Change
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Discard_Change C-Win
ON CHOOSE OF MENU-ITEM m_Discard_Change /* Discard Changes */
DO:
  if available data
   then run CancelRow in this-procedure (data.agentID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_1_QAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_1_QAR C-Win
ON CHOOSE OF MENU-ITEM m_View_1_QAR /* View Last Year's QAR */
DO:
  if available data
   then run ViewQARSummary in this-procedure (data.qarID1).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_2_QAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_2_QAR C-Win
ON CHOOSE OF MENU-ITEM m_View_2_QAR /* View Two Year Ago QAR */
DO:
  if available data
   then run ViewQARSummary in this-procedure (data.qarID2).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_3_QAR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_3_QAR C-Win
ON CHOOSE OF MENU-ITEM m_View_3_QAR /* View Three Year Ago QAR */
DO:
  if available data
   then run ViewQARSummary in this-procedure (data.qarID3).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tApplyERR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tApplyERR C-Win
ON VALUE-CHANGED OF tApplyERR IN FRAME fMain /* Apply Interval ERR Parameter */
DO:
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditors C-Win
ON VALUE-CHANGED OF tAuditors IN FRAME fMain
DO:
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tScoreAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tScoreAudit C-Win
ON LEAVE OF tScoreAudit IN FRAME fMain /* QAR Points Minimum */
DO:
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tScoreERR
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tScoreERR C-Win
ON LEAVE OF tScoreERR IN FRAME fMain /* ERR Points Minimum */
DO:
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ON ENTRY OF data.proposedAuditType IN browse {&browse-name} /* proposedAuditType */
DO:
  if available data
   then self:screen-value = data.proposedAuditType.
END.

ON ENTRY OF data.proposedAuditor IN browse {&browse-name} /* proposedAuditor */
DO:
  if available data
   then self:screen-value = data.proposedAuditor.
END.

ON VALUE-CHANGED OF data.proposedAuditType IN browse {&browse-name} /* proposedAuditType */
DO:
  if not available data
   then return.
  
  assign
      cQarType    = data.proposedAuditType:input-value in browse {&browse-name}          
      cQarDate    = string(data.proposedAuditDate)
      cQarAuditor = data.proposedAuditor     
      .
      
  run ModifyRow in this-procedure (input cQarType,
                                   input cQarDate,
                                   input cQarAuditor
                                   ).
END.

ON LEAVE OF data.proposedAuditDate IN browse {&browse-name} /* proposedAuditDate */
DO:
  if not available data and self:modified
   then return.
      
  assign 
      cQarType    = data.proposedAuditType                
      cQarDate    = data.proposedAuditDate:input-value in browse {&browse-name}
      cQarAuditor = data.proposedAuditor     
      .
  
  run ModifyRow in this-procedure (input cQarType,
                                   input cQarDate,
                                   input cQarAuditor
                                   ).
END.

ON VALUE-CHANGED OF data.proposedAuditor IN browse {&browse-name} /* proposedAuditor */
DO:
  if not available data
   then return.
  
  assign
      cQarType    = data.proposedAuditType                
      cQarDate    = string(data.proposedAuditDate)
      cQarAuditor = data.proposedAuditor:input-value in browse {&browse-name}    
      .
      
  run ModifyRow in this-procedure (input cQarType,
                                   input cQarDate,
                                   input cQarAuditor
                                   ).
                                   
END.

ON VALUE-CHANGED OF data.recordChanged IN browse {&browse-name} /* recordChanged */
DO:
  if not available data
   then return.
   
  if not self:input-value
   then run CancelRow in this-procedure (data.agentID).
   else data.recordChanged = false.
  
  assign
    std-ch = dataSortBy
    dataSortBy = ""
    .
  run sortData in this-procedure (std-ch).
END.

{lib/brw-main.i}                   
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/win-main.i}

subscribe to "closewindow" anywhere.

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

publish "GetAuditors"(output table auditor).
std-ch = "ALL,ALL,UNASSIGNED,UNASSIGNED,ASSIGNED,ASSIGNED".
for each auditor
      by auditor.name:
  std-ch  = addDelimiter(std-ch,",") + auditor.name + "," + auditor.UID.
end.
tAuditors:list-item-pairs = std-ch.
cSelectionYear:list-items = string(year(today) + 1) + "," +  string(year(today)).
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bSchedule:load-image("images/save.bmp").
bSchedule:load-image-insensitive("images/save-i.bmp").
bCancel:load-image("images/cancel.bmp").
bCancel:load-image-insensitive("images/cancel-i.bmp").

cmbState:screen-value = "ALL".
cmbStatus:screen-value = "ALL".
cSelectionYear:screen-value = string(year(today) + 1).
std-ch = "".
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.  
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  cSelectionYear:screen-value = string(year(today) + 1).
  std-ha = getColumn({&browse-name}:handle, "AgentName").
  IF VALID-HANDLE(std-ha)
   THEN dColumn = std-ha:width * session:pixels-per-column.

  run SetButton.
  run windowResized in this-procedure.
  clearDetails().

IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CancelRow C-Win 
PROCEDURE CancelRow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define buffer tempdata for tempdata.
  
  for first data exclusive-lock
      where data.agentID = pAgentID:
      
    for first tempdata no-lock
        where tempdata.agentID = data.agentID:
      
      assign
        data.proposedAuditType = tempdata.proposedAuditType
        data.proposedAuditDate = tempdata.proposedAuditDate
        data.proposedAuditor = tempdata.proposedAuditor
        data.recordChanged = false  
        data.recordChanged:screen-value in browse brwQar = "No"
        menu-item m_Discard_Change:sensitive in menu POPUP-MENU-brwQar = false
        data.proposedAuditType:screen-value in browse {&browse-name} = tempdata.proposedAuditType
        data.proposedAuditor:screen-value in browse {&browse-name} = tempdata.proposedAuditor
        data.proposedAuditDate:screen-value in browse {&browse-name} = string(tempdata.proposedAuditDate)
        .
    end.
  end.
  
  changeButtonSensitive().  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closewindow C-Win 
PROCEDURE closewindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
apply 'close' to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createScheduleERR C-Win 
PROCEDURE createScheduleERR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcErrMsg as character no-undo.
 
 empty temp-table scheduleErr.
 
 define variable cErrFile as character no-undo.
 define variable iErrFile as integer no-undo.
         
 ipcErrMsg = trim(ipcErrMsg,' | ').
 do iErrFile = 1 to num-entries(ipcErrMsg,'|'):
 
   create scheduleErr.
   assign
       scheduleErr.err         = string(iErrFile)
       scheduleErr.description = trim(entry(iErrFile,ipcErrMsg,'|'))
       .
 end.
 
 if can-find(first scheduleErr)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "ScheduleErrors_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table scheduleErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "scheduleErr",
                              "for each scheduleErr",
                              "err,description",
                              "Sequence,Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
    end.    
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAgentAddress tAgentName tAgentNameLabel tAgentState cSelectionYear 
          cmbState cmbStatus cmbType tAuditors tApplyERR tScoreERR tScoreAudit 
          tAuditDetails 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tAgentAddress tAgentName tAgentNameLabel tAgentState cSelectionYear 
         bRefresh cmbState cmbStatus cmbType tAuditors tApplyERR tScoreERR 
         tScoreAudit brwQar tAuditDetails RECT-1 RECT-2 RECT-3 rIntervalERR 
         rDetails 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY tDetailsDate3 tDetailsDate2 tDetailsDate1 tDetailsDatePlanned 
          tDetailsType3 tDetailsType2 tDetailsType1 tDetailsTypePlanned 
          tDetailsGrade3 tDetailsGrade2 tDetailsGrade1 tDetailsGradePlanned 
          tDetailsScore3 tDetailsScore2 tDetailsScore1 tDetailsScorePlanned 
          tDetailsERR3 tDetailsERR2 tDetailsERR1 tDetailsERRPlanned 
      WITH FRAME fDetails IN WINDOW C-Win.
  ENABLE RECT-52 RECT-53 RECT-54 RECT-55 tDetailsDate3 tDetailsDate2 
         tDetailsDate1 tDetailsDatePlanned tDetailsType3 tDetailsType2 
         tDetailsType1 tDetailsTypePlanned tDetailsGrade3 tDetailsGrade2 
         tDetailsGrade1 tDetailsGradePlanned tDetailsScore3 tDetailsScore2 
         tDetailsScore1 tDetailsScorePlanned tDetailsERR3 tDetailsERR2 
         tDetailsERR1 tDetailsERRPlanned 
      WITH FRAME fDetails IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fDetails}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwQAR:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Schedule_Regular_QAR"

  std-ch = "".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table data.
  do with frame {&frame-name}:
    run server/getscheduleagents.p (input cSelectionYear:input-value,
                                    output table data,
                                    output std-lo,
                                    output std-ch
                                    ).
  end.
  
  if not std-lo
   then message std-ch view-as alert-box error buttons ok.
   
  run searchData.
  assign
      bSchedule:sensitive in frame {&frame-name} = false
      bCancel:sensitive in frame {&frame-name} = false
      .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyRow C-Win 
PROCEDURE ModifyRow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAuditType as character no-undo.
  define input parameter ipcDate      as character no-undo.
  define input parameter ipcAuditor   as character no-undo.
    
  define buffer tempdata for tempdata.
   
  std-lo = false.
  if data.proposedAuditID = 0
   then
    do:
      assign
          data.proposedAuditor =  ipcAuditor
          data.proposedAuditDate = datetime(ipcDate).
          data.proposedAuditType = ipcAuditType.
          .
          
      if data.proposedAuditor:input-value in browse {&browse-name} = "" or data.proposedAuditor:input-value in browse {&browse-name} = ?
       then 
        return.        
      if data.proposedAuditDate:input-value in browse {&browse-name} = ?
       then 
        return.
      if data.proposedAuditType:input-value in browse {&browse-name} = ""
       then 
        return.
      std-lo = true.
    end.
   else
    for first tempdata no-lock
        where tempdata.agentID = data.agentID:
          
      if tempdata.proposedAuditType <> ipcAuditType
       then std-lo = true.
       
      if tempdata.proposedAuditDate <> datetime(ipcDate)
       then std-lo = true. 
       
      if tempdata.proposedAuditor <> ipcAuditor
       then std-lo = true.
   end.
    
  /* if record was changed */
  if std-lo
   then
    assign
        data.recordChanged:screen-value in browse {&browse-name} = "yes"  
        data.proposedAuditType   = ipcAuditType    
        data.proposedAuditDate   = datetime(ipcDate)
        data.proposedAuditor     = ipcAuditor 
        data.recordChanged       = yes
        .
  else
   assign
       data.recordChanged:screen-value in browse {&browse-name} = "no"
       data.proposedAuditType   = ipcAuditType    
       data.proposedAuditDate   = datetime(ipcDate)
       data.proposedAuditor     = ipcAuditor 
       data.recordChanged       = no
       .
    
  changeButtonSensitive().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ScheduleAgents C-Win 
PROCEDURE ScheduleAgents :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer data for data.
  define buffer tempdata for tempdata.
    
  empty temp-table qar.
  for each data no-lock
     where data.recordChanged = true:
     
    for first tempdata no-lock
        where tempdata.agentID = data.agentID:
      
      std-ch.
      buffer-compare tempdata except recordChanged to data save result in std-ch.
      
      create qar.
      assign
        qar.qarID = data.proposedAuditID
        qar.auditYear = year(data.proposedAuditDate)
        qar.agentID = data.agentID
        qar.auditStartDate = data.proposedAuditDate
        qar.uid = data.proposedAuditor
        qar.stateID = data.agentState
        qar.auditType = data.proposedAuditType
        qar.errType = (if data.proposedAuditType = "E" then "I" else "")
        qar.schedNewAudit = (qar.qarID = 0)
        qar.schedReassign = (qar.qarID > 0 and lookup("proposedAuditor", std-ch) > 0)
        qar.schedReschedule = (qar.qarID > 0 and lookup("proposedAuditDate", std-ch) > 0)
        qar.schedAuditType = (qar.qarID > 0 and lookup("proposedAuditType", std-ch) > 0)
        .
    end.
  end.
   
  if can-find(first qar)
   then
    do:
      run server/setScheduledagents.p (input table qar,
                                       output cErrMsg,
                                       output std-lo,
                                       output std-ch
                                       ).
      
      std-ch = "".
      if not std-lo
       then std-ch = " with errors".
      message "Agent(s) successfully scheduled" + std-ch view-as alert-box information buttons ok.
      
      if cErrMsg <> ""
       then
        run createScheduleERR in this-procedure (input cErrMsg).
        
      run getData in this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE searchData C-Win 
PROCEDURE searchData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:      
------------------------------------------------------------------------------*/
  empty temp-table tempdata.
  std-in = 0.
    
  for each data no-lock:
    std-in = std-in + 1.
    /* buffer-copy each record */
    create tempdata.
    buffer-copy data to tempdata.
    /* set the proposed auditor */
    std-ch = "".
    for each auditor:
      std-ch  = addDelimiter(std-ch,",") + auditor.name + "," + auditor.UID.
    end.
    data.proposedAuditor:list-item-pairs in browse brwQar = std-ch.
    /* set the proposed type */
    std-ch = "".
    publish "GetSysPropListMinusID" ("QAR", "Audit", "Type", "U", output std-ch).
    data.proposedAuditType:list-item-pairs in browse brwQar = replace(std-ch, {&msg-dlm}, ",").
  end.
          
  std-ch = string(std-in) + " record(s) found" .
  status default std-ch in window {&window-name}.
  status input std-ch in window {&window-name}.

  setFilterCombo("ALL").
  run sortDataDefault in this-procedure ("","A","","").
  bExport:sensitive in frame {&frame-name} = (query brwQar:num-results > 0).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetButton C-Win 
PROCEDURE SetButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign
    std-lo = (query brwQar:num-results > 0)
    bExport:sensitive in frame {&frame-name} = std-lo
    .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStatus as character no-undo.
  define variable cState as character no-undo.
  define variable cType as character no-undo.
  define variable cAuditor as character no-undo.
  
  define variable lApplyERR as logical no-undo.
  define variable iScoreAudit as character no-undo.
  define variable iScoreERR as character no-undo.
  
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
    assign
      cStatus = cmbStatus:screen-value
      cState = cmbState:screen-value
      cType = cmbType:screen-value
      cAuditor = tAuditors:screen-value
      lApplyERR = tApplyERR:checked
      iScoreAudit = tScoreAudit:screen-value
      iScoreERR = tScoreERR:screen-value
      .
  end.
  
  /* build the query */
  if cStatus <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "AgentStatus = '" + cStatus + "'".
  if cState <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "AgentState = '" + cState + "'".
  if cType <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "proposedAuditType = '" + cType + "'".
  /* select on the auditor */
  if lookup("ALL", cAuditor) = 0
   then
    case cAuditor:
     when "UNASSIGNED" then tWhereClause = addDelimiter(tWhereClause," and ") + "proposedAuditor = ''".
     when "ASSIGNED" then tWhereClause = addDelimiter(tWhereClause," and ") + "proposedAuditor <> ''".
     otherwise
      do:
        tWhereClause = addDelimiter(tWhereClause," and ") + "(".
        do std-in = 1 to num-entries(cAuditor):
          tWhereClause = tWhereClause + "proposedAuditor='" + entry(std-in,cAuditor) + "'".
          if std-in < num-entries(cAuditor)
           then tWhereClause = tWhereClause + " or ".
        end.
        tWhereClause = tWhereClause + ")".
      end.
    end case.
    
  /* apply ERR */
  if lApplyERR
   then tWhereClause = addDelimiter(tWhereClause," and ") + "((auditType1 = 'Q' and auditType2 = 'Q' and auditType3 = 'Q' and " +
                                                            "year1AuditScore >= " + iScoreAudit + " and year2AuditScore >= " + iScoreAudit + " and year3AuditScore >= " + iScoreAudit + " and " +
                                                            "year1ERRScore >= " + iScoreERR + " and year2ERRScore >= " + iScoreERR + " and year3ERRScore >= " + iScoreERR + ") or " +
                                                            "(auditType1 = 'Q' and auditType2 = 'E' and auditType3 = 'Q' and " +
                                                            "year1AuditScore >= " + iScoreAudit + " and year3AuditScore >= " + iScoreAudit + " and " +
                                                            "year1ERRScore >= " + iScoreERR + " and year2ERRScore >= " + iScoreERR + " and year3ERRScore >= " + iScoreERR + "))".
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  apply "VALUE-CHANGED" to browse {&browse-name}.
  std-ch = string(num-results("{&browse-name}")) + " record(s) found".
  status default std-ch in window {&window-name}.
  status input std-ch in window {&window-name}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortDataDefault C-Win 
PROCEDURE sortDataDefault :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cDefaultState as character no-undo.
  define input parameter cDefaultStatus as character no-undo.
  define input parameter cDefaultType as character no-undo.
  define input parameter cDefaultAuditor as character no-undo.
  
  do with frame {&frame-name}:
    if cDefaultState > "" and lookup(cDefaultState, cmbState:list-item-pairs) > 0
     then cmbState:screen-value = cDefaultState.
    
    if cDefaultStatus > "" and lookup(cDefaultStatus, cmbStatus:list-item-pairs) > 0
     then cmbStatus:screen-value = cDefaultStatus.
    
    if cDefaultType > "" and lookup(cDefaultType, cmbType:list-item-pairs) > 0
     then cmbType:screen-value = cDefaultType.
    
    if cDefaultAuditor > "" and lookup(cDefaultAuditor, tAuditors:list-item-pairs) > 0
     then tAuditors:screen-value = cDefaultAuditor.
  end.
  
  std-ch = dataSortBy.
  dataSortBy = "".
  run sortData in this-procedure (std-ch).
  apply "VALUE-CHANGED" to brwQAR in frame {&frame-name}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewQARSummary C-Win 
PROCEDURE ViewQARSummary :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pQarID as integer no-undo.

  publish "checkOpenQar" (input pQarID,
                          output lOpenQar,
                          output hQarSummary
                          ).
  if lOpenQar
   then
    do:
      run ViewWindow in hQarSummary no-error.
      return.
    end.
   else
    do:
      {lib/pbshow.i "''"}
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
      empty temp-table qar.
      publish "GetQarAudits" (0, pQarID, "", "", output table qar).
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 50}
      run QARSummary.w persistent (input table qar).
      run ViewWindow in hQarSummary no-error.
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
      {lib/pbhide.i}
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define  variable  dDiffWidth as decimal no-undo.
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.

  
  /* get the difference between the old and new width\height */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.
  frame fMain:width-pixels = {&window-name}:width-pixels.       
  /* resize browse */
  browse brwQAR:height-pixels = frame fMain:height-pixels - browse brwQAR:y - 179.
  browse brwQAR:width-pixels = frame fMain:width-pixels - 20.
  /* browse column */
  std-ha = getColumn({&browse-name}:handle, "AgentName").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + dDiffWidth.
   
  do with frame {&frame-name}:
    /* rectangles */
    rIntervalERR:width-pixels = frame fMain:width-pixels - rIntervalERR:x - 9.
    rDetails:width-pixels = frame fMain:width-pixels - 20.
    /* audit details */
    tAuditDetails:y = frame fMain:height-pixels - 174.
    rDetails:y = frame fMain:height-pixels - 169.
    tAgentNameLabel:y = frame fMain:height-pixels - 131.
    tAgentName:y = frame fMain:height-pixels - 134 .
    tAgentAddress:y = frame fMain:height-pixels - 109 .
    tAgentState:y = frame fMain:height-pixels - 94.
    /* we need to center the frame */
    frame fDetails:x = (frame fMain:width-pixels - frame fDetails:width-pixels + 525) / 2.
    frame fDetails:y = rDetails:y + 10 . 
  end.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION changeButtonSensitive C-Win 
FUNCTION changeButtonSensitive RETURNS logical
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define buffer data for data.
  
  find first data where data.recordChanged = true no-error.
  if available data
   then
    assign
        bSchedule:sensitive in frame {&frame-name} = true
        bCancel:sensitive in frame {&frame-name} = true
        menu-item m_Discard_Change:sensitive in menu POPUP-MENU-brwQar = true
        .
  else
   assign
       bSchedule:sensitive in frame {&frame-name} = false
       bCancel:sensitive in frame {&frame-name} = false
       menu-item m_Discard_Change:sensitive in menu POPUP-MENU-brwQar = false
       .
       
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearDetails C-Win 
FUNCTION clearDetails RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      /* agent details */
      tAgentName = ""
      tAgentAddress = ""
      tAgentState = ""
      .
    display
      /* agent details */
      tAgentName
      tAgentAddress
      tAgentState
      .
  end.
  
  do with frame fDetails:
    /* types */
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType3, output tDetailsType3).
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType2, output tDetailsType2).
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType1, output tDetailsType1).
    publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.proposedAuditType, output tDetailsTypePlanned).
    assign
      /* types */
      tDetailsType3 = ""
      tDetailsType2 = ""
      tDetailsType1 = ""
      tDetailsTypePlanned = ""
      /* dates */
      tDetailsDate3 = ?
      tDetailsDate2 = ?
      tDetailsDate1 = ?
      tDetailsDatePlanned = ?
      /* scores */
      tDetailsScore3 = ""
      tDetailsScore2 = ""
      tDetailsScore1 = ""
      tDetailsScorePlanned = ""
      /* grade */
      tDetailsGrade3 = ""
      tDetailsGrade2 = ""
      tDetailsGrade1 = ""
      tDetailsGradePlanned = ""
      /* err scores */
      tDetailsERR3 = ""
      tDetailsERR2 = ""
      tDetailsERR1 = ""
      tDetailsERRPlanned = ""
      .
    display
      /* types */
      tDetailsType3
      tDetailsType2
      tDetailsType1
      tDetailsTypePlanned
      /* dates */
      tDetailsDate3
      tDetailsDate2
      tDetailsDate1
      tDetailsDatePlanned
      /* scores */
      tDetailsScore3
      tDetailsScore2
      tDetailsScore1
      tDetailsScorePlanned
      /* grade */
      tDetailsGrade3 
      tDetailsGrade2 
      tDetailsGrade1 
      tDetailsGradePlanned 
      /* err scores */
      tDetailsERR3
      tDetailsERR2
      tDetailsERR1
      tDetailsERRPlanned
      .
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION dataChanged C-Win 
FUNCTION dataChanged RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Determines if the data changed in the report or not
------------------------------------------------------------------------------*/
  define variable lChanged as logical no-undo init true.

  define buffer data for data.
  define buffer tempdata for tempdata.
  
  for each data no-lock:
    if lChanged
     then
      for first tempdata no-lock
          where tempdata.agentID = data.agentID:
        
        buffer-compare tempdata except recordChanged to data save result in lChanged.
      end.
  end.

  RETURN not lChanged.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pManager as character,
    input pStatus as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cStateList as character no-undo.
  define variable cName as character no-undo.
  define buffer data for data.
 
  cStateList = "ALL,ALL".
  for each data no-lock:
  
    if pManager <> "ALL" and pManager <> data.AgentManager
     then next.
     
    if pStatus <> "ALL" and pStatus <> data.AgentStatus
     then next.
   
    if lookup(data.AgentState,cStateList) = 0
     then cStateList = addDelimiter(cStateList,",") + getStateName(data.AgentState) + "," + data.AgentState.
  end.
  RETURN cStateList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character,
    input pManager as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent statuses for the state
------------------------------------------------------------------------------*/
  define variable cStatusList as character no-undo.
  define variable cStatus as character no-undo.
  define buffer data for data.
 
  cStatusList = "ALL,ALL".
  for each data no-lock:
    
    if pState <> "ALL" and pState <> data.AgentState
     then next.
  
    if pManager <> "ALL" and pManager <> data.AgentManager
     then next.
     
    if lookup(data.AgentStatus,cStatusList) = 0
     then cStatusList = addDelimiter(cStatusList,",") + data.AgentStatusDesc + "," + data.AgentStatus.
  end.
  RETURN cStatusList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterTypeCombo C-Win 
FUNCTION getFilterTypeCombo RETURNS CHARACTER
  ( input pState as character,
    input pStatus as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cList as character no-undo.
  define variable cName as character no-undo.
  define buffer data for data.
 
  cList = "ALL,ALL".
  for each data no-lock:
  
    if pState <> "ALL" and pState <> data.AgentState
     then next.
     
    if pStatus <> "ALL" and pStatus <> data.AgentStatus
     then next.
   
    if data.proposedAuditType <> "" and lookup(data.proposedAuditType,cList) = 0
     then 
      do:
        publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.proposedAuditType, output cName).
        cList = addDelimiter(cList,",") + cName + "," + data.proposedAuditType.
      end.
  end.
  RETURN cList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getstatus C-Win 
FUNCTION getstatus RETURNS CHARACTER
  (input pstat as char /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pstat = "E" then
  RETURN "ERR".   /* Function return value. */
  else if pstat = "Q" then
  RETURN "QAR".   /* Function return value. */
  else if pstat = "U" then
  RETURN "Underwriter".   /* Function return value. */
  else if pstat = "T" then
  RETURN "TOR".   /* Function return value. */
  else  
  return "".
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombo C-Win 
FUNCTION setFilterCombo RETURNS LOGICAL
  ( input pName as character ) :
/*------------------------------------------------------------------------------
@description Sets the filter combo boxes
------------------------------------------------------------------------------*/
  define variable sortColumn as character no-undo.
  do with frame {&frame-name}:
    assign
      currState = cmbState:screen-value
      currType = cmbType:screen-value
      currStatus = cmbStatus:screen-value
      .
    case pName:
     when "ALL" then
      assign
        cmbState:list-item-pairs = getFilterStateCombo(currType, currStatus)
        cmbType:list-item-pairs = getFilterTypeCombo(currState, currStatus)
        cmbStatus:list-item-pairs = getFilterStatusCombo(currState, currType)
        .
     when "State" then
      assign
        cmbType:list-item-pairs = getFilterTypeCombo(currState, currStatus)
        cmbStatus:list-item-pairs = getFilterStatusCombo(currState, currType)
        .
     when "Manager" then
      assign
        cmbState:list-item-pairs = getFilterStateCombo(currType, currStatus)
        cmbStatus:list-item-pairs = getFilterStatusCombo(currState, currType)
        .
     when "Status" then
      assign
        cmbState:list-item-pairs = getFilterStateCombo(currType, currStatus)
        cmbType:list-item-pairs = getFilterTypeCombo(currState, currStatus)
        .
    end case.
    /* set the state */
    if lookup(currState,cmbState:list-item-pairs) > 0
     then cmbState:screen-value = currState.
     else cmbState:screen-value = "ALL".
    /* set the manager */
    if lookup(currType,cmbType:list-item-pairs) > 0
     then cmbType:screen-value = currType.
     else cmbType:screen-value = "ALL".
    /* set the status */
    if lookup(currStatus,cmbStatus:list-item-pairs) > 0
     then cmbStatus:screen-value = currStatus.
     else cmbStatus:screen-value = "ALL".
  end.
  sortColumn = dataSortBy.
  dataSortBy = "".
  run sortData in this-procedure (sortColumn).
  apply "VALUE-CHANGED" to brwQAR in frame {&frame-name}.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

