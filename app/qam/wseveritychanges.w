&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wseveritychanges.w
   Window of severitychanges across qar records
   08.16.2017
   @Anjly Chanana
   @Modified 07/15/2021 SA Task 83510 modified UI according to new field grade and score
             09/24/2021 SA Task 86696 Defects raised by david
   */

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/get-column.i}

{tt/agent.i}
{tt/auditor.i}
{tt/severitychanges.i}
{tt/state.i}

define variable dColumn as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwSeverityChanges

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES severitychanges

/* Definitions for BROWSE brwSeverityChanges                            */
&Scoped-define FIELDS-IN-QUERY-brwSeverityChanges severitychanges.qarID severitychanges.auditType severitychanges.errType severitychanges.state severitychanges.agentID severitychanges.agentName severitychanges.score severitychanges.grade severitychanges.auditor severitychanges.questionDesc severitychanges.changeDesc severitychanges.originalSeverity severitychanges.reportedSeverity   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSeverityChanges   
&Scoped-define SELF-NAME brwSeverityChanges
&Scoped-define QUERY-STRING-brwSeverityChanges FOR EACH severitychanges by severitychanges.qarID
&Scoped-define OPEN-QUERY-brwSeverityChanges OPEN QUERY {&SELF-NAME} FOR EACH severitychanges by severitychanges.qarID.
&Scoped-define TABLES-IN-QUERY-brwSeverityChanges severitychanges
&Scoped-define FIRST-TABLE-IN-QUERY-brwSeverityChanges severitychanges


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwSeverityChanges}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-45 RECT-46 tYear tStateID tAuditor ~
bRefresh bExport tQarID tAgentID brwSeverityChanges 
&Scoped-Define DISPLAYED-OBJECTS tYear tStateID tAuditor tQarID tAgentID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 85.2 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL",0
     DROP-DOWN-LIST
     SIZE 13.2 BY 1 NO-UNDO.

DEFINE VARIABLE tQarID AS INTEGER FORMAT ">>>>>>>>":U INITIAL 0 
     LABEL "QAR ID" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 141 BY 3.1.

DEFINE RECTANGLE RECT-46
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.2 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwSeverityChanges FOR 
      severitychanges SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwSeverityChanges
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSeverityChanges C-Win _FREEFORM
  QUERY brwSeverityChanges DISPLAY
      severitychanges.qarID label "QAR ID" format "99999999" width 10
severitychanges.auditType label "Audit Type" format "x(20)" width 12
severitychanges.errType label "ERR Type" format "x(20)" width 12
severitychanges.state column-label "State" format "x(15)" width 7
severitychanges.agentID label "Agent ID" format "x(10)" width 12
severitychanges.agentName label "Agent Name" format "x(200)" width 22
severitychanges.score label "Points" format "zzz"       width 8
severitychanges.grade label "Score%" format "zzz"       width 9
severitychanges.auditor label "Auditor" format "x(50)" width 28
severitychanges.questionDesc label "Question Description" format "x(150)" width 22
severitychanges.changeDesc label "Finding Description" format "x(150)" width 22
severitychanges.originalSeverity column-label "Original!Severity" format "x(20)" width 12
severitychanges.reportedSeverity column-label "Reported!Severity" format "x(20)" width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 207 BY 21.62 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tYear AT ROW 1.95 COL 12 COLON-ALIGNED WIDGET-ID 10
     tStateID AT ROW 1.95 COL 35 COLON-ALIGNED WIDGET-ID 150
     tAuditor AT ROW 1.95 COL 71 COLON-ALIGNED WIDGET-ID 152
     bRefresh AT ROW 2.19 COL 133 WIDGET-ID 4
     bExport AT ROW 2.19 COL 146 WIDGET-ID 156
     tQarID AT ROW 2.52 COL 108 COLON-ALIGNED WIDGET-ID 166
     tAgentID AT ROW 3.14 COL 12 COLON-ALIGNED WIDGET-ID 22
     brwSeverityChanges AT ROW 4.81 COL 3 WIDGET-ID 200
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.1 COL 144.6 WIDGET-ID 164
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 162
     RECT-45 AT ROW 1.43 COL 3 WIDGET-ID 158
     RECT-46 AT ROW 1.43 COL 143.6 WIDGET-ID 160
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 211.2 BY 25.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Severity Changes"
         HEIGHT             = 25.86
         WIDTH              = 211.2
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwSeverityChanges tAgentID fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwSeverityChanges:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwSeverityChanges:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSeverityChanges
/* Query rebuild information for BROWSE brwSeverityChanges
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH severitychanges by severitychanges.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwSeverityChanges */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Severity Changes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Severity Changes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Severity Changes */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSeverityChanges
&Scoped-define SELF-NAME brwSeverityChanges
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSeverityChanges C-Win
ON ROW-DISPLAY OF brwSeverityChanges IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSeverityChanges C-Win
ON START-SEARCH OF brwSeverityChanges IN FRAME fMain
DO: 
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON VALUE-CHANGED OF tAgentID IN FRAME fMain /* Agent */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditor C-Win
ON VALUE-CHANGED OF tAuditor IN FRAME fMain /* Auditor */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tQarID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tQarID C-Win
ON VALUE-CHANGED OF tQarID IN FRAME fMain /* QAR ID */
DO:
  do with frame {&frame-name}:
    std-lo = true.
    if self:input-value > 0
     then std-lo = false.
     
    assign
      tYear:sensitive = std-lo
      tStateID:sensitive = std-lo
      tAuditor:sensitive = std-lo
      tAgentID:sensitive = std-lo
      .
  end.
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain /* Year */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

/* get the years */
std-ch = "ALL,0".
do std-in = 0 to 9:
  std-ch =  addDelimiter(std-ch,",") + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in).
end.
tYear:list-item-pairs = std-ch.
tYear:screen-value = "0".

/* get the auditors */
publish "GetAuditors"(output table auditor).
tAuditor:delimiter = {&msg-dlm}.
std-ch = "ALL" {&msg-add} "ALL".
for each auditor no-lock:
  std-ch = addDelimiter(std-ch,{&msg-dlm}) + auditor.name {&msg-add} auditor.uid.
end.
tAuditor:list-item-pairs = std-ch.
tAuditor:screen-value = "ALL".

/* get the states */
{lib/get-state-list.i &combo=tStateID &addAll=true}
{lib/get-agent-list.i &combo=tAgentID &state=tStateID &addAll=true}.

session:immediate-display = yes.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  bExport:sensitive = false.
  
  std-ha = getColumn({&browse-name}:handle, "agentName").
  IF VALID-HANDLE(std-ha)
   THEN dColumn = std-ha:width * session:pixels-per-column.
  
  publish "GetCurrentValue" ("QARID", output std-in).
  if std-in > 0
   then
    do:
      define variable pAgentName as character no-undo.
      define variable pAgentID as character no-undo.
      
      tQarID:screen-value = string(std-in).
      apply "VALUE-CHANGED" to tQarID.
      publish "GetCurrentValue" ("AgentName", output pAgentName).
      publish "GetCurrentValue" ("Agent", output pAgentID).
      {&window-name}:title = {&window-name}:title + " - " + string(std-in) + " - " + pAgentName + " (" + pAgentID + ")".
      run getdata.
    end.
   else
    do:
      /* set the state */
      std-ch = "".
      publish "GetCurrentValue" ("State", output std-ch).
      if std-ch > "" and lookup(std-ch,tStateID:list-item-pairs) > 0
       then tStateID:screen-value = std-ch.
       else tStateID:screen-value = entry(2, tStateID:list-item-pairs).
       
      /* get the agents */
      apply "VALUE-CHANGED" to tStateID.
      
      /* set the agent */
      std-ch = "".
      publish "GetCurrentValue" ("Agent", output std-ch).
      if std-ch > "" and lookup(std-ch,tAgentID:list-item-pairs,{&msg-dlm}) > 0
       then tAgentID:screen-value = std-ch.
       else tAgentID:screen-value = "ALL".
      
      /* set the auditor */
      std-ch = "".
      publish "GetCurrentValue" ("Auditor", output std-ch).
      if std-ch > "" and lookup(std-ch,tAuditor:list-item-pairs,{&msg-dlm}) > 0
       then tAuditor:screen-value = std-ch.
       else tAuditor:screen-value = "ALL".
      
      /* set the year */
      std-ch = "".
      publish "GetCurrentValue" ("Year", output std-ch).
      if std-ch > "" and lookup(std-ch,tYear:list-item-pairs) > 0
       then tYear:screen-value = std-ch.
       else tYear:screen-value = "0".
    end.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tYear tStateID tAuditor tQarID tAgentID 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-45 RECT-46 tYear tStateID tAuditor bRefresh bExport tQarID 
         tAgentID brwSeverityChanges 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwSeverityChanges:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
  
  &scoped-define ReportName "Severity_Changes"
  
  std-ch = "".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iYear as integer no-undo.
  define variable cStateID as character no-undo.
  define variable cAuditor as character no-undo.
  define variable cAgentID as character no-undo.
  define variable iQarID as integer no-undo.

  clearData().
  do with frame {&frame-name}:
    assign
      iYear = tYear:input-value
      cStateID = tStateID:input-value
      cAuditor = tAuditor:input-value
      cAgentID = tAgentID:input-value
      iQarID = tQarID:input-value
      .
    
    empty temp-table severitychanges.
    run server/getseveritychanges.p (input iYear,
                                     input iQarID,
                                     input cStateID,
                                     input cAgentID,
                                     input cAuditor,
                                     output table severitychanges,
                                     output std-lo,
                                     output std-ch
                                     ).
                            
    if not std-lo
     then message std-ch view-as alert-box error buttons ok.
     else
      do:
        for each severitychanges exclusive-lock:
          severitychanges.changeDesc = replace(severitychanges.changeDesc,chr(10),"").
        end.
        run SortData("").
        
        if query brwSeverityChanges:num-results = 0
         then bExport:sensitive = false.
         else bExport:sensitive = true.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &post-by-clause=" + ' by qarID' "}
  std-ch =  " finding(s) found as of " + string(now).
  status default string(num-results("{&browse-name}")) + std-ch in window {&window-name}.
  status input string(num-results("{&browse-name}")) + std-ch  in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewWindow C-Win 
PROCEDURE ViewWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized  then
    {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dDiffWidth as decimal no-undo.
  
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* fMain components */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.
  brwSeverityChanges:width-pixels = frame fmain:width-pixels - 20 no-error.
  brwSeverityChanges:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 10 no-error.
 
  /* change the columns */
  std-ha = getColumn({&browse-name}:handle, "agentName").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + (dDiffWidth / 3).
   
  std-ha = getColumn({&browse-name}:handle, "questionDesc").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + (dDiffWidth / 3).
   
  std-ha = getColumn({&browse-name}:handle, "changeDesc").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + (dDiffWidth / 3).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query brwSeverityChanges.
  empty temp-table severitychanges.
  status default "".
  status input "".
  bExport:sensitive in frame {&frame-name} = false.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

