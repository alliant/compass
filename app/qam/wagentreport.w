&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wagentbyyear.w
   Window of AGENTs
   @author AC
   @modification
   Date          Name           Description
   10/14/2020    Anjly Chanana  Modified for TOR Audits.
   07/15/2021    SA             Task 83510 modified UI according 
                                 to new field grade
   09/24/2021    SA             Task 86696 Defects raised by david
   */

CREATE WIDGET-POOL.
{lib/std-def.i}
{tt/agentbyyear.i &tableAlias="Agents"}    
{tt/qaraudit.i &TableAlias="ttaudit"}
{tt/state.i}

def temp-table agentbyyear like Agents
   fields qID as char
   fields agentStat as char.
def temp-table tagentbyyear  like agentbyyear.

def temp-table ttagentbyyear like agents.

def temp-table openQars
 field qarid as int
 field hInstance as handle.

def variable iBgColor as integer no-undo.
def var yearpair as char.
def var tYear as char.
def var pType as char.
def var agentrecord as int no-undo.
define variable hColumn  as widget-handle .
define variable hBrw as widget-handle .
def var activeAudit as char no-undo.
def var activeRowid as rowid no-undo.

def var openqar as logical.
def var qarsummaryhandle as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAgents

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES agentbyyear

/* Definitions for BROWSE brwAgents                                     */
&Scoped-define FIELDS-IN-QUERY-brwAgents entry(index("ACQXP0", agentbyyear.qarstatus), "Active,Completed,Queued,Cancelled,Planned, ") @ agentbyyear.qarstatus entry(index("EQUT0", agentbyyear.audittype),"ERR,QAR,UnderWriter,TOR, ") @ agentbyyear.audittype entry(index("IPCM0", agentbyyear.errtype), "Interval,Presign,Corrective,Monthly, ") @ agentbyyear.errtype agentbyyear.qID agentbyyear.stateID "State" agentbyyear.score agentbyyear.grade agentbyyear.draftreportDate agentbyyear.auditFinishDate agentbyyear.contractDate agentbyyear.reviewDate entry(index("ACXPW0", agentbyyear.stat), "Active,Closed,Cancelled,Prospect,Withdrawn, ") @ agentbyyear.stat agentbyyear.agentID "Agent ID" agentbyyear.name "Agent Name"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAgents   
&Scoped-define SELF-NAME brwAgents
&Scoped-define QUERY-STRING-brwAgents FOR EACH agentbyyear by agentbyyear.qarstatus desc
&Scoped-define OPEN-QUERY-brwAgents OPEN QUERY {&SELF-NAME} FOR EACH agentbyyear by agentbyyear.qarstatus desc.
&Scoped-define TABLES-IN-QUERY-brwAgents agentbyyear
&Scoped-define FIRST-TABLE-IN-QUERY-brwAgents agentbyyear


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwAgents}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwAgents bExport bRefresh auditStatus ~
agentType cbyear cState RECT-37 RECT-38 
&Scoped-Define DISPLAYED-OBJECTS auditStatus agentType cbyear cState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwAgents 
       MENU-ITEM m_View_QAR_Summary LABEL "View QAR Summary".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE auditStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Active","A",
                     "Planned","P",
                     "Completed","C",
                     "Queued","Q",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 27.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbyear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 27.8 BY 1 NO-UNDO.

DEFINE VARIABLE agentType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Agents with an audit", "Yes",
"Agents without an audit", "No",
"Both", ""
     SIZE 28.6 BY 2.14 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.2 BY 3.1.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 39 BY 3.1.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73.4 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAgents FOR 
      agentbyyear SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAgents C-Win _FREEFORM
  QUERY brwAgents DISPLAY
      entry(index("ACQXP0",  agentbyyear.qarstatus), "Active,Completed,Queued,Cancelled,Planned, ") @ agentbyyear.qarstatus label "Status" format "x(12)"
 entry(index("EQUT0", agentbyyear.audittype),"ERR,QAR,UnderWriter,TOR, ") @ agentbyyear.audittype label "Audit Type"      format "x(12)" 
 entry(index("IPCM0", agentbyyear.errtype), "Interval,Presign,Corrective,Monthly, ") @ agentbyyear.errtype label "ERR Type"      format "x(12)" 
 agentbyyear.qID label "QAR ID"  width 15 format "x(20)" 
 agentbyyear.stateID          label         "State"                         width 12
 agentbyyear.score label "Points" format "zzz"  width 8
 agentbyyear.grade label "Score%" format "zzz"  width 9
 agentbyyear.draftreportDate  column-label  "Preliminary!Report Date" format "99/99/99" width 12
 agentbyyear.auditFinishDate  column-label  "Audit!Finish Date" format "99/99/99"
 agentbyyear.contractDate     column-label  "Contract!Date" format "99/99/99" width 12
 agentbyyear.reviewDate       column-label  "Last!Review"   format "99/99/99" width 12
 entry(index("ACXPW0",  agentbyyear.stat), "Active,Closed,Cancelled,Prospect,Withdrawn, ") @ agentbyyear.stat label "Agent Status" format "x(12)" 
 agentbyyear.agentID          label         "Agent ID"      format "x(12)"
 agentbyyear.name             label         "Agent Name"    format "x(78)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 184 BY 21.71 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwAgents AT ROW 4.76 COL 3 WIDGET-ID 200
     bExport AT ROW 2.05 COL 117.2 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 2.05 COL 32 WIDGET-ID 4 NO-TAB-STOP 
     auditStatus AT ROW 2.91 COL 49.4 COLON-ALIGNED WIDGET-ID 164
     agentType AT ROW 1.71 COL 83.8 NO-LABEL WIDGET-ID 62
     cbyear AT ROW 2.43 COL 9 COLON-ALIGNED WIDGET-ID 22
     cState AT ROW 1.81 COL 49.4 COLON-ALIGNED WIDGET-ID 150
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 115.6 WIDGET-ID 66
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.14 COL 42.6 WIDGET-ID 168
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 3.8 WIDGET-ID 162
     RECT-2 AT ROW 1.43 COL 114.8 WIDGET-ID 8
     RECT-37 AT ROW 1.43 COL 3 WIDGET-ID 58
     RECT-38 AT ROW 1.43 COL 41.6 WIDGET-ID 166
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 188 BY 25.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent QAR/ERR/TOR by Year"
         HEIGHT             = 25.81
         WIDTH              = 188
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAgents 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwAgents:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwAgents:HANDLE
       brwAgents:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwAgents:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAgents
/* Query rebuild information for BROWSE brwAgents
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH agentbyyear by agentbyyear.qarstatus desc.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAgents */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent QAR/ERR/TOR by Year */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent QAR/ERR/TOR by Year */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Agent QAR/ERR/TOR by Year */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME agentType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL agentType C-Win
ON VALUE-CHANGED OF agentType IN FRAME fMain
DO:
   open query brwAgents for each agentbyyear where agentbyyear.qarstatus = (if auditStatus:screen-value = "ALL" then agentbyyear.qarstatus else auditStatus:screen-value) and
                                          agentbyyear.whetheraudited = (if agentType:screen-value = "" then  agentbyyear.whetheraudited  else logical(agentType:screen-value)) and
                                          agentbyyear.state = (if cState:screen-value = "ALL" then agentbyyear.state else cState:screen-value)
                                         by agentbyyear.qarstatus desc.

  run setCount.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME auditStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL auditStatus C-Win
ON VALUE-CHANGED OF auditStatus IN FRAME fMain /* Status */
DO:
   
/*     open query brwAgents for each agentbyyear where agentbyyear.qarstatus = (if auditStatus:screen-value = "" then agentbyyear.qarstatus else auditStatus:screen-value) and           */
/*                                              agentbyyear.whetheraudited = (if agentType:screen-value = "" then  agentbyyear.whetheraudited  else logical(agentType:screen-value)) and */
/*                                              agentbyyear.state = (if cState:screen-value = "" then agentbyyear.state else cState:screen-value)                                        */
/*                                              by agentbyyear.qarstatus desc.                                                                                                           */
/*  run setCount.                                                                                                                                                                        */

    apply 'value-changed' to agentType.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "GetAgentbyYearData" (input cbyear:screen-value,
                                output table ttagentbyyear).
 
  empty temp-table agentbyyear.
  for each ttagentbyyear:
    create agentbyyear.
    buffer-copy ttagentbyyear to agentbyyear.
    assign
      agentbyyear.qID             = if ttagentbyyear.qarID          = 0  then "" else string(ttagentbyyear.qarID)
      agentbyyear.stateID         = if ttagentbyyear.stateID        = "?" then "" else agentbyyear.stateID 
      agentbyyear.agentID         = if ttagentbyyear.agentID        = "?" then "" else agentbyyear.agentID                
      agentbyyear.whetherAudited  = if ttagentbyyear.whetherAudited =  ? then logical("") else agentbyyear.whetherAudited  
      agentbyyear.stat            = if ttagentbyyear.stat           =  "" then "0" else agentbyyear.stat               
      agentbyyear.contractDate    = if ttagentbyyear.contractDate   =  ? then datetime("") else agentbyyear.contractDate   
      agentbyyear.reviewDate      = if ttagentbyyear.reviewDate     =  ? then datetime("") else agentbyyear.reviewDate     
      agentbyyear.name            = if ttagentbyyear.name           = "?" then ""  else agentbyyear.name 
      agentbyyear.qarstatus       = if ttagentbyyear.qarstatus      =  "" then "0" else agentbyyear.qarstatus 
      agentbyyear.agentStat       = if ttagentbyyear.whetherAudited =  ? then string("") else string(agentbyyear.whetherAudited)
      agentbyyear.audittype =  if ttagentbyyear.audittype = ""  or ttagentbyyear.audittype = "?" then "0" else  agentbyyear.audittype
      agentbyyear.errtype =  if   ttagentbyyear.errtype = "" or ttagentbyyear.errtype = "?" then "0" else  agentbyyear.errtype .

      if agentbyyear.audittype = "Q" or
             agentbyyear.audittype = "T" then
         agentbyyear.errtype = "0".
     
     agentrecord = agentrecord + 1 .
    end.
    status default string(agentrecord)+ " record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") in window {&window-name}. 
    status input string(agentrecord)+ " record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") in window {&window-name}. 
    agentrecord = 0.
/*     OPEN QUERY brwAgents FOR EACH agentbyyear by agentbyyear.qarstatus desc. */

   open query brwAgents for each agentbyyear where agentbyyear.qarstatus = (if auditStatus:screen-value = "ALL" then agentbyyear.qarstatus else auditStatus:screen-value) and
                                                  agentbyyear.whetheraudited =  (if agentType:screen-value = "" then  agentbyyear.whetheraudited  else logical(agentType:screen-value)) and 
                                                  agentbyyear.state = (if cState:screen-value = "ALL" then agentbyyear.state else cState:screen-value)
                                                  by agentbyyear.qarstatus desc.
   if query brwAgents:num-results = 0
    then
     assign cState     :sensitive = false
            auditStatus:sensitive = false
            agentType  :sensitive = false
            bexport    :sensitive = false.
    else 
     assign cState     :sensitive = true
            auditStatus:sensitive = true
            agentType  :sensitive = true
            bexport    :sensitive = true.

    tyear = cbYear:screen-value in frame fMain.
    pType = agentType:screen-value in frame fMain.
   run Setcount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAgents
&Scoped-define SELF-NAME brwAgents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgents C-Win
ON DEFAULT-ACTION OF brwAgents IN FRAME fMain
DO:
 find current agentbyyear no-error.
 if available agentbyyear then
 publish "SetCurrentValue"("ID",agentbyyear.agentID).
 run wqarbyagent.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgents C-Win
ON ROW-DISPLAY OF brwAgents IN FRAME fMain
DO:
  if current-result-row("brwAgents") modulo 2 = 0 then
  assign
    agentbyyear.stateID     :bgcolor in browse brwAgents = 17
    agentbyyear.agentID     :bgcolor in browse brwAgents = 17
    agentbyyear.stat        :bgcolor in browse brwAgents = 17
    agentbyyear.contractDate:bgcolor in browse brwAgents = 17
    agentbyyear.reviewDate  :bgcolor in browse brwAgents = 17
    agentbyyear.name        :bgcolor in browse brwAgents = 17
    agentbyyear.score       :bgcolor in browse brwAgents = 17
    agentbyyear.grade       :bgcolor in browse brwAgents = 17
    agentbyyear.stateID     :fgcolor in browse brwAgents = 0
    agentbyyear.agentID     :fgcolor in browse brwAgents = 0
    agentbyyear.stat        :fgcolor in browse brwAgents = 0
    agentbyyear.contractDate:fgcolor in browse brwAgents = 0
    agentbyyear.reviewDate  :fgcolor in browse brwAgents = 0
    agentbyyear.name        :fgcolor in browse brwAgents = 0
    agentbyyear.draftReportDate:fgcolor in browse brwAgents = 0
    agentbyyear.draftReportDate:bgcolor in browse brwAgents = 17
    agentbyyear.qarStatus:fgcolor in browse brwAgents = 0
    agentbyyear.qarStatus:bgcolor in browse brwAgents = 17
    agentbyyear.auditFinishDate:fgcolor in browse brwAgents = 0
    agentbyyear.auditFinishDate:bgcolor in browse brwAgents = 17
    agentbyyear.qID:fgcolor in browse brwAgents = 0
    agentbyyear.qID:bgcolor in browse brwAgents = 17
    agentbyyear.audittype:fgcolor in browse brwAgents = 0
    agentbyyear.audittype:bgcolor in browse brwAgents = 17
    agentbyyear.errtype:fgcolor in browse brwAgents = 0
    agentbyyear.errtype:bgcolor in browse brwAgents = 17

  . 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAgents C-Win
ON START-SEARCH OF brwAgents IN FRAME fMain
DO:
 {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbyear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbyear C-Win
ON VALUE-CHANGED OF cbyear IN FRAME fMain /* Year */
DO:
    assign cState     :sensitive = false
           auditStatus:sensitive = false
           agentType  :sensitive = false
           bexport    :sensitive = false.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
/*  open query brwAgents for each agentbyyear where agentbyyear.qarstatus = (if auditStatus:screen-value = "" then agentbyyear.qarstatus else auditStatus:screen-value) and                    */
/*                                                   agentbyyear.whetheraudited =  (if agentType:screen-value = "" then  agentbyyear.whetheraudited  else logical(agentType:screen-value)) and */
/*                                                   agentbyyear.state = (if cState:screen-value = "" then agentbyyear.state else cState:screen-value)                                         */
/*                                                   by agentbyyear.qarstatus desc.                                                                                                            */
/*                                                                                                                                                                                             */
/*  run setCount.                                                                                                                                                                              */
/*                                                                                                                                                                                             */
    apply 'value-changed' to agentType.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_QAR_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_QAR_Summary C-Win
ON CHOOSE OF MENU-ITEM m_View_QAR_Summary /* View QAR Summary */
DO:
   activeAudit = string(agentbyyear.qarID).
   activeRowid = rowid(agentbyyear).

 if not available agentbyyear 
  then return.

 find first agentbyyear where agentbyyear.QarId = integer(activeAudit) no-error.
 if available agentbyyear then
 do:              
   create ttaudit.
   assign
     ttaudit.QarID  =      agentbyyear.QarID  
     ttaudit.agentID =     agentbyyear.agentID  
     ttaudit.stat =        agentbyyear.stat
     ttaudit.name =        agentbyyear.name 
     ttaudit.addr =        agentbyyear.addr1
     ttaudit.city =        agentbyyear.city 
     ttaudit.state =       agentbyyear.state 
     ttaudit.zip =         agentbyyear.zip 
     .
 end.

publish "checkOpenQar" (input agentbyyear.QarId,
                        output openqar,
                        output qarsummaryhandle ).
if openqar
  then
   do:
     run ViewWindow in qarsummaryhandle no-error.
     empty temp-table ttaudit.
     return.
   end.
 else
   do:
     {lib/pbshow.i "''"}
     {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
     {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
     run QARSummary.w persistent ( input table ttaudit ).
     run ViewWindow in qarsummaryhandle no-error.   
     {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
     {lib/pbhide.i}
     empty temp-table ttaudit.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

{lib/win-main.i}

 do std-in = 0 to 9:
   yearpair = yearpair + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in) + ",".
 end.
 yearpair = trim(yearpair, ",").
 cbyear:list-item-pairs = yearpair.
 cbyear:screen-value = string(year(today)).

{lib/get-state-list.i &combo=cState &addAll=true}

run windowResized.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run windowResized in this-procedure.
  
  agenttype:screen-value = "".
  auditStatus:screen-value = "ALL".

  if query brwAgents:num-results = 0
  then
  do: bExport:sensitive = false.
      menu-item m_View_QAR_Summary:sensitive in menu POPUP-MENU-brwAgents = false.
  end.
  status input " " in window {&window-name}.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY auditStatus agentType cbyear cState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwAgents bExport bRefresh auditStatus agentType cbyear cState RECT-37 
         RECT-38 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 if query brwAgents:num-results = 0
  then
  do:
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.
  
  publish "GetReportDir" (output std-ch).
 
  empty temp-table tagentbyyear.
  for each agentbyyear :
   create tagentbyyear.
   buffer-copy agentbyyear to tagentbyyear. 
  
   if agentbyyear.qarstatus = "A" then
      tagentbyyear.qarstatus = "Active".
   else if agentbyyear.qarstatus = "C" then
      tagentbyyear.qarstatus = "Completed".
   else if agentbyyear.qarstatus = "Q" then
      tagentbyyear.qarstatus = "Queued".
   else if agentbyyear.qarstatus = "P" then
      tagentbyyear.qarstatus = "Planned".
   else if agentbyyear.qarstatus = "X" then
      tagentbyyear.qarstatus = "Cancelled".
   else tagentbyyear.qarstatus = "".

   if agentbyyear.stat = "A" then
      tagentbyyear.stat = "Active".
   else if agentbyyear.stat = "C" then
      tagentbyyear.stat = "Closed".
   if agentbyyear.stat = "P" then
      tagentbyyear.stat = "Prospect".
   if agentbyyear.stat = "X" then
      tagentbyyear.stat = "Cancelled".
   if agentbyyear.stat = "W" then
      tagentbyyear.stat = "Withdrawn".

    if agentbyyear.audittype = "Q" then
      tagentbyyear.audittype = "QAR".
   else if agentbyyear.audittype = "E" then
      tagentbyyear.audittype = "ERR".
   else if agentbyyear.audittype = "U" then
      tagentbyyear.audittype = "UnderWriter".
   else if agentbyyear.audittype = "T" then
      tagentbyyear.audittype = "TOR".
   else 
      tagentbyyear.audittype = "".

   if agentbyyear.errtype = "I" then
      tagentbyyear.errtype = "Interval".
   if agentbyyear.errtype = "M" then
      tagentbyyear.errtype = "Monthly".
   if agentbyyear.errtype = "P" then
      tagentbyyear.errtype = "Presign".
   if agentbyyear.errtype = "C" then
      tagentbyyear.errtype = "Corrective".
   if agentbyyear.errtype = "0" then
      tagentbyyear.errtype = " ".
 end.
 
  th = temp-table tagentbyyear:handle.
  run util/exporttable.p (table-handle th,
                         "tagentbyyear",
                         "for each tagentbyyear  group by tagentbyyear.qarstatus desc by tagentbyyear.qid desc ",
                         "qarStatus,qID,audittype,errtype,StateID,score,grade,draftReportDate,auditFinishDate,ContractDate,ReviewDate,Stat,AgentID,Name,Addr1,Addr2,City,Zip,Phone,Fax,Email,Website,SwVendor,SwVersion,StateLicense,StateLicenseEff,StateLicenseExp,ContractId,liabilityLimit,RemitType,RemitValue,RemitAlert,EoRequired,EoCompany,EoPolicy,EoCoverage,EoAggregate,EoStartDate,EoEndDate,ProspectDate,ActiveDate,ClosedDate,CancelDate,mName,mAddr1,mAddr2,mCity,mState,mZip,AltaUID",
                         "Status,QAR ID,Audit Type,ERR Type,State ID,Points,Score,Preliminary Report Date,Audit Finish Date,Contract Date,Review Date,Agent Status,Agent ID,Agent Name,Address1,Address2,City,Zip,Phone,Fax,Email,Website,SwVendor,SwVersion,StateLicense,StateLicenseEff,StateLicenseExp,Contract ID,Max Coverage,Remit Type,Remit Value,Remit Alert,EoRequired,EoCompany,EoPolicy,EoCoverage,EoAggregate,EoStartDate,EoEndDate,ProspectDate,ActiveDate,ClosedDate,CancelDate,mName,mAddr1,mAddr2,mCity,mState,mZip,AltaUID",
                         std-ch,
                         "AgentbyYear- " + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainQar C-Win 
PROCEDURE OpenMaintainQar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAuditId as int.
 def input parameter pWindow as handle.

 find openQars 
   where openQars.QarID = pAuditId no-error.
 if not available openQars 
  then
   do:
     create openQars.
     openQars.QarID = agentbyyear.QarID.
   end.
 openQars.hInstance = pWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  std-in = 0.
  for each agentbyyear  where agentbyyear.qarstatus = (if auditStatus:screen-value in frame fmain = "ALL" then agentbyyear.qarstatus else auditStatus:screen-value in frame fmain) and
                                                  agentbyyear.whetheraudited =  (if agentType:screen-value in frame fmain = "" then  agentbyyear.whetheraudited  else logical(agentType:screen-value in frame fmain)) and 
                                                  agentbyyear.state = (if cState:screen-value in frame fmain = "ALL" then agentbyyear.state else cState:screen-value in frame fmain):
  std-in = std-in + 1.
 end.
 std-ch = string(std-in) + " " + " record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") .
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.
 if brwagents:num-iterations in frame fMain = 0 then
 do:
   std-ch = "".
   status default std-ch in window {&window-name}.
   status input std-ch in window {&window-name}.
   bExport:sensitive in frame fMain = false.
   menu-item m_View_QAR_Summary:sensitive in menu POPUP-MENU-brwAgents = false.
 end.
 else
 do:
   bExport:sensitive in frame fMain = true.
   menu-item m_View_QAR_Summary:sensitive in menu POPUP-MENU-brwAgents = true.
 end.

 if agentType:screen-value in frame fmain = "no" then
   auditStatus:sensitive in frame fMain = false.
 else
   auditStatus:sensitive in frame fMain = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable tWhereClause as char no-undo.
 do with frame {&frame-name}:
   tWhereClause = "where " +
                   (if cState:screen-value <> "ALL" 
                   then "agentbyyear.state = '" + cState:screen-value + "' "
                   else "true ") +
                   "and " + 
                   (if agentType:screen-value <> "" 
                   then "agentbyyear.agentstat = '" + agentType:screen-value  + "' "
                   else "true ") +
                   "and " +
                   (if auditStatus:screen-value <> "ALL"
                   then "agentbyyear.qarStatus =  '" + auditStatus:screen-value + "' "
                   else "true ") 
                    .
                     
    {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 20.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

