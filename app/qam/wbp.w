&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wbp.w
   Window of BestPractice across qar records
   07.4.17 @author AG
   @modification
   Date          Name           Description
   10/14/2020    Anjly Chanana  Modified for TOR audits exports
   10/22/2020    AC             Modified to get the best practices of 
                                selected year audits.
   07/15/2021    SA             Task 83510 modified UI according to 
                                new field grade     
   09/24/2021    SA             Task 86696 Defects raised by david
   */

CREATE WIDGET-POOL.


{tt/auditor.i}
{tt/agent.i}
{tt/bestpracticereport.i &tableAlias ="ttqarbp"}
{tt/state.i}

{lib/std-def.i}
def temp-table qarbp like ttqarbp 
    field cqarId as char
    field taudittype as char
    field terrtype as char.

def var pqarID as int no-undo.
def var pstate as char no-undo.
def var tagent as char no-undo.
def var tauditor as char no-undo.
def var tyear as int no-undo.
def var tName as char no-undo.

def var pAgent as char no-undo.
def var pYear as char no-undo.
def var pAuditor as char no-undo.
def var tempstate as char no-undo.

def var auditorpair as char no-undo.
def var agentpair as char no-undo.
def var yearpair as char no-undo.
def var iBgColor as int no-undo.

def var hasData as log no-undo init no.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES qarbp

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData qarbp.cqarID entry(index("EQUT0", qarbp.audittype),"ERR,QAR,UnderWriter,TOR, ") @ qarbp.audittype entry(index("IPCM0", qarbp.errtype), "Interval,Presign,Corrective,Monthly, ") @ qarbp.errtype replace ( qarbp.state,"?", " ") @ qarbp.state qarbp.score qarbp.grade qarbp.questionID qarbp.auditStartDate replace(qarbp.auditor,"?", " ") @ qarbp.auditor qarbp.agentID qarbp.name   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH qarbp by qarbp.qarID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH qarbp by qarbp.qarID.
&Scoped-define TABLES-IN-QUERY-brwData qarbp
&Scoped-define FIRST-TABLE-IN-QUERY-brwData qarbp


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-45 RECT-46 sYear cState cbAuditor ~
bRefresh bExport tqarID cbAgent brwData tbestpractice 
&Scoped-Define DISPLAYED-OBJECTS sYear cState cbAuditor tqarID cbAgent ~
tbestpractice 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Run Report" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE cbAgent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 84.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 13.2 BY 1 NO-UNDO.

DEFINE VARIABLE tbestpractice AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 184.2 BY 5.05 NO-UNDO.

DEFINE VARIABLE tqarID AS INTEGER FORMAT ">>>>>>>>":U INITIAL 0 
     LABEL "QAR ID" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 140.6 BY 3.1.

DEFINE RECTANGLE RECT-46
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      qarbp SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      qarbp.cqarID                                      label "QAR ID"            format "x(15)"
 entry(index("EQUT0", qarbp.audittype),"ERR,QAR,UnderWriter,TOR, ") @ qarbp.audittype label "Audit Type"      format "x(12)" 
 entry(index("IPCM0", qarbp.errtype), "Interval,Presign,Corrective,Monthly, ") @ qarbp.errtype label "ERR Type"      format "x(12)" 
 replace ( qarbp.state,"?", " ")  @ qarbp.state   label "State"             format "x(2)"     
 qarbp.score label "Points" format "zzz"  width 8
 qarbp.grade label "Score%" format "zzz"  width 9
 qarbp.questionID                                 label "Question"          format "x(12)"  
qarbp.auditStartDate                             label "Audit Start Date"  format "99/99/9999" width 18
replace(qarbp.auditor,"?", " ")  @ qarbp.auditor label "Auditor"           format "x(20)"
qarbp.agentID                                    label "Agent ID"          format "x(12)"
qarbp.name                                       label "Agent Name"        format "x(40)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 184.2 BY 16.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     sYear AT ROW 2.19 COL 9 COLON-ALIGNED WIDGET-ID 22
     cState AT ROW 2.19 COL 32.6 COLON-ALIGNED WIDGET-ID 150
     cbAuditor AT ROW 2.19 COL 68.2 COLON-ALIGNED WIDGET-ID 24
     bRefresh AT ROW 2.33 COL 133.4 WIDGET-ID 162
     bExport AT ROW 2.33 COL 145.4 WIDGET-ID 160
     tqarID AT ROW 2.76 COL 105.6 COLON-ALIGNED WIDGET-ID 152
     cbAgent AT ROW 3.33 COL 4.2 WIDGET-ID 18
     brwData AT ROW 5.05 COL 3 WIDGET-ID 200
     tbestpractice AT ROW 22.38 COL 3 NO-LABEL WIDGET-ID 168
     "Action" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.38 COL 144.4 WIDGET-ID 158
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.38 COL 3.8 WIDGET-ID 156
     RECT-45 AT ROW 1.67 COL 3 WIDGET-ID 8
     RECT-46 AT ROW 1.67 COL 143.2 WIDGET-ID 154
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 187.8 BY 27 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Best Practices"
         HEIGHT             = 27
         WIDTH              = 187.8
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData cbAgent fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       tbestpractice:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH qarbp by qarbp.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Best Practices */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Best Practices */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Best Practices */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Run Report */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
   {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  find current qarbp no-error.
  if available qarbp then
    tbestpractice:screen-value = qarbp.comments.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME fMain /* Agent */
DO:
  close query brwData.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAuditor C-Win
ON VALUE-CHANGED OF cbAuditor IN FRAME fMain /* Auditor */
DO:
  close query brwData.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
  /* get the agents */
  close query brwData.
  run SetCount.
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain /* Year */
DO:
  close query brwData.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tqarID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tqarID C-Win
ON VALUE-CHANGED OF tqarID IN FRAME fMain /* QAR ID */
DO:
  run SetState.
  /* C-Win:title =  substring( C-Win:title, 1, 27).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/brw-main.i}
{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

do std-in = 0 to 9:
  yearpair = yearpair + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in) + "," .
end.

assign
  yearpair              = "ALL,0," + trim(yearpair, ",").
  syear:list-item-pairs = yearpair.
  syear:screen-value    = "0".

publish "GetAuditors"(output table auditor).
cbAuditor:delimiter = "," .
for each auditor no-lock:
  auditorpair  =  auditorpair + "," + string(auditor.name) + "," + string(auditor.UID) .
end.
auditorpair = trim(auditorpair , "|" ).
cbAuditor:list-item-pairs =  "ALL,ALL" + auditorpair.
cbAuditor:screen-value = cbAuditor:entry(1).

{lib/get-state-list.i &combo=cState &addAll=true}
{lib/get-agent-list.i &combo=cbAgent &state=cState &addAll=true}
  
std-ch = "".
status default std-ch in window {&window-name}.
status input std-ch in window {&window-name}.
run setButton.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
  RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

publish "GetCurrentValue" ("QARID", output pqarID).
publish "GetCurrentValue" ("State", output pstate).
publish "GetCurrentValue" ("Auditor", output tauditor).
publish "GetCurrentValue" ("Agent", output tagent).
publish "GetCurrentValue" ("Year", output tyear).
publish "GetCurrentValue" ("AgentName", output tName).

on 'value-changed' of tQarID 
do: 
  close query brwdata.
  C-Win:title = entry(1,C-Win:title,"-").
  run setcount.
  if tQarID:screen-value gt "" then
  assign
   syear:sensitive = false
   cState:sensitive = false
   cbAuditor:sensitive = false
   cbAgent:sensitive = false.
  else 
  assign
   syear:sensitive = true
   cState:sensitive = true
   cbAuditor:sensitive = true
   cbAgent:sensitive = true.
   tbestpractice:screen-value = "".
 end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.                  
  if pqarID > 0 then
  do:
    run setButton.
    tqarID:screen-value = string(pqarID).
    C-Win:title = C-Win:title + " - " + string(pqarID) + " - " + string(tName) + " (" + string(tagent) + ")" .
  end.
  else
  run SetButton.
  if tqarID:screen-value in frame fMain > "" then
  do: 
    run SetState.
    run getdata.
  end.
  else apply "value-changed" to cState.
  run windowResized in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY sYear cState cbAuditor tqarID cbAgent tbestpractice 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-45 RECT-46 sYear cState cbAuditor bRefresh bExport tqarID cbAgent 
         brwData tbestpractice 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.

 if query brwData:num-results = 0
  then
  do:
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
  end.

  for each qarbp:
    if qarbp.tauditType = "E" then
       qarbp.taudittype = "ERR".
    if qarbp.tauditType = "Q" then
       qarbp.taudittype = "QAR".
    if qarbp.tauditType = "U" then
       qarbp.taudittype = "UnderWriter".
    if qarbp.tauditType = "T" then
       qarbp.taudittype = "TOR".
    if qarbp.terrType = "I" then
       qarbp.terrtype = "Interval".
    if qarbp.terrType = "P" then
       qarbp.terrType = "Presign".
    if qarbp.terrType = "C" then
       qarbp.terrType = "Corrective".
    if qarbp.terrType = "0" then
       qarbp.terrType = " ".

 end.
  publish "GetReportDir" (output std-ch).

      
  th = temp-table qarbp:handle.
 run util/exporttable.p (table-handle th,
                         "qarbp",
                         "for each qarbp",
                         "qarID,taudittype,terrtype,state,score,grade,questionID,auditStartDate,auditor,agentID,name",
                         "QAR ID,Audit Type,Type,State,Points,Score,Question,Audit Start Date,Auditor,Agent ID,Agent Name",
                         std-ch,
                         "BestPractices- " + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
empty temp-table qarbp.

empty temp-table ttqarbp.


publish "GetBestPracticeData" (input tqarID:screen-value in frame fMain,
                               input sYear:screen-value in frame fMain ,
                               input cbAgent:screen-value in frame fMain,
                               input cbAuditor:screen-value in frame fmain,
                               input cState:screen-value in frame fMain,
                               output table ttqarbp).
for each ttqarbp:
    hasData = true.
    create qarbp.
    buffer-copy ttqarbp to qarbp.
    assign
        qarbp.cqarID = string (ttqarbp.qarID)
        qarbp.audittype        = if ttqarbp.audittype = ""  or ttqarbp.audittype = "?" then "0" else  ttqarbp.audittype
        qarbp.errtype          = if ttqarbp.errtype = "" or ttqarbp.errtype = "?" then "0" else  ttqarbp.errtype
        qarbp.taudittype       = if ttqarbp.audittype = ""  or ttqarbp.audittype = "?" then "0" else  ttqarbp.audittype
        qarbp.terrtype         = if ttqarbp.errtype = "" or ttqarbp.errtype = "?" then "0" else  ttqarbp.errtype .
        if qarbp.audittype = "Q" or
                   qarbp.audittype = "T" then
            qarbp.errtype = "0".
end.


std-in = 0.
for each qarbp no-lock:
  std-in = std-in + 1.
end.

if hasData then 
do:
   open query brwData for each qarbp by qarbp.qarID.
   std-ch = string(std-in) + " " +  "record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") .
   status default std-ch in window {&window-name}.
   status input std-ch in window {&window-name}.
end.
run SetButton.
assign
  pAuditor  = cbAuditor:screen-value in frame fmain   
  pyear  =sYear:screen-value in frame fMain
  pagent  =cbagent:screen-value in frame fmain
  tempstate =cstate:screen-value in frame fMain .
apply "value-changed" to brwData.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetButton C-Win 
PROCEDURE SetButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwData:num-results = 0
 then
   bExport:sensitive in frame fMain = false.
 else
   bExport:sensitive in frame fMain  = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 std-in = 0.
 for each qarbp no-lock:
   std-in = std-in + 1.
 end.
 std-ch = string(std-in) + " " + "record(s) found as of "  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am").
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.

 if brwData:num-iterations in frame fMain = 0 then
 do:
   std-ch = "" .
   status default std-ch in window {&window-name}.
   status input std-ch in window {&window-name}.
   bExport:sensitive in frame fMain = false.
   tbestpractice:screen-value = "".
 end.
 else
 bExport:sensitive in frame fMain = true.
 apply 'value-changed' to brwData.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetState C-Win 
PROCEDURE SetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if tqarID:screen-value in frame fMain = "" then
  do:
    sYear:sensitive in frame fMain = true.
    cState:sensitive in frame fMain = true.
    cbAuditor:sensitive in frame fMain = true.
    cbAgent:sensitive in frame fMain = true.
    close query brwData.
    run SetCount.
  end.
  else
  do:
    cState:sensitive in frame fMain = false.
    cbAuditor:sensitive in frame fMain = false.
    cbAgent:sensitive in frame fMain = false.
    sYear:sensitive in frame fMain = false.
    tbestpractice:screen-value = "".
    close query brwData.
    run SetCount.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i &post-by-clause=" + ' by qarbp.qarid' "}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = C-Win:width-pixels.
 frame fMain:virtual-width-pixels = C-Win:width-pixels.
 frame fMain:height-pixels = C-Win:height-pixels.
 frame fMain:virtual-height-pixels = C-Win:height-pixels.

 /* fMain components */
 browse brwData:width-pixels = frame fMain:width-pixels - 20.

if {&window-name}:width-pixels > frame fMain:width-pixels 
  then
   do: 
     frame fMain:width-pixels = {&window-name}:width-pixels.
     frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
   end.
else
  do:
    frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
    frame fMain:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
  end.
/*  browse brwData:height-pixels = frame fMain:height-pixels - 120. */
 brwData:width-pixels = frame fmain:width-pixels - 20 no-error.
 brwData:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 126 no-error.
 tbestpractice:x       =   tbestpractice:x +  0.002 * (frame fMain:width-pixels - frame fMain:width-pixels) no-error.
 tbestpractice:y       =   frame fMain:height-pixels - 116 no-error.
 tbestpractice:width-pixel = frame fMain:width-pixels - 22 no-error .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

