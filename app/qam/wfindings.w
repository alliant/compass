&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wfindings.w
    Modification:
    Date         Name           Description
    20/02/2022    SD            Task #91131: Added logic to fetch data according to the parameter 
                                selected from screen wfindingpareto.w.
    29/07/2022    SD            Task#95509: Modified tooltip for comments field to support clob 
                                related changes
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.
    
{lib/std-def.i}
{lib/add-delimiter.i}
{lib/set-button-def.i}
{lib/get-column.i}
{lib/winlaunch.i}

{tt/qarfinding.i &tableAlias="qarfinding"}
{tt/qarfinding.i &tableAlias="ttqarfinding"}

define input parameter ipquestionSeq  as int no-undo.
define input parameter ipcquestion    as char no-undo.
define input parameter ipcyear        as char no-undo.
define input parameter ipcauditor     as char no-undo.
define input parameter ipcauditorname as char no-undo.
define input parameter ipcstate       as char no-undo.
define input parameter ipcstatedesc   as char no-undo.
define input parameter ipcAgentID     as char no-undo.
define input parameter ipcAgentname   as char no-undo.
define input parameter ipcFindingType as char no-undo.

def var hasfinding as logical.
def var iBgcolor as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwFindings

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES qarfinding

/* Definitions for BROWSE brwFindings                                   */
&Scoped-define FIELDS-IN-QUERY-brwFindings qarfinding.qarID qarfinding.year qarfinding.version qarfinding.agentID qarfinding.agentName qarfinding.uid qarfinding.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFindings   
&Scoped-define SELF-NAME brwFindings
&Scoped-define QUERY-STRING-brwFindings preselect EACH qarfinding  by qarfinding.qarID
&Scoped-define OPEN-QUERY-brwFindings OPEN QUERY {&SELF-NAME} preselect EACH qarfinding  by qarfinding.qarID.
&Scoped-define TABLES-IN-QUERY-brwFindings qarfinding
&Scoped-define FIRST-TABLE-IN-QUERY-brwFindings qarfinding


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwFindings}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwFindings bRefresh bExport fillYear ~
fillState fillAuditor fillAgent fillFindingType tYear tAuditId tAuditor ~
tversion title1 RECT-49 RECT-50 RECT-52 
&Scoped-Define DISPLAYED-OBJECTS tYear tAuditId tAuditor tversion 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7 BY 1.62 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7 BY 1.62 TOOLTIP "Refresh".

DEFINE VARIABLE tAuditId AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Audit ID" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE tversion AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Version" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 13.6 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE fillAgent AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE fillAuditor AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Auditor" 
     VIEW-AS FILL-IN 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fillFindingType AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Finding Type" 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 NO-UNDO.

DEFINE VARIABLE fillState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fillYear AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Year" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE title1 AS CHARACTER FORMAT "X(256)":U INITIAL "title1" 
     LABEL "Question" 
     VIEW-AS FILL-IN 
     SIZE 158 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-49
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 153.8 BY 2.29.

DEFINE RECTANGLE RECT-50
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 173 BY 3.43.

DEFINE RECTANGLE RECT-52
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 19.6 BY 2.29.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFindings FOR 
      qarfinding SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFindings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFindings C-Win _FREEFORM
  QUERY brwFindings DISPLAY
      qarfinding.qarID label "Audit ID" format "zzzzzzzz" width 10
 qarfinding.year label "Year" format "zzzz" width 8
 qarfinding.version label "Version" format "x(12)" width 10
 qarfinding.agentID label "Agent ID" format "x(15)" width 10
 qarfinding.agentName label "Agent Name" format "x(45)" width 20
 qarfinding.uid label "Auditor" format "x(30)" width 15
 qarfinding.comments label "Comments" format "x(250)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 172.8 BY 19.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwFindings AT ROW 7.29 COL 3 WIDGET-ID 200
     bRefresh AT ROW 5.1 COL 159.2 WIDGET-ID 184 NO-TAB-STOP 
     bExport AT ROW 5.1 COL 166.4 WIDGET-ID 2 NO-TAB-STOP 
     fillYear AT ROW 3.19 COL 13 COLON-ALIGNED WIDGET-ID 166 NO-TAB-STOP 
     fillState AT ROW 3.19 COL 35.2 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     fillAuditor AT ROW 3.19 COL 62.6 COLON-ALIGNED WIDGET-ID 170 NO-TAB-STOP 
     fillAgent AT ROW 3.19 COL 120 COLON-ALIGNED WIDGET-ID 172 NO-TAB-STOP 
     fillFindingType AT ROW 3.19 COL 97.6 COLON-ALIGNED WIDGET-ID 174 NO-TAB-STOP 
     tYear AT ROW 5.33 COL 13 COLON-ALIGNED WIDGET-ID 10
     tAuditId AT ROW 5.33 COL 35.2 COLON-ALIGNED WIDGET-ID 162
     tAuditor AT ROW 5.33 COL 62.6 COLON-ALIGNED WIDGET-ID 158
     tversion AT ROW 5.33 COL 97.6 COLON-ALIGNED WIDGET-ID 160
     title1 AT ROW 1.95 COL 5.4 WIDGET-ID 28 NO-TAB-STOP 
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 4.38 COL 158.4 WIDGET-ID 32
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 4.38 COL 4.6 WIDGET-ID 154
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.05 COL 4.2 WIDGET-ID 176
     RECT-49 AT ROW 4.71 COL 3 WIDGET-ID 178
     RECT-50 AT ROW 1.38 COL 3 WIDGET-ID 180
     RECT-52 AT ROW 4.71 COL 156.4 WIDGET-ID 186
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 177.6 BY 33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Question's Findings"
         HEIGHT             = 26.67
         WIDTH              = 176.8
         MAX-HEIGHT         = 33
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33
         VIRTUAL-WIDTH      = 273.2
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwFindings 1 fMain */
ASSIGN 
       brwFindings:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwFindings:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR FILL-IN fillAgent IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       fillAgent:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fillAuditor IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       fillAuditor:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fillFindingType IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       fillFindingType:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fillState IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       fillState:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN fillYear IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       fillYear:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN title1 IN FRAME fMain
   NO-DISPLAY ALIGN-L                                                   */
ASSIGN 
       title1:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFindings
/* Query rebuild information for BROWSE brwFindings
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} preselect EACH qarfinding
 by qarfinding.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFindings */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Question's Findings */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Question's Findings */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Question's Findings */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO: 
   run getdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFindings
&Scoped-define SELF-NAME brwFindings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON LEFT-MOUSE-UP OF brwFindings IN FRAME fMain
DO:
   find current qarfinding no-error.
   if available qarfinding 
    then
     brwFindings:TOOLTIP in frame {&frame-name} = substr(qarfinding.comments,1,500).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON ROW-DISPLAY OF brwFindings IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON START-SEARCH OF brwFindings IN FRAME fMain
DO:
  hSortColumn = browse {&browse-name}:current-column.
     
  if hSortColumn:label = "comments"
   then
   return.

  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON VALUE-CHANGED OF brwFindings IN FRAME fMain
DO:
  find current qarfinding no-error.
  if available qarfinding 
   then
    brwFindings:TOOLTIP in frame {&frame-name}  = substr(qarfinding.comments,1,500).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditId C-Win
ON VALUE-CHANGED OF tAuditId IN FRAME fMain /* Audit ID */
DO:
  run filterData in this-procedure .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditor C-Win
ON VALUE-CHANGED OF tAuditor IN FRAME fMain /* Auditor */
DO:
  run filterData in this-procedure .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tversion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tversion C-Win
ON VALUE-CHANGED OF tversion IN FRAME fMain /* Version */
DO:
/*   assign                                   */
/*     std-ch = dataSortBy                    */
/*     dataSortBy = ""                        */
/*     .                                      */
/*   run sortData in this-procedure (std-ch). */
  run filterData in this-procedure .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tYear C-Win
ON VALUE-CHANGED OF tYear IN FRAME fMain /* Year */
DO:
  run filterData in this-procedure .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.
{lib/set-button.i &label="Refresh"     &toggle=false}
{lib/set-button.i &label="Export"}
setButtons().

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

title1:screen-value = ipcquestion .
title1:tooltip = ipcquestion.

fillYear:screen-value = if ipcyear = "" or ipcyear = "0" then "ALL" else ipcyear.
fillState:screen-value = if ipcstatedesc = "" then "ALL" else ipcstatedesc.
fillAuditor:screen-value = if ipcauditorname = "" then "ALL" else ipcauditorname .
fillAgent:screen-value = if ipcAgentname = ? or ipcAgentname = "" then "ALL" else ipcAgentname + " (" + ipcAgentID + ")".

if ipcFindingType = "0" then fillFindingType:screen-value = "ALL".
if ipcFindingType = "1" then fillFindingType:screen-value = "Minor".
if ipcFindingType = "2" then fillFindingType:screen-value = "Intermediate".
if ipcFindingType = "3" then fillFindingType:screen-value = "Major".

run getdata in this-procedure.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
 
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tYear tAuditId tAuditor tversion 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwFindings bRefresh bExport fillYear fillState fillAuditor fillAgent 
         fillFindingType tYear tAuditId tAuditor tversion title1 RECT-49 
         RECT-50 RECT-52 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportdata C-Win 
PROCEDURE exportdata :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 
 if query brwFindings:num-results = 0
 then
 do:
   MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
 end.
 
 publish "GetReportDir" (output std-ch).
 th = temp-table qarfinding:handle.
 
 run util/exporttable.p (table-handle th,
                         "qarfinding",
                         "for each qarfinding ",
                         "qarID,year,version,questionID,files,agentID,agentName,uid,comments",
                         "Audit ID,Year,Version,Question ID,Files,Agent Id,Agent Name,Auditor,Comments",
                         std-ch,
                         "QuestionFindings- "  + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttqarfinding  for ttqarfinding.
  
  close query brwFindings.
  empty temp-table qarfinding.

  do with frame {&frame-name}:
    for each ttqarfinding 
      where ttqarfinding.year    = (if tYear:input-value      = "ALL"  then ttqarfinding.year    else integer(tYear:input-value))
        and ttqarfinding.qarId   = (if tAuditId:input-value   = "ALL"  then ttqarfinding.qarId   else tAuditId:input-value)
        and ttqarfinding.uid     = (if tAuditor:input-value   = "ALL"  then ttqarfinding.uid     else tAuditor:input-value)
        and ttqarfinding.version = (if tversion:input-value   = "ALL"  then ttqarfinding.version else tversion:input-value):
        
      create qarfinding.
      buffer-copy ttqarfinding to qarfinding.
      
    end.
  end.

  open query brwFindings 
    preselect each qarfinding 
    by qarfinding.qarID.
    
       
  /* Makes widget enable-disable based on the data */ 
  if query brwFindings:num-results > 0 
   then
    assign 
        browse brwFindings:sensitive = true
               bExport:sensitive     = true
               .  
  else  
   assign 
       browse brwFindings:sensitive = false
              bExport:sensitive     = false
       .    
       
  /* Display no. of records on status bar */
  setStatusCount(query brwFindings:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getdata C-Win 
PROCEDURE getdata :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table ttqarfinding.
  empty temp-table qarfinding.
   
  do with frame {&frame-name}:
  end.
  
  /* in case of refresh data, to avoid setting the screen value to "ALL"  */
  if tYear:screen-value = ? or tYear:screen-value = "" 
   then 
   assign 
       tYear:list-items   = "ALL" 
       tYear:screen-value = "ALL". 
       
  if tAuditId:screen-value = ? or tAuditId:screen-value = "" 
   then
    assign
        tAuditId:list-items   = "ALL"
        tAuditId:screen-value   = "ALL".
      
  if tAuditor:screen-value = ? or tAuditor:screen-value = "" 
   then 
    assign 
        tAuditor:list-items   = "ALL" 
        tAuditor:screen-value = "ALL".     
    
  if tversion:screen-value = ? or tversion:screen-value = "" 
   then   
    assign
        tversion:list-items    = "ALL" 
        tversion:screen-value   = "ALL".
      
  publish "GetQuestionFindings" (input ipquestionSeq ,
                                 input ipcyear,
                                 input ipcauditor,
                                 input ipcFindingType,
                                 input ipcState,
                                 input ipcAgentID,
                                 output table ttqarfinding).

  for each ttqarfinding:
    publish "GetSysUserName" (ttqarfinding.uid, output std-ch) .
    ttqarfinding.uid = std-ch.
  end.
     
   /* Set the filters based on the data returned, with ALL as the first option */
  for each ttqarfinding fields(year)
    break by ttqarfinding.year:
    if not first-of(ttqarfinding.year) 
     then 
      next.
    /* in case of refresh, to avoid repetition of same values in combo box  */
    if lookup(string(ttqarfinding.year),tYear:list-items) = 0 
    then 
     tYear:add-last(string(ttqarfinding.year)).
  end.
  
   /* Set the filters based on the data returned, with ALL as the first option */
  for each ttqarfinding fields(qarid)
    break by ttqarfinding.qarid:
    if not first-of(ttqarfinding.qarid) 
     then 
      next.
    /* in case of refresh, to avoid repetition of same values in combo box  */
    if lookup(string(ttqarfinding.qarid),tAuditId:list-items) = 0 
     then 
      tAuditId:add-last(string(ttqarfinding.qarid)).
  end.
  
   /* Set the filters based on the data returned, with ALL as the first option */
  for each ttqarfinding fields(uid)
   break by ttqarfinding.uid:
    if not first-of(ttqarfinding.uid) 
     then 
      next.
    /* in case of refresh, to avoid repetition of same values in combo box  */
    if lookup(ttqarfinding.uid,tAuditor:list-items) = 0 
     then 
      tAuditor:add-last(ttqarfinding.uid).
  end.
  
   /* Set the filters based on the data returned, with ALL as the first option */
  for each ttqarfinding fields(version)
    break by ttqarfinding.version:
    if not first-of(ttqarfinding.version) 
     then 
      next.
    /* in case of refresh, to avoid repetition of same values in combo box  */
    if lookup(ttqarfinding.version,tversion:list-items) = 0 
    then 
     tversion:add-last(ttqarfinding.version).
  end.
  
  run filterdata in this-procedure.
        
 std-ch = (" records(s) found as of "  + " " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM")).
 status default string( query brwFindings:num-results) + std-ch in window {&window-name}.
 status input string( query brwFindings:num-results) + std-ch in window {&window-name}. 
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = true.
 {&window-name}:move-to-top().
 publish "WindowOpened" (input "Findings").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Window is shrinking... */
      
  if frame fMain:height-pixels > {&window-name}:height-pixels
   then
    do:
      brwFindings:width-pixels = {&window-name}:width-pixels - 20.
      std-in = round(({&window-name}:height-pixels - 10) / 2, 0).
      
      frame fMain:width-pixels = {&window-name}:width-pixels.
      frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
      frame fMain:height-pixels = {&window-name}:height-pixels.
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
      
      brwFindings:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 10.
    end.
   else
    do: 
      
      frame fMain:width-pixels = {&window-name}:width-pixels.
      frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
      frame fMain:height-pixels = {&window-name}:height-pixels.
      frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
      
      brwFindings:width-pixels = frame fmain:width-pixels - 20.
      brwFindings:height-pixels = frame fMain:height-pixels - {&browse-name}:y - 17.
      std-in = round((frame fMain:height-pixels - 10) / 2, 0).
    end.
    
  title1:screen-value = ipcquestion .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

