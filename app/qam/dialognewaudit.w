&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialognewaudit.w

  Description: Create and start new audit .

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 03-27-2017
  Modified: Date     Name  Description
            11-17-17  SC   Modified to capture reason code for QUR audits.
            05-20-22  SC   Modified Auditor combo-box for showing default type.
            11-20/24  SC  Task #116844 Add last completed audit note while creating/scheduling an audit
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def output parameter pauditId as int.
def output parameter pSuccess as logical.
 
/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{tt/auditor.i}
{tt/agent.i}
{tt/state.i}

def var agentpair as char.
def var auditorpair as char.
def var pAuditor as char.
def var pReviewdate as date.
def var pYear as char.
def var states as char.
def var chreason as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Agents auditType errType fRevierDate ~
cAuditors bCreate BtnCancel tStateID resnCode RECT-41 
&Scoped-Define DISPLAYED-OBJECTS Agents auditType errType fRevierDate ~
cAuditors fYear tStateID resnCode 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCreate AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE Agents AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN AUTO-COMPLETION
     SIZE 98.4 BY 1 NO-UNDO.

DEFINE VARIABLE auditType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Audit Type" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cAuditors AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE errType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE VARIABLE resnCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reason" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 69 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE fRevierDate AS DATE FORMAT "99/99/99":U 
     LABEL "Scheduled  Review Date" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-41
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 127.4 BY 7.71.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     Agents AT ROW 4.1 COL 28 COLON-ALIGNED WIDGET-ID 22
     auditType AT ROW 5.24 COL 28 COLON-ALIGNED WIDGET-ID 26
     errType AT ROW 5.24 COL 57.6 COLON-ALIGNED WIDGET-ID 28
     fRevierDate AT ROW 6.38 COL 28 COLON-ALIGNED WIDGET-ID 4
     cAuditors AT ROW 7.57 COL 28 COLON-ALIGNED WIDGET-ID 14
     bCreate AT ROW 9.57 COL 49 WIDGET-ID 10
     BtnCancel AT ROW 9.57 COL 68 WIDGET-ID 24
     fYear AT ROW 1.71 COL 28 COLON-ALIGNED WIDGET-ID 6
     tStateID AT ROW 2.91 COL 28 COLON-ALIGNED WIDGET-ID 82
     resnCode AT ROW 5.24 COL 57.6 COLON-ALIGNED WIDGET-ID 84
     RECT-41 AT ROW 1.38 COL 2.6 WIDGET-ID 12
     SPACE(0.99) SKIP(2.19)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Audit"
         CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fYear IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fYear:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Audit */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME auditType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL auditType Dialog-Frame
ON VALUE-CHANGED OF auditType IN FRAME Dialog-Frame /* Audit Type */
DO:
  if audittype:screen-value = "E" then
  do:
    errtype:visible = true.
    resnCode:visible = false.
    errtype:screen-value in frame {&frame-name} =  entry(2,errType:list-item-pairs).
  end.
  else if audittype:screen-value = "U" then
  do:
    publish "getreasoncode" (input "QurCause",output chreason).
    resnCode:list-item-pairs in frame Dialog-Frame  = chreason.
    errtype:visible = false.
    resnCode:visible = true.
  end.
  else
   assign errtype:visible = false
          resnCode:visible = false.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCreate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCreate Dialog-Frame
ON CHOOSE OF bCreate IN FRAME Dialog-Frame /* Create */
DO:
 run createaudit .
 IF RETURN-VALUE = "error" THEN
     RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  pSuccess = false.
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRevierDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRevierDate Dialog-Frame
ON LEAVE OF fRevierDate IN FRAME Dialog-Frame /* Scheduled  Review Date */
DO:
  pReviewdate = date(fRevierDate:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID Dialog-Frame
ON VALUE-CHANGED OF tStateID IN FRAME Dialog-Frame /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

 RUN enable_UI.
  {lib/get-sysprop-list.i &combo=cAuditors &appCode="'QAR'" &objAction="'Auditor'"}
  {lib/get-sysprop-list.i &combo=errType &appCode="'QAR'" &objAction="'ERR'" &objProperty="'Type'"}
  {lib/get-sysprop-list.i &combo=audittype &appCode="'QAR'" &objAction="'Audit'" &objProperty="'Type'"}
  {lib/get-state-list.i &combo=tStateID &addAll=true}
  {lib/get-agent-list.i &combo=Agents &state=tStateID}
  {lib/set-current-value.i &state=tStateID}
 apply "VALUE-CHANGED" to tStateID.
 publish "SearchYear" (output pYear).
 cAuditors:list-item-pairs in frame {&frame-name} = "--Select Auditor--,ALL," + cAuditors:list-item-pairs.
 cAuditors:screen-value = "ALL".
 audittype:screen-value = "Q".
 fYear:screen-value = entry(2,pYear,":").
 errtype:visible = false.
 resnCode:visible = false.
 WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CreateAudit Dialog-Frame 
PROCEDURE CreateAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pAiditID as char.
def var pLastAuditNote as char.
def var pMsg as char.
def var fDate as date.
assign 
  fdate = date(fRevierDate:screen-value in frame dialog-frame).

    if Agents:screen-value in frame dialog-frame = "" or Agents:screen-value in frame dialog-frame = ?  then
    do:
      MESSAGE "Agent can't be blank."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
        apply "entry" to agents in frame dialog-frame.
        return "error".
    end.

    if auditType:screen-value in frame dialog-frame = "" or auditType:screen-value in frame dialog-frame = ? then
    do:
      MESSAGE "Audit Type can't be blank."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
      apply "entry" to auditType in frame dialog-frame.
      return "error".
    end. 

    /*if fdate = ?  then
    do:
      MESSAGE "Scheduled Target Date can't be blank."
         VIEW-AS ALERT-BOX INFO BUTTONS OK.
      apply "entry" to fRevierDate in frame dialog-frame.
      return "Error".
    end.*/

    if cAuditors:screen-value in frame dialog-frame = "" or cAuditors:screen-value in frame dialog-frame = ? or cAuditors:screen-value in frame dialog-frame = "ALL"
     then
      do:
        MESSAGE "Auditor can't be blank."
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
        apply "entry" to cAuditors in frame dialog-frame.
        return "ERROR".
      end.


find agent where agent.agentID = agents:screen-value in frame dialog-frame no-error.
if avail agent then
do:
    if agent.stat <> "P" then
    do:
      if auditType:screen-value in frame dialog-frame = "E" and errtype:screen-value in frame dialog-frame = "P" then
      do: 
        MESSAGE "ERR type can only be presign for a prospective agent."
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
        apply "entry" to errtype in frame dialog-frame.
        return "error".
      end.
      
      publish "NewAudit"( input fYear:screen-value in frame dialog-frame,
                        input Agents:screen-value in frame dialog-frame,
                        input fRevierDate:screen-value in frame dialog-frame,
                        input cAuditors:screen-value in frame dialog-frame,
                        input audittype:screen-value in frame dialog-frame,
                        input if errtype:hidden in frame dialog-frame = true then "" else errtype:screen-value in frame dialog-frame,
                        input resnCode:screen-value in frame dialog-frame,
                        output pAuditID,
                        output pLastAuditNote,
                        output pSuccess,
                        output pMsg).

    end.
    else
    do:
      if errtype:screen-value in frame dialog-frame <> "P" then
      do: 
        MESSAGE "ERR type can only be presign for a prospective agent."
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
        apply "entry" to errtype in frame dialog-frame.
       return "error".
      end.
      else
      publish "NewAudit"( input fYear:screen-value in frame dialog-frame,
                            input Agents:screen-value in frame dialog-frame,
                            input fRevierDate:screen-value in frame dialog-frame,
                            input cAuditors:screen-value in frame dialog-frame,
                            input audittype:screen-value in frame dialog-frame,
                            input errtype:screen-value in frame dialog-frame,
                            input "",
                            output pAuditID,
                            output pLastAuditNote,
                            output pSuccess,
                            output pMsg).
    end.

end.

 if pSuccess then
  MESSAGE "Audit created with ID: " + string(pAuditID)
    VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY Agents auditType errType fRevierDate cAuditors fYear tStateID resnCode 
      WITH FRAME Dialog-Frame.
  ENABLE Agents auditType errType fRevierDate cAuditors bCreate BtnCancel 
         tStateID resnCode RECT-41 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

