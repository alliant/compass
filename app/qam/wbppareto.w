&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wbppareto.w
   Window of bestpracticeSpareto across qar records
   6.26.2017
   @Archana Gupta
   @modified : 09/24/21  SA  Task 86696 Defects raised by david*/

CREATE WIDGET-POOL.

{tt/qarbestpractice.i}
{tt/auditor.i}
{tt/agent.i}
{tt/qar.i}
{tt/bppareto.i}
{tt/state.i}
{lib/std-def.i}

def var auditorpair as char no-undo.
def var agentpair as char no-undo.

def var pAgent as char no-undo.
def var pYear as char no-undo.
def var pAuditor as char no-undo.
def var pState as char no-undo.
def var iBgColor as int no-undo.
def var yearpair as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES bppareto

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData bppareto.cnt bppareto.pct bppareto.questionID bppareto.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH bppareto by bppareto.cnt descending
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH bppareto by bppareto.cnt descending.
&Scoped-define TABLES-IN-QUERY-brwData bppareto
&Scoped-define FIRST-TABLE-IN-QUERY-brwData bppareto


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-47 RECT-48 sYear cState cbAuditor ~
bRefresh bExport cbAgent brwData 
&Scoped-Define DISPLAYED-OBJECTS sYear cState cbAuditor cbAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE cbAgent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 84.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 13.8 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-47
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 109.2 BY 3.1.

DEFINE RECTANGLE RECT-48
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.4 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      bppareto SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      bppareto.cnt label "Count" format ">>>,>>9"
 bppareto.pct label "Percent" format "  zz9.9%  "
 bppareto.questionID label "Question" format "x(20)"
 bppareto.description label "Description" format "x(200)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 185.2 BY 21.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     sYear AT ROW 2 COL 9.8 COLON-ALIGNED WIDGET-ID 22
     cState AT ROW 2 COL 33.4 COLON-ALIGNED WIDGET-ID 150
     cbAuditor AT ROW 2 COL 69.8 COLON-ALIGNED WIDGET-ID 24
     bRefresh AT ROW 2.14 COL 101.4 WIDGET-ID 4
     bExport AT ROW 2.14 COL 114.2 WIDGET-ID 2
     cbAgent AT ROW 3.14 COL 5 WIDGET-ID 18
     brwData AT ROW 4.81 COL 2.8 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.14 COL 3.8 WIDGET-ID 156
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.19 COL 113 WIDGET-ID 158
     RECT-47 AT ROW 1.43 COL 2.8 WIDGET-ID 152
     RECT-48 AT ROW 1.43 COL 111.6 WIDGET-ID 154
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 188 BY 26.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Best Practices Pareto"
         HEIGHT             = 26.24
         WIDTH              = 188
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData cbAgent fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME fMain
   ALIGN-L                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH bppareto by bppareto.cnt descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Best Practices Pareto */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Best Practices Pareto */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Best Practices Pareto */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
 if current-result-row("brwData") modulo 2 = 0 then
   iBgColor = 17.
 else
   iBgColor = 15.

 bppareto.cnt:bgcolor in browse brwData = iBgColor.
 bppareto.pct:bgcolor in browse brwData = iBgColor. 
 bppareto.questionID:bgcolor in browse brwData = iBgColor. 
 bppareto.description:bgcolor in browse brwData = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME fMain /* Agent */
DO:
  close query brwData.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAuditor C-Win
ON VALUE-CHANGED OF cbAuditor IN FRAME fMain /* Auditor */
DO:
  close query brwData.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
  close query brwData.
  run AgentComboState in this-procedure.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain /* Year */
DO:
  close query brwData.
  run SetCount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

  bExport:load-image("images/excel.bmp").
  bExport:load-image-insensitive("images/excel-i.bmp").
  bRefresh:load-image("images/completed.bmp").
  bRefresh:load-image-insensitive("images/completed-i.bmp").


  std-ch = "".
  status default std-ch in window {&window-name}.
  status input std-ch in window {&window-name}.
  do std-in = 0 to 9:
    yearpair = yearpair + string(year(today) + 1 - std-in) + "," + string(year(today) + 1 - std-in) + "," .
  end.
  assign
    yearpair               = "ALL,0," + trim(yearpair, ",").
    syear:list-item-pairs = yearpair.
    syear:screen-value    = "0".

  publish "GetAuditors"(output table auditor).
  cbAuditor:delimiter = "," .
  for each auditor no-lock:
    auditorpair  =  auditorpair + "," + string(auditor.name) + "," + string(auditor.UID) .
  end.
  auditorpair = trim(auditorpair , "|" ).
  cbAuditor:list-item-pairs =  "ALL, " + auditorpair.
  cbAuditor:screen-value = cbAuditor:entry(1).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-state-list.i &combo=cState &addAll=true}
  {lib/get-agent-list.i &combo=cbAgent &state=cState &addAll=true}
  
  run setButton.
  apply "value-changed" to cState.
  run windowResized in this-procedure.
IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY sYear cState cbAuditor cbAgent 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-47 RECT-48 sYear cState cbAuditor bRefresh bExport cbAgent 
         brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var th as handle no-undo.

if query brwData:num-results = 0
 then
 do:
   MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
 end.
publish "GetReportDir" (output std-ch).

th = temp-table bppareto:handle.

run util/exporttable.p (table-handle th,
                        "bppareto",
                        "for each bppareto",
                        "cnt,pct,questionID,description",
                        "Count,Percent,Question,Description",
                        std-ch,
                        "BestPracticesPareto- "  + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                        true,
                        output std-ch,
                        output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

publish "GetBPParetoData"  (input sYear:screen-value in frame fMain ,
                            input cbAgent:screen-value in frame fMain,
                            input cbAuditor:screen-value in frame fmain,
                            input cState:screen-value in frame fMain,
                            output table bppareto).

open query brwData for each bppareto by bppareto.cnt descending.
std-in = 0.
for each bppareto no-lock:
  std-in = std-in + 1.
end.
std-ch = string(std-in) + " record(s) found as of "  + " " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") .
status default std-ch in window {&window-name}.
status input std-ch in window {&window-name}.
run SetButton.
  pAgent = cbAgent:screen-value in frame fMain.
  pYear = sYear:screen-value in frame fMain.
  pAuditor = cbAuditor:screen-value in frame fmain.
  pState = cState:screen-value in frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetButton C-Win 
PROCEDURE SetButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwData:num-results = 0
 then
   bExport:sensitive in frame fMain = false.
 else
   bExport:sensitive in frame fMain  = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
std-in = 0.
for each bppareto no-lock:
  std-in = std-in + 1.
end.
std-ch = string(std-in) + " " + "record(s) found as of "  + " " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am")  .
status default std-ch in window {&window-name}.
status input std-ch in window {&window-name}.

if brwData:num-iterations in frame fMain = 0 then
do:
  std-ch = "".
  status default std-ch in window {&window-name}.
  status input std-ch in window {&window-name}.
  bExport:sensitive in frame fMain = false.
end.
else
bExport:sensitive in frame fMain = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i &post-by-clause=" + ' by bppareto.cnt' "}                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
frame fMain:width-pixels = {&window-name}:width-pixels.
frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
frame fMain:height-pixels = {&window-name}:height-pixels.
frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

/* fSearch components */
browse brwData:width-pixels = frame fMain:width-pixels - 16.

if {&window-name}:width-pixels > frame fMain:width-pixels 
 then
   do: 
     frame fMain:width-pixels = {&window-name}:width-pixels.
     frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
        
   end.
else
  do:
    frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
    frame fMain:width-pixels = {&window-name}:width-pixels.
        /* das: For some reason, shrinking a window size MAY cause the horizontal
           scroll bar.  The above sequence of widget setting should resolve it,
           but it doesn't every time.  So... */
   end.
browse brwData:height-pixels = frame fMain:height-pixels - browse {&browse-name}:y - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

