&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* dialogconfig.w
------------------------------------------------------------------------*/
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-45 tStates tOtherStates tOpenType ~
cStatus tReportDir tReportsSearch Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tStates tOtherStates tOpenType cStatus ~
tReportDir 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddState 
     LABEL "<--" 
     SIZE 8 BY .95 TOOLTIP "Add a State to the Search options".

DEFINE BUTTON bDeleteState 
     LABEL "-->" 
     SIZE 8 BY .95 TOOLTIP "Remove a State from the Search options".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON tReportsSearch 
     LABEL "..." 
     SIZE 4 BY .95.

DEFINE VARIABLE cStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Planned","P",
                     "Queued","Q",
                     "Active","A",
                     "Completed","C",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tReportDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Directory" 
     VIEW-AS FILL-IN 
     SIZE 56.6 BY 1 TOOLTIP "Enter the default directory to save report PDF documents and CSV export files" NO-UNDO.

DEFINE VARIABLE tOpenType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Open In Current Year", "C",
"Open In Last Year Viewed", "X"
     SIZE 29 BY 1.67 NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 6.91.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 1.91.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 2.62.

DEFINE VARIABLE tOtherStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY" 
     SIZE 24 BY 5.95 NO-UNDO.

DEFINE VARIABLE tStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Colorado","CO" 
     SIZE 25 BY 5.95 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tStates AT ROW 1.95 COL 14.2 NO-LABEL WIDGET-ID 74
     tOtherStates AT ROW 1.95 COL 49.2 NO-LABEL WIDGET-ID 82
     bAddState AT ROW 3.86 COL 40.2 WIDGET-ID 78
     bDeleteState AT ROW 4.81 COL 40.2 WIDGET-ID 80
     tOpenType AT ROW 9.48 COL 7.8 NO-LABEL WIDGET-ID 152
     cStatus AT ROW 9.81 COL 50.2 COLON-ALIGNED WIDGET-ID 148
     tReportDir AT ROW 12.71 COL 15.6 COLON-ALIGNED WIDGET-ID 2
     tReportsSearch AT ROW 12.71 COL 74.4 WIDGET-ID 30
     Btn_OK AT ROW 14.52 COL 28
     Btn_Cancel AT ROW 14.52 COL 46
     "Reports/Exports" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 11.91 COL 4 WIDGET-ID 46
     "Default Filters" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 8.62 COL 4 WIDGET-ID 150
     "States" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.14 COL 4 WIDGET-ID 70
     RECT-38 AT ROW 12.24 COL 2 WIDGET-ID 42
     RECT-31 AT ROW 1.48 COL 2 WIDGET-ID 68
     RECT-45 AT ROW 8.95 COL 2 WIDGET-ID 84
     SPACE(0.79) SKIP(4.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-38 IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Configuration */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddState fMain
ON CHOOSE OF bAddState IN FRAME fMain /* <-- */
DO:
  if tOtherStates:input-value = ?
   then return.
  tStates:add-last(entry(lookup(tOtherStates:input-value, tOtherStates:list-item-pairs) - 1, tOtherStates:list-item-pairs), 
                   tOtherStates:input-value).
  tOtherStates:delete(tOtherStates:input-value).
  bAddState:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteState fMain
ON CHOOSE OF bDeleteState IN FRAME fMain /* --> */
DO:
 if tStates:input-value = ?
  then return.
 tOtherStates:add-last(entry(lookup(tStates:input-value, tStates:list-item-pairs) - 1, tStates:list-item-pairs), 
                       tStates:input-value).
 tStates:delete(tStates:input-value).
 bDeleteState:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fMain
ON CHOOSE OF Btn_Cancel IN FRAME fMain /* Cancel */
DO:
  std-lo = no. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fMain
ON CHOOSE OF Btn_OK IN FRAME fMain /* Save */
DO:
  std-lo = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOtherStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fMain
ON DEFAULT-ACTION OF tOtherStates IN FRAME fMain
DO:
  apply "CHOOSE" to bAddState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fMain
ON VALUE-CHANGED OF tOtherStates IN FRAME fMain
DO:
  bAddState:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportsSearch fMain
ON CHOOSE OF tReportsSearch IN FRAME fMain /* ... */
DO:
  std-ch = tReportDir:screen-value.
  if tReportDir:screen-value > ""
   then system-dialog get-dir std-ch
          initial-dir std-ch.
   else system-dialog get-dir std-ch.
  if std-ch > "" and std-ch <> ?
   then tReportDir:screen-value = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fMain
ON DEFAULT-ACTION OF tStates IN FRAME fMain
DO:
 apply "CHOOSE" to bDeleteState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fMain
ON VALUE-CHANGED OF tStates IN FRAME fMain
DO:
  bDeleteState:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

 publish "GetSearchStates" (output std-ch).
 if lookup("CO", tOtherStates:list-item-pairs) = 0 
   and lookup("CO", std-ch) = 0
 then tOtherStates:add-last("Colorado", "CO").
 do std-in = 2 to num-entries(std-ch) by 2:
   tOtherStates:delete(entry(std-in, std-ch)).
 end.
 tStates:list-item-pairs = std-ch.

 publish "GetReportDir" (output tReportDir).
 publish "GetStatus" (output std-ch).
 cStatus:screen-value = std-ch.
 cStatus:inner-lines = 10.
 publish "GetYearCondition" (output tOpenType).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if tReportDir:screen-value > ""
   then
    do:
      publish "SetReportDir" (tReportDir:screen-value).
      if error-status:error 
       then
       do: 
         MESSAGE "Invalid Directory"
            VIEW-AS ALERT-BOX error BUTTONS OK.
         undo MAIN-BLOCK, retry MAIN-BLOCK.
       end.
    end.
   else publish "SetReportDir" ("").

  publish "SetSearchStates" (tStates:list-item-pairs).
  publish "SetStatus" (cStatus:screen-value).
  publish "SetYearCondition"(tOpenType:screen-value).
  publish "SetReportDir" (tReportDir:screen-value).
END.
               
RUN disable_UI.

return string(std-lo).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStates tOtherStates tOpenType cStatus tReportDir 
      WITH FRAME fMain.
  ENABLE RECT-45 tStates tOtherStates tOpenType cStatus tReportDir 
         tReportsSearch Btn_OK Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

