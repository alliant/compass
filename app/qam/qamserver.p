&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : qamserver.p
    Purpose     : Functionality to interact with the AES server
    Description :
    Author(s)   : D.Sinclair
    Created     : 1.24.2012
    Notes       :
    
    
    @modified
    Date           Name      Description
    02/02/2022     SD        Task #91131: Added new parameters in "GetfQuestionFindings" procedure
                             to fetch data according to them.
    11/20/24       SC        Task #116844 Add last completed audit note while creating/scheduling an audit
  ----------------------------------------------------------------------*/

{lib/std-def.i}
{tt/qaraudit.i &tableAlias="ttaudit"}

{tt/qaraudit.i &tableAlias="allaudit"}
{tt/qaraudit.i &tableAlias="audit"}
{tt/qaraudit.i &tableAlias="qaragent"}
{tt/qaraudit.i &tableAlias="qarauditgot" } 

{tt/qarfinding.i}
{tt/qarfinding.i &tableAlias="questionfindings"}
{tt/qarauditfinding.i &tableAlias="findinggot"} 

{tt/findingpareto.i}
{tt/findingreport.i &tableAlias="ttFindingReport"}
{tt/severitychanges.i}

{tt/qarbestpractice.i}
{tt/bestpracticereport.i &tableAlias ="qarbp"}
{tt/bppareto.i}
{tt/qarauditbp.i &tableAlias="bestpracticegot"} 

{tt/qarbkgquestion.i}
{tt/qarbkgquestion.i &tableAlias="bkgQuestiongot"}
{tt/scoreanalysis.i} 
{tt/scheduleQARdata.i} 

{tt/auditaction.i &tableAlias="qaraction"}
{tt/qaractionnote.i &tableAlias="qarnote"}
{tt/qarnote.i &tableAlias="qarsnote"}
{tt/qaractionstat.i &tableAlias="qaractionstat"}
{tt/qarauditaction.i &tableAlias="qaractiongot" } 

{tt/qarauditfile.i &tableAlias="agentFilegot"}  
{tt/qaraccount.i  &tableAlias="escrowaccounts"}

{tt/qarauditsection.i &tableAlias="sectiongot"}   
{tt/qarquestiond.i &tableAlias="questiongot"} 
{tt/qarauditanswer.i &tableAlias="answergot"} 

{tt/qardbstate.i}
{tt/qardbauditor.i}
{tt/agentbyyear.i &tableAlias="Agents"}

{tt/qarnote.i &tableAlias="qarnotes"}
{tt/sysuser.i}
{tt/syscode.i}
    
def temp-table agent like Agents.

def var tTraceFile as char no-undo.
def var tResponseFile as char no-undo.
def var activeUID as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15.52
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-EditAuditTypeService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EditAuditTypeService Procedure 
PROCEDURE EditAuditTypeService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pAuditType as char no-undo.
def input parameter pErrType as char no-undo.
def input parameter pReason as char no-undo.
def input parameter pAppType as char no-undo.
define output parameter pSuccess as logical .
define output parameter tmsg as char no-undo.

run server/editaudittype.p (input pAuditID,
                            input pAuditType,
                            input pErrType,
                            input pReason,
                            input pAppType,
                            output pSuccess,
                            output tMsg).

if not pSuccess
 then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetAgentByYearService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentByYearService Procedure 
PROCEDURE GetAgentByYearService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def output parameter table for agent.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/getagentsbyyear.p (input integer(piYear),
                              output table agent,
                              output pSuccess,
                              output tMsg ).

if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetAuditDetailReportService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAuditDetailReportService Procedure 
PROCEDURE GetAuditDetailReportService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter pFile as char no-undo.
def output parameter pSuccess as logical no-undo.
define output parameter tmsg as character   no-undo.

run server/getauditreport.p (input pAuditID,
                             output pfile,
                             output pSuccess,
                             output tMsg).
                             
if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBestPracticeService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBestPracticeService Procedure 
PROCEDURE GetBestPracticeService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as char no-undo.
def input parameter pyear as char.
def input parameter pAgentID as char.
def input parameter pAuditor as char.
def input parameter pState as char no-undo.
def output parameter table for qarbp.
def output parameter  pSuccess as logical.
def output parameter  pMsg as char.

run server/getbestpractices.p(input integer(pAuditID),
                              input integer(pYear),
                              input pAgentID,
                              input pAuditor,
                              input pState,
                              output table qarbp,
                              output pSuccess,
                              output pMsg).

if not pSuccess
then
do:
  MESSAGE pMsg VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  return error.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBPParetoService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBPParetoService Procedure 
PROCEDURE GetBPParetoService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pyear as char no-undo.
def input parameter pAgentID as char no-undo.
def input parameter pAuditor as char no-undo.
def input parameter pState as char no-undo.
def output parameter table for bppareto.
def output parameter pSuccess as logical no-undo.
def output parameter pMsg as char.

run server/getbestpracticepareto.p (input pyear,
                                    input pAgentID,
                                    input pAuditor,
                                    input pState,
                                    output table bppareto,
                                    output pSuccess,
                                    output pMsg).

if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetfFindingParetoService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetfFindingParetoService Procedure 
PROCEDURE GetfFindingParetoService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear        as char no-undo.
def input parameter piFindingType as int no-undo.
def input parameter pcState       as char no-undo.
def input parameter pcAgentID     as char no-undo.
def input parameter pcUid         as char no-undo.
def output parameter table for findingpareto.
define output parameter pSuccess as logical .
define output parameter tmsg as character no-undo.


run server/getfindingspareto.p (input piYear,
                                input piFindingType,
                                input pcState,
                                input pcAgentID,
                                input pcUid,
                                output table findingpareto,
                                output pSuccess,
                                output tMsg ).
if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetFindingService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetFindingService Procedure 
PROCEDURE GetFindingService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def input parameter piAuditID as char no-undo.
def input parameter pcState as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcUid as char no-undo.
def output parameter table for ttFindingReport.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.


 run server/getfindings.p (input integer(piYear),
                           input integer(piAuditID),
                           input pcState,
                           input pcAgentID,
                           input pcUid,
                           output table ttFindingReport,
                           output pSuccess,
                           output tMsg ).

 if not pSuccess
 then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetfQuestionFindings) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetfQuestionFindings Procedure 
PROCEDURE GetfQuestionFindings :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter ipiquestionseq as int no-undo.
def input parameter ipcyear as char no-undo.
def input parameter ipcauditor as char NO-UNDO.
def input parameter ipiFindingType as int  no-undo.
def input parameter ipcState       as char no-undo.
def input parameter ipcAgentID     as char no-undo.
def output parameter table for questionfindings.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/getquestionfindings.p (input ipiquestionseq,
                                  input ipcyear,
                                  input ipcauditor,
                                  input ipiFindingType,
                                  input ipcState,
                                  input ipcAgentID,
                                  output table questionfindings,
                                  output pSuccess,
                                  output tMsg ).
if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetInternalReviewReportService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetInternalReviewReportService Procedure 
PROCEDURE GetInternalReviewReportService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter pFile as char no-undo.
def output parameter pSuccess as logical no-undo.
define output parameter tmsg as character   no-undo.

run server/getinternalreport.p (input pAuditID,
                                output pfile,
                                output pSuccess,
                                output tMsg).
                             
if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarActionsService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarActionsService Procedure 
PROCEDURE GetQarActionsService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pQarID as char.
def output parameter table for  qarauditgot.     
def output parameter table for sectiongot.     
def output parameter table for questiongot.     
def output parameter table for agentFilegot.    
def output parameter table for findinggot.      
def output parameter table for qaractiongot.    
def output parameter table for bestPracticegot. 
def output parameter table for bkgQuestiongot.  
def output parameter table for escrowaccounts.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

run server/getaudit.p (input integer(pQarID), 
                       output table qarauditgot,       
                       output table sectiongot,     
                       output table questiongot,    
                       output table agentFilegot,   
                       output table findinggot,   
                       output table qaractiongot,     
                       output table bestPracticegot,
                       output table bkgQuestiongot,
                       output table escrowaccounts,
                       output pSuccess,
                       output pMsg ).

if not pSuccess
 then
  do:
    message pmsg view-as alert-box error buttons ok.
    return error .
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarByAgentService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarByAgentService Procedure 
PROCEDURE GetQarByAgentService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter syear as integer no-undo.
define input parameter pAgentID as char no-undo.
def input parameter pAuditor as char no-undo.
def output parameter table for qaragent.
def output parameter pSuccess as logical init false.
def var pMsg as char.

run server/getallaudits.p ( input syear ,     /* year */
                            input "",  /* state */
                            input pAgentID,  /* agent */
                            input pAuditor,      /* auditor */
                            input "",   /* status */
                            output table qaragent,
                            output pSuccess,
                            output pMsg).


if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarCancel) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarCancel Procedure 
PROCEDURE GetQarCancel :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/cancelAudit.p (input pAuditID,
                          input pReason,
                          output pSuccess,
                          output tMsg ).

if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarDequeued) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarDequeued Procedure 
PROCEDURE GetQarDequeued :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/dequeueAudit.p (input pAuditID,
                          input pReason,
                          output pSuccess,
                          output tMsg ).

if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarInspectService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarInspectService Procedure 
PROCEDURE GetQarInspectService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pQarID as char.
def output parameter table for  qarauditgot.     
def output parameter table for sectiongot.     
def output parameter table for questiongot.     
def output parameter table for answergot.
def output parameter table for agentFilegot.    
def output parameter table for findinggot.      
def output parameter table for qaractiongot.    
def output parameter table for bestPracticegot. 
def output parameter table for escrowaccounts.
def output parameter table for qarnotes. 
def output parameter table for sysuser.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

run server/inspectaudit.p (input integer(pQarID), 
                           output table qarauditgot,       
                           output table sectiongot,     
                           output table questiongot,  
                           output table answergot, 
                           output table agentFilegot,   
                           output table findinggot,   
                           output table qaractiongot,     
                           output table bestPracticegot,
                           output table escrowaccounts,
                           output table qarnotes,
                           output table sysuser,
                           output pSuccess,
                           output pMsg ).

if not pSuccess
 then
  do:
    message pmsg view-as alert-box error buttons ok.
    return error .
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarNotesService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarNotesService Procedure 
PROCEDURE GetQarNotesService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditId as int no-undo.
def output parameter table for qarsnote.

def var pSuccess as logical no-undo.
def var pMsg as character no-undo.

run server/getauditnotes.p (input pAuditId,
                            output table qarsnote,
                            output pSuccess,
                            output pMsg).

if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarReassigned) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarReassigned Procedure 
PROCEDURE GetQarReassigned :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pNewAuditor as char.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/reassignAudit.p (input pAuditID,
                           input pNewAuditor,
                          input pReason,
                          output pSuccess,
                          output tMsg ).

if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarRestoreAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarRestoreAudit Procedure 
PROCEDURE GetQarRestoreAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/restoreAudit.p (input pAuditID,
                          input pReason,
                          output pSuccess,
                          output tMsg ).

if not pSuccess
then
 do: 
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarsService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarsService Procedure 
PROCEDURE GetQarsService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input  parameter syear as integer   no-undo.
define output parameter table for allaudit.

def var pSuccess as logical init false no-undo.
def var pMsg     as char.
def var tqarID   as char.

Publish "GetCurrentValue" ("QARIDUpdated", output tqarID).

run server/getaudits.p (input syear ,     /* year */
                        input tqarID,
                        input "",         /* state */
                        input "",         /* agent */
                        input activeUID,  /* user */
                        input "",         /* status */
                        input "",         /* audit types */
                        output table allaudit,
                        output pSuccess,
                        output pMsg
                        ).
                         
if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQuestionnaireReportService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQuestionnaireReportService Procedure 
PROCEDURE GetQuestionnaireReportService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def output parameter pFile as char no-undo.
def output parameter pSuccess as logical no-undo.
define output parameter tmsg as character   no-undo.

run server/getquestionnaire.p (input pAuditID,
                               output pfile,
                               output pSuccess,
                               output tMsg).
                             
if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getReasonCodeService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getReasonCodeService Procedure 
PROCEDURE getReasonCodeService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter ptype as char .
 def output parameter table for syscode.

 def var pSuccess as log.
 def var tMsg as char.
 run server/getsyscodes.p (input ptype,
                           output table syscode,
                           output pSuccess,
                           output tMsg ).

 if not pSuccess
  then
  do:
    message tmsg view-as alert-box error buttons ok.
    return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetScheduleagentsService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetScheduleagentsService Procedure 
PROCEDURE GetScheduleagentsService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter pyear as integer   no-undo.
 define input  parameter paudittype as char   no-undo.
 define output parameter table for scheduleQARdata.
 
 def var pSuccess as logical init false no-undo.
 def var pMsg     as char.

run server/getscheduleagents.p (input pyear,     /* year */
                                input paudittype,
                                output table scheduleQARdata,
                                output pSuccess,
                                output pMsg
                                ).
if not pSuccess
 then message pmsg view-as alert-box error buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetScoreanalysisService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetScoreanalysisService Procedure 
PROCEDURE GetScoreanalysisService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter pyear as integer   no-undo.
 define output parameter table for scoreanalysis.
 
 def var pSuccess as logical init false no-undo.
 def var pMsg     as char.

run server/getscoreanalysis.p (input pyear ,     /* year */
                               output table scoreanalysis,
                               output pSuccess,
                               output pMsg
                               ).
if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetSeverityChangesService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSeverityChangesService Procedure 
PROCEDURE GetSeverityChangesService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter piYear as char no-undo.
def input parameter piAuditID as char no-undo.
def input parameter pcState as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcUid as char no-undo.
def output parameter table for severitychanges.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

 run server/getseveritychanges.p (input integer(piYear),
                                  input integer(piAuditID),
                                  input pcState,
                                  input pcAgentID,
                                  input pcUid,
                                  output table severitychanges,
                                  output pSuccess,
                                  output tMsg ).

 if not pSuccess
 then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStateDashboardService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateDashboardService Procedure 
PROCEDURE GetStateDashboardService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pcYear as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcAuditor as CHAR no-undo.
def input parameter pcState as char no-undo.
def output parameter table for dbstate .
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/statedashboard.p (input pcYear,
                             input pcAgentID,
                             input pcAuditor,
                             input pcState,
                             output table dbstate,
                             output pSuccess,
                             output tMsg ).

if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewAuditService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewAuditService Procedure 
PROCEDURE NewAuditService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pYear as char.
def input parameter pAgent as char.
def input parameter pReviewDate as char.
def input parameter pAuditor as char.
def input parameter pAuditType as char.
def input parameter pErrType as char.
def input parameter pResncode as char.
def output parameter pAuditID as char.
def output parameter pLastAuditNote as char no-undo.
def output parameter pSuccess as logical.
def output parameter pMsg as char.


run server/newaudit.p( input integer(pYear),
                       input pAgent,
                       input datetime(pReviewDate),
                       input pAuditor,
                       input pAuditType,
                       input pErrType,
                       input pResncode,
                       output pAuditID,
                       output pLastAuditNote,
                       output pSuccess,
                       output pMsg).
if not pSuccess then
do:
  MESSAGE pMsg
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
  return error.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PreQueueAuditService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PreQueueAuditService Procedure 
PROCEDURE PreQueueAuditService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditId as char no-undo.
def output parameter audityear as integer no-undo.
def output parameter eorequired as log no-undo.
def output parameter maxCoverage as decimal no-undo.
def output parameter lastauditscore as int no-undo.
def output parameter opencorrectact as char no-undo.
def output parameter ytdnetpremium as decimal no-undo.
def output parameter ytdgrosspremium as decimal no-undo.
def output parameter avggap as decimal no-undo.
def output parameter isminamtremitted as logical no-undo.
def output parameter twentypercentdrop as char no-undo.
def output parameter causetwentypercentdrop as char no-undo.
def output parameter opencorrquestionsid as char no-undo.
def output parameter claimprocess as char no-undo.
def output parameter pSuccess as logical no-undo.
def output parameter pMsg as char no-undo.
run server/prequeueaudit.p(input integer(pAuditId),
                           output audityear,
                           output eorequired,
                           output maxCoverage,
                           output lastauditscore,  
                           output opencorrectact, 
                           output ytdnetpremium ,
                           output ytdgrosspremium, 
                           output avggap, 
                           output isminamtremitted, 
                           output twentypercentdrop, 
                           output causetwentypercentdrop,
                           output opencorrquestionsid, 
                           output claimprocess, 
                           output pSuccess,
                           output pMsg).
if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-QueueAuditService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QueueAuditService Procedure 
PROCEDURE QueueAuditService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter  pAuditId as char.
def input parameter  pAuditor as char.
def input parameter table for audit .
def input parameter table for bkgQuestion .
def output parameter pSuccess as logical.
def output parameter  pMsg as char.

run server/queueaudit.p(input integer(pAuditId),
                        input pAuditor,
                        input table audit,
                        input table bkgQuestion,
                        output pSuccess,
                        output pMsg).
if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RescheduleAuditService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RescheduleAuditService Procedure 
PROCEDURE RescheduleAuditService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditID as int no-undo.
def input parameter rescheduledate as datetime no-undo.
def input parameter pReason as char no-undo.
define output parameter pSuccess as logical .
define output parameter tmsg as character   no-undo.

run server/rescheduleAudit.p (input pAuditID,
                              input rescheduledate,
                              input pReason,
                              output pSuccess,
                              output tMsg ).

if not pSuccess
then
 do:
   message tmsg view-as alert-box error buttons ok.
   return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetScheduleagentsService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetScheduleagentsService Procedure 
PROCEDURE SetScheduleagentsService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for ttaudit.
  run server/setScheduledagents.p (input table ttaudit,
                                   output std-lo,
                                   output std-ch).
  
  std-ch = "".
  if not std-lo
   then std-ch = " with errors".
  message "Agent(s) successfully scheduled" + std-ch view-as alert-box information buttons ok.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

