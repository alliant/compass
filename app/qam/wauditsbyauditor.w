&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wstartgapreport.w
   Window of qars diffrance between sheduled start date and audit start date
   10.10.2017
   @ Anjly Chanana
   @Modified  07/15/2021    SA  Task 83510 modified UI according to 
                                new field grade 
              09/24/2021    SA  Task 86696 Defects raised by david
*/

CREATE WIDGET-POOL.

{tt/auditor.i}
{tt/reportQARauditor.i &tableAlias="data"}
{tt/qaraudit.i &TableAlias="audit"}

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/get-column.i}
{lib/getstatename.i}
{lib/winlaunch.i}

define variable lOpenQAR as logical.
define variable hQarSummary as handle.

define variable currState as character no-undo.
define variable currStatus as character no-undo.
define variable currYear as integer no-undo.

define variable dColumn as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQar

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwQar                                        */
&Scoped-define FIELDS-IN-QUERY-brwQar data.qarID data.auditYear data.statDesc data.auditTypeDesc data.stateID data.auditscore data.grade data.agentId data.agentName data.auditFinishDate data.primaryAuditor data.secondaryAuditor   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQar   
&Scoped-define SELF-NAME brwQar
&Scoped-define QUERY-STRING-brwQar FOR EACH data by data.qarID
&Scoped-define OPEN-QUERY-brwQar OPEN QUERY {&SELF-NAME} FOR EACH data by data.qarID.
&Scoped-define TABLES-IN-QUERY-brwQar data
&Scoped-define FIRST-TABLE-IN-QUERY-brwQar data


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQar}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 bRefresh bExport ~
tAuditor fStatus fStateID fYear brwQar 
&Scoped-Define DISPLAYED-OBJECTS tAuditor fStatus fStateID fYear 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pStatus as character,
    input pYear as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character,
    input pYear as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilterYearCombo C-Win 
FUNCTION getFilterYearCombo RETURNS CHARACTER
  ( input pStatus as character,
    input pState as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFilterCombo C-Win 
FUNCTION setFilterCombo RETURNS LOGICAL
  ( input pName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE fStateID AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24.8 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fYear AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL",0
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 90.4 BY 2.62.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12 BY 2.62.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 45.4 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQar FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQar C-Win _FREEFORM
  QUERY brwQar DISPLAY
      data.qarID label "QAR ID" format ">9999999" width 10
data.auditYear label "Year" format "9999" width 8
data.statDesc label "Status" format "x(12)" width 12
data.auditTypeDesc label "Audit Type" format "x(20)" width 12
data.stateID label "State" format "x(20)" width 7
data.auditscore label "Points" format "zzz"  width 8
data.grade label "Score%" format "zzz"  width 9
data.agentId label "Agent ID" format "x(15)" width 12
data.agentName label "Agent Name" format "x(200)" width 28
data.auditFinishDate column-label "Finish Date" format "99/99/9999" width 15
data.primaryAuditor column-label "Primary Auditor" format "x(100)" width 28
data.secondaryAuditor column-label "Secondary Auditor" format "x(500)" width 28
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 188 BY 21.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bRefresh AT ROW 1.95 COL 39 WIDGET-ID 4
     bExport AT ROW 2 COL 140.4 WIDGET-ID 2
     tAuditor AT ROW 2.33 COL 11 COLON-ALIGNED WIDGET-ID 24
     fStatus AT ROW 2.33 COL 55 COLON-ALIGNED WIDGET-ID 148
     fStateID AT ROW 2.33 COL 84 COLON-ALIGNED WIDGET-ID 150
     fYear AT ROW 2.33 COL 117 COLON-ALIGNED WIDGET-ID 154
     brwQar AT ROW 4.33 COL 3 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 28
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 139 WIDGET-ID 32
     "Filter" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1.24 COL 49 WIDGET-ID 152
     RECT-1 AT ROW 1.48 COL 48 WIDGET-ID 26
     RECT-2 AT ROW 1.48 COL 138 WIDGET-ID 30
     RECT-3 AT ROW 1.48 COL 3 WIDGET-ID 36
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 191.6 BY 25.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Audit by Auditor"
         HEIGHT             = 25.76
         WIDTH              = 191.6
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQar fYear fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwQar:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQar:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQar
/* Query rebuild information for BROWSE brwQar
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH data by data.qarID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQar */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Audit by Auditor */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Audit by Auditor */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Audit by Auditor */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQar
&Scoped-define SELF-NAME brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON DEFAULT-ACTION OF brwQar IN FRAME fMain
DO:
 if not available data 
  then return.

  publish "checkOpenQar" (input data.qarID,
                          output lOpenQar,
                          output hQarSummary
                          ).
  if lOpenQar
   then
    do:
      run ViewWindow in hQarSummary no-error.
      return.
    end.
   else
    do:
      {lib/pbshow.i "''"}
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
      empty temp-table audit.
      publish "GetQarAudits" (0, data.qarID, "", "", output table audit).
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 50}
      run QARSummary.w persistent (input table audit).
      run ViewWindow in hQarSummary no-error.
      {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
      {lib/pbhide.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON ROW-DISPLAY OF brwQar IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON START-SEARCH OF brwQar IN FRAME fMain
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStateID C-Win
ON VALUE-CHANGED OF fStateID IN FRAME fMain /* State */
DO:
  setFilterCombo(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  setFilterCombo(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fYear C-Win
ON VALUE-CHANGED OF fYear IN FRAME fMain /* Year */
DO:
  setFilterCombo(self:label).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditor C-Win
ON VALUE-CHANGED OF tAuditor IN FRAME fMain /* Auditor */
DO:  
  close query brwQAR.
  empty temp-table data.
  bExport:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/brw-main.i}                   
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

/* get the auditors */
publish "GetAuditors"(output table auditor).
tAuditor:delimiter = {&msg-dlm}.
std-ch = "ALL" {&msg-add} "ALL".
for each auditor no-lock:
  std-ch = addDelimiter(std-ch,{&msg-dlm}) + auditor.name {&msg-add} auditor.uid.
end.
tAuditor:list-item-pairs = std-ch.
tAuditor:screen-value = "ALL".

session:immediate-display = yes.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  bExport:sensitive = false.
  
  std-ha = getColumn({&browse-name}:handle, "agentName").
  IF VALID-HANDLE(std-ha)
   THEN dColumn = std-ha:width * session:pixels-per-column.

  run windowResized in this-procedure.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.

IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAuditor fStatus fStateID fYear 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-3 bRefresh bExport tAuditor fStatus fStateID fYear 
         brwQar 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwQar:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
  
  &scoped-define ReportName "Audit_By_Auditor"
  
  std-ch = "".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    empty temp-table data.
    run server/getauditsbyauditor.p (input tAuditor:screen-value,
                                     input false,
                                     output table data,
                                     output std-lo,
                                     output std-ch
                                     ).
                            
    if not std-lo
     then message std-ch view-as alert-box error buttons ok.
     else
      do:
        for each data exclusive-lock:
          publish "GetSysPropDesc" ("QAR", "Audit", "Status", data.stat, output data.statDesc).
          publish "GetSysPropDesc" ("QAR", "Audit", "Type", data.auditType, output data.auditTypeDesc).
        end.
        setFilterCombo("ALL").
        
        if query brwQar:num-results = 0
         then bExport:sensitive = false.
         else bExport:sensitive = true.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 if {&window-name}:window-state eq window-minimized  then
 {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStatus as character no-undo.
  define variable cState as character no-undo.
  define variable iYear as integer no-undo.
  
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
    assign
      cStatus = fStatus:input-value
      cState = fStateID:input-value
      iYear = fYear:input-value
      .
  end.
  
  /* build the query */
  if cStatus <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stat = '" + cStatus + "'".
  if cState <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "stateID = '" + cState + "'".
  if iYear > 0
   then tWhereClause = addDelimiter(tWhereClause," and ") + "auditYear = " + string(iYear).
  
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +" &post-by-clause=" + ' by  data.qarID' "}
  std-ch =  " audit(s) found as of " + string(now).
  status default string(num-results("{&browse-name}")) + std-ch in window {&window-name}.
  status input string(num-results("{&browse-name}")) + std-ch  in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dDiffWidth as decimal no-undo.
  
  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

  /* fSearch components */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.
  browse brwQAR:width-pixels = frame fMain:width-pixels - 20.

  if {&window-name}:width-pixels > frame fMain:width-pixels 
   then
    do: 
     frame fMain:width-pixels = {&window-name}:width-pixels.
     frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
    end.
   else
    do:
     frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
     frame fMain:width-pixels = {&window-name}:width-pixels.
    end.
  browse brwQAR:height-pixels = frame fMain:height-pixels - browse brwQAR:y - 10.
   
  std-ha = getColumn({&browse-name}:handle, "agentName").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dColumn + (dDiffWidth * 0.75).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStateCombo C-Win 
FUNCTION getFilterStateCombo RETURNS CHARACTER
  ( input pStatus as character,
    input pYear as integer ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cList as character no-undo.
  define variable cName as character no-undo.
  define buffer data for data.
 
  cList = "ALL,ALL".
  for each data no-lock:
     
    if pStatus <> "ALL" and pStatus <> data.stat
     then next.
     
    if pYear > 0 and pYear <> data.auditYear
     then next.
   
    if lookup(data.stateID,cList) = 0
     then cList = addDelimiter(cList,",") + getStateName(data.stateID) + "," + data.stateID.
  end.
  RETURN cList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterStatusCombo C-Win 
FUNCTION getFilterStatusCombo RETURNS CHARACTER
  ( input pState as character,
    input pYear as integer ) :
/*------------------------------------------------------------------------------
@description Get a list of agent statuses for the state
------------------------------------------------------------------------------*/
  define variable cList as character no-undo.
  define variable cName as character no-undo.
  define buffer data for data.
 
  cList = "ALL,ALL".
  for each data no-lock:
    
    if pState <> "ALL" and pState <> data.stateID
     then next.
     
    if pYear > 0 and pYear <> data.auditYear
     then next.
     
    if lookup(data.stat,cList) = 0
     then
      do:
        cName = "".
        publish "GetSysPropDesc" ("QAR", "Audit", "Status", data.stat, output cName).
        cList = addDelimiter(cList,",") + cName + "," + data.stat.
      end.
  end.
  RETURN cList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilterYearCombo C-Win 
FUNCTION getFilterYearCombo RETURNS CHARACTER
  ( input pStatus as character,
    input pState as character ) :
/*------------------------------------------------------------------------------
@description Get a list of agent managers for the state
------------------------------------------------------------------------------*/
  define variable cList as character no-undo.
  define variable cName as character no-undo.
  define buffer data for data.
 
  cList = "ALL,0".
  for each data no-lock
        by data.auditYear desc:
     
    if pStatus <> "ALL" and pStatus <> data.stat
     then next.
     
    if pState <> "ALL" and pState <> data.stateID
     then next.
   
    if lookup(string(data.auditYear),cList) = 0
     then cList = addDelimiter(cList,",") + string(data.auditYear) + "," + string(data.auditYear).
  end.
  RETURN cList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFilterCombo C-Win 
FUNCTION setFilterCombo RETURNS LOGICAL
  ( input pName as character ) :
/*------------------------------------------------------------------------------
@description Sets the filter combo boxes
------------------------------------------------------------------------------*/
  define variable sortColumn as character no-undo.
  do with frame {&frame-name}:
    assign
      currState = fStateID:screen-value
      currStatus = fStatus:screen-value
      currYear = fYear:input-value
      .
    case pName:
     when "ALL" then
      assign
        fStateID:list-item-pairs = getFilterStateCombo(currStatus, currYear)
        fStatus:list-item-pairs = getFilterStatusCombo(currState, currYear)
        fYear:list-item-pairs = getFilterYearCombo(currStatus, currState)
        .
     when "State" then
      assign
        fStatus:list-item-pairs = getFilterStatusCombo(currState, currYear)
        fYear:list-item-pairs = getFilterYearCombo(currStatus, currState)
        .
     when "Year" then
      assign
        fStateID:list-item-pairs = getFilterStateCombo(currStatus, currYear)
        fStatus:list-item-pairs = getFilterStatusCombo(currState, currYear)
        .
     when "Status" then
      assign
        fStateID:list-item-pairs = getFilterStateCombo(currStatus, currYear)
        fYear:list-item-pairs = getFilterYearCombo(currStatus, currState)
        .
    end case.
    /* set the state */
    if lookup(currState,fStateID:list-item-pairs) > 0
     then fStateID:screen-value = currState.
     else fStateID:screen-value = "ALL".
    /* set the status */
    if lookup(currStatus,fStatus:list-item-pairs) > 0
     then fStatus:screen-value = currStatus.
     else fStatus:screen-value = "ALL".
    /* set the year */
    if lookup(string(currYear),fYear:list-item-pairs) > 0
     then fYear:screen-value = string(currYear).
     else fYear:screen-value = "0".
  end.
  sortColumn = dataSortBy.
  dataSortBy = "".
  run sortData in this-procedure (sortColumn).
  apply "VALUE-CHANGED" to brwQAR in frame {&frame-name}.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

