&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File: dialogrestore.w
  Description:restore audit from server
  Author: AG
  Created: 05.11.2017
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
 {tt/qarnote.i &tableAlias="qarsnote"}
 {lib/std-def.i}
/* Local Variable Definitions ---                                       */
define input parameter pQarId as character no-undo.
define input parameter pAgentId as character no-undo.
define input parameter pName as character no-undo.
define output parameter pReason as character no-undo .
define output parameter pCancel as logical init false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tReason tRestore Btn_OK Btn_Cancel tQarId ~
tName 
&Scoped-Define DISPLAYED-OBJECTS tReason tRestore tQarId tName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY  NO-CONVERT-3D-COLORS
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tReason AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 67 BY 2.24 NO-UNDO.

DEFINE VARIABLE tRestore AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 67 BY 3.43 NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
      VIEW-AS TEXT 
     SIZE 61.8 BY .62 NO-UNDO.

DEFINE VARIABLE tQarId AS CHARACTER FORMAT "X(256)":U 
     LABEL "QAR" 
      VIEW-AS TEXT 
     SIZE 30 BY .62 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tReason AT ROW 4.1 COL 5 NO-LABEL WIDGET-ID 2
     tRestore AT ROW 7.1 COL 5 NO-LABEL WIDGET-ID 18
     Btn_OK AT ROW 10.86 COL 22.2
     Btn_Cancel AT ROW 10.86 COL 39.2
     tQarId AT ROW 1.48 COL 8.8 COLON-ALIGNED WIDGET-ID 6
     tName AT ROW 2.48 COL 9.8 COLON-ALIGNED WIDGET-ID 10
     "Restore Reason:" VIEW-AS TEXT
          SIZE 16.2 BY .62 AT ROW 6.48 COL 5.2 WIDGET-ID 20
     "Cancelled Reason:" VIEW-AS TEXT
          SIZE 18.2 BY .62 AT ROW 3.48 COL 5 WIDGET-ID 16
     SPACE(51.79) SKIP(8.08)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Restore Audit"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       tReason:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE
       tReason:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tRestore:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Restore Audit */
DO:
  pCancel = true .
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  pCancel = true .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  pReason = tRestore:screen-value .
    if pReason = "" then
    do:
      message "Reason cannot be blank. "
        view-as alert-box info buttons ok.
      apply "entry" to tReason.
      return no-apply.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRestore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRestore Dialog-Frame
ON VALUE-CHANGED OF tRestore IN FRAME Dialog-Frame
DO:
  assign pReason = tRestore:screen-value.
  if length(pReason) > 0 then
    enable btn_ok with frame dialog-frame.
  if length(pReason) = 0 then
    disable btn_ok with frame dialog-frame.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


 publish "GetQarNotes" (input int (pQarId),
                     output table qarsnote) .

 find qarsnote where qarsnote.stat = "X" no-error.
 if available qarsnote then
 tReason =  substring(qarsnote.comments, 12).


MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  tQarId:screen-value = pQarId.
  if length(pname)> 50 then
    tName:screen-value = substring(pName,1,50) + " " + "..." + " " + "(" + pAgentId + ")".
  else
    tName:screen-value = substring(pName,1,50) + " " + "(" + pAgentId + ")".
  tname:tooltip = pname + " " + pAgentID.
  tRestore:screen-value = pReason.
  if length(pReason) = 0 then
    disable btn_ok with frame dialog-frame.
  apply 'entry' to tRestore.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tReason tRestore tQarId tName 
      WITH FRAME Dialog-Frame.
  ENABLE tReason tRestore Btn_OK Btn_Cancel tQarId tName 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

