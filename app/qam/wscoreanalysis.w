&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wscoreanalysis.w
   Window of scoreanalysis across qar records
   08.22.2017
   @ Anjly Chanana
   @ Modified :
    Date          Name           Description
    10/08/2020    Anjly Chanana  Added logic for TOR score fields
    07/15/2021    SA             Task 83510 modified UI according to 
                                 new field grade
    09/24/2021    SA             Task 86696 Defects raised by david
   */

CREATE WIDGET-POOL.

{tt/qarfinding.i}
{tt/qar.i}
{tt/auditor.i}
{tt/agent.i}
{tt/state.i}
{tt/qaraudit.i &TableAlias="ttcurrentaudit"}
{tt/qaraudit.i &TableAlias="ttprevaudit"}
{tt/scoreanalysis.i &TableAlias="tscoreanalysis"}
{tt/scoreanalysis.i &TableAlias="ttscoreanalysis"}

{lib/std-def.i}
def temp-table scoreanalysis like ttscoreanalysis
  field cprevQarID as char
  field ccurrentQarID as char
  field ErrScorechange as char
  field AuditScorechange as char
  field TorScorechange as char
  field tAuditType as char  .

def var pAgent as char no-undo.
def var pAuditor as char no-undo.
def var pYear as char no-undo.
def var auditorpair as char no-undo.
def var agentpair as char no-undo.
def var yearpair as char no-undo.
def var iBgColor as int.
def var opencurrentqar as logical.
def var currentqarsummaryhandle as handle.
def var openprevqar as logical.
def var prevqarsummaryhandle as handle.
{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQar

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES scoreanalysis

/* Definitions for BROWSE brwQar                                        */
&Scoped-define FIELDS-IN-QUERY-brwQar scoreanalysis.cprevQarID scoreanalysis.ccurrentQarID scoreanalysis.state scoreanalysis.prevAuditor scoreanalysis.currentAuditor scoreanalysis.prevAuditFinishDate scoreanalysis.currentAuditFinishDate entry(index("EQUT0", scoreanalysis.prevAudittype),"ERR,QAR,UnderWriter,TOR, ") @ scoreanalysis.prevAudittype entry(index("EQUT0", scoreanalysis.audittype),"ERR,QAR,UnderWriter,TOR, ") @ scoreanalysis.audittype scoreanalysis.prevAuditScore scoreanalysis.currentAuditScore scoreanalysis.prevAuditgrade scoreanalysis.currentAuditgrade scoreanalysis.auditGradeDiff scoreanalysis.agentID scoreanalysis.agentName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQar   
&Scoped-define SELF-NAME brwQar
&Scoped-define QUERY-STRING-brwQar FOR EACH scoreanalysis
&Scoped-define OPEN-QUERY-brwQar OPEN QUERY {&SELF-NAME} FOR EACH scoreanalysis.
&Scoped-define TABLES-IN-QUERY-brwQar scoreanalysis
&Scoped-define FIRST-TABLE-IN-QUERY-brwQar scoreanalysis


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQar}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 cState fAuditScoreGap ~
bRefresh bExport sYear cbAgent brwQar 
&Scoped-Define DISPLAYED-OBJECTS cState fAuditScoreGap sYear cbAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQar 
       MENU-ITEM m_View_Previous_Audit LABEL "View Previous Audit"
       MENU-ITEM m_View_Current_Audit LABEL "View Current Audit".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE cbAgent AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 87.8 BY 1 NO-UNDO.

DEFINE VARIABLE cState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1 TOOLTIP "Select current year for report" NO-UNDO.

DEFINE VARIABLE fAuditScoreGap AS INTEGER FORMAT "->>9%":U INITIAL 6 
     VIEW-AS FILL-IN 
     SIZE 6 BY .95 TOOLTIP "Minimum score change from previous to current" NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 4.52.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.4 BY 4.52.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 34.2 BY 4.52.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQar FOR 
      scoreanalysis SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQar C-Win _FREEFORM
  QUERY brwQar DISPLAY
      scoreanalysis.cprevQarID             column-label "Previous!QAR ID"  width 10 format "x(15)"     
 scoreanalysis.ccurrentQarID           column-label "Current!QAR ID"  width 10 format "x(15)"  
 
 scoreanalysis.state                   label "State" format "x(2)"        width 12
 scoreanalysis.prevAuditor             column-label "Previos!Auditor" format "x(3)"  width 12
 scoreanalysis.currentAuditor          column-label "Current!Auditor" format "x(3)"  width 12
 scoreanalysis.prevAuditFinishDate     column-label "Previous!Finish!Audit!Date" format "99/99/99" width 12
 scoreanalysis.currentAuditFinishDate  column-label "Current!Finish!Audit!Date" format "99/99/99" width 12
 
 entry(index("EQUT0", scoreanalysis.prevAudittype),"ERR,QAR,UnderWriter,TOR, ") @ scoreanalysis.prevAudittype column-label "Previous!Audit!Type"     format "x(12)"    width 12
 entry(index("EQUT0", scoreanalysis.audittype),"ERR,QAR,UnderWriter,TOR, ") @ scoreanalysis.audittype column-label "Current!Audit!Type"      format "x(12)"   width 12
 
 scoreanalysis.prevAuditScore          column-label "Previous!Audit!Point"  format ">>>>>9"         width 10
 scoreanalysis.currentAuditScore       column-label "Current!Audit!Point"   format ">>>>>9"         width 10
                                                                                                    
 scoreanalysis.prevAuditgrade          column-label "Previous!Audit!Score%"  format "zzz"           width 10
 scoreanalysis.currentAuditgrade       column-label "Current!Audit!Score%"   format "zzz"           width 10
 scoreanalysis.auditGradeDiff          column-label "Audit!Score%!Change"    format "-zzz"           width 10
                                                                                                    
 scoreanalysis.agentID                 column-label "Agent ID"              format "x(10)"          width 10
 scoreanalysis.agentName               column-label "Agent Name"            format "x(100)"         width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 208.6 BY 20.62 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cState AT ROW 2.86 COL 44 COLON-ALIGNED WIDGET-ID 150
     fAuditScoreGap AT ROW 2.86 COL 101.6 NO-LABEL WIDGET-ID 34
     bRefresh AT ROW 3.1 COL 34.2 RIGHT-ALIGNED WIDGET-ID 40
     bExport AT ROW 3.1 COL 138 WIDGET-ID 2
     sYear AT ROW 3.43 COL 8 COLON-ALIGNED WIDGET-ID 22
     cbAgent AT ROW 4.05 COL 39.2 WIDGET-ID 18
     brwQar AT ROW 6.48 COL 3.2 WIDGET-ID 200
     "Audit score change in :" VIEW-AS TEXT
          SIZE 22 BY 1 AT ROW 2.86 COL 79 WIDGET-ID 156
     "Parameters" VIEW-AS TEXT
          SIZE 11.4 BY .62 AT ROW 1.43 COL 4 WIDGET-ID 28
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.43 COL 37.6 WIDGET-ID 42
     "Action" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.43 COL 136.8 WIDGET-ID 32
     RECT-1 AT ROW 1.71 COL 38 WIDGET-ID 26
     RECT-2 AT ROW 1.71 COL 135.6 WIDGET-ID 30
     RECT-3 AT ROW 1.71 COL 3.2 WIDGET-ID 38
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 212.8 BY 26.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Score Analysis Report"
         HEIGHT             = 26.81
         WIDTH              = 212.8
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 328.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 328.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQar cbAgent fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bRefresh IN FRAME fMain
   ALIGN-R                                                              */
ASSIGN 
       brwQar:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwQar:HANDLE
       brwQar:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQar:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME fMain
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fAuditScoreGap IN FRAME fMain
   ALIGN-L                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQar
/* Query rebuild information for BROWSE brwQar
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH scoreanalysis.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQar */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Score Analysis Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Score Analysis Report */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Score Analysis Report */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQar
&Scoped-define SELF-NAME brwQar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON ROW-DISPLAY OF brwQar IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  if current-result-row("brwQar") modulo 2 = 0 then
    iBgColor = 17.
 else
    iBgColor = 15.


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON START-SEARCH OF brwQar IN FRAME fMain
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQar C-Win
ON VALUE-CHANGED OF brwQar IN FRAME fMain
DO:
  run SetButton.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent C-Win
ON VALUE-CHANGED OF cbAgent IN FRAME fMain /* Agent */
DO:
    if cbagent:screen-value in frame fmain = ? then
       cbagent:screen-value in frame fmain = " ".
    run searchData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cState C-Win
ON VALUE-CHANGED OF cState IN FRAME fMain /* State */
DO:
  run searchData in this-procedure.
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAuditScoreGap
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAuditScoreGap C-Win
ON VALUE-CHANGED OF fAuditScoreGap IN FRAME fMain
DO:
  run searchData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Current_Audit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Current_Audit C-Win
ON CHOOSE OF MENU-ITEM m_View_Current_Audit /* View Current Audit */
DO:
find current scoreanalysis no-error.
if available scoreanalysis then
 find first ttscoreanalysis where ttscoreanalysis.currentQarID = scoreanalysis.currentQarID no-error.
 if available ttscoreanalysis then
 do:              
   create ttcurrentaudit.
   assign
     ttcurrentaudit.QarID  =      ttscoreanalysis.currentQarID  
     ttcurrentaudit.agentID =     ttscoreanalysis.agentID  
     ttcurrentaudit.name =        ttscoreanalysis.agentName 
     ttcurrentaudit.state =       ttscoreanalysis.state 
     ttcurrentaudit.stat =    "C"
     .
 end.
publish "checkOpenQar" (input scoreanalysis.currentQarID,
                        output opencurrentqar,
                        output currentqarsummaryhandle ).
if opencurrentqar
  then
   do:
     run ViewWindow in currentqarsummaryhandle no-error.
     empty temp-table ttcurrentaudit.
     return.
   end.
   
else
  do:
    {lib/pbshow.i "''"}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
    run QARSummary.w persistent ( input table ttcurrentaudit ).
    run ViewWindow in currentqarsummaryhandle no-error.   
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
    {lib/pbhide.i}
    empty temp-table ttcurrentaudit.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Previous_Audit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Previous_Audit C-Win
ON CHOOSE OF MENU-ITEM m_View_Previous_Audit /* View Previous Audit */
DO:
    find current scoreanalysis no-error.
if available scoreanalysis then
 find first ttscoreanalysis where ttscoreanalysis.prevQarID = scoreanalysis.prevQarID no-error.
 if available ttscoreanalysis then
 do:              
   create ttprevaudit.
   assign
     ttprevaudit.QarID  =      ttscoreanalysis.prevQarID  
     ttprevaudit.agentID =     ttscoreanalysis.agentID  
     ttprevaudit.name =        ttscoreanalysis.agentName 
     ttprevaudit.state =       ttscoreanalysis.state 
            ttprevaudit.stat =    "C"
     .
 end.
publish "checkOpenQar" (input scoreanalysis.prevQarID,
                        output openprevqar,
                        output prevqarsummaryhandle ).
if openprevqar
  then
   do:
     run ViewWindow in prevqarsummaryhandle no-error.
     empty temp-table ttprevaudit.
     return.
   end.
   
else
  do:
    {lib/pbshow.i "''"}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
    run QARSummary.w persistent ( input table ttprevaudit ).
    run ViewWindow in prevqarsummaryhandle no-error.   
    {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
    {lib/pbhide.i}
    empty temp-table ttprevaudit.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain /* Year */
DO:
 empty temp-table scoreanalysis.
 if sYear:screen-value in frame fMain  = pyear and
     cbAGent:screen-value in frame fMain = pAgent  then
     open query brwqar for each scoreanalysis.
 else
     close query brwqar.
 run SetCount.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/brw-main.i}                   
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

do std-in = 0 to 9:
  yearpair = yearpair + string(year(today)+ 1 - std-in) + "," + string(year(today) + 1 - std-in) + "," .
end.
ASSIGN

syear:list-item-pairs = trim(yearpair, ",").
syear:screen-value    = string(year(today)).

{lib/get-state-list.i &combo=cState &addAll=true}
{lib/get-agent-list.i &combo=cbAgent &state=cState}

 std-ch = "".
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.  
 apply 'value-changed' to fAuditScoreGap.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/set-current-value.i &state=cState}

  
run SetButton.
run windowResized in this-procedure.

IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CreateScoreAnalysis C-Win 
PROCEDURE CreateScoreAnalysis :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 create scoreanalysis.
assign
  scoreanalysis.prevQarID               = if ttscoreanalysis.prevQarID = ? then 0 else ttscoreanalysis.prevQarID                                                                                                  
  scoreanalysis.currentQarID            = if ttscoreanalysis.currentQarID = ? then 0 else ttscoreanalysis.currentQarID                                                                                            
  scoreanalysis.agentID                 = if ttscoreanalysis.agentID = ? or ttscoreanalysis.agentID = "?" then "" else ttscoreanalysis.agentID                                                                                                     
  scoreanalysis.agentName               = if ttscoreanalysis.agentName = ? or ttscoreanalysis.agentName = "?" then "" else ttscoreanalysis.agentName                                                                                                 
  scoreanalysis.state                   = if ttscoreanalysis.state = ? or ttscoreanalysis.state = "?" then "" else ttscoreanalysis.state                                                                                                         
 
 scoreanalysis.prevAuditor             = if ttscoreanalysis.prevAuditor = ? or ttscoreanalysis.prevAuditor = "?" then "" else ttscoreanalysis.prevAuditor                                                                                             
  scoreanalysis.currentAuditor          = if ttscoreanalysis.currentAuditor = ? or ttscoreanalysis.currentAuditor = "?" then "" else ttscoreanalysis.currentAuditor                                                                                       
  scoreanalysis.prevAuditFinishDate     = if ttscoreanalysis.prevAuditFinishDate = ? then date("") else ttscoreanalysis.prevAuditFinishDate                                                                       
  scoreanalysis.currentAuditFinishDate  = if ttscoreanalysis.currentAuditFinishDate = ? then date("") else ttscoreanalysis.currentAuditFinishDate
  scoreanalysis.cprevQarID              = if ttscoreanalysis.prevQarID = ? then "" else string(ttscoreanalysis.prevQarID)                                                                                                  
  scoreanalysis.ccurrentQarID           = if ttscoreanalysis.currentQarID = ? then "" else string(ttscoreanalysis.currentQarID) 
  
  scoreanalysis.audittype               = if ttscoreanalysis.audittype = ""  or ttscoreanalysis.audittype = "?" then "0" else  ttscoreanalysis.audittype  
  scoreanalysis.prevAudittype           = if ttscoreanalysis.prevAudittype = ""  or ttscoreanalysis.prevAudittype = "?" then "0" else  ttscoreanalysis.prevAudittype  
  scoreanalysis.prevAuditScore          = if ttscoreanalysis.prevAuditScore = ? then 0 else ttscoreanalysis.prevAuditScore                                                                                        
  scoreanalysis.currentAuditScore       = if ttscoreanalysis.currentAuditScore = ? then 0 else ttscoreanalysis.currentAuditScore  
  scoreanalysis.prevAuditgrade          = if ttscoreanalysis.prevAuditgrade = ? then 0 else ttscoreanalysis.prevAuditgrade 
  scoreanalysis.currentAuditGrade       = if ttscoreanalysis.currentAuditGrade = ? then 0 else ttscoreanalysis.currentAuditGrade 
  scoreanalysis.auditGradeDiff          = if ttscoreanalysis.auditGradeDiff = ? then 0 else ttscoreanalysis.auditGradeDiff 
  .                                                     
  
  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cState fAuditScoreGap sYear cbAgent 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-3 cState fAuditScoreGap bRefresh bExport sYear 
         cbAgent brwQar 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var th as handle no-undo.
    
empty temp-table tscoreanalysis.
 if query brwQar:num-results = 0
  then
   do:
     bExport:sensitive in frame fMain = true.
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.
   else
   bExport:sensitive in frame fMain  = false.

   for each scoreanalysis:
     create tscoreanalysis.
     buffer-copy  scoreanalysis to tscoreanalysis.
     if tscoreanalysis.Audittype = "E" then
        tscoreanalysis.Audittype = "ERR".
     if tscoreanalysis.Audittype = "Q" then
        tscoreanalysis.Audittype = "QAR".
     if tscoreanalysis.Audittype = "U" then
        tscoreanalysis.Audittype = "UnderWriter".
     if tscoreanalysis.Audittype = "T" then
        tscoreanalysis.Audittype = "TOR".

     if tscoreanalysis.prevAudittype = "E" then
        tscoreanalysis.prevAudittype = "ERR".
     if tscoreanalysis.prevAudittype = "Q" then
        tscoreanalysis.prevAudittype = "QAR".
     if tscoreanalysis.prevAudittype = "U" then
        tscoreanalysis.prevAudittype = "UnderWriter".
     if tscoreanalysis.prevAudittype = "T" then
        tscoreanalysis.prevAudittype = "TOR".
   end.
       
 publish "GetReportDir" (output std-ch).
 th = temp-table tscoreanalysis:handle.

run util/exporttable.p (table-handle th,
                         "tscoreanalysis",
                         "for each tscoreanalysis ",
                         "cprevQarID,ccurrentQarID,State,prevAuditor,currentAuditor,prevAuditFinishDate,currentAuditFinishDate,prevAudittype,audittype,prevAuditScore,currentAuditScore,prevAuditgrade,currentAuditgrade,auditGradeDiff,agentID,agentName",
                         "Previous QAR ID,Current QAR ID,State,Previous Auditor,Current Auditor,Previous Audit Finish Date,Current Audit Finish Date,Previous Audit Type,Current Audit Type,Previous Audit Point,Current Audit Point,Previous Audit Score,Current Audit Score,Audit Score Change,Agent ID,Agent Name",
                         std-ch,
                         "Scoreanalysis-"  + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

if  cbAgent:screen-value in frame fMain       = ""  and                    
    string(sYear:screen-value in frame fMain) = "0"                            
    then    
 do:
   MESSAGE "By selecting all options the query could be slow. Are you sure you want to continue?"
     VIEW-AS ALERT-BOX question BUTTONS ok-cancel update lchoice as logical.
   if lChoice = true then
   do:
     publish "getscoreanalysis" (input sYear:screen-value in frame fMain ,
                                 output table ttscoreanalysis).
   end. 
   else if lchoice = false then return.
end.

publish "getscoreanalysis" (input sYear:screen-value in frame fMain ,
                            output table ttscoreanalysis).

apply 'value-changed' to fAuditScoreGap.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE searchData C-Win 
PROCEDURE searchData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:      
------------------------------------------------------------------------------*/
  empty temp-table scoreanalysis.
  do with frame fMain:
  end.
  std-in = 0.
  
  define variable igradeDiff as integer no-undo.
   
  igradeDiff = integer(trim(fAuditScoreGap:screen-value,'%')). 
   
 for each ttscoreanalysis no-lock where ttscoreanalysis.prevauditgrade ne 0:
 
   if (cbAgent:screen-value > "" and lookup(ttscoreanalysis.agentID, cbAgent:screen-value) = 0)
   then next.
   if (cState:screen-value <> "ALL" and lookup(ttscoreanalysis.state, cState:screen-value) = 0)
   then next.
   
   
   if igradeDiff > 0 then
     do:
     
       if ttscoreanalysis.auditGradeDiff ge igradeDiff then
       do:
         run CreateScoreAnalysis.
         std-in = std-in + 1.
      end.
     end.
   if igradeDiff < 0 then
     do:
       if ttscoreanalysis.auditGradeDiff le igradeDiff then
       do:
         run CreateScoreAnalysis.
         std-in = std-in + 1.
      end.
     end.
   if igradeDiff = 0 then
     do:
       if ttscoreanalysis.auditGradeDiff  eq igradeDiff then
       do:
         run CreateScoreAnalysis.
         std-in = std-in + 1.
       end.
     end.
     
  
end.
 std-ch = string(std-in) + " " +  "record(s) found" .
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.

 pAgent =  cbAgent:screen-value in frame fMain.
 pYear = sYear:screen-value in frame fMain.

OPEN QUERY brwQar FOR EACH scoreanalysis .
run SetButton.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetButton C-Win 
PROCEDURE SetButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

if query brwQar:num-results = 0
  then
  assign
    menu-item m_View_Previous_Audit:sensitive in menu POPUP-MENU-brwQAR = false
    menu-item m_View_Current_Audit:sensitive in menu POPUP-MENU-brwQAR = false
    bExport:sensitive in frame fMain = false.
 else
   do:
     bExport:sensitive in frame fMain  = true.
     find current scoreanalysis no-error.
     if available scoreanalysis then
     do:
       if scoreanalysis.prevQarID eq 0 then
         menu-item m_View_Previous_Audit:sensitive in menu POPUP-MENU-brwQAR = false.
       else
         menu-item m_View_Previous_Audit:sensitive in menu POPUP-MENU-brwQAR = true.
       if scoreanalysis.currentQarID eq 0 then
         menu-item m_View_Current_Audit:sensitive in menu POPUP-MENU-brwQAR = false.
       else
         menu-item m_View_Current_Audit:sensitive in menu POPUP-MENU-brwQAR = true. 
     end.
end.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetCount C-Win 
PROCEDURE SetCount :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
std-in = 0.
for each scoreanalysis no-lock:
    std-in = std-in + 1.
end.
 std-ch = string(std-in) + " " + "record(s) found".
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.

 if brwqar:num-iterations in frame fMain = 0 then
 do:
  
  std-ch = "" .
  status default std-ch in window {&window-name}.
  status input std-ch in window {&window-name}.
  bExport:sensitive in frame fMain = false.
 end.
 else
  bExport:sensitive in frame fMain = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
 if {&window-name}:window-state eq window-minimized  then
 {&window-name}:window-state = window-normal .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i }
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.


 /* fSearch components */
 browse brwQAR:width-pixels = frame fMain:width-pixels - 20.

if {&window-name}:width-pixels > frame fMain:width-pixels 
  then
   do: 
         frame fMain:width-pixels = {&window-name}:width-pixels.
         frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
         
   end.
  else
   do:
         frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
         frame fMain:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
    
   end.
 browse brwQAR:height-pixels = frame fMain:height-pixels - browse brwQAR:y - 10.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

