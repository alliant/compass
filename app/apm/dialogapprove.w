&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fApprove 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

define input parameter pInvoiceID as integer no-undo.
define input parameter pType as character no-undo.
define input parameter hFileDataSrv as handle no-undo.
define output parameter pSuccess as logical no-undo.

define variable dAmount as decimal no-undo.
define variable cNotes as character no-undo.
{lib/std-def.i}
{lib/add-delimiter.i}
{lib/get-reserve-type.i}

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fApprove

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAmount tNotes bApprove bReject bCancel 
&Scoped-Define DISPLAYED-OBJECTS tInvoiceAmount tAmount tNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bApprove 
     LABEL "Approve" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bReject 
     LABEL "Reject" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 60 BY 4.38
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Approval Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 TOOLTIP "The amount to approve"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tInvoiceAmount AS DECIMAL FORMAT "$ ->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Invoice Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     FONT 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fApprove
     tInvoiceAmount AT ROW 1.48 COL 18.4 COLON-ALIGNED WIDGET-ID 78
     tAmount AT ROW 2.67 COL 18.4 COLON-ALIGNED WIDGET-ID 70
     tNotes AT ROW 4.95 COL 3 NO-LABEL WIDGET-ID 72
     bApprove AT ROW 10.05 COL 9
     bReject AT ROW 10.05 COL 25 WIDGET-ID 82
     bCancel AT ROW 10.05 COL 41
     "Notes:" VIEW-AS TEXT
          SIZE 6.2 BY .91 AT ROW 4 COL 3 WIDGET-ID 74
     SPACE(56.19) SKIP(6.84)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Approve Invoice"
         DEFAULT-BUTTON bApprove CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fApprove
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fApprove:SCROLLABLE       = FALSE
       FRAME fApprove:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tInvoiceAmount IN FRAME fApprove
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fApprove fApprove
ON WINDOW-CLOSE OF FRAME fApprove /* Approve Invoice */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApprove fApprove
ON CHOOSE OF bApprove IN FRAME fApprove /* Approve */
DO:
  do with frame {&frame-name}:
    if tInvoiceAmount:input-value < tAmount:input-value
     then
      do:
        message "The Approved amount is greater than the Invoice Amount. Continue?" view-as alert-box warning buttons yes-no update std-lo.
        if not std-lo
         then
          do:
            apply "ENTRY" to tAmount.
            return.
          end.
      end.
  end.

  define variable cUserList as character no-undo.
  define variable cUID as character no-undo.
  define variable cName as character no-undo.
  define variable cUser as character no-undo.
  define variable cReason as character no-undo initial "".
  run GetPayableInvoiceApprover in hFileDataSrv (pInvoiceID, output cUserList).
  if cUserList = ""
   then
    do:
      /* I added if then else as users were getting an error I cannot recreate. Simply  */
      /* put, if the user approval list is empty, that means that the invoice has already */
      /* been fully approved */
      pSuccess = true.
      APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
    end.
   else
    do:
      publish "GetCredentialsID" (output cUID).
      std-lo = true.
      if lookup(cUID,cUserList) = 0
       then
        do:
          /* get the user list */
          std-ch = "".
          do std-in = 1 to num-entries(cUserList):
            cUser = entry(std-in,cUserList).
            publish "GetSysUserName" (cUser, output cName).
            std-ch = addDelimiter(std-ch,",") + cName + "," + cUser.
          end.
          run dialogapproveforuser.w (std-ch, output cUser, output cReason, output std-lo).
          /* add to the note */
          if std-lo
           then
            do:
              publish "GetSysUserName" (cUser, output cName).
              cReason = "APPROVED FOR: " + cName + chr(10) +
                        "REASON: " + cReason + chr(10) + chr(10).
            end.
        end.
       else cUser = cUID.
      
      if std-lo /* not canceled from dialogapproveforuser.w if used */
       then
        do with frame {&frame-name}:
          cReason = cReason + tNotes:screen-value.
          run value("Approve" + pType + "Invoice") in hFileDataSrv
                                                   (input pInvoiceID,
                                                    input tAmount:input-value,
                                                    input cReason,
                                                    input cUser,
                                                    output pSuccess
                                                    ).
          if pSuccess
           then APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
        end.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fApprove
ON CHOOSE OF bCancel IN FRAME fApprove /* Cancel */
DO:
  pSuccess = false.
  APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReject fApprove
ON CHOOSE OF bReject IN FRAME fApprove /* Reject */
DO:
  do with frame {&frame-name}:
    if tNotes:input-value = ""
     then
      do:
        MESSAGE "Please add a reason for rejecting the invoice" VIEW-AS ALERT-BOX INFO BUTTONS OK.
        return.
      end.
  end.
      
  define variable cUserList as character no-undo.
  define variable cUID as character no-undo.
  define variable cName as character no-undo.
  define variable cUser as character no-undo.
  define variable cReason as character no-undo initial "".
  run GetPayableInvoiceApprover in hFileDataSrv (pInvoiceID, output cUserList).
  publish "GetCredentialsID" (output cUID).
  std-lo = true.
  if lookup(cUID,cUserList) = 0
   then
    do:
      /* get the user list */
      std-ch = "".
      do std-in = 1 to num-entries(cUserList):
        cUser = entry(std-in,cUserList).
        publish "GetSysUserName" (cUser, output cName).
        std-ch = addDelimiter(std-ch,",") + cName + "," + cUser.
      end.
      run dialogapproveforuser.w (std-ch, output cUser, output cReason, output std-lo).
      /* add to the note */
      publish "GetSysUserName" (cUser, output cName).
      cReason = "REJECTED FOR: " + cName + chr(10) +
                "REASON: " + cReason + chr(10) + chr(10).
    end.
   else cUser = cUID.
  
  if std-lo /* not canceled from dialogapproveforuser.w if used */
   then
    do with frame {&frame-name}:
      cReason = cReason + tNotes:screen-value.
      run value("Reject" + pType + "Invoice") in hFileDataSrv
                                               (input pInvoiceID,
                                                input cReason,
                                                input cUser,
                                                output pSuccess
                                                ).
      if pSuccess
       then APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fApprove 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run GetPayableInvoiceAmount in hFileDataSrv (input pInvoiceID, output std-de).
  do with frame {&frame-name}:
    assign
      tInvoiceAmount:screen-value = string(std-de)
      tAmount:screen-value = string(std-de)
      .
  end.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fApprove  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fApprove.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fApprove  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tInvoiceAmount tAmount tNotes 
      WITH FRAME fApprove.
  ENABLE tAmount tNotes bApprove bReject bCancel 
      WITH FRAME fApprove.
  VIEW FRAME fApprove.
  {&OPEN-BROWSERS-IN-QUERY-fApprove}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

