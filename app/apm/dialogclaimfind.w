&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

{tt/claim.i}
{tt/state.i}
{tt/agent.i}
{tt/claimlink.i}

define output parameter pClaimID as character no-undo.
define output parameter pCancel as logical no-undo initial true.

{lib/add-delimiter.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bLookupClaim cmbClaimID tToClaimID ~
tDescription tAssignedTo tType tStat tState tAgent tFileNumber tInsuredName ~
tSummary Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cmbClaimID tToClaimID tDescription ~
tAssignedTo tType tStat tState tAgent tFileNumber tInsuredName tSummary 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearClaim fMain 
FUNCTION clearClaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayClaim fMain 
FUNCTION displayClaim RETURNS LOGICAL
  ( input pClaimID as integer )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAgentName fMain 
FUNCTION getAgentName RETURNS CHARACTER
  ( input pAgentID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStateName fMain 
FUNCTION getStateName RETURNS CHARACTER
  ( input pStateID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bLookupClaim  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search for the claim".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cmbClaimID AS INTEGER FORMAT ">>>>>>>9":U INITIAL 0 
     LABEL "Claim Number" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 16 BY 1.05
     FONT 0 NO-UNDO.

DEFINE VARIABLE tSummary AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 64 BY 3 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 100 BY 1 NO-UNDO.

DEFINE VARIABLE tAssignedTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS FILL-IN 
     SIZE 50.2 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 100 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent File Number" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tInsuredName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Name" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tToClaimID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Claim Number" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bLookupClaim AT ROW 1.71 COL 39 WIDGET-ID 366 NO-TAB-STOP 
     cmbClaimID AT ROW 1.71 COL 20 COLON-ALIGNED WIDGET-ID 394
     tToClaimID AT ROW 1.71 COL 20 COLON-ALIGNED WIDGET-ID 106
     tDescription AT ROW 3.76 COL 19.8 COLON-ALIGNED WIDGET-ID 390 NO-TAB-STOP 
     tAssignedTo AT ROW 4.95 COL 19.8 COLON-ALIGNED WIDGET-ID 346 NO-TAB-STOP 
     tType AT ROW 4.95 COL 78.8 COLON-ALIGNED WIDGET-ID 340 NO-TAB-STOP 
     tStat AT ROW 4.95 COL 103.8 COLON-ALIGNED WIDGET-ID 338 NO-TAB-STOP 
     tState AT ROW 6.14 COL 19.8 COLON-ALIGNED WIDGET-ID 370 NO-TAB-STOP 
     tAgent AT ROW 7.33 COL 19.8 COLON-ALIGNED WIDGET-ID 372 NO-TAB-STOP 
     tFileNumber AT ROW 8.52 COL 19.8 COLON-ALIGNED WIDGET-ID 42 NO-TAB-STOP 
     tInsuredName AT ROW 9.71 COL 19.8 COLON-ALIGNED WIDGET-ID 336 NO-TAB-STOP 
     tSummary AT ROW 10.91 COL 21.8 NO-LABEL WIDGET-ID 188 NO-TAB-STOP 
     Btn_OK AT ROW 14.33 COL 46
     Btn_Cancel AT ROW 14.33 COL 63
     "Summary:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 11 COL 12 WIDGET-ID 282
     SPACE(102.99) SKIP(4.47)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Find Claim"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

ASSIGN 
       cmbClaimID:HIDDEN IN FRAME fMain           = TRUE.

ASSIGN 
       tAgent:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAssignedTo:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tDescription:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tFileNumber:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tInsuredName:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tStat:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tState:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tSummary:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tType:READ-ONLY IN FRAME fMain        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Find Claim */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLookupClaim
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLookupClaim fMain
ON CHOOSE OF bLookupClaim IN FRAME fMain /* Lookup */
DO:
  clearClaim().
  if cmbClaimID:hidden
   then
    do:
      empty temp-table claim.
      if tToClaimID:screen-value > ""
       then publish "SearchClaim" (tToClaimID:screen-value, output table claim).
       else return.
       
      std-in = 0.
      for each claim no-lock:
        std-in = std-in + 1.
      end.
      
      case std-in:
       when 0 then return.
       when 1 then
        for first claim no-lock:
          displayClaim(claim.claimID).
          pClaimID = tToClaimID:screen-value.
        end.
       otherwise
        do:
          std-ch = "".
          for each claim no-lock:
            std-ch = addDelimiter(std-ch,",") + string(claim.claimID).
          end.
          assign
            cmbClaimID:list-items = std-ch
            cmbClaimID:hidden = false
            tToClaimID:hidden = true
            .
          bLookupClaim:load-image("images/s-erase.bmp").
          bLookupClaim:load-image-insensitive("images/s-erase-i.bmp").
        end.
      end case.
    end.
   else
    do:
      bLookupClaim:load-image("images/s-magnifier.bmp").
      bLookupClaim:load-image-insensitive("images/s-magnifier-i.bmp").
      assign
        tToClaimID:screen-value = ""
        tToClaimID:hidden = false
        cmbClaimID:hidden = true
        .
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbClaimID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbClaimID fMain
ON VALUE-CHANGED OF cmbClaimID IN FRAME fMain /* Claim Number */
DO:
  displayClaim(self:input-value).
  pClaimID = self:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tToClaimID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tToClaimID fMain
ON LEAVE OF tToClaimID IN FRAME fMain /* Claim Number */
DO:
  if cmbClaimID:hidden
   then apply 'choose' to bLookupClaim.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tToClaimID fMain
ON RETURN OF tToClaimID IN FRAME fMain /* Claim Number */
DO:
  if cmbClaimID:hidden
   then apply 'choose' to bLookupClaim.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bLookupClaim:load-image("images/s-magnifier.bmp").
bLookupClaim:load-image-insensitive("images/s-magnifier-i.bmp").

RUN enable_UI.
assign
  Btn_OK:sensitive = false
  cmbClaimID:hidden = true
  tToClaimID:hidden = false
  .

MAIN-BLOCK:
repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  pCancel = false.
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cmbClaimID tToClaimID tDescription tAssignedTo tType tStat tState 
          tAgent tFileNumber tInsuredName tSummary 
      WITH FRAME fMain.
  ENABLE bLookupClaim cmbClaimID tToClaimID tDescription tAssignedTo tType 
         tStat tState tAgent tFileNumber tInsuredName tSummary Btn_OK 
         Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearClaim fMain 
FUNCTION clearClaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
    assign
      tDescription:screen-value = ""
      tAssignedTo:screen-value = ""
      tType:screen-value = ""
      tStat:screen-value = ""
      tState:screen-value = ""
      tAgent:screen-value = ""
      tFileNumber:screen-value = ""
      tInsuredName:screen-value = ""
      tSummary:screen-value = ""
      .
  end.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayClaim fMain 
FUNCTION displayClaim RETURNS LOGICAL
  ( input pClaimID as integer ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  define variable tTypeList as character no-undo initial "Notice,N,Claim,C,Matter,M".
  define variable tStatList as character no-undo initial "Open,O,Closed,C".
  do with frame {&frame-name}:
  
    empty temp-table claim.
    if tToClaimID:input-value > 0
     then publish "SearchClaim" (tToClaimID:input-value, output table claim).
    find first claim where claimID = pClaimID no-error.
    
    if available claim
     then
      do:
        publish "GetSysUserName" (input claim.assignedTo, output tAssignedTo).
        
        if cmbClaimID:hidden
         then tToClaimID:screen-value = string(pClaimID).
      
        assign
          tType = entry(lookup(claim.type,tTypeList) - 1, tTypeList)
          tStat = entry(lookup(claim.stat,tStatList) - 1, tStatList)
          tDescription = claim.description
          tState = getStateName(claim.stateID)
          tAgent = getAgentName(claim.agentID)
          tFileNumber = claim.fileNumber
          tInsuredName = claim.insuredName
          tSummary = claim.summary
          Btn_OK:sensitive = true
          .
        apply 'entry' to Btn_OK.
          
        display
          tDescription
          tAssignedTo
          tType
          tStat
          tState
          tAgent
          tFileNumber
          tInsuredName
          tSummary
          .
      end.
    else clearClaim().
  end.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAgentName fMain 
FUNCTION getAgentName RETURNS CHARACTER
  ( input pAgentID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo initial "UNKNOWN".
  define buffer agent for agent.
  publish "GetAgent" (pAgentID, output table agent).
  
  for first agent no-lock:
    pReturn = agent.name + "  [" + agent.agentID + "]".
  end.
  
  RETURN pReturn.   

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStateName fMain 
FUNCTION getStateName RETURNS CHARACTER
  ( input pStateID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable pReturn as character no-undo initial "UNKNOWN".
  define buffer state for state.
  publish "GetStates" (output table state).
  
  for first state no-lock
      where state.stateID = pStateID:
    pReturn = state.description.
  end.
  
  RETURN pReturn.   

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

