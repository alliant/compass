&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wMain 
/*---------------------------------------------------------------------
@name wclaimpayable.w
@action 
@description Dialog for creating/modifying/viewing a claim payable

@param ClaimID;int;The claim ID
@param Apinv;complex;Table for the invoice
@param Apinva;complex;Table for the invoice approvals
@param Apinvd;complex;Table for the invoice distributions
@param FileDataSrv;handle;The handle to the filedatasrv.p procedure
@param Title;char;The title for the window
@param Success;logical;True if the invoice was saved

@author John Oliver
@version 1.0
@created 01/09/2017

@changelog
---------------------------------------------------------------------
01.26.2017 John Oliver: Added a check if the policy was valid for a
                        Loss payment
---------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{lib/brw-multi-def.i}
{lib/next-seq.i}
{lib/count-rows.i}
{lib/get-column.i}
{lib/get-reserve-type.i}
{lib/account-repository.i}
{lib/change-made.i}
{lib/std-def.i}
{lib/add-delimiter.i}
{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/account.i}
{tt/vendor.i}
{tt/sysuser.i}
{tt/claim.i}
{tt/claimcoverage.i}

/* Parameters Definitions ---                                           */
define input parameter pClaimID as integer no-undo.
define input parameter table for apinv.
define input parameter table for apinva.
define input parameter table for apinvd.
define input parameter pTitle as character no-undo.
define input parameter hFileDataSrv as handle no-undo.
define output parameter pSuccess as logical no-undo.

/* Local Variable Definitions ---                                       */
define variable lChanged as logical no-undo initial false.
define variable lLocked as logical no-undo initial false.
define variable lFileAdded as logical no-undo initial false.
define variable lFileDeleted as logical no-undo initial false.
define variable cCurrUserID as character no-undo.
define variable cAcctName as character no-undo.
define variable cVendorName as character no-undo.
define variable cVendorAddress as character no-undo.
define variable iInvoice as int no-undo.
define variable iSeq as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fInvoice
&Scoped-define BROWSE-NAME brwApproval

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES apinva apinvd

/* Definitions for BROWSE brwApproval                                   */
&Scoped-define FIELDS-IN-QUERY-brwApproval apinva.username apinva.amount apinva.stat apinva.dateActed apinva.uid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwApproval   
&Scoped-define SELF-NAME brwApproval
&Scoped-define QUERY-STRING-brwApproval FOR EACH apinva
&Scoped-define OPEN-QUERY-brwApproval OPEN QUERY {&SELF-NAME} FOR EACH apinva.
&Scoped-define TABLES-IN-QUERY-brwApproval apinva
&Scoped-define FIRST-TABLE-IN-QUERY-brwApproval apinva


/* Definitions for BROWSE brwDistribution                               */
&Scoped-define FIELDS-IN-QUERY-brwDistribution apinvd.acct apinvd.acctName apinvd.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDistribution apinvd.acct apinvd.amount   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwDistribution apinvd
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwDistribution apinvd
&Scoped-define SELF-NAME brwDistribution
&Scoped-define QUERY-STRING-brwDistribution FOR EACH apinvd
&Scoped-define OPEN-QUERY-brwDistribution OPEN QUERY {&SELF-NAME} FOR EACH apinvd.
&Scoped-define TABLES-IN-QUERY-brwDistribution apinvd
&Scoped-define FIRST-TABLE-IN-QUERY-brwDistribution apinvd


/* Definitions for FRAME fInvoice                                       */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fInvoice ~
    ~{&OPEN-QUERY-brwApproval}~
    ~{&OPEN-QUERY-brwDistribution}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-36 RECT-38 RECT-39 bVendorLookup ~
bVendorReport tVendor bLock chkInContention tVendorName tVendorAddress ~
bAddApproval brwApproval bDeleteApproval cmbRefCategory cmbCoverage ~
tInvoiceNbr tPONbr tInvoiceDate tDateReceived tAmount tDueDate ~
tApprovalNotes bFileUpload tFile tNotes bAddDistribution brwDistribution ~
bDeleteDistribution bSave bDelete bCancel 
&Scoped-Define DISPLAYED-OBJECTS tVendor chkInContention tRefID tVendorName ~
tVendorAddress cmbRefCategory cmbCoverage tInvoiceNbr tPONbr tInvoiceDate ~
tDateReceived tAmount tDueDate tApprovalNotes tFile tNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD IsPayableUsed wMain 
FUNCTION IsPayableUsed RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Lock wMain 
FUNCTION Lock RETURNS LOGICAL
  ( input pLock as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD LockAll wMain 
FUNCTION LockAll RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeApproval wMain 
FUNCTION SensitizeApproval RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeButtons wMain 
FUNCTION SensitizeButtons RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeDistribution wMain 
FUNCTION SensitizeDistribution RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeFileUpload wMain 
FUNCTION SensitizeFileUpload RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wMain AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU menuFile 
       MENU-ITEM m_Delete_File  LABEL "Delete File"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddApproval 
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add an approval".

DEFINE BUTTON bAddDistribution 
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a distribution".

DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bDeleteApproval 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected approver".

DEFINE BUTTON bDeleteDistribution 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected distribution".

DEFINE BUTTON bFileUpload 
     LABEL "Upload" 
     SIZE 4.8 BY 1.14 TOOLTIP "Upload an invoice document".

DEFINE BUTTON bLock 
     LABEL "Unlock" 
     SIZE 7.2 BY 1.71 TOOLTIP "Lock the invoice".

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bVendorLookup 
     LABEL "Vendor Lookup" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bVendorReport 
     LABEL "Vendor Report" 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE cmbCoverage AS INTEGER FORMAT "999999999":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 19 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE cmbRefCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX 
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 19 BY 1 TOOLTIP "The referring category of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tApprovalNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62 BY 11.67 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 86 BY 1.67
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The amount of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDateReceived AS DATE FORMAT "99/99/9999":U 
     LABEL "Date Received" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The date of when Alliant National received the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Due Date" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The due date of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 80 BY 1 TOOLTIP "The path of the invoice document within ShareFile" DROP-TARGET NO-UNDO.

DEFINE VARIABLE tInvoiceDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Invoice Date" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The date on the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tInvoiceNbr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Invoice #" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The invoice number" NO-UNDO.

DEFINE VARIABLE tPONbr AS CHARACTER FORMAT "X(256)":U 
     LABEL "P.O. Number" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The P.O. number associated with the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tRefID AS INTEGER FORMAT "99999999":U INITIAL 0 
     LABEL "Claim Number" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 TOOLTIP "The claim number"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor Number" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "Tthe GP Vendor Number"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tVendorAddress AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor Address" 
     VIEW-AS FILL-IN 
     SIZE 75 BY .95
     FONT 1 NO-UNDO.

DEFINE VARIABLE tVendorName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor Name" 
     VIEW-AS FILL-IN 
     SIZE 75 BY 1
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92 BY 6.19.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 91.8 BY 6.43.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 18.57.

DEFINE VARIABLE chkInContention AS LOGICAL INITIAL no 
     LABEL "Mark the Invoice in Contention" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.6 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwApproval FOR 
      apinva SCROLLING.

DEFINE QUERY brwDistribution FOR 
      apinvd SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwApproval wMain _FREEFORM
  QUERY brwApproval DISPLAY
      apinva.username column-label "Name" format "x(32)" width 20
      apinva.amount column-label "Amount" format "->,>>>,>>9.99" width 12
      apinva.stat column-label "Status" format "x(20)" width 7
      apinva.dateActed column-label "Approved" format "99/99/99" width 10
      apinva.uid column-label "User" format "x(30)" width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 57 BY 4.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDistribution wMain _FREEFORM
  QUERY brwDistribution DISPLAY
      apinvd.acct column-label "Account Number" format "x(25)" width 18
      apinvd.acctName column-label "Account Name" format "x(50)" width 47
      apinvd.amount column-label "Amount" format "->,>>>,>>9.99" width 12
    ENABLE apinvd.acct apinvd.amount
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 83 BY 4.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fInvoice
     bVendorLookup AT ROW 1.62 COL 34 WIDGET-ID 66 NO-TAB-STOP 
     bVendorReport AT ROW 1.62 COL 38.8 WIDGET-ID 100 NO-TAB-STOP 
     tVendor AT ROW 1.71 COL 18 COLON-ALIGNED WIDGET-ID 8
     bLock AT ROW 1.71 COL 156 WIDGET-ID 74 NO-TAB-STOP 
     chkInContention AT ROW 1.76 COL 92.4 WIDGET-ID 86
     tRefID AT ROW 2.1 COL 140 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tVendorName AT ROW 2.91 COL 18 COLON-ALIGNED WIDGET-ID 64
     tVendorAddress AT ROW 4.1 COL 18 COLON-ALIGNED WIDGET-ID 58
     bAddApproval AT ROW 4.76 COL 99 WIDGET-ID 44 NO-TAB-STOP 
     brwApproval AT ROW 4.81 COL 104 WIDGET-ID 300
     bDeleteApproval AT ROW 5.95 COL 99 WIDGET-ID 52 NO-TAB-STOP 
     cmbRefCategory AT ROW 6.71 COL 18 COLON-ALIGNED WIDGET-ID 28
     cmbCoverage AT ROW 6.71 COL 63 COLON-ALIGNED WIDGET-ID 84
     tInvoiceNbr AT ROW 7.91 COL 18 COLON-ALIGNED WIDGET-ID 2
     tPONbr AT ROW 7.91 COL 63 COLON-ALIGNED WIDGET-ID 62
     tInvoiceDate AT ROW 9.1 COL 18 COLON-ALIGNED WIDGET-ID 10
     tDateReceived AT ROW 9.1 COL 63 COLON-ALIGNED WIDGET-ID 56
     tAmount AT ROW 10.29 COL 18 COLON-ALIGNED WIDGET-ID 12
     tDueDate AT ROW 10.29 COL 63 COLON-ALIGNED WIDGET-ID 14
     tApprovalNotes AT ROW 10.29 COL 99 NO-LABEL WIDGET-ID 72 NO-TAB-STOP 
     bFileUpload AT ROW 12.19 COL 90.2 WIDGET-ID 82
     tFile AT ROW 12.29 COL 7 COLON-ALIGNED WIDGET-ID 80 NO-TAB-STOP 
     tNotes AT ROW 13.67 COL 9 NO-LABEL WIDGET-ID 30
     bAddDistribution AT ROW 16.95 COL 4 WIDGET-ID 42 NO-TAB-STOP 
     brwDistribution AT ROW 17 COL 9 WIDGET-ID 200
     bDeleteDistribution AT ROW 18.1 COL 4 WIDGET-ID 54 NO-TAB-STOP 
     bSave AT ROW 22.91 COL 58 WIDGET-ID 36
     bDelete AT ROW 22.91 COL 75 WIDGET-ID 96
     bCancel AT ROW 22.91 COL 92 WIDGET-ID 38
     "Approvals" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 3.62 COL 98 WIDGET-ID 46
          FONT 6
     "Mark the Invoice in Contention?" VIEW-AS TEXT
          SIZE 31 BY .62 AT ROW 1.86 COL 60.2 WIDGET-ID 88
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 13.67 COL 2.2 WIDGET-ID 32
     "Invoice Information" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 5.52 COL 25 RIGHT-ALIGNED WIDGET-ID 6
          FONT 6
     "Distributions" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 15.81 COL 4 WIDGET-ID 40
          FONT 6
     RECT-36 AT ROW 5.76 COL 3 WIDGET-ID 4
     RECT-38 AT ROW 16.05 COL 3.2 WIDGET-ID 76
     RECT-39 AT ROW 3.86 COL 97 WIDGET-ID 78
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 163.2 BY 23.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wMain ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 23.57
         WIDTH              = 163.4
         MAX-HEIGHT         = 23.57
         MAX-WIDTH          = 163.4
         VIRTUAL-HEIGHT     = 23.57
         VIRTUAL-WIDTH      = 163.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wMain
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fInvoice
   FRAME-NAME                                                           */
/* BROWSE-TAB brwApproval bAddApproval fInvoice */
/* BROWSE-TAB brwDistribution bAddDistribution fInvoice */
ASSIGN 
       tApprovalNotes:READ-ONLY IN FRAME fInvoice        = TRUE.

ASSIGN 
       tFile:POPUP-MENU IN FRAME fInvoice       = MENU menuFile:HANDLE.

/* SETTINGS FOR FILL-IN tRefID IN FRAME fInvoice
   NO-ENABLE                                                            */
/* SETTINGS FOR TEXT-LITERAL "Invoice Information"
          SIZE 22 BY .62 AT ROW 5.52 COL 25 RIGHT-ALIGNED               */

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wMain)
THEN wMain:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwApproval
/* Query rebuild information for BROWSE brwApproval
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apinva.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwApproval */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDistribution
/* Query rebuild information for BROWSE brwDistribution
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apinvd.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwDistribution */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wMain wMain
ON END-ERROR OF wMain
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wMain wMain
ON WINDOW-CLOSE OF wMain
DO:
  std-lo = true.
  if lChanged
   then message
          "Data has been modified." skip
          "Are you sure you want to exit without saving?"
          view-as alert-box warning buttons yes-no
          update std-lo.
  if std-lo
   then APPLY "CLOSE":U TO THIS-PROCEDURE.
  
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddApproval wMain
ON CHOOSE OF bAddApproval IN FRAME fInvoice /* Add */
DO:
  RUN AddApproval IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddDistribution wMain
ON CHOOSE OF bAddDistribution IN FRAME fInvoice /* Add */
DO:
  RUN AddDistribution IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel wMain
ON CHOOSE OF bCancel IN FRAME fInvoice /* Cancel */
DO:
  APPLY "WINDOW-CLOSE":U TO wMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete wMain
ON CHOOSE OF bDelete IN FRAME fInvoice /* Delete */
DO:
  if iInvoice > 0
   then 
    for first apinv no-lock:
      publish "DeleteInvoiceDialog" (iInvoice, "P").
      APPLY "CLOSE":U TO THIS-PROCEDURE.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteApproval wMain
ON CHOOSE OF bDeleteApproval IN FRAME fInvoice /* Delete */
DO:
  RUN DeleteApproval IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteDistribution wMain
ON CHOOSE OF bDeleteDistribution IN FRAME fInvoice /* Delete */
DO:
  RUN DeleteDistribution IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload wMain
ON CHOOSE OF bFileUpload IN FRAME fInvoice /* Upload */
DO:
  run FileUpload in this-procedure ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLock
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLock wMain
ON CHOOSE OF bLock IN FRAME fInvoice /* Unlock */
DO:
  Lock(not lLocked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwApproval
&Scoped-define SELF-NAME brwApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwApproval wMain
ON ROW-DISPLAY OF brwApproval IN FRAME fInvoice
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwApproval wMain
ON START-SEARCH OF brwApproval IN FRAME fInvoice
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDistribution
&Scoped-define SELF-NAME brwDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistribution wMain
ON ROW-DISPLAY OF brwDistribution IN FRAME fInvoice
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistribution wMain
ON ROW-LEAVE OF brwDistribution IN FRAME fInvoice
DO:
  define variable hBrowse as handle no-undo.
  define variable oldAcct as character no-undo initial "".
  define variable newAcct as character no-undo initial "".
  hBrowse = browse brwDistribution:handle.
  if valid-handle(hBrowse) and iSeq > 0 /* a sequence of 0 mean that this isn't really a row-leave */
   then
    do:
      /* if this is a new record */
      if hBrowse:new-row
       then lChanged = true.
       else
        assign
          iSeq = apinvd.seq
          oldAcct = apinvd.acct
          cAcctName = apinvd.acctname
          .
       
       
      /* get the account number and the amount to see if they changed */
      std-ha = getColumn(hBrowse,"acct").
      if valid-handle(std-ha)
       then newAcct = std-ha:input-value.
      /* only get the account name if it changed */
      if oldAcct <> newAcct
       then
        do:
          /* get the account name */
          publish "GetAccountName" (newAcct, output cAcctName).
          /* set the account name */
          std-ha = getColumn(hBrowse,"acctname").
          if valid-handle(std-ha)
           then std-ha:screen-value = cAcctName.
        end.
      
      
      /* modify the record */
      for first apinvd
          where apinvd.seq = iSeq:
         
        /* account name */
        apinvd.acctname = cAcctName.
          
        /* account number */
        std-ha = getColumn(hBrowse,"acct").
        if valid-handle(std-ha) and changeMade(std-ha:screen-value,"acct","apinvd")
         then 
          assign
            lChanged = true
            apinvd.acct = std-ha:input-value
            .
         
        /* amount */
        std-ha = getColumn(hBrowse,"amount").
        if valid-handle(std-ha) and changeMade(std-ha:screen-value,"amount","apinvd")
         then 
          assign
            lChanged = true
            apinvd.amount = std-ha:input-value
            .
      end.
      {&OPEN-QUERY-brwDistribution}
      
      /* prevent users from editting the browse */
      hBrowse:sensitive = false.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistribution wMain
ON START-SEARCH OF brwDistribution IN FRAME fInvoice
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave wMain
ON CHOOSE OF bSave IN FRAME fInvoice /* Save */
DO:
  /* check if the invoice date is after today */
  if tInvoiceDate:input-value > today
   then
    do:
      std-lo = false.
      message "The invoice date is after today. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
  RUN SaveInvoice IN THIS-PROCEDURE (output std-lo).
  if std-lo
   then APPLY "WINDOW-CLOSE" TO wMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bVendorLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bVendorLookup wMain
ON CHOOSE OF bVendorLookup IN FRAME fInvoice /* Vendor Lookup */
DO:
  run sys/wvendordialog.w (output std-ch).
  if std-ch > ""
   then run RefreshVendor in this-procedure (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bVendorReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bVendorReport wMain
ON CHOOSE OF bVendorReport IN FRAME fInvoice /* Vendor Report */
DO:
  publish "SetCurrentValue" ("VendorID", tVendor:screen-value in frame {&frame-name}).
  publish "ActionWindowShow" ("wapm02-r.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME chkInContention
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL chkInContention wMain
ON VALUE-CHANGED OF chkInContention IN FRAME fInvoice /* Mark the Invoice in Contention */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"contention","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbRefCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbRefCategory wMain
ON VALUE-CHANGED OF cmbRefCategory IN FRAME fInvoice /* Category */
DO:
  define variable lChangeMade as logical no-undo.
  
  lChangeMade = changeMade(self:screen-value,"refCategory","apinv").
  
  do with frame {&frame-name}:
    std-lo = true.
    if lChangeMade and can-find(first apinva where apinva.stat <> "P")
     then 
      do:
        message "Changing the category will also reset the approvals. Continue?" view-as alert-box warning buttons yes-no update std-lo as logical.
        lChanged = std-lo.
      end.
     else lChanged = lChangeMade or lChanged.
        
    if lChanged
     then
      do:
        lChanged = true.
        empty temp-table apinva.
        empty temp-table apinvd.
        cmbCoverage:hidden = true.
        if not self:screen-value = "Unknown"
         then
          case self:screen-value:
           when "L" then run MakeClaimLoss in this-procedure (0).
           when "E" then run MakeClaimLAE in this-procedure.
          end case.
        {&OPEN-BROWSERS-IN-QUERY-fInvoice}
      END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_File wMain
ON CHOOSE OF MENU-ITEM m_Delete_File /* Delete File */
DO:
  do with frame {&frame-name}:
    if tFile:screen-value <> ""
     then
      do:
        std-lo = true.
        publish "GetConfirmDelete" (output std-lo).
        if std-lo
         then
          do:
            std-lo = false.
            MESSAGE "Document will be permanently removed. Continue?" VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
            if not std-lo 
             then return.
          end.
        assign
          tFile:screen-value = ""
          lFileDeleted = true
          lChanged = true
          menu-item m_Delete_File:sensitive in menu menuFile = false
          .
      end.
    /* as the save button is disabled when the invoice is completed */
    /* we need to detach the invoice on the fly */
    if can-find(first apinv where stat = "C")
     then
      do:
        run DetachPayableDocument in hFileDataSrv (iInvoice, output pSuccess).
        pSuccess = true.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAmount wMain
ON LEAVE OF tAmount IN FRAME fInvoice /* Amount */
DO:
  
  define variable lChangeMade as logical no-undo.
  
  lChangeMade = changeMade(self:screen-value,"amount","apinv").
  do with frame {&frame-name}:
    if lChangeMade
     then
      for each apinvd exclusive-lock:
        apinvd.amount = 0.0.
        if apinvd.seq = 1
         then apinvd.amount = tAmount:input-value.
      end.
    /* for a loss payment, change the approver as they may have changed
    if lChangeMade and cmbRefCategory:screen-value = "L"
     then
      do:
        /* check if there is a valid approval for the new amount */
        define buffer apinva for apinva.
        publish "GetClaimLossApprover" (tRefID:input-value, 
                                        cmbRefCategory:screen-value, 
                                        self:input-value, 
                                        OUTPUT std-ch).
        /* remove all people that haven't approved as they might not be able to approve */
        for each apinva exclusive-lock
           where apinva.stat <> "A":
          
          delete apinva.
        end.
        run AddApprovalToList in this-procedure (std-ch).
      end. */
  end.
  lChanged = lChangeMade.
  if lChangeMade
   then {&OPEN-QUERY-brwDistribution}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDateReceived
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDateReceived wMain
ON LEAVE OF tDateReceived IN FRAME fInvoice /* Date Received */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"dateReceived","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDueDate wMain
ON LEAVE OF tDueDate IN FRAME fInvoice /* Due Date */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"dueDate","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFile wMain
ON DROP-FILE-NOTIFY OF tFile IN FRAME fInvoice /* File */
DO:
  run FileUpload in this-procedure (self:get-dropped-file(1)).
  tFile:end-file-drop().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFile wMain
ON MOUSE-SELECT-DBLCLICK OF tFile IN FRAME fInvoice /* File */
DO:
  for first apinv no-lock:
    if apinv.apinvID > 0 and apinv.hasDocument
     then run ViewPayableDocument in hFileDataSrv (apinv.apinvID).
     else
      do:
       if tFile:screen-value > ""
        then run util/openfile.p (tFile:screen-value).
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tInvoiceDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tInvoiceDate wMain
ON LEAVE OF tInvoiceDate IN FRAME fInvoice /* Invoice Date */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"invoiceDate","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tInvoiceNbr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tInvoiceNbr wMain
ON LEAVE OF tInvoiceNbr IN FRAME fInvoice /* Invoice # */
DO:
  if changeMade(self:screen-value,"invoiceNumber","apinv")
   then
    do:
      IsPayableUsed().
      lChanged = true.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes wMain
ON LEAVE OF tNotes IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"notes","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPONbr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPONbr wMain
ON LEAVE OF tPONbr IN FRAME fInvoice /* P.O. Number */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"PONumber","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRefID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRefID wMain
ON LEAVE OF tRefID IN FRAME fInvoice /* Claim Number */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"refID","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendor wMain
ON LEAVE OF tVendor IN FRAME fInvoice /* Vendor Number */
DO:
  if changeMade(self:screen-value,"vendorID","apinv")
   then
    do:
      IsPayableUsed().
      lChanged = true.
      APPLY "RETURN":U TO tVendor.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendor wMain
ON RETURN OF tVendor IN FRAME fInvoice /* Vendor Number */
DO:
  run RefreshVendor in this-procedure (tVendor:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVendorName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendorName wMain
ON LEAVE OF tVendorName IN FRAME fInvoice /* Vendor Name */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"vendorName","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwApproval
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wMain 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:title = pTitle.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* copies the amount from the invoice only if this is the first row */
ON ENTRY OF apinvd.amount IN BROWSE brwDistribution
DO:
  if iSeq = 1 and countRows(temp-table apinvd:default-buffer-handle) = 0
   then self:screen-value = tAmount:screen-value in frame {&frame-name}.
END.

{lib/brw-main-multi.i &browse-list="brwApproval,brwDistribution"}
publish "GetCredentialsID" (output cCurrUserID).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  run Initialize in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddApproval wMain 
PROCEDURE AddApproval :
/*------------------------------------------------------------------------------
@description Add an approval 
------------------------------------------------------------------------------*/
  define variable pInputUserList as character no-undo.
  define variable pOutputUserList as character no-undo.
  define variable cPrelimUserList as character no-undo.
  define variable cUserID as character no-undo.
  define variable cUserName as character no-undo.
  
  do with frame {&frame-name}:
    publish "GetClaimReserveApprover" (input pClaimID,
                                       input cmbRefCategory:screen-value,
                                       input tAmount:input-value,
                                       output cPrelimUserList
                                       ).
  end.
  /* remove the users that are already in the approval list */
  do std-in = 1 to num-entries(cPrelimUserList) / 2:
    assign
      cUserID = entry(std-in * 2,cPrelimUserList)
      cUserName = entry(std-in * 2 - 1,cPrelimUserList)
      .
    if not can-find(first apinva where uid = cUserID)
     then pInputUserList = addDelimiter(pInputUserList,",") + cUserName + "," + cUserID.
  end.
  run sys/userselect.w (pInputUserList,output cPrelimUserList).
  /* get the username for the userid */
  if cPrelimUserList > ""
   then
    do:
      do std-in = 1 to num-entries(cPrelimUserList):
        cUserID = entry(std-in,cPrelimUserList).
        publish "GetSysUserName" (cUserID,output cUserName).
        if cUserName = "NOT FOUND"
         then cUserName = cUserID.
        pOutputUserList = addDelimiter(pOutputUserList,",") + cUserName + "," + cUserID.
      end.
      run AddApprovalToList in this-procedure (pOutputUserList).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddApprovalToList wMain 
PROCEDURE AddApprovalToList :
/*------------------------------------------------------------------------------
@description Sets the approvals for the invoice
@param UserList;character;The users for the approval list

@notes If there is no list passed in, set the first user to qualify to approve
------------------------------------------------------------------------------*/
  define input parameter pUserList as character no-undo.
  
  define variable cUserID as character no-undo.
  define variable cUserName as character no-undo.
  
  do with frame {&frame-name}:
    if pUserList = ""
     then
      do:
        /* set the approval list to be the assigned person */
        publish "GetClaimAssignedTo" (pClaimID, output std-ch).
        /* check if the person is already in the approval list */
        if not can-find(first apinva where lookup(uid,std-ch) > 0)
         then pUserList = std-ch.
         else pUserList = "".
      end.
     
    if pUserList > ""
     then
      do std-in = 1 to num-entries(pUserList) / 2:
        assign
          cUserID = entry(std-in * 2, pUserList)
          cUserName = entry(std-in * 2 - 1, pUserList)
          .
        /* remove the people that haven't approved yet that can not */
        if not can-find(first apinva where apinva.uid = cUserID)
         then
          do:
            iSeq = nextSeq(temp-table apinva:default-buffer-handle).
            create apinva.
            assign
              apinva.apinvID = iInvoice
              apinva.seq = iSeq
              apinva.stat = "P"
              apinva.amount = 0.0
              apinva.username = cUserName
              apinva.uid = cUserID
              apinva.role = "Approver"
              lChanged = true
              .
            release apinva.
          end.
      end.
    {&OPEN-QUERY-brwApproval}
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddDistribution wMain 
PROCEDURE AddDistribution :
/*------------------------------------------------------------------------------
@description Add a distribution
------------------------------------------------------------------------------*/
  /* check if department is added */
  do with frame {&frame-name}:
    if tVendor:screen-value = ""
     then
      do:
        MESSAGE "Add a vendor before adding a distribution" VIEW-AS ALERT-BOX INFO BUTTONS OK.
        return.
      end.
  end.
  
  std-in = nextSeq(temp-table apinvd:default-buffer-handle).
  create apinvd.
  assign
    apinvd.apinvID = iInvoice
    apinvd.seq = std-in
    iSeq = apinvd.seq
    std-in = countRows(temp-table apinvd:default-buffer-handle) - 1
    .
  
  reposition brwDistribution to row std-in.
  std-ha = browse brwDistribution:handle.
  if valid-handle(std-ha)
   then std-ha:insert-row("AFTER").
   
  /* allow users to edit the browse */
  browse brwDistribution:sensitive = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddDistributionToList wMain 
PROCEDURE AddDistributionToList :
/*------------------------------------------------------------------------------
@description Sets the distribution for the invoice
@param UserList;character;The users for the approval list
------------------------------------------------------------------------------*/
  define input parameter pAccountNumber as character no-undo.
  
  define variable pAccountName as character no-undo.
  define variable hBrowse as handle no-undo.
  
  do with frame {&frame-name}:
    /* get the next sequence number */
    std-in = nextSeq(temp-table apinvd:default-buffer-handle).
    if std-in = 1
     then std-de = tAmount:input-value.
     else std-de = 0.
    /* if the account number is blank, then get it based on the referring category */
    if pAccountNumber = ""
     then publish "GetClaimPayableAccount" (pClaimID, cmbRefCategory:screen-value, output pAccountNumber).
    publish "GetAccountName" (pAccountNumber, output pAccountName).
    if can-find(first apinvd where lookup(acct,pAccountNumber) > 0)
     then pAccountNumber = "".
    /* if the account number is blank, don't add the distribution */
    if pAccountNumber > ""
     then
      do:
        create apinvd.
        assign
          apinvd.apinvID = iInvoice
          apinvd.seq = std-in
          apinvd.acct = pAccountNumber
          apinvd.acctname = pAccountName
          apinvd.amount = std-de
          .
      end.
    {&OPEN-QUERY-brwDistribution}
    /* prevent users from editting the browse */
    assign
      hBrowse = browse brwDistribution:handle
      std-ha = getColumn(hBrowse,"acct")
      std-ha:read-only = true
      std-ha = getColumn(hBrowse,"amount")
      std-ha:read-only = true
      .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteApproval wMain 
PROCEDURE DeleteApproval :
/*------------------------------------------------------------------------------
@description Delete an approval
------------------------------------------------------------------------------*/
  if not available apinva
   then return.
   
  delete apinva.
  lChanged = true.
  {&OPEN-QUERY-brwApproval}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteDistribution wMain 
PROCEDURE DeleteDistribution :
/*------------------------------------------------------------------------------
@description Delete a distribution
------------------------------------------------------------------------------*/
  if not available apinvd
   then return.
   
  delete apinvd.
  lChanged = true.
  {&OPEN-QUERY-brwDistribution}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wMain)
  THEN DELETE WIDGET wMain.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tVendor chkInContention tRefID tVendorName tVendorAddress 
          cmbRefCategory cmbCoverage tInvoiceNbr tPONbr tInvoiceDate 
          tDateReceived tAmount tDueDate tApprovalNotes tFile tNotes 
      WITH FRAME fInvoice IN WINDOW wMain.
  ENABLE RECT-36 RECT-38 RECT-39 bVendorLookup bVendorReport tVendor bLock 
         chkInContention tVendorName tVendorAddress bAddApproval brwApproval 
         bDeleteApproval cmbRefCategory cmbCoverage tInvoiceNbr tPONbr 
         tInvoiceDate tDateReceived tAmount tDueDate tApprovalNotes bFileUpload 
         tFile tNotes bAddDistribution brwDistribution bDeleteDistribution 
         bSave bDelete bCancel 
      WITH FRAME fInvoice IN WINDOW wMain.
  {&OPEN-BROWSERS-IN-QUERY-fInvoice}
  VIEW wMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FileUpload wMain 
PROCEDURE FileUpload :
/*------------------------------------------------------------------------------
@description Upload a new file from the users hard drive  
------------------------------------------------------------------------------*/
  define input parameter pFile as character no-undo.

  do with frame {&frame-name}:
    /* only one file per invoice */
    if tFile:screen-value <> ""
     then
      do:
        /* confirmation */
        publish "GetConfirmFileUpload" (output std-lo).
        if std-lo 
         then
          do:
            MESSAGE "The current file will be replaced. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
            if not std-lo 
             then return.
          end.
      end.
    /* open the system dialog */
    if pFile = ""
     then 
      do:
        SYSTEM-DIALOG GET-FILE pFile 
          TITLE   "Attach Invoice to ShareFile..."
          MUST-EXIST 
          USE-FILENAME 
          UPDATE std-lo.
      end.
     else std-lo = true.
    /* update the widget and flag */
    if std-lo
     then 
      assign
        lFileAdded = true
        lChanged = true
        tFile:screen-value = pFile
        menu-item m_Delete_File:sensitive in menu menuFile = true
        .
    /* as the save button is disabled when the invoice is completed */
    /* we need to attach/replace the invoice on the fly */
    if can-find(first apinv where stat = "C")
     then 
      do:
        pSuccess = true.
        /* if there was a document previously */
        for first apinv:
          if not apinv.hasDocument
           then run AttachPayableDocument in hFileDataSrv (apinv.apinvID, pFile, output pSuccess).
           else run ReplacePayableDocument in hFileDataSrv (table apinv, output pSuccess).
        end.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize wMain 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
@description Initialize the dialog by setting the images and getting the invoice data     
------------------------------------------------------------------------------*/
  define variable lIsApproved as logical no-undo initial false.

  define buffer apinv for apinv.
  /* set the save request flag to false */
  assign 
    pSuccess = false
    std-lo = false
    .
  /* get the vendor information if available */
  for first apinv:
    iInvoice = apinv.apinvID.
    if apinv.vendorID > ""
     then run RefreshVendor in this-procedure (apinv.vendorID).
  end.
  /* create a dummy apinvID for the client to use if one was not found */
  if not can-find(first apinv)
   then
    do:
      create apinv.
      assign
        iInvoice = 0
        apinv.apinvID = iInvoice
        apinv.refID = string(pClaimID)
        apinv.refType = "C"
        apinv.dateReceived = today
        .
    end.
  do with frame {&frame-name}:
    /* enable the delete file if necessary */
    menu-item m_Delete_File:sensitive in menu menuFile = can-find(first apinv where hasDocument = true).
    /* The distribution browse */
    bAddDistribution:load-image("images/s-add.bmp").
    bAddDistribution:load-image-insensitive("images/s-add-i.bmp").
    bDeleteDistribution:load-image("images/s-delete.bmp").
    bDeleteDistribution:load-image-insensitive("images/s-delete-i.bmp").
    /* The approval browse */
    bAddApproval:load-image("images/s-add.bmp").
    bAddApproval:load-image-insensitive("images/s-add-i.bmp").
    bDeleteApproval:load-image("images/s-delete.bmp").
    bDeleteApproval:load-image-insensitive("images/s-delete-i.bmp").
    /* The vendor lookup button */
    bVendorLookup:load-image("images/s-lookup.bmp").
    bVendorLookup:load-image-insensitive("images/s-lookup-i.bmp").
    bVendorReport:load-image("images/s-book.bmp").
    bVendorReport:load-image-insensitive("images/s-book-i.bmp").
    /* The lock/unlock button */
    bLock:load-image("images/lock.bmp").
    bLock:load-image-insensitive("images/lock-i.bmp").
    bLock:sensitive = true.
    /* The upload button */
    bFileUpload:load-image-up("images/s-upload.bmp").
    bFileUpload:load-image-insensitive("images/s-upload-i.bmp").
    /* set the fields */
    run SetFields in this-procedure.
    /* hide the uid column for the approval browse */
    std-ha = getColumn(browse brwApproval:handle,"uid").
    std-ha:visible = false.
    /* once all approved, don't allow the user to rename the vendor */
    if can-find(first apinv where stat = "A")
     then
      assign
        tVendorName:read-only = true
        tVendorAddress:read-only = true
        .
    /* if the invoice is already approved or denied, don't let the user change some stuff */
    if can-find(first apinva where stat = "A" or stat = "D")
     then Lock(true).
    /* once the invoice is complete or void, view only */
    if can-find(first apinv where stat = "C" or stat = "V")
     then LockAll().
    SensitizeApproval(false).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MakeClaimLAE wMain 
PROCEDURE MakeClaimLAE :
/*------------------------------------------------------------------------------
@description Changing the type to incidental ignores all approvals if the user
             can approve themselves
@note If the user can self approve the invoice, then it will automatically close
------------------------------------------------------------------------------*/
  /* a blank userlist means that the assigned to person will be the approver */
  if not can-find(first apinva)
   then run AddApprovalToList in this-procedure ("").
  
  if not can-find(first apinvd)
   then run AddDistributionToList in this-procedure ("").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MakeClaimLoss wMain 
PROCEDURE MakeClaimLoss :
/*------------------------------------------------------------------------------
@description Changing the type to loss lets the user choose the policy
------------------------------------------------------------------------------*/
  define input parameter pPolicyID as integer no-undo.
  define variable xEffDate as datetime no-undo.
  define variable xLiabilityAmount as decimal no-undo.
  define variable xGrossPremium as decimal no-undo.
  define variable xFormID as character no-undo.
  define variable xStateID as character no-undo.
  define variable xFileNumber as character no-undo.
  define variable xStatCode as character no-undo.
  define variable xNetPremium as decimal no-undo.
  define variable xRetention as decimal no-undo.
  define variable xCountyID as character no-undo.
  define variable xResidential as logical no-undo.
  define variable xInvoiceDate as date no-undo.

  define variable tMsgStat as character no-undo.
  define variable cList as character no-undo initial "".
  
  DO WITH FRAME {&frame-name}:
    if pPolicyID = 0
     then
       do:
        /* set the claim policy */
        publish "GetClaim" (pClaimID, output table claim).
        publish "GetClaimCoverages" (pClaimID, output table claimcoverage).
        for first claim:
          for each claimcoverage:
            publish "IsPolicyValid" (input pClaimID,
                                     input claimcoverage.coverageID,
                                     input claim.agentID,
                                     output xEffDate,
                                     output xLiabilityAmount,
                                     output xGrossPremium,
                                     output xFormID,
                                     output xStateID,
                                     output xFileNumber,
                                     output xStatCode,
                                     output xNetPremium,
                                     output xRetention,
                                     output xCountyID,
                                     output xResidential,
                                     output xInvoiceDate,
                                     output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                                     output std-ch).
            if tMsgStat = "S"
             then cList = addDelimiter(cList,",") + claimcoverage.coverageID.
          end.
        end.
      end.
     else cList = string(pPolicyID).
    if cList = ""
     then
      do:
        message "Please add a valid policy before making a Loss payment" view-as alert-box error buttons ok.
        if not can-find(first apinvd)
         then run AddDistributionToList in this-procedure ("").
        return.
      end.
    assign
      cmbCoverage:list-items = cList
      cmbCoverage:inner-lines = minimum(num-entries(cList),5)
      cmbCoverage:hidden = false
      cmbCoverage:sensitive = true
      cmbCoverage:screen-value = entry(1,cList)
      .
    IF INDEX(cList,",") = 0
     then cmbCoverage:sensitive = false.
    
    /* Change on 12/12/2016 to have the approver be the claim assigned to user */
    /* set the approval to everyone up to the limit
    std-ch = "".
    publish "GetClaimLossApprover" (input tRefID:input-value, 
                                    input cmbRefCategory:screen-value, 
                                    input tAmount:input-value, 
                                    OUTPUT std-ch).
    if std-ch > "" and not can-find(first apinva)
     then run AddApprovalToList in this-procedure (std-ch).
    /* add the distribution */
    if not can-find(first apinvd)
     then run AddDistributionToList in this-procedure (""). */
     
    /* making the claim an an LAE at this point will assign the invoice */
    /* to the claim assigned to user and add the default account */
    run MakeClaimLAE in this-procedure.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshVendor wMain 
PROCEDURE RefreshVendor :
/*------------------------------------------------------------------------------
@description Refresh the vendor information based on vendor id 
------------------------------------------------------------------------------*/
  define input parameter pVendorNbr as character no-undo.
  
  std-lo = false.
  if pVendorNbr > ""
   then 
    do:
      define buffer vendor for vendor.
      define buffer apinv for apinv.
      publish "GetVendor" (pVendorNbr, output table vendor).
            
      std-lo = false.
      for first vendor no-lock:
        /* modify the screen values */
        do with frame {&frame-name}:
          assign
            cVendorName = vendor.name
            cVendorAddress = vendor.fulladdress
            tVendor:screen-value = vendor.vendorID
            tVendorName:screen-value = cVendorName
            tVendorName:sensitive = false
            tVendorAddress:screen-value = cVendorAddress
            tVendorAddress:sensitive = false
            std-lo = true
            .
          /* if there is an account */
          for first apinv no-lock:
            if apinv.vendorID <> pVendorNbr
             then 
              assign
                apinv.vendorID = vendor.vendorID
                lChanged = true
                .
          end.
        end.
      end.
    end.
    
  /* if no vendor, std-lo is false */
  if not std-lo
   then 
    assign
      tVendorName:sensitive = true
      tVendorName:read-only = false
      tVendorAddress:sensitive = true
      tVendorAddress:read-only = false
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResetApproval wMain 
PROCEDURE ResetApproval :
/*------------------------------------------------------------------------------
@description Resets the approvals for the invoice
------------------------------------------------------------------------------*/
  /* reset the approvals */
  do with frame {&frame-name}:
    for each apinva exclusive-lock:
      assign
        apinva.stat = "P"
        apinva.dateActed = ?
        apinva.amount = 0.0
        apinva.notes = ""
        lChanged = true
        .
    end.
    for first apinv exclusive-lock:
      apinv.stat = "O".
    end.
    SensitizeButtons().
    assign
      tApprovalNotes:screen-value = ""
      .
    {&OPEN-QUERY-brwApproval}
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveInvoice wMain 
PROCEDURE SaveInvoice :
/*------------------------------------------------------------------------------
@description Saves the invoice
------------------------------------------------------------------------------*/
  define output parameter pSaveSuccess as logical no-undo.
  define buffer apinv for apinv.
  
  pSaveSuccess = false.
  /* check if there is a change to the invoice */
  if lChanged
   then
    do with frame {&frame-name}:
      /* Validation */
      run Validation in this-procedure (output std-lo).
      /* save the fields */
      if std-lo
       then 
        for first apinv exclusive-lock:
          assign
            apinv.refID = tRefID:screen-value
            apinv.refSeq = (if cmbCoverage:hidden then 0 else integer(cmbCoverage:screen-value))
            apinv.refCategory = cmbRefCategory:screen-value
            apinv.vendorID = tVendor:screen-value
            apinv.vendorName = tVendorName:screen-value
            apinv.vendorAddress = tVendorAddress:screen-value
            apinv.invoiceNumber = tInvoiceNbr:screen-value
            apinv.amount = tAmount:input-value
            apinv.deptID = ""
            apinv.notes = tNotes:screen-value
            apinv.PONumber = tPONbr:screen-value
            apinv.contention = logical(chkInContention:screen-value)
            apinv.isFileCreated = false
            apinv.isFileEdited = false
            apinv.isFileDeleted = false
            .
          /* if a file got added */
          if lFileAdded and tFile:screen-value <> ""
           then
            assign
              apinv.isFileCreated = not apinv.hasDocument
              apinv.isFileEdited = apinv.hasDocument
              apinv.isFileDeleted = false
              apinv.hasDocument = true
              apinv.documentFilename = tFile:screen-value
              .
          /* if a file got deleted */
          if lFileDeleted
           then
            assign
              apinv.isFileCreated = false
              apinv.isFileEdited = false
              apinv.isFileDeleted = true
              apinv.hasDocument = false
              apinv.documentFilename = ""
              .
          
          /* dates are optional */
          apinv.invoiceDate = tInvoiceDate:input-value.
           
          std-da = date(tDueDate:screen-value) no-error.
          if error-status:error
           then apinv.dueDate = ?.
           else apinv.dueDate = std-da.
           
          std-da = date(tDateReceived:screen-value) no-error.
          if error-status:error
           then apinv.dateReceived = today.
           else apinv.dateReceived = std-da.
          
          if apinv.refCategory = "Unknown"
           then apinv.refCategory = "".
          
          if iInvoice = 0
           then /* new */
            run NewPayableInvoice in hFileDataSrv
                                  (input table apinv,
                                   input table apinva,
                                   input table apinvd,
                                   output iInvoice,
                                   output pSaveSuccess
                                   ).
           else /* modify */
            run ModifyPayableInvoice in hFileDataSrv
                                     (input table apinv,
                                      input table apinva,
                                      input table apinvd,
                                      output pSaveSuccess
                                      ).
          lChanged = not pSaveSuccess.
        end.
    end.
   else pSaveSuccess = true.
  /* change the status */
  pSuccess = pSaveSuccess.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetFields wMain 
PROCEDURE SetFields :
/*------------------------------------------------------------------------------
@description Sets the fields during the initial load
------------------------------------------------------------------------------*/
  define variable cNotes as character no-undo.
  define variable iRefSeq as integer no-undo.
  define variable cCatList as character no-undo.
  define variable cType as character no-undo.
  
  define buffer apinv for apinv.
  define buffer apinva for apinva.
   
  publish "GetCategoryList" (output cCatList).
  publish "GetClaimType"  (pClaimID, output cType).
  
  do with frame {&frame-name}:
    /* set the invoice fields */
    assign
      cmbRefCategory:list-item-pairs = cCatList
      cmbRefCategory:inner-lines = num-entries(cmbRefCategory:list-item-pairs) / 2
      .
    for first apinv:
      assign
        tInvoiceNbr:screen-value = apinv.invoiceNumber
        tVendor:screen-value = apinv.vendorID
        tVendorName:screen-value = apinv.vendorName
        tVendorAddress:screen-value = apinv.vendorAddress
        chkInContention:screen-value = string(apinv.contention)
        tInvoiceDate:screen-value = string(apinv.invoiceDate)
        tAmount:screen-value = string(apinv.amount)
        cmbRefCategory:screen-value = (if apinv.refCategory = "" then "E" else apinv.refCategory)
        cmbRefCategory:screen-value = (if cType = "M" then "E" else cmbRefCategory:screen-value)
        cmbRefCategory:sensitive = (not cType = "M")
        tNotes:screen-value = apinv.notes
        tDueDate:screen-value = string(apinv.dueDate)
        tDateReceived:screen-value = string(apinv.dateReceived)
        tPONbr:screen-value = apinv.PONumber
        tFile:screen-value = (if apinv.hasDocument then getPayableServerPath(apinv.vendorID,apinv.invoicedate) + apinv.documentFilename else "")
        tRefID:screen-value = apinv.refID
        iRefSeq = apinv.refSeq
        .
    end.
    /* set the policy if the type is Loss */
    cmbCoverage:hidden = true.
    case cmbRefCategory:screen-value:
     when "L" then run MakeClaimLoss in this-procedure (iRefSeq).
     when "E" then run MakeClaimLAE in this-procedure.
    end case.
    /* set the approval notes */
    for each apinva, first apinv
       where apinva.apinvID = apinv.apinvID
         and (apinva.stat = "D" or apinva.stat = "A")
          by apinva.dateActed:
       
      assign
        std-ch = string(apinva.dateActed, "99/99/9999 HH:MM:SS") + " " +
                 (if apinva.stat = "D" then "REJECTED" else "APPROVED") + " " +
                 apinva.username + ":" + chr(10) 
        std-ch = std-ch + apinva.notes
        cNotes = addDelimiter(cNotes,chr(10) + chr(10)) + std-ch
        .
    end.
    tApprovalNotes:screen-value = cNotes.
    SensitizeButtons().
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData wMain 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
@description Sorts the data in the browse widgets
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Validation wMain 
PROCEDURE Validation :
/*------------------------------------------------------------------------------
@description Validates the claim payable
------------------------------------------------------------------------------*/
  DEFINE OUTPUT PARAMETER pSuccess AS LOGICAL NO-UNDO.
  DEFINE VARIABLE cMsg AS character NO-UNDO.
  
  define variable lHasDistribution as logical no-undo initial false.
  define variable lHasApproval as logical no-undo initial false.
  
  DEFINE BUFFER apinvd FOR apinvd.

  ASSIGN
    pSuccess = true
    cMsg = ""
    std-de = 0
    std-lo = FALSE
    .
  do with frame {&frame-name}:
    FOR EACH apinvd no-lock:
      ASSIGN
        std-de = std-de + apinvd.amount
        lHasDistribution = true
        .
    END.
    lHasApproval = can-find(first apinva).
    
    /* check for an amount */
    if tAmount:input-value < 0
     then cMsg = cMsg + "The amount cannot be negative~n".
     
    /* check if the vendor name is populated */
    if tVendorName:screen-value = ""
     then cMsg = cMsg + "The vendor name must be provided~n".
    
    /* there must be at least one approver */
    IF countRows(temp-table apinva:default-buffer-handle) = 0
     THEN cMsg = cMsg + "There must be at least one approver~n".
    
    /* check if the distribution amount(s) equal the invoice amount */
    if tAmount:input-value > 0 and lHasDistribution and std-de <> tAmount:input-value
     then cMsg = cMsg + "The Distribution Amount(s) must equal Invoice Amount~n".
     
    /* the following checks are only for loss type */
    if cmbRefCategory:screen-value = "L"
     then
      do:
        /* check if the policy is choosen for a loss category */
        if cmbCoverage:hidden or cmbCoverage:screen-value = ""
         then cMsg = cMsg + "For a Loss payment, there must be a policy selected~n".
         else
          do:
            if cMsg = ""
             then
              do:
                publish "GetClaimPolicyLimit" (cmbCoverage:input-value, output std-de).
                /* if the liability amount is known, make sure the amount of */
                /* the invoice is not more than the liability amount against */
                /* all claims. If the amount is more, than warn the user to  */
                /* continue */
                if std-de > 0 and std-de < tAmount:input-value
                 then MESSAGE "The amount is more than the policy's liability amount. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL UPDATE pSuccess.
              end.
          end.
      end.
    
    
    if cMsg > ""
     then 
      do:
        cMsg = trim(cMsg,"~n").
        MESSAGE cMsg VIEW-AS ALERT-BOX INFO BUTTONS OK.
        pSuccess = false.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION IsPayableUsed wMain 
FUNCTION IsPayableUsed RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cVendor as character no-undo.
  define variable cInvoiceNumber as character no-undo.
  define variable lIsUsed as logical no-undo initial false.
  do with frame {&frame-name}:
    assign
      cVendor = tVendor:screen-value
      cInvoiceNumber = tInvoiceNbr:screen-value
      .
  end.
  
  if cVendor > "" and cInvoiceNumber > ""
   then run IsPayableInvoiceUsed in hFileDataSrv (iInvoice, cVendor, cInvoiceNumber, output lIsUsed).

  if lIsUsed
   then message "There cannot be more than one invoice number for the same vendor ID" view-as alert-box warning buttons ok.

  RETURN lIsUsed.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Lock wMain 
FUNCTION Lock RETURNS LOGICAL
  ( input pLock as logical ) :
/*------------------------------------------------------------------------------
@decsription Locks/Unlocks the widgets 
------------------------------------------------------------------------------*/
  define buffer apinv for apinv.
  
  if not pLock and can-find(first apinva where apinva.stat <> "P")
   then
    do:
      message "Unlocking the invoice will reset the approvals. Continue?" view-as alert-box warning buttons yes-no update lContinue as logical.
      /* reset the approvals */
      if lContinue
       then RUN ResetApproval IN THIS-PROCEDURE.
      /* if the user proceeds, we need pLock to be false */
      pLock = (pLock and lContinue).
      for first apinv exclusive-lock
          where pLock = true:
          
        apinv.stat = "O".
      end.
    end.
  
  do with frame {&frame-name}:
    SensitizeDistribution(not pLock).
    assign
      tAmount:read-only = pLock
      tVendorName:read-only = pLock
      tVendorAddress:read-only = pLock
      cmbRefCategory:sensitive = not pLock
      lLocked = pLock
      .
    
    /* we need to change the image based on if the invoice is locked or unlocked */
    /* if the invoice is locked (pLock = true) then change the image to an unlock */
    if pLock
     then
      do:
        bLock:load-image("images/unlock.bmp").
        bLock:load-image-insensitive("images/unlock-i.bmp").
        bLock:tooltip = "Unlock the invoice".
      end.
     else
      do:
        bLock:load-image("images/lock.bmp").
        bLock:load-image-insensitive("images/lock-i.bmp").
        bLock:tooltip = "Lock the invoice".
      end.
  end.

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION LockAll wMain 
FUNCTION LockAll RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@decsription Locks all the widgets from editing
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.
  do with frame {&frame-name}:
    assign
      /* make texts read only */
      tInvoiceNbr:read-only = true
      tVendor:read-only = true
      tVendorName:read-only = true
      tVendorAddress:read-only = true
      tInvoiceDate:read-only = true
      tAmount:read-only = true
      tNotes:read-only = true
      tDueDate:read-only = true
      tDateReceived:read-only = true
      tPONbr:read-only = true
      /* disable all others */
      cmbRefCategory:sensitive = false
      bAddDistribution:sensitive = false
      bDeleteDistribution:sensitive = false
      bAddApproval:sensitive = false
      bDeleteApproval:sensitive = false
      bVendorLookup:sensitive = false
      bLock:sensitive = false
      bSave:sensitive = false
      chkInContention:sensitive = false
      /* disable distribution browse columns */
      hBrowse = browse brwDistribution:handle
      std-ha = getColumn(hBrowse,"acct")
      std-ha:read-only = true
      std-ha = getColumn(hBrowse,"amount")
      std-ha:read-only = true
      .
  end.
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeApproval wMain 
FUNCTION SensitizeApproval RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&frame-name}:
    assign
      bAddApproval:SENSITIVE = pEnable
      bDeleteApproval:SENSITIVE = pEnable
      .
  END.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeButtons wMain 
FUNCTION SensitizeButtons RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Choose whether to enable/disable the buttons based on state of invoice
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      bSave:sensitive = not can-find(first apinv where stat = "C" or stat = "V")
      bDelete:sensitive = can-find(first apinv where stat = "O" or stat = "D")
      .
  end.
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeDistribution wMain 
FUNCTION SensitizeDistribution RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
@description Enable/Disable the distribution add/delete buttons
------------------------------------------------------------------------------*/
  DO WITH FRAME {&frame-name}:
    assign
      bAddDistribution:SENSITIVE = pEnable
      bDeleteDistribution:SENSITIVE = pEnable
      .
  END.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeFileUpload wMain 
FUNCTION SensitizeFileUpload RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
@description Enables or disables the widgets to upload a file 
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      menu-item m_Delete_File:sensitive in menu menuFile = (pEnable and can-find(first apinv where hasDocument = true))
      bFileUpload:sensitive = pEnable
      tFile:screen-value = ""
      tFile:read-only = not pEnable
      tFile:drop-target = pEnable
      lFileAdded = not pEnable
      .
  end.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

