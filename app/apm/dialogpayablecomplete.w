&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fComplete 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

define input parameter pInvoiceID as integer no-undo.
define input parameter hFileDataSrv as handle no-undo.
define output parameter pSuccess as logical no-undo.

define variable dApprovalDate as datetime no-undo.
{lib/std-def.i}

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fComplete

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAmount tDate chkReport tNotes bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tInvoiceAmount tApprovedAmount tAmount ~
tDate chkReport tNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave 
     LABEL "Post" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 34 BY 1.67
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Posted Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 TOOLTIP "The amount to post"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tApprovedAmount AS DECIMAL FORMAT "$ ->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Approved Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDate AS DATETIME FORMAT "99/99/9999":U INITIAL ? 
     LABEL "Posted Date" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 TOOLTIP "The date that the amount was posted in accounting"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tInvoiceAmount AS DECIMAL FORMAT "$ ->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Invoice Amount" 
     VIEW-AS FILL-IN 
     SIZE 16.8 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE chkReport AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 TOOLTIP "If checked, the posted amount will be reported" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fComplete
     tInvoiceAmount AT ROW 1.48 COL 19 COLON-ALIGNED WIDGET-ID 84
     tApprovedAmount AT ROW 2.67 COL 19 COLON-ALIGNED WIDGET-ID 80
     tAmount AT ROW 3.86 COL 19 COLON-ALIGNED WIDGET-ID 70
     tDate AT ROW 5.05 COL 19 COLON-ALIGNED WIDGET-ID 82
     chkReport AT ROW 6.24 COL 21 WIDGET-ID 86
     tNotes AT ROW 7.19 COL 21 NO-LABEL WIDGET-ID 72
     bSave AT ROW 9.33 COL 14
     bCancel AT ROW 9.33 COL 31
     "Notes:" VIEW-AS TEXT
          SIZE 6.2 BY .91 AT ROW 7.19 COL 14.4 WIDGET-ID 74
     "Report Amount:" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 6.33 COL 5.4 WIDGET-ID 88
     SPACE(36.39) SKIP(4.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Post Invoice"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fComplete
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fComplete:SCROLLABLE       = FALSE
       FRAME fComplete:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tApprovedAmount IN FRAME fComplete
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tInvoiceAmount IN FRAME fComplete
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fComplete fComplete
ON WINDOW-CLOSE OF FRAME fComplete /* Post Invoice */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fComplete
ON CHOOSE OF bCancel IN FRAME fComplete /* Cancel */
DO:
  pSuccess = false.
  APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fComplete
ON CHOOSE OF bSave IN FRAME fComplete /* Post */
DO:
  do with frame {&frame-name}:
    /* check if the posted month matches the approval month */
    if month(dApprovalDate) <> month(tDate:input-value)
     then
      do:
        message "The posted month and the approval month do not match. Continue?" view-as alert-box question buttons yes-no update std-lo.
        if not std-lo
         then
          do:
            apply "ENTRY" to tDate.
            return.
          end.
      end.
    /* get the amounts */
    define variable dPostedAmount as decimal no-undo.
    define variable dInvoiceAmount as decimal no-undo.
    define variable dApprovedAmount as decimal no-undo.
    assign
      dPostedAmount = tAmount:input-value
      dInvoiceAmount = tInvoiceAmount:input-value
      dApprovedAmount = tApprovedAmount:input-value
      pSuccess = false
      std-lo = true
      .
    /* if any of the amounts the amounts differ */
    if dPostedAmount <> dInvoiceAmount or dPostedAmount <> dApprovedAmount
     then
      do:
        /* since both amounts differ from the posted amount */
        /* the amount that is different is not the one that is equal */
        /* so if the original amount is equal, the approved amount must be different */
        case dPostedAmount:
         when dInvoiceAmount then std-ch = "the approved amount".
         when dApprovedAmount then std-ch = "the invoice amount".
         otherwise std-ch = "both the invoice and approved amounts".
        end case.
        MESSAGE "The posted amount is different from " + std-ch + ". Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO TITLE "Confimation"  UPDATE std-lo.
      end.
      
    if std-lo
     then run CompletePayableInvoice in hFileDataSrv
                                        (input pInvoiceID,
                                         input dPostedAmount,
                                         input tDate:input-value,
                                         input tNotes:input-value,
                                         output pSuccess
                                         ).
    if pSuccess
     then APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fComplete 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN initialize in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fComplete  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fComplete.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fComplete  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tInvoiceAmount tApprovedAmount tAmount tDate chkReport tNotes 
      WITH FRAME fComplete.
  ENABLE tAmount tDate chkReport tNotes bSave bCancel 
      WITH FRAME fComplete.
  VIEW FRAME fComplete.
  {&OPEN-BROWSERS-IN-QUERY-fComplete}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialize fComplete 
PROCEDURE initialize :
/*------------------------------------------------------------------------------
@description Initialize the dialog by setting the fields
------------------------------------------------------------------------------*/
  define variable dInvoiceAmount as decimal no-undo.
  define variable dApprovedAmount as decimal no-undo.
  define variable cRefType as character no-undo.
  run GetPayableInvoiceApprovedAmount in hFileDataSrv (pInvoiceID, output dApprovedAmount).
  run GetPayableInvoiceAmount in hFileDataSrv (pInvoiceID, output dInvoiceAmount).
  run GetPayableInvoiceApprovedDate in hFileDataSrv (pInvoiceID, output dApprovalDate).
  do with frame {&frame-name}:
    assign
      tApprovedAmount:screen-value = string(dApprovedAmount)
      tInvoiceAmount:screen-value = string(dInvoiceAmount)
      tAmount:screen-value = string(dApprovedAmount)
      tDate:screen-value = string(date(dApprovalDate))
      chkReport:screen-value = "yes"
      .
    run GetPayableInvoiceType in hFileDataSrv (pInvoiceID, output cRefType).
    case cRefType:
     when "C" then std-ch = " to calculate the Claims Ratio".
     otherwise std-ch = "".
    end case.
    chkReport:tooltip = chkReport:tooltip + std-ch.
    
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

