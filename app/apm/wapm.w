&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*
@name wapy.w
@description main startup window for the Accounts PaYable module
@author D.Sinclair
@created 10/11/2016
@Modified
Date         Name         Description
07.31.2023   SB           Modified to get open active accounting period for posting filetask.
08.05.2023   SK           Modified to open Topicexpenses
08.28.2023   SK           Modified to open postedvendortasks
*/


CREATE WIDGET-POOL.

define variable tConfirmExit as logical no-undo.

/* Handles to the Reference windows */
define variable hVendor as handle.
define variable hAgent as handle.
define variable hAccount as handle.
define variable hPeriod as handle.
define variable hState as handle.

define variable dVendorColumnWidth as decimal no-undo.
define variable dApprovalColumnWidth as decimal no-undo.
define variable dFilterWidth as decimal no-undo.
define variable dEntityRect as decimal no-undo.
define variable dParamPos as decimal no-undo.

define variable dListLabelColumn as decimal no-undo.
define variable dListColumn as decimal no-undo.
define variable dEntityColumn as decimal no-undo.
define variable dButtonNewColumn as decimal no-undo.

{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/do-wait.i}
{lib/brw-multi-def.i}
{lib/get-reserve-type.i}
{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/arinv.i}
{tt/artrx.i}
{tt/apminvoice.i}
{tt/list.i}
{tt/listentity.i}
{tt/listfield.i}
{tt/listfilter.i}
{tt/openwindow.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES apminvoice list

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData apminvoice.invoiceType apminvoice.hasDocument apminvoice.contention apminvoice.refDescription apminvoice.refID apminvoice.refCategory apminvoice.vendorID apminvoice.vendorName apminvoice.invoiceNumber apminvoice.invoiceDate apminvoice.recDate apminvoice.approval apminvoice.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH apminvoice
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH apminvoice.
&Scoped-define TABLES-IN-QUERY-brwData apminvoice
&Scoped-define FIRST-TABLE-IN-QUERY-brwData apminvoice


/* Definitions for BROWSE brwList                                       */
&Scoped-define FIELDS-IN-QUERY-brwList list.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwList   
&Scoped-define SELF-NAME brwList
&Scoped-define QUERY-STRING-brwList FOR EACH list by list.displayName
&Scoped-define OPEN-QUERY-brwList OPEN QUERY {&SELF-NAME} FOR EACH list by list.displayName.
&Scoped-define TABLES-IN-QUERY-brwList list
&Scoped-define FIRST-TABLE-IN-QUERY-brwList list


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwData}~
    ~{&OPEN-QUERY-brwList}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rFilter rLists bSearchGo bSearchClear ~
tSearchVendor tSearchStart tSearchEnd tSearchMine cmbEntity bNewList ~
brwData brwList tListLabel tStartDateLabel tEndDateLabel tEntityLabel 
&Scoped-Define DISPLAYED-OBJECTS tSearchVendor tSearchStart tSearchEnd ~
tSearchMine cmbEntity tListLabel tStartDateLabel tEndDateLabel tEntityLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Module_Configure LABEL "Configure"      ACCELERATOR "CTRL-O"
       RULE
       MENU-ITEM m_Module_About LABEL "About"          ACCELERATOR "CTRL-A"
       RULE
       MENU-ITEM m_Module_Quit  LABEL "Quit"           ACCELERATOR "CTRL-Q".

DEFINE SUB-MENU m_Invoice 
       MENU-ITEM m_Invoice_New_General_Payable LABEL "Add New General Payable"
              DISABLED
       MENU-ITEM m_Invoice_New_Claims_Payable LABEL "Add New Claims Payable"
       RULE
       MENU-ITEM m_Invoice_New_General_Receivable LABEL "Add New General Receivable"
              DISABLED
       MENU-ITEM m_Invoice_New_Claims_Receivable LABEL "Add New Claims Receivable"
       RULE
       MENU-ITEM m_Invoice_Delete LABEL "Delete"        
              DISABLED
       MENU-ITEM m_Invoice_Modify LABEL "Modify"         ACCELERATOR "CTRL-U"
              DISABLED
       MENU-ITEM m_Invoice_Approve_Deny LABEL "Approve/Deny"  
              DISABLED.

DEFINE SUB-MENU m_References 
       MENU-ITEM m_References_Accounts LABEL "Accounts"      
       MENU-ITEM m_References_Agents LABEL "Agents"        
       MENU-ITEM m_References_Periods LABEL "Periods"       
       MENU-ITEM m_References_States LABEL "States"        
       MENU-ITEM m_References_Vendors LABEL "Vendors"       
       MENU-ITEM m_References_Destination LABEL "Destination"   .

DEFINE SUB-MENU m_Views 
       MENU-ITEM m_Views_Approved_Invoices LABEL "Approved Invoices" ACCELERATOR "CTRL-P"
       MENU-ITEM m_Views_Completed_Invoices LABEL "Completed Invoices" ACCELERATOR "CTRL-C"
       MENU-ITEM m_Views_Voided_Invoices LABEL "Voided Invoices" ACCELERATOR "CTRL-V"
       MENU-ITEM m_Views_Waived_Invoices LABEL "Waived Invoices" ACCELERATOR "CTRL-W".

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_Reports_Payables_By_Vendor LABEL "Vendor/Payee Payables"
       RULE
       MENU-ITEM m_Claim_Costs_Incurred_Browse LABEL "Claim Costs Incurred"
       MENU-ITEM m_Reports_Open_Reserve_Adjustme LABEL "Open (Unapproved) Reserve Adjustments"
       RULE
       MENU-ITEM m_Report_Agent_Statement LABEL "Agent Statement".

DEFINE SUB-MENU m_Vouchers 
       MENU-ITEM m_Post_File_Tasks LABEL "Post Vendor Tasks"
       MENU-ITEM m_Posted_File_Tasks LABEL "Posted Vouchers"
       RULE
       MENU-ITEM m_Task_State_Setup LABEL "State Tasks Setup"
       .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_Invoice      LABEL "Invoice"       
       SUB-MENU  m_References   LABEL "References"    
       SUB-MENU  m_Views        LABEL "Views"         
       SUB-MENU  m_Reports      LABEL "Reports"       
       SUB-MENU  m_Vouchers     LABEL "Vouchers"      .

DEFINE MENU POPUP-MENU-brwData 
       MENU-ITEM m_Popup_Delete LABEL "Delete Invoice"
              DISABLED
       MENU-ITEM m_Popup_Approve_Deny LABEL "Approve/Deny Invoice"
              DISABLED
       RULE
       MENU-ITEM m_View_Document LABEL "View Document" 
              DISABLED
       MENU-ITEM m_View_Check_Request LABEL "Create Check Request"
       MENU-ITEM m_Attach_Document LABEL "Attach Document"
       MENU-ITEM m_Delete_Document LABEL "Delete Document"
              DISABLED.

DEFINE MENU POPUP-MENU-brwList 
       MENU-ITEM m_List_Create_New_QL LABEL "Create New Quick List"
       MENU-ITEM m_List_Modify_QL LABEL "Modify Quick List"
       MENU-ITEM m_List_Delete_QL LABEL "Delete Quick List"
       RULE
       MENU-ITEM m_List_Share_QL LABEL "Share Quick List".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDeleteList  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected Quick List".

DEFINE BUTTON bModifyList  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected Quick List".

DEFINE BUTTON bNewList  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new Quick List".

DEFINE BUTTON bOpenList  NO-FOCUS
     LABEL "Run" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected Quick List".

DEFINE BUTTON bPrint 
     LABEL "Print" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bSearchClear 
     LABEL "CLR" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear the filters".

DEFINE BUTTON bSearchExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export the data to a CSV File".

DEFINE BUTTON bSearchGo 
     LABEL "GO" 
     SIZE 7.2 BY 1.71 TOOLTIP "Display the unposted payables based on the filters".

DEFINE BUTTON bShareList  NO-FOCUS
     LABEL "Share" 
     SIZE 7.2 BY 1.71 TOOLTIP "Share the selected Quick List".

DEFINE VARIABLE cmbEntity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 27.6 BY 1 NO-UNDO.

DEFINE VARIABLE tEndDateLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Ending Invoice Date:" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tEntityLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Entity:" 
      VIEW-AS TEXT 
     SIZE 5.8 BY .62 NO-UNDO.

DEFINE VARIABLE tListLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Quick Lists" 
      VIEW-AS TEXT 
     SIZE 11 BY .62 NO-UNDO.

DEFINE VARIABLE tSearchEnd AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Leave blank for end-of-time"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tSearchStart AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Leave blank (type ?) for beginning-of-time"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tSearchVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor" 
     VIEW-AS FILL-IN 
     SIZE 30.2 BY 1 NO-UNDO.

DEFINE VARIABLE tStartDateLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Starting Invoice Date:" 
      VIEW-AS TEXT 
     SIZE 21 BY .62 NO-UNDO.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 179 BY 2.05.

DEFINE RECTANGLE rLists
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.2 BY 4.

DEFINE VARIABLE tSearchMine AS LOGICAL INITIAL no 
     LABEL "Show Only My Approvals" 
     VIEW-AS TOGGLE-BOX
     SIZE 28 BY .81 TOOLTIP "Checking this box limits the display to unposted payables to be approved by you" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      apminvoice SCROLLING.

DEFINE QUERY brwList FOR 
      list SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      apminvoice.invoiceType column-label "Invoice!Type" format "x(20)" width 12
apminvoice.hasDocument column-label "Has!Doc" view-as toggle-box
apminvoice.contention column-label "In!Cont." view-as toggle-box
apminvoice.refDescription label "Type" format "x(20)" width 12
apminvoice.refID label "ID" format "x(20)" width 10
apminvoice.refCategory label "Category" format "x(20)" width 20
apminvoice.vendorID label "Vendor ID" format "x(20)" width 12
apminvoice.vendorName label "Vendor Name" format "x(100)" width 20
apminvoice.invoiceNumber column-label "Invoice!Number" format "x(30)" width 14
apminvoice.invoiceDate column-label "Invoice!Date" format "99/99/99" width 13
apminvoice.recDate column-label "Received!Date" format "99/99/99" width 13
apminvoice.approval column-label "Pending!Approval" format "x(200)" width 16
apminvoice.amount column-label "Amount" format "->,>>>,>>9.99" width 13
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 179 BY 18.48
         TITLE "Open Invoices" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwList C-Win _FREEFORM
  QUERY brwList DISPLAY
      list.displayName column-label "Report!Name" format "x(100)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 38.2 BY 16.48
         TITLE "Quick Lists" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bDeleteList AT ROW 3.43 COL 197.2 WIDGET-ID 278 NO-TAB-STOP 
     bSearchGo AT ROW 1.48 COL 150.6 WIDGET-ID 10
     bSearchClear AT ROW 1.48 COL 158 WIDGET-ID 12
     bSearchExport AT ROW 1.48 COL 165.4 WIDGET-ID 18
     bPrint AT ROW 1.48 COL 172.8 WIDGET-ID 288
     tSearchVendor AT ROW 1.86 COL 10.8 COLON-ALIGNED WIDGET-ID 2
     tSearchStart AT ROW 1.91 COL 65 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     tSearchEnd AT ROW 1.91 COL 102.2 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     tSearchMine AT ROW 2 COL 121.6 WIDGET-ID 4
     cmbEntity AT ROW 2 COL 189.2 COLON-ALIGNED NO-LABEL WIDGET-ID 260
     bNewList AT ROW 3.43 COL 182.4 WIDGET-ID 44 NO-TAB-STOP 
     brwData AT ROW 3.52 COL 2 WIDGET-ID 200
     bModifyList AT ROW 3.43 COL 189.8 WIDGET-ID 276 NO-TAB-STOP 
     brwList AT ROW 5.52 COL 181.8 WIDGET-ID 400
     bOpenList AT ROW 3.43 COL 212 WIDGET-ID 282 NO-TAB-STOP 
     bShareList AT ROW 3.43 COL 204.6 WIDGET-ID 280 NO-TAB-STOP 
     tListLabel AT ROW 1.1 COL 180.8 COLON-ALIGNED NO-LABEL WIDGET-ID 266
     tStartDateLabel AT ROW 2.1 COL 43.8 COLON-ALIGNED NO-LABEL WIDGET-ID 290
     tEndDateLabel AT ROW 2.1 COL 81.6 COLON-ALIGNED NO-LABEL WIDGET-ID 292
     tEntityLabel AT ROW 2.24 COL 182.4 COLON-ALIGNED NO-LABEL WIDGET-ID 286
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.1 COL 3 WIDGET-ID 16
     rFilter AT ROW 1.33 COL 2 WIDGET-ID 14
     rLists AT ROW 1.33 COL 181.8 WIDGET-ID 20
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 219.8 BY 21.24
         DEFAULT-BUTTON bSearchGo WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Accounts Payable"
         HEIGHT             = 21.24
         WIDTH              = 219.8
         MAX-HEIGHT         = 46.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 46.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData bNewList DEFAULT-FRAME */
/* BROWSE-TAB brwList bModifyList DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bDeleteList IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModifyList IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpenList IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrint IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwData:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwData:HANDLE
       brwData:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwList:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwList:HANDLE.

/* SETTINGS FOR BUTTON bSearchExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bShareList IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apminvoice.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwList
/* Query rebuild information for BROWSE brwList
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH list by list.displayName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwList */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Accounts Payable */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Accounts Payable */
DO:
  publish "GetConfirmExit" (output tConfirmExit).
  
  if tConfirmExit = yes then
  do:
    std-lo = no.
    message "Are you sure you want to exit?"
        view-as alert-box question buttons yes-no update std-lo.
    if not (std-lo = yes) then
    return no-apply.
  end.
  
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  run closeWindow in this-procedure.
  apply "CLOSE" to this-procedure.
  publish "ExitApplication".
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Accounts Payable */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteList C-Win
ON CHOOSE OF bDeleteList IN FRAME DEFAULT-FRAME /* Delete */
DO:
 run DeleteSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyList C-Win
ON CHOOSE OF bModifyList IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run OpenSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewList C-Win
ON CHOOSE OF bNewList IN FRAME DEFAULT-FRAME /* New */
DO:
  run NewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpenList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpenList C-Win
ON CHOOSE OF bOpenList IN FRAME DEFAULT-FRAME /* Run */
DO:
  run ViewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME DEFAULT-FRAME /* Print */
DO:
  if not available apminvoice
   then return.
   
  run ViewCheckRequest in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME DEFAULT-FRAME /* Open Invoices */
DO:
  if not available apminvoice
   then return.
   
  run ModifyInvoice in this-procedure (apminvoice.apinvID, apminvoice.invoiceType, (apminvoice.refType <> "C")).
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME DEFAULT-FRAME /* Open Invoices */
DO:
  {lib/brw-rowDisplay-multi.i}
  /* color the stat if denied */
  if apminvoice.stat = "D"
   then
    for first brw where brw.name = self:name:
      do std-in = 1 to num-entries(brw.colHandleList):
        colHandle = handle(entry(std-in, brw.colHandleList)).
        if valid-handle(colHandle)
         then 
          assign
            colHandle:bgcolor = 12
            colHandle:fgcolor = 15
            .
      end.
    end.
  /* change the category and the invoice type */
  case apminvoice.invoiceType:
   when "P" then 
    assign
      apminvoice.refCategory:screen-value in browse brwData = getReserveDesc(apminvoice.refCategory)
      apminvoice.invoiceType:screen-value in browse brwData = "Payable"
      .
   when "R" then
    do:
      std-ch = "".
      publish "GetReceivableCategoryDesc" (apminvoice.refCategory, output std-ch).
      apminvoice.refCategory:screen-value in browse brwData = std-ch.
      apminvoice.invoiceType:screen-value in browse brwData = "Receivable".
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME DEFAULT-FRAME /* Open Invoices */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME DEFAULT-FRAME /* Open Invoices */
DO:
  if not available apminvoice
   then return.
   
  /* disable/enable buttons based on answers */
  assign
    /* popup menu options */
    menu-item m_Popup_Delete:sensitive in menu POPUP-MENU-brwData = (apminvoice.stat = "O" or apminvoice.stat = "D")
    menu-item m_Popup_Approve_Deny:sensitive in menu POPUP-MENU-brwData = (apminvoice.stat = "O")
    menu-item m_View_Document:sensitive in menu POPUP-MENU-brwData = apminvoice.hasDocument
    menu-item m_Delete_Document:sensitive in menu POPUP-MENU-brwData = apminvoice.hasDocument
    /* invoice menu choices */
    menu-item m_Invoice_Delete:sensitive in menu m_Invoice = menu-item m_Popup_Delete:sensitive in menu POPUP-MENU-brwData
    menu-item m_Invoice_Modify:sensitive in menu m_Invoice = true
    menu-item m_Invoice_Approve_Deny:sensitive in menu m_Invoice = menu-item m_Popup_Approve_Deny:sensitive in menu POPUP-MENU-brwData
    .
  if apminvoice.vendorID <> ""
   then publish "SetCurrentValue" ("VendorID", apminvoice.vendorID).
   
  if apminvoice.hasDocument
   then menu-item m_Attach_Document:label in menu POPUP-MENU-brwData = "Re-Attach Document".
   else menu-item m_Attach_Document:label in menu POPUP-MENU-brwData = "Attach Document".
   
  case apminvoice.invoiceType:
   when "P" then
    assign
      menu-item m_Invoice_Approve_Deny:label in menu m_Invoice = "Approve/Deny"
      menu-item m_Popup_Approve_Deny:label in menu POPUP-MENU-brwData = "Approve/Deny Invoice"
      menu-item m_View_Check_Request:label in menu POPUP-MENU-brwData = "Create Check Request Document"
      .
   when "R" then
    assign
      menu-item m_Invoice_Approve_Deny:label in menu m_Invoice = "Finalize"
      menu-item m_Popup_Approve_Deny:label in menu POPUP-MENU-brwData = "Finalize Invoice"
      menu-item m_View_Check_Request:label in menu POPUP-MENU-brwData = "Create Invoice Document"
      .
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwList
&Scoped-define SELF-NAME brwList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON DEFAULT-ACTION OF brwList IN FRAME DEFAULT-FRAME /* Quick Lists */
DO:
  run ViewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON ROW-DISPLAY OF brwList IN FRAME DEFAULT-FRAME /* Quick Lists */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON START-SEARCH OF brwList IN FRAME DEFAULT-FRAME /* Quick Lists */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON VALUE-CHANGED OF brwList IN FRAME DEFAULT-FRAME /* Quick Lists */
DO:
  /* when the row is highlighted, display the name in the tooltip */
  if available list
   then
    do:
      /* determine if the column is big enough for the value */
      assign
        std-ha = getColumn(self:handle, "displayName")
        std-lo = (valid-handle(std-ha) and std-ha:width-chars < length(std-ha:screen-value))
        self:tooltip = (if std-lo then std-ha:screen-value else "")
        .
    end.
    
  std-lo = available list.
  assign
    bModifyList:sensitive in frame {&frame-name} = std-lo
    bOpenList:sensitive in frame {&frame-name} = std-lo
    bDeleteList:sensitive in frame {&frame-name} = std-lo
    bShareList:sensitive in frame {&frame-name} = std-lo
    menu-item m_List_Modify_QL:sensitive in menu POPUP-MENU-brwList = std-lo
    menu-item m_List_Delete_QL:sensitive in menu POPUP-MENU-brwList = std-lo
    menu-item m_List_Share_QL:sensitive in menu POPUP-MENU-brwList = std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearchClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearchClear C-Win
ON CHOOSE OF bSearchClear IN FRAME DEFAULT-FRAME /* CLR */
DO:
  do with frame {&frame-name}:
    assign
      tSearchVendor:screen-value = ""
      tSearchStart:screen-value = ""
      tSearchEnd:screen-value = ""
      tSearchMine:checked = false
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearchExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearchExport C-Win
ON CHOOSE OF bSearchExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  doWait(true).
  run exportData in this-procedure.
  doWait(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearchGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearchGo C-Win
ON CHOOSE OF bSearchGo IN FRAME DEFAULT-FRAME /* GO */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bShareList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bShareList C-Win
ON CHOOSE OF bShareList IN FRAME DEFAULT-FRAME /* Share */
DO:
  run ShareSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbEntity C-Win
ON VALUE-CHANGED OF cmbEntity IN FRAME DEFAULT-FRAME
DO:
  run GetSavedLists in this-procedure (self:screen-value).
  apply "VALUE-CHANGED" to brwList.
  publish "SetCurrentValue" ("Entity", self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Attach_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Attach_Document C-Win
ON CHOOSE OF MENU-ITEM m_Attach_Document /* Attach Document */
DO:
  if not available apminvoice
   then return.
  
  run AttachDocument in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Claim_Costs_Incurred_Browse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Claim_Costs_Incurred_Browse C-Win
ON CHOOSE OF MENU-ITEM m_Claim_Costs_Incurred_Browse /* Claim Costs Incurred */
DO:
  run rpt/claimscostincurred.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Document C-Win
ON CHOOSE OF MENU-ITEM m_Delete_Document /* Delete Document */
DO:
  if not available apminvoice
   then return.
  
  run DeleteDocument in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Approve_Deny
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Approve_Deny C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_Approve_Deny /* Approve/Deny */
DO:
  if not available apminvoice
   then return.
   
  case apminvoice.invoiceType:
   when "P" then run ApproveInvoice in this-procedure (apminvoice.apinvID).
   when "R" then run CompleteInvoice in this-procedure (apminvoice.apinvID, "R").
  end case.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Delete C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_Delete /* Delete */
DO:
  if not available apminvoice
   then return.
  
  run DeleteInvoice in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Modify C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_Modify /* Modify */
DO:
  if not available apminvoice
   then return.
   
  run ModifyInvoice in this-procedure (apminvoice.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_New_Claims_Payable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_New_Claims_Payable C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_New_Claims_Payable /* Add New Claims Payable */
DO:
  run NewPayable in this-procedure (false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_New_Claims_Receivable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_New_Claims_Receivable C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_New_Claims_Receivable /* Add New Claims Receivable */
DO:
  run NewReceivable in this-procedure (false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_New_General_Payable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_New_General_Payable C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_New_General_Payable /* Add New General Payable */
DO:
  run NewPayable in this-procedure (true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_New_General_Receivable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_New_General_Receivable C-Win
ON CHOOSE OF MENU-ITEM m_Invoice_New_General_Receivable /* Add New General Receivable */
DO:
  run NewReceivable in this-procedure (true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Create_New_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Create_New_QL C-Win
ON CHOOSE OF MENU-ITEM m_List_Create_New_QL /* Create New Quick List */
DO:
  run NewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Delete_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Delete_QL C-Win
ON CHOOSE OF MENU-ITEM m_List_Delete_QL /* Delete Quick List */
DO:
  run DeleteSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Modify_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Modify_QL C-Win
ON CHOOSE OF MENU-ITEM m_List_Modify_QL /* Modify Quick List */
DO:
  run OpenSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Share_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Share_QL C-Win
ON CHOOSE OF MENU-ITEM m_List_Share_QL /* Share Quick List */
DO:
  run ShareSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module_About C-Win
ON CHOOSE OF MENU-ITEM m_Module_About /* About */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Module_Configure /* Configure */
DO:
   run dialogapmconfig.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module_Quit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module_Quit C-Win
ON CHOOSE OF MENU-ITEM m_Module_Quit /* Quit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Approve_Deny
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Approve_Deny C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Approve_Deny /* Approve/Deny Invoice */
DO:
  if not available apminvoice
   then return.
   
  run ApproveInvoice in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Delete C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Delete /* Delete Invoice */
DO:
  if not available apminvoice
   then return.
   
  run DeleteInvoice in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Posted_File_Tasks
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Posted_File_Tasks C-Win
ON CHOOSE OF MENU-ITEM m_Posted_File_Tasks /* Posted Vendor Tasks */
DO:
  run ActionWindowShow in this-procedure("wpostedvendortasks.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Post_File_Tasks
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Post_File_Tasks C-Win
ON CHOOSE OF MENU-ITEM m_Post_File_Tasks /* Post Vendor Tasks */
DO:
  run ActionWindowShow in this-procedure("wunpostedvendortasks.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Accounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Accounts C-Win
ON CHOOSE OF MENU-ITEM m_References_Accounts /* Accounts */
DO:
  run ActionWindowShow in this-procedure ("sys/waccounts.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Agents C-Win
ON CHOOSE OF MENU-ITEM m_References_Agents /* Agents */
DO:
  run ActionWindowShow in this-procedure("referenceagent.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Destination
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Destination C-Win
ON CHOOSE OF MENU-ITEM m_References_Destination /* Destination */
DO:
  run ActionWindowShow in this-procedure ("referencesysdest.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Periods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Periods C-Win
ON CHOOSE OF MENU-ITEM m_References_Periods /* Periods */
DO:
  run ActionWindowShow in this-procedure ("referenceperiodaccounting.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_States
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_States C-Win
ON CHOOSE OF MENU-ITEM m_References_States /* States */
DO:
  run ActionWindowShow in this-procedure ("referencestate.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References_Vendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References_Vendors C-Win
ON CHOOSE OF MENU-ITEM m_References_Vendors /* Vendors */
DO:
  run ActionWindowShow in this-procedure ("sys/wvendor.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reports_Open_Reserve_Adjustme
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reports_Open_Reserve_Adjustme C-Win
ON CHOOSE OF MENU-ITEM m_Reports_Open_Reserve_Adjustme /* Open (Unapproved) Reserve Adjustments */
DO:
  run wapm04-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reports_Payables_By_Vendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reports_Payables_By_Vendor C-Win
ON CHOOSE OF MENU-ITEM m_Reports_Payables_By_Vendor /* Vendor/Payee Payables */
DO:
  run wapm02-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Agent_Statement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Agent_Statement C-Win
ON CHOOSE OF MENU-ITEM m_Report_Agent_Statement /* Agent Statement */
DO:
  run wapm05-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Task_State_Setup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Task_State_Setup C-Win
ON CHOOSE OF MENU-ITEM m_Task_State_Setup /* State Task Setup */
DO:
  run ActionWindowShow in this-procedure("wtopicexpense.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Views_Approved_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Views_Approved_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Views_Approved_Invoices /* Approved Invoices */
DO:
  run ActionWindowShow in this-procedure("wviewapprovedinvoices.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Views_Completed_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Views_Completed_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Views_Completed_Invoices /* Completed Invoices */
DO:
  run ActionWindowShow in this-procedure("wviewcompletedinvoices.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Views_Voided_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Views_Voided_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Views_Voided_Invoices /* Voided Invoices */
DO:
  run ActionWindowShow in this-procedure("wviewvoidedinvoices.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Views_Waived_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Views_Waived_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Views_Waived_Invoices /* Waived Invoices */
DO:
  run ActionWindowShow in this-procedure("wviewwaivedinvoices.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Check_Request
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Check_Request C-Win
ON CHOOSE OF MENU-ITEM m_View_Check_Request /* Create Check Request */
DO:
  apply "CHOOSE" to bPrint in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Document C-Win
ON CHOOSE OF MENU-ITEM m_View_Document /* View Document */
DO:
  if not available apminvoice
   then return.
   
  run ViewDocument in this-procedure (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}
{lib/brw-main-multi.i &browse-list="brwData,brwList"}
{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

/* subscribes for client dialogs */
subscribe to "DeleteInvoiceDialog" anywhere run-procedure "DeleteInvoice".
subscribe to "ModifyInvoiceDialog" anywhere run-procedure "ModifyInvoice".
subscribe to "ApproveInvoiceDialog" anywhere run-procedure "ApproveInvoice".
subscribe to "CompleteInvoiceDialog" anywhere run-procedure "CompleteInvoice".
subscribe to "VoidTransactionDialog" anywhere run-procedure "VoidTransaction".
subscribe to "VoidDetailsPayableDialog" anywhere run-procedure "VoidDetailsPayable".
subscribe to "WaiveInvoiceDialog" anywhere run-procedure "WaiveInvoice".
/* subscribes for the documents */
subscribe to "ViewCheckRequestClient" anywhere run-procedure "ViewCheckRequest".
subscribe to "AttachDocumentClient" anywhere run-procedure "AttachDocument".
subscribe to "DeleteDocumentClient" anywhere run-procedure "DeleteDocument".
subscribe to "ViewDocumentClient" anywhere run-procedure "ViewDocument".
subscribe to "SendPayableClient" anywhere run-procedure "SendPayable".
/* used to reload data if a change was made */
subscribe to "ClaimAccountingDataChanged" anywhere.
/* used to control the various windows in the application */
subscribe to "ActionWindowShow" anywhere.
/* subscribes for the list builder/references */
subscribe to "InvoiceSelected" anywhere.
subscribe to "VendorSelected" anywhere.

subscribe to "ActionWindowForAgent" anywhere.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run Initialize in this-procedure.
  
  status input "".
  status default "".
  
  /* set the original column and width */
  assign
    dFilterWidth = rFilter:width-pixels
    dListColumn = browse brwList:x
    dEntityColumn = cmbEntity:x
    dEntityRect = rLists:x
    dParamPos = tStartDateLabel:x
    dListLabelColumn = tListLabel:x
    dButtonNewColumn = bNewList:x
    .
  
  std-ha = getColumn({&browse-name}:handle, "vendorName").
  IF VALID-HANDLE(std-ha)
   THEN dVendorColumnWidth = std-ha:width * session:pixels-per-column.
  
  std-ha = getColumn({&browse-name}:handle, "approval").
  IF VALID-HANDLE(std-ha)
   THEN dApprovalColumnWidth = std-ha:width * session:pixels-per-column.
   
  run WindowResized in this-procedure.
  
  std-lo = false.
  publish "GetAutoView" (output std-lo).
  if std-lo
   then apply "CHOOSE" to bSearchGo.
  
  {&window-name}:window-state = 3.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindow C-Win 
PROCEDURE ActionWindow :
/*------------------------------------------------------------------------------
@description Shows a window
------------------------------------------------------------------------------*/
  define input parameter pWindow as character no-undo.
  
  if not can-find(first openwindow where procFile = pWindow)
   then
    do:
      create openwindow.
      assign
        openwindow.procFile = pWindow
        openwindow.procHandle = ?
        .
    end.
  
  for first openwindow no-lock
      where openwindow.procFile = pWindow:
      
    if valid-handle(openwindow.procHandle)
     then run ShowWindow in openwindow.procHandle no-error.
     else run value(openwindow.procFile) persistent set openwindow.procHandle.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent C-Win 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowShow C-Win 
PROCEDURE ActionWindowShow :
/*------------------------------------------------------------------------------
@description Shows a window
------------------------------------------------------------------------------*/
  define input parameter pWindow as character no-undo.
  
  if not can-find(first openwindow where procFile = pWindow)
   then
    do:
      create openwindow.
      assign
        openwindow.procFile = pWindow
        openwindow.procHandle = ?
        .
    end.
  
  for first openwindow no-lock
      where openwindow.procFile = pWindow:
      
    if valid-handle(openwindow.procHandle)
     then run ShowWindow in openwindow.procHandle no-error.
     else run value(openwindow.procFile) persistent set openwindow.procHandle.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApproveInvoice C-Win 
PROCEDURE ApproveInvoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-de = 0.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
   
  case pType:
   when "P" then
    do:
      run GetPayableInvoiceAmount in std-ha (pInvoiceID, output std-de).
      
      if std-de > 0
       then run dialogapprove.w (pInvoiceID, "Payable", std-ha, output std-lo).
       else message "The invoice amount must be greater than $0 to approve" view-as alert-box information buttons ok.
    end.
   when "R" then run ApproveReceivableInvoice in std-ha (pInvoiceID, output std-lo).
  end case.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttachDocument C-Win 
PROCEDURE AttachDocument :
/*------------------------------------------------------------------------------
@description Attaches the document to the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  define variable cProc as character no-undo.
  define variable hApinv as handle no-undo.
  
  hApinv = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output hApinv).
  if hApinv = ?
   then return.
   
  run HasDocument in hApinv (output std-lo).
  if std-lo
   then
    do:
      message "The current document will be overwritten. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
        
  /* the invoice doesn't have a document attached */
  define variable cFile as character no-undo.
  SYSTEM-DIALOG GET-FILE cFile 
    TITLE   "Attach Invoice to ShareFile..."
    MUST-EXIST 
    USE-FILENAME 
    UPDATE std-lo.
    
  if std-lo
   then
    do:
      /* confirmation */
      publish "GetConfirmFileUpload" (output std-lo).
      if std-lo 
       then
        do:
          MESSAGE cFile + " will be uploaded. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
          if not std-lo 
           then return.
        end.
       
      cProc = "".
      case pType:
       when "P" then cProc = "Payable".
       when "R" then cProc = "Receivable".
      end case.
      run HasDocument in hApinv (output std-lo).
      if std-lo
       then run value("Detach" + cProc + "Document") in hApinv (pInvoiceID, output std-lo).
      run value("Attach" + cProc + "Document") in hApinv (pInvoiceID, cFile, output std-lo).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClaimAccountingDataChanged C-Win 
PROCEDURE ClaimAccountingDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  for each openWindow:
    std-ha = openWindow.procHandle.     
    
    /* Close child window created in the window*/
    if valid-handle(std-ha) 
     then
      run closeWindow in std-ha.    
      
    delete openWindow.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CompleteInvoice C-Win 
PROCEDURE CompleteInvoice :
/*------------------------------------------------------------------------------
@description Completes the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-ha = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
   
  case pType:
   when "P" then std-ch = "payable".
   when "R" then std-ch = "receivable".
  end case.
  
  run value("dialog" + std-ch + "complete.w") (pInvoiceID, std-ha, output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteDocument C-Win 
PROCEDURE DeleteDocument :
/*------------------------------------------------------------------------------
@description Deletes the document attached to the invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-lo = true.
  publish "GetConfirmDelete" (output std-lo).
  if std-lo
   then
    do:
      std-lo = false.
      MESSAGE "Document will be permanently removed. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
      if not std-lo 
       then return.
    end.
        
  std-ha = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
   
  std-ch = "".
  case pType:
   when "P" then std-ch = "Payable".
   when "R" then std-ch = "Receivable".
  end case.
   
  run value("Detach" + std-ch + "Document")  in std-ha (pInvoiceID, output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteInvoice C-Win 
PROCEDURE DeleteInvoice :
/*------------------------------------------------------------------------------
@description Deletes the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-ha = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
  
  std-lo = true.
  publish "GetConfirmDelete" (output std-lo).
  if std-lo
   then
    do:
      std-lo = true.
      run HasDocument in std-ha (output std-lo).
      if std-lo
       then MESSAGE "Deleting the invoice will also delete the attached document. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
       else MESSAGE "Deleting the invoice cannot be undone. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
      if not std-lo 
       then return.
    end.
    
  
  std-ch = "".
  case pType:
   when "P" then std-ch = "Payable".
   when "R" then std-ch = "Receivable".
  end case.
  
  run value("Delete" + std-ch + "Invoice") in std-ha (pInvoiceID, output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSearchVendor tSearchStart tSearchEnd tSearchMine cmbEntity tListLabel 
          tStartDateLabel tEndDateLabel tEntityLabel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE rFilter rLists bSearchGo bSearchClear tSearchVendor tSearchStart 
         tSearchEnd tSearchMine cmbEntity bNewList brwData brwList tListLabel 
         tStartDateLabel tEndDateLabel tEntityLabel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
@description Exports the payments and invoices to Excel
------------------------------------------------------------------------------*/
  def var cBrwList as char no-undo.
  def var cReportDir as char no-undo.
  assign
    cBrwList = ""
    cBrwList = string(browse {&browse-name}:handle)
    std-ch = "Open_Invoices_" + replace(string(today),"/","_")
    .
  run util/exporttocsvbrowse.p (cBrwList,std-ch).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
@description Filter the data based on the criteria
------------------------------------------------------------------------------*/
  define variable dStartDate as datetime no-undo.
  define variable dEndDate as datetime no-undo.
  define variable cVendor as character no-undo.
  define variable lShowApprovals as logical no-undo.
  
  define variable dStartTime as datetime no-undo.
  
  /* get the filter values */
  do with frame {&frame-name}:
    assign
      std-ha = {&browse-name}:handle
      dStartDate = tSearchStart:input-value
      dEndDate = tSearchEnd:input-value
      cVendor = tSearchVendor:screen-value
      lShowApprovals = logical(tSearchMine:screen-value)
      .
  end.
  
  if dStartDate = ?
   then dStartDate = date(1,1,2005).
   
  if dEndDate = ?
   then dEndDate = add-interval(now,1,"days").
  
  dStartTime = now.
  empty temp-table apminvoice.
  run server/getapinvoicesbystatus.p ("O,D",
                                      cVendor,
                                      dStartDate,
                                      dEndDate,
                                      "ALL",
                                      output table apminvoice,
                                      output std-lo,
                                      output std-ch).
                                    
  if not std-lo
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "GetInvoicesOpened failed: " std-ch view-as alert-box warning.
    end.
   else
    do:
      std-ch = "".
      std-in = 0.
      publish "GetCredentialsID" (output std-ch).
      for each apminvoice exclusive-lock:
        if lShowApprovals and lookup(apminvoice.approval, std-ch) = 0
         then delete apminvoice.
         else std-in = std-in + 1.
      end.
      std-ch = string(std-in) + " invoice(s) found in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds".
      status input std-ch in window {&window-name}.
      status default std-ch in window {&window-name}.
      do with frame {&frame-name}:
        bSearchExport:sensitive = (std-in > 0).
        bPrint:sensitive = (std-in > 0).
        menu-item m_Popup_Delete:sensitive in menu POPUP-MENU-brwData = (std-in > 0).
      end.
      
      dataSortDesc = false.
      {lib/brw-startSearch-multi.i &browseName=brwData &sortClause="'by invoiceDate'"}
      
      apply "VALUE-CHANGED" to browse {&browse-name}.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSavedLists C-Win 
PROCEDURE GetSavedLists :
/*------------------------------------------------------------------------------
@description Gets the user's saved lists
------------------------------------------------------------------------------*/
  define input parameter pEntity as character no-undo.
  
  publish "GetLists" (pEntity, output table list, output std-lo).
  if std-lo
   then {&OPEN-QUERY-brwList}
   else
    do:
      message "Failed to get the saved lists." view-as alert-box error buttons ok.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize C-Win 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    bSearchGo:load-image("images/completed.bmp").
    bSearchGo:load-image-insensitive("images/completed-i.bmp").
    bSearchClear:load-image("images/erase.bmp").
    bSearchClear:load-image-insensitive("images/erase-i.bmp").
    bSearchExport:load-image("images/excel.bmp").
    bSearchExport:load-image-insensitive("images/excel-i.bmp").
    bOpenList:load-image-up("images/import.bmp").
    bOpenList:load-image-insensitive("images/import-i.bmp").
    bNewList:load-image-up("images/new.bmp").
    bNewList:load-image-insensitive("images/new-i.bmp").
    bModifyList:load-image-up("images/update.bmp").
    bModifyList:load-image-insensitive("images/update-i.bmp").
    bDeleteList:load-image-up("images/delete.bmp").
    bDeleteList:load-image-insensitive("images/delete-i.bmp").
    bShareList:load-image-up("images/users.bmp").
    bShareList:load-image-insensitive("images/users-i.bmp").
    bPrint:load-image-up("images/pdf.bmp").
    bPrint:load-image-insensitive("images/pdf-i.bmp").
  
    /* set the entity list */
    publish "GetEntities" (output table listentity,output std-lo).
    if std-lo
     then
      do:
        cmbEntity:delete(1).
        for each listentity no-lock:
          cmbEntity:add-last(listentity.displayName,listentity.entityName).
        end.
        apply "VALUE-CHANGED" to cmbEntity.
      end.
     else
      do:
        cmbEntity:delete(1).
        cmbEntity:add-last("ALL","A").
        cmbEntity:screen-value = "A".
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InvoiceSelected C-Win 
PROCEDURE InvoiceSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as character no-undo.
  define variable iInvoiceID as integer no-undo.
  
  iInvoiceID = integer(pInvoiceID) no-error.

  run ModifyInvoice in this-procedure (iInvoiceID).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyInvoice C-Win 
PROCEDURE ModifyInvoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo. 
  define input parameter pIsGeneral as logical no-undo.
  
  std-ha = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
  
  case pType:
   when "P" then
    do:
      empty temp-table apinv.
      empty temp-table apinva.
      empty temp-table apinvd.
       
      run GetPayableInvoice in std-ha 
                            (input pInvoiceID,
                             output table apinv,
                             output table apinva,
                             output table apinvd
                             ).
      
      for first apinv no-lock:
        if apinv.refType = "C"
         then 
          do:
            publish "LoadClaim" (integer(apinv.refID)).
            run wclaimpayable.w persistent
                                (input integer(apinv.refID),
                                 input table apinv,
                                 input table apinva,
                                 input table apinvd,
                                 input "Modify Claim Payable Invoice",
                                 input std-ha,
                                 output std-lo
                                 ).
          end.
      end.
    end.
   when "R" then
    do:
      empty temp-table arinv.
      run GetReceivableInvoice in std-ha 
                               (input pInvoiceID,
                                output table arinv
                                ).
      
      for first arinv no-lock:
        if pIsGeneral
         then run wreceivable.w persistent
                                (input table arinv,
                                 input std-ha,
                                 input "Create General Receivable Invoice",
                                 output std-lo
                                 ).
         else 
          case arinv.refType:
           when "C" then 
            do:
              publish "LoadClaim" (integer(arinv.refID)).
              run wclaimreceivable.w persistent
                                     (input integer(arinv.refID),
                                      input table arinv,
                                      input std-ha,
                                      input "Modify Claim Receivable Invoice",
                                      output std-lo
                                      ).
            end.
          end case.
      end.
    end.
  end case.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPayable C-Win 
PROCEDURE NewPayable :
/*------------------------------------------------------------------------------
@description Create a new payable
------------------------------------------------------------------------------*/ 
  define input parameter pIsGeneral as logical no-undo.
   
  empty temp-table apinv.
  empty temp-table apinva.
  empty temp-table apinvd.
  std-ha = ?.
  publish "OpenInvoice" (0, "P", output std-ha).
  if std-ha = ?
   then return.
   
  if pIsGeneral
   then run wpayable.w persistent
                       (input table apinv,
                        input table apinva,
                        input table apinvd,
                        input "Create General Payable Invoice",
                        input std-ha,
                        output std-lo
                        ).
   else 
    do:
      std-in = 0.
      run dialogclaimfind.w (output std-ch, output std-lo).
      if not std-lo
       then 
        do:
          std-in = integer(std-ch) no-error.
          publish "LoadClaim" (std-in).
          /* need to check the type as Notices do not have payables */
          std-ch = "".
          publish "GetClaimType" (std-in, output std-ch).
          if std-ch = "N"
           then message "Please change the file type to either a Matter or Claim to add invoices" view-as alert-box error buttons ok.
           else run wclaimpayable.w persistent
                                   (input std-in,
                                    input table apinv,
                                    input table apinva,
                                    input table apinvd,
                                    input "Create Claim Payable Invoice",
                                    input std-ha,
                                    output std-lo
                                    ).
        end.
    end.
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewReceivable C-Win 
PROCEDURE NewReceivable :
/*------------------------------------------------------------------------------
@description Create a new receivable
------------------------------------------------------------------------------*/ 
  define input parameter pIsGeneral as logical no-undo.
   
  empty temp-table arinv.
  std-ha = ?.
  publish "OpenInvoice" (0, "R", output std-ha).
  if std-ha = ?
   then return.
   
  if pIsGeneral
   then run wreceivable.w persistent
                          (input table arinv,
                           input std-ha,
                           input "Create General Receivable Invoice",
                           output std-lo
                           ).
   else 
    do:
      std-in = 0.
      run dialogclaimfind.w (output std-ch, output std-lo).
      if not std-lo
       then 
        do:
          std-in = integer(std-ch) no-error.
          publish "LoadClaim" (std-in).
          /* need to check the type as Notices do not have payables */
          std-ch = "".
          publish "GetClaimType" (std-in, output std-ch).
          if std-ch = "N"
           then message "Please change the file type to either a Matter or Claim to add invoices" view-as alert-box error buttons ok.
           else run wclaimreceivable.w persistent
                                       (input std-in,
                                        input table arinv,
                                        input std-ha,
                                        input "Create Claim Receivable Invoice",
                                        output std-lo
                                        ).
        end.
    end.
                            
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewSavedList C-Win 
PROCEDURE NewSavedList :
/*------------------------------------------------------------------------------
@description Create a new list
------------------------------------------------------------------------------*/
  /* create a new window to build the filters and fields */
  run sys/wlistbuild.w persistent (this-procedure, true).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenSavedList C-Win 
PROCEDURE OpenSavedList :
/*------------------------------------------------------------------------------
@description Opens a user's previously saved list  
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      run sys/wlistbuild.w persistent set std-ha (this-procedure, false).
      run BuildData in std-ha (list.displayName, list.reportName, true).
    end.
   else message "Please select a saved report" view-as alert-box warning buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveSavedList C-Win 
PROCEDURE SaveSavedList :
/*------------------------------------------------------------------------------
@description Saves the list
------------------------------------------------------------------------------*/
  define input parameter pIsNew as logical no-undo.
  define input parameter pCurrentName as character no-undo.
  define input parameter pDisplayName as character no-undo.
  define input parameter table for listfield.
  define input parameter table for listfilter.
  
  if (pIsNew or pCurrentName <> pDisplayName) and can-find(first list where displayName = pDisplayName)
   then
    do:
      message "There is already a saved list named " + pDisplayName + ". Do you want to overwrite?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
  
  pIsNew = not can-find(first list where displayName = pDisplayName).
  publish "SaveList" (pIsNew,
                      pDisplayName,
                      table listfield,
                      table listfilter,
                      output std-lo).
                      
  if std-lo
   then message "Successfully saved the report" view-as alert-box information buttons ok.
   else message "Could not save the report" view-as alert-box error buttons ok.
          
  /* if we renamed the report, we need to delete the old report as saving will make a new one */
  if pCurrentName <> pDisplayName
   then publish "DeleteList" (pCurrentName, output std-lo).
   
  run GetSavedLists in this-procedure (cmbEntity:screen-value in frame {&frame-name}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SendPayable C-Win 
PROCEDURE SendPayable :
/*------------------------------------------------------------------------------
@description Send a payable invoice for approval
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  publish "SendPayableInvoice" (pInvoiceID, output std-lo).
  if not std-lo
   then message "Failed to send the email" view-as alert-box error buttons ok.
   else message "Email sent successfully" view-as alert-box error buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShareSavedList C-Win 
PROCEDURE ShareSavedList :
/*------------------------------------------------------------------------------
@description Shares the selected list with other users
------------------------------------------------------------------------------*/
  if available list
   then
    do:
      /* the output will be the user list */
      run sys/userselect.w ("", output std-ch).
      
      if std-ch > ""
       then
        do: 
          publish "ShareList" (list.displayName,
                               std-ch,
                               output std-ch).
      
          if std-ch > ""
           then message std-ch view-as alert-box error buttons ok.
           else message "Successfully shared the report" view-as alert-box information buttons ok.
        end.
    end.
   else message "Please select a saved report to share" view-as alert-box warning buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorSelected C-Win 
PROCEDURE VendorSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pVendorID as character no-undo.

  publish "SetCurrentValue" ("VendorID", pVendorID).
  run wapm02-r.w persistent.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewCheckRequest C-Win 
PROCEDURE ViewCheckRequest :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-ha = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
   
  std-ch = "".
  case pType:
   when "P" then std-ch = "Payable".
   when "R" then std-ch = "Receivable".
  end case.
  
  run value("View" + std-ch + "CheckRequest") in std-ha (pInvoiceID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewDocument C-Win 
PROCEDURE ViewDocument :
/*------------------------------------------------------------------------------
@description View the attached document
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-ha = ?.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if std-ha = ?
   then return.
   
  std-ch = "".
  case pType:
   when "P" then std-ch = "Payable".
   when "R" then std-ch = "Receivable".
  end case.
   
  run value("View" + std-ch + "Document") in std-ha (pInvoiceID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewSavedList C-Win 
PROCEDURE ViewSavedList :
/*------------------------------------------------------------------------------
@description View the selected list
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      run sys/wlistresult.w persistent (list.displayName, list.reportName).
    end.
   else MESSAGE "Please select a saved report" VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VoidDetailsPayable C-Win 
PROCEDURE VoidDetailsPayable :
/*------------------------------------------------------------------------------
@description View the selected void
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  publish "OpenInvoice" (pInvoiceID, "P", output std-ha).
  if valid-handle(std-ha)
   then run dialogvoiddetails.w (pInvoiceID, std-ha).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VoidTransaction C-Win 
PROCEDURE VoidTransaction :
/*------------------------------------------------------------------------------
@description View the selected list
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pTransactionID as integer no-undo.
  define input parameter pType as character no-undo.
  
  std-in = 0.
  publish "OpenInvoice" (pInvoiceID, pType, output std-ha).
  if  valid-handle(std-ha)
   then 
    do:
      std-ch = "".
      case pType:
       when "P" then run dialogpayablevoid.w (pTransactionID, std-ha, output std-lo).
       when "R" then run dialogreceivablevoid.w (pTransactionID, std-ha, output std-lo).
      end case.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WaiveInvoice C-Win 
PROCEDURE WaiveInvoice :
/*------------------------------------------------------------------------------
@description Waive the rest of the receivable invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  std-in = 0.
  publish "OpenInvoice" (pInvoiceID, "R", output std-ha).
  if  valid-handle(std-ha)
   then 
    do:
      std-lo = false.
      publish "GetConfirmWaived" (output std-lo).
      if std-lo
       then
        do:
          std-lo = true.
          message "Waiving the invoice cannot be undone. Continue?" view-as alert-box question buttons yes-no update std-lo.
          if not std-lo
           then return.
        end.
        
      run WaiveReceivableInvoice in std-ha (pInvoiceID).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.
  define variable dDiffWidth as decimal no-undo.
  hBrowse = browse brwList:handle.
  
  assign
    /* resize the frame */
    frame {&frame-name}:width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
    /* resize the browse */
    {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 13 - hBrowse:width-pixels
    {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5
    /* get the difference in the width */
    dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:MIN-WIDTH-PIXELS
    hBrowse:height-pixels = {&browse-name}:height-pixels - 42.
    .
    
  std-ha = getColumn({&browse-name}:handle, "vendorName").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dVendorColumnWidth + dDiffWidth / 2.
  
  std-ha = getColumn({&browse-name}:handle, "approval").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dApprovalColumnWidth + dDiffWidth / 2.
   
  /* move the browse list and buttons */
  do with frame {&frame-name}:
    assign
      /* Filter */
      rFilter:width-pixels = {&browse-name}:width-pixels + 1
      tSearchVendor:width-pixels = (dParamPos - 70) + dDiffWidth
      tStartDateLabel:x = dParamPos + dDiffWidth
      tSearchStart:x = tStartDateLabel:x + 106
      tEndDateLabel:x = tSearchStart:x + 83
      tSearchEnd:x = tEndDateLabel:x + 102
      tSearchMine:x = tSearchEnd:x + 87
      bSearchGo:x = tSearchMine:x + 145
      bSearchClear:x = bSearchGo:x + + 37
      bSearchExport:x = bSearchClear:x + 37
      bPrint:x = bSearchExport:x + 37
      /* Quick Lists */
      rLists:x = dEntityRect + dDiffWidth
      cmbEntity:x = dEntityColumn + dDiffWidth
      tEntityLabel:x = cmbEntity:x - 32
      hBrowse:x = dListColumn + dDiffWidth
      tListLabel:x = dListLabelColumn + dDiffWidth
      bNewList:x = dButtonNewColumn + dDiffWidth
      bModifyList:x = bNewList:x + 37
      bDeleteList:x = bModifyList:x + 37
      bShareList:x = bDeleteList:x + 37
      bOpenList:x = bShareList:x + 37
      .
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

