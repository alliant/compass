&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/winlaunch.i}
{tt/reportagentstatement.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo tEffDate tAgentName bClear RECT-37 
&Scoped-Define DISPLAYED-OBJECTS tEffDate tAgentName 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bGo 
     LABEL "Go" 
     SIZE 7.2 BY 1.71.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent Name" 
     VIEW-AS FILL-IN 
     SIZE 43 BY 1 NO-UNDO.

DEFINE VARIABLE tEffDate AS DATETIME FORMAT "99/99/9999":U 
     LABEL "As Of Date" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 80 BY 3.81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bGo AT ROW 2.43 COL 64 WIDGET-ID 16
     tEffDate AT ROW 2.19 COL 17 COLON-ALIGNED WIDGET-ID 6
     tAgentName AT ROW 3.38 COL 17 COLON-ALIGNED WIDGET-ID 14
     bClear AT ROW 2.43 COL 72 WIDGET-ID 20
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 4
     RECT-37 AT ROW 1.48 COL 2 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81.8 BY 4.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Agent Statement"
         HEIGHT             = 4.48
         WIDTH              = 81.8
         MAX-HEIGHT         = 38.81
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 38.81
         VIRTUAL-WIDTH      = 320
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Agent Statement */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Agent Statement */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  do with frame {&frame-name}:
    assign
      tEffDate:screen-value = string(today)
      tAgentName:screen-value = ""
      .
    status input "" in window {&window-name}.
    status default "" in window {&window-name}.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  define variable dStartTime as datetime no-undo.
  define buffer reportagentstatement for reportagentstatement.

  status input "Exporting data..." in window {&window-name}.
  status default "Exporting data..." in window {&window-name}.

  do with frame {&frame-name}:
    empty temp-table reportagentstatement.
    
    dStartTime = now.
    run server/getaragentstatement.p (tEffDate:input-value,
                                      tAgentName:input-value,
                                      output table reportagentstatement,
                                      output std-lo,
                                      output std-ch).
     
    if not std-lo
     then message "AgentStatement failed: " std-ch view-as alert-box warning.
     else 
      do:
        std-in = 0.
        for each reportagentstatement:
          std-in = std-in + 1.
        end.
        std-ch = string(std-in) + " claim(s) found in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, "ZZZ,ZZ9.9")) + " seconds".
        status input std-ch in window {&window-name}.
        status default std-ch in window {&window-name}.
        
        if tAgentName:input-value = ""
         then std-ch = "agent_statement_" + replace(string(date(tEffDate:input-value)),"/","_") + ".csv".
         else std-ch = "agent_statement_" + replace(tAgentName:input-value," ","_") + "_" + replace(string(date(tEffDate:input-value)),"/","_") + ".csv".
        run CreateStatement in this-procedure (std-ch).
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bGo:load-image("images/completed.bmp").
bGo:load-image-insensitive("images/completed-i.bmp").
bClear:load-image("images/erase.bmp").
bClear:load-image-insensitive("images/erase-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  apply "CHOOSE" to bClear.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CreateStatement C-Win 
PROCEDURE CreateStatement :
/*------------------------------------------------------------------------------
@description Create the CSV file in the format of the Agent Statement
------------------------------------------------------------------------------*/
  define input parameter pFileName as character no-undo.
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable cDataLine as character no-undo.

  /* build a new query */
  hBuffer = temp-table reportagentstatement:default-buffer-handle.
  create query hQuery.
  hQuery:set-buffers(hBuffer).
  hQuery:query-prepare("preselect each reportagentstatement").
  hQuery:query-open().
  hQuery:get-first().
  
  output to value(pFileName) page-size 0.
  /* put the headers */
  put unformatted "ClaimNumber,".
  put unformatted "Policy #,".
  put unformatted "Agent File #,".
  put unformatted "Claim Category,".
  put unformatted "Open/Closed,".
  put unformatted "Closed Date,".
  put unformatted "Claim State,".
  put unformatted "Total Loss Pd,".
  put unformatted "Total LAE Pd,".
  put unformatted "Total Pd,".
  put unformatted "Deductible Amt,".
  put unformatted "Unadjusted Balance,".
  put unformatted "Billable Amount,".
  put unformatted "Amt Billed,".
  put unformatted "Date Billed,".
  put unformatted "Inv #,".
  put unformatted "Date Pd in Full,".
  put unformatted "Aging Days,".
  put unformatted "Amt Pd,".
  put unformatted "De Minimus Waived,".
  put unformatted "Other Waived,".
  put unformatted "Comments".
  put unformatted "" skip.

  /* loop through the buffer fields */
  repeat while not hQuery:query-off-end:
    cDataLine = "".
    do std-in = 1 to hBuffer:num-fields:
      std-ha = hBuffer:buffer-field(std-in).
      if std-ha:data-type = "datetime"
       then std-ch = string(std-ha:buffer-value(),"99/99/9999").
       else std-ch = replace(string(std-ha:buffer-value()), ",", " ").
      if std-ch = ? 
       then std-ch = "".
      cDataLine = addDelimiter(cDataLine,",") + std-ch.
    end.
    put unformatted cDataLine skip.
    hQuery:get-next().
  end.
  output close.

  publish "AddTempFile" (pFileName, pFileName).
  
  RUN ShellExecuteA in this-procedure (0,
                                       "open",
                                       pFileName,
                                       "",
                                       "",
                                       1,
                                       OUTPUT std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEffDate tAgentName 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bGo tEffDate tAgentName bClear RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

