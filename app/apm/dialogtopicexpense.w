&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*---------------------------------------------------------------------
@name Topic Expense
@description add/modify Topicexpense

@author Sagar Koralli
@version 1.0
@created 07/11/2023
@notes 
@modified
Date        Name            Comment
07/17/2024  SRK             Modified to show Tasks in   Orderwise
---------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* --Temp-Table Definitions ---                                         */
{tt/topicexpense.i}
{tt/topicexpense.i &tableAlias=tttopicexpense}
{tt/state.i}
{tt/tasktopic.i}
{lib/make-required-def.i}

/* Parameters Definitions ---                                           */
define input  parameter ipcActionType       as character no-undo.
define input  parameter table               for topicexpense   .
define output parameter chTopic             as character   no-undo.
define output parameter chStateID           as character   no-undo.
define output parameter oplSuccess          as logical   no-undo.
/* Local Variable Definitions ---                                       */
define variable chGLAcct     as character no-undo.
define variable chGLAcctDesc as character no-undo.

{lib/std-def.i}
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tState tTopic fGLAcct fDescription Btn_Link ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tState tTopic fGLAcct fDescription 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_Link AUTO-GO 
     LABEL "Save" 
     SIZE 14 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(36)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "item","item"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tTopic AS CHARACTER FORMAT "X(256)" 
     LABEL "Task" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "--Select Task--","None"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 35 BY .95 NO-UNDO.

DEFINE VARIABLE fGLAcct AS CHARACTER FORMAT "X(256)":U 
     LABEL "GL Account" 
     VIEW-AS FILL-IN 
     SIZE 35 BY .95 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tState AT ROW 1.24 COL 14.6 COLON-ALIGNED WIDGET-ID 104
     tTopic AT ROW 2.33 COL 14.6 COLON-ALIGNED WIDGET-ID 4
     fGLAcct AT ROW 3.43 COL 14.6 COLON-ALIGNED WIDGET-ID 6
     fDescription AT ROW 4.48 COL 14.6 COLON-ALIGNED WIDGET-ID 108
     Btn_Link AT ROW 6 COL 16.2
     Btn_Cancel AT ROW 6 COL 30.6
     SPACE(14.39) SKIP(0.50)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Topic Expense"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Topic Expense */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Link
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Link Dialog-Frame
ON CHOOSE OF Btn_Link IN FRAME Dialog-Frame /* Save */
DO:
  run savetopicexpense in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDescription Dialog-Frame
ON VALUE-CHANGED OF fDescription IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fGLAcct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fGLAcct Dialog-Frame
ON VALUE-CHANGED OF fGLAcct IN FRAME Dialog-Frame /* GL Account */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState Dialog-Frame
ON VALUE-CHANGED OF tState IN FRAME Dialog-Frame /* State */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tTopic
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tTopic Dialog-Frame
ON VALUE-CHANGED OF tTopic IN FRAME Dialog-Frame /* Task */
DO:
   run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/win-icon.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
   
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
/* get state list */
{lib/get-state-list.i &combo=tState}

/*get tasktopic list */
publish "Gettasktopic" (input "A",
                        output table tasktopic).
for each tasktopic by tasktopic.topic:
  tTopic:add-last(tasktopic.topic,tasktopic.topic).
end.
       
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  /* add required symbol */

  {lib/make-required.i tState:handle}
  {lib/make-required.i tTopic:handle}
  {lib/make-required.i fGLAcct:handle}
  setRequired().
  do with frame {&frame-name}:
  end.
  
  run displayData       in this-procedure.

  run enableDisableSave in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow Dialog-Frame 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    APPLY "END-ERROR":U TO SELF.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if ipcActionType = 'Modify' 
   then
    do:
      find first topicexpense no-error. 
      if available topicexpense 
       then
        do:
          assign 
              frame Dialog-Frame:title          = "Modify GL Account"
              Btn_Link          :label          = "Save"
              Btn_Link          :tooltip        = "Save"
              tTopic            :sensitive      = false
              tstate            :sensitive      = false
              tTopic            :screen-value   = topicexpense.topic 
              fGLAcct           :screen-value   = topicexpense.GLAccount 
              fDescription      :screen-value   = topicexpense.GLAccountDesc.
              tstate            :screen-value   = topicexpense.stateID.
        end.  /* if available topicexpense */
        chTopic  = topicexpense.topic.
        chStateID       = tstate         :screen-value  .
    end.
  else if ipcActionType = 'New' 
   then
    do:
     tState:add-first("--Select State--","None").
     assign
         frame Dialog-Frame:title          = "Assign GL Account to Task"
         Btn_Link          :label          = "Create"  
         Btn_Link          :tooltip        = "Create"  .
         tState            :screen-value   = "None"  . 
         tTopic            :screen-value   = "None"   .
         .  
    end. 
  else if ipcActionType = 'copy' 
   then
    do:
      find first topicexpense no-error. 
      if available topicexpense 
       then
        do:
          
          assign 
              frame Dialog-Frame:title          = "Assign GL Account to Task"
              Btn_Link          :label          = "Create"
              Btn_Link          :tooltip        = "Create"
              tTopic            :screen-value   = topicexpense.topic 
              fGLAcct           :screen-value   = topicexpense.GLAccount 
              fDescription      :screen-value   = topicexpense.GLAccountDesc
              tstate            :screen-value   = topicexpense.stateID 
              
              no-error.
        end.  /* if available topicexpense */
        chTopic    = topicexpense.topic.
        chStateID  = topicexpense.stateID   .
    end.
  assign
    
    chGLAcct        = fGLAcct        :screen-value   
    chGLAcctDesc    = fDescription   :screen-value   
      .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if ipcActionType = 'copy'
   then
    do: 
      Btn_Link:sensitive = not(chStateID     = tState:screen-value and
                              (fGLAcct:screen-value = "" or chGLAcct = fGLAcct:screen-value ) and
                               chTopic       = tTopic:screen-value and
                               tTopic:screen-value <> "select"     and
                               chGLAcctDesc = fDescription:screen-value  and
                               tState:screen-value <> "select")
                               no-error. 
    end.
  else if ipcActionType = 'New'
   then
    do: 
       Btn_Link:sensitive = if (tTopic:screen-value ne "select") and (tState:screen-value ne ?) and fGLAcct:screen-value <> "" then true else false.
    end.
  else 
      Btn_Link:sensitive = not((fGLAcct:screen-value = "" or chGLAcct = fGLAcct:screen-value ) and
                               chGLAcctDesc = fDescription:screen-value  and
                               chStateID    = tState:screen-value ) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tState tTopic fGLAcct fDescription 
      WITH FRAME Dialog-Frame.
  ENABLE tState tTopic fGLAcct fDescription Btn_Link Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savetopicexpense Dialog-Frame 
PROCEDURE savetopicexpense :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if ipcActionType = 'New' or ipcActionType = 'copy'
   then
    do:
      empty temp-table topicexpense.
      create topicexpense.
      assign
          topicexpense.topic         = tTopic       :screen-value
          topicexpense.GLAccount     = fGLAcct      :screen-value   
          topicexpense.GLAccountDesc = fDescription :screen-value   
          topicexpense.stateID       = tstate       :input-value .
          chTopic                    = tTopic       :screen-value.
          chStateID                  = tstate       :input-value .      
    end. 
   else if ipcActionType = 'Modify'
    then
     do:
       for first topicexpense no-lock where  topicexpense.topic = chTopic :
         assign
             topicexpense.topic         = tTopic            :screen-value
             topicexpense.GLAccount     = fGLAcct           :screen-value   
             topicexpense.GLAccountDesc = fDescription      :screen-value   .
       end.
     end. 
  
  if ipcActionType = 'New' or  ipcActionType = 'copy'
   then
    do:
      if tTopic:input-value = "ALL" or tTopic:input-value = ?  and  tstate:input-value = "ALL" or tstate:input-value = ?  
       then
        do:
          message "Topic\state cannot be blank"
             view-as alert-box error buttons ok.
          return. 
        end.
        publish "newtopicexpense"(input table  topicexpense,
                                  output oplSuccess,
                                  output std-ch ).         
    end.
   else if ipcActionType = 'Modify'
    then 
      do:
        publish "modifytopicexpense"(input chTopic,
                                     input table  topicexpense,
                                     output oplSuccess,
                                     output std-ch ).
      end.
  if oplSuccess
    then run CloseWindow in this-procedure.
  else
    message  std-ch
        view-as alert-box information buttons ok.
                             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

