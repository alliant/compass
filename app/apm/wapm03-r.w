&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/claimcostsincurred.i}
{tt/agent.i}
{tt/state.i}

def var tAgentID as char no-undo.
def var tAgentName as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo tStartDate tEndDate cmbState bClear ~
tAgent RECT-37 
&Scoped-Define DISPLAYED-OBJECTS tStartDate tEndDate cmbState tAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bGo 
     LABEL "Go" 
     SIZE 7.2 BY 1.71.

DEFINE VARIABLE cmbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 92 BY 1 NO-UNDO.

DEFINE VARIABLE tEndDate AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Ending Date" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tStartDate AS DATETIME FORMAT "99/99/9999":U 
     LABEL "Starting Date" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 119 BY 5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bGo AT ROW 4.33 COL 112 WIDGET-ID 16
     tStartDate AT ROW 2.19 COL 16 COLON-ALIGNED WIDGET-ID 6
     tEndDate AT ROW 2.19 COL 47 COLON-ALIGNED WIDGET-ID 8
     cmbState AT ROW 3.38 COL 16 COLON-ALIGNED WIDGET-ID 10
     bClear AT ROW 2.19 COL 112 WIDGET-ID 20
     tAgent AT ROW 4.81 COL 16 COLON-ALIGNED WIDGET-ID 84
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 4
     RECT-37 AT ROW 1.48 COL 2 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 120.8 BY 5.67 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Claim Costs Incurred"
         HEIGHT             = 5.67
         WIDTH              = 121.2
         MAX-HEIGHT         = 38.81
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 38.81
         VIRTUAL-WIDTH      = 320
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Claim Costs Incurred */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Claim Costs Incurred */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME DEFAULT-FRAME /* Clear */
DO:
  do with frame {&frame-name}:
    assign
      tStartDate:screen-value = ""
      tEndDate:screen-value = ""
      cmbState:screen-value = "ALL"
      tAgent:screen-value = "ALL"
      .
    run AgentComboClear in this-procedure.
    status input "" in window {&window-name}.
    status default "" in window {&window-name}.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME DEFAULT-FRAME /* Go */
DO:
  def var tFullFile as char no-undo.
  def var dStartTime as datetime no-undo.
  def var tSuccess as logical init false no-undo.
  def var tOpenFile as log init true no-undo.
  def var tMsg as char no-undo.
  
  if tStartDate:input-value = ?
   then tStartDate:screen-value = "01/01/2005".
  
  if tEndDate:input-value = ?
   then tEndDate:screen-value = string(today).
  
  def buffer claimcostsincurred for claimcostsincurred.

  status input "Exporting data..." in window {&window-name}.
  status default "Exporting data..." in window {&window-name}.

  do with frame {&frame-name}:
    empty temp-table claimcostsincurred.
    tFullFile = "claim_costs_incurred_" + replace(string(tStartDate:input-value,"99/99/9999"),"/","_") + "_to_" + replace(string(tEndDate:input-value,"99/99/9999"),"/","_").
    tAgentID = tAgent:input-value.
    tAgentName = "".
    if tAgentID = "ALL" then 
        assign tAgentID = ""
               tAgentName = ""
               .
    
    for first agent no-lock
        where agent.agentID = tAgentID and tAgentID > "":
        tAgentName = agent.name.
    end.

    dStartTime = now.
    run server/getclaimcostsincurred.p (tStartDate:input-value,
                                        tEndDate:input-value,
                                        cmbState:input-value,
                                        0,
                                        tAgentID,
                                        tAgentName,
                                        output table claimcostsincurred,
                                        output tSuccess,
                                        output tMsg).
     
    if not tSuccess
     then message "ClaimCostsIncurred failed: " tMsg view-as alert-box warning.
  end.
 
  std-in = 0.
  for each claimcostsincurred no-lock:
    std-in = std-in + 1.
  end.
  
  std-ch = string(std-in) + " claim(s) found in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds".
  status input std-ch in window {&window-name}.
  status default std-ch in window {&window-name}.
  
  if std-in > 0 then
     run util/exporttocsvtable.p (temp-table claimcostsincurred:default-buffer-handle, tFullFile).
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbState C-Win
ON VALUE-CHANGED OF cmbState IN FRAME DEFAULT-FRAME /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bGo:load-image("images/completed.bmp").
bGo:load-image-insensitive("images/completed-i.bmp").
bClear:load-image("images/erase.bmp").
bClear:load-image-insensitive("images/erase-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=cmbState &addAll=true}
    {lib/get-agent-list.i &combo=tAgent &state=cmbState &addAll=true}
    /* set the values */
    {lib/set-current-value.i &state=cmbState &agent=tAgent}
  end.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStartDate tEndDate cmbState tAgent 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bGo tStartDate tEndDate cmbState bClear tAgent RECT-37 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

