&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wtopicexpense.w

  Description: Window of  TopicExpense

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sagar K

  Created: 08.03.2023
  Modified:
  Date         Name         Description
  09/03/24     SRK          Modified to release lock
------------------------------------------------------------------------*/
create widget-pool.

{lib/winshowscrollbars.i}
{lib/set-button-def.i}
{lib/std-def.i}
{lib/ar-def.i}


/* Temp-table definitions */
{tt/topicexpense.i}
{tt/topicexpense.i &tableAlias=ttopicexpense}
{tt/topicexpense.i &tableAlias=tttopicexpense}

/* Local Variable Definitions */
define variable cTopic             as character  no-undo.
define variable cStateID           as character  no-undo.
define variable lApplySearchString as logical    no-undo.
define variable cSearchString      as character  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwTopicExpense

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES topicexpense

/* Definitions for BROWSE brwTopicExpense                               */
&Scoped-define FIELDS-IN-QUERY-brwTopicExpense topicexpense.stateID "State" topicexpense.topic "Task" topicexpense.GLAccount "GL Account" topicexpense.GLAccountDesc "Description"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTopicExpense   
&Scoped-define SELF-NAME brwTopicExpense
&Scoped-define QUERY-STRING-brwTopicExpense for each topicexpense
&Scoped-define OPEN-QUERY-brwTopicExpense open query {&SELF-NAME} for each topicexpense.
&Scoped-define TABLES-IN-QUERY-brwTopicExpense topicexpense
&Scoped-define FIRST-TABLE-IN-QUERY-brwTopicExpense topicexpense


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwTopicExpense}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport RECT-66 RECT-68 fSearch ~
brwTopicExpense bEdit bRefresh bcopy bNew bSearch 
&Scoped-Define DISPLAYED-OBJECTS fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
    ()  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bcopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.7 TOOLTIP "Copy".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.7 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.7 TOOLTIP "Export to Excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.7 TOOLTIP "Add".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.7 TOOLTIP "Reload data".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.7 TOOLTIP "Search data".

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 61.6 BY 1 TOOLTIP "Search Criteria (State ,Task, GL Account, Description)" NO-UNDO.

DEFINE RECTANGLE RECT-66
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37 BY 2.17.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71.4 BY 2.17.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwTopicExpense FOR 
      topicexpense SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwTopicExpense
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTopicExpense C-Win _FREEFORM
  QUERY brwTopicExpense DISPLAY
      topicexpense.stateID  label     "State"                format "x(10)"           
topicexpense.topic         label     "Task" format "x(25)" 
topicexpense.GLAccount      label     "GL Account"        format "x(23)"  
topicexpense.GLAccountDesc  label     "Description"                  format "x(197)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 107.8 BY 17.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.61 COL 9.8 WIDGET-ID 2 NO-TAB-STOP 
     fSearch AT ROW 1.96 COL 37.6 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     brwTopicExpense AT ROW 3.96 COL 2 WIDGET-ID 300
     bEdit AT ROW 1.61 COL 23.8 WIDGET-ID 8 NO-TAB-STOP 
     bRefresh AT ROW 1.61 COL 2.8 WIDGET-ID 4 NO-TAB-STOP 
     bcopy AT ROW 1.61 COL 30.8 WIDGET-ID 316 NO-TAB-STOP 
     bNew AT ROW 1.61 COL 16.8 WIDGET-ID 6 NO-TAB-STOP 
     bSearch AT ROW 1.61 COL 101.8 WIDGET-ID 308 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .61 AT ROW 1 COL 3.4 WIDGET-ID 60
     "Search" VIEW-AS TEXT
          SIZE 7.4 BY .61 AT ROW 1 COL 40 WIDGET-ID 320
     RECT-66 AT ROW 1.39 COL 2 WIDGET-ID 58
     RECT-68 AT ROW 1.39 COL 38.6 WIDGET-ID 318
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178.8 BY 21.52
         DEFAULT-BUTTON bSearch WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "State Task Setup"
         HEIGHT             = 20.96
         WIDTH              = 109.8
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwTopicExpense fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwTopicExpense:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwTopicExpense:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTopicExpense
/* Query rebuild information for BROWSE brwTopicExpense
     _START_FREEFORM
open query {&SELF-NAME} for each topicexpense.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwTopicExpense */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* State Task Setup */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* State Task Setup */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* State Task Setup */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bcopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bcopy C-Win
ON CHOOSE OF bcopy IN FRAME fMain /* Copy */
do:
  run copytopicexpense in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run modifytopicexpense in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run newtopicexpense in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:  
  /* refresh the topicexpense table in apmdatasrv with the database then call 
     getData to update the browser with updated table topicexpense */
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTopicExpense
&Scoped-define SELF-NAME brwTopicExpense
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTopicExpense C-Win
ON DEFAULT-ACTION OF brwTopicExpense IN FRAME fMain
do:
  apply "choose" to bedit.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTopicExpense C-Win
ON ROW-DISPLAY OF brwTopicExpense IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTopicExpense C-Win
ON START-SEARCH OF brwTopicExpense IN FRAME fMain
do:    
  {lib/brw-startSearch.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  lApplySearchString = true.
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON ENTRY OF fSearch IN FRAME fMain
DO:
  /* store the previous value of search string on which search is applied */
  cSearchString = fSearch:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain
DO:
  /* as soon as we change the search string, we track that string 
  is not applied and change the status in taskbar */
  lApplySearchString = false.
  resultsChanged().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i}

assign
    {&window-name}:min-height-pixels = C-Win:height-pixels
    {&window-name}:min-width-pixels  = C-Win:width-pixels
    {&window-name}:max-height-pixels = session:height-pixels
    {&window-name}:max-width-pixels  = session:width-pixels
    .

assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
   
  {&window-name}:window-state = window-minimized. 
  
  run enable_UI. 
  
  /* Initially search string (Blank) is applied */
  lApplySearchString = true.

  /* Get data from ardatasrv */
  run getData in this-procedure.
  
  setStatusRecords(query brwTopicExpense:num-results). 
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  
  {lib/set-button.i &b=bRefresh  &label="Reload data"     &image="images/refresh.bmp"   &inactive="images/refresh-i.bmp"}
  {lib/set-button.i &b=bnew      &label="Export to Excel" &image="images/add.bmp"       &inactive="images/add-i.bmp"}
  {lib/set-button.i &b=bExport   &label="Add"             &image="images/excel.bmp"     &inactive="images/excel-i.bmp"}
  {lib/set-button.i &b=bEdit     &label="Edit"            &image="images/update.bmp"    &inactive="images/update-i.bmp"}
  {lib/set-button.i &b=bSearch   &label="Search data"     &image="images/magnifier.bmp" &inactive="images/magnifier-i.bmp"}
  {lib/set-button.i &b=bcopy     &label="Copy"            &image="images/copy.bmp"      &inactive="images/copy-i.bmp"}      
setButtons().
  
  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copytopicexpense C-Win 
PROCEDURE copytopicexpense :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer tttopicexpense for tttopicexpense.   
  empty temp-table tttopicexpense.

  if not available topicexpense
   then return.
  
  create tttopicexpense.
  buffer-copy topicexpense to tttopicexpense.
  
  run dialogtopicexpense.w (input "copy",
                            input table tttopicexpense,
                            output cTopic,
                            output cStateID,
                            output std-lo).
  if not std-lo 
   then
    return.      
    
  run getData in this-procedure.

  find first topicexpense where topicexpense.topic = cTopic and topicexpense.stateID = cStateID no-error.

  if available topicexpense
   then      
    reposition brwTopicExpense to rowid rowid(topicexpense) no-error.

  setStatusCount(query brwTopicExpense:num-results).    

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletesyslock C-Win 
PROCEDURE deletesyslock :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* server call to delete the syslock records */
  run server/deleteglmodifylock.p(output std-lo,
                                  output std-ch).

  /* do not display the lock delete failed message */
  /*if not std-lo
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport RECT-66 RECT-68 fSearch brwTopicExpense bEdit bRefresh bcopy 
         bNew bSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define variable htableHandle as handle no-undo. 
  
  if query brwTopicExpense:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

 
  publish "GetReportDir" (output std-ch).
   
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).   

  htableHandle = temp-table topicexpense:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "topicexpense",
                          "for each topicexpense",
                          "stateID,topic,GLAccount,GLAccountDesc",
                          "State,Task,GL Account,Description",
                          std-ch,
                          "GL Account-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* if search string is already applied then filter on the basis of what is
     present inside fsearch fill-in */
  if lApplySearchString 
   then
    cSearchString = trim(fSearch:input-value).
  /* if search string is changed but not applied then restrore the fSearch fill-in
     to previous applied serach string and filter on the basis of what is
     present inside fsearch fill-in */
  else
    fSearch:screen-value = cSearchString.

  empty temp-table topicexpense.
  for each ttopicexpense where (if cSearchString <> "" then ttopicexpense.stateID   matches ("*" + cSearchString + "*")
                               else ttopicexpense.stateID = ttopicexpense.stateID)     or
                               (if cSearchString <> "" then ttopicexpense.topic    matches ("*" + cSearchString + "*")
                               else ttopicexpense.topic = ttopicexpense.topic)       or
                               (if cSearchString <> "" then ttopicexpense.GLAccount matches ("*" + cSearchString + "*")
                               else ttopicexpense.GLAccount = ttopicexpense.GLAccount) or
                               (if cSearchString <> "" then ttopicexpense.GLAccountDesc      matches ("*" + cSearchString + "*")
                               else ttopicexpense.GLAccountDesc = ttopicexpense.GLAccountDesc)
                               by ttopicexpense.stateID:
    
    create topicexpense.
    buffer-copy ttopicexpense to topicexpense.
  end.

  open query brwTopicExpense preselect each topicexpense.

  setStatusCount(query brwTopicExpense:num-results).   
        
  run setWidgetState in this-procedure. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.
  
  publish "GetTopicExpense" (output table ttopicexpense).


  run filterData in this-procedure. 


end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifytopicexpense C-Win 
PROCEDURE modifytopicexpense :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.

  define buffer tttopicexpense for tttopicexpense.   
  
  empty temp-table tttopicexpense.

  if not available topicexpense 
   then return.
  
  create tttopicexpense.
  buffer-copy topicexpense to tttopicexpense.
  
  run dialogtopicexpense.w (input "Modify",
                            input table tttopicexpense,
                            output cTopic,
                            output cStateID,
                            output std-lo).
  if not std-lo 
   then
    return.      
  run deletesyslock in this-procedure.
  run getData in this-procedure.
  
  find first topicexpense where topicexpense.topic = cTopic and topicexpense.stateID = cStateID no-error.

  if available topicexpense
   then      
    reposition brwTopicExpense to rowid rowid(topicexpense) no-error. 

  setStatusCount(query brwTopicExpense:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newtopicexpense C-Win 
PROCEDURE newtopicexpense :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  define buffer tttopicexpense for tttopicexpense.
  
  empty temp-table tttopicexpense.  

  run dialogtopicexpense.w (input "New",
                            input table tttopicexpense,
                            output cTopic,
                            output cStateID,
                            output std-lo).
  if not std-lo 
   then
    return.      
 
  run getData in this-procedure.
  
  find first topicexpense where topicexpense.topic = cTopic and topicexpense.stateID =cStateID no-error.

  if available topicexpense
   then      
    reposition brwTopicExpense to rowid rowid(topicexpense) no-error. 

  setStatusCount(query brwTopicExpense:num-results).
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table topicexpense.

  publish "LoadTopicExpense" (output table topicexpense).

  run getData in this-procedure.

  setStatusRecords(query brwTopicExpense:num-results).   

  lApplySearchString = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
      bExport:sensitive = available topicexpense
      bedit:sensitive   = available topicexpense
      bcopy:sensitive   = available topicexpense
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  {&window-name}:move-to-top().  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels           = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels   = {&window-name}:width-pixels
      frame fMain:height-pixels          = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels  = {&window-name}:height-pixels        
      {&browse-name}:width-pixels        = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels       = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
      .
  run ShowScrollBars(browse brwTopicExpense:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
    () :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatchSearchString}). 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

