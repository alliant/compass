&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wpostfiletasks.w

  Description: Window for AP unposted filetasks

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shefali

  Created: 07.17.2023
  
  @Modified:
  Date        Name     Description   
  09/11/23    Sagar K  Not select record if GL Account and stateId empty
  07/02/24    S Chandu Task #:113230 Bug fixes on the screen.
  07/17/24    S Chandu Validations and Completed through date changes.
  07/22/24    S Chandu Changed filter setup.
  09/03/24    SRK      Modified to call delete lock
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/*   Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */
{lib/set-button-def.i}
{lib/std-def.i}
{lib/winlaunch.i} 
{lib/get-column.i}
{lib/winshowscrollbars.i}
{lib/normalizefileid.i}  /* include file to normalize file number */
{lib/add-delimiter.i}

/* Temp-table Definition */
{tt/fileTask.i}
{tt/fileTask.i &tableAlias="ttFileTask"} 
{tt/fileTask.i &tableAlias="tFileTask"}
{tt/fileTask.i &tableAlias="inputFileTask"}
{tt/period.i}
{tt/consolidateFileTask.i}
{tt/consolidateFileTask.i &tableAlias="ttConsolidateFileTask"}
{tt/consolidateFileTask.i &tableAlias="tConsolidateFileTask"}
{tt/consolidateFileTask.i &tableAlias="inputConsolidateFileTask"}
{tt/proerr.i  &tableAlias="fileTaskPostErr"}

{lib/brw-multi-def.i}

define variable deTopicColumnWidth     as decimal    no-undo.
define variable iCount                 as integer    no-undo.
define variable cVendorList            as character  no-undo.
define variable dtFromDate             as date       no-undo.
define variable dtToDate               as date       no-undo.
define variable lFilterData            as logical    no-undo.
define variable lLockData              as logical    no-undo.
define variable lDefaultFilter         as logical    no-undo initial true.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwConsolidateFileTask

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES consolidateFileTask fileTask

/* Definitions for BROWSE brwConsolidateFileTask                        */
&Scoped-define FIELDS-IN-QUERY-brwConsolidateFileTask consolidateFileTask.stateID consolidateFileTask.vendorName consolidateFileTask.topic consolidateFileTask.glaccount consolidateFileTask.filetaskCount consolidateFileTask.CostSum consolidateFileTask.selectrecord   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwConsolidateFileTask consolidateFileTask.selectrecord   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwConsolidateFileTask ~
consolidateFileTask
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwConsolidateFileTask consolidateFileTask
&Scoped-define SELF-NAME brwConsolidateFileTask
&Scoped-define QUERY-STRING-brwConsolidateFileTask for each consolidateFileTask by stateID desc
&Scoped-define OPEN-QUERY-brwConsolidateFileTask open query {&SELF-NAME} for each consolidateFileTask by stateID desc.
&Scoped-define TABLES-IN-QUERY-brwConsolidateFileTask consolidateFileTask
&Scoped-define FIRST-TABLE-IN-QUERY-brwConsolidateFileTask consolidateFileTask


/* Definitions for BROWSE brwFileTask                                   */
&Scoped-define FIELDS-IN-QUERY-brwFileTask fileTask.fileNumber filetask.agent fileTask.AssignedDate fileTask.EndDateTime fileTask.AssignedToName fileTask.cost   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFileTask   
&Scoped-define SELF-NAME brwFileTask
&Scoped-define QUERY-STRING-brwFileTask for each fileTask
&Scoped-define OPEN-QUERY-brwFileTask open query {&SELF-NAME} for each fileTask.
&Scoped-define TABLES-IN-QUERY-brwFileTask fileTask
&Scoped-define FIRST-TABLE-IN-QUERY-brwFileTask fileTask


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwConsolidateFileTask}~
    ~{&OPEN-QUERY-brwFileTask}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbVendor brwConsolidateFileTask fDateEnd ~
brwFileTask bExport bRefresh RECT-79 RECT-89 RECT-81 
&Scoped-Define DISPLAYED-OBJECTS cbVendor fDateEnd dtPost 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  (pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bFilter  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filters".

DEFINE BUTTON bPost  NO-FOCUS
     LABEL "Post" 
     SIZE 7.2 BY 1.71 TOOLTIP "Post selected invoice(s)".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh data".

DEFINE VARIABLE cbVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 46.8 BY 1 NO-UNDO.

DEFINE VARIABLE dtPost AS DATE FORMAT "99/99/99":U 
     LABEL "Use Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fDateEnd AS DATE FORMAT "99/99/99":U 
     LABEL "Completed Through" 
     VIEW-AS FILL-IN 
     SIZE 12.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-79
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 2.38.

DEFINE RECTANGLE RECT-81
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 99.2 BY 2.38.

DEFINE RECTANGLE RECT-89
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 35 BY 2.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwConsolidateFileTask FOR 
      consolidateFileTask SCROLLING.

DEFINE QUERY brwFileTask FOR 
      fileTask SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwConsolidateFileTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwConsolidateFileTask C-Win _FREEFORM
  QUERY brwConsolidateFileTask DISPLAY
      consolidateFileTask.stateID            label  "State ID"           format "x(13)"  width 10
consolidateFileTask.vendorName         label  "Vendor"        format "x(60)"          width 40
consolidateFileTask.topic              label  "Task"               format "x(60)"          width 30
consolidateFileTask.glaccount          label  "GL account"         format "x(30)"          width 18
consolidateFileTask.filetaskCount      label  "Count"              format ">>9"            width 10
consolidateFileTask.CostSum            label  "Total"              format "->,>>>,>>9.99"  width 15

consolidateFileTask.selectrecord     column-label "Select to Post" width 17  view-as toggle-box  
enable consolidateFileTask.selectrecord
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 150.4 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFileTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFileTask C-Win _FREEFORM
  QUERY brwFileTask DISPLAY
      fileTask.fileNumber              label "File Number"   format "x(30)"          width 20
filetask.agent                         label "Agent"         format "x(70)"          width 38
fileTask.AssignedDate            column-label "Assigned"     format "99/99/9999"     width 15
fileTask.EndDateTime             column-label "Completed"    format "99/99/9999"     width 15
fileTask.AssignedToName          column-label "Completed By" format "x(30)"          width 35
fileTask.cost                           label "Cost"         format ">>>,>>>,>>9.99" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 150.4 BY 9.76
         TITLE "File Task Details" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     cbVendor AT ROW 1.95 COL 9.4 COLON-ALIGNED WIDGET-ID 458
     brwConsolidateFileTask AT ROW 3.95 COL 1.8 WIDGET-ID 200
     fDateEnd AT ROW 1.95 COL 77.4 COLON-ALIGNED WIDGET-ID 446
     brwFileTask AT ROW 16.05 COL 1.8 WIDGET-ID 300
     dtPost AT ROW 1.95 COL 127.4 COLON-ALIGNED WIDGET-ID 272
     bPost AT ROW 1.57 COL 143.8 WIDGET-ID 198 NO-TAB-STOP 
     bExport AT ROW 1.62 COL 109.2 WIDGET-ID 8 NO-TAB-STOP 
     bFilter AT ROW 1.62 COL 92.6 WIDGET-ID 302 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 102 WIDGET-ID 4 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 102.4 WIDGET-ID 194
     "Post" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 1 COL 119 WIDGET-ID 412
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1 COL 3.6 WIDGET-ID 454
     RECT-79 AT ROW 1.29 COL 100.8 WIDGET-ID 268
     RECT-89 AT ROW 1.29 COL 117.4 WIDGET-ID 410
     RECT-81 AT ROW 1.29 COL 2 WIDGET-ID 270
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COLUMN 1.2 ROW 1
         SIZE 152.6 BY 25.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Post Vendor Tasks"
         HEIGHT             = 24.91
         WIDTH              = 152.2
         MAX-HEIGHT         = 33.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwConsolidateFileTask cbVendor DEFAULT-FRAME */
/* BROWSE-TAB brwFileTask fDateEnd DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bFilter IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwConsolidateFileTask:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwConsolidateFileTask:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwFileTask:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFileTask:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN dtPost IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwConsolidateFileTask
/* Query rebuild information for BROWSE brwConsolidateFileTask
     _START_FREEFORM
open query {&SELF-NAME} for each consolidateFileTask by stateID desc.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwConsolidateFileTask */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFileTask
/* Query rebuild information for BROWSE brwFileTask
     _START_FREEFORM
open query {&SELF-NAME} for each fileTask.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFileTask */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Post Vendor Tasks */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Post Vendor Tasks */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Post Vendor Tasks */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME DEFAULT-FRAME /* Reset */
DO:
  if fDateEnd:input-value >= today
   then
    do:
      message "Completed Through date must be less than today"
          view-as alert-box error buttons ok.
      apply 'entry' to fDateEnd.
      return.
    end.
    
  run setFilterButton in this-procedure.
  run filterData      in this-procedure.

  run enableDisbleFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPost
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPost C-Win
ON CHOOSE OF bPost IN FRAME DEFAULT-FRAME /* Post */
DO:
  run postFileTasks in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* refresh */
DO:
  run getData in this-procedure.
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  /* Makes filters enable-disable based on the data */
  run enableDisbleFilterButton in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwConsolidateFileTask
&Scoped-define SELF-NAME brwConsolidateFileTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidateFileTask C-Win
ON ROW-DISPLAY OF brwConsolidateFileTask IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidateFileTask C-Win
ON START-SEARCH OF brwConsolidateFileTask IN FRAME DEFAULT-FRAME
do:
  define buffer consolidateFileTask for consolidateFileTask.
  
  std-ha = brwConsolidateFileTask:current-column.
  if std-ha:label = "Select to Post" 
   then
    do:     
      std-lo = can-find(first consolidateFileTask where not(consolidateFileTask.selectrecord)).
      for each consolidateFileTask where consolidateFileTask.stateID <> "" and consolidateFileTask.glaccount <> "" :            
        consolidateFileTask.selectrecord = std-lo.
        /* Retaining selected record */
        for first tConsolidateFileTask where tConsolidateFileTask.stateID   = consolidateFileTask.stateID and
                                              tConsolidateFileTask.vendor    = consolidateFileTask.vendor and
                                              tConsolidateFileTask.topic     = consolidateFileTask.topic and
                                              tConsolidateFileTask.glaccount = consolidateFileTask.glaccount:
          tConsolidateFileTask.selectrecord = consolidateFileTask.selectrecord.
        end.
      end. 
      browse brwConsolidateFileTask:refresh(). 
      run enablePosting in this-procedure. 
    end.
   else
    do:
      {lib/brw-startSearch-multi.i &browseName=brwConsolidateFileTask &sortClause="'by stateID'"}
    end.
    
    apply 'value-changed' to brwConsolidateFileTask. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwConsolidateFileTask C-Win
ON VALUE-CHANGED OF brwConsolidateFileTask IN FRAME DEFAULT-FRAME
DO:
  run fileTaskDetails in this-procedure.
  
  if not available consolidateFileTask
   then return.
  
  if (consolidateFileTask.stateID = "" or consolidateFileTask.stateID = ? ) or (consolidateFileTask.glaccount = "" or consolidateFileTask.glaccount = ?)   
   then
    do: 
      consolidateFileTask.selectrecord:read-only in browse  brwConsolidateFileTask   = true .
      consolidateFileTask.selectrecord:checked in browse  brwConsolidateFileTask   = false .
      
    end.
   else
       consolidateFileTask.selectrecord:read-only in browse  brwConsolidateFileTask   = false .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFileTask
&Scoped-define SELF-NAME brwFileTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileTask C-Win
ON ROW-DISPLAY OF brwFileTask IN FRAME DEFAULT-FRAME /* File Task Details */
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFileTask C-Win
ON START-SEARCH OF brwFileTask IN FRAME DEFAULT-FRAME /* File Task Details */
DO:
   {lib/brw-startSearch-multi.i &browseName=brwFileTask &sortClause="'by filetaskID'"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbVendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbVendor C-Win
ON VALUE-CHANGED OF cbVendor IN FRAME DEFAULT-FRAME /* Vendor */
DO:
  lFilterData = false.
  apply 'choose':U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDateEnd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDateEnd C-Win
ON RETURN OF fDateEnd IN FRAME DEFAULT-FRAME /* Completed Through */
DO:
    apply 'choose':U to bFilter. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDateEnd C-Win
ON VALUE-CHANGED OF fDateEnd IN FRAME DEFAULT-FRAME /* Completed Through */
DO:
  lFilterData = false.
  bFilter:load-image("images/filter.bmp").
  bFilter:load-image-insensitive("images/filter-i.bmp").
  bFilter:tooltip = "Set Filters".
  /* Enable reset filter button when filter applies */
  run enableDisbleFilterButton in this-procedure.
  
   resultsChanged(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwConsolidateFileTask
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
/* {lib/brw-main.i} */
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwConsolidateFileTask,brwFileTask"}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

setStatusMessage("").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
subscribe to "closeWindow"  anywhere.

{lib/set-button.i &b=bRefresh &label="Refresh" &image="images/sync.bmp"  &inactive="images/sync-i.bmp"}
{lib/set-button.i &b=bExport  &label="Export"  &image="images/excel.bmp" &inactive="images/excel-i.bmp"}
{lib/set-button.i &b=bPost    &label="Post"    &image="images/check.bmp"  &inactive="images/check-i.bmp"}
{lib/set-button.i &b=bFilter  &label="Reset"   &image="images/filtererase.bmp" &inactive="images/filtererase-i.bmp"}
setButtons().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
      
  run enable_UI.

  /* Getting date range from first and last open active accounting period */   
  publish "GetActiveAccountingPeriod" (output dtFromDate,output dtToDate).

  fDateEnd:screen-value   = string(today - 1).
  
  {lib/get-column-width.i &col="'topic'" &var=deTopicColumnWidth}
  
  assign dtPost:screen-value = if date(dtToDate) < date(today) 
                                then string(dtToDate)
                                else string(today).
  
  run getData     in this-procedure.
  lFilterData = true.
  run setFilterButton in this-procedure.
  
  /* Makes filters enable-disable based on the data */
  run enableDisbleFilterButton in this-procedure.
                                
  on 'value-changed':U of  consolidateFileTask.selectrecord in browse  brwConsolidateFileTask  
  do:
    do with frame {&frame-name}:
    end.
    if available consolidateFileTask 
     then
      do:  
        consolidateFileTask.selectrecord = consolidateFileTask.selectrecord:checked in browse brwConsolidateFileTask.
        /* Retaining selected record */
        for first tConsolidateFileTask where tConsolidateFileTask.stateID   = consolidateFileTask.stateID and
                                              tConsolidateFileTask.vendor    = consolidateFileTask.vendor and
                                              tConsolidateFileTask.topic     = consolidateFileTask.topic and
                                              tConsolidateFileTask.glaccount = consolidateFileTask.glaccount:
          tConsolidateFileTask.selectrecord = consolidateFileTask.selectrecord.
        end.
      end.
    browse brwConsolidateFileTask:refresh().
    run enablePosting in this-procedure.
  end.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  if lLockData
   then
    run deletesyslock in this-procedure.
    apply "CLOSE":U to this-procedure.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyInvoice C-Win 
PROCEDURE copyInvoice :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deletesyslock C-Win 
PROCEDURE deletesyslock :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* server call to delete the syslock records */
  run server/deletevoucherpostlock.p(output std-lo,
                                     output std-ch).

  /* do not display the lock delete failed message */                                   
 /* if not std-lo
   then
    do:
      message std-ch
        view-as alert-box info buttons ok.
      return.
    end. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign cbVendor:delimiter = ";".
         
  for each ttConsolidateFileTask no-lock by ttConsolidateFileTask.vendorname:
    if lookup(ttConsolidateFileTask.vendor,cvendorlist,";") = 0                                                                       
     then
      do:
        if cvendorlist <> "" then
           cvendorlist = cvendorlist + ";" + trim(ttConsolidateFileTask.vendorname) + " (" + string(trim(ttConsolidateFileTask.vendor)) + ")" + ";" + string(trim(ttConsolidateFileTask.vendor)).
         else
          cvendorlist = trim(ttConsolidateFileTask.vendorname) + " (" + string(trim(ttConsolidateFileTask.vendor)) + ")" + ";" + string(trim(ttConsolidateFileTask.vendor)).
       end. 
  end.

  cbVendor:list-item-pairs  = "All" + ";"  + "All" + (if cvendorlist  = "" then ""  else (";" + cvendorlist)).
  
  cbVendor:screen-value     = "ALL" .

  run enableActions in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableActions C-Win 
PROCEDURE enableActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwConsolidateFileTask:num-results > 0
   then
    assign bExport:sensitive = true.
   else
    assign bExport:sensitive  = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisbleFilterButton C-Win 
PROCEDURE enableDisbleFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   do with frame {&frame-name}:
   bFilter:sensitive = not ((cbVendor:screen-value  = "ALL" or cbVendor:screen-value  = ?)
                            and  (fDateEnd:screen-value  = string(today - 1) or fDateEnd:screen-value  = "" or fDateEnd:screen-value  = ? )
                            ).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enablePosting C-Win 
PROCEDURE enablePosting :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not can-find(first tconsolidateFileTask where tConsolidateFileTask.selectrecord = true)
   then
    assign
        bPost:sensitive      = false
        dtPost:sensitive     = false
        .
   else
    assign
        bPost:sensitive      = true
        dtPost:sensitive     = true
        .
        
  iCount = 0.
  for each tConsolidateFileTask where tConsolidateFileTask.selectrecord:
    icount = icount + 1.
  end.
  setStatusMessage(string(iCount) + " selected file task(s)").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbVendor fDateEnd dtPost 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE cbVendor brwConsolidateFileTask fDateEnd brwFileTask bExport bRefresh 
         RECT-79 RECT-89 RECT-81 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwFileTask:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
   
  empty temp-table inputFileTask. 
  for each tfileTask:
    create inputFileTask.
    buffer-copy tfileTask to inputFileTask.
  end.
  
  publish "GetReportDir" (output std-ch).
 
  std-ha = temp-table inputFileTask:handle.
  run util/exporttable.p (table-handle std-ha,
                          "inputFileTask",
                          "for each inputFileTask by filetaskID",
                          "filetaskID,vendorname,vendorID,fileNumber,topic,AssignedToName,AssignedDate,EndDateTime,cost",
                          "File TaskID,Vendor,ID,File Number,Task,Completed By,Assigned,Completed,Cost" ,
                          std-ch,
                          "File Task Details-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportErrorData C-Win 
PROCEDURE exportErrorData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cErrFile as character no-undo.
  
  if can-find(first fileTaskPostErr)
   then
    do:
      publish "GetReportDir" (output std-ch).
      cErrFile = "VendorTaskPostingErrors_" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv".
      std-ha = temp-table fileTaskPostErr:handle.
      run util/exporttable.p (table-handle std-ha,
                              "fileTaskPostErr",
                              "for each fileTaskPostErr",
                              "err,description",
                              "Sequence,Error",
                              std-ch,
                              cErrFile,
                              true,
                              output std-ch,
                              output std-in).
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fileTaskDetails C-Win 
PROCEDURE fileTaskDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table fileTask.
  for each tFileTask where tFileTask.stateID   = consolidateFileTask.stateID and
                          tFileTask.vendorID  = consolidateFileTask.vendor and
                          tFileTask.topic     = consolidateFileTask.topic and
                          tFileTask.glaccount = consolidateFileTask.glaccount:
    create fileTask.
    buffer-copy tFileTask to fileTask.
    assign
        filetask.agent = filetask.agent + ' (' + filetask.agentID + ')'.
  end.
  
  open query brwFileTask preselect each fileTask.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE iCount AS INTEGER     NO-UNDO.
  DEFINE VARIABLE dSum AS decimal       NO-UNDO.
  
  do with frame {&frame-name}:
  end.
  
  define buffer ttFileTask for ttFileTask.

  empty temp-table consolidateFileTask.
  empty temp-table tconsolidateFileTask.
  empty temp-table tFileTask.
  
  close query brwConsolidateFileTask.
  
  for each ttFileTask where date(ttFileTask.endDateTime) <= (if date(fDateEnd:screen-value) ne ? then date(fDateEnd:screen-value) else date(ttFileTask.endDateTime)) and  
                            ttFileTask.vendorID = (if cbVendor:input-value = "ALL" then ttFileTask.vendorID else cbVendor:input-value)
                  group by ttFileTask.stateID by ttFileTask.vendorID by ttFileTask.topic by ttFileTask.glaccount:
      
   create tFileTask.
   buffer-copy ttfiletask to tFileTask.
   
   if first-of(ttFileTask.glaccount)
   then
    do: 
      assign  
          icount = 0
          dsum   = 0.
                 
      create tconsolidatefileTask.
      assign
          tconsolidatefiletask.stateid    = ttFileTask.stateID
          tconsolidatefiletask.vendor     = ttFileTask.vendorID  
          tconsolidatefiletask.vendorName = ttFileTask.vendorname
          tconsolidatefiletask.topic      = ttFileTask.topic
          tconsolidatefiletask.glaccount  = ttFileTask.glaccount
          tconsolidatefiletask.glaccountDesc = ttFileTask.glaccountDesc
          .
     end.   
  assign 
      icount  = icount + 1
      dsum    = dsum + ttFileTask.cost.
      
  if last-of(ttFileTask.glaccount)
   then
    do:
      assign 
          tconsolidatefiletask.filetaskcount = icount
          tconsolidatefiletask.costSum       = dsum.
    end.
  end.
  
  for each tconsolidatefiletask:
    create consolidatefiletask.
    buffer-copy tconsolidatefiletask to consolidatefiletask.
    assign
      consolidatefiletask.vendorName = trim(consolidatefiletask.vendorName) + ' (' + trim(consolidatefiletask.vendor) + ')'.  
  end.
  
  open query brwConsolidateFileTask preselect each consolidateFileTask.
  
  apply "value-changed" to brwConsolidateFileTask.
  
  /* Makes Action buttons/browse enable-disable based on the data */
  run enableActions in this-procedure.
  
  run enablePosting in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer ttfileTask for ttfileTask.
  define buffer ttConsolidateFileTask for ttConsolidateFileTask.

  do with frame {&frame-name}:
  end.

   /* call server */
   run server/getunpostedfiletasks.p (
                                output table ttfiletask,
                                output table ttConsolidateFileTask,
                                output std-lo,
                                output std-ch).

  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.
  lLockData = true.
  run displaydata in this-procedure.
  /* This will use the screen-value of the filters which is ALL */
  run filterData  in this-procedure.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE postFileTasks C-Win 
PROCEDURE postFileTasks :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pSuccess as logical no-undo.
  define buffer fileTask                for fileTask.
  define buffer tFileTask               for tFileTask.
  define buffer ttFileTask              for ttFileTask.
  define buffer consolidateFileTask     for consolidateFileTask.
  define buffer tConsolidateFileTask    for tConsolidateFileTask.

  empty temp-table inputConsolidateFileTask.
  empty temp-table inputFileTask.
  empty temp-table fileTaskPostErr.

  do with frame {&frame-name}:
  end.

  if dtPost:input-value = ?
   then
    do:
      message "Post date cannot be blank."
          view-as alert-box error buttons ok.
      apply 'entry' to dtPost.
      return.
    end.

  if dtPost:input-value > today
   then
    do:
      message "Post date cannot be in the future."
          view-as alert-box error buttons ok.
      apply 'entry' to dtPost.
      return.
    end.
    
  if dtPost:input-value < fDateEnd:input-value
   then
    do:
      message "Post Date cannot be prior to the Completed Through Date."
          view-as alert-box error buttons ok.
      apply 'entry' to dtPost.
      return.
    end.


  publish "validatePostingDate" (input dtPost:input-value,
                                 output std-lo).

  if not std-lo
   then
    do:
      message "Acounting period not active for post date."
          view-as alert-box error buttons ok.
      apply 'entry' to dtPost.
      return.
    end.

  if not can-find(first consolidateFileTask where consolidateFileTask.selectrecord = true)
   then
    do:
      message "Please select at least one record for posting."
          view-as alert-box error buttons ok.
      return.
    end.

  for each consolidateFileTask where consolidateFileTask.selectrecord = true:
    create inputConsolidateFileTask.
    buffer-copy consolidateFileTask to inputConsolidateFileTask.
        
    for each tFileTask where tFileTask.stateID   = consolidateFileTask.stateID and
                             tFileTask.vendorID  = consolidateFileTask.vendor and
                             tFileTask.topic     = consolidateFileTask.topic and
                             tFileTask.glaccount = consolidateFileTask.glaccount:
                             
      if dtPost:input-value < date(tFiletask.enddatetime)
       then
        do:
          message "Post Date cannot be prior to the Task Completion Date."
           view-as alert-box error buttons ok.
          return.         
        end.
        
      create inputFileTask.
      assign 
          inputFileTask.filetaskID = tFileTask.fileTaskID
          inputFileTask.stateID    = tFileTask.stateID  
          inputFileTask.vendorID   = tFileTask.vendorID 
          inputFileTask.topic      = tFileTask.topic    
          inputFileTask.glAccount  = tFileTask.glaccount
          inputFileTask.cost       = tFileTask.cost
          .
    end.
  end.
  
  run server/postvendortasks.p (input dtPost:input-value,
                                input fDateEnd:input-value,
                                input table inputFileTask,
                                input table inputConsolidateFileTask,
                                output table fileTaskPostErr,
                                output pSuccess,
                                output std-ch).

  if not pSuccess
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
   else
    do:
      if can-find(first fileTaskPostErr) then
       /* Export error table into csv and open it */
        run exportErrorData in this-procedure.
      else
       do:
         message "Posting was successful."
           view-as alert-box information buttons ok.
         for each consolidateFileTask where consolidateFileTask.selectrecord = true:
           for each tFileTask where tFileTask.stateID   = consolidateFileTask.stateID and
                                    tFileTask.vendorID  = consolidateFileTask.vendor and
                                    tFileTask.topic     = consolidateFileTask.topic and
                                    tFileTask.glaccount = consolidateFileTask.glaccount:
             for each ttFileTask where ttFileTask.fileTaskID = tFileTask.fileTaskID:
               delete ttFileTask.
             end.
           end.
         end.
       end.
    end.
  run filterdata in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setFilterButton C-Win 
PROCEDURE setFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  if not lFilterData
   then
    do:  
      lFilterData = true.
      bFilter:load-image ("images/filtererase.bmp").
      bFilter:load-image-insensitive("images/filtererase-i.bmp").
      bFilter:tooltip = "Clear Filters".
      
    end.
   else
    do:      
      lFilterData = false.
      
      /* Reset filter widgets */
      assign
          cbVendor:screen-value    = "ALL"
          fDateEnd:screen-value    = string(today - 1)
        no-error.
       
      bFilter:load-image ("images/filter.bmp").
      bFilter:load-image-insensitive("images/filter-i.bmp").
      bFilter:tooltip = "Apply Filters".
      bFilter:sensitive = false.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData-multi.i}
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      
      brwfiletask:width-pixels               = frame {&frame-name}:width-pixels - 9.5
      
      brwconsolidatefiletask:width-pixels    = brwfiletask:width-pixels
      brwconsolidatefiletask:height-pixels   = frame {&frame-name}:height-pixels - brwfiletask:height-pixels - 73
      
      brwfiletask:y                          = frame {&frame-name}:height-pixels - brwfiletask:height-pixels - 5
      .

  {lib/resize-column.i &col="'topic'" &var=deTopicColumnWidth}
  run ShowScrollBars(frame {&frame-name}:handle, no, no).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  (pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match the current Completed Through date"). 
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

