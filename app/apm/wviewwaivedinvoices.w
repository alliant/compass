&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

define variable dVendorColumnWidth as decimal no-undo.
define variable dParamPos as decimal no-undo.

{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/get-reserve-type.i}
{lib/do-wait.i}
{tt/apminvoice.i}
{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/aptrx.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES apminvoice

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData apminvoice.invoiceType apminvoice.hasDocument apminvoice.refDescription apminvoice.refID apminvoice.refCategory apminvoice.vendorName apminvoice.recDate apminvoice.transDate apminvoice.age apminvoice.amount apminvoice.transAmount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH apminvoice
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH apminvoice.
&Scoped-define TABLES-IN-QUERY-brwData apminvoice
&Scoped-define FIRST-TABLE-IN-QUERY-brwData apminvoice


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rFilter bGo bClear cmbType tVendor ~
tStartDate tEndDate brwData tStartDateLabel tEndDateLabel 
&Scoped-Define DISPLAYED-OBJECTS cmbType tVendor tStartDate tEndDate ~
tStartDateLabel tEndDateLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwData 
       MENU-ITEM m_Popup_Modify LABEL "View Invoice"  
       RULE
       MENU-ITEM m_View_Document LABEL "View Document" 
              DISABLED
       MENU-ITEM m_Create_Invoice_Document LABEL "Create Invoice Document"
       MENU-ITEM m_Attach_Document LABEL "Attach Document"
              DISABLED
       MENU-ITEM m_Delete_Document LABEL "Delete Document"
              DISABLED.


/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear the filters".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export the data to a CSV File".

DEFINE BUTTON bGo 
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Display the posted payables based on the filters".

DEFINE BUTTON bPrint 
     LABEL "Print" 
     SIZE 7.2 BY 1.71.

DEFINE VARIABLE cmbType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tEndDate AS DATETIME FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 TOOLTIP "Leave blank for end-of-time"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tEndDateLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Ending Invoice Date:" 
      VIEW-AS TEXT 
     SIZE 20 BY .62 NO-UNDO.

DEFINE VARIABLE tStartDate AS DATETIME FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 TOOLTIP "Leave blank for beginning-of-time"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tStartDateLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Starting Invoice Date:" 
      VIEW-AS TEXT 
     SIZE 21 BY .62 NO-UNDO.

DEFINE VARIABLE tVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 175.2 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      apminvoice SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      apminvoice.invoiceType column-label "Invoice!Type" format "x(20)" width 12
      apminvoice.hasDocument column-label "Has!Doc" view-as toggle-box
      apminvoice.refDescription label "Type" format "x(20)" width 12
      apminvoice.refID label "ID" format "x(20)" width 10
      apminvoice.refCategory label "Category" format "x(20)" width 20
apminvoice.vendorName label "Name" format "x(100)" width 40
apminvoice.recDate column-label "Received!Date" format "99/99/99" width 13
apminvoice.transDate column-label "Completed!Date" format "99/99/99" width 13
apminvoice.age column-label "Age" format ">>,>>9" width 7
apminvoice.amount column-label "Requested!Amount" format "->,>>>,>>9.99" width 15
apminvoice.transAmount column-label "Waived!Amount" format "->,>>>,>>9.99" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 175 BY 28.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.57 COL 161.8 WIDGET-ID 30 NO-TAB-STOP 
     bGo AT ROW 1.57 COL 147.4 WIDGET-ID 32
     bClear AT ROW 1.57 COL 154.6 WIDGET-ID 28
     bPrint AT ROW 1.62 COL 169.2 WIDGET-ID 22
     cmbType AT ROW 2.05 COL 9 COLON-ALIGNED WIDGET-ID 34
     tVendor AT ROW 2.05 COL 38.2 COLON-ALIGNED WIDGET-ID 44
     tStartDate AT ROW 2.05 COL 91.8 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     tEndDate AT ROW 2.05 COL 129 COLON-ALIGNED NO-LABEL WIDGET-ID 38
     brwData AT ROW 3.43 COL 2 WIDGET-ID 200
     tStartDateLabel AT ROW 2.24 COL 70.6 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     tEndDateLabel AT ROW 2.24 COL 108.4 COLON-ALIGNED NO-LABEL WIDGET-ID 26
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 40
     rFilter AT ROW 1.48 COL 2 WIDGET-ID 36
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 177 BY 31.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Waived Invoices"
         HEIGHT             = 31.24
         WIDTH              = 177
         MAX-HEIGHT         = 46.62
         MAX-WIDTH          = 183.2
         VIRTUAL-HEIGHT     = 46.62
         VIRTUAL-WIDTH      = 183.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData tEndDate fMain */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrint IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwData:HANDLE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apminvoice.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Waived Invoices */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Waived Invoices */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Waived Invoices */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  do with frame {&frame-name}:
    assign
      tVendor:screen-value = ""
      tStartDate:screen-value = ""
      tEndDate:screen-value = ""
      cmbType:screen-value = "ALL"
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  doWait(true).
  run ExportData in this-procedure.
  doWait(false).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  if not available apminvoice
   then return.
   
  publish "ViewCheckRequestClient" (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if not available apminvoice
   then return.

  publish "ModifyInvoiceDialog" (apminvoice.apinvID, apminvoice.invoiceType, (apminvoice.refType <> "C")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  /* change the category and the invoice type */
  case apminvoice.invoiceType:
   when "P" then 
    assign
      apminvoice.refCategory:screen-value in browse brwData = getReserveDesc(apminvoice.refCategory)
      apminvoice.invoiceType:screen-value in browse brwData = "Payable"
      .
   when "R" then
    do:
      std-ch = "".
      publish "GetReceivableCategoryDesc" (apminvoice.refCategory, output std-ch).
      apminvoice.refCategory:screen-value in browse brwData = std-ch.
      apminvoice.invoiceType:screen-value in browse brwData = "Receivable".
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if not available apminvoice
   then return.
       
  /* disable/enable buttons based on answers */
  assign
    /* popup menu options */
    menu-item m_Popup_Modify:sensitive in menu POPUP-MENU-brwData = true
    menu-item m_View_Document:sensitive in menu POPUP-MENU-brwData = apminvoice.hasDocument
    menu-item m_Delete_Document:sensitive in menu POPUP-MENU-brwData = apminvoice.hasDocument
    .
  if apminvoice.vendorID <> ""
   then publish "SetCurrentValue" ("VendorID", apminvoice.vendorID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbType C-Win
ON RETURN OF cmbType IN FRAME fMain /* Type */
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Attach_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Attach_Document C-Win
ON CHOOSE OF MENU-ITEM m_Attach_Document /* Attach Document */
DO:
  if not available apminvoice
   then return.
   
  publish "AttachDocumentClient" (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Create_Invoice_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Create_Invoice_Document C-Win
ON CHOOSE OF MENU-ITEM m_Create_Invoice_Document /* Create Invoice Document */
DO:
  apply "CHOOSE" to bPrint in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Document C-Win
ON CHOOSE OF MENU-ITEM m_Delete_Document /* Delete Document */
DO:
  if not available apminvoice
   then return.
   
  publish "DeleteDocumentClient" (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Modify C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Modify /* View Invoice */
DO:
  if not available apminvoice
   then return.

  publish "ModifyInvoiceDialog" (apminvoice.apinvID, apminvoice.invoiceType, (apminvoice.refType <> "C")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Document C-Win
ON CHOOSE OF MENU-ITEM m_View_Document /* View Document */
DO:
  if not available apminvoice
   then return.
   
  publish "ViewDocumentClient" (apminvoice.apinvID, apminvoice.invoiceType).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEndDate C-Win
ON RETURN OF tEndDate IN FRAME fMain
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStartDate C-Win
ON RETURN OF tStartDate IN FRAME fMain
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendor C-Win
ON RETURN OF tVendor IN FRAME fMain /* Vendor */
DO:
  apply "CHOOSE" to bGo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}
{lib/brw-main.i}
{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

/* used to reload data if a change was made */
subscribe to "ClaimAccountingDataChanged" anywhere.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  status input "".
  status default "".
  
  /* load the type combo */
  cmbType:delete(1).
  cmbType:list-item-pairs = "ALL,ALL".
  publish "GetTypeList" (output std-ch).
  if std-ch > ""
   then cmbType:list-item-pairs = addDelimiter(cmbType:list-item-pairs,",") + std-ch.
  cmbType:screen-value = "ALL".
  
  bGo:load-image("images/completed.bmp").
  bGo:load-image-insensitive("images/completed-i.bmp").
  bClear:load-image("images/erase.bmp").
  bClear:load-image-insensitive("images/erase-i.bmp").
  bExport:load-image("images/excel.bmp").
  bExport:load-image-insensitive("images/excel-i.bmp").
  bPrint:load-image("images/pdf.bmp").
  bPrint:load-image-insensitive("images/pdf-i.bmp").
  
  std-ha = getColumn({&browse-name}:handle, "vendorName").
  IF VALID-HANDLE(std-ha)
   THEN dVendorColumnWidth = std-ha:width * session:pixels-per-column.
   
  dParamPos = tStartDateLabel:x.
  run windowResized in this-procedure.
  
  std-lo = false.
  publish "GetAutoView" (output std-lo).
  if std-lo
   then apply "CHOOSE" to bGo.
   
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClaimAccountingDataChanged C-Win 
PROCEDURE ClaimAccountingDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run FilterData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cmbType tVendor tStartDate tEndDate tStartDateLabel tEndDateLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE rFilter bGo bClear cmbType tVendor tStartDate tEndDate brwData 
         tStartDateLabel tEndDateLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var cBrwList as char no-undo.
  def var cReportDir as char no-undo.
  assign
    cBrwList = ""
    cBrwList = string(browse {&browse-name}:handle)
    std-ch = "Voided_Invoices_" + replace(string(today),"/","_")
    .
  run util/exporttocsvbrowse.p (cBrwList,std-ch).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
@description Filter the data based on the criteria
------------------------------------------------------------------------------*/
  define variable cType as character no-undo.
  define variable dStartDate as datetime no-undo.
  define variable dEndDate as datetime no-undo.
  define variable cVendor as character no-undo.
  define variable lShowApprovals as logical no-undo.
  
  define variable hQuery as handle no-undo.
  define variable dStartTime as datetime no-undo.
  
  /* get the filter values */
  do with frame {&frame-name}:
    assign
      std-ha = {&browse-name}:handle
      cType = cmbType:screen-value
      dStartDate = tStartDate:input-value
      dEndDate = tEndDate:input-value
      cVendor = tVendor:screen-value
      .
  end.
  
  dStartTime = now.
  empty temp-table apminvoice.
  run server/getapinvoicesbystatus.p ("W",
                                      cVendor,
                                      dStartDate,
                                      dEndDate,
                                      cType,
                                      output table apminvoice,
                                      output std-lo,
                                      output std-ch).
  
  if not std-lo
   then 
    do: std-lo = false.
        publish "GetAppDebug" (output std-lo).
        if std-lo 
         then message "GetInvoicesCompleted failed: " std-ch view-as alert-box warning.
    end.
   else
    do:
      std-in = 0.
      for each apminvoice no-lock:
        std-in = std-in + 1.
      end.
      std-ch = string(std-in) + " invoice(s) found in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds".
      status input std-ch in window {&window-name}.
      status default std-ch in window {&window-name}.
      
      bExport:sensitive in frame {&frame-name} = (std-in > 0).
      bPrint:sensitive in frame {&frame-name} = (std-in > 0).
      
      dataSortDesc = false.
      if dataSortBy = ""
       then dataSortBy = "invoiceDate".
      run sortData in this-procedure (dataSortBy).
      apply "VALUE-CHANGED" to {&browse-name}.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dDiffWidth as decimal no-undo.
  assign
    /* resize the frame */
    frame {&frame-name}:width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
    /* resize the browse */
    {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10
    {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5
    /* get the difference in size */
    dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:MIN-WIDTH-PIXELS
    /* move the buttons and rectangle */
    rFilter:width-pixels = {&browse-name}:width-pixels + 1
    tVendor:width-pixels = (dParamPos - 205) + dDiffWidth
    tStartDateLabel:x = dParamPos + dDiffWidth
    tStartDate:x = tStartDateLabel:x + 106
    tEndDateLabel:x = tStartDate:x + 84
    tEndDate:x = tEndDateLabel:x + 102
    bGo:x = tEndDate:x + 81
    bClear:x = bGo:x + + 37
    bExport:x = bClear:x + 37
    bPrint:x = bExport:x + 37
    
  std-ha = getColumn({&browse-name}:handle, "vendorName").
  IF VALID-HANDLE(std-ha)
   THEN std-ha:width-pixels = dVendorColumnWidth + dDiffWidth.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

