&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
create widget-pool.
{lib/std-def.i}

{tt/actionsummary.i}
 
define temp-table ttactionsummary like actionsummary
    field cMonth          as char
    field iLateper        as deci
    field iExtendper      as deci.

define temp-table ttactionsummaryexp like ttactionsummary
    field CAction            as char
    field CactionPlanned     as char
    field cActionOpen        as char
    field cActionComplete    as char
    field cActionCancel      as char
    field cActionLate        as char
    field cActionOverdue     as char
    field cActionExtend      as char
    field cLateper           as char
    field cExtendper         as char.

define variable iBgColor         as int     no-undo.
define variable ivAction         as int     no-undo.
define variable ivactionPlanned  as int     no-undo.
define variable ivActionComplete as int     no-undo.
define variable ivActionCancel   as int     no-undo.
define variable ivActionOpen     as int     no-undo.
define variable ivActionLate     as int     no-undo.
define variable ivOverdue        as int     no-undo.
define variable ivExtended       as int     no-undo.
define variable yearpair         as char    no-undo.
define variable outpath          as char    no-undo.
define variable cstatus          as char    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttactionsummary

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttactionsummary.cMonth ttactionsummary.iAction ttactionsummary.iactionPlanned ttactionsummary.iActionOpen ttactionsummary.iActionComplete ttactionsummary.iActionCancel ttactionsummary.iActionOverdue ttactionsummary.iActionLate ttactionsummary.iLateper ttactionsummary.iActionExtend ttactionsummary.iExtendper ttactionsummary.approvalStatus ttactionsummary.approvalUser ttactionsummary.approvalCreateDate ttactionsummary.approvalCompleteDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH ttactionsummary
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH ttactionsummary.
&Scoped-define TABLES-IN-QUERY-brwData ttactionsummary
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttactionsummary


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData sYear bOK RECT-3 RECT-2 
&Scoped-Define DISPLAYED-OBJECTS fTotal fcreate fPlanned fOpened fCompleted ~
fCancelled fLate fOverdue fExtend sYear fLateper fExtendper 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getvalue C-Win 
FUNCTION getvalue RETURNS CHARACTER
  (input pvalue as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bOK 
     LABEL "OK" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE sYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fCancelled AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fCompleted AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fcreate AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fExtend AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fExtendper AS DECIMAL FORMAT "ZZ,ZZ9.99":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fLate AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fLateper AS DECIMAL FORMAT "ZZ,ZZ9.99":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fOpened AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fOverdue AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fPlanned AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
      VIEW-AS TEXT 
     SIZE 12.6 BY .62
     FONT 0 NO-UNDO.

DEFINE VARIABLE fTotal AS CHARACTER FORMAT "X(256)":U INITIAL "Totals" 
      VIEW-AS TEXT 
     SIZE 9 BY .62
     FONT 0 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.4 BY 3.05.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36 BY 3.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttactionsummary SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttactionsummary.cMonth         width 8  column-label "Month"            format "x(12)"  
ttactionsummary.iAction              width 12  
ttactionsummary.iactionPlanned       width 12  
ttactionsummary.iActionOpen          width 12  
ttactionsummary.iActionComplete      width 12  
ttactionsummary.iActionCancel        width 12  
ttactionsummary.iActionOverdue       width 12
ttactionsummary.iActionLate          width 12  
ttactionsummary.iLateper             width 12 column-label "% Completed!Late" format ">>>>ZZ"
ttactionsummary.iActionExtend        width 12 
ttactionsummary.iExtendper           width 12 column-label "% Extended"       format ">>>>ZZ"
ttactionsummary.approvalStatus       width 12
ttactionsummary.approvalUser         width 25
ttactionsummary.approvalCreateDate   width 15
ttactionsummary.approvalCompleteDate width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL NO-TAB-STOP SIZE 208 BY 13.57
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 4.81 COL 2 WIDGET-ID 200
     bExport AT ROW 2.19 COL 40 WIDGET-ID 2 NO-TAB-STOP 
     fTotal AT ROW 18.48 COL 2 NO-LABEL WIDGET-ID 4
     fcreate AT ROW 18.48 COL 9.2 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     fPlanned AT ROW 18.48 COL 22 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     fOpened AT ROW 18.48 COL 34.8 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     fCompleted AT ROW 18.48 COL 47.6 COLON-ALIGNED NO-LABEL WIDGET-ID 12
     fCancelled AT ROW 18.48 COL 60.4 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     fLate AT ROW 18.48 COL 86 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     fOverdue AT ROW 18.48 COL 73.2 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     fExtend AT ROW 18.48 COL 111.6 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     sYear AT ROW 2.62 COL 9.2 COLON-ALIGNED WIDGET-ID 22
     bOK AT ROW 2.24 COL 29.4 WIDGET-ID 224
     fLateper AT ROW 18.48 COL 98.8 COLON-ALIGNED NO-LABEL WIDGET-ID 226
     fExtendper AT ROW 18.48 COL 124.6 COLON-ALIGNED NO-LABEL WIDGET-ID 228
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.24 COL 38.6 WIDGET-ID 32
     "Parameters" VIEW-AS TEXT
          SIZE 11.4 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 28
     RECT-3 AT ROW 1.48 COL 2 WIDGET-ID 36
     RECT-2 AT ROW 1.48 COL 37.6 WIDGET-ID 30
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 210.4 BY 18.76 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Action Analysis Report"
         HEIGHT             = 18.76
         WIDTH              = 210.4
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN fCancelled IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fCompleted IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fcreate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fExtend IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fExtendper IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fLate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fLateper IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fOpened IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fOverdue IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fPlanned IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fTotal IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttactionsummary.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Action Analysis Report */
or endkey of {&WINDOW-NAME} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Action Analysis Report */
DO:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Action Analysis Report */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK C-Win
ON CHOOSE OF bOK IN FRAME fMain /* OK */
DO:
  if sYear:screen-value = "0" then
  do:
    MESSAGE "By selecting all options the query could be slow. Are you sure you want to continue?"
       VIEW-AS ALERT-BOX question BUTTONS ok-cancel update lchoice as logical.
    if lChoice = true then
    do:
      run setData in this-procedure.
    end.
  end.
  else
    run setData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run wDetailedReport.w persistent (input int(sYear:screen-value), input ttactionsummary.iMonth).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain /* Year */
DO:
/* if sYear:screen-value in frame fMain  = pyear and
    cbAGent:screen-value in frame fMain = pAgent and
    cbAuditor:screen-value in frame fMain = pAuditor then
   open query brwqar for each startgapreport by startgapreport.qarid.
 else
   close query brwqar.
 run SetCount.*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.



/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

subscribe to "closeWindow" anywhere.
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bok:load-image("images/completed.bmp").
bok:load-image-insensitive("images/completed-i.bmp").
{lib/win-main.i}

do std-in = 0 to 9:
  yearpair = yearpair + string(year(today) - std-in) + "," + string(year(today) - std-in) + "," .
end.

assign
 yearpair              = trim(yearpair, ",")
 syear:list-item-pairs = yearpair
 syear:screen-value    = string(year(today)).

status default "" in window {&window-name}.
status input "" in window {&window-name}.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
 
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

APPLY "CLOSE":U TO THIS-PROCEDURE.
RETURN NO-APPLY.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fTotal fcreate fPlanned fOpened fCompleted fCancelled fLate fOverdue 
          fExtend sYear fLateper fExtendper 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData sYear bOK RECT-3 RECT-2 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 empty temp-table ttactionsummaryexp.
 def var th as handle no-undo.                  

 if query brwData:num-results = 0
 then
 do:
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
 end.
 for each ttactionsummary:
   create ttactionsummaryexp.
   assign ttactionsummaryexp.cMonth           =      string(ttactionsummary.cMonth)          
          ttactionsummaryexp.CAction          =      string(ttactionsummary.iAction)         
          ttactionsummaryexp.CactionPlanned   =      string(ttactionsummary.iactionPlanned)  
          ttactionsummaryexp.cActionOpen      =      string(ttactionsummary.iActionOpen)     
          ttactionsummaryexp.cActionComplete  =      string(ttactionsummary.iActionComplete) 
          ttactionsummaryexp.cActionCancel    =      string(ttactionsummary.iActionCancel)   
          ttactionsummaryexp.cActionLate      =      string(ttactionsummary.iActionLate)     
          ttactionsummaryexp.cActionOverdue   =      string(ttactionsummary.iActionOverdue)  
          ttactionsummaryexp.cActionExtend    =      string(ttactionsummary.iActionExtend)   
          ttactionsummaryexp.cLateper         =      string(ttactionsummary.iLateper)        
          ttactionsummaryexp.cExtendper       =      string(ttactionsummary.iExtendper).

   buffer-copy ttactionsummary to ttactionsummaryexp.
 end.
 create ttactionsummaryexp.
 assign ttactionsummaryexp.cMonth             =  fTotal:screen-value in frame fMain
        ttactionsummaryexp.CAction            =  string(fcreate:screen-value in frame fMain)    
        ttactionsummaryexp.CactionPlanned     =  string(fPlanned:screen-value in frame fMain)   
        ttactionsummaryexp.cActionOpen        =  string(fOpened:screen-value in frame fMain)    
        ttactionsummaryexp.cActionComplete    =  string(fCompleted:screen-value in frame fMain) 
        ttactionsummaryexp.cActionCancel      =  string(fCancelled:screen-value in frame fMain) 
        ttactionsummaryexp.cActionLate        =  string(fLate:screen-value in frame fMain)      
        ttactionsummaryexp.cActionOverdue     =  string(fOverdue:screen-value in frame fMain)   
        ttactionsummaryexp.cActionExtend      =  string(fExtend:screen-value in frame fMain)    
        ttactionsummaryexp.cLateper           =  string(fLateper:screen-value in frame fMain)   
        ttactionsummaryexp.cExtendper         =  string(fExtendper:screen-value in frame fMain) .

 create ttactionsummaryexp.
 assign ttactionsummaryexp.cMonth             =  cstatus.
        
        
 publish "GetReportDir" (output std-ch).
 th = temp-table ttactionsummaryexp:handle.
 run util/exporttable.p (table-handle th,
                         "ttactionsummaryexp",
                         "for each ttactionsummaryexp:",
                         "cMonth,cAction,cactionPlanned,cActionOpen,cActionComplete,cActionCancel,cActionOverdue,cActionLate,cLateper,cActionExtend,cExtendper" ,
                         "Month,#Created,# Planned,# Opened,# Completed,# Cancelled,# Currently Late,# Completed Late,% Completed Late,# Extended,% Extended",
                         std-ch,
                         "SummaryReport-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
empty temp-table ttactionsummary.

assign ivAction                     = 0
       ivactionPlanned              = 0
       ivActionComplete             = 0
       ivActionCancel               = 0
       ivActionOpen                 = 0
       ivActionLate                 = 0
       ivOverdue                    = 0
       ivExtended                   = 0.

run server/getSummary.p (input sYear:input-value in frame {&frame-name},
                         output table actionsummary,
                         output std-lo,
                         output std-ch).
if not std-lo
 then message std-ch view-as alert-box error buttons ok.
 else
  do:
    for each actionsummary:
      create ttactionsummary.
      buffer-copy actionsummary to ttactionsummary.

      assign ttactionsummary.cMonth           = entry(actionsummary.iMonth, "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec")       
             ttactionsummary.iLateper         = (if actionsummary.iActionComplete = 0 then 0 else (deci(actionsummary.iActionLate) / deci(actionsummary.iAction)) * 100)        
             ttactionsummary.iExtendper       = (if actionsummary.iActionExtend   = 0 then 0 else (actionsummary.iActionExtend / actionsummary.iAction) * 100) 
             ivAction                         = ivAction          +  actionsummary.iAction                 
             ivactionPlanned                  = ivactionPlanned   +  actionsummary.iactionPlanned 
             ivActionComplete                 = ivActionComplete  +  actionsummary.iActionComplete
             ivActionCancel                   = ivActionCancel    +  actionsummary.iActionCancel  
             ivActionOpen                     = ivActionOpen      +  actionsummary.iActionOpen    
             ivActionLate                     = ivActionLate      +  actionsummary.iActionLate    
             ivOverdue                        = ivOverdue         +  actionsummary.iActionOverdue 
             ivExtended                       = ivExtended        +  actionsummary.iActionExtend.

    end.
    open query brwData for each ttactionsummary.
    assign fcreate:screen-value in frame fMain    = string(ivAction)         
           fPlanned:screen-value in frame fMain   = string(ivactionPlanned)  
           fOpened:screen-value in frame fMain    = string(ivActionOpen)      
           fCompleted:screen-value in frame fMain = string(ivActionComplete)
           fCancelled:screen-value in frame fMain = string(ivActionCancel) 
           fLate:screen-value in frame fMain      = string(ivActionLate)     
           fOverdue:screen-value in frame fMain   = string(ivOverdue)        
           fExtend:screen-value in frame fMain    = string(ivExtended)
           fLateper:screen-value in frame fMain   = (if ivActionLate = 0  then "" else  string((ivActionLate / ivAction) * 100))
           fExtendper:screen-value in frame fMain = (if ivExtended   = 0 then "" else string((ivExtended / ivAction) * 100)).

      if can-find(first ttactionsummary) then
       enable bExport with frame fMain.
     else
       disable bExport with frame fMain.

     std-ch = "Action summary records found as of" + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM").
     status default std-ch in window {&window-name}.
     status input std-ch in window {&window-name}.
     assign cstatus =  std-ch.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 25.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 12.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getvalue C-Win 
FUNCTION getvalue RETURNS CHARACTER
  (input pvalue as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if pvalue = "0" or
     pvalue = "?" or
     pvalue = ? then
    return "".   /* Function return value. */
  else 
    return pvalue.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

