&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------
 File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Archana Gupta

  Created: 


------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input parameter piYear as int no-undo.
def input parameter piMonth as int no-undo.

/* Local Variable Definitions ---                                       */
/* {tt/qaraction.i &tableAlias="action"}  */
{lib/std-def.i}
{lib/getmonthname.i}

{tt/action.i &tableAlias="ttactionDetail"}
{tt/finding.i}
{tt/actionResults.i}

def temp-table action  like ttactionDetail
  field bExtend  as logical
  field bOverdue as logical
    .

define temp-table ttactions    like ttactionDetail.
define temp-table ttfindings   like finding.

def var chError             as char   no-undo.
def var pifindingID         as int    no-undo.
def var minSearchFrameWidth as int    no-undo.
def var iBgColor            as int    no-undo.
def var ifgColor            as int    no-undo.
def var imonth              as int    no-undo.
def var pActionID           as char   no-undo.  
def var cReason             as char   no-undo initial "test".  
def var pMsg                as char   no-undo.  
def var obFinding           as log    no-undo.
def var activerowid         as rowid  no-undo.       
def var hDocWindow          as handle no-undo.
def var actionsummaryhandle as handle no-undo.

{lib/winlaunch.i}

def temp-table openDocuments
 field findingID as int
 field hInstance as handle.

define variable valuepair as char no-undo.
def    variable yearpair  as char no-undo.
def    variable monthpair as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fSearch
&Scoped-define BROWSE-NAME brwAM

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES actionResults

/* Definitions for BROWSE brwAM                                         */
&Scoped-define FIELDS-IN-QUERY-brwAM actionResults.cfindingID actionResults.cstage actionResults.cactionID actionResults.cstat replace (actionResults.entity,"?", " ") @ actionResults.Entity replace (actionResults.entityID,"?", " ") @ actionResults.entityID replace (actionResults.entityname,"?", " ") @ actionResults.entityname replace (actionResults.ownername,"?", " ") @ actionResults.ownername replace (actionResults.Overdue,"?", " ") @ actionResults.Overdue replace (actionResults.source,"?", " ") @ actionResults.source replace (actionResults.iconMessages,"?", " ") @ actionResults.iconMessages actionResults.createdDate actionResults.dueDate actionResults.openedDate actionResults.completedDate actionResults.originalDueDate actionResults.extendedDate actionResults.followupDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAM   
&Scoped-define SELF-NAME brwAM
&Scoped-define QUERY-STRING-brwAM for each actionResults
&Scoped-define OPEN-QUERY-brwAM OPEN QUERY {&SELF-NAME} for each actionResults.
&Scoped-define TABLES-IN-QUERY-brwAM actionResults
&Scoped-define FIRST-TABLE-IN-QUERY-brwAM actionResults


/* Definitions for FRAME fSearch                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fSearch ~
    ~{&OPEN-QUERY-brwAM}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cStatus brwAM edQuestion edFinding edAction ~
cbOwner cbStage bOK sYear sMonth RECT-53 RECT-2 RECT-54 
&Scoped-Define DISPLAYED-OBJECTS fQuestion cStatus edQuestion edFinding ~
edAction fFinding fAction cbOwner cbStage sYear sMonth 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAction wWin 
FUNCTION getAction RETURNS CHARACTER
  (input actionID as int /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getstage wWin 
FUNCTION getstage RETURNS CHARACTER
  (input pcstat as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatus wWin 
FUNCTION getStatus RETURNS CHARACTER
  (input pcstat as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD repositionToCurrent wWin 
FUNCTION repositionToCurrent RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bOK 
     LABEL "OK" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE cbOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE cbStage AS CHARACTER FORMAT "X(256)":U 
     LABEL "Stage" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Active","A",
                     "Closed","C"
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE cStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Planned","P",
                     "Opened","O",
                     "Completed","C",
                     "Cancelled","X",
                     "Unaddressed","U"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE sMonth AS CHARACTER FORMAT "X(256)":U 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16.4 BY 1 NO-UNDO.

DEFINE VARIABLE edAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169 BY 2 NO-UNDO.

DEFINE VARIABLE edFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169 BY 2.

DEFINE VARIABLE edQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169 BY 2.

DEFINE VARIABLE fAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
      VIEW-AS TEXT 
     SIZE .8 BY .1 NO-UNDO.

DEFINE VARIABLE fFinding AS CHARACTER FORMAT "X(256)":U 
     LABEL "Finding" 
      VIEW-AS TEXT 
     SIZE .8 BY .1 NO-UNDO.

DEFINE VARIABLE fQuestion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Question" 
      VIEW-AS TEXT 
     SIZE 1 BY .1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12.4 BY 3.05.

DEFINE RECTANGLE RECT-53
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60 BY 3.05.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.6 BY 3.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAM FOR 
      actionResults SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAM
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAM wWin _FREEFORM
  QUERY brwAM DISPLAY
      actionResults.cfindingID                                              label  "Finding ID"   format "x(12)"
actionResults.cstage                                                        label  "Stage"        format "x(12)"  
actionResults.cactionID                                                     label  "Action ID"    format "x(12)"   
actionResults.cstat                                                         label  "Status"       format "x(15)"  
replace (actionResults.entity,"?", " ")  @ actionResults.Entity             label  "Entity"       format "x(12)"  
replace (actionResults.entityID,"?", " ")  @ actionResults.entityID         label  "ID"           
replace (actionResults.entityname,"?", " ")  @ actionResults.entityname     label  "Name"         format "x(35)" 
replace (actionResults.ownername,"?", " ")  @ actionResults.ownername       label  "Owner"        format "x(15)" 
replace (actionResults.Overdue,"?", " ")  @ actionResults.Overdue           label  "Alert"        format "x(10)" 
replace (actionResults.source,"?", " ")  @ actionResults.source             label  "Source"       format "x(12)" 
replace (actionResults.iconMessages,"?", " ")  @ actionResults.iconMessages label  "Comments"     width 20      format "x(50)" 
actionResults.createdDate                                                   label  "Created"      format "99/99/9999" 
actionResults.dueDate                                                       label  "Due"          format "99/99/9999" 
actionResults.openedDate                                                    label  "Opened"       format "99/99/9999"
actionResults.completedDate                                                 label  "Completed"    format "99/99/9999" 
actionResults.originalDueDate                                               label  "Original Due" format "99/99/9999" 
actionResults.extendedDate                                                  label  "Extended"     format "99/99/9999" 
actionResults.followupDate                                                  label  "Followup"     format "99/99/9999"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 183.4 BY 15.86
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fSearch
     bExport AT ROW 2.1 COL 103 WIDGET-ID 194 NO-TAB-STOP 
     fQuestion AT ROW 21.48 COL 14.8 COLON-ALIGNED WIDGET-ID 20
     cStatus AT ROW 3.05 COL 76.8 COLON-ALIGNED WIDGET-ID 6
     brwAM AT ROW 5 COL 185.2 RIGHT-ALIGNED WIDGET-ID 400
     edQuestion AT ROW 21.29 COL 17.6 NO-LABEL WIDGET-ID 8
     edFinding AT ROW 23.43 COL 17.6 NO-LABEL WIDGET-ID 10
     edAction AT ROW 25.57 COL 17.6 NO-LABEL WIDGET-ID 12
     fFinding AT ROW 23.62 COL 14.8 COLON-ALIGNED WIDGET-ID 22
     fAction AT ROW 25.71 COL 14.8 COLON-ALIGNED WIDGET-ID 24
     cbOwner AT ROW 1.86 COL 47.8 COLON-ALIGNED WIDGET-ID 220
     cbStage AT ROW 3.05 COL 47.8 COLON-ALIGNED WIDGET-ID 228
     bOK AT ROW 2.14 COL 30.8 WIDGET-ID 224
     sYear AT ROW 1.86 COL 9.2 COLON-ALIGNED WIDGET-ID 232
     sMonth AT ROW 3.05 COL 9.2 COLON-ALIGNED WIDGET-ID 234
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.19 COL 101.6 WIDGET-ID 32
     "Filter" VIEW-AS TEXT
          SIZE 5.2 BY .62 AT ROW 1.24 COL 42 WIDGET-ID 152
     "Parameters" VIEW-AS TEXT
          SIZE 11.4 BY .62 AT ROW 1.19 COL 3.8 WIDGET-ID 28
     RECT-53 AT ROW 1.48 COL 41 WIDGET-ID 2
     RECT-2 AT ROW 1.48 COL 100.6 WIDGET-ID 30
     RECT-54 AT ROW 1.48 COL 2.8 WIDGET-ID 230
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.1
         SIZE 187.8 BY 26.95 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Actions by month"
         HEIGHT             = 27
         WIDTH              = 188.2
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fSearch
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAM cStatus fSearch */
/* SETTINGS FOR BUTTON bExport IN FRAME fSearch
   NO-ENABLE                                                            */
/* SETTINGS FOR BROWSE brwAM IN FRAME fSearch
   ALIGN-R                                                              */
ASSIGN 
       brwAM:ALLOW-COLUMN-SEARCHING IN FRAME fSearch = TRUE
       brwAM:COLUMN-RESIZABLE IN FRAME fSearch       = TRUE.

ASSIGN 
       edAction:READ-ONLY IN FRAME fSearch        = TRUE.

ASSIGN 
       edFinding:READ-ONLY IN FRAME fSearch        = TRUE.

ASSIGN 
       edQuestion:READ-ONLY IN FRAME fSearch        = TRUE.

/* SETTINGS FOR FILL-IN fAction IN FRAME fSearch
   NO-ENABLE                                                            */
ASSIGN 
       fAction:READ-ONLY IN FRAME fSearch        = TRUE.

/* SETTINGS FOR FILL-IN fFinding IN FRAME fSearch
   NO-ENABLE                                                            */
ASSIGN 
       fFinding:READ-ONLY IN FRAME fSearch        = TRUE.

/* SETTINGS FOR FILL-IN fQuestion IN FRAME fSearch
   NO-ENABLE                                                            */
ASSIGN 
       fQuestion:READ-ONLY IN FRAME fSearch        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAM
/* Query rebuild information for BROWSE brwAM
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} for each actionResults.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAM */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Actions by month */
or endkey of {&WINDOW-NAME} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Actions by month */
DO:
  run closeWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Actions by month */
DO:
  run windowresized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wWin
ON CHOOSE OF bExport IN FRAME fSearch /* Export */
DO:
  run ActionExport in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK wWin
ON CHOOSE OF bOK IN FRAME fSearch /* OK */
DO:
  if sYear:screen-value = "ALL" or sMonth:screen-value = "ALL" then
  do:
    MESSAGE "By selecting all options the query could be slow. Are you sure you want to continue?"
       VIEW-AS ALERT-BOX question BUTTONS ok-cancel update lchoice as logical.
    if lChoice = true then
    do:
      run getData in this-procedure.
    end.
  end.
  else
    run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAM
&Scoped-define SELF-NAME brwAM
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAM wWin
ON DEFAULT-ACTION OF brwAM IN FRAME fSearch
DO:
  if not available actionResults
   then return.
   
  publish "OpenWindowForAction" (actionResults.findingID, actionResults.actionID, "dialogactionmodify.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAM wWin
ON ROW-DISPLAY OF brwAM IN FRAME fSearch
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAM wWin
ON START-SEARCH OF brwAM IN FRAME fSearch
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAM wWin
ON VALUE-CHANGED OF brwAM IN FRAME fSearch
DO:
  if not available  actionResults then
    assign edQuestion:screen-value in frame fSearch = ""
           edFinding:screen-value in frame fSearch  = ""
            edAction:screen-value in frame fSearch  = "".
  else
    assign edQuestion:screen-value in frame fSearch = (if (actionResults.sourceQuestion = ? or actionResults.sourceQuestion = "?") then "" else actionResults.sourceQuestion)
            edFinding:screen-value in frame fSearch  = (if (actionResults.description = ? or actionResults.description = "?") then "" else actionResults.description)
            edAction:screen-value in frame fSearch   = (if (actionResults.comments = ? or actionResults.comments = "?") then "" else actionResults.comments).    

  if not available actionResults then
    edAction:screen-value in frame fSearch  = "".
  else
    edAction:screen-value in frame fSearch   = (if (actionResults.comments = ? or actionResults.comments = "?") then "" else actionResults.comments).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbOwner
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbOwner wWin
ON VALUE-CHANGED OF cbOwner IN FRAME fSearch /* Owner */
DO:
  run Setdata.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStage wWin
ON VALUE-CHANGED OF cbStage IN FRAME fSearch /* Stage */
DO:
  run setData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cStatus wWin
ON VALUE-CHANGED OF cStatus IN FRAME fSearch /* Status */
DO:
  run Setdata.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sMonth wWin
ON VALUE-CHANGED OF sMonth IN FRAME fSearch /* Month */
DO:
/* if sYear:screen-value in frame fMain  = pyear and
    cbAGent:screen-value in frame fMain = pAgent and
    cbAuditor:screen-value in frame fMain = pAuditor then
   open query brwqar for each startgapreport by startgapreport.qarid.
 else
   close query brwqar.
 run SetCount.*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear wWin
ON VALUE-CHANGED OF sYear IN FRAME fSearch /* Year */
DO:
 if int(sYear:screen-value in frame fSearch)  = year(today) then
   imonth = month(today).
 else
   imonth = 12.

 do std-in = 1 to imonth:
   monthpair = monthpair + getMonthName(std-in) + "," + string(std-in) + "," .
 end.
 assign
   monthpair       = "ALL,0," + trim(monthpair, ",")
   sMonth:list-item-pairs = monthpair
   sMonth:screen-value = string(piMonth).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{&window-name}:window-state = 2.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bOK:load-image("images/completed.bmp").
bOK:load-image-insensitive("images/completed-i.bmp").
subscribe to "closeWindow" anywhere.
assign
  wWin:min-height-pixels = wWin:height-pixels
  wWin:min-width-pixels = wWin:width-pixels
  wWin:max-height-pixels = session:height-pixels
  wWin:max-width-pixels = session:width-pixels
  minSearchFrameWidth = frame fSearch:width-pixels
  .
{lib/win-main.i}
{lib/brw-main.i}

do std-in = 0 to 9:
  yearpair = yearpair + string(year(today) - std-in) + "," + string(year(today) - std-in) + "," .
end.

assign
  yearpair              = "ALL,0," + trim(yearpair, ",")
  syear:list-item-pairs = yearpair
  syear:screen-value    = "0".

{lib/get-sysprop-list.i &combo=cbOwner &appCode="'CAM'" &objAction="'ActionOwner'" &addAll=true}
{lib/get-sysprop-list.i &combo=cbStage &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Status'" &addAll=true}
{lib/get-sysprop-list.i &combo=cStatus &appCode="'CAM'" &objAction="'Action'" &objProperty="'Status'" &addAll=true}

status default "" in window {&window-name}.
status input "" in window {&window-name}.
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {&window-name}:title = {&window-name}:title + " - " + entry(piMonth, "January,February,March,April,May,June,July,August,September,October,November,December") + " " + string(piYear).
  assign cStatus:screen-value in frame fSearch = "ALL"
         cbOwner:screen-value in frame fSearch = "ALL"
         cbStage:screen-value in frame fSearch = "ALL"
         sYear:screen-value in frame fSearch  = string(piYear).
  apply "value-changed" to syear.
  run getData in this-procedure.
  {&window-name}:window-state = 3.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionExport wWin 
PROCEDURE ActionExport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.                  

 if query brwAM:num-results = 0
 then
 do:
   MESSAGE "There is nothing to export"
    VIEW-AS ALERT-BOX warning BUTTONS OK.
   return.
 end.

 publish "GetReportDir" (output std-ch).
 th = temp-table actionResults:handle.

 run util/exporttable.p (table-handle th,
                          "actionResults",
                          "for each actionResults:",
                          "findingID,cstage,actionID,cstat,entity,entityID,entityName,source,ownername,sourceQuestion,description," +
                          "comments,createdDate,startdate,openedDate,completedDate,originalDueDate,extendedDate,followupDate,dueDate,iconMessages" ,
                          "FindingID,Stage,ActionID,Status,Entity,ID,Name,Source,Owner,Question,Finding,Action," +
                          "Created Date,Start date,Opened Date,Completed Date,Original DueDate,Extended Date,Followup Date,Due Date,Comments",
                          std-ch,
                          "ActionManagement.csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow wWin 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fQuestion cStatus edQuestion edFinding edAction fFinding fAction 
          cbOwner cbStage sYear sMonth 
      WITH FRAME fSearch IN WINDOW wWin.
  ENABLE cStatus brwAM edQuestion edFinding edAction cbOwner cbStage bOK sYear 
         sMonth RECT-53 RECT-2 RECT-54 
      WITH FRAME fSearch IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fSearch}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData wWin 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  std-in = 0.
  empty temp-table action.
  empty temp-table finding.
  
  run server/getdetailactions.p (input int(sYear:screen-value in frame fSearch),
                                 input int(sMonth:screen-value in frame fSearch),
                                 output table action,
                                 output table finding,
                                 output std-lo,
                                 output std-ch).
  
  if not std-lo
   then message std-ch view-as alert-box error.
   else run setData.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MainWindowSensitive wWin 
PROCEDURE MainWindowSensitive :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:sensitive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setdata wWin 
PROCEDURE setdata :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 empty temp-table actionResults.
 std-in = 0.
 for each finding 
     where  finding.stat   = (if cbStage:screen-value in frame fSearch = "All"  then finding.stat else cbStage:screen-value in frame fSearch):

   if not can-find(first action where action.findingID = finding.findingID) then
   do:
     create actionResults.
     buffer-copy finding to actionResults. 
     assign
       actionResults.cfindingId = string(finding.findingId)
       actionResults.stat       = "U"
       actionResults.cstat      = getstatus(actionResults.stat).     
     std-in = std-in + 1.
   end.
   else
     for each action
       where action.findingID = finding.findingID
         and action.stat      = (if cStatus:screen-value in frame fSearch = "All" then action.stat else cStatus:screen-value in frame fSearch )
         and action.ownerId   = (if cbOwner:screen-value in frame fSearch = "All" then action.ownerId else cbOwner:screen-value in frame fSearch):

        create actionResults.
        buffer-copy finding to actionResults.
        buffer-copy action to actionResults.
        assign
            actionResults.cactionId    = string(action.actionId)
            actionResults.cfindingId   = string(action.findingId)
            actionResults.iconMessages = trim(action.iconMessages,",")
            actionResults.source       = finding.source
            actionResults.cstat        = getstatus(action.stat)     
            actionResults.stage        = finding.stat
            actionResults.cstage       = getstage(finding.stat).  
        publish "GetSysUserName" (actionResults.uid, output actionResults.ownername).
        
        if action.bOverdue = yes then
          actionResults.Overdue = "Overdue".
        else if action.bExtend = yes then
          actionResults.Overdue = "Extend".
        else 
          actionResults.Overdue = "".

        std-in = std-in + 1.
     end.   
 end.
 temp-table actionResults:write-xml("file","actionResults.xml").

 if can-find(first actionResults) then
   enable bExport with frame fSearch.
 else
   disable bExport with frame fSearch.
 std-ch = string(std-in) + " " + if std-in = 1 then "record found" else "record(s) found as of" + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM").
 status default std-ch in window {&window-name}.
 status input std-ch in window {&window-name}.
 
 open query brwAM for each actionResults 
                    where actionResults.stage    = (if cbStage:screen-value in frame fSearch = "All" then actionResults.stage else cbStage:screen-value in frame fSearch )
                      and actionResults.stat     = (if cStatus:screen-value in frame fSearch = "All" then actionResults.stat else cStatus:screen-value in frame fSearch )                      
                      and actionResults.ownerId  = (if cbOwner:screen-value in frame fSearch = "All"   then actionResults.ownerId else cbOwner:screen-value in frame fSearch).
apply "value-changed" to brwAM.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i }
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fSearch:width-pixels = wWin:width-pixels.
 frame fSearch:virtual-width-pixels = wWin:width-pixels.
 frame fSearch:height-pixels = wWin:height-pixels.
 frame fSearch:virtual-height-pixels = wWin:height-pixels.

 /* fSearch components */
 browse brwAM:width-pixels = frame fSearch:width-pixels - 16.
 edQuestion:width-pixel = frame fSearch:width-pixels - 92 no-error .
 edFinding:width-pixel  = frame fSearch:width-pixels - 92 no-error .
 edAction:width-pixel   = frame fSearch:width-pixels - 92 no-error .


 if {&window-name}:width-pixels > frame fSearch:width-pixels 
  then
   do: 
      frame fSearch:width-pixels = {&window-name}:width-pixels.
      frame fSearch:virtual-width-pixels = {&window-name}:width-pixels.
         
   end.
 else
   do:
     frame fSearch:virtual-width-pixels = {&window-name}:width-pixels.
     frame fSearch:width-pixels = {&window-name}:width-pixels.
     /* das: For some reason, shrinking a window size MAY cause the horizontal
        scroll bar.  The above sequence of widget setting should resolve it,
        but it doesn't every time.  So... */    
   end.

 browse brwAM:height-pixels = frame fSearch:height-pixels - 230.
 edQuestion:y  =   frame fSearch:height-pixels - 45 no-error.
 edFinding:y   =   frame fSearch:height-pixels - 90 no-error.
 edAction:y    =   frame fSearch:height-pixels - 135 no-error.

 fQuestion:side-label-handle:y  =   frame fSearch:height-pixels - 135 no-error.
 fFinding:side-label-handle:y   =   frame fSearch:height-pixels - 90 no-error.
 fAction:side-label-handle:y    =   frame fSearch:height-pixels - 45 no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAction wWin 
FUNCTION getAction RETURNS CHARACTER
  (input actionID as int /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if actionID = 0 then
    return "".   /* Function return value. */
  else
    return string(actionID).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getstage wWin 
FUNCTION getstage RETURNS CHARACTER
  (input pcstat as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

if pcstat = "A" then
  return "Active".   /* Function return value. */
else if pcstat = "C" then
  return "Close".   /* Function return value. */ 
else return "".   /* Function return value. */ 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatus wWin 
FUNCTION getStatus RETURNS CHARACTER
  (input pcstat as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

if pcstat = "P" then
  return "Planned".   /* Function return value. */
else if pcstat = "O" then
  return "Open".   /* Function return value. */ 
else if pcstat = "C" then
  return "Completed".   /* Function return value. */ 
else if pcstat = "X" then
  return "Cancelled".   /* Function return value. */ 
else if pcstat = "U" then
  return "Unaddressed".   /* Function return value. */ 
else if pcstat = "" then
  return "".   /* Function return value. */ 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION repositionToCurrent wWin 
FUNCTION repositionToCurrent RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if available ttactions and rowid(ttactions) = activeRowid 
  then return true.
 reposition brwAM to rowid activeRowid no-error.
 get next brwAM.
 return available ttactions.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

