&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogactionestablish.w

  Description: changes the state of action to "Open(O)".

  Input Parameters:
  @param piActionID;int;The ActionID of action to be established(Required). 

  Output Parameters:
  @param pcSuccess;logical;success message.

  Author:Sachin Chaturvedi

  Created: 08/28/2017
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input-output parameter pOwner    as character no-undo.
define input-output parameter pStart    as datetime no-undo.
define input-output parameter pFollowup as datetime no-undo.
define output       parameter pDue      as datetime no-undo.
define output       parameter pCancel   as logical no-undo initial true.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* Functions ---                                                        */
{lib/add-delimiter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbOwner tStartDate tDueDate tFollowupDate ~
bCancel 
&Scoped-Define DISPLAYED-OBJECTS cbOwner tStartDate tDueDate tFollowupDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bEstablish 
     LABEL "OK" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tFollowupDate AS DATE FORMAT "99/99/99":U 
     LABEL "Followup" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Start" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbOwner AT ROW 1.48 COL 11 COLON-ALIGNED WIDGET-ID 22
     tStartDate AT ROW 2.67 COL 11 COLON-ALIGNED WIDGET-ID 34
     tDueDate AT ROW 3.86 COL 11 COLON-ALIGNED WIDGET-ID 28
     tFollowupDate AT ROW 5.05 COL 11 COLON-ALIGNED WIDGET-ID 36
     bEstablish AT ROW 6.24 COL 12.8 WIDGET-ID 20
     bCancel AT ROW 6.24 COL 29 WIDGET-ID 18
     SPACE(1.79) SKIP(0.32)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Mark as Opened" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bEstablish IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Mark as Opened */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEstablish
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEstablish Dialog-Frame
ON CHOOSE OF bEstablish IN FRAME Dialog-Frame /* OK */
DO:
  std-ch = "".
  /* validate Start Date */
  if tStartDate:input-value = ? 
   then std-ch = addDelimiter(std-ch, chr(10)) + " Start Date not valid".

  /* validate Due Date */
  if tDueDate:input-value = ? 
   then std-ch = addDelimiter(std-ch, chr(10)) + " Due Date not valid".

  if std-ch > ""  
   then 
    do:
     message std-ch view-as alert-box info buttons OK.
     return no-apply.
    end.

  if interval(tDueDate:input-value, tStartDate:input-value, "days") < 0
   then std-ch = addDelimiter(std-ch, chr(10)) + "Due Date must be greater than Start Date.".

  if interval(tDueDate:input-value, today, "days") < 0
   then std-ch = addDelimiter(std-ch, chr(10)) + "Due Date must be greater than Today.".

  if std-ch > ""  
   then 
    do:
     message std-ch view-as alert-box info buttons OK.
     return no-apply.
    end.

  /* followup Date validation */
  if not tFollowupDate:input-value = ?
   then
    do:
      if interval(tFollowupDate:input-value, tDueDate:input-value, "days") > 0
       then std-ch = addDelimiter(std-ch, chr(10)) + "Followup Date must be less than Due Date".

      if interval(tFollowupDate:input-value, today, "days") < 0
       then std-ch = addDelimiter(std-ch, chr(10)) + "Followup Date must be greater than Today.".
    end.

  if std-ch > ""  
   then 
    do:
     message std-ch view-as alert-box info buttons OK.
     return no-apply.
    end.

  assign 
    pOwner    = cbOwner:screen-value
    pStart    = tStartDate:input-value
    pDue      = tDueDate:input-value
    pFollowup = tFollowupDate:input-value
    pCancel   = false
    .
  apply "CHOOSE" to bCancel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDueDate Dialog-Frame
ON LEAVE OF tDueDate IN FRAME Dialog-Frame /* Due */
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFollowupDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFollowupDate Dialog-Frame
ON LEAVE OF tFollowupDate IN FRAME Dialog-Frame /* Followup */
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStartDate Dialog-Frame
ON LEAVE OF tStartDate IN FRAME Dialog-Frame /* Start */
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
{lib/get-sysprop-list.i &combo=cbOwner &appCode="'CAM'" &objAction="'ActionOwner'" &d=pOwner}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
 
  RUN enable_UI.
  
  if pStart <> ?
   then tStartDate:screen-value = string(pStart, "99/99/99").
   
  if pFollowup <> ?
   then tFollowupDate:screen-value = string(pFollowup, "99/99/99").
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableOK Dialog-Frame 
PROCEDURE enableOK :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    bEstablish:sensitive = (not tStartDate:input-value = ? and
                            not tDueDate:input-value = ?).
  end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbOwner tStartDate tDueDate tFollowupDate 
      WITH FRAME Dialog-Frame.
  ENABLE cbOwner tStartDate tDueDate tFollowupDate bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

