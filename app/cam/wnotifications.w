&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{tt/findingaction.i}
/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName="findingaction"}
define temp-table ttAct
  field actionID  as int
  field lastNote  as char.

define variable iBgColor     as integer no-undo.
define variable piFindingID  as integer no-undo.
define variable iNumRecords  as integer no-undo.
define variable chStat       as character no-undo.
define variable valuepair    as character no-undo.
define variable cActionList  as character no-undo.
define variable flag         as character no-undo.
define variable dColumnWidth as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwNotify

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES findingaction

/* Definitions for BROWSE brwNotify                                     */
&Scoped-define FIELDS-IN-QUERY-brwNotify findingaction.findingID findingaction.actionID findingaction.statDesc findingaction.entity findingaction.entityID findingaction.ownerDesc findingaction.startDate findingaction.notificationDate findingaction.dueDate findingaction.dueIn findingaction.sendNotifications   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwNotify findingaction.sendNotifications   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwNotify findingaction
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwNotify findingaction
&Scoped-define SELF-NAME brwNotify
&Scoped-define QUERY-STRING-brwNotify for each findingaction
&Scoped-define OPEN-QUERY-brwNotify open query {&SELF-NAME} for each findingaction.
&Scoped-define TABLES-IN-QUERY-brwNotify findingaction
&Scoped-define FIRST-TABLE-IN-QUERY-brwNotify findingaction


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwNotify}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bGo fStatus fDueIn bExport bEmail tSelect ~
brwNotify fRequestorComm RECT-4 RECT-5 RECT-2 
&Scoped-Define DISPLAYED-OBJECTS fStatus fDueIn tSelect fText ~
fRequestorComm 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doFilterSort Procedure
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAction C-Win 
FUNCTION getAction RETURNS CHARACTER
  (input actionID as int /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeitemfromlist C-Win 
FUNCTION removeitemfromlist RETURNS CHARACTER
  ( pc-list as character,
     pc-removeitem as character,
     pc-delimiter as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bEmail 
     LABEL "Email" 
     SIZE 7.2 BY 1.71 TOOLTIP "Send Reminders".

DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bGo 
     LABEL "OK" 
     SIZE 7.2 BY 1.71 TOOLTIP "Click to get actions".

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Planned","P",
                     "Opened","O"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fRequestorComm AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 63.6 BY 1.71 NO-UNDO.

DEFINE VARIABLE fDueIn AS INTEGER FORMAT "->>>>>":U INITIAL 0 
     LABEL "Due in (days)" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE fText AS CHARACTER FORMAT "X(256)":U INITIAL "Email Comments:" 
      VIEW-AS TEXT 
     SIZE 16 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100.2 BY 2.81.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 10.6 BY 2.81.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 49 BY 2.81.

DEFINE VARIABLE tSelect AS LOGICAL INITIAL no 
     LABEL "Select ALL" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.8 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwNotify FOR 
      findingaction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwNotify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwNotify C-Win _FREEFORM
  QUERY brwNotify DISPLAY
      findingaction.findingID width 12
findingaction.actionID width 12
findingaction.statDesc width 12
findingaction.entity width 12
findingaction.entityID width 12
findingaction.ownerDesc width 40
findingaction.startDate width 15
findingaction.notificationDate width 15
findingaction.dueDate width 15
findingaction.dueIn width 10
findingaction.sendNotifications width 7 view-as toggle-box
enable findingaction.sendNotifications
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 174 BY 12.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bGo AT ROW 2 COL 3.6 WIDGET-ID 224
     fStatus AT ROW 2.38 COL 19 COLON-ALIGNED WIDGET-ID 242
     fDueIn AT ROW 2.38 COL 50 COLON-ALIGNED WIDGET-ID 232
     bExport AT ROW 2 COL 62.4 WIDGET-ID 236
     bEmail AT ROW 2.05 COL 152 WIDGET-ID 4
     tSelect AT ROW 3.52 COL 162 WIDGET-ID 2
     brwNotify AT ROW 4.57 COL 2 WIDGET-ID 200
     fText AT ROW 2.52 COL 69 COLON-ALIGNED NO-LABEL WIDGET-ID 240
     fRequestorComm AT ROW 2.05 COL 87.4 NO-LABEL WIDGET-ID 238
     "Filter" VIEW-AS TEXT
          SIZE 5.2 BY .62 AT ROW 1.24 COL 13.2 WIDGET-ID 152
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 61.8 WIDGET-ID 32
     RECT-4 AT ROW 1.48 COL 2 WIDGET-ID 226
     RECT-5 AT ROW 1.48 COL 12.2 WIDGET-ID 234
     RECT-2 AT ROW 1.48 COL 60.8 WIDGET-ID 30
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 176 BY 16.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Reminders"
         HEIGHT             = 16.33
         WIDTH              = 176
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwNotify tSelect DEFAULT-FRAME */
ASSIGN 
       brwNotify:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR FILL-IN fText IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       fText:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwNotify
/* Query rebuild information for BROWSE brwNotify
     _START_FREEFORM
open query {&SELF-NAME} for each findingaction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwNotify */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Reminders */
or endkey of {&WINDOW-NAME} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Reminders */
do:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Reminders */
DO:
  run windowresized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEmail C-Win
ON CHOOSE OF bEmail IN FRAME DEFAULT-FRAME /* Email */
do:
  empty temp-table ttAct no-error.
  for each findingaction where findingaction.sendnotifications = true:
    if not can-find(first ttAct where ttAct.actionID = findingaction.actionID)
     then
      do:
        create ttAct.
        assign ttAct.actionID = findingaction.actionID
               ttAct.lastNote = findingaction.lastNote. 
        iNumRecords = iNumRecords + 1.
      end.
  end.
  run notify.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME DEFAULT-FRAME /* OK */
do:   
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwNotify
&Scoped-define SELF-NAME brwNotify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON DEFAULT-ACTION OF brwNotify IN FRAME DEFAULT-FRAME
do:
  if not available findingaction or findingaction.actionID = 0
   then return.
   
  publish "OpenWindowForAction" (findingaction.findingID, findingaction.actionID, "dialogactionmodify.w").
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON ROW-DISPLAY OF brwNotify IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON ROW-ENTRY OF brwNotify IN FRAME DEFAULT-FRAME
do:
  find current findingaction no-error.  
  if available findingaction 
   then 
    do:
    fRequestorComm:read-only = not logical(findingaction.sendNotifications:screen-value in browse brwNotify).
     if logical(findingaction.sendNotifications:screen-value in browse brwNotify) = true 
      then
       do:
         if lookup(string(findingaction.actionID),flag,",") eq 0 then
         flag = flag + "," + string(findingaction.actionID).
       end.
     else
       do:
         flag = RemoveItemFromList(flag,string(findingaction.actionID),",").
       end.
    end.
  run setbutton. 
  apply "ROW-LEAVE" to browse {&browse-name}.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON ROW-LEAVE OF brwNotify IN FRAME DEFAULT-FRAME
do:
  find current findingaction no-error.
  if available findingaction then
    assign 
      findingaction.sendnotifications = logical(findingaction.sendnotifications:screen-value in browse brwNotify)
      fRequestorComm:read-only = not logical(findingaction.sendnotifications:screen-value in browse brwNotify)
      .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON START-SEARCH OF brwNotify IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotify C-Win
ON VALUE-CHANGED OF brwNotify IN FRAME DEFAULT-FRAME
DO:
  if not available findingaction
   then return.
  
  fRequestorComm:read-only = not logical(findingaction.sendNotifications:screen-value in browse brwNotify).
  fRequestorComm:screen-value = "".
  for first ttAct no-lock
      where ttAct.actionID = findingaction.actionID:
    
    if logical(findingaction.sendNotifications:screen-value in browse brwNotify)
     then fRequestorComm:screen-value = ttAct.lastNote.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDueIn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDueIn C-Win
ON RETURN OF fDueIn IN FRAME DEFAULT-FRAME /* Due in (days) */
do:
  dynamic-function("setFilterCombos", self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRequestorComm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRequestorComm C-Win
ON LEAVE OF fRequestorComm IN FRAME DEFAULT-FRAME
DO:
  apply "RETURN" to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRequestorComm C-Win
ON RETURN OF fRequestorComm IN FRAME DEFAULT-FRAME
DO:
  if not available findingaction
   then return.
  
  if not can-find(first ttAct where ttAct.actionID = findingaction.actionID)
   then
    do:
      create ttAct.
      ttAct.actionID = findingaction.actionID.
      iNumRecords = iNumRecords + 1.
    end.
    
  for first ttAct exclusive-lock
      where ttAct.actionID = findingaction.actionID:

    ttAct.lastNote = self:screen-value.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME DEFAULT-FRAME /* Status */
do:
  dynamic-function("setFilterCombos", self:label).
  dataSortDesc = not dataSortDesc.
  run sortData in this-procedure (dataSortBy).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSelect
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelect C-Win
ON VALUE-CHANGED OF tSelect IN FRAME DEFAULT-FRAME /* Select ALL */
do:
  if tSelect:checked = true 
   then
    do:
      bEmail:sensitive = true. 
      if fDueIn:input-value = 0 
       then 
        do:
          for each findingaction where findingaction.stat = if fStatus:screen-value eq "ALL" then findingaction.stat else fStatus:screen-value in frame default-frame:
            findingaction.sendnotifications = true.
         
            if lookup(string(findingaction.actionID),flag,",") eq 0 
             then
              flag = flag + "," + string(findingaction.actionID).
          end.
        end.
      else 
        do:
          for each findingaction where ((date(findingaction.dueDate) - today) le int(fDueIn:screen-value in frame default-frame))
                             and (findingaction.stat = if fStatus:screen-value eq "ALL" then findingaction.stat else fStatus:screen-value):                                    
            findingaction.sendnotifications = true.
         
            if lookup(string(findingaction.actionID),flag,",") eq 0 then
            flag = flag + "," + string(findingaction.actionID).
          end.
        end. 
    end.

  else 
    do: /* when tSelect:checked = false */
      flag = "".
      run setbutton.
      if fDueIn:input-value = 0 then 
      do:
        for each findingaction where findingaction.stat = if fStatus:screen-value eq "ALL" then findingaction.stat else fStatus:screen-value in frame default-frame:
          findingaction.sendnotifications = false.    
        end.
      end.
      else do:
        for each findingaction where ((date(findingaction.dueDate) - today) le int(fDueIn:screen-value in frame default-frame))
                            and (findingaction.stat = if fStatus:screen-value eq "ALL" then findingaction.stat else fStatus:screen-value):                                    
          findingaction.sendnotifications = false.
        end.
      end.
  end.
  brwNotify:refresh() no-error.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-status.i &entity="'Action'"}
{lib/brw-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/set-button.i &label="Go"     &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-filter.i &label="Status" &id="stat" &value="statDesc"}
setButtons().

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
bEmail:load-image("images/email.bmp").
bEmail:load-image-insensitive("images/email-i.bmp").
  
on 'value-changed':U of findingaction.sendNotifications 
do:
  apply 'row-entry' to brwNotify in frame default-frame.
  apply 'value-changed' to brwNotify in frame default-frame.
  /* apply 'row-leave' to brwNotify in frame default-frame. */
end.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  clearStatus().
  enableButtons(false).
  enableFilters(false).
  assign
    fRequestorComm:screen-value = ""
    fRequestorComm:read-only    = true
    tSelect:sensitive           = false
    bEmail:sensitive            = false
    fDueIn:screen-value         = "0"
    fDueIn:read-only            = true
    . 

  {lib/get-column-width.i &col="'ownerDesc'" &var=dColumnWidth}

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fStatus fDueIn tSelect fText fRequestorComm 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bGo fStatus fDueIn bExport bEmail tSelect brwNotify fRequestorComm 
         RECT-4 RECT-5 RECT-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwNotify:num-results = 0 
   then
    do: 
      MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
    end.

  &scoped-define ReportName "Notifications"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(brwNotify), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(brwNotify), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dStartTime as datetime no-undo.
  
  empty temp-table findingaction.
  dStartTime = now.
  run server/getfindingactions.p (input "P,O",
                                  input "A",
                                  input 0,
                                  input 0,
                                  input "", /*entity*/
                                  input "", /*entityID*/
                                  input "",
                                  input 0,
                                  input 0,
                                  output table findingaction,
                                  output std-lo,
                                  output std-ch).
  if not std-lo
   then message std-ch view-as alert-box warning.
   else
    do with frame {&frame-name}:
      for each findingaction exclusive-lock:
        publish "GetSysUserName" (findingaction.ownerID, output findingaction.ownerDesc).
        publish "GetSysUserName" (findingaction.findingUID, output findingaction.findingUIDDesc).
        publish "GetSysUserName" (findingaction.actionUID, output findingaction.actionUIDDesc).
        publish "GetSysPropDesc" ("CAM", "Finding", "Status", findingaction.stage, output findingaction.stageDesc).
        publish "GetSysPropDesc" ("CAM", "Finding", "Severity", findingaction.severity, output findingaction.severityDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "ActionType", findingaction.actionType, output findingaction.actionTypeDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "Status", findingaction.stat, output findingaction.statDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "CompleteStatus", findingaction.completeStatus, output findingaction.completeStatusDesc).
        findingaction.dueIn = interval(findingaction.dueDate, today, "days").
      end.
      assign
        fDueIn:screen-value = "0"
        tSelect:sensitive   = true
        fStatus:sensitive   = true 
        fDueIn:read-only    = false
        .
      setFilterCombos("ALL").
      dataSortBy = "".
      dataSortDesc = no.
      run sortData ("severity").
      appendStatus("in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE notify C-Win 
PROCEDURE notify PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var iCountMails as int no-undo.
  def var lSuccess    as log no-undo.
  def var pcMsg       as char no-undo.
  def var icnt        as int.

  publish "NotifyOwner" (input table ttAct,
                         output iCountMails,
                         output lSuccess).
                         
  MESSAGE iCountMails " out of " iNumRecords " mails sent." VIEW-AS ALERT-BOX INFO BUTTONS OK.

  /* to uncheck all */
  for each findingaction exclusive-lock:
    findingaction.sendnotifications = false. 
    findingaction.sendnotifications:checked in browse brwNotify = findingaction.sendnotifications. 
  end.
  browse brwNotify:refresh() no-error.
  apply "value-changed" to fDueIn in frame {&frame-name}.

  tselect:checked = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButton C-Win 
PROCEDURE setButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if flag ne "" 
  then
   assign       
    bEmail:sensitive in frame {&frame-name} = true.
 else
   assign
    bEmail:sensitive in frame {&frame-name} = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.

  tWhereClause = getWhereClause().
  do with frame {&frame-name}:
    if fDueIn:screen-value <> ""
     then 
      do:
        if tWhereClause > ""
         then tWhereClause = tWhereClause + " and ".
         else tWhereClause = tWhereClause + " where ".
        tWhereClause = tWhereClause + "dueIn < " + fDueIn:screen-value.
      end.
  end.
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}

  enableButtons(can-find(first findingaction)).
  enableFilters(can-find(first findingaction)).
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowresized C-Win 
PROCEDURE windowresized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10. 
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  {lib/resize-column.i &col="'ownerDesc'" &var=dColumnWidth}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doFilterSort Include 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  define variable tDueIn       as integer   no-undo.
  define variable tStatus      as character no-undo.

  do with frame {&frame-name}:
    assign
      tDueIn = fDueIn:input-value
      tStatus = fStatus:input-value
      .
    /* build the query */
    if tDueIn > 0
     then tWhereClause = addDelimiter(tWhereClause, " and ") + "dueIn = " + string(tDueIn).
    if tStatus <> "ALL"
     then tWhereClause = addDelimiter(tWhereClause, " and ") + "stat = '" + tStatus + "'".
    if tWhereClause > ""
     then tWhereClause = "where " +  tWhereClause.
  end.
  RETURN tWhereClause.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAction C-Win 
FUNCTION getAction RETURNS CHARACTER
  (input actionID as int /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if actionID = 0 then
    return "".   /* Function return value. */
  else
    return string(actionID).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeitemfromlist C-Win 
FUNCTION removeitemfromlist RETURNS CHARACTER
  ( pc-list as character,
     pc-removeitem as character,
     pc-delimiter as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable li-pos as integer no-undo.
  li-pos = lookup(pc-removeitem,pc-list,pc-delimiter).
  if li-pos > 0 then
  do:
      assign entry(li-pos,pc-list,pc-delimiter) = "".
      pc-list = trim(replace(pc-list,pc-delimiter + pc-delimiter,pc-delimiter),pc-delimiter).
  end.
  return pc-list.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

