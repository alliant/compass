&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------
 File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Archana Gupta

  Created: 


------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
/* {tt/qaraction.i &tableAlias="action"}  */
{lib/std-def.i}

{tt/action.i}
{tt/finding.i}
{tt/action.i &tableAlias="tempaction"}
{tt/finding.i &tableAlias="tempfinding"}
{tt/findingaction.i}
{tt/openwindow.i}

{tt/agent.i}
{tt/attorney.i}

define variable hDocWindow as handle no-undo.
define variable hNewWindow as handle no-undo.
define variable hConfig as handle no-undo.

{lib/winlaunch.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName="findingaction"}

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fSearch

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tActionLabel tFindingLabel tQuestionLabel ~
fSeverity brwData edQuestion edFinding edAction fEntity cbSubject fOwner ~
bClose bModify bNew bManageAction bDocs bExport bGo tStatus RECT-53 RECT-43 ~
RECT-54 
&Scoped-Define DISPLAYED-OBJECTS tActionLabel tFindingLabel tQuestionLabel ~
fSeverity edQuestion edFinding edAction fEntity cbSubject fOwner tStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */



&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fSearch

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bNew tActionLabel tFindingLabel ~
tQuestionLabel fSeverity edQuestion edFinding edAction fEntity ~
cbSubject fOwner bClose bModify bManageAction bDocs bExport bGo tStatus ~
RECT-53 RECT-43 RECT-54 
&Scoped-Define DISPLAYED-OBJECTS tActionLabel tFindingLabel tQuestionLabel ~
fSeverity edQuestion edFinding edAction fEntity cbSubject fOwner tStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fSearch

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bConfig bNew tActionLabel tFindingLabel ~
tQuestionLabel fSeverity edQuestion edFinding edAction fEntity cbSubject ~
fOwner bClose bModify bManageAction bDocs bExport bGo tStatus RECT-53 ~
RECT-43 RECT-54 
&Scoped-Define DISPLAYED-OBJECTS tActionLabel tFindingLabel tQuestionLabel ~
fSeverity edQuestion edFinding edAction fEntity cbSubject fOwner tStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData wWin 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createQuery wWin 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_File 
       MENU-ITEM m_Configuration LABEL "Configure"     
       MENU-ITEM m_About        LABEL "About"         
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_Action 
       MENU-ITEM m_Refresh      LABEL "Refresh"        ACCELERATOR "ALT-R"
       MENU-ITEM m_Export       LABEL "Export"         ACCELERATOR "ALT-E"
       RULE
       MENU-ITEM m_New          LABEL "New Finding"    ACCELERATOR "ALT-N"
       MENU-ITEM m_Edit         LABEL "Edit Finding/Actions" ACCELERATOR "ALT-F"
       MENU-ITEM m_Documents    LABEL "Documents"      ACCELERATOR "ALT-D"
       MENU-ITEM m_Close        LABEL "Close finding"  ACCELERATOR "ALT-X"
       RULE
       MENU-ITEM m_ManageAction LABEL "Manage Action"  ACCELERATOR "ALT-M".

DEFINE SUB-MENU m_References 
       MENU-ITEM m_My_References LABEL "My References" 
       MENU-ITEM m_Agents       LABEL "Agents"        
       MENU-ITEM m_Departments  LABEL "Departments"   
       MENU-ITEM m_States       LABEL "States"        
       MENU-ITEM m_Attorney     LABEL "Attorneys"     .

DEFINE SUB-MENU m_Report 
       MENU-ITEM m_Summary_Report LABEL "Summary Report"
       MENU-ITEM m_Actions_By_Month LABEL "Action Approval"
       RULE
       MENU-ITEM m_Notifications LABEL "Notifications" .

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_File         LABEL "Module"        
       SUB-MENU  m_Action       LABEL "Ac&tion"       
       SUB-MENU  m_References   LABEL "References"    
       SUB-MENU  m_Report       LABEL "Reports"       .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bClose  NO-FOCUS
     LABEL "Close" 
     SIZE 7.2 BY 1.71 TOOLTIP "Close Finding".

DEFINE BUTTON bConfig  NO-FOCUS
     LABEL "Config" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add/Remove columns".

DEFINE BUTTON bDocs  NO-FOCUS
     LABEL "Documents" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh".

DEFINE BUTTON bManageAction  NO-FOCUS
     LABEL "Manage Action" 
     SIZE 7.2 BY 1.71 TOOLTIP "Manage Action".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit Finding/Actions".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New Finding".

DEFINE VARIABLE cbSubject AS CHARACTER INITIAL "ALL" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN AUTO-COMPLETION
     SIZE 58 BY 1 NO-UNDO.

DEFINE VARIABLE fEntity AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fOwner AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 50.6 BY 1 NO-UNDO.

DEFINE VARIABLE fSeverity AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Severity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 16.4 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE edAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169 BY 2 NO-UNDO.

DEFINE VARIABLE edFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169.2 BY 2.

DEFINE VARIABLE edQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 169.2 BY 2.

DEFINE VARIABLE tActionLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Actions:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE tFindingLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Finding:" 
      VIEW-AS TEXT 
     SIZE 8 BY .62 NO-UNDO.

DEFINE VARIABLE tQuestionLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Question:" 
      VIEW-AS TEXT 
     SIZE 9.2 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-43
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56 BY 3.1.

DEFINE RECTANGLE RECT-53
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92.4 BY 3.1.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 35.4 BY 3.1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fSearch
     bConfig AT ROW 2.19 COL 175.4 WIDGET-ID 246 NO-TAB-STOP 
     bNew AT ROW 2.19 COL 138.4 WIDGET-ID 204 NO-TAB-STOP 
     tActionLabel AT ROW 25.43 COL 5.4 COLON-ALIGNED NO-LABEL WIDGET-ID 244
     tFindingLabel AT ROW 23.29 COL 5.6 COLON-ALIGNED NO-LABEL WIDGET-ID 242
     tQuestionLabel AT ROW 21.14 COL 4 COLON-ALIGNED NO-LABEL WIDGET-ID 236
     fSeverity AT ROW 3.14 COL 46 COLON-ALIGNED WIDGET-ID 6
     edQuestion AT ROW 20.95 COL 15.8 NO-LABEL WIDGET-ID 8
     edFinding AT ROW 23.1 COL 15.8 NO-LABEL WIDGET-ID 10
     edAction AT ROW 25.24 COL 16 NO-LABEL WIDGET-ID 12
     fEntity AT ROW 1.95 COL 46 COLON-ALIGNED WIDGET-ID 26
     cbSubject AT ROW 1.95 COL 67 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     fOwner AT ROW 3.14 COL 74.4 COLON-ALIGNED WIDGET-ID 220
     bClose AT ROW 2.19 COL 160.6 WIDGET-ID 202 NO-TAB-STOP 
     bModify AT ROW 2.19 COL 145.8 WIDGET-ID 184 NO-TAB-STOP 
     bManageAction AT ROW 2.19 COL 168 WIDGET-ID 210 NO-TAB-STOP 
     bDocs AT ROW 2.19 COL 153.2 WIDGET-ID 188 NO-TAB-STOP 
     bExport AT ROW 2.19 COL 131 WIDGET-ID 194 NO-TAB-STOP 
     bGo AT ROW 2.19 COL 28.6 WIDGET-ID 206 NO-TAB-STOP 
     tStatus AT ROW 2.52 COL 9 COLON-ALIGNED WIDGET-ID 234
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 38 WIDGET-ID 222
     "Actions" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.24 COL 130 WIDGET-ID 224
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 228
     RECT-53 AT ROW 1.48 COL 37 WIDGET-ID 2
     RECT-43 AT ROW 1.48 COL 129 WIDGET-ID 198
     RECT-54 AT ROW 1.48 COL 2 WIDGET-ID 226
     SPACE(147.60) SKIP(16.18)
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE  WIDGET-ID 100.

DEFINE FRAME fBrowse
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 4.81
         SIZE 183 BY 15.95 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 26.52
         WIDTH              = 184.8
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fBrowse:FRAME = FRAME fSearch:HANDLE.

/* SETTINGS FOR FRAME fBrowse
                                                                        */
/* SETTINGS FOR FRAME fSearch
   FRAME-NAME Size-to-Fit Custom                                        */
ASSIGN 
       FRAME fSearch:HEIGHT           = 26.52
       FRAME fSearch:WIDTH            = 184.8.

ASSIGN 
       bClose:PRIVATE-DATA IN FRAME fSearch     = 
                "P".

ASSIGN 
       bGo:PRIVATE-DATA IN FRAME fSearch     = 
                "Q".

ASSIGN 
       bNew:PRIVATE-DATA IN FRAME fSearch     = 
                "A".

ASSIGN 
       edAction:READ-ONLY IN FRAME fSearch        = TRUE.

ASSIGN 
       edFinding:READ-ONLY IN FRAME fSearch        = TRUE.

ASSIGN 
       edQuestion:READ-ONLY IN FRAME fSearch        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin
or endkey of {&WINDOW-NAME} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin
DO:
  {lib/confirm-exit.i}
  
  apply "CLOSE":U to this-procedure.
  publish "ExitApplication".
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin
DO:
  run windowresized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose wWin
ON CHOOSE OF bClose IN FRAME fSearch /* Close */
do:
  if not available findingaction
   then return.

  define variable cReason as character no-undo.
  run dialogfindingclose.w (output cReason, 
                            output std-lo).
  
  if not std-lo
   then
    do:
      std-ha = ?.
      publish "OpenFinding" (findingaction.findingID, output std-ha).
      if valid-handle(std-ha)
       then run CloseFinding in std-ha (cReason, output std-lo).
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConfig wWin
ON CHOOSE OF bConfig IN FRAME fSearch /* Config */
DO:
  run SetDynamicBrowseColumns.
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocs wWin
ON CHOOSE OF bDocs IN FRAME fSearch /* Documents */
DO:
  if not available findingaction
   then return.

  if valid-handle(hDocWindow)
   then run ShowWindow in hDocWindow.
   else run documents.w persistent set hDocWindow ("AM",
                                                   "/Finding/" + string(year(findingaction.findingCreatedDate)) + "/" + string(findingaction.findingID) + "/",
                                                   "Action Management",
                                                   "new,delete,open,modify,send,request",
                                                   "Finding",
                                                   string(findingaction.findingID),
                                                   "").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wWin
ON CHOOSE OF bExport IN FRAME fSearch /* Export */
DO:
  run ActionExport in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo wWin
ON CHOOSE OF bGo IN FRAME fSearch /* Go */
DO:
  clearData().
  define variable dStartTime as datetime no-undo.
  dStartTime = now.
  publish "LoadFindingAction" (tStatus:screen-value in frame {&frame-name}, 0, "", "", "").
  run getData in this-procedure.
  dynamic-function("appendStatus", "in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bManageAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bManageAction wWin
ON CHOOSE OF bManageAction IN FRAME fSearch /* Manage Action */
DO:
  if not available findingaction or findingaction.actionID = 0
   then return.
   
  run OpenWindowForAction (findingaction.findingID, findingaction.actionID, "dialogactionmodify.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify wWin
ON CHOOSE OF bModify IN FRAME fSearch /* Edit */
DO:
  if not available findingaction
   then return.
  
  run ModifyFinding (findingaction.findingID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew wWin
ON CHOOSE OF bNew IN FRAME fSearch /* New */
DO:
  run dialogfinding.w (?).
  if tStatus:screen-value = "P" and can-find(first findingaction)
   then run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbSubject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSubject wWin
ON VALUE-CHANGED OF cbSubject IN FRAME fSearch
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEntity wWin
ON VALUE-CHANGED OF fEntity IN FRAME fSearch /* Entity */
DO:
  cbSubject:hidden in frame fSearch = false. 
  cbSubject:delimiter = ",".
  cbSubject:inner-lines = 15.
  run AgentComboHide in this-procedure (true).
  run AttorneyComboHide in this-procedure (true).
  case self:screen-value:
   when "ALL" then
    do:
      cbSubject:inner-lines = 1.
      cbsubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
   when "Agent" then
    do:
      cbsubject:delimiter = {&msg-dlm}.
      run AgentComboHide in this-procedure (false).
    end.
   when "Attorney" then
    do:
      cbsubject:delimiter = {&msg-dlm}.
      run AttorneyComboHide in this-procedure (false).
    end.
   when "Company" then
    do:
      cbsubject:hidden in frame fSearch = true. 
    end.
   when "Department" then
    do:
      std-ch = "".
      publish "GetDeptCombo" (output std-ch).
      if std-ch > ""
       then cbSubject:list-item-pairs = "ALL,ALL," + std-ch.
       else cbSubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
  end case.
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOwner
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOwner wWin
ON VALUE-CHANGED OF fOwner IN FRAME fSearch /* Owner */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSeverity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSeverity wWin
ON VALUE-CHANGED OF fSeverity IN FRAME fSearch /* Severity */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About wWin
ON CHOOSE OF MENU-ITEM m_About /* About */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Actions_By_Month
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Actions_By_Month wWin
ON CHOOSE OF MENU-ITEM m_Actions_By_Month /* Action Approval */
DO:
  run OpenWindow ("rpt/actionapproval.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents wWin
ON CHOOSE OF MENU-ITEM m_Agents /* Agents */
DO:
  run OpenWindow ("referenceagent.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Attorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Attorney wWin
ON CHOOSE OF MENU-ITEM m_Attorney /* Attorneys */
DO:
  run OpenWindow ("wattorney.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Close wWin
ON CHOOSE OF MENU-ITEM m_Close /* Close finding */
DO:
  apply "choose" to bClose in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configuration
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configuration wWin
ON CHOOSE OF MENU-ITEM m_Configuration /* Configure */
DO:
  run OpenWindow ("dialogcamconfig.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Departments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Departments wWin
ON CHOOSE OF MENU-ITEM m_Departments /* Departments */
DO:
  run wdept.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Documents wWin
ON CHOOSE OF MENU-ITEM m_Documents /* Documents */
DO:
  apply "choose" to bDocs in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Edit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Edit wWin
ON CHOOSE OF MENU-ITEM m_Edit /* Edit Finding/Actions */
DO:
  apply "choose" to bModify in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit wWin
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export wWin
ON CHOOSE OF MENU-ITEM m_Export /* Export */
DO:
  apply "choose" to bExport in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ManageAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ManageAction wWin
ON CHOOSE OF MENU-ITEM m_ManageAction /* Manage Action */
DO:
  apply "choose" to bManageAction in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_My_References
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_My_References wWin
ON CHOOSE OF MENU-ITEM m_My_References /* My References */
DO:
  run OpenWindow ("wreferences.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_New wWin
ON CHOOSE OF MENU-ITEM m_New /* New Finding */
DO:
  apply "choose" to bNew in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Notifications
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Notifications wWin
ON CHOOSE OF MENU-ITEM m_Notifications /* Notifications */
DO:
  run OpenWindow ("wnotifications.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Refresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Refresh wWin
ON CHOOSE OF MENU-ITEM m_Refresh /* Refresh */
DO:
  apply "choose" to bGo in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_States
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_States wWin
ON CHOOSE OF MENU-ITEM m_States /* States */
DO:
  run OpenWindow ("referencestate.w").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Summary_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Summary_Report wWin
ON CHOOSE OF MENU-ITEM m_Summary_Report /* Summary Report */
DO:
  run wSummaryReport.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStatus wWin
ON VALUE-CHANGED OF tStatus IN FRAME fSearch /* Status */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-status.i &entity="'Action'"}
{lib/win-close.i}
{lib/win-show.i}
{lib/build-dynamic-browse.i &table=findingaction &entity="'FindingAction'" &keyfield="'findingactionID'" &resizeColumn="'iconMessages'"}
{lib/set-filter.i &label="Entity"   &id="entity"   &value="entity"}
{lib/set-filter.i &label="Severity" &id="severity" &value="severityDesc"}
{lib/set-filter.i &label="Owner"    &id="ownerID"  &value="ownerDesc"}
{lib/set-filter.i &label="Subject"  &id="entityID" &value="entityID" &f=cbSubject &populate=false}
{lib/set-button.i &label="Close"}
{lib/set-button.i &label="Go"       &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-button.i &label="Config"}
{lib/set-button.i &label="Docs"}
{lib/set-button.i &label="ManageAction" &image="images/blank-modify.bmp" &inactive="images/blank-modify-i.bmp"}
{lib/set-button.i &label="Modify"}
{lib/set-button.i &label="New"}
setButtons().

subscribe to "ActionDataChanged" anywhere.
subscribe to "FindingDataChanged" anywhere.

subscribe to "FindingActionRowChanged" anywhere.
subscribe to "FindingActionSelected" anywhere.

subscribe to "OpenWindow" anywhere.
subscribe to "OpenWindowForAction" anywhere.
subscribe to "OpenWindowForFinding" anywhere.
subscribe to "ActionWindowForAgent" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
subscribe to "FindingRefresh" anywhere.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

assign
  {&window-name}:max-width-pixels = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.
  .

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.
  {lib/get-attorney-list.i &combo=cbsubject &addAll=true}
  {lib/get-agent-list.i &combo=cbsubject &addAll=true}
  {lib/get-sysprop-list.i &combo=tStatus &appCode="'CAM'" &objAction="'Action'" &objProperty="'Status'" &addAll=true &d="'O'"}
  
  clearData().
  run windowResized in this-procedure.
  run SetDynamicBrowseColumnWidth ("iconMessages").
  
  std-lo = false.
  publish "GetAutoView" (output std-lo).
  if std-lo
   then run getData in this-procedure.
  apply "VALUE-CHANGED" to fEntity.
  
  cbSubject:screen-value = "ALL".
  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDataChanged wWin 
PROCEDURE ActionDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
  displayStatus().
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionExport wWin 
PROCEDURE ActionExport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.

  hBrowse = getBrowseHandle().
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      std-ha = hBrowse:query.
      if std-ha:num-results = 0
       then
        do:
          message "No results to export" view-as alert-box warning buttons ok.
          return.
        end.
    end.

  &scoped-define ReportName "Action_Management"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(hBrowse), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(hBrowse), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent wWin 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE buildFieldList wWin 
PROCEDURE buildFieldList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  run BuildDynamicBrowseColumns.
  for each listfield exclusive-lock:
    case listfield.columnBuffer:
     when "hasDoc" then listfield.columnWidth = 5.
     when "findingID" then listfield.columnWidth = 8.
     when "actionID" then listfield.columnWidth = 8.
     when "statDesc" then listfield.columnWidth = 12.
     when "stageDesc" then listfield.columnWidth = 12.
     when "severityDesc" then listfield.columnWidth = 12.
     when "entity" then listfield.columnWidth = 12.
     when "entityID" then listfield.columnWidth = 12.
     when "source" then listfield.columnWidth = 12.
     when "sourceID" then listfield.columnWidth = 12.
     when "ownerDesc" then listfield.columnWidth = 26.
     when "iconMessages" then listfield.columnWidth = 50.
    end case.
    case listfield.dataType:
     when "datetime" then listfield.columnWidth = 15.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tActionLabel tFindingLabel tQuestionLabel fSeverity edQuestion 
          edFinding edAction fEntity cbSubject fOwner tStatus 
      WITH FRAME fSearch IN WINDOW wWin.
  ENABLE bConfig bNew tActionLabel tFindingLabel tQuestionLabel fSeverity 
         edQuestion edFinding edAction fEntity cbSubject fOwner bClose bModify 
         bManageAction bDocs bExport bGo tStatus RECT-53 RECT-43 RECT-54 
      WITH FRAME fSearch IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fSearch}
  VIEW FRAME fBrowse IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fBrowse}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FindingActionRowChanged wWin 
PROCEDURE FindingActionRowChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFindingActionID as character no-undo.
  
  define variable iFindingID as integer no-undo.
  define variable iActionID as integer no-undo.
  
  iFindingID = integer(entry(1, pFindingActionID, "-")).
  iActionID = integer(entry(2, pFindingActionID, "-")).
  do with frame {&frame-name}:
    for first findingaction no-lock
        where findingaction.findingID = iFindingID
          and findingaction.actionID = iActionID:
      
      assign 
        edQuestion:screen-value = (if (findingaction.question = ? or findingaction.question = "?") then "" else findingaction.question)
        edFinding:screen-value = (if (findingaction.finding = ? or findingaction.finding = "?") then "" else findingaction.finding)
        edAction:screen-value = (if (findingaction.action = ? or findingaction.action = "?") then "" else findingaction.action)
        bExport:sensitive = true
        menu-item m_Export:sensitive in menu m_action = true
        std-lo = (findingaction.stage = "A")
        bDocs:sensitive = std-lo
        menu-item m_Documents:sensitive in menu m_action = std-lo
        bManageAction:sensitive = (std-lo and findingaction.actionID > 0)
        menu-item m_ManageAction:sensitive in menu m_action = std-lo
        bModify:sensitive = std-lo
        menu-item m_Edit:sensitive in menu m_action = std-lo
        bClose:sensitive = std-lo
        menu-item m_Close:sensitive in menu m_action = std-lo
        .
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FindingActionSelected wWin 
PROCEDURE FindingActionSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFindingActionID as character no-undo.
  
  define variable iFindingID as integer no-undo.
  define variable iActionID as integer no-undo.
  
  iFindingID = integer(entry(1, pFindingActionID, "-")).
  iActionID = integer(entry(2, pFindingActionID, "-")).
  if iActionID = 0
   then run ModifyFinding in this-procedure (iFindingID).
   else run ModifyAction in this-procedure (iFindingID, iActionID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FindingDataChanged wWin 
PROCEDURE FindingDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run getData in this-procedure.
  displayStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FindingRefresh wWin 
PROCEDURE FindingRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "FindingChanged".
  displayStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData wWin 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table findingaction.
  publish "GetFindingActions" (tStatus:screen-value in frame {&frame-name}, 0, "", "", "", output table findingaction).
  
  if can-find(first findingaction)
   then
    do:
      enableButtons(true).
      enableFilters(true).
      run buildFieldList in this-procedure.
      run BuildDynamicBrowse (createQuery()).
      cbSubject:sensitive in frame {&frame-name} = true.
      dataSortDesc = not dataSortDesc.
      if dataSortBy = ""
       then
        assign
          dataSortBy = "severity"
          dataSortDesc = false
          .
      setFilterCombos("ALL").
      std-ha = frame fBrowse:first-child:first-child.
      if valid-handle(std-ha) and std-ha:type = "BROWSE"
       then
        do:
          std-ha = std-ha:query.
          setStatusCount(std-ha:num-results).
        end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyAction wWin 
PROCEDURE ModifyAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFindingID as character no-undo.
  define input parameter pActionID as character no-undo.
  
  run OpenWindowForAction (pFindingID, pActionID, "dialogactionmodify.w").
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyFinding wWin 
PROCEDURE ModifyFinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFindingID as character no-undo.
  
  run OpenWindowForFinding (pFindingID, "dialogfinding.w").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenWindow wWin 
PROCEDURE OpenWindow :
/*------------------------------------------------------------------------------
@description Opens the window
------------------------------------------------------------------------------*/
  define input parameter pFile as character no-undo.
  
  define variable hWindow as handle no-undo. 
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  
  hWindow = ?.
  for first openwindow no-lock
      where openwindow.procFile = pFile:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      run value(pFile) persistent set hWindow.
      create openwindow.
      assign
        openwindow.procFile = pFile
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenWindowForAction wWin 
PROCEDURE OpenWindowForAction :
/*------------------------------------------------------------------------------
@description Opens the window for an action
------------------------------------------------------------------------------*/
  define input parameter pFindingID as integer no-undo.
  define input parameter pActionID as integer no-undo.
  define input parameter pFile as character no-undo.

  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cOpenAction as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.

  assign
    hWindow = ?
    hFileDataSrv = ?
    cOpenAction = string(pFindingID) + "-" + string(pActionID) + "-" + pFile
    .
  for first openwindow no-lock
      where openwindow.procFile = cOpenAction:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      publish "OpenAction" (pFindingID, pActionID, output hFileDataSrv).
      run value(pFile) persistent set hWindow (hFileDataSrv).
      create openwindow.
      assign
        openwindow.procFile = cOpenAction
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenWindowForFinding wWin 
PROCEDURE OpenWindowForFinding :
/*------------------------------------------------------------------------------
@description Opens the window for a finding
------------------------------------------------------------------------------*/
  define input parameter pFindingID as integer no-undo.
  define input parameter pFile as character no-undo.
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cOpenFinding as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.

  assign
    hWindow = ?
    hFileDataSrv = ?
    cOpenFinding = string(pFindingID) + "-" + pFile
    .
  for first openwindow no-lock
      where openwindow.procFile = cOpenFinding:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      publish "OpenFinding" (pFindingID, output hFileDataSrv).
      run value(pFile) persistent set hWindow (hFileDataSrv).
      create openwindow.
      assign
        openwindow.procFile = cOpenFinding
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

  /* fSearch components */
  frame fBrowse:width-pixels = frame {&frame-name}:width-pixels - 10.
  frame fBrowse:virtual-width-pixels = frame fBrowse:width-pixels.
  frame fBrowse:height-pixels = frame {&frame-name}:height-pixels - frame fBrowse:y - 150.
  frame fBrowse:virtual-height-pixels = frame fBrowse:height-pixels.
  
  edQuestion:width-pixels = frame {&frame-name}:width-pixels - 79.
  edFinding:width-pixels  = frame {&frame-name}:width-pixels - 79.
  edAction:width-pixels   = frame {&frame-name}:width-pixels - 79.
  edQuestion:y = frame {&frame-name}:height-pixels - 143.
  edFinding:y  = frame {&frame-name}:height-pixels - 95.
  edAction:y   = frame {&frame-name}:height-pixels - 47.
  tQuestionLabel:y = edQuestion:y + 3.
  tFindingLabel:y  = edFinding:y + 3.
  tActionLabel:y   = edAction:y + 3.
  
  run buildFieldList in this-procedure.
  run BuildDynamicBrowse (createQuery()).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData wWin 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.
  define variable hQuery as handle no-undo.
  
  hBrowse = frame fBrowse:first-child:first-child.
  if valid-handle(hBrowse) and hBrowse:type = "BROWSE"
   then
    do:
      hQuery = hBrowse:query.
      hQuery:query-close().
    end.
    
  clearStatus().
  enableButtons(false).
  enableFilters(false).
  run AgentComboHide (true).
  run AttorneyComboHide (true).
  do with frame {&frame-name}:
    assign 
      edQuestion:screen-value = ""
      edFinding:screen-value  = ""
      edAction:screen-value   = ""
      cbSubject:sensitive     = false
      fEntity:screen-value    = "ALL"
      /* buttons */
      bExport:sensitive       = false
      bDocs:sensitive         = false
      bManageAction:sensitive = false
      bModify:sensitive       = false
      bClose:sensitive        = false
      /* menu items */
      menu-item m_Export:sensitive in menu m_action       = false
      menu-item m_Documents:sensitive in menu m_action    = false
      menu-item m_ManageAction:sensitive in menu m_action = false
      menu-item m_Edit:sensitive in menu m_action         = false
      menu-item m_Close:sensitive in menu m_action        = false
      .
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createQuery wWin 
FUNCTION createQuery RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE tQuery as CHARACTER no-undo.

  tQuery = "preselect each findingaction no-lock " + getWhereClause().
  RETURN tQuery.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

