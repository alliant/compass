&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogextendaction.w

  Description: changes the due date of an open action.

  Input Parameters:
  @param piActionID;int;The ActionID of action to be extended(Required).
  @param pdDueDate;datetime;Previous due date of action.
  @param pdFollupDtOrig;datetime;Original Followup date of action.
  
  Output Parameters:
  @param pcSuccess;logical;success message.

  Author:Sachin Chaturvedi

  Created: 08/28/2017
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input-output parameter pDueDate      as datetime no-undo.
define input-output parameter pFollowupDate as datetime no-undo.
define output       parameter pReason       as character no-undo.
define output       parameter pCancel       as logical no-undo initial true.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fOrigDueDt fNewDueDt fOrigFollDt ~
fNewFollupDt edReason bOK bCancel 
&Scoped-Define DISPLAYED-OBJECTS fOrigDueDt fNewDueDt fOrigFollDt ~
fNewFollupDt edReason 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bOK 
     LABEL "OK" 
     SIZE 15 BY 1.14 DROP-TARGET.

DEFINE VARIABLE edReason AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 45 BY 4.05 NO-UNDO.

DEFINE VARIABLE fNewDueDt AS DATE FORMAT "99/99/99":U 
     LABEL "New Due" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fNewFollupDt AS DATE FORMAT "99/99/99":U 
     LABEL "New Followup" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fOrigDueDt AS DATE FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fOrigFollDt AS DATE FORMAT "99/99/99":U 
     LABEL "Followup" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fOrigDueDt AT ROW 1.48 COL 11 COLON-ALIGNED WIDGET-ID 42 NO-TAB-STOP 
     fNewDueDt AT ROW 1.48 COL 42 COLON-ALIGNED WIDGET-ID 44
     fOrigFollDt AT ROW 2.67 COL 11 COLON-ALIGNED WIDGET-ID 48 NO-TAB-STOP 
     fNewFollupDt AT ROW 2.67 COL 42 COLON-ALIGNED WIDGET-ID 46
     edReason AT ROW 3.86 COL 13 NO-LABEL WIDGET-ID 28
     bOK AT ROW 8.14 COL 15 WIDGET-ID 24
     bCancel AT ROW 8.14 COL 31 WIDGET-ID 22
     "Reason:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 3.95 COL 4.4 WIDGET-ID 36
     SPACE(47.39) SKIP(5.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Extend Action" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       fOrigDueDt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fOrigFollDt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Extend Action */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK Dialog-Frame
ON CHOOSE OF bOK IN FRAME Dialog-Frame /* OK */
DO:
  define variable dDate   as date        no-undo.
  define variable cMessage as character   no-undo. 

  /* Due Date validation */
  dDate = date (fNewDueDt:screen-value)  no-error.
  if error-status:error 
   then
    do:
      cMessage = (if cMessage ne "" then (cMessage + chr(10)) else "" ) + "Changed Due not valid".
    end.
    
  if date (fNewDueDt:screen-value) lt date(today) 
   then
    do:
      cMessage = (if cMessage ne "" then (cMessage + chr(10)) else "" ) + "Changed Due must be >= Today.".        
    end.
      
  if date (fNewDueDt:screen-value) le date(fOrigDueDt:screen-value) then do:
     cMessage = (if cMessage ne "" then (cMessage + chr(10)) else "" ) + "Changed Due must be greater than Current Due.".
  end.
 
  /* followup Date validation */
  dDate = date (fNewFollupDt:screen-value)  no-error.
  if (date (fNewFollupDt:screen-value) ne ?) and 
     (date(fNewFollupDt:screen-value) gt date(fNewDueDt:screen-value))  
   then 
    do:
      cMessage = (if cMessage ne "" then (cMessage + chr(10)) else "" ) + "Followup must be less than Changed Due".
    end.
 
  if (date (fNewFollupDt:screen-value) ne ?) and 
     (date(fNewFollupDt:screen-value) lt date(today))  
   then 
    do:
      cMessage = (if cMessage ne "" then (cMessage + chr(10)) else "" ) + "Followup must be >= Today".
    end.

  /*  run enableOk. */

  if cMessage ne ""  
   then 
    do:
      message cMessage
           view-as alert-box info buttons OK.
       return no-apply.
    end.
 
  assign
    pDueDate      = datetime(fNewDueDt:screen-value)
    pFollowupDate = datetime(fNewFollupDt:screen-value)
    pReason       = edReason:screen-value
    pCancel       = false
    .
  apply "CHOOSE" to bCancel.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edReason
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edReason Dialog-Frame
ON VALUE-CHANGED OF edReason IN FRAME Dialog-Frame
DO:
  run enableOk.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNewDueDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNewDueDt Dialog-Frame
ON VALUE-CHANGED OF fNewDueDt IN FRAME Dialog-Frame /* New Due */
DO:
  define variable d1 as datetime no-undo.
  d1 = date(fNewDueDt:screen-value in frame Dialog-Frame) no-error.

  if (d1 = ?)                      or 
     (edReason:screen-value eq "") or 
     (fNewDueDt:screen-value  in frame Dialog-Frame eq string(date(pDueDate)) ) 
   then
    bOk:sensitive = false.
  else 
    bOk:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  assign
    fOrigDueDt:screen-value  = string(pDueDate)
    fOrigFollDt:screen-value = string(pFollowupDate) 
    bOk:sensitive            = false
    .
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableOk Dialog-Frame 
PROCEDURE enableOk :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if (date(fNewDueDt:screen-value  in frame Dialog-Frame) ne date("")  )            and
         fNewDueDt:screen-value  in frame Dialog-Frame ne string(date(pDueDate))  and 
         edReason:screen-value   in frame Dialog-Frame ne ""  
 then enable bOK with frame Dialog-Frame.
 else disable bOK with frame Dialog-Frame.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fOrigDueDt fNewDueDt fOrigFollDt fNewFollupDt edReason 
      WITH FRAME Dialog-Frame.
  ENABLE fOrigDueDt fNewDueDt fOrigFollDt fNewFollupDt edReason bOK bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

