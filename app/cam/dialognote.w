&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNote 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

def input parameter pTitle as char no-undo.
def input parameter pEnableFields as char no-undo.

def input-output parameter pDate as date.
def input-output parameter pTakenBy as char.
def input-output parameter pNotes as char.

def output parameter pError as logical init true.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNote

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tNotes Btn_OK 
&Scoped-Define DISPLAYED-OBJECTS tNotes tDate tTakenBy 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 83.8 BY 6.19 NO-UNDO.

DEFINE VARIABLE tDate AS DATE FORMAT "99/99/99":U 
     LABEL "Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tTakenBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "By" 
     VIEW-AS FILL-IN 
     SIZE 57.4 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNote
     tNotes AT ROW 1.43 COL 2 NO-LABEL WIDGET-ID 6
     tDate AT ROW 7.81 COL 69.6 COLON-ALIGNED WIDGET-ID 2
     tTakenBy AT ROW 7.86 COL 4.4 COLON-ALIGNED WIDGET-ID 4
     Btn_OK AT ROW 9.1 COL 27.8
     Btn_Cancel AT ROW 9.1 COL 43.8
     SPACE(27.99) SKIP(0.32)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE  SCROLLABLE 
         TITLE "Title is passed as parameter"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNote
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fNote:SCROLLABLE       = FALSE
       FRAME fNote:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_Cancel IN FRAME fNote
   NO-ENABLE                                                            */
ASSIGN 
       Btn_Cancel:HIDDEN IN FRAME fNote           = TRUE.

/* SETTINGS FOR FILL-IN tDate IN FRAME fNote
   NO-ENABLE                                                            */
ASSIGN 
       tNotes:READ-ONLY IN FRAME fNote        = TRUE.

/* SETTINGS FOR FILL-IN tTakenBy IN FRAME fNote
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX fNote
/* Query rebuild information for DIALOG-BOX fNote
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX fNote */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNote fNote
ON WINDOW-CLOSE OF FRAME fNote /* Title is passed as parameter */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNote 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

frame {&frame-name}:title = pTitle.

assign
 tDate = pDate
 tTakenBy = pTakenBy
 tNotes = pNotes
 .

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  assign
   tDate:sensitive in frame {&frame-name} = lookup("date", pEnableFields) > 0
   tTakenBy:sensitive in frame {&frame-name} = lookup("by", pEnableFields) > 0
   tNotes:read-only in frame {&frame-name} = lookup("notes", pEnableFields) = 0
   Btn_Cancel:visible = pEnableFields > ""
   Btn_Cancel:sensitive = pEnableFields > ""
   .

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if lookup("notes", pEnableFields) > 0
   then .
  pNotes = tNotes:screen-value in frame {&frame-name}.

  assign
    pDate = date(tDate:screen-value in frame {&frame-name})
    pTakenBy = tTakenBy:screen-value in frame {&frame-name}
    pError = false
    .
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNote  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNote.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNote  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tNotes tDate tTakenBy 
      WITH FRAME fNote.
  ENABLE tNotes Btn_OK 
      WITH FRAME fNote.
  VIEW FRAME fNote.
  {&OPEN-BROWSERS-IN-QUERY-fNote}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

