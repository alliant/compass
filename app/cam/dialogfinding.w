&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Temp Table Definitions ---                                           */
{tt/action.i}
{tt/action.i &tableAlias="data"}
{tt/action.i &tableAlias="tempaction"}
{tt/finding.i}
{tt/finding.i &tableAlias="tempfinding"}
{tt/agent.i}
{tt/attorney.i}

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
define variable tFindingID as integer no-undo.
{lib/std-def.i}
{lib/set-button-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwAction

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES data

/* Definitions for BROWSE brwAction                                     */
&Scoped-define FIELDS-IN-QUERY-brwAction data.actionID data.statDesc data.ownerDesc data.createdDate data.startDate data.dueDate data.followupDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAction   
&Scoped-define SELF-NAME brwAction
&Scoped-define QUERY-STRING-brwAction for each data
&Scoped-define OPEN-QUERY-brwAction OPEN QUERY {&SELF-NAME} for each data.
&Scoped-define TABLES-IN-QUERY-brwAction data
&Scoped-define FIRST-TABLE-IN-QUERY-brwAction data


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwAction}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-59 RECT-58 tEntity cbSubject tSeverity ~
tSource tQuestion tFinding bNew brwAction bModify bDelete bCopy edAction ~
bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tEntity cbSubject tSeverity tSource ~
tQuestion tFinding edAction 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bCopy 
     LABEL "Copy" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bModify 
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Edit Action".

DEFINE BUTTON bNew 
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "New Action".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbSubject AS CHARACTER INITIAL "ALL" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 90 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Severity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE edAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 TOOLTIP "What business process found the issue?" NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 7.86.

DEFINE RECTANGLE RECT-59
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 8.19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAction FOR 
      data SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAction C-Win _FREEFORM
  QUERY brwAction DISPLAY
      data.actionID width 8
data.statDesc width 12
data.ownerDesc width 25
data.createdDate width 15
data.startDate width 15
data.dueDate width 15
data.followupDate width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 113 BY 4.57 ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tEntity AT ROW 2.19 COL 14 COLON-ALIGNED WIDGET-ID 244
     cbSubject AT ROW 2.19 COL 37 COLON-ALIGNED NO-LABEL WIDGET-ID 240
     tSeverity AT ROW 3.38 COL 14 COLON-ALIGNED WIDGET-ID 256
     tSource AT ROW 3.38 COL 45 COLON-ALIGNED WIDGET-ID 258
     tQuestion AT ROW 4.57 COL 16 NO-LABEL WIDGET-ID 254
     tFinding AT ROW 6.71 COL 16 NO-LABEL WIDGET-ID 252
     bNew AT ROW 10.48 COL 10.8 WIDGET-ID 224
     brwAction AT ROW 10.52 COL 16 WIDGET-ID 200
     bModify AT ROW 11.67 COL 10.8 WIDGET-ID 226
     bDelete AT ROW 12.86 COL 10.8 WIDGET-ID 228
     bCopy AT ROW 14.05 COL 10.8 WIDGET-ID 230
     edAction AT ROW 15.52 COL 16 NO-LABEL WIDGET-ID 236
     bSave AT ROW 18.38 COL 51.6 WIDGET-ID 10
     bCancel AT ROW 18.38 COL 67.4 WIDGET-ID 24
     "Action:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 15.67 COL 8.8 WIDGET-ID 238
     "Actions" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 9.57 COL 4 WIDGET-ID 234
     "Question:" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 4.71 COL 6.4 WIDGET-ID 246
     "Finding:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 6.86 COL 8 WIDGET-ID 250
     "Finding" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 248
     RECT-59 AT ROW 9.95 COL 3 WIDGET-ID 222
     RECT-58 AT ROW 1.48 COL 3 WIDGET-ID 242
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 132 BY 19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "New Finding"
         HEIGHT             = 19
         WIDTH              = 132
         MAX-HEIGHT         = 27.14
         MAX-WIDTH          = 173.6
         VIRTUAL-HEIGHT     = 27.14
         VIRTUAL-WIDTH      = 173.6
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAction bNew DEFAULT-FRAME */
ASSIGN 
       brwAction:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwAction:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       edAction:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAction
/* Query rebuild information for BROWSE brwAction
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} for each data.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAction */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Modify Finding */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Modify Finding */
DO:
  {lib/confirm-close.i "Finding"}
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME DEFAULT-FRAME /* Cancel */
do:
  apply "WINDOW-CLOSE" to {&window-name}.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCopy C-Win
ON CHOOSE OF bCopy IN FRAME DEFAULT-FRAME /* Copy */
DO:
  if not available data
   then return.

  if data.actionID = 0
   then
    do:
      message "Save before copying" view-as alert-box warning buttons ok.
      return.
    end.

  create action.
  buffer-copy data except data.actionID to action.
  action.actionID = 0.

  run rebuildBrowse in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
DO:
  if not available data
   then return.

  for first action exclusive-lock
      where action.actionID = data.actionID:

    delete action.
  end.
  run rebuildBrowse in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Edit */
DO:
  if not available data
   then return.

  for first finding no-lock:
    publish "OpenWindowForAction" (finding.findingID, data.actionID, "dialogactionmodify.w").
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* Add */
DO:
  /* create a new temporary finding */
  empty temp-table tempfinding.
  create tempfinding.
  assign
    tempfinding.findingID      = tFindingID
    tempfinding.entityID       = cbSubject:screen-value
    tempfinding.refType        = tEntity:screen-value
    tempfinding.refID          = cbSubject:screen-value
    tempfinding.severity       = tSeverity:input-value
    tempfinding.source         = tSource:screen-value
    tempfinding.sourceQuestion = tQuestion:screen-value
    tempfinding.description    = tFinding:screen-value
    .

  empty temp-table tempaction.
  run dialogactionnew.w (table tempfinding, output table tempaction, output std-lo).
  if std-lo and can-find(first tempaction)
   then
    for first tempaction no-lock:
      create action.
      buffer-copy tempaction to action.
      action.actionID = 0.

      run rebuildBrowse in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAction
&Scoped-define SELF-NAME brwAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction C-Win
ON DEFAULT-ACTION OF brwAction IN FRAME DEFAULT-FRAME
DO:
  apply "CHOOSE" to bModify.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction C-Win
ON ROW-DISPLAY OF brwAction IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction C-Win
ON START-SEARCH OF brwAction IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAction C-Win
ON VALUE-CHANGED OF brwAction IN FRAME DEFAULT-FRAME
DO:
  assign
    bDelete:sensitive = false
    bModify:sensitive = false
    bCopy:sensitive = false
    .

  if not available data
   then return.

  assign 
    edAction:screen-value = data.comment
    bDelete:sensitive = (data.stat = "P")
    bModify:sensitive = not (data.stat = "C")
    bModify:sensitive = bModify:sensitive and data.actionID > 0
    bCopy:sensitive = (data.stat = "P" and data.actionID > 0)
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME DEFAULT-FRAME /* Save */
DO:
  if valid-handle(hFileDataSrv)
   then
    do:
      for first finding exclusive-lock:
        if not finding.source = "QAR" and not finding.source = "QUR"
         then
          assign
            finding.entityID       = cbSubject:screen-value
            finding.refType        = tEntity:screen-value
            finding.refID          = cbSubject:screen-value
            finding.severity       = tSeverity:input-value
            finding.source         = tSource:screen-value
            finding.sourceQuestion = tQuestion:screen-value
            finding.description    = tFinding:screen-value
            .
      end.
      run ModifyFinding in hFileDataSrv (table finding, table action, output std-lo).
      run GetFinding in hFileDataSrv (output table finding).
      run GetActions in hFileDataSrv (output table action).
      run setData in this-procedure.
      run ShowWindow in this-procedure.
    end.
   else
    do:
      std-ch = "".
      publish "GetSysPropDesc" ("CAM", "Finding", "Entity", tEntity:screen-value, output std-ch).
      create finding.
      assign
        finding.entity         = std-ch
        finding.entityID       = cbSubject:screen-value
        finding.refType        = tEntity:screen-value
        finding.refID          = cbSubject:screen-value
        finding.severity       = tSeverity:input-value
        finding.source         = tSource:screen-value
        finding.sourceQuestion = tQuestion:screen-value
        finding.description    = tFinding:screen-value
        .
      publish "NewFinding" (table finding, table action, output std-lo).
      if std-lo
       then apply "CLOSE":U to this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntity C-Win
ON VALUE-CHANGED OF tEntity IN FRAME DEFAULT-FRAME /* Entity */
DO:
  cbSubject:delimiter = ",".
  cbSubject:inner-lines = 15.
  run AgentComboHide in this-procedure (true).
  run AttorneyComboHide in this-procedure (true).
  case self:screen-value:
   when "A" then
    do:
      cbSubject:delimiter = {&msg-dlm}.
      run AgentComboHide in this-procedure (false).
    end.
   when "T" then
    do:
      cbSubject:delimiter = {&msg-dlm}.
      run AttorneyComboHide in this-procedure (false).
    end.
   when "C" then
    do:
      cbSubject:hidden = true. 
    end.
   when "D" then
    do:
      std-ch = "".
      publish "GetDeptCombo" (output std-ch).
      if std-ch > ""
       then cbSubject:list-item-pairs = "ALL,ALL," + std-ch.
       else cbSubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/brw-main.i}
{lib/set-button.i &label="New"    &useSmall=true}
{lib/set-button.i &label="Delete" &useSmall=true}
{lib/set-button.i &label="Copy"   &useSmall=true}
{lib/set-button.i &label="Modify" &useSmall=true}
setButtons().

subscribe to "ActionChanged" anywhere.
subscribe to "FindingChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-attorney-list.i &combo=cbsubject}
  {lib/get-agent-list.i &combo=cbsubject}
  {lib/get-sysprop-list.i &combo=tEntity &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Entity'"}
  {lib/get-sysprop-list.i &combo=tSeverity &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Severity'"}
  
  if valid-handle(hFileDataSrv)
   then
    do:
      run GetFinding in hFileDataSrv (output table finding).
      run GetActions in hFileDataSrv (output table action).
    end.
    
  run setData in this-procedure.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionChanged C-Win 
PROCEDURE ActionChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pActionID as integer no-undo.

  if can-find(first action where actionID = pActionID) and valid-handle(hFileDataSrv)
   then
    do:
      run GetActions in hFileDataSrv (output table action).
      run setData in this-procedure.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEntity cbSubject tSeverity tSource tQuestion tFinding edAction 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-59 RECT-58 tEntity cbSubject tSeverity tSource tQuestion tFinding 
         bNew brwAction bModify bDelete bCopy edAction bSave bCancel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FindingChanged C-Win 
PROCEDURE FindingChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFindingID as integer no-undo.

  if can-find(first finding where findingID = pFindingID) and valid-handle(hFileDataSrv)
   then
    do:
      run GetFinding in hFileDataSrv (output table finding).
      run GetActions in hFileDataSrv (output table action).
      run setData in this-procedure.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rebuildBrowse C-Win 
PROCEDURE rebuildBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query {&browse-name}.
  empty temp-table data.
  for each action exclusive-lock:
    publish "GetSysUserName" (action.ownerID, output action.ownerDesc).
    publish "GetSysPropDesc" ("CAM", "Action", "ActionType", action.actionType, output action.actionTypeDesc).
    publish "GetSysPropDesc" ("CAM", "Action", "Status", action.stat, output action.statDesc).
    publish "GetSysPropDesc" ("CAM", "Action", "CompleteStatus", action.completeStatus, output action.completeStatusDesc).
    
    create data.
    buffer-copy action to data.
  end.
  {&OPEN-QUERY-brwAction}
  apply "VALUE-CHANGED" to browse {&browse-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    /* finding */
    cbSubject:delimiter = ",".
    cbSubject:inner-lines = 15.
    run AgentComboHide (true).
    run AttorneyComboHide (true).
    if valid-handle(hFileDataSrv)
     then
      for first finding no-lock:
        {&window-name}:title = "Modify Finding " + string(finding.findingID).
        case finding.refType:
         when "A" then
          do:
            cbSubject:delimiter = {&msg-dlm}.
            run AgentComboHide (false).
            run AgentComboSet (finding.entityID).
          end.
         when "T" then
          do:
            cbSubject:delimiter = {&msg-dlm}.
            run AttorneyComboHide (false).
            run AttorneyComboSet (finding.entityID).
          end.
         when "C" then cbSubject:hidden = true.
         when "D" then cbSubject:screen-value = finding.entityID.
        end case.
        assign
          tFindingID             = finding.findingID
          tEntity:screen-value   = finding.refType
          tSeverity:screen-value = string(finding.severity)
          tSource:screen-value   = finding.source + (if finding.sourceID > "" then " (" + finding.sourceID + ")" else "")
          tQuestion:screen-value = finding.sourceQuestion
          tFinding:screen-value  = finding.description
          std-lo                 = (finding.stat = "A")
          std-lo                 = (std-lo and not (finding.source = "QAR" or finding.source = "QUR"))
          tEntity:sensitive      = std-lo
          cbSubject:sensitive    = std-lo
          tSeverity:sensitive    = std-lo
          tSource:read-only      = not std-lo
          tQuestion:read-only    = not std-lo
          tFinding:read-only     = not std-lo
          bNew:sensitive         = std-lo
          bSave:sensitive        = std-lo
          .
        run AgentComboEnable (std-lo).
        run AttorneyComboEnable (std-lo).
        run rebuildBrowse in this-procedure.
      end.
     else
      do:
        tEntity:screen-value = "A".
        cbSubject:delimiter = {&msg-dlm}.
        run AgentComboHide (false).
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &post-by-clause=" + ' by actionID'"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

