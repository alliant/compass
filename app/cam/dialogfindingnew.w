&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* Temp-Table Definitions ---                                           */
{tt/agent.i}
{tt/attorney.i}
{tt/finding.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-58 tEntity cbSubject tSeverity tSource ~
tQuestion tFinding bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tEntity cbSubject tSeverity tSource ~
tQuestion tFinding 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbSubject AS CHARACTER INITIAL "ALL" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 90 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Severity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 TOOLTIP "What business process found the issue?" NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 7.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tEntity AT ROW 2.19 COL 14 COLON-ALIGNED WIDGET-ID 26
     cbSubject AT ROW 2.19 COL 37 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     tSeverity AT ROW 3.38 COL 14 COLON-ALIGNED WIDGET-ID 72
     tSource AT ROW 3.38 COL 45 COLON-ALIGNED WIDGET-ID 60
     tQuestion AT ROW 4.57 COL 16 NO-LABEL WIDGET-ID 64
     tFinding AT ROW 6.71 COL 16 NO-LABEL WIDGET-ID 56
     bSave AT ROW 9.57 COL 51 WIDGET-ID 90
     bCancel AT ROW 9.57 COL 67 WIDGET-ID 92
     "Question:" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 4.71 COL 6.4 WIDGET-ID 52
     "Finding" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 80
     "Finding:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 6.86 COL 8 WIDGET-ID 66
     RECT-58 AT ROW 1.48 COL 3 WIDGET-ID 78
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 131.4 BY 10.05 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Create Finding"
         HEIGHT             = 10.05
         WIDTH              = 131.4
         MAX-HEIGHT         = 26.19
         MAX-WIDTH          = 138.6
         VIRTUAL-HEIGHT     = 26.19
         VIRTUAL-WIDTH      = 138.6
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Create Finding */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Create Finding */
DO:
  {lib/confirm-close.i "Finding"}
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  run SaveFinding in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntity C-Win
ON VALUE-CHANGED OF tEntity IN FRAME fMain /* Entity */
DO:
  cbSubject:delimiter = ",".
  cbSubject:inner-lines = 15.
  run AgentComboHide in this-procedure (true).
  run AttorneyComboHide in this-procedure (true).
  case self:screen-value:
   when "A" then
    do:
      cbSubject:delimiter = {&msg-dlm}.
      run AgentComboHide in this-procedure (false).
    end.
   when "T" then
    do:
      cbSubject:delimiter = {&msg-dlm}.
      run AttorneyComboHide in this-procedure (false).
    end.
   when "C" then
    do:
      cbSubject:hidden = true. 
    end.
   when "D" then
    do:
      std-ch = "".
      publish "GetDeptCombo" (output std-ch).
      if std-ch > ""
       then cbSubject:list-item-pairs = "ALL,ALL," + std-ch.
       else cbSubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-attorney-list.i &combo=cbsubject}
  {lib/get-agent-list.i &combo=cbsubject}
  {lib/get-sysprop-list.i &combo=tEntity &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Entity'"}
  {lib/get-sysprop-list.i &combo=tSeverity &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Severity'"}
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEntity cbSubject tSeverity tSource tQuestion tFinding 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-58 tEntity cbSubject tSeverity tSource tQuestion tFinding bSave 
         bCancel 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveFinding C-Win 
PROCEDURE SaveFinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    std-ch = "".
    publish "GetSysPropDesc" ("CAM", "Finding", "Entity", tEntity:screen-value, output std-ch).
    create finding.
    assign
      finding.entity         = std-ch
      finding.entityID       = cbSubject:screen-value
      finding.refType        = tEntity:screen-value
      finding.refID          = cbSubject:screen-value
      finding.severity       = integer(tSeverity:screen-value)
      finding.source         = tSource:screen-value
      finding.sourceQuestion = tQuestion:screen-value
      finding.description    = tFinding:screen-value
      .
    publish "NewFinding" (input table finding, output std-lo).
    if std-lo
     then apply "CLOSE":U to this-procedure.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

