&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogdeferaction.w

  Description: changes the start date and due date of a Planned action.

  Input Parameters:
  @param piActionID;int;The ActionID of action to be established(Required).
  @param pcEntity;char;The Entity which is required to perfom the action. 
  @param pcEntityID;char;The EntityID of Entity.
  @param pcEntityID;char;The EntityID of Entity.
  @param pdStartDateOrig;datetime;The action original start date. 
  @param pdDueDateOrig;datetime;The action original due date.
  
  Output Parameters:
  @param pcSuccess;logical;success message.

  Author:Sachin Chaturvedi

  Created: 08/28/2017 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input  parameter pEntity       as character no-undo.
define input  parameter pEntityID     as character no-undo.
define input  parameter pComments     as character no-undo.
define input  parameter pStartOrig    as datetime no-undo.
define input  parameter pDueOrig      as datetime no-undo.
define input  parameter pFollowupOrig as datetime no-undo.
define output parameter pStartNew     as datetime no-undo.
define output parameter pDueNew       as datetime no-undo.
define output parameter pFollowupNew  as datetime no-undo.
define output parameter pReason       as character no-undo.
define output parameter pCancel       as logical no-undo initial true.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tEntity tEntityID edComments tOrigStartDt ~
tNewStartDt tOrigDueDt tNewDueDt tOrigFollDt tNewFollupDt edReason bOK ~
bCancel 
&Scoped-Define DISPLAYED-OBJECTS tEntity tEntityID edComments tOrigStartDt ~
tNewStartDt tOrigDueDt tNewDueDt tOrigFollDt tNewFollupDt edReason 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bOK 
     LABEL "OK" 
     SIZE 15 BY 1.14 DROP-TARGET.

DEFINE VARIABLE edComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 61 BY 4.05 NO-UNDO.

DEFINE VARIABLE edReason AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 61 BY 4 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tEntityID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tNewDueDt AS DATE FORMAT "99/99/99":U 
     LABEL "New Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tNewFollupDt AS DATE FORMAT "99/99/99":U 
     LABEL "New Followup" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tNewStartDt AS DATE FORMAT "99/99/99":U 
     LABEL "New Start" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOrigDueDt AS DATE FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOrigFollDt AS DATE FORMAT "99/99/99":U 
     LABEL "Followup" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOrigStartDt AS DATE FORMAT "99/99/99":U 
     LABEL "Start" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tEntity AT ROW 1.48 COL 12 COLON-ALIGNED WIDGET-ID 30
     tEntityID AT ROW 2.67 COL 12 COLON-ALIGNED WIDGET-ID 32
     edComments AT ROW 3.86 COL 14 NO-LABEL WIDGET-ID 26
     tOrigStartDt AT ROW 8.14 COL 12 COLON-ALIGNED WIDGET-ID 38
     tNewStartDt AT ROW 8.14 COL 53 COLON-ALIGNED WIDGET-ID 40
     tOrigDueDt AT ROW 9.33 COL 12 COLON-ALIGNED WIDGET-ID 42
     tNewDueDt AT ROW 9.33 COL 53 COLON-ALIGNED WIDGET-ID 44
     tOrigFollDt AT ROW 10.52 COL 12 COLON-ALIGNED WIDGET-ID 48
     tNewFollupDt AT ROW 10.52 COL 53 COLON-ALIGNED WIDGET-ID 46
     edReason AT ROW 11.71 COL 14 NO-LABEL WIDGET-ID 50
     bOK AT ROW 16 COL 23.4 WIDGET-ID 24
     bCancel AT ROW 16 COL 39.4 WIDGET-ID 22
     "Reason:" VIEW-AS TEXT
          SIZE 8.6 BY .62 AT ROW 11.86 COL 5.4 WIDGET-ID 36
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 4.05 COL 3 WIDGET-ID 34
     SPACE(63.19) SKIP(12.84)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Defer Action" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edComments:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tEntity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tEntityID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tOrigDueDt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tOrigFollDt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tOrigStartDt:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Defer Action */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOK Dialog-Frame
ON CHOOSE OF bOK IN FRAME Dialog-Frame /* OK */
DO:
  assign
    pStartNew    = tOrigStartDt:input-value
    pDueNew      = tOrigDueDt:input-value
    pFollowupNew = tOrigFollDt:input-value
    pReason      = edReason:input-value
    pCancel      = false
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edReason
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edReason Dialog-Frame
ON VALUE-CHANGED OF edReason IN FRAME Dialog-Frame
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNewDueDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNewDueDt Dialog-Frame
ON LEAVE OF tNewDueDt IN FRAME Dialog-Frame /* New Due */
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNewFollupDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNewFollupDt Dialog-Frame
ON LEAVE OF tNewFollupDt IN FRAME Dialog-Frame /* New Followup */
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNewStartDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNewStartDt Dialog-Frame
ON LEAVE OF tNewStartDt IN FRAME Dialog-Frame /* New Start */
DO:
  run enableOK in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  run setData.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableOK Dialog-Frame 
PROCEDURE enableOK :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    bOK:sensitive = (not tNewStartDt:input-value = ? and 
                     not tNewDueDt:input-value = ? and 
                     not tNewFollupDt:input-value = ? and
                     not edReason:input-value > "").
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEntity tEntityID edComments tOrigStartDt tNewStartDt tOrigDueDt 
          tNewDueDt tOrigFollDt tNewFollupDt edReason 
      WITH FRAME Dialog-Frame.
  ENABLE tEntity tEntityID edComments tOrigStartDt tNewStartDt tOrigDueDt 
         tNewDueDt tOrigFollDt tNewFollupDt edReason bOK bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      tEntity:screen-value      = pEntity
      tEntityID:screen-value    = pEntityID
      edComments:screen-value   = pComments
      tOrigStartDt:screen-value = string(pStartOrig)
      tOrigDueDt:screen-value   = string(pDueOrig)
      tOrigFollDt:screen-value  = string(pFollowupOrig)
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

