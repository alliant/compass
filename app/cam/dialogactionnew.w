&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
{tt/action.i}
{tt/finding.i}
{tt/agent.i}
{tt/attorney.i}

/* Parameters Definitions ---                                           */
define input  parameter table for finding.
define output parameter table for action.
define output parameter pSave as logical no-undo initial false.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-55 RECT-58 cbSubject tSource tQuestion ~
tFinding tType tOwner tStartDate tOpenedDate tFollowupDate tAction bSave ~
bCancel 
&Scoped-Define DISPLAYED-OBJECTS tEntity cbSubject tSeverity tSource ~
tQuestion tFinding tType tOwner tStatus tStartDate tOpenedDate ~
tFollowupDate tAction 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbSubject AS CHARACTER INITIAL "ALL" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 90 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Severity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2 NO-UNDO.

DEFINE VARIABLE tFollowupDate AS DATE FORMAT "99/99/99":U 
     LABEL "Followup" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tOpenedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Opened" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 TOOLTIP "What business process found the issue?" NO-UNDO.

DEFINE VARIABLE tStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Start" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 5.71.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 7.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tEntity AT ROW 2.19 COL 14 COLON-ALIGNED WIDGET-ID 298
     cbSubject AT ROW 2.19 COL 37 COLON-ALIGNED NO-LABEL WIDGET-ID 294
     tSeverity AT ROW 3.38 COL 14 COLON-ALIGNED WIDGET-ID 310
     tSource AT ROW 3.38 COL 45 COLON-ALIGNED WIDGET-ID 312
     tQuestion AT ROW 4.57 COL 16 NO-LABEL WIDGET-ID 308
     tFinding AT ROW 6.71 COL 16 NO-LABEL WIDGET-ID 306
     tType AT ROW 10.52 COL 14 COLON-ALIGNED WIDGET-ID 288
     tOwner AT ROW 10.52 COL 57 COLON-ALIGNED WIDGET-ID 314
     tStatus AT ROW 10.52 COL 102 COLON-ALIGNED WIDGET-ID 292
     tStartDate AT ROW 11.71 COL 14 COLON-ALIGNED WIDGET-ID 42
     tOpenedDate AT ROW 11.71 COL 57 COLON-ALIGNED WIDGET-ID 82
     tFollowupDate AT ROW 11.71 COL 102 COLON-ALIGNED WIDGET-ID 232
     tAction AT ROW 12.91 COL 16 NO-LABEL WIDGET-ID 192
     bSave AT ROW 15.76 COL 51.4 WIDGET-ID 10
     bCancel AT ROW 15.76 COL 67.2 WIDGET-ID 24
     "Question:" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 4.71 COL 6.4 WIDGET-ID 304
     "Finding:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 6.86 COL 8 WIDGET-ID 302
     "Finding" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 300
     "Action:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 13.05 COL 8.8 WIDGET-ID 216
     "Action" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 9.57 COL 4 WIDGET-ID 224
     RECT-55 AT ROW 9.81 COL 3 WIDGET-ID 76
     RECT-58 AT ROW 1.48 COL 3 WIDGET-ID 296
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 131.8 BY 16.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "New Action"
         HEIGHT             = 16.24
         WIDTH              = 131.8
         MAX-HEIGHT         = 37.81
         MAX-WIDTH          = 164.4
         VIRTUAL-HEIGHT     = 37.81
         VIRTUAL-WIDTH      = 164.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR COMBO-BOX tEntity IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       tFinding:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tQuestion:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR COMBO-BOX tSeverity IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       tSource:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR COMBO-BOX tStatus IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* New Action */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* New Action */
DO:
  {lib/confirm-close.i "Action"}
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME DEFAULT-FRAME /* Cancel */
do:
  apply "WINDOW-CLOSE" to {&window-name}.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME DEFAULT-FRAME /* Save */
DO:  
  for first finding no-lock:
    create action.
    assign
      action.source       = finding.source
      action.sourceID     = finding.sourceID
      action.findingID    = finding.findingID
      action.comments     = tAction:screen-value
      action.actionType   = tType:screen-value
      action.ownerID      = tOwner:screen-value
      action.stat         = tStatus:screen-value
      action.createdDate  = today
      action.startDate    = tStartDate:input-value
      action.openedDate   = tOpenedDate:input-value
      action.followupDate = tFollowupDate:input-value
      pSave               = true
      .
  end.
  apply "CLOSE":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntity C-Win
ON VALUE-CHANGED OF tEntity IN FRAME DEFAULT-FRAME /* Entity */
DO:
  cbSubject:delimiter = ",".
  cbSubject:inner-lines = 15.
  run AgentComboHide in this-procedure (true).
  run AttorneyComboHide in this-procedure (true).
  case self:screen-value:
   when "A" then
    do:
      cbSubject:delimiter = {&msg-dlm}.
      run AgentComboHide in this-procedure (false).
    end.
   when "T" then
    do:
      cbSubject:delimiter = {&msg-dlm}.
      run AttorneyComboHide in this-procedure (false).
    end.
   when "C" then
    do:
      cbSubject:hidden = true. 
    end.
   when "D" then
    do:
      std-ch = "".
      publish "GetDeptCombo" (output std-ch).
      if std-ch > ""
       then cbSubject:list-item-pairs = "ALL,ALL," + std-ch.
       else cbSubject:list-item-pairs = "ALL,ALL".
      cbSubject:screen-value = "ALL".
    end.
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-attorney-list.i &combo=cbsubject}
  {lib/get-agent-list.i &combo=cbsubject}
  {lib/get-sysprop-list.i &combo=tEntity &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Entity'"}
  {lib/get-sysprop-list.i &combo=tSeverity &appCode="'CAM'" &objAction="'Finding'" &objProperty="'Severity'"}
  {lib/get-sysprop-list.i &combo=tStatus &appCode="'CAM'" &objAction="'Action'" &objProperty="'Status'" &d="'P'"}
  {lib/get-sysprop-list.i &combo=tType &appCode="'CAM'" &objAction="'Action'" &objProperty="'ActionType'"}
  {lib/get-sysprop-list.i &combo=tOwner &appCode="'CAM'" &objAction="'ActionOwner'"}
  
  run setData in this-procedure.
      
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEntity cbSubject tSeverity tSource tQuestion tFinding tType tOwner 
          tStatus tStartDate tOpenedDate tFollowupDate tAction 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-55 RECT-58 cbSubject tSource tQuestion tFinding tType tOwner 
         tStartDate tOpenedDate tFollowupDate tAction bSave bCancel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    /* finding */
    cbSubject:delimiter = ",".
    cbSubject:inner-lines = 15.
    run AgentComboHide (true).
    run AttorneyComboHide (true).
    for first finding no-lock:
      std-ch = if finding.findingID = 0 then "New Finding" else "Finding " + string(finding.findingID).
      {&window-name}:title = "New Action for " + std-ch.
      case finding.refType:
       when "A" then
        do:
          cbSubject:delimiter = {&msg-dlm}.
          run AgentComboHide (false).
          run AgentComboSet (finding.entityID).
        end.
       when "T" then
        do:
          cbSubject:delimiter = {&msg-dlm}.
          run AttorneyComboHide (false).
          run AttorneyComboSet (finding.entityID).
        end.
       when "C" then cbSubject:hidden = true.
       when "D" then cbSubject:screen-value = finding.entityID.
      end case.
      assign
        tEntity:screen-value = finding.refType
        tSeverity:screen-value = string(finding.severity)
        tSource:screen-value = finding.source + (if finding.sourceID > "" then " (" + finding.sourceID + ")" else "")
        tQuestion:screen-value = finding.sourceQuestion
        tFinding:screen-value = finding.description
        tStartDate:screen-value = string(today)
        tOpenedDate:screen-value = string(today)
        .
      run AgentComboEnable (false).
      run AttorneyComboEnable (false).
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

