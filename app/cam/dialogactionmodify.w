&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
{tt/action.i}
{tt/actionnote.i}
{tt/finding.i}
{tt/agent.i}
{tt/attorney.i}

/* Parameters Definitions ---                                           */
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwNotes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES actionNote

/* Definitions for BROWSE brwNotes                                      */
&Scoped-define FIELDS-IN-QUERY-brwNotes actionnote.datecreated actionnote.category actionnote.UserName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwNotes   
&Scoped-define SELF-NAME brwNotes
&Scoped-define QUERY-STRING-brwNotes for each actionNote
&Scoped-define OPEN-QUERY-brwNotes OPEN QUERY {&SELF-NAME} for each actionNote.
&Scoped-define TABLES-IN-QUERY-brwNotes actionNote
&Scoped-define FIRST-TABLE-IN-QUERY-brwNotes actionNote


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwNotes}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bDefer bReassigned bCancelled bComplete ~
bEstablish bPlanned RECT-55 RECT-58 RECT-59 iIcon RECT-56 tEntity tSeverity ~
tSource tQuestion tFinding tActionID tCreatedDate tFollowupDate tType ~
tStartDate tDueDate tOwner tOpenedDate tOriginalDueDate tStatus ~
tCompletedDate tExtendedDate tCompleteStatus tStartGap tAction bNew ~
brwNotes bModify bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tEntity cbSubject tSeverity tSource ~
tQuestion tFinding tActionID tCreatedDate tFollowupDate tType tStartDate ~
tDueDate tOwner tOpenedDate tOriginalDueDate tStatus tCompletedDate ~
tExtendedDate tCompleteStatus tStartGap tAction edComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bCancelled  NO-FOCUS
     LABEL "Mark as Cancelled" 
     SIZE 7.2 BY 1.71 TOOLTIP "Mark as Cancelled".

DEFINE BUTTON bComplete  NO-FOCUS
     LABEL "Mark as Completed" 
     SIZE 7.2 BY 1.71 TOOLTIP "Mark as Completed".

DEFINE BUTTON bDefer  NO-FOCUS
     LABEL "Defer" 
     SIZE 7.2 BY 1.71 TOOLTIP "Extend Action".

DEFINE BUTTON bEstablish  NO-FOCUS
     LABEL "Mark as Opened" 
     SIZE 7.2 BY 1.71 TOOLTIP "Mark as Opened".

DEFINE BUTTON bModify 
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Edit Action".

DEFINE BUTTON bNew 
     LABEL "Add" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bPdf  NO-FOCUS
     LABEL "Pdf" 
     SIZE 7.2 BY 1.71 TOOLTIP "PDF".

DEFINE BUTTON bPlanned  NO-FOCUS
     LABEL "Mark as Planned" 
     SIZE 7.2 BY 1.71 TOOLTIP "Mark as Planned".

DEFINE BUTTON bReassigned  NO-FOCUS
     LABEL "Reassign" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reassign Action".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbSubject AS CHARACTER INITIAL "ALL" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 90 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE edComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 30 BY 5.95 NO-UNDO.

DEFINE VARIABLE tAction AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2.14 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2.14 NO-UNDO.

DEFINE VARIABLE tQuestion AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 113 BY 2.14 NO-UNDO.

DEFINE VARIABLE tActionID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action ID" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tCompletedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Completed" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tCompleteStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Complete" 
     VIEW-AS FILL-IN 
     SIZE 63 BY 1 NO-UNDO.

DEFINE VARIABLE tCreatedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Created" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tExtendedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Extended" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tFollowupDate AS DATE FORMAT "99/99/99":U 
     LABEL "Followup" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOpenedDate AS DATE FORMAT "99/99/99":U 
     LABEL "Opened" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOriginalDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Originally Due" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tOwner AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSeverity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Severity" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSource AS CHARACTER FORMAT "X(256)":U 
     LABEL "Source" 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 TOOLTIP "What business process found the issue?" NO-UNDO.

DEFINE VARIABLE tStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Started" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tStartGap AS CHARACTER FORMAT "X(256)":U 
     LABEL "Start Gap" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 DROP-TARGET NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE IMAGE iIcon
     SIZE 3.6 BY .86.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 9.29.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 7.14.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 128 BY 8.33.

DEFINE RECTANGLE RECT-59
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 53 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwNotes FOR 
      actionNote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwNotes C-Win _FREEFORM
  QUERY brwNotes DISPLAY
      actionnote.datecreated width 15
actionnote.category width 20
actionnote.UserName
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 82 BY 5.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bPdf AT ROW 1.62 COL 48 WIDGET-ID 276 NO-TAB-STOP 
     bDefer AT ROW 1.62 COL 11 WIDGET-ID 186 NO-TAB-STOP 
     bReassigned AT ROW 1.62 COL 3.6 WIDGET-ID 244 NO-TAB-STOP 
     bCancelled AT ROW 1.62 COL 40.6 WIDGET-ID 190 NO-TAB-STOP 
     bComplete AT ROW 1.62 COL 33.2 WIDGET-ID 208 NO-TAB-STOP 
     bEstablish AT ROW 1.62 COL 25.8 WIDGET-ID 242 NO-TAB-STOP 
     bPlanned AT ROW 1.62 COL 18.4 WIDGET-ID 250 NO-TAB-STOP 
     tEntity AT ROW 4.81 COL 14 COLON-ALIGNED WIDGET-ID 324
     cbSubject AT ROW 4.81 COL 37 COLON-ALIGNED NO-LABEL WIDGET-ID 294
     tSeverity AT ROW 6 COL 14 COLON-ALIGNED WIDGET-ID 326
     tSource AT ROW 6 COL 45 COLON-ALIGNED WIDGET-ID 312
     tQuestion AT ROW 7.19 COL 16 NO-LABEL WIDGET-ID 308
     tFinding AT ROW 9.57 COL 16 NO-LABEL WIDGET-ID 306
     tActionID AT ROW 13.38 COL 14 COLON-ALIGNED WIDGET-ID 228
     tCreatedDate AT ROW 13.38 COL 57 COLON-ALIGNED WIDGET-ID 220
     tFollowupDate AT ROW 13.38 COL 107 COLON-ALIGNED WIDGET-ID 232
     tType AT ROW 14.57 COL 14 COLON-ALIGNED WIDGET-ID 288
     tStartDate AT ROW 14.57 COL 57 COLON-ALIGNED WIDGET-ID 42
     tDueDate AT ROW 14.57 COL 107 COLON-ALIGNED WIDGET-ID 40
     tOwner AT ROW 15.76 COL 14 COLON-ALIGNED WIDGET-ID 322
     tOpenedDate AT ROW 15.76 COL 57 COLON-ALIGNED WIDGET-ID 82
     tOriginalDueDate AT ROW 15.76 COL 107 COLON-ALIGNED WIDGET-ID 222
     tStatus AT ROW 16.95 COL 14 COLON-ALIGNED WIDGET-ID 318
     tCompletedDate AT ROW 16.95 COL 57 COLON-ALIGNED WIDGET-ID 218
     tExtendedDate AT ROW 16.95 COL 107 COLON-ALIGNED WIDGET-ID 86
     tCompleteStatus AT ROW 18.14 COL 14 COLON-ALIGNED WIDGET-ID 320
     tStartGap AT ROW 18.14 COL 107 COLON-ALIGNED WIDGET-ID 280
     tAction AT ROW 19.33 COL 16 NO-LABEL WIDGET-ID 192
     bNew AT ROW 23.1 COL 10.8 WIDGET-ID 238
     brwNotes AT ROW 23.14 COL 16 WIDGET-ID 200
     edComments AT ROW 23.14 COL 99 NO-LABEL WIDGET-ID 278
     bModify AT ROW 24.29 COL 10.8 WIDGET-ID 240
     bSave AT ROW 29.81 COL 51.6 WIDGET-ID 10
     bCancel AT ROW 29.81 COL 67.4 WIDGET-ID 24
     "Finding:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 9.71 COL 8 WIDGET-ID 302
     "Question:" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 7.33 COL 6.4 WIDGET-ID 304
     "Action:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 19.48 COL 8.8 WIDGET-ID 216
     "Action" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 12.43 COL 4 WIDGET-ID 224
     "Action Notes" VIEW-AS TEXT
          SIZE 12.6 BY .62 AT ROW 22.19 COL 4 WIDGET-ID 236
     "Finding" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 3.62 COL 4 WIDGET-ID 300
     RECT-55 AT ROW 12.67 COL 3 WIDGET-ID 76
     RECT-58 AT ROW 3.86 COL 3 WIDGET-ID 296
     RECT-59 AT ROW 1.48 COL 3 WIDGET-ID 248
     iIcon AT ROW 13.48 COL 27 WIDGET-ID 268
     RECT-56 AT ROW 22.43 COL 3 WIDGET-ID 234
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 131.8 BY 30.38 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Modify Action"
         HEIGHT             = 30.38
         WIDTH              = 131.8
         MAX-HEIGHT         = 37.81
         MAX-WIDTH          = 164.4
         VIRTUAL-HEIGHT     = 37.81
         VIRTUAL-WIDTH      = 164.4
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwNotes bNew DEFAULT-FRAME */
ASSIGN 
       bCancelled:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "X".

ASSIGN 
       bComplete:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "P".

/* SETTINGS FOR BUTTON bPdf IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bPdf:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "X".

ASSIGN 
       brwNotes:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwNotes:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR COMBO-BOX cbSubject IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR edComments IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       tAction:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tActionID:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tCompletedDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tCompleteStatus:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tCreatedDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDueDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tEntity:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tExtendedDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tFinding:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tFollowupDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tOpenedDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tOriginalDueDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tOwner:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tQuestion:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tSeverity:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tSource:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tStartDate:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tStartGap:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tStatus:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwNotes
/* Query rebuild information for BROWSE brwNotes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} for each actionNote.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwNotes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* New Action */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* New Action */
DO:
  {lib/confirm-close.i "Action"}
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME DEFAULT-FRAME /* Cancel */
do:
  apply "WINDOW-CLOSE" to {&window-name}.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancelled
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancelled C-Win
ON CHOOSE OF bCancelled IN FRAME DEFAULT-FRAME /* Mark as Cancelled */
do:
  run dialogactioncancel.w (output std-ch, output std-lo).

  if not std-lo and valid-handle(hFileDataSrv)
   then
    do:
      run CancelAction in hFileDataSrv (std-ch, output std-lo).
      if std-lo
       then run setData in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bComplete C-Win
ON CHOOSE OF bComplete IN FRAME DEFAULT-FRAME /* Mark as Completed */
do:
  define variable pStatus as character no-undo.
  define variable pReason as character no-undo.
  run dialogactioncomplete.w (output pStatus, output pReason, output std-lo).

  if not std-lo and valid-handle(hFileDataSrv)
   then
    do:
      run CompleteAction in hFileDataSrv (pStatus, pReason, output std-lo).
      if std-lo
       then run setData in this-procedure.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDefer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDefer C-Win
ON CHOOSE OF bDefer IN FRAME DEFAULT-FRAME /* Defer */
DO:
  for first action no-lock:
    run dialogactionextend.w (input-output action.dueDate,
                              input-output action.followupDate,
                              output       std-ch,
                              output       std-lo).
                              
    if not std-lo and valid-handle(hFileDataSrv)
     then
      do:
        run ExtendAction in hFileDataSrv (std-ch, action.dueDate, action.followupDate, output std-lo).
        if std-lo
         then run setData in this-procedure.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEstablish
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEstablish C-Win
ON CHOOSE OF bEstablish IN FRAME DEFAULT-FRAME /* Mark as Opened */
do:
  if valid-handle(hFileDataSrv)
   then
    for first action no-lock:
      /* for planned actions */
      if action.stat = "P"
       then
        do:
          run dialogactionestablish.w (input-output action.ownerID,
                                       input-output action.startDate,
                                       input-output action.followupDate,
                                       output       action.dueDate,
                                       output       std-lo).
                              
          if not std-lo
           then
            do:
              run EstablishAction in hFileDataSrv (action.ownerID, action.startDate, action.followupDate, action.dueDate, output std-lo).
              if std-lo
               then run setData in this-procedure.
            end.
        end.
      
      /* for closed actions */
      if action.stat = "C"
       then
        do:
          run dialogactionrevert.w (output std-ch,
                                    output std-lo).
                              
          if not std-lo
           then
            do:
              run RevertAction in hFileDataSrv (std-ch, output std-lo).
              if std-lo
               then run setData in this-procedure.
            end.
        end.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME DEFAULT-FRAME /* Edit */
DO:
  if not available actionnote
   then return.
  
  for first action no-lock:
    run dialogactionnote.w (input        false,
                            input        action.actionID, 
                            input-output actionnote.category,
                            input-output actionnote.comments,
                            output       std-lo).
    
    if not std-lo and valid-handle(hFileDataSrv)
     then
      do:
        run ModifyNote in hFileDataSrv (actionnote.seq, actionnote.category, actionnote.comments, output std-lo).
        if std-lo
         then run setData in this-procedure.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* Add */
DO:
  define variable pCategory as character no-undo initial "".
  define variable pNote as character no-undo initial "".
  
  for first action no-lock:
    run dialogactionnote.w (input        true,
                            input        action.actionID, 
                            input-output pCategory,
                            input-output pNote,
                            output       std-lo).
    
    if not std-lo and valid-handle(hFileDataSrv)
     then
      do:
        run NewNote in hFileDataSrv (pCategory, pNote, output std-lo).
        if std-lo
         then run setData in this-procedure.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPlanned
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPlanned C-Win
ON CHOOSE OF bPlanned IN FRAME DEFAULT-FRAME /* Mark as Planned */
do:
  run dialogactionrevert.w (output std-ch,
                            output std-lo). 
                      
  if not std-lo and valid-handle(hFileDataSrv)
   then
    do:
      run RevertAction in hFileDataSrv (std-ch, output std-lo).
      if std-lo
       then run setData in this-procedure.
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReassigned
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReassigned C-Win
ON CHOOSE OF bReassigned IN FRAME DEFAULT-FRAME /* Reassign */
do:
  for first action no-lock:
    run dialogactionreassign.w (input-output action.ownerID,
                                output       std-ch,
                                output       std-lo).
                      
    if not std-lo and valid-handle(hFileDataSrv)
     then
      do:
        run ReassignAction in hFileDataSrv (action.ownerID, std-ch, output std-lo).
        if std-lo
         then run setData in this-procedure.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwNotes
&Scoped-define SELF-NAME brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON DEFAULT-ACTION OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  if actionNote.category ne "system" then
  apply "CHOOSE" to bModify in frame DEFAULT-FRAME.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON ROW-DISPLAY OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON START-SEARCH OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes C-Win
ON VALUE-CHANGED OF brwNotes IN FRAME DEFAULT-FRAME
DO:
  if available actionNote then
  do:
    edComments:screen-value = actionNote.comments.
    if actionNote.category eq "system" then
      disable bModify with frame DEFAULT-FRAME.
    else
      enable bModify with frame DEFAULT-FRAME.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME DEFAULT-FRAME /* Save */
DO:
  for first finding no-lock:
    for first action exclusive-lock:
      assign
        action.actionType   = tType:screen-value
        action.comments     = tAction:screen-value
        .
    end.
    run ModifyAction in hFileDataSrv (table action, output std-lo).
    run ShowWindow in this-procedure.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}
{lib/brw-main.i}
{lib/set-buttons.i &exclude="'Save,Cancel'"}

subscribe to "ActionChanged" anywhere.
subscribe to "FindingChanged" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       
bCancelled:load-image("images/cancel.bmp").
bCancelled:load-image-insensitive("images/cancel-i.bmp").
bReassigned:load-image("images/usergo.bmp").
bReassigned:load-image-insensitive("images/usergo-i.bmp").
bComplete:load-image("images/Check.bmp").
bComplete:load-image-insensitive("images/Check-i.bmp").
bEstablish:load-image ("images/import.bmp").
bEstablish:load-image-insensitive("images/import-i.bmp").
bDefer:load-image ("images/clock.bmp").
bDefer:load-image-insensitive("images/clock-i.bmp").
bReassigned:load-image ("images/users.bmp").
bReassigned:load-image-insensitive("images/users-i.bmp").
bPlanned:load-image ("images/task.bmp").
bPlanned:load-image-insensitive("images/task-i.bmp").
bPdf:load-image ("images/pdf.bmp").
bPdf:load-image-insensitive("images/pdf-i.bmp").

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-attorney-list.i &combo=cbsubject}
  {lib/get-agent-list.i &combo=cbsubject}
  {lib/get-sysprop-list.i &combo=tType &appCode="'CAM'" &objAction="'Action'" &objProperty="'ActionType'"}

  run setData in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionChanged C-Win 
PROCEDURE ActionChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pActionID as integer no-undo.

  if can-find(first action where actionID = pActionID) and valid-handle(hFileDataSrv)
   then run setData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEntity cbSubject tSeverity tSource tQuestion tFinding tActionID 
          tCreatedDate tFollowupDate tType tStartDate tDueDate tOwner 
          tOpenedDate tOriginalDueDate tStatus tCompletedDate tExtendedDate 
          tCompleteStatus tStartGap tAction edComments 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bDefer bReassigned bCancelled bComplete bEstablish bPlanned RECT-55 
         RECT-58 RECT-59 iIcon RECT-56 tEntity tSeverity tSource tQuestion 
         tFinding tActionID tCreatedDate tFollowupDate tType tStartDate 
         tDueDate tOwner tOpenedDate tOriginalDueDate tStatus tCompletedDate 
         tExtendedDate tCompleteStatus tStartGap tAction bNew brwNotes bModify 
         bSave bCancel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FindingChanged C-Win 
PROCEDURE FindingChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pFindingID as integer no-undo.

  if can-find(first finding where findingID = pFindingID) and valid-handle(hFileDataSrv)
   then run setData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pStat as character.
  
  do with frame {&frame-name}:
    disable bReassigned bCancelled bPlanned bComplete bDefer bEstablish.

    case pStat:
     when "P" then enable bEstablish bCancelled bReassigned.
     when "O" then enable bComplete bDefer bCancelled bReassigned.
     when "C" then enable bEstablish.
     when "X" then enable bPlanned.
    end case.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData C-Win 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hFileDataSrv)
   then
    do with frame {&frame-name}:
      run GetFinding in hFileDataSrv (output table finding).
      run GetAction in hFileDataSrv (output table action).
      run GetActionNotes in hFileDataSrv (output table actionnote).
      
      /* finding */
      cbSubject:delimiter = ",".
      cbSubject:inner-lines = 15.
      run AgentComboHide (true).
      run AttorneyComboHide (true).
      for first finding no-lock:
        publish "GetSysPropDesc" ("CAM", "Finding", "Entity", finding.refType, output finding.entityName).
        publish "GetSysPropDesc" ("CAM", "Finding", "Severity", string(finding.severity), output finding.severityDesc).
        case finding.refType:
         when "A" then
          do:
            cbSubject:delimiter = {&msg-dlm}.
            run AgentComboHide (false).
            run AgentComboSet (finding.entityID).
          end.
         when "T" then
          do:
            cbSubject:delimiter = {&msg-dlm}.
            run AttorneyComboHide (false).
            run AttorneyComboSet (finding.entityID).
          end.
         when "C" then cbSubject:hidden = true.
         when "D" then cbSubject:screen-value = finding.entityID.
        end case.
        assign
          tEntity:screen-value = finding.entityName
          tSeverity:screen-value = finding.severityDesc
          tSource:screen-value = finding.source + (if finding.sourceID > "" then " (" + finding.sourceID + ")" else "")
          tQuestion:screen-value = finding.sourceQuestion
          tFinding:screen-value = finding.description
          .
        run AgentComboEnable (false).
        run AttorneyComboEnable (false).
      end.
      for first action exclusive-lock:
        publish "GetSysPropDesc" ("CAM", "Action", "Status", action.stat, output action.statDesc).
        publish "GetSysPropDesc" ("CAM", "Action", "CompleteStatus", action.completeStatus, output action.completeStatusDesc).
        publish "GetSysPropDesc" ("CAM", "ActionOwner", "", action.ownerID, output action.ownerDesc).
        assign
          tActionID:screen-value = string(action.actionID)
          tType:screen-value = action.actionType
          tOwner:screen-value = action.ownerDesc
          tStatus:screen-value = action.statDesc
          tCompleteStatus:screen-value = action.completeStatusDesc
          tCreatedDate:screen-value = string(action.createdDate)
          tStartDate:screen-value = string(action.startDate)
          tOpenedDate:screen-value = string(action.openedDate)
          tCompletedDate:screen-value = string(action.completedDate)
          tFollowupDate:screen-value = string(action.followupDate)
          tOriginalDueDate:screen-value = string(action.originalDueDate)
          tDueDate:screen-value = string(action.dueDate)
          tExtendedDate:screen-value = string(action.extendedDate)
          tAction:screen-value = action.comments
          tStartGap:screen-value = (if action.startDate = ? then "" else string(interval(action.startDate, action.openedDate, "days") + 1))
          tType:sensitive = not (action.stat = "X" or action.stat = "C")
          tAction:read-only = (action.stat = "X" or action.stat = "C")
          .
        run setImage in this-procedure (action.iconSeverity, action.iconMessages).
        run setButtons in this-procedure (action.stat).
        {&OPEN-QUERY-brwNotes}
        apply "VALUE-CHANGED" to browse {&browse-name}.
        run ShowWindow in this-procedure.
      end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setImage C-Win 
PROCEDURE setImage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pSeverity as integer no-undo.
  define input parameter pMessage  as character no-undo.

  do with frame {&frame-name}:
    if pSeverity = 3 then
      iIcon:load-image ("images/smiley-sqare-red-18.gif").
    else if pSeverity = 2 then
      iIcon:load-image ("images/smiley-sqare-yellow-18.gif").
    else if pSeverity = 1 then
      iIcon:load-image ("images/smiley-sqare-green-18.gif").
      
    iIcon:tooltip = replace(trim(pMessage,","),",",chr(10)).
  end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

