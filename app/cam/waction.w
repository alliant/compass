&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/*  def var pActionID as int no-undo.  */
def input parameter pActionID as int.
def input parameter pAgentID as char.
def input parameter pAgentName as char.
def input parameter pState as char.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* {tt/qar.i}  */
/* Used for display within this program */
{tt/qaraction.i &tableAlias="actionUpd"}
{tt/qaractionnote.i &tableAlias="noteUpd"}

def var activeUser as char no-undo.
def var hParentProcedure as handle no-undo.

/* static values */
publish "GetCredentialsID" (output activeUser).
hParentProcedure = source-procedure.

{lib/std-def.i}
/* {util/cal-def.i} */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwNotes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES noteUpd

/* Definitions for BROWSE brwNotes                                      */
&Scoped-define FIELDS-IN-QUERY-brwNotes noteUpd.noteDate noteUpd.takenBy noteUpd.notes   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwNotes   
&Scoped-define SELF-NAME brwNotes
&Scoped-define QUERY-STRING-brwNotes for each noteUpd                           by noteUpd.seq descending
&Scoped-define OPEN-QUERY-brwNotes OPEN QUERY {&SELF-NAME} for each noteUpd                           by noteUpd.seq descending.
&Scoped-define TABLES-IN-QUERY-brwNotes noteUpd
&Scoped-define FIRST-TABLE-IN-QUERY-brwNotes noteUpd


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwNotes}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAction brwNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAction wWin 
FUNCTION displayAction RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD reloadAction wWin 
FUNCTION reloadAction RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Action 
       MENU-ITEM m_Modify       LABEL "Modify"        
       MENU-ITEM m_Save_Changes LABEL "Save"          
       MENU-ITEM m_Cancel_Changes LABEL "Cancel"        
       MENU-ITEM m_Add_Note     LABEL "Add Note..."   
       MENU-ITEM m_attach       LABEL "Attachments..."
       MENU-ITEM m_Close        LABEL "Close"         .

DEFINE SUB-MENU m_Status 
       MENU-ITEM m_Change_to_Open LABEL "Open..."       
       MENU-ITEM m_Change_to_In_Process LABEL "In Process..." 
       MENU-ITEM m_Change_to_Completed LABEL "Complete..."   
       MENU-ITEM m_Change_to_Rejected LABEL "Reject..."     .

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_Action       LABEL "Action"        
       SUB-MENU  m_Status       LABEL "Status"        .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddNote  NO-FOCUS
     LABEL "Note" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add Note".

DEFINE BUTTON bAttach  NO-FOCUS
     LABEL "Attach" 
     SIZE 7.2 BY 1.71 TOOLTIP "Attachments...".

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel".

DEFINE BUTTON bClose  NO-FOCUS
     LABEL "Close" 
     SIZE 7.2 BY 1.71 TOOLTIP "Close".

DEFINE BUTTON bComplete  NO-FOCUS
     LABEL "Comp" 
     SIZE 7.2 BY 1.71 TOOLTIP "Completed...".

DEFINE BUTTON bHelp  NO-FOCUS
     LABEL "Help" 
     SIZE 7.2 BY 1.71 TOOLTIP "Help".

DEFINE BUTTON bInProcess  NO-FOCUS
     LABEL "InProc" 
     SIZE 7.2 BY 1.71 TOOLTIP "In Process...".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open".

DEFINE BUTTON bReject  NO-FOCUS
     LABEL "Reject" 
     SIZE 7.2 BY 1.71 TOOLTIP "Rejected...".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save".

DEFINE RECTANGLE rectButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86.2 BY 1.95.

DEFINE BUTTON bDueDate  NO-FOCUS
     LABEL "v" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bFollowupDate  NO-FOCUS
     LABEL "v" 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE tAction AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 1000 SCROLLBAR-VERTICAL
     SIZE 82 BY 3.57 NO-UNDO.

DEFINE VARIABLE tFinding AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 82 BY 2.62 NO-UNDO.

DEFINE VARIABLE tQuestionDesc AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 500
     SIZE 68 BY 1.91 NO-UNDO.

DEFINE VARIABLE tReference AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 82 BY 1.19 NO-UNDO.

DEFINE VARIABLE tActionType AS CHARACTER FORMAT "X(256)":U INITIAL "Recommendation" 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Due Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFiles AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent Files" 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 NO-UNDO.

DEFINE VARIABLE tFollowupDate AS DATE FORMAT "99/99/99":U 
     LABEL "Follow Up" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tQuestionID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Question" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1
     FONT 6 NO-UNDO.

DEFINE RECTANGLE rectAction
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100 BY 4.52.

DEFINE RECTANGLE rectFinding
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100 BY 6.19.

DEFINE RECTANGLE rectNotes
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100 BY 7.86.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwNotes FOR 
      noteUpd SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwNotes wWin _FREEFORM
  QUERY brwNotes DISPLAY
      noteUpd.noteDate label "Date" format "99/99/9999"
 noteUpd.takenBy label "By"         format "x(40)"
 noteUpd.notes label "Comments"     format "x(60)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 97 BY 6.67 ROW-HEIGHT-CHARS .62 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bFollowupDate AT ROW 4.33 COL 63 WIDGET-ID 142 NO-TAB-STOP 
     tActionType AT ROW 3.19 COL 12 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     tDueDate AT ROW 3.19 COL 47 COLON-ALIGNED WIDGET-ID 30
     tStatus AT ROW 4.33 COL 12.2 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tFollowupDate AT ROW 4.38 COL 47 COLON-ALIGNED WIDGET-ID 32 NO-TAB-STOP 
     bDueDate AT ROW 3.14 COL 63 WIDGET-ID 136 NO-TAB-STOP 
     tQuestionID AT ROW 5.57 COL 12 COLON-ALIGNED WIDGET-ID 8 NO-TAB-STOP 
     tQuestionDesc AT ROW 5.57 COL 29 NO-LABEL WIDGET-ID 6 NO-TAB-STOP 
     tFinding AT ROW 8.19 COL 15 NO-LABEL WIDGET-ID 10 NO-TAB-STOP 
     tFiles AT ROW 11.05 COL 13 COLON-ALIGNED WIDGET-ID 24 NO-TAB-STOP 
     tReference AT ROW 12.48 COL 15 NO-LABEL WIDGET-ID 26 NO-TAB-STOP 
     tAction AT ROW 14.62 COL 15 NO-LABEL WIDGET-ID 16
     brwNotes AT ROW 19.62 COL 3 WIDGET-ID 200
     "Reference:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 12.48 COL 3.8 WIDGET-ID 28
     "Notes" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 18.67 COL 4 WIDGET-ID 36
          FONT 6
     "Action" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 13.91 COL 4 WIDGET-ID 20
          FONT 6
     "Finding" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 7.48 COL 4 WIDGET-ID 14
          FONT 6
     rectFinding AT ROW 7.71 COL 2 WIDGET-ID 12
     rectAction AT ROW 14.14 COL 2 WIDGET-ID 18
     rectNotes AT ROW 18.91 COL 2 WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.14
         SIZE 102.2 BY 25.81 WIDGET-ID 100.

DEFINE FRAME fButtons
     bAddNote AT ROW 1.14 COL 34 RIGHT-ALIGNED WIDGET-ID 190 NO-TAB-STOP 
     bAttach AT ROW 1.14 COL 35.2 WIDGET-ID 192 NO-TAB-STOP 
     bCancel AT ROW 1.14 COL 20.4 WIDGET-ID 188 NO-TAB-STOP 
     bClose AT ROW 1.14 COL 42.8 WIDGET-ID 194 NO-TAB-STOP 
     bComplete AT ROW 1.14 COL 65.4 WIDGET-ID 200 NO-TAB-STOP 
     bHelp AT ROW 1.14 COL 80.2 WIDGET-ID 204 NO-TAB-STOP 
     bInProcess AT ROW 1.14 COL 58 WIDGET-ID 198 NO-TAB-STOP 
     bModify AT ROW 1.14 COL 5.4 WIDGET-ID 184 NO-TAB-STOP 
     bOpen AT ROW 1.14 COL 50.4 WIDGET-ID 196 NO-TAB-STOP 
     bReject AT ROW 1.14 COL 72.8 WIDGET-ID 202 NO-TAB-STOP 
     bSave AT ROW 1.14 COL 12.8 WIDGET-ID 186 NO-TAB-STOP 
     rectButtons AT ROW 1.05 COL 3.4 WIDGET-ID 160
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER OVERLAY NO-HELP 
         NO-LABELS SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 7 ROW 1
         SIZE 90 BY 2 WIDGET-ID 600.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Maintain Action"
         HEIGHT             = 25.95
         WIDTH              = 102.2
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fButtons:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fButtons
                                                                        */
/* SETTINGS FOR BUTTON bAddNote IN FRAME fButtons
   ALIGN-R                                                              */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwNotes tAction fMain */
/* SETTINGS FOR BUTTON bDueDate IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bDueDate:HIDDEN IN FRAME fMain           = TRUE.

/* SETTINGS FOR BUTTON bFollowupDate IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bFollowupDate:HIDDEN IN FRAME fMain           = TRUE.

/* SETTINGS FOR RECTANGLE rectAction IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rectFinding IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rectNotes IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tAction IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tAction:RETURN-INSERTED IN FRAME fMain  = TRUE
       tAction:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tActionType IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tDueDate IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tFiles IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR EDITOR tFinding IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tFinding:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFollowupDate IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tFollowupDate:HIDDEN IN FRAME fMain           = TRUE.

/* SETTINGS FOR EDITOR tQuestionDesc IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tQuestionDesc:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tQuestionID IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tQuestionID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR EDITOR tReference IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tReference:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwNotes
/* Query rebuild information for BROWSE brwNotes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} for each noteUpd
                          by noteUpd.seq descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwNotes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Maintain Action */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Maintain Action */
DO:
  /* This event will close the window and terminate the procedure.  */
   run CloseMaintainAction in hParentProcedure (input pActionID).
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Maintain Action */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bAddNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddNote wWin
ON CHOOSE OF bAddNote IN FRAME fButtons /* Note */
DO:
  run ActionNote in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAttach
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAttach wWin
ON CHOOSE OF bAttach IN FRAME fButtons /* Attach */
DO:
  run ActionAttach in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel wWin
ON CHOOSE OF bCancel IN FRAME fButtons /* Cancel */
DO:
  run ActionCancel in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose wWin
ON CHOOSE OF bClose IN FRAME fButtons /* Close */
DO:
  run ActionClose in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bComplete wWin
ON CHOOSE OF bComplete IN FRAME fButtons /* Comp */
DO:
  run ActionCompleted in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDueDate wWin
ON CHOOSE OF bDueDate IN FRAME fMain /* v */
DO:
/*   CalendarView(tDueDate:handle in frame fMain). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFollowupDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFollowupDate wWin
ON CHOOSE OF bFollowupDate IN FRAME fMain /* v */
DO:
/*   CalendarView(tFollowupDate:handle in frame fMain). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bInProcess
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bInProcess wWin
ON CHOOSE OF bInProcess IN FRAME fButtons /* InProc */
DO:
  run ActionInProcess in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify wWin
ON CHOOSE OF bModify IN FRAME fButtons /* Modify */
DO:
  run ActionModify in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen wWin
ON CHOOSE OF bOpen IN FRAME fButtons /* Open */
DO:
  run ActionOpen in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReject wWin
ON CHOOSE OF bReject IN FRAME fButtons /* Reject */
DO:
  run ActionRejected in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwNotes
&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes wWin
ON DEFAULT-ACTION OF brwNotes IN FRAME fMain
DO:
  run ActionViewNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave wWin
ON CHOOSE OF bSave IN FRAME fButtons /* Save */
DO:
  run ActionSave in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_Note
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_Note wWin
ON CHOOSE OF MENU-ITEM m_Add_Note /* Add Note... */
DO:
  run ActionNote in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_attach
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_attach wWin
ON CHOOSE OF MENU-ITEM m_attach /* Attachments... */
DO:
  run ActionAttach in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Cancel_Changes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Cancel_Changes wWin
ON CHOOSE OF MENU-ITEM m_Cancel_Changes /* Cancel */
DO:
  run ActionCancel in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Change_to_Completed
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Change_to_Completed wWin
ON CHOOSE OF MENU-ITEM m_Change_to_Completed /* Complete... */
DO:
  run ActionCompleted in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Change_to_In_Process
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Change_to_In_Process wWin
ON CHOOSE OF MENU-ITEM m_Change_to_In_Process /* In Process... */
DO:
  run ActionInProcess in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Change_to_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Change_to_Open wWin
ON CHOOSE OF MENU-ITEM m_Change_to_Open /* Open... */
DO:
  run ActionOpen in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Change_to_Rejected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Change_to_Rejected wWin
ON CHOOSE OF MENU-ITEM m_Change_to_Rejected /* Reject... */
DO:
  run ActionRejected in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Close wWin
ON CHOOSE OF MENU-ITEM m_Close /* Close */
DO:
  run ActionClose in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Modify wWin
ON CHOOSE OF MENU-ITEM m_Modify /* Modify */
DO:
  run ActionModify in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Save_Changes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Save_Changes wWin
ON CHOOSE OF MENU-ITEM m_Save_Changes /* Save */
DO:
  run ActionSave in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME tDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDueDate wWin
ON ENTRY OF tDueDate IN FRAME fMain /* Due Date */
DO:
  bDueDate:visible in frame fMain = true.
  bDueDate:sensitive in frame fMain = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDueDate wWin
ON LEAVE OF tDueDate IN FRAME fMain /* Due Date */
DO:
  bDueDate:visible in frame fMain = false.
  /*  CalendarHide(). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFollowupDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFollowupDate wWin
ON ENTRY OF tFollowupDate IN FRAME fMain /* Follow Up */
DO:
  bFollowupDate:visible in frame fMain = true.
  bFollowupDate:sensitive in frame fMain = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFollowupDate wWin
ON LEAVE OF tFollowupDate IN FRAME fMain /* Follow Up */
DO:
   bFollowupDate:visible in frame fMain = false.
  /*  CalendarHide(). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
/* {util/cal-main.i} */
/* calendarInitialize(). */
bDueDate:load-image("images/s-calendar.bmp").
bFollowupDate:load-image("images/s-calendar.bmp").

reloadAction().

/* DEFINE VARIABLE iStartPage AS INTEGER NO-UNDO. */


/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON ENTRY OF {&WINDOW-NAME} DO:
      ASSIGN CURRENT-WINDOW = SELF NO-ERROR.
    END.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* workaround */
tAction:sensitive in frame fMain = false.

bModify:load-image("images/update.bmp")in frame fButtons.
bModify:load-image-insensitive("images/update-i.bmp") in frame fButtons.
bSave:load-image("images/save.bmp") in frame fButtons.
bSave:load-image-insensitive("images/save-i.bmp")in frame fButtons .
bCancel:load-image("images/cancel.bmp") in frame fButtons.
bCancel:load-image-insensitive("images/cancel-i.bmp") in frame fButtons.
bAddNote:load-image("images/note.bmp") in frame fButtons.
bAddNote:load-image-insensitive("images/note-i.bmp")in frame fButtons .
bAttach:load-image("images/attach.bmp") in frame fButtons.
bAttach:load-image-insensitive("images/attach-i.bmp")in frame fButtons .
bClose:load-image("images/exit.bmp")in frame fButtons.
bClose:load-image-insensitive("images/exit-i.bmp") in frame fButtons.
bHelp:load-image("images/help.bmp") in frame fButtons.
bHelp:load-image-insensitive("images/help.bmp") in frame fButtons.

bOpen:load-image("images/blank.bmp") in frame fButtons.
bOpen:load-image-insensitive("images/blank-i.bmp")in frame fButtons.
bInProcess:load-image("images/blank-modify.bmp")in frame fButtons.
bInProcess:load-image-insensitive("images/blank-modify-i.bmp")in frame fButtons.
bComplete:load-image("images/blank-close.bmp") in frame fButtons.
bComplete:load-image-insensitive("images/blank-close-i.bmp") in frame fButtons .
bReject:load-image("images/blank-error.bmp") in frame fButtons.
bReject:load-image-insensitive("images/blank-error-i.bmp") in frame fButtons.



/* run addModifyButton in h_toolbar (menu-item m_modify:handle in menu m_action).                          */
/* run addSaveButton in h_toolbar (menu-item m_save_changes:handle in menu m_action).                      */
/* run addCancelButton in h_toolbar (menu-item m_cancel_changes:handle in menu m_action).                  */
/* run addAttachButton in h_toolbar (menu-item m_attach:handle in menu m_action).                          */
/* run addNoteButton in h_toolbar (menu-item m_add_note:handle in menu m_action).                          */
/* run addButton in h_toolbar                                                                              */
/*   ("open", "Open", "Open", "images/blank.bmp", "", "images/blank-i.bmp",                                */
/*    false, true, this-procedure, menu-item m_change_to_open:handle in menu m_status).                    */
/* run addButton in h_toolbar                                                                              */
/*   ("inprocess", "In Process", "In Process", "images/blank-modify.bmp", "", "images/blank-modify-i.bmp", */
/*    false, true, this-procedure, menu-item m_change_to_in_process:handle in menu m_status).              */
/* run addButton in h_toolbar                                                                              */
/*   ("completed", "Complete", "Complete", "images/blank-close.bmp", "", "images/blank-close-i.bmp",       */
/*    false, true, this-procedure, menu-item m_change_to_completed:handle in menu m_status).               */
/* run addButton in h_toolbar                                                                              */
/*   ("rejected", "Reject", "Reject", "images/blank-error.bmp", "", "images/blank-error-i.bmp",            */
/*    false, true, this-procedure, menu-item m_change_to_rejected:handle in menu m_status).                */
/* run addHelpButton in h_toolbar (?).                                                                     */
/* run addCloseButton in h_toolbar (menu-item m_close:handle in menu m_action).                            */
/* run EnableActions in h_toolbar ("attach,note,help,close").                                              */

assign
  wWin:min-height-pixels = wWin:height-pixels
  wWin:min-width-pixels = wWin:width-pixels
  wWin:max-height-pixels = session:height-pixels
  wWin:max-width-pixels = session:width-pixels
  .
run OpenMaintainAction in hParentProcedure (pActionID, this-procedure) no-error.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

{lib/winlaunch.i}

displayAction().

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAttach wWin 
PROCEDURE ActionAttach :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 run docattach.w ("QAM",
                  actionUpd.questionID + " " + actionUpd.questionDesc
                    + "  (" + string(actionUpd.actionID) + ")",
                  "new,delete,open,modify,email",
                  "QAR",
                  pAgentID,
                  pAgentName,
                  pState,
                  actionUpd.qarID,
                  "Action",
                  string(actionUpd.actionID),
                  "",
                  "").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionCancel wWin 
PROCEDURE ActionCancel :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.
 bSave:sensitive in frame fButtons = false.
 bCancel:sensitive in frame fButtons = false.
 /*  run disableActions in h_toolbar ("save,cancel"). */
 /*  run enableActions in h_toolbar ("close").        */
 bClose:sensitive in frame fButtons = true.
  assign
    tDueDate:sensitive in frame fMain = false
 /*    tFollowupDate:sensitive in frame fMain = false  */
    tAction:sensitive in frame fMain = false
    .
 
 displayAction().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionClose wWin 
PROCEDURE ActionClose :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.
 apply "WINDOW-CLOSE" to current-window.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionCompleted wWin 
PROCEDURE ActionCompleted :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 def var tDate    as date init today no-undo.
 def var tTakenBy as char            no-undo.
 def var tNotes   as char init "Status changed to Completed." no-undo.

 if not available actionUpd
  then return.

 publish "GetCredentialsName" (output tTakenBy).

 TRY-BLOCK:
 repeat:
  run dialognote.w ("Status changed to Completed",
                    "notes",
                    input-output tDate,
                    input-output tTakenBy,
                    input-output tNotes,
                    output pError).
  if pError 
   then return.

  publish "CompleteAction" (pActionID,
                            tNotes,
                            output pError).
  if pError /* Success */
   then leave TRY-BLOCK.
 end.

 reloadAction().
 displayAction().
 publish "ActionChanged" (pActionID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionHelp wWin 
PROCEDURE ActionHelp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 MESSAGE "Action Identifier: " pActionID
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionInProcess wWin 
PROCEDURE ActionInProcess :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 def var tDate    as date init today no-undo.
 def var tTakenBy as char no-undo.
 def var tNotes   as char init "Status changed to In Process." no-undo.

 if not available actionUpd 
  then return.

 publish "GetCredentialsName" (output tTakenBy).

 TRY-BLOCK:
 repeat:
  run dialognote.w ("Status changed to In Process",
                    "notes",
                    input-output tDate,
                    input-output tTakenBy,
                    input-output tNotes,
                    output pError).
  if pError 
   then return.

  publish "InProcessAction" (pActionID,
                             tNotes,
                             output pError).
  if pError /* Success */
   then leave TRY-BLOCK.
 end.

 reloadAction().
 displayAction().
 publish "ActionChanged" (pActionID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify wWin 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 /*  run enableActions in h_toolbar ("save,cancel").                                     */
 /*  run disableActions in h_toolbar ("open,inprocess,completed,rejected,modify,close"). */
 bSave:sensitive in frame fButtons = true.
 bCancel:sensitive in frame fButtons = true.
 bOpen:sensitive in frame fButtons = false.
 bInProcess:sensitive in frame fButtons = false.
 bComplete:sensitive in frame fButtons = false.
 bReject:sensitive in frame fButtons = false.
 bModify:sensitive in frame fButtons = false.
 bClose:sensitive in frame fButtons = false.
 
 assign
  tDueDate:sensitive in frame fMain = true
 /*   tFollowupDate:sensitive in frame fMain = true  */
   .
 enable tAction with frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNote wWin 
PROCEDURE ActionNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init true.

def var tDate as date init today no-undo.
def var tTakenBy as char no-undo.
 def var tNotes as char no-undo.
 
 def buffer x-notes for noteUpd.
 
 publish "GetCredentialsName" (output tTakenBy).

 TRY-BLOCK:
 repeat:
   run dialognote.w ("New Note",
                     "notes",
                     input-output tDate,
                     input-output tTakenBy,
                     input-output tNotes,
                     output pError).
   if pError 
    then return.
   
   publish "AddActionNote" (pActionID,
                            tDate,
                            tTakenBy,
                            tNotes,
                            output pError).
   if pError /* Success */ 
    then leave TRY-BLOCK.
 end.

 reloadAction().
 displayAction().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRejected wWin 
PROCEDURE ActionRejected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 def var tDate as date init today no-undo.
 def var tTakenBy as char no-undo.
 def var tNotes as char init "Status changed to Rejected." no-undo.

 if not available actionUpd
  then return.

 publish "GetCredentialsName" (output tTakenBy).

 TRY-BLOCK:
 repeat:
   run dialognote.w ("Change Status to Rejected",
                     "notes",
                     input-output tDate,
                     input-output tTakenBy,
                     input-output tNotes,
                     output pError).
   if pError 
    then return.
   publish "RejectAction" (pActionID,
                           tNotes,
                           output pError).
   if pError /* Success */
    then leave TRY-BLOCK.
 end.

 reloadAction().
 displayAction().
 publish "ActionChanged" (pActionID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSave wWin 
PROCEDURE ActionSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init true.

 publish "ModifyAction" (pActionID,
                         tDueDate:input-value in frame fMain,
                         tFollowupDate:input-value in frame fMain,
                         tAction:screen-value in frame fMain,
                         output pError).
 if not pError  
  then return. /* Must select Cancel */

 bSave:sensitive in frame fButtons = false.
 bCancel:sensitive in frame fButtons = false.
 bClose:sensitive in frame fButtons = true.
 /*  run disableActions in h_toolbar ("save,cancel"). */
 /*  run enableActions in h_toolbar ("close").        */
 
 assign
  tDueDate:sensitive in frame fMain = false
  /* tFollowupDate:sensitive in frame fMain = false  */
  tAction:sensitive in frame fMain = false
   .

 reloadAction().
 displayAction().
 publish "ActionChanged" (pActionID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionViewNote wWin 
PROCEDURE ActionViewNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available noteUpd 
   then return.

  def var tDate as date no-undo.
  def var tTakenBy as char no-undo.
  def var tNotes as char no-undo.

  assign
   tDate = noteUpd.noteDate
   tTakenBy = noteUpd.takenBy
   tNotes = noteUpd.notes
   .

  run dialognote.w ("View Note",
                    "",
                    input-output tDate,
                    input-output tTakenBy,
                    input-output tNotes,
                    output std-lo).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bAddNote rectButtons bAttach bCancel bClose bComplete bHelp bInProcess 
         bModify bOpen bReject bSave 
      WITH FRAME fButtons IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fButtons}
  ENABLE tAction brwNotes 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewWindow wWin 
PROCEDURE ViewWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 wWin:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = wWin:width-pixels.
 frame fMain:virtual-width-pixels = wWin:width-pixels.
 frame fMain:height-pixels = wWin:height-pixels.
 frame fMain:virtual-height-pixels = wWin:height-pixels.

 /* fMain components */
 rectFinding:width-pixels in frame fMain = frame fMain:width-pixels - 11.
 tFinding:width-pixels in frame fMain = frame fMain:width-pixels - 101.
 tFiles:width-pixels in frame fMain = frame fMain:width-pixels - 101.
 tReference:width-pixels in frame fMain = frame fMain:width-pixels - 101.

 rectAction:width-pixels in frame fMain = frame fMain:width-pixels - 10.
 tAction:width-pixels in frame fMain = frame fMain:width-pixels - 101.

 rectNotes:width-pixels in frame fMain = frame fMain:width-pixels - 10.
 rectNotes:height-pixels = frame fMain:height-pixels - 380.
 brwNotes:width-pixels = frame fmain:width-pixels - 26.
 brwNotes:height-pixels = frame fMain:height-pixels - 405.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAction wWin 
FUNCTION displayAction RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tEnabledActions as char no-undo.
 def var tDisabledActions as char no-undo.

 if not available actionUpd
  then
   do: clear frame fMain.
       pause 0.
/*        run DisableActions in h_toolbar ("modify,save,cancel,open,inprocess,completed,rejected,note"). */
       return false.
   end.

 if tDueDate:sensitive in frame fMain = true 
  then return false. /* in "Modify" action */

 assign
   tActionType:screen-value in frame fMain = entry(index("CRS", actionUpd.actionType), "Corrective,Recommendation,Suggestion")
   tDueDate:screen-value in frame fMain = string(actionUpd.dueDate)
   tFollowupDate:screen-value in frame fMain = string(actionUpd.followupDate)
   tQuestionID:screen-value in frame fMain = actionUpd.questionID
   tQuestionDesc:screen-value in frame fMain = actionUpd.questionDesc
   tFinding:screen-value in frame fMain = replace(actionUpd.finding, "&#10;", chr(10))
   tFiles:screen-value in frame fMain = replace(actionUpd.files, "|", ", ")
   tReference:screen-value in frame fMain = replace(actionUpd.reference, "&#10;", chr(10))
   /*    tAction:screen-value in frame fMain = replace(actionUpd.action, "&#10;", chr(10)) */
   .

 tStatus:screen-value in frame fMain
         = entry(lookup(actionUpd.stat, "O,I,C,R"), "Open,In Process,Completed,Rejected").
 open query brwNotes for each noteUpd
   by noteUpd.seq descending.
 
 if actionUpd.actionType = "C" 
  then
   case actionUpd.stat:
    when "O" or when "" 
     then assign tEnabledActions = "InProcess,Completed,Rejected,Modify"
                 tDisabledActions = ""
                 .
    when "I" 
     then assign tEnabledActions = "Completed,Rejected,Modify"
                 tDisabledActions = "InProcess"
                 .
    when "C" or when "R" 
     then assign tEnabledActions = "InProcess"
                 tDisabledActions = "Completed,Rejected,Modify"
                 .
   end case.
 else assign
        tEnabledActions = ""
        tDisabledActions = "InProcess,Completed,Rejected,Modify"
        .

/*  run EnableActions in h_toolbar (tEnabledActions).   */
/*  run DisableActions in h_toolbar (tDisabledActions). */

 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION reloadAction wWin 
FUNCTION reloadAction RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var pSuccess as logical no-undo.

 close query brwNotes.

 publish "GetAction"
   (input pActionID,
    output table actionUpd,
    output table noteUpd,
    output pSuccess).

 find first actionUpd no-error.

 return available actionUpd.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

