&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: pipeline.w

  Description: Calculate the revenue pipeline (outstanding) for various
               states and periods using policy (form) data.
               
               The purpose is to estimate the percentage of business that
               is received in various months after the policy was issued
               by our agent.
               
               Start with "calculatePipeline" which is the internal
               procedure called when the user chooses Start.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: D.Sinclair

  Created: 1.20.2012

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{lib/std-def.i}

def temp-table policy
 field agentId as char
 field formCode as char
 field policyNumber as char
 field fileNumber as char
 field postYear as int
 field postMonth as int
 field stat as char
 field batch as char
 field prmType as char
 field statCode as char
 field liability as dec
 field reissue as dec
 field premium as dec
 field retention as dec
 field due as dec
 field state as char
 field county as char
 field hoi as char
 field propType as char
 field closed as char
 field effective as char
 field remitDate as date
 field effectDate as date
 field period as logical extent 16 format "Y/N"
 .

def temp-table period
 field state as char
 field bop as date
 field eop as date
 field premium as dec
 field retention as dec
 field due as dec
 field amount as dec extent 16
 field liability as dec
 field policyCount as int
 index pi-state state bop eop.

def var tContinue as logical.


/* Used to launch another Windows program */
PROCEDURE ShellExecuteA EXTERNAL "shell32" :
    DEFINE INPUT PARAMETER HWND AS LONG.
    DEFINE INPUT PARAMETER lpOperation AS CHARACTER.
    DEFINE INPUT PARAMETER lpFile AS CHARACTER.
    DEFINE INPUT PARAMETER lpParameters AS CHARACTER.
    DEFINE INPUT PARAMETER lpDirectory AS CHARACTER.
    DEFINE INPUT PARAMETER nShowCmd AS LONG.
    DEFINE RETURN PARAMETER hInstance AS LONG.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAdd bAddDir bFormat tBuckets tLag bHow ~
tOutputFile sFiles bFileLookup tOpenResults tProcessFile bFileLookup2 bWhat ~
bGo 
&Scoped-Define DISPLAYED-OBJECTS tLastDate txtOutput tBuckets tLag ~
tOutputFile sFiles tOpenResults tProcessFile tStatus txtProcess txtInput 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_File 
       MENU-ITEM m_About        LABEL "About..."      
       MENU-ITEM m_Configure    LABEL "Configure..."  
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_File         LABEL "File"          .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Add..." 
     SIZE 10 BY 1.14.

DEFINE BUTTON bAddDir 
     LABEL "Add Dir..." 
     SIZE 10 BY 1.14 TOOLTIP "Add all CSV files from a specified directory".

DEFINE BUTTON bDelete 
     LABEL "Remove" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bFileLookup 
     LABEL "..." 
     SIZE 5 BY .95.

DEFINE BUTTON bFileLookup2 
     LABEL "..." 
     SIZE 5 BY .95.

DEFINE BUTTON bFormat 
     LABEL "Format?" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bGo 
     LABEL "Start" 
     SIZE 18 BY 1.91.

DEFINE BUTTON bHow 
     LABEL "How?" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bStop 
     LABEL "Stop" 
     SIZE 18 BY 1.91.

DEFINE BUTTON bWhat 
     LABEL "What?" 
     SIZE 9.8 BY 1.14.

DEFINE VARIABLE tBuckets AS INTEGER FORMAT ">9":U INITIAL 15 
     LABEL "Buckets" 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 TOOLTIP "Enter the number of monthly reporting buckets for each state" NO-UNDO.

DEFINE VARIABLE tLag AS INTEGER FORMAT ">>9":U INITIAL 120 
     LABEL "Lag Days" 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 TOOLTIP "Enter the number of days backward for the last reporting period" NO-UNDO.

DEFINE VARIABLE tLastDate AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tOutputFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Results" 
     VIEW-AS FILL-IN 
     SIZE 66.4 BY 1 NO-UNDO.

DEFINE VARIABLE tProcessFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Processing" 
     VIEW-AS FILL-IN 
     SIZE 66.4 BY 1 TOOLTIP "Enter a filename to generate a ~"consolidated~" input file" NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 82 BY 1 NO-UNDO.

DEFINE VARIABLE txtInput AS CHARACTER FORMAT "X(256)":U INITIAL "Input Files" 
      VIEW-AS TEXT 
     SIZE 12 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE txtOutput AS CHARACTER FORMAT "X(256)":U INITIAL "Output Files" 
      VIEW-AS TEXT 
     SIZE 14 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE txtProcess AS CHARACTER FORMAT "X(256)":U INITIAL "Processing" 
      VIEW-AS TEXT 
     SIZE 14 BY .62
     FONT 6 NO-UNDO.

DEFINE RECTANGLE rectInput
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 9.76.

DEFINE RECTANGLE rectOutput
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 3.57.

DEFINE RECTANGLE rectProcess
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 2.14.

DEFINE VARIABLE sFiles AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SORT SCROLLBAR-VERTICAL 
     SIZE 76 BY 8.81 NO-UNDO.

DEFINE VARIABLE tOpenResults AS LOGICAL INITIAL no 
     LABEL "Open" 
     VIEW-AS TOGGLE-BOX
     SIZE 11 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tLastDate AT ROW 12.43 COL 53 COLON-ALIGNED NO-LABEL WIDGET-ID 54
     txtOutput AT ROW 14.57 COL 3 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     bAdd AT ROW 1.95 COL 88 WIDGET-ID 14
     bDelete AT ROW 3.14 COL 88 WIDGET-ID 30
     bAddDir AT ROW 5.52 COL 88 WIDGET-ID 46
     bFormat AT ROW 9.81 COL 89 WIDGET-ID 38
     tBuckets AT ROW 12.43 COL 19 COLON-ALIGNED WIDGET-ID 6
     tLag AT ROW 12.43 COL 43 COLON-ALIGNED WIDGET-ID 4
     bHow AT ROW 12.43 COL 89 WIDGET-ID 40
     tOutputFile AT ROW 15.76 COL 12.6 COLON-ALIGNED WIDGET-ID 10
     sFiles AT ROW 1.95 COL 11 NO-LABEL WIDGET-ID 16 NO-TAB-STOP 
     bFileLookup AT ROW 15.76 COL 82 WIDGET-ID 12
     tOpenResults AT ROW 15.76 COL 88 WIDGET-ID 44
     tProcessFile AT ROW 16.95 COL 12.6 COLON-ALIGNED WIDGET-ID 34
     bFileLookup2 AT ROW 16.95 COL 82 WIDGET-ID 36
     bWhat AT ROW 16.95 COL 89 WIDGET-ID 42
     bGo AT ROW 19.1 COL 35 WIDGET-ID 2
     bStop AT ROW 19.1 COL 55 WIDGET-ID 32
     tStatus AT ROW 21.48 COL 8 COLON-ALIGNED NO-LABEL WIDGET-ID 8 NO-TAB-STOP 
     txtProcess AT ROW 11.48 COL 3 COLON-ALIGNED NO-LABEL WIDGET-ID 50
     txtInput AT ROW 1.24 COL 3 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     rectInput AT ROW 1.48 COL 2 WIDGET-ID 18
     rectProcess AT ROW 11.71 COL 2 WIDGET-ID 20
     rectOutput AT ROW 14.81 COL 2 WIDGET-ID 26
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 100 BY 21.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Accounting Revenue Pipeline"
         HEIGHT             = 21.62
         WIDTH              = 100
         MAX-HEIGHT         = 21.62
         MAX-WIDTH          = 100
         VIRTUAL-HEIGHT     = 21.62
         VIRTUAL-WIDTH      = 100
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bStop IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rectInput IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rectOutput IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rectProcess IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tLastDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN txtInput IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN txtOutput IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN txtProcess IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Accounting Revenue Pipeline */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Accounting Revenue Pipeline */
DO:
  /* This event will close the window and terminate the procedure.  */
  publish "ExitApplication".
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Accounting Revenue Pipeline */
DO:
  run resizedWindow in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd wWin
ON CHOOSE OF bAdd IN FRAME fMain /* Add... */
DO:
  run addFile.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddDir wWin
ON CHOOSE OF bAddDir IN FRAME fMain /* Add Dir... */
DO:
  run addDir.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete wWin
ON CHOOSE OF bDelete IN FRAME fMain /* Remove */
DO:
  run removeFile.
  self:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileLookup wWin
ON CHOOSE OF bFileLookup IN FRAME fMain /* ... */
DO:
  run setOutputFile (output std-ch, output std-lo).
  if std-lo 
   then tOutputFile:screen-value in frame {&frame-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileLookup2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileLookup2 wWin
ON CHOOSE OF bFileLookup2 IN FRAME fMain /* ... */
DO:
  run setOutputFile (output std-ch, output std-lo).
  if std-lo 
   then tProcessFile:screen-value in frame {&frame-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFormat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFormat wWin
ON CHOOSE OF bFormat IN FRAME fMain /* Format? */
DO:
 MESSAGE "The import file is a CSV (comma-separated value) text file with the following format:" skip(1)
     "AgentID,FormCode,Policy#,File#,RemitYear,RemitMonth,Status,Batch," skip
     "  PrmType,StatCode,Liability,Reissue,Premium,Retention,DueANTIC," skip
     "  State,County,HOI,PropertyType,ClosedDate,EffectiveDate" skip(1)
     "The first line of the file is header labels and is ignored." skip(1)
     "The following fields are used:" skip(1)
     "  RemitYear, RemitMonth, Premium, State, and EffectiveDate"
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo wWin
ON CHOOSE OF bGo IN FRAME fMain /* Start */
DO:
  assign
   self:sensitive = false
   sFiles:sensitive in frame {&frame-name} = false
   bAdd:sensitive in frame {&frame-name} = false
   bDelete:sensitive in frame {&frame-name} = false
   bAddDir:sensitive in frame {&frame-name} = false
   
   tBuckets:sensitive in frame {&frame-name} = false
   tLag:sensitive in frame {&frame-name} = false
   
   tOutputFile:sensitive in frame {&frame-name} = false
   bFileLookup:sensitive in frame {&frame-name} = false
   tProcessFile:sensitive in frame {&frame-name} = false
   bFileLookup2:sensitive in frame {&frame-name} = false

   bStop:sensitive in frame {&frame-name} = true
   tContinue = true
   .

  run calculatePipeline.
  
  assign
   self:sensitive = true
   sFiles:sensitive in frame {&frame-name} = true
   bAdd:sensitive in frame {&frame-name} = true
   bDelete:sensitive in frame {&frame-name} = sFiles:selected in frame {&frame-name}
   bAddDir:sensitive in frame {&frame-name} = true
   
   tBuckets:sensitive in frame {&frame-name} = true
   tLag:sensitive in frame {&frame-name} = true
   
   tOutputFile:sensitive in frame {&frame-name} = true
   bFileLookup:sensitive in frame {&frame-name} = true
   tProcessFile:sensitive in frame {&frame-name} = true
   bFileLookup2:sensitive in frame {&frame-name} = true
   
   bStop:sensitive in frame {&frame-name} = false
   tContinue = false
   .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bHow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bHow wWin
ON CHOOSE OF bHow IN FRAME fMain /* How? */
DO:
  MESSAGE "The Buckets field is the number of 'monthly periods' to calculate the"
          "remittance total value." skip(1)
          "The Lag Days identifies how close the last bucket should be to the current date." skip(1) 
          "For example, with buckets set to 15, and the lag set to 120 days," skip
          "the resulting output will have 15 rows (one for each month) for" skip
          "each state in the import files and the last monthly bucket will be in the" skip
          "month that is 120 days back from today."  skip(1)
          "Each monthly row (based on the number of requested buckets) has 16" skip
          "periods that aggregate the total amount of premium remitted after the"
          " bucket for policies effective prior to and including the bucket." skip(1)
   
          "Looping through each row/line from the imported files, the following"
          "logic is applied:" skip(1)
          "  IF the policy effective date is before the end of a bucket AND" skip
          "     the policy remittance date is after the end of a bucket" skip
          "   THEN the policy premium amount is included in a future period."
   VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStop
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStop wWin
ON CHOOSE OF bStop IN FRAME fMain /* Stop */
DO:
  tContinue = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bWhat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bWhat wWin
ON CHOOSE OF bWhat IN FRAME fMain /* What? */
DO:
  MESSAGE "The Results file contains the number of buckets (rows) for" skip
          "each state that was in the import files." skip(1)
          "The file format is a CSV with the following fields:" skip(1)
          "  State,BeginningOfBucket,EndOfBucket," skip
          "  Period+1,%1,Period+2,%2...,Period+15,%15,Late/Early,%,Total,#Forms" skip(1)
          "Each period is the total of all policies (forms) from the import" skip
          "files that meets the logic explained elsewhere.  The percentages are" skip
          "of the amount of premium in the period of the total of all premiums" skip 
          "captured for the bucket." skip(2)
          "The Processing file is a CSV file formatted to understand the processing" skip
          "of this routine.  This allows for comparison of the import data to the" skip
          "buckets to see if a specific policy is included in the pipeline."
   VIEW-AS ALERT-BOX INFO buttons OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About wWin
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure wWin
ON CHOOSE OF MENU-ITEM m_Configure /* Configure... */
DO:
  run dialogconfig.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit wWin
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" TO CURRENT-WINDOW.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sFiles wWin
ON VALUE-CHANGED OF sFiles IN FRAME fMain
DO:
  bDelete:sensitive in frame {&frame-name} = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLag wWin
ON LEAVE OF tLag IN FRAME fMain /* Lag Days */
DO:
  if self:input-value = ? 
   then self:screen-value = "0".
  std-da = today - int(self:screen-value).
  tLastDate:screen-value in frame {&frame-name} = 
    string(month(std-da)) + "/" + string(year(std-da), "9999").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOpenResults
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOpenResults wWin
ON VALUE-CHANGED OF tOpenResults IN FRAME fMain /* Open */
DO:
  if self:checked 
    and not tOutputFile:screen-value matches "*.csv"
   then
  do: 
      MESSAGE "Please specify a CSV results filename."
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      self:checked = false.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOutputFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOutputFile wWin
ON ANY-KEY OF tOutputFile IN FRAME fMain /* Results */
DO:
  if not self:screen-value matches "*.csv" 
   then tOpenResults:checked in frame {&frame-name} = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

assign
  wWin:min-width-pixels = wWin:width-pixels
  wWin:min-height-pixels = wWin:height-pixels
  wWin:max-width-pixels = session:width-pixels
  wWin:max-height-pixels = session:height-pixels
  .

{lib/win-main.i}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addDir wWin 
PROCEDURE addDir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as char.
 def var tAttr as char.

 std-ch = "".
 system-dialog get-dir std-ch.
 if std-ch = "" or std-ch = ? 
  then return.

 input from os-dir(std-ch).
 repeat:
  import ^ tFile tAttr.

  if index (tAttr, "F") = 0 or not tFile matches "*.csv"
   then next.
  sFiles:add-last(tFile) in frame {&frame-name}.
 end.
 input close.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addFile wWin 
PROCEDURE addFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var xFile as char.
 def var tOk as logical.

 system-dialog get-file xFile
   filters "CSV" "*.csv"
   must-exist
  update tOk.
 if not tOk 
  then return.

 sFiles:add-last(xFile) in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculatePipeline wWin 
PROCEDURE calculatePipeline :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tCnt as int.

  empty temp-table policy.
  empty temp-table period.

  if tOutputFile:screen-value in frame {&frame-name} = ""
   then
  do: 
   MESSAGE "Please set the output file."
    VIEW-AS ALERT-BOX warning BUTTONS OK.
   return.
  end.
  if sFiles:num-items in frame {&frame-name} = 0
   then
  do: 
   MESSAGE "Please set at least one input file."
    VIEW-AS ALERT-BOX warning BUTTONS OK.
   return.
  end.

  do tCnt = 1 to sFiles:num-items in frame {&frame-name}:
   run importData (sFiles:entry(tCnt)).
  end.

  run processData.
  run outputData.

  if tContinue 
   then tStatus:screen-value in frame {&frame-name} = "".
   else tStatus:screen-value in frame {&frame-name} = "Cancelled".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tLastDate txtOutput tBuckets tLag tOutputFile sFiles tOpenResults 
          tProcessFile tStatus txtProcess txtInput 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bAdd bAddDir bFormat tBuckets tLag bHow tOutputFile sFiles bFileLookup 
         tOpenResults tProcessFile bFileLookup2 bWhat bGo 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importData wWin 
PROCEDURE importData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pFile as char.
 
 def var tFileNumber as char.
 def var tCnt as int.
 def var tTotal as decimal.
  
 tStatus:screen-value in frame {&frame-name} = "Importing from " + pFile.
 input from value(pFile).
 import ^.

 repeat:
  if retry 
   then
    DO:
     MESSAGE "Error after file: " tFileNumber
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     leave.
    END.
  process events.
  if not tContinue 
   then leave.
  create policy. 
  import delimiter ","
   policy.agentId 
   policy.formCode 
   policy.policyNumber 
   policy.fileNumber 
   policy.postYear 
   policy.postMonth 
   policy.stat 
   policy.batch 
   policy.prmType 
   policy.statCode 
   policy.liability 
   policy.reissue 
   policy.premium 
   policy.retention 
   policy.due 
   policy.state 
   policy.county 
   policy.hoi
   policy.propType 
   policy.closed 
   policy.effective
   .
  policy.remitDate = date(policy.postMonth, 15, policy.postYear).
  if policy.effective = "" or policy.effective = "00/00/0000" 
   then policy.effective = "12/31/2100".
  policy.effectDate = date(int(entry(1, policy.effective, "/")), 
                           int(entry(2, policy.effective, "/")),
                           int(entry(3, policy.effective, "/"))).

  tTotal = tTotal + policy.due.
  tCnt = tCnt + 1.
  tFileNumber = policy.fileNumber.
  run setupState (policy.state).
 end.
 input close.

 tStatus:screen-value in frame {&frame-name} = 
    "Imported " + string(tCnt, ">>,>>>,>>>") 
     + " records totaling " + string(tTotal).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE outputData wWin 
PROCEDURE outputData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tCnt as int.
 def var tPeriod as int.
 def var tPct as deci extent 16.
 def var tCalc as deci.

 tStatus:screen-value in frame {&frame-name} = "Exporting to " + tOutputFile:screen-value.
 output to value(tOutputFile:screen-value) page-size 0.
 export delimiter "," 
  "State"
  "Period"
  ""
  "Period + 1"
  "%"
  "Period + 2"
  "%"
  "Period + 3"
  "%"
  "Period + 4"
  "%"
  "Period + 5"
  "%"
  "Period + 6"
  "%"
  "Period + 7"
  "%"
  "Period + 8"
  "%"
  "Period + 9"
  "%"
  "Period + 10"
  "%"
  "Period + 11"
  "%"
  "Period + 12"
  "%"
  "Period + 13"
  "%"
  "Period + 14"
  "%"
  "Period + 15"
  "%"
  "Period Early/Late"
  "%"
  "Total Premium"
  "Form Count"
  .

 for each period
   break by period.state
         by period.bop:
  if first-of(period.state) 
   then assign
          tCnt = 0
          tPct = 0
          .

  do tPeriod = 1 to 16:
   if period.premium > 0 
    then tCalc = period.amount[tPeriod] / period.premium.
    else tCalc = 0.
   tPct[tPeriod] = tPct[tPeriod] + tCalc.
  end.
  tCnt = tCnt + 1.
  export delimiter "," 
   period.state
   period.bop
   period.eop
   period.amount[1]
   if period.premium > 0 then (period.amount[1] / period.premium) else 0
   period.amount[2]
   if period.premium > 0 then (period.amount[2] / period.premium) else 0
   period.amount[3]
   if period.premium > 0 then (period.amount[3] / period.premium) else 0
   period.amount[4]
   if period.premium > 0 then (period.amount[4] / period.premium) else 0
   period.amount[5]
   if period.premium > 0 then (period.amount[5] / period.premium) else 0
   period.amount[6]
   if period.premium > 0 then (period.amount[6] / period.premium) else 0
   period.amount[7]
   if period.premium > 0 then (period.amount[7] / period.premium) else 0
   period.amount[8]
   if period.premium > 0 then (period.amount[8] / period.premium) else 0
   period.amount[9]
   if period.premium > 0 then (period.amount[9] / period.premium) else 0
   period.amount[10]
   if period.premium > 0 then (period.amount[10] / period.premium) else 0
   period.amount[11]
   if period.premium > 0 then (period.amount[11] / period.premium) else 0
   period.amount[12]
   if period.premium > 0 then (period.amount[12] / period.premium) else 0
   period.amount[13]
   if period.premium > 0 then (period.amount[13] / period.premium) else 0
   period.amount[14]
   if period.premium > 0 then (period.amount[14] / period.premium) else 0
   period.amount[15]
   if period.premium > 0 then (period.amount[15] / period.premium) else 0
   period.amount[16]
   if period.premium > 0 then (period.amount[16] / period.premium) else 0
   period.premium
   period.policyCount
   .
/*   if last-of(period.state)   */
/*    then export delimiter "," */
/*           period.state       */
/*           "Estimated"        */
/*           ""                 */
/*           ""                 */
/*           tPct[1] / tCnt     */
/*           ""                 */
/*           tPct[2] / tCnt     */
/*           ""                 */
/*           tPct[3] / tCnt     */
/*           ""                 */
/*           tPct[4] / tCnt     */
/*           ""                 */
/*           tPct[5] / tCnt     */
/*           ""                 */
/*           tPct[6] / tCnt     */
/*           ""                 */
/*           tPct[7] / tCnt     */
/*           ""                 */
/*           tPct[8] / tCnt     */
/*           ""                 */
/*           tPct[9] / tCnt     */
/*           ""                 */
/*           tPct[10] / tCnt    */
/*           ""                 */
/*           tPct[11] / tCnt    */
/*           ""                 */
/*           tPct[12] / tCnt    */
/*           ""                 */
/*           tPct[13] / tCnt    */
/*           ""                 */
/*           tPct[14] / tCnt    */
/*           ""                 */
/*           tPct[15] / tCnt    */
/*           ""                 */
/*           tPct[16] / tCnt    */
/*           ""                 */
/*           ""                 */
/*           .                  */
 end.
 output close.

 if tContinue and tOpenResults:checked in frame {&frame-name} 
  then RUN ShellExecuteA in this-procedure (0,
                            "open",
                            tOutputFile:screen-value in frame {&frame-name},
                            "",
                            "",
                            1,
                            OUTPUT std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE processData wWin 
PROCEDURE processData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tPeriod as int.
 def var tCnt as int.
 def var tOutputOpen as logical init false.

 tStatus:screen-value in frame {&frame-name} = "Processing".


 if tProcessFile:screen-value in frame {&frame-name} > "" 
  then 
   do: tStatus:screen-value in frame {&frame-name} = "Processing and Exporting to " + tProcessFile:screen-value.
       output to value(tProcessFile:screen-value) page-size 0.

       export delimiter "," 
        "State"
        "Policy"
        "FileNumber"
        "Premium"
        "Posted Month"
        "Posted Year"
        "Calc Remit Date"
        "Imported Eff Date"
        "Calc Eff Date"
        "Bucket BOP"
        "Bucket EOP"
        "Period Applied"
        .
       tOutputOpen = true.
   end.

 for each policy:
  process events.
  if not tContinue 
   then leave.

  for each period 
    where period.state = policy.state
      and period.eop > policy.effectDate
      and period.eop < policy.remitDate:
   tPeriod = ((year(policy.remitDate) - year(period.eop)) * 12) +
              (month(policy.remitDate) - month(period.eop)).
   assign 
    period.policyCount = period.policyCount + 1
    period.premium = period.premium + policy.premium
    tPeriod = min(16, tPeriod)
    .
   if tPeriod <= 0 
    then tPeriod = 16.
   policy.period[tPeriod] = true.
   period.amount[tPeriod] = period.amount[tPeriod] + policy.premium.
   if tOutputOpen
    then export delimiter ","
           policy.state
           policy.policyNumber
           policy.fileNumber
           policy.premium
           policy.postMonth
           policy.postYear
           policy.remitDate
           policy.effective
           policy.effectDate
           period.bop
           period.eop
           tPeriod
           .
  end.
  tCnt = tCnt + 1.
 end.
 if tOutputOpen
  then output close.

 tStatus:screen-value in frame {&frame-name} = "Finished Processing".
 MESSAGE "Processed " tCnt " policy/form records"
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE removeFile wWin 
PROCEDURE removeFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if sFiles:num-items in frame {&frame-name} = 0
    or sFiles:screen-value = ""
  then return.
 sFiles:delete(sFiles:screen-value).
 if sFiles:num-items = 0 
  then bDelete:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resizedWindow wWin 
PROCEDURE resizedWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tMainFrameWidth as int no-undo.
 def var tSearchFrameWidth as int no-undo.
 def var tMainFrameX as int no-undo.
 def var tResizeFrameX as int no-undo.

 
 if wWin:width-pixels > frame fMain:virtual-width-pixels
  then frame fMain:virtual-width-pixels = wWin:width-pixels.
 if wWin:height-pixels > frame fMain:virtual-height-pixels
  then frame fMain:virtual-height-pixels = wWin:height-pixels.

 frame fMain:width-pixels = wWin:width-pixels.
 frame fMain:height-pixels = wWin:height-pixels.

 /* fMain components */
 tStatus:y = frame fMain:height-pixels - 24.
 tStatus:width-pixels = frame fMain:width-pixels - 90.
 bGo:x = (frame fMain:width-pixels / 2) - 80.
 bGo:y = frame fMain:height-pixels - 74.
 bStop:x = (frame fMain:width-pixels / 2) + 20.
 bStop:y = frame fMain:height-pixels - 74.

 /* Output */
 rectOutput:y = frame fMain:height-pixels - 164.
 rectOutput:width-pixels = frame fMain:width-pixels - 10.
 txtOutput:y = rectOutput:y - 5.
 
 tOutputFile:y = rectOutput:y + 20.
 tOutputFile:side-label-handle:y = tOutputFile:y.
 tOutputFile:width-pixels = rectOutput:width-pixels - 168.
 bFileLookup:y = tOutputFile:y.
 bFileLookup:x = tOutputFile:x + tOutputFile:width-pixels + 15.
 tOpenResults:x = bFileLookup:x + 30.
 tOpenResults:y = bFileLookup:y.

 tProcessFile:y = rectOutput:y + 45.
 tProcessFile:side-label-handle:y = tProcessFile:y.
 tProcessFile:width-pixels = rectOutput:width-pixels - 168.
 bFileLookup2:y = tProcessFile:y.
 bFileLookup2:x = tProcessFile:x + tProcessFile:width-pixels + 15.
 bWhat:x = tOpenResults:x + 5.
 bWhat:y = bFileLookup2:y.

 /* Processing */
 rectProcess:y = frame fMain:height-pixels - 219.
 rectProcess:width-pixels = frame fMain:width-pixels - 10.
 txtProcess:y = rectProcess:y - 5.

 tBuckets:y = rectProcess:y + 15.
 tBuckets:side-label-handle:y = tBuckets:y.
 tLag:y = rectProcess:y + 15.
 tLag:side-label-handle:y = tLag:y.
 bHow:y = tLag:y.
 bHow:x = wWin:width-pixels - 60.

 /* Input */
 rectInput:width-pixels = frame fMain:width-pixels - 10.
 rectInput:height-pixels = frame fMain:height-pixels - 249.
 bAdd:x = frame fMain:width-pixels - 65.
 bDelete:x = bAdd:x.
 bAddDir:x = bAdd:x.
 bFormat:x = bAdd:x.

 sFiles:width-pixels = frame fMain:width-pixels - 120.
 sFiles:height-pixels = frame fMain:height-pixels - 269.

 assign
   frame fMain:virtual-width-pixels = wWin:width-pixels
   frame fMain:virtual-height-pixels = wWin:height-pixels
   .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setOutputFile wWin 
PROCEDURE setOutputFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter xFile as char.
 def output parameter tOk as logical.

 system-dialog get-file xFile
   filters "CSV" "*.csv"
  default-extension "csv"
  create-test-file
  update tOk.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setupState wWin 
PROCEDURE setupState PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pState as char.
 def var i as int.
 def var tDate as date.
 
 if can-find(first period where period.state = pState) 
  then return.

 tLag = int(tLag:screen-value in frame {&frame-name}).
 tDate = date(month(today - tLag), 1, year(today - tLag)).
 
  do i = 1 to int(tBuckets:screen-value in frame {&frame-name}):
   create period.
   period.state = pState.
   period.bop = tDate.
   if month(tDate) = 12
    then period.eop = date(12, 31, year(tDate)).
    else period.eop = date(month(tDate) + 1, 1, year(tDate)) - 1.
   if month(tDate) = 1 
    then tDate = date(12, 1, year(tDate) - 1).
    else tDate = date(month(tDate) - 1, 1, year(tDate)).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

