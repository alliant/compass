&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def output parameter pCode as character no-undo.
def output parameter pName as character no-undo.
def output parameter pValue as character no-undo.
def output parameter pSave as logical no-undo init true.

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/syscode.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAttribute tValue cmbValue Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tAttribute tValue cmbValue 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cmbValue AS CHARACTER FORMAT "X(256)":U 
     LABEL "Value" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tAttribute AS CHARACTER 
     LABEL "Attribute" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tValue AS CHARACTER FORMAT "X(256)":U 
     LABEL "Value" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tAttribute AT ROW 1.48 COL 10.8 COLON-ALIGNED WIDGET-ID 104
     tValue AT ROW 2.67 COL 10.8 COLON-ALIGNED WIDGET-ID 108
     cmbValue AT ROW 2.67 COL 11 COLON-ALIGNED WIDGET-ID 110
     Btn_OK AT ROW 4.1 COL 20.2
     Btn_Cancel AT ROW 4.1 COL 38.2
     SPACE(21.99) SKIP(0.61)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Claim Attribute"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE.

ASSIGN 
       cmbValue:HIDDEN IN FRAME fMain           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON GO OF FRAME fMain /* Select Claim Attribute */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Select Claim Attribute */
DO:
  pSave = false.
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fMain
ON CHOOSE OF Btn_Cancel IN FRAME fMain /* Cancel */
DO:
  pSave = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fMain
ON CHOOSE OF Btn_OK IN FRAME fMain /* Save */
DO:
  do with frame {&frame-name}:
    assign
      pCode = tAttribute:screen-value
      pName = entry(lookup(pCode,tAttribute:list-item-pairs) - 1,tAttribute:list-item-pairs)
      pSave = true
      .
    if not tValue:hidden
     then
      if tValue:read-only
       then pValue = "TRUE".
       else pValue = tValue:screen-value.
     else pValue = cmbValue:screen-value.
       
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAttribute
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAttribute fMain
ON VALUE-CHANGED OF tAttribute IN FRAME fMain /* Attribute */
DO:
  do with frame {&frame-name}:
    for first syscode no-lock
        where syscode.code = tAttribute:screen-value:
    
      case syscode.type:
       when "logical" then
        assign
          tValue:hidden = false
          cmbValue:hidden = true
          tValue:read-only = true
          .
       when "list" then
        assign
          tValue:hidden = true
          cmbValue:hidden = false
          .
       otherwise
        assign
          tValue:hidden = false
          cmbValue:hidden = true
          tValue:read-only = false
          .
      end case.
    end.
    if not cmbValue:hidden
     then
      do:
        std-ch = "".
        publish "GetClmAttrList" (tAttribute:screen-value, output std-ch).
        assign
          cmbValue:list-items = std-ch
          cmbValue:screen-value = entry(1,cmbValue:list-items)
          .
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

MAIN-BLOCK:
repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  
  std-ch = "".
  publish "GetCodes" ("ClaimAttribute", output table syscode).
  for each syscode no-lock by syscode.description:
    std-ch = addDelimiter(std-ch,",") + syscode.description + "," + syscode.code.
  end.
  assign
    tAttribute:list-item-pairs = std-ch
    tAttribute:screen-value = entry(2,tAttribute:list-item-pairs)
    .
  apply "value-changed" to tAttribute.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAttribute tValue cmbValue 
      WITH FRAME fMain.
  ENABLE tAttribute tValue cmbValue Btn_OK Btn_Cancel 
      WITH FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

