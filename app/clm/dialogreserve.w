&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fReserve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fReserve 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{tt/claimadjreq.i}
{tt/sysuser.i}

{lib/add-delimiter.i}
{lib/std-def.i}
{lib/change-made.i}
define input parameter table for claimadjreq.
define input parameter hFileDataSrv as handle no-undo.
define input parameter pTitle as character no-undo.
define output parameter pSuccess as logical.

define variable lIsChanged as logical no-undo initial false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fReserve

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tNotes tAmount cmbRefCategory cmbUid bSave ~
bCancel 
&Scoped-Define DISPLAYED-OBJECTS tCurrentReserve tNotes tAmount ~
cmbRefCategory cmbUid tAvailableLimit 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cmbRefCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 17 BY 1 TOOLTIP "The type of the adjustment" NO-UNDO.

DEFINE VARIABLE cmbUid AS CHARACTER FORMAT "X(256)":U 
     LABEL "Approver" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 31 BY 1 TOOLTIP "The user that willl approve the adjustment request" NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 31 BY 3.57
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT ">>,>>>,>>9.99":U INITIAL 0 
     LABEL "Raise Adj. To" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 TOOLTIP "The requested amount of the adjustment"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAvailableLimit AS CHARACTER FORMAT "X(50)":U INITIAL 0 
     LABEL "Available Limit" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tCurrentReserve AS DECIMAL FORMAT "$ >>,>>>,>>9.99":U INITIAL 0 
     LABEL "Current Reserve" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1
     FONT 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fReserve
     tCurrentReserve AT ROW 2.67 COL 18 COLON-ALIGNED WIDGET-ID 36
     tNotes AT ROW 7.43 COL 20 NO-LABEL WIDGET-ID 30
     tAmount AT ROW 3.86 COL 18 COLON-ALIGNED WIDGET-ID 4
     cmbRefCategory AT ROW 5.05 COL 18 COLON-ALIGNED WIDGET-ID 2
     cmbUid AT ROW 6.24 COL 18 COLON-ALIGNED WIDGET-ID 34
     bSave AT ROW 11.48 COL 12 WIDGET-ID 38
     bCancel AT ROW 11.48 COL 28 WIDGET-ID 40
     tAvailableLimit AT ROW 1.48 COL 18 COLON-ALIGNED WIDGET-ID 42
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 7.43 COL 13 WIDGET-ID 32
     SPACE(34.19) SKIP(4.94)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fReserve
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fReserve:SCROLLABLE       = FALSE
       FRAME fReserve:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tAvailableLimit IN FRAME fReserve
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCurrentReserve IN FRAME fReserve
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fReserve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fReserve fReserve
ON WINDOW-CLOSE OF FRAME fReserve
DO:
  std-lo = true.
  if lIsChanged
   then message
          "Data has been modified." skip
          "Are you sure you want to exit without saving?"
          view-as alert-box warning buttons yes-no
          update std-lo.
  if std-lo
   then APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fReserve
ON CHOOSE OF bCancel IN FRAME fReserve /* Cancel */
DO:
  APPLY "WINDOW-CLOSE":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fReserve
ON CHOOSE OF bSave IN FRAME fReserve /* Save */
DO:
  RUN SaveReserve IN THIS-PROCEDURE (output std-lo).
  if std-lo
   then APPLY "END-ERROR":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbRefCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbRefCategory fReserve
ON VALUE-CHANGED OF cmbRefCategory IN FRAME fReserve /* Type */
DO:
  define buffer claimadjreq for claimadjreq.
  do with frame {&frame-name}:
    for first claimadjreq no-lock:
      if changeMade(self:screen-value,"refCategory","claimadjreq")
       then
        do:
          claimadjreq.refcategory = self:screen-value.
          run GetReserve in this-procedure (claimadjreq.claimid,self:input-value,output std-de).
          run GetUserList in this-procedure (std-de, output std-ch).
          run SetUserList in this-procedure (std-ch).
          tCurrentReserve:screen-value = string(std-de).
          tAmount:screen-value = string(std-de).
          run GetReserveLimit in this-procedure (claimadjreq.claimid,self:input-value,output std-ch).
          tAvailableLimit:screen-value = std-ch.
          lIsChanged = true.
        end.
    end.
  end.
  APPLY "LEAVE" TO tAmount.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbUid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbUid fReserve
ON VALUE-CHANGED OF cmbUid IN FRAME fReserve /* Approver */
DO:
  lIsChanged = changeMade(self:screen-value,"uid","claimadjreq") or lIsChanged.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAmount fReserve
ON LEAVE OF tAmount IN FRAME fReserve /* Raise Adj. To */
DO:
  lIsChanged = true.
  do with frame {&frame-name}:
    if self:input-value < tCurrentReserve:input-value
     then std-de = 0.
     else std-de = self:input-value - tCurrentReserve:input-value.
    run GetUserList in this-procedure (std-de, output std-ch).
    run SetUserList in this-procedure (std-ch).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes fReserve
ON LEAVE OF tNotes IN FRAME fReserve
DO:
  lIsChanged = changeMade(self:screen-value,"notes","claimadjreq") or lIsChanged.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fReserve 


/* ***************************  Main Block  *************************** */

/* set the title */
frame {&frame-name}:title = pTitle.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  run Initialize in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fReserve  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fReserve.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fReserve  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCurrentReserve tNotes tAmount cmbRefCategory cmbUid tAvailableLimit 
      WITH FRAME fReserve.
  ENABLE tNotes tAmount cmbRefCategory cmbUid bSave bCancel 
      WITH FRAME fReserve.
  VIEW FRAME fReserve.
  {&OPEN-BROWSERS-IN-QUERY-fReserve}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReserve fReserve 
PROCEDURE GetReserve :
/*------------------------------------------------------------------------------
@description Gets the reserve amount for the type
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define input parameter pRefCategory as character no-undo.
  define output parameter pReserveAmount as decimal no-undo.
  
  run GetClaimReserveAmount in hFileDataSrv (pClaimID, pRefCategory, output pReserveAmount).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetReserveLimit fReserve 
PROCEDURE GetReserveLimit :
/*------------------------------------------------------------------------------
@description Gets the reserve limit for the user and type
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define input parameter pRefCategory as character no-undo.
  define output parameter pReserveAmount as character no-undo.
  
  std-ch = "".
  publish "GetCredentialsID" (output std-ch).
  std-de = 0.
  run GetClaimReserveLimit in hFileDataSrv (pClaimID, pRefCategory, std-ch, output std-de).
  if std-de = lf-max
   then pReserveAmount = "Unlimited".
   else pReserveAmount = trim(string(std-de, "$ >>,>>>,>>9.99")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetUserList fReserve 
PROCEDURE GetUserList :
/*------------------------------------------------------------------------------
@description gets the reserve balance for the type choosen 
------------------------------------------------------------------------------*/
  define input parameter pAmount as decimal no-undo.
  define output parameter pUserList as character no-undo.
  
  define variable cUser as character no-undo.
  define variable cName as character no-undo.
  
  FOR FIRST claimadjreq:
    std-de = 0.
    run GetReserve in this-procedure (claimadjreq.claimID, claimadjreq.refCategory, output std-de).
    run GetClaimReserveApprover in hFileDataSrv
                                  (claimadjreq.claimID,
                                   claimadjreq.refCategory, 
                                   pAmount, 
                                   OUTPUT pUserList
                                   ).
  END.
  publish "GetCredentialsID" (output cUser).
  if lookup(cUser,pUserList) > 0
   then
    do:
      publish "GetCredentialsName" (output cName).
      pUserList = cName + "," + cUser.
    end.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize fReserve 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
@description Initialize the dialog by setting the reserve data     
------------------------------------------------------------------------------*/
  define variable cType as character no-undo.
  define variable cCatList as character no-undo.
  define buffer claimadjreq for claimadjreq.
  
  /* set the fields */
  for first claimadjreq exclusive-lock:
    run GetClaimType in hFileDataSrv (claimadjreq.claimID, output cType).
    publish "GetCategoryList" (output cCatList).
    do with frame {&frame-name}:
      /* set the refCategory if not set */
      claimadjreq.refCategory = (if claimadjreq.refCategory = "" then entry(2,cCatList) else claimadjreq.refCategory).
      run GetUserList in this-procedure (claimadjreq.requestedAmount,output std-ch).
      run SetUserList in this-procedure (std-ch).
      assign
        /* set the approver list */
        cmbRefCategory:list-item-pairs = cCatList
        cmbRefCategory:inner-lines = num-entries(cmbRefCategory:list-item-pairs) / 2
        cmbRefCategory:screen-value = claimadjreq.refCategory
        cmbRefCategory:screen-value = (if cType = "M" then "E" else cmbRefCategory:screen-value)
        /* set the notes */
        tNotes:screen-value = claimadjreq.notes
        .
      /* set the current reserve balance */
      run GetReserve in this-procedure (claimadjreq.claimID,cmbRefCategory:screen-value,output std-de).
      assign
        tCurrentReserve:screen-value = string(std-de).
        /* set the amount of the request (if there) */
        tAmount:screen-value = (if claimadjreq.isNew then string(std-de) else string(std-de + claimadjreq.requestedAmount))
        .
      /* set the available limit reserve */
      std-ch = "".
      run GetReserveLimit in this-procedure (claimadjreq.claimID,cmbRefCategory:screen-value,output std-ch).
      assign
        tAvailableLimit:screen-value = std-ch.
        .
      /* disable the fields if needed */
      assign
        tAmount:read-only = claimadjreq.isView
        tNotes:read-only = claimadjreq.isView
        cmbRefCategory:sensitive = (not claimadjreq.isView and not cType = "M")
        bSave:sensitive = not claimadjreq.isView
        .
    end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveReserve fReserve 
PROCEDURE SaveReserve :
/*------------------------------------------------------------------------------
@description Saves the reserve adjustment
------------------------------------------------------------------------------*/
  define output parameter pSaveSuccess as logical no-undo.
  
  define buffer claimadjreq for claimadjreq.
  if lIsChanged
   then
    do with frame {&frame-name}:
      if tNotes:input-value = ""
       then message "Please add a note." view-as alert-box information buttons ok.
       else
        for first claimadjreq exclusive-lock:
          run GetReserve in this-procedure (claimadjreq.claimID, cmbRefCategory:screen-value, output std-de).
          assign
            claimadjreq.requestedAmount = tAmount:input-value - std-de
            claimadjreq.notes = tNotes:input-value
            claimadjreq.refCategory = cmbRefCategory:screen-value
            claimadjreq.uid = cmbUid:screen-value
            claimadjreq.dateRequested = now
            .
          if claimadjreq.requestedamount < 0
           then
            do:
              message "The requested amount is less than 0. Do you wish to continue?" view-as alert-box question buttons yes-no update std-lo.
              if not std-lo
               then return.
            end.
            
          /* either modify or create */
          if claimadjreq.isNew or (not claimadjreq.isNew and not claimadjreq.isView)
           then run NewClaimReserve in hFileDataSrv (input table claimadjreq, output pSaveSuccess).
           else run ModifyClaimReserve in hFileDataSrv (input table claimadjreq, output pSaveSuccess).
          /* assign the flags */
          assign
            lIsChanged = not pSaveSuccess
            pSuccess = pSaveSuccess
            .
        end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetUserList fReserve 
PROCEDURE SetUserList :
/*------------------------------------------------------------------------------
@description gets the reserve balance for the type choosen 
------------------------------------------------------------------------------*/
  define input parameter pUserList as character no-undo.
  
  define buffer claimadjreq for claimadjreq.
  
  do with frame {&frame-name}:
    for first claimadjreq exclusive-lock:
      if not pUserList = ""
       then
        assign
          cmbUid:list-item-pairs = pUserList
          cmbUid:inner-lines = min(num-entries(cmbUid:list-item-pairs) / 2 + 1, 7)
          cmbUid:screen-value = (if lookup(claimadjreq.uid,pUserList) = 0 then entry(2,std-ch) else claimadjreq.uid)
          cmbUid:sensitive = not (claimadjreq.isView or (num-entries(cmbUid:list-item-pairs) = 2 and not cmbUid:screen-value = ""))
          .
       else
        assign
          cmbUid:list-item-pairs = " , "
          cmbUid:sensitive = false
          .
    end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

