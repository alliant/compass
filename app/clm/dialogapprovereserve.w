&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fReserve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fReserve 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{tt/claimadjreq.i}
{tt/sysuser.i}
{tt/claim.i}
{tt/claimcode.i}

{lib/get-reserve-type.i}
{lib/add-delimiter.i}
{lib/std-def.i}
define input parameter pClaimID as integer no-undo.
define input parameter pRefCategory as character no-undo.
define input parameter pRequestedAmount as decimal no-undo.
define input parameter pNotes as character no-undo.
define input parameter hFileDataSrv as handle no-undo.
define output parameter pSuccess as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fReserve

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tEmailPreviewText bReject tAmount tNotes ~
bApprove bCancel tDate tEmailPreview 
&Scoped-Define DISPLAYED-OBJECTS tEmailPreviewText tAmount tCurrentReserve ~
tNotes tRequestedAmount tDate tEmailPreview 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getReserve fReserve 
FUNCTION getReserve RETURNS CHAR
  ( INPUT pClaimID as INT,
    INPUT pRefCategory AS CHAR )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bApprove 
     LABEL "Approve" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bReject 
     LABEL "Reject" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tEmailPreview AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 49 BY 9.05
     FONT 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 49 BY 5
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "The amount to approve from the requested amount."
     FONT 1 NO-UNDO.

DEFINE VARIABLE tCurrentReserve AS DECIMAL FORMAT "$ ->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Current Reserve" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "The date of the adjustment request"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tEmailPreviewText AS CHARACTER FORMAT "X(256)":U INITIAL "Executive Email:" 
      VIEW-AS TEXT 
     SIZE 16 BY .62 NO-UNDO.

DEFINE VARIABLE tRequestedAmount AS DECIMAL FORMAT "$ ->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Requested Amount" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fReserve
     tEmailPreviewText AT ROW 12.67 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 94
     bReject AT ROW 22.67 COL 20 WIDGET-ID 44
     tAmount AT ROW 3.86 COL 19 COLON-ALIGNED WIDGET-ID 42
     tCurrentReserve AT ROW 1.48 COL 19 COLON-ALIGNED WIDGET-ID 36
     tNotes AT ROW 7.19 COL 3 NO-LABEL WIDGET-ID 30
     tRequestedAmount AT ROW 2.67 COL 19 COLON-ALIGNED WIDGET-ID 4
     bApprove AT ROW 22.67 COL 4 WIDGET-ID 38
     bCancel AT ROW 22.67 COL 36 WIDGET-ID 40
     tDate AT ROW 5.05 COL 19 COLON-ALIGNED WIDGET-ID 46
     tEmailPreview AT ROW 13.38 COL 3 NO-LABEL WIDGET-ID 92
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 6.48 COL 3 WIDGET-ID 32
     SPACE(44.39) SKIP(17.18)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Approve Claim Adjustment" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fReserve
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fReserve:SCROLLABLE       = FALSE
       FRAME fReserve:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tCurrentReserve IN FRAME fReserve
   NO-ENABLE                                                            */
ASSIGN 
       tEmailPreview:RETURN-INSERTED IN FRAME fReserve  = TRUE
       tEmailPreview:READ-ONLY IN FRAME fReserve        = TRUE.

/* SETTINGS FOR FILL-IN tRequestedAmount IN FRAME fReserve
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fReserve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fReserve fReserve
ON WINDOW-CLOSE OF FRAME fReserve /* Approve Claim Adjustment */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApprove fReserve
ON CHOOSE OF bApprove IN FRAME fReserve /* Approve */
DO:
  do with frame {&frame-name}:
    /* if the requested amount and approved amounts differ, then ask the user to continue */
    std-lo = true.
    if tAmount:input-value <> tRequestedAmount:input-value
     then MESSAGE "The requested amount and the approved amount are different. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO TITLE "Confimation"  UPDATE std-lo.
    if not std-lo
     then return.
    
    run ApproveClaimReserve in hFileDataSrv
                                  (input pClaimID,
                                   input pRefCategory,
                                   input tAmount:input-value,
                                   input tDate:input-value,
                                   input tNotes:input-value,
                                   output pSuccess
                                   ).
    if pSuccess
     then APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fReserve
ON CHOOSE OF bCancel IN FRAME fReserve /* Cancel */
DO:
  pSuccess = false.
  APPLY "WINDOW-CLOSE":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReject fReserve
ON CHOOSE OF bReject IN FRAME fReserve /* Reject */
DO:
  do with frame {&frame-name}:
    /* prompt the user to continue */
    MESSAGE "Rejected requests are removed. Continue?" VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
    if not std-lo 
     then return.
      
    run RejectClaimReserve in hFileDataSrv
                                 (input pClaimID,
                                  input pRefCategory,
                                  output pSuccess
                                  ).
    if pSuccess
     then APPLY "WINDOW-CLOSE":U TO FRAME {&FRAME-NAME}.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fReserve 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  do with frame {&frame-name}:
    run GetClaim in hFileDataSrv (output table claim).
    run GetClaimCodes in hFileDataSrv (output table claimcode).
    assign
      tCurrentReserve:screen-value = getReserve(pClaimID, pRefCategory)
      tRequestedAmount:screen-value = string(pRequestedAmount)
      tAmount:screen-value = string(pRequestedAmount)
      tDate:screen-value = string(today)
      tNotes:screen-value = pNotes
      .
  end.
  run getExecutiveEmail in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fReserve  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fReserve.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fReserve  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tEmailPreviewText tAmount tCurrentReserve tNotes tRequestedAmount 
          tDate tEmailPreview 
      WITH FRAME fReserve.
  ENABLE tEmailPreviewText bReject tAmount tNotes bApprove bCancel tDate 
         tEmailPreview 
      WITH FRAME fReserve.
  VIEW FRAME fReserve.
  {&OPEN-BROWSERS-IN-QUERY-fReserve}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getExecutiveEmail fReserve 
PROCEDURE getExecutiveEmail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* variables */
  define variable tAgentName     as character no-undo.
  define variable tStateName     as character no-undo.
  define variable tCompanyCause  as character no-undo.
  define variable tCompanyDesc   as character no-undo.
  define variable tAgentError    as character no-undo.
  define variable tAttorneyError as character no-undo.
  define variable tSearcherError as character no-undo.
  define variable tCurrLAE       as decimal   no-undo.
  define variable tCurrLoss      as decimal   no-undo.
  define variable tIncreaseLAE   as decimal   no-undo initial 0.
  define variable tIncreaseLoss  as decimal   no-undo initial 0.
  define variable tAdjustedLAE   as decimal   no-undo.
  define variable tAdjustedLoss  as decimal   no-undo.
  define variable tSummary       as character no-undo.
  
  /* ensure that the threshold is met */
  do with frame {&frame-name}:
    publish "GetSysPropDesc" ("CLM", "Reserve", "Threshold", "Amount", output std-ch).
    std-de = decimal(std-ch).
    if tAmount:input-value < std-de
     then 
      do:
        assign
          tEmailPreviewText:hidden          = true
          tEmailPreview:hidden              = true
          bCancel:y                         = tNotes:y + tNotes:height-pixels + 10
          bApprove:y                        = bCancel:y
          bReject:y                         = bCancel:y
          frame {&frame-name}:height-pixels = bCancel:y + bCancel:height-pixels + 40
          .
        return.
      end.
    
    /* get the descriptions */
    for first claim no-lock:
      /* get agent name */
      publish "GetAgentName" (claim.agentID, output tAgentName, output std-lo).
      /* state name */
      tStateName = claim.stateID.
      /* errors */
      publish "GetSysPropDesc" ("CLM", "ClaimDescription", "AgentError", claim.agentError, output tAgentError).
      publish "GetSysPropDesc" ("CLM", "ClaimDescription", "AttorneyError", claim.attorneyError, output tAttorneyError).
      publish "GetSysPropDesc" ("CLM", "ClaimDescription", "AgentError", claim.searcherError, output tSearcherError).
      /* codes */
      for each claimcode no-lock
         where claimcode.claimID = claim.claimID:
       
        if claimcode.codeType = "ClaimCause"
         then publish "GetSysCodeDesc" ("ClaimCause", claimcode.code, output tCompanyCause).
       
        if claimcode.codeType = "ClaimDescription"
         then publish "GetSysCodeDesc" ("ClaimDescription", claimcode.code, output tCompanyDesc).
      end.
      /* reserves */
      run GetClaimReserveApprovedAmount in hFileDataSrv (claim.claimID, "E", output tCurrLAE).
      run GetClaimReserveApprovedAmount in hFileDataSrv (claim.claimID, "L", output tCurrLoss).
      if pRefCategory = "E"
       then tIncreaseLAE = tAmount:input-value.
       else tIncreaseLoss = tAmount:input-value.
      tAdjustedLAE = tCurrLAE + tIncreaseLAE.
      tAdjustedLoss = tCurrLoss + tIncreaseLoss.
      /* summary */
      tSummary = claim.summary.
    end.
    
    /* place in widget */
    assign
      tEmailPreview:screen-value = ""
      tEmailPreview:screen-value = tEmailPreview:screen-value + "Agent: " + tAgentName + chr(10) + 
                                   tEmailPreview:screen-value + "State: " + tStateName + chr(10) + 
                                   tEmailPreview:screen-value + "Company Desc: " + tCompanyDesc + chr(10) + 
                                   tEmailPreview:screen-value + "Company Cause: " + tCompanyCause + chr(10) + 
                                   tEmailPreview:screen-value + "Agent Error: " + tAgentError + chr(10) + 
                                   tEmailPreview:screen-value + "Attorney Error: " + tAttorneyError + chr(10) + 
                                   tEmailPreview:screen-value + "Searcher Error: " + tSearcherError + chr(10) + 
                                   tEmailPreview:screen-value + "Current LAE Reserve: " + trim(string(tCurrLAE, "$->>>,>>9.99")) + chr(10) + 
                                   tEmailPreview:screen-value + "Current Loss Reserve: " + trim(string(tCurrLoss, "$->>>,>>9.99")) + chr(10) + 
                                   tEmailPreview:screen-value + "Increase in LAE Reserve: " + trim(string(tIncreaseLAE, "$->>>,>>9.99")) + chr(10) + 
                                   tEmailPreview:screen-value + "Increase in Loss Reserve: " + trim(string(tIncreaseLoss, "$->>>,>>9.99")) + chr(10) + 
                                   tEmailPreview:screen-value + "Adjusted LAE Reserve: " + trim(string(tAdjustedLAE, "$->>>,>>9.99")) + chr(10) + 
                                   tEmailPreview:screen-value + "Adjusted Loss Reserve: " + trim(string(tAdjustedLoss, "$->>>,>>9.99")) + chr(10) + 
                                   tEmailPreview:screen-value + "Summary: " + tSummary
      
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getReserve fReserve 
FUNCTION getReserve RETURNS CHAR
  ( INPUT pClaimID as INT,
    INPUT pRefCategory AS CHAR ) :
/*------------------------------------------------------------------------------
@description gets the reserve balance for the type choosen 
------------------------------------------------------------------------------*/
  def var reserveAmount as decimal no-undo.
  
  run GetClaimReserveAmount in hFileDataSrv (pClaimID, pRefCategory, output reserveAmount).
  
  RETURN string(reserveAmount).   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

