&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS tNotes 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                            */
{tt/claimnote.i}
define input parameter table for claimnote.
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                        */
define variable noteClaimID as integer no-undo.
define variable noteSeq as integer no-undo.
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME tNotes
&Scoped-define BROWSE-NAME brwNotes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES claimnote

/* Definitions for BROWSE brwNotes                                      */
&Scoped-define FIELDS-IN-QUERY-brwNotes claimnote.edited claimnote.categoryDesc claimnote.subject claimnote.noteDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwNotes   
&Scoped-define SELF-NAME brwNotes
&Scoped-define QUERY-STRING-brwNotes FOR EACH claimnote where claimnote.noteType = "U" no-lock
&Scoped-define OPEN-QUERY-brwNotes OPEN QUERY {&SELF-NAME} FOR EACH claimnote where claimnote.noteType = "U" no-lock.
&Scoped-define TABLES-IN-QUERY-brwNotes claimnote
&Scoped-define FIRST-TABLE-IN-QUERY-brwNotes claimnote


/* Definitions for DIALOG-BOX tNotes                                    */
&Scoped-define OPEN-BROWSERS-IN-QUERY-tNotes ~
    ~{&OPEN-QUERY-brwNotes}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-37 brwNotes tSubject tNote bCancel 
&Scoped-Define DISPLAYED-OBJECTS tCategory tSubject tNote 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE tNote AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 170 BY 14.95
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 115 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 170.2 BY 2.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwNotes FOR 
      claimnote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwNotes tNotes _FREEFORM
  QUERY brwNotes NO-LOCK DISPLAY
      claimnote.edited column-label "Edited" view-as toggle-box
      claimnote.categoryDesc column-label "Category" format "x(50)" width 20
      claimnote.subject column-label "Subject" format "x(200)" width 113
      claimnote.noteDate column-label "Date" format "99/99/9999 HH:MM:SS" width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 170 BY 6.19 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME tNotes
     bSpellCheck AT ROW 8.48 COL 164.6 WIDGET-ID 308 NO-TAB-STOP 
     brwNotes AT ROW 1.48 COL 3 WIDGET-ID 400
     tCategory AT ROW 8.86 COL 13 COLON-ALIGNED WIDGET-ID 240 NO-TAB-STOP 
     tSubject AT ROW 8.86 COL 46 COLON-ALIGNED WIDGET-ID 302
     tNote AT ROW 10.43 COL 3 NO-LABEL WIDGET-ID 6
     bSave AT ROW 26 COL 74
     bCancel AT ROW 26 COL 90
     RECT-37 AT ROW 8.14 COL 3 WIDGET-ID 4
     SPACE(1.59) SKIP(17.14)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify Notes"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX tNotes
   FRAME-NAME                                                           */
/* BROWSE-TAB brwNotes RECT-37 tNotes */
ASSIGN 
       FRAME tNotes:SCROLLABLE       = FALSE
       FRAME tNotes:HIDDEN           = TRUE.

ASSIGN 
       brwNotes:COLUMN-RESIZABLE IN FRAME tNotes       = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME tNotes
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSpellCheck IN FRAME tNotes
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tCategory IN FRAME tNotes
   NO-ENABLE                                                            */
ASSIGN 
       tNote:RETURN-INSERTED IN FRAME tNotes  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwNotes
/* Query rebuild information for BROWSE brwNotes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimnote where claimnote.noteType = "U" no-lock
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwNotes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes tNotes
ON WINDOW-CLOSE OF FRAME tNotes /* Modify Notes */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwNotes
&Scoped-define SELF-NAME brwNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes tNotes
ON ROW-DISPLAY OF brwNotes IN FRAME tNotes
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes tNotes
ON START-SEARCH OF brwNotes IN FRAME tNotes
DO:
  {lib/brw-startSearch.i}
  apply 'value-changed' to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwNotes tNotes
ON VALUE-CHANGED OF brwNotes IN FRAME tNotes
DO:
  if not available claimnote
   then return.
   
  assign
    tNote:screen-value = claimnote.notes
    tCategory:screen-value = claimnote.category
    tSubject:screen-value = claimnote.subject
    noteClaimID = claimnote.claimID
    noteSeq = claimnote.seq
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave tNotes
ON CHOOSE OF bSave IN FRAME tNotes /* Save */
DO:
  run ModifyClaimNote in hFileDataSrv (table claimnote).
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck tNotes
ON CHOOSE OF bSpellCheck IN FRAME tNotes /* Spell Check */
DO:
  ASSIGN tNote.
  run util/spellcheck.p ({&window-name}:handle, input-output tNote, output std-lo).
  display tNote with frame {&frame-name}.
  apply "value-changed" to tNote in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCategory tNotes
ON VALUE-CHANGED OF tCategory IN FRAME tNotes /* Category */
DO:
  do with frame {&frame-name}:
    run EditNote in this-procedure.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNote tNotes
ON VALUE-CHANGED OF tNote IN FRAME tNotes
DO:
  run EditNote in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSubject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSubject tNotes
ON VALUE-CHANGED OF tSubject IN FRAME tNotes /* Subject */
DO:
  run EditNote in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK tNotes 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bSpellCheck:load-image-up("images/spellcheck.bmp").
bSpellCheck:load-image-insensitive("images/spellcheck-i.bmp").


{lib/brw-main.i}
publish "GetNoteCategoryUserList" (output std-ch).

tCategory:list-item-pairs = std-ch.
tCategory:screen-value = entry(2,std-ch).

tNote:read-only = not (can-find(first claimnote where claimnote.noteType = "U")).
tSubject:read-only = tNote:read-only.
tCategory:sensitive = (can-find(first claimnote where claimnote.noteType = "U")).
bSpellCheck:sensitive  = tCategory:sensitive.
bSave:sensitive = bSpellCheck:sensitive.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  apply "VALUE-CHANGED" to {&browse-name}.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI tNotes  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME tNotes.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EditNote tNotes 
PROCEDURE EditNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
   do with frame {&frame-name}:
    
    for first claimnote exclusive-lock
        where claimnote.claimID = noteClaimID
          and claimnote.seq = noteSeq:
      
      assign
        claimnote.category = tCategory:screen-value
        claimnote.subject = tSubject:screen-value
        claimnote.notes = tNote:screen-value
        claimnote.edited = true
        .
      publish "GetNoteCategoryDesc" (claimnote.category, output claimnote.categoryDesc).
      
    end.
      browse {&browse-name}:refresh().
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI tNotes  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCategory tSubject tNote 
      WITH FRAME tNotes.
  ENABLE RECT-37 brwNotes tSubject tNote bCancel 
      WITH FRAME tNotes.
  VIEW FRAME tNotes.
  {&OPEN-BROWSERS-IN-QUERY-tNotes}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData tNotes 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo initial "where noteType = 'U'".
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

