&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNote 
/* dialogtransfer.w
@desc Allow the adminstrator to transfer to another user
@author D.Sinclair
@date 12.30.2016
 */

define input parameter hFileDataSrv as handle no-undo.
def input parameter pCurrentUid as char.
def output parameter pSuccess as logical init false.
def output parameter pNewUid as char.

{lib/std-def.i}

{tt/sysuser.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNote

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSpellCheck tUser tNote bCancel 
&Scoped-Define DISPLAYED-OBJECTS tNote 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 21 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Transfer" 
     SIZE 21 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE tUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "Transfer To" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Select","NA"
     DROP-DOWN-LIST
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE tNote AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 160 BY 9.52
     BGCOLOR 15 FONT 5 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNote
     bSpellCheck AT ROW 1.24 COL 168 WIDGET-ID 308 NO-TAB-STOP 
     tUser AT ROW 1.71 COL 13 COLON-ALIGNED WIDGET-ID 10
     tNote AT ROW 3.14 COL 15 NO-LABEL WIDGET-ID 8
     bSave AT ROW 13.14 COL 64
     bCancel AT ROW 13.14 COL 90
     "Note:" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 3.19 COL 9.2 WIDGET-ID 6
     SPACE(161.99) SKIP(11.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Transfer"
         DEFAULT-BUTTON bCancel CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNote
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fNote:SCROLLABLE       = FALSE
       FRAME fNote:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME fNote
   NO-ENABLE                                                            */
ASSIGN 
       tNote:RETURN-INSERTED IN FRAME fNote  = TRUE.

/* SETTINGS FOR COMBO-BOX tUser IN FRAME fNote
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNote fNote
ON WINDOW-CLOSE OF FRAME fNote /* Transfer */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fNote
ON CHOOSE OF bCancel IN FRAME fNote /* Cancel */
DO:
  APPLY "WINDOW-CLOSE":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck fNote
ON CHOOSE OF bSpellCheck IN FRAME fNote /* Spell Check */
DO:
  ASSIGN tNote.
  run util/spellcheck.p ({&window-name}:handle, input-output tNote, output std-lo).
  display tNote with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUser fNote
ON VALUE-CHANGED OF tUser IN FRAME fNote /* Transfer To */
DO:
  bSave:sensitive in frame {&frame-name} = tUser:screen-value <> pCurrentUid.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNote 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:parent = ACTIVE-WINDOW.


RUN enable_UI.

bSpellCheck:load-image-up("images/spellcheck.bmp").
bSpellCheck:load-image-insensitive("images/spellcheck-i.bmp").

publish "GetClmUsersList" (tUser:delimiter,
                            "", "", 
                            output std-ch).
tUser:list-item-pairs = std-ch.
tUser:screen-value = entry(2, std-ch, tUser:delimiter).
apply "VALUE-CHANGED" to tUser.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
 
  run SetAssignee in hFileDataSrv (tUser:screen-value,
                                   tNote:screen-value, 
                                   output std-lo,
                                   output std-ch) no-error.
  if std-lo 
   then 
    do: pSuccess = true.
        pNewUid = tUser:screen-value.
        leave MAIN-BLOCK.
    end.
  MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNote  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNote.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNote  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tNote 
      WITH FRAME fNote.
  ENABLE bSpellCheck tUser tNote bCancel 
      WITH FRAME fNote.
  VIEW FRAME fNote.
  {&OPEN-BROWSERS-IN-QUERY-fNote}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

