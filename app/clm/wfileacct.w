&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: 

  Description: from cntnrwin.w - ADM SmartWindow Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: New V9 Version - January 15, 1998
          
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.
define input parameter pClaimID as int no-undo.
define input parameter hFileDataSrv as handle no-undo.

/* ***************************  Definitions  ************************** */

{src/adm2/widgetprto.i}

{lib/brw-multi-def.i}
{lib/get-reserve-type.i}
{lib/add-delimiter.i}
{lib/std-def.i}
{lib/count-rows.i}
{lib/winlaunch.i}
{lib/set-button-def.i}
/* {lib/do-wait.i}  */

{tt/claimacct.i}
{tt/claimacct.i &tableAlias=viewclaimacct}
{tt/claimadjust.i}
{tt/arclm.i}
{tt/apclm.i}
{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/aptrx.i}
{tt/arinv.i}
{tt/artrx.i}
{tt/claimadjreq.i}
{tt/claimadjtrx.i}

def var activePageNumber as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwClaimAcct

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES claimacct apclm arclm claimadjust

/* Definitions for BROWSE brwClaimAcct                                  */
&Scoped-define FIELDS-IN-QUERY-brwClaimAcct claimacct.transDescription claimacct.statDesc claimacct.refID claimacct.refDescription claimacct.requestedDesc claimacct.dateRequested claimacct.uidDesc claimacct.approvedDate claimacct.transDate claimacct.transAmount claimacct.balance   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwClaimAcct   
&Scoped-define SELF-NAME brwClaimAcct
&Scoped-define QUERY-STRING-brwClaimAcct FOR EACH claimacct where claimacct.acctType = "E"   by claimacct.seq by claimacct.transDate. /* by claimacct.transDate is required, ~
       but is left here for clarify */
&Scoped-define OPEN-QUERY-brwClaimAcct OPEN QUERY {&SELF-NAME} FOR EACH claimacct where claimacct.acctType = "E"   by claimacct.seq by claimacct.transDate. /* by claimacct.transDate is required, ~
       but is left here for clarify */.
&Scoped-define TABLES-IN-QUERY-brwClaimAcct claimacct
&Scoped-define FIRST-TABLE-IN-QUERY-brwClaimAcct claimacct


/* Definitions for BROWSE brwPayables                                   */
&Scoped-define FIELDS-IN-QUERY-brwPayables apclm.stat apclm.hasDocument apclm.reduceLiability apclm.report apclm.refCategory apclm.vendorID apclm.vendorName apclm.invoiceDate apclm.invoiceNumber apclm.approvalDate apclm.approval apclm.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwPayables   
&Scoped-define SELF-NAME brwPayables
&Scoped-define QUERY-STRING-brwPayables FOR EACH apclm by apclm.seq by apclm.transDate
&Scoped-define OPEN-QUERY-brwPayables OPEN QUERY {&SELF-NAME} FOR EACH apclm by apclm.seq by apclm.transDate.
&Scoped-define TABLES-IN-QUERY-brwPayables apclm
&Scoped-define FIRST-TABLE-IN-QUERY-brwPayables apclm


/* Definitions for BROWSE brwReceivables                                */
&Scoped-define FIELDS-IN-QUERY-brwReceivables arclm.stat arclm.hasDocument arclm.refCategory arclm.name arclm.contactName arclm.contactPhone arclm.dateRequested arclm.uid arclm.requestedAmount arclm.transAmount arclm.waivedAmount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwReceivables   
&Scoped-define SELF-NAME brwReceivables
&Scoped-define QUERY-STRING-brwReceivables FOR EACH arclm by arclm.seq by arclm.transDate
&Scoped-define OPEN-QUERY-brwReceivables OPEN QUERY {&SELF-NAME} FOR EACH arclm by arclm.seq by arclm.transDate.
&Scoped-define TABLES-IN-QUERY-brwReceivables arclm
&Scoped-define FIRST-TABLE-IN-QUERY-brwReceivables arclm


/* Definitions for BROWSE brwReserves                                   */
&Scoped-define FIELDS-IN-QUERY-brwReserves claimadjust.stat claimadjust.refCategory claimadjust.notes claimadjust.requestedBy claimadjust.dateRequested claimadjust.transDate claimadjust.uid claimadjust.requestedAmount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwReserves   
&Scoped-define SELF-NAME brwReserves
&Scoped-define QUERY-STRING-brwReserves FOR EACH claimadjust by claimadjust.stat desc by claimadjust.transDate
&Scoped-define OPEN-QUERY-brwReserves OPEN QUERY {&SELF-NAME} FOR EACH claimadjust by claimadjust.stat desc by claimadjust.transDate.
&Scoped-define TABLES-IN-QUERY-brwReserves claimadjust
&Scoped-define FIRST-TABLE-IN-QUERY-brwReserves claimadjust


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwClaimAcct}

/* Definitions for FRAME fPayables                                      */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fPayables ~
    ~{&OPEN-QUERY-brwPayables}

/* Definitions for FRAME fReceivables                                   */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fReceivables ~
    ~{&OPEN-QUERY-brwReceivables}

/* Definitions for FRAME fReserves                                      */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fReserves ~
    ~{&OPEN-QUERY-brwReserves}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bClaimAcctRefresh tClaimID tDescription ~
bLedgerExport fLedgerType brwClaimAcct 
&Scoped-Define DISPLAYED-OBJECTS tClaimID fLedgerType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetReserveCategoryList wWin 
FUNCTION GetReserveCategoryList RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU menuBrwInvoices 
       MENU-ITEM m_Invoice_View_Document LABEL "View Document" 
       MENU-ITEM m_Invoice_Attach_Document LABEL "Attach Document"
       MENU-ITEM m_Invoice_Delete_Document LABEL "Delete Document"
       RULE
       MENU-ITEM m_Invoice_Modify LABEL "Modify"        
       RULE
       MENU-ITEM m_Reduce_Liability LABEL "Reduce Liability"
              TOGGLE-BOX.

DEFINE MENU menuBrwReceipts 
       MENU-ITEM m_Receipt_View_Document LABEL "View Document" 
       MENU-ITEM m_Receipt_Attach_Document LABEL "Attach Document"
       MENU-ITEM m_Receipt_Delete_Document LABEL "Delete Document"
       RULE
       MENU-ITEM m_Receipt_Modify LABEL "Modify"        .


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClaimAcctRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bLedgerExport 
     LABEL "Export" 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE tClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 139 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE fLedgerType AS CHARACTER INITIAL "E" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "LAE", "E",
"Loss", "L"
     SIZE 21 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 189.2 BY 1.91.

DEFINE BUTTON bPayableApprove  NO-FOCUS
     LABEL "Appr" 
     SIZE 4.8 BY 1.14 TOOLTIP "Approve the selected invoice".

DEFINE BUTTON bPayableCheck  NO-FOCUS
     LABEL "Check" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create check request".

DEFINE BUTTON bPayableCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy the selected invoice".

DEFINE BUTTON bPayableDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected invoice".

DEFINE BUTTON bPayableDoc  NO-FOCUS
     LABEL "Doc" 
     SIZE 4.8 BY 1.14 TOOLTIP "Attach document for selected invoice".

DEFINE BUTTON bPayableExport 
     LABEL "Export" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bPayableModify  NO-FOCUS
     LABEL "Mod" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify the selected invoice".

DEFINE BUTTON bPayableNew  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new invoice".

DEFINE BUTTON bPayableSend  NO-FOCUS
     LABEL "Send" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send the selected invoice for approvals".

DEFINE VARIABLE cmbPayableAcctType AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Ledger" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Loss","L",
                     "LAE","E"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tPayableNotes AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 183 BY .91 NO-UNDO.

DEFINE BUTTON bReceivableCheck  NO-FOCUS
     LABEL "Check" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create an invoice document".

DEFINE BUTTON bReceivableCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy the selected receipt into a new receipt".

DEFINE BUTTON bReceivableDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected receipt".

DEFINE BUTTON bReceivableDoc  NO-FOCUS
     LABEL "Doc" 
     SIZE 4.8 BY 1.14 TOOLTIP "Attach documents for selected receipt".

DEFINE BUTTON bReceivableExport 
     LABEL "Export" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bReceivableModify  NO-FOCUS
     LABEL "Mod" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify the selected receipt".

DEFINE BUTTON bReceivableNew  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new receipt".

DEFINE VARIABLE cmbReceiptCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tReceivableNotes AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 183 BY .91 NO-UNDO.

DEFINE BUTTON bReserveApprove  NO-FOCUS
     LABEL "Appr" 
     SIZE 4.8 BY 1.14 TOOLTIP "Approve the selected adjustment request".

DEFINE BUTTON bReserveCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy the selected adjustment request".

DEFINE BUTTON bReserveDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected adjustment request".

DEFINE BUTTON bReserveExport 
     LABEL "Export" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bReserveModify  NO-FOCUS
     LABEL "Mod" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bReserveNew  NO-FOCUS
     LABEL "New" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new adjustment request".

DEFINE BUTTON bReserveSend  NO-FOCUS
     LABEL "Send" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send the selected adjustment request for approval".

DEFINE VARIABLE cmbReserveAcctType AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Ledger" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwClaimAcct FOR 
      claimacct SCROLLING.

DEFINE QUERY brwPayables FOR 
      apclm SCROLLING.

DEFINE QUERY brwReceivables FOR 
      arclm SCROLLING.

DEFINE QUERY brwReserves FOR 
      claimadjust SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwClaimAcct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwClaimAcct wWin _FREEFORM
  QUERY brwClaimAcct DISPLAY
      claimacct.transDescription column-label "Type" format "x(20)" width 12
claimacct.statDesc column-label "Stat" format "X(20)" width 12
claimacct.refID column-label "ID" format "x(10)" width 9
claimacct.refDescription column-label "Description" format "x(50)" width 30
claimacct.requestedDesc column-label "Requested By" format "x(32)" width 15
claimacct.dateRequested column-label "Date!Requested" format "99/99/99" width 13
claimacct.uidDesc column-label "Approver" format "x(32)" width 29
claimacct.approvedDate column-label "Approval!Date" format "99/99/9999" width 13
claimacct.transDate column-label "Trx!Date" format "99/99/9999" width 13
claimacct.transAmount column-label "Trx!Amount" format "(>>,>>>,>>9.99)" width 15
claimacct.balance column-label "Balance" format "(>>,>>>,>>9.99)" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-AUTO-VALIDATE NO-ROW-MARKERS NO-COLUMN-SCROLLING SEPARATORS NO-VALIDATE SIZE 189 BY 10.48
         TITLE "Transactions" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwPayables
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwPayables wWin _FREEFORM
  QUERY brwPayables DISPLAY
      apclm.stat column-label "Stat" format "x(20)" width 12
apclm.hasDocument column-label "Has!Doc" view-as toggle-box
apclm.reduceLiability column-label "Red.!Liab." view-as toggle-box
apclm.report column-label "Is!Rpt" view-as toggle-box
apclm.refCategory column-label "Ref!Cat" format "x(4)" width 7
apclm.vendorID column-label "Vendor ID" format "x(10)" width 13
apclm.vendorName column-label "Vendor Name" format "x(50)" width 35
apclm.invoiceDate column-label "Invoice!Date" format "99/99/99" width 13
apclm.invoiceNumber column-label "Invoice!Number" format "x(20)" width 13
apclm.approvalDate column-label "Approval!Date" format "99/99/99" width 13
apclm.approval column-label "Approved By" format "x(50)" width 35
apclm.amount column-label "Invoice!Amount" format "(>>,>>>,>>9.99)" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 183 BY 9.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwReceivables
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwReceivables wWin _FREEFORM
  QUERY brwReceivables DISPLAY
      arclm.stat column-label "Stat" format "x(20)"  width 12
      arclm.hasDocument column-label "Has!Doc" view-as toggle-box
arclm.refCategory column-label "Ref!Cat" format "x(20)" width 20
arclm.name column-label "Name" format "x(200)" width 20
arclm.contactName column-label "Contact Name" format "x(100)" width 20
arclm.contactPhone column-label "Phone" format "x(50)" width 14
arclm.dateRequested column-label "Date!Requested" format "99/99/99" width 13
arclm.uid column-label "Requested By" format "x(100)" width 22
arclm.requestedAmount column-label "Requested!Amount" format "(>>,>>>,>>9.99)" width 15
arclm.transAmount column-label "Completed!Amount" format "(>>,>>>,>>9.99)" width 15
arclm.waivedAmount column-label "Waived!Amount" format "(>>,>>>,>>9.99)" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 183 BY 9.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwReserves
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwReserves wWin _FREEFORM
  QUERY brwReserves DISPLAY
      claimadjust.stat column-label "Stat" format "x(20)" width 12
claimadjust.refCategory column-label "Ref!Cat" format "x(4)" width 7
claimadjust.notes column-label "Notes" format "x(200)" width 59
claimadjust.requestedBy column-label "Requested By" format "x(32)" width 25
claimadjust.dateRequested column-label "Date!Requested" format "99/99/99" width 15
claimadjust.transDate column-label "Date!Approved" format "99/99/99" width 15
claimadjust.uid column-label "Approved By" format "x(32)" width 25
claimadjust.requestedAmount column-label "Requested!Amount" format "(>>,>>>,>>9.99)" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 183 BY 10.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bClaimAcctRefresh AT ROW 1.24 COL 183.8 WIDGET-ID 4 NO-TAB-STOP 
     tClaimID AT ROW 1.71 COL 13 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     tDescription AT ROW 1.71 COL 41 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     bLedgerExport AT ROW 19.91 COL 184.8 WIDGET-ID 68
     fLedgerType AT ROW 20.05 COL 4 NO-LABEL WIDGET-ID 64
     brwClaimAcct AT ROW 21.38 COL 2 WIDGET-ID 1300
     "Ledger" VIEW-AS TEXT
          SIZE 8 BY .95 AT ROW 19.1 COL 3.4 WIDGET-ID 62
          FONT 6
     RECT-35 AT ROW 19.57 COL 2 WIDGET-ID 38
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 191 BY 31.19 WIDGET-ID 100.

DEFINE FRAME fReserves
     bReserveExport AT ROW 1.48 COL 31.6 WIDGET-ID 68
     bReserveNew AT ROW 1.48 COL 2.8 WIDGET-ID 208 NO-TAB-STOP 
     cmbReserveAcctType AT ROW 1.48 COL 162 COLON-ALIGNED WIDGET-ID 278
     brwReserves AT ROW 2.67 COL 3 WIDGET-ID 1500
     bReserveCopy AT ROW 1.48 COL 7.6 WIDGET-ID 284 NO-TAB-STOP 
     bReserveSend AT ROW 1.48 COL 22 WIDGET-ID 282 NO-TAB-STOP 
     bReserveApprove AT ROW 1.48 COL 26.8 WIDGET-ID 276 NO-TAB-STOP 
     bReserveDelete AT ROW 1.48 COL 12.4 WIDGET-ID 210 NO-TAB-STOP 
     bReserveModify AT ROW 1.48 COL 17.2 WIDGET-ID 212 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 3 ROW 4.57
         SIZE 187 BY 13.81
         TITLE "Reserves" WIDGET-ID 600.

DEFINE FRAME fReceivables
     bReceivableCheck AT ROW 1.48 COL 26.8 WIDGET-ID 320 NO-TAB-STOP 
     bReceivableExport AT ROW 1.48 COL 31.6 WIDGET-ID 68
     cmbReceiptCategory AT ROW 1.48 COL 162 COLON-ALIGNED WIDGET-ID 318
     brwReceivables AT ROW 2.67 COL 3 WIDGET-ID 2200
     tReceivableNotes AT ROW 12.38 COL 3 NO-LABEL WIDGET-ID 310
     bReceivableCopy AT ROW 1.48 COL 7.6 WIDGET-ID 316 NO-TAB-STOP 
     bReceivableDoc AT ROW 1.48 COL 22 WIDGET-ID 304 NO-TAB-STOP 
     bReceivableNew AT ROW 1.48 COL 2.8 WIDGET-ID 298 NO-TAB-STOP 
     bReceivableDelete AT ROW 1.48 COL 12.4 WIDGET-ID 302 NO-TAB-STOP 
     bReceivableModify AT ROW 1.48 COL 17.2 WIDGET-ID 306 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 3 ROW 4.57
         SIZE 187 BY 13.81
         TITLE "Receivables" WIDGET-ID 1700.

DEFINE FRAME fPayables
     bPayableExport AT ROW 1.48 COL 41.2 WIDGET-ID 68
     bPayableCheck AT ROW 1.48 COL 36.4 WIDGET-ID 322 NO-TAB-STOP 
     cmbPayableAcctType AT ROW 1.48 COL 162 COLON-ALIGNED WIDGET-ID 278
     brwPayables AT ROW 2.67 COL 3 WIDGET-ID 1800
     tPayableNotes AT ROW 12.38 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     bPayableCopy AT ROW 1.48 COL 7.6 WIDGET-ID 320 NO-TAB-STOP 
     bPayableNew AT ROW 1.48 COL 2.8 WIDGET-ID 298 NO-TAB-STOP 
     bPayableSend AT ROW 1.48 COL 22 WIDGET-ID 316 NO-TAB-STOP 
     bPayableApprove AT ROW 1.48 COL 26.8 WIDGET-ID 300 NO-TAB-STOP 
     bPayableDoc AT ROW 1.48 COL 31.6 WIDGET-ID 304 NO-TAB-STOP 
     bPayableDelete AT ROW 1.48 COL 12.4 WIDGET-ID 302 NO-TAB-STOP 
     bPayableModify AT ROW 1.48 COL 17.2 WIDGET-ID 306 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 3 ROW 4.57
         SIZE 187 BY 13.81
         TITLE "Payables" WIDGET-ID 1600.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "File Accounting"
         HEIGHT             = 31.19
         WIDTH              = 191
         MAX-HEIGHT         = 48.91
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.91
         VIRTUAL-WIDTH      = 384
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fPayables:FRAME = FRAME fMain:HANDLE
       FRAME fReceivables:FRAME = FRAME fMain:HANDLE
       FRAME fReserves:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwClaimAcct fLedgerType fMain */
ASSIGN 
       brwClaimAcct:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwClaimAcct:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tClaimID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tDescription IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tDescription:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME fPayables
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwPayables cmbPayableAcctType fPayables */
ASSIGN 
       FRAME fPayables:HIDDEN           = TRUE
       FRAME fPayables:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bPayableApprove IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableCheck IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableCopy IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableDelete IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableDoc IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableModify IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableNew IN FRAME fPayables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPayableSend IN FRAME fPayables
   NO-ENABLE                                                            */
ASSIGN 
       brwPayables:POPUP-MENU IN FRAME fPayables             = MENU menuBrwInvoices:HANDLE
       brwPayables:COLUMN-RESIZABLE IN FRAME fPayables       = TRUE
       brwPayables:COLUMN-MOVABLE IN FRAME fPayables         = TRUE.

ASSIGN 
       tPayableNotes:READ-ONLY IN FRAME fPayables        = TRUE.

/* SETTINGS FOR FRAME fReceivables
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwReceivables cmbReceiptCategory fReceivables */
ASSIGN 
       FRAME fReceivables:HIDDEN           = TRUE
       FRAME fReceivables:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bReceivableCheck IN FRAME fReceivables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReceivableCopy IN FRAME fReceivables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReceivableDelete IN FRAME fReceivables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReceivableDoc IN FRAME fReceivables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReceivableModify IN FRAME fReceivables
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReceivableNew IN FRAME fReceivables
   NO-ENABLE                                                            */
ASSIGN 
       brwReceivables:POPUP-MENU IN FRAME fReceivables             = MENU menuBrwReceipts:HANDLE
       brwReceivables:COLUMN-RESIZABLE IN FRAME fReceivables       = TRUE
       brwReceivables:COLUMN-MOVABLE IN FRAME fReceivables         = TRUE.

/* SETTINGS FOR FILL-IN tReceivableNotes IN FRAME fReceivables
   ALIGN-L                                                              */
ASSIGN 
       tReceivableNotes:READ-ONLY IN FRAME fReceivables        = TRUE.

/* SETTINGS FOR FRAME fReserves
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwReserves cmbReserveAcctType fReserves */
ASSIGN 
       FRAME fReserves:HIDDEN           = TRUE
       FRAME fReserves:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bReserveApprove IN FRAME fReserves
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReserveCopy IN FRAME fReserves
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReserveDelete IN FRAME fReserves
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReserveModify IN FRAME fReserves
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReserveNew IN FRAME fReserves
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bReserveSend IN FRAME fReserves
   NO-ENABLE                                                            */
ASSIGN 
       brwReserves:ALLOW-COLUMN-SEARCHING IN FRAME fReserves = TRUE
       brwReserves:COLUMN-RESIZABLE IN FRAME fReserves       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwClaimAcct
/* Query rebuild information for BROWSE brwClaimAcct
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimacct where claimacct.acctType = "E"
  by claimacct.seq by claimacct.transDate.
/* by claimacct.transDate is required, but is left here for clarify */
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwClaimAcct */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwPayables
/* Query rebuild information for BROWSE brwPayables
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apclm by apclm.seq by apclm.transDate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwPayables */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwReceivables
/* Query rebuild information for BROWSE brwReceivables
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH arclm by arclm.seq by arclm.transDate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwReceivables */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwReserves
/* Query rebuild information for BROWSE brwReserves
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimadjust by claimadjust.stat desc by claimadjust.transDate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwReserves */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fPayables
/* Query rebuild information for FRAME fPayables
     _Query            is NOT OPENED
*/  /* FRAME fPayables */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fReceivables
/* Query rebuild information for FRAME fReceivables
     _Query            is NOT OPENED
*/  /* FRAME fReceivables */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fReserves
/* Query rebuild information for FRAME fReserves
     _Query            is NOT OPENED
*/  /* FRAME fReserves */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* File Accounting */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* File Accounting */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClaimAcctRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClaimAcctRefresh wWin
ON CHOOSE OF bClaimAcctRefresh IN FRAME fMain /* Refresh */
DO:
  run LoadClaimAccounting in hFileDataSrv.
  
  /* refresh the transaction ledger */
  run GetData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLedgerExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLedgerExport wWin
ON CHOOSE OF bLedgerExport IN FRAME fMain /* Export */
DO:
  run exportBrowse in this-procedure (browse brwClaimAcct:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPayables
&Scoped-define SELF-NAME bPayableApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableApprove wWin
ON CHOOSE OF bPayableApprove IN FRAME fPayables /* Appr */
DO:
  if not available apclm
   then return.
   
  /* Approving an invoice (payable) */
  run ApprovePayable in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableCheck wWin
ON CHOOSE OF bPayableCheck IN FRAME fPayables /* Check */
DO:
  if not available apclm
   then return.
   
  run ViewPayableCheckRequest in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableCopy wWin
ON CHOOSE OF bPayableCopy IN FRAME fPayables /* Copy */
DO:
  if not available apclm
   then return.
   
  /* Creating a new invoice (payable) via copy */
  run CopyPayable in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableDelete wWin
ON CHOOSE OF bPayableDelete IN FRAME fPayables /* Del */
DO:
  if not available apclm
   then return.
   
  /* Deleteing an invoice (payable) */
  run DeletePayable in this-procedure (apclm.apinvID,apclm.hasDocument).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableDoc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableDoc wWin
ON CHOOSE OF bPayableDoc IN FRAME fPayables /* Doc */
DO:
  if not available apclm
   then return.
   
  if apclm.hasDocument
   then run ViewPayableDocument in this-procedure (apclm.apinvID).
   else run AttachPayableDocument in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableExport wWin
ON CHOOSE OF bPayableExport IN FRAME fPayables /* Export */
DO:
  run exportBrowse in this-procedure (browse brwPayables:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableModify wWin
ON CHOOSE OF bPayableModify IN FRAME fPayables /* Mod */
DO:
  if not available apclm
   then return.
   
  /* Modify an invoice */
  run ModifyPayable in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableNew wWin
ON CHOOSE OF bPayableNew IN FRAME fPayables /* New */
DO:
  /* Newing a new invoice (payable) */
  run NewPayable in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPayableSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPayableSend wWin
ON CHOOSE OF bPayableSend IN FRAME fPayables /* Send */
DO:
  /* Send the invoice for approvals */
  if not available apclm
   then return.
   
  define variable cEmail as character no-undo.
  define variable cEmailList as character no-undo initial "".
  define variable cPendingApproval as character no-undo initial "".
  
  run GetPayableInvoiceApprover in hFileDataSrv (apclm.apinvID, output cPendingApproval).
  do std-in = 1 to num-entries(cPendingApproval):
    std-ch = entry(std-in,cPendingApproval).
    cEmail = "".
    publish "GetSysUserEmail" (std-ch,output cEmail).
    if cEmail > ""
     then cEmailList = addDelimiter(cEmailList,";") + "mailto:" + cEmail.
  end.
  if cEmailList > ""
   then
    do:
      cEmailList = cEmailList + "?subject=Pending Approval for File " + string(pClaimID).
      RUN ShellExecuteA in this-procedure (0,
                                           "open",
                                           cEmailList,
                                           "",
                                           "",
                                           1,
                                           OUTPUT std-in).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fReceivables
&Scoped-define SELF-NAME bReceivableCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableCheck wWin
ON CHOOSE OF bReceivableCheck IN FRAME fReceivables /* Check */
DO:
  if not available arclm
   then return.
   
  run ViewReceivableCheckRequest in this-procedure (arclm.arinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReceivableCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableCopy wWin
ON CHOOSE OF bReceivableCopy IN FRAME fReceivables /* Copy */
DO:
  if not available arclm
   then return.
   
  /* Newing a new receivable */
  run CopyReceivable in this-procedure (arclm.arinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReceivableDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableDelete wWin
ON CHOOSE OF bReceivableDelete IN FRAME fReceivables /* Del */
DO:
  if not available arclm
   then return.
   
  /* Deleteing an invoice (receivable) */
  run DeleteReceivable in this-procedure (arclm.arinvID,arclm.hasDocument).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReceivableDoc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableDoc wWin
ON CHOOSE OF bReceivableDoc IN FRAME fReceivables /* Doc */
DO:
  /* Attach a document to the receivable */
  if not available arclm
   then return.
   
  if arclm.hasDocument
   then run ViewReceivableDocument in this-procedure (arclm.arinvID).
   else run AttachReceivableDocument in this-procedure (arclm.arinvID, output std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReceivableExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableExport wWin
ON CHOOSE OF bReceivableExport IN FRAME fReceivables /* Export */
DO:
  run exportBrowse in this-procedure (browse brwReceivables:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReceivableModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableModify wWin
ON CHOOSE OF bReceivableModify IN FRAME fReceivables /* Mod */
DO:
  if not available arclm
   then return.
   
  /* Modify an invoice */
  run ModifyReceivable in this-procedure (arclm.arinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReceivableNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReceivableNew wWin
ON CHOOSE OF bReceivableNew IN FRAME fReceivables /* New */
DO:
  /* Creating a new invoice (payable) via copy */
  run NewReceivable in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fReserves
&Scoped-define SELF-NAME bReserveApprove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveApprove wWin
ON CHOOSE OF bReserveApprove IN FRAME fReserves /* Appr */
DO:
  if not available claimadjust
   then return.
   
  /* Approving a request adjustment */
  run ApproveReserve in this-procedure (claimadjust.refCategory,claimadjust.requestedAmount,claimadjust.notes).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReserveCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveCopy wWin
ON CHOOSE OF bReserveCopy IN FRAME fReserves /* Copy */
DO:
  if not available claimadjust
   then return.
   
  /* Copying the selected adjustment request */
  run CopyReserve in this-procedure (claimadjust.refCategory,claimadjust.transDate).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReserveDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveDelete wWin
ON CHOOSE OF bReserveDelete IN FRAME fReserves /* Del */
DO:
  if not available claimadjust
   then return.
   
  /* Deleting an adjustment request */
  run DeleteReserve in this-procedure (claimadjust.refCategory).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReserveExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveExport wWin
ON CHOOSE OF bReserveExport IN FRAME fReserves /* Export */
DO:
  run exportBrowse in this-procedure (browse brwReserves:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReserveModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveModify wWin
ON CHOOSE OF bReserveModify IN FRAME fReserves /* Mod */
DO:
  if not available claimadjust
   then return.
   
  /* Modifying an adjustment request */
  run ModifyReserve in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReserveNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveNew wWin
ON CHOOSE OF bReserveNew IN FRAME fReserves /* New */
DO:
  /* Creating a new adjustment request */
  run NewReserve in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReserveSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReserveSend wWin
ON CHOOSE OF bReserveSend IN FRAME fReserves /* Send */
DO:
  /* Send the adjustment request for approval */
  if not available claimadjust
   then return.
   
  run OpenReserveTemplate in this-procedure (input claimadjust.uid,
                                             input claimadjust.requestedAmount,
                                             input claimadjust.refCategory,
                                             input claimadjust.dateRequested
                                             ).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwClaimAcct
&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME brwClaimAcct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaimAcct wWin
ON DEFAULT-ACTION OF brwClaimAcct IN FRAME fMain /* Transactions */
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaimAcct wWin
ON ROW-DISPLAY OF brwClaimAcct IN FRAME fMain /* Transactions */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwClaimAcct wWin
ON START-SEARCH OF brwClaimAcct IN FRAME fMain /* Transactions */
DO:
/*   {lib/brw-startSearch-multi.i}  */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwPayables
&Scoped-define FRAME-NAME fPayables
&Scoped-define SELF-NAME brwPayables
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayables wWin
ON DEFAULT-ACTION OF brwPayables IN FRAME fPayables
DO:
  if not available apclm
   then return.
   
  /* double clicking in the browse will modify */
  run ModifyPayable in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayables wWin
ON ROW-DISPLAY OF brwPayables IN FRAME fPayables
DO:
  define variable cUsername as character no-undo initial "".
  define variable cUser as character no-undo.
  {lib/brw-rowDisplay-multi.i}

  /* set the payable type */
  std-ch = "".
  publish "GetPayableTypeDesc" (apclm.refCategory, output std-ch).
  apclm.refCategory:screen-value in browse brwPayables = std-ch.
  /* set the payable stat */
  std-ch = "".
  publish "GetPayableStatusDesc" (apclm.stat, output std-ch).
  apclm.stat:screen-value in browse brwPayables = std-ch.
  /* set the user's name for the approval column */
  do std-in = 1 to num-entries(apclm.approval):
    cUser = trim(entry(std-in,apclm.approval)).
    std-ch = "".
    publish "GetSysUserName" (cUser,output std-ch).
    if std-ch = "NOT FOUND"
     then std-ch = cUser.
    cUsername = addDelimiter(cUsername,", ") + std-ch.
  end.
  apclm.approval:screen-value in browse brwPayables = cUsername.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayables wWin
ON START-SEARCH OF brwPayables IN FRAME fPayables
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwPayables wWin
ON VALUE-CHANGED OF brwPayables IN FRAME fPayables
DO:
  /* the value-changed trigger can be used to dynamically */
  /* set buttons while navigating the browse window */
  run SetTab in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwReceivables
&Scoped-define FRAME-NAME fReceivables
&Scoped-define SELF-NAME brwReceivables
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReceivables wWin
ON DEFAULT-ACTION OF brwReceivables IN FRAME fReceivables
DO:
  if not available arclm
   then return.
   
  /* Modify an invoice */
  run ModifyReceivable in this-procedure (arclm.arinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReceivables wWin
ON ROW-DISPLAY OF brwReceivables IN FRAME fReceivables
DO:
  {lib/brw-rowDisplay-multi.i}
  /* set the requested by column to the user's name */
  std-ch = "".
  publish "GetSysUserName" (arclm.uid, output std-ch).
  if std-ch = "NOT FOUND"
   then std-ch = arclm.uid.
  arclm.uid:screen-value in browse brwReceivables = std-ch.
  /* set the receivable category */
  std-ch = "".
  publish "GetReceivableCategoryDesc" (arclm.refCategory, output std-ch).
  arclm.refCategory:screen-value in browse brwReceivables = std-ch.
  /* set the receivable stat */
  std-ch = "".
  publish "GetReceivableStatusDesc" (arclm.stat, output std-ch).
  arclm.stat:screen-value in browse brwReceivables = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReceivables wWin
ON START-SEARCH OF brwReceivables IN FRAME fReceivables
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReceivables wWin
ON VALUE-CHANGED OF brwReceivables IN FRAME fReceivables
DO:
  /* the value-changed trigger can be used to dynamically */
  /* set buttons while navigating the browse window */
  run SetTab in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwReserves
&Scoped-define FRAME-NAME fReserves
&Scoped-define SELF-NAME brwReserves
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReserves wWin
ON DEFAULT-ACTION OF brwReserves IN FRAME fReserves
DO:
  if not available claimadjust
   then return.
   
   run ModifyReserve in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReserves wWin
ON ROW-DISPLAY OF brwReserves IN FRAME fReserves
DO:
  {lib/brw-rowDisplay-multi.i}

  /* set the payable type */
  std-ch = "".
  publish "GetPayableTypeDesc" (claimadjust.refCategory, output std-ch).
  claimadjust.refCategory:screen-value in browse brwReserves = std-ch.
  /* set the reserve stat */
  std-ch = "".
  publish "GetReserveStatusDesc" (claimadjust.stat, output std-ch).
  claimadjust.stat:screen-value in browse brwReserves = std-ch.
  /* set the requested by column to the user's name */
  std-ch = "".
  publish "GetSysUserName" (claimadjust.requestedBy, output std-ch).
  if std-ch = "NOT FOUND"
   then std-ch = claimadjust.requestedBy.
  claimadjust.requestedBy:screen-value in browse brwReserves = std-ch.
  /* set the approval column to the user's name */
  if claimadjust.stat <> "O"
   then
    do:
      std-ch = "".
      publish "GetSysUserName" (claimadjust.uid, output std-ch).
      if std-ch = "NOT FOUND"
       then std-ch = claimadjust.uid.
      claimadjust.uid:screen-value in browse brwReserves = std-ch.
    end.
   else claimadjust.uid:screen-value in browse brwReserves = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReserves wWin
ON START-SEARCH OF brwReserves IN FRAME fReserves
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwReserves wWin
ON VALUE-CHANGED OF brwReserves IN FRAME fReserves
DO:
  /* the value-changed trigger can be used to dynamically */
  /* set buttons while navigating the browse window */
  run SetTab in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPayables
&Scoped-define SELF-NAME cmbPayableAcctType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbPayableAcctType wWin
ON VALUE-CHANGED OF cmbPayableAcctType IN FRAME fPayables /* Ledger */
DO:
  RUN FilterPayable IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fReceivables
&Scoped-define SELF-NAME cmbReceiptCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbReceiptCategory wWin
ON VALUE-CHANGED OF cmbReceiptCategory IN FRAME fReceivables /* Category */
DO:
  run FilterReceivables in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fReserves
&Scoped-define SELF-NAME cmbReserveAcctType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbReserveAcctType wWin
ON VALUE-CHANGED OF cmbReserveAcctType IN FRAME fReserves /* Ledger */
DO:
  run FilterReserves in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME fLedgerType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLedgerType wWin
ON VALUE-CHANGED OF fLedgerType IN FRAME fMain
DO:
  run FilterLedger in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Attach_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Attach_Document wWin
ON CHOOSE OF MENU-ITEM m_Invoice_Attach_Document /* Attach Document */
DO:
  if not available apclm
   then return.
   
  run AttachPayableDocument in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Delete_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Delete_Document wWin
ON CHOOSE OF MENU-ITEM m_Invoice_Delete_Document /* Delete Document */
DO:
  if not available apclm
   then return.
   
  message "Document will be permanently removed. Continue?"
    view-as alert-box question buttons yes-no update std-lo.
  if not std-lo 
   then return.
   
  run DetachPayableDocument in hFileDataSrv (apclm.apinvID, output std-lo).
  run GetData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_Modify wWin
ON CHOOSE OF MENU-ITEM m_Invoice_Modify /* Modify */
DO:
  if not available apclm
   then return.
   
  run ModifyPayable in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoice_View_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoice_View_Document wWin
ON CHOOSE OF MENU-ITEM m_Invoice_View_Document /* View Document */
DO:
  if not available apclm
   then return.
   
  run ViewPayableDocument in this-procedure (apclm.apinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Receipt_Attach_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Receipt_Attach_Document wWin
ON CHOOSE OF MENU-ITEM m_Receipt_Attach_Document /* Attach Document */
DO:
  if not available arclm
   then return.
   
  run AttachReceivableDocument in this-procedure (arclm.arinvID, output std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Receipt_Delete_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Receipt_Delete_Document wWin
ON CHOOSE OF MENU-ITEM m_Receipt_Delete_Document /* Delete Document */
DO:
  if not available arclm
   then return.
   
  message "Document will be permanently removed. Continue?"
    view-as alert-box question buttons yes-no update std-lo.
  if not std-lo 
   then return.
   
  run DetachReceivableDocument in hFileDataSrv (arclm.arinvID, output std-lo).
  run GetData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Receipt_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Receipt_Modify wWin
ON CHOOSE OF MENU-ITEM m_Receipt_Modify /* Modify */
DO:
  if not available arclm
   then return.
   
  run ModifyReceivable in this-procedure (arclm.arinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Receipt_View_Document
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Receipt_View_Document wWin
ON CHOOSE OF MENU-ITEM m_Receipt_View_Document /* View Document */
DO:
  if not available arclm
   then return.
   
  run ViewReceivableDocument in this-procedure (arclm.arinvID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reduce_Liability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reduce_Liability wWin
ON VALUE-CHANGED OF MENU-ITEM m_Reduce_Liability /* Reduce Liability */
DO:
  if not available apclm
   then return.
   
  run ReduceLiability in hFileDataSrv (apclm.apinvID, self:checked, output std-lo).
  run GetData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwClaimAcct
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

subscribe to "ClaimAccountingDataChanged" anywhere.
{lib/win-main.i}

/* DoWait(true).  */
run GetClaimAccounting in hFileDataSrv
                       (output table apclm,
                        output table arclm,
                        output table claimadjust,
                        output table claimacct
                        ).

/* DoWait(false).  */

{lib/brw-main-multi.i &browse-list="brwPayables,brwReceivables,brwReserves,brwClaimAcct"}

{&window-name}:title = "File " + string(pClaimID) + " Accounting".
{&window-name}:max-width-pixels = {&window-name}:width-pixels.

run initializeObject in this-procedure.

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

subscribe to "OpenReserveTemplate" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Reserves|Payables|Receivables' + 'FolderTabWidth0FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 3.38 , 2.00 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 15.24 , 189.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApprovePayable wWin 
PROCEDURE ApprovePayable :
/*------------------------------------------------------------------------------
@description Approves the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  if apclm.amount > 0
   then
    do:
      run dialogapprove.w (pInvoiceID, "Payable", hFileDataSrv, output std-lo).
      if std-lo
       then run GetData in this-procedure.
    end.
   else message "The invoice amount must be greater than $0 to approve" view-as alert-box information buttons ok.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApproveReserve wWin 
PROCEDURE ApproveReserve :
/*------------------------------------------------------------------------------
@description Approves the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pRefCategory as character no-undo.
  define input parameter pRequestedAmount as decimal no-undo.
  define input parameter pNotes as character no-undo.
  
  
  run dialogapprovereserve.w (input pClaimID, 
                              input pRefCategory,
                              input pRequestedAmount,
                              input pNotes,
                              input hFileDataSrv,
                              output std-lo).
  if std-lo
   then run GetData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttachPayableDocument wWin 
PROCEDURE AttachPayableDocument :
/*------------------------------------------------------------------------------
@description Attaches a document to the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.

  /* the invoice doesn't have a document attached */
  define variable cFile as character no-undo.
  SYSTEM-DIALOG GET-FILE cFile 
    TITLE   "Attach Invoice to ShareFile..."
    MUST-EXIST 
    USE-FILENAME 
    UPDATE std-lo.
    
  if std-lo
   then
    do:
      /* confirmation */
      publish "GetConfirmFileUpload" (output std-lo).
      if std-lo 
       then
        do:
          MESSAGE cFile + " will be uploaded. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
          if not std-lo 
           then return.
        end.
      if apclm.hasDocument
       then run DetachPayableDocument in hFileDataSrv (pInvoiceID, output std-lo).
      run AttachPayableDocument in hFileDataSrv (pInvoiceID, cFile, output std-lo).
       
      run GetData in this-procedure.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AttachReceivableDocument wWin 
PROCEDURE AttachReceivableDocument :
/*------------------------------------------------------------------------------
@description Attaches a document to the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define output parameter pFile as character no-undo.

  /* the invoice doesn't have a document attached */
  SYSTEM-DIALOG GET-FILE pFile 
    TITLE   "Attach Invoice to ShareFile..."
    MUST-EXIST 
    USE-FILENAME 
    UPDATE std-lo.
    
  if std-lo
   then
    do:
      /* confirmation */
      publish "GetConfirmFileUpload" (output std-lo).
      if std-lo 
       then
        do:
          MESSAGE pFile + " will be uploaded. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
          if not std-lo 
           then return.
        end.
      if pInvoiceID > 0
       then 
        do:
          run GetClaimReceivable in hFileDataSrv
                                 (input pClaimID,
                                  input pInvoiceID,
                                  output table arinv
                                  ).
                                  
          for first arinv no-lock:
            if arinv.hasDocument
             then run DetachReceivableDocument in hFileDataSrv (arinv.arinvID, output std-lo).
            run AttachReceivableDocument in hFileDataSrv (arinv.arinvID, pFile, output std-lo).
            run GetData in this-procedure.
          end.
       
        end.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePage wWin 
PROCEDURE changePage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/

  RUN SUPER.

  {get CurrentPage activePageNumber}.
  
  case activePageNumber:
  when 1 
   then
    do:
      view frame fReserves.
      enable all with frame fReserves.
      hide frame fPayables.
      hide frame fReceivables.
    end.
  when 2 
   then
    do: 
      hide frame fReserves.
      view frame fPayables.
      enable all with frame fPayables.
      hide frame fReceivables.
    end.
  when 3 
   then
    do: 
      hide frame fReserves.
      hide frame fPayables.
      view frame fReceivables.
      enable all with frame fReceivables.
    end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClaimAccountingDataChanged wWin 
PROCEDURE ClaimAccountingDataChanged :
/*------------------------------------------------------------------------------
@description Refreshes the accounting tab whenever a change is made
------------------------------------------------------------------------------*/
  apply "CHOOSE" to bClaimAcctRefresh in frame {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow wWin 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "WINDOW-CLOSE" to {&window-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopyPayable wWin 
PROCEDURE CopyPayable :
/*------------------------------------------------------------------------------
@description Copies the selected invoice to a new invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  def buffer apinv for apinv.
  def buffer apinva for apinva.
  
  run GetClaimPayable in hFileDataSrv
                            (input pClaimID,
                             input pInvoiceID,
                             output table apinv,
                             output table apinva,
                             output table apinvd
                             ).
                             
  for first apinv exclusive-lock
      where apinv.apinvID = apclm.apinvID:
      
    for each apinva exclusive-lock
       where apinva.apinvID = apinv.apinvID:
       
      assign
        apinv.apinvID = 0
        apinv.stat = "O"
        apinva.stat = "P"
        apinva.amount = 0.0
        apinva.dateActed = ?
        apinv.documentfilename = ""
        .
    end.
  end.
                             
  run dialogclaimpayable.w (input pClaimID,
                            input table apinv,
                            input table apinva,
                            input table apinvd,
                            input hFileDataSrv,
                            input "Copy Payable Invoice",
                            output std-lo
                            ).
                       
  if std-lo
   then run GetData in this-procedure.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopyReceivable wWin 
PROCEDURE CopyReceivable :
/*------------------------------------------------------------------------------
@description Copies the selected invoice to a new invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  def buffer arinv for arinv.
  
  run GetClaimReceivable in hFileDataSrv
                         (input pClaimID,
                          input pInvoiceID,
                          output table arinv
                          ).
                             
  for first arinv exclusive-lock
      where arinv.arinvID = pInvoiceID:
      
    assign
      arinv.arinvID = 0
      arinv.stat = "O"
      .
    MESSAGE "Would you like to add a document?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE std-lo.
    if std-lo
     then
      do:
        std-ch = "".
        run AttachReceivableDocument (arinv.arinvID, output std-ch).
        assign
          arinv.hasDocument = true
          arinv.documentFilename = std-ch
          .
      end.
     else
      assign
        arinv.hasDocument = false
        arinv.documentFilename = ""
        .
        
     
  end.                        
  std-ha = ?.
  run dialogclaimreceivable.w (input pClaimID,
                               input table arinv,
                               input hFileDataSrv,
                               input "Copy Claim Receivable Invoice",
                               output std-lo
                               ).
                       
  if std-lo
   then run GetData in this-procedure.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopyReserve wWin 
PROCEDURE CopyReserve :
/*------------------------------------------------------------------------------
@description Copies the selected adjustment request to a new adjustment request
------------------------------------------------------------------------------*/
  define input parameter pRefCategory as character no-undo.
  define input parameter pTransDate as datetime no-undo.
  define buffer claimadjreq for claimadjreq.
  
  run GetClaimReserve in hFileDataSrv
                      (input pClaimID,
                       input pRefCategory,
                       input pTransDate,
                       output table claimadjreq
                       ).
  
  do with frame fReserves:
    for first claimadjreq exclusive-lock:
      assign
        claimadjreq.isNew = false
        claimadjreq.isView = false
        .
    end.
  end.
  run dialogreserve.w (input table claimadjreq,
                       input hFileDataSrv,
                       input "Copy Claim Adjustment",
                       output std-lo
                       ).
  
  if std-lo
   then run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeletePayable wWin 
PROCEDURE DeletePayable :
/*------------------------------------------------------------------------------
@description Deletes the selected invoice
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pHasDocument as logical no-undo.
  
  std-lo = true.
  if pHasDocument
   then MESSAGE "Deleting the invoice will also delete the attached document. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
   else MESSAGE "Deleting the invoice cannot be undone. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
  
  if std-lo
   then run DeletePayableInvoice in hFileDataSrv (pInvoiceID, output std-lo).
  
  if std-lo
   then run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteReceivable wWin 
PROCEDURE DeleteReceivable :
/*------------------------------------------------------------------------------
@description Deletes the selected receivable
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pHasDocument as logical no-undo.
  
  std-lo = true.
  if pHasDocument
   then MESSAGE "Deleting the invoice will also delete the attached document. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
   else MESSAGE "Deleting the invoice cannot be undone. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
  
  if std-lo
   then run DeleteReceivableInvoice in hFileDataSrv (pInvoiceID, output std-lo).
  
  if std-lo
   then run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteReserve wWin 
PROCEDURE DeleteReserve :
/*------------------------------------------------------------------------------
@description Deletes the selected adjustment request
------------------------------------------------------------------------------*/
  define input parameter pRefCategory as character no-undo.
   
  MESSAGE "Deleting the adjustment request cannot be undone. Continue?" VIEW-AS ALERT-BOX WARNING BUTTONS YES-NO UPDATE std-lo.
  
  if std-lo
   then run DeleteClaimReserve in hFileDataSrv (pClaimID, pRefCategory, output std-lo).
  
  if std-lo
   then run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tClaimID fLedgerType 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bClaimAcctRefresh tClaimID tDescription bLedgerExport fLedgerType 
         brwClaimAcct 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY cmbPayableAcctType tPayableNotes 
      WITH FRAME fPayables IN WINDOW wWin.
  ENABLE bPayableExport cmbPayableAcctType brwPayables tPayableNotes 
      WITH FRAME fPayables IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fPayables}
  FRAME fPayables:SENSITIVE = NO.
  DISPLAY cmbReceiptCategory tReceivableNotes 
      WITH FRAME fReceivables IN WINDOW wWin.
  ENABLE bReceivableExport cmbReceiptCategory brwReceivables tReceivableNotes 
      WITH FRAME fReceivables IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fReceivables}
  FRAME fReceivables:SENSITIVE = NO.
  DISPLAY cmbReserveAcctType 
      WITH FRAME fReserves IN WINDOW wWin.
  ENABLE bReserveExport cmbReserveAcctType brwReserves 
      WITH FRAME fReserves IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fReserves}
  FRAME fReserves:SENSITIVE = NO.
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportBrowse wWin 
PROCEDURE exportBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pBrowse as handle no-undo.

  if pBrowse:query:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "Claim_Accounting_Export"

  std-ch = "C".
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(pBrowse), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(pBrowse), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterLedger wWin 
PROCEDURE FilterLedger :
/*------------------------------------------------------------------------------
@description Applies the filters the user chose for the ledger browse     
------------------------------------------------------------------------------*/
  define variable tQueryString as character no-undo.
  
  define variable hQuery as handle no-undo. 

  std-ha = browse brwClaimAcct:handle.
  hQuery = std-ha:query.
  hQuery:query-close().

  tQueryString = "for each claimacct where claimacct.acctType='" 
        + fLedgerType:screen-value in frame {&frame-name}
        + "' by claimacct.seq by claimacct.transDate".

  hQuery:query-prepare(tQueryString).
  hQuery:query-open().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterPayable wWin 
PROCEDURE FilterPayable :
/*------------------------------------------------------------------------------
@description Applies the filters the user chose for the payable browse 
------------------------------------------------------------------------------*/
  define variable cAcctType as character no-undo.
  define variable tWhereClause as character no-undo.
  define variable tQueryString as character no-undo.
  
  define variable hQuery as handle no-undo.
  
  /* get the filter values */
  do with frame fPayables:
    assign
      std-ha = browse brwPayables:handle
      cAcctType = cmbPayableAcctType:screen-value
      .
  end.
  /* build the query */
  IF cAcctType <> "ALL"
   THEN tWhereClause = "where refCategory='" + cAcctType + "' ".
   
  hQuery = std-ha:query.
  hQuery:query-close().
  tQueryString = "preselect each apclm no-lock " + tWhereClause +
                 "by apclm.seq descending by apclm.transDate".
  hQuery:query-prepare(tQueryString).
  hQuery:query-open().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterReceivables wWin 
PROCEDURE FilterReceivables :
/*------------------------------------------------------------------------------
@description Applies the filters the user chose for the payable browse 
------------------------------------------------------------------------------*/
  define variable cAcctType as character no-undo.
  define variable tWhereClause as character no-undo.
  define variable tQueryString as character no-undo.
  
  define variable hQuery as handle no-undo.
  
  /* get the filter values */
  do with frame fReceivables:
    assign
      std-ha = browse brwReceivables:handle
      cAcctType = cmbReceiptCategory:screen-value
      .
  end.
  /* build the query */
  IF cAcctType <> "ALL"
   THEN tWhereClause = "where refCategory='" + cAcctType + "' ".
   
  hQuery = std-ha:query.
  hQuery:query-close().
  tQueryString = "preselect each arclm no-lock " + tWhereClause +
                 "by arclm.seq descending by arclm.transDate".
  hQuery:query-prepare(tQueryString).
  hQuery:query-open().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterReserves wWin 
PROCEDURE FilterReserves :
/*------------------------------------------------------------------------------
@description Applies the filters the user chose for the payable browse 
------------------------------------------------------------------------------*/
  define variable cAcctType as character no-undo.
  define variable tWhereClause as character no-undo.
  define variable tQueryString as character no-undo.
  
  define variable hQuery as handle no-undo.
  
  /* get the filter values */
  do with frame fReserves:
    assign
      std-ha = browse brwReserves:handle
      cAcctType = cmbReserveAcctType:screen-value
      .
  end.
  /* build the query */
  IF cAcctType <> "ALL"
   THEN tWhereClause = "where refCategory='" + cAcctType + "' ".
   
  hQuery = std-ha:query.
  hQuery:query-close().
  tQueryString = "preselect each claimadjust no-lock " + tWhereClause +
                 "by claimadjust.seq descending by claimadjust.transDate".
  hQuery:query-prepare(tQueryString).
  hQuery:query-open().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData wWin 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
@description Retrieves the data from filedatasrv.p
------------------------------------------------------------------------------*/
  run GetClaimAccounting in hFileDataSrv
                         (output table apclm,
                          output table arclm,
                          output table claimadjust,
                          output table claimacct
                          ).

  {&OPEN-BROWSERS-IN-QUERY-fReserves}
  {&OPEN-BROWSERS-IN-QUERY-fPayables}
  {&OPEN-BROWSERS-IN-QUERY-fReceivables}
  run FilterLedger in this-procedure.
  run SetTab in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCatList as character no-undo.
  define variable cDescription as character no-undo.

  RUN SUPER.
  {&window-name}:max-width-pixels = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.

  /*run GetClaimDescription in hFileDataSrv (pClaimID, output cDescription).*/
  do with frame fMain:
    assign
      /* set the claim ID and description */
      tClaimID:screen-value = string(pClaimID)
      tDescription:screen-value = cDescription
      /* the claim acct browse */
      browse brwClaimAcct:allow-column-searching = no
      .
  end.
  
  /* buttons for main screen */
  {lib/set-button.i &frame-name=fMain        &label="ClaimAcctRefresh" &image="images/sync.bmp"     &inactive="images/sync-i.bmp"}
  {lib/set-button.i &frame-name=fMain        &label="LedgerExport"     &image="images/s-excel.bmp"  &inactive="images/s-excel-i.bmp"}
  /* buttons for reserves */
  {lib/set-button.i &frame-name=fReserves    &label="ReserveNew"       &image="images/s-add.bmp"    &inactive="images/s-add-i.bmp"}
  {lib/set-button.i &frame-name=fReserves    &label="ReserveCopy"      &image="images/s-copy.bmp"   &inactive="images/s-copy-i.bmp"}
  {lib/set-button.i &frame-name=fReserves    &label="ReserveDelete"    &image="images/s-delete.bmp" &inactive="images/s-delete-i.bmp"}
  {lib/set-button.i &frame-name=fReserves    &label="ReserveModify"    &image="images/s-update.bmp" &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=fReserves    &label="ReserveSend"      &image="images/s-email.bmp"  &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=fReserves    &label="ReserveApprove"   &image="images/s-check.bmp"  &inactive="images/s-check-i.bmp"}
  {lib/set-button.i &frame-name=fReserves    &label="ReserveExport"    &image="images/s-excel.bmp"  &inactive="images/s-excel-i.bmp"}
  /* buttons for payables */
  {lib/set-button.i &frame-name=fPayables    &label="PayableNew"       &image="images/s-add.bmp"    &inactive="images/s-add-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableCopy"      &image="images/s-copy.bmp"   &inactive="images/s-copy-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableDelete"    &image="images/s-delete.bmp" &inactive="images/s-delete-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableModify"    &image="images/s-update.bmp" &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableSend"      &image="images/s-email.bmp"  &inactive="images/s-email-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableApprove"   &image="images/s-check.bmp"  &inactive="images/s-check-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableDoc"       &image="images/s-attach.bmp" &inactive="images/s-attach-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableCheck"     &image="images/s-report.bmp" &inactive="images/s-report-i.bmp"}
  {lib/set-button.i &frame-name=fPayables    &label="PayableExport"    &image="images/s-excel.bmp"  &inactive="images/s-excel-i.bmp"}
  /* buttons for receivables */
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableNew"    &image="images/s-add.bmp"    &inactive="images/s-add-i.bmp"}
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableCopy"   &image="images/s-copy.bmp"   &inactive="images/s-copy-i.bmp"}
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableDelete" &image="images/s-delete.bmp" &inactive="images/s-delete-i.bmp"}
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableModify" &image="images/s-update.bmp" &inactive="images/s-update-i.bmp"}
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableDoc"    &image="images/s-attach.bmp" &inactive="images/s-attach-i.bmp"}
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableCheck"  &image="images/s-report.bmp" &inactive="images/s-report-i.bmp"}
  {lib/set-button.i &frame-name=fReceivables &label="ReceivableExport" &image="images/s-excel.bmp"  &inactive="images/s-excel-i.bmp"}
  setButtons().

  /* the page is based on the claim type */
  run SelectPage(1).
   
  /* resize the window */
  run WindowResized in this-procedure.
  
  /* set the browse widgets
  run GetData in this-procedure. */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyPayable wWin 
PROCEDURE ModifyPayable :
/*------------------------------------------------------------------------------
@description Modifies the selected invoice 
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  empty temp-table apinv.
  empty temp-table apinva.
  empty temp-table apinvd.
  run GetClaimPayable in hFileDataSrv
                      (input pClaimID,
                       input pInvoiceID,
                       output table apinv,
                       output table apinva,
                       output table apinvd
                       ).
  
  run dialogclaimpayable.w (input pClaimID,
                            input table apinv,
                            input table apinva,
                            input table apinvd,
                            input hFileDataSrv,
                            input "Modify Claim Payable Invoice",
                            output std-lo
                            ).
              
  if std-lo
   then run GetData in this-procedure.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyReceivable wWin 
PROCEDURE ModifyReceivable :
/*------------------------------------------------------------------------------
@description Modifies the selected receivable
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  
  empty temp-table arinv.
  run GetClaimReceivable in hFileDataSrv
                         (input pClaimID,
                          input pInvoiceID,
                          output table arinv
                          ).
  
  run dialogclaimreceivable.w (input pClaimID,
                               input table arinv,
                               input hFileDataSrv,
                               input "Modify Claim Receivable Invoice",
                               output std-lo
                               ).
              
  if std-lo
   then run GetData in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyReserve wWin 
PROCEDURE ModifyReserve :
/*------------------------------------------------------------------------------
@description Modifies the selected reserve 
------------------------------------------------------------------------------*/
  define buffer claimadjreq for claimadjreq.
  
  empty temp-table claimadjreq.
  run GetClaimReserve in hFileDataSrv
                      (input pClaimID,
                       input claimadjust.refCategory,
                       input claimadjust.transDate,
                       output table claimadjreq
                       ).
  
  do with frame fReserves:
    for first claimadjreq exclusive-lock:
      if not bReserveDelete:sensitive
       then 
        assign
          std-ch = "View Claim Adjustment"
          claimadjreq.isNew = false
          claimadjreq.isView = true
          .
       else 
        assign
          std-ch = "Modify Claim Adjustment"
          claimadjreq.isNew = false
          claimadjreq.isView = false
          .
    end.
  end.
  run dialogreserve.w (input table claimadjreq,
                       input hFileDataSrv,
                       input std-ch,
                       output std-lo
                       ).
  
  if std-lo
   then run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPayable wWin 
PROCEDURE NewPayable :
/*------------------------------------------------------------------------------
@description Create a claim invoice
------------------------------------------------------------------------------*/
  empty temp-table apinv.
  empty temp-table apinva.
  empty temp-table apinvd.
  run dialogclaimpayable.w (input pClaimID,
                            input table apinv,
                            input table apinva,
                            input table apinvd,
                            input hFileDataSrv,
                            input "Create Claim Payable Invoice",
                            output std-lo
                            ).
                             
  run GetData in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewReceivable wWin 
PROCEDURE NewReceivable :
/*------------------------------------------------------------------------------
@description Create a claim receivable 
------------------------------------------------------------------------------*/
  empty temp-table arinv.
  run dialogclaimreceivable.w (input pClaimID,
                               input table arinv,
                               input hFileDataSrv,
                               input "Create Claim Receivable Invoice",
                               output std-lo
                               ).
                             
  run GetData in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewReserve wWin 
PROCEDURE NewReserve :
/*------------------------------------------------------------------------------
@description Creates a new reserve adjustment request to the system  
------------------------------------------------------------------------------*/
  define buffer claimadjreq for claimadjreq.
  
  empty temp-table claimadjreq.
  create claimadjreq.
  assign
    claimadjreq.claimID = pClaimID
    claimadjreq.isNew = true
    claimadjreq.isView = false
    .
  run dialogreserve.w (input table claimadjreq,
                       input hFileDataSrv,
                       input "Create Claim Adjustment",
                       output std-lo
                       ).
                             
  run GetData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenReserveTemplate wWin 
PROCEDURE OpenReserveTemplate :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pUser as character no-undo.
  define input parameter pAmount as decimal no-undo.
  define input parameter pRefCategory as character no-undo.
  define input parameter pDateRequested as datetime no-undo.
  
  define variable cEmail as character no-undo initial "".
  define variable cEmailList as character no-undo initial "".
  define variable cBody as character no-undo.
  
  publish "GetSysUserEmail" (pUser, output cEmail).
  if cEmail > ""
   then cEmailList = addDelimiter(cEmailList,";") + cEmail.
   
  if cEmailList > ""
   then
    do:
      assign
        cBody = "Here are the details:%0D%0D" +
                "Amount:%09%09" + trim(string(pAmount,"$ (>>,>>>,>>9.99)")) + "%0D" +
                "Category:%09%09" + getReserveDesc(pRefCategory) + "%0D" +
                "Requested On:%09%09" + string(pDateRequested,"99/99/99")
        cEmailList = "mailto:" + cEmailList
        cEmailList = cEmailList + "?subject=Pending Adjustment Request Approval for File " + string(pClaimID)
        cEmailList = cEmailList + "&body=" + cBody
        .
      RUN ShellExecuteA in this-procedure (0,
                                           "open",
                                           cEmailList,
                                           "",
                                           "",
                                           1,
                                           OUTPUT std-in).
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SelectPage wWin 
PROCEDURE SelectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER piPageNum AS INTEGER NO-UNDO.

  RUN SUPER( INPUT piPageNum).
  activePageNumber = piPageNum.
  
  run SetTab in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetTab wWin 
PROCEDURE SetTab :
/*------------------------------------------------------------------------------
@description Sets the buttons on each tab to be disabled/enabled depending on the state of the invoice
------------------------------------------------------------------------------*/
  define variable lIsClosed as logical no-undo initial false.
  run IsClaimClosed in hFileDataSrv (pClaimID, output lIsClosed).
  
  if not lIsClosed
   then
    case activePageNumber:
     /* Reserves */
     when 1 then run SetTabReserve in this-procedure.
     /* Payables */
     when 2 then run SetTabPayable in this-procedure.
     /* Receivables */
     when 3 then run SetTabReceivable in this-procedure.
    end case.
   else 
    do:
      enableButtons(false).
      assign
        bLedgerExport:sensitive in frame fMain            = can-find(first claimacct)
        bPayableExport:sensitive in frame fPayables       = can-find(first apclm)
        bReserveExport:sensitive in frame fReserves       = can-find(first claimadjust)
        bReceivableExport:sensitive in frame fReceivables = can-find(first arclm)
        .
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setTabPayable wWin 
PROCEDURE setTabPayable :
/*------------------------------------------------------------------------------
@description Sets the buttons on the payable tab
------------------------------------------------------------------------------*/
  /* buttons for the payable invoices */
  do with frame fPayables:
    /* default all buttons to enabled and all menu options to disabled */
    ASSIGN
      cmbPayableAcctType:list-item-pairs = "ALL,ALL," + GetReserveCategoryList()
      cmbPayableAcctType:screen-value = "ALL"
      bPayableDelete:sensitive = available apclm
      bPayableModify:sensitive = available apclm
      bPayableSend:sensitive = available apclm
      bPayableApprove:sensitive = available apclm
      bPayableDoc:sensitive = available apclm
      bPayableCheck:sensitive = available apclm
      bPayableExport:sensitive = available apclm
      menu-item m_Invoice_View_Document:sensitive in menu menuBrwInvoices = available apclm and apclm.hasDocument
      menu-item m_Invoice_Attach_Document:sensitive in menu menuBrwInvoices = available apclm
      menu-item m_Invoice_Delete_Document:sensitive in menu menuBrwInvoices = available apclm
      menu-item m_Invoice_Modify:sensitive in menu menuBrwInvoices = available apclm
      menu-item m_Reduce_Liability:sensitive in menu menuBrwInvoices = available apclm and apclm.refCategory = "L"
      menu-item m_Reduce_Liability:checked in menu menuBrwInvoices = available apclm and apclm.reduceLiability
      .
    /* default menu labels and images */
    bPayableModify:load-image("images/s-update.bmp").
    bPayableModify:load-image-insensitive("images/s-update-i.bmp").
    bPayableModify:tooltip = "Modify the selected invoice".
    bPayableDoc:load-image("images/s-attach.bmp").
    bPayableDoc:load-image-insensitive("images/s-attach-i.bmp").
    bPayableDoc:tooltip = "Attach document for selected invoice".
    menu-item m_Invoice_Attach_Document:label in menu menuBrwInvoices = "Attach Document".
    menu-item m_Invoice_Modify:label in menu menuBrwInvoices = "Modify Invoice".
  
    if available apclm
     then
      do:
        /* the current user */
        publish "GetCredentialsID" (output std-ch).
        /* disable/enable buttons based on answers */
        assign
          /* notes */
          tPayableNotes:screen-value = apclm.notes
          /* top buttons */
          bPayableDelete:sensitive = (apclm.stat = "O" or apclm.stat = "D")
          bPayableSend:sensitive = (apclm.stat = "O")
          bPayableApprove:sensitive = (apclm.stat = "O")
          .
        
        /* change the attach document button to a view button */
        if apclm.hasDocument
         then
          do:
            bPayableDoc:tooltip = "View document for selected invoice".
            menu-item m_Invoice_Attach_Document:label in menu menuBrwInvoices = "Re-Attach Document".
          end.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetTabReceivable wWin 
PROCEDURE SetTabReceivable :
/*------------------------------------------------------------------------------
@description Sets the buttons on the receivable tab
------------------------------------------------------------------------------*/
  /* buttons for the payable invoices */
  do with frame fReceivables:
    /* default all buttons to enabled and all menu options to disabled */
    /* set the category */
    std-ch = "".
    publish "GetReceivableCategoryList" (true, output std-ch).
    ASSIGN
      cmbReceiptCategory:list-item-pairs = std-ch
      cmbReceiptCategory:screen-value = "ALL"
      bReceivableDelete:sensitive = available arclm
      bReceivableCopy:sensitive = available arclm
      bReceivableModify:sensitive = available arclm
      bReceivableDoc:sensitive = available arclm
      bReceivableCheck:sensitive = available arclm
      bReceivableExport:sensitive = available arclm
      bReceivableDoc:tooltip = "Attach document for selected receipt"
      menu-item m_Receipt_View_Document:sensitive in menu menuBrwReceipts = available arclm and arclm.hasDocument
      menu-item m_Receipt_Attach_Document:sensitive in menu menuBrwReceipts = available arclm and not arclm.hasDocument
      menu-item m_Receipt_Delete_Document:sensitive in menu menuBrwReceipts = available arclm and arclm.hasDocument
      menu-item m_Receipt_Modify:sensitive in menu menuBrwReceipts = available arclm
      .
    /* default menu labels and images */
    menu-item m_Receipt_Attach_Document:label in menu menuBrwReceipts = "Attach Document".
  
    if available arclm
     then
      do:
        /* the current user */
        publish "GetCredentialsID" (output std-ch).
        /* disable/enable buttons based on answers */
        assign
          /* notes */
          tReceivableNotes:screen-value = arclm.notes
          /* top buttons */
          bReceivableDelete:sensitive = (arclm.stat = "O")
          /* menu options */
          menu-item m_Receipt_View_Document:sensitive in menu menuBrwReceipts = arclm.hasDocument
          menu-item m_Receipt_Delete_Document:sensitive in menu menuBrwReceipts = arclm.hasDocument
          .
        
        /* change the attach document button to a view button */
        if arclm.hasDocument
         then
          do:
            bReceivableDoc:tooltip = "View document for selected receipt".
            menu-item m_Receipt_Attach_Document:label in menu menuBrwReceipts = "Re-Attach Document".
          end.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetTabReserve wWin 
PROCEDURE SetTabReserve :
/*------------------------------------------------------------------------------
@description Sets the buttons on the reserve tab
------------------------------------------------------------------------------*/
  /* buttons for the payable invoices */
  do with frame fReserves:
    /* default all buttons to enabled and all menu options to disabled */
    assign
      cmbReserveAcctType:list-item-pairs = "ALL,ALL," + GetReserveCategoryList()
      cmbReserveAcctType:screen-value = "ALL"
      bReserveDelete:sensitive = available claimadjust
      bReserveModify:sensitive = available claimadjust
      bReserveSend:sensitive = available claimadjust
      bReserveApprove:sensitive = available claimadjust
      bReserveExport:sensitive = available claimadjust
      /* menu options 
      menu-item m_Invoice_Modify:sensitive in menu menuBrwInvoices = false
      */
      .
    /* default menu labels and images */
    bReserveModify:load-image("images/s-update.bmp").
    bReserveModify:load-image-insensitive("images/s-update-i.bmp").
    bReserveModify:tooltip = "Modify the selected adjustment request".
  
    if available claimadjust
     then
      do:
        define variable lHasApproved as logical no-undo.
        define variable lIsApproved as logical no-undo.
        define variable lIsApprover as logical no-undo.
       
        /* find out if there is an approver for the reserve */
        run HasClaimReserveApproved in hFileDataSrv (claimadjust.claimID, claimadjust.refCategory, output lHasApproved).
        /* determine if the reserve is approved */
        run IsClaimReserveApproved in hFileDataSrv (claimadjust.claimID, claimadjust.refCategory, claimadjust.transDate, output lIsApproved).
        /* determine if the current user is the approver or not */
        run IsClaimReserveApprover in hFileDataSrv (claimadjust.claimID, claimadjust.refCategory, output lIsApprover).
        /* disable/enable buttons based on answers */
        assign
          /* top buttons */
          bReserveDelete:sensitive = not lIsApproved
          bReserveSend:sensitive = not lIsApproved
          bReserveApprove:sensitive = not lIsApproved
          /* menu options 
          menu-item m_Invoice_Modify:sensitive in menu menuBrwInvoices = true
          */
          .
          
        /* if the reserve is approved, change the modify reserve button to view only */
        if lIsApproved
         then
          do:
            bReserveModify:load-image("images/s-magnifier.bmp").
            bReserveModify:load-image-insensitive("images/s-magnifier-i.bmp").
            bReserveModify:tooltip = "View the selected reserve adjustment request".
          end.
          
        /* as per Noemi, disable reserve buttons for a Matter file type */
        std-ch = "".
        run GetClaimType in hFileDataSrv (pClaimID, output std-ch).
        if std-ch = "M"
         then
          assign
            bReserveNew:sensitive = false
            bReserveCopy:sensitive = false
            bReserveModify:sensitive = lIsApproved
            bReserveDelete:sensitive = false
            bReserveSend:sensitive = false
            bReserveApprove:sensitive = false
            .
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData wWin 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
@description Sorts the data in the browse widget by the column selected   
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewPayableCheckRequest wWin 
PROCEDURE ViewPayableCheckRequest :
/*------------------------------------------------------------------------------
@description Creates a check request for the payable  
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  run ViewPayableCheckRequest in hFileDataSrv (pInvoiceID).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewPayableDocument wWin 
PROCEDURE ViewPayableDocument :
/*------------------------------------------------------------------------------
@description Views an invoice document from ShareFile  
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  run ViewPayableDocument in hFileDataSrv (pInvoiceID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewReceivableCheckRequest wWin 
PROCEDURE ViewReceivableCheckRequest :
/*------------------------------------------------------------------------------
@description Creates a check request for the payable  
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  run ViewReceivableCheckRequest in hFileDataSrv (pInvoiceID).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewReceivableDocument wWin 
PROCEDURE ViewReceivableDocument :
/*------------------------------------------------------------------------------
@description Views an invoice document from ShareFile  
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  run ViewReceivableDocument in hFileDataSrv (pInvoiceID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized wWin 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = frame {&frame-name}:width-pixels.

  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = frame {&frame-name}:height-pixels.

/*   run resizeObject in h_folder (frame {&frame-name}:height - 2,  */
/*                                 frame {&frame-name}:width - 2).  */
  /* The resize causes the tab label to be off-set, so... */
  run showCurrentPage in h_folder(activePageNumber).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetReserveCategoryList wWin 
FUNCTION GetReserveCategoryList RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Gets a list of reserve types minus Unknown
------------------------------------------------------------------------------*/
  define variable cCategoryList as character no-undo.
  define variable pReserveList as character no-undo.
  define variable iCount as integer no-undo.
  
  publish "GetCategoryList" (output cCategoryList).
  pReserveList = "".
  do iCount = 1 to num-entries(cCategoryList) - 2:
    pReserveList = addDelimiter(pReserveList,",") + entry(iCount,cCategoryList).
  end.
  
  RETURN pReserveList.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

