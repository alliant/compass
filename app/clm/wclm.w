&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wclm.w
   main Window for the CLaim Management client application
   created 11.30.2014 D.Sinclair
 */

CREATE WIDGET-POOL.


{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{tt/claim.i}
{tt/list.i}
{tt/listentity.i}
{tt/listfield.i}
{tt/listfilter.i}
{lib/brw-multi-def.i}

&global-define ALTARISK "ClaimAltaRisk"
&global-define ALTARESPONSIBILITY "ClaimAltaResp"
&global-define INTERNALCAUSE "ClaimCause"
&global-define INTERNALDESCRIPTION "ClaimDescription"

def var hQuickList as handle no-undo.

/* This temp-table holds the instances of wfile.w so we don't duplicate and know 
   if a window is busy (ie, the app shouldn't close)
 */
define temp-table openWindow
 field claimID as int
 field procFile as character
 field procHandle as handle
 field isBusy as logical
 .
 
define variable dFilterWidth as decimal no-undo.
define variable dListColumn as decimal no-undo.
define variable dEntityColumn as decimal no-undo.
define variable dEntityLabelColumn as decimal no-undo.
define variable dEntityRect as decimal no-undo.

/* Handles to the reference windows */
def var hPeriodWindow as handle no-undo.
def var hAgentWindow as handle no-undo.
def var hVendorWindow as handle no-undo.
def var hTemplateWindow as handle no-undo.
def var hResearchWindow as handle no-undo.

/* Pop-up menu */
define menu popmenu title "Actions"
 menu-item m_Clear label "Clear Results"
 menu-item m_History label "Clear History"
 menu-item m_Export label "Export Results to Excel" disabled
 rule 
 menu-item m_Open label "Open Selected" disabled
 menu-item m_Add label "New File"
 .

ON 'choose':U OF menu-item m_Clear in menu popmenu
DO:
 run ActionClear in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_History in menu popmenu
DO:
 run ActionHistory in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_Open in menu popmenu
DO:
 run ActionOpen in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_Export in menu popmenu
DO:
 run ActionExport in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_Add in menu popmenu
DO:
 run ActionAdd in this-procedure.
 RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES claim list

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData claim.claimID claim.insuredName claim.statDesc claim.typeDesc claim.stageDesc claim.actionDesc claim.assignedToName claim.stateID claim.displayPropAddr claim.displayAgentName /* claim.lastActivity */ /* claim.description */ /* claim.claimCode */ /* claim.borrower */ /* claim.propertyAddress */   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH claim
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH claim.
&Scoped-define TABLES-IN-QUERY-brwData claim
&Scoped-define FIRST-TABLE-IN-QUERY-brwData claim


/* Definitions for BROWSE brwList                                       */
&Scoped-define FIELDS-IN-QUERY-brwList list.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwList   
&Scoped-define SELF-NAME brwList
&Scoped-define QUERY-STRING-brwList FOR EACH list by list.displayName
&Scoped-define OPEN-QUERY-brwList OPEN QUERY {&SELF-NAME} FOR EACH list by list.displayName.
&Scoped-define TABLES-IN-QUERY-brwList list
&Scoped-define FIRST-TABLE-IN-QUERY-brwList list


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwList}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fSearch brwData bNewList cmbEntity ~
chkAutoRun brwList bClear fStatus fAssignedTo bNew bSearch rLists 
&Scoped-Define DISPLAYED-OBJECTS fSearch cmbEntity chkAutoRun tEntityLabel ~
fStatus fAssignedTo cmbEntityLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showOpenWindow C-Win 
FUNCTION showOpenWindow RETURNS LOGICAL PRIVATE
  ( pClaimID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Configure    LABEL "Configure..."  
       RULE
       MENU-ITEM m_About        LABEL "About..."      
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_Action 
       MENU-ITEM m_Do_Search    LABEL "Do Search"     
       MENU-ITEM m_Clear_Search LABEL "Clear Results" 
       MENU-ITEM m_Clear_Search_History LABEL "Clear History" 
       MENU-ITEM m_Export_Search_Results LABEL "Export Results to Excel"
              DISABLED
       RULE
       MENU-ITEM m_Open_Selected_File LABEL "Open File"     
              DISABLED
       MENU-ITEM m_Add_New_File LABEL "New File"      .

DEFINE SUB-MENU m_References 
       MENU-ITEM m_Cause_Codes  LABEL "Cause Codes"   
       MENU-ITEM m_Description_Codes LABEL "Description Codes"
       RULE
       MENU-ITEM m_ALTA_Risk_Codes LABEL "ALTA Risk Codes"
       MENU-ITEM m_ALTA_Responsibility_Codes LABEL "ALTA Responsibility Codes"
       RULE
       MENU-ITEM m_Agents       LABEL "Agents"        
       MENU-ITEM m_Accounting_Periods LABEL "Payable Periods"
       MENU-ITEM m_Vendors      LABEL "Vendors"       
       RULE
       MENU-ITEM m_Form_Templates LABEL "Form Templates"
       MENU-ITEM m_Research     LABEL "Research"      .

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_Claim_Summary LABEL "Claim Summary" 
       MENU-ITEM m_Mgmt_Summary LABEL "Management Summary"
       MENU-ITEM m_Reserves_Worksheet LABEL "Reserves Worksheet"
       MENU-ITEM m_Significant_Claims LABEL "Significant Claims"
       MENU-ITEM m_Dormant_Open_Files LABEL "Dormant Open Files"
       MENU-ITEM m_Insufficient_Reserve_Balances LABEL "Insufficient Reserve Balances"
       MENU-ITEM m_Claims_Load  LABEL "Claims Load"   
       MENU-ITEM m_Wells_Fargo_Report LABEL "Lender Report" 
       RULE
       MENU-ITEM m_Report_Claim_Costs_Incurred LABEL "Claim Costs Incurred"
       MENU-ITEM m_Report_Claims_Ratio LABEL "Claims Ratio"  
       RULE
       MENU-ITEM m_Unapproved_Invoices LABEL "Open (Unapproved) Payables"
       MENU-ITEM m_Unapproved_Reserve_Adjustme LABEL "Open (Unapproved) Reserve Adjustments"
       RULE
       MENU-ITEM m_VendorPayee_Payables LABEL "Vendor/Payee Payables".

DEFINE SUB-MENU m_QL 
       MENU-ITEM m_Open_Selected_QL LABEL "Open"          
              DISABLED
       MENU-ITEM m_Modify_Selected_QL LABEL "Modify"        
              DISABLED
       MENU-ITEM m_Delete_Selected_QL LABEL "Delete"        
              DISABLED
       MENU-ITEM m_Share_Selected_QL LABEL "Share"         
              DISABLED
       RULE
       MENU-ITEM m_Add_New_QL   LABEL "New"           .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_Action       LABEL "Action"        
       SUB-MENU  m_References   LABEL "References"    
       SUB-MENU  m_Reports      LABEL "Reports"       
       SUB-MENU  m_QL           LABEL "Quick Lists"   .

DEFINE MENU POPUP-MENU-brwList 
       MENU-ITEM m_List_Open    LABEL "Open"          
              DISABLED
       MENU-ITEM m_List_Modify  LABEL "Modify"        
              DISABLED
       MENU-ITEM m_List_Delete  LABEL "Delete"        
              DISABLED
       MENU-ITEM m_List_Share   LABEL "Share"         
              DISABLED
       RULE
       MENU-ITEM m_List_New     LABEL "New"           .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear the search results".

DEFINE BUTTON bDeleteList  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected Quick List".

DEFINE BUTTON bModifyList  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected Quick List".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new file".

DEFINE BUTTON bNewList  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new Quick List".

DEFINE BUTTON bOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected file".

DEFINE BUTTON bOpenList  NO-FOCUS
     LABEL "Run" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected Quick List".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search for files based on entered terms".

DEFINE BUTTON bShareList  NO-FOCUS
     LABEL "Share" 
     SIZE 7.2 BY 1.71 TOOLTIP "Share the selected Quick List".

DEFINE VARIABLE cmbEntity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 26.2 BY 1 NO-UNDO.

DEFINE VARIABLE fAssignedTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "All","A"
     DROP-DOWN-LIST
     SIZE 25.6 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN AUTO-COMPLETION
     SIZE 63 BY 1 TOOLTIP "Enter criteria (blank=Open; C=Claims; M=Matters; X=Closed; 1-9 weeks dormant)" NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "All","A",
                     "Open","O",
                     "Closed","C"
     DROP-DOWN-LIST
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE cmbEntityLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Quick Lists" 
      VIEW-AS TEXT 
     SIZE 10.6 BY .62 NO-UNDO.

DEFINE VARIABLE tEntityLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Entity:" 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16.2 BY 2.38.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 81 BY 2.38.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 90.2 BY 2.38.

DEFINE RECTANGLE rLists
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38.2 BY 4.38.

DEFINE VARIABLE chkAutoRun AS LOGICAL INITIAL yes 
     LABEL "Auto Run on Modify" 
     VIEW-AS TOGGLE-BOX
     SIZE 23.2 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      claim SCROLLING.

DEFINE QUERY brwList FOR 
      list SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      claim.claimID column-label "File Number" format "zzzzzzzz"
claim.insuredName column-label "Insured Name" format "x(60)" width 30
claim.statDesc column-label "Status" format "x(7)"
claim.typeDesc column-label "Type" format "x(7)"
claim.stageDesc column-label "Stage" format "x(14)"
claim.actionDesc column-label "Action" format "x(14)"
claim.assignedToName column-label "Assigned To" format "x(30)" width 18
claim.stateID column-label "ST" format "x(4)"
claim.displayPropAddr column-label "Property Address" format "x(120)" width 40
claim.displayAgentName column-label "Agent" format "x(120)" width 40
/* claim.lastActivity column-label "Last Activity" format "99/99/9999" */
/* claim.description column-label "Description" format "x(120)" */
/* claim.claimCode column-label "Claim!Code" format "x(10)"                */
/* claim.borrower label "Borrower" format "x(250)" width 25                */
/* claim.propertyAddress label "Property Address" format "x(250)" width 40 */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 189 BY 16.86
         TITLE "Search Results" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwList C-Win _FREEFORM
  QUERY brwList DISPLAY
      list.displayName column-label "Name" format "x(100)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 38.2 BY 14.86
         TITLE "Quick Lists" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Right-click for actions".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fSearch AT ROW 2.29 COL 20 NO-LABEL WIDGET-ID 46
     brwData AT ROW 4 COL 2 WIDGET-ID 300
     bNewList AT ROW 3.91 COL 192.6 WIDGET-ID 44 NO-TAB-STOP 
     cmbEntity AT ROW 1.95 COL 199.8 COLON-ALIGNED NO-LABEL WIDGET-ID 260
     bOpen AT ROW 1.91 COL 10.2 WIDGET-ID 8 NO-TAB-STOP 
     chkAutoRun AT ROW 3.05 COL 201.8 WIDGET-ID 288
     brwList AT ROW 6 COL 192 WIDGET-ID 400
     tEntityLabel AT ROW 2.14 COL 193.4 COLON-ALIGNED NO-LABEL WIDGET-ID 286
     bClear AT ROW 1.86 COL 91.8 WIDGET-ID 284 NO-TAB-STOP 
     fStatus AT ROW 2.29 COL 108.4 COLON-ALIGNED WIDGET-ID 54 NO-TAB-STOP 
     bDeleteList AT ROW 3.91 COL 207.4 WIDGET-ID 278 NO-TAB-STOP 
     fAssignedTo AT ROW 2.29 COL 136.8 COLON-ALIGNED WIDGET-ID 258 NO-TAB-STOP 
     bModifyList AT ROW 3.91 COL 200 WIDGET-ID 276 NO-TAB-STOP 
     bOpenList AT ROW 3.91 COL 222.2 WIDGET-ID 282 NO-TAB-STOP 
     bShareList AT ROW 3.91 COL 214.8 WIDGET-ID 280 NO-TAB-STOP 
     cmbEntityLabel AT ROW 1.19 COL 191.4 COLON-ALIGNED NO-LABEL WIDGET-ID 270
     bNew AT ROW 1.91 COL 2.8 WIDGET-ID 48 NO-TAB-STOP 
     bSearch AT ROW 1.86 COL 84.4 WIDGET-ID 42 NO-TAB-STOP 
     "Filter Results" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.19 COL 102 WIDGET-ID 56
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.19 COL 20 WIDGET-ID 58
     "Actions" VIEW-AS TEXT
          SIZE 7.2 BY .62 AT ROW 1.19 COL 3 WIDGET-ID 272
     RECT-35 AT ROW 1.43 COL 2 WIDGET-ID 50
     RECT-36 AT ROW 1.43 COL 19 WIDGET-ID 52
     rFilter AT ROW 1.43 COL 101 WIDGET-ID 60
     rLists AT ROW 1.43 COL 192 WIDGET-ID 266
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 230.2 BY 20.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 20.1
         WIDTH              = 230.2
         MAX-HEIGHT         = 23.76
         MAX-WIDTH          = 230.8
         VIRTUAL-HEIGHT     = 23.76
         VIRTUAL-WIDTH      = 230.8
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fMain */
/* BROWSE-TAB brwList chkAutoRun fMain */
/* SETTINGS FOR BUTTON bDeleteList IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModifyList IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpen IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOpenList IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       brwList:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwList:HANDLE.

/* SETTINGS FOR BUTTON bShareList IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN cmbEntityLabel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fSearch IN FRAME fMain
   ALIGN-L                                                              */
/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rFilter IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tEntityLabel IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claim.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwList
/* Query rebuild information for BROWSE brwList
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH list by list.displayName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwList */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
/* das 12.30.2016: This gets complicated as we have fields enabled by default

  std-lo = showOpenWindow(0). /* Any busy open file */
  if std-lo 
   then
    do:
         MESSAGE "Please complete the action in the open file."
           VIEW-AS ALERT-BOX INFO BUTTONS OK.
         return no-apply.
    end.
 */

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  publish "ExitApplication".
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  run ActionClear in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteList C-Win
ON CHOOSE OF bDeleteList IN FRAME fMain /* Delete */
DO:
 run DeleteSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyList C-Win
ON CHOOSE OF bModifyList IN FRAME fMain /* Modify */
DO:
  run OpenSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  run ActionNew in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewList C-Win
ON CHOOSE OF bNewList IN FRAME fMain /* New */
DO:
  run NewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpen C-Win
ON CHOOSE OF bOpen IN FRAME fMain /* Open */
DO:
  run ActionOpen in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpenList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpenList C-Win
ON CHOOSE OF bOpenList IN FRAME fMain /* Run */
DO:
  run ViewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain /* Search Results */
DO:
  run ActionOpen in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain /* Search Results */
DO:
 {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain /* Search Results */
DO:
{lib/brw-startSearch-multi.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain /* Search Results */
DO:
  bOpen:sensitive in frame {&frame-name} = avail claim.
  menu-item m_Open:sensitive in menu popmenu = avail claim.
  menu-item m_Export:sensitive in menu popmenu = avail claim.

  menu-item m_Export_Search_Results:sensitive in menu m_Action = avail claim.
  menu-item m_Open_Selected_File:sensitive in menu m_Action = avail claim.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwList
&Scoped-define SELF-NAME brwList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON DEFAULT-ACTION OF brwList IN FRAME fMain /* Quick Lists */
DO:
  run ViewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON ROW-DISPLAY OF brwList IN FRAME fMain /* Quick Lists */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON START-SEARCH OF brwList IN FRAME fMain /* Quick Lists */
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON VALUE-CHANGED OF brwList IN FRAME fMain /* Quick Lists */
DO:
  
  /* when the row is highlighted, display the name in the tooltip */
  if available list
   then
    do:
      /* determine if the column is big enough for the value */
      assign
        std-ha = getColumn(self:handle, "displayName")
        std-lo = (valid-handle(std-ha) and std-ha:width-chars < length(std-ha:screen-value))
        self:tooltip = (if std-lo then std-ha:screen-value else "")
        .
    end.

  std-lo = available list.
  assign
    menu-item m_List_Open:sensitive in menu POPUP-MENU-brwList = std-lo
    menu-item m_List_Modify:sensitive in menu POPUP-MENU-brwList = std-lo
    menu-item m_List_Delete:sensitive in menu POPUP-MENU-brwList = std-lo
    menu-item m_List_Share:sensitive in menu POPUP-MENU-brwList = std-lo
    bModifyList:sensitive in frame {&frame-name} = std-lo
    bOpenList:sensitive in frame {&frame-name} = std-lo
    bDeleteList:sensitive in frame {&frame-name} = std-lo
    bShareList:sensitive in frame {&frame-name} = std-lo
    menu-item m_Open_Selected_QL:sensitive in menu m_QL = std-lo
    menu-item m_Modify_Selected_QL:sensitive in menu m_QL = std-lo
    menu-item m_Delete_Selected_QL:sensitive in menu m_QL = std-lo
    menu-item m_Share_Selected_QL:sensitive in menu m_QL = std-lo
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  run ActionSearch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bShareList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bShareList C-Win
ON CHOOSE OF bShareList IN FRAME fMain /* Share */
DO:
  run ShareSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbEntity C-Win
ON VALUE-CHANGED OF cmbEntity IN FRAME fMain
DO:
  run GetSavedLists in this-procedure (self:screen-value).
  apply "VALUE-CHANGED" to brwList.
  publish "SetCurrentValue" ("Entity", self:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAssignedTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAssignedTo C-Win
ON VALUE-CHANGED OF fAssignedTo IN FRAME fMain /* Assigned To */
DO:
  run filterData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain
DO:
  run ActionSearch in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus C-Win
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  run filterData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Accounting_Periods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Accounting_Periods C-Win
ON CHOOSE OF MENU-ITEM m_Accounting_Periods /* Payable Periods */
DO:
  if valid-handle(hPeriodWindow) 
   then
    do: run ShowWindow in hPeriodWindow no-error.
        if not error-status:error 
         then return.
    end.

  run referenceperiod.w persistent set hPeriodWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_New_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_New_File C-Win
ON CHOOSE OF MENU-ITEM m_Add_New_File /* New File */
DO:
  apply "CHOOSE" to bNew in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_New_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_New_QL C-Win
ON CHOOSE OF MENU-ITEM m_Add_New_QL /* New */
DO:
  apply "CHOOSE" to bNewList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents C-Win
ON CHOOSE OF MENU-ITEM m_Agents /* Agents */
DO:
  if valid-handle(hAgentWindow) 
   then
    do: run ShowWindow in hAgentWindow no-error.
        if not error-status:error 
         then return.
    end.

  run referenceagent.w persistent set hAgentWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ALTA_Responsibility_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ALTA_Responsibility_Codes C-Win
ON CHOOSE OF MENU-ITEM m_ALTA_Responsibility_Codes /* ALTA Responsibility Codes */
DO:
  run wcodes.w persistent ({&ALTARESPONSIBILITY}, "ALTA Responsibility Codes").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ALTA_Risk_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ALTA_Risk_Codes C-Win
ON CHOOSE OF MENU-ITEM m_ALTA_Risk_Codes /* ALTA Risk Codes */
DO:
  run wcodes.w persistent ({&ALTARISK}, "ALTA Risk Codes").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Cause_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Cause_Codes C-Win
ON CHOOSE OF MENU-ITEM m_Cause_Codes /* Cause Codes */
DO:
  run wcodes.w persistent ({&INTERNALCAUSE}, "Cause Codes").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Claims_Load
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Claims_Load C-Win
ON CHOOSE OF MENU-ITEM m_Claims_Load /* Claims Load */
DO:
  run wclm07-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Claim_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Claim_Summary C-Win
ON CHOOSE OF MENU-ITEM m_Claim_Summary /* Claim Summary */
DO:
  run wclm08-r.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Clear_Search
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Clear_Search C-Win
ON CHOOSE OF MENU-ITEM m_Clear_Search /* Clear Results */
DO:
  apply "CHOOSE" to bClear in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Clear_Search_History
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Clear_Search_History C-Win
ON CHOOSE OF MENU-ITEM m_Clear_Search_History /* Clear History */
DO:
 run ActionHistory in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure... */
DO:
  run dialogclmconfig.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Selected_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Selected_QL C-Win
ON CHOOSE OF MENU-ITEM m_Delete_Selected_QL /* Delete */
DO:
  apply "CHOOSE" to bDeleteList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Description_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Description_Codes C-Win
ON CHOOSE OF MENU-ITEM m_Description_Codes /* Description Codes */
DO:
  run wcodes.w persistent ({&INTERNALDESCRIPTION}, "Description Codes").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Dormant_Open_Files
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Dormant_Open_Files C-Win
ON CHOOSE OF MENU-ITEM m_Dormant_Open_Files /* Dormant Open Files */
DO:
  run wclm02-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Do_Search
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Do_Search C-Win
ON CHOOSE OF MENU-ITEM m_Do_Search /* Do Search */
DO:
  apply "CHOOSE" to bSearch in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export_Search_Results
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export_Search_Results C-Win
ON CHOOSE OF MENU-ITEM m_Export_Search_Results /* Export Results to Excel */
DO:
  run ActionExport in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Form_Templates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Form_Templates C-Win
ON CHOOSE OF MENU-ITEM m_Form_Templates /* Form Templates */
DO:
  if valid-handle(hTemplateWindow) 
   then
    do: run ShowWindow in hTemplateWindow no-error.
        if not error-status:error 
         then return.
    end.


  run documents.w persistent set hTemplateWindow
    ("CLM",
     "/claims/templates",
     "Claims Templates",
     "new,delete,open,modify,share,send,request",   /* permissions */
     "claims",
     "templates",
     "" /* Seq */) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Insufficient_Reserve_Balances
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Insufficient_Reserve_Balances C-Win
ON CHOOSE OF MENU-ITEM m_Insufficient_Reserve_Balances /* Insufficient Reserve Balances */
DO:
  run wclm03-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Delete C-Win
ON CHOOSE OF MENU-ITEM m_List_Delete /* Delete */
DO:
  apply "CHOOSE" to bDeleteList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Modify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Modify C-Win
ON CHOOSE OF MENU-ITEM m_List_Modify /* Modify */
DO:
  apply "CHOOSE" to bModifyList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_New C-Win
ON CHOOSE OF MENU-ITEM m_List_New /* New */
DO:
  apply "CHOOSE" to bNewList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Open C-Win
ON CHOOSE OF MENU-ITEM m_List_Open /* Open */
DO:
  apply "CHOOSE" to bOpenList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_List_Share
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_List_Share C-Win
ON CHOOSE OF MENU-ITEM m_List_Share /* Share */
DO:
  apply "CHOOSE" to bShareList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Mgmt_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Mgmt_Summary C-Win
ON CHOOSE OF MENU-ITEM m_Mgmt_Summary /* Management Summary */
DO:
  run wclm01-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Modify_Selected_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Modify_Selected_QL C-Win
ON CHOOSE OF MENU-ITEM m_Modify_Selected_QL /* Modify */
DO:
  apply "CHOOSE" to bModifyList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open_Selected_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open_Selected_File C-Win
ON CHOOSE OF MENU-ITEM m_Open_Selected_File /* Open File */
DO:
  apply "CHOOSE" to bOpen in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open_Selected_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open_Selected_QL C-Win
ON CHOOSE OF MENU-ITEM m_Open_Selected_QL /* Open */
DO:
  apply "CHOOSE" to bOpenList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Claims_Ratio
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Claims_Ratio C-Win
ON CHOOSE OF MENU-ITEM m_Report_Claims_Ratio /* Claims Ratio */
DO:
  run rpt/claimsratio.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Report_Claim_Costs_Incurred
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Report_Claim_Costs_Incurred C-Win
ON CHOOSE OF MENU-ITEM m_Report_Claim_Costs_Incurred /* Claim Costs Incurred */
DO:
  run rpt/claimscostincurred.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Research
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Research C-Win
ON CHOOSE OF MENU-ITEM m_Research /* Research */
DO:
  if valid-handle(hResearchWindow) 
   then
    do: run ShowWindow in hResearchWindow no-error.
        if not error-status:error 
         then return.
    end.


  run documents.w persistent set hResearchWindow
    ("CLM",
     "/claims/research",
     "Claims Research",
     "new,delete,open,modify,share,send,request",   /* permissions */
     "claims",
     "research",
     "" /* Seq */) no-error.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reserves_Worksheet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reserves_Worksheet C-Win
ON CHOOSE OF MENU-ITEM m_Reserves_Worksheet /* Reserves Worksheet */
DO:
  run wclm09-r.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Share_Selected_QL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Share_Selected_QL C-Win
ON CHOOSE OF MENU-ITEM m_Share_Selected_QL /* Share */
DO:
  apply "CHOOSE" to bShareList in frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Significant_Claims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Significant_Claims C-Win
ON CHOOSE OF MENU-ITEM m_Significant_Claims /* Significant Claims */
DO:
  run wclm10-r.w persistent.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unapproved_Invoices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unapproved_Invoices C-Win
ON CHOOSE OF MENU-ITEM m_Unapproved_Invoices /* Open (Unapproved) Payables */
DO:
  run wclm04-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unapproved_Reserve_Adjustme
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unapproved_Reserve_Adjustme C-Win
ON CHOOSE OF MENU-ITEM m_Unapproved_Reserve_Adjustme /* Open (Unapproved) Reserve Adjustments */
DO:
  run wclm06-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_VendorPayee_Payables
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_VendorPayee_Payables C-Win
ON CHOOSE OF MENU-ITEM m_VendorPayee_Payables /* Vendor/Payee Payables */
DO:
  run wclm05-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Vendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Vendors C-Win
ON CHOOSE OF MENU-ITEM m_Vendors /* Vendors */
DO:
  if valid-handle(hVendorWindow) 
   then
    do: run ShowWindow in hVendorWindow no-error.
        if not error-status:error 
         then return.
    end.

  run sys/wvendor.w persistent set hVendorWindow.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Wells_Fargo_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Wells_Fargo_Report C-Win
ON CHOOSE OF MENU-ITEM m_Wells_Fargo_Report /* Lender Report */
DO:
  run wclm11-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "ClaimSelected" anywhere.
subscribe to "VendorSelected" anywhere.
subscribe to "AgentSelected" anywhere.
subscribe to "ActionWindowForAgent" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/brw-main-multi.i &browse-list="brwData,brwList"}
{lib/win-main.i}
{lib/win-status.i &entity="'file'"}
{lib/set-filters.i &tableName="'claim'" &labelName="'AssignedTo,Status'" &columnName="'assignedTo,stat'"}

{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.


bSearch:load-image("images/find.bmp").
bClear:load-image("images/erase.bmp").
bOpen:load-image("images/open.bmp").
bOpen:load-image-insensitive("images/open-i.bmp").
bNew:load-image("images/new.bmp").

bOpenList:load-image-up("images/import.bmp").
bOpenList:load-image-insensitive("images/import-i.bmp").
bNewList:load-image-up("images/new.bmp").
bNewList:load-image-insensitive("images/new-i.bmp").
bModifyList:load-image-up("images/update.bmp").
bModifyList:load-image-insensitive("images/update-i.bmp").
bDeleteList:load-image-up("images/delete.bmp").
bDeleteList:load-image-insensitive("images/delete-i.bmp").
bShareList:load-image-up("images/users.bmp").
bShareList:load-image-insensitive("images/users-i.bmp").

clearStatus().

browse brwData:POPUP-MENU = MENU popmenu:HANDLE.

{lib/get-sysprop-list.i &combo=fStatus &appCode="'CLM'" &objAction="'ClaimDescription'" &objProperty="'Status'" &addAll=true}
{lib/get-claimuser-list.i &combo=fAssignedTo}
  
std-ch = ?.
publish "GetSearchHistory" (output std-ch).
fSearch:list-items = std-ch.

apply "VALUE-CHANGED" TO brwList. /* Set the inital button/menu state */

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  /* set the entity list */
  publish "GetEntities" (output table listentity,output std-lo).
  if std-lo
   then
    do:
      cmbEntity:delete(1).
      for each listentity no-lock:
        cmbEntity:add-last(listentity.displayName,listentity.entityName).
      end.
      if lookup("claim",cmbEntity:list-item-pairs) > 0
       then cmbEntity:screen-value = "claim".
       else cmbEntity:screen-value = entry(2,cmbEntity:list-item-pairs).
      apply "VALUE-CHANGED":u to cmbEntity.
    end.
   else
    do:
      cmbEntity:delete(1).
      cmbEntity:add-last("ALL","A").
      cmbEntity:screen-value = "A".
    end.
  /* set the original column and width */
  assign
    dFilterWidth = rFilter:width-pixels
    dListColumn = browse brwList:x
    dEntityColumn = cmbEntity:x
    dEntityLabelColumn = cmbEntityLabel:x
    dEntityRect = rLists:x
    .
    
  /* auto open the last file if the user wishes it */
  publish "GetAutoViewRecent" (output std-lo).
  publish "GetLastFile" (output std-ch).
  if std-lo and std-ch <> ? and std-ch <> "" and std-ch <> "0"
   then run ClaimSelected in this-procedure (std-ch).
  
  run windowResized in this-procedure.
  
  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionClear C-Win 
PROCEDURE ActionClear PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query brwData.
 empty temp-table claim.  
 status input " ".
 status default " ".
 apply "VALUE-CHANGED" to brwData in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionExport C-Win 
PROCEDURE ActionExport PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 &scoped-define ReportName "ClaimSearchResults"

 std-ch = "C".
 publish "GetExportType" (output std-ch).
 if std-ch = "X" 
  then run util/exporttoexcelbrowse.p (string(browse brwData:handle), {&ReportName}).
  else run util/exporttocsvbrowse.p (string(browse brwData:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionHistory C-Win 
PROCEDURE ActionHistory PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 fSearch:list-items in frame {&frame-name} = "".
 publish "SetSearchHistory" (fSearch:list-items).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew C-Win 
PROCEDURE ActionNew PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 def var tClaimID as int.
 def var tSeq as int.         /* claimcoverage sequence number */

 def var tDateRcvd as datetime.
 def var tAssignedTo as char.
 def var tStateID as char.
 def var tAgentID as char.
 def var tInsuredName as char.
 def var tCoverageID as char.
 def var tEffDate as datetime.
 def var tCoverageType as char.
 def var tInsuredType as char.
 def var tSummary as char.
 def var tPropAddr1 as char.
 def var tPropAddr2 as char.
 def var tPropCity as char.
 def var tPropState as char.
 def var tPropCounty as char.
 def var tPropZipCode as char.
 def var tPropSubdivision as char.
 def var tPropResidential as log.
 def var tPropLegalDesc as char.
 
 def var tLiabilityAmount as dec.
 def var tFileNumber as char.

 def buffer x-claim for claim.
 
          
 publish "GetCredentialsID" (output tAssignedTo).

 run dialognewclaim.w (input-output tDateRcvd,
                       input-output tAssignedTo,
                       input-output tStateID,
                       input-output tAgentID,
                       input-output tInsuredName,
                       input-output tCoverageID,
                       input-output tEffDate,
                       input-output tCoverageType,
                       input-output tInsuredType,
                       input-output tSummary,
                       input-output tPropAddr1,
                       input-output tPropAddr2,
                       input-output tPropCity,
                       input-output tPropState,
                       input-output tPropCounty,
                       input-output tPropZipCode,
                       input-output tPropSubdivision,
                       input-output tPropResidential,
                       input-output tPropLegalDesc,
                       output tLiabilityAmount,
                       output tFileNumber,
                       output pError).
 if pError 
  then return.
  
 publish "NewClaim"
    (input tDateRcvd,
     input tAssignedTo,
     input tStateID,
     input tAgentID,
     input tInsuredName,
     input tCoverageID,
     input tEffDate,
     input tCoverageType,
     input tInsuredType,
     input tSummary,
     input tPropAddr1,
     input tPropAddr2,
     input tPropCity,
     input tPropState,
     input tPropCounty,
     input tPropZipCode,
     input tPropSubdivision,
     input tPropResidential,
     input tPropLegalDesc,
     input tLiabilityAmount,
     input tFileNumber,
     output tClaimID,
     output std-lo,
     output std-ch).
  if not std-lo 
   then
    do: 
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.

/*  std-lo = showOpenWindow(false, tClaimID). */
/*  if std-lo = true                          */
/*   then return.                             */

 run ClaimSelected in this-procedure (string(tClaimID)).

/*  run wfileview.w persistent set std-ha (tClaimID).  */
/*  run "InitializeObject" in std-ha.                  */
/*                                                     */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionOpen C-Win 
PROCEDURE ActionOpen PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available claim 
   then return.
 
  run ClaimSelected in this-procedure (string(claim.claimid)).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRecent C-Win 
PROCEDURE ActionRecent PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSearch C-Win 
PROCEDURE ActionSearch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tStartTime as datetime no-undo.
def var tClaimID as char no-undo.

clearStatus().
fStatus:screen-value in frame {&frame-name} = "ALL".
fAssignedTo:screen-value in frame {&frame-name} = "ALL".

/* Remove the list item delimiter */
fSearch:screen-value in frame {&frame-name} = trim(replace(replace(fSearch:screen-value, fSearch:delimiter, " "), "  ", " ")).

/* if fSearch:screen-value = ""   */
/*  or fSearch:screen-value = "?" */
/* then return.                   */

std-ch = fSearch:screen-value.
std-in = lookup(fSearch:screen-value, fSearch:list-items, fSearch:delimiter).
if (std-in = 0 or std-in = ?) and fsearch:screen-value <> "" and fsearch:screen-value <> ?
then /* Add it to the top of the list */
 do: 
     if fSearch:num-items > 20
      then fSearch:delete(fSearch:num-items). /* Delete the oldest */
     fSearch:add-first(std-ch).

     publish "SetSearchHistory" (fSearch:list-items).
 end.

close query brwData.
empty temp-table claim no-error.

tStartTime = now.
run server/searchclaims.p (input std-ch,
                           output table claim,
                           output std-lo,
                           output std-ch).
if not std-lo then
do:
  message std-ch view-as alert-box error.
  return no-apply.
end.

for each claim exclusive-lock:
  publish "GetStatDesc" (input claim.stat, output claim.statDesc).
  publish "GetTypeDesc" (input claim.type, output claim.typeDesc).
  publish "GetStageDesc" (input claim.stage, output claim.stageDesc).
  publish "GetActionDesc" (input claim.action, output claim.actionDesc).
  publish "GetSysUserName" (input claim.assignedTo, output claim.assignedToName).
  tClaimID = string(claim.claimID).
  
  /* Since the column is 2 char long, so showing N/A in UI */
  if claim.stateID = "Unknown"
   then
    claim.stateID = "N/A".
end.

open query brwData preselect each claim by claim.claimid.

setStatusCount(num-results("{&browse-name}")).
appendStatus(" found in " + trim(string(interval(now, tStartTime, "seconds"), ">>>,>>9.9")) + " seconds").
displayStatus().


apply "VALUE-CHANGED" to brwData.

/* automatically open the claim if only one found */
publish "GetAutoViewSearch" (output std-lo).
if std-lo and num-results("{&browse-name}") = 1
and tClaimID <> ? and tClaimID <> "" and tClaimID <> "0"
 then run ClaimSelected in this-procedure (tClaimID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowBusy C-Win 
PROCEDURE ActionWindowBusy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pClaimID as int.
 def input parameter pBusy as logical.

 def buffer openWindow for openWindow.

 find openWindow
   where openWindow.claimID = pClaimID no-error.
 if available openWindow 
  then openWindow.isBusy = pBusy.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowClosed C-Win 
PROCEDURE ActionWindowClosed :
/*------------------------------------------------------------------------------
  Purpose:     This is called once when a file window is closed by the user.
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pClaimID as int.

 def buffer openWindow for openWindow.

 find openWindow
   where openWindow.claimID = pClaimID no-error.
 if available openWindow 
  then delete openWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent C-Win 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowOpened C-Win 
PROCEDURE ActionWindowOpened :
/*------------------------------------------------------------------------------
  Purpose:     Called once by a child window when it opens successfully
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pClaimID as int.

 def buffer openWindow for openWindow.

 create openWindow.
 openWindow.claimID = pClaimID.
 openWindow.procHandle = source-procedure.
 openWindow.isBusy = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentSelected C-Win 
PROCEDURE AgentSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAgentID as char.

 if pAgentID = "" then return.

 publish "SetCurrentValue" ("AgentID", pAgentID).
 run wclm01-r.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClaimSelected C-Win 
PROCEDURE ClaimSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pClaimID as character no-undo.
  define variable iClaimID as integer no-undo.
  def var hNewWindow as handle no-undo.
  def buffer openWindow for openWindow.

  iClaimID = integer(pClaimID) no-error.

  if iClaimID > 0
   then
    do:
      std-lo = showOpenWindow(iClaimID).
      if std-lo 
       then return.

      {lib/pbshow.i "''"}
      run wfileview.w persistent set hNewWindow (iClaimID).
      {lib/pbupdate.i "'Done.'" 100}
      {lib/pbhide.i}

      if not valid-handle(hNewWindow) 
       then return.

      create openWindow.
      assign
        openWindow.claimID = iClaimID
        openWindow.procHandle = hNewWindow 
        .
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteSavedList C-Win 
PROCEDURE DeleteSavedList :
/*------------------------------------------------------------------------------
@description Deletes a previously saved list
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      message "Do you want to delete the report " + list.displayName + "?" 
        view-as alert-box question buttons yes-no update std-lo as logical.
      
      if not std-lo
       then return.
      
      publish "DeleteList" (list.displayName, output std-lo).
      
      /* delete the temp table entry */
      if std-lo
       then run GetSavedLists in this-procedure (cmbEntity:screen-value).
       else message "Could not delete the report." view-as alert-box error buttons ok.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch cmbEntity chkAutoRun tEntityLabel fStatus fAssignedTo 
          cmbEntityLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fSearch brwData bNewList cmbEntity chkAutoRun brwList bClear fStatus 
         fAssignedTo bNew bSearch rLists 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
@description Filters the search reults
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  tWhereClause = doFilterSort().
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  
  setStatusCount(num-results("{&browse-name}")).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSavedLists C-Win 
PROCEDURE GetSavedLists :
/*------------------------------------------------------------------------------
@description Gets the user's saved lists
------------------------------------------------------------------------------*/
  define input parameter pEntity as character no-undo.
  
  publish "GetLists" (pEntity, output table list, output std-lo).
  if std-lo
   then
    do:
      {&OPEN-QUERY-brwList}
      apply "VALUE-CHANGED" to browse brwList.
    end.
   else message "Failed to get the saved lists." view-as alert-box error buttons ok.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewSavedList C-Win 
PROCEDURE NewSavedList :
/*------------------------------------------------------------------------------
@description Create a new list
------------------------------------------------------------------------------*/
  /* create a new window to build the filters and fields */
  run sys/wlistbuild.w persistent (this-procedure, true).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenSavedList C-Win 
PROCEDURE OpenSavedList :
/*------------------------------------------------------------------------------
@description Opens a user's previously saved list  
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      run sys/wlistbuild.w persistent set std-ha (this-procedure, false).
      run BuildData in std-ha (list.displayName, list.reportName, chkAutoRun:checked).
    end.
   else message "Please select a saved report" view-as alert-box warning buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveSavedList C-Win 
PROCEDURE SaveSavedList :
/*------------------------------------------------------------------------------
@description Saves the list
------------------------------------------------------------------------------*/
  define input parameter pIsNew as logical no-undo.
  define input parameter pCurrentName as character no-undo.
  define input parameter pDisplayName as character no-undo.
  define input parameter table for listfield.
  define input parameter table for listfilter.
  
  if (pIsNew or pCurrentName <> pDisplayName) and can-find(first list where displayName = pDisplayName)
   then
    do:
      message "There is already a saved list named " + pDisplayName + ". Do you want to overwrite?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
  
  pIsNew = not can-find(first list where displayName = pDisplayName).
  publish "SaveList" (pIsNew,
                      pDisplayName,
                      table listfield,
                      table listfilter,
                      output std-lo).
                      
  if std-lo
   then message "Successfully saved the report" view-as alert-box information buttons ok.
   else message "Could not save the report" view-as alert-box error buttons ok.
          
  /* if we renamed the report, we need to delete the old report as saving will make a new one */
  if pCurrentName <> pDisplayName
   then publish "DeleteList" (pCurrentName, output std-lo).
   
  run GetSavedLists in this-procedure (cmbEntity:screen-value in frame {&frame-name}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShareSavedList C-Win 
PROCEDURE ShareSavedList :
/*------------------------------------------------------------------------------
@description Shares the selected list with other users
------------------------------------------------------------------------------*/
  if available list
   then
    do:
      /* the output will be the user list */
      run sys/userselect.w ("", output std-ch).
      
      if std-ch > ""
       then
        do: 
          publish "ShareList" (list.displayName,
                               std-ch,
                               output std-ch).
      
          if std-ch > ""
           then message std-ch view-as alert-box error buttons ok.
           else message "Successfully shared the report" view-as alert-box information buttons ok.
        end.
    end.
   else message "Please select a saved report to share" view-as alert-box warning buttons ok.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VendorSelected C-Win 
PROCEDURE VendorSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pVendorID as char.

 if pVendorID = "" then return.

 publish "SetCurrentValue" ("VendorID", pVendorID).
 run wclm05-r.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewSavedList C-Win 
PROCEDURE ViewSavedList :
/*------------------------------------------------------------------------------
@description View the selected list
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      run sys/wlistresult.w persistent (list.displayName, list.reportName).
    end.
   else MESSAGE "Please select a saved report" VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.
  define  variable  dDiffWidth as decimal no-undo.
  hBrowse = browse brwList:handle.

  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  /* get the difference between the old and new width\height */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.

  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 15 - hBrowse:width-pixels.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  hBrowse:height-pixels = frame {&frame-name}:height-pixels - 110.
  
  /* move the browse list */
  do with frame {&frame-name}:
    assign
      rFilter:width-pixels = dFilterWidth + dDiffWidth - 1
      rLists:x = dEntityRect + dDiffWidth
      cmbEntity:x = dEntityColumn + dDiffWidth
      chkAutoRun:x = cmbEntity:x
      cmbEntityLabel:x = dEntityLabelColumn + dDiffWidth
      hBrowse:x = dListColumn + dDiffWidth

      tEntityLabel:x = frame {&frame-name}:width-pixels - 191
      bNewList:x = frame {&frame-name}:width-pixels - 194
      bModifyList:x = bNewList:x + 37
      bDeleteList:x = bModifyList:x + 37
      bShareList:x = bDeleteList:x + 37
      bOpenList:x = bShareList:x + 37
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showOpenWindow C-Win 
FUNCTION showOpenWindow RETURNS LOGICAL PRIVATE
  ( pClaimID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  Check if an open file
------------------------------------------------------------------------------*/
 def buffer openWindow for openWindow.

 for each openWindow:
  if not valid-handle(openWindow.procHandle) 
   then delete openWindow.
 end.

 find openWindow
   where openWindow.claimID = pClaimID no-error.

 if not available openWindow 
  then return false.

 run ShowWindow in openWindow.procHandle no-error.
 return not error-status:error.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

