&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* filedatasrv.p
   claim FILE DATA model interacting with the application SeRVer
   Created 2.23.2015 D.Sinclair
   
   @changelog
   -------------------------------------------------------------
   01.26.2017 John Oliver: Added approval date to ledger
   -------------------------------------------------------------
 */

def input parameter pBaseClaimID as int no-undo.
def input parameter hFileWindow as handle no-undo.

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/get-reserve-type.i}
{lib/next-seq.i}

{tt/account.i}
{tt/agent.i}
{tt/claim.i}
{tt/claimadjreq.i}
{tt/claimadjtrx.i}
{tt/claimattr.i}
{tt/claimproperty.i}
{tt/claimcode.i}
{tt/claimcontact.i}
{tt/claimcoverage.i}
{tt/claimcarrier.i}
{tt/claimbond.i}
{tt/claimlitigation.i}
{tt/claimlink.i}
{tt/checkrequest.i &tableAlias="claimcheckrequest"}
{tt/claimnote.i}
{tt/policy.i}
{tt/state.i}
{tt/sysuser.i}
{tt/sysprop.i}

/* datastructures for the accounting screen */
/* the common accounting tables are in account-server-calls */
{tt/claimacct.i}
{tt/claimacct.i &tableAlias="claimaccttab"}
{tt/claimadjust.i}
{tt/arclm.i}
{tt/apclm.i}

/* Temporary datastructures to handle server calls */
{tt/claim.i &tableAlias="tempclaim"}
{tt/claimadjreq.i &tableAlias="tempclaimadjreq"}
{tt/claimadjtrx.i &tableAlias="tempclaimadjtrx"}
{tt/claimattr.i &tableAlias="tempclaimattr"}
{tt/claimproperty.i &tableAlias="tempclaimproperty"}
{tt/claimcode.i &tableAlias="tempclaimcode"}
{tt/claimcontact.i &tableAlias="tempclaimcontact"}
{tt/claimcoverage.i &tableAlias="tempclaimcoverage"}
{tt/claimcarrier.i &tableAlias="tempclaimcarrier"}
{tt/claimbond.i &tableAlias="tempclaimbond"}
{tt/claimlitigation.i &tableAlias="tempclaimlitigation"}
{tt/claimlink.i &tableAlias="tempclaimlink"}
{tt/claimnote.i &tableAlias="tempclaimnote"}
{tt/claimnote.i &tableAlias="userclaimnote"}
{tt/sysuser.i &tableAlias="tempsysuser"}

/* Temporary datastructures for the accounting screen */
/* the common accounting tables are in account-server-calls */
{tt/claimacct.i &tableAlias="tempclaimacct"}
{tt/claimadjust.i &tableAlias="tempclaimadjust"}
{tt/arclm.i &tableAlias="temparclm"}
{tt/apclm.i &tableAlias="tempapclm"}

{tt/esbmsg.i}

def var tLastEsbMsgSeq as int init 0 no-undo.
def var tLastSync as datetime no-undo.
def var cUser as char no-undo.
def var cName as char no-undo.
def var lIsAccountingLoaded as logical no-undo init false.
def var lIsAccountingTabLoaded as logical no-undo init false.
def var lIsClaimLoaded as logical no-undo init false.
def var lIsUserLoaded as logical no-undo init false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-refreshClaim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaim Procedure 
FUNCTION refreshClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimBonds) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimBonds Procedure 
FUNCTION refreshClaimBonds RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimCarriers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimCarriers Procedure 
FUNCTION refreshClaimCarriers RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimCodes Procedure 
FUNCTION refreshClaimCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimContacts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimContacts Procedure 
FUNCTION refreshClaimContacts RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimCoverages) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimCoverages Procedure 
FUNCTION refreshClaimCoverages RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimLinks) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimLinks Procedure 
FUNCTION refreshClaimLinks RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimLitigations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimLitigations Procedure 
FUNCTION refreshClaimLitigations RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimNotes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimNotes Procedure 
FUNCTION refreshClaimNotes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimProperties) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaimProperties Procedure 
FUNCTION refreshClaimProperties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 29.76
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

{lib/account-server-calls.i &refType="Claim" &refID=pBaseClaimID}

subscribe to "EXTERNALESBMESSAGE" anywhere. /* Called by esbdata.p/NewStompMessage when another user changes 
                                               something we're interested in (as specified in param to lib/appstart.i) */

subscribe to "QUIT" anywhere.               /* Published by lib/appstart.i just before quitting; used for testing */

subscribe to "DATADUMP" anywhere.           /* Published by sys/about.w to allow trouble-shooting of data contents */


tLastSync = now.                            /* Time we retrieved the current data set */

if pBaseClaimID = 0 /* New */
 then return.


{lib/pbupdate.i "'Getting file data'" 5}
refreshClaim().                                                      

/*refreshClaimCodes().*/
/*refreshClaimContacts().*/

{lib/pbupdate.i "'Getting file coverages'" 10}
refreshClaimCoverages().

/*refreshClaimNotes().*/

{lib/pbupdate.i "'Getting file properties'" 20}
refreshClaimProperties().

/*refreshClaimCarriers().*/
/*refreshClaimBonds().*/
/*refreshClaimLitigations().*/
/*refreshClaimLinks().*/


{lib/pbupdate.i "'Getting user preferences'" 30}
publish "GetCredentialsID" (output cUser).
publish "GetCredentialsName" (output cName).
publish "GetClmUsers" (output table sysuser).
publish "SetLastFile" (pBaseClaimID).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-AddClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddClaimReserve Procedure 
PROCEDURE AddClaimReserve :
/*------------------------------------------------------------------------------
@description Adds a claim reserve
@note The claim request was added at the server so just add the transaction
      on the client
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define input parameter pRefCategory as character no-undo.
  define input parameter pAmount as decimal no-undo.
  
  /* check for a pending reserve adjustment */
  std-lo = false.
  for first claimadjreq exclusive-lock
      where claimadjreq.refcategory = pRefCategory:
      
    std-lo = true.
    create claimadjtrx.
    assign
      claimadjtrx.claimID = claimadjreq.claimID
      claimadjtrx.refCategory = claimadjreq.refCategory
      claimadjtrx.requestedAmount = claimadjreq.requestedAmount
      claimadjtrx.requestedBy = claimadjreq.requestedBy
      claimadjtrx.requestedUsername = claimadjreq.requestedUsername
      claimadjtrx.dateRequested = claimadjreq.dateRequested
      claimadjtrx.notes = (if claimadjreq.notes > "" then claimadjreq.notes else "Automatically added due to user's limit")
      claimadjtrx.uid = cUser
      claimadjtrx.username = cName
      claimadjtrx.transDate = now
      claimadjtrx.transAmount = pAmount
      .
    validate claimadjtrx.
    
    delete claimadjreq.
  end.
  
  if not std-lo
   then
    do:
          create claimadjtrx.
          assign
            claimadjtrx.claimID = pClaimID
            claimadjtrx.refCategory = pRefCategory
            claimadjtrx.requestedAmount = pAmount
            claimadjtrx.requestedBy = cUser
            claimadjtrx.requestedUsername = cName
            claimadjtrx.dateRequested = now
            claimadjtrx.notes = "Automatically approved due to user's limit"
            claimadjtrx.uid = cUser
            claimadjtrx.username = cName
            claimadjtrx.transDate = now
            claimadjtrx.transAmount = pAmount
            .
          validate claimadjtrx.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ApproveClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApproveClaimReserve Procedure 
PROCEDURE ApproveClaimReserve :
/*------------------------------------------------------------------------------
@description Approves the claim adjustment request 
------------------------------------------------------------------------------*/
  def input parameter pClaimID as integer no-undo.
  def input parameter pRefCategory as char no-undo.
  def input parameter pAmount as decimal no-undo.
  def input parameter pDate as datetime no-undo.
  def input parameter pNotes as char no-undo.
  def output parameter pSuccess as logical no-undo.
  
  def buffer claimadjreq for claimadjreq.
  
    run server/approveclaimadjreq.p (input pClaimID,
                                     input pRefCategory,
                                     input pAmount,
                                     input pDate,
                                     input pNotes,
                                     output pSuccess,
                                     output std-ch
                                     ).
  
    if pSuccess
     then
     for first claimadjreq exclusive-lock
        where claimadjreq.claimID = pClaimID
          and claimadjreq.refCategory = pRefCategory:

        create claimadjtrx.
        assign
          claimadjtrx.claimID = claimadjreq.claimID
          claimadjtrx.refCategory = claimadjreq.refCategory
          claimadjtrx.requestedAmount = claimadjreq.requestedAmount
          claimadjtrx.requestedBy = claimadjreq.requestedBy
          claimadjtrx.requestedUsername = claimadjreq.requestedUsername
          claimadjtrx.dateRequested = claimadjreq.dateRequested
          claimadjtrx.notes = pNotes
          claimadjtrx.uid = cUser
          claimadjtrx.username = cName
          claimadjtrx.transDate = pDate
          claimadjtrx.transAmount = pAmount
          .
        validate claimadjtrx.
        
        delete claimadjreq.
        publish "RefreshNotes".
      end.
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CanReserveCoverOpenInvoices) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CanReserveCoverOpenInvoices Procedure 
PROCEDURE CanReserveCoverOpenInvoices :
/*------------------------------------------------------------------------------
@description Determines if the reserve amount can cover the open invoices
------------------------------------------------------------------------------*/
  define input parameter pClaimID as int no-undo.
  define input parameter pCategory as character no-undo.
  define input parameter pAmount as decimal no-undo.
  define output parameter pContinue as logical no-undo.

  define buffer apinv for apinv.
  
  /* get the currrent reserve amount */
  std-de = 0.
  run GetClaimReserveAmount in this-procedure (pClaimID, pCategory, output std-de).
  pAmount = std-de + pAmount.
  
  assign
    /* this will hold the sum of the invoice */
    std-de = 0.0
    /* this will be the claim id */
    std-ch = string(pClaimID)
    .
  /* sum up the invoices */
  for each apinv no-lock
     where apinv.refID = std-ch
       and apinv.refCategory = pCategory
       and apinv.stat = "O":
    
    std-de = std-de + apinv.amount.
  end.
  /* compare the invoice total to the reserve amount and warn if less */
  pContinue = true.
  if pAmount < std-de
   then message "The reserve amount will not cover all of the open invoices. Continue?" 
          view-as alert-box warning buttons yes-no update pContinue.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DATADUMP) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DATADUMP Procedure 
PROCEDURE DATADUMP :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tPrefix as char no-undo.

 publish "DeleteTempFile" ("DataClaim" + string(pBaseClaimID), output std-lo).

 tPrefix = string(pBaseClaimID) 
                  + string(year(today), "9999")
                  + string(month(today), "99")
                  + string(day(today), "99") 
                  + string(etime).

 /* Active temp-tables */
 temp-table claim:write-xml("file", tPrefix + "1").
 publish "AddTempFile" ("DataClaim" + string(pBaseClaimID), tPrefix + "1").

 temp-table claimcontact:write-xml("file", tPrefix + "2").
 publish "AddTempFile" ("DataContact" + string(pBaseClaimID), tPrefix + "2").

 temp-table claimproperty:write-xml("file", tPrefix + "3").
 publish "AddTempFile" ("DataProperty" + string(pBaseClaimID), tPrefix + "3").

 temp-table claimcoverage:write-xml("file", tPrefix + "4").
 publish "AddTempFile" ("DataPolicy" + string(pBaseClaimID), tPrefix + "4").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimAttribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimAttribute Procedure 
PROCEDURE DeleteClaimAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pCode as character no-undo.
  define output parameter pSuccess as logical no-undo.
 
  run server/deleteclaimattr.p (input pBaseClaimID,
                                input pCode,
                                output pSuccess,
                                output std-ch).
                                
  if not pSuccess and std-ch > ""
    then message std-ch view-as alert-box error buttons ok.
    
  /* Refresh the claim attributes temp-tables */
  empty temp-table claimattr no-error.
  empty temp-table tempclaimattr no-error.
  run GetClaimAttributes (output table tempclaimattr).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimBond) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimBond Procedure 
PROCEDURE DeleteClaimBond :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pClaimID as int no-undo.
def input parameter pBondID as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimbond for claimbond.

  find first claimbond where claimbond.claimID = pClaimID
                       and claimbond.bondID = pBondID no-error.
  if not avail claimbond then
  do: 
    pSuccess = false.
    pMsg = "Claim bond record for claim " + string(pClaimID) 
         + ", ID " + string(pBondID) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimbond.p (input pClaimID,
                                input pBondID,
                                output pSuccess,
                                output pMsg).
  if not pSuccess 
    then return.

  refreshclaimbonds().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimCode Procedure 
PROCEDURE DeleteClaimCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pClaimID as int no-undo.
def input parameter pCodeType as char no-undo.
def input parameter pCode as char no-undo.
def input parameter pSeq as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimcode for claimcode.

  find first claimcode where claimcode.claimID = pClaimID
                         and claimcode.codeType = pCodeType
                         and claimcode.code = pCode
                         and claimcode.seq = pSeq no-error.
  if not avail claimcode then
  do: 
    pSuccess = false.
    pMsg = "Claim code record for claim " + string(pClaimID) 
         + ", code type " + pCodeType 
         + ", code " + pCode  
         + ", seq " + string(pSeq) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimcode.p (input pClaimID,
                                input pCodeType,
                                input pCode,
                                input pSeq,
                                output pSuccess,
                                output pMsg).
  if not pSuccess 
    then return.

  refreshclaimcodes().
  publish "RefreshNotes".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimContact) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimContact Procedure 
PROCEDURE DeleteClaimContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pClaimID as int no-undo.
def input parameter pContactID as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimcontact for claimcontact.

  find first claimcontact where claimcontact.claimID = pClaimID
                          and claimcontact.contactID = pContactID no-error.
  if not avail claimcontact then
  do: 
    pSuccess = false.
    pMsg = "Claim contact record for claim " + string(pClaimID) 
         + ", ID " + string(pContactID) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimcontact.p (input pClaimID,
                                   input pContactID,
                                   output pSuccess,
                                   output pMsg).
  if not pSuccess 
    then return.

  refreshclaimcontacts().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimCoverage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimCoverage Procedure 
PROCEDURE DeleteClaimCoverage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pClaimID as int no-undo.
def input parameter pSeq as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimcoverage for claimcoverage.

  find first claimcoverage where claimcoverage.claimID = pClaimID
                           and claimcoverage.seq = pSeq no-error.
  if not avail claimcoverage then
  do: 
    pSuccess = false.
    pMsg = "Claim coverage record for claim " + string(pClaimID) 
         + ", sequence " + string(pSeq) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimcoverage.p (input pClaimID,
                                    input pSeq,
                                    output pSuccess,
                                    output pMsg).
  if not pSuccess 
    then return.

  refreshclaimcoverages().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimLink) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimLink Procedure 
PROCEDURE DeleteClaimLink :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pFromClaimID as int no-undo.
def input parameter pToClaimID as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimlink for claimlink.

  find first claimlink where claimlink.fromClaimID = pFromClaimID
                       and claimlink.toClaimID = pToClaimID no-error.
  if not avail claimlink then
  do: 
    pSuccess = false.
    pMsg = "Claim link record for 'from' claim " + string(pFromClaimID) 
         + ", 'to' claim " + string(pToClaimID) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimlink.p (input pFromClaimID,
                                input pToClaimID,
                                output pSuccess,
                                output pMsg).
  if not pSuccess 
    then return.

  refreshclaimlinks().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimLitigation) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimLitigation Procedure 
PROCEDURE DeleteClaimLitigation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pClaimID as int no-undo.
def input parameter pLitigationID as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimlitigation for claimlitigation.

  find first claimlitigation where claimlitigation.claimID = pClaimID
                             and claimlitigation.litigationID = pLitigationID no-error.
  if not avail claimlitigation then
  do: 
    pSuccess = false.
    pMsg = "Claim litigation record for claim " + string(pClaimID) 
         + ", ID " + string(pLitigationID) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimlitigation.p (input pClaimID,
                                      input pLitigationID,
                                      output pSuccess,
                                      output pMsg).
  if not pSuccess 
    then return.

  refreshclaimlitigations().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimProperty) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimProperty Procedure 
PROCEDURE DeleteClaimProperty :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pClaimID as int no-undo.
def input parameter pSeq as int no-undo.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimproperty for claimproperty.

  find first claimproperty where claimproperty.claimID = pClaimID
                           and claimproperty.seq = pSeq no-error.
  if not avail claimproperty then
  do: 
    pSuccess = false.
    pMsg = "Claim property record for claim " + string(pClaimID) 
         + ", sequence " + string(pSeq) + " does not exist.".
    return.
  end.
 
  run server/deleteclaimproperty.p (input pClaimID,
                                    input pSeq,
                                    output pSuccess,
                                    output pMsg).
  if not pSuccess 
    then return.

  refreshclaimproperties().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteClaimReserve Procedure 
PROCEDURE DeleteClaimReserve :
/*------------------------------------------------------------------------------
@description Deletes the payable invoice from the database
------------------------------------------------------------------------------*/
  def input parameter pClaimID as integer no-undo.
  def input parameter pRefCategory as char no-undo.
  def output parameter pSuccess as logical.
  
  def buffer claimadjreq for claimadjreq.
  
  for first claimadjreq exclusive-lock
      where claimadjreq.claimID = pClaimID
        and claimadjreq.refCategory = pRefCategory:
        
    run server/deleteclaimadjreq.p (input pClaimID,
                                    input pRefCategory,
                                    output pSuccess,
                                    output std-ch
                                    ).
    
    if pSuccess
     then delete claimadjreq.
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-EXTERNALESBMESSAGE) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EXTERNALESBMESSAGE Procedure 
PROCEDURE EXTERNALESBMESSAGE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pEntity as char no-undo.
 def input parameter pAction as char no-undo.
 def input parameter pKey as char no-undo.

 tLastEsbMsgSeq = tLastEsbMsgSeq + 1.

 create esbmsg.
 assign
   esbmsg.seq = tLastEsbMsgSeq
   esbmsg.rcvd = now
   esbmsg.entity = pEntity
   esbmsg.action = pAction
   esbmsg.keyID = pKey
   .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaim Procedure 
PROCEDURE GetClaim :
/*------------------------------------------------------------------------------
@description Get the claim 
------------------------------------------------------------------------------*/
def output parameter table for claim.

find first claim no-error.
if not avail claim 
then refreshClaim().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAccounting) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAccounting Procedure 
PROCEDURE GetClaimAccounting :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/
  def output parameter table for apclm. 
  def output parameter table for arclm. 
  def output parameter table for claimadjust. 
  def output parameter table for claimacct.
  
  if not lIsAccountingLoaded
   then run LoadClaimAccounting in this-procedure.
  
  /* APCLM from APINV, APINVA, and APTRX */
  run GetClaimAccountingPayables in this-procedure (output table apclm).
  
  /* ARCLM from ARINV and ARTRX */
  run GetClaimAccountingReceivables in this-procedure (output table arclm).
  
  /* CLAIMADJUST from CLAIMADJREQ and CLAIMADJTRX */
  run GetClaimAccountingReserves in this-procedure (output table claimadjust).
  
  /* CLAIMACCT from APTRX, ARTRX, and CLAIMADJTRX */
  run GetClaimAccountingLedger in this-procedure (output table claimacct).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAccountingLedger) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAccountingLedger Procedure 
PROCEDURE GetClaimAccountingLedger :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/
  define output parameter table for claimacct.
  
  /* buffer */
  define buffer apinv for apinv.
  define buffer apinva for apinva.
  define buffer aptrx for aptrx.
  define buffer claimadjreq for claimadjreq.
  define buffer claimadjtrx for claimadjtrx.
  
  /* empty the table */
  empty temp-table claimacct.
  
  /* build the CLAIMACCT table */
  /* the payables */
  for each apinv no-lock
     where apinv.refID = string(pBaseClaimID):
    
   create claimacct.
   assign
      claimacct.claimID = integer(apinv.refID)
      claimacct.acctType = apinv.refCategory
      claimacct.transType = "P"
      claimacct.transDescription = "Payable"
      claimacct.refType = "V"
      claimacct.stat = "O"  /* (if apinv.stat = "D" then "O" else apinv.stat)  */
      claimacct.refID = apinv.vendorID
      claimacct.refDescription = apinv.vendorName
      claimacct.dateRequested = apinv.dateReceived
      claimacct.requestedAmount = apinv.amount
      claimacct.requestedBy = apinv.uid

      /*claimacct.transDate = apinv.dateReceived*/ /* Artificial */
      claimacct.uid = ""
      .
   
  for each apinva no-lock
     where apinva.apinvID = apinv.apinvID
       and apinva.stat = "A"
        by apinva.dateActed:
            
    assign
      claimacct.approvedDate = apinva.dateActed
      claimacct.uid = addDelimiter(claimacct.uid,", ") + apinva.uid
      .
  end.

   /* Update if the payable is completed */
   find aptrx
     where aptrx.apinvID = apinv.apinvID
       and aptrx.transType = "C" no-error.
   if available aptrx 
    then assign
/*            claimacct.seq = 2  */
           claimacct.stat = "C" /* Completed -- reduces balance */
           claimacct.transDate = aptrx.transDate
           claimacct.transAmount = aptrx.transAmount
           .

   find aptrx
     where aptrx.apinvID = apinv.apinvID
       and aptrx.transType = "V" no-error.
   if available aptrx 
    then assign
/*            claimacct.seq = 2  */
           claimacct.stat = "V" /* Voided -- no effect on balance */
           claimacct.transDate = aptrx.transDate
           claimacct.transAmount = aptrx.transAmount
           .

/*    for last aptrx                                            */
/*      where aptrx.apinvID = apinv.apinvID                     */
/*        and (aptrx.transType = "C" or aptrx.transType = "V"): */
/*     assign                                                   */
/*        claimacct.seq = 2                                     */
/*        claimacct.transDate = aptrx.transDate                 */
/*        claimacct.transAmount =  aptrx.transAmount            */
/*        .                                                     */
/*    end.                                                      */
  end.

  /* the adjustment requests */
  for each claimadjreq no-lock:
    create claimacct.
    assign
/*       claimacct.seq = 1  */
      claimacct.claimID = claimadjreq.claimID
      claimacct.acctType = claimadjreq.refCategory
      claimacct.transType = "A"
      claimacct.transDescription = "Reserve Adj"
      claimacct.stat = "O"
      claimacct.refType = ""
      claimacct.refID = ""
      claimacct.refDescription = ""
      claimacct.requestedBy = claimadjreq.requestedBy
      claimacct.dateRequested = claimadjreq.dateRequested
      claimacct.requestedAmount = claimadjreq.requestedAmount
      claimacct.transDate = claimadjreq.dateRequested /* Artificial */
      claimacct.transAmount = 0  /* No change to balance */
      .
  end.

  /* the adjustment transactions */
  for each claimadjtrx no-lock:
    create claimacct.
    assign
/*       claimacct.seq = 2  */
      claimacct.claimID = claimadjtrx.claimID
      claimacct.acctType = claimadjtrx.refCategory
      claimacct.transType = "A"
      claimacct.transDescription = "Reserve Adj"
      claimacct.stat = "C"
      claimacct.refType = ""
      claimacct.refID = ""
      claimacct.refDescription = ""
      claimacct.requestedBy = claimadjtrx.requestedBy
      claimacct.uid = claimadjtrx.uid
      claimacct.dateRequested = claimadjtrx.dateRequested
      claimacct.requestedAmount = claimadjtrx.requestedAmount
      claimacct.transDate = claimadjtrx.transDate
      claimacct.transAmount = claimadjtrx.transAmount /* + or - */
      .
  end.

  std-de = 0.0.  /* calculate the rolling balance */
  std-in = 0.
  for each claimacct
     where claimacct.stat = "C"
        or claimacct.stat = "V"
  break by claimacct.acctType
        by claimacct.transDate
        by claimacct.transType /* A = Adjustment, P = Payable */
        /*by claimacct.seq */:
        
/*     std-in = std-in + 1.  */
    if first-of(claimacct.acctType)
     then assign
            std-de = 0.
/*             claimacct.balance = 0 */
            .

    std-in = std-in + 1. /* Sequence of records */
    std-de = std-de + claimacct.transAmount.

     /* if the record is a payable */                                 
     if claimacct.transType = "P"                                     
      then                                                            
       case claimacct.stat:                                           
        /* we want to subtract the amount if it's complete */         
        when "C" then std-de = std-de - (claimacct.transAmount * 2).  
        /* if it's void, then the balance will stay the same */       
        when "V" then std-de = std-de - claimacct.transAmount.        
       end.                                                           

    claimacct.seq = std-in.
    claimacct.balance = std-de.
  end.
  
  /* add the descriptions */
  for each claimacct exclusive-lock:
    /* set the payable type */
    publish "GetPayableStatusDesc" (claimacct.stat, output claimacct.statDesc).
    /* set the user's name for the requested by column */
    publish "GetSysUserName" (claimacct.requestedBy, output claimacct.requestedDesc).
    /* set the user's name for the approval column */
    publish "GetSysUserName" (claimacct.uid, output claimacct.uidDesc).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAccountingPayables) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAccountingPayables Procedure 
PROCEDURE GetClaimAccountingPayables :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/
  def output parameter table for apclm.
  
  /* buffers */
  def buffer apinv for apinv.
  def buffer apinva for apinva.
  def buffer aptrx for aptrx.
  
  /* empty the tables so we can recreate them */
  empty temp-table apclm.
  
  /* build the APCLM table */
  std-in = 0.
  for each apinv no-lock:
    create apclm.
    assign
      std-in = std-in + 1
      apclm.seq = std-in
      apclm.apinvID = apinv.apinvID
      apclm.stat = apinv.stat
      apclm.refType = apinv.refType
      apclm.refID = apinv.refID
      apclm.refSeq = apinv.refSeq
      apclm.refCategory = apinv.refCategory
      apclm.notes = apinv.notes
      apclm.vendorID = apinv.vendorID
      apclm.vendorName = apinv.vendorName
      apclm.invoiceNumber = apinv.invoiceNumber
      apclm.invoiceDate = apinv.invoiceDate
      apclm.dateReceived = apinv.dateReceived
      apclm.amount = apinv.amount
      apclm.dueDate = apinv.dueDate
      apclm.PONumber = apinv.PONumber
      apclm.hasDocument = apinv.hasDocument
      apclm.documentFilename = apinv.documentFilename
      apclm.reduceLiability = apinv.reduceLiability
      .
      
    for each apinva no-lock
       where apinva.apinvID = apinv.apinvID
         and apinva.stat = "A"
          by apinva.dateActed:
              
      assign
        apclm.approvalDate = apinva.dateActed
        apclm.approval = addDelimiter(apclm.approval,", ") + apinva.uid
        .
    end.
    
    for each aptrx no-lock
       where aptrx.apinvID = apinv.apinvID:
         
      assign
        apclm.aptrxID = aptrx.aptrxID
        apclm.transType = aptrx.transType
        apclm.transAmount = apclm.transAmount + aptrx.transAmount
        apclm.transDate = aptrx.transDate
        apclm.report = aptrx.report
        .
    end.
    
    validate apclm.
    release apclm.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAccountingReceivables) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAccountingReceivables Procedure 
PROCEDURE GetClaimAccountingReceivables :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/
  def output parameter table for arclm.
  
  /* buffers */
  define buffer arinv for arinv.
  define buffer artrx for artrx.
  
  /* empty the tables so we can recreate them */
  empty temp-table arclm.
  
  /* build the ARCLM table */
  for each arinv no-lock:
    create arclm.
    assign
       arclm.transType = "O"
       arclm.arinvID = arinv.arinvID
       arclm.stat = arinv.stat
       arclm.refType = arinv.refType
       arclm.refID = arinv.refID
       arclm.refSeq = arinv.refSeq
       arclm.refCategory = arinv.refCategory
       arclm.uid = arinv.uid
       arclm.notes = arinv.notes
       arclm.name = arinv.name
       arclm.addr1 = arinv.addr1
       arclm.addr2 = arclm.addr2
       arclm.city = arclm.city
       arclm.stateID = arclm.stateID
       arclm.zipcode = arinv.zipcode
       arclm.contactName = arinv.contactName
       arclm.contactPhone = arinv.contactPhone
       arclm.contactEmail = arinv.contactemail
       arclm.invoiceNumber = arinv.invoiceNumber
       arclm.dateRequested = arinv.dateRequested
       arclm.dueDate = arinv.dueDate
       arclm.requestedAmount = arinv.requestedAmount
       arclm.waivedAmount = arinv.waivedAmount
       arclm.hasDocument = arinv.hasDocument
       arclm.documentFilename = arinv.documentFilename
      .
      
    for each artrx no-lock
       where artrx.arinvID = arinv.arinvID:
         
      assign
        arclm.artrxID = artrx.artrxID
        arclm.transType = artrx.transType
        arclm.transAmount = arclm.transAmount + artrx.transAmount
        arclm.transDate = artrx.transDate
        .
    end.
  end.
  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAccountingReserves) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAccountingReserves Procedure 
PROCEDURE GetClaimAccountingReserves :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/
  def output parameter table for claimadjust.
  
  /* buffers */
  def buffer claimadjreq for claimadjreq.
  def buffer claimadjtrx for claimadjtrx.
  
  /* empty the tables so we can recreate them */
  empty temp-table claimadjust.
  
  /* CLAIMADJUST from CLAIMADJREQ and CLAIMADJTRX */
  std-in = 0.
  for each claimadjreq:
    create claimadjust.
    assign
      std-in = std-in + 1
      claimadjust.claimID = claimadjreq.claimID
      claimadjust.seq = std-in
      claimadjust.stat = "O"
      claimadjust.refCategory = claimadjreq.refCategory
      claimadjust.requestedAmount = claimadjreq.requestedAmount
      claimadjust.requestedBy = claimadjreq.requestedBy
      claimadjust.dateRequested = claimadjreq.dateRequested
      claimadjust.notes = claimadjreq.notes
      claimadjust.uid = claimadjreq.uid
      .
  end.
  
  for each claimadjtrx:
    create claimadjust.
    assign
      std-in = std-in + 1
      claimadjust.claimID = claimadjtrx.claimID
      claimadjust.seq = std-in
      claimadjust.stat = "A"
      claimadjust.refCategory = claimadjtrx.refCategory
      claimadjust.requestedAmount = claimadjtrx.requestedAmount
      claimadjust.uid = claimadjtrx.uid
      claimadjust.dateRequested = claimadjtrx.dateRequested
      claimadjust.notes = claimadjtrx.notes
      claimadjust.transDate = claimadjtrx.transDate
      claimadjust.transAmount = claimadjtrx.transAmount
      claimadjust.requestedBy = claimadjtrx.requestedBy
      .
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAccountingTab) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAccountingTab Procedure 
PROCEDURE GetClaimAccountingTab :
/*------------------------------------------------------------------------------
@description Gets the data for the fill-in fields on the accounting tab
------------------------------------------------------------------------------*/
  define output parameter table for claimaccttab.
  
  if not lIsAccountingTabLoaded
   then run LoadClaimAccountingTab.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAgentState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAgentState Procedure 
PROCEDURE GetClaimAgentState :
/*------------------------------------------------------------------------------
@description Get the agent for the claim
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pAgentState as character no-undo.
 
  define buffer agent for agent.
  define buffer claim for claim.
  define buffer state for state.
  
  publish "GetAgents" (output table agent).
  publish "GetStates" (output table state).
  
  for first claim, each agent no-lock
      where claim.agentID = agent.agentID
        and claim.claimID = pClaimID:
    
    for first state no-lock
        where state.stateID = agent.stateID:
        
      pAgentState = fill("0",(if state.seq < 10 then 1 else 0)) + string(state.seq).
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAssignedTo) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAssignedTo Procedure 
PROCEDURE GetClaimAssignedTo :
/*------------------------------------------------------------------------------
@description Get the agent for the claim
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pAssignedTo as character no-undo.
 
  define buffer claim for claim.
  
  std-ch = "".
  for first claim no-lock:
    publish "GetSysUserName" (claim.assignedTo, output std-ch).
    pAssignedTo = std-ch + "," + claim.assignedTo.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimAttributes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimAttributes Procedure 
PROCEDURE GetClaimAttributes :
/*------------------------------------------------------------------------------
@description Get the claim attributes
------------------------------------------------------------------------------*/
  define output parameter table for tempclaimattr.
  
  define buffer claimattr for claimattr.
  
  if not can-find(first claimattr)
   then run LoadClaimAttributes in this-procedure.
   
  empty temp-table tempclaimattr.
  for each claimattr no-lock
     where claimattr.claimID = pBaseClaimID:
     
    create tempclaimattr.
    buffer-copy claimattr to tempclaimattr.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimBonds) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimBonds Procedure 
PROCEDURE GetClaimBonds :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimbond.

find first claimbond no-error.
if not avail claimbond then
refreshClaimBonds().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimCarriers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimCarriers Procedure 
PROCEDURE GetClaimCarriers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimcarrier.

find first claimcarrier no-error.
if not avail claimcarrier then
refreshClaimCarriers().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimCodes Procedure 
PROCEDURE GetClaimCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimcode.

find first claimcode no-error.
if not avail claimcode then
refreshClaimCodes().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimContacts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimContacts Procedure 
PROCEDURE GetClaimContacts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimcontact.

find first claimcontact no-error.
if not avail claimcontact then
refreshClaimContacts().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimCoverages) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimCoverages Procedure 
PROCEDURE GetClaimCoverages :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimcoverage.

find first claimcoverage no-error.
if not avail claimcoverage then
refreshClaimCoverages().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimDescription) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimDescription Procedure 
PROCEDURE GetClaimDescription :
/*------------------------------------------------------------------------------
@description Get the claim reserve based on the type entered
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pDescrip as character no-undo.
  
  define buffer claim for claim.
  
  if not lIsClaimLoaded
   then run LoadClaim.
   
  for first claim no-lock
      where claim.claimID = pClaimID:
    
    pDescrip = claim.description.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimLinks) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimLinks Procedure 
PROCEDURE GetClaimLinks :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimlink.

find first claimlink no-error.
if not avail claimlink then
refreshClaimLinks().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimLitigations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimLitigations Procedure 
PROCEDURE GetClaimLitigations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimlitigation.

find first claimlitigation no-error.
if not avail claimlitigation then
refreshClaimLitigations().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimLossApprover) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimLossApprover Procedure 
PROCEDURE GetClaimLossApprover :
/*------------------------------------------------------------------------------
@description Get list of approvers up to the user that can approve the amount
@note 
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pClaimID AS INTEGER NO-UNDO.
  DEFINE INPUT PARAMETER pRefCategory AS CHARACTER NO-UNDO.
  DEFINE INPUT PARAMETER pAmount AS DECIMAL NO-UNDO.
  DEFINE OUTPUT PARAMETER pUserList AS CHARACTER NO-UNDO.
  
  define variable lAddedApprover as logical no-undo initial false.
  DEFINE VARIABLE cRefDesc AS CHARACTER NO-UNDO.
  define variable cSysUserID as character no-undo.
  define variable cSysUserName as character no-undo.
  
  /* buffer */
  DEFINE BUFFER sysprop for sysprop.
  
  PUBLISH "GetCategoryDesc" (pRefCategory, OUTPUT cRefDesc).
  
  publish "GetSysProps" (output table sysprop).
  for each sysprop no-lock
     where sysprop.appCode = "CLM"
       and sysprop.objAction = "ClaimAdjustmentRequest"
       and sysprop.objProperty = cRefDesc + "ReserveLimit"
       and sysprop.objValue <> "-1"
        by decimal(sysprop.objValue):
       
    cSysUserID = addDelimiter(cSysUserID,",") + sysprop.objID.
    cSysUserName = addDelimiter(cSysUserName,",") + sysprop.objName.
  end.
  for each sysprop no-lock
     where sysprop.appCode = "CLM"
       and sysprop.objAction = "ClaimAdjustmentRequest"
       and sysprop.objProperty = cRefDesc + "ReserveLimit"
       and sysprop.objValue = "-1":
       
    cSysUserID = addDelimiter(cSysUserID,",") + sysprop.objID.
    cSysUserName = addDelimiter(cSysUserName,",") + sysprop.objName.
  end.
  do std-in = 1 to num-entries(cSysUserID):
    assign
      std-ch = entry(std-in,cSysUserID)
      std-de = 0
      .
    {lib/getclaimreservelimit.i pClaimID pRefCategory std-ch std-de}.
    
    if std-de < pAmount
     then pUserList = addDelimiter(pUserList,",") + entry(std-in,cSysUserName) + "," + std-ch.
     else
      do:
        if not lAddedApprover
         then
          assign
            pUserList = addDelimiter(pUserList,",") + entry(std-in,cSysUserName) + "," + std-ch.
            lAddedApprover = true
            .
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimNotes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimNotes Procedure 
PROCEDURE GetClaimNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter table for claimnote.

define buffer claimnote for claimnote.

find first claimnote no-error.
if not available claimnote
 then refreshClaimNotes().

for each claimnote exclusive-lock:
  claimnote.edited = false.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimPayable) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimPayable Procedure 
PROCEDURE GetClaimPayable :
/*------------------------------------------------------------------------------
@description Get the claim payable invoice
------------------------------------------------------------------------------*/
  def input parameter pClaimID as int no-undo.
  def input parameter pInvoiceID as int no-undo.
  def output parameter table for tempapinv.
  def output parameter table for tempapinva.
  def output parameter table for tempapinvd.
  
  if pBaseClaimID = pClaimID
   then run GetPayableInvoice in this-procedure
                             (input pInvoiceID,
                              output table tempapinv,
                              output table tempapinva,
                              output table tempapinvd
                              ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimPayableAccount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimPayableAccount Procedure 
PROCEDURE GetClaimPayableAccount :
/*------------------------------------------------------------------------------
@description Get the claim payable invoice account
------------------------------------------------------------------------------*/
  define input parameter pRefCategory as character no-undo.
  define output parameter pAccountNumber as character no-undo.
  
  define buffer sysprop for sysprop.
  
  publish "GetCategoryDesc" (pRefCategory,output std-ch).
  
  publish "GetSysProps" (output table sysprop).

  for first sysprop no-lock
      where sysprop.appCode = "CLM"
       and sysprop.objAction = "PayableInvoice"
       and sysprop.objProperty = "Account"
       and sysprop.objID = std-ch:
  
    run GetClaimAgentState (pBaseClaimID, output std-ch).
    pAccountNumber = replace(sysprop.objvalue,"XX",std-ch).
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimPDFDetails) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimPDFDetails Procedure 
PROCEDURE GetClaimPDFDetails :
/*------------------------------------------------------------------------------
@description Get the claim loss pdf details
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define input parameter pPolicyID as integer no-undo.
  define output parameter table for claimcheckrequest.
  
  define buffer claim for claim.
  define buffer agent for agent.
  
  empty temp-table claimcheckrequest.
  for first claim no-lock
      where claim.claimID = pClaimID:
      
    create claimcheckrequest.
    /* get claim data */
    assign
      claimcheckrequest.claimID = pClaimID
      claimcheckrequest.altaRisk = claim.altaRisk
      claimcheckrequest.altaResponsibility = claim.altaResponsibility
      claimcheckrequest.claimSummary = claim.summary
      claimcheckrequest.agentFileNumber = claim.fileNumber
      claimcheckrequest.insuredName = claim.insuredName
      claimcheckrequest.claimCategory = ""
      .
    run GetClaimCodes in this-procedure (output table claimcode).
    for first claimcode no-lock
        where claimcode.seq = 1:
      publish "GetSysCodeDescription" ("ClaimDescription", claimcode.code, output claimcheckrequest.claimCategory).
    end.
    /* get the agent data */
    if not can-find(first agent where agentID = claim.agentID)
     then publish "GetAgents" (output table agent).
     
    for first agent no-lock
        where agent.agentID = claim.agentID:
        
      claimcheckrequest.agentID = agent.agentID.
      claimcheckrequest.agentName = agent.name.
    end.
    /* get policy data */
    if pPolicyID <> 0
     then
      do:
        run server/getpolicies.p (?,?,string(pPolicyID),string(pPolicyID),?,?,?,
                                  output table policy,output std-lo,output std-ch).
        for first policy no-lock:
          assign
            claimcheckrequest.policyID = policy.policyID
            claimcheckrequest.policyAmount = policy.issuedLiabilityAmount
            claimcheckrequest.policyDate = policy.issuedEffDate
            .
        end.
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimPolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimPolicy Procedure 
PROCEDURE GetClaimPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pPolicyID as integer no-undo.
  
  define buffer claimcoverage for claimcoverage.
  
  if not can-find(first claimcoverage where claimID = pClaimID)
   then run GetClaimCoverages in this-procedure (output table claimcoverage).
  
  pPolicyID = 0.
  for first claimcoverage no-lock
      where claimcoverage.claimID = pClaimID:
      
    pPolicyID = integer(claimcoverage.coverageID) no-error.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimPolicyLimit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimPolicyLimit Procedure 
PROCEDURE GetClaimPolicyLimit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pPolicyID as integer no-undo.
  define output parameter pLimitAmount as decimal no-undo.
  
  define buffer policy for policy.
  
  run server/getclaimpolicyliability.p (input pPolicyID,
                                        output pLimitAmount,
                                        output std-lo,
                                        output std-ch).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimProperties) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimProperties Procedure 
PROCEDURE GetClaimProperties :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for claimproperty.

find first claimproperty no-error.

if not avail claimproperty then
refreshClaimProperties().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimReceivable) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimReceivable Procedure 
PROCEDURE GetClaimReceivable :
/*------------------------------------------------------------------------------
@description Get the claim payable invoice
------------------------------------------------------------------------------*/
  def input parameter pClaimID as int no-undo.
  def input parameter pInvoiceID as int no-undo.
  def output parameter table for temparinv.
  
  if pBaseClaimID = pClaimID
   then run GetReceivableInvoice in this-procedure
                                 (input pInvoiceID,
                                  output table temparinv
                                  ).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimReserve Procedure 
PROCEDURE GetClaimReserve :
/*------------------------------------------------------------------------------
@description Get the claim reserve based on the type entered
------------------------------------------------------------------------------*/
  def input parameter pClaimID as int no-undo.
  def input parameter pCategory as char no-undo.
  def input parameter pDate as datetime no-undo.
  def output parameter table for tempclaimadjreq.
  
  /* buffer */
  def buffer claimadjreq for claimadjreq.
  
  /* empty the table */
  empty temp-table tempclaimadjreq.
  
  /* first try to get the request from the transaction table */
  for first claimadjtrx no-lock
      where claimadjtrx.claimID = pClaimID
        and claimadjtrx.refCategory = pCategory
        and claimadjtrx.transDate = pDate:
        
    create tempclaimadjreq.
    assign
      tempclaimadjreq.claimID = claimadjtrx.claimID
      tempclaimadjreq.refCategory = claimadjtrx.refCategory
      tempclaimadjreq.requestedAmount = claimadjtrx.requestedAmount
      tempclaimadjreq.uid = claimadjtrx.uid
      tempclaimadjreq.notes = claimadjtrx.notes
      .
  end.
  /* if the temp table isn't created, then try the request table */
  if not can-find(first tempclaimadjreq)
   then
    for first claimadjreq no-lock
        where claimadjreq.claimID = pClaimID
          and claimadjreq.refCategory = pCategory:
          
      create tempclaimadjreq.
      buffer-copy claimadjreq to tempclaimadjreq.
    end.
    
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimReserveAmount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimReserveAmount Procedure 
PROCEDURE GetClaimReserveAmount :
/*------------------------------------------------------------------------------
@description Get the claim reserve based on the type entered
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pClaimID AS INTEGER NO-UNDO.
  DEFINE INPUT PARAMETER pCategory AS CHARACTER NO-UNDO.
  DEFINE OUTPUT PARAMETER pAmount AS DECIMAL NO-UNDO.
  
  {lib/getclaimreserve.i pClaimID pCategory pAmount}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimReserveApprovedAmount) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimReserveApprovedAmount Procedure 
PROCEDURE GetClaimReserveApprovedAmount :
/*------------------------------------------------------------------------------
@description Get the claim reserve based on the type entered
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pClaimID AS INTEGER NO-UNDO.
  DEFINE INPUT PARAMETER pCategory AS CHARACTER NO-UNDO.
  DEFINE OUTPUT PARAMETER pAmount AS DECIMAL NO-UNDO.
  
  {lib/getclaimapprovereserve.i pClaimID pCategory pAmount}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimReserveApprover) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimReserveApprover Procedure 
PROCEDURE GetClaimReserveApprover :
/*------------------------------------------------------------------------------
@description Get the users allowed to approve the reserve
@note The reserve balance is accumlative per claim meaning that a user with a
      limit of 5000 can approve a reserve adjustment for 3000 but not a second
      one for 3000 as the limit is only 2000 (5000-3000)
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pClaimID AS INTEGER NO-UNDO.
  DEFINE INPUT PARAMETER pRefCategory AS CHARACTER NO-UNDO.
  DEFINE INPUT PARAMETER pAmount AS DECIMAL NO-UNDO.
  DEFINE OUTPUT PARAMETER pUserList AS CHARACTER NO-UNDO.
  
  DEFINE VARIABLE cRefDesc AS CHARACTER NO-UNDO.
  define variable cSysUserID as character no-undo.
  define variable cSysUserName as character no-undo.
  
  /* buffer */
  DEFINE BUFFER sysprop for sysprop.
  
  PUBLISH "GetCategoryDesc" (pRefCategory, OUTPUT cRefDesc).
  
  publish "GetSysProps" (output table sysprop).
  for each sysprop no-lock
     where sysprop.appCode = "CLM"
       and sysprop.objAction = "ClaimAdjustmentRequest"
       and sysprop.objProperty = cRefDesc + "ReserveLimit"
       and sysprop.objValue <> "-1"
        by decimal(sysprop.objValue):
       
    cSysUserID = addDelimiter(cSysUserID,",") + sysprop.objID.
    cSysUserName = addDelimiter(cSysUserName,",") + sysprop.objName.
  end.
  for each sysprop no-lock
     where sysprop.appCode = "CLM"
       and sysprop.objAction = "ClaimAdjustmentRequest"
       and sysprop.objProperty = cRefDesc + "ReserveLimit"
       and sysprop.objValue = "-1":
       
    cSysUserID = addDelimiter(cSysUserID,",") + sysprop.objID.
    cSysUserName = addDelimiter(cSysUserName,",") + sysprop.objName.
  end.
  do std-in = 1 to num-entries(cSysUserID):
    assign
      std-ch = entry(std-in,cSysUserID)
      std-de = 0
      .
    {lib/getclaimreservelimit.i pClaimID pRefCategory std-ch std-de}.
    if std-de >= pAmount
     THEN pUserList = addDelimiter(pUserList,",") + entry(std-in,cSysUserName) + "," + std-ch.
  END.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimReserveLimit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimReserveLimit Procedure 
PROCEDURE GetClaimReserveLimit :
/*------------------------------------------------------------------------------
@description Get the reserve limit for the user
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pClaimID AS INTEGER NO-UNDO.
  DEFINE INPUT PARAMETER pRefCategory AS CHARACTER NO-UNDO.
  define input parameter pUser as character no-undo.
  DEFINE OUTPUT PARAMETER pAmount AS DECIMAL NO-UNDO.
  
  {lib/getclaimreservelimit.i pClaimID pRefCategory pUser pAmount}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimState Procedure 
PROCEDURE GetClaimState :
/*------------------------------------------------------------------------------
@description Get the reserve limit for the user
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pState as character no-undo.
  
  define buffer claim for claim.
  
  if not lIsClaimLoaded
   then run LoadClaim.
   
  for first claim no-lock
      where claim.claimID = pClaimID:
    
    pState = claim.stateID.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetClaimType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetClaimType Procedure 
PROCEDURE GetClaimType :
/*------------------------------------------------------------------------------
@description Get the reserve limit for the user
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pType as character no-undo.
  
  define buffer claim for claim.
  
  if not lIsClaimLoaded
   then run LoadClaim.
   
  for first claim no-lock
      where claim.claimID = pClaimID:
    
    pType = claim.type.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-HasClaimReserveApproved) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HasClaimReserveApproved Procedure 
PROCEDURE HasClaimReserveApproved :
/*------------------------------------------------------------------------------
@description Determines is the claim is has an approval or not
------------------------------------------------------------------------------*/
  def input parameter pClaimID as int no-undo.
  def input parameter pCategory as char no-undo.
  def output parameter lIsApproved as logical no-undo.
  
  lIsApproved = can-find(first claimadjreq no-lock
                         where claimadjreq.claimID = pClaimID
                           and claimadjreq.refCategory = pCategory
                           and claimadjreq.uid <> "").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IsClaimClosed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsClaimClosed Procedure 
PROCEDURE IsClaimClosed :
/*------------------------------------------------------------------------------
@description True if the status is closed
------------------------------------------------------------------------------*/
  define input parameter pClaimID as integer no-undo.
  define output parameter pIsClosed as logical no-undo.
  
  define buffer claim for claim.
  
  if not lIsClaimLoaded
   then run LoadClaim.
   
  for first claim no-lock
      where claim.claimID = pClaimID:
    
    pIsClosed = if claim.stat = "C" then true else false.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IsClaimReserveApproved) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsClaimReserveApproved Procedure 
PROCEDURE IsClaimReserveApproved :
/*------------------------------------------------------------------------------
@description Determines is the claim is fully
------------------------------------------------------------------------------*/
  def input parameter pClaimID as int no-undo.
  def input parameter pCategory as char no-undo.
  def input parameter pDate as datetime no-undo.
  def output parameter lIsApproved as logical no-undo.
  
  lIsApproved = can-find(first claimadjtrx no-lock
                         where claimadjtrx.claimID = pClaimID
                           and claimadjtrx.refCategory = pCategory
                           and claimadjtrx.transDate = pDate).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IsClaimReserveApprover) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsClaimReserveApprover Procedure 
PROCEDURE IsClaimReserveApprover :
/*------------------------------------------------------------------------------
@description Determines if the current user is the approver for the reserve
------------------------------------------------------------------------------*/
  def input parameter pClaimID as int no-undo.
  def input parameter pCategory as char no-undo.
  def output parameter lIsApprover as logical no-undo.
  
  lIsApprover = can-find(first claimadjreq  no-lock
                         where claimadjreq.claimID = pClaimID
                           and claimadjreq.refCategory = pCategory
                           and claimadjreq.uid = cUser)
                or
                can-find(first sysprop no-lock
                         where sysprop.appCode = "CLM"
                           and sysprop.objAction = "ClaimAdjustmentRequest"
                           and sysprop.objID = cUser
                           and sysprop.objValue = "-1").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaim Procedure 
PROCEDURE LoadClaim :
/*------------------------------------------------------------------------------
@description Load the claim from the server
------------------------------------------------------------------------------*/

  refreshClaim().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaimAccounting) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaimAccounting Procedure 
PROCEDURE LoadClaimAccounting :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/

  {lib/pbshow.i "''"}

  {lib/pbupdate.i "'Retrieving Reserves'" 10}
  run LoadClaimReserves in this-procedure.

  {lib/pbupdate.i "'Retrieving Coverages'" 25}
  refreshClaimCoverages().

  {lib/pbupdate.i "'Retrieving Payables'" 50}
  run LoadPayableInvoices in this-procedure.

  {lib/pbupdate.i "'Retrieving Receivables'" 75}
  run LoadReceivableInvoices in this-procedure.

  {lib/pbupdate.i "'Preparation Complete'" 100}
  {lib/pbhide.i}

  assign
    lIsAccountingLoaded = true
    tLastSync = now
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaimAccountingTab) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaimAccountingTab Procedure 
PROCEDURE LoadClaimAccountingTab :
/*------------------------------------------------------------------------------
@description Gets the data for the four browse widgets for the Accounting screen
------------------------------------------------------------------------------*/
  empty temp-table claimaccttab.

  run server/getclaimaccounting.p (input pBaseClaimID,
                                   output table claimaccttab,
                                   output std-lo,
                                   output std-ch
                                   ).
  if not std-lo and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
   else
    assign
      lIsAccountingTabLoaded = true
      tLastSync = now
      .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaimAttributes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaimAttributes Procedure 
PROCEDURE LoadClaimAttributes :
/*------------------------------------------------------------------------------
@description Load the claim attributes from the server
------------------------------------------------------------------------------*/ 
  empty temp-table claimattr.
  run server/getclaimattr.p (input pBaseClaimID,
                             output table claimattr,
                             output std-lo,
                             output std-ch
                             ).
  if not std-lo and std-ch > ""
   then message std-ch view-as alert-box error buttons ok.
   else tLastSync = now.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaimLinks) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaimLinks Procedure 
PROCEDURE LoadClaimLinks :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshClaimLinks().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaimNotes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaimNotes Procedure 
PROCEDURE LoadClaimNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshClaimNotes().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadClaimReserves) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadClaimReserves Procedure 
PROCEDURE LoadClaimReserves :
/*------------------------------------------------------------------------------
@description Get the claim reserve based on the type entered
------------------------------------------------------------------------------*/
  
  run server/getclaimreserves.p (input pBaseClaimID,
                                 output table claimadjreq,
                                 output table claimadjtrx,
                                 output std-lo,
                                 output std-ch
                                 ).
                                 
  if not std-lo and std-ch > ""
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaim Procedure 
PROCEDURE ModifyClaim :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaim.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claim for claim.
def buffer tempclaim for tempclaim.


  find tempclaim 
    where tempclaim.claimID = pBaseClaimID no-error.
  if not avail tempclaim 
   then
    do: 
        pMsg = "Updated data is not available.".
        return.
    end.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not avail claim 
   then
    do: 
      pMsg = "Source data is not available.".
      return.
    end.

  run server/modifyclaim.p (input table tempclaim,
                            output pSuccess,
                            output pMsg).
  if not pSuccess
   then return.                     

  buffer-copy tempclaim to claim.
/*   refreshClaimNotes(). */
  PUBLISH "RefreshNotes".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimBond) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimBond Procedure 
PROCEDURE ModifyClaimBond :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaimbond.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimbond for claimbond.
def buffer tempclaimbond for tempclaimbond.

  find first tempclaimbond no-error.
  if not avail tempclaimbond then
  do: 
    pSuccess = false.
    pMsg = "Passed claim bond record does not exist.".
    return.
  end.

  find claimbond where claimbond.claimID = tempclaimbond.claimID
                 and claimbond.bondID = tempclaimbond.bondID 
                 no-error.
  if not avail claimbond then
  do: 
    pSuccess = false.
    pMsg = "Claim bond " + 
           string(tempclaimbond.claimID) + "-" + string(tempclaimbond.bondID) + 
           " does not exist.".
    return.
  end.

  run server/modifyclaimbond.p (input table tempclaimbond,
                                output pSuccess,
                                output pMsg).
  if not pSuccess
  then return.                     

  buffer-copy tempclaimbond to claimbond.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimCode Procedure 
PROCEDURE ModifyClaimCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaimcode.
def input parameter pDirection as char.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimcode for claimcode.
def buffer tempclaimcode for tempclaimcode.

  find first tempclaimcode no-error.
  if not avail tempclaimcode then
  do: 
    pSuccess = false.
    pMsg = "Passed claim code record does not exist.".
    return.
  end.

  find first claimcode where claimcode.claimID = tempclaimcode.claimID
                         and claimcode.codeType = tempclaimcode.codeType
                         and claimcode.code = tempclaimcode.code
                         and claimcode.seq = tempclaimcode.seq no-error.
  if not avail claimcode then
  do: 
    pSuccess = false.
    pMsg = "Claim code record for claim " + string(tempclaimcode.ClaimID) 
         + ", code type " + tempclaimcode.CodeType 
         + ", code " + tempclaimcode.Code  
         + ", seq " + string(tempclaimcode.Seq) + " does not exist.".
    return.
  end.

  run server/modifyclaimcode.p (input tempclaimcode.claimID,
                                input tempclaimcode.codeType,
                                input tempclaimcode.code,
                                input tempclaimcode.seq,
                                input pDirection,
                                output pSuccess,
                                output pMsg).

  if not pSuccess
  then return.

  refreshclaimcodes().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimContact) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimContact Procedure 
PROCEDURE ModifyClaimContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaimcontact.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimcontact for claimcontact.
def buffer tempclaimcontact for tempclaimcontact.

  find first tempclaimcontact no-error.
  if not avail tempclaimcontact then
  do: 
    pSuccess = false.
    pMsg = "Passed claim contact record does not exist.".
    return.
  end.

  find claimcontact where claimcontact.claimID = tempclaimcontact.claimID
                    and claimcontact.contactID = tempclaimcontact.contactID 
                    no-error.
  if not avail claimcontact then
  do: 
    pSuccess = false.
    pMsg = "Claim contact " + 
           string(tempclaimcontact.claimID) + "-" + string(tempclaimcontact.contactID) + 
           " does not exist.".
    return.
  end.

  run server/modifyclaimcontact.p (input table tempclaimcontact,
                                   output pSuccess,
                                   output pMsg).
  if not pSuccess
  then return.                     

  publish "GetContactRoleDesc" (input  tempclaimcontact.role, 
                                output tempclaimcontact.roleDesc).
  
  buffer-copy tempclaimcontact to claimcontact.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimCoverage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimCoverage Procedure 
PROCEDURE ModifyClaimCoverage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaimcoverage.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimcoverage for claimcoverage.
def buffer tempclaimcoverage for tempclaimcoverage.

  find first tempclaimcoverage no-error.
  if not avail tempclaimcoverage then
  do: 
    pSuccess = false.
    pMsg = "Passed claim coverage record does not exist.".
    return.
  end.

  find claimcoverage where claimcoverage.claimID = tempclaimcoverage.claimID
                     and claimcoverage.seq = tempclaimcoverage.seq 
                     no-error.
  if not avail claimcoverage then
  do: 
    pSuccess = false.
    pMsg = "Claim coverage " + 
           string(tempclaimcoverage.claimID) + "-" + string(tempclaimcoverage.seq) + 
           " does not exist.".
    return.
  end.

  run server/modifyclaimcoverage.p (input table tempclaimcoverage,
                                    output pSuccess,
                                    output pMsg).
  if not pSuccess
  then return.                     
  
  publish "GetCoverageTypeDesc" (input  tempclaimcoverage.coverageType, 
                                 output tempclaimcoverage.coverageTypeDesc).
  publish "GetInsuredTypeDesc"  (input tempclaimcoverage.insuredType, 
                                 output tempclaimcoverage.insuredTypeDesc).

  buffer-copy tempclaimcoverage to claimcoverage.
  empty temp-table claimcoverage.
  publish "RefreshCoverages".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimLitigation) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimLitigation Procedure 
PROCEDURE ModifyClaimLitigation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaimlitigation.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimlitigation for claimlitigation.
def buffer tempclaimlitigation for tempclaimlitigation.

  find first tempclaimlitigation no-error.
  if not avail tempclaimlitigation then
  do: 
    pSuccess = false.
    pMsg = "Passed claim litigation record does not exist.".
    return.
  end.

  find claimLitigation where claimlitigation.claimID = tempclaimlitigation.claimID
                       and claimlitigation.litigationID = tempclaimlitigation.litigationID 
                       no-error.
  if not avail claimlitigation then
  do: 
    pSuccess = false.
    pMsg = "Claim litigation " + 
           string(tempclaimlitigation.claimID) + "-" + string(tempclaimlitigation.litigationID) + 
           " does not exist.".
    return.
  end.

  run server/modifyclaimlitigation.p (input table tempclaimlitigation,
                                      output pSuccess,
                                      output pMsg).
  if not pSuccess
  then return.                     

  buffer-copy tempclaimlitigation to claimlitigation.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimNote) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimNote Procedure 
PROCEDURE ModifyClaimNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for userclaimnote.
  
  define buffer claimnote for claimnote.
  define buffer userclaimnote for userclaimnote.
  
  for each claimnote, each userclaimnote exclusive-lock
     where claimnote.claimID = userclaimnote.claimID
       and claimnote.seq = userclaimnote.seq:
       
    std-ch = "".
    buffer-compare userclaimnote to claimnote case-sensitive save result in std-ch.
    if std-ch > ""
     then
      do:
        empty temp-table tempclaimnote.
        create tempclaimnote.
        buffer-copy userclaimnote to tempclaimnote.
        run server/modifyclaimnote.p (input table tempclaimnote,
                                      output std-lo,
                                      output std-ch).

        if not std-lo 
         then message std-ch view-as alert-box error buttons ok.
      end.
  end.
  run LoadClaimNotes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimProperty) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimProperty Procedure 
PROCEDURE ModifyClaimProperty :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter table for tempclaimproperty.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claimproperty for claimproperty.
def buffer tempclaimproperty for tempclaimproperty.

  find first tempclaimproperty no-error.
  if not avail tempclaimproperty then
  do: 
    pSuccess = false.
    pMsg = "Passed claim property record does not exist.".
    return.
  end.

  find claimproperty where claimproperty.claimID = tempclaimproperty.claimID
                     and claimproperty.seq = tempclaimproperty.seq 
                     no-error.
  if not avail claimproperty then
  do: 
    pSuccess = false.
    pMsg = "Claim property " + 
           string(tempclaimproperty.claimID) + "-" + string(tempclaimproperty.seq) + 
           " does not exist.".
    return.
  end.

  run server/modifyclaimproperty.p (input table tempclaimproperty,
                                    output pSuccess,
                                    output pMsg).
  if not pSuccess
  then return.                     

  buffer-copy tempclaimproperty to claimproperty.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyClaimReserve Procedure 
PROCEDURE ModifyClaimReserve :
/*------------------------------------------------------------------------------
@description Modify a claim reserve
------------------------------------------------------------------------------*/
  def input parameter table for tempclaimadjreq.
  def output parameter pSuccess as logical.
  
  def var cCompare as char no-undo init "".
  
  def buffer claimadjreq for claimadjreq.
  def buffer tempclaimadjreq for tempclaimadjreq.
  
  /* find the correct reserve */
  pSuccess = true.
  TRX-BLOCK:
  for each claimadjreq, first tempclaimadjreq exclusive-lock
     where claimadjreq.claimID = tempclaimadjreq.claimID 
       and claimadjreq.refCategory = tempclaimadjreq.refCategory TRANSACTION
      on error undo TRX-BLOCK, leave TRX-BLOCK:
      
    buffer-compare claimadjreq to tempclaimadjreq save result in cCompare.
    if cCompare > ""
     then
      do:
        run server/modifyclaimadjreq.p (input tempclaimadjreq.claimID,
                                        input tempclaimadjreq.refCategory,
                                        input tempclaimadjreq.requestedAmount,
                                        input tempclaimadjreq.uid,
                                        input tempclaimadjreq.notes,
                                        output pSuccess,
                                        output std-ch
                                        ).
        if pSuccess
         then buffer-copy tempclaimadjreq to claimadjreq.
         else 
          if std-ch > ""
           then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      end. /* if cCompare > "" */
  end. /* for first tempclaimadjreq */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimAttribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimAttribute Procedure 
PROCEDURE NewClaimAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pCode as character no-undo.
  define input parameter pName as character no-undo.
  define input parameter pValue as character no-undo.
  define output parameter pSuccess as logical no-undo.
 
  run server/newclaimattr.p (input pBaseClaimID,
                             input pCode,
                             input pName,
                             input pValue,
                             output pSuccess,
                             output std-ch).

   if not pSuccess and std-ch > ""
    then message std-ch view-as alert-box error buttons ok.

  /* Refresh the claim attributes temp-tables */
  empty temp-table claimattr no-error.
  empty temp-table tempclaimattr no-error.
  run GetClaimAttributes (output table tempclaimattr).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimBond) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimBond Procedure 
PROCEDURE NewClaimBond :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimbond.
 def output parameter pBondID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimbond for tempclaimbond.

  find first tempclaimbond no-error.
  if not avail tempclaimbond then
  do: 
    pSuccess = false.
    pMsg = "Passed claim bond record does not exist.".
    return.
  end.
 
  for first tempclaimbond:
   run server/newclaimbond.p (input table tempclaimbond,
                              output pBondID,
                              output pSuccess,
                              output pMsg).

   if not pSuccess 
    then return.
    
   tempclaimbond.bondID = pBondID.
  end.

  refreshclaimbonds().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimCode Procedure 
PROCEDURE NewClaimCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimcode.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimcode for tempclaimcode.

  find first tempclaimcode no-error.
  if not avail tempclaimcode then
  do: 
    pSuccess = false.
    pMsg = "Passed claim code record does not exist.".
    return.
  end.
 
  for first tempclaimcode:
   run server/newclaimcode.p (input table tempclaimcode,
                              output pSuccess,
                              output pMsg).

   if not pSuccess 
    then return.
  end.

  refreshclaimcodes().
  publish "RefreshNotes".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimContact) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimContact Procedure 
PROCEDURE NewClaimContact :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimcontact.
 def output parameter pContactID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimcontact for tempclaimcontact.

  find first tempclaimcontact no-error.
  if not avail tempclaimcontact then
  do: 
    pSuccess = false.
    pMsg = "Passed claim contact record does not exist.".
    return.
  end.
 
  for first tempclaimcontact:
   run server/newclaimcontact.p (input table tempclaimcontact,
                                 output pContactID,
                                 output pSuccess,
                                 output pMsg).

   if not pSuccess 
    then return.
    
   tempclaimcontact.contactID = pContactID.
  end.

  refreshclaimcontacts().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimCoverage) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimCoverage Procedure 
PROCEDURE NewClaimCoverage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimcoverage.
 def output parameter pSeq as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimcoverage for tempclaimcoverage.

  find first tempclaimcoverage no-error.
  if not avail tempclaimcoverage then
  do: 
    pSuccess = false.
    pMsg = "Passed claim coverage record does not exist.".
    return.
  end.
 
  for first tempclaimcoverage:
   run server/newclaimcoverage.p (input table tempclaimcoverage,
                                  output pSeq,
                                  output pSuccess,
                                  output pMsg).

   if not pSuccess 
    then return.
    
   tempclaimcoverage.seq = pSeq.
  end.

  refreshclaimcoverages().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimLink) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimLink Procedure 
PROCEDURE NewClaimLink :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimlink.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimlink for tempclaimlink.

  find first tempclaimlink no-error.
  if not avail tempclaimlink then
  do: 
    pSuccess = false.
    pMsg = "Passed claim link record does not exist.".
    return.
  end.
 
  for first tempclaimlink:
   run server/newclaimlink.p (input table tempclaimlink,
                              output pSuccess,
                              output pMsg).

   if not pSuccess 
    then return.
  end.

  refreshclaimlinks().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimLitigation) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimLitigation Procedure 
PROCEDURE NewClaimLitigation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimlitigation.
 def output parameter pLitigationID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimlitigation for tempclaimlitigation.

  find first tempclaimlitigation no-error.
  if not avail tempclaimlitigation then
  do: 
    pSuccess = false.
    pMsg = "Passed claim litigation record does not exist.".
    return.
  end.
 
  for first tempclaimlitigation:
   run server/newclaimlitigation.p (input table tempclaimlitigation,
                                    output pLitigationID,
                                    output pSuccess,
                                    output pMsg).

   if not pSuccess 
    then return.
    
   tempclaimlitigation.litigationID = pLitigationID.
  end.

  refreshclaimlitigations().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimNote) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimNote Procedure 
PROCEDURE NewClaimNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tempclaimnote.
  define output parameter pSuccess as logical.
  
  define buffer tempclaimnote for tempclaimnote.
  
  for first tempclaimnote exclusive-lock:
    run server/newclaimnote.p (input table tempclaimnote,
                               output pSuccess,
                               output std-ch).

   if not pSuccess and std-ch > ""
    then message std-ch view-as alert-box error buttons ok.
    else run LoadClaimNotes.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimProperty) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimProperty Procedure 
PROCEDURE NewClaimProperty :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter table for tempclaimproperty.
 def output parameter pSeq as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempclaimproperty for tempclaimproperty.

  find first tempclaimproperty no-error.
  if not avail tempclaimproperty then
  do: 
    pSuccess = false.
    pMsg = "Passed claim property record does not exist.".
    return.
  end.
 
  for first tempclaimproperty:
   run server/newclaimproperty.p (input table tempclaimproperty,
                                  output pSeq,
                                  output pSuccess,
                                  output pMsg).

   if not pSuccess 
    then return.
    
   tempclaimproperty.seq = pSeq.
  end.

  refreshclaimproperties().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewClaimReserve Procedure 
PROCEDURE NewClaimReserve :
/*------------------------------------------------------------------------------
@description Create a claim reserve
------------------------------------------------------------------------------*/
  define input parameter table for tempclaimadjreq.
  define output parameter pSuccess as logical.
  
  define variable cUsername as character no-undo.
  define variable lApproved as logical no-undo.
  
  define buffer tempclaimadjreq for tempclaimadjreq.
  
  /* create the new request */
  assign
    lApproved = false
    pSuccess = false
    std-lo = false
    .
  TRX-BLOCK:
  for first tempclaimadjreq exclusive-lock TRANSACTION
    on error undo TRX-BLOCK, leave TRX-BLOCK:
      
    run CanReserveCoverOpenInvoices(input tempclaimadjreq.claimID,
                                    input tempclaimadjreq.refCategory,
                                    input tempclaimadjreq.requestedAmount,
                                    output std-lo).
                                    
    if not std-lo
     then leave TRX-BLOCK.
      
    /* make sure that there is not already a pending request */
    if can-find(first claimadjreq where refCategory = tempclaimadjreq.refCategory)
     then
      do:
        message "There is already a pending " + getReserveDesc(tempclaimadjreq.refCategory) + " Reserve Adjustment." view-as alert-box information buttons ok. 
        leave TRX-BLOCK.
      end.
      
    run server/newclaimadjreq.p (input tempclaimadjreq.claimID,
                                 input tempclaimadjreq.refCategory,
                                 input tempclaimadjreq.requestedAmount,
                                 input tempclaimadjreq.uid,
                                 input tempclaimadjreq.notes,
                                 output lApproved,
                                 output pSuccess,
                                 output std-ch
                                 ).
    if pSuccess
     then
      do:
        /* get the user's name */
        publish "GetSysUserName" (tempclaimadjreq.uid, output cUsername).
        if lApproved
         then
          do:
            /* we need to go straight to creating an adjustment transaction as */
            /* the user approved the request automatically */
            create claimadjtrx.
            assign
              claimadjtrx.claimID = tempclaimadjreq.claimID
              claimadjtrx.refCategory = tempclaimadjreq.refCategory
              claimadjtrx.requestedAmount = tempclaimadjreq.requestedAmount
              claimadjtrx.uid = tempclaimadjreq.uid
              claimadjtrx.username = cUsername
              claimadjtrx.dateRequested = tempclaimadjreq.dateRequested
              claimadjtrx.notes = (if tempclaimadjreq.notes > "" 
                                    then tempclaimadjreq.notes
                                    else "Automatically approved due to user's limit")
              claimadjtrx.transDate = now
              claimadjtrx.transAmount = tempclaimadjreq.requestedAmount
              claimadjtrx.requestedBy = cUser
              claimadjtrx.requestedUsername = cName
              .
          end.
         else
          do:
            run SendClaimReserve in this-procedure (tempclaimadjreq.claimID, tempclaimadjreq.refCategory, tempclaimadjreq.uid, output std-lo).
            assign
              tempclaimadjreq.requestedBy = cUser
              tempclaimadjreq.requestedUsername = cName
              tempclaimadjreq.dateRequested = now
              tempclaimadjreq.username = cUsername
              .
            create claimadjreq.
            buffer-copy tempclaimadjreq to claimadjreq.
          end.
          publish "RefreshNotes".
      end.
     else 
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end. /* for first tempclaimadjreq */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-QUIT) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE QUIT Procedure 
PROCEDURE QUIT :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 delete procedure this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ReduceLiability) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReduceLiability Procedure 
PROCEDURE ReduceLiability :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pInvoiceID as integer no-undo.
  define input parameter pLiability as logical no-undo.
  define output parameter pSuccess as logical.
  
  define buffer apinv for apinv.
  define buffer claimcoverage for claimcoverage.
  
  pSuccess = false.        
  for first apinv exclusive-lock
      where apinv.apinvID = pInvoiceID
        and apinv.refCategory = "L":
    
    std-de = 0.
    run server/reduceclaimliability.p (input pInvoiceID,
                                       input pLiability,
                                       output pSuccess,
                                       output std-ch
                                       ).
                                       
    if pSuccess  
     then 
      do:
        apinv.reduceLiability = pLiability.
        refreshClaimCoverages().
        publish "RefreshCoverages".
      end.
     else MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-RejectClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RejectClaimReserve Procedure 
PROCEDURE RejectClaimReserve :
/*------------------------------------------------------------------------------
@description Rejects the claim adjustment request 
------------------------------------------------------------------------------*/
  define input  parameter pClaimID     as integer   no-undo.
  define input  parameter pRefCategory as character no-undo.
  define output parameter pSuccess     as logical   no-undo.
  
  def buffer claimadjreq for claimadjreq.
  
    run server/rejectclaimadjreq.p (input pClaimID,
                                    input pRefCategory,
                                    output pSuccess,
                                    output std-ch
                                    ).
  
    if pSuccess
     then
     for first claimadjreq exclusive-lock
         where claimadjreq.claimID = pClaimID
           and claimadjreq.refCategory = pRefCategory:
        
        delete claimadjreq.
        publish "RefreshNotes".
      end.
     else
      if std-ch > ""
       then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SendClaimReserve) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SendClaimReserve Procedure 
PROCEDURE SendClaimReserve :
/*------------------------------------------------------------------------------
@description Send a claim reserve for approval
------------------------------------------------------------------------------*/
  define input  parameter pClaimID  as integer   no-undo.
  define input  parameter pCategory as character no-undo.
  define input  parameter pUser     as character no-undo.
  define output parameter pSuccess  as logical   no-undo initial true.
  
  /* get the sysprops */
  if not can-find(first sysprop)
   then publish "GetSysProps" (output table sysprop).
   
  /* automatically send an email to the approver */
  std-lo = false.
  for first sysprop no-lock
      where sysprop.appCode = "CLM"
        and sysprop.objAction = "SendEmail"
        and sysprop.objProperty = "ReserveApproval"
        and sysprop.objID = pUser
        and sysprop.objValue = "TRUE":
    
    std-lo = true.
  end.
  
  if not std-lo
   then return.
  
  run server/sendclaimreserve.p (input pClaimID,
                                 input pCategory,
                                 output pSuccess,
                                 output std-ch
                                 ).
                                 
  if not pSuccess and std-ch > ""    
   then MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetAssignee) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetAssignee Procedure 
PROCEDURE SetAssignee :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pUid as char.
def input parameter pNotes as char.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claim for claim.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not avail claim 
   then return.

  run server/setclaimassignee.p (input pBaseClaimID,
                                 input pUid,
                                 input pNotes,
                                 output pSuccess,
                                 output pMsg).
  if not pSuccess
   then return.                     

  claim.assignedTo = pUid.
/*   refreshClaimNotes(). */
  publish "RefreshNotes".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetClosed) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetClosed Procedure 
PROCEDURE SetClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pNote as char.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claim for claim.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not avail claim 
   then return.

  if (avail claim and claim.stat = "C")
   then
    do: pSuccess = true.
        return.
    end.

  run server/setclaimclosed.p (input pBaseClaimID,
                               input pNote,
                               output pSuccess,
                               output pMsg).
  if not pSuccess
   then return.                     


  publish "GetCredentialsID" (output std-ch).

  claim.stat = "C".
  if claim.dateClosed = ? 
   then assign
          claim.dateClosed = now
          claim.userClosed = std-ch
          .
   else assign
          claim.dateReClosed = now
          claim.userReClosed = std-ch
          .

/*   refreshClaimNotes(). */
  publish "RefreshNotes".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetOpen) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetOpen Procedure 
PROCEDURE SetOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pNote as char.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claim for claim.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not avail claim 
   then return.

  if (avail claim and claim.stat = "O")
   then
    do: pSuccess = true.
        return.
    end.

  run server/setclaimopen.p (input pBaseClaimID,
                             input pNote,
                             output pSuccess,
                             output pMsg).
  if not pSuccess
   then return.                     

  publish "GetCredentialsID" (output std-ch).

  claim.stat = "O".
  if claim.dateReOpened = ? 
   then assign
          claim.dateReopened = now
          claim.userReopened = std-ch
          .

/*   refreshClaimNotes(). */
  publish "RefreshNotes".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetToClaim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetToClaim Procedure 
PROCEDURE SetToClaim :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claim for claim.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not avail claim 
   then return.

  if (avail claim and claim.type = "C")
   then
    do: pSuccess = true.
        return.
    end.

  run server/setclaimtypeclaim.p (input pBaseClaimID,
                                output pSuccess,
                                output pMsg).
  if not pSuccess
   then return.                     

  claim.type = "C".
/*   refreshClaimNotes(). */
  publish "RefreshNotes".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetToMatter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetToMatter Procedure 
PROCEDURE SetToMatter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.

def buffer claim for claim.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not avail claim 
   then return.

  if (avail claim and claim.type = "M")
   then
    do: pSuccess = true.
        return.
    end.

  run server/setclaimtypematter.p (input pBaseClaimID,
                                 output pSuccess,
                                 output pMsg).
  if not pSuccess
   then return.                     

  claim.type = "M".
/*   refreshClaimNotes(). */
  publish "RefreshNotes".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-refreshClaim) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaim Procedure 
FUNCTION refreshClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claim datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaim for tempclaim.

 def buffer claim for claim.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.

 empty temp-table tempclaim.

 run server/getclaims.p (input pBaseClaimID,
                         output table tempclaim,
                         output tSuccess, 
                         output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 find first tempclaim
   where tempclaim.claimID = pBaseClaimID no-error.
 if not available tempclaim
  then return false.

 empty temp-table claim.

 create claim.
 buffer-copy tempclaim to claim.
 
 lIsClaimLoaded = true.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimBonds) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimBonds Procedure 
FUNCTION refreshClaimBonds RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimbond datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimbond for tempclaimbond.

 def buffer claimbond for claimbond.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimbond.

 run server/getclaimbond.p (pBaseClaimID,
                            0,           /* BondID - zero for all */
                            output table tempclaimbond,
                            output tSuccess, 
                            output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 empty temp-table claimbond.

 for each tempclaimbond:
  create claimbond.
  buffer-copy tempclaimbond to claimbond.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimCarriers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimCarriers Procedure 
FUNCTION refreshClaimCarriers RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimcarrier datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimcarrier for tempclaimcarrier.

 def buffer claimcarrier for claimcarrier.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimcarrier.

 run server/getclaimcarrier.p (pBaseClaimID,
                               0,           /* CarrierID - zero for all */
                               output table tempclaimcarrier,
                               output tSuccess, 
                               output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 empty temp-table claimcarrier.

 for each tempclaimcarrier:
  create claimcarrier.
  buffer-copy tempclaimcarrier to claimcarrier.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimCodes Procedure 
FUNCTION refreshClaimCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimcode datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimcode for tempclaimcode.

 def buffer claimcode for claimcode.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimcode.

 run server/getclaimcode.p (pBaseClaimID,
                            "",           /* CodeType - blank for all */
                            0,            /* Seq - zero for all */
                            output table tempclaimcode,
                            output tSuccess, 
                            output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 empty temp-table claimcode.

 for each tempclaimcode:
  create claimcode.
  buffer-copy tempclaimcode to claimcode.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimContacts) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimContacts Procedure 
FUNCTION refreshClaimContacts RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimcontact datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimcontact for tempclaimcontact.

 def buffer claimcontact for claimcontact.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimcontact.

 run server/getclaimcontact.p (pBaseClaimID,
                               0,           /* ContactID - zero for all */
                               output table tempclaimcontact,
                               output tSuccess, 
                               output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 for each tempclaimcontact:
    publish "GetContactRoleDesc" (input  tempclaimcontact.role, 
                                  output tempclaimcontact.roleDesc).
 end.
   
 empty temp-table claimcontact.

 for each tempclaimcontact:
  create claimcontact.
  buffer-copy tempclaimcontact to claimcontact.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimCoverages) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimCoverages Procedure 
FUNCTION refreshClaimCoverages RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimcoverage datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimcoverage for tempclaimcoverage.

 def buffer claimcoverage for claimcoverage.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimcoverage.

 run server/getclaimcoverage.p (pBaseClaimID,
                                0,            /* Sequence - zero for all */
                                "",           /* CoverageID - blank for all */
                                output table tempclaimcoverage,
                                output tSuccess, 
                                output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 for each tempclaimcoverage:
    publish "GetCoverageTypeDesc" (input  tempclaimcoverage.coverageType, 
                                   output tempclaimcoverage.coverageTypeDesc).
    publish "GetInsuredTypeDesc"  (input tempclaimcoverage.insuredType, 
                                   output tempclaimcoverage.insuredTypeDesc).
 end.
   
 empty temp-table claimcoverage.

 for each tempclaimcoverage:
  create claimcoverage.
  buffer-copy tempclaimcoverage to claimcoverage.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimLinks) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimLinks Procedure 
FUNCTION refreshClaimLinks RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimlink datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimlink for tempclaimlink.

 def buffer claimlink for claimlink.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimlink.

 run server/getclaimlink.p (pBaseClaimID, /* 'From' ClaimID - required */
                            0,            /* 'To' ClaimID - optional - zero for all */
                            output table tempclaimlink,
                            output tSuccess, 
                            output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 for each tempclaimlink:
    publish "GetLinkTypeDesc" (input  tempclaimlink.linkType, 
                               output tempclaimlink.linkTypeDesc).
    publish "GetStatDesc"     (input  tempclaimlink.stat, 
                               output tempclaimlink.statDesc).
    publish "GetTypeDesc"     (input  tempclaimlink.type, 
                               output tempclaimlink.typeDesc).
 end.

 empty temp-table claimlink.

 for each tempclaimlink:
  create claimlink.
  buffer-copy tempclaimlink to claimlink.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimLitigations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimLitigations Procedure 
FUNCTION refreshClaimLitigations RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimlitigation datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimlitigation for tempclaimlitigation.

 def buffer claimlitigation for claimlitigation.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimlitigation.

 run server/getclaimlitigation.p (pBaseClaimID,
                                  0,           /* LitigationID - zero for all */
                                  output table tempclaimlitigation,
                                  output tSuccess, 
                                  output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 empty temp-table claimlitigation.

 for each tempclaimlitigation:
  create claimlitigation.
  buffer-copy tempclaimlitigation to claimlitigation.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimNotes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimNotes Procedure 
FUNCTION refreshClaimNotes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimnote for tempclaimnote.

 def buffer claimnote for claimnote.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimnote.

 run server/getclaimnote.p (pBaseClaimID,
                            0,            /* Sequence - zero for all notes */
                            output table tempclaimnote,
                            output tSuccess, 
                            output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 empty temp-table claimnote.

 for each tempclaimnote:
  create claimnote.
  publish "GetNoteCategoryDesc" (tempclaimnote.category, output std-ch).
  tempclaimnote.categoryDesc = std-ch.
  buffer-copy tempclaimnote to claimnote.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshClaimProperties) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaimProperties Procedure 
FUNCTION refreshClaimProperties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Reload just the claimproperty datastructure
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempclaimproperty for tempclaimproperty.

 def buffer claimproperty for claimproperty.

 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.


 Empty temp-table tempclaimproperty.

 run server/getclaimproperty.p (pBaseClaimID,
                                0,            /* Sequence 0 for all */
                                output table tempclaimproperty,
                                output tSuccess, 
                                output tMsg).

 if not tSuccess
  then
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.     
   end.

 empty temp-table claimproperty.

 for each tempclaimproperty:
  create claimproperty.
  buffer-copy tempclaimproperty to claimproperty.
 end.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

