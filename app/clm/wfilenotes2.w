&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wfilenotes2.w
@desc Grid-based view of notes
@author D.Sinclair   
@date 12.29.2016
 */


CREATE WIDGET-POOL.

define input parameter pClaimID as int no-undo.
define input parameter hFileDataSrv as handle no-undo.

{lib/std-def.i}
{lib/set-button-def.i}
{tt/claimnote.i}
{tt/claimnote.i &tableAlias="changedNote"}

def temp-table usernote like claimnote
 field username as char
 .

{lib/winlaunch.i}


define variable cCategoryList as character no-undo.
define variable cDescription as character no-undo.
def var tCurrentUID as char no-undo.  
def var tModifyActive as logical init false.
define variable hNoteHistory as handle no-undo.

publish "GetCredentialsID" (output tCurrentUID).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES usernote

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData usernote.notedate usernote.username usernote.categorydesc usernote.subject   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH usernote
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH usernote.
&Scoped-define TABLES-IN-QUERY-brwData usernote
&Scoped-define FIRST-TABLE-IN-QUERY-brwData usernote


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bRefresh tDateUser tSubject ~
tNoteText bAddNote tClaimID tDescription bFilter bClear fSearch ~
tViewNoteCategory bNoteHistory RECT-36 RECT-37 
&Scoped-Define DISPLAYED-OBJECTS tDateUser tSubject tClaimID tDescription ~
fSearch tViewNoteCategory tCategory 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddNote  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new note".

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel changes to the note".

DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bModifyNote  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the note".

DEFINE BUTTON bNoteHistory  NO-FOCUS
     LABEL "History" 
     SIZE 7.2 BY 1.71 TOOLTIP "View the created notes history".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh notes".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save changes to the note".

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE tViewNoteCategory AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE tNoteText AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 96 BY 13.24
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 45 BY 1 NO-UNDO.

DEFINE VARIABLE tClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Claim ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDateUser AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 53 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 123 BY 1 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 77 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 115 BY 2.33.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 47.8 BY 2.33.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      usernote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      usernote.notedate label "Date" format "99/99/9999" width 14
usernote.username label "User" format "x(50)" width 15
usernote.categorydesc label "Category" format "x(30)" width 20
usernote.subject label "Subject" format "x(200)" width 50
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 115 BY 15.62 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 5.76 COL 2 WIDGET-ID 200
     bRefresh AT ROW 3.24 COL 147.6 WIDGET-ID 330 NO-TAB-STOP 
     bSpellCheck AT ROW 6 COL 206.2 WIDGET-ID 326 NO-TAB-STOP 
     tDateUser AT ROW 5.76 COL 150 COLON-ALIGNED NO-LABEL WIDGET-ID 324 NO-TAB-STOP 
     bModifyNote AT ROW 3.24 COL 126 WIDGET-ID 306 NO-TAB-STOP 
     tSubject AT ROW 6.95 COL 126 COLON-ALIGNED WIDGET-ID 320 NO-TAB-STOP 
     bCancel AT ROW 3.24 COL 140.4 WIDGET-ID 318 NO-TAB-STOP 
     bSave AT ROW 3.24 COL 133.2 WIDGET-ID 316 NO-TAB-STOP 
     tNoteText AT ROW 8.14 COL 118 NO-LABEL WIDGET-ID 312
     bAddNote AT ROW 3.24 COL 118.8 WIDGET-ID 224 NO-TAB-STOP 
     tClaimID AT ROW 1.52 COL 10 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     tDescription AT ROW 1.52 COL 38.2 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     bFilter AT ROW 3.52 COL 93 WIDGET-ID 308
     bClear AT ROW 3.52 COL 103 WIDGET-ID 68
     fSearch AT ROW 3.57 COL 45 COLON-ALIGNED WIDGET-ID 310
     tViewNoteCategory AT ROW 3.62 COL 11.4 COLON-ALIGNED WIDGET-ID 240 NO-TAB-STOP 
     tCategory AT ROW 5.76 COL 126 COLON-ALIGNED WIDGET-ID 322 NO-TAB-STOP 
     bNoteHistory AT ROW 3.24 COL 154.8 WIDGET-ID 332 NO-TAB-STOP 
     RECT-36 AT ROW 2.95 COL 2 WIDGET-ID 298
     RECT-37 AT ROW 2.95 COL 116.6 WIDGET-ID 314
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 214 BY 20.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "File Notes"
         HEIGHT             = 20.62
         WIDTH              = 214
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModifyNote IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSpellCheck IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tCategory IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tClaimID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tDateUser:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tDescription:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR EDITOR tNoteText IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNoteText:RETURN-INSERTED IN FRAME fMain  = TRUE
       tNoteText:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tSubject:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH usernote.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* File Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* File Notes */
DO:
  if tModifyActive
    and available usernote
    and (usernote.notes <> tNoteText:screen-value in frame {&frame-name}
       or usernote.category <> tCategory:screen-value
       or usernote.subject <> tSubject:screen-value)
   then
    do: message "Changes to the note will be lost.  Continue?"
          view-as alert-box buttons yes-no set std-lo.
        if not std-lo 
         then return no-apply.
    end.

  if valid-handle(hNoteHistory)
   then run CloseWindow in hNoteHistory.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* File Notes */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddNote C-Win
ON CHOOSE OF bAddNote IN FRAME fMain /* Add */
DO:
  run dialogaddnote.w (pClaimID, hFileDataSrv).
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  run cancelNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  assign
    fSearch:screen-value = ""
    tViewNoteCategory:screen-value = "ALL"
    .
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  run filterData in this-procedure.
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyNote C-Win
ON CHOOSE OF bModifyNote IN FRAME fMain /* Modify */
DO:
  run modifyNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNoteHistory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNoteHistory C-Win
ON CHOOSE OF bNoteHistory IN FRAME fMain /* History */
DO:
  if valid-handle(hNoteHistory)
   then run ShowWindow in hNoteHistory.
   else run dialognoteshistory.w persistent set hNoteHistory ("Claim", string(pClaimID)).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run RefreshNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
    /* das: Start editing... */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if available usernote 
   then 
    do: tNoteText:screen-value = usernote.notes.
        if lookup(usernote.category, tCategory:list-item-pairs) > 0 
         then tCategory:screen-value = usernote.category.
         else tCategory:screen-value = "".

        tSubject:screen-value = usernote.subject.
        tDateUser:screen-value = string(usernote.noteDate, "99/99/9999 HH:MM AM") + "  " + usernote.username.
        bModifyNote:sensitive = (usernote.uid = tCurrentUID and usernote.category <> "0").
    end.
   else 
    do: tNoteText:screen-value = "".
        tCategory:screen-value = "".
        tSubject:screen-value = "".
        tDateUser:screen-value = "".
        bModifyNote:sensitive = false.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  run saveNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck C-Win
ON CHOOSE OF bSpellCheck IN FRAME fMain /* Spell Check */
DO:
  ASSIGN tNoteText.
  run util/spellcheck.p ({&window-name}:handle, input-output tNoteText, output std-lo).
  display tNoteText with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewNoteCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewNoteCategory C-Win
ON VALUE-CHANGED OF tViewNoteCategory IN FRAME fMain /* Category */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{&window-name}:title = "File " + string(pclaimID) + " Notes".
{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

subscribe to "RefreshNotes" anywhere.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* bRefresh:load-image("images/completed.bmp").               */
/* bRefresh:load-image-insensitive("images/completed-i.bmp"). */
{lib/set-button.i &label="NoteHistory" &image="images/book.bmp"       &inactive="images/book-i.bmp"}
{lib/set-button.i &label="AddNote"     &image="images/blank-add.bmp"  &inactive="images/blank-i.bmp"}
{lib/set-button.i &label="SpellCheck"  &image="images/spellcheck.bmp" &inactive="images/spellcheck-i.bmp"}
{lib/set-button.i &label="ModifyNote"  &image="images/update.bmp"     &inactive="images/update.bmp"}
{lib/set-button.i &label="Save"}
{lib/set-button.i &label="Cancel"}
{lib/set-button.i &label="Refresh"}
setButtons().

/* note category */
publish "GetNoteCategoryList" (output cCategoryList).
tCategory:list-item-pairs = "Select,," + cCategoryList.
tCategory:screen-value = "".

if cCategoryList > ""
 then std-ch = "ALL,ALL," + cCategoryList.
 else std-ch = "ALL,ALL".
tViewNoteCategory:list-item-pairs = std-ch.
tViewNoteCategory:screen-value = "ALL".

/* claim description */
run GetClaimDescription in hFileDataSrv (pClaimID, output tDescription) no-error.

tClaimID = pClaimID.

session:immediate-display = yes.

run windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  run getData in this-procedure.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelNote C-Win 
PROCEDURE cancelNote PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 disable tCategory bSave bCancel bSpellCheck with frame {&frame-name}.
 tSubject:read-only in frame {&frame-name} = true.
 tNoteText:read-only in frame {&frame-name} = true.

 tNoteText:screen-value in frame {&frame-name} = usernote.notes.
 tCategory:screen-value = usernote.category.
 tSubject:screen-value = usernote.subject.

 enable bModifyNote bAddNote tViewNoteCategory fSearch bFilter bClear brwData
   with frame {&frame-name}.
 tModifyActive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDateUser tSubject tClaimID tDescription fSearch tViewNoteCategory 
          tCategory 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData bRefresh tDateUser tSubject tNoteText bAddNote tClaimID 
         tDescription bFilter bClear fSearch tViewNoteCategory bNoteHistory 
         RECT-36 RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.

 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).
 
 th = temp-table claimnote:handle.
 
 run util/exporttable.p (table-handle th,
                         "claimnote",
                         "for each claimnote by claimnote.notedate ",
                         "notedate,subject,uid,notes",
                         "Date,Subject,User,Notes",
                         std-ch,
                         "FileNotes" + string(pClaimID) + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer usernote for usernote.
 def buffer claimnote for claimnote.

 close query brwData.
 empty temp-table usernote.

 for each claimnote:
  /* test if the record is the category */
  if tViewNoteCategory:screen-value in frame {&frame-name} <> "ALL" 
    and claimnote.category <> tViewNoteCategory:screen-value
   then next.

  /* test if the record contains the search text */
  if fSearch:screen-value > "" 
    and not (claimnote.notes matches "*" + fSearch:screen-value + "*"
          or claimnote.subject matches "*" + fSearch:screen-value + "*")
   then next.

  create usernote.
  buffer-copy claimnote to usernote.

  publish "GetSysUserName" (claimnote.uid, output std-ch).
  if std-ch = "NOT FOUND"
   then std-ch = claimnote.uid.
  usernote.username = std-ch.

  publish "GetNoteCategoryDesc" (claimnote.category, output usernote.categorydesc).
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer claimnote for claimnote.

 empty temp-table claimnote.

 run GetClaimNotes in hFileDataSrv (output table claimnote).

 run filterData in this-procedure.

 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("notedate").
 
 apply "value-changed" to brwData in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyNote C-Win 
PROCEDURE modifyNote PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 enable 
   tCategory tSubject tNoteText bSave bCancel bSpellCheck 
   with frame {&frame-name}.
 tSubject:read-only in frame {&frame-name} = false.
 tNoteText:read-only in frame {&frame-name} = false .
 disable bModifyNote bAddNote tViewNoteCategory fSearch bFilter bClear brwData
   with frame {&frame-name}.
 tModifyActive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshNotes C-Win 
PROCEDURE RefreshNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  run LoadClaimNotes in hFileDataSrv. 
  run getData in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveNote C-Win 
PROCEDURE saveNote PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer xusernote for usernote.
 def var tSeq as int.

 if tCategory:screen-value in frame {&frame-name} = "0" 
   or tCategory:screen-value = ""
  then
   do: message "Category cannot be set to System or be blank." view-as alert-box error.
       return.
   end.

 disable tCategory bSave bCancel bSpellCheck with frame {&frame-name}.
 tSubject:read-only in frame {&frame-name} = true.
 tNoteText:read-only in frame {&frame-name} = true.

 empty temp-table changedNote.

 create changedNote.
 buffer-copy usernote to changedNote.
 changedNote.notes = tNoteText:screen-value in frame {&frame-name}.
 changedNote.subject = tSubject:screen-value.
 changedNote.category = tCategory:screen-value.

 run ModifyClaimNote in hFileDataSrv (table changedNote).
 
 tSeq = usernote.seq.
 run getData in this-procedure.

 find xusernote
   where xusernote.seq = tSeq no-error.
 if available xusernote 
  then reposition brwData to rowid rowid(xusernote) no-error.

 apply "value-changed" to brwData in frame {&frame-name}.

 enable bModifyNote bAddNote tViewNoteCategory fSearch bFilter bClear brwData
   with frame {&frame-name}.
 tModifyActive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
apply "value-changed" to brwData in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
 tNoteText:width-pixels = {&window-name}:width-pixels - 590.
 tNoteText:height-pixels = frame {&frame-name}:height-pixels - tNoteText:y - 5.
 tSubject:width-pixels = frame {&frame-name}:width-pixels - 685.
 bSpellCheck:x = frame {&frame-name}:width-pixels - 44.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  hide message no-pause.
  close query brwData.
  clear frame fMain.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

