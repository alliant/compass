&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* wfileview.w
@desc Window to view a file
@author B.Johnson
@Modified
08/07/19      Rahul     Modified to add new fields in Overview and Activity Tab
*/

CREATE WIDGET-POOL.

def input parameter pBaseClaimID as int.


{src/adm2/widgetprto.i}

{lib/std-def.i}
{lib/winlaunch.i}

def var tWinHandle as handle.
def var activePageNumber as int no-undo.
def var dataChanged as log no-undo init no.
def var newRecord as log no-undo init no.
def var hFileDataSrv as handle no-undo.
def var tPrimaryCvgSeq as int no-undo.
def var tCurrentCvgSeq as int no-undo.
def var tPrimaryPropSeq as int no-undo.
def var tCurrentPropSeq as int no-undo.
def var tCurrentContactID as int no-undo.
def var tCurrentLitigationID as int no-undo.

def var tOverviewLoaded as log init no.
def var tCoverageLoaded as log init no.
def var tPropertiesLoaded as log init no.
def var tContactsLoaded as log init no.
def var tAccountingLoaded as log init no.
def var tRecoveryLoaded as log init no.
def var tLitigationLoaded as log init no.
def var tLinkedClaimsLoaded as log init no.
def var tActivityLoaded as log init no.

def var tAgentErrorSubject as char init "".
def var tAttorneyErrorSubject as char init "".
def var tSeekingRecoverySubject as char init "".

def var tSeq as int no-undo.
def var tContactID as int no-undo.
def var tLitigationID as int no-undo.

{tt/state.i}
{tt/agent.i}
{tt/sysuser.i}

{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/claim.i}
{tt/claimadjreq.i}
{tt/claimacct.i}
{tt/claimcode.i}
{tt/claimcontact.i}
{tt/claimproperty.i}
{tt/claimcoverage.i}
{tt/claimcarrier.i}
{tt/claimbond.i}
{tt/claimlitigation.i}
{tt/claimlink.i}
{tt/claimnote.i}
{tt/task.i}
{tt/syscode.i}

{tt/claimcode.i &tableAlias="desccode"}
{tt/claimcode.i &tableAlias="causecode"}

{tt/claim.i &tableAlias="tempclaim"}
{tt/claimcode.i &tableAlias="tempclaimcode"}
{tt/claimcontact.i &tableAlias="tempclaimcontact"}
{tt/claimproperty.i &tableAlias="tempclaimproperty"}
{tt/claimcoverage.i &tableAlias="tempclaimcoverage"}
{tt/claimcarrier.i &tableAlias="tempclaimcarrier"}
{tt/claimbond.i &tableAlias="tempclaimbond"}
{tt/claimlitigation.i &tableAlias="tempclaimlitigation"}
{tt/claimlink.i &tableAlias="tempclaimlink"}
{tt/claimnote.i &tableAlias="tempclaimnote"}
{tt/task.i &tableAlias="temptask"}
{tt/syscode.i &tableAlias="tempsyscode"}

def buffer primclaimcoverage for claimcoverage.
def buffer primclaimproperty for claimproperty.

def var hNoteWindow as handle no-undo.
def var hDocWindow as handle no-undo.
def var hAcctWindow as handle no-undo.

def var xEffDate as datetime.
def var xLiabilityAmount as decimal.
def var xGrossPremium as decimal.
def var xFormID as char.
def var xStateID as char.
def var xFileNumber as char.
def var xStatCode as char.
def var xNetPremium as dec.
def var xRetention as dec.
def var xCountyID as char.
def var xResidential as log.
def var xInvoiceDate as date.

def var tMsgStat as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwBonds

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES claimbond causecode claimcontact ~
claimcoverage desccode claimlink claimlitigation claimproperty task

/* Definitions for BROWSE brwBonds                                      */
&Scoped-define FIELDS-IN-QUERY-brwBonds claimbond.type claimbond.ID claimbond.entity claimbond.effDate claimbond.amount   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBonds   
&Scoped-define SELF-NAME brwBonds
&Scoped-define QUERY-STRING-brwBonds FOR EACH claimbond by claimbond.type
&Scoped-define OPEN-QUERY-brwBonds OPEN QUERY {&SELF-NAME} FOR EACH claimbond by claimbond.type.
&Scoped-define TABLES-IN-QUERY-brwBonds claimbond
&Scoped-define FIRST-TABLE-IN-QUERY-brwBonds claimbond


/* Definitions for BROWSE brwCauseCodes                                 */
&Scoped-define FIELDS-IN-QUERY-brwCauseCodes causecode.code causecode.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCauseCodes   
&Scoped-define SELF-NAME brwCauseCodes
&Scoped-define QUERY-STRING-brwCauseCodes FOR EACH causecode by claimcode.seq
&Scoped-define OPEN-QUERY-brwCauseCodes OPEN QUERY {&SELF-NAME} FOR EACH causecode by claimcode.seq.
&Scoped-define TABLES-IN-QUERY-brwCauseCodes causecode
&Scoped-define FIRST-TABLE-IN-QUERY-brwCauseCodes causecode


/* Definitions for BROWSE brwContacts                                   */
&Scoped-define FIELDS-IN-QUERY-brwContacts claimcontact.roleDesc claimcontact.isActive   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwContacts   
&Scoped-define SELF-NAME brwContacts
&Scoped-define QUERY-STRING-brwContacts FOR EACH claimcontact by claimcontact.contactID
&Scoped-define OPEN-QUERY-brwContacts OPEN QUERY {&SELF-NAME} FOR EACH claimcontact by claimcontact.contactID.
&Scoped-define TABLES-IN-QUERY-brwContacts claimcontact
&Scoped-define FIRST-TABLE-IN-QUERY-brwContacts claimcontact


/* Definitions for BROWSE brwCoverage                                   */
&Scoped-define FIELDS-IN-QUERY-brwCoverage claimcoverage.coverageID claimcoverage.coverageTypeDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCoverage   
&Scoped-define SELF-NAME brwCoverage
&Scoped-define QUERY-STRING-brwCoverage FOR EACH claimcoverage by claimcoverage.coverageID
&Scoped-define OPEN-QUERY-brwCoverage OPEN QUERY {&SELF-NAME} FOR EACH claimcoverage by claimcoverage.coverageID.
&Scoped-define TABLES-IN-QUERY-brwCoverage claimcoverage
&Scoped-define FIRST-TABLE-IN-QUERY-brwCoverage claimcoverage


/* Definitions for BROWSE brwDescCodes                                  */
&Scoped-define FIELDS-IN-QUERY-brwDescCodes desccode.code desccode.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDescCodes   
&Scoped-define SELF-NAME brwDescCodes
&Scoped-define QUERY-STRING-brwDescCodes FOR EACH desccode by desccode.seq
&Scoped-define OPEN-QUERY-brwDescCodes OPEN QUERY {&SELF-NAME} FOR EACH desccode by desccode.seq.
&Scoped-define TABLES-IN-QUERY-brwDescCodes desccode
&Scoped-define FIRST-TABLE-IN-QUERY-brwDescCodes desccode


/* Definitions for BROWSE brwLinkedClaims                               */
&Scoped-define FIELDS-IN-QUERY-brwLinkedClaims claimlink.toClaimID claimlink.fileNumber claimlink.linkTypeDesc claimlink.typeDesc claimlink.statDesc   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwLinkedClaims   
&Scoped-define SELF-NAME brwLinkedClaims
&Scoped-define QUERY-STRING-brwLinkedClaims FOR EACH claimlink by claimlink.toClaimID
&Scoped-define OPEN-QUERY-brwLinkedClaims OPEN QUERY {&SELF-NAME} FOR EACH claimlink by claimlink.toClaimID.
&Scoped-define TABLES-IN-QUERY-brwLinkedClaims claimlink
&Scoped-define FIRST-TABLE-IN-QUERY-brwLinkedClaims claimlink


/* Definitions for BROWSE brwLitigation                                 */
&Scoped-define FIELDS-IN-QUERY-brwLitigation claimlitigation.caseNumber   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwLitigation   
&Scoped-define SELF-NAME brwLitigation
&Scoped-define QUERY-STRING-brwLitigation FOR EACH claimlitigation by claimcoverage.litigationID
&Scoped-define OPEN-QUERY-brwLitigation OPEN QUERY {&SELF-NAME} FOR EACH claimlitigation by claimcoverage.litigationID.
&Scoped-define TABLES-IN-QUERY-brwLitigation claimlitigation
&Scoped-define FIRST-TABLE-IN-QUERY-brwLitigation claimlitigation


/* Definitions for BROWSE brwProperties                                 */
&Scoped-define FIELDS-IN-QUERY-brwProperties claimproperty.isPrimary claimproperty.addr1   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwProperties   
&Scoped-define SELF-NAME brwProperties
&Scoped-define QUERY-STRING-brwProperties FOR EACH claimproperty by claimproperty.seq
&Scoped-define OPEN-QUERY-brwProperties OPEN QUERY {&SELF-NAME} FOR EACH claimproperty by claimproperty.seq.
&Scoped-define TABLES-IN-QUERY-brwProperties claimproperty
&Scoped-define FIRST-TABLE-IN-QUERY-brwProperties claimproperty


/* Definitions for BROWSE brwTasks                                      */
&Scoped-define FIELDS-IN-QUERY-brwTasks task.stat task.dueDate task.subject task.priority task.owner   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTasks   
&Scoped-define SELF-NAME brwTasks
&Scoped-define QUERY-STRING-brwTasks FOR EACH task by task.stat by task.dueDate by task.taskID
&Scoped-define OPEN-QUERY-brwTasks OPEN QUERY {&SELF-NAME} FOR EACH task by task.stat by task.dueDate by task.taskID.
&Scoped-define TABLES-IN-QUERY-brwTasks task
&Scoped-define FIRST-TABLE-IN-QUERY-brwTasks task


/* Definitions for FRAME fContacts                                      */

/* Definitions for FRAME fCoverage                                      */

/* Definitions for FRAME fLinked                                        */

/* Definitions for FRAME fLitigation                                    */

/* Definitions for FRAME fOverview                                      */

/* Definitions for FRAME fProperties                                    */

/* Definitions for FRAME fRecovery                                      */

/* Definitions for FRAME fTasks                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fTasks ~
    ~{&OPEN-QUERY-brwTasks}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS hdClaimID hdSuppClaimID hdLitigation ~
bTransfer hdDateCreated hdLastActivity hdStat hdAssignedTo hdSignificant ~
hdDateClosed hdType bAttr bClose bReopen bMarkClaim bMarkMatter bNewNote ~
bAcct bDocs bNotes 
&Scoped-Define DISPLAYED-OBJECTS hdClaimID hdSuppClaimID hdLitigation ~
hdDateCreated hdLastActivity hdStat hdAssignedTo hdSignificant hdDateClosed ~
hdType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkAgentValidation wWin 
FUNCTION checkAgentValidation RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD checkDataChanged wWin 
FUNCTION checkDataChanged RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearContact wWin 
FUNCTION clearContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearCoverage wWin 
FUNCTION clearCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearLitigation wWin 
FUNCTION clearLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearProperty wWin 
FUNCTION clearProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteBond wWin 
FUNCTION deleteBond RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteCode wWin 
FUNCTION deleteCode RETURNS LOGICAL PRIVATE
  ( input pCodeType as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteContact wWin 
FUNCTION deleteContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteCoverage wWin 
FUNCTION deleteCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteLink wWin 
FUNCTION deleteLink RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteLitigation wWin 
FUNCTION deleteLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteProperty wWin 
FUNCTION deleteProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD discardPage wWin 
FUNCTION discardPage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayAccounting wWin 
FUNCTION displayAccounting RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayActivity wWin 
FUNCTION displayActivity RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayContacts wWin 
FUNCTION displayContacts RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayCoverage wWin 
FUNCTION displayCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayHeader wWin 
FUNCTION displayHeader RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayLinkedClaims wWin 
FUNCTION displayLinkedClaims RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayLitigation wWin 
FUNCTION displayLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayOverview wWin 
FUNCTION displayOverview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayProperties wWin 
FUNCTION displayProperties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayRecovery wWin 
FUNCTION displayRecovery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTasks wWin 
FUNCTION displayTasks RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getBaseClaim wWin 
FUNCTION getBaseClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getCurrentContact wWin 
FUNCTION getCurrentContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getCurrentCoverage wWin 
FUNCTION getCurrentCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getCurrentLitigation wWin 
FUNCTION getCurrentLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getCurrentProperty wWin 
FUNCTION getCurrentProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPrimaryCoverage wWin 
FUNCTION getPrimaryCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPrimaryLitigation wWin 
FUNCTION getPrimaryLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPrimaryProperty wWin 
FUNCTION getPrimaryProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getSupplementalClaim wWin 
FUNCTION getSupplementalClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getUserName wWin 
FUNCTION getUserName RETURNS CHARACTER PRIVATE
  ( input pUID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD moveCode wWin 
FUNCTION moveCode RETURNS LOGICAL PRIVATE
  ( input pCodeType as char, input pDirection as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openBonds wWin 
FUNCTION openBonds RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openCauseCodes wWin 
FUNCTION openCauseCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openContacts wWin 
FUNCTION openContacts RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openCoverage wWin 
FUNCTION openCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openDescCodes wWin 
FUNCTION openDescCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openLinkedClaims wWin 
FUNCTION openLinkedClaims RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openLitigation wWin 
FUNCTION openLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openProperties wWin 
FUNCTION openProperties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openURL wWin 
FUNCTION openURL RETURNS LOGICAL PRIVATE
  ( input pURL as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshClaim wWin 
FUNCTION refreshClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveActivity wWin 
FUNCTION saveActivity RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveContact wWin 
FUNCTION saveContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveCoverage wWin 
FUNCTION saveCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveLitigation wWin 
FUNCTION saveLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveOverview wWin 
FUNCTION saveOverview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD savePage wWin 
FUNCTION savePage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveProperty wWin 
FUNCTION saveProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveRecovery wWin 
FUNCTION saveRecovery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveStateDisable wWin 
FUNCTION saveStateDisable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveStateEnable wWin 
FUNCTION saveStateEnable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pNewRecord as log )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setAssignee wWin 
FUNCTION setAssignee RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setHeaderState wWin 
FUNCTION setHeaderState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setLockState wWin 
FUNCTION setLockState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setPageState wWin 
FUNCTION setPageState RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pState as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setToClaim wWin 
FUNCTION setToClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setToMatter wWin 
FUNCTION setToMatter RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Action 
       MENU-ITEM m_Mark_as_Matter LABEL "Set Type to Matter" ACCELERATOR "ALT-M"
       MENU-ITEM m_Mark_as_Claim LABEL "Set Type to Claim" ACCELERATOR "ALT-C"
       RULE
       MENU-ITEM m_Transfer_to  LABEL "Transfer to..." ACCELERATOR "ALT-T"
       RULE
       MENU-ITEM m_Close        LABEL "Close"         
       MENU-ITEM m_Reopen       LABEL "Reopen"        
       RULE
       MENU-ITEM m_Add_Note     LABEL "Add Note"       ACCELERATOR "ALT-N".

DEFINE SUB-MENU m_View 
       MENU-ITEM m_Notes        LABEL "Notes"          ACCELERATOR "ALT-1"
       MENU-ITEM m_Documents    LABEL "Documents"      ACCELERATOR "ALT-2"
       MENU-ITEM m_Accounting   LABEL "Accounting"     ACCELERATOR "ALT-3"
       MENU-ITEM m_Attributes   LABEL "Attributes"     ACCELERATOR "ALT-4".

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_Action       LABEL "Action"        
       SUB-MENU  m_View         LABEL "View"          .


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddLAEAdjustment  NO-FOCUS
     LABEL "Adj" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new LAE (Loss Adjustment Expense) Reserve Adjustment".

DEFINE BUTTON bAddLAEPayment  NO-FOCUS
     LABEL "Pay" 
     SIZE 4.8 BY 1.14 TOOLTIP "Adjust the LAE (Loss Adjustment Expense) Reserve".

DEFINE BUTTON bAddLossAdjustment  NO-FOCUS
     LABEL "Adj" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new Loss Reserve Adjustment".

DEFINE BUTTON bAddLossPayment  NO-FOCUS
     LABEL "Pay" 
     SIZE 4.8 BY 1.14 TOOLTIP "Create a new Loss Claim Payable".

DEFINE VARIABLE acCompleteRecoveries AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Complete" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE acOpenRecoveries AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Open" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE acRequestedRecoveries AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Requested" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE acWaivedRecoveries AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Waived" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tLAEApprovedPayments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Sum of all Approved Payments" NO-UNDO.

DEFINE VARIABLE tLAEApprovedPaymentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Current Reserve Balance - Approved Payments" NO-UNDO.

DEFINE VARIABLE tLAECompletedPayments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Completed Payments" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Sum of all Complete Payments" NO-UNDO.

DEFINE VARIABLE tLAECurrentBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Approved Reserve Adjustments - Completed Payments" NO-UNDO.

DEFINE VARIABLE tLAEOpenAdjustments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Any Open (Not Approved) Reserve Adjustment" NO-UNDO.

DEFINE VARIABLE tLAEOpenAdjustmentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Approved Payments Reserve Balance + Any Open Reserve Adjustment" NO-UNDO.

DEFINE VARIABLE tLAEOpenPayments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Sum of all the Open (Not Approved) Payments" NO-UNDO.

DEFINE VARIABLE tLAEOpenPaymentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Open Adjustment Reserve Balance - Open Payments" NO-UNDO.

DEFINE VARIABLE tLAEPotentialCost AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total Potential LAE" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Open Payments Reserve Balance + Completed Payments" NO-UNDO.

DEFINE VARIABLE tLossApprovedPayments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Sum of all Approved Payments" NO-UNDO.

DEFINE VARIABLE tLossApprovedPaymentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Current Reserve Balance - Approved Payments" NO-UNDO.

DEFINE VARIABLE tLossCompletedPayments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Completed Payments" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Sum of all Complete Payments" NO-UNDO.

DEFINE VARIABLE tLossCurrentBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Approved Reserve Adjustments - Completed Payments" NO-UNDO.

DEFINE VARIABLE tLossOpenAdjustments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Any Open (Not Approved) Reserve Adjustment" NO-UNDO.

DEFINE VARIABLE tLossOpenAdjustmentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Approved Payments Reserve Balance + Any Open Reserve Adjustment" NO-UNDO.

DEFINE VARIABLE tLossOpenPayments AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Sum of all the Open (Not Approved) Payments" NO-UNDO.

DEFINE VARIABLE tLossOpenPaymentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Open Adjustment Reserve Balance - Open Payments" NO-UNDO.

DEFINE VARIABLE tLossPotentialCost AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Total Potential Loss" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Open Payments Reserve Balance + Completed Payments" NO-UNDO.

DEFINE VARIABLE tTotalApprovedPaymentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalCurrentBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalOpenAdjustmentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalOpenPaymentsBalance AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalPotentialCost AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 TOOLTIP "Total Potential LAE + Total Potential Loss" NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56.4 BY 4.14.

DEFINE RECTANGLE RECT-11
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57 BY 4.14.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.4 BY 4.14.

DEFINE RECTANGLE RECT-43
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43 BY .19.

DEFINE RECTANGLE RECT-44
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43 BY .19.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 19 BY .19.

DEFINE RECTANGLE RECT-48
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 19 BY .19.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56.4 BY 7.86.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57 BY 7.86.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 165 BY 2.38.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.4 BY 7.86.

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 165 BY 5.95.

DEFINE BUTTON bActivityCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bActivitySave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE avinitUserResponded AS CHARACTER FORMAT "X(256)":U INITIAL "ndedouh" 
     LABEL "Responded By" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Brett Blair","bblair",
                     "Noemi Dedouh","ndedouh",
                     "Sabina Pierson","spierson"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avUserAcknowledged AS CHARACTER FORMAT "X(256)":U INITIAL "ndedouh" 
     LABEL "Acknowledged By" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Brett Blair","bblair",
                     "Noemi Dedouh","ndedouh",
                     "Sabina Pierson","spierson"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avUserAssigned AS CHARACTER FORMAT "X(256)":U INITIAL "ndedouh" 
     LABEL "Assigned By" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Brett Blair","bblair",
                     "Noemi Dedouh","ndedouh",
                     "Sabina Pierson","spierson"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avUserResponded AS CHARACTER FORMAT "X(256)":U INITIAL "ndedouh" 
     LABEL "Responded By" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Brett Blair","bblair",
                     "Noemi Dedouh","ndedouh",
                     "Sabina Pierson","spierson"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avDateAcknowledged AS DATE FORMAT "99/99/99":U 
     LABEL "Acknowledged" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateAssigned AS DATE FORMAT "99/99/99":U 
     LABEL "Assigned" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateClmReceived AS DATE FORMAT "99/99/99":U 
     LABEL "Received" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateClosed AS DATE FORMAT "99/99/99":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateCreated AS DATE FORMAT "99/99/99":U 
     LABEL "Opened" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateReClosed AS DATE FORMAT "99/99/99":U 
     LABEL "Re-Closed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateRecReceived AS DATE FORMAT "99/99/99":U 
     LABEL "Received" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateReOpened AS DATE FORMAT "99/99/99":U 
     LABEL "Re-Opened" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateResponded AS DATE FORMAT "99/99/99":U 
     LABEL "Responded" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avDateTransferred AS DATE FORMAT "99/99/99":U 
     LABEL "Transferred" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avinitDateResponded AS DATE FORMAT "99/99/99":U 
     LABEL "Responded" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE avUserClosed AS CHARACTER FORMAT "X(256)":U 
     LABEL "Closed By" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avUserCreated AS CHARACTER FORMAT "X(256)":U 
     LABEL "Opened By" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avUserReClosed AS CHARACTER FORMAT "X(256)":U 
     LABEL "Re-Closed By" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE avUserReOpened AS CHARACTER FORMAT "X(256)":U 
     LABEL "Re-Opened By" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 96 BY 6.33.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 96 BY 9.81.

DEFINE BUTTON bContactAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a contact".

DEFINE BUTTON bContactCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bContactDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected contact".

DEFINE BUTTON bContactEmail  NO-FOCUS
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send an email to the selected contact".

DEFINE BUTTON bContactSave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE coRole AS CHARACTER FORMAT "X(256)":U INITIAL "1" 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "Agent","1",
                     "Correspondent","2",
                     "Claimant","3",
                     "Claimant Private Counsel","4",
                     "Insured's Private Counsel","5",
                     "Insured Owner","6",
                     "Insured Lender","7",
                     "Outside Counsel - Owner","8",
                     "Outside Counsel - Lender","9"
     DROP-DOWN-LIST
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE coStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "AZ","FL","TX" 
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE coNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE coAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE coAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE coCity AS CHARACTER FORMAT "X(256)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE coCompany AS CHARACTER FORMAT "X(256)":U 
     LABEL "Company" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE coCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE coEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE coFax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE coFullName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE coJobTitle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Title" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE coMobile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Mobile" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE coPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE coZipCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Zip Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE coIsActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE BUTTON bCoverageAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a coverage".

DEFINE BUTTON bCoverageCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bCoverageDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected coverage".

DEFINE BUTTON bCoverageSave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE cvCoverageType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Coverage Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Owner","O",
                     "Lender","L",
                     "Commitment","T",
                     "CPL","C",
                     "Ownership & Encumberance","E",
                     "Plat Certificate","P"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE cvInsuredType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Owner","O",
                     "Lender","L",
                     "Seller","S",
                     "Other","T"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE cvIsPrimary AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Primary" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE cvCoverageID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Policy Number" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE cvCoverageYear AS INTEGER FORMAT ">>>>>>>9":U INITIAL 0 
     LABEL "Coverage Year" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE cvCurrentLiabilityAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Current Liability Amount" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE cvEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE cvInsuredName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Name" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE cvOrigLiabilityAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Original Liability Amount" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE BUTTON bClaimLinkAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a linked claim".

DEFINE BUTTON bClaimLinkDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected linked claim".

DEFINE BUTTON bLitigationAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a litigation".

DEFINE BUTTON bLitigationCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bLitigationDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected litigation".

DEFINE BUTTON bLitigationSave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE liStat AS CHARACTER FORMAT "X(256)":U INITIAL "O" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Open","O",
                     "Closed","C"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE liNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 80 BY 4 NO-UNDO.

DEFINE VARIABLE liCaseNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "Case Number" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE liDateFiled AS DATE FORMAT "99/99/99":U 
     LABEL "Date Filed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE liDateServed AS DATE FORMAT "99/99/99":U 
     LABEL "Date Served" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE liDefendant AS CHARACTER FORMAT "X(256)":U 
     LABEL "Defendant" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE liPlaintiff AS CHARACTER FORMAT "X(256)":U 
     LABEL "Plaintiff" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE liIsParty AS LOGICAL INITIAL no 
     LABEL "Is Our Company a Party?" 
     VIEW-AS TOGGLE-BOX
     SIZE 40 BY .81 NO-UNDO.

DEFINE BUTTON bAcct  NO-FOCUS
     LABEL "Acct" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open accounting window".

DEFINE BUTTON bAttr  NO-FOCUS
     LABEL "Attr" 
     SIZE 7.2 BY 1.71 TOOLTIP "Claim attributes".

DEFINE BUTTON bClose  NO-FOCUS
     LABEL "Close" 
     SIZE 7.2 BY 1.71 TOOLTIP "Close this file".

DEFINE BUTTON bDocs  NO-FOCUS
     LABEL "Docs" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open documents window".

DEFINE BUTTON bMarkClaim  NO-FOCUS
     LABEL "Claim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Set Type to Claim".

DEFINE BUTTON bMarkMatter  NO-FOCUS
     LABEL "Matter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Set Type to Matter".

DEFINE BUTTON bNewNote  NO-FOCUS
     LABEL "New Note" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new note".

DEFINE BUTTON bNotes  NO-FOCUS
     LABEL "Notes" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open notes window".

DEFINE BUTTON bOpenSuppClaim  NO-FOCUS
     LABEL "Open" 
     SIZE 4.8 BY 1.14 TOOLTIP "View supplemental claim".

DEFINE BUTTON bReopen  NO-FOCUS
     LABEL "Reopen" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reopen this file".

DEFINE BUTTON bTransfer  NO-FOCUS
     LABEL "Transfer" 
     SIZE 7.2 BY 1.71 TOOLTIP "Transfer this file to a different administrator".

DEFINE VARIABLE hdAssignedTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE hdClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE hdDateClosed AS DATE FORMAT "99/99/99":U 
     LABEL "Date Closed" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE hdDateCreated AS DATE FORMAT "99/99/99":U 
     LABEL "Date Opened" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE hdLastActivity AS DATE FORMAT "99/99/99":U 
     LABEL "Last Note" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE hdLitigation AS LOGICAL FORMAT "Yes/No":U INITIAL NO 
     LABEL "Litigation" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE hdSignificant AS LOGICAL FORMAT "Yes/No":U INITIAL NO 
     LABEL "Significant" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE hdStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE hdSuppClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Supplemental Claim" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE hdType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 75.2 BY 1.86.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.2 BY 1.86.

DEFINE BUTTON bCauseCodeAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a cause code".

DEFINE BUTTON bCauseCodeDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected cause code".

DEFINE BUTTON bCauseCodeDown  NO-FOCUS
     LABEL "Down" 
     SIZE 4.8 BY 1.14 TOOLTIP "Move cause code down in priority".

DEFINE BUTTON bCauseCodeUp  NO-FOCUS
     LABEL "Up" 
     SIZE 4.8 BY 1.14 TOOLTIP "Move cause code up in priority".

DEFINE BUTTON bDescCodeAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a description code".

DEFINE BUTTON bDescCodeDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected description code".

DEFINE BUTTON bDescCodeDown  NO-FOCUS
     LABEL "Down" 
     SIZE 4.8 BY 1.14 TOOLTIP "Move description code down in priority".

DEFINE BUTTON bDescCodeUp  NO-FOCUS
     LABEL "Up" 
     SIZE 4.8 BY 1.14 TOOLTIP "Move description code up in priority".

DEFINE BUTTON bOverviewCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bOverviewSave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE ovAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "","",
                     "Reviewed","V",
                     "Accepted","A",
                     "Not Covered","N",
                     "Resolved","R",
                     "Indemnified","I",
                     "Settled","S",
                     "Withdrawn","W"
     DROP-DOWN-LIST
     SIZE 18.4 BY 1 NO-UNDO.

DEFINE VARIABLE ovAgentError AS CHARACTER FORMAT "X(256)" 
     LABEL "Agent Error" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N",
                     "Unknown","U",
                     "Mixed","M"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN AUTO-COMPLETION
     SIZE 98 BY 1 NO-UNDO.

DEFINE VARIABLE ovALTAResp AS CHARACTER FORMAT "X(256)":U 
     LABEL "ALTA Responsibility" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","Select"
     DROP-DOWN-LIST
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE ovALTARisk AS CHARACTER FORMAT "X(256)":U 
     LABEL "ALTA Risk" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","Select"
     DROP-DOWN-LIST
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE ovAttorneyError AS CHARACTER FORMAT "X(256)" 
     LABEL "Attorney Error" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N",
                     "Unknown","U",
                     "Mixed","M"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovCvgPrimary AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Primary Coverage" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE ovDifficulty AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Difficulty" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "",0,
                     "1-Low",1,
                     "2-Medium",2,
                     "3-High",3
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropPrimary AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Primary Address" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropResidential AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Residential" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "AZ","FL","TX" 
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE ovSearcher AS CHARACTER FORMAT "X(256)" 
     LABEL "Searcher" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE ovSearcherError AS CHARACTER FORMAT "X(256)" 
     LABEL "Searcher Error" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N",
                     "Unknown","U",
                     "Mixed","M"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovStage AS CHARACTER FORMAT "X(256)":U 
     LABEL "Stage" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "","",
                     "Evaluation","E",
                     "Active","A",
                     "Dormant","D",
                     "Recovery","R",
                     "Litigation","L",
                     "Complete","C"
     DROP-DOWN-LIST
     SIZE 18.4 BY 1 NO-UNDO.

DEFINE VARIABLE ovStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE ovUrgency AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Urgency" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "",0,
                     "1-Low",1,
                     "2-Medium",2,
                     "3-High",3
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropLegalDesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE ovSummary AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE ovAgentStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent Status" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE ovBorrowerName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Borrower Name" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE ovCoverageID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Policy Number" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE ovEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent File Number" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE ovInsuredName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Name" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE ovOrigLiabilityAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Original Liab Amt" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Property Address" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropCity AS CHARACTER FORMAT "X(256)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropSubdivision AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subdivision" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE ovPropZipCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Zip Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE ovRefYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Reference Year" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE ovYearFirstReport AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Year of First Report" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE BUTTON bPropertyAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a property".

DEFINE BUTTON bPropertyCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bPropertyDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the current property".

DEFINE BUTTON bPropertySave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE prIsPrimary AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Primary" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE prResidential AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Residential" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE prStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEMS "AZ","FL","TX" 
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE prLegalDesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE prAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE prAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE prCity AS CHARACTER FORMAT "X(256)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE prCountyID AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE prSubdivision AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subdivision" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE prZipCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Zip Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE BUTTON bBondAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a bond".

DEFINE BUTTON bBondDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Detete the selected bond".

DEFINE BUTTON bBondModify  NO-FOCUS
     LABEL "Mod" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify the selected bond".

DEFINE BUTTON bRecoveryCancel  NO-FOCUS
     LABEL "X" 
     SIZE 4.8 BY 1.14 TOOLTIP "Cancel operation".

DEFINE BUTTON bRecoverySave  NO-FOCUS
     LABEL "Save" 
     SIZE 4.8 BY 1.14 TOOLTIP "Save changes".

DEFINE VARIABLE reHasDeductible AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Deductible" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE reHasReinsurance AS LOGICAL FORMAT "yes/no":U INITIAL NO 
     LABEL "Reinsurance" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE rePriorPolicyUsed AS CHARACTER FORMAT "X(256)" 
     LABEL "Prior Policy Used" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N",
                     "Unknown","U"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE reSeekingRecovery AS CHARACTER FORMAT "X(256)":U 
     LABEL "Seeking Recovery" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N"
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE reSellerBreach AS CHARACTER FORMAT "X(256)" 
     LABEL "Seller Breach" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N",
                     "N/A","A"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE reUnderwritingCompliance AS CHARACTER FORMAT "X(256)" 
     LABEL "Underwriting Compliance" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "","",
                     "Yes","Y",
                     "No","N",
                     "Unknown","U",
                     "N/A","A"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE reRecoveryNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 80 BY 4.52 NO-UNDO.

DEFINE VARIABLE reAmountWaived AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Amount Waived" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE reDeductibleAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Deductible Amount" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE reEOCarrier AS CHARACTER FORMAT "X(256)":U 
     LABEL "EO Carrier" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE reEOCoverageAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "EO Coverage Amt" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE reEOEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "EO Eff Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE reEOPolicy AS CHARACTER FORMAT "X(256)":U 
     LABEL "EO Policy" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE reEORetroDate AS DATE FORMAT "99/99/99":U 
     LABEL "EO Retro Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE rePriorPolicyAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE rePriorPolicyCarrier AS CHARACTER FORMAT "X(256)":U 
     LABEL "Prior Policy Carrier" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE rePriorPolicyEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Prior Policy Eff Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE rePriorPolicyID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Prior Policy ID" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 168 BY 13.29.

DEFINE RECTANGLE RECT-42
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 168 BY 5.91.

DEFINE BUTTON bTaskAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a new task".

DEFINE BUTTON bTaskComplete  NO-FOCUS
     LABEL "Complete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Mark the task complete".

DEFINE BUTTON bTaskDelete  NO-FOCUS
     LABEL "Del" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove the selected task".

DEFINE BUTTON bTaskEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "View/Modify the selected task".

DEFINE BUTTON bTaskRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE VARIABLE taPriority AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Priority" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     LIST-ITEM-PAIRS "ALL","ALL",
                     "High","H",
                     "Normal","N",
                     "Low","L"
     DROP-DOWN-LIST
     SIZE 12.2 BY 1 NO-UNDO.

DEFINE VARIABLE taStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 7
     LIST-ITEM-PAIRS "ALL","ALL",
                     "Not Started","N",
                     "In Process","I",
                     "Completed","C",
                     "Waiting","W",
                     "Deferred","D",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE taDueDays AS INTEGER FORMAT "->>9":U INITIAL 7 
     LABEL "Due Within" 
     VIEW-AS FILL-IN 
     SIZE 6.8 BY 1 TOOLTIP "Number of days; enter zero for only past due tasks" NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73.2 BY 1.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwBonds FOR 
      claimbond SCROLLING.

DEFINE QUERY brwCauseCodes FOR 
      causecode SCROLLING.

DEFINE QUERY brwContacts FOR 
      claimcontact SCROLLING.

DEFINE QUERY brwCoverage FOR 
      claimcoverage SCROLLING.

DEFINE QUERY brwDescCodes FOR 
      desccode SCROLLING.

DEFINE QUERY brwLinkedClaims FOR 
      claimlink SCROLLING.

DEFINE QUERY brwLitigation FOR 
      claimlitigation SCROLLING.

DEFINE QUERY brwProperties FOR 
      claimproperty SCROLLING.

DEFINE QUERY brwTasks FOR 
      task SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwBonds
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBonds wWin _FREEFORM
  QUERY brwBonds DISPLAY
      claimbond.type column-label "Type" format "x(25)"
claimbond.ID column-label "ID" format "x(25)"
claimbond.entity column-label "Entity" format "x(75)"
claimbond.effDate column-label "Eff Date" format "99/99/99"
claimbond.amount column-label "Amount" format "->>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 157 BY 4.71
         TITLE "Bonds / Carriers" ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.

DEFINE BROWSE brwCauseCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCauseCodes wWin _FREEFORM
  QUERY brwCauseCodes DISPLAY
      causecode.code  column-label "Code" format "x(8)"
causecode.description column-label "Description" format "x(40)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET NO-TAB-STOP SIZE 65 BY 4.71
         TITLE "Company Cause Codes" ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.

DEFINE BROWSE brwContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwContacts wWin _FREEFORM
  QUERY brwContacts DISPLAY
      claimcontact.roleDesc column-label "Role" format "x(26)"
claimcontact.isActive column-label "Active" format "Yes/No"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET NO-TAB-STOP SIZE 38 BY 10.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwCoverage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCoverage wWin _FREEFORM
  QUERY brwCoverage DISPLAY
      claimcoverage.coverageID column-label "Policy Number" format "x(20)"
claimcoverage.coverageTypeDesc column-label "Coverage Type" format "x(30)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET NO-TAB-STOP SIZE 55 BY 6.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwDescCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDescCodes wWin _FREEFORM
  QUERY brwDescCodes DISPLAY
      desccode.code  column-label "Code" format "x(8)"
desccode.description column-label "Description" format "x(40)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET NO-TAB-STOP SIZE 65 BY 4.71
         TITLE "Company Description Codes" ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.

DEFINE BROWSE brwLinkedClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwLinkedClaims wWin _FREEFORM
  QUERY brwLinkedClaims DISPLAY
      claimlink.toClaimID column-label "File Number" format ">>>>>>>>>9"
claimlink.fileNumber column-label "Agent File Number" format "x(30)"      
claimlink.linkTypeDesc column-label "Link Type" format "x(20)"      
claimlink.typeDesc column-label "Claim Type" format "x(15)"
claimlink.statDesc column-label "Claim Status" format "x(15)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 98 BY 6.62
         TITLE "Supplemental/Related Claims" ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.

DEFINE BROWSE brwLitigation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwLitigation wWin _FREEFORM
  QUERY brwLitigation DISPLAY
      claimlitigation.caseNumber column-label "Case Number" format "x(40)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 55 BY 5.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwProperties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwProperties wWin _FREEFORM
  QUERY brwProperties DISPLAY
      claimproperty.isPrimary column-label "Primary" format "Yes/No"
claimproperty.addr1 column-label "Address" format "x(40)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET NO-TAB-STOP SIZE 66 BY 6.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwTasks
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTasks wWin _FREEFORM
  QUERY brwTasks DISPLAY
      task.stat label "Status" format "x(8)"
 task.dueDate label "Due Date" format "99/99/9999" width 12
 task.subject label "Subject" format "x(256)" width 70
 task.priority label "Priority" format "x(10)"
 task.owner label "Owner" format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 169.6 BY 19.86
         TITLE "Tasks" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     hdClaimID AT ROW 3.14 COL 14 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     hdSuppClaimID AT ROW 3.14 COL 50 COLON-ALIGNED WIDGET-ID 352 NO-TAB-STOP 
     hdLitigation AT ROW 3.14 COL 83 COLON-ALIGNED WIDGET-ID 382 NO-TAB-STOP 
     bTransfer AT ROW 1.05 COL 30.4 WIDGET-ID 380 NO-TAB-STOP 
     hdDateCreated AT ROW 3.14 COL 105 COLON-ALIGNED WIDGET-ID 372 NO-TAB-STOP 
     hdLastActivity AT ROW 3.14 COL 133 COLON-ALIGNED WIDGET-ID 332 NO-TAB-STOP 
     hdStat AT ROW 3.14 COL 158 COLON-ALIGNED WIDGET-ID 338 NO-TAB-STOP 
     hdAssignedTo AT ROW 4.33 COL 14 COLON-ALIGNED WIDGET-ID 346 NO-TAB-STOP 
     hdSignificant AT ROW 4.33 COL 83 COLON-ALIGNED WIDGET-ID 384 NO-TAB-STOP 
     hdDateClosed AT ROW 4.33 COL 105 COLON-ALIGNED WIDGET-ID 328 NO-TAB-STOP 
     hdType AT ROW 4.33 COL 158 COLON-ALIGNED WIDGET-ID 340 NO-TAB-STOP 
     bOpenSuppClaim AT ROW 3.1 COL 66.4 WIDGET-ID 366 NO-TAB-STOP 
     bAttr AT ROW 1.05 COL 38.6 WIDGET-ID 376 NO-TAB-STOP 
     bClose AT ROW 1.05 COL 16 WIDGET-ID 144 NO-TAB-STOP 
     bReopen AT ROW 1.05 COL 23.2 WIDGET-ID 348 NO-TAB-STOP 
     bMarkClaim AT ROW 1.05 COL 8.8 WIDGET-ID 142 NO-TAB-STOP 
     bMarkMatter AT ROW 1.05 COL 1.6 WIDGET-ID 140 NO-TAB-STOP 
     bNewNote AT ROW 1.05 COL 68.4 WIDGET-ID 336 NO-TAB-STOP 
     bAcct AT ROW 1.05 COL 45.8 WIDGET-ID 296 NO-TAB-STOP 
     bDocs AT ROW 1.05 COL 53 WIDGET-ID 298 NO-TAB-STOP 
     bNotes AT ROW 1.05 COL 60.2 WIDGET-ID 294 NO-TAB-STOP 
     RECT-33 AT ROW 1 COL 1 WIDGET-ID 226
     RECT-34 AT ROW 1 COL 38 WIDGET-ID 378
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 176 BY 29.67 WIDGET-ID 100.

DEFINE FRAME fActivity
     bActivityCancel AT ROW 2.95 COL 3 WIDGET-ID 386 NO-TAB-STOP 
     avDateClmReceived AT ROW 2.67 COL 31 COLON-ALIGNED WIDGET-ID 314
     avDateCreated AT ROW 3.86 COL 31 COLON-ALIGNED WIDGET-ID 316
     avUserCreated AT ROW 3.86 COL 66 COLON-ALIGNED WIDGET-ID 318
     avinitDateResponded AT ROW 5.05 COL 31 COLON-ALIGNED WIDGET-ID 388
     avinitUserResponded AT ROW 5.05 COL 66 COLON-ALIGNED WIDGET-ID 390
     avDateAssigned AT ROW 6.24 COL 31 COLON-ALIGNED WIDGET-ID 320
     avUserAssigned AT ROW 6.24 COL 66 COLON-ALIGNED WIDGET-ID 392
     avDateAcknowledged AT ROW 7.43 COL 31 COLON-ALIGNED WIDGET-ID 342
     avUserAcknowledged AT ROW 7.43 COL 66 COLON-ALIGNED WIDGET-ID 344
     avDateTransferred AT ROW 8.62 COL 31 COLON-ALIGNED WIDGET-ID 360
     avDateClosed AT ROW 9.76 COL 31 COLON-ALIGNED WIDGET-ID 328
     avUserClosed AT ROW 9.76 COL 66 COLON-ALIGNED WIDGET-ID 348
     avDateRecReceived AT ROW 13 COL 31 COLON-ALIGNED WIDGET-ID 358
     avDateReOpened AT ROW 14.19 COL 31 COLON-ALIGNED WIDGET-ID 354
     avUserReOpened AT ROW 14.19 COL 66 COLON-ALIGNED WIDGET-ID 356
     avDateResponded AT ROW 15.38 COL 31 COLON-ALIGNED WIDGET-ID 324
     avUserResponded AT ROW 15.38 COL 66 COLON-ALIGNED WIDGET-ID 258
     bActivitySave AT ROW 1.76 COL 3 WIDGET-ID 384 NO-TAB-STOP 
     avDateReClosed AT ROW 16.57 COL 31 COLON-ALIGNED WIDGET-ID 352
     avUserReClosed AT ROW 16.57 COL 66 COLON-ALIGNED WIDGET-ID 350
     "Reconsideration" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 11.81 COL 11 WIDGET-ID 334
     "General Activity" VIEW-AS TEXT
          SIZE 15.4 BY .62 AT ROW 1.48 COL 11 WIDGET-ID 340
     RECT-38 AT ROW 12.14 COL 10 WIDGET-ID 332
     RECT-39 AT ROW 1.81 COL 10 WIDGET-ID 338
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Activity" WIDGET-ID 600.

DEFINE FRAME fLitigation
     brwLitigation AT ROW 3.62 COL 5 WIDGET-ID 2300
     bLitigationSave AT ROW 1.71 COL 14.6 WIDGET-ID 376 NO-TAB-STOP 
     liCaseNumber AT ROW 3.62 COL 75 COLON-ALIGNED WIDGET-ID 294
     liStat AT ROW 3.62 COL 130.8 COLON-ALIGNED WIDGET-ID 40 NO-TAB-STOP 
     liDateFiled AT ROW 4.81 COL 75 COLON-ALIGNED WIDGET-ID 296
     liDateServed AT ROW 6 COL 75 COLON-ALIGNED WIDGET-ID 300
     liPlaintiff AT ROW 7.19 COL 75 COLON-ALIGNED WIDGET-ID 302
     liDefendant AT ROW 8.38 COL 75 COLON-ALIGNED WIDGET-ID 304
     liIsParty AT ROW 9.57 COL 77 WIDGET-ID 328
     liNotes AT ROW 11.24 COL 76.8 NO-LABEL WIDGET-ID 188
     bLitigationAdd AT ROW 1.71 COL 5 WIDGET-ID 284 NO-TAB-STOP 
     bLitigationCancel AT ROW 1.71 COL 19.4 WIDGET-ID 374 NO-TAB-STOP 
     bLitigationDelete AT ROW 1.71 COL 9.8 WIDGET-ID 286 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 11.24 COL 70 WIDGET-ID 282
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Litigation" WIDGET-ID 900.

DEFINE FRAME fCoverage
     brwCoverage AT ROW 3.62 COL 5 WIDGET-ID 500
     cvCoverageID AT ROW 3.62 COL 85 COLON-ALIGNED WIDGET-ID 304
     cvIsPrimary AT ROW 3.62 COL 131 COLON-ALIGNED WIDGET-ID 370
     bCoverageSave AT ROW 1.71 COL 14.6 WIDGET-ID 374 NO-TAB-STOP 
     cvCoverageType AT ROW 4.81 COL 85 COLON-ALIGNED WIDGET-ID 330
     cvInsuredType AT ROW 6 COL 85 COLON-ALIGNED WIDGET-ID 308
     cvEffDate AT ROW 6 COL 131 COLON-ALIGNED WIDGET-ID 302
     cvOrigLiabilityAmount AT ROW 7.19 COL 85 COLON-ALIGNED WIDGET-ID 298
     cvCoverageYear AT ROW 7.19 COL 131 COLON-ALIGNED WIDGET-ID 314
     cvCurrentLiabilityAmount AT ROW 8.38 COL 85 COLON-ALIGNED WIDGET-ID 372
     cvInsuredName AT ROW 9.57 COL 85 COLON-ALIGNED WIDGET-ID 306
     bCoverageCancel AT ROW 1.71 COL 19.4 WIDGET-ID 376 NO-TAB-STOP 
     bCoverageAdd AT ROW 1.71 COL 5 WIDGET-ID 284 NO-TAB-STOP 
     bCoverageDelete AT ROW 1.71 COL 9.8 WIDGET-ID 286 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Coverage" WIDGET-ID 400.

DEFINE FRAME fAccounting
     bAddLAEAdjustment AT ROW 6.43 COL 76.6 WIDGET-ID 334 NO-TAB-STOP 
     bAddLossAdjustment AT ROW 6.43 COL 134.2 WIDGET-ID 336 NO-TAB-STOP 
     bAddLossPayment AT ROW 7.62 COL 134.2 WIDGET-ID 514 NO-TAB-STOP 
     bAddLAEPayment AT ROW 7.62 COL 76.6 WIDGET-ID 512 NO-TAB-STOP 
     tLAECurrentBalance AT ROW 3.14 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 344 NO-TAB-STOP 
     tLossCurrentBalance AT ROW 3.14 COL 108.6 COLON-ALIGNED NO-LABEL WIDGET-ID 356 NO-TAB-STOP 
     tTotalCurrentBalance AT ROW 3.14 COL 142 COLON-ALIGNED NO-LABEL WIDGET-ID 452 NO-TAB-STOP 
     tLAEApprovedPayments AT ROW 4.33 COL 27 COLON-ALIGNED NO-LABEL WIDGET-ID 342 NO-TAB-STOP 
     tLAEApprovedPaymentsBalance AT ROW 4.33 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 346 NO-TAB-STOP 
     tLossApprovedPayments AT ROW 4.33 COL 84.6 COLON-ALIGNED NO-LABEL WIDGET-ID 354 NO-TAB-STOP 
     tLossApprovedPaymentsBalance AT ROW 4.33 COL 108.6 COLON-ALIGNED NO-LABEL WIDGET-ID 450 NO-TAB-STOP 
     tTotalApprovedPaymentsBalance AT ROW 4.33 COL 142 COLON-ALIGNED NO-LABEL WIDGET-ID 544 NO-TAB-STOP 
     tLAEOpenAdjustments AT ROW 6.48 COL 27 COLON-ALIGNED NO-LABEL WIDGET-ID 490 NO-TAB-STOP 
     tLAEOpenAdjustmentsBalance AT ROW 6.48 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 492 NO-TAB-STOP 
     tLossOpenAdjustments AT ROW 6.48 COL 84.6 COLON-ALIGNED NO-LABEL WIDGET-ID 498 NO-TAB-STOP 
     tLossOpenAdjustmentsBalance AT ROW 6.48 COL 108.6 COLON-ALIGNED NO-LABEL WIDGET-ID 500 NO-TAB-STOP 
     tTotalOpenAdjustmentsBalance AT ROW 6.48 COL 142 COLON-ALIGNED NO-LABEL WIDGET-ID 550 NO-TAB-STOP 
     tLAEOpenPayments AT ROW 7.67 COL 27 COLON-ALIGNED NO-LABEL WIDGET-ID 486 NO-TAB-STOP 
     tLAEOpenPaymentsBalance AT ROW 7.67 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 488 NO-TAB-STOP 
     tLossOpenPayments AT ROW 7.67 COL 84.6 COLON-ALIGNED NO-LABEL WIDGET-ID 502 NO-TAB-STOP 
     tLossOpenPaymentsBalance AT ROW 7.67 COL 108.6 COLON-ALIGNED NO-LABEL WIDGET-ID 504 NO-TAB-STOP 
     tTotalOpenPaymentsBalance AT ROW 7.67 COL 142 COLON-ALIGNED NO-LABEL WIDGET-ID 552 NO-TAB-STOP 
     tLAECompletedPayments AT ROW 10.52 COL 51 COLON-ALIGNED WIDGET-ID 482 NO-TAB-STOP 
     tLossCompletedPayments AT ROW 10.52 COL 108.6 COLON-ALIGNED WIDGET-ID 526 NO-TAB-STOP 
     tLAEPotentialCost AT ROW 11.71 COL 51 COLON-ALIGNED WIDGET-ID 582 NO-TAB-STOP 
     tLossPotentialCost AT ROW 11.71 COL 108.6 COLON-ALIGNED WIDGET-ID 588 NO-TAB-STOP 
     tTotalPotentialCost AT ROW 11.71 COL 142 COLON-ALIGNED NO-LABEL WIDGET-ID 570 NO-TAB-STOP 
     acRequestedRecoveries AT ROW 14.57 COL 27 COLON-ALIGNED WIDGET-ID 624 NO-TAB-STOP 
     acOpenRecoveries AT ROW 14.57 COL 63 COLON-ALIGNED WIDGET-ID 382 NO-TAB-STOP 
     acCompleteRecoveries AT ROW 14.57 COL 103 COLON-ALIGNED WIDGET-ID 380 NO-TAB-STOP 
     acWaivedRecoveries AT ROW 14.62 COL 142 COLON-ALIGNED WIDGET-ID 616 NO-TAB-STOP 
     "Open Payments:" VIEW-AS TEXT
          SIZE 15.6 BY .62 AT ROW 7.86 COL 9.6 WIDGET-ID 464
     "Totals" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 1.48 COL 143.4 WIDGET-ID 406
     "Total Potential Cost" VIEW-AS TEXT
          SIZE 18.6 BY .62 AT ROW 10.71 COL 146 WIDGET-ID 518
     "Reserve Balance" VIEW-AS TEXT
          SIZE 17 BY .62 AT ROW 2.43 COL 147 WIDGET-ID 612
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38 WIDGET-ID 1000.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fAccounting
     "Approved Payments Reserve Balance =" VIEW-AS TEXT
          SIZE 38 BY .62 AT ROW 18.33 COL 7.4 WIDGET-ID 432
     "Loss" VIEW-AS TEXT
          SIZE 4.8 BY .62 AT ROW 1.48 COL 85.4 WIDGET-ID 404
     "Open Adjustment Reserve Balance - Pending Open Payments" VIEW-AS TEXT
          SIZE 62 BY .62 AT ROW 20.24 COL 46 WIDGET-ID 542
     "Current Reserve Balance - Approved Payments" VIEW-AS TEXT
          SIZE 45.2 BY .62 AT ROW 18.33 COL 46.2 WIDGET-ID 532
     "Approved Adjustments (not shown) - Completed Payments" VIEW-AS TEXT
          SIZE 56 BY .62 AT ROW 17.38 COL 46.2 WIDGET-ID 586
     "Approved Payments Reserve Balance + Pending Open Adjustment (only one at a time)" VIEW-AS TEXT
          SIZE 82.8 BY .62 AT ROW 19.29 COL 46.2 WIDGET-ID 560
     "Approved Payments:" VIEW-AS TEXT
          SIZE 20 BY .62 AT ROW 4.48 COL 5.6 WIDGET-ID 462
     "Open Payments Reserve Balance =" VIEW-AS TEXT
          SIZE 34 BY .62 AT ROW 20.24 COL 11.4 WIDGET-ID 534
     "Total Potential LAE + Total Potential Loss" VIEW-AS TEXT
          SIZE 40.4 BY .62 AT ROW 21.19 COL 46 WIDGET-ID 576
     "Open Adjustment Reserve Balance =" VIEW-AS TEXT
          SIZE 35 BY .62 AT ROW 19.29 COL 10.2 WIDGET-ID 536
     "Pending" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 2.43 COL 94 WIDGET-ID 592
     "Reserve Balance" VIEW-AS TEXT
          SIZE 17 BY .62 AT ROW 2.43 COL 55.8 WIDGET-ID 384
     "Total Potential Cost =" VIEW-AS TEXT
          SIZE 20 BY .62 AT ROW 21.19 COL 25.2 WIDGET-ID 540
     "Current:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 3.33 COL 17.8 WIDGET-ID 460
     "Legend" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 16.43 COL 5.4 WIDGET-ID 430
     "LAE" VIEW-AS TEXT
          SIZE 4 BY .62 AT ROW 1.48 COL 28 WIDGET-ID 348
     "Reserve Balance" VIEW-AS TEXT
          SIZE 17 BY .62 AT ROW 2.43 COL 113.8 WIDGET-ID 448
     "Current Reserve Balance =" VIEW-AS TEXT
          SIZE 26 BY .62 AT ROW 17.38 COL 19.6 WIDGET-ID 584
     "Pending" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 2.43 COL 37 WIDGET-ID 442
     "Recoveries" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 13.67 COL 5.4 WIDGET-ID 374
     " Open Adjustment:" VIEW-AS TEXT
          SIZE 17.4 BY .62 AT ROW 6.67 COL 7.8 WIDGET-ID 480
     RECT-5 AT ROW 1.71 COL 26.6 WIDGET-ID 88
     RECT-7 AT ROW 13.91 COL 4.4 WIDGET-ID 372
     RECT-6 AT ROW 1.71 COL 84 WIDGET-ID 402
     RECT-8 AT ROW 1.71 COL 142 WIDGET-ID 420
     RECT-9 AT ROW 16.67 COL 4.4 WIDGET-ID 428
     RECT-43 AT ROW 5.76 COL 30.4 WIDGET-ID 444
     RECT-44 AT ROW 5.76 COL 88.8 WIDGET-ID 456
     RECT-45 AT ROW 5.76 COL 146.4 WIDGET-ID 458
     RECT-48 AT ROW 5.76 COL 146.4 WIDGET-ID 548
     RECT-10 AT ROW 9.48 COL 26.6 WIDGET-ID 606
     RECT-11 AT ROW 9.48 COL 84 WIDGET-ID 608
     RECT-12 AT ROW 9.48 COL 142 WIDGET-ID 610
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Accounting" WIDGET-ID 1000.

DEFINE FRAME fRecovery
     bBondAdd AT ROW 16.24 COL 6 WIDGET-ID 332 NO-TAB-STOP 
     reHasReinsurance AT ROW 2.43 COL 34 COLON-ALIGNED WIDGET-ID 282 NO-TAB-STOP 
     reSeekingRecovery AT ROW 2.43 COL 66 COLON-ALIGNED WIDGET-ID 348 NO-TAB-STOP 
     reRecoveryNotes AT ROW 2.43 COL 87.8 NO-LABEL WIDGET-ID 188
     reHasDeductible AT ROW 3.62 COL 34 COLON-ALIGNED WIDGET-ID 346 NO-TAB-STOP 
     reDeductibleAmount AT ROW 4.81 COL 34 COLON-ALIGNED WIDGET-ID 338
     reAmountWaived AT ROW 6 COL 34 COLON-ALIGNED WIDGET-ID 342
     reEOPolicy AT ROW 7.67 COL 34 COLON-ALIGNED WIDGET-ID 292
     reEOEffDate AT ROW 7.67 COL 80 COLON-ALIGNED WIDGET-ID 296
     reEORetroDate AT ROW 7.67 COL 122 COLON-ALIGNED WIDGET-ID 298
     reEOCarrier AT ROW 8.86 COL 34 COLON-ALIGNED WIDGET-ID 294
     reEOCoverageAmount AT ROW 8.86 COL 114 COLON-ALIGNED WIDGET-ID 290
     rePriorPolicyUsed AT ROW 10.52 COL 34 COLON-ALIGNED WIDGET-ID 328
     rePriorPolicyID AT ROW 10.52 COL 76 COLON-ALIGNED WIDGET-ID 326
     reUnderwritingCompliance AT ROW 11.71 COL 34 COLON-ALIGNED WIDGET-ID 324
     bRecoveryCancel AT ROW 3.62 COL 6 WIDGET-ID 386 NO-TAB-STOP 
     rePriorPolicyCarrier AT ROW 11.71 COL 76 COLON-ALIGNED WIDGET-ID 320
     reSellerBreach AT ROW 12.91 COL 34 COLON-ALIGNED WIDGET-ID 318
     rePriorPolicyAmount AT ROW 12.91 COL 76 COLON-ALIGNED WIDGET-ID 316
     bRecoverySave AT ROW 2.43 COL 6 WIDGET-ID 384 NO-TAB-STOP 
     rePriorPolicyEffDate AT ROW 12.91 COL 122 COLON-ALIGNED WIDGET-ID 322
     brwBonds AT ROW 16.24 COL 11 WIDGET-ID 1900
     bBondDelete AT ROW 17.38 COL 6 WIDGET-ID 334 NO-TAB-STOP 
     bBondModify AT ROW 18.52 COL 6 WIDGET-ID 336 NO-TAB-STOP 
     "Other Recovery" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 15.29 COL 4 WIDGET-ID 360
     "Reinsurance / E&&O / Prior Policy" VIEW-AS TEXT
          SIZE 32 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 352
     "Notes:" VIEW-AS TEXT
          SIZE 6.2 BY .62 AT ROW 2.62 COL 81 WIDGET-ID 344
     RECT-40 AT ROW 1.52 COL 3 WIDGET-ID 350
     RECT-42 AT ROW 15.67 COL 3 WIDGET-ID 358
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Recovery" WIDGET-ID 1600.

DEFINE FRAME fProperties
     bPropertySave AT ROW 1.71 COL 14.6 WIDGET-ID 376 NO-TAB-STOP 
     brwProperties AT ROW 3.62 COL 5 WIDGET-ID 2200
     prAddr1 AT ROW 3.62 COL 86 COLON-ALIGNED WIDGET-ID 306
     prAddr2 AT ROW 4.81 COL 86 COLON-ALIGNED NO-LABEL WIDGET-ID 300
     prCity AT ROW 6 COL 86 COLON-ALIGNED WIDGET-ID 314
     prStateID AT ROW 6 COL 141 COLON-ALIGNED WIDGET-ID 320
     prCountyID AT ROW 7.19 COL 86 COLON-ALIGNED WIDGET-ID 316
     prZipCode AT ROW 7.19 COL 137 COLON-ALIGNED WIDGET-ID 322
     prSubdivision AT ROW 8.38 COL 86 COLON-ALIGNED WIDGET-ID 326
     prIsPrimary AT ROW 9.52 COL 86 COLON-ALIGNED WIDGET-ID 332
     prResidential AT ROW 9.52 COL 141 COLON-ALIGNED WIDGET-ID 330
     prLegalDesc AT ROW 10.71 COL 88 NO-LABEL WIDGET-ID 188
     bPropertyDelete AT ROW 1.71 COL 9.8 WIDGET-ID 286 NO-TAB-STOP 
     bPropertyCancel AT ROW 1.71 COL 19.4 WIDGET-ID 374 NO-TAB-STOP 
     bPropertyAdd AT ROW 1.71 COL 5 WIDGET-ID 284 NO-TAB-STOP 
     "Legal Desc:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 10.81 COL 76 WIDGET-ID 282
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Properties" WIDGET-ID 700.

DEFINE FRAME fTasks
     bTaskAdd AT ROW 2 COL 1.8 WIDGET-ID 14 NO-TAB-STOP 
     taStatus AT ROW 1.86 COL 105.6 COLON-ALIGNED WIDGET-ID 20
     taPriority AT ROW 1.86 COL 130.6 COLON-ALIGNED WIDGET-ID 28
     taDueDays AT ROW 1.86 COL 156 COLON-ALIGNED WIDGET-ID 22
     brwTasks AT ROW 3.29 COL 2 WIDGET-ID 1200
     bTaskComplete AT ROW 2 COL 16.8 WIDGET-ID 18 NO-TAB-STOP 
     bTaskDelete AT ROW 2 COL 6.8 WIDGET-ID 286 NO-TAB-STOP 
     bTaskEdit AT ROW 2 COL 11.8 WIDGET-ID 10 NO-TAB-STOP 
     bTaskRefresh AT ROW 1.76 COL 165.6 WIDGET-ID 4 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 99.4 WIDGET-ID 26
     RECT-3 AT ROW 1.52 COL 98.4 WIDGET-ID 24
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Tasks" WIDGET-ID 1100.

DEFINE FRAME fLinked
     brwLinkedClaims AT ROW 2.43 COL 11 WIDGET-ID 2000
     bClaimLinkAdd AT ROW 2.43 COL 6 WIDGET-ID 366 NO-TAB-STOP 
     bClaimLinkDelete AT ROW 3.62 COL 6 WIDGET-ID 368 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Linked Claims" WIDGET-ID 2100.

DEFINE FRAME fContacts
     bContactEmail AT ROW 1.71 COL 24.2 WIDGET-ID 328 NO-TAB-STOP 
     coRole AT ROW 3.62 COL 54 COLON-ALIGNED WIDGET-ID 330
     coFullName AT ROW 4.81 COL 54 COLON-ALIGNED WIDGET-ID 296
     brwContacts AT ROW 3.62 COL 5 WIDGET-ID 800
     coJobTitle AT ROW 6 COL 54 COLON-ALIGNED WIDGET-ID 300
     coEmail AT ROW 7.19 COL 54 COLON-ALIGNED WIDGET-ID 302
     bContactCancel AT ROW 1.71 COL 19.4 WIDGET-ID 374 NO-TAB-STOP 
     coPhone AT ROW 8.38 COL 54 COLON-ALIGNED WIDGET-ID 304
     coFax AT ROW 9.57 COL 54 COLON-ALIGNED WIDGET-ID 332
     coMobile AT ROW 10.76 COL 54 COLON-ALIGNED WIDGET-ID 334
     coIsActive AT ROW 3.71 COL 108 WIDGET-ID 306
     coCompany AT ROW 4.81 COL 106 COLON-ALIGNED WIDGET-ID 308
     coAddr1 AT ROW 6 COL 106 COLON-ALIGNED WIDGET-ID 324
     coAddr2 AT ROW 7.19 COL 106 COLON-ALIGNED NO-LABEL WIDGET-ID 326
     coCity AT ROW 8.38 COL 106 COLON-ALIGNED WIDGET-ID 314
     coStateID AT ROW 8.38 COL 156 COLON-ALIGNED WIDGET-ID 320
     coCounty AT ROW 9.57 COL 106 COLON-ALIGNED WIDGET-ID 316
     coZipCode AT ROW 9.57 COL 152 COLON-ALIGNED WIDGET-ID 322
     coNotes AT ROW 12 COL 56 NO-LABEL WIDGET-ID 188
     bContactSave AT ROW 1.71 COL 14.6 WIDGET-ID 376 NO-TAB-STOP 
     bContactAdd AT ROW 1.71 COL 5 WIDGET-ID 284 NO-TAB-STOP 
     bContactDelete AT ROW 1.71 COL 9.8 WIDGET-ID 286 NO-TAB-STOP 
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 12.24 COL 48 WIDGET-ID 282
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Contacts" WIDGET-ID 300.

DEFINE FRAME fOverview
     ovStateID AT ROW 1.38 COL 18.2 COLON-ALIGNED WIDGET-ID 82
     ovAgentStat AT ROW 1.38 COL 91.2 COLON-ALIGNED WIDGET-ID 388
     ovDifficulty AT ROW 1.38 COL 128 COLON-ALIGNED WIDGET-ID 376
     ovStage AT ROW 1.38 COL 151 COLON-ALIGNED WIDGET-ID 234
     ovAgentID AT ROW 2.57 COL 18.2 COLON-ALIGNED WIDGET-ID 84
     bOverviewCancel AT ROW 2.52 COL 2.4 WIDGET-ID 386 NO-TAB-STOP 
     ovUrgency AT ROW 2.57 COL 128 COLON-ALIGNED WIDGET-ID 378
     ovAction AT ROW 2.57 COL 151 COLON-ALIGNED WIDGET-ID 374
     ovFileNumber AT ROW 3.76 COL 18.2 COLON-ALIGNED WIDGET-ID 42
     ovAgentError AT ROW 3.76 COL 64.2 COLON-ALIGNED WIDGET-ID 300
     ovSearcherError AT ROW 4.95 COL 18.2 COLON-ALIGNED WIDGET-ID 372
     ovInsuredName AT ROW 7.33 COL 18.2 COLON-ALIGNED WIDGET-ID 336
     ovCoverageID AT ROW 8.52 COL 18.2 COLON-ALIGNED WIDGET-ID 338
     ovEffDate AT ROW 8.52 COL 64.2 COLON-ALIGNED WIDGET-ID 340
     ovOrigLiabilityAmount AT ROW 9.71 COL 18.2 COLON-ALIGNED WIDGET-ID 344
     ovCvgPrimary AT ROW 9.71 COL 68.2 COLON-ALIGNED WIDGET-ID 370
     ovBorrowerName AT ROW 10.91 COL 18.2 COLON-ALIGNED WIDGET-ID 342
     ovRefYear AT ROW 12.19 COL 18.2 COLON-ALIGNED WIDGET-ID 392
     ovYearFirstReport AT ROW 12.19 COL 68.2 COLON-ALIGNED WIDGET-ID 394
     ovSummary AT ROW 13.62 COL 20.2 NO-LABEL WIDGET-ID 188
     ovPropAddr1 AT ROW 6.14 COL 104.4 COLON-ALIGNED WIDGET-ID 306
     ovPropAddr2 AT ROW 7.33 COL 104.4 COLON-ALIGNED NO-LABEL WIDGET-ID 350
     ovPropCity AT ROW 8.52 COL 104.4 COLON-ALIGNED WIDGET-ID 314
     ovPropStateID AT ROW 8.52 COL 159.4 COLON-ALIGNED WIDGET-ID 356
     ovPropCounty AT ROW 9.71 COL 104.4 COLON-ALIGNED WIDGET-ID 316
     ovPropZipCode AT ROW 9.71 COL 155.4 COLON-ALIGNED WIDGET-ID 360
     ovPropSubdivision AT ROW 10.91 COL 104.4 COLON-ALIGNED WIDGET-ID 358
     ovPropPrimary AT ROW 12.19 COL 104.4 COLON-ALIGNED WIDGET-ID 352
     ovPropResidential AT ROW 12.19 COL 159.4 COLON-ALIGNED WIDGET-ID 354
     ovPropLegalDesc AT ROW 13.62 COL 106.4 NO-LABEL WIDGET-ID 348
     ovALTARisk AT ROW 17.1 COL 18.4 COLON-ALIGNED WIDGET-ID 102
     ovALTAResp AT ROW 17.1 COL 104.4 COLON-ALIGNED WIDGET-ID 104
     brwDescCodes AT ROW 18.38 COL 20.2 WIDGET-ID 1500
     brwCauseCodes AT ROW 18.38 COL 106.4 WIDGET-ID 1400
     bOverviewSave AT ROW 1.43 COL 2.4 WIDGET-ID 384 NO-TAB-STOP 
     bCauseCodeDelete AT ROW 19.57 COL 101 WIDGET-ID 322 NO-TAB-STOP 
     bCauseCodeDown AT ROW 21.95 COL 101 WIDGET-ID 330 NO-TAB-STOP 
     bCauseCodeUp AT ROW 20.76 COL 101 WIDGET-ID 328 NO-TAB-STOP 
     bDescCodeAdd AT ROW 18.38 COL 14.8 WIDGET-ID 324 NO-TAB-STOP 
     bDescCodeDelete AT ROW 19.57 COL 14.8 WIDGET-ID 326 NO-TAB-STOP 
     bDescCodeDown AT ROW 21.95 COL 14.8 WIDGET-ID 332 NO-TAB-STOP 
     bDescCodeUp AT ROW 20.76 COL 14.8 WIDGET-ID 334 NO-TAB-STOP 
     bCauseCodeAdd AT ROW 18.38 COL 101 WIDGET-ID 320 NO-TAB-STOP 
     ovAttorneyError AT ROW 4.95 COL 64.2 COLON-ALIGNED WIDGET-ID 396
     ovSearcher AT ROW 6.14 COL 18.2 COLON-ALIGNED WIDGET-ID 398
     "Summary:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 13.71 COL 10.4 WIDGET-ID 282
     "Legal Desc:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 13.71 COL 94.4 WIDGET-ID 346
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY NO-HELP 
         SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D 
         AT COL 2.8 ROW 6.81
         SIZE 172 BY 23.38
         TITLE "Overview" WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "File"
         HEIGHT             = 29.67
         WIDTH              = 176
         MAX-HEIGHT         = 47.19
         MAX-WIDTH          = 296
         VIRTUAL-HEIGHT     = 47.19
         VIRTUAL-WIDTH      = 296
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fAccounting:FRAME = FRAME fMain:HANDLE
       FRAME fActivity:FRAME = FRAME fMain:HANDLE
       FRAME fContacts:FRAME = FRAME fMain:HANDLE
       FRAME fCoverage:FRAME = FRAME fMain:HANDLE
       FRAME fLinked:FRAME = FRAME fMain:HANDLE
       FRAME fLitigation:FRAME = FRAME fMain:HANDLE
       FRAME fOverview:FRAME = FRAME fMain:HANDLE
       FRAME fProperties:FRAME = FRAME fMain:HANDLE
       FRAME fRecovery:FRAME = FRAME fMain:HANDLE
       FRAME fTasks:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fAccounting
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME fAccounting:HIDDEN           = TRUE
       FRAME fAccounting:SENSITIVE        = FALSE.

ASSIGN 
       acCompleteRecoveries:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       acOpenRecoveries:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       acRequestedRecoveries:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       acWaivedRecoveries:READ-ONLY IN FRAME fAccounting        = TRUE.

/* SETTINGS FOR BUTTON bAddLAEAdjustment IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAddLAEPayment IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAddLossAdjustment IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAddLossPayment IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-10 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-11 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-12 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-7 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-8 IN FRAME fAccounting
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-9 IN FRAME fAccounting
   NO-ENABLE                                                            */
ASSIGN 
       tLAEApprovedPayments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAEApprovedPaymentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAECompletedPayments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAECurrentBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAEOpenAdjustments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAEOpenAdjustmentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAEOpenPayments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAEOpenPaymentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLAEPotentialCost:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossApprovedPayments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossApprovedPaymentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossCompletedPayments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossCurrentBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossOpenAdjustments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossOpenAdjustmentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossOpenPayments:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossOpenPaymentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tLossPotentialCost:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalApprovedPaymentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalCurrentBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalOpenAdjustmentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalOpenPaymentsBalance:READ-ONLY IN FRAME fAccounting        = TRUE.

ASSIGN 
       tTotalPotentialCost:READ-ONLY IN FRAME fAccounting        = TRUE.

/* SETTINGS FOR FRAME fActivity
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME fActivity:HIDDEN           = TRUE
       FRAME fActivity:SENSITIVE        = FALSE.

ASSIGN 
       avDateClosed:READ-ONLY IN FRAME fActivity        = TRUE
       avDateClosed:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

/* SETTINGS FOR FILL-IN avDateCreated IN FRAME fActivity
   NO-ENABLE                                                            */
ASSIGN 
       avDateCreated:READ-ONLY IN FRAME fActivity        = TRUE
       avDateCreated:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avDateReClosed:READ-ONLY IN FRAME fActivity        = TRUE
       avDateReClosed:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avDateReOpened:READ-ONLY IN FRAME fActivity        = TRUE
       avDateReOpened:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avDateTransferred:READ-ONLY IN FRAME fActivity        = TRUE
       avDateTransferred:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avUserClosed:READ-ONLY IN FRAME fActivity        = TRUE
       avUserClosed:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avUserCreated:READ-ONLY IN FRAME fActivity        = TRUE
       avUserCreated:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avUserReClosed:READ-ONLY IN FRAME fActivity        = TRUE
       avUserReClosed:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

ASSIGN 
       avUserReOpened:READ-ONLY IN FRAME fActivity        = TRUE
       avUserReOpened:PRIVATE-DATA IN FRAME fActivity     = 
                "READ-ONLY".

/* SETTINGS FOR BUTTON bActivityCancel IN FRAME fActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bActivitySave IN FRAME fActivity
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME fContacts
   NOT-VISIBLE Custom                                                   */
/* BROWSE-TAB brwContacts coFullName fContacts */
ASSIGN 
       FRAME fContacts:HIDDEN           = TRUE
       FRAME fContacts:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bContactAdd IN FRAME fContacts
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bContactCancel IN FRAME fContacts
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bContactDelete IN FRAME fContacts
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bContactEmail IN FRAME fContacts
   NO-ENABLE                                                            */
ASSIGN 
       bContactEmail:PRIVATE-DATA IN FRAME fContacts     = 
                "STATIC".

/* SETTINGS FOR BUTTON bContactSave IN FRAME fContacts
   NO-ENABLE                                                            */
ASSIGN 
       brwContacts:COLUMN-RESIZABLE IN FRAME fContacts       = TRUE
       brwContacts:COLUMN-MOVABLE IN FRAME fContacts         = TRUE.

ASSIGN 
       coNotes:RETURN-INSERTED IN FRAME fContacts  = TRUE.

/* SETTINGS FOR FRAME fCoverage
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwCoverage 1 fCoverage */
ASSIGN 
       FRAME fCoverage:HIDDEN           = TRUE
       FRAME fCoverage:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bCoverageAdd IN FRAME fCoverage
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCoverageCancel IN FRAME fCoverage
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCoverageDelete IN FRAME fCoverage
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCoverageSave IN FRAME fCoverage
   NO-ENABLE                                                            */
ASSIGN 
       brwCoverage:COLUMN-RESIZABLE IN FRAME fCoverage       = TRUE
       brwCoverage:COLUMN-MOVABLE IN FRAME fCoverage         = TRUE.

ASSIGN 
       cvCurrentLiabilityAmount:PRIVATE-DATA IN FRAME fCoverage     = 
                "READ-ONLY".

/* SETTINGS FOR FRAME fLinked
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwLinkedClaims 1 fLinked */
ASSIGN 
       FRAME fLinked:HIDDEN           = TRUE
       FRAME fLinked:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bClaimLinkAdd IN FRAME fLinked
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bClaimLinkDelete IN FRAME fLinked
   NO-ENABLE                                                            */
ASSIGN 
       brwLinkedClaims:COLUMN-RESIZABLE IN FRAME fLinked       = TRUE
       brwLinkedClaims:COLUMN-MOVABLE IN FRAME fLinked         = TRUE.

/* SETTINGS FOR FRAME fLitigation
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwLitigation TEXT-36 fLitigation */
ASSIGN 
       FRAME fLitigation:HIDDEN           = TRUE
       FRAME fLitigation:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bLitigationAdd IN FRAME fLitigation
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bLitigationCancel IN FRAME fLitigation
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bLitigationDelete IN FRAME fLitigation
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bLitigationSave IN FRAME fLitigation
   NO-ENABLE                                                            */
ASSIGN 
       brwLitigation:COLUMN-RESIZABLE IN FRAME fLitigation       = TRUE
       brwLitigation:COLUMN-MOVABLE IN FRAME fLitigation         = TRUE.

ASSIGN 
       liNotes:RETURN-INSERTED IN FRAME fLitigation  = TRUE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON bOpenSuppClaim IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       hdAssignedTo:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdClaimID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdDateClosed:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdDateCreated:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdLastActivity:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdLitigation:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdSignificant:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdStat:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdSuppClaimID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       hdType:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-33 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-34 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME fOverview
   NOT-VISIBLE Custom                                                   */
/* BROWSE-TAB brwDescCodes ovALTAResp fOverview */
/* BROWSE-TAB brwCauseCodes brwDescCodes fOverview */
ASSIGN 
       FRAME fOverview:HIDDEN           = TRUE
       FRAME fOverview:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bCauseCodeAdd IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCauseCodeDelete IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCauseCodeDown IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bCauseCodeUp IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDescCodeAdd IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDescCodeDelete IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDescCodeDown IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDescCodeUp IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOverviewCancel IN FRAME fOverview
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bOverviewSave IN FRAME fOverview
   NO-ENABLE                                                            */
ASSIGN 
       brwCauseCodes:COLUMN-RESIZABLE IN FRAME fOverview       = TRUE
       brwCauseCodes:COLUMN-MOVABLE IN FRAME fOverview         = TRUE.

ASSIGN 
       brwDescCodes:COLUMN-RESIZABLE IN FRAME fOverview       = TRUE
       brwDescCodes:COLUMN-MOVABLE IN FRAME fOverview         = TRUE.

ASSIGN 
       ovAgentStat:READ-ONLY IN FRAME fOverview        = TRUE
       ovAgentStat:PRIVATE-DATA IN FRAME fOverview     = 
                "READ-ONLY".

ASSIGN 
       ovCoverageID:READ-ONLY IN FRAME fOverview        = TRUE
       ovCoverageID:PRIVATE-DATA IN FRAME fOverview     = 
                "READ-ONLY".

/* SETTINGS FOR COMBO-BOX ovCvgPrimary IN FRAME fOverview
   NO-ENABLE                                                            */
ASSIGN 
       ovCvgPrimary:PRIVATE-DATA IN FRAME fOverview     = 
                "READ-ONLY".

ASSIGN 
       ovEffDate:READ-ONLY IN FRAME fOverview        = TRUE
       ovEffDate:PRIVATE-DATA IN FRAME fOverview     = 
                "READ-ONLY".

ASSIGN 
       ovOrigLiabilityAmount:READ-ONLY IN FRAME fOverview        = TRUE
       ovOrigLiabilityAmount:PRIVATE-DATA IN FRAME fOverview     = 
                "READ-ONLY".

ASSIGN 
       ovPropLegalDesc:RETURN-INSERTED IN FRAME fOverview  = TRUE.

ASSIGN 
       ovSummary:RETURN-INSERTED IN FRAME fOverview  = TRUE.

ASSIGN 
       ovYearFirstReport:READ-ONLY IN FRAME fOverview        = TRUE
       ovYearFirstReport:PRIVATE-DATA IN FRAME fOverview     = 
                "READ-ONLY".

/* SETTINGS FOR FRAME fProperties
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwProperties bPropertySave fProperties */
ASSIGN 
       FRAME fProperties:HIDDEN           = TRUE
       FRAME fProperties:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bPropertyAdd IN FRAME fProperties
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPropertyCancel IN FRAME fProperties
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPropertyDelete IN FRAME fProperties
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPropertySave IN FRAME fProperties
   NO-ENABLE                                                            */
ASSIGN 
       brwProperties:COLUMN-RESIZABLE IN FRAME fProperties       = TRUE
       brwProperties:COLUMN-MOVABLE IN FRAME fProperties         = TRUE.

ASSIGN 
       prLegalDesc:RETURN-INSERTED IN FRAME fProperties  = TRUE.

/* SETTINGS FOR FRAME fRecovery
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwBonds rePriorPolicyEffDate fRecovery */
ASSIGN 
       FRAME fRecovery:HIDDEN           = TRUE
       FRAME fRecovery:SENSITIVE        = FALSE.

/* SETTINGS FOR BUTTON bBondAdd IN FRAME fRecovery
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBondDelete IN FRAME fRecovery
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBondModify IN FRAME fRecovery
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRecoveryCancel IN FRAME fRecovery
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRecoverySave IN FRAME fRecovery
   NO-ENABLE                                                            */
ASSIGN 
       brwBonds:COLUMN-RESIZABLE IN FRAME fRecovery       = TRUE
       brwBonds:COLUMN-MOVABLE IN FRAME fRecovery         = TRUE.

ASSIGN 
       reRecoveryNotes:RETURN-INSERTED IN FRAME fRecovery  = TRUE.

/* SETTINGS FOR FRAME fTasks
   NOT-VISIBLE                                                          */
/* BROWSE-TAB brwTasks taDueDays fTasks */
ASSIGN 
       FRAME fTasks:HIDDEN           = TRUE
       FRAME fTasks:SENSITIVE        = FALSE.

ASSIGN 
       brwTasks:COLUMN-RESIZABLE IN FRAME fTasks       = TRUE
       brwTasks:COLUMN-MOVABLE IN FRAME fTasks         = TRUE.

/* SETTINGS FOR BUTTON bTaskAdd IN FRAME fTasks
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bTaskComplete IN FRAME fTasks
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bTaskDelete IN FRAME fTasks
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bTaskEdit IN FRAME fTasks
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bTaskRefresh IN FRAME fTasks
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBonds
/* Query rebuild information for BROWSE brwBonds
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimbond by claimbond.type.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwBonds */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCauseCodes
/* Query rebuild information for BROWSE brwCauseCodes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH causecode by claimcode.seq.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwCauseCodes */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwContacts
/* Query rebuild information for BROWSE brwContacts
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimcontact by claimcontact.contactID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwContacts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCoverage
/* Query rebuild information for BROWSE brwCoverage
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimcoverage by claimcoverage.coverageID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwCoverage */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDescCodes
/* Query rebuild information for BROWSE brwDescCodes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH desccode by desccode.seq.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDescCodes */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwLinkedClaims
/* Query rebuild information for BROWSE brwLinkedClaims
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimlink by claimlink.toClaimID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwLinkedClaims */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwLitigation
/* Query rebuild information for BROWSE brwLitigation
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimlitigation by claimcoverage.litigationID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwLitigation */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwProperties
/* Query rebuild information for BROWSE brwProperties
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimproperty by claimproperty.seq.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwProperties */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTasks
/* Query rebuild information for BROWSE brwTasks
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH task by task.stat by task.dueDate by task.taskID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwTasks */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fAccounting
/* Query rebuild information for FRAME fAccounting
     _Query            is NOT OPENED
*/  /* FRAME fAccounting */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fActivity
/* Query rebuild information for FRAME fActivity
     _Query            is NOT OPENED
*/  /* FRAME fActivity */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fContacts
/* Query rebuild information for FRAME fContacts
     _Query            is NOT OPENED
*/  /* FRAME fContacts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fCoverage
/* Query rebuild information for FRAME fCoverage
     _Query            is NOT OPENED
*/  /* FRAME fCoverage */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fLinked
/* Query rebuild information for FRAME fLinked
     _Query            is NOT OPENED
*/  /* FRAME fLinked */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fLitigation
/* Query rebuild information for FRAME fLitigation
     _Query            is NOT OPENED
*/  /* FRAME fLitigation */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fOverview
/* Query rebuild information for FRAME fOverview
     _Query            is NOT OPENED
*/  /* FRAME fOverview */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fProperties
/* Query rebuild information for FRAME fProperties
     _Query            is NOT OPENED
*/  /* FRAME fProperties */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fRecovery
/* Query rebuild information for FRAME fRecovery
     _Query            is NOT OPENED
*/  /* FRAME fRecovery */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fTasks
/* Query rebuild information for FRAME fTasks
     _Query            is NOT OPENED
*/  /* FRAME fTasks */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* File */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* File */
DO:
  run exitObject in this-procedure.
  
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
/*   RETURN NO-APPLY.                   */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* File */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAcct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAcct wWin
ON CHOOSE OF bAcct IN FRAME fMain /* Acct */
DO:
  run ActionAccounting in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fActivity
&Scoped-define SELF-NAME bActivityCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivityCancel wWin
ON CHOOSE OF bActivityCancel IN FRAME fActivity /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivitySave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivitySave wWin
ON CHOOSE OF bActivitySave IN FRAME fActivity /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fAccounting
&Scoped-define SELF-NAME bAddLAEAdjustment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddLAEAdjustment wWin
ON CHOOSE OF bAddLAEAdjustment IN FRAME fAccounting /* Adj */
DO:
   run NewReserve in this-procedure ("E").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddLAEPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddLAEPayment wWin
ON CHOOSE OF bAddLAEPayment IN FRAME fAccounting /* Pay */
DO:
   run NewPayable in this-procedure ("E").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddLossAdjustment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddLossAdjustment wWin
ON CHOOSE OF bAddLossAdjustment IN FRAME fAccounting /* Adj */
DO:
   run NewReserve in this-procedure ("L").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddLossPayment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddLossPayment wWin
ON CHOOSE OF bAddLossPayment IN FRAME fAccounting /* Pay */
DO:
   run newPayable in this-procedure ("L").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bAttr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAttr wWin
ON CHOOSE OF bAttr IN FRAME fMain /* Attr */
DO:
  run dialogclaimattribute.w (hFileDataSrv, output std-lo).
  if std-lo = true then /* If a change has been made refresh the claim data */
  refreshClaim().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fRecovery
&Scoped-define SELF-NAME bBondAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBondAdd wWin
ON CHOOSE OF bBondAdd IN FRAME fRecovery /* Add */
DO:
  run actionNewBond.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBondDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBondDelete wWin
ON CHOOSE OF bBondDelete IN FRAME fRecovery /* Del */
DO:
  deleteBond().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBondModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBondModify wWin
ON CHOOSE OF bBondModify IN FRAME fRecovery /* Mod */
DO:
  run actionModifyBond.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fOverview
&Scoped-define SELF-NAME bCauseCodeAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCauseCodeAdd wWin
ON CHOOSE OF bCauseCodeAdd IN FRAME fOverview /* Add */
DO:
  run actionNewCode(input "ClaimCause").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCauseCodeDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCauseCodeDelete wWin
ON CHOOSE OF bCauseCodeDelete IN FRAME fOverview /* Del */
DO:
  deleteCode("ClaimCause").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCauseCodeDown
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCauseCodeDown wWin
ON CHOOSE OF bCauseCodeDown IN FRAME fOverview /* Down */
DO:
  moveCode("ClaimCause", "DOWN").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCauseCodeUp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCauseCodeUp wWin
ON CHOOSE OF bCauseCodeUp IN FRAME fOverview /* Up */
DO:
  moveCode("ClaimCause", "UP").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fLinked
&Scoped-define SELF-NAME bClaimLinkAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClaimLinkAdd wWin
ON CHOOSE OF bClaimLinkAdd IN FRAME fLinked /* Add */
DO:
  run actionNewLink.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClaimLinkDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClaimLinkDelete wWin
ON CHOOSE OF bClaimLinkDelete IN FRAME fLinked /* Del */
DO:
  deleteLink().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose wWin
ON CHOOSE OF bClose IN FRAME fMain /* Close */
DO:
  run ActionClose in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fContacts
&Scoped-define SELF-NAME bContactAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactAdd wWin
ON CHOOSE OF bContactAdd IN FRAME fContacts /* Add */
DO:
  setPageState(activePageNumber, "Enabled").
  clearContact().
  saveStateEnable(activePageNumber, yes).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactCancel wWin
ON CHOOSE OF bContactCancel IN FRAME fContacts /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactDelete wWin
ON CHOOSE OF bContactDelete IN FRAME fContacts /* Del */
DO:
  deleteContact().
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactEmail wWin
ON CHOOSE OF bContactEmail IN FRAME fContacts /* Email */
DO:
  if avail claimcontact then
  do:
    if claimcontact.email <> "" 
    and claimcontact.email matches "*@*~~.*" then
    do:
      openURL("mailto:" + claimcontact.email).
    end.
    else
    do:
      message 
        "Email address is not valid."
        view-as alert-box.
    end.
  end.
  else
  message
    "No claim contact record is available."
    view-as alert-box.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bContactSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bContactSave wWin
ON CHOOSE OF bContactSave IN FRAME fContacts /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fCoverage
&Scoped-define SELF-NAME bCoverageAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCoverageAdd wWin
ON CHOOSE OF bCoverageAdd IN FRAME fCoverage /* Add */
DO:
  setPageState(activePageNumber, "Enabled").
  clearCoverage().
  saveStateEnable(activePageNumber, yes).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCoverageCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCoverageCancel wWin
ON CHOOSE OF bCoverageCancel IN FRAME fCoverage /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCoverageDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCoverageDelete wWin
ON CHOOSE OF bCoverageDelete IN FRAME fCoverage /* Del */
DO:
  deleteCoverage().
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCoverageSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCoverageSave wWin
ON CHOOSE OF bCoverageSave IN FRAME fCoverage /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fOverview
&Scoped-define SELF-NAME bDescCodeAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDescCodeAdd wWin
ON CHOOSE OF bDescCodeAdd IN FRAME fOverview /* Add */
DO:
  run actionNewCode(input "ClaimDescription").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDescCodeDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDescCodeDelete wWin
ON CHOOSE OF bDescCodeDelete IN FRAME fOverview /* Del */
DO:
  deleteCode("ClaimDescription").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDescCodeDown
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDescCodeDown wWin
ON CHOOSE OF bDescCodeDown IN FRAME fOverview /* Down */
DO:
  moveCode("ClaimDescription", "DOWN").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDescCodeUp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDescCodeUp wWin
ON CHOOSE OF bDescCodeUp IN FRAME fOverview /* Up */
DO:
  moveCode("ClaimDescription", "UP").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocs wWin
ON CHOOSE OF bDocs IN FRAME fMain /* Docs */
DO:
  run ActionDocs in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fLitigation
&Scoped-define SELF-NAME bLitigationAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLitigationAdd wWin
ON CHOOSE OF bLitigationAdd IN FRAME fLitigation /* Add */
DO:
  setPageState(activePageNumber, "Enabled").
  clearLitigation().
  saveStateEnable(activePageNumber, yes).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLitigationCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLitigationCancel wWin
ON CHOOSE OF bLitigationCancel IN FRAME fLitigation /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLitigationDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLitigationDelete wWin
ON CHOOSE OF bLitigationDelete IN FRAME fLitigation /* Del */
DO:
  deleteLitigation().
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLitigationSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLitigationSave wWin
ON CHOOSE OF bLitigationSave IN FRAME fLitigation /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bMarkClaim
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMarkClaim wWin
ON CHOOSE OF bMarkClaim IN FRAME fMain /* Claim */
DO:
  setToClaim().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bMarkMatter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bMarkMatter wWin
ON CHOOSE OF bMarkMatter IN FRAME fMain /* Matter */
DO:
  setToMatter().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewNote wWin
ON CHOOSE OF bNewNote IN FRAME fMain /* New Note */
DO:
  run dialogaddnote.w (pBaseClaimID, hFileDataSrv).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNotes wWin
ON CHOOSE OF bNotes IN FRAME fMain /* Notes */
DO:
  run ActionNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOpenSuppClaim
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOpenSuppClaim wWin
ON CHOOSE OF bOpenSuppClaim IN FRAME fMain /* Open */
DO:
  run ActionView (hdSuppClaimID:input-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fOverview
&Scoped-define SELF-NAME bOverviewCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOverviewCancel wWin
ON CHOOSE OF bOverviewCancel IN FRAME fOverview /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOverviewSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOverviewSave wWin
ON CHOOSE OF bOverviewSave IN FRAME fOverview /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fProperties
&Scoped-define SELF-NAME bPropertyAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPropertyAdd wWin
ON CHOOSE OF bPropertyAdd IN FRAME fProperties /* Add */
DO:
  setPageState(activePageNumber, "Enabled").
  clearProperty().
  saveStateEnable(activePageNumber, yes).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPropertyCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPropertyCancel wWin
ON CHOOSE OF bPropertyCancel IN FRAME fProperties /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPropertyDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPropertyDelete wWin
ON CHOOSE OF bPropertyDelete IN FRAME fProperties /* Del */
DO:
  deleteProperty().
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPropertySave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPropertySave wWin
ON CHOOSE OF bPropertySave IN FRAME fProperties /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fRecovery
&Scoped-define SELF-NAME bRecoveryCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRecoveryCancel wWin
ON CHOOSE OF bRecoveryCancel IN FRAME fRecovery /* X */
DO:
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRecoverySave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRecoverySave wWin
ON CHOOSE OF bRecoverySave IN FRAME fRecovery /* Save */
DO:
  run ActionSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bReopen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReopen wWin
ON CHOOSE OF bReopen IN FRAME fMain /* Reopen */
DO:
  run ActionOpen in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBonds
&Scoped-define FRAME-NAME fRecovery
&Scoped-define SELF-NAME brwBonds
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBonds wWin
ON DEFAULT-ACTION OF brwBonds IN FRAME fRecovery /* Bonds / Carriers */
DO:
  if avail claim and claim.stat = "O" then
  run ActionModifyBond.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBonds wWin
ON ROW-DISPLAY OF brwBonds IN FRAME fRecovery /* Bonds / Carriers */
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBonds wWin
ON START-SEARCH OF brwBonds IN FRAME fRecovery /* Bonds / Carriers */
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCauseCodes
&Scoped-define FRAME-NAME fOverview
&Scoped-define SELF-NAME brwCauseCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCauseCodes wWin
ON DEFAULT-ACTION OF brwCauseCodes IN FRAME fOverview /* Company Cause Codes */
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCauseCodes wWin
ON ROW-DISPLAY OF brwCauseCodes IN FRAME fOverview /* Company Cause Codes */
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCauseCodes wWin
ON START-SEARCH OF brwCauseCodes IN FRAME fOverview /* Company Cause Codes */
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwContacts
&Scoped-define FRAME-NAME fContacts
&Scoped-define SELF-NAME brwContacts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts wWin
ON DEFAULT-ACTION OF brwContacts IN FRAME fContacts
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts wWin
ON ROW-DISPLAY OF brwContacts IN FRAME fContacts
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts wWin
ON START-SEARCH OF brwContacts IN FRAME fContacts
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwContacts wWin
ON VALUE-CHANGED OF brwContacts IN FRAME fContacts
DO:
  displayContacts().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCoverage
&Scoped-define FRAME-NAME fCoverage
&Scoped-define SELF-NAME brwCoverage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCoverage wWin
ON DEFAULT-ACTION OF brwCoverage IN FRAME fCoverage
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCoverage wWin
ON ROW-DISPLAY OF brwCoverage IN FRAME fCoverage
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCoverage wWin
ON START-SEARCH OF brwCoverage IN FRAME fCoverage
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCoverage wWin
ON VALUE-CHANGED OF brwCoverage IN FRAME fCoverage
DO:
  displayCoverage().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDescCodes
&Scoped-define FRAME-NAME fOverview
&Scoped-define SELF-NAME brwDescCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDescCodes wWin
ON DEFAULT-ACTION OF brwDescCodes IN FRAME fOverview /* Company Description Codes */
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDescCodes wWin
ON ROW-DISPLAY OF brwDescCodes IN FRAME fOverview /* Company Description Codes */
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDescCodes wWin
ON START-SEARCH OF brwDescCodes IN FRAME fOverview /* Company Description Codes */
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwLinkedClaims
&Scoped-define FRAME-NAME fLinked
&Scoped-define SELF-NAME brwLinkedClaims
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLinkedClaims wWin
ON DEFAULT-ACTION OF brwLinkedClaims IN FRAME fLinked /* Supplemental/Related Claims */
DO:
  if not avail claimlink then
  return no-apply.
  
  run ActionView (claimlink.toClaimID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLinkedClaims wWin
ON ROW-DISPLAY OF brwLinkedClaims IN FRAME fLinked /* Supplemental/Related Claims */
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLinkedClaims wWin
ON START-SEARCH OF brwLinkedClaims IN FRAME fLinked /* Supplemental/Related Claims */
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwLitigation
&Scoped-define FRAME-NAME fLitigation
&Scoped-define SELF-NAME brwLitigation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLitigation wWin
ON DEFAULT-ACTION OF brwLitigation IN FRAME fLitigation
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLitigation wWin
ON ROW-DISPLAY OF brwLitigation IN FRAME fLitigation
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLitigation wWin
ON START-SEARCH OF brwLitigation IN FRAME fLitigation
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLitigation wWin
ON VALUE-CHANGED OF brwLitigation IN FRAME fLitigation
DO:
  displayLitigation().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwProperties
&Scoped-define FRAME-NAME fProperties
&Scoped-define SELF-NAME brwProperties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProperties wWin
ON DEFAULT-ACTION OF brwProperties IN FRAME fProperties
DO:
   /*run ActionOpen in this-procedure (output std-lo).*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProperties wWin
ON ROW-DISPLAY OF brwProperties IN FRAME fProperties
DO:
  /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProperties wWin
ON START-SEARCH OF brwProperties IN FRAME fProperties
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwProperties wWin
ON VALUE-CHANGED OF brwProperties IN FRAME fProperties
DO:
  displayProperties().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTasks
&Scoped-define FRAME-NAME fTasks
&Scoped-define SELF-NAME brwTasks
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTasks wWin
ON DEFAULT-ACTION OF brwTasks IN FRAME fTasks /* Tasks */
DO:
  run showTask in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTasks wWin
ON ROW-DISPLAY OF brwTasks IN FRAME fTasks /* Tasks */
DO:
 /*{lib/brw-rowdisplay.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTasks wWin
ON START-SEARCH OF brwTasks IN FRAME fTasks /* Tasks */
DO:
  /*{lib/brw-startsearch.i}*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTasks wWin
ON VALUE-CHANGED OF brwTasks IN FRAME fTasks /* Tasks */
DO:
/*   if available task                 */
/*    then tCurrentSeq = task.taskID.  */
/*    else tCurrentSeq = 0.            */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bTransfer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTransfer wWin
ON CHOOSE OF bTransfer IN FRAME fMain /* Transfer */
DO:
  def buffer claim for claim.

  std-lo = checkDataChanged().
  if not std-lo 
   then return no-apply.

  std-lo = getBaseClaim().
  if not std-lo 
   then return no-apply.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not available claim 
   then
    do: 
        MESSAGE "Source data not available."
            VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.

  run dialogtransfer.w (input hFileDataSrv, 
                        input claim.assignedTo, /* Current Assignee */
                        output std-lo,
                        output std-ch).         /* New Assignee */

  if not std-lo 
   then return.

  claim.assignedTo = std-ch.

  displayHeader().
  setHeaderState().
  
/* setAssignee(). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fCoverage
&Scoped-define SELF-NAME cvCoverageID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cvCoverageID wWin
ON LEAVE OF cvCoverageID IN FRAME fCoverage /* Policy Number */
DO:
  if self:screen-value <> "" then 
  do: 
    publish "IsPolicyValid" (input claim.claimID,
                             input self:input-value,
                             input claim.agentID,
                             output xEffDate,
                             output xLiabilityAmount,
                             output xGrossPremium,
                             output xFormID,
                             output xStateID,
                             output xFileNumber,
                             output xStatCode,
                             output xNetPremium,
                             output xRetention,
                             output xCountyID,
                             output xResidential,
                             output xInvoiceDate,
                             output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                             output std-ch).
                             
    if tMsgStat = "S" or tMsgStat = "W" then  /* Success or Warning */
    do:
      if cvEffDate:input-value = ? and xEffDate <> ? then
      assign cvEffDate:screen-value = string(xEffDate).
      
      if (cvCoverageYear:input-value = ? or cvCoverageYear:input-value = 0)
      and xEffDate <> ? then
      assign cvCoverageYear:screen-value = string(year(xEffDate)).

      if cvOrigLiabilityAmount:input-value = 0 and xLiabilityAmount <> 0 then
      assign cvOrigLiabilityAmount:screen-value = string(xLiabilityAmount).
      
      if cvCurrentLiabilityAmount:input-value = 0 and xLiabilityAmount <> 0 then
      assign cvCurrentLiabilityAmount:screen-value = string(xLiabilityAmount).
    end.
    else   
    if tMsgStat = "E" then  /* Error */
    do:
      message std-ch view-as alert-box error.
    end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Accounting
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Accounting wWin
ON CHOOSE OF MENU-ITEM m_Accounting /* Accounting */
DO:
  apply "choose" to bAcct in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_Note
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_Note wWin
ON CHOOSE OF MENU-ITEM m_Add_Note /* Add Note */
DO:
  run dialogaddnote.w (pBaseClaimID, hFileDataSrv).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Attributes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Attributes wWin
ON CHOOSE OF MENU-ITEM m_Attributes /* Attributes */
DO:
  apply "choose" to bAttr in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Close wWin
ON CHOOSE OF MENU-ITEM m_Close /* Close */
DO:
  apply "choose" to bClose in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Documents wWin
ON CHOOSE OF MENU-ITEM m_Documents /* Documents */
DO:
  apply "choose" to bDocs in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Mark_as_Claim
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Mark_as_Claim wWin
ON CHOOSE OF MENU-ITEM m_Mark_as_Claim /* Set Type to Claim */
DO:
  apply "choose" to bMarkClaim in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Mark_as_Matter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Mark_as_Matter wWin
ON CHOOSE OF MENU-ITEM m_Mark_as_Matter /* Set Type to Matter */
DO:
  apply "choose" to bMarkMatter in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Notes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Notes wWin
ON CHOOSE OF MENU-ITEM m_Notes /* Notes */
DO:
  apply "choose" to bNotes in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reopen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reopen wWin
ON CHOOSE OF MENU-ITEM m_Reopen /* Reopen */
DO:
  apply "choose" to bReopen in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Transfer_to
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Transfer_to wWin
ON CHOOSE OF MENU-ITEM m_Transfer_to /* Transfer to... */
DO:
  apply "choose" to bTransfer in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fOverview
&Scoped-define SELF-NAME ovAgentError
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ovAgentError wWin
ON VALUE-CHANGED OF ovAgentError IN FRAME fOverview /* Agent Error */
DO:
  std-lo = getBaseClaim().
  if std-lo
   then
    for first claim no-lock
        where claim.agentError <> self:screen-value:
        
      std-ch = "".
      publish "GetCredentialsName" (output std-ch).
      if std-ch = ""
       then std-ch = "Someone".
      tAgentErrorSubject = std-ch + " has changed the value of Agent Error from '" + 
                           claim.agentError + "' to '" + self:screen-value + "'".
    end.
  saveStateEnable(activePageNumber, no).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ovAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ovAgentID wWin
ON VALUE-CHANGED OF ovAgentID IN FRAME fOverview /* Agent */
DO:
  find first agent where agent.agentID = ovAgentID:input-value no-error.
  std-ch = "".
  publish "GetAgentStatusDesc" (input if avail agent then agent.stat else "",
                                output std-ch).
  ovAgentStat:screen-value = std-ch.
  saveStateEnable(activePageNumber, no).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ovAttorneyError
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ovAttorneyError wWin
ON VALUE-CHANGED OF ovAttorneyError IN FRAME fOverview /* Attorney Error */
DO:
  std-lo = getBaseClaim().
  if std-lo
   then
    for first claim no-lock
        where claim.attorneyError <> self:screen-value:
        
      std-ch = "".
      publish "GetCredentialsName" (output std-ch).
      if std-ch = ""
       then std-ch = "Someone".
      tAttorneyErrorSubject = std-ch + " has changed the value of Attorney Error from '" + 
                              claim.attorneyError + "' to '" + self:screen-value + "'".
    end.
  saveStateEnable(activePageNumber, no).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME ovStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL ovStateID wWin
ON VALUE-CHANGED OF ovStateID IN FRAME fOverview /* State */
DO:
  saveStateEnable(activePageNumber, no).
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fRecovery
&Scoped-define SELF-NAME reSeekingRecovery
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL reSeekingRecovery wWin
ON VALUE-CHANGED OF reSeekingRecovery IN FRAME fRecovery /* Seeking Recovery */
DO:
  std-lo = getBaseClaim().
  if std-lo
   then
    for first claim no-lock
        where claim.seekingRecovery <> self:screen-value:
        
      std-ch = "".
      publish "GetCredentialsName" (output std-ch).
      if std-ch = ""
       then std-ch = "Someone".
      tSeekingRecoverySubject = std-ch + " has changed the value of Seeking Recovery from '" + 
                                claim.seekingRecovery + "' to '" + self:screen-value + "'".
    end.
  saveStateEnable(activePageNumber, no).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwBonds
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}
{lib/win-main.i}

subscribe to "RefreshAccounting" anywhere.
subscribe to "RefreshCoverages" anywhere.

/* Trigger to detect changes by the user */
ON 'value-changed':U anywhere 
DO:
  assign
    std-ch = ""
    std-ha = focus.
  do while std-ha <> ?:
    /*std-ch = std-ch + "," + std-ha:type.*/
    if std-ha:type = "DIALOG-BOX" or std-ha:type = "WINDOW" then
    do:
      tWinHandle = std-ha.
      leave.
    end.
    std-ha = std-ha:parent.
  end.
  /*std-ch = trim(std-ch,",").*/
  
  /*
  message 'you changed it' skip
    'function:' last-event:function skip
    'type:' focus:type skip
    'name:' focus:name skip
    /*'parent type:' std-ha:type skip
    'parent name:' std-ha:name skip*/
    'parent list:' std-ch skip
    'this-procedure:' tWinHandle:instantiating-procedure = this-procedure skip
    view-as alert-box.*/

  if valid-handle(tWinHandle) then
  do:
    if tWinHandle:instantiating-procedure = this-procedure
    and focus:type <> "BROWSE"
    and avail claim 
    and claim.stat <> "C" then
    do:
      if dataChanged = no then
      saveStateEnable(activePageNumber, no).
      
      /* Start countdown timer for auto save.
      
        My thinking here is that as long as the
        user is changing something, that is
        'value-changed' is firing, then continue to 
        reset the countdown timer (say 10 seconds).
        Once activity has ceased, then the timer control
        will cause the 10 seconds to expire and then
        fire the ActionSave procedure.
        
        Also, if the user tries to close the window
        with any changes pending, then a warning can
        be displayed and appropriate action taken.
        
        OR
        
        I can just set the 'dataChanged' flag to yes
        and the user has to always click the button to
        save, but on window close, I can warn them
        
      */
    end.
  end.
END.

{lib/pbupdate.i "'Retrieving data...'" 5}
run filedatasrv.p persistent set hFileDataSrv (input pBaseClaimID,
                                               input this-procedure).

{lib/pbupdate.i "'Formatting screen...'" 80}

{lib/get-state-list.i &combo=ovStateID &firstValue="'Unknown,Unknown'" &addAll=true}
{lib/get-state-list.i &combo=ovPropStateID &useSingle=true &useFilter=false &onlyActive=false}
{lib/get-state-list.i &combo=prStateID     &useSingle=true &useFilter=false &onlyActive=false}
{lib/get-state-list.i &combo=coStateID     &useSingle=true &useFilter=false &onlyActive=false}
{lib/get-agent-list.i &combo=ovAgentID &state=ovStateID &frame-name=fOverview &noApply=true &addUnknown=true}

run initializeObject in this-procedure.

{lib/pbupdate.i "'Finalizing screen...'" 98}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAccounting wWin 
PROCEDURE ActionAccounting PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if valid-handle(hAcctWindow) 
 then
  do: run ShowWindow in hAcctWindow no-error.
      return.
  end.

empty temp-table claimacct.
run wfileacct.w persistent set hAcctWindow (pBaseClaimID, hFileDataSrv).
/* run "InitializeObject" in std-ha.  */
tAccountingLoaded = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionClose wWin 
PROCEDURE ActionClose :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tNote as char no-undo.

  std-lo = checkDataChanged().
  if not std-lo 
   then return.

  std-lo = getBaseClaim().
  if not std-lo 
   then return.

  if claim.agentError = "" or claim.agentError = ? then
  do:
    message
      "This file cannot be closed until the Agent Error status is determined." 
      skip(1)
      "Please set the Agent Error flag on the Overview tab and enter any related notes."
      view-as alert-box.
    return.
  end.

  run dialogclose.w (pBaseClaimID, claim.stage, output tNote, output std-lo).
  if not std-lo 
   then return.

  run SetClosed in hFileDataSrv (tNote, output std-lo, output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
          VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.

  publish "GetCredentialsID" (output std-ch).

  claim.stat = "C".

  if claim.dateClosed = ? 
   then assign
          claim.dateClosed = now
          claim.userClosed = std-ch
          .
   else assign
          claim.dateReclosed = now
          claim.userReclosed = std-ch
          .

  displayHeader().
  displayActivity().
  setHeaderState().

  do std-in = 1 to 10:
    discardPage(std-in).
    saveStateDisable(std-in).
    setPageState(std-in, "ReadOnly").
  end.

/*   if valid-handle(hNoteWindow) then  */
/*   run RefreshNotes in hNoteWindow.   */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDocs wWin 
PROCEDURE ActionDocs PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if valid-handle(hDocWindow) 
  then
   do: run ShowWindow in hDocWindow no-error.
       return.
   end.

  /*run docstart.w.*/

  std-ch = "new,delete,open,modify,share,send,request".   /* permissions */

  if not avail claim then
  do:
    message 
      "Claim " + string(pBaseClaimID) + " is not available."
      view-as alert-box.
  end.
  
  run documents.w persistent set hDocWindow
    ("CLM",
     "/claims"
      + "/" + substring(string(pBaseClaimID),1,4)
      + "/" + string(pBaseClaimID) + "/",
     "File " + string(pBaseClaimID),
     std-ch,
     "claims",
     string(pBaseClaimID),
     "" /* Seq */) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModifyBond wWin 
PROCEDURE ActionModifyBond PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def var tBondID as int no-undo.
  def var tType as char no-undo.
  def var tID as char no-undo.
  def var tEntity as char no-undo.
  def var tEffDate as date no-undo.
  def var tAmount as dec no-undo.
  def var tCancel as logical no-undo.

  if not avail claimbond then
  do:
    message 
      "No record is selected to modify."
      view-as alert-box.
    return.
  end.

  if num-results("brwBonds") > 0 then
  brwBonds:select-focused-row() in frame fRecovery.
  
  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
  
  dataChanged = yes. /* Prevent 'value-changed' event from enabling save buttons
                        while in the dialog box */
                        
  assign
    tType = claimbond.type
    tID = claimbond.ID
    tEntity = claimbond.entity
    tEffDate = claimbond.effDate
    tAmount = claimbond.amount
    .
                            
  run dialogclaimbond.w (input no,            /* Not a new record */
                         input-output tType,
                         input-output tID,
                         input-output tEntity,
                         input-output tEffDate,
                         input-output tAmount,
                         output tCancel).
  dataChanged = no.
  if tCancel then return.
  
  empty temp-table tempclaimbond no-error.
  
  create tempclaimbond.
  buffer-copy claimbond to tempclaimbond
  assign tempclaimbond.type = tType
         tempclaimbond.ID = tID
         tempclaimbond.entity = tEntity
         tempclaimbond.effDate = tEffDate
         tempclaimbond.amount = tAmount
         .
  
  run ModifyClaimBond in hFileDataSrv (input table tempclaimbond,
                                               output std-lo,
                                               output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return.
  end.
    
  run GetClaimBonds in hFileDataSrv (output table claimbond).
  
  openBonds().
  
  /* refreshClaim(). */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewBond wWin 
PROCEDURE ActionNewBond PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def var tBondID as int no-undo.
  def var tType as char no-undo.
  def var tID as char no-undo.
  def var tEntity as char no-undo.
  def var tEffDate as date no-undo.
  def var tAmount as dec no-undo.
  def var tCancel as logical no-undo.

  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
  
  dataChanged = yes. /* Prevent 'value-changed' event from enabling save buttons
                        while in the dialog box */
  
  run dialogclaimbond.w (input yes,           /* New record */
                         input-output tType,
                         input-output tID,
                         input-output tEntity,
                         input-output tEffDate,
                         input-output tAmount,
                         output tCancel).
  dataChanged = no.
  if tCancel then return.
  
  empty temp-table tempclaimbond no-error.
  
  create tempclaimbond.
  assign tempclaimbond.claimID = pBaseClaimID
         tempclaimbond.bondID = 0     /* Assigned server-side */
         tempclaimbond.type = tType
         tempclaimbond.ID = tID
         tempclaimbond.entity = tEntity
         tempclaimbond.effDate = tEffDate
         tempclaimbond.amount = tAmount
         .
  
  run NewClaimBond in hFileDataSrv (input table tempclaimbond,
                                            output tBondID,
                                            output std-lo,
                                            output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return.
  end.
    
  run GetClaimBonds in hFileDataSrv (output table claimbond).
  
  openBonds().
  
  /* refreshClaim(). */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewCode wWin 
PROCEDURE ActionNewCode PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def input param pCodeType as char.
  
  def var tCode as char no-undo.
  def var tCancel as logical no-undo.

  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
  
  dataChanged = yes. /* Prevent 'value-changed' event from enabling save buttons
                        while in the dialog box */
  run dialogclaimcode.w (input pCodeType,
                         output tCode,
                         output tCancel).
  dataChanged = no.
  if tCancel then return.
  
  
  empty temp-table tempclaimcode no-error.
  
  create tempclaimcode.
  assign tempclaimcode.claimID = pBaseClaimID
         tempclaimcode.codeType = pCodetype
         tempclaimcode.seq = 0
         tempclaimcode.code = tCode
         tempclaimcode.dateAdded = ? /* Set server-side */
         tempclaimcode.uid = ""      /* Set server-side */
         .
  
  run NewClaimCode in hFileDataSrv (input table tempclaimcode,
                                            output std-lo,
                                            output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return.
  end.
    
  run GetClaimCodes in hFileDataSrv (output table claimcode).
  
  case pCodeType:
    when "ClaimDescription" then
    openDescCodes().
    when "ClaimCause" then
    openCauseCodes().
    otherwise
    .
  end case.
  
  /* refreshClaim(). */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewLink wWin 
PROCEDURE ActionNewLink PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def var tToClaimID as int no-undo.
  def var tCancel as logical no-undo.

  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).
  
  dataChanged = yes. /* Prevent 'value-changed' event from enabling save buttons
                        while in the dialog box */
  
  run dialogclaimlink.w (input pBaseClaimID,
                         input table state,
                         input table agent,
                         input table claimlink,
                         output tToClaimID,
                         output tCancel).
  dataChanged = no.
  if tCancel then return.
  
  empty temp-table tempclaimLink no-error.
  
  create tempclaimLink.
  assign tempclaimLink.fromClaimID = pBaseClaimID
         tempclaimLink.toClaimID = tToClaimID
         tempclaimLink.linkType = "R"         /* (R)elated */
         .
  
  run NewClaimLink in hFileDataSrv (input table tempclaimLink,
                                            output std-lo,
                                            output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return.
  end.
    
  run GetClaimLinks in hFileDataSrv (output table claimLink).
  
  openLinkedClaims().
  
  /* refreshClaim(). */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNotes wWin 
PROCEDURE ActionNotes PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if valid-handle(hNoteWindow) 
 then
  do: run ShowWindow in hNoteWindow no-error.
      return.
  end.

publish "GetNoteWindow" (output std-ch).
if std-ch = "B" 
 then run wfilenotes2.w persistent set hNoteWindow (pBaseClaimID, hFileDataSrv).
 else run wfilenotes.w persistent set hNoteWindow (pBaseClaimID, hFileDataSrv).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionOpen wWin 
PROCEDURE ActionOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tNote as char no-undo.

  std-lo = checkDataChanged().
  if not std-lo
   then return.

  std-lo = getBaseClaim().
  if not std-lo 
   then return.
  
  run dialogopen.w (pBaseClaimID, output tNote, output std-lo).
  if not std-lo 
   then return.

  run SetOpen in hFileDataSrv (tNote,
                               output std-lo,
                               output std-ch).

  if not std-lo 
   then
    do:
        MESSAGE std-ch
          VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.


  publish "GetCredentialsID" (output std-ch).

  claim.stat = "O".
  claim.dateReopened = now.
  claim.userReopened = std-ch.

  displayHeader().
  displayActivity().
  setHeaderState().
  
  do std-in = 1 to 10:
    setPageState(std-in, "Enabled").
    discardPage(std-in).
    saveStateDisable(std-in).
  end.

/*   if valid-handle(hNoteWindow) then */
/*   run RefreshNotes in hNoteWindow.  */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionReports wWin 
PROCEDURE ActionReports PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* We can use this to run the QuickList builder.
    It doesn't really make sense from the File View window.
    Instead, it is now callable from the main Claims window.
    Removed 12.30.2016 D.Sinclair
 */
 run sys/wlistsave.w persistent.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSave wWin 
PROCEDURE ActionSave PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

std-lo = savePage(activePageNumber).
if not std-lo then
return.

/* prompt for notes if agent error was changed */
if std-lo and tAgentErrorSubject > ""
 then run dialogpromptnote.w (input pBaseClaimID,
                              input "1",
                              input tAgentErrorSubject,
                              input hFileDataSrv
                              ).

/* prompt for notes if attorney error was changed */
if std-lo and tAttorneyErrorSubject > ""
 then run dialogpromptnote.w (input pBaseClaimID,
                              input "1",
                              input tAttorneyErrorSubject,
                              input hFileDataSrv
                              ).
                              
/* prompt for notes if seeking recovery was changed */
if std-lo and tSeekingRecoverySubject > ""
 then run dialogpromptnote.w (input pBaseClaimID,
                              input "1",
                              input tSeekingRecoverySubject,
                              input hFileDataSrv
                              ).
                              
assign
  tAgentErrorSubject = ""
  tAttorneyErrorSubject = ""
  tSeekingRecoverySubject = ""
  .

saveStateDisable(activePageNumber).
                 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionView wWin 
PROCEDURE ActionView PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input param pClaimID as int.

 /* das: File opening is handled by wclm.w */
 publish "ClaimSelected" (string(pClaimID)).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Overview|Coverage|Properties|Contacts|Accounting|Recovery|Litigation|Linked Claims|Activity|Tasks' + 'FolderTabWidth17.25FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 5.71 , 2.00 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 24.76 , 174.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changePage wWin 
PROCEDURE changePage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/

  RUN SUPER.
  
  {get CurrentPage activePageNumber}.
  
  case activePageNumber:
  when 1 
   then
    do: 
        if not tOverviewLoaded then
        do:
          run GetClaim in hFileDataSrv (output table claim).
          run GetClaimCodes in hFileDataSrv (output table claimcode).
          run GetClaimCoverages in hFileDataSrv (output table claimcoverage).
          run GetClaimProperties in hFileDataSrv (output table claimproperty).
          openDescCodes().
          openCauseCodes().
          displayOverview().
          tOverviewLoaded = yes.
        end.
        
        view frame fOverview.
        enable all with frame fOverview.
        setPageState(activePageNumber, "Enabled").
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
        run AgentComboHide (false).
    end.
  when 2 
   then
    do: 
        if not tCoverageLoaded
         then run RefreshCoverages in this-procedure.
        
        hide frame fOverview.
        view frame fCoverage.
        enable all with frame fCoverage.
        setPageState(activePageNumber, "Enabled").
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
        apply 'entry' to brwCoverage.
    end.
  when 3 
   then
    do: 
        if not tPropertiesLoaded then
        do:
          run GetClaimProperties in hFileDataSrv (output table claimproperty).
          openProperties().
          displayProperties().
          tPropertiesLoaded = yes.
        end.
        
        hide frame fOverview.
        hide frame fCoverage.
        view frame fProperties.
        enable all with frame fProperties.
        setPageState(activePageNumber, "Enabled").
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
        apply 'entry' to brwProperties.
    end.
  when 4 
   then
    do: 
        if not tContactsLoaded then
        do:
          run GetClaimContacts in hFileDataSrv (output table claimcontact).
          openContacts().
          displayContacts().
          tContactsLoaded = yes.
        end.
        
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        view frame fContacts.
        enable all with frame fContacts.
        setPageState(activePageNumber, "Enabled").
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
        apply 'entry' to brwContacts.
    end.
  when 5
   then
    do: run RefreshAccounting in this-procedure.
        
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        view frame fAccounting.
        enable all with frame fAccounting.
        setPageState(activePageNumber, "ReadOnly").
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
        displayAccounting().
    end.
  when 6
   then
    do: 
        if not tRecoveryLoaded then
        do:
          run GetClaimBonds in hFileDataSrv (output table claimbond).
          openBonds().
          displayRecovery().
          tRecoveryLoaded = yes.
        end.
        
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        view frame fRecovery.
        enable all with frame fRecovery.
        setPageState(activePageNumber, "Enabled").
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
    end.
  when 7
   then
    do: 
        if not tLitigationLoaded then
        do:
          run GetClaimLitigations in hFileDataSrv (output table claimlitigation).
          getPrimaryLitigation().
          openLitigation().
          displayLitigation().
          tLitigationLoaded = yes.
        end.
        
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        view frame fLitigation.
        enable all with frame fLitigation.
        setPageState(activePageNumber, "Enabled").
        hide frame fLinked.
        hide frame fActivity.
        hide frame fTasks.
        apply 'entry' to brwLitigation.
    end.
  when 8
   then
    do: 
        if not tLinkedClaimsLoaded then
        do:
          run GetClaimLinks in hFileDataSrv (output table claimlink).
          openLinkedClaims().
          displayLinkedClaims().
          tLinkedClaimsLoaded = yes.
        end.
        
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        view frame fLinked.
        enable all with frame fLinked.
        setPageState(activePageNumber, "Enabled").
        hide frame fActivity.
        hide frame fTasks.
        apply 'entry' to brwLinkedClaims.
    end.
  when 9
   then
    do: 
        if not tActivityLoaded then
        do:
          run GetClaim in hFileDataSrv (output table claim).
          displayActivity().
          tActivityLoaded = yes.
        end.
        
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        view frame fActivity.
        enable all with frame fActivity.
        setPageState(activePageNumber, "Enabled").
        hide frame fTasks.
    end.
  when 10
   then
    do: 
        hide frame fOverview.
        hide frame fCoverage.
        hide frame fProperties.
        hide frame fContacts.
        hide frame fAccounting.
        hide frame fRecovery.
        hide frame fLitigation.
        hide frame fLinked.
        hide frame fActivity.
        view frame fTasks.
/*         enable all with frame fTasks.  */
        setPageState(activePageNumber, "Enabled").
    end.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableObject wWin 
PROCEDURE enableObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/


  RUN SUPER.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY hdClaimID hdSuppClaimID hdLitigation hdDateCreated hdLastActivity 
          hdStat hdAssignedTo hdSignificant hdDateClosed hdType 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE hdClaimID hdSuppClaimID hdLitigation bTransfer hdDateCreated 
         hdLastActivity hdStat hdAssignedTo hdSignificant hdDateClosed hdType 
         bAttr bClose bReopen bMarkClaim bMarkMatter bNewNote bAcct bDocs 
         bNotes 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  DISPLAY tLAECurrentBalance tLossCurrentBalance tTotalCurrentBalance 
          tLAEApprovedPayments tLAEApprovedPaymentsBalance tLossApprovedPayments 
          tLossApprovedPaymentsBalance tTotalApprovedPaymentsBalance 
          tLAEOpenAdjustments tLAEOpenAdjustmentsBalance tLossOpenAdjustments 
          tLossOpenAdjustmentsBalance tTotalOpenAdjustmentsBalance 
          tLAEOpenPayments tLAEOpenPaymentsBalance tLossOpenPayments 
          tLossOpenPaymentsBalance tTotalOpenPaymentsBalance 
          tLAECompletedPayments tLossCompletedPayments tLAEPotentialCost 
          tLossPotentialCost tTotalPotentialCost acRequestedRecoveries 
          acOpenRecoveries acCompleteRecoveries acWaivedRecoveries 
      WITH FRAME fAccounting IN WINDOW wWin.
  ENABLE RECT-43 RECT-44 RECT-45 RECT-48 tLAECurrentBalance tLossCurrentBalance 
         tTotalCurrentBalance tLAEApprovedPayments tLAEApprovedPaymentsBalance 
         tLossApprovedPayments tLossApprovedPaymentsBalance 
         tTotalApprovedPaymentsBalance tLAEOpenAdjustments 
         tLAEOpenAdjustmentsBalance tLossOpenAdjustments 
         tLossOpenAdjustmentsBalance tTotalOpenAdjustmentsBalance 
         tLAEOpenPayments tLAEOpenPaymentsBalance tLossOpenPayments 
         tLossOpenPaymentsBalance tTotalOpenPaymentsBalance 
         tLAECompletedPayments tLossCompletedPayments tLAEPotentialCost 
         tLossPotentialCost tTotalPotentialCost acRequestedRecoveries 
         acOpenRecoveries acCompleteRecoveries acWaivedRecoveries 
      WITH FRAME fAccounting IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fAccounting}
  FRAME fAccounting:SENSITIVE = NO.
  DISPLAY avDateClmReceived avDateCreated avUserCreated avinitDateResponded 
          avinitUserResponded avDateAssigned avUserAssigned avDateAcknowledged 
          avUserAcknowledged avDateTransferred avDateClosed avUserClosed 
          avDateRecReceived avDateReOpened avUserReOpened avDateResponded 
          avUserResponded avDateReClosed avUserReClosed 
      WITH FRAME fActivity IN WINDOW wWin.
  ENABLE RECT-38 RECT-39 avDateClmReceived avUserCreated avinitDateResponded 
         avinitUserResponded avDateAssigned avUserAssigned avDateAcknowledged 
         avUserAcknowledged avDateTransferred avDateClosed avUserClosed 
         avDateRecReceived avDateReOpened avUserReOpened avDateResponded 
         avUserResponded avDateReClosed avUserReClosed 
      WITH FRAME fActivity IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fActivity}
  FRAME fActivity:SENSITIVE = NO.
  DISPLAY coRole coFullName coJobTitle coEmail coPhone coFax coMobile coIsActive 
          coCompany coAddr1 coAddr2 coCity coStateID coCounty coZipCode coNotes 
      WITH FRAME fContacts IN WINDOW wWin.
  ENABLE coRole coFullName brwContacts coJobTitle coEmail coPhone coFax 
         coMobile coIsActive coCompany coAddr1 coAddr2 coCity coStateID 
         coCounty coZipCode coNotes 
      WITH FRAME fContacts IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fContacts}
  FRAME fContacts:SENSITIVE = NO.
  DISPLAY cvCoverageID cvIsPrimary cvCoverageType cvInsuredType cvEffDate 
          cvOrigLiabilityAmount cvCoverageYear cvCurrentLiabilityAmount 
          cvInsuredName 
      WITH FRAME fCoverage IN WINDOW wWin.
  ENABLE brwCoverage cvCoverageID cvIsPrimary cvCoverageType cvInsuredType 
         cvEffDate cvOrigLiabilityAmount cvCoverageYear 
         cvCurrentLiabilityAmount cvInsuredName 
      WITH FRAME fCoverage IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fCoverage}
  FRAME fCoverage:SENSITIVE = NO.
  ENABLE brwLinkedClaims 
      WITH FRAME fLinked IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fLinked}
  FRAME fLinked:SENSITIVE = NO.
  DISPLAY liCaseNumber liStat liDateFiled liDateServed liPlaintiff liDefendant 
          liIsParty liNotes 
      WITH FRAME fLitigation IN WINDOW wWin.
  ENABLE brwLitigation liCaseNumber liStat liDateFiled liDateServed liPlaintiff 
         liDefendant liIsParty liNotes 
      WITH FRAME fLitigation IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fLitigation}
  FRAME fLitigation:SENSITIVE = NO.
  DISPLAY ovStateID ovAgentStat ovDifficulty ovStage ovAgentID ovUrgency 
          ovAction ovFileNumber ovAgentError ovSearcherError ovInsuredName 
          ovCoverageID ovEffDate ovOrigLiabilityAmount ovCvgPrimary 
          ovBorrowerName ovRefYear ovYearFirstReport ovSummary ovPropAddr1 
          ovPropAddr2 ovPropCity ovPropStateID ovPropCounty ovPropZipCode 
          ovPropSubdivision ovPropPrimary ovPropResidential ovPropLegalDesc 
          ovALTARisk ovALTAResp ovAttorneyError ovSearcher 
      WITH FRAME fOverview IN WINDOW wWin.
  ENABLE ovStateID ovAgentStat ovDifficulty ovStage ovAgentID ovUrgency 
         ovAction ovFileNumber ovAgentError ovSearcherError ovInsuredName 
         ovCoverageID ovEffDate ovOrigLiabilityAmount ovBorrowerName ovRefYear 
         ovYearFirstReport ovSummary ovPropAddr1 ovPropAddr2 ovPropCity 
         ovPropStateID ovPropCounty ovPropZipCode ovPropSubdivision 
         ovPropPrimary ovPropResidential ovPropLegalDesc ovALTARisk ovALTAResp 
         brwDescCodes brwCauseCodes ovAttorneyError ovSearcher 
      WITH FRAME fOverview IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fOverview}
  FRAME fOverview:SENSITIVE = NO.
  DISPLAY prAddr1 prAddr2 prCity prStateID prCountyID prZipCode prSubdivision 
          prIsPrimary prResidential prLegalDesc 
      WITH FRAME fProperties IN WINDOW wWin.
  ENABLE brwProperties prAddr1 prAddr2 prCity prStateID prCountyID prZipCode 
         prSubdivision prIsPrimary prResidential prLegalDesc 
      WITH FRAME fProperties IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fProperties}
  FRAME fProperties:SENSITIVE = NO.
  DISPLAY reHasReinsurance reSeekingRecovery reRecoveryNotes reHasDeductible 
          reDeductibleAmount reAmountWaived reEOPolicy reEOEffDate reEORetroDate 
          reEOCarrier reEOCoverageAmount rePriorPolicyUsed rePriorPolicyID 
          reUnderwritingCompliance rePriorPolicyCarrier reSellerBreach 
          rePriorPolicyAmount rePriorPolicyEffDate 
      WITH FRAME fRecovery IN WINDOW wWin.
  ENABLE RECT-40 RECT-42 reHasReinsurance reSeekingRecovery reRecoveryNotes 
         reHasDeductible reDeductibleAmount reAmountWaived reEOPolicy 
         reEOEffDate reEORetroDate reEOCarrier reEOCoverageAmount 
         rePriorPolicyUsed rePriorPolicyID reUnderwritingCompliance 
         rePriorPolicyCarrier reSellerBreach rePriorPolicyAmount 
         rePriorPolicyEffDate brwBonds 
      WITH FRAME fRecovery IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fRecovery}
  FRAME fRecovery:SENSITIVE = NO.
  DISPLAY taStatus taPriority taDueDays 
      WITH FRAME fTasks IN WINDOW wWin.
  ENABLE RECT-3 taStatus taPriority taDueDays brwTasks 
      WITH FRAME fTasks IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fTasks}
  FRAME fTasks:SENSITIVE = NO.
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  if dataChanged then
  do:
    std-lo = no.
    message
      "Data has been modified." skip
      "Are you sure you want to exit without saving?"
      view-as alert-box warning buttons yes-no
      update std-lo.
    if std-lo = no 
     then return.
  end.

  /* close children windows */
  if valid-handle(hDocWindow) 
   then run CloseWindow in hDocWindow no-error.
  if valid-handle(hNoteWindow) 
   then run CloseWindow in hNoteWindow no-error.
  if valid-handle(hAcctWindow) 
   then run CloseWindow in hAcctWindow no-error.

  delete procedure hFileDataSrv.

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/

  {&window-name}:window-state = 2. /* Minimized */
  {&window-name}:title = "File " + string(pBaseClaimID).

  RUN SUPER.
  {&window-name}:max-width-pixels = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.
  
  do with frame fMain:
    bMarkMatter:load-image("images/flag_black.bmp").
    bMarkMatter:load-image-insensitive("images/flag-i.bmp").
    bMarkClaim:load-image("images/flag_green.bmp").
    bMarkClaim:load-image-insensitive("images/flag-i.bmp").
/*     bLock:load-image("images/lock.bmp").               */
/*     bLock:load-image-insensitive("images/lock-i.bmp"). */
    bClose:load-image("images/blank-close.bmp").
    bClose:load-image-insensitive("images/blank-close-i.bmp").
    bReopen:load-image("images/process.bmp").
    bReopen:load-image-insensitive("images/process-i.bmp").
    bTransfer:load-image("images/usergo.bmp").
    bTransfer:load-image-insensitive("images/usergo-i.bmp").

/*     bSuppClaim:load-image("images/copy.bmp").               */
/*     bSuppClaim:load-image-insensitive("images/copy-i.bmp"). */
/*     bReports:load-image("images/report.bmp").               */
/*     bReports:load-image-insensitive("images/report-i.bmp"). */
    bAcct:load-image("images/dollar.bmp").
    bAcct:load-image-insensitive("images/dollar-i.bmp").
    bDocs:load-image("images/docs.bmp").
    bDocs:load-image-insensitive("images/docs-i.bmp").
    bNotes:load-image("images/note.bmp").
    bNotes:load-image-insensitive("images/note-i.bmp").
    bNewNote:load-image("images/blank-add.bmp").
    bNewNote:load-image-insensitive("images/blank-i.bmp").
    bAttr:load-image("images/book.bmp").
    bAttr:load-image-insensitive("images/book-i.bmp").
    bOpenSuppClaim:load-image("images/s-magnifier.bmp").
    bOpenSuppClaim:load-image-insensitive("images/s-magnifier-i.bmp").
    
/*     bSetAssignedTo:load-image("images/s-update.bmp").               */
/*     bSetAssignedTo:load-image-insensitive("images/s-update-i.bmp"). */
  end.

  do with frame fOverview:
    bOverviewSave:load-image("images/s-save.bmp").
    bOverviewSave:load-image-insensitive("images/s-save-i.bmp").
    bOverviewCancel:load-image("images/s-cancel.bmp").
    bOverviewCancel:load-image-insensitive("images/s-cancel-i.bmp").
    bCauseCodeAdd:load-image("images/s-add.bmp").
    bCauseCodeAdd:load-image-insensitive("images/s-add-i.bmp").
    bCauseCodeDelete:load-image("images/s-delete.bmp").
    bCauseCodeDelete:load-image-insensitive("images/s-delete-i.bmp").
    bCauseCodeUp:load-image("images/s-up.bmp").
    bCauseCodeUp:load-image-insensitive("images/s-up-i.bmp").
    bCauseCodeDown:load-image("images/s-down.bmp").
    bCauseCodeDown:load-image-insensitive("images/s-down-i.bmp").
    bDescCodeAdd:load-image("images/s-add.bmp").
    bDescCodeAdd:load-image-insensitive("images/s-add-i.bmp").
    bDescCodeDelete:load-image("images/s-delete.bmp").
    bDescCodeDelete:load-image-insensitive("images/s-delete-i.bmp").
    bDescCodeUp:load-image("images/s-up.bmp").
    bDescCodeUp:load-image-insensitive("images/s-up-i.bmp").
    bDescCodeDown:load-image("images/s-down.bmp").
    bDescCodeDown:load-image-insensitive("images/s-down-i.bmp").
  end.

  do with frame fCoverage:
    bCoverageSave:load-image("images/s-save.bmp").
    bCoverageSave:load-image-insensitive("images/s-save-i.bmp").
    bCoverageAdd:load-image("images/s-add.bmp").
    bCoverageAdd:load-image-insensitive("images/s-add-i.bmp").
    bCoverageDelete:load-image("images/s-delete.bmp").
    bCoverageDelete:load-image-insensitive("images/s-delete-i.bmp").
    bCoverageCancel:load-image("images/s-cancel.bmp").
    bCoverageCancel:load-image-insensitive("images/s-cancel-i.bmp").
  end.
  
  do with frame fProperties:
    bPropertySave:load-image("images/s-save.bmp").
    bPropertySave:load-image-insensitive("images/s-save-i.bmp").
    bPropertyAdd:load-image("images/s-add.bmp").
    bPropertyAdd:load-image-insensitive("images/s-add-i.bmp").
    bPropertyDelete:load-image("images/s-delete.bmp").
    bPropertyDelete:load-image-insensitive("images/s-delete-i.bmp").
    bPropertyCancel:load-image("images/s-cancel.bmp").
    bPropertyCancel:load-image-insensitive("images/s-cancel-i.bmp").
  end.
  
  do with frame fContacts:
    bContactSave:load-image("images/s-save.bmp").
    bContactSave:load-image-insensitive("images/s-save-i.bmp").
    bContactAdd:load-image("images/s-add.bmp").
    bContactAdd:load-image-insensitive("images/s-add-i.bmp").
    bContactDelete:load-image("images/s-delete.bmp").
    bContactDelete:load-image-insensitive("images/s-delete-i.bmp").
    bContactCancel:load-image("images/s-cancel.bmp").
    bContactCancel:load-image-insensitive("images/s-cancel-i.bmp").
    bContactEmail:load-image("images/s-email.bmp").
    bContactEmail:load-image-insensitive("images/s-email-i.bmp").
  end.

  do with frame fAccounting:
    bAddLossAdjustment:load-image("images/s-add.bmp").
    bAddLossAdjustment:load-image-insensitive("images/s-add-i.bmp").
    bAddLAEAdjustment:load-image("images/s-add.bmp").
    bAddLAEAdjustment:load-image-insensitive("images/s-add-i.bmp").
    bAddLossPayment:load-image("images/s-add.bmp").
    bAddLossPayment:load-image-insensitive("images/s-add-i.bmp").
    bAddLAEPayment:load-image("images/s-add.bmp").
    bAddLAEPayment:load-image-insensitive("images/s-add-i.bmp").
  end.
  
  do with frame fRecovery:
    bRecoverySave:load-image("images/s-save.bmp").
    bRecoverySave:load-image-insensitive("images/s-save-i.bmp").
    bRecoveryCancel:load-image("images/s-cancel.bmp").
    bRecoveryCancel:load-image-insensitive("images/s-cancel-i.bmp").
    bBondAdd:load-image("images/s-add.bmp").
    bBondAdd:load-image-insensitive("images/s-add-i.bmp").
    bBondDelete:load-image("images/s-delete.bmp").
    bBondDelete:load-image-insensitive("images/s-delete-i.bmp").
    bBondModify:load-image("images/s-update.bmp").
    bBondModify:load-image-insensitive("images/s-update-i.bmp").
  end.
  
  do with frame fLitigation:
    bLitigationSave:load-image("images/s-save.bmp").
    bLitigationSave:load-image-insensitive("images/s-save-i.bmp").
    bLitigationAdd:load-image("images/s-add.bmp").
    bLitigationAdd:load-image-insensitive("images/s-add-i.bmp").
    bLitigationDelete:load-image("images/s-delete.bmp").
    bLitigationDelete:load-image-insensitive("images/s-delete-i.bmp").
    bLitigationCancel:load-image("images/s-cancel.bmp").
    bLitigationCancel:load-image-insensitive("images/s-cancel-i.bmp").
  end.
  
  do with frame fLinked:
    bClaimLinkAdd:load-image("images/s-add.bmp").
    bClaimLinkAdd:load-image-insensitive("images/s-add-i.bmp").
    bClaimLinkDelete:load-image("images/s-delete.bmp").
    bClaimLinkDelete:load-image-insensitive("images/s-delete-i.bmp").
  end.
    
  do with frame fActivity:
    bActivitySave:load-image("images/s-save.bmp").
    bActivitySave:load-image-insensitive("images/s-save-i.bmp").
    bActivityCancel:load-image("images/s-cancel.bmp").
    bActivityCancel:load-image-insensitive("images/s-cancel-i.bmp").
  end.
  
  do with frame fTasks:
    bTaskAdd:load-image("images/s-add.bmp").
    bTaskAdd:load-image-insensitive("images/s-add-i.bmp").
    bTaskDelete:load-image("images/s-delete.bmp").
    bTaskDelete:load-image-insensitive("images/s-delete-i.bmp").
    bTaskEdit:load-image("images/s-update.bmp").
    bTaskEdit:load-image-insensitive("images/s-update-i.bmp").
    bTaskComplete:load-image("images/s-check.bmp").  
    bTaskComplete:load-image-insensitive("images/s-check-i.bmp").  
    bTaskRefresh:load-image("images/s-refresh.bmp").
    bTaskRefresh:load-image-insensitive("images/s-refresh-i.bmp").
  end.

  {lib/pbupdate.i "'Getting users...'" 86}

  publish "GetClmUsers" (output table sysuser). /* Subset of just claims users */
  std-ch = ",".
/*   hdAssignedTo:list-item-pairs = std-ch. */
  for each sysuser no-lock by sysuser.name:
    std-ch = std-ch + "," + trim(sysuser.name) + "," + trim(sysuser.uid).
  end.
/*   hdAssignedTo:list-item-pairs = std-ch.  */
  avUserAcknowledged:list-item-pairs in frame fActivity = std-ch.
  avUserResponded:list-item-pairs in frame fActivity = std-ch.
  avinitUserResponded:list-item-pairs in frame fActivity = std-ch.
  avUserAssigned:list-item-pairs in frame fActivity = std-ch.

  std-ch = "".
  publish "GetDifficultyList" (output std-ch).
  if std-ch <> "" then
  ovDifficulty:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetUrgencyList" (output std-ch).
  if std-ch <> "" then
  ovUrgency:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetStageList" (output std-ch).
  if std-ch <> "" then
  ovStage:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetActionList" (output std-ch).
  if std-ch <> "" then
  ovAction:list-item-pairs = std-ch.

  std-ch = "".
  publish "GetAgentErrorList" (output std-ch).
  if std-ch <> "" then
  ovAgentError:list-item-pairs = std-ch.

  std-ch = "".
  publish "GetSearcherList" (output std-ch).
  if std-ch <> "" then
  ovSearcher:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetAttorneyErrorList" (output std-ch).
  if std-ch <> "" then
  ovAttorneyError:list-item-pairs = std-ch.

  std-ch = "".
  publish "GetSearcherErrorList" (output std-ch).
  if std-ch <> "" then
  ovSearcherError:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetPriorPolicyList" (output std-ch).
  if std-ch <> "" then
  rePriorPolicyUsed:list-item-pairs = std-ch.

  std-ch = "".
  publish "GetSellerBreachList" (output std-ch).
  if std-ch <> "" then
  reSellerBreach:list-item-pairs = std-ch.

  std-ch = "".
  publish "GetUnderwritingList" (output std-ch).
  if std-ch <> "" then
  reUnderwritingCompliance:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetCoverageTypeList" (output std-ch).
  if std-ch <> "" then
  cvCoverageType:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetInsuredTypeList" (output std-ch).
  if std-ch <> "" then
  cvInsuredType:list-item-pairs = std-ch.

  {lib/pbupdate.i "'Getting codes...'" 90}
  
  empty temp-table syscode no-error.
  publish "GetCodes" (input "ClaimAltaRisk", output table syscode append).
  publish "GetCodes" (input "ClaimAltaResp", output table syscode append).
  publish "GetCodes" (input "ClaimDescription", output table syscode append).
  publish "GetCodes" (input "ClaimCause", output table syscode append).
  
  std-ch = "|".
  ovALTARisk:delimiter = "|".
  ovALTARisk:list-item-pairs = std-ch.
  for each syscode where syscode.codeType = "ClaimAltaRisk" no-lock by syscode.code:
    std-ch = std-ch + "|" + trim(syscode.code) + ". " + trim(syscode.description) + "|" + trim(syscode.code).
  end.
  ovALTARisk:list-item-pairs = std-ch.

  std-ch = "|".
  ovALTAResp:delimiter = "|".
  ovALTAResp:list-item-pairs = std-ch.
  for each syscode where syscode.codeType = "ClaimAltaResp" no-lock by syscode.code:
    std-ch = std-ch + "|" + trim(syscode.code) + ". " + trim(syscode.description) + "|" + trim(syscode.code).
  end.
  ovALTAResp:list-item-pairs = std-ch.
  
  std-ch = "".
  publish "GetContactRoleList" (output std-ch).
  if std-ch <> "" then
  coRole:list-item-pairs = std-ch.

  {lib/pbupdate.i "'Getting agents...'" 92}

  publish "GetAgents" (output table agent).

  {lib/pbupdate.i "'Getting file data...'" 94}

  run GetClaim in hFileDataSrv (output table claim).
  run GetClaimCodes in hFileDataSrv (output table claimcode).
  /*run GetClaimContacts in hFileDataSrv (output table claimcontact).*/
  run GetClaimCoverages in hFileDataSrv (output table claimcoverage).
  /*run GetClaimNotes in hFileDataSrv (output table claimnote).*/
  run GetClaimProperties in hFileDataSrv (output table claimproperty).
  /*
  run GetClaimCarriers in hFileDataSrv (output table claimcarrier).
  run GetClaimBonds in hFileDataSrv (output table claimbond).
  run GetClaimLitigations in hFileDataSrv (output table claimlitigation).
  */
  run GetClaimLinks in hFileDataSrv (output table claimlink).
  
  std-lo = getBaseClaim().

  {lib/pbupdate.i "'Displaying file data...'" 96}

  if std-lo = true then
  do:
    getSupplementalClaim().
    getPrimaryCoverage().  
    getPrimaryProperty().
    /*getPrimaryLitigation().*/
    
    displayHeader().
    openDescCodes().
    openCauseCodes().
    openCoverage().
    displayCoverage().
    openProperties().
    displayProperties().
    openContacts().
    displayContacts().
    displayAccounting().
    openBonds().
    displayRecovery().
    openLitigation().
    displayLitigation().
    openLinkedClaims().
    displayLinkedClaims().
    displayActivity().
    displayTasks().
    
    tOverviewLoaded = yes.
    tCoverageLoaded = yes.
    tPropertiesLoaded = yes.
    tLinkedClaimsLoaded = yes.
    tActivityLoaded = yes.
  
    setHeaderState().
    run selectPage(1).
    displayOverview().
  end.

  {&window-name}:window-state = 3. /* Restored */

  run windowResized in this-procedure.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPayable wWin 
PROCEDURE NewPayable :
/*------------------------------------------------------------------------------
@description Create a claim invoice
------------------------------------------------------------------------------*/
  define input parameter pRefCategory as character no-undo.
  
  define buffer apinv for apinv.

  if not tAccountingLoaded
   then run LoadClaimAccounting in hFileDataSrv.
  tAccountingLoaded = true.
   
  empty temp-table apinv.
  create apinv.
  assign
    apinv.apinvID = 0
    apinv.refCategory = pRefCategory
    apinv.refID = string(pBaseClaimID)
    apinv.refType = "C"
    apinv.dateReceived = today
    .
  empty temp-table apinva.
  empty temp-table apinvd.
  run dialogclaimpayable.w (input pBaseClaimID,
                            input table apinv,
                            input table apinva,
                            input table apinvd,
                            input hFileDataSrv,
                            input "Create Claim Payable Invoice",
                            output std-lo
                            ).
                             
  if std-lo
   then run RefreshAccounting in this-procedure.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewReserve wWin 
PROCEDURE NewReserve :
/*------------------------------------------------------------------------------
@description Creates a new reserve adjustment request to the system  
------------------------------------------------------------------------------*/
  define input parameter cRefCategory as character no-undo.
  
  define buffer claimadjreq for claimadjreq.
  
  if not tAccountingLoaded
   then run LoadClaimAccounting in hFileDataSrv.
  tAccountingLoaded = true.
  
  empty temp-table claimadjreq.
  create claimadjreq.
  assign
    claimadjreq.claimID = pBaseClaimID
    claimadjreq.refCategory = cRefCategory
    claimadjreq.isNew = true
    claimadjreq.isView = false
    .
  run dialogreserve.w (input table claimadjreq,
                       input hFileDataSrv,
                       input "Create Claim Adjustment",
                       output std-lo
                       ).
                             
  if std-lo
   then run RefreshAccounting in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshAccounting wWin 
PROCEDURE RefreshAccounting :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run LoadClaimAccountingTab in hFileDataSrv.
  run GetClaimAccountingTab in hFileDataSrv (output table claimacct).
  displayAccounting().
  tAccountingLoaded = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshCoverages wWin 
PROCEDURE RefreshCoverages :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run GetClaimCoverages in hFileDataSrv (output table claimcoverage).
  openCoverage().
  displayCoverage().
  tCoverageLoaded = yes.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectPage wWin 
PROCEDURE selectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/

  DEFINE INPUT PARAMETER piPageNum AS INTEGER NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* 
  Prevent user from changing pages if something has been modified on the
  current page.  Prompt if they want to save or discard, then handle the
  choice accordingly.
  
  Saving changes needs to be page specific. Saving all the data on all
  pages will take too much time.
  */
  std-lo = checkDataChanged().
  if not std-lo then
  return no-apply.
  
  RUN SUPER( INPUT piPageNum).

  /* Code placed here will execute AFTER standard behavior.    */
  
  activePageNumber = piPageNum.
  
  saveStateDisable(activePageNumber).
  
  /* Disable editing if claim is closed */
  if not avail claim or (avail claim and claim.stat = "C") then
  setPageState(activePageNumber, "ReadOnly").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setContainerState wWin 
PROCEDURE setContainerState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/*DEFINE INPUT PARAMETER phProcedure AS HANDLE NO-UNDO.*/
DEFINE INPUT PARAMETER pContainer AS HANDLE NO-UNDO.
DEFINE INPUT PARAMETER pState AS CHAR NO-UNDO.
  
DEFINE VARIABLE hObject AS HANDLE NO-UNDO.
DEFINE VARIABLE hChild AS HANDLE NO-UNDO.

  /* Start with current window handle */
  /*hObject = phProcedure:CURRENT-WINDOW NO-ERROR.*/
  
  /* Start walking the widget tree only if a valid handle was obtained.
  Walk down child branch first, then across to siblings at each level.
  */
  /* IF VALID-HANDLE (hObject)         */
  /* THEN RUN WalkChildTree (hObject). */

  IF VALID-HANDLE(pContainer)
  THEN RUN setWidgetState (pContainer, pState).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState wWin 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE INPUT PARAMETER pObject AS HANDLE NO-UNDO.
DEFINE INPUT PARAMETER pState AS CHAR NO-UNDO.
  
DEFINE VARIABLE hNextObject AS HANDLE NO-UNDO.

  /* Drill down child object chain first */
  hNextObject = pObject:FIRST-CHILD NO-ERROR.
  
  if valid-handle(hNextObject)
  then run setWidgetState (hNextObject, pState).
  /* Check for siblings and their children.
  */
  hNextObject = pObject:NEXT-SIBLING NO-ERROR.
  if valid-handle(hNextObject) then
  do:
    /* Do not process any sibling frames or their children */
    if hNextObject:type <> "FRAME" then
    run setWidgetState (hNextObject, pState).
  end.
  
  /* This object does not have any children or siblings, or the children or
  siblings have already been processed in the recursive call. Process
  object information now.
  */
  /*RUN WidgetAction (pObject, pState).*/
  
  if pObject:type <> ? then /* Should never be the case, but just to be safe */
  case pState:
    when "Enabled" then
    do:
      if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
      do:
        if pObject:private-data = "READ-ONLY" then
        assign
          pObject:sensitive = yes
          pObject:read-only = yes.
        else
        assign
          pObject:sensitive = yes
          pObject:read-only = no.
      end.
      
      if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
      do:
        if pObject:private-data = "READ-ONLY" then
        assign
          pObject:sensitive = no.
        else
        assign
          pObject:sensitive = yes.
      end.
    end.
    when "Disabled" then
    do:
      if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
      do:
        assign
          pObject:sensitive = yes
          pObject:read-only = yes.
      end.
      
      if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
      do:
        assign
          pObject:sensitive = no.
      end.
    end.
    when "ReadOnly" then
    do:
      if pObject:type = "BUTTON" then
      do:
        if pObject:private-data <> "STATIC" then
        pObject:sensitive = no.
      end.
      
      if lookup(pObject:type,"EDITOR,FILL-IN") <> 0 then
      do:
        assign
          pObject:sensitive = yes
          pObject:read-only = yes.
      end.
      
      if lookup(pObject:type,"COMBO-BOX,RADIO-SET,SELECTION-LIST,TOGGLE-BOX") <> 0 then
      do:
        assign
          pObject:sensitive = no.
      end.
    end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow wWin 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  frame fMain:width-pixels = {&window-name}:width-pixels.
  frame fMain:virtual-width-pixels = frame fMain:width-pixels.

  frame fMain:height-pixels = {&window-name}:height-pixels.
  frame fMain:virtual-height-pixels = frame fMain:height-pixels.

/*   run resizeObject in h_folder (frame fMain:height - 2,  */
/*                                 frame fMain:width - 2).  */
  /* The resize causes the tab label to be off-set, so... */
  run showCurrentPage in h_folder(activePageNumber).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkAgentValidation wWin 
FUNCTION checkAgentValidation RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if ovAgentID:screen-value in frame fOverview eq "Unknown" 
   then
    do:
     message "Agent ID cannot be Unknown."
         view-as alert-box error buttons ok.
     return false.  
    end.    
   else if ovStateID:screen-value in frame fOverview eq "Unknown"  then
    do:
     message "State ID cannot be Unknown."
         view-as alert-box error buttons ok.
     return false.  
    end.    
   else
   return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION checkDataChanged wWin 
FUNCTION checkDataChanged RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  if dataChanged then
  do:
    std-lo = ?.
    message
      "Data has been modified on the current tab." skip
      "Do you want to save the changes before continuing?"
      skip(1)
      "Choices:" skip
      "'Yes' will save the changes" skip 
      "'No' will discard the changes" skip
      "'Cancel' will take no action and remain on the current tab"
      view-as alert-box warning buttons yes-no-cancel
      update std-lo.
      
    if std-lo = ? then
    return false.
    else
    if std-lo = yes then
    savePage(activePageNumber).
    else
    if std-lo = no then
    discardPage(activePageNumber).
    else
    return false.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearContact wWin 
FUNCTION clearContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame fContacts:
    clear frame fContacts no-pause.
    coRole:screen-value = entry(2,coRole:list-item-pairs).
    coStateID:screen-value = entry(1,coStateID:list-items).
    coNotes:screen-value = "".
    browse brwContacts:sensitive = no.
    apply 'entry' to coRole.
  end.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearCoverage wWin 
FUNCTION clearCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame fCoverage:
    clear frame fCoverage no-pause.
    cvCoverageType:screen-value = entry(2,cvCoverageType:list-item-pairs).
    cvInsuredType:screen-value = entry(2,cvInsuredType:list-item-pairs).
    cvIsPrimary:screen-value = string(no).
    browse brwCoverage:sensitive = no.
    apply 'entry' to cvCoverageID.
  end.
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearLitigation wWin 
FUNCTION clearLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  do with frame fLitigation:
    clear frame fLitigation no-pause.
    liStat:screen-value = entry(2,liStat:list-item-pairs).
    liNotes:screen-value = "".
    browse brwLitigation:sensitive = no.
    apply 'entry' to liCaseNumber.
  end.

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearProperty wWin 
FUNCTION clearProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def var coverageid as char no-undo.
def var agentid as char no-undo.
def var xEffDate as datetime.
def var xLiabilityAmount as decimal.
def var xGrossPremium as decimal.
def var xFormID as char.
def var xStateID as char.
def var xFileNumber as char.
def var xStatCode as char.
def var xNetPremium as dec.
def var xRetention as dec.
def var xCountyID as char.
def var xResidential as log.
def var xInvoiceDate as date. 

coverageid = ?.
for first claimcoverage no-lock:
   coverageid = claimcoverage.coverageID.
end.

agentid = ?.
for first claim no-lock:
    agentid = claim.agentID.
end.

do with frame fProperties:
    clear frame fProperties no-pause.
    prStateID:screen-value = entry(1,prStateID:list-items).
    prIsPrimary:screen-value = string(no).
    prResidential:screen-value = string(no).
    prLegalDesc:screen-value = "".
    browse brwProperties:sensitive = no.

    /* Default the residential flag to policy residential for property*/
    publish "IsPolicyValid" (input pBaseClaimID,
                             input coverageid,
                             input agentid,
                             output xEffDate,
                             output xLiabilityAmount,
                             output xGrossPremium,
                             output xFormID,
                             output xStateID,
                             output xFileNumber,
                             output xStatCode,
                             output xNetPremium,
                             output xRetention,
                             output xCountyID,
                             output xResidential,
                             output xInvoiceDate,
                             output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                             output std-ch).
                            
    prResidential:screen-value = string(xResidential).

    apply 'entry' to prAddr1.
  end.
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteBond wWin 
FUNCTION deleteBond RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  get current brwBonds.

  if not avail claimbond then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  if num-results("brwBonds") > 0 then
  brwBonds:select-focused-row() in frame fRecovery.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.

  run DeleteClaimBond in hFileDataSrv (input claimbond.claimID,
                                               input claimbond.bondID,
                                               output std-lo,
                                               output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimBonds in hFileDataSrv (output table claimbond).

  openBonds().

  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteCode wWin 
FUNCTION deleteCode RETURNS LOGICAL PRIVATE
  ( input pCodeType as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

do with frame fOverview:

  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).

  case pCodeType:
    when "ClaimDescription" then
    do:
      if num-results("brwDescCodes") > 0 then
      brwDescCodes:select-focused-row().
      
      if not avail desccode then
      do:
        message
          "No record is selected for deletion."
          view-as alert-box.
        return false.
      end.
      
      find first claimcode where claimcode.claimID = desccode.claimID
                           and claimcode.codetype = desccode.codetype
                           and claimcode.code = desccode.code
                           and claimcode.seq = desccode.seq
                           no-error.
    end.
    when "ClaimCause" then
    do:
      if num-results("brwCauseCodes") > 0 then
      brwCauseCodes:select-focused-row().
      
      if not avail causecode then
      do:
        message
          "No record is selected for deletion."
          view-as alert-box.
        return false.
      end.
      
      find first claimcode where claimcode.claimID = causecode.claimID
                           and claimcode.codetype = causecode.codetype
                           and claimcode.code = causecode.code
                           and claimcode.seq = causecode.seq
                           no-error.
    end.
    otherwise.
  end case.

  if not avail claimcode then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.

  run DeleteClaimCode in hFileDataSrv (input claimcode.claimID,
                                               input claimcode.codeType,
                                               input claimcode.code,
                                               input claimcode.seq,
                                               output std-lo,
                                               output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimCodes in hFileDataSrv (output table claimcode).

  case pCodeType:
    when "ClaimDescription" then
    openDescCodes().
    when "ClaimCause" then
    openCauseCodes().
    otherwise.
  end case.
  
  /* refreshClaim(). */
  
  RETURN true.
end.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteContact wWin 
FUNCTION deleteContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  get current brwContacts.

  if not avail claimcontact then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.

  run DeleteClaimContact in hFileDataSrv (input claimcontact.claimID,
                                                  input claimcontact.contactID,
                                                  output std-lo,
                                                  output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimContacts in hFileDataSrv (output table claimcontact).

  tCurrentContactID = 0.
  openContacts().
  displayContacts().

  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteCoverage wWin 
FUNCTION deleteCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  get current brwCoverage.

  if not avail claimcoverage then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.


  run DeleteClaimCoverage in hFileDataSrv (input claimcoverage.claimID,
                                                   input claimcoverage.seq,
                                                   output std-lo,
                                                   output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimCoverages in hFileDataSrv (output table claimcoverage).

  tCurrentCvgSeq = 0.
  openCoverage().
  displayCoverage().
  
  /* Refresh overview page in the event that the primary coverage changed */
  tPrimaryCvgSeq = 0.
  getPrimaryCoverage().
  displayOverview().

  /* Refresh claim links in the event a supplemental link was 
     automatically deleted if the claim no longer references
     the same policy number as another claim
  */ 
  run LoadClaimLinks in hFileDataSrv.
  run GetClaimLinks in hFileDataSrv (output table claimLink).
  openLinkedClaims().
  getSupplementalClaim().
  displayHeader().

  /* refreshClaim(). */

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteLink wWin 
FUNCTION deleteLink RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  get current brwLinkedClaims.

  if not avail claimLink then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  if num-results("brwLinkedClaims") > 0 then
  brwLinkedClaims:select-focused-row() in frame fLinked.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.

  run DeleteClaimLink in hFileDataSrv (input claimlink.fromClaimID,
                                               input claimlink.toClaimID,
                                               output std-lo,
                                               output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimLinks in hFileDataSrv (output table claimlink).

  openLinkedClaims().

  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteLitigation wWin 
FUNCTION deleteLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  get current brwLitigation.
  
  if not avail claimlitigation then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.

  run DeleteClaimLitigation in hFileDataSrv (input claimlitigation.claimID,
                                                     input claimlitigation.litigationID,
                                                     output std-lo,
                                                     output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimLitigations in hFileDataSrv (output table claimlitigation).

  tCurrentLitigationID = 0.
  openLitigation().
  displayLitigation().

  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteProperty wWin 
FUNCTION deleteProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  get current brwProperties.
  
  if not avail claimproperty then
  do:
    message
      "No record is selected for deletion."
      view-as alert-box.
    return false.
  end.
  
  std-lo = no.
  message
    "Are you sure you want to delete the selected record?"
    view-as alert-box buttons yes-no update std-lo.
  if not (std-lo = yes) then
  return false.

  run DeleteClaimProperty in hFileDataSrv (input claimproperty.claimID,
                                                   input claimproperty.seq,
                                                   output std-lo,
                                                   output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimProperties in hFileDataSrv (output table claimproperty).

  tCurrentPropSeq = 0.
  openProperties().
  displayProperties().
  
  /* Refresh overview page in the event that the primary property changed */
  tPrimaryPropSeq = 0.
  getPrimaryProperty().
  displayOverview().
  
  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION discardPage wWin 
FUNCTION discardPage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  displayHeader().

  case pPageNumber:
    when 1 then
    do:
      displayOverview().
    end.
    when 2 then
    do:
      displayCoverage().
    end.
    when 3 then
    do:
      displayProperties().
    end.
    when 4 then
    do:
      displayContacts().
    end.
    when 5 then
    do:
      displayAccounting().
    end.
    when 6 then
    do:
      displayRecovery().
    end.
    when 7 then
    do:
      displayLitigation().
    end.
    when 8 then
    do:
      displayLinkedClaims().
    end.
    when 9 then
    do:
      displayActivity().
    end.
    when 10 then
    do:
      displayTasks().
    end.
  end case.
  
  tAgentErrorSubject = "".
  tAttorneyErrorSubject = "".
  tSeekingRecoverySubject = "".

  saveStateDisable(pPageNumber).
  
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayAccounting wWin 
FUNCTION displayAccounting RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame fAccounting:

    define variable cClaimType as character no-undo.
    define variable dOpenAdjustments as decimal no-undo.
    define variable dApprovedAdjustments as decimal no-undo.
    define variable dOpenPayments as decimal no-undo.
    define variable dApprovedPayments as decimal no-undo.
    define variable dCompletedPayments as decimal no-undo.
    define variable dWaivedPayments as decimal no-undo.
    define variable dRequestedPayments as decimal no-undo.

    /* LAE */
    assign
      dOpenAdjustments = 0
      dApprovedAdjustments = 0
      dOpenPayments = 0
      dApprovedPayments = 0
      dCompletedPayments = 0
      .
    for each claimacct no-lock
       where claimacct.acctType = "E":
      
      /* open payments */
      if claimacct.stat = "O" and claimacct.transType = "P"
       then dOpenPayments = dOpenPayments + claimacct.requestedAmount.
      /* approved payments */
      if claimacct.stat = "A" and claimacct.transType = "P"
       then dApprovedPayments = dApprovedPayments + claimacct.requestedAmount.
      /* completed payments */
      if claimacct.stat = "C" and claimacct.transType = "P"
       then dCompletedPayments = dCompletedPayments + claimacct.transAmount.
      /* open adjustments */
      if claimacct.stat = "O" and claimacct.transType = "A"
       then dOpenAdjustments = dOpenAdjustments + claimacct.requestedAmount.
      /* approved adjustments */
      if claimacct.stat = "A" and claimacct.transType = "A"
       then dApprovedAdjustments = dApprovedAdjustments + claimacct.transAmount.
    end.
    
    assign
      tLAECurrentBalance = dApprovedAdjustments - dCompletedPayments
      tLAEApprovedPayments = dApprovedPayments
      tLAEApprovedPaymentsBalance = tLAECurrentBalance - tLAEApprovedPayments
      tLAEOpenAdjustments = dOpenAdjustments
      tLAEOpenAdjustmentsBalance = tLAEApprovedPaymentsBalance + tLAEOpenAdjustments
      tLAEOpenPayments = dOpenPayments
      tLAEOpenPaymentsBalance = tLAEOpenAdjustmentsBalance - tLAEOpenPayments
      tLAECompletedPayments = dCompletedPayments
      tLAEPotentialCost = tLAEOpenPaymentsBalance + tLAECompletedPayments
      .
    /* Loss */
    assign
      dOpenAdjustments = 0
      dApprovedAdjustments = 0
      dOpenPayments = 0
      dApprovedPayments = 0
      dCompletedPayments = 0
      .
    for each claimacct no-lock
       where claimacct.acctType = "L":
      
      /* open payments */
      if claimacct.stat = "O" and claimacct.transType = "P"
       then dOpenPayments = dOpenPayments + claimacct.requestedAmount.
      /* approved payments */
      if claimacct.stat = "A" and claimacct.transType = "P"
       then dApprovedPayments = dApprovedPayments + claimacct.requestedAmount.
      /* completed payments */
      if claimacct.stat = "C" and claimacct.transType = "P"
       then dCompletedPayments = dCompletedPayments + claimacct.transAmount.
      /* open adjustments */
      if claimacct.stat = "O" and claimacct.transType = "A"
       then dOpenAdjustments = dOpenAdjustments + claimacct.requestedAmount.
      /* approved adjustments */
      if claimacct.stat = "A" and claimacct.transType = "A"
       then dApprovedAdjustments = dApprovedAdjustments + claimacct.transAmount.
    end.
    
    assign
      tLossCurrentBalance = dApprovedAdjustments - dCompletedPayments
      tLossApprovedPayments = dApprovedPayments
      tLossApprovedPaymentsBalance = tLossCurrentBalance - tLossApprovedPayments
      tLossOpenAdjustments = dOpenAdjustments
      tLossOpenAdjustmentsBalance = tLossApprovedPaymentsBalance + tLossOpenAdjustments
      tLossOpenPayments = dOpenPayments
      tLossOpenPaymentsBalance = tLossOpenAdjustmentsBalance - tLossOpenPayments
      tLossCompletedPayments = dCompletedPayments
      tLossPotentialCost = tLossOpenPaymentsBalance + tLossCompletedPayments
      .
        
    /* Recoveries */
    assign
      dOpenPayments = 0
      dCompletedPayments = 0
      dWaivedPayments = 0
      dRequestedPayments = 0
      .
    for each claimacct no-lock
       where claimacct.acctType = "R":
      
      assign
        dRequestedPayments = dRequestedPayments + claimacct.requestedAmount
        dOpenPayments = dOpenPayments + (claimacct.requestedAmount - claimacct.transAmount - claimacct.waivedAmount)
        dCompletedPayments = dCompletedPayments + claimacct.transAmount
        dWaivedPayments = dWaivedPayments + claimacct.waivedAmount
        .
    end.
    assign
      acRequestedRecoveries = dRequestedPayments
      acOpenRecoveries = dOpenPayments
      acCompleteRecoveries = dCompletedPayments
      acWaivedRecoveries = dWaivedPayments
      .
        
    /* Totals */
    assign
      tTotalCurrentBalance = tLAECurrentBalance + tLossCurrentBalance
      tTotalApprovedPaymentsBalance = tLAEApprovedPaymentsBalance + tLossApprovedPaymentsBalance
      tTotalOpenAdjustmentsBalance = tLAEOpenAdjustmentsBalance + tLossOpenAdjustmentsBalance
      tTotalOpenPaymentsBalance = tLAEOpenPaymentsBalance + tLossOpenPaymentsBalance
      tTotalPotentialCost = tLAEPotentialCost + tLossPotentialCost
      .
    
    display
      tLAECurrentBalance 
      tLAEApprovedPayments 
      tLAEApprovedPaymentsBalance 
      tLAEOpenAdjustments 
      tLAEOpenAdjustmentsBalance 
      tLAEOpenPayments 
      tLAEOpenPaymentsBalance 
      tLAECompletedPayments
      tLAEPotentialCost 
      
      tLossCurrentBalance 
      tLossApprovedPayments 
      tLossApprovedPaymentsBalance 
      tLossOpenAdjustments 
      tLossOpenAdjustmentsBalance 
      tLossOpenPayments 
      tLossOpenPaymentsBalance 
      tLossCompletedPayments
      tLossPotentialCost
      
      tTotalCurrentBalance
      tTotalApprovedPaymentsBalance
      tTotalOpenAdjustmentsBalance
      tTotalOpenPaymentsBalance
      
      tTotalPotentialCost
      
      acRequestedRecoveries
      acOpenRecoveries
      acCompleteRecoveries
      acWaivedRecoveries
      .
      
    assign
      bAddLAEAdjustment:sensitive  = avail claim and 
                                     claim.stat = "O" and 
                                     claim.type = "C" and 
                                     tLAEOpenAdjustments = 0
      bAddLAEPayment:sensitive     = avail claim and 
                                     claim.stat = "O" and 
                                     (claim.type = "M" or claim.type = "C")
      bAddLossAdjustment:sensitive = avail claim and 
                                     claim.stat = "O" and
                                     claim.type = "C" and
                                     tLossOpenAdjustments = 0
      bAddLossPayment:sensitive    = avail claim and 
                                     claim.stat = "O" and
                                     claim.type = "C"
      .
        
    
  end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayActivity wWin 
FUNCTION displayActivity RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fActivity:

  assign
    avDateClmReceived   = if avail claim then claim.dateClmReceived else ?
    avDateTransferred   = if avail claim then claim.dateTransferred else ?
    avDateCreated       = if avail claim then claim.dateCreated else ?  
    avUserCreated       = if avail claim then getUserName(claim.userCreated) else ""
    avinitDateResponded = if avail claim then claim.dateInitResponded else ?  
    avinitUserResponded = if avail claim then claim.userInitResponded else ""
    avDateAssigned      = if avail claim then claim.userDateAssigned else ?
    avUserAssigned      = if avail claim then claim.userUserAssigned else ""
    avDateAcknowledged  = if avail claim then claim.dateAcknowledged else ?
    avUserAcknowledged  = if avail claim then claim.userAcknowledged else ""
    avDateClosed        = if avail claim then claim.dateClosed else ?
    avUserClosed        = if avail claim then getUserName(claim.userClosed) else ""
    avDateRecReceived   = if avail claim then claim.dateRecReceived else ?
    avDateReOpened      = if avail claim then claim.dateReOpened else ?
    avUserReOpened      = if avail claim then getUserName(claim.userReOpened) else ""
    avDateResponded     = if avail claim then claim.dateResponded else ?
    avUserResponded     = if avail claim then claim.userResponded else ""
    avDateReClosed      = if avail claim then claim.dateReClosed else ?
    avUserReClosed      = if avail claim then getUserName(claim.userReClosed) else ""
    .
    
  display
    avDateClmReceived 
    avDateTransferred 
    avDateCreated     
    avUserCreated     
    avinitDateResponded     
    avinitUserResponded    
    avDateAssigned    
    avUserAssigned    
    avDateAcknowledged
    avUserAcknowledged
    avDateClosed      
    avUserClosed      
    avDateRecReceived 
    avDateReOpened    
    avUserReOpened    
    avDateResponded   
    avUserResponded   
    avDateReClosed    
    avUserReClosed    
    .

end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayContacts wWin 
FUNCTION displayContacts RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fContacts:
  
  assign
    coRole      = if avail claimcontact then claimcontact.role else ""
    coFullName  = if avail claimcontact then claimcontact.fullName else ""
    coJobTitle  = if avail claimcontact then claimcontact.jobTitle else ""
    coEmail     = if avail claimcontact then claimcontact.email else ""
    coPhone     = if avail claimcontact then claimcontact.phone else ""
    coFax       = if avail claimcontact then claimcontact.fax else ""
    coMobile    = if avail claimcontact then claimcontact.mobile else ""
    coIsActive  = if avail claimcontact then claimcontact.isActive else no
    coCompany   = if avail claimcontact then claimcontact.company else ""
    coAddr1     = if avail claimcontact then claimcontact.addr1 else ""
    coAddr2     = if avail claimcontact then claimcontact.addr2 else ""
    coCity      = if avail claimcontact then claimcontact.city else ""
    coStateID   = if avail claimcontact then claimcontact.state else ""
    coCounty    = if avail claimcontact then claimcontact.county else ""
    coZipCode   = if avail claimcontact then claimcontact.zipcode else ""
    coNotes     = if avail claimcontact then claimcontact.notes else ""
    .
    
  display
    coRole      
    coFullName     
    coJobTitle  
    coEmail     
    coPhone     
    coFax     
    coMobile     
    coIsActive  
    coCompany   
    coAddr1     
    coAddr2     
    coCity      
    coStateID     
    coCounty    
    coZipCode   
    coNotes   
    .
    
  tCurrentContactID = if avail claimcontact then claimcontact.contactID else 0.
      
  browse brwContacts:sensitive = yes.
  apply 'entry' to brwContacts.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayCoverage wWin 
FUNCTION displayCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fCoverage:

  assign
    cvCoverageID              = if avail claimcoverage then claimcoverage.coverageID else ""
    cvIsPrimary               = if avail claimcoverage then claimcoverage.isPrimary else no
    cvCoverageType            = if avail claimcoverage then claimcoverage.coverageType else "O"
    cvInsuredType             = if avail claimcoverage then claimcoverage.insuredType else "O"
    cvEffDate                 = if avail claimcoverage then claimcoverage.effDate else ?
    cvOrigLiabilityAmount     = if avail claimcoverage then claimcoverage.origLiabilityAmount else 0
    cvCoverageYear            = if avail claimcoverage then claimcoverage.coverageYear else 0
    cvCurrentLiabilityAmount  = if avail claimcoverage then claimcoverage.currentLiabilityAmount else 0
    cvInsuredName             = if avail claimcoverage then claimcoverage.insuredName else ""
    .
    
  display
    cvCoverageID            
    cvIsPrimary             
    cvCoverageType          
    cvInsuredType           
    cvEffDate               
    cvOrigLiabilityAmount   
    cvCoverageYear          
    cvCurrentLiabilityAmount
    cvInsuredName           
    .

  tCurrentCvgSeq = if avail claimcoverage then claimcoverage.seq else 0.
      
  browse brwCoverage:sensitive = yes.
  apply 'entry' to brwCoverage.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayHeader wWin 
FUNCTION displayHeader RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 do with frame fMain:

  publish "GetTypeDesc" (input claim.type, output hdType).
  publish "GetStatDesc" (input claim.stat, output hdStat).
  publish "GetSysUserName" (input claim.assignedTo, output hdAssignedTo).
  
  assign
    hdClaimID       = if avail claim then claim.claimID else 0
    hdSuppClaimID   = if avail claimlink then claimlink.toClaimID else 0
    hdLitigation    = if avail claim then claim.litigation else false
    hdSignificant   = if avail claim then claim.significant else false
    hdDateCreated   = if avail claim then claim.dateCreated else ?
    hdLastActivity  = if avail claim then claim.lastActivity else ?
    hdDateClosed    = if avail claim then claim.dateClosed else ?
    .

  display
    hdClaimID
    hdSuppClaimID
    hdLitigation
    hdSignificant
    hdDateCreated
    hdLastActivity
    hdDateClosed
    hdAssignedTo 
    hdType
    hdStat
    .

  /* das: 12.30.2016 */
  bOpenSuppClaim:sensitive = (hdSuppClaimID > 0).
 end.
  
 RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayLinkedClaims wWin 
FUNCTION displayLinkedClaims RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fLinkedClaims:



end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayLitigation wWin 
FUNCTION displayLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fLitigation:

  assign
    liStat        = if avail claimlitigation then claimlitigation.stat else "O"
    liCaseNumber  = if avail claimlitigation then claimlitigation.caseNumber else ""
    liDateFiled   = if avail claimlitigation then claimlitigation.dateFiled else ?
    liDateServed  = if avail claimlitigation then claimlitigation.dateServed else ?
    liPlaintiff   = if avail claimlitigation then claimlitigation.plaintiff else ""
    liDefendant   = if avail claimlitigation then claimlitigation.defendant else ""
    liIsParty     = if avail claimlitigation then claimlitigation.isParty else no
    liNotes       = if avail claimlitigation then claimlitigation.notes else ""
    .
    
  display
    liStat          
    liCaseNumber    
    liDateFiled     
    liDateServed    
    liPlaintiff      
    liDefendant     
    liIsParty       
    liNotes         
    .    
    
  tCurrentLitigationID = if avail claimlitigation then claimlitigation.litigationID else 0.
      
  browse brwLitigation:sensitive = yes.
  apply 'entry' to brwLitigation.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayOverview wWin 
FUNCTION displayOverview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fOverview:

  release agent.
  if avail claim then
  find first agent where agent.agentID = claim.agentID no-lock no-error.

  publish "GetAgentStatusDesc" (input if avail agent then agent.stat else "",
                                output ovAgentStat).

  ovStateID:screen-value = if avail claim then claim.stateID else "".
  
  run AgentComboSet (claim.agentID).
  assign
    ovDifficulty              = if avail claim then claim.difficulty else 0
    ovStage                   = if avail claim then claim.stage else ""                              
    ovUrgency                 = if avail claim then claim.urgency else 0                            
    ovAction                  = if avail claim then claim.action else ""
    ovFileNumber              = if avail claim then claim.fileNumber else ""                         
    ovInsuredName             = if avail claim then claim.insuredName else ""
    ovCoverageID              = if avail primclaimcoverage then primclaimcoverage.coverageID else "" 
    ovEffDate                 = if avail primclaimcoverage then primclaimcoverage.effDate else ?
    ovOrigLiabilityAmount     = if avail primclaimcoverage then primclaimcoverage.origLiabilityAmount else 0
    ovCvgPrimary              = if avail primclaimcoverage then primclaimcoverage.isPrimary else no
    ovBorrowerName            = if avail claim then claim.borrowerName else ""
    ovRefYear                 = if avail claim then claim.refYear else 0       
    ovYearFirstReport         = if avail claim then claim.yearFirstReport else 0
    ovSummary                 = if avail claim then claim.summary else ""
    ovAgentError              = if avail claim then claim.agentError else ""
    ovAttorneyError           = if avail claim then claim.attorneyError else ""
    ovSearcherError           = if avail claim then claim.searcherError else ""
    ovSearcher                = if avail claim then claim.searcher else ""    
    ovALTARisk                = if avail claim then claim.altaRisk else ""
    ovALTAResp                = if avail claim then claim.altaResp else ""
    ovPropAddr1               = if avail primclaimproperty then primclaimproperty.addr1 else ""
    ovPropAddr2               = if avail primclaimproperty then primclaimproperty.addr2 else ""
    ovPropCity                = if avail primclaimproperty then primclaimproperty.city else ""
    ovPropStateID             = if avail primclaimproperty then primclaimproperty.stateID else ""
    ovPropCounty              = if avail primclaimproperty then primclaimproperty.countyID else ""
    ovPropZipCode             = if avail primclaimproperty then primclaimproperty.zipcode else ""
    ovPropSubdivision         = if avail primclaimproperty then primclaimproperty.subdivision else ""
    ovPropPrimary             = if avail primclaimproperty then primclaimproperty.isPrimary else no
    ovPropResidential         = if avail primclaimproperty then primclaimproperty.residential else no
    ovPropLegalDesc           = if avail primclaimproperty then primclaimproperty.legalDescription else ""
    .

  display    
    ovStateID
    ovAgentStat
    ovDifficulty
    ovStage
    ovUrgency
    ovAction
    ovFileNumber 
    ovInsuredName
    ovCoverageID
    ovEffDate
    ovOrigLiabilityAmount
    ovCvgPrimary
    ovBorrowerName 
    ovRefYear
    ovYearFirstReport
    ovSummary
    ovAgentError
    ovAttorneyError
    ovSearcherError
    ovSearcher
    ovALTARisk 
    ovALTAResp
    ovPropAddr1
    ovPropAddr2
    ovPropCity
    ovPropStateID 
    ovPropCounty
    ovPropZipCode
    ovPropSubdivision
    ovPropPrimary
    ovPropResidential
    ovPropLegalDesc
    .

  /* In order to set a combo box to a blank screen value, it has to be set to chr(32).
   */
  if claim.stage = "" then ovStage:screen-value = chr(32).
  if claim.action = "" then ovAction:screen-value = chr(32).
  if claim.agentError = "" then ovAgentError:screen-value = chr(32).
  if claim.attorneyError = "" then ovAttorneyError:screen-value = chr(32).
  if claim.searcherError = "" then ovSearcherError:screen-value = chr(32).
  if claim.ALTARisk = "" then ovALTARisk:screen-value = chr(32).
  if claim.ALTAResp = "" then ovALTAResp:screen-value = chr(32).

end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayProperties wWin 
FUNCTION displayProperties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fProperties:

  assign
    prAddr1       = if avail claimproperty then claimproperty.addr1 else ""
    prAddr2       = if avail claimproperty then claimproperty.addr2 else ""
    prCity        = if avail claimproperty then claimproperty.city else ""
    prStateID     = if avail claimproperty then claimproperty.stateID else ""
    prCountyID    = if avail claimproperty then claimproperty.countyID else ""
    prZipCode     = if avail claimproperty then claimproperty.zipcode else ""
    prSubdivision = if avail claimproperty then claimproperty.subdivision else ""
    prIsPrimary   = if avail claimproperty then claimproperty.isPrimary else no
    prResidential = if avail claimproperty then claimproperty.residential else no
    prLegalDesc   = if avail claimproperty then claimproperty.legalDescription else ""
    .  

  display
    prAddr1       
    prAddr2       
    prCity        
    prStateID     
    prCountyID    
    prZipCode     
    prSubdivision 
    prIsPrimary   
    prResidential 
    prLegalDesc   
    .  

  tCurrentPropSeq = if avail claimproperty then claimproperty.seq else 0.
  
  browse brwProperties:sensitive = yes.
  apply 'entry' to brwProperties.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayRecovery wWin 
FUNCTION displayRecovery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fRecovery:

  assign
    reHasReinsurance          = if avail claim then claim.hasReinsurance else no
    reHasDeductible           = if avail claim then claim.hasDeductible else no
    reDeductibleAmount        = if avail claim then claim.deductibleAmount else 0
    reAmountWaived            = if avail claim then claim.amountWaived else 0
    reSeekingRecovery         = if avail claim then claim.seekingRecovery else ""
    reRecoveryNotes           = if avail claim then claim.recoveryNotes else ""
    reEOPolicy                = if avail claim then claim.eoPolicy else ""
    reEOEffDate               = if avail claim then claim.eoEffDate else ?
    reEORetroDate             = if avail claim then claim.eoRetroDate else ?
    reEOCarrier               = if avail claim then claim.eoCarrier else ""
    reEOCoverageAmount        = if avail claim then claim.eoCoverageAmount else 0
    rePriorPOlicyUsed         = if avail claim then claim.priorPolicyUsed else ""
    reUnderwritingCompliance  = if avail claim then claim.underwritingCompliance else ""
    reSellerBreach            = if avail claim then claim.sellerBreach else ""
    rePriorPolicyID           = if avail claim then claim.priorPolicyID else ""
    rePriorPOlicyCarrier      = if avail claim then claim.priorPolicyCarrier else ""
    rePriorPolicyAmount       = if avail claim then claim.priorPolicyAmount else 0
    rePriorPolicyEffDate      = if avail claim then claim.priorPolicyEffDate else ?
    .
    
  display                    
    reHasReinsurance        
    reHasDeductible         
    reDeductibleAmount      
    reAmountWaived          
    reSeekingRecovery       
    reRecoveryNotes         
    reEOPolicy              
    reEOEffDate             
    reEORetroDate           
    reEOCarrier             
    reEOCoverageAmount      
    rePriorPOlicyUsed       
    reUnderwritingCompliance
    reSellerBreach          
    rePriorPolicyID         
    rePriorPOlicyCarrier    
    rePriorPolicyAmount     
    rePriorPolicyEffDate    
    .

  /* In order to set a combo box to a blank screen value, it has to be set to chr(32),
   */
  if claim.seekingRecovery = "" then reSeekingRecovery:screen-value = chr(32).

end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTasks wWin 
FUNCTION displayTasks RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame fTasks:

  
  

end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getBaseClaim wWin 
FUNCTION getBaseClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
 if not avail claim 
  then find first claim no-error.

 if not avail claim 
  then
   do: message 
         'Claim ' + string(pBaseClaimID) + ' was not loaded.'
         view-as alert-box error.
       return false.
   end.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getCurrentContact wWin 
FUNCTION getCurrentContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if tCurrentContactID > 0 then
  do:
    find first claimcontact where claimcontact.contactID = tCurrentContactID no-error.
    if avail claimcontact then
    return true.
  end.
  
  find first claimcontact no-error.
  
  tCurrentContactID = if avail claimcontact then claimcontact.contactID else 0.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getCurrentCoverage wWin 
FUNCTION getCurrentCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if tCurrentCvgSeq > 0 then
  do:
    find first claimcoverage where claimcoverage.seq = tCurrentCvgSeq no-error.
    if avail claimcoverage then
    return true.
  end.
  
  getPrimaryCoverage().
  
  tCurrentCvgSeq = if avail claimcoverage then claimcoverage.seq else 0.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getCurrentLitigation wWin 
FUNCTION getCurrentLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if tCurrentLitigationID > 0 then
  do:
    find first claimLitigation where claimLitigation.LitigationID = tCurrentLitigationID no-error.
    if avail claimLitigation then
    return true.
  end.
  
  find first claimLitigation no-error.
  
  tCurrentLitigationID = if avail claimLitigation then claimLitigation.LitigationID else 0.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getCurrentProperty wWin 
FUNCTION getCurrentProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if tCurrentPropSeq > 0 then
  do:
    find first claimproperty where claimproperty.seq = tCurrentPropSeq no-error.
    if avail claimproperty then
    return true.
  end.
  
  getPrimaryProperty().
  
  tCurrentPropSeq = if avail claimproperty then claimproperty.seq else 0.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPrimaryCoverage wWin 
FUNCTION getPrimaryCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

release primclaimcoverage.

if tPrimaryCvgSeq > 0 then
find first primclaimcoverage where primclaimcoverage.seq = tPrimaryCvgSeq no-error.
else
do:
  tPrimaryCvgSeq = 0.
  for each primclaimcoverage where primclaimcoverage.isPrimary = true by primclaimcoverage.seq:
    tPrimaryCvgSeq = primclaimcoverage.seq.
    leave.
  end.
  
  if tPrimaryCvgSeq = 0 then
  for each primclaimcoverage by primclaimcoverage.seq:
    tPrimaryCvgSeq = primclaimcoverage.seq.
    leave.
  end.
  
  if tPrimaryCvgSeq > 0 then
  find first primclaimcoverage where primclaimcoverage.seq = tPrimaryCvgSeq no-error.
end.

tPrimaryCvgSeq = if avail primclaimcoverage then primclaimcoverage.seq else 0.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPrimaryLitigation wWin 
FUNCTION getPrimaryLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  find first claimlitigation where claimlitigation.stat = "O" no-error.
  if not avail claimlitigation then
  find first claimlitigation no-error.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPrimaryProperty wWin 
FUNCTION getPrimaryProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

release primclaimproperty.

if tPrimaryPropSeq > 0 then
find first primclaimproperty where primclaimproperty.seq = tPrimaryPropSeq no-error.
else
do:
  tPrimaryPropSeq = 0.
  for each primclaimproperty where primclaimproperty.isPrimary = true by primclaimproperty.seq:
    tPrimaryPropSeq = primclaimproperty.seq.
    leave.
  end.
  
  if tPrimaryPropSeq = 0 then
  for each primclaimproperty by primclaimproperty.seq:
    tPrimaryPropSeq = primclaimproperty.seq.
    leave.
  end.
  
  if tPrimaryPropSeq > 0 then
  find first primclaimproperty where primclaimproperty.seq = tPrimaryPropSeq no-error.
end.

tPrimaryPropSeq = if avail primclaimproperty then primclaimproperty.seq else 0.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getSupplementalClaim wWin 
FUNCTION getSupplementalClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  find first claimlink where claimlink.linkType = "S" no-error.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getUserName wWin 
FUNCTION getUserName RETURNS CHARACTER PRIVATE
  ( input pUID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer sysuser for sysuser.

 find first sysuser 
   where sysuser.uid = pUID no-error.

 if avail sysuser then 
  return sysuser.name.
 else return pUID.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION moveCode wWin 
FUNCTION moveCode RETURNS LOGICAL PRIVATE
  ( input pCodeType as char, input pDirection as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

do with frame fOverview:

  discardPage(activePageNumber).
  saveStateDisable(activePageNumber).

  case pCodeType:
    when "ClaimDescription" then
    do:
      if num-results("brwDescCodes") > 0 then
      brwDescCodes:select-focused-row().
      
      if not avail desccode then
      do:
        message
          "No record is selected to move."
          view-as alert-box.
        return false.
      end.
      
      find first claimcode where claimcode.claimID = desccode.claimID
                           and claimcode.codetype = desccode.codetype
                           and claimcode.code = desccode.code
                           and claimcode.seq = desccode.seq
                           no-error.
    end.
    when "ClaimCause" then
    do:
      if num-results("brwCauseCodes") > 0 then
      brwCauseCodes:select-focused-row().
      
      if not avail causecode then
      do:
        message
          "No record is selected to move."
          view-as alert-box.
        return false.
      end.
      
      find first claimcode where claimcode.claimID = causecode.claimID
                           and claimcode.codetype = causecode.codetype
                           and claimcode.code = causecode.code
                           and claimcode.seq = causecode.seq
                           no-error.
    end.
    otherwise.
  end case.

  if not avail claimcode then
  do:
    message
      "No record is selected to move."
      view-as alert-box.
    return false.
  end.
  
  empty temp-table tempclaimcode no-error.
  create tempclaimcode.
  buffer-copy claimcode to tempclaimcode.
  
  run ModifyClaimCode in hFileDataSrv (input table tempclaimcode,
                                               input pDirection,
                                               output std-lo,
                                               output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  run GetClaimCodes in hFileDataSrv (output table claimcode).

  case pCodeType:
    when "ClaimDescription" then
    openDescCodes().
    when "ClaimCause" then
    openCauseCodes().
    otherwise.
  end case.
  
  /* refreshClaim(). */
  
  RETURN true.
end.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openBonds wWin 
FUNCTION openBonds RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  open query brwBonds for each claimbond by claimbond.type.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openCauseCodes wWin 
FUNCTION openCauseCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  empty temp-table causecode no-error.
  
  for each claimcode where claimcode.codetype = "ClaimCause" no-lock:
    create causecode.
    buffer-copy claimcode to causecode.
  end.
  
  open query brwCauseCodes for each causecode by causecode.seq.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openContacts wWin 
FUNCTION openContacts RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  open query brwContacts for each claimcontact
                         by claimcontact.isActive desc
                         by claimcontact.contactID.
  
  if tCurrentContactID <> 0 then
  do:
    find first claimcontact where claimcontact.contactID = tCurrentContactID no-error.
    if avail claimcontact then
    reposition brwContacts to rowid rowid(claimcontact) no-error.
    if not error-status:error then
    get current brwContacts.
  end.
  else
  get first brwContacts.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openCoverage wWin 
FUNCTION openCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  open query brwCoverage for each claimcoverage by claimcoverage.seq.
  
  if tCurrentCvgSeq <> 0 then
  do:
    find first claimcoverage where claimcoverage.seq = tCurrentCvgSeq no-error.
    if avail claimcoverage then
    reposition brwCoverage to rowid rowid(claimcoverage) no-error.
    if not error-status:error then
    get current brwCoverage.
  end.
  else
  get first brwCoverage.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openDescCodes wWin 
FUNCTION openDescCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  empty temp-table desccode no-error.
  
  for each claimcode where claimcode.codetype = "ClaimDescription" no-lock:
    create desccode.
    buffer-copy claimcode to desccode.
  end.
  
  open query brwDescCodes for each desccode by desccode.seq.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openLinkedClaims wWin 
FUNCTION openLinkedClaims RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  open query brwLinkedClaims for each claimlink by claimlink.toClaimID.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openLitigation wWin 
FUNCTION openLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  open query brwLitigation for each claimLitigation by claimLitigation.litigationID.
  
  if tCurrentLitigationID <> 0 then
  do:
    find first claimlitigation where claimlitigation.litigationID = tCurrentLitigationID no-error.
    if avail claimlitigation then
    reposition brwlitigation to rowid rowid(claimlitigation) no-error.
    if not error-status:error then
    get current brwLitigation.
  end.
  else
  get first brwLitigation.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openProperties wWin 
FUNCTION openProperties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  open query brwProperties for each claimproperty by claimproperty.seq.
  
  if tCurrentPropSeq <> 0 then
  do:
    find first claimproperty where claimproperty.seq = tCurrentPropSeq no-error.
    if avail claimproperty then
    reposition brwProperties to rowid rowid(claimproperty) no-error.
    if not error-status:error then
    get current brwProperties.
  end.
  else
  get first brwProperties.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openURL wWin 
FUNCTION openURL RETURNS LOGICAL PRIVATE
  ( input pURL as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 RUN ShellExecuteA in this-procedure (0,
                             "open",
                             pURL,
                             "",
                             "",
                             1,
                             OUTPUT std-in).

 return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshClaim wWin 
FUNCTION refreshClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  /* Reload claim from the server */
  run LoadClaim in hFileDataSrv.    
  run GetClaim in hFileDataSrv (output table claim).
  getBaseClaim().
  getSupplementalClaim().

/*   if valid-handle(hNoteWindow) then  */
/*   run RefreshNotes in hNoteWindow.   */

  displayHeader().
  displayOverview().
  displayRecovery().
  displayActivity().

  RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveActivity wWin 
FUNCTION saveActivity RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  std-lo = getBaseClaim().
  if not std-lo then
  return false.

  empty temp-table tempclaim no-error.
  
  create tempclaim.
  buffer-copy claim to tempclaim.
  
  /* Refresh all screen-value buffers to their corresponding variables
  */
  do with frame fActivity:
    assign
      avDateClmReceived 
      /*avDateTransferred*/
      avDateAcknowledged 
      avUserAcknowledged 
      avDateRecReceived 
      avDateResponded 
      avUserResponded 
      avinitDateResponded 
      avinitUserResponded   
      avDateAssigned
      avUserAssigned
      .  
  end.
  
  if avDateClmReceived <> ? and avDateClmReceived > date(tempclaim.dateCreated) then
  do:
    message
      "Date received cannot be later than date opened."
      view-as alert-box.
    return false.
  end.

  if avDateAcknowledged <> ? and avDateAcknowledged < date(tempclaim.dateCreated) then
  do:
    message
      "Date acknowledged cannot be earlier than date opened."
      view-as alert-box.
    return false.
  end.

  assign
    tempclaim.dateClmReceived   = avDateClmReceived
    /*tempclaim.dateTransferred   = avDateTransferred*/
    tempclaim.dateAcknowledged  = avDateAcknowledged            
    tempclaim.userAcknowledged  = avUserAcknowledged
    tempclaim.dateRecReceived   = avDateRecReceived     
    tempclaim.dateResponded     = avDateResponded
    tempclaim.userResponded     = avUserResponded
    tempclaim.dateInitResponded = avinitDateResponded
    tempclaim.userInitResponded = avinitUserResponded    
    tempclaim.userDateAssigned  = avDateAssigned
    tempclaim.userUserAssigned  = avUserAssigned
    .

  run ModifyClaim in hFileDataSrv (input table tempclaim,
                                   output std-lo,
                                   output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return false.
  end.

  buffer-copy tempclaim to claim.

  /* refreshClaim(). */

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveContact wWin 
FUNCTION saveContact RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if newRecord = no then
  do:
    std-lo = getCurrentContact().
    if not std-lo then
    return false.
  end.

  empty temp-table tempclaimcontact no-error.
  
  create tempclaimcontact.
  
  if newRecord = no then
  buffer-copy claimcontact to tempclaimcontact.
  else
  assign
    tempclaimcontact.claimID   = pBaseClaimID
    tempclaimcontact.contactID = 0.
  
  do with frame fContacts:
    assign
      coRole 
      coIsActive 
      coFullName 
      coJobTitle 
      coEmail 
      coPhone 
      coFax 
      coMobile 
      coCompany 
      coAddr1 
      coAddr2 
      coCity 
      coStateID 
      coCounty 
      coZipCode 
      coNotes    
      .
  end.

  assign    
    tempclaimcontact.role     = coRole
    tempclaimcontact.fname    = ""
    tempclaimcontact.lname    = ""
    tempclaimcontact.fullName = coFullName
    tempclaimcontact.jobTitle = coJobTitle 
    tempclaimcontact.email    = coEmail
    tempclaimcontact.phone    = coPhone
    tempclaimcontact.fax      = coFax
    tempclaimcontact.mobile   = coMobile
    tempclaimcontact.isActive = coIsActive
    tempclaimcontact.company  = coCompany
    tempclaimcontact.addr1    = coAddr1
    tempclaimcontact.addr2    = coAddr2
    tempclaimcontact.city     = coCity
    tempclaimcontact.countyID = coCounty
    tempclaimcontact.stateID  = coStateID
    tempclaimcontact.zipcode  = coZipCode
    tempclaimcontact.notes    = coNotes
    .

  if newRecord = yes then
  do:
    run NewClaimContact in hFileDataSrv (input table tempclaimcontact,
                                                  output tContactID,
                                                  output std-lo,
                                                  output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  
    tempclaimcontact.contactID = tContactID.
  end.
  else 
  do:
    run ModifyClaimContact in hFileDataSrv (input table tempclaimcontact,
                                                     output std-lo,
                                                     output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  end.
  
  run GetClaimContacts in hFileDataSrv (output table claimcontact).

  tCurrentContactID = if avail tempclaimcontact then tempclaimcontact.contactID else 0.
  openContacts().
  displayContacts().

  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveCoverage wWin 
FUNCTION saveCoverage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if newRecord = no then
  do:
    std-lo = getCurrentCoverage().
    if not std-lo then
    return false.
  end.

  publish "IsPolicyValid" (input claim.claimID,
                           input cvCoverageID:input-value in frame fCoverage,
                           input claim.agentID,
                           output xEffDate,
                           output xLiabilityAmount,
                           output xGrossPremium,
                           output xFormID,
                           output xStateID,
                           output xFileNumber,
                           output xStatCode,
                           output xNetPremium,
                           output xRetention,
                           output xCountyID,
                           output xResidential,
                           output xInvoiceDate,
                           output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                           output std-ch).
                           
  if tMsgStat = "W" then  /* Warning */
  do:
    std-lo = no.
    message std-ch skip 
      "OK to continue?"
      view-as alert-box warning buttons yes-no
      update std-lo.
      
    if not(std-lo = yes) then
    return false.
  end.
  else   
  if tMsgStat = "E" then  /* Error */
  do:
    message std-ch view-as alert-box error.
    return false.
  end.

  empty temp-table tempclaimcoverage no-error.
  
  create tempclaimcoverage.
  
  if newRecord = no then
  buffer-copy claimcoverage to tempclaimcoverage.
  else
  assign
    tempclaimcoverage.claimID = pBaseClaimID
    tempclaimcoverage.seq     = 0.
  
  do with frame fCoverage:
    assign
      cvCoverageID            
      cvIsPrimary             
      cvCoverageType          
      cvInsuredType           
      cvEffDate               
      cvOrigLiabilityAmount   
      cvCoverageYear          
      cvCurrentLiabilityAmount
      cvInsuredName           
      .
  end.

  assign    
    tempclaimcoverage.coverageID              = cvCoverageID
    tempclaimcoverage.coverageType            = cvCoverageType
    tempclaimcoverage.insuredType             = cvInsuredType
    tempclaimcoverage.origLiabilityAmount     = cvOrigLiabilityAmount
    tempclaimcoverage.currentLiabilityAmount  = cvCurrentLiabilityAmount
    tempclaimcoverage.effDate                 = cvEffDate
    tempclaimcoverage.coverageYear            = if cvCoverageYear = 0 or cvCoverageYear = ?
                                                then year(cvEffDate)
                                                else cvCoverageYear
    tempclaimcoverage.insuredName             = cvInsuredName
    tempclaimcoverage.claimant                = tempclaimcoverage.claimant
    tempclaimcoverage.isPrimary               = cvIsPrimary
    .

  if newRecord = yes then
  do:
    run NewClaimCoverage in hFileDataSrv (input table tempclaimcoverage,
                                                  output tSeq,
                                                  output std-lo,
                                                  output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  
    tempclaimcoverage.seq = tSeq.
  end.
  else 
  do:
    run ModifyClaimCoverage in hFileDataSrv (input table tempclaimcoverage,
                                                     output std-lo,
                                                     output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  end.
  
  run GetClaimCoverages in hFileDataSrv (output table claimcoverage).

  tCurrentCvgSeq = if avail tempclaimcoverage then tempclaimcoverage.seq else 0.
  openCoverage().
  displayCoverage().
  
  /* Refresh overview page in the event that the primary coverage changed */
  tPrimaryCvgSeq = 0.
  getPrimaryCoverage().
  displayOverview().

  /* Refresh claim links in the event a supplemental link was 
     automatically created if the claim references the same policy
     number as another claim
  */
  run LoadClaimLinks in hFileDataSrv.
  run GetClaimLinks in hFileDataSrv (output table claimLink).
  openLinkedClaims().
  getSupplementalClaim().
  displayHeader().

  /* refreshClaim(). */

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveLitigation wWin 
FUNCTION saveLitigation RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if newRecord = no then
  do:
    std-lo = getCurrentlitigation().
    if not std-lo then
    return false.
  end.

  empty temp-table tempclaimlitigation no-error.
  
  create tempclaimlitigation.
  
  if newRecord = no then
  buffer-copy claimlitigation to tempclaimlitigation.
  else
  assign
    tempclaimlitigation.claimID      = pBaseClaimID
    tempclaimlitigation.litigationID = 0.
  
  do with frame flitigation:
    assign
      liCaseNumber 
      liStat 
      liDateFiled 
      liDateServed 
      liPlaintiff 
      liDefendant 
      liIsParty 
      liNotes     
      .
  end.

  assign    
    tempclaimlitigation.stat        = liStat
    tempclaimlitigation.caseNumber  = liCaseNumber
    tempclaimlitigation.dateFiled   = liDateFiled
    tempclaimlitigation.courtName   = ""
    tempclaimlitigation.addr1       = ""
    tempclaimlitigation.addr2       = ""
    tempclaimlitigation.city        = ""
    tempclaimlitigation.stateID     = ""
    tempclaimlitigation.zipcode     = ""
    tempclaimlitigation.judge       = ""
    tempclaimlitigation.dateServed  = liDateServed
    tempclaimlitigation.isParty     = liIsParty
    tempclaimlitigation.plaintiff   = liPlaintiff
    tempclaimlitigation.defendant   = liDefendant
    tempclaimlitigation.notes       = liNotes
    .

  if newRecord = yes then
  do:
    run NewClaimLitigation in hFileDataSrv (input table tempclaimlitigation,
                                                    output tLitigationID,
                                                    output std-lo,
                                                    output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  
    tempclaimlitigation.litigationID = tLitigationID.
  end.
  else 
  do:
    run ModifyClaimLitigation in hFileDataSrv (input table tempclaimlitigation,
                                                       output std-lo,
                                                       output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  end.
  
  run GetClaimLitigations in hFileDataSrv (output table claimlitigation).

  tCurrentLitigationID = if avail tempclaimlitigation then tempclaimlitigation.litigationID else 0.
  openLitigation().
  displayLitigation().
  
  /* refreshClaim(). */

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveOverview wWin 
FUNCTION saveOverview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  std-lo = getBaseClaim().
  if not std-lo then
  return false.

  /*

  std-lo = getPrimaryCoverage().
  if not std-lo then
  return false.
  */

  std-lo = getPrimaryProperty().
  if not std-lo then
  return false.
  
  if hdType:screen-value in frame fMain ne "Notice"
   then
    do:
     if ovStateID:screen-value in frame fOverview eq "Unknown" 
      then
       do:
        message "State ID connot be Unknown for " + hdType:screen-value
            view-as alert-box error buttons ok.
        return false.             
       end.

      else if ovAgentID:screen-value in frame fOverview eq "Unknown" 
       then
        do:
         message "Agent ID connot be Unknown for " + hdType:screen-value
            view-as alert-box error buttons ok.
         return false.
        end.
    end.

  empty temp-table tempclaim no-error.
  empty temp-table tempclaimcoverage no-error.
  empty temp-table tempclaimproperty no-error.
  
  create tempclaim.
  buffer-copy claim to tempclaim.
  
  /*
  
  if avail primclaimcoverage then
  do:
    create tempclaimcoverage.
    buffer-copy primclaimcoverage to tempclaimcoverage.
  end.
  
  */
  
  if avail primclaimproperty then
  do:
    create tempclaimproperty.
    buffer-copy primclaimproperty to tempclaimproperty.
  end.
  
  /* Refresh all screen-value buffers to their corresponding variables
  */
  do with frame fOverview:
    assign
      ovAgentID
      ovStateID
      ovDifficulty
      ovStage
      ovUrgency
      ovAction
      ovFileNumber 
      ovInsuredName
      /*
      ovCoverageID
      ovEffDate
      ovOrigLiabilityAmount
      ovCvgPrimary
      */
      ovBorrowerName
      ovRefYear
      /*ovYearFirstReport*/
      ovSummary
      ovAgentError
      ovAttorneyError
      ovSearcherError
      ovSearcher
      ovALTARisk 
      ovALTAResp
      ovPropAddr1
      ovPropAddr2
      ovPropCity
      ovPropStateID 
      ovPropCounty
      ovPropZipCode
      ovPropSubdivision
      ovPropPrimary
      ovPropResidential
      ovPropLegalDesc
      .  
  end.

  find first agent where agent.agentID = ovAgentID no-error.

  assign
    tempclaim.stage                   = ovStage             
    tempclaim.action                  = ovAction            
    tempclaim.difficulty              = ovDifficulty        
    tempclaim.urgency                 = ovUrgency           
    tempclaim.stateID                 = ovStateID
    tempclaim.agentID                 = ovAgentID
    tempclaim.agentName               = if avail agent then agent.name else ""
    tempclaim.agentStat               = if avail agent then agent.stat else ""
    tempclaim.fileNumber              = ovFileNumber
    tempclaim.insuredName             = ovInsuredName
    tempclaim.borrowerName            = ovBorrowerName
    tempclaim.agentError              = ovAgentError     
    tempclaim.attorneyError           = ovAttorneyError
    tempclaim.searcherError           = ovSearcherError     
    tempclaim.searcher                = ovSearcher
    tempclaim.altaRisk                = ovALTARisk
    tempclaim.altaResponsibility      = ovALTAResp
    tempclaim.refYear                 = ovRefYear
    /*tempclaim.yearFirstReport         = ovYearFirstReport*/
    tempclaim.summary                 = ovSummary
    .


  /*

  if avail tempclaimcoverage then
  assign    
    tempclaimcoverage.coverageID              = ovCoverageID
    tempclaimcoverage.coverageType            = tempclaimcoverage.coverageType
    tempclaimcoverage.insuredType             = tempclaimcoverage.insuredType
    tempclaimcoverage.origLiabilityAmount     = ovOrigLiabilityAmount
    tempclaimcoverage.currentLiabilityAmount  = tempclaimcoverage.currentLiabilityAmount
    tempclaimcoverage.effDate                 = ovEffDate
    tempclaimcoverage.coverageYear            = tempclaimcoverage.coverageYear
    tempclaimcoverage.insuredName             = tempclaimcoverage.insuredName
    tempclaimcoverage.claimant                = tempclaimcoverage.claimant
    tempclaimcoverage.isPrimary               = ovCvgPrimary
    .
    */

  if avail tempclaimproperty then
  assign    
    tempclaimproperty.isPrimary         = ovPropPrimary
    tempclaimproperty.addr1             = ovPropAddr1
    tempclaimproperty.addr2             = ovPropAddr2
    tempclaimproperty.city              = ovPropCity
    tempclaimproperty.countyID          = ovPropCounty
    tempclaimproperty.stateID           = ovPropStateID 
    tempclaimproperty.zipcode           = ovPropZipCode
    tempclaimproperty.residential       = ovPropResidential
    tempclaimproperty.subdivision       = ovPropSubdivision
    tempclaimproperty.legalDescription  = ovPropLegalDesc
    tempclaimproperty.propertyType      = tempclaimproperty.propertyType
    .
      

  run ModifyClaim in hFileDataSrv (input table tempclaim,
                                   output std-lo,
                                   output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return false.
  end.

  /*

  if avail tempclaimcoverage then
  do:
    run ModifyClaimCoverage in hFileDataSrv (input table tempclaimcoverage,
                                                     output std-lo,
                                                     output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
    
    buffer-copy tempclaimcoverage to primclaimcoverage.
  end.
  */
  
  if avail tempclaimproperty then
  do:
    run ModifyClaimProperty in hFileDataSrv (input table tempclaimproperty,
                                                     output std-lo,
                                                     output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
    
    buffer-copy tempclaimproperty to primclaimproperty.
  end.

  buffer-copy tempclaim to claim.

  refreshClaim().
  
  /*
  tPrimaryCvgSeq = 0.
  getPrimaryCoverage().
  */

  tPrimaryPropSeq = 0.
  getPrimaryProperty().

  /*
  openCoverage().
  displayCoverage().
  */
  
  openProperties().
  displayProperties().
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION savePage wWin 
FUNCTION savePage RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  case pPageNumber:
    when 1 then
    do:
      std-lo = saveOverview().
      if not std-lo then
      return false.
    end.
    when 2 then
    do:
      std-lo = saveCoverage().
      if not std-lo then
      return false.
    end.
    when 3 then
    do:
      std-lo = saveProperty().
      if not std-lo then
      return false.
    end.
    when 4 then
    do:
      std-lo = saveContact().
      if not std-lo then
      return false.
    end.
    when 5 then
    do:
      /* Accounting page is read only */
    end.
    when 6 then
    do:
      std-lo = saveRecovery().
      if not std-lo then
      return false.
    end.
    when 7 then
    do:
      std-lo = saveLitigation().
      if not std-lo then
      return false.
    end.
    when 8 then
    do:
      /* Linked Claims page is read only */
    end.
    when 9 then
    do:
      std-lo = saveActivity().
      if not std-lo then
      return false.
    end.
    when 10 then
    do:
      /* saveTasks(). */
    end.
  end case.
  
  saveStateDisable(pPageNumber).

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveProperty wWin 
FUNCTION saveProperty RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if newRecord = no then
  do:
    std-lo = getCurrentProperty().
    if not std-lo then
    return false.
  end.

  empty temp-table tempclaimproperty no-error.
  
  create tempclaimproperty.
  
  if newRecord = no then
  buffer-copy claimproperty to tempclaimproperty.
  else
  assign
    tempclaimproperty.claimID = pBaseClaimID
    tempclaimproperty.seq     = 0.
  
  do with frame fProperties:
    assign
      prAddr1 
      prAddr2 
      prCity 
      prStateID 
      prCountyID 
      prZipCode 
      prSubdivision 
      prIsPrimary 
      prResidential 
      prLegalDesc     
      .
  end.

  assign    
    tempclaimproperty.isPrimary         = prIsPrimary
    tempclaimproperty.addr1             = prAddr1
    tempclaimproperty.addr2             = prAddr2
    tempclaimproperty.city              = prCity
    tempclaimproperty.countyID          = prCountyID
    tempclaimproperty.stateID           = prStateID
    tempclaimproperty.zipcode           = prZipCode
    tempclaimproperty.residential       = prResidential
    tempclaimproperty.subdivision       = prSubdivision
    tempclaimproperty.legalDescription  = prLegalDesc
    tempclaimproperty.propertyType      = ""
    .

  if newRecord = yes then
  do:
    run NewClaimProperty in hFileDataSrv (input table tempclaimproperty,
                                                  output tSeq,
                                                  output std-lo,
                                                  output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  
    tempclaimproperty.seq = tSeq.
  end.
  else 
  do:
    run ModifyClaimProperty in hFileDataSrv (input table tempclaimproperty,
                                                     output std-lo,
                                                     output std-ch).
    if not std-lo 
     then
      do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX error BUTTONS OK.
          return false.
      end.
  end.
  
  run GetClaimProperties in hFileDataSrv (output table claimproperty).

  tCurrentPropSeq = if avail tempclaimproperty then tempclaimproperty.seq else 0.
  openProperties().
  displayProperties().
  
  /* Refresh overview page in the event that the primary Property changed */
  tPrimaryPropSeq = 0.
  getPrimaryProperty().
  displayOverview().

  /* refreshClaim(). */

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveRecovery wWin 
FUNCTION saveRecovery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  std-lo = getBaseClaim().
  if not std-lo then
  return false.

  empty temp-table tempclaim no-error.
  
  create tempclaim.
  buffer-copy claim to tempclaim.
  
  /* Refresh all screen-value buffers to their corresponding variables
  */
  do with frame fRecovery:
    assign
      reHasReinsurance 
      reSeekingRecovery 
      reRecoveryNotes 
      reHasDeductible 
      reDeductibleAmount 
      reAmountWaived 
      reEOPolicy 
      reEOEffDate 
      reEORetroDate 
      reEOCarrier 
      reEOCoverageAmount 
      rePriorPolicyUsed 
      rePriorPolicyID 
      reUnderwritingCompliance 
      rePriorPolicyCarrier 
      reSellerBreach 
      rePriorPolicyAmount 
      rePriorPolicyEffDate       
      .  
  end.

  assign
    tempclaim.hasReinsurance          = reHasReinsurance
    tempclaim.hasDeductible           = reHasDeductible
    tempclaim.deductibleAmount        = reDeductibleAmount            
    tempclaim.amountWaived            = reAmountWaived
    tempclaim.seekingRecovery         = reSeekingRecovery     
    tempclaim.recoveryNotes           = reRecoveryNotes
    tempclaim.eoPolicy                = reEOPolicy
    tempclaim.eoCarrier               = reEOCarrier
    tempclaim.eoEffDate               = reEOEffDate
    tempclaim.eoRetroDate             = reEORetroDate
    tempclaim.eoCoverageAmount        = reEOCoverageAmount
    tempclaim.priorPolicyUsed         = rePriorPolicyUsed               
    tempclaim.priorPolicyID           = rePriorPolicyID
    tempclaim.underwritingCompliance  = reUnderwritingCompliance
    tempclaim.sellerBreach            = reSellerBreach
    tempclaim.priorPolicyCarrier      = rePriorPolicyCarrier
    tempclaim.priorPolicyAmount       = rePriorPolicyAmount
    tempclaim.priorPolicyEffDate      = rePriorPolicyEffDate
    .

  run ModifyClaim in hFileDataSrv (input table tempclaim,
                                           output std-lo,
                                           output std-ch).
  if not std-lo then
  do:
    MESSAGE std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
    return false.
  end.

  buffer-copy tempclaim to claim.

  /* refreshClaim(). */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveStateDisable wWin 
FUNCTION saveStateDisable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  case pPageNumber:
    when 1 then
    do with frame fOverview:
      bOverviewSave:sensitive = no.
      bOverviewCancel:sensitive = no.
      
      bDescCodeAdd:sensitive = yes.
      bDescCodeDelete:sensitive = yes.
      bDescCodeUp:sensitive = yes.
      bDescCodeDown:sensitive = yes.
      
      bCauseCodeAdd:sensitive = yes.
      bCauseCodeDelete:sensitive = yes.
      bCauseCodeUp:sensitive = yes.
      bCauseCodeDown:sensitive = yes.
    end.
    when 2 then
    do with frame fCoverage:
      bCoverageSave:sensitive = no.
      bCoverageAdd:sensitive = yes.
      bCoverageDelete:sensitive = yes.
      bCoverageCancel:sensitive = no.
      browse brwCoverage:sensitive = yes.
      
      if not avail claimcoverage then
      do:
        setPageState(pPageNumber, "Disabled").
        bCoverageDelete:sensitive = no.
      end.
    end.
    when 3 then
    do with frame fProperties:
      bPropertySave:sensitive = no.
      bPropertyAdd:sensitive = yes.
      bPropertyDelete:sensitive = yes.
      bPropertyCancel:sensitive = no.
      browse brwProperties:sensitive = yes.
      
      if not avail claimproperty then
      do:
        setPageState(pPageNumber, "Disabled").
        bPropertyDelete:sensitive = no.
      end.
    end.
    when 4 then
    do with frame fContacts:
      bContactSave:sensitive = no.
      bContactAdd:sensitive = yes.
      bContactDelete:sensitive = yes.
      bContactCancel:sensitive = no.
      bContactEmail:sensitive = yes.
      browse brwContacts:sensitive = yes.
    
      if not avail claimcontact then
      do:
        setPageState(pPageNumber, "Disabled").
        bContactDelete:sensitive = no.
      end.
    end.
    when 5 then
    do with frame fAccounting:
      /* Accounting page is read only */
    end.
    when 6 then
    do with frame fRecovery:
      bRecoverySave:sensitive = no.
      bRecoveryCancel:sensitive = no.
      
      bBondAdd:sensitive = yes.
      bBondDelete:sensitive = yes.
      bBondModify:sensitive = yes.
    end.
    when 7 then
    do with frame fLitigation:
      bLitigationSave:sensitive = no.
      bLitigationAdd:sensitive = yes.
      bLitigationDelete:sensitive = yes.
      bLitigationCancel:sensitive = no.
      browse brwLitigation:sensitive = yes.
    
      if not avail claimlitigation then
      do:
        setPageState(pPageNumber, "Disabled").
        bLitigationDelete:sensitive = no.
      end.
    end.
    when 8 then
    do with frame fLinkedClaims:
      /* Linked Claims page is read only */
    end.
    when 9 then
    do with frame fActivity:
      bActivitySave:sensitive = no.
      bActivityCancel:sensitive = no.
    end.
    when 10 then
    do with frame fTasks:
      /* saveTasks(). */
    end.
  end case.

  dataChanged = no.
  newRecord = no.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveStateEnable wWin 
FUNCTION saveStateEnable RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pNewRecord as log ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  case pPageNumber:
    when 1 then
    do with frame fOverview:
      bOverviewSave:sensitive = yes.
      bOverviewCancel:sensitive = yes.
    end.
    when 2 then
    do with frame fCoverage:
      bCoverageSave:sensitive = yes.
      bCoverageAdd:sensitive = no.
      bCoverageDelete:sensitive = no.
      bCoverageCancel:sensitive = yes.
      browse brwCoverage:sensitive = no.
    end.
    when 3 then
    do with frame fProperties:
      bPropertySave:sensitive = yes.
      bPropertyAdd:sensitive = no.
      bPropertyDelete:sensitive = no.
      bPropertyCancel:sensitive = yes.
      browse brwProperties:sensitive = no.
    end.
    when 4 then
    do with frame fContacts:
      bContactSave:sensitive = yes.
      bContactAdd:sensitive = no.
      bContactDelete:sensitive = no.
      bContactCancel:sensitive = yes.
      bContactEmail:sensitive = no.
      browse brwContacts:sensitive = no.
    end.
    when 5 then
    do with frame fAccounting:
      /* Accounting page is read only */
    end.
    when 6 then
    do with frame fRecovery:
      bRecoverySave:sensitive = yes.
      bRecoveryCancel:sensitive = yes.
    end.
    when 7 then
    do with frame fLitigation:
      bLitigationSave:sensitive = yes.
      bLitigationAdd:sensitive = no.
      bLitigationDelete:sensitive = no.
      bLitigationCancel:sensitive = yes.
      browse brwLitigation:sensitive = no.
    end.
    when 8 then
    do with frame fLinkedClaims:
      /* Linked Claims page is read only */
    end.
    when 9 then
    do with frame fActivity:
      bActivitySave:sensitive = yes.
      bActivityCancel:sensitive = yes.
    end.
    when 10 then
    do with frame fTasks:
      /* saveTasks(). */
    end.
  end case.

  /* only flag that the data was changed if the tab is not on the accounting screen */
  if pPageNumber <> 5
   then dataChanged = yes.

  if pNewRecord
   then newRecord = yes.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setAssignee wWin 
FUNCTION setAssignee RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def buffer claim for claim.

  std-lo = checkDataChanged().
  if not std-lo 
   then return no-apply.

  std-lo = getBaseClaim().
  if not std-lo 
   then return false.

  find claim 
    where claim.claimID = pBaseClaimID no-error.
  if not available claim 
   then
    do: 
        MESSAGE "Source data not available."
            VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.

  run dialogtransfer.w (input hFileDataSrv, 
                        input claim.assignedTo, /* Current Assignee */
                        output std-lo,
                        output std-ch).         /* New Assignee */

  if not std-lo 
   then return false.

  claim.assignedTo = std-ch.

  displayHeader().
  setHeaderState().

  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setHeaderState wWin 
FUNCTION setHeaderState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame fMain:
    bMarkMatter:sensitive = avail claim and claim.stat <> "C" and claim.type <> "M".
    bMarkClaim:sensitive = avail claim and claim.stat <> "C" and claim.type <> "C".
/*     bLock:sensitive = avail claim and claim.stat <> "C".  */
    bClose:sensitive = avail claim and claim.stat <> "C".
    bReopen:sensitive = avail claim and claim.stat = "C".
    bTransfer:sensitive = avail claim and claim.stat <> "C".

/*     bSuppClaim:sensitive = avail claim.  */
/*     bReports:sensitive = avail claim.  */
    bAcct:sensitive = avail claim and claim.type <> "N".
    bDocs:sensitive = avail claim.
    bNotes:sensitive = avail claim.
    bAttr:sensitive = avail claim.
    bNewNote:sensitive = avail claim.
/*     bSetAssignedTo:sensitive = avail claim and claim.stat <> "C". */
  
    menu-item m_Mark_as_Matter:sensitive in menu MENU-BAR-wWin = bMarkMatter:sensitive.
    menu-item m_Mark_as_Claim:sensitive in menu MENU-BAR-wWin = bMarkClaim:sensitive.
    menu-item m_Transfer_to:sensitive in menu MENU-BAR-wWin = bTransfer:sensitive.
/*     menu-item m_Lock:sensitive in menu MENU-BAR-wWin = bLock:sensitive. */
    menu-item m_Close:sensitive in menu MENU-BAR-wWin = bClose:sensitive.
    menu-item m_Reopen:sensitive in menu MENU-BAR-wWin = bReopen:sensitive.
    menu-item m_Add_Note:sensitive in menu MENU-BAR-wWin = bNewNote:sensitive.
    menu-item m_Notes:sensitive in menu MENU-BAR-wWin = bNotes:sensitive.
    menu-item m_Documents:sensitive in menu MENU-BAR-wWin = bDocs:sensitive.
    menu-item m_Accounting:sensitive in menu MENU-BAR-wWin = bAcct:sensitive.
    menu-item m_Attributes:sensitive in menu MENU-BAR-wWin = bAttr:sensitive.
/*     menu-item m_Reports:sensitive in menu MENU-BAR-wWin = bReports:sensitive.  */
  end.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setLockState wWin 
FUNCTION setLockState RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 /* das: Do nothing yet...until we know what we want to do

  std-lo = checkDataChanged().
  if not std-lo then
  return no-apply.

  std-lo = getBaseClaim().
  if not std-lo then
  return false.

  */
  
  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setPageState wWin 
FUNCTION setPageState RETURNS LOGICAL PRIVATE
  ( input pPageNumber as int, input pState as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  case pPageNumber:
    when 1 then
    run setContainerState(frame fOverview:handle, pState).
    when 2 then
    run setContainerState(frame fCoverage:handle, pState).
    when 3 then
    run setContainerState(frame fProperties:handle, pState).
    when 4 then
    run setContainerState(frame fContacts:handle, pState).
    when 5 then
    run setContainerState(frame fAccounting:handle, pState).
    when 6 then
    run setContainerState(frame fRecovery:handle, pState).
    when 7 then
    run setContainerState(frame fLitigation:handle, pState).
    when 8 then
    run setContainerState(frame fLinked:handle, pState).
    when 9 then
    run setContainerState(frame fActivity:handle, pState).
    when 10 then
    run setContainerState(frame fTasks:handle, pState).
  end case.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setToClaim wWin 
FUNCTION setToClaim RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-lo = checkAgentValidation().
  if not std-lo 
   then return no-apply.
   
  std-lo = checkDataChanged().
  if not std-lo 
   then return no-apply.

  std-lo = getBaseClaim().
  if not std-lo 
   then return false.
  
  std-lo = no.
  message
    "Set this file to Claim?"
    view-as alert-box buttons yes-no update std-lo.
  if not std-lo
   then return false.

/*   empty temp-table tempclaim no-error.                      */
/*                                                             */
/*   create tempclaim.                                         */
/*   buffer-copy claim to tempclaim.                           */
/*                                                             */
/*   assign                                                    */
/*     tempclaim.type = "C".                                   */
/*                                                             */
/*   run ModifyClaim in hFileDataSrv (input table tempclaim,   */
/*                                            output std-lo,   */
/*                                            output std-ch).  */

  run SetToClaim in hFileDataSrv (output std-lo, output std-ch).

  if not std-lo 
   then
    do: MESSAGE std-ch
          VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.
  
  claim.type = "C".

/*   buffer-copy tempclaim to claim.  */

  displayHeader().
  setHeaderState().

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setToMatter wWin 
FUNCTION setToMatter RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-lo = checkAgentValidation().
  if not std-lo 
   then return no-apply.
   
  std-lo = checkDataChanged().
  if not std-lo 
   then return no-apply.

  std-lo = getBaseClaim().
  if not std-lo 
   then return false.
  
  std-lo = no.
  message
    "Set this file to Matter?"
    view-as alert-box buttons yes-no update std-lo.
  if not std-lo 
   then return false.

/*   empty temp-table tempclaim no-error.                     */
/*                                                            */
/*   create tempclaim.                                        */
/*   buffer-copy claim to tempclaim.                          */
/*                                                            */
/*   assign                                                   */
/*     tempclaim.type = "M".                                  */
/*                                                            */
/*   run ModifyClaim in hFileDataSrv (input table tempclaim,  */
/*                                            output std-lo,  */
/*                                            output std-ch). */

  run SetToMatter in hFileDataSrv (output std-lo, output std-ch).
  if not std-lo 
   then
    do: MESSAGE std-ch
          VIEW-AS ALERT-BOX error BUTTONS OK.
        return false.
    end.

/*   buffer-copy tempclaim to claim. */

  claim.type = "M".

  displayHeader().
  setHeaderState().

  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

