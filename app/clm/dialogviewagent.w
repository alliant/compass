&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

def input parameter pAgentID as char.  
def input parameter pStateID as char.

def input-output parameter pName as char.
def input-output parameter pAddr1 as char.
def input-output parameter pAddr2 as char.
def input-output parameter pCity as char.
def input-output parameter pState as char.
def input-output parameter pZip as char.
def input-output parameter pPhone as char.
def input-output parameter pFax as char.
def input-output parameter pEmail as char.
def input-output parameter pWebsite as char.
def input-output parameter pSwVendor as char.
def input-output parameter pSwVersion as char.

def input-output parameter pStateLicense as char.
def input-output parameter pStateLicenseEff as datetime.
def input-output parameter pStateLicenseExp as datetime.

def input-output parameter pContractID as char.
def input-output parameter pContractDate as datetime.
def input-output parameter pMaxCoverage as decimal.
def input-output parameter pRemitType as char.
def input-output parameter pRemitValue as decimal.
def input-output parameter pRemitAlert as decimal.

def input-output parameter pEoCompany as char.
def input-output parameter pEoPolicy as char.
def input-output parameter pEoCoverage as decimal.
def input-output parameter pEoAggregate as decimal.
def input-output parameter pEoStart as datetime.
def input-output parameter pEoEnd as datetime.

def input-output parameter pStat as char.
def input-output parameter pProspectDate as datetime.
def input-output parameter pActiveDate as datetime.
def input-output parameter pClosedDate as datetime.
def input-output parameter pCancelDate as datetime.
def input-output parameter pReviewDate as datetime.

def input-output parameter pmName as char.
def input-output parameter pmAddr1 as char.
def input-output parameter pmAddr2 as char.
def input-output parameter pmCity as char.
def input-output parameter pmState as char.
def input-output parameter pmZip as char.

def input-output parameter pAltaUID as char. 

def output parameter pCancel as logical init true.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tName tAddr1 tAddr2 tCity tState tZip ~
tAgentID tPhone tFax tEmail tWebsite tSwVendor tSwVersion tStateLicense ~
tStateLicenseEff tStateLicenseExp tContractID tContractDate tMaxCoverage ~
tRemitValue tRemitAlert tEoCompany tEoPolicy tEoCoverage tEoAggregate ~
tEoStart tEoEnd tProspectDate tActiveDate tReviewDate tClosedDate ~
tCancelDate mName mAddr1 mAddr2 mCity mZip tAltaUID Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tName tAddr1 tAddr2 tStateID tCity tState ~
tZip tStat tAgentID tPhone tFax tEmail tWebsite tSwVendor tSwVersion ~
tStateLicense tStateLicenseEff tStateLicenseExp tContractID tContractDate ~
tMaxCoverage tRemitType tRemitValue tRemitAlert tEoCompany tEoPolicy ~
tEoCoverage tEoAggregate tEoStart tEoEnd tProspectDate tActiveDate ~
tReviewDate tClosedDate tCancelDate mName mAddr1 mAddr2 mCity mState mZip ~
tAltaUID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Done" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE mState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Colorado","CO",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY"
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     LIST-ITEM-PAIRS "Active","A",
                     "Prospect","P",
                     "Closed","C",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Colorado","CO",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY"
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Colorado","CO",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY"
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE mAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE mAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE mCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 23.2 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE mName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE mZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE VARIABLE tActiveDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Active" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAltaUID AS CHARACTER FORMAT "X(200)":U 
     LABEL "ALTA ID" 
     VIEW-AS FILL-IN 
     SIZE 39.4 BY 1 NO-UNDO.

DEFINE VARIABLE tCancelDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Cancelled" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 23.2 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE tClosedDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Closed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tContractDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Signed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tContractID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contract" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 50.4 BY 1 NO-UNDO.

DEFINE VARIABLE tEoAggregate AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Aggregate" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEoCompany AS CHARACTER FORMAT "X(256)":U 
     LABEL "Carrier" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE tEoCoverage AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Coverage" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEoEnd AS DATETIME FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEoPolicy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE tEoStart AS DATETIME FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tMaxCoverage AS DECIMAL FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "Coverage" 
     VIEW-AS FILL-IN 
     SIZE 20.8 BY 1 TOOLTIP "Maximum coverage w/o prior approval" NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 NO-UNDO.

DEFINE VARIABLE tPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tProspectDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Prospect" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRemitAlert AS DECIMAL FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "Alert" 
     VIEW-AS FILL-IN 
     SIZE 18.6 BY 1 TOOLTIP "Processing threshold for review" NO-UNDO.

DEFINE VARIABLE tRemitValue AS DECIMAL FORMAT "9.999":U INITIAL 0 
     LABEL "Remits" 
     VIEW-AS FILL-IN 
     SIZE 11.4 BY 1 TOOLTIP "Percentage, then value is % of gross :: Liability, then value is rate/1000" NO-UNDO.

DEFINE VARIABLE tReviewDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Reviewed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Last review" NO-UNDO.

DEFINE VARIABLE tStateLicense AS CHARACTER FORMAT "X(256)":U 
     LABEL "License" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE tStateLicenseEff AS DATETIME FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tStateLicenseExp AS DATETIME FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tSwVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Software" 
     VIEW-AS FILL-IN 
     SIZE 23.2 BY 1 TOOLTIP "Title company software vendor" NO-UNDO.

DEFINE VARIABLE tSwVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN 
     SIZE 18.2 BY 1 TOOLTIP "Version of title company software" NO-UNDO.

DEFINE VARIABLE tWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN 
     SIZE 50.6 BY 1 NO-UNDO.

DEFINE VARIABLE tZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE VARIABLE tRemitType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Percentage", "P",
"Liability", "L"
     SIZE 15.2 BY 1.71 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 61 BY 5.71.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 5.71.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 5.95.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66.2 BY 4.62.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60.8 BY 4.62.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 60.8 BY 5.95.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     tName AT ROW 2.67 COL 10.8 COLON-ALIGNED WIDGET-ID 4
     tAddr1 AT ROW 3.86 COL 10.8 COLON-ALIGNED WIDGET-ID 6
     tAddr2 AT ROW 5.05 COL 10.8 COLON-ALIGNED NO-LABEL WIDGET-ID 8
     tStateID AT ROW 8.52 COL 13 COLON-ALIGNED WIDGET-ID 104 NO-TAB-STOP 
     tCity AT ROW 6.24 COL 10.8 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     tState AT ROW 6.24 COL 34.4 COLON-ALIGNED NO-LABEL WIDGET-ID 150
     tZip AT ROW 6.24 COL 52.2 COLON-ALIGNED NO-LABEL WIDGET-ID 148
     tStat AT ROW 1.43 COL 78.6 COLON-ALIGNED WIDGET-ID 160
     tAgentID AT ROW 1.48 COL 10.8 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tPhone AT ROW 2.67 COL 78.4 COLON-ALIGNED WIDGET-ID 142
     tFax AT ROW 2.67 COL 106.8 COLON-ALIGNED WIDGET-ID 140
     tEmail AT ROW 3.86 COL 78.4 COLON-ALIGNED WIDGET-ID 20
     tWebsite AT ROW 5.05 COL 78.4 COLON-ALIGNED WIDGET-ID 22
     tSwVendor AT ROW 6.24 COL 78.4 COLON-ALIGNED WIDGET-ID 162
     tSwVersion AT ROW 6.24 COL 110.8 COLON-ALIGNED WIDGET-ID 164
     tStateLicense AT ROW 9.71 COL 12.8 COLON-ALIGNED WIDGET-ID 26
     tStateLicenseEff AT ROW 10.91 COL 12.8 COLON-ALIGNED WIDGET-ID 28
     tStateLicenseExp AT ROW 12.1 COL 12.8 COLON-ALIGNED WIDGET-ID 30
     tContractID AT ROW 14.86 COL 12.6 COLON-ALIGNED WIDGET-ID 34
     tContractDate AT ROW 16 COL 12.6 COLON-ALIGNED WIDGET-ID 36
     tMaxCoverage AT ROW 17.14 COL 12.6 COLON-ALIGNED WIDGET-ID 40
     tRemitType AT ROW 14.24 COL 47.8 NO-LABEL WIDGET-ID 152
     tRemitValue AT ROW 16 COL 45.6 COLON-ALIGNED WIDGET-ID 38
     tRemitAlert AT ROW 17.14 COL 45.6 COLON-ALIGNED WIDGET-ID 158
     tEoCompany AT ROW 8.62 COL 86 COLON-ALIGNED WIDGET-ID 56
     tEoPolicy AT ROW 9.81 COL 86 COLON-ALIGNED WIDGET-ID 58
     tEoCoverage AT ROW 11 COL 86 COLON-ALIGNED WIDGET-ID 60
     tEoAggregate AT ROW 12.19 COL 86 COLON-ALIGNED WIDGET-ID 62
     tEoStart AT ROW 11 COL 112 COLON-ALIGNED WIDGET-ID 64
     tEoEnd AT ROW 12.19 COL 112 COLON-ALIGNED WIDGET-ID 66
     tProspectDate AT ROW 14.71 COL 86.8 COLON-ALIGNED WIDGET-ID 48
     tActiveDate AT ROW 15.91 COL 86.8 COLON-ALIGNED WIDGET-ID 50
     tReviewDate AT ROW 17.05 COL 86.8 COLON-ALIGNED WIDGET-ID 46
     tClosedDate AT ROW 14.67 COL 112 COLON-ALIGNED WIDGET-ID 52
     tCancelDate AT ROW 15.86 COL 112 COLON-ALIGNED WIDGET-ID 54
     mName AT ROW 20.05 COL 10.8 COLON-ALIGNED WIDGET-ID 172
     mAddr1 AT ROW 21.24 COL 10.8 COLON-ALIGNED WIDGET-ID 166
     mAddr2 AT ROW 22.43 COL 10.8 COLON-ALIGNED NO-LABEL WIDGET-ID 168
     mCity AT ROW 23.62 COL 10.8 COLON-ALIGNED NO-LABEL WIDGET-ID 170
     mState AT ROW 23.62 COL 34.4 COLON-ALIGNED NO-LABEL WIDGET-ID 174
     mZip AT ROW 23.62 COL 52.2 COLON-ALIGNED NO-LABEL WIDGET-ID 176
     tAltaUID AT ROW 20.05 COL 88.6 COLON-ALIGNED WIDGET-ID 186
     Btn_Cancel AT ROW 25.52 COL 62 WIDGET-ID 132
     "Activity" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 13.76 COL 72.2 WIDGET-ID 138
          FONT 6
     "E&&O Coverage" VIEW-AS TEXT
          SIZE 17 BY .62 AT ROW 7.57 COL 72.2 WIDGET-ID 82
          FONT 6
     "Agency Agreement" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 13.76 COL 4.8 WIDGET-ID 78
          FONT 6
     "Operating State" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 7.57 COL 4.8 WIDGET-ID 76
          FONT 6
     "Remit:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 14.33 COL 40.8 WIDGET-ID 156
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE  WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fNew
     "Mailing Address" VIEW-AS TEXT
          SIZE 18 BY .62 AT ROW 18.86 COL 5 WIDGET-ID 180
          FONT 6
     "Other" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 18.86 COL 72.2 WIDGET-ID 184
          FONT 6
     RECT-1 AT ROW 7.81 COL 70.4 WIDGET-ID 68
     RECT-2 AT ROW 7.81 COL 2.8 WIDGET-ID 70
     RECT-4 AT ROW 14 COL 2.8 WIDGET-ID 74
     RECT-5 AT ROW 14 COL 70.6 WIDGET-ID 136
     RECT-3 AT ROW 19.1 COL 3 WIDGET-ID 178
     RECT-6 AT ROW 19.1 COL 70.6 WIDGET-ID 182
     SPACE(1.59) SKIP(2.27)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Agent" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

ASSIGN 
       mAddr1:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       mAddr2:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       mCity:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       mName:READ-ONLY IN FRAME fNew        = TRUE.

/* SETTINGS FOR COMBO-BOX mState IN FRAME fNew
   NO-ENABLE                                                            */
ASSIGN 
       mZip:READ-ONLY IN FRAME fNew        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME fNew
   NO-ENABLE                                                            */
ASSIGN 
       tActiveDate:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tAddr1:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tAddr2:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tAgentID:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tAltaUID:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tCancelDate:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tCity:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tClosedDate:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tContractDate:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tContractID:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEmail:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEoAggregate:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEoCompany:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEoCoverage:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEoEnd:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEoPolicy:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tEoStart:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tFax:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tMaxCoverage:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tName:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tPhone:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tProspectDate:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tRemitAlert:READ-ONLY IN FRAME fNew        = TRUE.

/* SETTINGS FOR RADIO-SET tRemitType IN FRAME fNew
   NO-ENABLE                                                            */
ASSIGN 
       tRemitValue:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tReviewDate:READ-ONLY IN FRAME fNew        = TRUE.

/* SETTINGS FOR COMBO-BOX tStat IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tStateID IN FRAME fNew
   NO-ENABLE                                                            */
ASSIGN 
       tStateLicense:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tStateLicenseEff:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tStateLicenseExp:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tSwVendor:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tSwVersion:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tWebsite:READ-ONLY IN FRAME fNew        = TRUE.

ASSIGN 
       tZip:READ-ONLY IN FRAME fNew        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* Agent */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStat fNew
ON VALUE-CHANGED OF tStat IN FRAME fNew /* Status */
DO:
  case self:screen-value:
   when "A" then if tActiveDate:input-value in frame fNew = ?
                  then tActiveDate:screen-value in frame fNew = string(today).
   when "P" then if tProspectDate:input-value in frame fNew = ?
                  then tProspectDate:screen-value in frame fNew = string(today).
   when "C" then if tClosedDate:input-value in frame fNew = ?
                  then tClosedDate:screen-value in frame fNew = string(today).
   when "X" then if tCancelDate:input-value in frame fNew = ?
                  then tCancelDate:screen-value in frame fNew = string(today).
  end case.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

assign
  tAgentID = pAgentID
  tName = pName
  tAddr1 = pAddr1
  tAddr2 = pAddr2
  tCity = pCity
  tState = pState
  tZip = pZip

  tPhone = pPhone
  tFax = pFax
  tEmail = pEmail
  tWebsite = pWebsite
  tSwVendor = pSwVendor
  tSwVersion = pSwVersion

  tStateID = pStateID
  tStateLicense = pStateLicense
  tStateLicenseEff = pStateLicenseEff
  tStateLicenseExp = pStateLicenseExp

  tContractID = pContractID
  tContractDate = pContractDate
  tMaxCoverage = pMaxCoverage
  tRemitType = pRemitType
  tRemitValue = pRemitValue
  tRemitAlert = pRemitAlert
  
  tStat = pStat
  tProspectDate = pProspectDate
  tActiveDate = pActiveDate
  tClosedDate = pClosedDate
  tCancelDate = pCancelDate
  tReviewDate = pReviewDate

  tEoCompany = pEoCompany
  tEoPolicy = pEoPolicy
  tEoCoverage = pEoCoverage
  tEoAggregate = pEoAggregate
  tEoStart = pEoStart
  tEoEnd = pEoEnd
  
  mName = pmName
  mAddr1 = pmAddr1
  mAddr2 = pmAddr2
  mCity = pmCity
  mState = pmState
  mZip = pmZip
  tAltaUID = pAltaUID
  .

if tStat = "" 
 then tStat = "A".
if lookup(tRemitType, "P,L") = 0 
 then tRemitType = "P".

RUN enable_UI.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  do with frame {&frame-name}:
    assign
      pName = tName:screen-value 
      pAddr1 = tAddr1:screen-value 
      pAddr2 = tAddr2:screen-value 
      pCity = tCity:screen-value 
      pState = tState:screen-value 
      pZip = tZip:screen-value 
      pStat = tStat:screen-value 
      pPhone = tPhone:screen-value 
      pFax = tFax:screen-value 
      pEmail = tEmail:screen-value 
      pWebsite = tWebsite:screen-value 
      pSwVendor = tSwVendor:screen-value 
      pSwVersion = tSwVersion:screen-value 
  
    /*pStateID = tStateID:screen-value */
      pStateLicense = tStateLicense:screen-value 
      pStateLicenseEff = tStateLicenseEff:input-value 
      pStateLicenseExp = tStateLicenseExp:input-value 
      pContractID = tContractID:screen-value 
      pContractDate = tContractDate:input-value 
      pMaxCoverage = tMaxCoverage:input-value 
      pRemitType = tRemitType:screen-value 
      pRemitValue = tRemitValue:input-value 
      pRemitAlert = tRemitAlert:input-value 
  
      pEoCompany = tEoCompany:screen-value 
      pEoPolicy = tEoPolicy:screen-value 
      pEoCoverage = tEoCoverage:input-value 
      pEoAggregate = tEoAggregate:input-value 
      pEoStart = tEoStart:input-value 
      pEoEnd = tEoEnd:input-value 
  
      pProspectDate = tProspectDate:input-value 
      pActiveDate = tActiveDate:input-value 
      pReviewDate = tReviewDate:input-value 
      pClosedDate = tClosedDate:input-value 
      pCancelDate = tCancelDate:input-value
      
      pmName = mName:screen-value
      pmAddr1 = mAddr1:screen-value 
      pmAddr2 = mAddr2:screen-value
      pmCity = mCity:screen-value
      pmState = mState:screen-value
      pmZip = mZip:screen-value
      
      pAltaUID = tAltaUID:screen-value
      
      pCancel = false
      .
  end.
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tName tAddr1 tAddr2 tStateID tCity tState tZip tStat tAgentID tPhone 
          tFax tEmail tWebsite tSwVendor tSwVersion tStateLicense 
          tStateLicenseEff tStateLicenseExp tContractID tContractDate 
          tMaxCoverage tRemitType tRemitValue tRemitAlert tEoCompany tEoPolicy 
          tEoCoverage tEoAggregate tEoStart tEoEnd tProspectDate tActiveDate 
          tReviewDate tClosedDate tCancelDate mName mAddr1 mAddr2 mCity mState 
          mZip tAltaUID 
      WITH FRAME fNew.
  ENABLE tName tAddr1 tAddr2 tCity tState tZip tAgentID tPhone tFax tEmail 
         tWebsite tSwVendor tSwVersion tStateLicense tStateLicenseEff 
         tStateLicenseExp tContractID tContractDate tMaxCoverage tRemitValue 
         tRemitAlert tEoCompany tEoPolicy tEoCoverage tEoAggregate tEoStart 
         tEoEnd tProspectDate tActiveDate tReviewDate tClosedDate tCancelDate 
         mName mAddr1 mAddr2 mCity mZip tAltaUID Btn_Cancel 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

