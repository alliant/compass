&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNote 
/*---------------------------------------------------------------------
@name dialogaddnote.w
@description Add a note 

@param 

@author John Oliver
@version 1.0
@created 11/18/2016
@notes 
---------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

define input parameter pClaimID as integer no-undo.
define input parameter pCategory as character no-undo.
define input parameter pSubject as character no-undo.
define input parameter hFileDataSrv as handle no-undo.

/* Local Variable Definitions ---                                       */
define variable lTimer as logical no-undo initial false.

{lib/std-def.i}
{tt/claimnote.i}

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
define variable lSaveNote as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNote

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSpellCheck tCategory tSubject tNote bSave 
&Scoped-Define DISPLAYED-OBJECTS tCategory tSubject tNote 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE tNote AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 160 BY 16
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 119 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNote
     bSpellCheck AT ROW 1.1 COL 166 WIDGET-ID 308 NO-TAB-STOP 
     tCategory AT ROW 1.48 COL 11 COLON-ALIGNED WIDGET-ID 310
     tSubject AT ROW 1.48 COL 44 COLON-ALIGNED WIDGET-ID 4
     tNote AT ROW 2.91 COL 13 NO-LABEL WIDGET-ID 8
     bSave AT ROW 19.57 COL 79.4
     "Note:" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 3 COL 7.2 WIDGET-ID 6
     SPACE(162.59) SKIP(17.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add Note"
         DEFAULT-BUTTON bSave WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNote
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fNote:SCROLLABLE       = FALSE
       FRAME fNote:HIDDEN           = TRUE.

ASSIGN 
       tCategory:READ-ONLY IN FRAME fNote        = TRUE.

ASSIGN 
       tNote:RETURN-INSERTED IN FRAME fNote  = TRUE.

ASSIGN 
       tSubject:READ-ONLY IN FRAME fNote        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNote fNote
ON WINDOW-CLOSE OF FRAME fNote /* Add Note */
DO:
  if not lSaveNote
   then message "You must save the note." view-as alert-box buttons ok.
   else APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fNote
ON CHOOSE OF bSave IN FRAME fNote /* Save */
DO:
  lTimer = true.
  run SaveNote in this-procedure.
  apply "WINDOW-CLOSE" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck fNote
ON CHOOSE OF bSpellCheck IN FRAME fNote /* Spell Check */
DO:
  ASSIGN tNote.
  run util/spellcheck.p ({&window-name}:handle, input-output tNote, output std-lo).
  display tNote with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNote 


/* ***************************  Main Block  *************************** */
{lib/notestimer.i &entity="'Claim'" &entityID=pClaimID &notes="tNote:screen-value in frame {&frame-name}" &subject="tSubject:screen-value in frame {&frame-name}" &category="tCategory:screen-value in frame {&frame-name}"}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:parent = ACTIVE-WINDOW.

bSpellCheck:load-image-up("images/spellcheck.bmp").
bSpellCheck:load-image-insensitive("images/spellcheck-i.bmp").


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  std-ch = "".
  publish "GetNoteCategoryDesc" (pCategory, output std-ch).
  if std-ch = ""
   then std-ch = "Unknown".
  tCategory:screen-value = std-ch.
  tSubject:screen-value = pSubject.
  lSaveNote = false.
  /*WAIT-FOR GO OF FRAME {&FRAME-NAME}.*/

  do while true:
    wait-for "U1" of frame {&frame-name} pause 5.
    run SaveNoteHistory.
    if lTimer
     then leave.
  end.

END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNote  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNote.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNote  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCategory tSubject tNote 
      WITH FRAME fNote.
  ENABLE bSpellCheck tCategory tSubject tNote bSave 
      WITH FRAME fNote.
  VIEW FRAME fNote.
  {&OPEN-BROWSERS-IN-QUERY-fNote}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveNote fNote 
PROCEDURE SaveNote :
/*------------------------------------------------------------------------------
@description Save the note
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    create claimnote.
    assign
      claimnote.claimID = pClaimID
      claimnote.category = pCategory
      claimnote.subject = tSubject:screen-value
      claimnote.notes = tNote:screen-value
      claimnote.noteDate = now
      claimnote.noteType = "U"
      .
    release claimnote.
    std-lo = false.
    run NewClaimNote in hFileDataSrv (table claimnote, output std-lo).
    if std-lo
     then
      do:
        run CompleteNoteHistory.
        lSaveNote = true.
        apply "WINDOW-CLOSE" to frame {&frame-name}.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

