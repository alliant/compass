&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wclm01-r.w
@desc Management Summary report
@author D.Sinclair   
@date 12.28.2016
@Modified
08/07/19 - RS - Removed "Policy Effective Date" from browse
 */


CREATE WIDGET-POOL.

{lib/add-delimiter.i}
{lib/std-def.i}
{tt/syscode.i &tableAlias="altarisk"}
{tt/syscode.i &tableAlias="altaresp"}
{tt/syscode.i &tableAlias="desccode"}
{tt/syscode.i &tableAlias="causecode"}
{tt/clm01.i}
{tt/agent.i}
{tt/state.i}
{tt/sysprop.i}

def var hData as handle no-undo.

{lib/winlaunch.i}

/* Do these here to avoid a non-fully displayed screen */
publish "GetCodes" ("ClaimAltaRisk", output table altarisk).
publish "GetCodes" ("ClaimAltaResp", output table altaresp).
publish "GetCodes" ("ClaimDescription", output table desccode).
publish "GetCodes" ("ClaimCause", output table causecode).
publish "GetSysProps" (output table sysprop).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES clm01

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData clm01.claimIDDesc clm01.assignedTo clm01.agentName clm01.stateID clm01.ownerPolicyOrigLiability clm01.lenderPolicyOrigLiability clm01.laeReserve clm01.lossReserve clm01.laeLTD clm01.lossLTD clm01.stage clm01.significant clm01.litigation clm01.descriptionCode clm01.causeCode clm01.agentError clm01.searcherError clm01.summary   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH clm01 by clm01.claimID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH clm01 by clm01.claimID.
&Scoped-define TABLES-IN-QUERY-brwData clm01
&Scoped-define FIRST-TABLE-IN-QUERY-brwData clm01


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport bGo RECT-39 tState tUser ~
fLossReserve fLossLTD fSignificant tAgent fLAEReserve fLAELTD fLitigation ~
brwData 
&Scoped-Define DISPLAYED-OBJECTS tState fLossReserve fLossLTD fSignificant ~
tAgent fLAEReserve fLAELTD fLitigation 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doFilterSort C-Win 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE fLitigation AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Litigation" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL","Yes","No" 
     DROP-DOWN-LIST
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE fSignificant AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Significant" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL","Yes","No" 
     DROP-DOWN-LIST
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 78 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE fLAELTD AS DECIMAL FORMAT "->>>,>>9":U INITIAL 0 
     LABEL "LAE Paid LTD is at Least" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE fLAEReserve AS DECIMAL FORMAT "->>>,>>9":U INITIAL 0 
     LABEL "LAE Reserve is at Least" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE fLossLTD AS DECIMAL FORMAT "->>>,>>9":U INITIAL 0 
     LABEL "Loss Paid LTD is at Least" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE fLossReserve AS DECIMAL FORMAT "->>>,>>9":U INITIAL 0 
     LABEL "Loss Reserve is at Least" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 106 BY 3.1.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 105 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      clm01 SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      clm01.claimIDDesc column-label "File Number" format "x(10)"
clm01.assignedTo column-label "Assigned To" format "x(100)" width 20
clm01.agentName column-label "Agent" format "x(100)" width 30
clm01.stateID column-label "State" format "x(4)"
clm01.ownerPolicyOrigLiability column-label "Owner Policy!Orig Liab" format "->>>,>>>,>>9"
clm01.lenderPolicyOrigLiability column-label "Lender Policy!Orig Liab" format "->>>,>>>,>>9"
clm01.laeReserve column-label "LAE Reserve!Balance" format "->>>,>>>,>>9"
clm01.lossReserve column-label "Loss Reserve!Balance" format "->>>,>>>,>>9"
clm01.laeLTD column-label "LAE Paid!LTD" format "->>>,>>>,>>9"
clm01.lossLTD column-label "Loss Paid!LTD" format "->>>,>>>,>>9"
clm01.stage column-label "Stage" format "x(14)"
clm01.significant column-label "Sig" format "Yes/No" width 4
clm01.litigation column-label "Lit" format "Yes/No" width 4
clm01.descriptionCode column-label "Company Description" format "x(500)" width 30
clm01.causeCode column-label "Company Cause" format "x(500)" width 30
clm01.agentError column-label "Agent!Error" format "x(10)"
clm01.searcherError column-label "Searcher!Error" format "x(10)"
clm01.summary column-label "Summary" format "x(200)" width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 235 BY 17.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.91 COL 97.4 WIDGET-ID 2 NO-TAB-STOP 
     bGo AT ROW 1.91 COL 90 WIDGET-ID 4 NO-TAB-STOP 
     tState AT ROW 1.95 COL 9 COLON-ALIGNED WIDGET-ID 82
     tUser AT ROW 1.95 COL 54 COLON-ALIGNED WIDGET-ID 92
     fLossReserve AT ROW 1.95 COL 135 COLON-ALIGNED WIDGET-ID 98
     fLossLTD AT ROW 1.95 COL 175 COLON-ALIGNED WIDGET-ID 102
     fSignificant AT ROW 1.95 COL 200.4 COLON-ALIGNED WIDGET-ID 94
     tAgent AT ROW 3.14 COL 9 COLON-ALIGNED WIDGET-ID 84
     fLAEReserve AT ROW 3.14 COL 135 COLON-ALIGNED WIDGET-ID 100
     fLAELTD AT ROW 3.14 COL 175 COLON-ALIGNED WIDGET-ID 104
     fLitigation AT ROW 3.14 COL 200.4 COLON-ALIGNED WIDGET-ID 96
     brwData AT ROW 4.81 COL 2 WIDGET-ID 200
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.24 COL 110 WIDGET-ID 108
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-39 AT ROW 1.48 COL 109 WIDGET-ID 106
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 237.2 BY 21.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Significant Claims"
         HEIGHT             = 21.62
         WIDTH              = 237.2
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData fLitigation fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tUser IN FRAME fMain
   NO-DISPLAY                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH clm01 by clm01.claimID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 3.67
       COLUMN          = 90
       HEIGHT          = .48
       WIDTH           = 14.4
       WIDGET-ID       = 90
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(fLitigation:HANDLE IN FRAME fMain).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Significant Claims */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Significant Claims */
DO:
  /* This event will close the window and terminate the procedure.  */

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Significant Claims */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if available clm01
   then publish "ClaimSelected" (string(clm01.claimID)).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  if clm01.laeReserve < 0 
   then clm01.laeReserve:fgcolor in browse brwData = 12.
   else clm01.laeReserve:fgcolor in browse brwData = ?.
  if clm01.lossReserve < 0 
   then clm01.lossReserve:fgcolor in browse brwData = 12.
   else clm01.lossReserve:fgcolor in browse brwData = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLAELTD
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLAELTD C-Win
ON LEAVE OF fLAELTD IN FRAME fMain /* LAE Paid LTD is at Least */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLAEReserve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLAEReserve C-Win
ON LEAVE OF fLAEReserve IN FRAME fMain /* LAE Reserve is at Least */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLitigation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLitigation C-Win
ON VALUE-CHANGED OF fLitigation IN FRAME fMain /* Litigation */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLossLTD
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLossLTD C-Win
ON LEAVE OF fLossLTD IN FRAME fMain /* Loss Paid LTD is at Least */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLossReserve
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLossReserve C-Win
ON LEAVE OF fLossReserve IN FRAME fMain /* Loss Reserve is at Least */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSignificant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSignificant C-Win
ON VALUE-CHANGED OF fSignificant IN FRAME fMain /* Significant */
DO:
  run sortData in this-procedure (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgent C-Win
ON VALUE-CHANGED OF tAgent IN FRAME fMain /* Agent */
DO:
  clearData().  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState C-Win
ON VALUE-CHANGED OF tState IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUser C-Win
ON VALUE-CHANGED OF tUser IN FRAME fMain /* Assigned To */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-status.i &entity="'Claim'"}
{lib/brw-main.i}
{lib/report-progress-bar.i}
{lib/set-buttons.i}
{lib/set-filters.i &tableName="'clm01'" &noSort=true}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

publish "GetClmUsersList" (",", "ALL", "ALL", output std-ch).
tUser:list-item-pairs in frame {&frame-name} = std-ch.

/*
publish "GetCredentialsID" (output std-ch).
if lookup(std-ch, fUser:list-item-pairs) > 0 
 then fUser:screen-value = std-ch.
 else fUser:screen-value = "ALL".
*/
tUser:screen-value = "ALL".

session:immediate-display = yes.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  /* create the combos */
  {lib/get-state-list.i &combo=tState &addAll=true}
  {lib/get-agent-list.i &combo=tAgent &state=tState &addAll=true}
  
  /* Get default Loss and LAE filter thresholds */
  find first sysprop where objAction = "SignificantClaimsReport"
                     and objProperty = "LossReserveThreshold"
                     no-lock no-error.
  fLossReserve:screen-value = if avail sysprop then sysprop.objValue else "0".
  
  find first sysprop where objAction = "SignificantClaimsReport"
                     and objProperty = "LAEReserveThreshold"
                     no-lock no-error.
  fLAEReserve:screen-value = if avail sysprop then sysprop.objValue else "0".
  
  find first sysprop where objAction = "SignificantClaimsReport"
                     and objProperty = "LossLTDThreshold"
                     no-lock no-error.
  fLossLTD:screen-value = if avail sysprop then sysprop.objValue else "0".
  
  find first sysprop where objAction = "SignificantClaimsReport"
                     and objProperty = "LAELTDThreshold"
                     no-lock no-error.
  fLAELTD:screen-value = if avail sysprop then sysprop.objValue else "0".
  
  clearData().
  run windowResized.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wclm10-r.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wclm10-r.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tState fLossReserve fLossLTD fSignificant tAgent fLAEReserve fLAELTD 
          fLitigation 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport bGo RECT-39 tState tUser fLossReserve fLossLTD fSignificant 
         tAgent fLAEReserve fLAELTD fLitigation brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

  &scoped-define ReportName "SignificantClaims"
  
  publish "GetExportType" (output std-ch).
  if std-ch = "X" 
   then run util/exporttoexcelbrowse.p (string(browse brwData:handle), {&ReportName}).
   else run util/exporttocsvbrowse.p (string(browse brwData:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def buffer clm01 for clm01.
  def var tFile as char no-undo.
  def buffer agent for agent.
  def var tStartTime as datetime.
  
  close query brwData.
  clearData().
  empty temp-table clm01.

  bExport:sensitive in frame {&frame-name} = false.
  
  tStartTime = now.
  run server/getclaimsummary.p (input tState:screen-value in frame fMain,
                                input tAgent:screen-value in frame fMain,
                                input ?,  /* No cutoff restriction */
                                input tUser:screen-value in frame fMain,
                                "O", /* (O)pen claims only */
                                output table clm01,
                                output std-lo,
                                output std-ch).
  
  if not std-lo 
   then message std-ch view-as alert-box information buttons ok.
   else
    do:
      run SetProgressStatus.
      publish "GetAgents" (output table agent).
      
      /* Set fields used during export for more complete data */
      for each clm01:
        clm01.yearOpened = year(clm01.dateOpened) no-error.
        clm01.claimIDDesc = string(clm01.claimID).
        
        publish "GetSysUserName" (input clm01.assignedTo, output clm01.assignedTo).
        publish "GetStatDesc" (input clm01.stat, output clm01.stat).
        publish "GetTypeDesc" (input clm01.type, output clm01.type).
        publish "GetStageDesc" (input clm01.stage, output clm01.stage).
        publish "GetActionDesc" (input clm01.action, output clm01.action).
        publish "GetAgentErrorDesc" (input clm01.agentError, output clm01.agentError).
        publish "GetSearcherErrorDesc" (input clm01.searcherError, output clm01.searcherError).
        if clm01.agentID = "Unknown"
	 then
  	  clm01.agentName = "Unknown".
	 else
	  do:
           find agent
             where agent.agentID = clm01.agentID no-error.
           if available agent 
            then clm01.agentName = agent.name.		  
	  end.

        find altarisk
          where altarisk.code = clm01.altaRiskCode no-error.
        if available altarisk 
         then clm01.altariskdesc = trim(clm01.altaRiskCode) + ". " + trim(altarisk.description).
        
        find altaresp
          where altaresp.code = clm01.altaResponsibilityCode no-error.
        if available altaresp 
         then clm01.altaresponsibilitydesc = trim(clm01.altaResponsibilityCode) + ". " + trim(altaresp.description).
        
        clm01.coDescription = "".
        if clm01.descriptionCode <> "" then
        do std-in = 1 to num-entries(clm01.descriptionCode):
          find desccode 
            where desccode.code = entry(std-in,clm01.descriptionCode) no-error.
          if avail desccode then
          clm01.coDescription = clm01.coDescription + (if clm01.coDescription <> "" then " / " else "") +
                                trim(entry(std-in,clm01.descriptionCode)) + ". " + trim(desccode.description).
        end.
        
        clm01.coCause = "".
        if clm01.causeCode <> "" then
        do std-in = 1 to num-entries(clm01.causeCode):
          find causecode where causecode.code = entry(std-in,clm01.causeCode) no-error.
          if avail causecode
           then clm01.coCause = clm01.coCause + (if clm01.coCause <> "" then " / " else "") + trim(entry(std-in,clm01.causeCode)) + ". " + trim(causecode.description).
        end.
        
        clm01.summary = trim(substring(clm01.summary,1,200)).
	if clm01.stateID = "Unknown"
	 then
	  clm01.stateID = "N/A".
      end.
      
      dataSortBy = "".
      dataSortDesc = no.
      run sortData ("claimIDDesc").
      appendStatus("in " + trim(string(interval(now, tStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds").
      displayStatus().
      
      enableButtons(can-find(first clm01)).
    end.
  run SetProgressEnd.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define variable tWhereClause as character no-undo.
  
  tWhereClause = doFilterSort().
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  setStatus(string(num-results("{&browse-name}")) + " significant claim(s) shown").
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame fMain:
    close query brwData.
    /*clear frame fMain.*/
    enableButtons(false).
    clearStatus().
  end.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doFilterSort Include 
FUNCTION doFilterSort RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cSignificant as character no-undo.
  define variable cLitigation as character no-undo.
  
  define variable tWhereClause as character no-undo.
  
  do with frame {&frame-name}:
    assign
      cSignificant = fSignificant:screen-value
      cLitigation = fLitigation:screen-value
      .
  end.
  
  /* build the query */
  if cSignificant <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "significant = '" + cSignificant + "'".
  if cLitigation <> "ALL"
   then tWhereClause = addDelimiter(tWhereClause," and ") + "litigation = '" + cLitigation + "'".
  /* set the amounts */
  tWhereClause = addDelimiter(tWhereClause," and ") + "lossReserve >= " + string(fLossReserve:input-value).
  tWhereClause = addDelimiter(tWhereClause," and ") + "laeReserve >= " + string(fLAEReserve:input-value).
  tWhereClause = addDelimiter(tWhereClause," and ") + "lossLTD >= " + string(fLossLTD:input-value).
  tWhereClause = addDelimiter(tWhereClause," and ") + "laeLTD >= " + string(fLAELTD:input-value).
  if tWhereClause > ""
   then tWhereClause = "where " + tWhereClause.
  
  RETURN tWhereClause.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

