&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fInvoice 
/*---------------------------------------------------------------------
@name dialogclaimpayable.w
@action 
@description Dialog for creating/modifying/viewing a claim payable

@param ClaimID;int;The claim ID
@param Apinv;complex;Table for the invoice
@param Apinva;complex;Table for the invoice approvals
@param Apinvd;complex;Table for the invoice distributions
@param FileDataSrv;handle;The handle to the filedatasrv.p procedure
@param Title;char;The title for the window
@param Success;logical;True if the invoice was saved


@author John Oliver
@version 1.0
@created 01/09/2017

@changelog
---------------------------------------------------------------------
01.25.2017 John Oliver: Added a check if the policy was valid for a
                        Loss payment
---------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/brw-multi-def.i}
{lib/next-seq.i}
{lib/count-rows.i}
{lib/get-column.i}
{lib/get-reserve-type.i}
{lib/account-repository.i}
{lib/change-made.i}
{lib/std-def.i}
{lib/add-delimiter.i}
{tt/apinv.i}
{tt/apinva.i}
{tt/apinvd.i}
{tt/account.i}
{tt/vendor.i}
{tt/sysuser.i}
{tt/claim.i}
{tt/claimcoverage.i}

/* Parameters Definitions ---                                           */
define input parameter pClaimID as integer no-undo.
define input parameter table for apinv.
define input parameter table for apinva.
define input parameter table for apinvd.
define input parameter hFileDataSrv as handle no-undo.
define input parameter pTitle as character no-undo.
define output parameter pSuccess as logical no-undo.

/* Local Variable Definitions ---                                       */
define variable lChanged as logical no-undo initial false.
define variable lLocked as logical no-undo initial false.
define variable lFileAdded as logical no-undo initial false.
define variable lFileDeleted as logical no-undo initial false.
define variable cCurrUserID as character no-undo.
define variable cAcctName as character no-undo.
define variable cVendorName as character no-undo.
define variable cVendorAddress as character no-undo.
define variable iInvoice as int no-undo.
define variable iSeq as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fInvoice
&Scoped-define BROWSE-NAME brwApproval

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES apinva apinvd

/* Definitions for BROWSE brwApproval                                   */
&Scoped-define FIELDS-IN-QUERY-brwApproval apinva.username apinva.amount apinva.stat apinva.dateActed apinva.uid   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwApproval   
&Scoped-define SELF-NAME brwApproval
&Scoped-define QUERY-STRING-brwApproval FOR EACH apinva
&Scoped-define OPEN-QUERY-brwApproval OPEN QUERY {&SELF-NAME} FOR EACH apinva.
&Scoped-define TABLES-IN-QUERY-brwApproval apinva
&Scoped-define FIRST-TABLE-IN-QUERY-brwApproval apinva


/* Definitions for BROWSE brwDistribution                               */
&Scoped-define FIELDS-IN-QUERY-brwDistribution apinvd.acct apinvd.acctName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDistribution   
&Scoped-define SELF-NAME brwDistribution
&Scoped-define QUERY-STRING-brwDistribution FOR EACH apinvd
&Scoped-define OPEN-QUERY-brwDistribution OPEN QUERY {&SELF-NAME} FOR EACH apinvd.
&Scoped-define TABLES-IN-QUERY-brwDistribution apinvd
&Scoped-define FIRST-TABLE-IN-QUERY-brwDistribution apinvd


/* Definitions for DIALOG-BOX fInvoice                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fInvoice ~
    ~{&OPEN-QUERY-brwApproval}~
    ~{&OPEN-QUERY-brwDistribution}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS chkInContention tVendor bVendorLookup ~
tVendorName tVendorAddress cmbRefCategory cmbCoverage tInvoiceNbr tPONbr ~
tInvoiceDate tDateReceived tAmount tDueDate bLock tFile bFileUpload tNotes ~
bSave bAddDistribution bDeleteDistribution bCancel bAddApproval ~
bDeleteApproval brwApproval brwDistribution tApprovalNotes tRefID RECT-36 ~
RECT-38 RECT-39 
&Scoped-Define DISPLAYED-OBJECTS chkInContention tVendor tVendorName ~
tVendorAddress cmbRefCategory cmbCoverage tInvoiceNbr tPONbr tInvoiceDate ~
tDateReceived tAmount tDueDate tFile tNotes tApprovalNotes tRefID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Lock fInvoice 
FUNCTION Lock RETURNS LOGICAL
  ( input pLock as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD LockAll fInvoice 
FUNCTION LockAll RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeApproval fInvoice 
FUNCTION SensitizeApproval RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeDistribution fInvoice 
FUNCTION SensitizeDistribution RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeFileUpload fInvoice 
FUNCTION SensitizeFileUpload RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Menu Definitions                                                     */
DEFINE MENU menuFile 
       MENU-ITEM m_Delete_File  LABEL "Delete File"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddApproval 
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add an approval".

DEFINE BUTTON bAddDistribution 
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a distribution".

DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bDeleteApproval 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected approver".

DEFINE BUTTON bDeleteDistribution 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the selected distribution".

DEFINE BUTTON bFileUpload 
     LABEL "Upload" 
     SIZE 4.8 BY 1.14 TOOLTIP "Upload an invoice document".

DEFINE BUTTON bLock 
     LABEL "Unlock" 
     SIZE 7.2 BY 1.71 TOOLTIP "Lock the invoice".

DEFINE BUTTON bSave 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bVendorLookup 
     LABEL "Vendor Lookup" 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE cmbCoverage AS INTEGER FORMAT "999999999":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 19 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE cmbRefCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX 
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 19 BY 1 TOOLTIP "The referring category of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tApprovalNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62.2 BY 5.71 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 86 BY 8.76
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The amount of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDateReceived AS DATE FORMAT "99/99/9999":U 
     LABEL "Date Received" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The date of when Alliant National received the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Due Date" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The due date of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 80 BY 1 TOOLTIP "The path of the invoice document within ShareFile" DROP-TARGET NO-UNDO.

DEFINE VARIABLE tInvoiceDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Invoice Date" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The date on the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tInvoiceNbr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Invoice #" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The invoice number" NO-UNDO.

DEFINE VARIABLE tPONbr AS CHARACTER FORMAT "X(256)":U 
     LABEL "P.O. Number" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 TOOLTIP "The P.O. number associated with the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tRefID AS INTEGER FORMAT "99999999":U INITIAL 0 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "The file number"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tVendor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Payee Number" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "Tthe GP Vendor Number"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tVendorAddress AS CHARACTER FORMAT "X(256)":U 
     LABEL "Payee Address" 
     VIEW-AS FILL-IN 
     SIZE 75 BY .95
     FONT 1 NO-UNDO.

DEFINE VARIABLE tVendorName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Payee Name" 
     VIEW-AS FILL-IN 
     SIZE 75 BY 1
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92 BY 6.19.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66.4 BY 6.19.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66.4 BY 12.14.

DEFINE VARIABLE chkInContention AS LOGICAL INITIAL no 
     LABEL "Mark the Invoice in Contention" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.6 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwApproval FOR 
      apinva SCROLLING.

DEFINE QUERY brwDistribution FOR 
      apinvd SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwApproval fInvoice _FREEFORM
  QUERY brwApproval DISPLAY
      apinva.username column-label "Name" format "x(32)" width 18
      apinva.amount column-label "Amount" format "->,>>>,>>9.99" width 12
      apinva.stat column-label "Status" format "x(20)" width 7
      apinva.dateActed column-label "Approved" format "99/99/99" width 12
      apinva.uid column-label "User" format "x(30)" width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 57.2 BY 4.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDistribution fInvoice _FREEFORM
  QUERY brwDistribution DISPLAY
      apinvd.acct column-label "Account Number" format "x(25)" width 18
      apinvd.acctName column-label "Account Name" format "x(50)" width 35
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 57.2 BY 4.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fInvoice
     chkInContention AT ROW 1.76 COL 92.4 WIDGET-ID 86
     tVendor AT ROW 1.71 COL 18 COLON-ALIGNED WIDGET-ID 8
     bVendorLookup AT ROW 1.62 COL 34 WIDGET-ID 66
     tVendorName AT ROW 2.91 COL 18 COLON-ALIGNED WIDGET-ID 64
     tVendorAddress AT ROW 4.1 COL 18 COLON-ALIGNED WIDGET-ID 58
     cmbRefCategory AT ROW 6.71 COL 18 COLON-ALIGNED WIDGET-ID 28
     cmbCoverage AT ROW 6.71 COL 63 COLON-ALIGNED WIDGET-ID 84
     tInvoiceNbr AT ROW 7.91 COL 18 COLON-ALIGNED WIDGET-ID 2
     tPONbr AT ROW 7.91 COL 63 COLON-ALIGNED WIDGET-ID 62
     tInvoiceDate AT ROW 9.1 COL 18 COLON-ALIGNED WIDGET-ID 10
     tDateReceived AT ROW 9.1 COL 63 COLON-ALIGNED WIDGET-ID 56
     tAmount AT ROW 10.29 COL 18 COLON-ALIGNED WIDGET-ID 12
     tDueDate AT ROW 10.29 COL 63 COLON-ALIGNED WIDGET-ID 14
     bLock AT ROW 1.71 COL 156.2 WIDGET-ID 74 NO-TAB-STOP 
     tFile AT ROW 12.29 COL 7 COLON-ALIGNED WIDGET-ID 80
     bFileUpload AT ROW 12.19 COL 90.2 WIDGET-ID 82
     tNotes AT ROW 13.67 COL 9 NO-LABEL WIDGET-ID 30
     bSave AT ROW 22.91 COL 67 WIDGET-ID 36
     bAddDistribution AT ROW 4.33 COL 99 WIDGET-ID 42 NO-TAB-STOP 
     bDeleteDistribution AT ROW 5.52 COL 99 WIDGET-ID 54 NO-TAB-STOP 
     bCancel AT ROW 22.91 COL 84 WIDGET-ID 38
     bAddApproval AT ROW 11 COL 99 WIDGET-ID 44 NO-TAB-STOP 
     bDeleteApproval AT ROW 12.19 COL 99 WIDGET-ID 52 NO-TAB-STOP 
     brwApproval AT ROW 11.05 COL 104 WIDGET-ID 300
     brwDistribution AT ROW 4.38 COL 104 WIDGET-ID 200
     tApprovalNotes AT ROW 16.24 COL 99 NO-LABEL WIDGET-ID 72 NO-TAB-STOP 
     tRefID AT ROW 2.1 COL 140 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     "Distributions" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 3.38 COL 98 WIDGET-ID 40
          FONT 6
     "Invoice Information" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 5.52 COL 25 RIGHT-ALIGNED WIDGET-ID 6
          FONT 6
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 13.67 COL 2.2 WIDGET-ID 32
     "Approvals" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 10.05 COL 98 WIDGET-ID 46
          FONT 6
     "Mark the Invoice in Contention?" VIEW-AS TEXT
          SIZE 31 BY .62 AT ROW 1.86 COL 60.2 WIDGET-ID 88
     RECT-36 AT ROW 5.76 COL 3 WIDGET-ID 4
     RECT-38 AT ROW 3.62 COL 97 WIDGET-ID 76
     RECT-39 AT ROW 10.29 COL 97 WIDGET-ID 78
     SPACE(1.99) SKIP(1.94)
    WITH VIEW-AS DIALOG-BOX 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fInvoice
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwApproval bDeleteApproval fInvoice */
/* BROWSE-TAB brwDistribution brwApproval fInvoice */
ASSIGN 
       FRAME fInvoice:SCROLLABLE       = FALSE
       FRAME fInvoice:HIDDEN           = TRUE.

ASSIGN 
       tApprovalNotes:READ-ONLY IN FRAME fInvoice        = TRUE.

ASSIGN 
       tFile:POPUP-MENU IN FRAME fInvoice       = MENU menuFile:HANDLE.

ASSIGN 
       tNotes:RETURN-INSERTED IN FRAME fInvoice  = TRUE.

/* SETTINGS FOR TEXT-LITERAL "Invoice Information"
          SIZE 22 BY .62 AT ROW 5.52 COL 25 RIGHT-ALIGNED               */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwApproval
/* Query rebuild information for BROWSE brwApproval
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apinva.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwApproval */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDistribution
/* Query rebuild information for BROWSE brwDistribution
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH apinvd.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwDistribution */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fInvoice fInvoice
ON WINDOW-CLOSE OF FRAME fInvoice
DO:
  std-lo = true.
  if lChanged
   then message
          "Data has been modified." skip
          "Are you sure you want to exit without saving?"
          view-as alert-box warning buttons yes-no
          update std-lo.
  if std-lo
   then APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddApproval fInvoice
ON CHOOSE OF bAddApproval IN FRAME fInvoice /* Add */
DO:
  RUN AddApproval IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddDistribution fInvoice
ON CHOOSE OF bAddDistribution IN FRAME fInvoice /* Add */
DO:
  RUN AddDistribution IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fInvoice
ON CHOOSE OF bCancel IN FRAME fInvoice /* Cancel */
DO:
  APPLY "WINDOW-CLOSE":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteApproval fInvoice
ON CHOOSE OF bDeleteApproval IN FRAME fInvoice /* Delete */
DO:
  RUN DeleteApproval IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteDistribution fInvoice
ON CHOOSE OF bDeleteDistribution IN FRAME fInvoice /* Delete */
DO:
  RUN DeleteDistribution IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload fInvoice
ON CHOOSE OF bFileUpload IN FRAME fInvoice /* Upload */
DO:
  run FileUpload in this-procedure ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLock
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLock fInvoice
ON CHOOSE OF bLock IN FRAME fInvoice /* Unlock */
DO:
  Lock(not lLocked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwApproval
&Scoped-define SELF-NAME brwApproval
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwApproval fInvoice
ON ROW-DISPLAY OF brwApproval IN FRAME fInvoice
DO:
  {lib/brw-rowDisplay-multi.i}
  /* set the user's name for the requested by column */
  std-ch = "".
  publish "GetSysUserName" (apinva.uid,output std-ch).
  if std-ch = "NOT FOUND"
   then std-ch = apinva.uid.
  apinva.username:screen-value in browse brwApproval = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwApproval fInvoice
ON START-SEARCH OF brwApproval IN FRAME fInvoice
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDistribution
&Scoped-define SELF-NAME brwDistribution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistribution fInvoice
ON ROW-DISPLAY OF brwDistribution IN FRAME fInvoice
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistribution fInvoice
ON ROW-LEAVE OF brwDistribution IN FRAME fInvoice
DO:
  define variable hBrowse as handle no-undo.
  define variable oldAcct as character no-undo initial "".
  define variable newAcct as character no-undo initial "".
  hBrowse = browse brwDistribution:handle.
  if valid-handle(hBrowse) and iSeq > 0 /* a sequence of 0 mean that this isn't really a row-leave */
   then
    do:
      /* if this is a new record */
      if hBrowse:new-row
       then lChanged = true.
       else
        assign
          iSeq = apinvd.seq
          oldAcct = apinvd.acct
          cAcctName = apinvd.acctname
          .
       
       
      /* get the account number and the amount to see if they changed */
      std-ha = getColumn(hBrowse,"acct").
      if valid-handle(std-ha)
       then newAcct = std-ha:input-value.
      /* only get the account name if it changed */
      if oldAcct <> newAcct
       then
        do:
          /* get the account name */
          publish "GetAccount" (newAcct, output table account).
          /* get the account name */
          for first account no-lock:
            cAcctName = account.acctname.
          end.
          /* set the account name */
          std-ha = getColumn(hBrowse,"acctname").
          if valid-handle(std-ha)
           then std-ha:screen-value = cAcctName.
        end.
      
      
      /* modify the record */
      for first apinvd
          where apinvd.seq = iSeq:
         
        /* account name */
        apinvd.acctname = cAcctName.
          
        /* account number */
        std-ha = getColumn(hBrowse,"acct").
        if valid-handle(std-ha) and changeMade(std-ha:screen-value,"acct","apinvd")
         then 
          assign
            lChanged = true
            apinvd.acct = std-ha:input-value
            .
      end.
      {&OPEN-QUERY-brwDistribution}
      
      /* prevent users from editting the browse */
      hBrowse:sensitive = false.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDistribution fInvoice
ON START-SEARCH OF brwDistribution IN FRAME fInvoice
DO:
  {lib/brw-startSearch-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fInvoice
ON CHOOSE OF bSave IN FRAME fInvoice /* Save */
DO:
  /* check if the invoice date is after today */
  if tInvoiceDate:input-value > today
   then
    do:
      std-lo = false.
      message "The invoice date is after today. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
  RUN SaveInvoice IN THIS-PROCEDURE (output std-lo).
  if std-lo
   then APPLY "END-ERROR":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bVendorLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bVendorLookup fInvoice
ON CHOOSE OF bVendorLookup IN FRAME fInvoice /* Vendor Lookup */
DO:
  define variable cVendorNbr as character no-undo.
  hide frame {&frame-name}.
  run sys/wvendordialog.w (output cVendorNbr).
  view frame {&frame-name}.
  if cVendorNbr > ""
   then 
    do:
      tVendor:screen-value in frame {&frame-name} = cVendorNbr.
      APPLY "RETURN":U TO tVendor.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbRefCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbRefCategory fInvoice
ON VALUE-CHANGED OF cmbRefCategory IN FRAME fInvoice /* Category */
DO:
  define variable lChangeMade as logical no-undo.
  
  lChangeMade = changeMade(self:screen-value,"refCategory","apinv").
  
  do with frame {&frame-name}:
    std-lo = true.
    if lChangeMade and can-find(first apinva where apinva.stat <> "P")
     then 
      do:
        message "Changing the category will also reset the approvals. Continue?" view-as alert-box warning buttons yes-no update std-lo as logical.
        lChanged = std-lo.
      end.
     else lChanged = lChangeMade or lChanged.
        
    if lChanged
     then
      do:
        lChanged = true.
        empty temp-table apinva.
        empty temp-table apinvd.
        cmbCoverage:hidden = true.
        if self:screen-value <> "Unknown"
         then
          case self:screen-value:
           when "E" then run MakeClaimLAE in this-procedure.
           when "L" then run MakeClaimLoss in this-procedure (0).
          end case.
        {&OPEN-BROWSERS-IN-QUERY-fInvoice}
      END.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME chkInContention
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL chkInContention wMain
ON VALUE-CHANGED OF chkInContention IN FRAME fInvoice /* Mark the Invoice in Contention */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"contention","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_File fInvoice
ON CHOOSE OF MENU-ITEM m_Delete_File /* Delete File */
DO:
  do with frame {&frame-name}:
    if tFile:screen-value <> ""
     then
      do:
        message "Document will be permanently removed. Continue?"
          view-as alert-box question buttons yes-no update std-lo.
        if not std-lo 
         then return.
        assign
          tFile:screen-value = ""
          lFileDeleted = false
          lChanged = true
          menu-item m_Delete_File:sensitive in menu menuFile = false
          .
      end.
    /* as the save button is disabled when the invoice is completed */
    /* we need to detach the invoice on the fly */
    if can-find(first apinv where stat = "C")
     then
      do:
        run DetachPayableDocument in hFileDataSrv (iInvoice, output pSuccess).
        pSuccess = true.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAmount fInvoice
ON LEAVE OF tAmount IN FRAME fInvoice /* Amount */
DO:
  
  std-lo = changeMade(self:screen-value,"amount","apinv").
  do with frame {&frame-name}:
    if std-lo
     then
      for each apinvd exclusive-lock:
        apinvd.amount = 0.0.
        if apinvd.seq = 1
         then apinvd.amount = tAmount:input-value.
      end.
    /* for a loss payment, change the approver as they may have changed
    if lChangeMade and cmbRefCategory:screen-value = "L"
     then
      do:
        define buffer apinva for apinva.
        /* check if there is a valid approval for the new amount */
        run GetClaimLossApprover in hFileDataSrv
                                       (tRefID:input-value, 
                                        cmbRefCategory:screen-value, 
                                        self:input-value, 
                                        OUTPUT std-ch).
        /* remove all people that haven't approved as they might not be able to approve */
        for each apinva exclusive-lock
           where apinva.stat <> "A":
          
          delete apinva.
        end.
        run AddApprovalToList in this-procedure (std-ch).
      end. */
  end.
  lChanged = std-lo.
  {&OPEN-QUERY-brwDistribution}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDateReceived
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDateReceived fInvoice
ON LEAVE OF tDateReceived IN FRAME fInvoice /* Date Received */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"dateReceived","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDueDate fInvoice
ON LEAVE OF tDueDate IN FRAME fInvoice /* Due Date */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"dueDate","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFile fInvoice
ON DROP-FILE-NOTIFY OF tFile IN FRAME fInvoice /* File */
DO:
  run FileUpload in this-procedure (self:get-dropped-file(1)).
  tFile:end-file-drop().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFile fInvoice
ON MOUSE-SELECT-DBLCLICK OF tFile IN FRAME fInvoice /* File */
DO:
  for first apinv no-lock:
    if apinv.apinvID > 0 and apinv.hasDocument
     then run ViewPayableDocument in hFileDataSrv (apinv.apinvID).
     else
      do:
       if tFile:screen-value > ""
        then run util/openfile.p (tFile:screen-value).
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tInvoiceDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tInvoiceDate fInvoice
ON LEAVE OF tInvoiceDate IN FRAME fInvoice /* Invoice Date */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"invoiceDate","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tInvoiceNbr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tInvoiceNbr fInvoice
ON LEAVE OF tInvoiceNbr IN FRAME fInvoice /* Invoice # */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"invoiceNumber","apinv")).
  lFileAdded = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes fInvoice
ON LEAVE OF tNotes IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"notes","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPONbr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPONbr fInvoice
ON LEAVE OF tPONbr IN FRAME fInvoice /* P.O. Number */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"PONumber","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRefID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRefID fInvoice
ON LEAVE OF tRefID IN FRAME fInvoice /* File Number */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"refID","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVendor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendor fInvoice
ON LEAVE OF tVendor IN FRAME fInvoice /* Payee Number */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"vendorID","apinv")).
  APPLY "RETURN":U TO tVendor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendor fInvoice
ON RETURN OF tVendor IN FRAME fInvoice /* Payee Number */
DO:
  run RefreshVendor in this-procedure (tVendor:screen-value).
  lFileAdded = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVendorName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVendorName fInvoice
ON LEAVE OF tVendorName IN FRAME fInvoice /* Payee Name */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"vendorName","apinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwApproval
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fInvoice 


/* ***************************  Main Block  *************************** */

/* set the title */
frame {&frame-name}:title = pTitle.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/brw-main-multi.i &browse-list="brwApproval,brwDistribution"}
publish "GetCredentialsID" (output cCurrUserID).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  run enable_UI in this-procedure.
  run Initialize in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddApproval fInvoice 
PROCEDURE AddApproval :
/*------------------------------------------------------------------------------
@description Add an approval 
------------------------------------------------------------------------------*/
  define variable pInputUserList as character no-undo.
  define variable pOutputUserList as character no-undo.
  define variable cReserveList as character no-undo.
  
  define buffer sysuser for sysuser.
  
  do with frame {&frame-name}:
    publish "GetClaimReserveApprover" (input pClaimID,
                                       input cmbRefCategory:screen-value,
                                       input tAmount:input-value,
                                       output cReserveList).
  end.
  publish "GetSysUsers" (output table sysuser).
  do std-in = 1 to num-entries(cReserveList) / 2:
    for first sysuser
        where sysuser.isActive = true
          and sysuser.uid = entry(std-in * 2,cReserveList):
       
      if can-find(first apinva where apinva.uid = sysuser.uid)
       then next.

      pInputUserList = addDelimiter(pInputUserList,",") + sysuser.name + "," + sysuser.uid.
    end.
  end.
  run sys/userselect.w (pInputUserList,output pOutputUserList).
  if pOutputUserList > ""
   then run AddApprovalToList in this-procedure (pOutputUserList).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddApprovalToList fInvoice 
PROCEDURE AddApprovalToList :
/*------------------------------------------------------------------------------
@description Sets the approvals for the invoice
@param UserList;character;The users for the approval list

@notes If there is no list passed in, set the first user to qualify to approve
------------------------------------------------------------------------------*/
  define input parameter pUserList as character no-undo.
  
  define variable cUserID as character no-undo.
  define variable cUserName as character no-undo.
  
  do with frame {&frame-name}:
    if pUserList = ""
     then
      do:
        /* set the approval list to be the assigned person */
        run GetClaimAssignedTo in hFileDataSrv (pClaimID, output std-ch).
        /* check if the person is already in the approval list */
        if not can-find(first apinva where lookup(uid,std-ch) > 0)
         then pUserList = std-ch.
         else pUserList = "".
      end.
     
    if pUserList > ""
     then
      do std-in = 1 to num-entries(pUserList) / 2:
        assign
          cUserID = entry(std-in * 2, pUserList)
          cUserName = entry(std-in * 2 - 1, pUserList)
          .
        /* remove the people that haven't approved yet that can not */
        if not can-find(first apinva where apinva.uid = cUserID)
         then
          do:
            iSeq = nextSeq(temp-table apinva:default-buffer-handle).
            create apinva.
            assign
              apinva.apinvID = iInvoice
              apinva.seq = iSeq
              apinva.stat = "P"
              apinva.amount = 0.0
              apinva.username = cUserName
              apinva.uid = cUserID
              apinva.role = "Approver"
              lChanged = true
              .
            release apinva.
          end.
      end.
    {&OPEN-QUERY-brwApproval}
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddDistribution fInvoice 
PROCEDURE AddDistribution :
/*------------------------------------------------------------------------------
@description Add a distribution
------------------------------------------------------------------------------*/
  /* check if department is added */
  do with frame {&frame-name}:
    if tVendor:screen-value = ""
     then
      do:
        MESSAGE "Add a vendor before adding a distribution" VIEW-AS ALERT-BOX INFO BUTTONS OK.
        return.
      end.
  end.
  
  std-in = nextSeq(temp-table apinvd:default-buffer-handle).
  create apinvd.
  assign
    apinvd.apinvID = iInvoice
    apinvd.seq = std-in
    iSeq = apinvd.seq
    std-in = countRows(temp-table apinvd:default-buffer-handle) - 1
    .
  
  reposition brwDistribution to row std-in.
  std-ha = browse brwDistribution:handle.
  if valid-handle(std-ha)
   then std-ha:insert-row("AFTER").
   
  /* allow users to edit the browse */
  browse brwDistribution:sensitive = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddDistributionToList fInvoice 
PROCEDURE AddDistributionToList :
/*------------------------------------------------------------------------------
@description Sets the distribution for the invoice
@param UserList;character;The users for the approval list
------------------------------------------------------------------------------*/
  define input parameter pAccountNumber as character no-undo.
  
  define variable pAccountName as character no-undo.
  define variable hBrowse as handle no-undo.
  
  do with frame {&frame-name}:
    /* get the next sequence number */
    std-in = nextSeq(temp-table apinvd:default-buffer-handle).
    if std-in = 1
     then std-de = tAmount:input-value.
     else std-de = 0.
    /* if the account number is blank, then get it based on the referring category */
    if pAccountNumber = ""
     then run GetClaimPayableAccount in hFileDataSrv (cmbRefCategory:screen-value,output pAccountNumber).
    publish "GetAccountName" (pAccountNumber, output pAccountName).
    if can-find(first apinvd where lookup(acct,pAccountNumber) > 0)
     then pAccountNumber = "".
    /* if the account number is blank, don't add the distribution */
    if pAccountNumber > ""
     then
      do:
        create apinvd.
        assign
          apinvd.apinvID = iInvoice
          apinvd.seq = std-in
          apinvd.acct = pAccountNumber
          apinvd.acctname = pAccountName
          apinvd.amount = std-de
          .
      end.
    {&OPEN-QUERY-brwDistribution}
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteApproval fInvoice 
PROCEDURE DeleteApproval :
/*------------------------------------------------------------------------------
@description Add 
------------------------------------------------------------------------------*/
  if not available apinva
   then return.
   
  delete apinva.
  lChanged = true.
  {&OPEN-QUERY-brwApproval}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteDistribution fInvoice 
PROCEDURE DeleteDistribution :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available apinvd
   then return.
   
  delete apinvd.
  lChanged = true.
  {&OPEN-QUERY-brwDistribution}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fInvoice  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fInvoice.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fInvoice  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY chkInContention tVendor tVendorName tVendorAddress cmbRefCategory 
          cmbCoverage tInvoiceNbr tPONbr tInvoiceDate tDateReceived tAmount 
          tDueDate tFile tNotes tApprovalNotes tRefID 
      WITH FRAME fInvoice.
  ENABLE chkInContention tVendor bVendorLookup tVendorName tVendorAddress 
         cmbRefCategory cmbCoverage tInvoiceNbr tPONbr tInvoiceDate 
         tDateReceived tAmount tDueDate bLock tFile bFileUpload tNotes bSave 
         bAddDistribution bDeleteDistribution bCancel bAddApproval 
         bDeleteApproval brwApproval brwDistribution tApprovalNotes tRefID 
         RECT-36 RECT-38 RECT-39 
      WITH FRAME fInvoice.
  VIEW FRAME fInvoice.
  {&OPEN-BROWSERS-IN-QUERY-fInvoice}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FileUpload fInvoice 
PROCEDURE FileUpload :
/*------------------------------------------------------------------------------
@description Upload a new file from the users hard drive  
------------------------------------------------------------------------------*/
  define input parameter pFile as character no-undo.

  do with frame {&frame-name}:
    /* only one file per invoice */
    if tFile:screen-value <> ""
     then
      do:
        /* confirmation */
        publish "GetConfirmFileUpload" (output std-lo).
        if std-lo 
         then
          do:
            MESSAGE "The current file will be replaced. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
            if not std-lo 
             then return.
          end.
      end.
    /* open the system dialog */
    if pFile = ""
     then 
      do:
        SYSTEM-DIALOG GET-FILE pFile 
          TITLE   "Attach Invoice to ShareFile..."
          MUST-EXIST 
          USE-FILENAME 
          UPDATE std-lo.
      end.
     else std-lo = true.
    /* update the widget and flag */
    if std-lo
     then 
      assign
        lFileAdded = true
        lChanged = true
        tFile:screen-value = pFile
        menu-item m_Delete_File:sensitive in menu menuFile = true
        .
    /* as the save button is disabled when the invoice is completed */
    /* we need to attach/replace the invoice on the fly */
    if can-find(first apinv where stat = "C")
     then 
      do:
        pSuccess = true.
        /* if there was a document previously */
        for first apinv:
          if not apinv.hasDocument
           then run AttachReceivableDocument in hFileDataSrv (apinv.apinvID, pFile, output pSuccess).
           else run ReplaceReceivableDocument in hFileDataSrv (table apinv, output pSuccess).
        end.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize fInvoice 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
@description Initialize the dialog by setting the images and getting the invoice data     
------------------------------------------------------------------------------*/
  define variable lIsApproved as logical no-undo initial false.

  define buffer apinv for apinv.
  /* set the save request flag to false */
  pSuccess = false.
  /* get the vendor information if available */
  for first apinv:
    iInvoice = apinv.apinvID.
    if apinv.vendorID > ""
     then run RefreshVendor in this-procedure (apinv.vendorID).
  end.
  /* create a dummy apinvID for the client to use if one was not found */
  if not can-find(first apinv)
   then
    do:
      create apinv.
      assign
        iInvoice = 0
        apinv.apinvID = iInvoice
        apinv.refID = string(pClaimID)
        apinv.refType = "C"
        apinv.dateReceived = today
        .
    end.
  do with frame {&frame-name}:
    /* enable the delete file if necessary */
    menu-item m_Delete_File:sensitive in menu menuFile = can-find(first apinv where hasDocument = true).
    /* The distribution browse */
    bAddDistribution:load-image("images/s-add.bmp").
    bAddDistribution:load-image-insensitive("images/s-add-i.bmp").
    bDeleteDistribution:load-image("images/s-delete.bmp").
    bDeleteDistribution:load-image-insensitive("images/s-delete-i.bmp").
    /* The approval browse */
    bAddApproval:load-image("images/s-add.bmp").
    bAddApproval:load-image-insensitive("images/s-add-i.bmp").
    bDeleteApproval:load-image("images/s-delete.bmp").
    bDeleteApproval:load-image-insensitive("images/s-delete-i.bmp").
    /* The vendor lookup button */
    bVendorLookup:load-image("images/s-lookup.bmp").
    bVendorLookup:load-image-insensitive("images/s-lookup-i.bmp").
    /* The lock/unlock button */
    bLock:load-image("images/lock.bmp").
    bLock:load-image-insensitive("images/lock-i.bmp").
    bLock:sensitive = true.
    /* The upload button */
    bFileUpload:load-image-up("images/s-upload.bmp").
    bFileUpload:load-image-insensitive("images/s-upload-i.bmp").
    /* set the fields */
    run SetFields in this-procedure.
    /* hide the uid column for the approval browse */
    std-ha = getColumn(browse brwApproval:handle,"uid").
    std-ha:visible = false.
    /* once all approved, don't allow the user to rename the vendor */
    if can-find(first apinv where stat = "A")
     then
      assign
        tVendorName:read-only = true
        tVendorAddress:read-only = true
        .
    /* if the invoice is already approved or denied, don't let the user change some stuff */
    if can-find(first apinva where stat = "A" or stat = "D")
     then Lock(true).
    /* once the invoice is complete or void, view only */
    if can-find(first apinv where stat = "C" or stat = "V")
     then LockAll().
    /* don't allow users to add distributions or approvals */
    SensitizeApproval(false).
    SensitizeDistribution(false).
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MakeClaimLAE fInvoice 
PROCEDURE MakeClaimLAE :
/*------------------------------------------------------------------------------
@description Changing the type to incidental ignores all approvals if the user
             can approve themselves
@note If the user can self approve the invoice, then it will automatically close
------------------------------------------------------------------------------*/
  /* a blank userlist means that the assigned to person will be the approver */
  if not can-find(first apinva)
   then run AddApprovalToList in this-procedure ("").
  
  if not can-find(first apinvd)
   then run AddDistributionToList in this-procedure ("").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MakeClaimLoss fInvoice 
PROCEDURE MakeClaimLoss :
/*------------------------------------------------------------------------------
@description Changing the type to loss lets the user choose the policy
------------------------------------------------------------------------------*/
  define input parameter pPolicyID as integer no-undo.
  define variable xEffDate as datetime no-undo.
  define variable xLiabilityAmount as decimal no-undo.
  define variable xGrossPremium as decimal no-undo.
  define variable xFormID as character no-undo.
  define variable xStateID as character no-undo.
  define variable xFileNumber as character no-undo.
  define variable xStatCode as character no-undo.
  define variable xNetPremium as decimal no-undo.
  define variable xRetention as decimal no-undo.
  define variable xCountyID as character no-undo.
  define variable xResidential as logical no-undo.
  define variable xInvoiceDate as date no-undo.

  define variable tMsgStat as character no-undo.
  define variable cList as character no-undo initial "".
  
  DO WITH FRAME {&frame-name}:
    if pPolicyID = 0
     then
      do:
        /* set the claim policy */
        run GetClaim in hFileDataSrv (output table claim).
        run GetClaimCoverages in hFileDataSrv (output table claimcoverage).
        for first claim:
          for each claimcoverage:
            publish "IsPolicyValid" (input pClaimID,
                                     input claimcoverage.coverageID,
                                     input claim.agentID,
                                     output xEffDate,
                                     output xLiabilityAmount,
                                     output xGrossPremium,
                                     output xFormID,
                                     output xStateID,
                                     output xFileNumber,
                                     output xStatCode,
                                     output xNetPremium,
                                     output xRetention,
                                     output xCountyID,
                                     output xResidential,
                                     output xInvoiceDate,
                                     output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                                     output std-ch).
            if tMsgStat = "S"
             then cList = addDelimiter(cList,",") + claimcoverage.coverageID.
          end.
        end.
      end.
     else cList = string(pPolicyID).
    if cList = ""
     then
      do:
        message "Please add a valid policy before making a Loss payment" view-as alert-box error buttons ok.
        if not can-find(first apinvd)
         then run AddDistributionToList in this-procedure ("").
        return.
      end.
    assign
      cmbCoverage:list-items = cList
      cmbCoverage:inner-lines = minimum(num-entries(cList),5)
      cmbCoverage:hidden = false
      cmbCoverage:sensitive = true
      cmbCoverage:screen-value = entry(1,cList)
      .
    IF INDEX(cList,",") = 0
     then cmbCoverage:sensitive = false.
    
    /* Change on 12/12/2016 to have the approver be the claim assigned to user */
    /* set the approval to everyone up to the limit
    std-ch = "".
    run GetClaimLossApprover in hFileDataSrv
                                   (input tRefID:input-value, 
                                    input cmbRefCategory:screen-value, 
                                    input tAmount:input-value, 
                                    OUTPUT std-ch).
    if std-ch > "" and not can-find(first apinva)
     then run AddApprovalToList in this-procedure (std-ch).
    /* add the distribution */
    if not can-find(first apinvd)
     then run AddDistributionToList in this-procedure (""). */
     
    /* making the claim an an LAE at this point will assign the invoice */
    /* to the claim assigned to user and add the default account */
    run MakeClaimLAE in this-procedure.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshVendor fInvoice 
PROCEDURE RefreshVendor :
/*------------------------------------------------------------------------------
@description Refresh the distribution rows 
------------------------------------------------------------------------------*/
  define input parameter pVendorNbr as character no-undo.
  
  std-lo = false.
  if pVendorNbr > ""
   then 
    do:
      define buffer vendor for vendor.
      define buffer apinv for apinv.
      publish "GetVendor" (pVendorNbr, output table vendor).
            
      std-lo = false.
      for first vendor no-lock:
        /* modify the screen values */
        do with frame {&frame-name}:
          assign
            cVendorName = vendor.name
            cVendorAddress = vendor.fulladdress
            tVendor:screen-value = vendor.vendorID
            tVendorName:screen-value = cVendorName
            tVendorName:sensitive = false
            tVendorAddress:screen-value = cVendorAddress
            tVendorAddress:sensitive = false
            std-lo = true
            .
          /* if there is an account */
          for first apinv no-lock:
            if apinv.vendorID <> pVendorNbr
             then 
              assign
                apinv.vendorID = vendor.vendorID
                lChanged = true
                .
          end.
        end.
      end.
    end.
    
  /* if no vendor, std-lo is false */
  if not std-lo
   then 
    assign
      tVendorName:sensitive = true
      tVendorName:read-only = false
      tVendorAddress:sensitive = true
      tVendorAddress:read-only = false
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ResetApproval fInvoice 
PROCEDURE ResetApproval :
/*------------------------------------------------------------------------------
@description Resets the approvals for the invoice
------------------------------------------------------------------------------*/
  /* reset the approvals */
  do with frame {&frame-name}:
    for each apinva exclusive-lock:
      assign
        apinva.stat = "P"
        apinva.dateActed = ?
        apinva.amount = 0.0
        apinva.notes = ""
        lChanged = true
        .
    end.
    for first apinv exclusive-lock:
      apinv.stat = "O".
    end.
    assign
      tApprovalNotes:screen-value = ""
      cmbCoverage:HIDDEN = TRUE
      .
    {&OPEN-QUERY-brwApproval}
  end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveInvoice fInvoice 
PROCEDURE SaveInvoice :
/*------------------------------------------------------------------------------
@description Saves the invoice
------------------------------------------------------------------------------*/
  define output parameter pSaveSuccess as logical no-undo.
  define buffer apinv for apinv.
  
  pSaveSuccess = false.
  /* check if there is a change to the invoice */
  if lChanged
   then
    do with frame {&frame-name}:
      /* Validation */
      run Validation in this-procedure (output std-lo).
      /* save the fields */
      if std-lo
       then 
        for first apinv exclusive-lock:
          assign
            apinv.refID = tRefID:screen-value
            apinv.refSeq = (if cmbCoverage:hidden then 0 else integer(cmbCoverage:screen-value))
            apinv.refCategory = cmbRefCategory:screen-value
            apinv.vendorID = tVendor:screen-value
            apinv.vendorName = tVendorName:screen-value
            apinv.vendorAddress = tVendorAddress:screen-value
            apinv.invoiceNumber = tInvoiceNbr:screen-value
            apinv.amount = tAmount:input-value
            apinv.deptID = ""
            apinv.notes = tNotes:screen-value
            apinv.PONumber = tPONbr:screen-value
            apinv.contention = logical(chkInContention:screen-value)
            apinv.isFileCreated = false
            apinv.isFileEdited = false
            apinv.isFileDeleted = false
            .
          /* if a file got added */
          if lFileAdded and tFile:screen-value <> ""
           then
            assign
              apinv.isFileCreated = not apinv.hasDocument
              apinv.isFileEdited = apinv.hasDocument
              apinv.isFileDeleted = false
              apinv.hasDocument = true
              apinv.documentFilename = tFile:screen-value
              .
          /* if a file got deleted */
          if lFileDeleted
           then
            assign
              apinv.isFileCreated = false
              apinv.isFileEdited = false
              apinv.isFileDeleted = true
              apinv.hasDocument = false
              apinv.documentFilename = ""
              .
          /* dates are optional */
          apinv.invoiceDate = tInvoiceDate:input-value.
          
          std-da = date(tDueDate:screen-value) no-error.
          if error-status:error
           then apinv.dueDate = ?.
           else apinv.dueDate = std-da.
           
          std-da = date(tDateReceived:screen-value) no-error.
          if error-status:error
           then apinv.dateReceived = today.
           else apinv.dateReceived = std-da.
          
          if apinv.refCategory = "Unknown"
           then apinv.refCategory = "".
          
          if iInvoice = 0
           then /* new */
            run NewPayableInvoice in hFileDataSrv
                                  (input table apinv,
                                   input table apinva,
                                   input table apinvd,
                                   output iInvoice,
                                   output pSaveSuccess
                                   ).
           else /* modify */
            run ModifyPayableInvoice in hFileDataSrv
                                     (input table apinv,
                                      input table apinva,
                                      input table apinvd,
                                      output pSaveSuccess
                                      ).
          lChanged = not pSaveSuccess.
        end.
    end.
   else pSaveSuccess = true.
  /* change the status */
  pSuccess = pSaveSuccess.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetFields fInvoice 
PROCEDURE SetFields :
/*------------------------------------------------------------------------------
@description Sets the fields during the initial load
------------------------------------------------------------------------------*/
  define variable cNotes as character no-undo.
  define variable iRefSeq as integer no-undo.
  define variable cCatList as character no-undo.
  define variable cType as character no-undo.
  
  define buffer apinv for apinv.
  define buffer apinva for apinva.
  
  run GetClaimType in hFileDataSrv (pClaimID, output cType).
  
  do with frame {&frame-name}:
    {lib/get-sysprop-list.i &combo=cmbRefCategory &appCode="'CLM'" &objAction="'PayableInvoice'" &objProperty="'Category'"}
    /* set the invoice fields */
    for first apinv:
      assign
        tInvoiceNbr:screen-value = apinv.invoiceNumber
        tVendor:screen-value = apinv.vendorID
        tVendorName:screen-value = apinv.vendorName
        tVendorAddress:screen-value = apinv.vendorAddress
        chkInContention:screen-value = string(apinv.contention)
        tInvoiceDate:screen-value = string(apinv.invoiceDate)
        tAmount:screen-value = string(apinv.amount)
        cmbRefCategory:screen-value = (if apinv.refCategory = "" then "E" else apinv.refCategory)
        cmbRefCategory:screen-value = (if cType = "M" then "E" else cmbRefCategory:screen-value)
        cmbRefCategory:sensitive = (not cType = "M")
        tNotes:screen-value = apinv.notes
        tDueDate:screen-value = string(apinv.dueDate)
        tDateReceived:screen-value = string(apinv.dateReceived)
        tPONbr:screen-value = apinv.PONumber
        tFile:screen-value = (if apinv.hasDocument then getPayableServerPath(apinv.vendorID,apinv.invoicedate) + apinv.documentFilename else "")
        tRefID:screen-value = apinv.refID
        iRefSeq = apinv.refSeq
        .
      if integer(apinv.refID) > 0
       then tRefID:sensitive = false.
    end.
    /* set the policy if the type is Loss */
    cmbCoverage:hidden = true.
    case cmbRefCategory:screen-value:
     when "L" then run MakeClaimLoss in this-procedure (iRefSeq).
     when "E" then run MakeClaimLAE in this-procedure.
    end case.
    /* set the approval notes */
    for each apinva, first apinv
       where apinva.apinvID = apinv.apinvID
         and (apinva.stat = "D" or apinva.stat = "A")
          by apinva.dateActed:
       
      std-ch = "".
      publish "GetSysUserName" (apinva.uid,output std-ch).
      assign
        std-ch = string(apinva.dateActed, "99/99/9999 HH:MM:SS") + " " +
                 (if apinva.stat = "D" then "REJECTED" else "APPROVED") + " " +
                 std-ch + ":" + chr(10) 
        std-ch = std-ch + apinva.notes
        cNotes = addDelimiter(cNotes,chr(10) + chr(10)) + std-ch
        .
    end.
    tApprovalNotes:screen-value = cNotes.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData fInvoice 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
@description Sorts the data in the browse widgets
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Validation fInvoice 
PROCEDURE Validation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE OUTPUT PARAMETER pSuccess AS LOGICAL NO-UNDO.
  DEFINE VARIABLE cMsg AS character NO-UNDO.
  
  define variable lHasDistribution as logical no-undo initial false.
  define variable lHasApproval as logical no-undo initial false.
  
  DEFINE BUFFER apinvd FOR apinvd.

  ASSIGN
    pSuccess = true
    cMsg = ""
    std-de = 0
    std-lo = FALSE
    .
  do with frame {&frame-name}:
    FOR EACH apinvd no-lock:
      ASSIGN
        std-de = std-de + apinvd.amount
        lHasDistribution = true
        .
    END.
     
    /* check for invoice date */
    if tInvoiceDate:input-value = ?
     then cMsg = cMsg + "The invoice date must be valid~n".
    
    /* check for an amount */
    if tAmount:input-value < 0
     then cMsg = cMsg + "The amount cannot be negative~n".
     
    /* check if the vendor name is populated */
    if tVendorName:screen-value = ""
     then cMsg = cMsg + "The vendor name must be provided~n".
     
    /* there must be at least one approver */
    IF countRows(temp-table apinva:default-buffer-handle) = 0
     THEN cMsg = cMsg + "There must be at least one approver~n".
    
    /* don't validate the approvals if the category is unknown */
    /*if lHasApproval and not cmbRefCategory:screen-value = "Unknown"
     then
      do:
        /* we need to make sure that there is at least one valid approver */
        if cmbRefCategory:screen-value = "L"
         then
          do:
            define variable lValidApprove as logical no-undo initial false.
            run GetClaimReserveApprover in hFileDataSrv
                                              (tRefID:input-value,
                                               cmbRefCategory:screen-value,
                                               tAmount:input-value,
                                               output std-ch).
            do std-in = 1 to num-entries(std-ch) / 2:
              lValidApprove = can-find(first apinva where apinva.uid = entry(std-in * 2,std-ch)).
              if lValidApprove
               then std-in = num-entries(std-ch) / 2.
            end.
            if not lValidApprove
             then
              do:
                define variable pUserList as character no-undo.
                message "Please choose a valid approver" view-as alert-box warning buttons ok.
                run sys/userselect.w (std-ch,output pUserList).
                if pUserList > ""
                 then run AddApprovalToList in this-procedure (pUserList).
              end.
          end.
      end.*/
    
    /* check if the distribution amount(s) equal the invoice amount */
    if tAmount:input-value > 0 and lHasDistribution and std-de <> tAmount:input-value
     then cMsg = cMsg + "The Distribution Amount(s) must equal Invoice Amount~n".
     
    /* the following checks are only for loss type */
    if cmbRefCategory:screen-value = "L"
     then
      do:
        /* check if the policy is choosen for a loss category */
        if cmbCoverage:hidden or cmbCoverage:screen-value = ""
         then cMsg = cMsg + "For a Loss payment, there must be a policy selected~n".
         else
          do:
            if cMsg = ""
             then
              do:
                run GetClaimPolicyLimit in hFileDataSrv (cmbCoverage:input-value, output std-de).
                /* if the liability amount is known, make sure the amount of */
                /* the invoice is not more than the liability amount against */
                /* all claims. If the amount is more, than warn the user to  */
                /* continue */
                if std-de > 0 and std-de < tAmount:input-value
                 then MESSAGE "The amount is more than the policy's liability amount. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL UPDATE pSuccess.
              end.
          end.
      end.
    
    
    if cMsg > ""
     then 
      do:
        cMsg = trim(cMsg,"~n").
        MESSAGE cMsg VIEW-AS ALERT-BOX INFO BUTTONS OK.
        pSuccess = false.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Lock fInvoice 
FUNCTION Lock RETURNS LOGICAL
  ( input pLock as logical ) :
/*------------------------------------------------------------------------------
@decsription Locks/Unlocks the widgets 
------------------------------------------------------------------------------*/
  if not pLock and can-find(first apinva where apinva.stat <> "P")
   then
    do:
      message "Unlocking the invoice will reset the approvals. Continue?" view-as alert-box warning buttons yes-no update lContinue as logical.
      /* reset the approvals */
      if lContinue
       then RUN ResetApproval IN THIS-PROCEDURE.
      /* if the user proceeds, we need pLock to be false */
      pLock = (pLock and lContinue).
      for first apinv exclusive-lock
          where pLock = true:
          
        apinv.stat = "O".
      end.
    end.
  
  do with frame {&frame-name}:
    assign
      tAmount:read-only = pLock
      tVendorName:read-only = pLock
      tVendorAddress:read-only = pLock
      cmbRefCategory:sensitive = not pLock
      lLocked = pLock
      .
    
    /* we need to change the image based on if the invoice is locked or unlocked */
    /* if the invoice is locked (pLock = true) then change the image to an unlock */
    if pLock
     then
      do:
        bLock:load-image("images/unlock.bmp").
        bLock:load-image-insensitive("images/unlock-i.bmp").
        bLock:tooltip = "Unlock the invoice".
      end.
     else
      do:
        bLock:load-image("images/lock.bmp").
        bLock:load-image-insensitive("images/lock-i.bmp").
        bLock:tooltip = "Lock the invoice".
      end.
  end.

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION LockAll fInvoice 
FUNCTION LockAll RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@decsription Locks all the widgets from editing
------------------------------------------------------------------------------*/
  define variable hBrowse as handle no-undo.
  do with frame {&frame-name}:
    assign
      /* make texts read only */
      tInvoiceNbr:read-only = true
      tVendor:read-only = true
      tVendorName:read-only = true
      tVendorAddress:read-only = true
      tInvoiceDate:read-only = true
      tAmount:read-only = true
      tNotes:read-only = true
      tDueDate:read-only = true
      tDateReceived:read-only = true
      tPONbr:read-only = true
      /* disable all others */
      cmbRefCategory:sensitive = false
      bAddDistribution:sensitive = false
      bDeleteDistribution:sensitive = false
      bAddApproval:sensitive = false
      bDeleteApproval:sensitive = false
      bVendorLookup:sensitive = false
      bLock:sensitive = false
      bSave:sensitive = false
      chkInContention:sensitive = false
      .
  end.
  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeApproval fInvoice 
FUNCTION SensitizeApproval RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&frame-name}:
    assign
      bAddApproval:SENSITIVE = pEnable
      bDeleteApproval:SENSITIVE = pEnable
      .
  END.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeDistribution fInvoice 
FUNCTION SensitizeDistribution RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  DO WITH FRAME {&frame-name}:
    assign
      bAddDistribution:SENSITIVE = pEnable
      bDeleteDistribution:SENSITIVE = pEnable
      .
  END.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeFileUpload fInvoice 
FUNCTION SensitizeFileUpload RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
@description Enables or disables the widgets to upload a file 
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      menu-item m_Delete_File:sensitive in menu menuFile = (pEnable and can-find(first apinv where hasDocument = true))
      bFileUpload:sensitive = pEnable
      tFile:screen-value = ""
      tFile:read-only = not pEnable
      tFile:drop-target = pEnable
      lFileAdded = not pEnable
      .
  end.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

