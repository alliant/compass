&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*---------------------------------------------------------------------
@name Lender Report
@description Generate a report for the specific Lender

@param 

@author John Oliver
@version 1.0
@created 2017/05/05
@notes 
---------------------------------------------------------------------*/

/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{lib/add-delimiter.i}
{tt/claimattr.i}
{tt/claimlenderreport.i}
{tt/period.i}
{tt/syscode.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAttribute tMonth tYear bGo bClear RECT-37 
&Scoped-Define DISPLAYED-OBJECTS tAttribute tMonth tYear 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON bGo 
     LABEL "Go" 
     SIZE 7.2 BY 1.71.

DEFINE VARIABLE tAttribute AS CHARACTER FORMAT "X(256)":U 
     LABEL "Attribute" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 40 BY 1 NO-UNDO.

DEFINE VARIABLE tMonth AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 18 BY 1
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 3.57.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tAttribute AT ROW 3.48 COL 12 COLON-ALIGNED WIDGET-ID 22
     tMonth AT ROW 2.19 COL 12 COLON-ALIGNED WIDGET-ID 24
     tYear AT ROW 2.19 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 26
     bGo AT ROW 2.43 COL 55.4 WIDGET-ID 16
     bClear AT ROW 2.43 COL 63.2 WIDGET-ID 20
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 4
     RECT-37 AT ROW 1.48 COL 2 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 72.8 BY 4.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Lender Report"
         HEIGHT             = 4.24
         WIDTH              = 72.8
         MAX-HEIGHT         = 38.81
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 38.81
         VIRTUAL-WIDTH      = 320
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Lender Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Lender Report */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME DEFAULT-FRAME /* Clear */
DO:
  do with frame {&frame-name}:
    /* reset the attribute */
    tAttribute:screen-value = entry(1,tAttribute:list-items).
    /* set the year value */
    std-ch = "".
    publish "GetCurrentValue" ("PeriodYear", output std-ch).
    if std-ch = "" 
     then std-ch = string(year(today)).
    tYear:screen-value = std-ch.
    /* set the month value */
    std-ch = "".
    publish "GetCurrentValue" ("PeriodMonth", output std-ch).
    if std-ch = "" 
     then std-ch = string(month(today)).
    tMonth:screen-value = std-ch.
    
    status input "" in window {&window-name}.
    status default "" in window {&window-name}.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME DEFAULT-FRAME /* Go */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* set the year list */
publish "GetPeriods" (output table period).
std-ch = "".
for each period break by period.periodYear:
  if not first-of(period.periodYear) 
   then next.
  std-ch = addDelimiter(std-ch,",") + string(period.periodYear).
end.
if std-ch = "" 
 then std-ch = string(year(today)).
tYear:list-items = std-ch.

/* set the attributes */
std-ch = "".
publish "GetClmAttrList" ("CA10", output std-ch).
if std-ch = ""
 then std-ch = "None".
assign
  tAttribute:list-items = std-ch
  tAttribute:screen-value = entry(1,tAttribute:list-items)
  .
apply "value-changed" to tAttribute.

status input "" in window {&window-name}.
status default "" in window {&window-name}.

bGo:load-image("images/completed.bmp").
bGo:load-image-insensitive("images/completed-i.bmp").
bClear:load-image("images/erase.bmp").
bClear:load-image-insensitive("images/erase-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  /* set the year value */
  std-ch = "".
  publish "GetCurrentValue" ("PeriodYear", output std-ch).
  if std-ch = "" 
   then std-ch = string(year(today)).
  tYear:screen-value = std-ch.
  
  /* set the month value */
  std-ch = "".
  publish "GetCurrentValue" ("PeriodMonth", output std-ch).
  if std-ch = "" 
   then std-ch = string(month(today)).
  tMonth:screen-value = std-ch.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAttribute tMonth tYear 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE tAttribute tMonth tYear bGo bClear RECT-37 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData Procedure 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
@description: Export the lender report data
------------------------------------------------------------------------------*/
  def var tFullFile as char no-undo.
  def var dStartTime as datetime no-undo.
  def var iPeriod as integer no-undo.
  def var tSuccess as logical init false no-undo.
  def var tMsg as char no-undo.
  
  def buffer claimlenderreport for claimlenderreport.
  def buffer period for period.

  status input "Exporting data..." in window {&window-name}.
  status default "Exporting data..." in window {&window-name}.

  do with frame {&frame-name}:
     
    for first period no-lock
        where period.periodMonth = tMonth:input-value
          and period.periodYear = tYear:input-value:
          
      iPeriod = period.periodID.
    end.
  
    empty temp-table claimlenderreport.
    tFullFile = "claim_lender_" + replace(string(today),"/","_").
    
    dStartTime = now.
    run server/getclaimlenderreport.p (iPeriod,
                                       output table claimlenderreport,
                                       output tSuccess,
                                       output tMsg).
     
    if not tSuccess
     then message "LenderReport failed: " tMsg view-as alert-box warning.
  end.
  
  std-in = 0.
  for each claimlenderreport no-lock:
    std-in = std-in + 1.
  end.
  
  std-ch = string(std-in) + " claim(s) found in " + trim(string(interval(now, dStartTime, "milliseconds") / 1000, ">>>,>>9.9")) + " seconds".
  status input std-ch in window {&window-name}.
  status default std-ch in window {&window-name}.
  
  if std-in > 0
   then run util/exporttocsvtable.p (temp-table claimlenderreport:default-buffer-handle, tFullFile).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

