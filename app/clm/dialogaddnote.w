&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
@name dialogaddnote.w
@description Add a note 

@param 

@author John Oliver
@version 1.0
@created 11/18/2016
@notes 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input parameter pClaimID     as integer no-undo.
define input parameter hFileDataSrv as handle  no-undo.

/* Local Variable Definitions ---                                       */
define variable lTimer as logical no-undo initial false.
{lib/std-def.i}
{lib/set-button-def.i}

/* Temp Tables ---                                                      */
{tt/claimnote.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSpellCheck cmbCategory tSubject tNote bSave ~
bCancel 
&Scoped-Define DISPLAYED-OBJECTS cmbCategory tSubject tNote 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSpellCheck 
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check Spelling".

DEFINE VARIABLE cmbCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tNote AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 161 BY 16
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 119 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bSpellCheck AT ROW 1.24 COL 166 WIDGET-ID 314
     cmbCategory AT ROW 1.62 COL 10 COLON-ALIGNED WIDGET-ID 2
     tSubject AT ROW 1.62 COL 44 COLON-ALIGNED WIDGET-ID 4
     tNote AT ROW 3.14 COL 12 NO-LABEL WIDGET-ID 8
     bSave AT ROW 19.57 COL 71.6 WIDGET-ID 12
     bCancel AT ROW 19.57 COL 88.4 WIDGET-ID 10
     "Note:" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 3.33 COL 6.2 WIDGET-ID 6
     SPACE(162.99) SKIP(17.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add Note" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       tNote:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Add Note */
DO:
  lTimer = true.
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  run DeleteNoteHistory.
  APPLY "WINDOW-CLOSE":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
DO:
  lTimer = true.
  run saveNote in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck Dialog-Frame
ON CHOOSE OF bSpellCheck IN FRAME Dialog-Frame /* Spell Check */
do:
  ASSIGN tNote.
  run util/spellcheck.p ({&window-name}:handle, input-output tNote, output std-lo).
  display tNote with frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

{lib/notestimer.i &entity="'Claim'" &entityID=pClaimID &notes="tNote:screen-value in frame {&frame-name}" &subject="tSubject:screen-value in frame {&frame-name}" &category="entry(lookup(cmbCategory:screen-value in frame {&frame-name}, cmbCategory:list-item-pairs in frame {&frame-name}) - 1, cmbCategory:list-item-pairs in frame {&frame-name})"}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/set-button.i &label="SpellCheck"  &image="images/spellcheck.bmp" &inactive="images/spellcheck-i.bmp"}
setButtons().

define variable tDefault as character no-undo.
publish "GetDefaultNoteCategory" (output tDefault).
{lib/get-sysprop-list.i &combo=cmbCategory &appCode="'CLM'" &objAction="'ClaimNote'" &objProperty="'Category'" &d=tDefault}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  do while true:
    wait-for "U1" of frame {&frame-name} pause 5.
    run SaveNoteHistory.
    if lTimer
     then leave.
  end.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cmbCategory tSubject tNote 
      WITH FRAME Dialog-Frame.
  ENABLE bSpellCheck cmbCategory tSubject tNote bSave bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveNote Dialog-Frame 
PROCEDURE saveNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    create claimnote.
    assign
      claimnote.claimID = pClaimID
      claimnote.category = cmbCategory:screen-value
      claimnote.subject = tSubject:screen-value
      claimnote.notes = tNote:screen-value
      claimnote.noteDate = now
      claimnote.noteType = "U"
      .
    release claimnote.
    std-lo = false.
    run NewClaimNote in hFileDataSrv (table claimnote, output std-lo).
    if std-lo
     then
      do:
        run CompleteNoteHistory.
        apply "WINDOW-CLOSE" to frame {&frame-name}.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

