&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

{tt/claim.i}
{tt/state.i}
{tt/agent.i}
{tt/claimlink.i}

def input  parameter pFromClaimID as int.
def input  parameter table for state.
def input  parameter table for agent.
def input  parameter table for claimlink.
def output parameter pToClaimID as int.
def output parameter pCancel as logical init true.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bLookupClaim tToClaimID tDescription ~
tAssignedTo tType tStat tState tAgent tFileNumber tInsuredName tSummary ~
Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tToClaimID tDescription tAssignedTo tType ~
tStat tState tAgent tFileNumber tInsuredName tSummary 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearClaim fMain 
FUNCTION clearClaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayClaim fMain 
FUNCTION displayClaim RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getAgentName fMain 
FUNCTION getAgentName RETURNS CHARACTER
  ( input pAgentID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getClaim fMain 
FUNCTION getClaim RETURNS LOGICAL
  ( input pClaimID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStateName fMain 
FUNCTION getStateName RETURNS CHARACTER
  ( input pStateID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bLookupClaim  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.8 BY 1.14 TOOLTIP "Lookup claim details".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tSummary AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE tAgent AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 100 BY 1 NO-UNDO.

DEFINE VARIABLE tAssignedTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 100 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent File Number" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tInsuredName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Name" 
     VIEW-AS FILL-IN 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tToClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "File Number to Link" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bLookupClaim AT ROW 1.91 COL 43.4 WIDGET-ID 366 NO-TAB-STOP 
     tToClaimID AT ROW 1.95 COL 27 COLON-ALIGNED WIDGET-ID 106
     tDescription AT ROW 4.1 COL 27 COLON-ALIGNED WIDGET-ID 390
     tAssignedTo AT ROW 5.29 COL 27 COLON-ALIGNED WIDGET-ID 346
     tType AT ROW 5.29 COL 86 COLON-ALIGNED WIDGET-ID 340
     tStat AT ROW 5.29 COL 111 COLON-ALIGNED WIDGET-ID 338
     tState AT ROW 6.48 COL 27 COLON-ALIGNED WIDGET-ID 370
     tAgent AT ROW 7.67 COL 27 COLON-ALIGNED WIDGET-ID 372
     tFileNumber AT ROW 8.86 COL 27 COLON-ALIGNED WIDGET-ID 42
     tInsuredName AT ROW 10.05 COL 27 COLON-ALIGNED WIDGET-ID 336
     tSummary AT ROW 11.24 COL 29 NO-LABEL WIDGET-ID 188
     Btn_OK AT ROW 15.29 COL 51
     Btn_Cancel AT ROW 15.29 COL 69
     "Summary:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 11.33 COL 19.2 WIDGET-ID 282
     SPACE(104.59) SKIP(5.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Link to Another File"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

ASSIGN 
       tAgent:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAssignedTo:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tDescription:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tFileNumber:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tInsuredName:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tStat:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tState:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tSummary:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tType:READ-ONLY IN FRAME fMain        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Link to Another File */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLookupClaim
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLookupClaim fMain
ON CHOOSE OF bLookupClaim IN FRAME fMain /* Lookup */
DO:
  clearClaim().
  
  std-lo = getClaim(tToClaimID:input-value).
  
  if std-lo = yes then
  displayClaim().
  else
  message  
    "Claim " + tToClaimID:screen-value + " is not valid."
    view-as alert-box.
  
  apply 'entry' to tToClaimID.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tToClaimID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tToClaimID fMain
ON RETURN OF tToClaimID IN FRAME fMain /* File Number to Link */
DO:
  apply 'choose' to bLookupClaim.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bLookupClaim:load-image("images/s-magnifier.bmp").
bLookupClaim:load-image-insensitive("images/s-magnifier-i.bmp").

frame {&frame-name}:title = "Create Link for Claim " + string(pFromClaimID).

RUN enable_UI.

MAIN-BLOCK:
repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  std-lo = getClaim(tToClaimID:input-value).
  if not std-lo then
  do:
    message 
      "Claim " + tToClaimID:screen-value + " is not valid."
      view-as alert-box.
    clearClaim().
    next.
  end.
  
  if pFromClaimID = tToClaimID:input-value then
  do:
    message 
      "Claim " + string(pFromClaimID) + " cannot be linked to itself."
      view-as alert-box.
    next.
  end.
  
  find first claimlink where claimlink.fromClaimID = pFromClaimID
                       and claimlink.toClaimID = tToClaimID:input-value
                       no-error.
  if avail claimlink then
  do:
    message 
      "Link to claim " + tToClaimID:screen-value + " already exists."
      view-as alert-box.
    next.
  end.

  assign
    pToClaimID = tToClaimID:input-value in frame fMain
    pCancel = false
    .
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tToClaimID tDescription tAssignedTo tType tStat tState tAgent 
          tFileNumber tInsuredName tSummary 
      WITH FRAME fMain.
  ENABLE bLookupClaim tToClaimID tDescription tAssignedTo tType tStat tState 
         tAgent tFileNumber tInsuredName tSummary Btn_OK Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearClaim fMain 
FUNCTION clearClaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
    assign
      tDescription:screen-value = ""
      tAssignedTo:screen-value = ""
      tType:screen-value = ""
      tStat:screen-value = ""
      tState:screen-value = ""
      tAgent:screen-value = ""
      tFileNumber:screen-value = ""
      tInsuredName:screen-value = ""
      tSummary:screen-value = ""
      .
  end.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayClaim fMain 
FUNCTION displayClaim RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
    
    find first claim no-error.
    
    if avail claim then
    do:
      publish "GetTypeDesc" (input claim.type, output tType).
      publish "GetStatDesc" (input claim.stat, output tStat).
      publish "GetSysUserName" (input claim.assignedTo, output tAssignedTo).
    
      assign
        tDescription = claim.description
        tState = getStateName(claim.stateID)
        tAgent = getAgentName(claim.agentID)
        tFileNumber = claim.fileNumber
        tInsuredName = claim.insuredName
        tSummary = claim.summary
        .
        
      display
        tDescription
        tAssignedTo
        tType
        tStat
        tState
        tAgent
        tFileNumber
        tInsuredName
        tSummary
        .
    end.
    else
    clearClaim().
  end.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getAgentName fMain 
FUNCTION getAgentName RETURNS CHARACTER
  ( input pAgentID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  find first agent where agent.agentID = pAgentID no-error.
  
  RETURN if avail agent 
         then replace(agent.name, ",", " ") + "  [" + 
              replace(agent.agentID, ",", " ") + "]"
         else "UNKNOWN".   

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getClaim fMain 
FUNCTION getClaim RETURNS LOGICAL
  ( input pClaimID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  def var tSuccess as logical no-undo.
  def var tMsg as char no-undo.
  
  empty temp-table claim.
  
  run server/getclaims.p (input pClaimID,
                          output table claim,
                          output tSuccess, 
                          output tMsg).
  
  if not tSuccess then
  do: 
    message "Error: " tMsg view-as alert-box warning.
    return false.     
  end.
  
  find first claim where claim.claimID = pClaimID no-error.
  if not available claim then 
  return false.

  RETURN true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStateName fMain 
FUNCTION getStateName RETURNS CHARACTER
  ( input pStateID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  find first state where state.stateID = pStateID no-error.
  
  RETURN if avail state then state.description else "UNKNOWN".   

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

