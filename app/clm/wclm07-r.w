&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  wclm07-r.w - Claim Load
  
  Author: y. Chin

  Created: Nov 04 2016

------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */


{tt/claimload.i}
{tt/claimload.i &tableAlias = ttclaimload}

{lib/std-def.i}

DEF VAR starttime AS DATETIME.
{lib/winlaunch.i}

 
/*DEF TEMP-TABLE ttDatacause LIKE ttData.
DEF TEMP-TABLE ttDatadesc LIKE ttData.*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttclaimload

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttclaimload.claimID ttclaimload.assignedToName ttclaimload.stagedesc ttclaimload.actiondesc ttclaimload.difficulty ttclaimload.urgency ttclaimload.causecode ttclaimload.desccode ttclaimload.lastnotedate ttclaimload.numclaimnote ttclaimload.summary   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH ttclaimload
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH ttclaimload.
&Scoped-define TABLES-IN-QUERY-brwData ttclaimload
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttclaimload


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cRefresh RECT-39 RECT-40 brwData 
&Scoped-Define DISPLAYED-OBJECTS cAssignedTo cStage cAction cDifficulty ~
cUrgency 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD filterData2 C-Win 
FUNCTION filterData2 RETURNS LOGICAL
  ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ManageControls C-Win 
FUNCTION ManageControls RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SetFilterValues C-Win 
FUNCTION SetFilterValues RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON cExport 
     LABEL "Export" 
     SIZE 7 BY 1.67 TOOLTIP "Export to Excel".

DEFINE BUTTON cRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7 BY 1.67 TOOLTIP "Run report".

DEFINE VARIABLE cAction AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cAssignedTo AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Assigned To" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE cDifficulty AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Difficulty" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE cStage AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Stage" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cUrgency AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Urgency" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-39
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 19 BY 2.62.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 156.4 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttclaimload SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttclaimload.claimID  column-label "File Number" format "X(10)"
ttclaimload.assignedToName column-label "Assigned To" format "x(100)" WIDTH 20
ttclaimload.stagedesc column-label "Stage" format "x(14)"
ttclaimload.actiondesc column-label "Action" format "x(14)"
ttclaimload.difficulty column-label "Difficulty" format "zzz9"
ttclaimload.urgency column-label "Urgency" format "zzz9"
ttclaimload.causecode COLUMN-LABEL "Company!Cause Codes"  FORMAT "x(50)" WIDTH 25
ttclaimload.desccode COLUMN-LABEL "Company!Description Codes" FORMAT "x(50)" WIDTH 25
ttclaimload.lastnotedate COLUMN-LABEL "Last!Activity" FORMAT "99/99/99" width 10
ttclaimload.numclaimnote COLUMN-LABEL "Notes!Count" FORMAT "x(7)"
ttclaimload.summary column-label "Summary" format "x(200)" WIDTH 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 217 BY 19.62 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cRefresh AT ROW 2.19 COL 4 WIDGET-ID 34 NO-TAB-STOP 
     cExport AT ROW 2.19 COL 12 WIDGET-ID 14
     cAssignedTo AT ROW 2.43 COL 33.6 COLON-ALIGNED WIDGET-ID 24
     cStage AT ROW 2.43 COL 77.6 COLON-ALIGNED WIDGET-ID 28
     cAction AT ROW 2.43 COL 103.6 COLON-ALIGNED WIDGET-ID 30
     cDifficulty AT ROW 2.43 COL 131.6 COLON-ALIGNED WIDGET-ID 38
     cUrgency AT ROW 2.43 COL 157.6 COLON-ALIGNED WIDGET-ID 40
     brwData AT ROW 4.52 COL 2 WIDGET-ID 200
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .95 AT ROW 1.24 COL 21.6 WIDGET-ID 18
     "Actions" VIEW-AS TEXT
          SIZE 8 BY .95 AT ROW 1.24 COL 3 WIDGET-ID 6
     RECT-39 AT ROW 1.71 COL 2 WIDGET-ID 2
     RECT-40 AT ROW 1.71 COL 20.6 WIDGET-ID 16
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 218.8 BY 23.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Claims Load"
         HEIGHT             = 23.33
         WIDTH              = 218.8
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData cUrgency fMain */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX cAction IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cAssignedTo IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cDifficulty IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON cExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cStage IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cUrgency IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttclaimload.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Claims Load */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Claims Load */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Claims Load */
DO:
  RUN windowResized IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAction C-Win
ON VALUE-CHANGED OF cAction IN FRAME fMain /* Action */
DO:
    cleardata().
    starttime = NOW.
    RUN filterData ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAssignedTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAssignedTo C-Win
ON VALUE-CHANGED OF cAssignedTo IN FRAME fMain /* Assigned To */
DO:
    cleardata().
    starttime = NOW.
    RUN filterData("").
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cDifficulty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cDifficulty C-Win
ON VALUE-CHANGED OF cDifficulty IN FRAME fMain /* Difficulty */
DO:
  cleardata().
    starttime = NOW.
    RUN filterData ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cExport C-Win
ON CHOOSE OF cExport IN FRAME fMain /* Export */
DO:
  RUN  ExportData IN this-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cRefresh C-Win
ON CHOOSE OF cRefresh IN FRAME fMain /* Go */
DO:
  RUN RefreshAction IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cStage
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cStage C-Win
ON VALUE-CHANGED OF cStage IN FRAME fMain /* Stage */
DO:
    cleardata().
    starttime = NOW.
    RUN filterData("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cUrgency
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cUrgency C-Win
ON VALUE-CHANGED OF cUrgency IN FRAME fMain /* Urgency */
DO:
  cleardata().
    starttime = NOW.
    RUN filterData ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
cRefresh:LOAD-IMAGE("images/completed.bmp").
cExport:LOAD-IMAGE("images/excel.bmp").
cExport:LOAD-IMAGE-INSENSITIVE("images/excel-i.bmp").

std-ch = "".
cAssignedTo:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = "ALL,ALL".

std-ch = "".
PUBLISH "GetStageList" (OUTPUT std-ch).

cStage:LIST-ITEM-PAIRS = "ALL,ALL" + (IF std-ch > "" THEN "," ELSE "") + std-ch.

std-ch = "".
PUBLISH "GetActionList" (OUTPUT std-ch).

cAction:LIST-ITEM-PAIRS = "ALL,ALL" + (IF std-ch > "" THEN "," ELSE "") + std-ch.

std-ch = "".
PUBLISH "GetDifficultyList" (OUTPUT std-ch).

cDifficulty:LIST-ITEM-PAIRS = "ALL,ALL" + (IF std-ch > "" THEN "," ELSE "") + std-ch.

std-ch = "".
PUBLISH "GetUrgencyList" (OUTPUT std-ch).

cUrgency:LIST-ITEM-PAIRS  = "ALL,ALL" + (IF std-ch > "" THEN "," ELSE "") + std-ch.

    cAssignedTo:SCREEN-VALUE IN FRAME {&FRAME-NAME}   = "ALL".  

    cStage:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    cAction:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    cDifficulty:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    cUrgency:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

/* PUBLISH "GetSysUsers" (OUTPUT TABLE tmpsysuser).  */

    SESSION:IMMEDIATE-DISPLAY = YES.

RUN windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cAssignedTo cStage cAction cDifficulty cUrgency 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE cRefresh RECT-39 RECT-40 brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
IF QUERY brwData:NUM-RESULTS = 0 THEN 
    DO:
      MESSAGE "There's nothing to export"
          VIEW-AS ALERT-BOX WARNING BUTTONS OK.
      RETURN.
    END.


&scoped-define ReportName "ClaimsLoad"

 publish "GetExportType" (output std-ch).
 
 if std-ch = "X" 
  then 
      run util/exporttoexcelbrowse.p (string(browse brwData:handle), {&ReportName}).
  else 
      run util/exporttocsvbrowse.p (string(browse brwData:handle), {&ReportName}).



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEF VAR cassigned AS CHAR.
DEF VAR cstagev AS CHAR.
DEF VAR cActionv AS CHAR.
DEF VAR cdifficultyv AS CHAR.
DEF VAR curgencyv AS CHAR.
DEF VAR cstagelistitemprs AS CHAR NO-UNDO.
DEF VAR cstagevaluepos AS INT NO-UNDO.
DEF VAR cstagelabelpos AS INT NO-UNDO.
DEF VAR cactionlistitemprs AS CHAR NO-UNDO.
DEF VAR cactionvaluepos AS INT NO-UNDO.
DEF VAR queryfilter AS CHAR NO-UNDO.


DEF VAR cstagelabel AS CHAR NO-UNDO.
DEF VAR cactionlabel AS CHAR NO-UNDO.
std-lo = TRUE.

cassigned = cAssignedTo:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
cstagev = cStage:SCREEN-VALUE IN FRAME {&FRAME-NAME}.

PUBLISH "GetStageDesc" (INPUT cStageV, OUTPUT cstagelabel).

/* ASSIGN                                          */
/*     cstagelistitemprs = cStage :LIST-ITEM-PAIRS */
/*     cstageValuepos = LOOKUP(cstagev,cstagelistitemprs)      */
/*     cstagelabelpos = cstagevaluepos - 1                     */
/*     cstagelabel = ENTRY(cstagelabelpos ,cstagelistitemprs). */
/*                                                             */
cActionv = cAction:SCREEN-VALUE IN FRAME {&FRAME-NAME}.

PUBLISH "GetActionDesc" (INPUT cActionV, OUTPUT cactionlabel).

cdifficultyv = cDifficulty:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
curgencyv = cUrgency:SCREEN-VALUE IN FRAME {&FRAME-NAME}.


/* ASSIGN                                                            */
/*     cactionlistitemprs = cAction:LIST-ITEM-PAIRS                  */
/*     cactionvaluepos = LOOKUP(cactionv,cactionlistitemprs)         */
/*     cactionlabel = entry(cactionvaluepos - 1,cactionlistitemprs). */

/*IF cassigned = "ALL" AND cstagev = "ALL" AND cActionv = "ALL" AND THEN*/
   /* DO: queryfilter = "".*/
/*        OPEN QUERY brwData PRESELECT EACH ttclaimload . */
/* END. */


EMPTY TEMP-TABLE ttclaimload.

FOR EACH claimload :
     
    CREATE ttclaimload.
    
    BUFFER-COPY claimload TO ttclaimload.
       
    PUBLISH "GetStageDesc" (INPUT claimload.stage, OUTPUT ttclaimload.stagedesc).
    PUBLISH "GetActionDesc" (INPUT claimload.action, OUTPUT ttclaimload.actiondesc).

END.


queryfilter = "".
IF cassigned <> "ALL"  THEN

  DO: 
      
      queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ") 
                                +  "ttclaimload.assignedTo = '" 
                                +   cAssigned 
                                + "' " .
  END.
         
IF cstagev <> "ALL"  THEN
  DO:
     queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ") 
                               + "ttclaimload.stageDesc = '" 
                               + cstagelabel 
                               + "' ".
    
  END.

IF cactionv <> "ALL" THEN
  DO:
     queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ") 

                               + "ttclaimload.actionDesc = '" 
                               + cactionlabel
                               + "' ".
  END.

IF cdifficultyv <> "ALL" THEN
  DO:
    queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ")
                              + "ttclaimload.difficulty = " 
                              +  cdifficultyv
                              + " ".

  END.

IF curgencyv <> "ALL" THEN
DO:
    queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ")
                              + "ttclaimload.urgency = "
                              + curgencyv
                              + " ".
END.
   
  

{lib/brw-sortData.i &pre-by-clause = "queryfilter + "} 


    std-ch = STRING(QUERY brwData:NUM-RESULTS) 
             + " records as of " + STRING(NOW,"99/99/9999 HH:MM:SS")
             + " (" + string(integer(interval(now, Starttime, "seconds"))) + " seconds elapsed)".

    HIDE MESSAGE NO-PAUSE.
    MESSAGE std-ch.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 CLOSE QUERY brwData.

 EMPTY TEMP-TABLE claimload.
 EMPTY TEMP-TABLE ttclaimload.

 /*EMPTY TEMP-TABLE ttData.*/
/* DEFINE BUFFER ttclaimload FOR ttclaimload.  */
 
 cAssignedTo:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    /* Stage: Set at design time */
 cStage:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    /* Action: Set at design time */
 cAction:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

 cDifficulty:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

 cUrgency:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

 HIDE MESSAGE NO-PAUSE.
 MESSAGE "Getting data...". 
starttime = NOW.
RUN SERVER/getclaimsload.p ( OUTPUT TABLE claimload,
                             OUTPUT std-lo,
                             OUTPUT std-ch ).

 IF NOT std-lo THEN
 DO:
    HIDE MESSAGE NO-PAUSE.
     MESSAGE std-ch.
    RETURN.
 END.

for each claimload no-lock:
  claimload.summary = trim(substring(claimload.summary,1,200)).
end.

RUN filterData("").
/* OPEN QUERY brwData PRESELECT EACH ttclaimload. */


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshAction C-Win 
PROCEDURE RefreshAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 /* clear screen */
 
  
  RUN getData IN THIS-PROCEDURE.
  
  IF NOT std-lo THEN RETURN.

  setFilterValues().
 
  manageControls().


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pName as char.
RUN filterData (pName).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  HIDE MESSAGE NO-PAUSE.
  CLOSE QUERY brwData.
  manageControls().

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION filterData2 C-Win 
FUNCTION filterData2 RETURNS LOGICAL
  ( ) :

/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

DEF VAR cassigned AS CHAR.
DEF VAR cstagev AS CHAR.
DEF VAR cActionv AS CHAR.

DEF VAR cstagelistitemprs AS CHAR NO-UNDO.
DEF VAR cstagevaluepos AS INT NO-UNDO.
DEF VAR cstagelabelpos AS INT NO-UNDO.
DEF VAR cactionlistitemprs AS CHAR NO-UNDO.
DEF VAR cactionvaluepos AS INT NO-UNDO.


DEF VAR cstagelabel AS CHAR NO-UNDO.
DEF VAR cactionlabel AS CHAR NO-UNDO.


cassigned = cAssignedTo:INPUT-VALUE IN FRAME {&FRAME-NAME}.
cstagev = cStage:INPUT-VALUE IN FRAME {&FRAME-NAME}.

ASSIGN 
    cstagelistitemprs = cStage :LIST-ITEM-PAIRS
   /* cstagev = cStage:INPUT-VALUE IN FRAME {&FRAME-NAME}*/
    cstageValuepos = LOOKUP(cstagev,cstagelistitemprs)
    cstagelabelpos = cstagevaluepos - 1
    cstagelabel = ENTRY(cstagelabelpos ,cstagelistitemprs).

cActionv = cAction:INPUT-VALUE IN FRAME {&FRAME-NAME}.

ASSIGN 
    cactionlistitemprs = cAction:LIST-ITEM-PAIRS
    cactionvaluepos = LOOKUP(cactionv,cactionlistitemprs)
    cactionlabel = entry(cactionvaluepos - 1,cactionlistitemprs).



/*IF cassigned = "ALL" AND cstagev = "ALL" AND cActionv = "ALL" THEN
    DO: queryfilter = "".
       OPEN QUERY brwData PRESELECT EACH ttData .
    
END.*/


/*queryfilter = "".
IF cassigned <> "ALL"  THEN

  DO: 
      
      queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ") 
                                +  "ttData.assignedTo = '" 
                                +   cAssigned 
                                + "' " .
  END.
         
IF cstagev <> "ALL"  THEN
  DO:
     queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ") 
                               + "ttData.stageDesc = '" 
                               + cstagelabel 
                               + "' ".
    
  END.

IF cactionv <> "ALL" THEN
  DO:
     queryfilter = queryfilter + (IF queryfilter = "" THEN "where " ELSE "and ") 

                               + "ttData.actionDesc = '" 
                               + cactionlabel
                               + "'".
  END.*/

 
 RUN sortdata.

        /* PRESELECT EACH ttData WHERE ttData.assignedTo = cassigned .*/

   /*   IF cassigned = "ALL" AND cstagev <> "ALL" AND cactionv = "ALL" THEN
          OPEN QUERY brwData
          PRESELECT EACH ttData WHERE ttData.stageDesc = cstagelabel .

      IF cassigned = "ALL" AND cstagev = "ALL" AND cactionv <> "ALL" THEN
          OPEN QUERY brwData
          PRESELECT EACH ttData WHERE ttData.actionDesc = cactionlabel.
      IF cassigned = "ALL" AND cstagev <> "ALL" AND cactionv <> "ALL" THEN
          OPEN QUERY brwData
          PRESELECT EACH ttData WHERE ttData.stageDesc = cstagelabel AND ttData.actionDesc = cactionlabel.
      IF cassigned <> "ALL" AND cstagev <> "ALL" AND cactionv = "ALL" THEN
          OPEN QUERY brwData
           PRESELECT EACH ttData WHERE ttData.assignedTo = cassigned AND ttData.stageDesc = cstagelabel.
      IF cassigned <> "ALL" AND cstagev = "ALL" AND cactionv <>  "ALL" THEN
          OPEN QUERY brwData
            PRESELECT EACH ttData WHERE ttData.assignedTo = cassigned AND ttData.actionDesc = cactionlabel.
      IF cassigned <> "ALL" AND cstagev <> "ALL" AND cactionv <> "ALL" THEN
          OPEN QUERY brwData
            PRESELECT EACH ttData WHERE ttData.assignedTo = cassigned 
                                    AND ttData.stageDesc = cstagelabel
                                    AND ttData.actionDesc = cactionlabel.*/



 
 /* END.*/

/*IF cAssignedTo:INPUT-VALUE IN FRAME {&FRAME-NAME} NE "ALL" THEN
OPEN QUERY brwData PRESELECT EACH ttData WHERE  ttData.assignedTo = cAssignedTo:INPUT-VALUE IN FRAME {&FRAME-NAME}.    
ELSE
  OPEN QUERY brwData PRESELECT EACH ttData.*/

  RETURN TRUE.   /* Function return value. */
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ManageControls C-Win 
FUNCTION ManageControls RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    
    ENABLE cExport WITH FRAME {&FRAME-NAME}.
    ENABLE cAssignedTo WITH FRAME {&FRAME-NAME}.
    ENABLE cStage WITH FRAME {&FRAME-NAME}. 
    ENABLE cAction WITH FRAME {&FRAME-NAME}.
    ENABLE cDifficulty WITH FRAME {&FRAME-NAME}.
    ENABLE cUrgency WITH FRAME {&FRAME-NAME}.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SetFilterValues C-Win 
FUNCTION SetFilterValues RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
 
 std-ch = "".
   
    FOR EACH ttclaimload NO-LOCK BREAK BY ttclaimload.assignedTo BY ttclaimload.assignedTo :
       
        IF FIRST-OF(ttclaimload.assignedTo) THEN
        DO:
            std-ch = IF CAPS(ttclaimload.assignedToName) <> "NOT FOUND" THEN std-ch + "," + ttclaimload.assignedToName + "," + ttclaimload.assignedTo ELSE std-ch.
        END.
    END.

    std-ch = TRIM(std-ch,",").
   
    cAssignedTo:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = "ALL,ALL," + std-ch.
    cAssignedTo:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    /* Stage: Set at design time */
    cStage:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    /* Action: Set at design time */
    cAction:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    cDifficulty:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".

    cUrgency:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "ALL".
    

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

