&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fAttribute
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fAttribute 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

define input parameter hFileDataSrv as handle no-undo.
define output parameter pChangeMade as log no-undo init false.

{lib/std-def.i}
{tt/claimattr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fAttribute
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES claimattr

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData claimattr.attrname claimattr.attrvalue   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH claimattr by claimattr.attrName
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH claimattr by claimattr.attrName.
&Scoped-define TABLES-IN-QUERY-brwData claimattr
&Scoped-define FIRST-TABLE-IN-QUERY-brwData claimattr


/* Definitions for DIALOG-BOX fAttribute                                */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fAttribute ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAttributeAdd bAttributeDelete brwData ~
bClose 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAttributeAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add attribute".

DEFINE BUTTON bAttributeDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete attribute".

DEFINE BUTTON bClose 
     LABEL "Close" 
     SIZE 15 BY 1.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      claimattr SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData fAttribute _FREEFORM
  QUERY brwData DISPLAY
      claimattr.attrname column-label "Name" format "x(20)" width 30
claimattr.attrvalue column-label "Value" format "X(100)" width 68
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 85 BY 14.29 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fAttribute
     bAttributeAdd AT ROW 1.57 COL 3 WIDGET-ID 294 NO-TAB-STOP 
     bAttributeDelete AT ROW 3.43 COL 3 WIDGET-ID 336 NO-TAB-STOP 
     brwData AT ROW 1.62 COL 11 WIDGET-ID 200
     bClose AT ROW 16.48 COL 42 WIDGET-ID 338
     SPACE(41.39) SKIP(0.61)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Claim Attributes"
         CANCEL-BUTTON bClose WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fAttribute
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData 1 fAttribute */
ASSIGN 
       FRAME fAttribute:SCROLLABLE       = FALSE
       FRAME fAttribute:HIDDEN           = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fAttribute       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fAttribute         = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH claimattr by claimattr.attrName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fAttribute
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAttribute fAttribute
ON WINDOW-CLOSE OF FRAME fAttribute /* Claim Attributes */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAttributeAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAttributeAdd fAttribute
ON CHOOSE OF bAttributeAdd IN FRAME fAttribute /* Add */
DO:
  run AddAttribute in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAttributeDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAttributeDelete fAttribute
ON CHOOSE OF bAttributeDelete IN FRAME fAttribute /* Delete */
DO:
  run DeleteAttribute in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClose fAttribute
ON CHOOSE OF bClose IN FRAME fAttribute /* Close */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData fAttribute
ON ROW-DISPLAY OF brwData IN FRAME fAttribute
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData fAttribute
ON START-SEARCH OF brwData IN FRAME fAttribute
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fAttribute 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/brw-main.i}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run Initialize in this-procedure.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddAttribute fAttribute 
PROCEDURE AddAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tCode as character no-undo.
  define variable tName as character no-undo.
  define variable tValue as character no-undo.

  run dialogclaimattradd.w (output tCode, output tName, output tValue, output std-lo).
  
  if std-lo
   then
    do:
      if can-find(first claimattr where attrCode = tCode)
       then message "Attribute already added" view-as alert-box warning buttons ok.
       else
        do:
          run NewClaimAttribute in hFileDataSrv (tCode, tName, tValue, output std-lo).
    
          if std-lo
           then
            do:
              create claimattr.
              assign
                claimattr.attrCode = tCode
                claimattr.attrName = tName
                claimattr.attrValue = tValue
                .
              release claimattr.
              {&OPEN-BROWSERS-IN-QUERY-fAttribute}
              
              pChangeMade = true.
            end.
        end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteAttribute fAttribute 
PROCEDURE DeleteAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available claimattr
   then return.
   
  run DeleteClaimAttribute in hFileDataSrv (claimattr.attrCode, output std-lo).
  
  if not std-lo
   then return.
   
  delete claimattr.
  {&OPEN-BROWSERS-IN-QUERY-fAttribute}

  pChangeMade = true.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fAttribute  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fAttribute.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fAttribute  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bAttributeAdd bAttributeDelete brwData bClose 
      WITH FRAME fAttribute.
  VIEW FRAME fAttribute.
  {&OPEN-BROWSERS-IN-QUERY-fAttribute}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize fAttribute 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    bAttributeAdd:load-image("images/add.bmp").
    bAttributeAdd:load-image-insensitive("images/add-i.bmp").
    bAttributeDelete:load-image("images/delete.bmp").
    bAttributeDelete:load-image-insensitive("images/delete-i.bmp").
  end.
  run GetClaimAttributes in hFileDataSrv (output table claimattr).
  {&OPEN-BROWSERS-IN-QUERY-fAttribute}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData fAttribute 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

