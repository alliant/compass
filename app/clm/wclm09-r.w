&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wclm01-r.w
@desc Management Summary report
@author D.Sinclair   
@date 12.28.2016
 */


CREATE WIDGET-POOL.

{lib/std-def.i}
{tt/syscode.i &tableAlias="altarisk"}
{tt/syscode.i &tableAlias="altaresp"}
{tt/syscode.i &tableAlias="desccode"}
{tt/syscode.i &tableAlias="causecode"}
{tt/clm01.i}
{tt/agent.i}
{tt/state.i}

def var hData as handle no-undo.

{lib/winlaunch.i}

/* Do these here to avoid a non-fully displayed screen */
publish "GetCodes" ("ClaimAltaRisk", output table altarisk).
publish "GetCodes" ("ClaimAltaResp", output table altaresp).
publish "GetCodes" ("ClaimDescription", output table desccode).
publish "GetCodes" ("ClaimCause", output table causecode).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES clm01

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData clm01.claimIDDesc clm01.assignedTo clm01.stat clm01.laeReserve clm01.lossReserve clm01.laeLTD clm01.lossLTD clm01.dateOpened clm01.lastNoteDate clm01.type clm01.stage clm01.action clm01.litigation clm01.significant clm01.agentError clm01.searcherError clm01.attorneyError clm01.altaRiskDesc clm01.altaResponsibilityDesc clm01.descriptionCode clm01.causeCode clm01.summary   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH clm01 by clm01.claimID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH clm01 by clm01.claimID.
&Scoped-define TABLES-IN-QUERY-brwData clm01
&Scoped-define FIRST-TABLE-IN-QUERY-brwData clm01


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bRefresh fStat fState fAgent fUser 
&Scoped-Define DISPLAYED-OBJECTS fStat fState fAgent 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Run report".

DEFINE VARIABLE fAgent AS CHARACTER INITIAL "ALL" 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 92 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE fUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE fStat AS CHARACTER INITIAL "O" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Open", "O",
"Closed", "C",
"Both", "OC"
     SIZE 12 BY 2.14 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 139 BY 3.1.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 12 BY 3.1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      clm01 SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      clm01.claimIDDesc column-label "File Number" format "x(10)"
clm01.assignedTo column-label "Assigned To" format "x(100)" width 20
clm01.stat column-label "Status" format "x(7)"
clm01.laeReserve column-label "LAE Reserve!Balance" format "->>>,>>>,>>9.99"
clm01.lossReserve column-label "Loss Reserve!Balance" format "->>>,>>>,>>9.99"
clm01.laeLTD column-label "LAE Paid!LTD" format "->>>,>>>,>>9.99"
clm01.lossLTD column-label "Loss Paid!LTD" format "->>>,>>>,>>9.99"
clm01.dateOpened column-label "Date!Opened" format "99/99/99"
clm01.lastNoteDate column-label "Last!Activity" format "99/99/99"
clm01.type column-label "Type" format "x(7)"
clm01.stage column-label "Stage" format "x(14)"
clm01.action column-label "Action" format "x(14)"
clm01.litigation column-label "Lit" format "Yes/No" width 4
clm01.significant column-label "Sig" format "Yes/No" width 4
clm01.agentError column-label "Agent!Error" format "x(10)"
clm01.searcherError column-label "Searcher!Error" format "x(10)"
clm01.attorneyError column-label "Attorney!Error" format "x(10)"
clm01.altaRiskDesc column-label "ALTA Risk" format "x(40)" width 25
clm01.altaResponsibilityDesc column-label "ALTA Responsibility" format "x(40)" width 25
clm01.descriptionCode column-label "Company Desc Code(s)" format "x(120)" width 40
clm01.causeCode column-label "Company Cause Code(s)" format "x(120)" width 40
clm01.summary column-label "Summary" format "x(200)" width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 235 BY 17.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 4.81 COL 2 WIDGET-ID 200
     bExport AT ROW 2.19 COL 142.8 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 2.19 COL 131 WIDGET-ID 4 NO-TAB-STOP 
     fStat AT ROW 1.95 COL 118 NO-LABEL WIDGET-ID 86
     fState AT ROW 1.95 COL 12 COLON-ALIGNED WIDGET-ID 82
     fAgent AT ROW 3.14 COL 12 COLON-ALIGNED WIDGET-ID 84
     fUser AT ROW 1.95 COL 71 COLON-ALIGNED WIDGET-ID 92
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 56
     "Action" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 1.14 COL 141.4 WIDGET-ID 64
     "Status:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.95 COL 110.4 WIDGET-ID 90
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-38 AT ROW 1.48 COL 140.4 WIDGET-ID 62
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 237.2 BY 21.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Reserves Worksheet"
         HEIGHT             = 21.62
         WIDTH              = 237.2
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX fUser IN FRAME fMain
   NO-DISPLAY                                                           */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-38 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH clm01 by clm01.claimID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Reserves Worksheet */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Reserves Worksheet */
DO:
  /* This event will close the window and terminate the procedure.  */

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Reserves Worksheet */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if available clm01
   then
    do: 
        publish "ClaimSelected" (string(clm01.claimID)).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
  if clm01.laeReserve < 0 
   then clm01.laeReserve:fgcolor in browse brwData = 12.
   else clm01.laeReserve:fgcolor in browse brwData = ?.
  if clm01.lossReserve < 0 
   then clm01.lossReserve:fgcolor in browse brwData = 12.
   else clm01.lossReserve:fgcolor in browse brwData = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain /* Agent */
DO:
  clearData().  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  clearData().
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fUser C-Win
ON VALUE-CHANGED OF fUser IN FRAME fMain /* Assigned To */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

{lib/get-state-list.i &combo=fState &addAll=true}

publish "GetClmUsersList" (",", "ALL", "ALL", output std-ch).
fUser:list-item-pairs in frame {&frame-name} = std-ch.

/*
publish "GetCredentialsID" (output std-ch).
if lookup(std-ch, fUser:list-item-pairs) > 0 
 then fUser:screen-value = std-ch.
 else fUser:screen-value = "ALL".*/
fUser:screen-value = "ALL".

/*  std-ch = "".                                           */
/* publish "GetAgentList" (true, "", output std-ch).       */
/* fAgent:list-item-pairs in frame {&frame-name} = std-ch. */

/* Solved the stack issue : Field too large for a data item. Try to increase -s*/

session:immediate-display = yes.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  /* create the combos */
  {lib/get-state-list.i &combo=fState &addAll=true}
  {lib/get-agent-list.i &combo=fAgent &state=fState &addAll=true}
  /* set the combos */
  {lib/set-current-value.i &state=fState &agent=fAgent}
  
  run windowResized.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fStat fState fAgent 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData bRefresh fStat fState fAgent fUser 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 &scoped-define ReportName "ReservesWorksheet"

 publish "GetExportType" (output std-ch).
 if std-ch = "X" 
  then run util/exporttoexcelbrowse.p (string(browse brwData:handle), {&ReportName}).
  else run util/exporttocsvbrowse.p (string(browse brwData:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer clm01 for clm01.
 def var tFile as char no-undo.
 def buffer agent for agent.
 def var tStartTime as datetime.

 if fAgent:screen-value in frame {&frame-name} = "ALL"
   and fState:screen-value in frame {&frame-name} = "None" 
  then
   do: 
       MESSAGE "Please select a State or an Agent"
           VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 hide message no-pause.
 message "Getting data...".

 close query brwData.
 empty temp-table clm01.

 bExport:sensitive in frame {&frame-name} = false.

 tStartTime = now.
 run server/getclaimsummary.p (input fState:screen-value in frame fMain,
                               input fAgent:screen-value in frame fMain,
                               input ?,  /* No cutoff restriction */
                               input fUser:screen-value in frame fMain,
                               input fStat:screen-value in frame fMain, /* stat */
                               output table clm01,
                               output std-lo,
                               output std-ch).

 if not std-lo 
  then 
   do: hide message no-pause.
       message std-ch.
       return.
   end.

 publish "GetAgents" (output table agent).

 /* Set fields used during export for more complete data */
 for each clm01:
  clm01.yearOpened = year(clm01.dateOpened) no-error.
  clm01.claimIDDesc = string(clm01.claimID).
  
  publish "GetSysUserName" (input clm01.assignedTo, output clm01.assignedTo).
  publish "GetStatDesc" (input clm01.stat, output clm01.stat).
  publish "GetTypeDesc" (input clm01.type, output clm01.type).
  publish "GetStageDesc" (input clm01.stage, output clm01.stage).
  publish "GetActionDesc" (input clm01.action, output clm01.action).
  publish "GetAgentErrorDesc" (input clm01.agentError, output clm01.agentError).
  publish "GetSearcherErrorDesc" (input clm01.searcherError, output clm01.searcherError).
  publish "GetAttorneyErrorDesc" (input clm01.attorneyError, output clm01.attorneyError).
  
  find agent
    where agent.agentID = clm01.agentID no-error.
  if available agent 
   then clm01.agentName = agent.name.
  
  find altarisk
    where altarisk.code = clm01.altaRiskCode no-error.
  if available altarisk 
   then clm01.altariskdesc = trim(clm01.altaRiskCode) + ". " + trim(altarisk.description).
  
  find altaresp
    where altaresp.code = clm01.altaResponsibilityCode no-error.
  if available altaresp 
   then clm01.altaresponsibilitydesc = trim(clm01.altaResponsibilityCode) + ". " + trim(altaresp.description).
  
  clm01.summary = trim(substring(clm01.summary,1,200)).
 end.

 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("claimIDDesc").
 
 std-ch = string(query brwData:num-results) 
    + " records  as of " + string(now,"99/99/99 HH:MM AM") 
    + " (" + string(integer(interval(now, tStartTime, "seconds"))) + " seconds)".
 
 hide message no-pause.
 message std-ch.

 bExport:sensitive in frame {&frame-name} = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  hide message no-pause.
  close query brwData.
  clear frame fMain.
  bExport:sensitive = false.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

