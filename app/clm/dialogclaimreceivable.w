&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fInvoice 
/*---------------------------------------------------------------------
@name dialogclaimreceivable.w
@action 
@description Dialog for creating/modifying/viewing a claim payable

@param ClaimID;int;The claim ID
@param Arinv;complex;Table for the invoice
@param FileDataSrv;handle;The handle to the filedatasrv.p procedure
@param Title;char;The title for the window
@param Success;logical;True if the invoice was saved


@author John Oliver
@version 1.0
@created 04/27/2017
---------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib/account-repository.i}
{lib/change-made.i}
{lib/std-def.i}
{tt/arinv.i}

/* Parameters Definitions ---                                           */
define input parameter pClaimID as integer no-undo.
define input parameter table for arinv.
define input parameter hFileDataSrv as handle no-undo.
define input parameter pTitle as character no-undo.
define output parameter pSuccess as logical no-undo.

/* Local Variable Definitions ---                                       */
define variable lChanged as logical no-undo initial false.
define variable lLocked as logical no-undo initial false.
define variable lFileAdded as logical no-undo initial false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fInvoice

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tRefID tName tAddr1 RECT-40 tAddr2 RECT-41 ~
tCity tState tZip tContact tPhone tEmail tRefCategory tInvoiceNumber ~
tAmount tDateRequested tDueDate tFile bFileUpload tNotes bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tRefID tName tAddr1 tAddr2 tCity tState ~
tZip tContact tPhone tEmail tRefCategory tInvoiceNumber tAmount ~
tDateRequested tDueDate tFile tNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Menu Definitions                                                     */
DEFINE MENU menuFile 
       MENU-ITEM m_Delete_File  LABEL "Delete File"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bFileUpload 
     LABEL "Upload" 
     SIZE 4.8 BY 1.14 TOOLTIP "Upload an invoice document".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tRefCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Receivable Type" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Other","O"
     DROP-DOWN-LIST
     SIZE 53 BY 1 TOOLTIP "The referring category of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(256)" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","",
                     "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Colorado","CO",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY"
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 69.2 BY 5.43
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 63 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 63 BY 1 NO-UNDO.

DEFINE VARIABLE tAmount AS DECIMAL FORMAT "->>,>>>,>>9.99":U INITIAL 0 
     LABEL "Amount" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 TOOLTIP "The amount of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 TOOLTIP "City" NO-UNDO.

DEFINE VARIABLE tContact AS CHARACTER FORMAT "X(256)":U 
     LABEL "Contact" 
     VIEW-AS FILL-IN 
     SIZE 63 BY 1 NO-UNDO.

DEFINE VARIABLE tDateRequested AS DATE FORMAT "99/99/9999":U 
     LABEL "Date Requested" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 TOOLTIP "The date of when Alliant National received the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/9999":U 
     LABEL "Due Date" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 TOOLTIP "The due date of the invoice"
     FONT 1 NO-UNDO.

DEFINE VARIABLE tEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 39 BY 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 63.2 BY 1 TOOLTIP "The path of the invoice document within ShareFile" DROP-TARGET NO-UNDO.

DEFINE VARIABLE tInvoiceNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "Invoice Number" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 63 BY 1 NO-UNDO.

DEFINE VARIABLE tPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tRefID AS INTEGER FORMAT "99999999":U INITIAL 0 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 15.6 BY 1 TOOLTIP "Zipcode" NO-UNDO.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 76 BY 8.57.

DEFINE RECTANGLE RECT-41
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 76 BY 5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fInvoice
     tRefID AT ROW 1.48 COL 13 COLON-ALIGNED WIDGET-ID 192
     tName AT ROW 3.86 COL 12 COLON-ALIGNED WIDGET-ID 6
     tAddr1 AT ROW 5.05 COL 12 COLON-ALIGNED WIDGET-ID 8
     tAddr2 AT ROW 6.24 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     tCity AT ROW 7.43 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 170
     tState AT ROW 7.43 COL 41 COLON-ALIGNED NO-LABEL WIDGET-ID 174
     tZip AT ROW 7.43 COL 59 COLON-ALIGNED NO-LABEL WIDGET-ID 176
     tContact AT ROW 8.62 COL 12 COLON-ALIGNED WIDGET-ID 186
     tPhone AT ROW 9.81 COL 12 COLON-ALIGNED WIDGET-ID 188
     tEmail AT ROW 9.81 COL 36 COLON-ALIGNED WIDGET-ID 190
     tRefCategory AT ROW 12.91 COL 22 COLON-ALIGNED WIDGET-ID 28
     tInvoiceNumber AT ROW 14.1 COL 22 COLON-ALIGNED WIDGET-ID 198
     tAmount AT ROW 14.1 COL 55 COLON-ALIGNED WIDGET-ID 182
     tDateRequested AT ROW 15.29 COL 22 COLON-ALIGNED WIDGET-ID 56
     tDueDate AT ROW 15.29 COL 55 COLON-ALIGNED WIDGET-ID 14
     tFile AT ROW 17.43 COL 8 COLON-ALIGNED WIDGET-ID 80
     bFileUpload AT ROW 17.33 COL 74 WIDGET-ID 82
     tNotes AT ROW 18.86 COL 10 NO-LABEL WIDGET-ID 30
     bSave AT ROW 24.57 COL 25
     bCancel AT ROW 24.62 COL 42
     "Contact Information" VIEW-AS TEXT
          SIZE 22.6 BY .62 AT ROW 2.67 COL 4 WIDGET-ID 4
          FONT 6
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 18.86 COL 3 WIDGET-ID 32
     "Invoice Information" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 11.71 COL 4 WIDGET-ID 180
          FONT 6
     RECT-40 AT ROW 2.91 COL 3 WIDGET-ID 2
     RECT-41 AT ROW 11.95 COL 3 WIDGET-ID 178
     SPACE(1.99) SKIP(9.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE ""
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fInvoice
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fInvoice:SCROLLABLE       = FALSE
       FRAME fInvoice:HIDDEN           = TRUE.

ASSIGN 
       tFile:POPUP-MENU IN FRAME fInvoice       = MENU menuFile:HANDLE.

ASSIGN 
       tNotes:RETURN-INSERTED IN FRAME fInvoice  = TRUE.

ASSIGN 
       tRefID:READ-ONLY IN FRAME fInvoice        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fInvoice fInvoice
ON WINDOW-CLOSE OF FRAME fInvoice
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileUpload
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileUpload fInvoice
ON CHOOSE OF bFileUpload IN FRAME fInvoice /* Upload */
DO:
  run FileUpload in this-procedure ("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fInvoice
ON CHOOSE OF bSave IN FRAME fInvoice /* Save */
DO:
  /* check if the invoice date is after today */
  if tDateRequested:input-value > today
   then
    do:
      std-lo = false.
      message "The invoice date is after today. Continue?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
  run SaveInvoice in this-procedure (output std-lo).
  if std-lo
   then APPLY "END-ERROR":U TO FRAME {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_File fInvoice
ON CHOOSE OF MENU-ITEM m_Delete_File /* Delete File */
DO:
  do with frame {&frame-name}:
    if tFile:screen-value <> ""
     then
      do:
        message "Document will be permanently removed. Continue?"
          view-as alert-box question buttons yes-no update std-lo.
        if not std-lo 
         then return.
        assign
          tFile:screen-value = ""
          lFileAdded = false
          lChanged = true
          menu-item m_Delete_File:sensitive in menu menuFile = false
          .
      end.
    /* as the save button is disabled when the invoice is completed */
    /* we need to detach the invoice on the fly */
    if can-find(first arinv where stat = "C")
     then
      for first arinv no-lock:
        run DetachPayableDocument in hFileDataSrv (arinv.arinvID, output pSuccess).
        pSuccess = true.
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAddr1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr1 fInvoice
ON LEAVE OF tAddr1 IN FRAME fInvoice /* Address */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"addr1","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAddr2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr2 fInvoice
ON LEAVE OF tAddr2 IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"addr2","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAmount fInvoice
ON LEAVE OF tAmount IN FRAME fInvoice /* Amount */
DO:
  lChanged = changeMade(self:screen-value,"requestedAmount","arinv").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCity fInvoice
ON LEAVE OF tCity IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"city","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContact fInvoice
ON LEAVE OF tContact IN FRAME fInvoice /* Contact */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"contactName","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDateRequested
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDateRequested fInvoice
ON LEAVE OF tDateRequested IN FRAME fInvoice /* Date Requested */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"dateRequested","arinv")).
  apply "RETURN" to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDateRequested fInvoice
ON RETURN OF tDateRequested IN FRAME fInvoice /* Date Requested */
DO:
  do with frame {&frame-name}:
    tDueDate:screen-value = string(add-interval(self:input-value,1,"months")).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDueDate fInvoice
ON LEAVE OF tDueDate IN FRAME fInvoice /* Due Date */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"dueDate","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEmail fInvoice
ON LEAVE OF tEmail IN FRAME fInvoice /* Email */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"contactEmail","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFile fInvoice
ON DROP-FILE-NOTIFY OF tFile IN FRAME fInvoice /* File */
DO:
  run FileUpload in this-procedure (self:get-dropped-file(1)).
  tFile:end-file-drop().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFile fInvoice
ON MOUSE-SELECT-DBLCLICK OF tFile IN FRAME fInvoice /* File */
DO:
  for first arinv no-lock:
    if arinv.arinvID > 0 and arinv.hasDocument
     then run ViewReceivableDocument in hFileDataSrv (arinv.arinvID).
     else
      do:
       if tFile:screen-value > "" and search(tFile:screen-value) <> ?
        then run util/openfile.p (tFile:screen-value).
      end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tInvoiceNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tInvoiceNumber fInvoice
ON LEAVE OF tInvoiceNumber IN FRAME fInvoice /* Invoice Number */
DO:
  lChanged = changeMade(self:screen-value,"invoiceNumber","arinv").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName fInvoice
ON LEAVE OF tName IN FRAME fInvoice /* Name */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"name","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes fInvoice
ON LEAVE OF tNotes IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"notes","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPhone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPhone fInvoice
ON LEAVE OF tPhone IN FRAME fInvoice /* Phone */
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"contactPhone","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRefCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRefCategory fInvoice
ON VALUE-CHANGED OF tRefCategory IN FRAME fInvoice /* Receivable Type */
DO:
  lChanged = changeMade(self:screen-value,"refCategory","arinv").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState fInvoice
ON VALUE-CHANGED OF tState IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"state","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tZip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tZip fInvoice
ON LEAVE OF tZip IN FRAME fInvoice
DO:
  lChanged = (lChanged OR changeMade(self:screen-value,"zip","arinv")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fInvoice 


/* ***************************  Main Block  *************************** */

/* set the title */
frame {&frame-name}:title = pTitle.

if entry(1,pTitle," ") = "Copy"
 then lChanged = true.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  run Initialize in this-procedure.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fInvoice  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fInvoice.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fInvoice  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tRefID tName tAddr1 tAddr2 tCity tState tZip tContact tPhone tEmail 
          tRefCategory tInvoiceNumber tAmount tDateRequested tDueDate tFile 
          tNotes 
      WITH FRAME fInvoice.
  ENABLE tRefID tName tAddr1 RECT-40 tAddr2 RECT-41 tCity tState tZip tContact 
         tPhone tEmail tRefCategory tInvoiceNumber tAmount tDateRequested 
         tDueDate tFile bFileUpload tNotes bSave bCancel 
      WITH FRAME fInvoice.
  VIEW FRAME fInvoice.
  {&OPEN-BROWSERS-IN-QUERY-fInvoice}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FileUpload fInvoice 
PROCEDURE FileUpload :
/*------------------------------------------------------------------------------
@description Upload a new file from the users hard drive  
------------------------------------------------------------------------------*/
  define input parameter pFile as character no-undo.

  do with frame {&frame-name}:
    /* only one file per invoice */
    if tFile:screen-value <> ""
     then
      do:
        /* confirmation */
        publish "GetConfirmFileUpload" (output std-lo).
        if std-lo 
         then
          do:
            MESSAGE "The current file will be replaced. Continue?" VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "Confirmation" UPDATE std-lo.
            if not std-lo 
             then return.
          end.
      end.
    /* open the system dialog */
    if pFile = ""
     then 
      do:
        SYSTEM-DIALOG GET-FILE pFile 
          TITLE   "Attach Invoice to ShareFile..."
          MUST-EXIST 
          USE-FILENAME 
          UPDATE std-lo.
      end.
     else std-lo = true.
    /* update the widget and flag */
    if std-lo
     then 
      assign
        lFileAdded = true
        lChanged = true
        tFile:screen-value = pFile
        menu-item m_Delete_File:sensitive in menu menuFile = true
        .
    /* as the save button is disabled when the invoice is completed */
    /* we need to attach/replace the invoice on the fly */
    if can-find(first arinv where stat = "C")
     then 
      do:
        pSuccess = true.
        /* if there was a document previously */
        for first arinv:
          if not arinv.hasDocument
           then run AttachReceivableDocument in hFileDataSrv (arinv.arinvID, pFile, output pSuccess).
           else run ReplaceReceivableDocument in hFileDataSrv (table arinv, output pSuccess).
        end.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize fInvoice 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
@description Initializes the window
------------------------------------------------------------------------------*/
  /* create a new receivable if one isn't available */
  if not can-find(first arinv)
   then
    do:
      create arinv.
      assign
        arinv.arinvID = 0
        arinv.refID = string(pClaimID)
        arinv.refType = "C"
        arinv.stat = "O"
        .
    end.
    
  do with frame {&frame-name}:
    /* set the category */
    std-ch = "".
    publish "GetReceivableCategoryList" (false, output std-ch).
    if std-ch > ""
     then tRefCategory:list-item-pairs = std-ch.
    /* The upload button */
    bFileUpload:load-image-up("images/s-upload.bmp").
    bFileUpload:load-image-insensitive("images/s-upload-i.bmp").
    for first arinv no-lock:
      /* get the claim's state */
      std-ch = "".
      if valid-handle(hFileDataSrv)
       then run GetClaimState in hFileDataSrv (integer(arinv.refID), output std-ch).
       else std-ch = "".
       
      assign
        /* enable the delete file if necessary */
        menu-item m_Delete_File:sensitive in menu menuFile = arinv.hasDocument
        tRefID:screen-value = string(arinv.refID)
        tName:screen-value = arinv.name
        tAddr1:screen-value = arinv.addr1
        tAddr2:screen-value = arinv.addr2
        tCity:screen-value = arinv.city
        tState:screen-value = (if arinv.stateID = "" then std-ch else arinv.stateID)
        tZip:screen-value = arinv.zipcode
        tContact:screen-value = arinv.contactName
        tPhone:screen-value = arinv.contactPhone
        tEmail:screen-value = arinv.contactEmail
        tRefCategory:screen-value = (if arinv.refCategory = "" then "O" else arinv.refCategory)
        tInvoiceNumber:screen-value = arinv.invoiceNumber
        tAmount:screen-value = string(arinv.requestedAmount)
        tDateRequested:screen-value = (if arinv.dateRequested <> ? then string(arinv.dateRequested) else string(today))
        tDueDate:screen-value = string(arinv.dueDate)
        tNotes:screen-value = string(arinv.notes)
        /* determine if the boxes should be enabled or not */
        std-lo = (arinv.stat <> "O")
        tName:read-only = std-lo
        tAddr1:read-only = std-lo
        tAddr2:read-only = std-lo
        tCity:read-only = std-lo
        tState:sensitive = not std-lo
        tZip:read-only = std-lo
        tContact:read-only = std-lo
        tPhone:read-only = std-lo
        tEmail:read-only = std-lo
        tRefCategory:sensitive = not std-lo
        tInvoiceNumber:read-only = std-lo
        tAmount:read-only = std-lo
        tDateRequested:read-only = std-lo
        tDueDate:read-only = std-lo
        tNotes:read-only = std-lo
        .
      if not std-lo
       then apply "RETURN" to tDateRequested.
       
      if arinv.hasDocument
       then
        if search(arinv.documentFilename) = ?
         then tFile:screen-value = getReceivableServerPath(arinv.dateRequested) + arinv.documentFilename.
         else tFile:screen-value = arinv.documentFilename.
      
      apply "ENTRY" to tName.
    end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveInvoice fInvoice 
PROCEDURE SaveInvoice :
/*------------------------------------------------------------------------------
@description Saves the receivable
------------------------------------------------------------------------------*/
  define output parameter pSaveSuccess as logical no-undo.
  define buffer arinv for arinv.
  
  if lChanged
   then   
    do with frame {&frame-name}:
      if tDateRequested:input-value = ?
       then 
        do:
          message "The date requested must be valid" view-as alert-box information buttons ok.
          pSaveSuccess = false.
        end.
       else
        for first arinv exclusive-lock:
          assign
            arinv.refID = tRefID:input-value
            arinv.name = tName:input-value
            arinv.addr1 = tAddr1:input-value
            arinv.addr2 = tAddr2:input-value
            arinv.city = tCity:input-value
            arinv.stateID = tState:input-value
            arinv.zipcode = tZip:input-value
            arinv.contactName = tContact:input-value
            arinv.contactPhone = tPhone:input-value
            arinv.contactEmail = tEmail:input-value
            arinv.refCategory = tRefCategory:input-value
            arinv.invoiceNumber = tInvoiceNumber:input-value
            arinv.requestedAmount = tAmount:input-value
            arinv.dateRequested = tDateRequested:input-value
            arinv.dueDate = tDueDate:input-value
            arinv.notes = tNotes:input-value
            .
          /* if a file got added */
          if tFile:screen-value <> ""
           then
            assign
              arinv.isFileCreated = not arinv.hasDocument
              arinv.isFileEdited = arinv.hasDocument
              arinv.isFileDeleted = false
              arinv.hasDocument = true
              arinv.documentFilename = tFile:screen-value
              .
          /* if a file got deleted */
          if arinv.hasDocument and tFile:screen-value = ""
           then
            assign
              arinv.isFileCreated = false
              arinv.isFileEdited = false
              arinv.isFileDeleted = true
              arinv.hasDocument = false
              arinv.documentFilename = ""
              .
          if arinv.arinvID = 0
           then /* new */
            run NewReceivableInvoice in hFileDataSrv
                                     (input table arinv,
                                      output pSaveSuccess
                                      ).
           else /* modify */
            run ModifyReceivableInvoice in hFileDataSrv
                                        (input table arinv,
                                         output pSaveSuccess
                                         ).
          lChanged = not pSaveSuccess.
        end.
    end.
   /* if nothing was changed, the save was still successful */
   else pSaveSuccess = true.
  pSuccess = pSaveSuccess.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

