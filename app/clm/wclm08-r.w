&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  Author: YSChin

  Created: 

--------------------------------------------------------------------------*/
CREATE WIDGET-POOL.


{lib/std-def.i}
/* {tt/claim.i &tableAlias = tempclaim}  */
{tt/claim.i}
{tt/claimcoverage.i}
{tt/claimproperty.i}
{tt/claimcontact.i}
{tt/claimnote.i}
{tt/claimLink.i}
{tt/claimadjreq.i}
{tt/claimadjtrx.i}
{tt/apinv.i}
{tt/aptrx.i}

DEF VAR iclaimid AS INT NO-UNDO.
DEF VAR pSuccess AS LOG NO-UNDO.
DEF VAR chkOverview AS LOG NO-UNDO.
DEF VAR chkCoverage AS LOG NO-UNDO.
DEF VAR chkProperty AS LOG NO-UNDO.
DEF VAR chkContact AS LOG NO-UNDO.
DEF VAR chkAccount AS LOG NO-UNDO.
DEF VAR chkRecovery AS LOG NO-UNDO.
DEF VAR chkLitigation AS LOG NO-UNDO.
DEF VAR chkLinkedClaims AS LOG NO-UNDO.
DEF VAR chkActivity AS LOG NO-UNDO.
DEF VAR chkNotes AS LOG NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-49 RClaimID 
&Scoped-Define DISPLAYED-OBJECTS RClaimID cOverview cCoverage cProperty ~
cContact cAccount cRecovery cLitigation cLinked cActivity cNotes cAllTabs 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ResetOptions C-Win 
FUNCTION ResetOptions RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD WindowReset C-Win 
FUNCTION WindowReset RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View claim summary as PDF".

DEFINE VARIABLE RClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65 BY 2.86.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 13 BY 2.86.

DEFINE RECTANGLE RECT-49
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65 BY 4.29.

DEFINE VARIABLE cAccount AS LOGICAL INITIAL no 
     LABEL "Accounting" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 NO-UNDO.

DEFINE VARIABLE cActivity AS LOGICAL INITIAL no 
     LABEL "Activity" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cAllTabs AS LOGICAL INITIAL no 
     LABEL "All" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cContact AS LOGICAL INITIAL no 
     LABEL "Contacts" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cCoverage AS LOGICAL INITIAL no 
     LABEL "Coverage" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cLinked AS LOGICAL INITIAL no 
     LABEL "Linked Claims" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

DEFINE VARIABLE cLitigation AS LOGICAL INITIAL no 
     LABEL "Litigation" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cNotes AS LOGICAL INITIAL no 
     LABEL "Notes" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cOverview AS LOGICAL INITIAL yes 
     LABEL "Overview" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cProperty AS LOGICAL INITIAL no 
     LABEL "Properties" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE cRecovery AS LOGICAL INITIAL no 
     LABEL "Recovery" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bPrint AT ROW 2.24 COL 70.6 WIDGET-ID 14 NO-TAB-STOP 
     RClaimID AT ROW 2.67 COL 19 COLON-ALIGNED WIDGET-ID 6
     cOverview AT ROW 5.76 COL 6 WIDGET-ID 74
     cCoverage AT ROW 5.76 COL 22 WIDGET-ID 68
     cProperty AT ROW 5.76 COL 36 WIDGET-ID 70
     cContact AT ROW 5.76 COL 50 WIDGET-ID 78
     cAccount AT ROW 6.71 COL 6 WIDGET-ID 80
     cRecovery AT ROW 6.71 COL 22 WIDGET-ID 82
     cLitigation AT ROW 6.71 COL 36 WIDGET-ID 84
     cLinked AT ROW 6.71 COL 50 WIDGET-ID 86
     cActivity AT ROW 7.67 COL 6 WIDGET-ID 88
     cNotes AT ROW 7.67 COL 22 WIDGET-ID 90
     cAllTabs AT ROW 7.67 COL 36 WIDGET-ID 92
     "Action" VIEW-AS TEXT
          SIZE 6.6 BY .71 AT ROW 1.29 COL 69 WIDGET-ID 12
     "Parameter" VIEW-AS TEXT
          SIZE 10 BY .95 AT ROW 1.24 COL 4 WIDGET-ID 4
     "Options" VIEW-AS TEXT
          SIZE 8 BY .95 AT ROW 4.57 COL 5 WIDGET-ID 76
     RECT-38 AT ROW 1.71 COL 67.6 WIDGET-ID 62
     RECT-36 AT ROW 1.71 COL 3 WIDGET-ID 54
     RECT-49 AT ROW 5.05 COL 3 WIDGET-ID 72
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 82.6 BY 9 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Claim Summary"
         HEIGHT             = 9
         WIDTH              = 82.6
         MAX-HEIGHT         = 47
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47
         VIRTUAL-WIDTH      = 384
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON bPrint IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cAccount IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cActivity IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cAllTabs IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cContact IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cCoverage IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cLinked IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cLitigation IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cNotes IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cOverview IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cProperty IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX cRecovery IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-38 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Claim Summary */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Claim Summary */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Claim Summary */
DO:
 
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
    IF rClaimID:SCREEN-VALUE IN FRAME fMain = "" 
   OR rClaimID:SCREEN-VALUE IN FRAME fMain = "0" 
THEN
DO:
    MESSAGE "ClaimID cannot be blank or zero"
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
    RETURN.
END.

DEF VAR starttime AS DATETIME NO-UNDO.
starttime = now.
HIDE MESSAGE NO-PAUSE.
MESSAGE "Generating PDF...".
    
    run printData in this-procedure.
    HIDE MESSAGE NO-PAUSE.
    MESSAGE STRING(INTEGER(INTERVAL(NOW, starttime, "seconds"))) + " seconds".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAllTabs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAllTabs C-Win
ON VALUE-CHANGED OF cAllTabs IN FRAME fMain /* All */
DO:
/*     cOverview:CHECKED IN FRAME {&FRAME-NAME} = (cOverview:CHECKED). */

    DO WITH FRAME {&FRAME-NAME}:
        ASSIGN 
           std-lo = cAllTabs:CHECKED
           cOverview:CHECKED = TRUE
           cCoverage:CHECKED = std-lo
           cProperty:CHECKED = std-lo
           cContact:CHECKED = std-lo
           cAccount:CHECKED = std-lo
           cRecovery:CHECKED = std-lo
           cLitigation:CHECKED = std-lo
           cLinked:CHECKED = std-lo
           cActivity:CHECKED = std-lo
           cNotes:CHECKED = std-lo
           .
    END.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RClaimID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RClaimID C-Win
ON ENTRY OF RClaimID IN FRAME fMain /* File Number */
DO:
    
    ENABLE bPrint WITH FRAME {&FRAME-NAME}.
    ENABLE cOverview WITH FRAME {&FRAME-NAME}.
    ENABLE cCoverage WITH FRAME {&FRAME-NAME}.
    ENABLE cProperty WITH FRAME {&FRAME-NAME}.
    ENABLE cContact WITH FRAME {&FRAME-NAME}.
    ENABLE cAccount WITH FRAME {&FRAME-NAME}.
    ENABLE cRecovery WITH FRAME {&FRAME-NAME}.
    ENABLE cLitigation WITH FRAME {&FRAME-NAME}.
    ENABLE cLinked WITH FRAME {&FRAME-NAME}.
    ENABLE cActivity WITH FRAME {&FRAME-NAME}.
    ENABLE cNotes WITH FRAME {&FRAME-NAME}.
    ENABLE cAllTabs WITH FRAME {&FRAME-NAME}.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RClaimID C-Win
ON LEFT-MOUSE-CLICK OF RClaimID IN FRAME fMain /* File Number */
DO:
  HIDE MESSAGE NO-PAUSE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RClaimID C-Win
ON VALUE-CHANGED OF RClaimID IN FRAME fMain /* File Number */
DO:
    
    ResetOptions().
    WindowReset().    
    HIDE MESSAGE NO-PAUSE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */


{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.


/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bPrint:LOAD-IMAGE("images/pdf.bmp").
bPrint:LOAD-IMAGE-INSENSITIVE("images/pdf-i.bmp").

DISABLE cOverview WITH FRAME {&FRAME-NAME}.
DISABLE cCoverage WITH FRAME {&FRAME-NAME}.
DISABLE cProperty WITH FRAME {&FRAME-NAME}.
DISABLE cContact WITH FRAME {&FRAME-NAME}.
DISABLE cAccount WITH FRAME {&FRAME-NAME}.
DISABLE cRecovery WITH FRAME {&FRAME-NAME}.
DISABLE cLitigation WITH FRAME {&FRAME-NAME}.
DISABLE cLinked WITH FRAME {&FRAME-NAME}.
DISABLE cActivity WITH FRAME {&FRAME-NAME}.
DISABLE cNotes WITH FRAME {&FRAME-NAME}.

cOverview:CHECKED IN FRAME {&FRAME-NAME}= TRUE. 

SESSION:IMMEDIATE-DISPLAY = YES.

/* APPLY "value-changed" TO cOverview IN FRAME {&FRAME-NAME}. */
/* MESSAGE "Overview :" cOverview:SCREEN-VALUE IN FRAME {&FRAME-NAME} VIEW-AS ALERT-BOX. */
/* MESSAGE "Coverage :" cCoverage:SCREEN-VALUE IN FRAME {&FRAME-NAME} VIEW-AS ALERT-BOX. */
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY RClaimID cOverview cCoverage cProperty cContact cAccount cRecovery 
          cLitigation cLinked cActivity cNotes cAllTabs 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-49 RClaimID 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
WindowReset().

EMPTY TEMP-TABLE claim.

ASSIGN 
iclaimID = INT(rClaimID:SCREEN-VALUE IN FRAME fMain)
chkOverview = LOGICAL(cOverview:SCREEN-VALUE)
chkCoverage = LOGICAL(cCoverage:SCREEN-VALUE)
chkProperty = LOGICAL(cProperty:SCREEN-VALUE)
chkContact = LOGICAL(cContact:SCREEN-VALUE)
chkAccount = LOGICAL(cAccount:SCREEN-VALUE)
chkRecovery = LOGICAL(cRecovery:SCREEN-VALUE)
chkLitigation = LOGICAL(cLitigation:SCREEN-VALUE)
chkLinkedClaims = LOGICAL(cLinked:SCREEN-VALUE)
chkActivity = LOGICAL(cActivity:SCREEN-VALUE)
chkNotes = LOGICAL(cNotes:SCREEN-VALUE)
.

/* RUN SERVER/getclaims.p(INPUT iclaimID,     */
/*                        OUTPUT TABLE claim, */
/*                        OUTPUT pSuccess,    */
/*                        OUTPUT pMsg).       */

/*IF NOT pSuccess 
 THEN
  DO:
    MESSAGE "claimID not found!"
    VIEW-AS ALERT-BOX WARNING.
    RETURN.
END.*/

/* FIND FIRST claim WHERE claim.claimID = iclaimID NO-ERROR. */
/* IF NOT AVAILABLE claim THEN                               */
/* DO:                                                       */
/*    MESSAGE "There's nothing to print"                     */
/*       VIEW-AS ALERT-BOX WARNING BUTTONS OK.               */
/*    RETURN.                                                */
/* END.                                                      */

/* HIDE MESSAGE NO-PAUSE.                                    */
/*     MESSAGE "ClaimId: " + string(claim.claimID).          */
/*     MESSAGE "Stage: " + claim.stage.                      */
/*     MESSAGE "Date Created: " + string(claim.dateCreated). */
/*     MESSAGE "PMsg: " + pMsg.                              */
/*                                                           */

 
RUN clm08-r.p (iclaimID, chkOverview, 
               chkCoverage, chkProperty, 
               chkContact, chkAccount, 
               chkRecovery, chkLitigation, 
               chkLinkedClaims, chkActivity,
               chkNotes,
               OUTPUT pSuccess).

IF NOT pSuccess THEN 
  DO:
    RClaimID:SCREEN-VALUE IN FRAME fMain = "".
    WindowReset().
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ResetOptions C-Win 
FUNCTION ResetOptions RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    cOverview:CHECKED IN FRAME {&FRAME-NAME} = (cOverview:CHECKED).
    cCoverage:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cProperty:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cContact:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cAccount:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cRecovery:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cLitigation:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cLinked:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cActivity:CHECKED IN FRAME {&FRAME-NAME} = FALSE.
    cNotes:CHECKED IN FRAME {&FRAME-NAME} = FALSE.  
    cAllTabs:CHECKED IN FRAME {&FRAME-NAME} = FALSE.


  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION WindowReset C-Win 
FUNCTION WindowReset RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  {&WINDOW-NAME}:TITLE = "Claim Details" + IF STRING(rClaimID:SCREEN-VALUE IN FRAME fMain) <> "" THEN " - " + STRING(rClaimID:SCREEN-VALUE IN FRAME fMain) ELSE "". 

  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

