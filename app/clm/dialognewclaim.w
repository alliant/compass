&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  Modified:
  08/08/2019  Rahul   Modified to remove mandatory validation from fields.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input-output param pDateRcvd as datetime.
def input-output param pAssignedTo as char.
def input-output param pStateID as char.
def input-output param pAgentID as char.
def input-output param pInsuredName as char.
def input-output param pCoverageID as char.
def input-output param pEffDate as datetime.
def input-output param pCoverageType as char.
def input-output param pInsuredType as char.
def input-output param pSummary as char.
def input-output param pPropAddr1 as char.
def input-output param pPropAddr2 as char.
def input-output param pPropCity as char.
def input-output param pPropState as char.
def input-output param pPropCounty as char.
def input-output param pPropZipCode as char.
def input-output param pPropSubdivision as char.
def input-output param pPropResidential as log.
def input-output param pPropLegalDesc as char.

def output param pLiabilityAmount as dec.
def output param pFileNumber as char.
def output param pError as logical init true.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}

{tt/agent.i}
{tt/state.i}
{tt/sysuser.i}

def var tName as char no-undo.

def var xEffDate as datetime.
def var xLiabilityAmount as decimal.
def var xGrossPremium as decimal.
def var xFormID as char.
def var xStateID as char.
def var xFileNumber as char.
def var xStatCode as char.
def var xNetPremium as dec.
def var xRetention as dec.
def var xCountyID as char.
def var xResidential as log.
def var xInvoiceDate as date.

def var tMsgStat as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tDateRcvd tAssignedTo tStateID tAgentID ~
tInsuredName tCoverageID tEffDate tCoverageType tInsuredType tSummary ~
tPropAddr1 tPropAddr2 tPropCity tPropState tPropCounty tPropZipCode ~
tPropSubdivision tPropResidential tPropLegalDesc Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tAssignedTo tStateID tAgentID tInsuredName ~
tCoverageID tEffDate tCoverageType tInsuredType tSummary tPropAddr1 ~
tPropAddr2 tPropCity tPropState tPropCounty tPropZipCode tPropSubdivision ~
tPropResidential tPropLegalDesc 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Create Claim" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN AUTO-COMPLETION
     SIZE 100 BY 1 NO-UNDO.

DEFINE VARIABLE tAssignedTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Assigned To" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tCoverageType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Coverage Type" 
     VIEW-AS COMBO-BOX INNER-LINES 6
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tInsuredType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Type" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tPropResidential AS LOGICAL FORMAT "yes/no":U INITIAL YES 
     LABEL "Residential" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Yes",yes,
                     "No",no
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tPropState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "AZ","FL","TX" 
     DROP-DOWN-LIST
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPropLegalDesc AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE tSummary AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65 BY 3 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 61.6 BY .62 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 61.6 BY .62 NO-UNDO.

DEFINE VARIABLE tCoverageID AS CHARACTER FORMAT "X(100)":U 
     LABEL "Policy Number" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 TOOLTIP "Policy ID or other coverage ID" NO-UNDO.

DEFINE VARIABLE tDateRcvd AS DATETIME FORMAT "99/99/99":U 
     LABEL "Date Received" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tInsuredName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Insured Name" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE tPropAddr1 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Property Address" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE tPropAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE tPropCity AS CHARACTER FORMAT "X(256)":U 
     LABEL "City" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE tPropCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE tPropSubdivision AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subdivision" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE VARIABLE tPropZipCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Zip Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     tDateRcvd AT ROW 1.95 COL 20 COLON-ALIGNED WIDGET-ID 12
     tAssignedTo AT ROW 3.14 COL 20 COLON-ALIGNED WIDGET-ID 258
     tStateID AT ROW 4.33 COL 20 COLON-ALIGNED WIDGET-ID 82
     tAgentID AT ROW 5.52 COL 20 COLON-ALIGNED WIDGET-ID 84
     tInsuredName AT ROW 8.62 COL 20 COLON-ALIGNED WIDGET-ID 336
     tAddr1 AT ROW 6.67 COL 20 COLON-ALIGNED NO-LABEL WIDGET-ID 86
     tCoverageID AT ROW 9.81 COL 20 COLON-ALIGNED WIDGET-ID 94
     tEffDate AT ROW 9.81 COL 71 COLON-ALIGNED WIDGET-ID 340
     tCoverageType AT ROW 11 COL 20 COLON-ALIGNED WIDGET-ID 330
     tAddr2 AT ROW 7.33 COL 20 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     tInsuredType AT ROW 12.19 COL 20 COLON-ALIGNED WIDGET-ID 308
     tSummary AT ROW 13.86 COL 22 NO-LABEL WIDGET-ID 188
     tPropAddr1 AT ROW 17.43 COL 20 COLON-ALIGNED WIDGET-ID 362
     tPropAddr2 AT ROW 18.62 COL 20 COLON-ALIGNED NO-LABEL WIDGET-ID 364
     tPropCity AT ROW 19.81 COL 20 COLON-ALIGNED WIDGET-ID 366
     tPropState AT ROW 19.81 COL 75 COLON-ALIGNED WIDGET-ID 376
     tPropCounty AT ROW 21 COL 20 COLON-ALIGNED WIDGET-ID 368
     tPropZipCode AT ROW 21 COL 71 COLON-ALIGNED WIDGET-ID 380
     tPropSubdivision AT ROW 22.19 COL 20 COLON-ALIGNED WIDGET-ID 378
     tPropResidential AT ROW 23.38 COL 75 COLON-ALIGNED WIDGET-ID 374
     tPropLegalDesc AT ROW 24.57 COL 22 NO-LABEL WIDGET-ID 370
     Btn_OK AT ROW 28.67 COL 47.2
     Btn_Cancel AT ROW 28.67 COL 65.2
     "Summary:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 13.95 COL 12 WIDGET-ID 282
     "Legal Desc:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 24.67 COL 10 WIDGET-ID 382
     SPACE(105.59) SKIP(5.41)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Claim"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tAddr1 IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tAddr2 IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tDateRcvd IN FRAME fNew
   NO-DISPLAY                                                           */
ASSIGN 
       tPropLegalDesc:RETURN-INSERTED IN FRAME fNew  = TRUE.

ASSIGN 
       tSummary:RETURN-INSERTED IN FRAME fNew  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* New Claim */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fNew
ON CHOOSE OF Btn_OK IN FRAME fNew /* Create Claim */
DO:
  if tDateRcvd:input-value in frame fNew = ? or
     tDateRcvd:input-value > today 
   then
  do: 
      MESSAGE "Date Received cannot be blank or in the future"
       VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      apply 'entry' to tDateRcvd.
      return no-apply.
  end.
  
  if tAssignedTo:input-value = ? or tAssignedTo:input-value = "" then
  do:
      MESSAGE "Assigned To cannot be blank"
       VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      apply 'entry' to tAssignedTo.
      return no-apply.
  end.

/*   if tStateID:input-value = ? or tStateID:input-value = "" then */
/*   do:                                                           */
/*       MESSAGE "State cannot be blank"                           */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                      */
/*       apply 'entry' to tStateID.                                */
/*       return no-apply.                                          */
/*   end.                                                          */
/*                                                                 */
/*   if tAgentID:input-value = ? or tAgentID:input-value = "" then */
/*   do:                                                           */
/*       MESSAGE "Agent cannot be blank"                           */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                      */
/*       apply 'entry' to tAgentID.                                */
/*       return no-apply.                                          */
/*   end.                                                          */

/*   if not can-find(first agent where agent.agentID = tAgentID:input-value  */
/*                               and agent.agentID <> "") then               */
/*     do:                                                                   */
/*         MESSAGE "Invalid Agent"                                           */
/*          VIEW-AS ALERT-BOX ERROR BUTTONS OK.                            */
/*         apply 'entry' to tAgentID.                                        */
/*         return no-apply.                                                  */
/*     end.                                                                  */

/*   if tInsuredName:input-value = ? or tInsuredName:input-value = "" then */
/*   do:                                                                   */
/*       MESSAGE "Insured Name cannot be blank"                            */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                              */
/*       apply 'entry' to tInsuredName.                                    */
/*       return no-apply.                                                  */
/*   end.                                                                  */
/*                                                                         */
/*   if tCoverageID:input-value = ? or tCoverageID:input-value = "" then   */
/*   do:                                                                   */
/*       MESSAGE "Policy number cannot be blank"                           */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                              */
/*       apply 'entry' to tCoverageID.                                     */
/*       return no-apply.                                                  */
/*   end.                                                                  */
if tCoverageID:screen-value ne ""
 then
  do:
    publish "IsPolicyValid" (input 0,                   /* ClaimID - not created yet */
                             input tCoverageID:input-value,
                             input tAgentID:input-value,
                             output xEffDate,
                             output xLiabilityAmount,
                             output xGrossPremium,
                             output xFormID,
                             output xStateID,
                             output xFileNumber,
                             output xStatCode,
                             output xNetPremium,
                             output xRetention,
                             output xCountyID,
                             output xResidential,
                             output xInvoiceDate,
                             output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                             output std-ch).
                           
    if tMsgStat = "W" then  /* Warning */
    do:
      std-lo = no.
      message std-ch skip 
        "OK to continue?"
        view-as alert-box warning buttons yes-no
        update std-lo.
      
      if not(std-lo = yes) then
      do:
        apply 'entry' to tCoverageID.
        return no-apply.
      end.

      assign
        pLiabilityAmount = xLiabilityAmount
        pFileNumber = trim(xFileNumber).
    end.
    else   
    if tMsgStat = "E" then  /* Error */
    do:
      message std-ch view-as alert-box error.
      apply 'entry' to tCoverageID.
      return no-apply.
    end.
    
  end.
  
/*   if tEffDate:input-value = ? then                                        */
/*   do:                                                                     */
/*       MESSAGE "Effective Date cannot be blank"                            */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                                */
/*       apply 'entry' to tEffDate.                                          */
/*       return no-apply.                                                    */
/*   end.                                                                    */
/*                                                                           */
/*   if tCoverageType:input-value = ? or tCoverageType:input-value = "" then */
/*   do:                                                                     */
/*       MESSAGE "Coverage Type cannot be blank"                             */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                                */
/*       apply 'entry' to tCoverageType.                                     */
/*       return no-apply.                                                    */
/*   end.                                                                    */
/*                                                                           */
/*   if tInsuredType:input-value = ? or tInsuredType:input-value = "" then   */
/*   do:                                                                     */
/*       MESSAGE "Insured Type cannot be blank"                              */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                                */
/*       apply 'entry' to tInsuredType.                                      */
/*       return no-apply.                                                    */
/*   end.                                                                    */
/*                                                                           */
/*   if tSummary:input-value = ? or tSummary:input-value = "" then           */
/*   do:                                                                     */
/*       MESSAGE "Summary cannot be blank"                                   */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                                */
/*       apply 'entry' to tSummary.                                          */
/*       return no-apply.                                                    */
/*   end.                                                                    */
/*                                                                           */
/*   if tPropAddr1:input-value = ? or tPropAddr1:input-value = "" then       */
/*   do:                                                                     */
/*       MESSAGE "Propery Address cannot be blank"                           */
/*        VIEW-AS ALERT-BOX ERROR BUTTONS OK.                                */
/*       apply 'entry' to tPropAddr1.                                        */
/*       return no-apply.                                                    */
/*   end.                                                                    */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID fNew
ON VALUE-CHANGED OF tAgentID IN FRAME fNew /* Agent */
DO:
  find agent
    where agent.agentID = self:screen-value no-error.
  if not available agent 
   then assign
          tAddr1:screen-value in frame fNew = ""
          tAddr2:screen-value in frame fNew = ""
          .
   else assign
          tAddr1:screen-value in frame fNew = agent.addr1 + "  " + agent.addr2
          tAddr2:screen-value in frame fNew = agent.city 
                  + (if agent.city > "" and agent.state > "" then ", " else "")
                  + agent.state + "  " + agent.zip
          .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCoverageID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCoverageID fNew
ON LEAVE OF tCoverageID IN FRAME fNew /* Policy Number */
DO:
  if self:screen-value <> "" then 
  do: 
    publish "IsPolicyValid" (input 0,                   /* ClaimID - not created yet */
                             input self:input-value,
                             input tAgentID:input-value,
                             output xEffDate,
                             output xLiabilityAmount,
                             output xGrossPremium,
                             output xFormID,
                             output xStateID,
                             output xFileNumber,
                             output xStatCode,
                             output xNetPremium,
                             output xRetention,
                             output xCountyID,
                             output xResidential,
                             output xInvoiceDate,
                             output tMsgStat,  /* Returns S)uccess, E)rror, or W)arning */
                             output std-ch).
                             
    if tMsgStat = "S" or tMsgStat = "W" then  /* Success or Warning */
    do:
      if tEffDate:input-value = ? and xEffDate <> ? then
      assign tEffDate:screen-value = string(xEffDate).
      
      assign
        pLiabilityAmount = xLiabilityAmount
        pFileNumber = trim(xFileNumber)
        tPropResidential:screen-value = string(xResidential)
        .
    end.
    else   
    if tMsgStat = "E" then  /* Error */
    do:
      message std-ch view-as alert-box error.
    end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDateRcvd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDateRcvd fNew
ON LEAVE OF tDateRcvd IN FRAME fNew /* Date Received */
DO:
  if self:input-value = ? or self:input-value > today 
   then self:bgcolor = 14.
   else self:bgcolor = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID fNew
ON VALUE-CHANGED OF tStateID IN FRAME fNew /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetClmUsers" (output table sysuser). /* Subset of just claims users */
std-ch = ",".
tAssignedTo:list-item-pairs in frame fNew = std-ch.
for each sysuser no-lock by sysuser.name:
  std-ch = std-ch + "," + trim(sysuser.name) + "," + trim(sysuser.uid).
end.
tAssignedTo:list-item-pairs in frame fNew = std-ch.

std-ch = "".
for each state where state.active = true no-lock by state.seq:
  std-ch = std-ch + caps(state.stateID) + ",".
end.
std-ch = trim(std-ch,",").
tPropState:list-items in frame fNew = "," + std-ch.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

assign
  tDateRcvd:screen-value = string(today)
  tAssignedTo:screen-value = pAssignedTo
  tStateID:screen-value = pStateID
  tAgentID:screen-value = pAgentID
  tInsuredName:screen-value = pInsuredName
  tCoverageID:screen-value = pCoverageID
  tEffDate:screen-value = string(pEffDate)
  tCoverageType:screen-value = pCoverageType
  tInsuredType:screen-value = pInsuredType
  tSummary:screen-value = pSummary
  tPropAddr1:screen-value = pPropAddr1
  tPropAddr2:screen-value = pPropAddr2
  tPropCity:screen-value = pPropCity
  tPropState:screen-value = pPropState
  tPropCounty:screen-value = pPropCounty
  tPropZipCode:screen-value = pPropZipCode
  tPropSubdivision:screen-value = pPropSubdivision
  tPropResidential:screen-value = string(pPropResidential)
  tPropLegalDesc:screen-value = pPropLegalDesc
  .
  

MAIN-BLOCK:
repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.
  
  /* create the combos */
  {lib/get-sysprop-list.i &combo=tCoverageType &appCode="'CLM'" &objAction="'ClaimDescription'" &objProperty="'Coverage'"}
  {lib/get-sysprop-list.i &combo=tInsuredType &appCode="'CLM'" &objAction="'ClaimDescription'" &objProperty="'Insured'"}
  {lib/get-state-list.i &combo=tStateID &addUnknown=true}
  {lib/get-state-list.i &combo=tPropState &useFullName=false &useFilter=false &useActive=false}
  {lib/get-agent-list.i &combo=tAgentID &state=tStateID &addUnknown=true}
  tStateID:screen-value = "Unknown".
  run AgentComboSet ("Unknown").
  
/*   publish "GetCurrentValue" ("ViewState", output std-ch).  */
/*   if std-ch <> ? and std-ch <> "" then                     */
/*   tStateID:screen-value = std-ch.                            */
/*   apply "VALUE-CHANGED" to tStateID in frame fNew.           */
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  /* Force screen values to variables */
  assign
    tDateRcvd
    tAssignedTo
    tStateID
    tAgentID
    tInsuredName
    tCoverageID
    tEffDate
    tCoverageType
    tInsuredType
    tSummary
    tPropAddr1
    tPropAddr2
    tPropCity
    tPropState
    tPropCounty
    tPropZipCode
    tPropSubdivision
    tPropResidential
    tPropLegalDesc
    .
  
  assign
    pDateRcvd        =  tDateRcvd       
    pAssignedTo      =  tAssignedTo            
    pStateID         =  tStateID        
    pAgentID         =  tAgentID        
    pInsuredName     =  tInsuredName    
    pCoverageID      =  tCoverageID   
    pEffDate         =  tEffDate     
    pCoverageType    =  tCoverageType
    pInsuredType     =  tInsuredType   
    pSummary         =  tSummary        
    pPropAddr1       =  tPropAddr1      
    pPropAddr2       =  tPropAddr2      
    pPropCity        =  tPropCity       
    pPropState       =  tPropState      
    pPropCounty      =  tPropCounty     
    pPropZipCode     =  tPropZipCode    
    pPropSubdivision =  tPropSubdivision
    pPropResidential =  tPropResidential
    pPropLegalDesc   =  tPropLegalDesc  
    pError = false
    .
  
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAssignedTo tStateID tAgentID tInsuredName tCoverageID tEffDate 
          tCoverageType tInsuredType tSummary tPropAddr1 tPropAddr2 tPropCity 
          tPropState tPropCounty tPropZipCode tPropSubdivision tPropResidential 
          tPropLegalDesc 
      WITH FRAME fNew.
  ENABLE tDateRcvd tAssignedTo tStateID tAgentID tInsuredName tCoverageID 
         tEffDate tCoverageType tInsuredType tSummary tPropAddr1 tPropAddr2 
         tPropCity tPropState tPropCounty tPropZipCode tPropSubdivision 
         tPropResidential tPropLegalDesc Btn_OK Btn_Cancel 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

