&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.
define input parameter pClaimID as int no-undo.
define input parameter hFileDataSrv as handle no-undo.

define variable hNoteHistory as handle no-undo.

/* ***************************  Definitions  ************************** */

{lib/std-def.i}
{lib/set-button-def.i}
{tt/claimnote.i}
{tt/claimnote.i &tableAlias="usernote"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-36 bNoteHistory tClaimID tDescription ~
bModifyNote bFilter bClear fSearch tViewNoteCategory tViewNoteOrder ~
editorNotes bRefresh bAddNote 
&Scoped-Define DISPLAYED-OBJECTS tClaimID tDescription fSearch ~
tViewNoteCategory tViewNoteOrder 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SearchNote C-Win 
FUNCTION SearchNote RETURNS LOGICAL
  ( input hBuffer as handle,
    input pSearch as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddNote  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new note".

DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bModifyNote  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify notes".

DEFINE BUTTON bNoteHistory  NO-FOCUS
     LABEL "History" 
     SIZE 7.2 BY 1.71 TOOLTIP "View the created notes history".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh notes".

DEFINE VARIABLE tViewNoteCategory AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Category" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 23 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE editorNotes AS LONGCHAR 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 160 BY 25
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 67 BY 1 NO-UNDO.

DEFINE VARIABLE tClaimID AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Claim ID" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 92 BY 1 NO-UNDO.

DEFINE VARIABLE tViewNoteOrder AS CHARACTER INITIAL "D" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Asc", "A",
"Desc", "D"
     SIZE 16.6 BY 1 TOOLTIP "Select to display notes in date oldest first (Asc) or newest first (Des) order" NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 160.2 BY 1.91.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bNoteHistory AT ROW 1.24 COL 155 WIDGET-ID 332 NO-TAB-STOP 
     tClaimID AT ROW 1.71 COL 10 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     tDescription AT ROW 1.71 COL 38 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     bModifyNote AT ROW 1.24 COL 140.2 WIDGET-ID 306 NO-TAB-STOP 
     bFilter AT ROW 3.71 COL 140.8 WIDGET-ID 64
     bClear AT ROW 3.71 COL 150.8 WIDGET-ID 68
     fSearch AT ROW 3.81 COL 69 COLON-ALIGNED WIDGET-ID 62
     tViewNoteCategory AT ROW 3.86 COL 11.4 COLON-ALIGNED WIDGET-ID 240 NO-TAB-STOP 
     tViewNoteOrder AT ROW 3.86 COL 44.2 NO-LABEL WIDGET-ID 242 NO-TAB-STOP 
     editorNotes AT ROW 5.19 COL 2 NO-LABEL WIDGET-ID 200
     bRefresh AT ROW 1.24 COL 147.6 WIDGET-ID 308 NO-TAB-STOP 
     bAddNote AT ROW 1.24 COL 132.8 WIDGET-ID 224 NO-TAB-STOP 
     "Sort:" VIEW-AS TEXT
          SIZE 5 BY .62 AT ROW 4.05 COL 39.2 WIDGET-ID 300
     RECT-36 AT ROW 3.38 COL 2 WIDGET-ID 298
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 162 BY 29.38 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "File Notes"
         HEIGHT             = 29.38
         WIDTH              = 162
         MAX-HEIGHT         = 39.14
         MAX-WIDTH          = 229.4
         VIRTUAL-HEIGHT     = 39.14
         VIRTUAL-WIDTH      = 229.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR EDITOR editorNotes IN FRAME DEFAULT-FRAME
   NO-DISPLAY                                                           */
ASSIGN 
       editorNotes:RETURN-INSERTED IN FRAME DEFAULT-FRAME  = TRUE
       editorNotes:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tClaimID:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tDescription:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* File Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* File Notes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* File Notes */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddNote C-Win
ON CHOOSE OF bAddNote IN FRAME DEFAULT-FRAME /* Add */
DO:
  run dialogaddnote.w (pClaimID, hFileDataSrv).
  run FilterNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME DEFAULT-FRAME /* Clear */
DO:
  assign
    fSearch:screen-value = ""
    tViewNoteCategory:screen-value = "ALL"
    tViewNoteOrder:screen-value = "D"
    .
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME DEFAULT-FRAME /* Filter */
DO:
  run FilterNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyNote C-Win
ON CHOOSE OF bModifyNote IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNoteHistory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNoteHistory C-Win
ON CHOOSE OF bNoteHistory IN FRAME DEFAULT-FRAME /* History */
DO:
  if valid-handle(hNoteHistory)
   then run ShowWindow in hNoteHistory.
   else run dialognoteshistory.w persistent set hNoteHistory ("Claim", string(pClaimID)).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
 run RefreshNotes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewNoteCategory
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewNoteCategory C-Win
ON VALUE-CHANGED OF tViewNoteCategory IN FRAME DEFAULT-FRAME /* Category */
DO:
  apply "CHOOSE" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tViewNoteOrder
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tViewNoteOrder C-Win
ON VALUE-CHANGED OF tViewNoteOrder IN FRAME DEFAULT-FRAME
DO:
  run FilterNotes in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{&window-name}:title = "File " + string(pClaimID) + " Notes".
{lib/win-main.i}
  
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.
       

assign
  {&window-name}:min-height-pixels = {&window-name}:height-pixels
  {&window-name}:min-width-pixels = {&window-name}:width-pixels
  {&window-name}:max-height-pixels = session:height-pixels
  {&window-name}:max-width-pixels = session:width-pixels
  .

{lib/set-button.i &label="NoteHistory" &image="images/book.bmp"       &inactive="images/book-i.bmp"}
{lib/set-button.i &label="AddNote"     &image="images/blank-add.bmp"  &inactive="images/blank-i.bmp"}
{lib/set-button.i &label="ModifyNote"  &image="images/update.bmp"     &inactive="images/update.bmp"}
{lib/set-button.i &label="Refresh"}
setButtons().

subscribe to "RefreshNotes" anywhere.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  run Initialize in this-procedure.
  {&window-name}:move-to-top().
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "WINDOW-CLOSE" to {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tClaimID tDescription fSearch tViewNoteCategory tViewNoteOrder 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-36 bNoteHistory tClaimID tDescription bModifyNote bFilter bClear 
         fSearch tViewNoteCategory tViewNoteOrder editorNotes bRefresh bAddNote 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterNotes C-Win 
PROCEDURE FilterNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hQuery as handle no-undo.
  define variable hBuffer as handle no-undo.
  define variable tQuery as character no-undo.
  define variable cType as character no-undo.
  define variable cCatDesc as character no-undo.
  define variable cUsername as character no-undo.
  define variable cSubject as character no-undo.
  define variable tNote as character no-undo.
  define variable tNotes as longchar no-undo.
  define variable isValid as logical no-undo.
  
  run GetClaimNotes in hFileDataSrv (output table claimnote).
  
  /* get the category */
  do with frame {&frame-name}:
    /* create the query */
    tQuery = "for each claimnote by noteDate".
    if tViewNoteOrder:screen-value = "D"
     then tQuery = tQuery + " descending".
    
    create buffer hBuffer for table "claimNote".
    create query hQuery.
    hQuery:set-buffers(hBuffer).
    hQuery:query-prepare(tQuery).
    hQuery:query-open().
    hQuery:get-first().
    repeat while not hQuery:query-off-end:
      assign
        isValid = true
        std-ch = hBuffer:buffer-field("category"):buffer-value()
        cType = hBuffer:buffer-field("noteType"):buffer-value()
        .
      /* test if the record is the category */
      if tViewNoteCategory:screen-value <> "ALL" and std-ch <> tViewNoteCategory:screen-value
       then isValid = false.
       else publish "GetNoteCategoryDesc" (std-ch, output cCatDesc).
      /* test if the record contains the search text */
      if fSearch:screen-value > "" and not SearchNote(hBuffer, fSearch:screen-value)
       then isValid = false.
 
      if isValid
       then
        do:
          /* get the user's name */
          std-ch = hBuffer:buffer-field("uid"):buffer-value().
          publish "GetSysUserName" (std-ch, output cUsername).
          if cUsername = ""
           then cUsername = std-ch.
          /* get the subject */
          std-ch = hBuffer:buffer-field("subject"):buffer-value().
          if std-ch > "" or std-ch = ?
           then cSubject = "" + std-ch.
           else cSubject = "".
          assign
            /* build the note */
            tNote = ""
            tNote = "[  " + string(hBuffer:buffer-field("noteDate"):buffer-value(), "99/99/9999 HH:MM AM  ")
            tNote = tNote + "|  " + caps(cUsername) 
                          + (if cCatDesc > "" then "  |  " else "") 
                          + caps(cCatDesc) 
                          + (if cSubject > "" then "  |  " else "")  + caps(cSubject) + "  ]" 
                          + chr(10) + chr(10)
            tNote = tNote + hBuffer:buffer-field("notes"):buffer-value() + chr(10) + chr(10)
            tNotes = tNotes + tNote
            .
        end.
        hQuery:get-next().
    end.
    hQuery:query-close().
    delete object hQuery no-error.
    
    editorNotes:screen-value = tNotes.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize C-Win 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCategoryList as character no-undo.
  define variable cDescription as character no-undo.
  
  /* note category */
  publish "GetNoteCategoryList" (output cCategoryList).
  if cCategoryList > ""
   then std-ch = "ALL,ALL," + cCategoryList.
   else std-ch = "ALL,ALL".
  /* claim description */
  run GetClaimDescription in hFileDataSrv (pClaimID, output cDescription).
  
  do with frame {&frame-name}:
    assign
      tClaimID:screen-value = string(pClaimID)
      tDescription:screen-value = cDescription
      tViewNoteCategory:list-item-pairs = std-ch
      tViewNoteCategory:screen-value = "ALL"
      .
  end.
  
  apply "CHOOSE" to bClear.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyNotes C-Win 
PROCEDURE modifyNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cCatDesc as character no-undo.

  run GetClaimNotes in hFileDataSrv (output table claimnote).
  publish "GetCredentialsID" (output std-ch).
  
  empty temp-table usernote.
  for each  claimnote no-lock
     where claimnote.uid = std-ch:
     
    create usernote.
    buffer-copy claimnote to usernote.
  end.
  run dialogmodifynotes.w (table usernote, hFileDataSrv).

  run FilterNotes in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshNotes C-Win 
PROCEDURE RefreshNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

do with frame {&frame-name}:
  run LoadClaimNotes in hFileDataSrv.
  apply "CHOOSE" to bClear. 
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

do with frame {&frame-name}:
  assign
    frame {&frame-name}:width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels
    frame {&frame-name}:height-pixels = {&window-name}:height-pixels
    frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
    editorNotes:width-pixels = frame {&frame-name}:width-pixels - 10
    editorNotes:height-pixels = frame {&frame-name}:height-pixels - editorNotes:y - 5
    .
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SearchNote C-Win 
FUNCTION SearchNote RETURNS LOGICAL
  ( input hBuffer as handle,
    input pSearch as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  return hBuffer:buffer-field("notes"):buffer-value() matches "*" + pSearch + "*" or
         hBuffer:buffer-field("subject"):buffer-value() matches "*" + pSearch + "*".

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

