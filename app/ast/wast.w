&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*
  wmon.w
  COMMunications Window for client-side debugging
 */

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winlaunch.i}
{tt/process.i}

def var tSeq as int no-undo.
def var tKeepTrying as logical no-undo.
def var tStartTime as datetime no-undo.
def var tEndTime as datetime no-undo.

def var tTraceFile as char no-undo.
def var tResponseFile as char no-undo.

{lib/xmlparse.i &exclude-startElement=true
            &traceFile=tTraceFile
            &xmlFile=tResponseFile}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES processLog

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData processLog.seq processLog.logProcess processLog.startTime processLog.endTime processLog.logDuration processLog.logStatus processLog.logMsg   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH processLog by processLog.logDate descending
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH processLog by processLog.logDate descending.
&Scoped-define TABLES-IN-QUERY-brwData processLog
&Scoped-define FIRST-TABLE-IN-QUERY-brwData processLog


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAction tType tQuery tCount tPause bStart ~
bClear bExport brwData bViewRequest bViewResponse bViewCommand bViewConfig ~
bViewLog bViewTrace tContents 
&Scoped-Define DISPLAYED-OBJECTS tProcess tAction tType tQuery tPostFile ~
tCount tPause tContents tStatus tStatus2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteTempFiles C-Win 
FUNCTION deleteTempFiles RETURNS LOGICAL PRIVATE
  ( pProcess as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setFields C-Win 
FUNCTION setFields RETURNS LOGICAL PRIVATE
  ( pStat as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Application 
       MENU-ITEM m_Restore_Last LABEL "Restore Last"  
       RULE
       MENU-ITEM m_Configure    LABEL "Configure..."  
       MENU-ITEM m_About        LABEL "About"         
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Application  LABEL "Application"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bStart 
     LABEL "Start" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bStop 
     LABEL "Stop" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bViewCommand 
     LABEL "Command" 
     SIZE 11 BY 1.19.

DEFINE BUTTON bViewConfig 
     LABEL "Config" 
     SIZE 11 BY 1.19 TOOLTIP "View the request configuration last sent to the server".

DEFINE BUTTON bViewLog 
     LABEL "Log" 
     SIZE 11 BY 1.19 TOOLTIP "View the request payload last sent to the server".

DEFINE BUTTON bViewRequest 
     LABEL "Request" 
     SIZE 11 BY 1.19 TOOLTIP "View the request payload last sent to the server".

DEFINE BUTTON bViewResponse 
     LABEL "Response" 
     SIZE 11 BY 1.19 TOOLTIP "View the server response to the last request".

DEFINE BUTTON bViewTrace 
     LABEL "Trace" 
     SIZE 11 BY 1.19.

DEFINE VARIABLE tContents AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 49.6 BY 14.57 NO-UNDO.

DEFINE VARIABLE tAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 30.8 BY 1 NO-UNDO.

DEFINE VARIABLE tCount AS INTEGER FORMAT ">>9":U INITIAL 5 
     LABEL "Iterations" 
     VIEW-AS FILL-IN 
     SIZE 7.4 BY 1 NO-UNDO.

DEFINE VARIABLE tPause AS INTEGER FORMAT ">>9":U INITIAL 5 
     LABEL "Pause" 
     VIEW-AS FILL-IN 
     SIZE 7.4 BY 1 TOOLTIP "Seconds to pause between iterations (60 = 1min, 300 = 5min, 900 = 15min)" NO-UNDO.

DEFINE VARIABLE tPostFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "XML" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 TOOLTIP "Data to post" NO-UNDO.

DEFINE VARIABLE tProcess AS CHARACTER FORMAT "X(256)":U 
     LABEL "Last Event" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 TOOLTIP "Named application process"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tQuery AS CHARACTER FORMAT "X(256)":U 
     LABEL "Query" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 TOOLTIP "Query String without the ?" NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Get", 1,
"Post", 2
     SIZE 18.2 BY 1.14 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      processLog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      processLog.seq label "Seq" format "zzz,zz9"
 processLog.logProcess label "Action" format "x(25)" width 12
 processLog.startTime label "Started"
 processLog.endTime label "Completed"
 processLog.logDuration label "Duration" format "zzz,zz9"
 processLog.logStatus label "Status" format "Success/Failure" width 12
 processLog.logMsg label "Message" format "x(256)" width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 118.8 BY 14.52 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tProcess AT ROW 1.71 COL 131.6 COLON-ALIGNED WIDGET-ID 102
     tAction AT ROW 1.24 COL 8 COLON-ALIGNED WIDGET-ID 130
     tType AT ROW 2.24 COL 10.6 NO-LABEL WIDGET-ID 122
     tQuery AT ROW 3.43 COL 8 COLON-ALIGNED WIDGET-ID 128
     tPostFile AT ROW 4.52 COL 8 COLON-ALIGNED WIDGET-ID 126
     tCount AT ROW 3.43 COL 49.6 COLON-ALIGNED WIDGET-ID 132
     tPause AT ROW 4.52 COL 49.6 COLON-ALIGNED WIDGET-ID 138
     bStart AT ROW 3.29 COL 61 WIDGET-ID 116
     bStop AT ROW 4.48 COL 61.2 WIDGET-ID 118
     bClear AT ROW 3.29 COL 70.2 WIDGET-ID 136
     bExport AT ROW 4.48 COL 70.2 WIDGET-ID 120
     brwData AT ROW 5.81 COL 2 WIDGET-ID 200
     bViewRequest AT ROW 3.1 COL 121.6 WIDGET-ID 94
     bViewResponse AT ROW 4.43 COL 121.8 WIDGET-ID 96
     bViewCommand AT ROW 3.1 COL 136.2 WIDGET-ID 108
     bViewConfig AT ROW 4.43 COL 136 WIDGET-ID 112
     bViewLog AT ROW 3.1 COL 149.8 WIDGET-ID 104
     bViewTrace AT ROW 4.43 COL 150 WIDGET-ID 92
     tContents AT ROW 5.81 COL 121.6 NO-LABEL WIDGET-ID 110
     tStatus AT ROW 20.33 COL 1.6 NO-LABEL WIDGET-ID 134 NO-TAB-STOP 
     tStatus2 AT ROW 20.33 COL 29.4 NO-LABEL WIDGET-ID 140 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 170.2 BY 20.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Service Testing"
         HEIGHT             = 20.33
         WIDTH              = 170.2
         MAX-HEIGHT         = 20.33
         MAX-WIDTH          = 170.2
         VIRTUAL-HEIGHT     = 20.33
         VIRTUAL-WIDTH      = 170.2
         SHOW-IN-TASKBAR    = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData bExport fMain */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bStop IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tContents:RETURN-INSERTED IN FRAME fMain  = TRUE.

/* SETTINGS FOR FILL-IN tPostFile IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tProcess IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN tStatus2 IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH processLog by processLog.logDate descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Service Testing */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Service Testing */
DO:
  /* This event will close the window and terminate the procedure.  */
  deleteTempFiles(tProcess:screen-value in frame fMain).
  apply "CLOSE" to this-procedure.
  publish "ExitApplication".
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  close query brwData.
  empty temp-table processLog.
  tSeq = 0.
  tStatus:screen-value in frame fMain = "".
  tStatus2:screen-value in frame fMain = "".
  deleteTempFiles(tProcess:screen-value in frame fMain).
  apply "choose" to bViewRequest in frame fMain.
  tProcess:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run doExport in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if available processLog 
   then apply "choose" to bViewResponse in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStart C-Win
ON CHOOSE OF bStart IN FRAME fMain /* Start */
DO:
  if tAction:screen-value in frame fMain = "" 
   then
    do:
        MESSAGE "Action cannot be blank"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        return no-apply.
    end.
  if tType:input-value = 2 
   and search(tPostFile:screen-value in frame fMain) = ? 
   then
    do:
        MESSAGE "Invalid data file"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        return no-apply.
    end.
  if tCount:input-value < 1
   then
    do:
        MESSAGE "Iterations must be at least 1"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        return no-apply.
    end.
  if tPause:input-value < 1
   then
    do:
        MESSAGE "Pause interval must be at least 1 second"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        return no-apply.
    end.
    
  deleteTempFiles(tProcess:screen-value in frame fMain).
  
  subscribe to "ProcessLog" anywhere.

  publish "SetLastAction" (tAction:screen-value in frame fMain).
  publish "SetLastType" (tType:input-value in frame fMain).
  publish "SetLastQuery" (tQuery:screen-value in frame fMain).
  publish "SetLastFile" (tPostFile:screen-value in frame fMain).
  publish "SetLastIterations" (tCount:input-value in frame fMain).
  publish "SetLastPause" (tPause:input-value in frame fMain).

  if tType:input-value = 1 
   then run doGetAction in this-procedure.
   else run doPostAction in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStop
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStop C-Win
ON CHOOSE OF bStop IN FRAME fMain /* Stop */
DO:
  tKeepTrying = false.
  unsubscribe to "ProcessLog".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewCommand
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewCommand C-Win
ON CHOOSE OF bViewCommand IN FRAME fMain /* Command */
DO:
  run viewfile in this-procedure ("command").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewConfig C-Win
ON CHOOSE OF bViewConfig IN FRAME fMain /* Config */
DO:
 run viewfile in this-procedure ("config") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewLog C-Win
ON CHOOSE OF bViewLog IN FRAME fMain /* Log */
DO:
 run viewfile in this-procedure ("log") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewRequest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewRequest C-Win
ON CHOOSE OF bViewRequest IN FRAME fMain /* Request */
DO:
 run viewfile in this-procedure ("request") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewResponse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewResponse C-Win
ON CHOOSE OF bViewResponse IN FRAME fMain /* Response */
DO:
 run viewfile in this-procedure ("response").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewTrace
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewTrace C-Win
ON CHOOSE OF bViewTrace IN FRAME fMain /* Trace */
DO:
  run viewfile in this-procedure ("trace").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About */
DO:
 publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure... */
DO:
  run dialogconfig.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Restore_Last
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Restore_Last C-Win
ON CHOOSE OF MENU-ITEM m_Restore_Last /* Restore Last */
DO:
  publish "GetLastAction" (output std-ch).
  tAction:screen-value in frame fMain = std-ch.
  publish "GetLastType" (output std-in).
  tType:screen-value in frame fMain = string(std-in).
  publish "GetLastQuery" (output std-ch).
  tQuery:screen-value in frame fMain = std-ch.
  publish "GetLastFile" (output std-ch).
  tPostFile:screen-value in frame fMain = std-ch.

  publish "GetLastIterations" (output std-in).
  tCount:screen-value in frame fMain = string(std-in).
  publish "GetLastPause" (output std-in).
  tPause:screen-value in frame fMain = string(std-in).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tType C-Win
ON VALUE-CHANGED OF tType IN FRAME fMain
DO:
  if self:input-value = 1 
   then tPostFile:sensitive in frame fMain = false.
   else tPostFile:sensitive in frame fMain = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}

publish "GetIterations" (output tCount).
publish "GetPause" (output tPause).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

tStartTime = now.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doExport C-Win 
PROCEDURE doExport PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle.
 th = temp-table processLog:handle.

 if not th:has-records 
  then
   do: 
       MESSAGE "No data to export"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 run util/exporttable.p
   (table-handle th,
    "processLog",
    "for each processLog by processLog.seq ",
    "logProcess,seq,startTime,endTime,logDuration,logStatus,logMsg",
    "Action,Sequence,Started,Completed,Duration,Success,Message",
    "",
    "",
    yes,
    output std-ch,
    output std-in).
 if std-ch = ""
  then
   MESSAGE std-in " records exported."
    VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doGetAction C-Win 
PROCEDURE doGetAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tCnt as int no-undo.
 def var tFile as char no-undo.

 setFields(false).
 tKeepTrying = true.
 tStatus:screen-value in frame fMain = "".
 tStatus2:screen-value in frame fMain = "".

 DO-LOOP:
 do while tKeepTrying:
  tCnt = tCnt + 1.
  if tCnt > tCount:input-value in frame fMain 
   then leave DO-LOOP.
  tStatus:screen-value in frame fMain = "Iteration: " + string(tCnt). 
  tStatus2:screen-value in frame fMain = "". 
  tStartTime = now.
  run httpget.p ("mon",
                 tAction:screen-value in frame fMain,
                 tQuery:screen-value in frame fMain,
                 output std-lo,
                 output std-ch,
                 output tFile).

  close query brwData.

  tTraceFile = "mon" + tAction:screen-value in frame fMain + "-trc.dat".
  tResponseFile = tFile.
  
  parseMsg = "".

  /* Only parse the XML response file if we actually made the call */
  if std-lo 
   then std-lo = parseXML(output std-ch).

  find processLog
    where processLog.seq = tSeq no-error.
  if not available processLog 
   then
    do: /* Much bigger issues (started app incorrectly) */
        tKeepTrying = false.
        next.
    end.
  processLog.logStatus = std-lo.
  processLog.logMsg = std-ch.

  open query brwData for each processLog by processLog.seq descending.
  apply "DEFAULT-ACTION" to brwData in frame fMain.

  if tCnt = tCount:input-value in frame fMain
   then leave DO-LOOP.

  PAUSE-LOOP:
  do std-in = 1 to tPause:input-value in frame fMain:
   tStatus2:screen-value in frame fMain = "Pausing: " + string(std-in) 
     + (if std-in = 1 then " second" else " seconds").
   
   wait-for "choose" of bStop pause 1.

   if not tKeepTrying 
    then 
     do: tStatus2:screen-value in frame fMain = "Stopped".
         leave PAUSE-LOOP.
     end.
  end.
 end.

 if tKeepTrying 
  then assign
         tStatus:screen-value in frame fMain = "" 
         tStatus2:screen-value in frame fMain = "". 

 setFields(true).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doPostAction C-Win 
PROCEDURE doPostAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tCnt as int no-undo.
 def var tFile as char no-undo.

 setFields(false).
 tKeepTrying = true.
 tStatus:screen-value in frame fMain = "".
 tStatus2:screen-value in frame fMain = "".

 DO-LOOP:
 do while tKeepTrying:
  tCnt = tCnt + 1.
  if tCnt > tCount:input-value in frame fMain 
   then leave DO-LOOP.
  tStatus:screen-value in frame fMain = "Iteration: " + string(tCnt). 
  tStatus2:screen-value in frame fMain = "". 
  tStartTime = now.
  run httppost.p ("mon",
                  tAction:screen-value in frame fMain,
                  tPostFile:screen-value in frame fMain,
                  output std-lo,
                  output std-ch,
                  output tFile).

  close query brwData.

  tTraceFile = "mon" + tAction:screen-value in frame fMain + "-trc.dat".
  tResponseFile = tFile.
  
  parseMsg = "".

  /* Only parse the XML response file if we actually made the call */
  if std-lo 
   then std-lo = parseXML(output std-ch).

  find processLog
    where processLog.seq = tSeq no-error.
  if not available processLog 
   then
    do: /* Much bigger issues (started app incorrectly) */
        tKeepTrying = false.
        next.
    end.
  processLog.logStatus = std-lo.
  processLog.logMsg = std-ch.

  open query brwData for each processLog by processLog.seq descending.
  apply "DEFAULT-ACTION" to brwData in frame fMain.

  if tCnt = tCount:input-value in frame fMain
   then leave DO-LOOP.

  PAUSE-LOOP:
  do std-in = 1 to tPause:input-value in frame fMain:
   tStatus2:screen-value in frame fMain = "Pausing: " + string(std-in) 
     + (if std-in = 1 then " second" else " seconds").
   
   wait-for "choose" of bStop pause 1.

   if not tKeepTrying 
    then 
     do: tStatus2:screen-value in frame fMain = "Stopped".
         leave PAUSE-LOOP.
     end.
  end.
 end.

 if tKeepTrying 
  then assign
         tStatus:screen-value in frame fMain = "" 
         tStatus2:screen-value in frame fMain = "". 

 setFields(true).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tProcess tAction tType tQuery tPostFile tCount tPause tContents 
          tStatus tStatus2 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tAction tType tQuery tCount tPause bStart bClear bExport brwData 
         bViewRequest bViewResponse bViewCommand bViewConfig bViewLog 
         bViewTrace tContents 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsEventViewerOpen C-Win 
PROCEDURE IsEventViewerOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pOpen as logical init true.

this-procedure:current-window:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ProcessLog C-Win 
PROCEDURE ProcessLog :
/*------------------------------------------------------------------------------
  Purpose:     Refresh if any process is called while the window is open
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAction as char.
 def input parameter pType as char.
 def input parameter pDuration as int.
 def input parameter pStatus as logical.
 
 tEndTime = now.

 tProcess:screen-value in frame fMain = pAction.

 tSeq = tSeq + 1.
 create processLog.

 processLog.seq = tSeq.
 processLog.logProcess = tAction:screen-value in frame fMain.
 processLog.logType = pType.
 processLog.logDate = now.
 processLog.startTime = tStartTime.
 processLog.endTime = tEndTime.
 processLog.logStatus = pStatus.
 processLog.logDuration = interval(tEndTime, tStartTime, "milliseconds").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement C-Win 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
 DEFINE INPUT PARAMETER localName AS CHARACTER.
 DEFINE INPUT PARAMETER qName AS CHARACTER.
 DEFINE INPUT PARAMETER attributes AS HANDLE.

 DEF VAR i AS INT.
 DEF VAR fldvalue AS CHAR.

 def var responseCode as char no-undo.

 case qName: 
  when "Fault" 
   then 
    do:
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "faultCode" THEN
                 responseCode = fldvalue.
             WHEN "faultString" THEN
                 parseMsg = fldvalue.
         END CASE.
        END.
        parseStatus = false.
    end.
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewFile C-Win 
PROCEDURE viewFile PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pType as char.
def var tFile as char no-undo.
def var tFilefullpath as char no-undo.

 if tProcess:screen-value in frame fMain = "" 
  then return.
 tFile = tProcess:screen-value in frame fMain.

 case pType:
  when "command" then tFile = tFile + ".bat".
  when "request" then tFile = tFile + "-xml.dat".
  when "response" then tFile = tFile + "-srv.dat".
  when "log" then tFile = tFile + "-log.dat".
  when "trace" then tFile = tFile + "-trc.dat".
  when "config" then tFile = tFile + "-ini.dat".
 end case.
 tFilefullpath = search(tFile).
 if tFilefullpath = ? 
  then 
   do: tContents:screen-value in frame fMain = "No content".
       return.
   end.
 tContents:read-file(tFilefullpath).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteTempFiles C-Win 
FUNCTION deleteTempFiles RETURNS LOGICAL PRIVATE
  ( pProcess as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if pProcess = "" 
  then return false.

 os-delete value(pProcess + ".bat").
 os-delete value(pProcess + "-ini.dat").
 os-delete value(pProcess + "-url.dat").
 os-delete value(pProcess + "-srv.dat").
 os-delete value(pProcess + "-log.dat").
 os-delete value(pProcess + "-trc.dat").

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setFields C-Win 
FUNCTION setFields RETURNS LOGICAL PRIVATE
  ( pStat as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 assign
   tAction:sensitive in frame fMain = pStat
   tType:sensitive in frame fMain = pStat
   tQuery:sensitive in frame fMain = pStat
   tPostFile:sensitive in frame fMain = pStat
   tCount:sensitive in frame fMain = pStat
   tPause:sensitive in frame fMain = pStat
   bStart:sensitive in frame fMain = pStat
   bClear:sensitive in frame fMain = pStat
   bExport:sensitive in frame fMain = pStat

   bStop:sensitive in frame fMain = not pStat
   .

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

