&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialog 
/* dialogconfig.w
------------------------------------------------------------------------*/
{lib/std-def.i}

def var tMonth as int no-undo.
def var tYear as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tTempDir tReportsSearch tIterations tPause ~
Btn_OK Btn_Cancel RECT-34 
&Scoped-Define DISPLAYED-OBJECTS tLastIterations tAction tFile tQuery tType ~
tTempDir tIterations tPause tLastPause 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON tReportsSearch 
     LABEL "..." 
     SIZE 4 BY .95.

DEFINE VARIABLE tAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 27.2 BY 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "XML" 
     VIEW-AS FILL-IN 
     SIZE 70 BY 1 NO-UNDO.

DEFINE VARIABLE tIterations AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Default number of iterations" 
     VIEW-AS FILL-IN 
     SIZE 6.6 BY 1 NO-UNDO.

DEFINE VARIABLE tLastIterations AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Iterations" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tLastPause AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Pause" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE tPause AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Default pause duration" 
     VIEW-AS FILL-IN 
     SIZE 6.6 BY 1 NO-UNDO.

DEFINE VARIABLE tQuery AS CHARACTER FORMAT "X(256)":U 
     LABEL "Query" 
     VIEW-AS FILL-IN 
     SIZE 70 BY 1 NO-UNDO.

DEFINE VARIABLE tTempDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Temporary Directory" 
     VIEW-AS FILL-IN 
     SIZE 56.6 BY 1 TOOLTIP "Enter the default directory to save files; blank is session working directory" NO-UNDO.

DEFINE VARIABLE tType AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Get", 1,
"Post", 2
     SIZE 22.4 BY 1.19 NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 6.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 83.6 BY 7.62.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialog
     tLastIterations AT ROW 13.05 COL 12.2 COLON-ALIGNED WIDGET-ID 128
     tAction AT ROW 8.43 COL 12 COLON-ALIGNED WIDGET-ID 114
     tFile AT ROW 11.95 COL 12 COLON-ALIGNED WIDGET-ID 118
     tQuery AT ROW 10.81 COL 12 COLON-ALIGNED WIDGET-ID 116
     tType AT ROW 9.52 COL 14 NO-LABEL WIDGET-ID 120
     tTempDir AT ROW 2.19 COL 21.4 COLON-ALIGNED WIDGET-ID 2
     tReportsSearch AT ROW 2.19 COL 80 WIDGET-ID 30
     tIterations AT ROW 4.38 COL 42.4 COLON-ALIGNED WIDGET-ID 88
     tPause AT ROW 5.62 COL 42.4 COLON-ALIGNED WIDGET-ID 112
     Btn_OK AT ROW 16.29 COL 28.4
     Btn_Cancel AT ROW 16.29 COL 46.4
     tLastPause AT ROW 14.14 COL 12.2 COLON-ALIGNED WIDGET-ID 130
     "Options" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 70
          FONT 6
     "Last Test" VIEW-AS TEXT
          SIZE 11.4 BY .62 AT ROW 7.67 COL 4 WIDGET-ID 126
          FONT 6
     RECT-31 AT ROW 1.48 COL 2 WIDGET-ID 68
     RECT-34 AT ROW 8 COL 2.2 WIDGET-ID 124
     SPACE(1.59) SKIP(2.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialog
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fDialog:SCROLLABLE       = FALSE
       FRAME fDialog:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAction IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tFile IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tLastIterations IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tLastPause IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tQuery IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET tType IN FRAME fDialog
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialog fDialog
ON WINDOW-CLOSE OF FRAME fDialog /* Configuration */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportsSearch fDialog
ON CHOOSE OF tReportsSearch IN FRAME fDialog /* ... */
DO:
  std-ch = tTempDir:screen-value.
  if tTempDir:screen-value > ""
    then system-dialog get-dir std-ch
           initial-dir std-ch.
    else system-dialog get-dir std-ch.
  if std-ch > "" and std-ch <> ?
   then tTempDir:screen-value = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialog 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetTempDir" (output tTempDir).
publish "GetIterations" (output tIterations).
publish "GetPause" (output tPause).

publish "GetLastAction" (output tAction).
publish "GetLastType" (output tType).
publish "GetLastQuery" (output tQuery).
publish "GetLastFile" (output tFile).
publish "GetLastIterations" (output tLastIterations).
publish "GetLastPause" (output tLastPause).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  assign
   tTempDir = tTempDir:screen-value in frame fDialog
   tIterations = tIterations:input-value in frame fDialog
   tPause = tPause:input-value in frame fDialog
   .
  
  if tTempDir:screen-value > ""
   then
    do: file-info:file-name = tTempDir:screen-value in frame fDialog.
        if file-info:full-pathname = ?
         or index(file-info:file-type, "D") = 0 
         or index(file-info:file-type, "W") = 0
         then
          do: 
              MESSAGE "Invalid Directory"
               VIEW-AS ALERT-BOX error BUTTONS OK.
              undo MAIN-BLOCK, retry MAIN-BLOCK.
          end.
         else tTempDir:screen-value in frame fDialog = file-info:full-pathname.
    end.

  publish "SetTempDir" (tTempDir:screen-value).
  publish "SetIterations" (tIterations:input-value in frame fDialog).
  publish "SetPause" (tPause:input-value in frame fDialog).
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tLastIterations tAction tFile tQuery tType tTempDir tIterations tPause 
          tLastPause 
      WITH FRAME fDialog.
  ENABLE tTempDir tReportsSearch tIterations tPause Btn_OK Btn_Cancel RECT-34 
      WITH FRAME fDialog.
  VIEW FRAME fDialog.
  {&OPEN-BROWSERS-IN-QUERY-fDialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

