&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        :  ratesheet.p
    Purpose     :  Used by rate calculator desktop UI and web UI.

    Syntax      :

    Description :

    Author(s)   :  Vikas Jain
    Created     :  08-16-2018
    Notes       :
    @Modification:
    Date          Name      Description
    08/23/2018    Anjly     Testing bug fix.
    08/28/2018    Vikas     PDF formatting.
    08/30/2018    Anjly     Testing bug fix.
    09/12/2018    Vikas     Layout fixed
    09/28/2018    Vikas     Testing bug fix.
    11/22/2018    Vikas     Testing bug fix.
    11/24/2018    Vikas     Modified to generate error if amount exceeds the 
                            allowed limit.
    03/26/2019    Anubha    Added Effective date.     
    06/18/2019    Vikas     Added field for Loan Prior effective date  
    09/12/2019    Anubha    Increased display format of endorsement code
    02/22/2022    Vignesh   Modified to remove the invalid characters from 
                            the file name.
    10/19/2022    S Chandu  Modified to added Commitment Pdf label changes.
    05/13/2024    Vignesh   Task 112559 - New Mexico Version 2 Implementation
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/
/* Parameters Definitions ---                                           */
define input-output parameter pFilename as character no-undo.
define input parameter pState           as character no-undo. 
define input parameter pResulta         as character no-undo. 
define input parameter pUiInfo          as character no-undo. 
define input parameter pInputParameters as character no-undo. 

{lib/rpt-def.i}
{lib/rcm-std-def.i}
{lib/normalizefileid.i}
/* result premium parameter parsing variables */
define variable premiumOwner              as character no-undo initial "premiumOwner".           
define variable premiumOEndors            as character no-undo initial "premiumOEndors".        
define variable premiumOEndorsDetail      as character no-undo initial "premiumOEndorsDetail".   
define variable premiumLoan               as character no-undo initial "premiumLoan".            
define variable premiumLEndors            as character no-undo initial "premiumLEndors".         
define variable premiumLEndorsDetail      as character no-undo initial "premiumLEndorsDetail".   
define variable premiumScnd               as character no-undo initial "premiumScnd".            
define variable premiumSEndors            as character no-undo initial "premiumSEndors".         
define variable premiumSEndorsDetail      as character no-undo initial "premiumSEndorsDetail".   
define variable premiumLenderCPL          as character no-undo initial "premiumLenderCPL".       
define variable premiumBuyerCPL           as character no-undo initial "premiumBuyerCPL".        
define variable premiumSellerCPL          as character no-undo initial "premiumSellerCPL".       
define variable premiumCPL                as character no-undo initial "premiumCPL".             
         
/* Parameters - parse the calculator UI input parameter and store the information into these variables. */
define variable rateTypeCode              as character no-undo initial "rateTypeCode".
define variable rateType                  as character no-undo initial "rateType".
define variable coverageAmount            as character no-undo initial "coverageAmount".                    
define variable reissueCoverageAmount     as character no-undo initial "reissueCoverageAmount".
define variable priorEffectiveDate        as character no-undo initial "priorEffectiveDate".
define variable loanRateTypeCode          as character no-undo initial "loanRateTypeCode".
define variable loanRateType              as character no-undo initial "loanRateType".
define variable loanReissueCoverageAmount as character no-undo initial "loanReissueCoverageAmount".
define variable loanPriorEffectiveDate    as character no-undo initial "loanPriorEffectiveDate".
define variable loanCoverageAmount        as character no-undo initial "loanCoverageAmount".
define variable secondLoanRateTypeCode    as character no-undo initial "secondLoanRateTypeCode".
define variable secondLoanRateType        as character no-undo initial "secondLoanRateType".
define variable secondLoanCoverageAmount  as character no-undo initial "secondLoanCoverageAmount".  
define variable ownerEndors               as character no-undo initial "ownerEndors".
define variable loanEndors                as character no-undo initial "loanEndors".
define variable secondLoanEndors          as character no-undo initial "secondLoanEndors".
define variable Version                   as character no-undo initial "Version".

/* General UI parameter parsing variables */
define variable fileNumber                as character no-undo initial "FileNumber".
define variable seller                    as character no-undo initial "Seller".
define variable buyer                     as character no-undo initial "Buyer".
define variable reference                 as character no-undo initial "Reference".
define variable property                  as character no-undo initial "Property".
define variable state                     as character no-undo initial "State".
define variable county                    as character no-undo initial "County".
define variable batchProcess              as character no-undo initial "batchProcess".

define variable ctempID                   as character no-undo.
define variable lSuccess                  as logical   no-undo.
define variable iCntParameters            as integer   no-undo.
define variable lBatchProcess             as logical   no-undo .
define variable loPrOwner                 as logical   no-undo.
define variable loPrLoan                  as logical   no-undo.
define variable loPrScnd                  as logical   no-undo.
define variable loPrTotal                 as logical   no-undo.
define variable loTrkNewLIne              as logical   no-undo.
define variable cCommitmentState          as character no-undo initial "New Mexico".
define variable cStateID                  as character no-undo initial "NM".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 12.29
         WIDTH              = 59.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
run extractOutputParameters.
run extractParameters.
run extractInputParameters.

assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .
run getFilename in this-procedure no-error.
if error-status:error 
 then return.

{lib/rpt-open.i &tempID=ctempID}

run section0 in this-procedure.
if rateTypeCode ne {&NoRatetype} then
  run section1 in this-procedure.
if LoanrateTypeCode ne {&NoRatetype} then 
  run section2 in this-procedure.
if secondLoanRateTypecode ne {&NoRatetype} then
  run section3 in this-procedure.
if decimal(premiumLenderCPL) > 0 or decimal(premiumBuyerCPL) > 0 or decimal(premiumSellerCPL) > 0 or decimal(premiumCPL) > 0 then
  run section4 in this-procedure.
run section5 in this-procedure.
      

if lBatchProcess then
do:
 {lib/rpt-cloz.i &tempID=ctempID &no-save=true &exclude-pdfview=true}
end.
else
do:
 {lib/rpt-cloz.i &tempID=ctempID}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-extractInputParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractInputParameters Procedure 
PROCEDURE extractInputParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
do iCntParameters = 1 to num-entries(pInputParameters,","):

case entry(1,entry(iCntParameters,pInputParameters,","),"^"):
    
    when fileNumber then
         fileNumber         = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.
    
    when seller then
         seller             = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.
    
    when buyer  then
         buyer              = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.
    
    when reference  then
         reference          = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.
    
    when property  then
         property           = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.
    
    when state  then
         state              = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.
    
    when county  then                            
         county             = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error.  

    when batchProcess  then                            
         batchProcess       = entry(2,entry(iCntParameters,pInputParameters,","),"^") no-error. 

  end case.  
end.

 if fileNumber = "fileNumber" then
     fileNumber = "".
 if seller = "seller" then
     seller = "".
 if buyer = "buyer" then
     buyer = "".
 if reference = "reference" then
     reference = "".
 if property = "property" then
     property = "".
 if state = "state" then
     state = "".
 if county = "county" then
     county = "".
 if batchProcess = "batchProcess" then
   lBatchProcess = false.

 lBatchProcess = logical(batchProcess) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-extractOutputParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractOutputParameters Procedure 
PROCEDURE extractOutputParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iCount as integer no-undo.
 
do iCntParameters = 1 to num-entries(pResulta,","):

case entry(1,entry(iCntParameters,pResulta,","),"^"):
  
    when premiumOwner  then
         premiumOwner                  = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumOEndors  then
         premiumOEndors            = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumOEndorsDetail  then
         premiumOEndorsDetail     = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumLoan then
         premiumLoan              = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumLEndors  then
         premiumLEndors        = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumLEndorsDetail  then
         premiumLEndorsDetail = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumScnd  then
         premiumScnd        = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumSEndors  then
         premiumSEndors  = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumSEndorsDetail  then
         premiumSEndorsDetail               = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumLenderCPL then
         premiumLenderCPL                = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumBuyerCPL then
         premiumBuyerCPL          = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumSellerCPL  then
         premiumSellerCPL              = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
    when premiumCPL  then
         premiumCPL                   = entry(2,entry(iCntParameters,pResulta,","),"^") no-error.
    
  end case.  
end.
 
 if  premiumOwner = "premiumOwner" then
     premiumOwner = "0".
 if  premiumOEndors = "premiumOEndors" then
     premiumOEndors = "0".
 if  premiumLoan = "premiumLoan" then
     premiumLoan = "0".
 if  premiumLEndors = "premiumLEndors" then
     premiumLEndors = "0".
 if  premiumScnd = "premiumScnd" then
     premiumScnd = "0".
 if  premiumSEndors = "premiumSEndors" then
     premiumSEndors = "0".
 if  premiumLenderCPL = "premiumLenderCPL" then
     premiumLenderCPL = "0".
 if  premiumBuyerCPL = "premiumBuyerCPL" then
     premiumBuyerCPL = "0".
 if  premiumSellerCPL = "premiumSellerCPL" then
     premiumSellerCPL = "0".
 if  premiumCPL = "premiumCPL" then
     premiumCPL = "0".
 if premiumOEndorsDetail = "premiumOEndorsDetail" then
     premiumOEndorsDetail = "".
 if premiumLEndorsDetail = "premiumLEndorsDetail" then
     premiumLEndorsDetail = "".
 if premiumSEndorsDetail = "premiumSEndorsDetail" then
     premiumSEndorsDetail = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-extractParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractParameters Procedure 
PROCEDURE extractParameters :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do iCntParameters = 1 to num-entries(pUiInfo,","):

case entry(1,entry(iCntParameters,pUiInfo,","),"^"):
  
    when rateTypeCode  then
         rateTypeCode              = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.

    when rateType  then
         rateType                  = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when coverageAmount  then
         coverageAmount            = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when reissueCoverageAmount  then
         reissueCoverageAmount     = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
  
    when loanRateTypeCode then
         loanRateTypeCode              = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when loanRateType then
         loanRateType              = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when loanCoverageAmount  then
         loanCoverageAmount        = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when loanReissueCoverageAmount  then
         loanReissueCoverageAmount = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when secondLoanRateTypeCode  then
         secondLoanRateTypeCode        = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.

    when secondLoanRateType  then
         secondLoanRateType        = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when secondLoanCoverageAmount  then
         secondLoanCoverageAmount  = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when ownerEndors  then
         ownerEndors               = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when loanEndors then
         loanEndors                = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when secondLoanEndors then
         secondLoanEndors          = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.
    
    when Version  then
         Version                   = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.  

    when loanPriorEffectiveDate  then
         loanPriorEffectiveDate    = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.  

    when priorEffectiveDate  then
         priorEffectiveDate        = entry(2,entry(iCntParameters,pUiInfo,","),"^") no-error.  
    
  end case.  
end.
 if rateTypeCode = "rateTypeCode" or rateTypeCode = {&NoRatetype}then
  assign
   rateTypeCode = {&NoRatetype}
     ratetype   = "".

 if loanRateTypeCode = "loanRateTypeCode" or loanRateTypeCode = {&NoRatetype}then
  assign
   loanRateTypeCode = {&NoRatetype}
     loanratetype   = "".

 if secondLoanRateTypeCode = "secondLoanRateTypeCode" or secondLoanRateTypeCode = {&NoRatetype}then
  assign
   secondLoanRateTypeCode = {&NoRatetype}
     secondloanratetype   = "".

 if reissueCoverageAmount = "reissueCoverageAmount" then
     reissueCoverageAmount = "0".
 if loanReissueCoverageAmount = "loanReissueCoverageAmount" then
     loanReissueCoverageAmount = "0".
 
 if loanPriorEffectiveDate = "loanPriorEffectiveDate" then
     loanPriorEffectiveDate = "".

 if priorEffectiveDate = "priorEffectiveDate" then
     priorEffectiveDate = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getFilename Procedure 
PROCEDURE getFilename PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tFile      as character no-undo.
 define variable doSave     as logical   no-undo initial true.
 define variable tReportDir as character no-undo.
 define variable tFileId    as character no-undo.
 
 if pFilename = ""
 then
 do:
   publish "GetSystemParameter" ("dataRoot", OUTPUT tReportDir).
   if tReportDir = ""
     then publish "GetReportDir" (output tReportDir).
   if length(tReportDir) > 2 then
   if substring(tReportDir,length(tReportDir) , 1) <> "\"
     then tReportDir = tReportDir + "\".
     
   tFileId = normalizeFileID(fileNumber).
   pFilename = tReportDir + pState + "_Premium_Rate_Quote" + (if tFileId <> "" and tFileId <> "?" then ("_for_file_" + tFileId) else "") + ".pdf".
 end.
 activeFilename = pFilename.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportFooter) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportFooter Procedure 
PROCEDURE reportFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/rpt-ftr-rc.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-reportHeader) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reportHeader Procedure 
PROCEDURE reportHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tTitle as character no-undo.
 define variable tXpos  as integer   no-undo.

 tTitle = "PREMIUM RATE QUOTE".
 tXPos = 402.
 
 {lib/rpt-hdr-rc.i &title=tTitle &title-x=tXPos &title-y=730}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-section0) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE section0 Procedure 
PROCEDURE section0 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  DEFINE VARIABLE chOwnerTotalPremium      AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE chLoanTotalPremium       AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE chSecondLoanTotalPremium AS CHARACTER   NO-UNDO.
  DEFINE VARIABLE chTotalPremium           AS CHARACTER   NO-UNDO.

  chOwnerTotalPremium = string(decimal(premiumOwner) + decimal(premiumOEndors), "zzz,zz9.99") no-error. 
  if error-status:error or error-status:num-messages > 0 then
    chOwnerTotalPremium = "Error".

  chLoanTotalPremium = string(decimal(premiumLoan) + decimal(premiumLEndors), "zzz,zz9.99") no-error.
  if error-status:error or error-status:num-messages > 0 then
      chLoanTotalPremium = "Error".    

  chSecondLoanTotalPremium = string(decimal(premiumScnd) + decimal(premiumSEndors), "zzz,zz9.99") no-error.
  if error-status:error or error-status:num-messages > 0 then
      chSecondLoanTotalPremium = "Error".

  if pState = 'New Mexico' 
   then
    chTotalPremium = string(round(decimal(premiumOwner) + decimal(premiumOEndors) + decimal(premiumLoan) + decimal(premiumLEndors) + decimal(premiumScnd) + decimal(premiumSEndors),0), "zzz,zz9.99") no-error.
   else
    chTotalPremium = string(decimal(premiumOwner) + decimal(premiumOEndors) + decimal(premiumLoan) + decimal(premiumLEndors) + decimal(premiumScnd) + decimal(premiumSEndors), "zzz,zz9.99") no-error.
  if error-status:error or error-status:num-messages > 0 then
      chTotalPremium = "Error".

  setFont("Helvetica", 10.0).
  if fileNumber ne "" then
  do:
    loTrkNewLIne = true.
    run pdf_text_align ({&rpt},string("File Number: "),"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(fileNumber),"LEFT",120, pdf_TextY ({&rpt})).
    if not(loPrOwner) and rateTypeCode ne "none" then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).      
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium, decimal(premiumSEndors), "zzz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.    
  end.
  if seller ne "" then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("Seller: "),"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(seller),"LEFT",120, pdf_TextY ({&rpt})). 
    if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.   
  end.
  if buyer ne "" then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("Buyer: ") ,"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(buyer),"LEFT",120, pdf_TextY ({&rpt})). 
    if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium, "RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.    
  end.
  if reference ne "" then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("Reference: "),"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(reference),"LEFT",120, pdf_TextY ({&rpt})). 
    if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.    
  end.
  if property ne "" then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("Property: "),"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(property),"LEFT",120, pdf_TextY ({&rpt})). 
    if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.    
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.   
  end.  
  if state ne "" then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("State: "),"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(state),"LEFT",120, pdf_TextY ({&rpt})). 
    if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.    
  end.
  if county ne "" then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("County: "),"RIGHT",113, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(county),"LEFT",120, pdf_TextY ({&rpt})). 
  if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Owner Policy:  $") ,"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrOwner = true.
    end.
    else if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 
      loPrLoan = true.
    end.
    else if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
    do:
      run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).
      loPrScnd = true.
    end.
    else if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
    do:
      run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},chTotalPremium, "RIGHT",550, pdf_TextY ({&rpt})).                                                                                                       
      loPrTotal = true.
    end.    
  end.

  if not(loPrOwner) and rateTypeCode ne {&NoRatetype} then
  do:      
    run pdf_text_align ({&rpt},string("Owner Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).        
    run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).   
    loPrOwner = true.
    loTrkNewLine = true.
  end.
  if not(loPrLoan) and loanrateTypeCode ne {&NoRatetype} then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.    
    run pdf_text_align ({&rpt},string("Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).           
    run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).    
    loPrLoan = true.    
  end.
  if not(loPrScnd) and secondloanrateTypeCode ne {&NoRatetype} then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).         
    run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).    
    loPrScnd = true.    
  end.
  if not(loPrTotal) and (rateTypeCode ne {&NoRatetype} or loanrateTypeCode ne {&NoRatetype} or secondLoanrateTypeCode ne {&NoRatetype}) then
  do:
    if loTrkNewLine = true then
      newLine(1).
    else
      loTrkNewLine = true.
    run pdf_text_align ({&rpt},string("Total Premium:  $"),"RIGHT",490, pdf_TextY ({&rpt})).    
    run pdf_text_align ({&rpt},chTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})).    
    loPrTotal = true.    
  end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-section1) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE section1 Procedure 
PROCEDURE section1 PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iCountEndors as integer no-undo.
 define variable iXPos as integer no-undo.
 define variable endorsCode as character no-undo.
 define variable endorsAmount as character no-undo.
 define variable deEndorsCode as decimal no-undo.
 define variable endorsLine as decimal no-undo.

 define variable chOwnerTotalPremium as character   no-undo. 
  
 premiumOwner = string(decimal(premiumOwner), "zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    premiumOwner = "Error".

 premiumOEndors = string(decimal(premiumOEndors),"zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    premiumOEndors = "Error".

 chOwnerTotalPremium = string(decimal(premiumOwner) + decimal(premiumOEndors), "zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    chOwnerTotalPremium = "Error".

 blockTitle-rc("OWNER POLICY"). 
 textColor(0.0, 0.0, 0.0).
 
 setFont("Helvetica", 10.0).
  
 run pdf_text_align ({&rpt},(string("Coverage Amount:  ") + string(decimal(coverageAmount), "$zzz,zzz,zz9")),"LEFT",56, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},string("Rate Type:  " + rateType),"LEFT",256, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},string("Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
 run pdf_text_align ({&rpt},premiumOwner,"RIGHT",550, pdf_TextY ({&rpt})). 
 newLine(1).

 if decimal(reissueCoverageAmount) > 0 then
 do:
   run pdf_text_align ({&rpt},(string("Prior Policy Amount:  ") + string(decimal(reissueCoverageAmount), "$zzz,zzz,zz9")),"LEFT",56, pdf_TextY ({&rpt})).   
   newLine(1).
 end. 

 if priorEffectiveDate <> "" then
 do:
   run pdf_text_align ({&rpt},(string("Effective Date:  ") + priorEffectiveDate),"LEFT",56, pdf_TextY ({&rpt})).   
   newLine(1).
 end.

 if num-entries(premiumOEndorsDetail , "|") > 0 then
 do:

   endorsLine = num-entries(premiumOEndorsDetail , "|") / 3.
   endorsLine = trunc(endorsLine,0) + (if (num-entries(premiumOEndorsDetail , "|") modulo 3) = 0 then 1 else 2).
   run pdf_rgb ({&rpt}, "pdf_stroke_fill", "231230230").
   run pdf_rgb ({&rpt}, "pdf_stroke_color", "231230230").
   run pdf_rect ({&rpt}, 50, 707 - ((iLine + endorsLine - 3) * 10), 511, (10 * endorsLine), 0.0). 
   
   ixPos = 60.   
   run pdf_text_align ({&rpt},string("Endorsements:  "),"LEFT",56, pdf_TextY ({&rpt})).
   newLine(1).  
   run pdf_rgb ({&rpt}, "pdf_stroke_fill", "231230230").
   run pdf_rgb ({&rpt}, "pdf_stroke_color", "231230230").
   
   do iCountEndors = 1 to num-entries(premiumOEndorsDetail , "|"):

       assign
           endorsCode   = string(trim(entry(1,entry(iCountEndors,premiumOEndorsDetail,"|"),"-")), "X(7)")
           endorsAmount = string(decimal(entry(2,entry(iCountEndors,premiumOEndorsDetail,"|"),"-")), "zzz,zz9.99") no-error.
        if error-status:error or error-status:num-messages > 0 then
          endorsAmount = "Error".
        
       run pdf_text_align ({&rpt},endorsCode,"LEFT",ixPos, pdf_TextY ({&rpt})).       
       run pdf_text_align ({&rpt},"$","LEFT",ixPos + 45, pdf_TextY ({&rpt})).       
       run pdf_text_align ({&rpt},endorsAmount,"RIGHT",ixPos + 102, pdf_TextY ({&rpt})).

       ixPos = ixPos + 150.
       if (iCountEndors modulo 3 = 0) then
       do:           
           ixPos = 60.
           newLine(1).
       end.  
   end.
   if (iCountEndors modulo 3 ne 1) then 
     newLine(1).

   run pdf_text_align ({&rpt},string("Endorsements:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
   run pdf_text_align ({&rpt},premiumOEndors,"RIGHT",550, pdf_TextY ({&rpt})).
   newLine(1).
end.   
 
 run pdf_text_align ({&rpt},string("Total premium for Owner Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
 run pdf_text_align ({&rpt},chOwnerTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-section2) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE section2 Procedure 
PROCEDURE section2 PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iCountEndors as integer no-undo.
 define variable iXPos as integer no-undo.
 define variable endorsCode as character no-undo.
 define variable endorsAmount as character no-undo.
 define variable endorsLine   as decimal no-undo.

 define variable chLoanTotalPremium as character   no-undo. 
  
 premiumLoan = string(decimal(premiumLoan), "zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    premiumLoan = "Error".

 premiumLEndors = string(decimal(premiumLEndors),"zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    premiumLEndors = "Error".

 chLoanTotalPremium = string(decimal(premiumLoan) + decimal(premiumLEndors), "zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    chLoanTotalPremium = "Error".
 
 blockTitle-rc("LOAN POLICY"). 
 textColor(0.0, 0.0, 0.0).
 
 setFont("Helvetica", 10.0).
  
 run pdf_text_align ({&rpt},(string("Coverage Amount:  ") + string(decimal(loanCoverageAmount), "$zzz,zzz,zz9")),"LEFT",56, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},string("Rate Type:  " + loanRateType),"LEFT",256, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},string("Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
 run pdf_text_align ({&rpt},premiumLoan,"RIGHT",550, pdf_TextY ({&rpt})). 
 newLine(1).

 if decimal(loanReissueCoverageAmount) > 0 then
 do:
   run pdf_text_align ({&rpt},(string("Prior Policy Amount:  ") + string(decimal(loanReissueCoverageAmount), "$zzz,zzz,zz9")),"LEFT",56, pdf_TextY ({&rpt})).   
   newLine(1).
 end. 

 if loanPriorEffectiveDate <> "" then
 do:
   run pdf_text_align ({&rpt},(string("Effective Date:  ") + loanPriorEffectiveDate),"LEFT",56, pdf_TextY ({&rpt})).   
   newLine(1).
 end. 
 
 if num-entries(premiumLEndorsDetail , "|") > 0 then
 do:   
   /*newLine(1).*/
   endorsLine = num-entries(premiumLEndorsDetail , "|") / 3.   
   endorsLine = trunc(endorsLine,0) + (if (num-entries(premiumLEndorsDetail , "|") modulo 3) = 0 then 1 else 2).

   run pdf_rgb ({&rpt}, "pdf_stroke_fill", "231230230").
   run pdf_rgb ({&rpt}, "pdf_stroke_color", "231230230"). 
   run pdf_rect ({&rpt}, 50, 707 - ((iLine + endorsLine - 3) * 10), 511, (10 * endorsLine), 0.0). 
   ixPos = 60.   
   run pdf_text_align ({&rpt},string("Endorsements:  "),"LEFT",56, pdf_TextY ({&rpt})).
   newLine(1).   
   do iCountEndors = 1 to num-entries(premiumLEndorsDetail , "|"):

       assign
           endorsCode   = string(entry(1,entry(iCountEndors,premiumLEndorsDetail,"|"),"-"),"X(7)")
           endorsAmount = string(decimal(entry(2,entry(iCountEndors,premiumLEndorsDetail,"|"),"-")), "zzz,zz9.99") no-error.       
       if error-status:error or error-status:num-messages > 0 then
           endorsAmount = "Error".
       
       run pdf_text_align ({&rpt},endorsCode,"LEFT",ixPos, pdf_TextY ({&rpt})).       
       run pdf_text_align ({&rpt},"$","LEFT",ixPos + 45, pdf_TextY ({&rpt})).       
       run pdf_text_align ({&rpt},endorsAmount,"RIGHT",ixPos + 102, pdf_TextY ({&rpt})).

       ixPos = ixPos + 150.
       if (iCountEndors modulo 3 = 0) then
       do:
           ixPos = 60.
           newLine(1).
       end.  
   end.
   if (iCountEndors modulo 3 ne 1) then
     newLine(1).
   run pdf_text_align ({&rpt},string("Endorsements:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
   run pdf_text_align ({&rpt},premiumLEndors,"RIGHT",550, pdf_TextY ({&rpt})).
   newLine(1).
end.   
  
 run pdf_text_align ({&rpt},string("Total premium for Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
 run pdf_text_align ({&rpt},chLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-section3) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE section3 Procedure 
PROCEDURE section3 PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iCountEndors as integer no-undo.
 define variable iXPos as integer no-undo.
 define variable endorsCode as character no-undo.
 define variable endorsAmount as character no-undo.
 define variable endorsLine as decimal no-undo.

 define variable chSecondLoanTotalPremium as character   no-undo. 
  
 premiumScnd = string(decimal(premiumScnd), "zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    premiumScnd = "Error".

 premiumSEndors = string(decimal(premiumSEndors),"zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    premiumSEndors = "Error".

 chSecondLoanTotalPremium = string(decimal(premiumScnd) + decimal(premiumSEndors), "zzz,zz9.99") no-error.
 if error-status:error or error-status:num-messages > 0 then
    chSecondLoanTotalPremium = "Error".
 
 blockTitle-rc("SECOND LOAN POLICY"). 
 textColor(0.0, 0.0, 0.0).
 
 setFont("Helvetica", 10.0).
  
 run pdf_text_align ({&rpt},(string("Coverage Amount:  ") + string(decimal(secondLoanCoverageAmount), "$zzz,zzz,zz9")),"LEFT",56, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},string("Rate Type:  " + secondLoanRateType),"LEFT",256, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},string("Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})). 
 run pdf_text_align ({&rpt},premiumScnd,"RIGHT",550, pdf_TextY ({&rpt})).
 newLine(1).
 
 
 if num-entries(premiumSEndorsDetail , "|") > 0 then
 do:     
   /*newLine(1).*/
   endorsLine = num-entries(premiumSEndorsDetail , "|") / 3.
   endorsLine = trunc(endorsLine,0) + (if (num-entries(premiumSEndorsDetail , "|") modulo 3) = 0 then 1 else 2).
   run pdf_rgb ({&rpt}, "pdf_stroke_fill", "231230230").
   run pdf_rgb ({&rpt}, "pdf_stroke_color", "231230230").
   run pdf_rect ({&rpt}, 50, 707 - ((iLine + endorsLine - 3) * 10), 511, (10 * endorsLine), 0.0). 

   ixPos = 60.   
   run pdf_text_align ({&rpt},string("Endorsements: "),"LEFT",56, pdf_TextY ({&rpt})).
   newLine(1).   
   do iCountEndors = 1 to num-entries(premiumSEndorsDetail , "|"):

       assign
           endorsCode   = string(entry(1,entry(iCountEndors,premiumSEndorsDetail,"|"),"-"),"X(7)")
           endorsAmount = string(decimal(entry(2,entry(iCountEndors,premiumSEndorsDetail,"|"),"-")), "zzz,zz9.99") no-error.
       if error-status:error or error-status:num-messages > 0 then
           endorsAmount = "Error".
       
       run pdf_text_align ({&rpt},endorsCode,"LEFT",ixPos, pdf_TextY ({&rpt})).       
       run pdf_text_align ({&rpt},"$","LEFT",ixPos + 45, pdf_TextY ({&rpt})).       
       run pdf_text_align ({&rpt},endorsAmount,"RIGHT",ixPos + 102, pdf_TextY ({&rpt})).

       ixPos = ixPos + 150.
       if (iCountEndors modulo 3 = 0) then
       do:
           ixPos = 60.
           newLine(1).
       end.  
   end.
   if (iCountEndors modulo 3 ne 1) then
     newLine(1).
   run pdf_text_align ({&rpt},string("Endorsements:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
   run pdf_text_align ({&rpt},premiumSEndors,"RIGHT",550, pdf_TextY ({&rpt})).
   newLine(1).
end.   

 run pdf_text_align ({&rpt},string("Total premium for Second Loan Policy:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
 run pdf_text_align ({&rpt},chSecondLoanTotalPremium,"RIGHT",550, pdf_TextY ({&rpt})). 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-section4) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE section4 Procedure 
PROCEDURE section4 PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if (lookup(pState, cCommitmentState) > 0 or lookup(pState, cStateID) > 0 ) 
  then
   do:
     blockTitle-rc("COMMITMENT").
     textColor(0.0, 0.0, 0.0).
     
      setFont("Helvetica", 10.0).
      
     if decimal(premiumLenderCPL) > 0 then
      do:
        run pdf_text_align ({&rpt},string("Commitment:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
        run pdf_text_align ({&rpt},string(decimal(premiumLenderCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).
        newLine(1).
      end. 
     
     run pdf_text_align ({&rpt},string("Total for Commitment:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
     run pdf_text_align ({&rpt},string(decimal(premiumLenderCPL) + decimal(premiumBuyerCPL) + decimal(premiumSellerCPL) + decimal(premiumCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).
   end.
 else                        
  do:            
    blockTitle-rc("CLOSING PROTECTION LETTER").
    textColor(0.0, 0.0, 0.0).
    
    setFont("Helvetica", 10.0).
    
    if decimal(premiumLenderCPL) > 0 then
    do:
      run pdf_text_align ({&rpt},string("Lender:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},string(decimal(premiumLenderCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).
      newLine(1).
    end.
    if decimal(premiumBuyerCPL) > 0 then
    do:
      run pdf_text_align ({&rpt},string("Buyer:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},string(decimal(premiumBuyerCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).
      newLine(1).
    end.
    if decimal(premiumSellerCPL) > 0 then
    do:
      run pdf_text_align ({&rpt},string("Seller:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},string(decimal(premiumSellerCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).
      newLine(1).
    end.
    if decimal(premiumCPL) > 0 then
    do:
      run pdf_text_align ({&rpt},string("CPL:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
      run pdf_text_align ({&rpt},string(decimal(premiumCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})).
      newLine(1).
    end.
    
    run pdf_text_align ({&rpt},string("Total for Closing Protection Letters:  $"),"RIGHT",490, pdf_TextY ({&rpt})).
    run pdf_text_align ({&rpt},string(decimal(premiumLenderCPL) + decimal(premiumBuyerCPL) + decimal(premiumSellerCPL) + decimal(premiumCPL), "zz,zz9.99"),"RIGHT",550, pdf_TextY ({&rpt})). 
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-section5) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE section5 Procedure 
PROCEDURE section5 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

newLine(2).
setFont("Helvetica", 8.0).

 textAt ("The Alliant National Title Calculator (ANTC) is an Internet-based platform, which provides our customers with a user-friendly method of",21).
 newLine(1).
 textAt ("obtaining estimates for certain categories of settlement related costs. There may be variables that need to be considered in determining",21).
 newLine(1).
 textAt ("the final rate to be charged, including geographic and transaction-specific items, which are beyond the functionality provided by the ANTC.",21).
 newLine(1).
 textAt ("All estimates obtained using this calculator are dependent upon the accuracy of the information input into the calculator and no guarantee",21).
 newLine(1).
 textAt ("of issuance is expressed or implied. Please contact your local Alliant National Title agent to confirm your quote. Contact information for",21).
 newLine(1).
 textAt ("Alliant National Title agent offices in your area is available at www.alliantnational.com.",21).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

