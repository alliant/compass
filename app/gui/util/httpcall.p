&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : util/httpcall.p
    Purpose     : perform a generic HTTP get or post
    Author(s)   : 
    Created     : 05.26.2012
    Notes       : All temporary filenames are NOT fully-qualified

    Example     : The input parameters are set as follows:
                   pName = Prefix for filenames (optional, usually a GUID)
                   pURL = Full URL (including query-string) to call
                   pHeaders = chr(0) delimited text to be added as HTTP headers
                   pContentType = Type of data in pDataFile (text/xml or text/json)
                   pDataFile = Fully-qualified path to file to submit
                   
                  This example will result in up to 6 files:
                   [GUID]CMD.bat  Windows BATch file executed silently
                   [GUID]INI      Configuration of WGET options (INI file)
                   [GUID]URL      Complete Service URL
                   [GUID]LOG      Log file from WGET command (optional)
                   [GUID]TRC      Trace file from this procedure
                   [GUID]RES      Response file from server (XML)
                    *the suffixes are used only when debug is true
                   
                  The output parameters will be set as follows:
                   responseStatus    True (Success), False (Failure somewhere)
                   responseMsg       Text message suitable to present to user
                   responseFile      Fully-qualified path to Response file
  ----------------------------------------------------------------------*/
 
def input parameter pMethod          as char no-undo. /* Get or Post */
def input parameter pServiceAddrType as char no-undo. /* 'do' or '' */
def input parameter pAction          as char no-undo. /* sysaction.actionID */
def input parameter pContentType     as char no-undo. /* HTTP content-type such as "text/xml" only required if DataFile is non-blank */
def input parameter pDataFile        as char no-undo. /* full-path to data file to send to server that is expected to be of type content-type */
def input parameter pQuery           as char no-undo. /* HTTP-encoded name=value[&name=value]* excluding initial "?" */
{lib/rpt-getparams.i}                                 /* structure to support I9 and I10 parameters */

def output parameter responseStatus as logical init false.
def output parameter responseMsg    as char.
def output parameter clientResponse as util.HttpResponse.

def var tService as char no-undo.
def var tTimeout as char no-undo.
def var tUser as char no-undo.
def var tPw as char no-undo.
def var tMyID as char no-undo.
def var tDestination as char no-undo.

def var httptool  as char no-undo.
def var ctoolName as char no-undo.

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/encrypt.i}

def var tIniFile as char no-undo.
def var tCmdFile as char no-undo.
def var tUrlFile as char no-undo.
def var tLogFile as char no-undo.
def var tResponseFile as char no-undo.
def var tTraceFile as char no-undo.
def var tBodyFile as char no-undo.
def var tHeadersFile as char no-undo.

def stream parseStream.

def var tDebugging as logical init false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-createCmdFile-get) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createCmdFile-get Procedure 
FUNCTION createCmdFile-get RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createCmdFile-post) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createCmdFile-post Procedure 
FUNCTION createCmdFile-post RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createINIFile-get) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createINIFile-get Procedure 
FUNCTION createINIFile-get RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createINIfile-post) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createINIfile-post Procedure 
FUNCTION createINIfile-post RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createURLfile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createURLfile Procedure 
FUNCTION createURLfile RETURNS LOGICAL PRIVATE
 () FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

etime(true).

assign
  std-ch        = replace(guid, "-", "")
  tIniFile      = pAction + std-ch + "INI"
  tCmdFile      = pAction + std-ch + "CMD" + ".bat"
  tUrlFile      = pAction + std-ch + "URL"
  tLogFile      = pAction + std-ch + "LOG"
  tResponseFile = pAction + std-ch + "RES"
  tTraceFile    = pAction + std-ch + "TRC"
  tBodyFile     = pAction + std-ch + "BDY"
  tHeadersFile  = pAction + std-ch + "HDR"
  .
  
tDestination = 'I8='    + rpt-behavior        +
               '&I8a='  + rpt-notifyRequestor +
               '&I9='   + rpt-destination     +
               '&I9e='  + rpt-email           +
               '&I9p='  + rpt-printer         +
               '&I9s='  + rpt-repository      +
               '&I10='  + rpt-outputFormat.
               
  
publish "DeleteActionTempFiles" (pAction). /* httpget*/

publish "GetCredentials" (output tUser, output tPw).
publish "GetCurrentValue" ("ApplicationID", output tMyID).
publish "GetServiceAddress" (output tService).

publish "GetServiceTimeout" (output tTimeout). /* httpget*/

session:set-wait-state("general").

DO-BLOCK:
do:
    if tService = "" or tService = ?
     then
      do: trace("Service Host is not set for: " + pAction).
          responseMsg = "Service Address not set.  Contact the System Administrator.".
          leave DO-BLOCK.
      end.
    
    if pServiceAddrType = ''
     then
      tService = tService + 'act'.
	 else
      tService = tService + pServiceAddrType.	  
      
    if pQuery <> "" and pQuery <> ? 
     then tService = tService + "?" + pQuery. 
     
    if tDestination <> "" and tDestination <> ?
     then
      do:
       if pQuery = ""
        then tService = tService + "?" + tDestination.
       else
         tService = tService + "&" + tDestination.
      end.

    ctoolName = if pMethod = 'get' then 'wget.exe' else 'curl.exe'.

    httptool = search(ctoolName).

    if httptool = ? 
     then 
      do: trace(ctoolName + " was not found.").
          responseMsg = "Unable to initiate windows command to the Service.".
          leave DO-BLOCK.
      end.
    
    if pAction = ? or pAction = ""
     then
      do: trace("Action set incorrectly: " + (if pAction = ? then "Unknown" else pAction)).
          responseMsg = "Command action set incorrectly.  Contact the System Administrator.".
          leave DO-BLOCK.
      end.
    
    /* Verify data file exists and is readable since this is a POST */

    if pDataFile <> ""
     then
      do:
        if pContentType = "" or pContentType = ?
         then
          do: trace("Content Type is not set").
            responseMsg = "Data content type is not set to submit to the Service.".
            leave DO-BLOCK.
          end.
          
        file-info:file-name = pDataFile.
        if file-info:full-pathname = ? 
         then
          do: trace("Data file was not found: " + pDataFile).
            responseMsg = "Unable to locate data to submit to the Service.".
            leave DO-BLOCK.
          end.
        if index(file-info:file-type, "R") = 0
         then
          do: trace("Unable to read data file").
            responseMsg = "Unable to read data to submit to the Service.".
            leave DO-BLOCK.
          end.
        pDataFile = file-info:full-pathname.
      end.
      
    /* Create the Server URL specification input file */
    responseStatus = createUrlFile().
    if not responseStatus 
     then leave DO-BLOCK.
   
    /* Create the WGET configuration file */
    if pMethod = 'get' then
      responseStatus = createIniFile-get().
    else
      responseStatus = createIniFile-post().
    if not responseStatus 
     then leave DO-BLOCK.

    /* Create the Windows BATch file to set the configuration environment variable */
    if pMethod = 'get' then 
      responseStatus = createCmdFile-get().
    else
      responseStatus = createCmdFile-post().
    if not responseStatus 
     then leave DO-BLOCK.

    /* This condition is here so we get the above files when in debug mode */    
    if httptool = ? 
     then leave DO-BLOCK.

    /* Call the BATch file */
    os-command silent value(tCmdFile).

    /* Rename to the BATch file to avoid duplication of the call */
    os-rename value(tCmdFile) value(replace(tCmdFile, ".bat", "")).
    tCmdFile = replace(tCmdFile, ".bat", "").

    /* Check condition of the WGET response file and parse it (basically) */
    clientResponse = new util.HttpResponse(tResponseFile,tTraceFile,tHeadersFile).
    assign 
      responseStatus = clientResponse:responseStatus
      responseMsg    = clientResponse:responseMsg.


 end.

session:set-wait-state("").

publish "AddTempFile" (pAction + "1", search(tCmdFile)).
publish "AddTempFile" (pAction + "2", search(tResponseFile)).
publish "AddTempFile" (pAction + "3", search(tLogFile)).
publish "AddTempFile" (pAction + "4", search(tIniFile)).
publish "AddTempFile" (pAction + "5", search(tUrlFile)).
publish "AddTempFile" (pAction + "6", search(tTraceFile)).
publish "AddTempFile" (pAction + "7", search(pDataFile)).
publish "AddTempFile" (pAction + "8", search(tHeadersFile)).

if pMethod = 'get'
 then
  publish "ProcessLog" (pAction, "HTTP/GET", etime, responseStatus).
 else
  publish "ProcessLog" (pAction, "HTTP/POST", etime, responseStatus). /* change for get */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-createCmdFile-get) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createCmdFile-get Procedure 
FUNCTION createCmdFile-get RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* Create in the current working directory */
  
  output to value(tCmdFile) page-size 0.

  put unformatted 'set wgetrc=' search(tIniFile) skip.
  put unformatted httpTool skip.
  put unformatted 'exit' skip.
  output close.

  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createCmdFile-post) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createCmdFile-post Procedure 
FUNCTION createCmdFile-post RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  /* Create in the current working directory */
  output to value(tCmdFile) page-size 0.
  put unformatted httptool + " --config ~"" + search(tIniFile) + "~" --verbose 2> " + tLogFile skip.
  output close.

  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createINIFile-get) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createINIFile-get Procedure 
FUNCTION createINIFile-get RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tAuth as longchar.
 def var tMemptr as memptr.
 def var appCode as char.

 if tUser = ? then tUser = "".
 if tPw = ? then tPw = "".
 
/* Create in the current working directory */
 output to value(tIniFile) page-size 0.

 std-lo = false.
 publish "GetAppDebug" (output std-lo).
 if not std-lo
  then 
   do:  put unformatted "quiet = on" skip.
   end.
 put unformatted "logfile = " tHeadersFile skip.   
 std-ch = "COMPASSGET".
 publish "GetCurrentValue" ("ApplicationCode", output std-ch).
 put unformatted "user_agent = " std-ch skip.
 appCode = std-ch.

 put unformatted "netrc = off" skip.
 put unformatted "http_keep_alive = off" skip.
 put unformatted "tries = 2" skip.
 
 put unformatted "timeout = " + tTimeout skip.
 put unformatted "input = " tUrlFile skip.
 put unformatted "output_document = " tResponseFile skip.
 put unformatted "check_certificate = off" skip.
 put unformatted "compression = gzip " skip.
 put unformatted "server_response = on" skip.

 
 put unformatted "header = Cookie: I1=" + tUser + "; I2=" encrypt(tPw).
 if pAction > "" then put unformatted "; I3=" pAction.
 if appCode > "" then put unformatted "; I4=" appCode.
 if tMyID > "" then put unformatted "; I5=" tMyID.
 put skip.
 
 /* Base64 encode credentials delimited by a colon */
 tAuth = tUser + ":" + tPw.
 copy-lob tAuth to tMemptr.
 tAuth = base64-encode(tMemptr).
 set-size(tMemptr) = 0.
 put unformatted "header = Authorization: Basic " + string(tAuth) skip.

 output close.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createINIfile-post) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createINIfile-post Procedure 
FUNCTION createINIfile-post RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable tAuth   as longchar  no-undo.
  define variable tMemptr as memptr    no-undo.
  define variable appCode as character no-undo.

  /* Create in the current working directory */
  output to value(tIniFile) page-size 0.
  
  std-lo = false.
/*  publish "GetAppDebug" (output std-lo).
  if std-lo
   then put unformatted "debug = on" skip. */

  std-ch = "COMPASSPOST". 
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  put unformatted "user-agent = ~"" + std-ch + "~"" skip.
   
  put unformatted "connect-timeout = 1200" skip.
  put unformatted "url = ~"" + tService + "~"" skip.
  put unformatted "output = " tResponseFile skip.
  put unformatted "dump-header = ~"" + tHeadersFile + "~""  skip.
  
  put unformatted "header = ~"Cookie: I1=" + tUser + "; I2=" + encrypt(tPw).
  if pAction > ""
   then put unformatted "; I3=" + pAction.
   
  std-ch = "COMPASSPOST". 
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  appCode = std-ch.
  if appCode > "" 
   then 
    put unformatted "; I4=" + appCode.
  if tMyID > "" 
   then
  put unformatted "; I5=" + tMyID + "~"" skip.
  
  put unformatted "header = ~"Content-Type: " + pContentType + "~"" skip.
  put unformatted "data-binary = ~"@" + replace(pDataFile, "\", "\\") + "~"" skip.
  output close.
  
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createURLfile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createURLfile Procedure 
FUNCTION createURLfile RETURNS LOGICAL PRIVATE
 ():
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Must result in a file containing a single line such as:
            http://localhost/do/cgiip.exe/aesping.p
------------------------------------------------------------------------------*/

 /* Create in the current working directory */
 output to value(tUrlFile) page-size 0.
 put unformatted tService skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 /* Create in the current working directory */
 output to value(tTraceFile) append.
 put unformatted now " " pError skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

