&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: util\rpt-dialogDestination.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  Modifications:
    Date        Name      Comments
    06/07/24    Sachin C  Task# 113416 Removed leading and trailing white spaces while validating emailID
    11/14/24    S Chandu  Modified to set defalut values according to parameter.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define input  parameter ipcReportFormat     as character no-undo.
define input  parameter ipcDestinationType  as character no-undo.
define output parameter opcBehavior         as character no-undo.
define output parameter opcNotifyRequestor  as character no-undo. 
define output parameter opcDestination      as character no-undo.
define output parameter opcEmail            as character no-undo.
define output parameter opcPrinter          as character no-undo.
define output parameter opcRepository       as character no-undo.
define output parameter opcOutputFormat     as character no-undo.
define output parameter oplCancel           as logical   no-undo.


/* Local Variable Definitions ---                                       */
define variable cCurrentUID as character no-undo.

define variable rsDestination as handle no-undo.
define variable rsFormat      as handle no-undo.
define variable configText    as handle no-undo.


{lib/std-def.i}
{lib/ar-def.i}
{lib/validEmailAddr.i}
{lib/rpt-defs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bCancel tgNotify btDone 
&Scoped-Define DISPLAYED-OBJECTS fEmailID cbPrinter tgNotify 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON btDone AUTO-GO 
     LABEL "Generate" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbPrinter AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 46.8 BY 1 NO-UNDO.

DEFINE VARIABLE fEmailID AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.8 BY 1 TOOLTIP "Email address" NO-UNDO.

DEFINE VARIABLE tgNotify AS LOGICAL INITIAL no 
     LABEL "Notify once completed" 
     VIEW-AS TOGGLE-BOX
     SIZE 25 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bCancel AT ROW 8.81 COL 44 WIDGET-ID 488
     fEmailID AT ROW 3 COL 25 COLON-ALIGNED NO-LABEL WIDGET-ID 454
     cbPrinter AT ROW 4.1 COL 25 COLON-ALIGNED NO-LABEL WIDGET-ID 472
     tgNotify AT ROW 7.91 COL 27 WIDGET-ID 474
     btDone AT ROW 8.81 COL 27
     "Format" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 6 COL 3 WIDGET-ID 428
     "Destination" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 1.19 COL 3 WIDGET-ID 452
     SPACE(60.39) SKIP(8.47)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Output Options"
         CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cbPrinter IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fEmailID IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Output Options */
DO:
  oplCancel = true.
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplCancel = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btDone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btDone Dialog-Frame
ON CHOOSE OF btDone IN FRAME Dialog-Frame /* Generate */
DO:
  run setOutputOptions in this-procedure  (output std-ch).
  if std-ch <> ''
   then
    do:
     message std-ch
         view-as alert-box error buttons ok.
     return no-apply.
    end.
    

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgNotify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgNotify Dialog-Frame
ON VALUE-CHANGED OF tgNotify IN FRAME Dialog-Frame /* Notify once completed */
DO:
  publish "SetNotifyRequestor" (input tgNotify:checked). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetCredentialsID"   (output cCurrentUID).
publish "GetSysPropList"     ({&printappcode},
                              {&printobjAction},
                              {&printProperty},
                              output std-ch).
publish "GetNotifyRequestor" (output tgNotify).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  RUN enable_UI.
  
  RUN createRadioSet in this-procedure.
  
  RUN setPreference in this-procedure.
  
  ON 'VALUE-CHANGED':U OF rsDestination
   run setDestinationButtons in this-procedure.
   
  if std-ch > ""
   then 
    cbPrinter:list-item-pairs = trim(replace(std-ch,"^",","),","). 
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createRadioSet Dialog-Frame 
PROCEDURE createRadioSet :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 /* for setting destinations as A,E,P,V */
 CREATE RADIO-SET rsDestination
     ASSIGN ROW    = (2.0)
     COLUMN        = (11.0)
     HEIGHT-CHARS  = (9.30)
     WIDTH-CHARS   = 14.0
     FRAME         = FRAME {&frame-name}:HANDLE
     HORIZONTAL    = FALSE 
     AUTO-RESIZE   = TRUE 
     .
 /* for setting output format as P,C i.s. pdf or csv */    
 CREATE RADIO-SET rsFormat
     ASSIGN ROW    = (6.5)
     COLUMN        = (11.0)
     HEIGHT-CHARS  = (2.30)
     WIDTH-CHARS   = 10.0
     FRAME         = FRAME {&frame-name}:HANDLE
     HORIZONTAL    = FALSE
     AUTO-RESIZE   = TRUE
     .
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE createText Dialog-Frame 
PROCEDURE createText :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 create text configText
  assign row    = (2.10)
  column        = (27.0)
  height-chars  = (0.62)
  width-chars   = 41.0
  frame         = frame {&frame-name}:handle
  format        = "x(42)".
  
  configText:screen-value = "Destination is based on preferences setup". 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fEmailID cbPrinter tgNotify 
      WITH FRAME Dialog-Frame.
  ENABLE bCancel tgNotify btDone 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setDestinationButtons Dialog-Frame 
PROCEDURE setDestinationButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign
      fEmailID:sensitive       = false
      fEmailID:screen-value    = ""
      cbPrinter:sensitive      = false
      tgNotify:sensitive       = false
      rsFormat:sensitive       = true
      .
      
  case rsDestination:screen-value:
    when {&agent} 
     then
      do:
        if num-entries(ipcReportFormat,',') > 1 
         then
          rsFormat:screen-value = "P".
         else
          rsFormat:screen-value = ipcReportFormat.
          
        assign 
            rsFormat:sensitive    = false
            tgNotify:sensitive    = true
            .
      end.
    when {&email} 
     then
      do:
        assign
            fEmailID:sensitive    = not fEmailID:hidden
            fEmailID:screen-value = cCurrentUID
            tgNotify:sensitive    = true
            .  
      end.
    when {&printer} 
     then 
      do:
        if num-entries(ipcReportFormat,',') > 1 
         then
          rsFormat:screen-value = "P".
         else
          rsFormat:screen-value = ipcReportFormat.
        assign
            cbPrinter:sensitive   = not cbprinter:hidden
            rsFormat:sensitive    = false
            tgNotify:sensitive    = true.
      end.
    when {&view}
     then
      assign
       tgNotify:checked = false.

  end case.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setOutputOptions Dialog-Frame 
PROCEDURE setOutputOptions PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE OUTPUT PARAMETER opError  AS CHARACTER   NO-UNDO.

  do with frame dialog-frame:
  end.

  if rsDestination:input-value = {&email}
   then   
    do:
     if fEmailID:screen-value = ""
      then
       do:
         opError = "Email ID cannot be blank.".
         return.
       end.
      
     if fEmailID:screen-value <> "" and not validEmailAddr(fEmailID:screen-value)
      then 
       do:   
         opError = "Email ID is Invalid.".
         return. 
       end.  
    end.
    
  if rsDestination:input-value = {&printer}
   then   
    do:
     if cbPrinter:screen-value = "" or cbPrinter:screen-value = ?
      then
       do:
         opError = "Printer name cannot be blank.".
         return.
       end.      
    end.    

  if rsFormat:input-value = 'C' and 
     (rsFormat:input-value = 'A' or rsFormat:input-value = 'P') 
   then 
      message "A CSV file can only be emailed." view-as alert-box error buttons ok.
  
  if rsDestination:input-value = {&view}
   then
    assign
     opcOutputFormat = rsFormat:input-value.     
   else
    assign
        opcBehavior        = 'Q'
        opcNotifyRequestor = (if tgNotify:input-value then 'Yes' else 'No')
        opcDestination     = (if rsDestination:input-value = 'A' then 'C' else 'S')
        opcEmail           = (if fEmailID:input-value <> '' then replace(fEmailID:input-value, ' ', '') else '')
        opcPrinter         = (if cbPrinter:input-value <> '' then cbPrinter:input-value else '')
        opcOutputFormat    = rsFormat:input-value
        .
   
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setPreference Dialog-Frame 
PROCEDURE setPreference :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable iPosition as decimal no-undo.
  define variable lAdded    as logical no-undo.
  
  assign
   fEmailID:hidden     = true
   cbPrinter:hidden    = true
   fEmailID:sensitive  = false
   cbPrinter:sensitive = false
   iPosition           = 2.0
   lAdded             = false.
  
  /* setting destinations */
  if lookup({&rpt-destConfigured}, ipcDestinationType) > 0 then
  do:
    rsDestination:ADD-LAST("Configured", "A").
    
    run createText in this-procedure.
    assign
     rsDestination:screen-value = {&agent}
     iPosition                  = iPosition + 0.75
     lAdded                     = true.
     
  end.
    
  if lookup({&rpt-destEmail}, ipcDestinationType) > 0 then
  do:
   rsDestination:ADD-LAST("Email", "E").
   assign
    fEmailID:hidden       = false
    fEmailID:sensitive    = true
    fEmailID:row          = iPosition
    iPosition             = iPosition + 1.10.
    
   if not lAdded 
    then
     assign
      rsDestination:screen-value = {&email}
      lAdded                     = true.
   
  end.
    
  if lookup({&rpt-destPrinter}, ipcDestinationType) > 0 then
  do:
   rsDestination:ADD-LAST("Printer", "P").
   assign
    cbPrinter:hidden      = false
    cbPrinter:sensitive   = true
    cbPrinter:row         = iPosition.
    
   if not lAdded 
    then
     assign
      rsDestination:screen-value = {&printer}
      lAdded                     = true.
     
  end.
    
  if lookup({&rpt-destView}, ipcDestinationType) > 0 then
  do:
   rsDestination:ADD-LAST("View", "V").
   
   if not lAdded 
    then
     assign
      rsDestination:screen-value = {&view}
      lAdded                     = true.
  end.
  
  /* setting report format */
  iPosition = 7.52.
  if lookup({&rpt-pdf}, ipcReportFormat) > 0 then
  do:
   rsFormat:ADD-LAST("PDF", "P").
  end.
  
  if lookup({&rpt-csv}, ipcReportFormat) > 0 then
  do:
   rsFormat:ADD-LAST("CSV", "C").
  end.
  
  ENABLE UNLESS-HIDDEN ALL WITH FRAME {&frame-name}.
  
  run setDestinationButtons in this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

