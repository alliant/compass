/*------------------------------------------------------------------------
@name  arwriteoffpdf.p
@description Creates the Write-off PDF

@author Shefali
@version 1.0
@created 05/25/2021
@modified 
----------------------------------------------------------------------*/
{tt/ledgerReport.i &tableAlias="glWriteoff"}

define input  parameter ipcReportAction     as character  no-undo.
define input  parameter table               for glWriteoff. 
define output parameter opcFilename         as character no-undo.

{lib/rpt-def.i &ID="'arwriteoff'" &rptOrientation="'Landscape'" &logo=images\alliantblue.jpg &blockTitleColor=169169169}
{lib/std-def.i}
{lib/ar-def.i}

/* Local Variables */
define variable xVal            as integer  initial 457  no-undo.
define variable iCount          as integer               no-undo.
define variable dewriteoffTotal as decimal               no-undo.
define variable cPostedBy       as character             no-undo.
define variable deWrtofTotal    as decimal               no-undo.
define variable lShowHeader     as logical               no-undo.
define variable dtPostDate      as date                  no-undo.
define variable cPostingId      as character             no-undo.
define variable cWriteoffDate   as character             no-undo.
define variable cWriteoffId     as character             no-undo.

/* Function Prototype */
FUNCTION setxPos RETURNS INTEGER
  ( input ixPos as integer,
    input lAdd as logical )  FORWARD.

FUNCTION calculateAmount RETURNS DECIMAL
  ( input ipcAgentID  as character,
    input ipcAcctType as character )  FORWARD.    
    
assign
  activeFont = "Helvetica"
  activeSize = 12.0
  .

/* Get commonly used variables */  
iCount = 0.
for each glWriteoff:
  if iCount < 1
   then
    assign
        cPostedBy  = glWriteoff.ledgerCreateBy 
        dtPostDate = glWriteoff.ledgerCreateDate
        cPostingId = string(glWriteoff.ledgerID)
        cWriteoffId  = string(glWriteoff.actionID).  
  assign
      dewriteoffTotal = dewriteoffTotal + glWriteoff.debit
      iCount     = iCount + 1
      .
end.

for first glWriteoff where glWriteoff.ledgerID > 0: 
  assign
      cWriteoffDate = string(date(glWriteoff.transDate)).
end.

run getFilename in this-procedure no-error.
if error-status:error
 then return.

activeFilename = opcFilename.   
  
{lib/rpt-open.i &tempID='arwriteoff' }
  
run setMainWoffHead    in this-procedure.
run setWriteoffDetails in this-procedure.
run setAccountSummary in this-procedure.
  
if ipcReportAction = {&agentdefault}
 then
  do:
    {lib/rpt-cloz.i &tempID='arwriteoff' &no-save=true &exclude-pdfview=true}
  end.
 else if ipcReportAction = {&view} 
  then
   do:
     {lib/rpt-cloz.i &tempID='arwriteoff' &no-save=true }
   end.


PROCEDURE getFilename :
  if ipcReportAction = {&agentdefault} 
   then
    std-ch = session:temp-directory.
   else if ipcReportAction = {&view} 
    then
     publish "getReportDir" (output std-ch).
    
  opcFilename = std-ch  + (if dtPostDate <> ? then "VoidWrite-off_" else "PrelimVoidWrite-off_") + replace(string(today), "/", "") + "_" + replace(string(time,"HH:MM:SS"),":","") + ".pdf".

  if ipcReportAction = {&view} 
   then
    publish "SetCurrentValue" ("filename", opcFilename).
END PROCEDURE.

PROCEDURE pmtDetailHeader :
  define input parameter iPos as integer no-undo.
  
  /* GL Header */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
  run pdf_rect({&rpt}, 10, iPos + 17, 772, 24, 0.5).  
  
  run pdf_set_font({&rpt}, "Helvetica-bold", 11).
  run pdf_text_color({&rpt}, 1, 1, 1).
  run pdf_text_xy({&rpt}, "Agent ID", 20, iPos + 26).    
  run pdf_text_xy({&rpt}, "Name", 80, iPos + 26).
  run pdf_text_xy({&rpt}, "Write-off ID", 330, iPos + 26).
  run pdf_text_xy({&rpt}, "Account Number", 410, iPos + 26).
  run pdf_text_xy({&rpt}, "Description", 540, iPos + 26).
  run pdf_text_xy({&rpt}, "Debit", 653, iPos + 26).
  run pdf_text_xy({&rpt}, "Credit", 736, iPos + 26).
    
END PROCEDURE.

PROCEDURE pmtHeader :
  define input parameter iPos as integer no-undo.
  
  /* Write-off Header */ 
  run pdf_set_font({&rpt}, "Helvetica-boldoblique", 9.5).
  run pdf_text_color({&rpt}, 0, 0, 0).  
 
  run pdf_text_xy({&rpt}, "File Date", 350, iPos + 4). 
  run pdf_text_xy({&rpt}, "File Number", 455, iPos + 4).
  run pdf_text_xy({&rpt}, "Write-off Amount", 570, iPos + 4).
  /* linestrock */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "0,0,0").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "0,0,0").
  run pdf_rect({&rpt}, 345, iPos - 2, 320, 0.10, 0.5).
  
  run pdf_text_color({&rpt}, 0, 0, 0).  
END PROCEDURE.

 
PROCEDURE reportFooter :
  run pdf_text_color ({&rpt}, 0.0, 0.0, 0.0).
  run pdf_set_font ({&rpt}, "Courier", 7.0).
  
  run pdf_text_xy ({&rpt}, "Printed:", 20, 14).
  run pdf_text_xy ({&rpt}, string(now , "99/99/9999 HH:MM:SS" ), 55, 14).
 
  /* Line added for "Posted By" */
  if cPostedBy > ""
   then
    run pdf_text_xy ({&rpt}, "Voided: " + cPostedBy + " " + string(dtPostDate), 390, 14). 
 
  run pdf_text_xy ({&rpt}, string(iPage, "zz9"), 
                    if rptOrientation = "Landscape" then 765 else 580, 
                    14).
                     
  setFont(activeFont, activeSize).
END PROCEDURE.

 
PROCEDURE reportHeader :
  def var tTitle as char no-undo.
  def var tXpos as int no-undo.
  tXpos = 400 - (length(tTitle) / 2 * 6). /* there are 6 points in one character and 400 points is the middle of the header */
  {lib/rpt-hdr.i &title=tTitle &title-x=tXpos &title-y=740}
END PROCEDURE.

 
PROCEDURE setAccountSummary : 
  define variable deDebit  as decimal no-undo.
  define variable deCredit as decimal no-undo.
  
  setxPos(50,no).
  
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").   
  run pdf_rect({&rpt}, 10, setxPos(0,no), 772, 24, 0.5). 
  
  run pdf_set_font({&rpt}, "Helvetica-bold", 11).    
  run pdf_text_color({&rpt}, 1, 1, 1).
  run pdf_text_xy({&rpt}, "Summary", 20, setxPos(0,no) + 9).
  
  /* Summary Sub Header */
  run pdf_set_font({&rpt}, "Helvetica-boldoblique", 11).
  run pdf_text_color({&rpt}, 0, 0, 0).
  run pdf_text_xy({&rpt}, "Account Number", 330, setxPos(20,no)).
  run pdf_text_xy({&rpt}, "Description", 460, xVal).
  run pdf_text_xy({&rpt}, "Debit", 633, xVal).
  run pdf_text_xy({&rpt}, "Credit", 736, xVal).
  /* linestrock */
  run pdf_rgb ({&rpt}, "pdf_stroke_fill", "0,0,0").
  run pdf_rgb ({&rpt}, "pdf_stroke_color", "0,0,0").
  run pdf_rect({&rpt}, 320, xVal - 9 , 461, 0.10, 0.5).
  setxPos(12,no).
    
  setFont("Helvetica", 9.0). 
  for each glWriteoff break by glWriteoff.glRef:  
    accumulate glWriteoff.debit (total by glWriteoff.glRef).
    accumulate glWriteoff.credit (total by glWriteoff.glRef).
    
    if last-of(glWriteoff.glRef)
     then
      do:
        setxPos(15,no).
        run pdf_text_xy({&rpt}, glWriteoff.glRef, 330, setxPos(0,no)). 
        run pdf_text_xy({&rpt}, glWriteoff.glDesc, 460, setxPos(0,no)).  
        assign
            deDebit  = accum total by glWriteoff.glRef glWriteoff.debit
            deCredit = accum total by glWriteoff.glRef glWriteoff.credit
            .
        if deDebit > 0
         then
          run pdf_text_align({&rpt}, string(deDebit, ">>>,>>>,>>>,>>9.99"), "RIGHT", 660, setxPos(0,no)).
          
        if deCredit > 0
         then
          run pdf_text_align({&rpt}, string(deCredit, ">>>,>>>,>>>,>>9.99"), "RIGHT", 768, setxPos(0,no)).
      end.       
  end.  
END PROCEDURE.


PROCEDURE setMainWoffHead :
  run pdf_skipn ({&rpt},1).
  
  if dtPostDate <> ?
   then
    do:
      /* Write-off label on top right */
      setFont("Helvetica-Bold", 14.0).
      run pdf_text_xy({&rpt}, "VOIDED WRITE-OFF JOURNAL", 500, 560). 
      run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
      run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
      
      /*--------------Write-off Total Amount--------------*/
      setFont("Helvetica-Bold", 11.0).
      /*run pdf_text_xy({&rpt}, "Write-off ID:", 20, 515).
      run pdf_text_align({&rpt}, cWriteoffId,"RIGHT", 162, 515).*/
      run pdf_text_xy({&rpt}, "Void ID:", 574, 515).
      run pdf_text_align({&rpt}, cPostingId,"RIGHT", 697, 515).
      
      setFont("Helvetica", 11.0).                                                                               
      run pdf_text_xy({&rpt}, "Write-off Date:", 20, 500).                                                       
      run pdf_text_align({&rpt}, cWriteoffDate,"RIGHT", 162, 500).                                              
      run pdf_text_xy({&rpt}, "Void Date:", 574, 500).                                                       
      run pdf_text_align({&rpt}, string(dtPostDate),"RIGHT", 697, 500).
      run pdf_text_xy ({&rpt}, "Total Amount:", 20, /*510*/ 485). 
      run pdf_text_align({&rpt}, string(dewriteoffTotal,">>>,>>>,>>>,>>9.99"),"RIGHT", 162, 485).
    end.
   else
    do:
      /* Write-off label on top right */
      setFont("Helvetica-Bold", 14.0).
      run pdf_text_xy({&rpt}, "PRELIMINARY VOID WRITE-OFF JOURNAL", 400, 560). 
      run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
      run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
      
      /*--------------Write-off Total Amount--------------*/
      setFont("Helvetica-Bold", 11.0).
      run pdf_text_xy ({&rpt}, "Total Amount:", 20, /*510*/ 485). 
      run pdf_text_align({&rpt}, string(dewriteoffTotal,">>>,>>>,>>>,>>9.99"),"RIGHT", 162, 485).
    end.
END PROCEDURE.


PROCEDURE setWriteoffDetails :
  define variable lNewAgent as logical no-undo.
  
  run pmtDetailHeader in this-procedure (input 425).
  
  setxPos(18,no).
  for each glWriteoff break by glWriteoff.agentID BY glWriteoff.filenumber by glWriteoff.debit desc by glWriteoff.glRef:
    lShowHeader = true.
    run pdf_text_color({&rpt}, 0, 0, 0).
     
    if first-of(glWriteoff.agentID)
     then
      do:      
        iCount = 0.
        setxPos(10,no).
        lNewAgent = true.
        setFont("Helvetica", 9.0).
        run pdf_text_xy({&rpt}, glWriteoff.agentID, 20, setxPos(0,no)).
        
        if length(glWriteoff.agentName) gt 46 
         then
          do:
            run pdf_text_xy ({&rpt}, substring(glWriteoff.agentName,1,46), 80, setxPos(0,no)). 
            run pdf_text_xy ({&rpt}, substring(glWriteoff.agentName,47), 80, setxPos(0,no) - 10).
          end.
         else
          run pdf_text_xy ({&rpt}, glWriteoff.agentName, 80, setxPos(0,no)).
      end.
     
    if first-of(glWriteoff.glRef) and (iCount < 2) 
     then
      do:
        setFont("Helvetica", 9.0).
        run pdf_text_xy({&rpt}, STRING(glWriteoff.actionID), 330, setxPos(0,no)).
        
        if iCount > 0
         then setxPos(15,no).
                
        setFont("Helvetica", 9.0).
        run pdf_text_xy({&rpt}, glWriteoff.glRef, 410, setxPos(0,no)).
        run pdf_text_xy({&rpt}, glWriteoff.glDesc, 540, setxPos(0,no)).
     
        if glWriteoff.debit > 0 
         then 
          run pdf_text_align({&rpt}, string(calculateAmount(input glWriteoff.agentID, input if glWriteoff.debit > 0 then "D" else "C"), ">>>,>>>,>>>,>>9.99"),"RIGHT", 680, setxPos(0,no)). /* Debit amount */
         else 
          run pdf_text_align({&rpt}, string(calculateAmount(input glWriteoff.agentID, input if glWriteoff.debit > 0 then "D" else "C"), ">>>,>>>,>>>,>>9.99"),"RIGHT", 768, setxPos(0,no)). /* Credit amount */
              
        iCount = iCount + 1.
        
        if iCount > 1
         then setxPos(10,no). 
            
      end.    
    if last-of(glWriteoff.filenumber) AND (glWriteoff.filedate NE ? )
     then
      do:
        setxPos(15,no).
        if lNewAgent 
         then
          do:
            lNewAgent = false.
            run pmtHeader(input setxPos(5,no)).
            xval = setxPos(0,no) - 15. 
          end.
        setFont("Helvetica", 9.0).
        
        run pdf_text_align({&rpt}, date(glWriteoff.filedate),"RIGHT", 385, setxPos(0,no)).
        run pdf_text_xy({&rpt}, glWriteoff.filenumber, 455, setxPos(0,no)).
        run pdf_text_align({&rpt}, string(glWriteoff.transAmt, "->>>,>>>,>>>,>>9.99"), "RIGHT", 647, setxPos(0,no)).
          
      end.
    
    if last-of(glWriteoff.agentID)
     then
      do:
        lNewAgent = false.      
        setxPos(10,no).
        run pdf_rgb ({&rpt}, "pdf_stroke_fill", "070130180").
        run pdf_rgb ({&rpt}, "pdf_stroke_color", "070130180").
        run pdf_line ({&rpt},20,setxPos(0,no), 772,setxPos(0,no),2).        
        setxPos(6,no).
      end.    
  end.
  lShowHeader = false.    
END PROCEDURE.

/* ************************  Function Implementations ***************** */

FUNCTION calculateAmount RETURNS DECIMAL
  ( input ipcAgentID as character,
    input ipcAcctType as character ) :
 
  define variable deAmount as decimal no-undo. 
  define buffer glWriteoff for glWriteoff.
  
  for each glWriteoff where glWriteoff.agentID = ipcAgentID:   
    deAmount  = deAmount + if ipcAcctType = "D" then glWriteoff.debit else glWriteoff.credit.
  end.   
    
  return deAmount.  /* Function return value. */

END FUNCTION.
   
   
FUNCTION setxPos RETURNS INTEGER
  ( input ixPos as integer,
    input lAdd as logical ) :
 
  if xVal le (ixPos + 38)
   then
    do:
      run pdf_new_page2({&rpt}, "Landscape").
      if lShowHeader
       then
        do:
          run pmtDetailHeader in this-procedure(input 480).
          xVal = 495.
        end.
       else
        xVal = 540.
    end.
    
  if lAdd
   then
    xval = xval +  ixPos.
   else
    xval = xval -  ixPos.   
    
  return xval.   /* Function return value. */

END FUNCTION.


