&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : util/httppost.p
    Purpose     : perform a generic HTTP Post using wget.exe
    Author(s)   : D.Sinclair
    Created     : 7.4.2012
    Notes       : All temporary filenames are NOT fully-qualified

    Example     : The input parameters are set as follows:
                   pName = Prefix for filenames (optional, usually a GUID)
                   pURL = Full URL (including query-string) to call
                   pHeaders = chr(0) delimited text to be added as HTTP headers
                   pContentType = Type of data in pDataFile (text/xml or text/json)
                   pDataFile = Fully-qualified path to file to submit
                   
                  This example will result in up to 6 files:
                   [GUID]CMD.bat  Windows BATch file executed silently
                   [GUID]INI      Configuration of WGET options (INI file)
                   [GUID]URL      Complete Service URL
                   [GUID]LOG      Log file from WGET command (optional)
                   [GUID]TRC      Trace file from this procedure
                   [GUID]RES      Response file from server (XML)
                    *the suffixes are used only when debug is true
                   
                  The output parameters will be set as follows:
                   responseStatus    True (Success), False (Failure somewhere)
                   responseMsg       Text message suitable to present to user
                   responseFile      Fully-qualified path to Response file

 1.23.2014 das Restructured to use GUID for uniqueness and obfuscation.
 8.31.2014 das Added ContentType input
               Added Basic Authentication scheme
               Removed pProcess input (not used)
 2.19.2015 das Restructured use of debugging; standardized comments
  ----------------------------------------------------------------------*/
def input parameter pName as char.
def input parameter pURL as char.
def input parameter pHeaders as char.
def input parameter pContentType as char.
def input parameter pDataFile as char.

def output parameter responseStatus as logical init false.
def output parameter responseMsg as char.
def output parameter responseFile as char.

def var httpTool as char no-undo.
&scoped-define httptool wget.exe

{lib/std-def.i}
{lib/add-delimiter.i}

def var tIniFile as char no-undo.
def var tCmdFile as char no-undo.
def var tUrlFile as char no-undo.
def var tLogFile as char no-undo.
def var tResponseFile as char no-undo.
def var tTraceFile as char no-undo.

def var tDebugging as logical init false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-createCmdFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createCmdFile Procedure 
FUNCTION createCmdFile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createINIfile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createINIfile Procedure 
FUNCTION createINIfile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createURLfile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createURLfile Procedure 
FUNCTION createURLfile RETURNS LOGICAL PRIVATE
 () FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-verifyResponse) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD verifyResponse Procedure 
FUNCTION verifyResponse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.95
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

etime(true).

if pName = "" 
 then pName = replace(guid, "-", "").

assign
  std-ch        = replace(guid, "-", "")
  tIniFile      = pName + std-ch + "INI"
  tCmdFile      = pName + std-ch + "CMD" + ".bat"
  tUrlFile      = pName + std-ch + "URL"
  tLogFile      = pName + std-ch + "LOG"
  tResponseFile = pName + std-ch + "RES"
  tTraceFile    = pName + std-ch + "TRC"
  .

session:set-wait-state("general").

DO-BLOCK:
do:
    
    httpTool = search("{&httptool}").
    
    /* Verify data file exists and is readable since this is a POST */
    if pContentType = "" or pContentType = ?
     then
      do: trace("Content Type is not set").
          responseMsg = "Data content type is not set to submit to the Service.".
          leave DO-BLOCK.
      end.

    file-info:file-name = pDataFile.
    if file-info:full-pathname = ? 
     then
      do: trace("Data file was not found: " + pDataFile).
          responseMsg = "Unable to locate data to submit to the Service.".
          leave DO-BLOCK.
      end.
    if index(file-info:file-type, "R") = 0
     then
      do: trace("Unable to read data file").
          responseMsg = "Unable to read data to submit to the Service.".
          leave DO-BLOCK.
      end.
    pDataFile = file-info:full-pathname.

    /* Create the Server URL specification input file */
    responseStatus = createUrlFile().
    if not responseStatus 
     then leave DO-BLOCK.
   
    /* Create the WGET configuration file */
    responseStatus = createIniFile().
    if not responseStatus 
     then leave DO-BLOCK.

    /* Create the Windows BATch file to set the configuration environment variable */
    responseStatus = createCmdFile().
    if not responseStatus 
     then leave DO-BLOCK.

    /* This condition is here so we get the above files when in debug mode */    
    if httpTool = ? 
     then leave DO-BLOCK.

    /* Call the BATch file */
    os-command silent value(tCmdFile).

    /* Rename to the BATch file to avoid duplication of the call */
    os-rename value(tCmdFile) value(replace(tCmdFile, ".bat", "")).
    tCmdFile = replace(tCmdFile, ".bat", "").

    /* Check condition of the WGET response file (basically) */
    responseStatus = verifyResponse().
 end.

session:set-wait-state("").

publish "AddTempFile" (pName + "1", search(tCmdFile)).
publish "AddTempFile" (pName + "2", search(tResponseFile)).
publish "AddTempFile" (pName + "3", search(tLogFile)).
publish "AddTempFile" (pName + "4", search(tIniFile)).
publish "AddTempFile" (pName + "5", search(tUrlFile)).
publish "AddTempFile" (pName + "6", search(tTraceFile)).
publish "AddTempFile" (pName + "7", search(pDataFile)).

publish "ProcessLog" (pName, "HTTP/POST", etime, responseStatus).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-createCmdFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createCmdFile Procedure 
FUNCTION createCmdFile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  /* Create in the current working directory */
  output to value(tCmdFile) page-size 0.
  put unformatted 'set wgetrc=' search(tIniFile) skip.
  put unformatted httpTool skip.
/*   put unformatted 'exit' skip.  */
  output close.

  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createINIfile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createINIfile Procedure 
FUNCTION createINIfile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  /* Create in the current working directory */
  output to value(tIniFile) page-size 0.
  put unformatted "debug = on" skip.
  put unformatted "logfile = " tLogFile skip.
  
  std-ch = "COMPASSPOST". 
  publish "GetCurrentValue" ("ApplicationCode", output std-ch).
  put unformatted "user_agent = " std-ch skip.
   
  put unformatted "netrc = off" skip.
  put unformatted "http_keep_alive = off" skip.
  put unformatted "tries = 2" skip.
  put unformatted "timeout = 60" skip.
  put unformatted "check_certificate = off" skip.
  put unformatted "content_on_error = on" skip.
  put unformatted "method = delete" skip.
  
  put unformatted "input = " tUrlFile skip.
  put unformatted "output_document = " tResponseFile skip.
  
  do std-in = 1 to num-entries(pHeaders):
   put unformatted "header = " + entry(std-in, pHeaders) skip.
  end.
  
  put unformatted "header = Content-Type: " + pContentType skip.
  put unformatted "body_file = " pDataFile skip.
  output close.
  
  RETURN true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-createURLfile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createURLfile Procedure 
FUNCTION createURLfile RETURNS LOGICAL PRIVATE
 ():
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  Must result in a file containing a single line such as:
            http://localhost/do/cgiip.exe/aesping.p
------------------------------------------------------------------------------*/

 /* Create in the current working directory */
 output to value(tUrlFile) page-size 0.
 put unformatted pURL skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 /* Create in the current working directory */
 output to value(tTraceFile) append.
 put unformatted now " " pError skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-verifyResponse) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION verifyResponse Procedure 
FUNCTION verifyResponse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cLogLine      as character no-undo.
  define variable lEndResponse  as logical no-undo initial false.
  define variable iCounter      as integer no-undo.
  define variable iResponseCode as integer no-undo initial 0.
  define variable lValidCode    as logical no-undo initial false.
  file-info:file-name =  tResponseFile.
  
  if search(tLogFile) <> ?
   then
    do:
      responseMsg = "".
      input from value(search(tLogFile)).
      READ-LOG:
      repeat:
        import unformatted cLogLine.
        if cLogLine = "Giving up."
         then
          do:
            lValidCode = false.
            responseMsg = "File " + pDataFile + " could not be uploaded.".
            leave READ-LOG.
          end.
        if lEndResponse
         then
          do:
            lEndResponse = false.
            iResponseCode = integer(entry(1, cLogLine, " ")) no-error.
            lValidCode = (iResponseCode >= 200 and iResponseCode < 300).
            leave READ-LOG.
          end.
         else lEndResponse = (cLogLine = "---response end---").
      end.
      input close.
    end.
  
  if lValidCode and iResponseCode <> 204
   then
    do:
      if lValidCode and (index(file-info:file-type, "F") = 0 or file-info:file-type = ?)
       then
        do: trace("Unable to locate response file: " + tResponseFile).
            trace("Attempting to retrieve HTTP Status Code from logfile").
            responseMsg = "Service response was not found.  Contact the System Administrator".
            return false.
        end.
      if lValidCode and index(file-info:file-type, "R") = 0
       then
        do: trace("Unable to read response file: " + tResponseFile).
            responseMsg = "Service response was corrupted.  Contact the System Administrator.".
            return false.
        end.
      if lValidCode and file-info:file-size = 0
       then
        do: trace("Unable to connect to Service; check process files").
            responseMsg = "Unable to connect to Service.  Contact the System Administrator.".
            return false.
        end.
    end.

  responseFile = file-info:full-pathname.
  RETURN lValidCode.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

