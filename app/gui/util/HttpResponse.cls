 
 /*------------------------------------------------------------------------
    File        : HttpResponse
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : kumar.raunak
    Created     : Thu Jan 05 11:32:25 IST 2023
    Notes       : 
  ----------------------------------------------------------------------
  Modify
  Name     Date            Notes
  Sagar K  06/21/2023      Removed illegal Special charcters from response file  
  K.R      12/29/2023      Improve HTTP Call Performance
  ----------------------------------------------------------------------*/

using Progress.Lang.*.

class util.HttpResponse: 
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    
    define public property compassStatus  as logical initial true no-undo
    get.
    private set.
    define public property compassMessage as char    no-undo
    get.
    private set.
    define public property contentType    as char    no-undo
    get.  
    private set.
    define public property attachmentName as char    no-undo
    get.
    private set.
    define public property bodyFilePath   as char    no-undo
    get.
    private set.
    define public property responseStatus as logical no-undo
    get.
    private set.
    define public property responseMsg    as char    no-undo
    get.
    private set.
    define public property hasBody        as logical no-undo
    get.
    private set.
    
    define private variable responseFile   as char    no-undo.
    define private variable traceFile      as char    no-undo.
    define private variable headerFile     as char    no-undo.
    define private variable headerEndPos   as int64   no-undo.
    
    define private stream parseStream.
    
    constructor public HttpResponse ( input responseFile as char,
                                            input traceFile    as char,
                                            input headerFile  as char                                     
                                           ):
      assign 
          this-object:responseFile = responseFile  
          this-object:traceFile = traceFile
          this-object:headerFile = headerFile.
          
      Parse(). /*Start parsing*/
    end constructor.
    
    method private void Parse():
      if not VerifyResponse() then 
       return. 
        
      if not VerifyHeaders() then 
       return.
  
      ParseHeaders() .

    end.
    
    method private logical VerifyResponse():
      file-info:file-name =  this-object:responseFile.
  
      if (index(file-info:file-type, "F") = 0 or file-info:file-type = ?)
       then
        do: Trace("Unable to locate response file: " + this-object:responseFile).
            Trace("Attempting to retrieve HTTP Status Code from logfile").
            this-object:responseMsg = "Service response was not found.  Contact the System Administrator".
            this-object:responseStatus = false.
            return this-object:responseStatus.
        end.
      if index(file-info:file-type, "R") = 0
       then
        do: Trace("Unable to read response file: " + this-object:responseFile).
            this-object:responseMsg = "Service temp response was corrupted.  Contact the System Administrator.".
            this-object:responseStatus = false.
            return this-object:responseStatus.
        end.

      if file-info:file-size = 0
       then
        this-object:hasBody = false.
      else
       this-object:hasBody = true. 
      
      this-object:bodyFilePath = file-info:full-pathname.
      this-object:responseStatus = true. 
      return this-object:responseStatus.
      
    end.    
    
    method private logical VerifyHeaders():
      file-info:file-name =  this-object:headerFile.
  
      if (index(file-info:file-type, "F") = 0 or file-info:file-type = ?)
       then
        do: Trace("Unable to locate header file: " + this-object:headerFile).
            Trace("Attempting to retrieve HTTP Status Code from logfile").
            this-object:responseMsg = "Service headers was not found.  Contact the System Administrator".
            this-object:responseStatus = false.
            return this-object:responseStatus.
        end.
      if index(file-info:file-type, "R") = 0
       then
        do: Trace("Unable to read headers file: " + this-object:headerFile).
            this-object:responseMsg = "Service headers was corrupted.  Contact the System Administrator.".
            this-object:responseStatus = false.
            return this-object:responseStatus.
        end.
      if file-info:file-size = 0
       then
        do: Trace("Unable to parse Headers; check process files").
            this-object:responseMsg = "Unable to parse Headers.  Contact the System Administrator.".
            this-object:responseStatus = false.
            return this-object:responseStatus.
        end.
       this-object:responseStatus = true.
       return this-object:responseStatus.    
    end.
    
    method private void ParseHeaders():
      define variable fileLine      as char no-undo.
      define variable filename      as char no-undo.
      define variable compassStatus as char no-undo.
      input from value(this-object:headerFile).

      repeat:
        import unformatted fileLine.
        if lookup("X-Compass:",fileLine," ") > 0 then
         do:
           compassStatus = trim(substring(fileLine,index(fileLine,":") + 1)).
           if compassStatus = 'S'
            then
             this-object:compassStatus = true.
           else
            this-object:compassStatus = false.  
         end.
        if lookup("X-CompassMsg:",fileLine," ") > 0 then
          this-object:compassMessage = trim(substring(fileLine,index(fileLine,":") + 1)).
        if lookup("Content-Type:",fileLine," ") > 0 then
          this-object:contentType = trim(substring(fileLine,index(fileLine,":") + 1)). 
        if lookup("Content-Disposition:",fileLine," ") > 0
         then
          do:
            filename = trim(substring(fileLine,index(fileLine,":") + 1)).
            this-object:attachmentName = substring(filename,index(filename,"=") + 1).
          end.      
      end.

    end.    
        
    method private logical Trace(input pError as char):
      output to value(traceFile) append.
      put unformatted now " " pError skip.
      output close.

      return true.
    end.

    destructor public HttpResponse ( ):

    end destructor.

end class.
