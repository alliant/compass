&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* importdialog.w
   IMPORT data 
   2.14.2013 D.Sinclair
   
   pDatatypes   Comma-delimited list of datatypes for each column
                CH - character
                DE - decimal
                IN - integer
                LO - logical
                DA - date
                DT - datetime
  @modified 
  Date          Name         Description  
  09/18/2018    Gurvindar    Modification for person Import
   */

create widget-pool.

{tt/importdata.i}
{tt/importdata.i &tableAlias="importrecord"}

def input parameter pFieldNames as char no-undo.
def input parameter pDatatypes as char no-undo.
def input parameter pFields as char no-undo.
def input parameter pCallbackProcedure as char no-undo.

{lib/std-def.i}
def var hCaller as handle no-undo.
def var tStopImport as logical no-undo.

hCaller = source-procedure. /* Calling routine that must implement pCallbackProcedure */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES importdata

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData stat no-label data[1] no-label data[2] no-label data[3] no-label data[4] no-label data[5] no-label data[6] no-label data[7] no-label data[8] no-label data[9] no-label data[10] no-label data[11] no-label data[12] no-label data[13] no-label data[14] no-label data[15] no-label data[16] no-label data[17] no-label data[18] no-label data[19] no-label data[20] no-label   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH importdata by importdata.seq
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH importdata by importdata.seq.
&Scoped-define TABLES-IN-QUERY-brwData importdata
&Scoped-define FIRST-TABLE-IN-QUERY-brwData importdata


/* Definitions for DIALOG-BOX fMain                                     */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFile bFileSearch bImport tDelimiter ~
tStartRow tStopOnError bPreview bDatatypes bFields brwData bDone 
&Scoped-Define DISPLAYED-OBJECTS tFile tDelimiter tStartRow tStopOnError ~
tStatus tPreviewStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDatatypes 
     LABEL "Datatypes..." 
     SIZE 14 BY 1.14.

DEFINE BUTTON bDone AUTO-END-KEY 
     LABEL "Done" 
     SIZE 15 BY 1.14.

DEFINE BUTTON bFields 
     LABEL "Fields..." 
     SIZE 14 BY 1.14.

DEFINE BUTTON bFileSearch 
     LABEL "..." 
     SIZE 4 BY 1 TOOLTIP "Select file".

DEFINE BUTTON bImport 
     LABEL "Import" 
     SIZE 9.6 BY 1.14.

DEFINE BUTTON bPreview 
     LABEL "Preview" 
     SIZE 10.2 BY 1.14.

DEFINE BUTTON bStop 
     LABEL "Stop" 
     SIZE 9.6 BY 1.14.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 79.2 BY 1 NO-UNDO.

DEFINE VARIABLE tPreviewStatus AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1 TOOLTIP "Lines in the file / data rows read" NO-UNDO.

DEFINE VARIABLE tStartRow AS INTEGER FORMAT ">9":U INITIAL 1 
     LABEL "Number of header lines to skip" 
     VIEW-AS FILL-IN 
     SIZE 7.4 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 56.8 BY 1 NO-UNDO.

DEFINE VARIABLE tDelimiter AS CHARACTER INITIAL "C" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Comma", "C",
"Tab", "T",
"Semicolon", "S",
"Space", "P"
     SIZE 14.2 BY 3.52 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 121 BY 4.91.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 144 BY 11.81.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 22.2 BY 4.91.

DEFINE VARIABLE tStopOnError AS LOGICAL INITIAL yes 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 4 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      importdata SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData fMain _FREEFORM
  QUERY brwData DISPLAY
      stat no-label format "x(6)"    
  data[1] no-label format "x(20)"
  data[2] no-label format "x(20)"
  data[3] no-label format "x(20)"
  data[4] no-label format "x(20)"
  data[5] no-label format "x(20)"
  data[6] no-label format "x(20)"
  data[7] no-label format "x(20)"
  data[8] no-label format "x(20)"
  data[9] no-label format "x(20)"
  data[10] no-label format "x(20)"
  data[11] no-label format "x(20)"
  data[12] no-label format "x(20)"
  data[13] no-label format "x(20)"
  data[14] no-label format "x(20)"
  data[15] no-label format "x(20)"
  data[16] no-label format "x(20)"
  data[17] no-label format "x(20)"
  data[18] no-label format "x(20)"
  data[19] no-label format "x(20)"
  data[20] no-label format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 140 BY 10.71 FIT-LAST-COLUMN TOOLTIP "The first column displays ~"Error~" if there are any datatype conversion issues".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tFile AT ROW 1.48 COL 8.4 COLON-ALIGNED WIDGET-ID 20
     bFileSearch AT ROW 1.48 COL 89.8 WIDGET-ID 22
     bImport AT ROW 2.57 COL 10.6 WIDGET-ID 30
     bStop AT ROW 2.57 COL 21.4 WIDGET-ID 52
     tDelimiter AT ROW 4.81 COL 19.4 NO-LABEL WIDGET-ID 6
     tStartRow AT ROW 4.57 COL 84 COLON-ALIGNED WIDGET-ID 4
     tStopOnError AT ROW 5.62 COL 89 RIGHT-ALIGNED WIDGET-ID 18
     bPreview AT ROW 6.48 COL 86 WIDGET-ID 2
     bDatatypes AT ROW 5.24 COL 130.2 WIDGET-ID 34
     bFields AT ROW 6.67 COL 130.2 WIDGET-ID 38
     brwData AT ROW 10.29 COL 6 WIDGET-ID 200
     bDone AT ROW 21.95 COL 68.6 WIDGET-ID 28
     tStatus AT ROW 2.67 COL 32.8 NO-LABEL WIDGET-ID 50 NO-TAB-STOP 
     tPreviewStatus AT ROW 7.67 COL 84 COLON-ALIGNED NO-LABEL WIDGET-ID 46 NO-TAB-STOP 
     "Do not import if any datatype errors:" VIEW-AS TEXT
          SIZE 34 BY .62 AT ROW 5.67 COL 52 WIDGET-ID 32
     "Delimiter:" VIEW-AS TEXT
          SIZE 8.8 BY .62 AT ROW 4.81 COL 9 WIDGET-ID 16
     "Guidelines" VIEW-AS TEXT
          SIZE 10.4 BY .62 AT ROW 3.86 COL 127.8 WIDGET-ID 42
     "Settings" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 3.86 COL 6 WIDGET-ID 44
     "Data Preview" VIEW-AS TEXT
          SIZE 13.2 BY .62 AT ROW 9.33 COL 6 WIDGET-ID 26
     RECT-2 AT ROW 4.19 COL 4 WIDGET-ID 14
     RECT-3 AT ROW 9.67 COL 4 WIDGET-ID 24
     RECT-4 AT ROW 4.19 COL 126 WIDGET-ID 40
     SPACE(2.79) SKIP(14.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Import Data"
         DEFAULT-BUTTON bDone WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData bFields fMain */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bStop IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tPreviewStatus IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR TOGGLE-BOX tStopOnError IN FRAME fMain
   ALIGN-R                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH importdata by importdata.seq.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Import Data */
DO:
  /* apply "choose" to bdone. */
  apply "END-ERROR":U to self.
 /* apply "GO" to frame {&frame-name}. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDatatypes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDatatypes fMain
ON CHOOSE OF bDatatypes IN FRAME fMain /* Datatypes... */
DO:
  MESSAGE 
          "Text:" skip
          "Characters including a-z,A-Z,0-9, or symbols; delimiter must be within quotes" skip(2)
          "Decimal:" skip
          "Positive or negative numbers including up to 10 digits to the right of the decimal" skip(2)
          "Integer:" skip
          "Positive or negative whole numbers (no decimal point)" skip(2)
          "Boolean:" skip
          "Logical value including Y, Yes, T, True or N, No, F, False" skip(2)
          "Date:" skip
          "A date in the form MM/DD/YY or MM/DD/YYYY or blank" skip(3)
          "**A row will not be imported if there is a conversion error on that row**"
   VIEW-AS ALERT-BOX INFO BUTTONS OK title "Datatypes".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDone fMain
ON CHOOSE OF bDone IN FRAME fMain /* Done */
DO:
  if tfile:screen-value in frame {&frame-name} ne "" then
    publish "filepath" (input tfile:screen-value).
  apply "CLOSE" to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFields
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFields fMain
ON CHOOSE OF bFields IN FRAME fMain /* Fields... */
DO:
  pFields = replace(pFields, "|", chr(13)).
  if pFields = "" 
   then pFields = "None Specified".
  MESSAGE 
          pFields
   VIEW-AS ALERT-BOX INFO BUTTONS OK title "Expected Fields".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFileSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFileSearch fMain
ON CHOOSE OF bFileSearch IN FRAME fMain /* ... */
DO:
 run selectFile in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bImport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bImport fMain
ON CHOOSE OF bImport IN FRAME fMain /* Import */
DO:
 run importData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPreview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPreview fMain
ON CHOOSE OF bPreview IN FRAME fMain /* Preview */
DO:
  run previewData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStop
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStop fMain
ON CHOOSE OF bStop IN FRAME fMain /* Stop */
DO:
 tStopImport = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

if num-entries(pFieldNames) <> num-entries(pDatatypes) 
 then
  do:
      MESSAGE "Fields and Datatypes do not match." skip(2)
              "Contact the System Administrator."
       VIEW-AS ALERT-BOX error BUTTONS OK.
      return.
  end.

do std-in = 1 to num-entries(pDatatypes):
 case entry(std-in, pDatatypes):
  when "CH" then std-ch = entry(std-in, pFieldNames) + " (Text)".
  when "DE" then std-ch = entry(std-in, pFieldNames) + " (Decimal)".
  when "LO" then std-ch = entry(std-in, pFieldNames) + " (Boolean)".
  when "IN" then std-ch = entry(std-in, pFieldNames) + " (Integer)".
  when "DA" then std-ch = entry(std-in, pFieldNames) + " (Date)".
  when "DT" then std-ch = entry(std-in, pFieldNames) + " (DateTime)".
  otherwise std-ch = entry(std-in, pFieldNames) + " (Unknown)".
 end case.
 case std-in:
  when 1 then data[1]:label in browse brwData = std-ch.
  when 2 then data[2]:label in browse brwData = std-ch.
  when 3 then data[3]:label in browse brwData = std-ch.
  when 4 then data[4]:label in browse brwData = std-ch.
  when 5 then data[5]:label in browse brwData = std-ch.
  when 6 then data[6]:label in browse brwData = std-ch.
  when 7 then data[7]:label in browse brwData = std-ch.
  when 8 then data[8]:label in browse brwData = std-ch.
  when 9 then data[9]:label in browse brwData = std-ch.
  when 10 then data[10]:label in browse brwData = std-ch.
  when 11 then data[11]:label in browse brwData = std-ch.
  when 12 then data[12]:label in browse brwData = std-ch.
  when 13 then data[13]:label in browse brwData = std-ch.
  when 14 then data[14]:label in browse brwData = std-ch.
  when 15 then data[15]:label in browse brwData = std-ch.
  when 16 then data[16]:label in browse brwData = std-ch.
  when 17 then data[17]:label in browse brwData = std-ch.
  when 18 then data[18]:label in browse brwData = std-ch.
  when 19 then data[19]:label in browse brwData = std-ch.
  when 20 then data[20]:label in browse brwData = std-ch.
 end case.
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFile tDelimiter tStartRow tStopOnError tStatus tPreviewStatus 
      WITH FRAME fMain.
  ENABLE tFile bFileSearch bImport tDelimiter tStartRow tStopOnError bPreview 
         bDatatypes bFields brwData bDone 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importData fMain 
PROCEDURE importData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tError as logical.
 def var tImportCnt as int.
 def var tImportLines as int.
 def var tErrorCnt as int no-undo.
 def var tTotalCnt as int no-undo.

 assign
   bImport:sensitive in frame {&frame-name} = false
   bStop:sensitive in frame {&frame-name} = true
   tDelimiter:sensitive in frame {&frame-name} = false
   tStartRow:sensitive in frame {&frame-name} = false
   tStopOnError:sensitive in frame {&frame-name} = false

   bPreview:sensitive in frame {&frame-name} = false
   bDatatypes:sensitive in frame {&frame-name} = false
   bFields:sensitive in frame {&frame-name} = false
   bDone:sensitive in frame {&frame-name} = false
   
   tStopImport = false
   tStatus:screen-value in frame {&frame-name} = ""
   .

 close query brwData.
 
 run loadData in this-procedure (output tError,
                                 output tImportLines,
                                 output tTotalCnt).

 if not tError
   or (tError and not tStopOnError:checked in frame fMain)
  then /* Import non-errors */
   do: tImportCnt = 0.
       tErrorCnt = 0.
       
       std-dt = now.
       IMPORT-LOOP:
       for each importdata:
        tStatus = trim(string(importdata.seq, ">>>,>>>,>>9"))
                 + " of "
                 + trim(string(tImportLines, ">>>,>>>,>>9"))
                 + " records".
        tStatus:screen-value in frame {&frame-name} = tStatus.
        process events.
        if tStopImport
         then 
          do: tStatus:screen-value in frame {&frame-name} = 
                tStatus:screen-value + " (Stopped)".
              leave IMPORT-LOOP.
          end.

        if importdata.stat = "Error" 
         then tErrorCnt = tErrorCnt + 1.
         else 
          do: 
              empty temp-table importrecord.
              create importrecord.
              buffer-copy importdata to importrecord.

              run value(pCallbackProcedure) in hCaller
                (input table importrecord,
                 output std-lo,
                 output std-ch).
              if std-lo 
               then tImportCnt = tImportCnt + 1.
               else assign
                      importdata.stat = "Error " + std-ch
                      tErrorCnt = tErrorCnt + 1
                      .
          end.
       end.
       std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo
        then message "Time taken: " + string(interval(now, std-dt, "milliseconds") / 1000, "ZZZ,ZZ9.9") + " seconds" view-as alert-box information buttons ok.
   end.

 publish "DeleteTempFile" ("ImportDataErrors", output std-lo).
 std-ch = "importdataerrors.csv".

 /* Create error file */
 if tErrorCnt > 0 
  then
   do: output to value(std-ch) page-size 0.
       for each importdata
         where importdata.stat begins "Error".
        export delimiter "," importdata.
       end.
       output close.
       publish "AddTempFile" ("ImportDataErrors", search(std-ch)).
   end.

 if tErrorCnt > 0 
  then 
   do: MESSAGE  tImportLines " data lines read" skip(2)
                tImportCnt " records imported" skip(2)
                tErrorCnt " records skipped" skip(2)
                "**Results may be incomplete**" skip(2)  
                "Would you like to view import errors?"
         VIEW-AS ALERT-BOX warning BUTTONS YES-NO update std-lo.
       if std-lo 
        then publish "OpenTempFile" ("ImportDataErrors").
   end.
  else
 if tImportCnt = 0 
  then MESSAGE tImportLines " data lines read" skip(2)
               "No records imported"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
  else MESSAGE tImportLines " data lines read" skip(2)
               tImportCnt " records imported"
         VIEW-AS ALERT-BOX info BUTTONS OK.

  assign
    bImport:sensitive in frame {&frame-name} = true
    bStop:sensitive in frame {&frame-name} = false
    tDelimiter:sensitive in frame {&frame-name} = true
    tStartRow:sensitive in frame {&frame-name} = true
    tStopOnError:sensitive in frame {&frame-name} = true

    bPreview:sensitive in frame {&frame-name} = true
    bDatatypes:sensitive in frame {&frame-name} = true
    bFields:sensitive in frame {&frame-name} = true
    bDone:sensitive in frame {&frame-name} = true
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loadData fMain 
PROCEDURE loadData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 def output parameter pImportLines as int.
 def output parameter pImportCnt as int.

 def var i as int.
 def var f as char extent 50.

 empty temp-table importdata.

 if search(tFile:screen-value in frame fMain) = ? 
  then
   do: 
     MESSAGE "Import file not found.  Please check the filename and try again."
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     pError = true.
     return.
   end.

 output to "importdataloaderrors.txt" no-echo.
 input from value(tFile:screen-value in frame fMain).
 do std-in = 1 to tStartRow:input-value in frame fMain:
  import ^.
  pImportLines = pImportLines + 1.
 end.

 pError = false.
 repeat on error undo, retry:
  assign
    f = "".

  if retry 
   then
    do: pError = true.
        leave.
    end.
  case tDelimiter:screen-value in frame fMain:
   when "C" then import delimiter "," f[1] f[2] f[3] f[4] f[5] f[6] f[7] f[8] f[9] f[10]
                                      f[11] f[12] f[13] f[14] f[15] f[16] f[17] f[18] f[19] f[20]
                                      f[21] f[22] f[23] f[24] f[25] f[26] f[27] f[28] f[29] f[30]
                                      f[31] f[32] f[33] f[34] f[35] f[36] f[37] f[38] f[39] f[40]
                                      f[41] f[42] f[43] f[44] f[45] f[46] f[47] f[48] f[49] f[50]
                                      .
   when "T" then import delimiter "~t" f[1] f[2] f[3] f[4] f[5] f[6] f[7] f[8] f[9] f[10]
                                       f[11] f[12] f[13] f[14] f[15] f[16] f[17] f[18] f[19] f[20]
                                       f[21] f[22] f[23] f[24] f[25] f[26] f[27] f[28] f[29] f[30]
                                       f[31] f[32] f[33] f[34] f[35] f[36] f[37] f[38] f[39] f[40]
                                       f[41] f[42] f[43] f[44] f[45] f[46] f[47] f[48] f[49] f[50]
                                       .
   when "S" then import delimiter ";" f[1] f[2] f[3] f[4] f[5] f[6] f[7] f[8] f[9] f[10]
                                      f[11] f[12] f[13] f[14] f[15] f[16] f[17] f[18] f[19] f[20]
                                      f[21] f[22] f[23] f[24] f[25] f[26] f[27] f[28] f[29] f[30]
                                      f[31] f[32] f[33] f[34] f[35] f[36] f[37] f[38] f[39] f[40]
                                      f[41] f[42] f[43] f[44] f[45] f[46] f[47] f[48] f[49] f[50]
                                      .
   when "P" then import f[1] f[2] f[3] f[4] f[5] f[6] f[7] f[8] f[9] f[10]
                        f[11] f[12] f[13] f[14] f[15] f[16] f[17] f[18] f[19] f[20]
                        f[21] f[22] f[23] f[24] f[25] f[26] f[27] f[28] f[29] f[30]
                        f[31] f[32] f[33] f[34] f[35] f[36] f[37] f[38] f[39] f[40]
                        f[41] f[42] f[43] f[44] f[45] f[46] f[47] f[48] f[49] f[50]
                        .
  end case.
  pImportLines = pImportLines + 1.
  pImportCnt = pImportCnt + 1.
  create importdata.
  importdata.seq = pImportCnt.
  do std-in = 1 to 50:
   importdata.data[std-in] = f[std-in].
   if num-entries(pDatatypes) >= std-in 
    then /* type check */
     do:
         case entry(std-in, pDatatypes):
          when "DE" 
           then 
            do: std-de = decimal(f[std-in]) no-error.
                if error-status:error 
                 then assign
                        pError = true
                        importdata.stat = "Error".
                 else importdata.data[std-in] = string(std-de).
            end.
          when "LO" 
           then
            do: if lookup(f[std-in], "y,yes,t,true,n,no,f,false") = 0
                 then assign
                        pError = true
                        importdata.stat = "Error".
                 else if lookup(f[std-in], "y,yes,t,true") > 0 
                       then importdata.data[std-in] = "Yes".
                       else importdata.data[std-in] = "No".
            end.
          when "IN" 
           then
            do: i = integer(f[std-in]) no-error.
                if error-status:error 
                 then assign
                        pError = true
                        importdata.stat = "Error".
                 else importdata.data[std-in] = string(i).
            end.
          when "DA" 
           then
            do: std-da = date(f[std-in]) no-error.
                if error-status:error 
                 then assign
                        pError = true
                        importdata.stat = "Error".
                 else importdata.data[std-in] = string(std-da).
            end.
          when "DT" 
           then
            do: std-da = datetime(f[std-in]) no-error.
                if error-status:error 
                 then assign
                        pError = true
                        importdata.stat = "Error".
                 else importdata.data[std-in] = string(std-da).
            end.
          when "CH"
           then
            do: if hCaller:name = "wprocessing.w" /* state forms */
                 then case std-in:
                        when 1 or
                        when 3 or
                        when 5 or
                        when 6 or
                        when 7 then importdata.data[std-in] = upper(importdata.data[std-in]).
                      end case.
            end.
         end case.
     end.
  end.
 end.
 input close.

 output close.

 publish "AddTempFile" ("ImportDataLoadErrors", search("importdataloaderrors.txt")).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE previewData fMain 
PROCEDURE previewData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tError as logical.
 def var tImportCnt as int.
 def var tImportLines as int.

 close query brwData.
 tPreviewStatus:screen-value in frame fMain = "".

 run loadData in this-procedure (output tError,
                                 output tImportLines,
                                 output tImportCnt).

 if tError 
  then
   MESSAGE "Invalid data or file format." skip
           "Results may be incomplete." skip(2)
           "Check the data and try again."
    VIEW-AS ALERT-BOX warning BUTTONS OK.

  tPreviewStatus:screen-value in frame fMain = string(tImportLines) 
     + " / " + string(tImportCnt).
 open query brwData for each importdata by importdata.seq.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectFile fMain 
PROCEDURE selectFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tFilename as char no-undo.
def var tOK as logical no-undo.

SYSTEM-DIALOG GET-FILE tFilename    
 TITLE   "Choose File to Import ..."    
 MUST-EXIST    
 USE-FILENAME    
 UPDATE tOk.   
IF not tOK
 THEN return.
tFile:screen-value in frame fMain = tFilename.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

