&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* sys/wtask.w
   Add/Update a TASK in a separate Window
   D.Sinclair
   2.20.2014
 */

CREATE WIDGET-POOL.

/* def input parameter pTaskID as int no-undo. */

def var pTaskID as int no-undo.
def var tCurrentUserid as char no-undo.

{src/adm2/widgetprto.i}
{lib/std-def.i}

{tt/task.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define DISPLAYED-OBJECTS tSubject tAgentID tAgentName tEntityType ~
tEntityID tDueDate tStartDate tStatus tFollowupDate tCompleteDate tPriority ~
tOwner tModule tSecure tComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_toolbar AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tModule AS CHARACTER FORMAT "X(256)":U 
     LABEL "Module" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "OPS" 
     DROP-DOWN-LIST
     SIZE 20.2 BY 1 TOOLTIP "Module used to resolve this task" NO-UNDO.

DEFINE VARIABLE tPriority AS CHARACTER FORMAT "X(20)":U INITIAL "N" 
     LABEL "Priority" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEM-PAIRS "High","H",
                     "Normal","N",
                     "Low","L"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(20)":U INITIAL "N" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 6
     LIST-ITEM-PAIRS "Not Started","N",
                     "In Progress","I",
                     "Completed","C",
                     "Waiting","W",
                     "Deferred","D",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 111 BY 7.86 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(15)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 85.6 BY 1 NO-UNDO.

DEFINE VARIABLE tCompleteDate AS DATE FORMAT "99/99/99":U 
     LABEL "Completed" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDueDate AS DATE FORMAT "99/99/99":U 
     LABEL "Due Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tEntityID AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 85.6 BY 1 NO-UNDO.

DEFINE VARIABLE tEntityType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Category" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFollowupDate AS DATE FORMAT "99/99/99":U 
     LABEL "Follow-up" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tOwner AS CHARACTER FORMAT "X(25)":U 
     LABEL "Owner" 
     VIEW-AS FILL-IN 
     SIZE 42 BY 1 NO-UNDO.

DEFINE VARIABLE tStartDate AS DATE FORMAT "99/99/99":U 
     LABEL "Started" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN 
     SIZE 100.4 BY 1 NO-UNDO.

DEFINE VARIABLE tUserids AS CHARACTER FORMAT "X(256)":U 
     LABEL "Users" 
     VIEW-AS FILL-IN 
     SIZE 42.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 110.8 BY 3.91.

DEFINE VARIABLE tSecure AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 TOOLTIP "System generated" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tSubject AT ROW 3.14 COL 10.6 COLON-ALIGNED WIDGET-ID 2
     tAgentID AT ROW 4.29 COL 10.6 COLON-ALIGNED WIDGET-ID 26
     tAgentName AT ROW 4.29 COL 25.4 COLON-ALIGNED NO-LABEL WIDGET-ID 28
     tEntityType AT ROW 5.43 COL 10.6 COLON-ALIGNED WIDGET-ID 40
     tEntityID AT ROW 5.43 COL 25.4 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     tDueDate AT ROW 7.14 COL 15.8 COLON-ALIGNED WIDGET-ID 6
     tStartDate AT ROW 7.14 COL 43.8 COLON-ALIGNED WIDGET-ID 4
     tStatus AT ROW 7.19 COL 83.2 COLON-ALIGNED WIDGET-ID 18
     tFollowupDate AT ROW 8.29 COL 15.8 COLON-ALIGNED WIDGET-ID 8
     tCompleteDate AT ROW 8.29 COL 43.8 COLON-ALIGNED WIDGET-ID 34
     tPriority AT ROW 8.33 COL 83.2 COLON-ALIGNED WIDGET-ID 20
     tOwner AT ROW 9.43 COL 15.8 COLON-ALIGNED WIDGET-ID 22
     tModule AT ROW 9.48 COL 83 COLON-ALIGNED WIDGET-ID 48
     tSecure AT ROW 9.57 COL 108 WIDGET-ID 46
     tUserids AT ROW 10.52 COL 5.8 COLON-ALIGNED WIDGET-ID 24
     tComments AT ROW 11.95 COL 2 NO-LABEL WIDGET-ID 52
     RECT-1 AT ROW 6.86 COL 2.2 WIDGET-ID 36
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 113.4 BY 18.95 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Task"
         HEIGHT             = 18.95
         WIDTH              = 113.4
         MAX-HEIGHT         = 28.81
         MAX-WIDTH          = 146.2
         VIRTUAL-HEIGHT     = 28.81
         VIRTUAL-WIDTH      = 146.2
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentName IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tComments IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCompleteDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tDueDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tEntityID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tEntityType IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tFollowupDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tModule IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tOwner IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tPriority IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tSecure IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStartDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tStatus IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tSubject IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tUserids IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tUserids:HIDDEN IN FRAME fMain           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Task */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Task */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

{&window-name}:title = "Task " + string(pTaskID, ">>>>>>>>>>").

run addButton in h_toolbar
  ("save", "Save and Close", "Save and Close", "images/save.bmp", "", "images/save-i.bmp",
   false, true, this-procedure, ?).
run addDeleteButton in h_toolbar (?).
run addButton in h_toolbar
  ("complete", "Complete", "Mark Complete", "images/check.bmp", "", "images/check-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("agent", "Agent", "Assign Agent", "images/agent.bmp", "", "images/agent-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("type", "Type", "Assign Category", "images/docs.bmp", "", "images/docs-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("users", "Users", "Assign Users", "images/users.bmp", "", "images/users-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("assign", "Assign", "Assign Owner", "images/usergo.bmp", "", "images/usergo-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("history", "History", "Task History", "images/clock.bmp", "", "images/clock-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("pdf", "Pdf", "Task Report", "images/pdf.bmp", "", "images/pdf-i.bmp",
   false, true, this-procedure, ?).


if pTaskID = 0
 then std-ch = "Created " + string(now).
status input std-ch.
status default std-ch.

{lib/win-main.i}

publish "GetCredentialsUserid" (output tCurrentUserid).

if pTaskID <> 0
 then
  do: publish "GetTask" (pTaskID, 
                         output table task,
                         output std-lo,
                         output std-ch).
      find first task
        where task.taskID = pTaskID no-error.
      if not std-lo or not available task
       then
        do: if std-ch = ""
             then std-ch = "Task with ID [" + string(pTaskID) + "] is not available.".
            message std-ch
              view-as alert-box error.
            delete procedure this-procedure.
            return.
        end.
          
      assign
        tSubject = task.subject
        tAgentID = task.agentID
        tAgentName = task.agentName
        tEntityType = task.entityType
        tEntityID = task.entityID
        tDueDate = task.dueDate
        tFollowupDate = task.followupDate
        tStartDate = task.startDate
        tCompleteDate = task.completeDate
        tStatus = task.stat
        tPriority = task.priority
        tModule = task.module
        tOwner = task.owner
        tUserids = task.userids
        tComments = task.comments
        .
  end.
 else
  do:
      publish "GetAppCode" (output tModule).
      
      assign
        tStatus = "N"
        tPriority = "N"
        tOwner = tCurrentUserid
        .
  end.
  
if this-procedure:persistent
 then run initializeObject in this-procedure.

if tOwner = tCurrentUserid
 then
  do: enable
        tSubject
        tAgentID
        tAgentName
        tEntityType
        tEntityID
        tDueDate
        tFollowupDate
        tStartDate
        tCompleteDate
        tStatus
        tPriority
        tModule
        tOwner
        tComments
        with frame fMain.
      run enableActions in h_toolbar ("save,delete,complete,agent,type,users,assign").  
      
      if pTaskID <> 0
       then run enableActions in h_toolbar ("delete").  
       else run disableActions in h_toolbar ("delete").  
  end.
 else run disableActions in h_toolbar ("save,delete").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionComplete wWin 
PROCEDURE ActionComplete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pStop as logical init false no-undo.
 tStatus:screen-value in frame fMain = "C".
 if tCompleteDate:input-value = ?
  then tCompleteDate:screen-value = string(today).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete wWin 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pStop as logical init false no-undo.

 message "The task will be deleted." skip(1) "Do you want to continue?" 
   view-as alert-box question buttons YES-NO set std-lo.
 if not std-lo
  then return.
  
 publish "DeleteTask" (pTaskID).
 APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSave wWin 
PROCEDURE ActionSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pStop as logical init false no-undo.

 publish "SaveTask" ("").
 APPLY "CLOSE":U TO THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionType wWin 
PROCEDURE ActionType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 tEntityType = tEntityType:screen-value in frame fMain.
 run sys/dialogcategory.w ("Category", "Claim,Review,Audit,CA", true, input-output std-ch).
 tEntityType:screen-value = tEntityType. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'toolbar.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'LogicalObjectNamePhysicalObjectNameDynamicObjectnoRunAttributeHideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_toolbar ).
       RUN repositionObject IN h_toolbar ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in AB:  ( 2.00 , 102.80 ) */

       /* Adjust the tab order of the smart objects. */
       RUN adjustTabOrder ( h_toolbar ,
             tSubject:HANDLE IN FRAME fMain , 'BEFORE':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tSubject tAgentID tAgentName tEntityType tEntityID tDueDate tStartDate 
          tStatus tFollowupDate tCompleteDate tPriority tOwner tModule tSecure 
          tComments 
      WITH FRAME fMain IN WINDOW wWin.
  VIEW FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

