&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
define variable iCounterMax as integer no-undo initial 6.
define variable iCounter as integer no-undo.

/* Variables to hold the username, password, and application */
define variable cUsername as character no-undo.
define variable cPassword as character no-undo.

/* the starting height of the window */
define variable iHeight as decimal no-undo.

/* used to tell if buttons are clicked */
define variable lClickLogin as logical no-undo initial false.
define variable lClickExit as logical no-undo initial false.

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/do-wait.i}
{windows.i}

/* The following procedure is used to prevent the 'not responding' message
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tUsername tPassword bForgot bView tRemember ~
bLogin bLogo 
&Scoped-Define DISPLAYED-OBJECTS tRemember tApplication 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD centerText C-Win 
FUNCTION centerText RETURNS LOGICAL
  ( input pWidget as handle )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createApplicationName C-Win 
FUNCTION createApplicationName RETURNS LOGICAL
  ( input pAppName as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD createLoginTip C-Win 
FUNCTION createLoginTip RETURNS LOGICAL
  ( input pLoginTip as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD enableScreen C-Win 
FUNCTION enableScreen RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL PRIVATE
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bForgot 
     LABEL "Forgot Password?" 
     SIZE 21 BY 1.

DEFINE BUTTON bLogin 
     LABEL "Login" 
     SIZE 21.6 BY 1.81.

DEFINE BUTTON bLogo  NO-FOCUS
     LABEL "" 
     SIZE 7.6 BY 1.81.

DEFINE BUTTON bView 
     LABEL "View" 
     SIZE 4.6 BY 1.1.

DEFINE VARIABLE tApplication AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 43 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE tPassword AS CHARACTER FORMAT "X(256)":U 
     LABEL "Password" 
     VIEW-AS FILL-IN 
     SIZE 43 BY 1 NO-UNDO.

DEFINE VARIABLE tUsername AS CHARACTER FORMAT "X(256)":U 
     LABEL "Username" 
     VIEW-AS FILL-IN 
     SIZE 43 BY 1 NO-UNDO.

DEFINE VARIABLE tRemember AS LOGICAL INITIAL no 
     LABEL "Remember Me" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .71 NO-UNDO.

DEFINE BUTTON bExit  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "Exit" 
     SIZE 3.2 BY .76.

DEFINE IMAGE IMAGE-1
     FILENAME "images/compass.bmp":U
     SIZE 39 BY 1.81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tUsername AT ROW 8.14 COL 12 COLON-ALIGNED WIDGET-ID 54
     tPassword AT ROW 9.33 COL 12 COLON-ALIGNED WIDGET-ID 56 PASSWORD-FIELD 
     bForgot AT ROW 10.52 COL 36 WIDGET-ID 120
     bView AT ROW 9.29 COL 57.4 WIDGET-ID 64 NO-TAB-STOP 
     tRemember AT ROW 11.62 COL 36.2 WIDGET-ID 62
     bLogin AT ROW 10.52 COL 13.8 WIDGET-ID 58
     tApplication AT ROW 6.14 COL 12 COLON-ALIGNED NO-LABEL WIDGET-ID 118
     bLogo AT ROW 5.76 COL 3.8 WIDGET-ID 94 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 65 BY 12.81
         DEFAULT-BUTTON bLogin WIDGET-ID 100.

DEFINE FRAME fSplash
     bExit AT ROW 1 COL 62.8 WIDGET-ID 8 NO-TAB-STOP 
     IMAGE-1 AT ROW 2 COL 14 WIDGET-ID 96
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 65 BY 3.71
         BGCOLOR 15  WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Splash"
         HEIGHT             = 12.81
         WIDTH              = 65
         MAX-HEIGHT         = 32.81
         MAX-WIDTH          = 204.6
         VIRTUAL-HEIGHT     = 32.81
         VIRTUAL-WIDTH      = 204.6
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSplash:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fSplash:MOVE-AFTER-TAB-ITEM (bLogin:HANDLE IN FRAME fMain)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FILL-IN tApplication IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tPassword IN FRAME fMain
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN tUsername IN FRAME fMain
   NO-DISPLAY                                                           */
/* SETTINGS FOR FRAME fSplash
                                                                        */
/* SETTINGS FOR IMAGE IMAGE-1 IN FRAME fSplash
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4.71
       COLUMN          = 1
       HEIGHT          = .48
       WIDTH           = 65
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(FRAME fSplash:HANDLE).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Splash */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Splash */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fSplash
&Scoped-define SELF-NAME bExit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExit C-Win
ON CHOOSE OF bExit IN FRAME fSplash /* Exit */
DO:
  lClickExit = true.
  publish "ExitApplication".
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bForgot
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bForgot C-Win
ON CHOOSE OF bForgot IN FRAME fMain /* Forgot Password? */
DO:
  if tUsername:screen-value = ""
   then message "Please add a username" view-as alert-box information buttons ok.
   else
    do:
      run server/emailpassword.p (tUsername:screen-value, output std-lo, output std-ch).
      if std-lo
       then message "Please check your email for your credentials" view-as alert-box information buttons ok.
       else message std-ch view-as alert-box error buttons ok.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLogin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLogin C-Win
ON CHOOSE OF bLogin IN FRAME fMain /* Login */
DO:
  lClickLogin = true.
  run DoLogin in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON MOUSE-SELECT-DOWN OF bView IN FRAME fMain /* View */
DO:
  tPassword:password-field = false.
  tPassword:read-only = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON MOUSE-SELECT-UP OF bView IN FRAME fMain /* View */
DO:
  tPassword:password-field = true.
  tPassword:read-only = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPassword
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPassword C-Win
ON ENTRY OF tPassword IN FRAME fMain /* Password */
DO:
  tPassword:password-field = true.
  tPassword:read-only = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPassword C-Win
ON MOUSE-SELECT-CLICK OF tPassword IN FRAME fMain /* Password */
DO:
  tPassword:password-field = true.
  tPassword:read-only = false.
  self:set-selection(1,length(self:screen-value) + 1).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tUsername
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUsername C-Win
ON LEAVE OF tUsername IN FRAME fMain /* Username */
DO:
  if self:screen-value <> cUsername
   then tRemember:checked = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUsername C-Win
ON MOUSE-SELECT-CLICK OF tUsername IN FRAME fMain /* Username */
DO:
  self:set-selection(1,length(self:screen-value) + 1).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

subscribe to "SetSplashStatus" anywhere.
subscribe to "SetSplashCounter" anywhere.
subscribe to "DoLogin" anywhere.

iHeight = {&window-name}:height-chars.

DisableNotResponding().

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
   
bView:load-image("images/s-eye.bmp").
bView:load-image-insensitive("images/s-eye-i.bmp").
bExit:load-image("images/s-exit.bmp").
bExit:load-image-insensitive("images/s-exit.bmp").

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{&window-name}:window-state = 2.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  /* Center window on screen */
  assign
    {&window-name}:x = integer((session:width-pixels - {&window-name}:width-pixels) / 2)
    {&window-name}:y = integer((session:height-pixels - {&window-name}:height-pixels - 36) / 2)
    no-error.
  
  RUN enable_UI.

  /* set the logo */
  publish "GetLargeLogo" (output std-ch).
  if search(std-ch) = ? 
   then std-ch = "images\logoltgray.bmp".
   
  bLogo:load-image(std-ch).
  bLogo:load-image-insensitive(std-ch).
  bLogo:sensitive = false.
  
  /* set the remember credentials flag */
  std-lo = false.
  publish "LoadCredentials" (output std-lo).
  tRemember:checked = std-lo.
  /* set the credentials */
  publish "GetCredentials" (output cUsername, output cPassword).
  if std-lo
   then
    assign
      tUsername:screen-value = cUsername
      tPassword:screen-value = cPassword
      .
  /* set the application name */
  std-ch = "".
  publish "GetAppName" (output std-ch).
  createApplicationName(std-ch).
   
  /* get the login text */
  std-ch = "".
  publish "GetLoginTip" (output std-ch).
  {&window-name}:window-state = 3.
  createLoginTip(std-ch).
  
  run sys/MkSplash.p ({&WINDOW-NAME}:HWND, true, false).
  
  session:set-wait-state("").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseSplash C-Win 
PROCEDURE CloseSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  pbUpdateStatus(100, 1).
  pbMinStatus().

  apply "WINDOW-CLOSE" to {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "login.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "login.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DoLogin C-Win 
PROCEDURE DoLogin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lExpired as logical no-undo initial false.
  define variable lValid as logical no-undo initial false.
  
  /* disable the screen */
  enableScreen(false).
  do with frame {&frame-name}:
    publish "ValidateCredentials" (tUsername:screen-value, tPassword:screen-value, output lValid, output std-ch).
    
    if not lValid
     then MESSAGE std-ch VIEW-AS ALERT-BOX error BUTTONS OK title "Verification Failed".
     else
      do:
        /* set the username and password */
        publish "SetCredentialsID" (tUsername:screen-value).
        publish "SetCredentialsPassword" (tPassword:screen-value).
        /* check if the file should be deleted */
        run SetSplashStatus in this-procedure.
        publish "CheckLoginFile".
        /* If the password has expired, open the change password box with the message that the password has expired */
        /* If the password is about to expire, open the box with the number of days until it will expire */
        std-dt = now.
        std-lo = false.
        std-ch = "".
        run SetSplashStatus in this-procedure.
        publish "GetPasswordExpired" (output std-lo).
        publish "GetPasswordExpireDate" (output std-dt).
        publish "GetPasswordWarnDays" (output std-ch).
        
        if std-lo or lookup(string(interval(std-dt, today, "days")),std-ch) <> 0
         then 
          do:
            if std-lo
             then std-ch = "Your password has expired!".
             else 
              do:
                std-in = interval(std-dt, today, "days").
                std-ch = "Your password will expire in " + string(std-in) + " day".
                if std-in > 1
                 then std-ch = std-ch + "s!".
                 else std-ch = std-ch + "!".
              end.
              
            run sys\authpwdialog.w (std-ch).
          end.
        
        /* Check if the password is expired or not. If so, then quit the application */
        /* if the user just changed the password, the return logical will be false */
        publish "GetPasswordExpired" (output lExpired).
        if not lExpired
         then 
          do:
            if tRemember:checked
             then publish "SaveCredentials".
            publish "ContinueStartup".
          end.
      end.
  end.
  
  if lExpired
   then apply "CHOOSE" to bExit in frame fSplash.
   else 
    if lValid /* we need to close the splash if the credentials were accepted */
     then run CloseSplash in this-procedure.
     else enableScreen(true).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tRemember tApplication 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tUsername tPassword bForgot bView tRemember bLogin bLogo 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  ENABLE bExit 
      WITH FRAME fSplash IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSplash}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetSplashCounter C-Win 
PROCEDURE SetSplashCounter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pCounterMax as integer no-undo.
  
  iCounterMax = max(6,pCounterMax).
  SetFocus(tUsername:handle in frame {&frame-name}).
  
  do with frame {&frame-name}:
    wait-for "CHOOSE" of bLogin or "CHOOSE" of bExit in frame fSplash pause 10.
    if tUsername:screen-value > "" and tPassword:screen-value > ""
     then
      do:
        if not lClickLogin and not lClickExit
         then run DoLogin in this-procedure.
      end.
     else wait-for close of this-procedure.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetSplashStatus C-Win 
PROCEDURE SetSplashStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
  /* Prevent Not Responding and Update Process Status */
  DisableNotResponding().
 
  iCounter = iCounter + 1.
  pbUpdateStatus(int(min(iCounter / iCounterMax, .99) * 100), 0). /* Don't hit 100%...yet */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION centerText C-Win 
FUNCTION centerText RETURNS LOGICAL
  ( input pWidget as handle ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE reps AS INTEGER NO-UNDO.
  reps = (pWidget:WIDTH-PIXELS - FONT-TABLE:GET-TEXT-WIDTH-PIXELS(TRIM(pWidget:SCREEN-VALUE),pWidget:FONT)) / FONT-TABLE:GET-TEXT-WIDTH-PIXELS(' ',pWidget:FONT).
  reps = (reps / 2).
  pWidget:SCREEN-VALUE = FILL(' ',reps) + TRIM(pWidget:SCREEN-VALUE).
  RETURN yes.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createApplicationName C-Win 
FUNCTION createApplicationName RETURNS LOGICAL
  ( input pAppName as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cName as character no-undo.
  define variable iName as integer no-undo.
  define variable iAppLength as integer no-undo.
  do with frame {&frame-name}:
    iAppLength = integer(tApplication:width-chars) - 5.
    if pAppName = ""
     then pAppName = "Default Application".
    if length(pAppName) > iAppLength
     then pAppName = substring(pAppName, 1,  r-index(pAppName, " ", iAppLength - 1)).
    tApplication:screen-value = pAppName.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION createLoginTip C-Win 
FUNCTION createLoginTip RETURNS LOGICAL
  ( input pLoginTip as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable hTip as handle no-undo.
  
  /* used to store the different lines */
  define variable cLineTemp as character no-undo.
  define variable cSubstring as character no-undo.
   
  /* used for counters and offsets */
  define variable iNumOfTips as integer.
  define variable iTipLength as integer.
  define variable iCounter as integer no-undo.
  define variable iEntry as integer no-undo.
  define variable iPos as integer no-undo initial 1.
  define variable dRow as decimal no-undo.

  if pLoginTip = ""
   then pLoginTip = "Help someone feel appreciated today.".
   
  do with frame {&frame-name}:
    assign
      dRow = bLogin:row + bLogin:height-chars + 0.5
      cLineTemp = pLoginTip
      iTipLength = 55
      iNumOfTips = truncate(length(pLoginTip) / iTipLength, 0)
      .
    /* if the length of the tip happens to be evenly divided, then subtract one from the number of tips */
    if iNumOfTips * iTipLength = length(pLoginTip)
     then iNumOfTips = iNumOfTips - 1.
    /* change the frame height */
    assign
      {&window-name}:height-chars = iHeight + decimal(iNumOfTips * 0.80) + 1
      frame {&frame-name}:height-pixels = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      .
    /* if the tip is too big for one line */
    if length(pLoginTip) > iTipLength
     then
      do:
        do iCounter = 1 to iNumOfTips:
          /* add the line */
          create text hTip assign
            frame = frame {&frame-name}:handle
            row = dRow + (0.80 * (iCounter - 1))
            column = 3.5
            width-chars = iTipLength + 5
            height-chars = 0.80
            data-type = "character"
            sensitive = true
            visible = true
            format = "x(100)"
            .
          assign
            hTip:screen-value = substring(cLineTemp, 1,  r-index(cLineTemp, " ", iTipLength) - 1)
            iPos = length(hTip:screen-value) + 1
            cLineTemp = trim(substring(cLineTemp, iPos))
            .
          centerText(hTip).
        end.
        /* second tip line */
        create text hTip assign
          frame = frame {&frame-name}:handle
          row = dRow + (0.80 * iNumOfTips)
          column = 3.5
          width-chars = iTipLength + 5
          height-chars = 0.80
          data-type = "character"
          sensitive = true
          visible = true
          format = "x(100)"
          .
        hTip:screen-value = cLineTemp.
        centerText(hTip).
      end.
     else
      do:
        create text hTip assign
          frame = frame {&frame-name}:handle
          row = dRow
          column = 3.5
          width-chars = iTipLength + 5
          height-chars = 0.80
          data-type = "character"
          sensitive = true
          visible = true
          format = "x(100)"
          .
        hTip:screen-value = pLoginTip.
        centerText(hTip).
      end.
  end.
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION enableScreen C-Win 
FUNCTION enableScreen RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
    assign
      tUsername:read-only = not pEnable
      tPassword:read-only = not pEnable
      bView:sensitive = pEnable
      bLogin:sensitive = pEnable
      bForgot:sensitive = pEnable
      tRemember:sensitive = pEnable
      .
  end.
  bExit:sensitive in frame fSplash = pEnable.
  doWait(true).
  pause 0.5 no-message.
  doWait(false).
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
  
  /* Prevent Not Responding and Update Process Status */ 
  DisableNotResponding().

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL PRIVATE
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

/*   {&WINDOW-NAME}:move-to-top().  */
  
  /* Prevent Not Responding and Update Process Status */ 
  DisableNotResponding().
 
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

