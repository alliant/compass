&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* sys/esbsenddialog.w
   SEND a message on via the ESB server
   @author D.Sinclair
 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tType tTopic tAction tBody bSend bDone 
&Scoped-Define DISPLAYED-OBJECTS tType tTopic tAction tBody 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDone AUTO-END-KEY 
     LABEL "Done" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSend 
     LABEL "Send" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tAction AS CHARACTER 
     LABEL "Action" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEMS "Created","Deleted","Updated" 
     DROP-DOWN
     SIZE 50.6 BY 1 NO-UNDO.

DEFINE VARIABLE tTopic AS CHARACTER 
     LABEL "Name" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "Agent","Batch","BatchForm","County","Period","StatCode","State","Stateform","System.Shutdown.","System.Alert." 
     DROP-DOWN
     SIZE 50.6 BY 1 NO-UNDO.

DEFINE VARIABLE tBody AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 50.4 BY 7 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Topic", "T",
"Queue", "Q"
     SIZE 12 BY 1.91 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tType AT ROW 1.19 COL 12.4 NO-LABEL WIDGET-ID 8
     tTopic AT ROW 3.57 COL 10.4 COLON-ALIGNED WIDGET-ID 12
     tAction AT ROW 4.71 COL 10.4 COLON-ALIGNED WIDGET-ID 14
     tBody AT ROW 5.91 COL 12.6 NO-LABEL WIDGET-ID 6
     bSend AT ROW 13.14 COL 12.8
     bDone AT ROW 13.19 COL 48.4
     SPACE(1.79) SKIP(0.23)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Send Message"
         DEFAULT-BUTTON bSend CANCEL-BUTTON bDone WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Send Message */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSend fMain
ON CHOOSE OF bSend IN FRAME fMain /* Send */
DO:
  if tTopic:screen-value in frame fMain = "" 
   then
  do:
      MESSAGE "Topic/Queue cannot be blank"
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.

  publish "SendEsbMessage" (tType:screen-value in frame fMain,
                            tTopic:screen-value in frame fMain,
                            tAction:screen-value in frame fMain,
                            tBody:screen-value in frame fMain).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tType tTopic tAction tBody 
      WITH FRAME fMain.
  ENABLE tType tTopic tAction tBody bSend bDone 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

