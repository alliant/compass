&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* sys/admindialog.w
   allow the maintenance of ADMINistrative services ports
   D.Sinclair
   9.14.2013
   */

def temp-table data
 field f as char
 field v as int.

def var tAdminFile as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tMinPort tMaxPort Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tMinPort tMaxPort 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tMaxPort AS INTEGER FORMAT "zzzzz":U INITIAL 0 
     LABEL "Maximum Local Port" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the maximum port for a module administrative service (recommend 52599)" NO-UNDO.

DEFINE VARIABLE tMinPort AS INTEGER FORMAT "zzzzz":U INITIAL 0 
     LABEL "Minimum Local Port" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the minimum port for a module administrative service (recommend 52560)" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     tMinPort AT ROW 2.14 COL 24.6 COLON-ALIGNED WIDGET-ID 2
     tMaxPort AT ROW 3.33 COL 24.6 COLON-ALIGNED WIDGET-ID 4
     Btn_OK AT ROW 5.1 COL 11.4
     Btn_Cancel AT ROW 5.1 COL 28.2
     SPACE(7.19) SKIP(0.80)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Administrative Services"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Administrative Services */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

 tAdminFile = os-getenv("appdata") + "\Alliant\compass2.dat".

 TEMP-TABLE data:READ-JSON("file", tAdminFile, "empty") NO-ERROR.
  

 find data 
   where data.f = "min" no-error.
 if available data 
  then tMinPort = data.v.
  else tMinPort = 52560.

 find data 
   where data.f = "max" no-error.
 if available data 
  then tMaxPort = data.v.
  else tMaxPort = 52599.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if tMinPort:input-value = 0 and tMaxPort:input-value = 0 
   then
    do: os-delete value(tAdminFile).
        leave.
    end.

  if tMinPort:input-value <= 1024 
    or tMaxPort:input-value <= 1024
   then
    do: 
        MESSAGE "Port numbers must be above 1024"
         VIEW-AS ALERT-BOX INFO BUTTONS OK.
        next.
    end.

  if tMinPort:input-value >= 65535 
    or tMaxPort:input-value >= 65535
   then
    do: 
        MESSAGE "Port numbers must be below 65535"
         VIEW-AS ALERT-BOX INFO BUTTONS OK.
        next.
    end.
  
  if tMaxPort:input-value < tMinPort:input-value 
   then 
    do:
        MESSAGE "Maximum must be greater than minimum"
         VIEW-AS ALERT-BOX INFO BUTTONS OK.
        next.
    end.

  empty temp-table data.
  create data.
  data.f = "min".
  data.v = tMinPort:input-value.

  create data.
  data.f = "max".
  data.v = tMaxPort:input-value.

  TEMP-TABLE data:WRITE-JSON("file", tAdminFile).
  leave.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tMinPort tMaxPort 
      WITH FRAME Dialog-Frame.
  ENABLE tMinPort tMaxPort Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

