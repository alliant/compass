&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{lib/std-def.i}

def var tMsg as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tUser tOldPw tNewPw bResetCompass tAlfUser ~
tAlfService tAlfUserid tAlfPassword bResetAlf tSfUser tSfService1 ~
tSfService2 tSfService3 tSfUserid tSfPassword bResetSf tArcUser tArcService ~
tArcUserid tArcPassword bResetArc RECT-1 RECT-2 RECT-3 RECT-4 
&Scoped-Define DISPLAYED-OBJECTS tUser tOldPw tNewPw tAlfUser tAlfService ~
tAlfUserid tAlfPassword tSfUser tSfService1 tSfService2 tSfService3 ~
tSfUserid tSfPassword tArcUser tArcService tArcUserid tArcPassword 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bResetAlf 
     LABEL "Reset" 
     SIZE 15 BY 1.14 TOOLTIP "Change the user's Alfresco password".

DEFINE BUTTON bResetArc 
     LABEL "Reset" 
     SIZE 15 BY 1.14 TOOLTIP "Change the user's ARC password".

DEFINE BUTTON bResetCompass 
     LABEL "Reset" 
     SIZE 15 BY 1.14 TOOLTIP "Change the user's Compass password (this should change all related subsystems)"
     FONT 6.

DEFINE BUTTON bResetSf 
     LABEL "Reset" 
     SIZE 15 BY 1.14 TOOLTIP "Change the user's Sharefile password".

DEFINE VARIABLE tAlfPassword AS CHARACTER FORMAT "X(256)":U 
     LABEL "Administrator Password" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tAlfService AS CHARACTER FORMAT "X(256)":U INITIAL "https://docs.alliantnational.com/alfresco/service/api/person/changepassword/&1" 
     LABEL "Service" 
     VIEW-AS FILL-IN 
     SIZE 112 BY 1 TOOLTIP "Replace &1 with the userID" NO-UNDO.

DEFINE VARIABLE tAlfUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "UserID" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tAlfUserid AS CHARACTER FORMAT "X(256)":U INITIAL "compass@alliantnational.com" 
     LABEL "Administrator UserID" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tArcPassword AS CHARACTER FORMAT "X(256)":U 
     LABEL "Administrator Password" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tArcService AS CHARACTER FORMAT "X(256)":U INITIAL "http://apitest.alliantnational.com/user/&1/changepassword" 
     LABEL "Service" 
     VIEW-AS FILL-IN 
     SIZE 112 BY 1 TOOLTIP "Replace &1 with the userID" NO-UNDO.

DEFINE VARIABLE tArcUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "UserID" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tArcUserid AS CHARACTER FORMAT "X(256)":U INITIAL "compass@alliantnational.com" 
     LABEL "Administrator UserID" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tNewPw AS CHARACTER FORMAT "X(256)":U 
     LABEL "New Password" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tOldPw AS CHARACTER FORMAT "X(256)":U 
     LABEL "Old Password" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tSfPassword AS CHARACTER FORMAT "X(256)":U 
     LABEL "Administrator Password" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tSfService1 AS CHARACTER FORMAT "X(256)":U INITIAL "https://alliantnational.sharefile.com/rest/getAuthID.aspx?op=login&fmt=xml&username=&1&password=&2" 
     LABEL "Login Service" 
     VIEW-AS FILL-IN 
     SIZE 112 BY 1 TOOLTIP "Replace &1 with the Admin userID and &2 with Admin pw" NO-UNDO.

DEFINE VARIABLE tSfService2 AS CHARACTER FORMAT "X(256)":U INITIAL "https://alliantnational.sharefile.com/rest/users.aspx?op=get&fmt=xml&id=&1@alliantnational.com&authid=&2" 
     LABEL "User Service" 
     VIEW-AS FILL-IN 
     SIZE 112 BY 1 TOOLTIP "Replace &1 with the Sharefile userID and &2 with the AuthID" NO-UNDO.

DEFINE VARIABLE tSfService3 AS CHARACTER FORMAT "X(256)":U INITIAL "https://alliantnational.sharefile.com/rest/users.aspx?op=resetp&fmt=xml&id=&1&oldp=&2&newp=&3&authid=&4" 
     LABEL "PW Service" 
     VIEW-AS FILL-IN 
     SIZE 111.8 BY 1 TOOLTIP "Replace &1 with the SF userID, &2 with the new pw, and &3 with the AuthID" NO-UNDO.

DEFINE VARIABLE tSfUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "UserID" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE VARIABLE tSfUserid AS CHARACTER FORMAT "X(256)":U INITIAL "compass@alliantnational.com" 
     LABEL "Administrator UserID" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE tUser AS CHARACTER FORMAT "X(256)":U 
     LABEL "UserID" 
     VIEW-AS FILL-IN 
     SIZE 47 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 132 BY 5.48.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 132 BY 8.1.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 132 BY 5.48.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 21 BY 2.14.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     tUser AT ROW 1.48 COL 17 COLON-ALIGNED WIDGET-ID 14
     tOldPw AT ROW 2.67 COL 17 COLON-ALIGNED WIDGET-ID 16
     tNewPw AT ROW 3.86 COL 17 COLON-ALIGNED WIDGET-ID 18
     bResetCompass AT ROW 3.29 COL 71.8 WIDGET-ID 62
     tAlfUser AT ROW 5.76 COL 17 COLON-ALIGNED WIDGET-ID 42
     tAlfService AT ROW 6.95 COL 17 COLON-ALIGNED WIDGET-ID 2
     tAlfUserid AT ROW 8.19 COL 29 COLON-ALIGNED WIDGET-ID 4
     tAlfPassword AT ROW 9.38 COL 29 COLON-ALIGNED WIDGET-ID 6
     bResetAlf AT ROW 5.67 COL 71.8 WIDGET-ID 12
     tSfUser AT ROW 11.71 COL 17 COLON-ALIGNED WIDGET-ID 34
     tSfService1 AT ROW 13.05 COL 17 COLON-ALIGNED WIDGET-ID 32
     tSfService2 AT ROW 14.19 COL 17 COLON-ALIGNED WIDGET-ID 38
     tSfService3 AT ROW 15.33 COL 17.2 COLON-ALIGNED WIDGET-ID 40
     tSfUserid AT ROW 16.71 COL 30 COLON-ALIGNED WIDGET-ID 36
     tSfPassword AT ROW 17.91 COL 30 COLON-ALIGNED WIDGET-ID 30
     bResetSf AT ROW 11.62 COL 71.8 WIDGET-ID 56
     tArcUser AT ROW 20.29 COL 17 COLON-ALIGNED WIDGET-ID 50
     tArcService AT ROW 21.48 COL 17 COLON-ALIGNED WIDGET-ID 48
     tArcUserid AT ROW 22.71 COL 29 COLON-ALIGNED WIDGET-ID 52
     tArcPassword AT ROW 23.91 COL 29 COLON-ALIGNED WIDGET-ID 46
     bResetArc AT ROW 20.19 COL 71.8 WIDGET-ID 58
     "Compass" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 2.38 COL 70.4 WIDGET-ID 74
          FONT 6
     "Alfresco" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 5.05 COL 4 WIDGET-ID 10
          FONT 6
     "ARC" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 19.57 COL 4 WIDGET-ID 54
          FONT 6
     "Sharefile" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 11 COL 4 WIDGET-ID 24
          FONT 6
     RECT-1 AT ROW 5.29 COL 2 WIDGET-ID 8
     RECT-2 AT ROW 11.24 COL 2 WIDGET-ID 22
     RECT-3 AT ROW 19.81 COL 2 WIDGET-ID 44
     RECT-4 AT ROW 2.67 COL 69.2 WIDGET-ID 64
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 133.8 BY 24.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Password Reset"
         HEIGHT             = 24.43
         WIDTH              = 133.8
         MAX-HEIGHT         = 39.86
         MAX-WIDTH          = 288
         VIRTUAL-HEIGHT     = 39.86
         VIRTUAL-WIDTH      = 288
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Password Reset */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Password Reset */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bResetAlf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bResetAlf C-Win
ON CHOOSE OF bResetAlf IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run util/setalfrescopassword.p (tAlfUser:screen-value in frame {&frame-name},
                                 tOldPw:screen-value,
                                 tNewPw:screen-value,
                                 output std-lo,
                                 output std-ch).

  MESSAGE "Success: " std-lo skip(2) std-ch
      VIEW-AS ALERT-BOX INFO BUTTONS OK title "Alfresco Password Change".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bResetArc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bResetArc C-Win
ON CHOOSE OF bResetArc IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run util/setarcpassword.p (tArcUser:screen-value in frame {&frame-name},
                            tNewPw:screen-value,
                            output std-lo,
                            output std-ch).

  MESSAGE "Success: " std-lo skip(2) std-ch
      VIEW-AS ALERT-BOX INFO BUTTONS OK title "ARC Password Change".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bResetCompass
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bResetCompass C-Win
ON CHOOSE OF bResetCompass IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run server/setuserpassword.p (tUser:screen-value in frame {&frame-name},
                                 tNewPw:screen-value,
                                 output std-lo,
                                 output std-ch).

  MESSAGE "Success: " std-lo skip(2) std-ch
      VIEW-AS ALERT-BOX INFO BUTTONS OK title "Compass Password Change".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bResetSf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bResetSf C-Win
ON CHOOSE OF bResetSf IN FRAME DEFAULT-FRAME /* Reset */
DO:
  run util/setsharefilepassword.p (tSfUser:screen-value in frame {&frame-name},
                                  tOldPw:screen-value,
                                  tNewPw:screen-value,
                                  output std-lo,
                                  output std-ch).
  MESSAGE "Success: " std-lo skip(2) std-ch
      VIEW-AS ALERT-BOX INFO BUTTONS OK title "Sharefile Password Change".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tUser C-Win
ON LEAVE OF tUser IN FRAME DEFAULT-FRAME /* UserID */
DO:
  tAlfUser:screen-value = tUser:screen-value.
  tSfUser:screen-value = tUser:screen-value.
  tArcUser:screen-value = tUser:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "GetSystemParameter" anywhere.
subscribe to "IsPasswordResetOpen" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tUser tOldPw tNewPw tAlfUser tAlfService tAlfUserid tAlfPassword 
          tSfUser tSfService1 tSfService2 tSfService3 tSfUserid tSfPassword 
          tArcUser tArcService tArcUserid tArcPassword 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE tUser tOldPw tNewPw bResetCompass tAlfUser tAlfService tAlfUserid 
         tAlfPassword bResetAlf tSfUser tSfService1 tSfService2 tSfService3 
         tSfUserid tSfPassword bResetSf tArcUser tArcService tArcUserid 
         tArcPassword bResetArc RECT-1 RECT-2 RECT-3 RECT-4 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSystemParameter C-Win 
PROCEDURE GetSystemParameter :
/*------------------------------------------------------------------------------
  Purpose:     This routine mimics the server to make these calls available via
                a GUI client
  Parameters:  Input name of parameter typically stored in the server INI
               Output value of the parameter
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pName as char.
def output parameter pValue as char.

case pName:
 when "AlfrescoPasswordService" 
  then pValue = tAlfService:screen-value in frame {&frame-name}.
 when "AlfrescoAdminID" 
  then pValue = tAlfUserID:screen-value in frame {&frame-name}.
 when "AlfrescoAdminPW" 
  then pValue = tAlfPassword:screen-value in frame {&frame-name}.

 when "SharefileLoginService" 
  then pValue = tSfService1:screen-value in frame {&frame-name}.
 when "SharefileUserService" 
  then pValue = tSfService2:screen-value in frame {&frame-name}.
 when "SharefilePasswordService" 
  then pValue = tSfService3:screen-value in frame {&frame-name}.
 when "SharefileAdminID" 
  then pValue = tSfUserID:screen-value in frame {&frame-name}.
 when "SharefileAdminPW" 
  then pValue = tSfPassword:screen-value in frame {&frame-name}.

 when "ARCPasswordService" 
  then pValue = tArcService:screen-value in frame {&frame-name}.
 when "ARCAdminID" 
  then pValue = tArcUserID:screen-value in frame {&frame-name}.
 when "ARCAdminPW" 
  then pValue = tArcPassword:screen-value in frame {&frame-name}.

end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsPasswordResetOpen C-Win 
PROCEDURE IsPasswordResetOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pOpen as logical init true no-undo.

 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

