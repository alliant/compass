&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* authdialog.w
   D.Sinclair
   6.14.2012
   Visualize and update credentials
   9.28.2014 D.Sinclair Added mechanism to view password as entered.  Too bad
                        Progress doesn't have a on-mouse-out event.  As a result
                        there is some rudimentary events to try to consistently
                        hide the now visualized password.
 */

define input parameter pID as character no-undo.
define input parameter pPass as character no-undo.

{lib/std-def.i}

def var tVerified as logical init false no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bEye tID tPassword1 bDelete bTest tName ~
tInitials bCancel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Select to not update the local credentials cache (may result in no data)".

DEFINE BUTTON bDelete 
     LABEL "Reset" 
     SIZE 15 BY 1.19 TOOLTIP "Select to purge the local credentials cache in case of inconsistency".

DEFINE BUTTON bEye  NO-FOCUS
     LABEL "View" 
     SIZE 4.6 BY 1.1 TOOLTIP "View/Hide password".

DEFINE BUTTON bOK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Select to update the local credentials cache"
     BGCOLOR 8 .

DEFINE BUTTON bTest 
     LABEL "Verify" 
     SIZE 15 BY 1.19 TOOLTIP "Select to validate the entered credentials prior to use during this session"
     BGCOLOR 8 .

DEFINE VARIABLE tID AS CHARACTER FORMAT "X(100)":U 
     LABEL "User ID" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 TOOLTIP "Enter your user identifier (usually first initial + full last name)" NO-UNDO.

DEFINE VARIABLE tInitials AS CHARACTER FORMAT "X(3)":U 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "User initials" NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 28.6 BY 1 TOOLTIP "Your full name" NO-UNDO.

DEFINE VARIABLE tPassword1 AS CHARACTER FORMAT "X(20)":U 
     LABEL "Password" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 TOOLTIP "Enter your Enterprise Services password" NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57 BY 5.24.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bEye AT ROW 3.48 COL 52 WIDGET-ID 24 NO-TAB-STOP 
     tID AT ROW 2.33 COL 13.4 COLON-ALIGNED WIDGET-ID 6
     tPassword1 AT ROW 3.52 COL 13.4 COLON-ALIGNED WIDGET-ID 8 PASSWORD-FIELD 
     bDelete AT ROW 5.05 COL 36 WIDGET-ID 20
     bTest AT ROW 5.1 COL 15.4 WIDGET-ID 12
     tName AT ROW 7.91 COL 14 COLON-ALIGNED WIDGET-ID 2
     tInitials AT ROW 7.91 COL 43.8 COLON-ALIGNED NO-LABEL WIDGET-ID 4
     bCancel AT ROW 9.57 COL 16 WIDGET-ID 18
     bOK AT ROW 9.57 COL 32
     "Session Credentials" VIEW-AS TEXT
          SIZE 22.6 BY .62 AT ROW 1.24 COL 4.4 WIDGET-ID 16
          FONT 6
     RECT-1 AT ROW 1.48 COL 3 WIDGET-ID 14
     SPACE(2.19) SKIP(4.70)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Server Credentials"
         DEFAULT-BUTTON bTest WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bOK IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tID IN FRAME fMain
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN tInitials IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tInitials:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tName IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPassword1 IN FRAME fMain
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON ENTRY OF FRAME fMain /* Server Credentials */
or "entry" of bTest
or "entry" of bOk
or "entry" of bCancel
or "entry" of bDelete
or "entry" of tID

DO:
  tPassword1:visible = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Server Credentials */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete fMain
ON CHOOSE OF bDelete IN FRAME fMain /* Reset */
DO:
  publish "DeleteCredentials".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEye
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEye fMain
ON MOUSE-SELECT-DOWN OF bEye IN FRAME fMain /* View */
DO:
  tPassword1:password-field = false.
  tPassword1:read-only = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEye fMain
ON MOUSE-SELECT-UP OF bEye IN FRAME fMain /* View */
DO:
  tPassword1:password-field = true.
  tPassword1:read-only = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTest fMain
ON CHOOSE OF bTest IN FRAME fMain /* Verify */
DO:
 run doTest in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tID fMain
ON LEAVE OF tID IN FRAME fMain /* User ID */
DO:
 if self:modified 
  then tVerified = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tID fMain
ON VALUE-CHANGED OF tID IN FRAME fMain /* User ID */
DO:
  tVerified = false.
  bOK:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName fMain
ON ANY-PRINTABLE OF tName IN FRAME fMain /* Name */
DO:
  bOk:sensitive in frame {&frame-name} = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPassword1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPassword1 fMain
ON LEAVE OF tPassword1 IN FRAME fMain /* Password */
DO:
  if self:modified 
   then tVerified = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPassword1 fMain
ON VALUE-CHANGED OF tPassword1 IN FRAME fMain /* Password */
DO:
  tVerified = false.
  bOK:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

aSSIGN 
   {&window-NAME}:X = (SESSION:WIDTH-PIXELS -  {&window-NAME}:WIDTH-PIXELS) / 2
   {&window-NAME}:Y = (SESSION:HEIGHT-PIXELS -  {&window-NAME}:HEIGHT-PIXELS) / 2.

assign
  tID:screen-value in frame fMain = pID
  tPassword1:screen-value in frame fMain = pPass
  .
bEye:load-image("images/s-eye.bmp").


MAIN-BLOCK:
REPEAT ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if tVerified 
   then 
    do:
      publish "SetCredentialsID" (pID).
      publish "SetCredentialsPassword" (pPass).
      /* Name and Initials only come from the server */
      publish "SetCredentialsName" (tName:screen-value in frame fMain).
      publish "SetCredentialsInitials" (tInitials:screen-value in frame fMain).
      publish "UserDataChanged".
    end.

  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doTest fMain 
PROCEDURE doTest PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tTempInitials as character no-undo.
  define variable tTempName as character no-undo.

  do with frame {&frame-name}:
    assign
      tName:sensitive = false
      tInitials:sensitive = false
      tID:sensitive = false
      tPassword1:sensitive = false
      bTest:sensitive = false
      bOK:sensitive = false
      bCancel:sensitive = false
      .
    
    publish "ValidateCredentials" (tID:screen-value, tPassword1:screen-value, output std-lo, output std-ch).
    
    assign
      pID = tID:screen-value
      pPass = tPassword1:screen-value
      tID:sensitive = true
      tPassword1:sensitive = true
      bTest:sensitive = true
      bCancel:sensitive = true
      .

    if std-lo
     then
      do:
        MESSAGE "Credentials are valid" VIEW-AS ALERT-BOX information BUTTONS OK title "Verification Successful".
        publish "GetCredentialsInitials" (output tTempInitials).
        publish "GetCredentialsName" (output tTempName).
        assign
          tInitials:screen-value = tTempInitials
          tName:screen-value = tTempName
          bOK:sensitive = true
          .
      end.
     else MESSAGE std-ch VIEW-AS ALERT-BOX error BUTTONS OK title "Verification Failed".
  end.
  
  tVerified = std-lo.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE bEye tID tPassword1 bDelete bTest tName tInitials bCancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

