&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* splash.w
   display a startup splash window
   D.Sinclair 1.1.2012
   
   The seemingly wierd presence of frame-A is due to how mksplash.p leaves a
   visible gap on the right-hand side of the borderless splash window.  In order
   to balance it, we created an empty (but color-filled) frame on the left
   so it seems to be on purpose.

   11.26.2014 D.Sinclair    Added progress bar and associated procedures.
   09.04.2015 J.Oliver      Hide and show window for better UI experience.
                            The process works by sending the counter of where
                            the progress bar is at before closing and when
                            opening back again, set the counter to wherever it
                            was before the splash screen closed.
 */

CREATE WIDGET-POOL.

def var tStartTime as datetime no-undo.
def var tCounter as int no-undo.
def var tCounterMax as int init 8 no-undo.

{lib/std-def.i}

tStartTime = now.


/* The following procedure is used to prevent the 'not responding' message
*/
&SCOPED-DEFINE PM_NOREMOVE 0

PROCEDURE PeekMessageA EXTERNAL "user32.dll" :
    DEFINE INPUT  PARAMETER lpmsg         AS MEMPTR.
    DEFINE INPUT  PARAMETER hWnd          AS LONG.
    DEFINE INPUT  PARAMETER wMsgFilterMin AS LONG.
    DEFINE INPUT  PARAMETER wMsgFilterMax AS LONG.
    DEFINE INPUT  PARAMETER wRemoveMsg    AS LONG.
    DEFINE RETURN PARAMETER lResult       AS LONG.
END PROCEDURE.

DEFINE VARIABLE Msg     AS MEMPTR  NO-UNDO.
DEFINE VARIABLE lResult AS INTEGER NO-UNDO.

SET-SIZE(Msg) = 48. /* big enough for 64-bit */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bLogo 
&Scoped-Define DISPLAYED-OBJECTS tApplication 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL PRIVATE
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bLogo  NO-FOCUS FLAT-BUTTON
     LABEL "" 
     SIZE 7.6 BY 1.81.

DEFINE VARIABLE tApplication AS CHARACTER FORMAT "X(256)":U INITIAL "Quality Assurance Management" 
      VIEW-AS TEXT 
     SIZE 37 BY .95
     FONT 6 NO-UNDO.

DEFINE IMAGE imgBkg
     FILENAME "adeicon/blank":U
     SIZE 50 BY 7.76.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bLogo AT ROW 1.19 COL 51.4 WIDGET-ID 50 NO-TAB-STOP 
     tApplication AT ROW 6.91 COL 16.4 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     imgBkg AT ROW 3.29 COL 1 WIDGET-ID 62
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.8 ROW 1
         SIZE 59 BY 10.1
         BGCOLOR 15  WIDGET-ID 100.

DEFINE FRAME FRAME-A
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         
         AT COL 1 ROW 1
         SIZE 3.8 BY 10.1 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Compass"
         HEIGHT             = 10.1
         WIDTH              = 59.8
         MAX-HEIGHT         = 15.76
         MAX-WIDTH          = 82.2
         VIRTUAL-HEIGHT     = 15.76
         VIRTUAL-WIDTH      = 82.2
         SMALL-TITLE        = yes
         SHOW-IN-TASKBAR    = no
         CONTROL-BOX        = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         ALWAYS-ON-TOP      = yes
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         MESSAGE-AREA       = no
         SENSITIVE          = no.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SENSITIVE        = FALSE.

/* SETTINGS FOR IMAGE imgBkg IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tApplication IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME FRAME-A
   UNDERLINE                                                            */
ASSIGN 
       FRAME FRAME-A:SENSITIVE        = FALSE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fMain
/* Query rebuild information for FRAME fMain
     _Query            is NOT OPENED
*/  /* FRAME fMain */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-A
/* Query rebuild information for FRAME FRAME-A
     _Query            is NOT OPENED
*/  /* FRAME FRAME-A */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 10.52
       COLUMN          = 40
       HEIGHT          = .48
       WIDTH           = 19.6
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Compass */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Compass */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bLogo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bLogo C-Win
ON CHOOSE OF bLogo IN FRAME fMain
DO:
  run SetSplashStatus in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}
       .

/* Force frame/window realization to pass valid hwnd to Windows API */
if frame {&frame-name}:border-bottom > 0
 then .
run sys/MkSplash.p ({&WINDOW-NAME}:HWND, true, false).

subscribe to "SetSplashCounter" anywhere.
subscribe to "SetSplashStatus" anywhere.
subscribe to "CloseSplash" anywhere.
subscribe to "HideSplash" anywhere.
subscribe to "ShowSplash" anywhere.

session:immediate-display = yes.
SESSION:MULTITASKING-INTERVAL = 100.
session:set-wait-state("General").
{&window-name}:hidden = false.

RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).

/* imgLogo:load-image("images/ANcompass.jpg"). */
imgBkg:load-image("images/splashcompass.bmp").
publish "GetAppName" (output tApplication).

std-ch = string(today).
publish "GetAppRevised" (output std-ch).
std-da = date(std-ch) no-error.
if error-status:error
 then std-ch = string(year(today)).
 else std-ch = string(year(std-da)).
/*
tCopyright = "Copyright " + std-ch.
*/
publish "GetLargeLogo" (output std-ch).
if search(std-ch) <> ? 
 then bLogo:load-image(std-ch).
 else bLogo:hidden = true.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Dimensions need to be updated as a result of mksplash.p */
{&window-name}:height-pixels = imgBkg:y + imgBkg:height-pixels.

{&window-name}:hidden = false.
/* MESSAGE "9" skip                         */
/*         {&window-name}:width-pixels skip */
/*         imgBkg:width-pixels              */
/*         imgBkg:x                         */
/*  VIEW-AS ALERT-BOX INFO BUTTONS OK.      */
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
    
  session:set-wait-state("").
  SESSION:MULTITASKING-INTERVAL = 0.
  session:immediate-display = no.     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseSplash C-Win 
PROCEDURE CloseSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tElapsedTime as int no-undo.

 tElapsedTime = interval(now, tStartTime, "seconds").
 
 /* Increment to 2 (long enough to review splash screen) */
 do while tElapsedTime < 2:
  run SetSplashStatus in this-procedure.
  pause 1 no-message.
  tElapsedTime = tElapsedTime + 1.
 end.

 pbUpdateStatus(100, 1).
 pbMinStatus().

 apply "Close" to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "splash.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "splash.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  VIEW FRAME FRAME-A IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-A}
  FRAME FRAME-A:SENSITIVE = NO.
  DISPLAY tApplication 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bLogo 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  FRAME fMain:SENSITIVE = NO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HideSplash C-Win 
PROCEDURE HideSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def output parameter pNbr as integer no-undo.

  /* Prevent Not Responding and Update Process Status */ 
  RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).
  
  pNbr = tCounter.
  apply "Close" to this-procedure.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetSplashCounter C-Win 
PROCEDURE SetSplashCounter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       Eight is the default number of calls made to SetSplashStatus
                by lib/appstart.i.  Simply adjust this denominator for the 
                module specific number of calls (usually added in the data
                model) to make the progress bar meaningful.
------------------------------------------------------------------------------*/
 def input parameter tMax as int.

 /* Prevent Not Responding and Update Process Status */ 
 RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).
 
 tCounterMax = max(8, tMax).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetSplashStatus C-Win 
PROCEDURE SetSplashStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
  /* Prevent Not Responding and Update Process Status */ 
  RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).
 
  tCounter = tCounter + 1.

  pbUpdateStatus(int(min(tCounter / tCounterMax, .99) * 100), 0). /* Don't hit 100%...yet */

/*  if tCounter > 1                                                   */
/*   then tCounter = 0.                                               */
/*  tInitializing:screen-value in frame fMain = fill(" .", tCounter). */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowSplash C-Win 
PROCEDURE ShowSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter pNbr as integer no-undo.
  
  /* Prevent Not Responding and Update Process Status */ 
  RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).
 
  tCounter = pNbr.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
  
  /* Prevent Not Responding and Update Process Status */ 
  RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL PRIVATE
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

/*   {&WINDOW-NAME}:move-to-top().  */
  
  /* Prevent Not Responding and Update Process Status */ 
  RUN PeekMessageA(Msg, 0, 0, 0, {&PM_NOREMOVE}, OUTPUT lResult).
 
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

