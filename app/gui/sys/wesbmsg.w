&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wesbmsg.w
   Window of ESB Messages
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/esbmsg.i}

{lib/std-def.i}

def var tAutoPlay as logical init false no-undo. /* This initially gets flipped in the MAIN block */
def var tCurrentSeq as int no-undo.

 DEFINE MENU popmenu TITLE "Actions"
  MENU-ITEM m_PopRefresh LABEL "Pause"
  rule
  MENU-ITEM m_PopSend LABEL "Send Message..."
  MENU-ITEM m_PopView LABEL "View Message..."
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwCodes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES esbmsg

/* Definitions for BROWSE brwCodes                                      */
&Scoped-define FIELDS-IN-QUERY-brwCodes esbmsg.rcvd esbmsg.appcode esbmsg.ext esbmsg.appuser esbmsg.subject esbmsg.action esbmsg.body   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCodes   
&Scoped-define SELF-NAME brwCodes
&Scoped-define QUERY-STRING-brwCodes FOR EACH esbmsg by esbmsg.rcvd
&Scoped-define OPEN-QUERY-brwCodes OPEN QUERY {&SELF-NAME} FOR EACH esbmsg by esbmsg.rcvd.
&Scoped-define TABLES-IN-QUERY-brwCodes esbmsg
&Scoped-define FIRST-TABLE-IN-QUERY-brwCodes esbmsg


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tStartup brwCodes bRefresh bExport bAdd ~
bDelete bList bReset bSend bView 
&Scoped-Define DISPLAYED-OBJECTS tStartup 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new subscription".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove a subscription".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export Messages".

DEFINE BUTTON bList  NO-FOCUS
     LABEL "List" 
     SIZE 7.2 BY 1.71 TOOLTIP "List subscriptions".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Pause".

DEFINE BUTTON bReset  NO-FOCUS
     LABEL "Reset" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset the Message Server connection".

DEFINE BUTTON bSend  NO-FOCUS
     LABEL "Send" 
     SIZE 7.2 BY 1.71 TOOLTIP "Send a message".

DEFINE BUTTON bView  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View the details of the selected message".

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 58 BY 1.91.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.4 BY 1.91.

DEFINE VARIABLE tStartup AS LOGICAL INITIAL no 
     LABEL "Launch on Startup" 
     VIEW-AS TOGGLE-BOX
     SIZE 23 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwCodes FOR 
      esbmsg SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCodes C-Win _FREEFORM
  QUERY brwCodes DISPLAY
      esbmsg.rcvd label "Received" width 30
 esbmsg.appcode label "APP" format "x(8)"
 esbmsg.ext label "External" format "Yes/No"
 esbmsg.appuser label "User" format "x(20)"
 esbmsg.subject label "Topic/Queue" format "x(20)"
 esbmsg.action label "Action" format "x(20)"
 esbmsg.body label "Content" format "x(255)" width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 137.8 BY 16.1 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tStartup AT ROW 1.52 COL 61.8 WIDGET-ID 22
     brwCodes AT ROW 3 COL 2 WIDGET-ID 200
     bRefresh AT ROW 1.1 COL 1 WIDGET-ID 4 NO-TAB-STOP 
     bExport AT ROW 1.1 COL 51.4 WIDGET-ID 20 NO-TAB-STOP 
     bAdd AT ROW 1.1 COL 29.8 WIDGET-ID 14 NO-TAB-STOP 
     bDelete AT ROW 1.1 COL 37 WIDGET-ID 16 NO-TAB-STOP 
     bList AT ROW 1.1 COL 44.2 WIDGET-ID 18 NO-TAB-STOP 
     bReset AT ROW 1.1 COL 22.6 WIDGET-ID 12 NO-TAB-STOP 
     bSend AT ROW 1.1 COL 8.2 WIDGET-ID 2 NO-TAB-STOP 
     bView AT ROW 1.1 COL 15.4 WIDGET-ID 10 NO-TAB-STOP 
     RECT-2 AT ROW 1 COL 1 WIDGET-ID 8
     RECT-3 AT ROW 1 COL 58.6 WIDGET-ID 24
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 139.6 BY 18.29 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Messages"
         HEIGHT             = 18.29
         WIDTH              = 139.6
         MAX-HEIGHT         = 18.29
         MAX-WIDTH          = 173
         VIRTUAL-HEIGHT     = 18.29
         VIRTUAL-WIDTH      = 173
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwCodes 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwCodes:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwCodes:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCodes
/* Query rebuild information for BROWSE brwCodes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH esbmsg by esbmsg.rcvd.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwCodes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Messages */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Messages */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Messages */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add */
DO:
 std-ch = "".
 MESSAGE "Add the following subscription: " set std-ch format "x(20)".
 if std-ch = ""
  then return.
 publish "AddEsbSubscription" (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
 MESSAGE "Remove the following subscription: " set std-ch format "x(20)".
 if std-ch = ""
  then return.
 publish "DeleteEsbSubscription" (std-ch).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bList C-Win
ON CHOOSE OF bList IN FRAME fMain /* List */
DO:
 publish "GetEsbSubscriptions" (output std-ch).

 MESSAGE "Current subscriptions: " std-ch
  VIEW-AS ALERT-BOX INFO.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  tAutoPlay = not tAutoPlay.
  if tAutoPlay 
   then 
    do: self:load-image("images/pause.bmp").
        self:tooltip = "Pause and do not refresh when a new message is received".

        menu-item m_PopRefresh:label in menu popmenu = "Pause".
        run getData in this-procedure.
    end.
   else 
    do: self:load-image("images/play.bmp").
        self:tooltip = "Refresh when a new message is received".
        menu-item m_PopRefresh:label in menu popmenu = "Refresh".
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReset
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReset C-Win
ON CHOOSE OF bReset IN FRAME fMain /* Reset */
DO:
 std-lo = false.
 MESSAGE "Please ensure you have a good network connection." skip
         "This can take a few seconds to complete." skip(1)
         "Continue?"
  VIEW-AS ALERT-BOX INFO BUTTONS yes-no update std-lo.
 if not std-lo 
  then return.

 publish "ResetEsb".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCodes
&Scoped-define SELF-NAME brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON DEFAULT-ACTION OF brwCodes IN FRAME fMain
DO:
  run showMsg in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON ROW-DISPLAY OF brwCodes IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON START-SEARCH OF brwCodes IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON VALUE-CHANGED OF brwCodes IN FRAME fMain
DO:
  if available esbmsg 
   then tCurrentSeq = esbmsg.seq.
   else tCurrentSeq = 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSend
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSend C-Win
ON CHOOSE OF bSend IN FRAME fMain /* Send */
DO:
 run sys\esbsenddialog.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME fMain /* View */
DO:
 run showMsg in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStartup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStartup C-Win
ON VALUE-CHANGED OF tStartup IN FRAME fMain /* Launch on Startup */
DO:
  publish "SetAppEsbLaunch" (self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/win-main.i}
{lib/brw-main.i}

subscribe to "IsExternalEventsOpen" anywhere.
subscribe to "ReceivedEsbMessage" anywhere.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

publish "GetAppEsbLaunch" (output tStartup).

bSend:load-image("images/email.bmp").
bView:load-image("images/open.bmp").
bReset:load-image("images/resetlink.bmp").
bAdd:load-image("images/new.bmp").
bDelete:load-image("images/delete.bmp").
bList:load-image("images/report.bmp").
bExport:load-image("images/excel.bmp").

brwCodes:popup-menu in frame fMain = menu popmenu:handle.

ON 'choose':U OF menu-item m_PopRefresh in menu popmenu
 DO: apply "CHOOSE" to bRefresh in frame {&frame-name}.
     RETURN.
 END.

ON 'choose':U OF menu-item m_PopSend in menu popmenu
DO:
 run sys\esbsenddialog.w.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopView in menu popmenu
DO:
 run showMsg in this-procedure.
 RETURN.
END.


ON 'mouse-select-down':U OF esbmsg.rcvd in browse brwCodes
 or 'mouse-select-down' of esbmsg.appcode in browse brwCodes
 or 'mouse-select-down' of esbmsg.ext in browse brwCodes
 or 'mouse-select-down' of esbmsg.appuser in browse brwCodes
 or 'mouse-select-down' of esbmsg.subject in browse brwCodes
DO:
 run sortData in this-procedure (self:name).
 RETURN.
END.

PAUSE 0 BEFORE-HIDE.

/* run getData in this-procedure. */
apply "CHOOSE" to bRefresh in frame {&frame-name}.


MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStartup 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tStartup brwCodes bRefresh bExport bAdd bDelete bList bReset bSend 
         bView 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 th = temp-table esbmsg:handle.

 if query brwCodes:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 std-ch = "".
 publish "GetReportDir" (output std-ch).

 run util/exporttable.p (table-handle th,
                         "esbmsg",
                         "for each esbmsg by esbmsg.rcvd descending ",
                         "rcvd,appuser,appcode,appid,ext,frameType,subject,appaction,entity,keyID,action,ipaddr,body",
                         "Received,User,Application Code,Unique ID,External,Frame,Subject,Server Action,Entity,Key,Action,IP,Message",
                         std-ch,
                         "EsbMessagesAsOf" 
                           + string(year(today))
                           + string(month(today))
                           + string(day(today))
                           + "-" + replace(string(time, "HH:MM:SS"), ":", "")
                           + ".csv",
                         true,
                         output std-ch,
                         output std-in).


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer esbmsg for esbmsg.
 def var tFile as char no-undo.
 
 close query brwCodes.
 empty temp-table esbmsg.

 publish "GetEsbMessages" (output table esbmsg).

 dataSortBy = "".
 dataSortDesc = yes.
 run sortData ("rcvd").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsExternalEventsOpen C-Win 
PROCEDURE IsExternalEventsOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pOpen as logical init true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReceivedEsbMessage C-Win 
PROCEDURE ReceivedEsbMessage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAppCode as char.
 def input parameter pAppAction as char.
 def input parameter pSubject as char.
 def input parameter pEntity as char.
 def input parameter pIpAddr as char.
 def input parameter pAction as char.
 def input parameter pKeyID as char.
 def input parameter pExt as logical.

 if not tAutoPlay 
  then return.

 tCurrentSeq = tCurrentSeq + 1.
 run getData.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showMsg C-Win 
PROCEDURE showMsg :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available esbmsg 
  then return.
 MESSAGE "From: " esbmsg.appid skip(2)
         "Headers:" skip
         esbmsg.headers skip (2)
         "Body:" skip
         esbmsg.body
  VIEW-AS ALERT-BOX INFO BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 {lib/brw-sortData.i}
 
 def buffer x-esbmsg for esbmsg.

 if hQueryHandle:num-results > 0 
  then
   do: find x-esbmsg
         where x-esbmsg.seq = tCurrentSeq no-error.
       if available x-esbmsg
        then
         do: std-ro = rowid(x-esbmsg).
             hQueryHandle:reposition-to-rowid(std-ro).
             browse brwCodes:select-focused-row().
         end.
       apply "VALUE-CHANGED" to brwCodes in frame fMain.
   end.
  else tCurrentSeq = 0.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwCodes:width-pixels = frame fmain:width-pixels - 10.
 brwCodes:height-pixels = frame fMain:height-pixels - 44.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

