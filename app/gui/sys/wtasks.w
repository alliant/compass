&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wtasks.w
   Window of Tasks
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/task.i}

{lib/std-def.i}

def var tCurrentSeq as int no-undo.

DEFINE MENU popmenu TITLE "Actions"
  MENU-ITEM m_PopRefresh LABEL "Refresh"
  rule
  MENU-ITEM m_PopAdd LABEL "Add New Task..."
  MENU-ITEM m_PopEdit LABEL "Modify Task..."
  MENU-ITEM m_PopComplete LABEL "Mark Task Complete"
  .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES task

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData task.stat task.dueDate task.subject task.priority task.owner   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH task by task.stat by task.dueDate by task.taskID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH task by task.stat by task.dueDate by task.taskID.
&Scoped-define TABLES-IN-QUERY-brwData task
&Scoped-define FIRST-TABLE-IN-QUERY-brwData task


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bComplete fStatus bAdd tPriority tDueDays ~
brwData bEdit bRefresh RECT-3 tStartup tAutoLoad 
&Scoped-Define DISPLAYED-OBJECTS fStatus tPriority tDueDays tStartup ~
tAutoLoad 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new task".

DEFINE BUTTON bComplete  NO-FOCUS
     LABEL "Complete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Mark the selected task as Completed".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "View/Modify the details of the selected task".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 7
     LIST-ITEM-PAIRS "All Open","ALL",
                     "Not Started","N",
                     "In Process","I",
                     "Completed","C",
                     "Waiting","W",
                     "Deferred","D",
                     "Cancelled","X"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tPriority AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Priority" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     LIST-ITEM-PAIRS "ALL","ALL",
                     "High","H",
                     "Normal","N",
                     "Low","L"
     DROP-DOWN-LIST
     SIZE 12.2 BY 1 NO-UNDO.

DEFINE VARIABLE tDueDays AS INTEGER FORMAT "->>9":U INITIAL 7 
     LABEL "Due Within" 
     VIEW-AS FILL-IN 
     SIZE 6.8 BY 1 TOOLTIP "Number of days; enter zero for only past due tasks" NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.2 BY 2.05.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 76.2 BY 2.05.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 35.2 BY 2.05.

DEFINE VARIABLE tAutoLoad AS LOGICAL INITIAL no 
     LABEL "Load when first opened" 
     VIEW-AS TOGGLE-BOX
     SIZE 26 BY .81 TOOLTIP "Check to load tasks during startup of this application" NO-UNDO.

DEFINE VARIABLE tStartup AS LOGICAL INITIAL no 
     LABEL "Launch on startup" 
     VIEW-AS TOGGLE-BOX
     SIZE 25 BY .81 TOOLTIP "Check to show this task window during startup of this application" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      task SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      task.stat label "Status" format "x(8)"
 task.dueDate label "Due Date" format "99/99/9999" width 12
 task.subject label "Subject" format "x(256)" width 70
 task.priority label "Priority" format "x(10)"
 task.owner label "Owner" format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 137.4 BY 15.05 ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bComplete AT ROW 1.52 COL 92.8 WIDGET-ID 18 NO-TAB-STOP 
     fStatus AT ROW 1.95 COL 10 COLON-ALIGNED WIDGET-ID 20
     bAdd AT ROW 1.52 COL 78.4 WIDGET-ID 14 NO-TAB-STOP 
     tPriority AT ROW 1.95 COL 34.6 COLON-ALIGNED WIDGET-ID 28
     tDueDays AT ROW 1.95 COL 59.4 COLON-ALIGNED WIDGET-ID 22
     brwData AT ROW 3.57 COL 1.6 WIDGET-ID 200
     bEdit AT ROW 1.52 COL 85.6 WIDGET-ID 10 NO-TAB-STOP 
     bRefresh AT ROW 1.52 COL 69.6 WIDGET-ID 4 NO-TAB-STOP 
     tStartup AT ROW 1.71 COL 109 WIDGET-ID 32 NO-TAB-STOP 
     tAutoLoad AT ROW 2.43 COL 109 WIDGET-ID 38 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.1 COL 2.8 WIDGET-ID 26
     "Options" VIEW-AS TEXT
          SIZE 7.8 BY .62 AT ROW 1.1 COL 105.2 WIDGET-ID 36
     RECT-2 AT ROW 1.38 COL 77.6 WIDGET-ID 8
     RECT-3 AT ROW 1.38 COL 1.8 WIDGET-ID 24
     RECT-4 AT ROW 1.38 COL 103.8 WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 139.4 BY 17.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "My Tasks"
         HEIGHT             = 17.81
         WIDTH              = 138.8
         MAX-HEIGHT         = 17.81
         MAX-WIDTH          = 173
         VIRTUAL-HEIGHT     = 17.81
         VIRTUAL-WIDTH      = 173
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData tDueDays fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH task by task.stat by task.dueDate by task.taskID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* My Tasks */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* My Tasks */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* My Tasks */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add */
DO:
  run addTask in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bComplete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bComplete C-Win
ON CHOOSE OF bComplete IN FRAME fMain /* Complete */
DO:
 run completeTask in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
DO:
 run showTask in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run showTask in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
 {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  if available task 
   then tCurrentSeq = task.taskID.
   else tCurrentSeq = 0.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAutoLoad
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAutoLoad C-Win
ON VALUE-CHANGED OF tAutoLoad IN FRAME fMain /* Load when first opened */
DO:
  publish "SetAppTaskLoad" (self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStartup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStartup C-Win
ON VALUE-CHANGED OF tStartup IN FRAME fMain /* Launch on startup */
DO:
  publish "SetAppTaskLaunch" (self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/win-main.i}
{lib/brw-main.i}

subscribe to "IsTasksOpen" anywhere.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

bRefresh:load-image("images/refresh.bmp").
bEdit:load-image("images/update.bmp").
bAdd:load-image("images/new.bmp").
bComplete:load-image("images/check.bmp").

publish "GetAppTaskLaunch" (tStartup).
publish "GetAppTaskLoad" (tAutoLoad).

brwData:popup-menu in frame fMain = menu popmenu:handle.

ON 'choose':U OF menu-item m_PopRefresh in menu popmenu
DO:
 run getData in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopAdd in menu popmenu
DO:
 run addTask in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopEdit in menu popmenu
DO:
 run showTask in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopComplete in menu popmenu
DO:
 run completeTask in this-procedure.
 RETURN.
END.


/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.
status input "" in window {&window-name}.
status default "" in window {&window-name}.

run getData in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addTask C-Win 
PROCEDURE addTask :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run sys/wtask.w persistent (0).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE completeTask C-Win 
PROCEDURE completeTask :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available task
  then return.
  
 assign
   std-lo = false
   std-ch = "Unable to complete task."
   . 
 publish "CompleteTask" (task.taskID,
                         output std-lo,
                         output std-ch).
 if not std-lo
  then message std-ch view-as alert-box warning.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fStatus tPriority tDueDays tStartup tAutoLoad 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bComplete fStatus bAdd tPriority tDueDays brwData bEdit bRefresh 
         RECT-3 tStartup tAutoLoad 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There are no tasks to export."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
    return.
   end.
   
 std-ha = temp-table task:handle.
 run util/exporttable.p
   (table-handle std-ha,
    "task",
    "for each task ",
    "",                 /* fields */
    "",                 /* labels */
    "",                 /* directory */
    "",                 /* filename */
    true,               /* open? */
    output std-ch,      /* msg */
    output std-lo).     /* count */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer task for task.
 def var tFile as char no-undo.
 
 close query brwData.
 empty temp-table task.

 publish "GetTasks" (output table task).
 
 std-ch = string(now).
 status input std-ch in window {&window-name}.
 status default std-ch in window {&window-name}.

 dataSortBy = "".
 dataSortDesc = false.
 run sortData ("stat").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsTasksOpen C-Win 
PROCEDURE IsTasksOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pOpen as logical init true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showTask C-Win 
PROCEDURE showTask :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available task 
  then return.
 run sys/wtask.w persistent (task.taskID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortdata.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwData:width-pixels = frame fmain:width-pixels - 10.
 brwData:height-pixels = frame fMain:height-pixels - 58.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

