&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME dlgUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS dlgUser 
/* 
@description A generic dialog to select users and pass the user list into another callback procedure

@returns The selected users

@author John Oliver
@created XX.XX.20156
@notes To only allow one selection, call the dialog like the following:
       run sys/userselect (output std-ch) "true".
       
Date          Name      Description
03/25/2019    Rahul     Modified to add input parameters for getsysusers.p 
                        in MainBlock       
 */
{lib/move-item.i}
{lib/add-delimiter.i}
{lib/std-def.i}
{tt/sysuser.i}
define input parameter pInputUserList as character no-undo.
define output parameter pOutputUserList as character no-undo.

/* Local Variable Definitions ---                                       */
define variable cUserList as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME dlgUser

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAvailableList tSelectedList bAddUser ~
bRemoveUser bOK bCancel 
&Scoped-Define DISPLAYED-OBJECTS tAvailableList tSelectedList 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddUser 
     LABEL ">" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add field to selected list".

DEFINE BUTTON bCancel AUTO-GO 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bOK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bRemoveUser 
     LABEL "<" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete field from selected list".

DEFINE VARIABLE tAvailableList AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     SIZE 24 BY 5.95 NO-UNDO.

DEFINE VARIABLE tSelectedList AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "ALL","ALL" 
     SIZE 24 BY 5.95 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME dlgUser
     tAvailableList AT ROW 2.19 COL 3 NO-LABEL WIDGET-ID 128
     tSelectedList AT ROW 2.19 COL 35 NO-LABEL WIDGET-ID 132
     bAddUser AT ROW 3.86 COL 28.6 WIDGET-ID 138
     bRemoveUser AT ROW 5.29 COL 28.6 WIDGET-ID 140
     bOK AT ROW 8.71 COL 14 WIDGET-ID 142
     bCancel AT ROW 8.71 COL 33 WIDGET-ID 136
     "Available Users:" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 1.48 COL 3 WIDGET-ID 130
     "Selected Users:" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 1.48 COL 35 WIDGET-ID 134
     SPACE(3.99) SKIP(8.32)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Select Users" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX dlgUser
   FRAME-NAME                                                           */
ASSIGN 
       FRAME dlgUser:SCROLLABLE       = FALSE
       FRAME dlgUser:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dlgUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dlgUser dlgUser
ON WINDOW-CLOSE OF FRAME dlgUser /* Select Users */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddUser dlgUser
ON CHOOSE OF bAddUser IN FRAME dlgUser /* > */
DO:
  do with frame {&frame-name}:
    MoveItem(tAvailableList:handle,tSelectedList:handle,",").
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel dlgUser
ON CHOOSE OF bCancel IN FRAME dlgUser /* Cancel */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveUser
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveUser dlgUser
ON CHOOSE OF bRemoveUser IN FRAME dlgUser /* < */
DO:
  do with frame {&frame-name}:
    MoveItem(tSelectedList:handle,tAvailableList:handle,",").
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAvailableList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAvailableList dlgUser
ON DEFAULT-ACTION OF tAvailableList IN FRAME dlgUser
DO:
  APPLY "CHOOSE":U TO bAddUser.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSelectedList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSelectedList dlgUser
ON DEFAULT-ACTION OF tSelectedList IN FRAME dlgUser
DO:
  APPLY "CHOOSE":U TO bRemoveUser.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK dlgUser 


/* ***************************  Main Block  *************************** */

/* create the user list if one wasn't provided */
if not pInputUserList > ""
 then
  do:
    publish "GetSysUsers" (output table sysuser).
    if not can-find(first sysuser)
     then run server/getsysusers.p (input "B",    /* ActionType (A)ctive,(I)nactive,(B)oth */
                                    input "ALL",  /* UID */
                                    input ?,      /* StartDate*/
                                    input ? ,     /* EndDate */
                                    input "",     /* fieldList delimiter "," */
                                    output table sysuser,
                                    output std-lo,
                                    output std-ch
                                    ).
    
    publish "GetCredentialsID" (output std-ch).
    for each sysuser
       where sysuser.isActive = true:

      /* don't add the current user to the list */
      if sysuser.uid = std-ch
       then next.
       
      pInputUserList = addDelimiter(pInputUserList,",") + sysuser.name + "," + sysuser.uid.
    end.
  end.
  
bAddUser:load-image-up("images/s-right.bmp").
bAddUser:load-image-insensitive("images/s-right-i.bmp").
bRemoveUser:load-image-up("images/s-left.bmp").
bRemoveUser:load-image-insensitive("images/s-left-i.bmp").

/* add the users */
tSelectedList:delete(1).
tAvailableList:list-item-pairs = pInputUserList.
                         
/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
       ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  
  if num-entries(tSelectedList:list-item-pairs) = ?
   then
    do:
      message "Please select at least one user" view-as alert-box information buttons ok.
      next.
    end.
    
  pOutputUserList = "".
  do std-in = 1 to num-entries(tSelectedList:list-item-pairs) / 2:
    pOutputUserList = addDelimiter(pOutputUserList,",") + entry(std-in * 2,tSelectedList:list-item-pairs).
  end.
  
  LEAVE MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI dlgUser  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME dlgUser.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI dlgUser  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAvailableList tSelectedList 
      WITH FRAME dlgUser.
  ENABLE tAvailableList tSelectedList bAddUser bRemoveUser bOK bCancel 
      WITH FRAME dlgUser.
  VIEW FRAME dlgUser.
  {&OPEN-BROWSERS-IN-QUERY-dlgUser}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

