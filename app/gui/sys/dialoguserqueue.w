&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogcomlog.p

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 04/13/20
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/sysqueue.i}
{lib/std-def.i}

/* Parameters Definitions ---                                           */
define input parameter table for sysqueue.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS flQueueID flAction flQueueDate flStat ~
edParam edDest 
&Scoped-Define DISPLAYED-OBJECTS flQueueID flAction flQueueDate flStat ~
edParam edDest 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE edDest AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 58 BY 3.38 NO-UNDO.

DEFINE VARIABLE edParam AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 58 BY 3.38 NO-UNDO.

DEFINE VARIABLE flAction AS CHARACTER FORMAT "X(256)":U 
     LABEL "Action" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE flQueueDate AS CHARACTER FORMAT "X(256)":U 
     LABEL "Queue" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE flQueueID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Queue ID" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE flStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     flQueueID AT ROW 1.48 COL 13 COLON-ALIGNED WIDGET-ID 26
     flAction AT ROW 2.57 COL 13 COLON-ALIGNED WIDGET-ID 48
     flQueueDate AT ROW 3.67 COL 13 COLON-ALIGNED WIDGET-ID 34
     flStat AT ROW 4.76 COL 13 COLON-ALIGNED WIDGET-ID 36
     edParam AT ROW 5.86 COL 15 NO-LABEL WIDGET-ID 40
     edDest AT ROW 9.33 COL 15 NO-LABEL WIDGET-ID 52
     "Parameters:" VIEW-AS TEXT
          SIZE 11.2 BY .62 AT ROW 5.81 COL 3 WIDGET-ID 30
     "Destinations:" VIEW-AS TEXT
          SIZE 12.2 BY .62 AT ROW 9.24 COL 2 WIDGET-ID 54
     SPACE(59.99) SKIP(3.18)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "View Queued Action" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edDest:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       edParam:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flAction:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flQueueDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flQueueID:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flStat:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* View Queued Action */
do:
  apply "END-ERROR":u to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&FRAME-NAME}:parent eq ?
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.
  
  /* sets default values to the widgets */
  run setData in this-procedure.

  wait-for go of frame {&FRAME-NAME}.
end.

run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flQueueID flAction flQueueDate flStat edParam edDest 
      WITH FRAME Dialog-Frame.
  ENABLE flQueueID flAction flQueueDate flStat edParam edDest 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setData Dialog-Frame 
PROCEDURE setData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first sysqueue no-error.
  if not available sysqueue 
   then
    return.

  assign 
      flQueueID:screen-value = string(sysqueue.queueID)
      flAction:screen-value  = sysqueue.actionID
      flQueueDate:screen-value  = string(sysqueue.queueDate)
      flStat:screen-value       = sysqueue.queuestatdesc 
      edParam:screen-value      = sysqueue.parameters
      edDest:screen-value       = sysqueue.destinations
      .     
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

