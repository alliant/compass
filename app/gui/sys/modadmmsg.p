&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
 sys/socketclt.p
 implements a SOCKET CLienT for a module
 D.Sinclair
 9.11.2013
 */

def input parameter pModule as char no-undo. 
def input parameter pText as char no-undo.
def output parameter pSuccess as logical init false no-undo.
def output parameter pMsg as char no-undo.

def var pPort as int no-undo.
def var hSocket as handle no-undo.
def var mContent as memptr no-undo.
def var tModuleFile as char no-undo.

{lib/std-def.i}
{tt/module.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

if pText = ""
 then 
  do: pMsg = "Message is blank".
      return.
  end.
if length(pText) > 50
 then 
  do: pMsg = "Message length must be less than 50 characters".
      return.
  end.

tModuleFile = os-getenv("appdata") + "\Alliant\compass3.dat".
TEMP-TABLE module:READ-JSON("file", tModuleFile, "empty") NO-ERROR.

find first module 
  where module.appCode = pModule 
    and module.appPort > 0 no-error.
if not available module 
 then 
  do: pMsg = "Module " + pModule + " is not running with an Admin Service".
      return.
  end.

if module.appPort < 1024 or module.appPort > 65536 
 then 
  do: pMsg = "Port " + string(module.appPort) + " is invalid".
      return.
  end.

create socket hSocket.
hSocket:connect("-H localhost -S " + string(module.appPort)) no-error.

if not hSocket:connected() 
 then
  do:
      pMsg = "Unable to connect to Admin Service on port " + string(pPort).
      if this-procedure:persistent 
       then delete procedure this-procedure.
      return.
  end.

hSocket:set-socket-option("SO-KEEPALIVE", "FALSE").

std-in = 4 + length(pText).
set-size(mContent) = 100.
put-long(mContent, 1) = length(pText).
put-string(mContent, 5) = pText.
std-lo = hSocket:write(mContent, 1, std-in).
hSocket:disconnect().
delete object hSocket.
set-size(mContent) = 0.

if not std-lo 
 then
  do: 
    pMsg = "Unable to send message to Admin Service on port " + string(pPort).
    return.
  end.

pSuccess = true.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


