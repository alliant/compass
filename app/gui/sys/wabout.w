&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* sys/wabout.w
   ABOUT as a Window rather than a dialog-box
Created D.Sinclair 6.29.2014

09.28.2014 D.Sinclair  Added "Data" tool to publish "DataDump" event.
09.04.2015 J.Oliver    Added a parameter to the authpwdialog.p run statement
05.07.2021 Shubham     Added myQueue functionality
 */

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winlaunch.i}

define variable hSysQueue as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bHome bApacheMQ bAzure bCurl tApp bDotr ~
tVersion tRevised bFamFam bProgress tSplash tDebug bSharefile bWget ~
bQueueAction bAdmin bServer bData bPassword bEvents bOS bSession bObjects ~
bPropath bTempFiles 
&Scoped-Define DISPLAYED-OBJECTS tApp tVersion tRevised tSplash tDebug 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD adjustWindowTitles C-Win 
FUNCTION adjustWindowTitles RETURNS LOGICAL PRIVATE
  ( pShowProgram as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdmin  NO-FOCUS
     LABEL "adm" 
     SIZE 4.4 BY 1.05 TOOLTIP "Administrative Password Reset".

DEFINE BUTTON bApacheMQ  NO-FOCUS FLAT-BUTTON
     LABEL "Apache ActiveMQ (activemq.apache.org)" 
     SIZE 41.6 BY 1.14 TOOLTIP "http://activemq.apache.org/".

DEFINE BUTTON bAzure  NO-FOCUS FLAT-BUTTON
     LABEL "Azure (azure.microsoft.com)" 
     SIZE 28.4 BY 1.14 TOOLTIP "https://azure.microsoft.com/".

DEFINE BUTTON bConfig  NO-FOCUS
     LABEL "Configuration" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Configuration".

DEFINE BUTTON bCurl  NO-FOCUS FLAT-BUTTON
     LABEL "cURL (curl.se)" 
     SIZE 16.2 BY 1.14 TOOLTIP "https://curl.se/".

DEFINE BUTTON bData  NO-FOCUS
     LABEL "data" 
     SIZE 4.4 BY 1.05 TOOLTIP "Export data".

DEFINE BUTTON bDotr  NO-FOCUS FLAT-BUTTON
     LABEL "Stomp (www.dotr.com)" 
     SIZE 23.8 BY 1.14 TOOLTIP "http://www.dotr.com/".

DEFINE BUTTON bEsb  NO-FOCUS
     LABEL "esb" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Received Messages".

DEFINE BUTTON bEvents  NO-FOCUS
     LABEL "Calls..." 
     SIZE 4.4 BY 1.05 TOOLTIP "View server requests".

DEFINE BUTTON bFamFam  NO-FOCUS FLAT-BUTTON
     LABEL "www.famfamfam.com/lab/icons/silk/" 
     SIZE 38 BY 1.14 TOOLTIP "http://www.famfamfam.com/lab/icons/silk/".

DEFINE BUTTON bHome  NO-FOCUS FLAT-BUTTON
     LABEL "Site" 
     SIZE 7.6 BY 1.81
     BGCOLOR 15 FGCOLOR 15 .

DEFINE BUTTON bObjects  NO-FOCUS
     LABEL "Objects" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Objects".

DEFINE BUTTON bOS  NO-FOCUS
     LABEL "Operating System" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Windows Environment".

DEFINE BUTTON bPassword 
     LABEL "Credentials" 
     SIZE 20.4 BY 1.05 TOOLTIP "Update user credentials for accessing the Application Server".

DEFINE BUTTON bProgress  NO-FOCUS FLAT-BUTTON
     LABEL "Progress WebClient (www.progress.com)" 
     SIZE 41 BY 1.14 TOOLTIP "http://www.progress.com/".

DEFINE BUTTON bPropath  NO-FOCUS
     LABEL "Path" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Path".

DEFINE BUTTON bQueueAction 
     LABEL "My Queue" 
     SIZE 20.4 BY 1.05 TOOLTIP "Get queued action of login user".

DEFINE BUTTON bServer 
     LABEL "Endpoints" 
     SIZE 20.4 BY 1.05 TOOLTIP "Update the Application Server address temporarily for this application".

DEFINE BUTTON bSession  NO-FOCUS
     LABEL "Session" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Session".

DEFINE BUTTON bSettings  NO-FOCUS
     LABEL "Options" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Options".

DEFINE BUTTON bSharefile  NO-FOCUS FLAT-BUTTON
     LABEL "Sharefile (www.sharefile.com)" 
     SIZE 30.2 BY 1.14 TOOLTIP "http://www.sharefile.com/".

DEFINE BUTTON bTask  NO-FOCUS
     LABEL "tasks" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Tasks".

DEFINE BUTTON bTempFiles  NO-FOCUS
     LABEL "Temporary Files" 
     SIZE 4.4 BY 1.05 TOOLTIP "View Temporary Files".

DEFINE BUTTON bWget  NO-FOCUS FLAT-BUTTON
     LABEL "WGET (www.gnu.org/software/wget/)" 
     SIZE 39 BY 1.14 TOOLTIP "http://www.gnu.org/software/wget/".

DEFINE VARIABLE tApp AS CHARACTER FORMAT "X(256)":U 
     LABEL "Application" 
     VIEW-AS FILL-IN 
     SIZE 46.4 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tRevised AS CHARACTER FORMAT "X(256)":U 
     LABEL "Released" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 59.6 BY 10.91.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 21.6 BY 4.05.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 21.6 BY 4.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 21.6 BY 2.1.

DEFINE VARIABLE tDebug AS LOGICAL INITIAL no 
     LABEL "Debug" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.8 BY .81 TOOLTIP "Select to enable debugging features" NO-UNDO.

DEFINE VARIABLE tSplash AS LOGICAL INITIAL no 
     LABEL "Splash" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.8 BY .81 TOOLTIP "Select to show startup splash screen" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bHome AT ROW 1.33 COL 4.2 WIDGET-ID 46 NO-TAB-STOP 
     bApacheMQ AT ROW 12.91 COL 16.4 WIDGET-ID 100 NO-TAB-STOP 
     bAzure AT ROW 11.71 COL 16.4 WIDGET-ID 92 NO-TAB-STOP 
     bCurl AT ROW 8.14 COL 16.4 WIDGET-ID 36 NO-TAB-STOP 
     bEsb AT ROW 4.29 COL 77.4 WIDGET-ID 84 NO-TAB-STOP 
     tApp AT ROW 1.24 COL 23.2 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     bTask AT ROW 4.29 COL 73 WIDGET-ID 88 NO-TAB-STOP 
     bDotr AT ROW 9.33 COL 16.4 WIDGET-ID 80 NO-TAB-STOP 
     tVersion AT ROW 2.43 COL 23.2 COLON-ALIGNED WIDGET-ID 6 NO-TAB-STOP 
     tRevised AT ROW 2.43 COL 55.6 COLON-ALIGNED WIDGET-ID 8 NO-TAB-STOP 
     bFamFam AT ROW 5.76 COL 16.4 WIDGET-ID 28 NO-TAB-STOP 
     bProgress AT ROW 4.57 COL 16.4 WIDGET-ID 38 NO-TAB-STOP 
     tSplash AT ROW 8.52 COL 67.2 WIDGET-ID 62
     tDebug AT ROW 9.33 COL 67.2 WIDGET-ID 96
     bSharefile AT ROW 10.52 COL 16.4 WIDGET-ID 66 NO-TAB-STOP 
     bWget AT ROW 6.95 COL 16.4 WIDGET-ID 32 NO-TAB-STOP 
     bQueueAction AT ROW 11.19 COL 63 WIDGET-ID 108
     bAdmin AT ROW 6.67 COL 77.4 WIDGET-ID 86 NO-TAB-STOP 
     bServer AT ROW 12.29 COL 63 WIDGET-ID 48
     bData AT ROW 5.48 COL 77.4 WIDGET-ID 106 NO-TAB-STOP 
     bPassword AT ROW 13.38 COL 63 WIDGET-ID 60
     bEvents AT ROW 5.48 COL 64.2 WIDGET-ID 26 NO-TAB-STOP 
     bOS AT ROW 6.67 COL 68.6 WIDGET-ID 74 NO-TAB-STOP 
     bSession AT ROW 5.48 COL 73 WIDGET-ID 72 NO-TAB-STOP 
     bObjects AT ROW 6.67 COL 73 WIDGET-ID 68 NO-TAB-STOP 
     bPropath AT ROW 6.67 COL 64.2 WIDGET-ID 70 NO-TAB-STOP 
     bTempFiles AT ROW 5.48 COL 68.6 WIDGET-ID 58 NO-TAB-STOP 
     bConfig AT ROW 4.29 COL 64.2 WIDGET-ID 54 NO-TAB-STOP 
     bSettings AT ROW 4.29 COL 68.6 WIDGET-ID 56 NO-TAB-STOP 
     "Documents:" VIEW-AS TEXT
          SIZE 12.4 BY .62 AT ROW 10.81 COL 4 WIDGET-ID 64
     "Server" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 10.43 COL 63.6 WIDGET-ID 52
          FONT 6
     "Platform:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 4.81 COL 7 WIDGET-ID 40
     "Libraries:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 7.19 COL 6.8 WIDGET-ID 34
     "Images:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 6 COL 7.8 WIDGET-ID 30
     "Tools" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 3.57 COL 63.4 WIDGET-ID 78
          FONT 6
     "Options" VIEW-AS TEXT
          SIZE 8.6 BY .62 AT ROW 7.91 COL 63.4 WIDGET-ID 104
          FONT 6
     "Resources" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 3.57 COL 2.8 WIDGET-ID 44
          FONT 6
     "Messaging:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 13.1 COL 4.8 WIDGET-ID 98
     RECT-2 AT ROW 3.91 COL 1.6 WIDGET-ID 42
     RECT-3 AT ROW 10.76 COL 62.4 WIDGET-ID 50
     RECT-4 AT ROW 3.91 COL 62.2 WIDGET-ID 76
     RECT-5 AT ROW 8.24 COL 62.2 WIDGET-ID 102
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 83.4 BY 14.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "About"
         HEIGHT             = 14.1
         WIDTH              = 83.4
         MAX-HEIGHT         = 19.81
         MAX-WIDTH          = 107.8
         VIRTUAL-HEIGHT     = 19.81
         VIRTUAL-WIDTH      = 107.8
         SHOW-IN-TASKBAR    = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
ASSIGN 
       bApacheMQ:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://activemq.apache.org/".

ASSIGN 
       bAzure:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "https://docs.alliantnational.com/share".

/* SETTINGS FOR BUTTON bConfig IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bCurl:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://wput.sourceforge.net/".

ASSIGN 
       bDotr:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://www.dotr.com/".

/* SETTINGS FOR BUTTON bEsb IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bFamFam:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://www.famfamfam.com/lab/icons/silk/".

ASSIGN 
       bHome:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://compass.alliantnational.com:8008/".

ASSIGN 
       bProgress:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://www.progress.com/".

/* SETTINGS FOR BUTTON bSettings IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bSharefile:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "https://alliantnational.sharefile.com/login.aspx".

/* SETTINGS FOR BUTTON bTask IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bWget:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "http://www.gnu.org/software/wget/".

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       tApp:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tRevised:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       tVersion:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* About */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* About */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME DEFAULT-FRAME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL DEFAULT-FRAME C-Win
ON MOUSE-MENU-DBLCLICK OF FRAME DEFAULT-FRAME
DO:
  std-ch = "".
  message "Task:" set std-ch format "x(50)".

  case std-ch:
   when "" or when ? or when "?" then return.
   when "EVENT"
    then 
     do: apply "CHOOSE" to bEvents in frame {&frame-name}.
         return.
     end.
   when "CFG"
    then 
     do: apply "CHOOSE" to bConfig in frame {&frame-name}.
         return.
     end.
   when "TASK"
    then 
     do: apply "CHOOSE" to bTask in frame {&frame-name}.
         return.
     end.
   when "DATA"
    then 
     do: apply "CHOOSE" to bData in frame {&frame-name}.
         return.
     end.
   when "ESB"
    then 
     do: apply "CHOOSE" to bEsb in frame {&frame-name}.
         return.
     end.
   when "OBJ"
    then 
     do: apply "CHOOSE" to bObjects in frame {&frame-name}.
         return.
     end.
   when "PW"
    then 
     do: apply "CHOOSE" to bAdmin in frame {&frame-name}.
         return.
     end.
  end case.

  do on error undo, return on stop undo, return:
   run value(std-ch) persistent no-error.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdmin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdmin C-Win
ON CHOOSE OF bAdmin IN FRAME DEFAULT-FRAME /* adm */
DO:
  std-lo = false.
  publish "IsPasswordResetOpen" (output std-lo).
  if std-lo 
   then return.
  run sys/wpasswordreset.w persistent.

/* das: The Module Admin Services is a "good idea" that is not practical due to local firewall issues */
/*   std-lo = false.                              */
/*   publish "IsModuleAdminOpen" (output std-lo). */
/*   if not std-lo                                */
/*    then run sys/wmodadmin.w persistent.        */
/*   apply "GO" to FRAME {&FRAME-NAME}.           */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bApacheMQ
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApacheMQ C-Win
ON CHOOSE OF bApacheMQ IN FRAME DEFAULT-FRAME /* Apache ActiveMQ (activemq.apache.org) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bApacheMQ C-Win
ON MOUSE-MENU-CLICK OF bApacheMQ IN FRAME DEFAULT-FRAME /* Apache ActiveMQ (activemq.apache.org) */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAzure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAzure C-Win
ON CHOOSE OF bAzure IN FRAME DEFAULT-FRAME /* Azure (azure.microsoft.com) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAzure C-Win
ON MOUSE-MENU-CLICK OF bAzure IN FRAME DEFAULT-FRAME /* Azure (azure.microsoft.com) */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bConfig C-Win
ON CHOOSE OF bConfig IN FRAME DEFAULT-FRAME /* Configuration */
DO:
/*      run sys/fileview.w persistent (self:private-data, "Configuration"). */
     run sys/configviewer.w persistent (true, self:private-data).
     apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCurl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCurl C-Win
ON CHOOSE OF bCurl IN FRAME DEFAULT-FRAME /* cURL (curl.se) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCurl C-Win
ON MOUSE-MENU-CLICK OF bCurl IN FRAME DEFAULT-FRAME /* cURL (curl.se) */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bData C-Win
ON CHOOSE OF bData IN FRAME DEFAULT-FRAME /* data */
DO:
  publish "DataDump".
  MESSAGE "Data Exported"
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDotr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDotr C-Win
ON CHOOSE OF bDotr IN FRAME DEFAULT-FRAME /* Stomp (www.dotr.com) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDotr C-Win
ON MOUSE-MENU-CLICK OF bDotr IN FRAME DEFAULT-FRAME /* Stomp (www.dotr.com) */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEsb
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEsb C-Win
ON CHOOSE OF bEsb IN FRAME DEFAULT-FRAME /* esb */
DO:

  std-lo = false.
  publish "IsExternalEventsOpen" (output std-lo).
  if std-lo 
   then return.

  run sys/wesbmsg.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEvents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEvents C-Win
ON CHOOSE OF bEvents IN FRAME DEFAULT-FRAME /* Calls... */
DO:
  std-lo = false.
  publish "IsEventViewerOpen" (output std-lo).
  if not std-lo 
   then run sys/eventviewer.w persistent.
  apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFamFam
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFamFam C-Win
ON CHOOSE OF bFamFam IN FRAME DEFAULT-FRAME /* www.famfamfam.com/lab/icons/silk/ */
DO:
  run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFamFam C-Win
ON MOUSE-MENU-CLICK OF bFamFam IN FRAME DEFAULT-FRAME /* www.famfamfam.com/lab/icons/silk/ */
DO:
  run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bHome
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bHome C-Win
ON CHOOSE OF bHome IN FRAME DEFAULT-FRAME /* Site */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bObjects
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bObjects C-Win
ON CHOOSE OF bObjects IN FRAME DEFAULT-FRAME /* Objects */
DO:
     run sys/wobjviewer.w persistent.
     apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOS
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOS C-Win
ON CHOOSE OF bOS IN FRAME DEFAULT-FRAME /* Operating System */
DO:
    run sys/wos.p.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPassword
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPassword C-Win
ON CHOOSE OF bPassword IN FRAME DEFAULT-FRAME /* Credentials */
DO:
  define variable tID as character no-undo.
  define variable tPass as character no-undo.
  
  publish "GetCredentialsID" (output tID).
  publish "GetCredentialsPassword" (output tPass).
  run sys/authdialog.w (tID, tPass).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bProgress
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bProgress C-Win
ON CHOOSE OF bProgress IN FRAME DEFAULT-FRAME /* Progress WebClient (www.progress.com) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bProgress C-Win
ON MOUSE-MENU-CLICK OF bProgress IN FRAME DEFAULT-FRAME /* Progress WebClient (www.progress.com) */
DO:
 run openUrl (self:private-data).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPropath
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPropath C-Win
ON CHOOSE OF bPropath IN FRAME DEFAULT-FRAME /* Path */
DO:
    run sys/wpropath.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQueueAction
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQueueAction C-Win
ON CHOOSE OF bQueueAction IN FRAME DEFAULT-FRAME /* My Queue */
DO:
  if valid-handle(hSysQueue)
   then 
    run ShowWindow in hSysQueue.
   else 
    run sys/wqueue.w persistent set hSysQueue.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bServer
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bServer C-Win
ON CHOOSE OF bServer IN FRAME DEFAULT-FRAME /* Endpoints */
DO:
  run sys/hostdialog.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSession
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSession C-Win
ON CHOOSE OF bSession IN FRAME DEFAULT-FRAME /* Session */
DO:
     run sys/wsession.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSettings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSettings C-Win
ON CHOOSE OF bSettings IN FRAME DEFAULT-FRAME /* Options */
DO:
/*     run sys/fileview.w persistent (self:private-data, "Options"). */
    run sys/configviewer.w persistent (false, self:private-data).
    apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSharefile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSharefile C-Win
ON CHOOSE OF bSharefile IN FRAME DEFAULT-FRAME /* Sharefile (www.sharefile.com) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSharefile C-Win
ON MOUSE-MENU-CLICK OF bSharefile IN FRAME DEFAULT-FRAME /* Sharefile (www.sharefile.com) */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTask
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTask C-Win
ON CHOOSE OF bTask IN FRAME DEFAULT-FRAME /* tasks */
DO:
  std-lo = false.
  publish "IsTasksOpen" (output std-lo).
  if not std-lo 
   then run sys/wtasks.w persistent.
  apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTempFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTempFiles C-Win
ON CHOOSE OF bTempFiles IN FRAME DEFAULT-FRAME /* Temporary Files */
DO:
    std-lo = false.
    publish "IsTempFilesOpen" (output std-lo).
    if not std-lo 
     then run sys/wtempfiles.w persistent.
    apply "GO" to FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bWget
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bWget C-Win
ON CHOOSE OF bWget IN FRAME DEFAULT-FRAME /* WGET (www.gnu.org/software/wget/) */
DO:
 run openUrl (self:tooltip).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bWget C-Win
ON MOUSE-MENU-CLICK OF bWget IN FRAME DEFAULT-FRAME /* WGET (www.gnu.org/software/wget/) */
DO:
 run openUrl (self:private-data).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDebug
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDebug C-Win
ON VALUE-CHANGED OF tDebug IN FRAME DEFAULT-FRAME /* Debug */
DO:
  publish "SetAppDebug" (self:checked).
  adjustWindowTitles(self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSplash
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSplash C-Win
ON VALUE-CHANGED OF tSplash IN FRAME DEFAULT-FRAME /* Splash */
DO:
  publish "SetAppSplash" (self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tVersion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tVersion C-Win
ON CTRL-C OF tVersion IN FRAME DEFAULT-FRAME /* Version */
DO:
  clipboard:value = encode(string(today)).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "IsAboutOpen" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


{lib/win-main.i}

publish "GetAppName" (output tApp).
publish "GetAppVersion" (output tVersion).
publish "GetAppRevised" (output tRevised).
publish "GetAppSplash" (output tSplash).
PUBLISH "GetAppDebug" (OUTPUT tDebug).

publish "GetAppSiteAddress" (output std-ch).
if std-ch > "" 
 then assign
        bHome:private-data = std-ch
        bHome:tooltip = std-ch
        .

publish "GetLargeLogo" (output std-ch).
if search(std-ch) <> ? 
 then bHome:load-image(std-ch).

bSettings:load-image("images/options.bmp").
bSettings:load-image-insensitive("images/options-i.bmp").
std-ch = "".

publish "GetSettingsFile" (output std-ch).
if std-ch > "" and search(std-ch) <> ? 
 then assign
        bSettings:sensitive in frame {&frame-name} = true
        bSettings:private-data = std-ch
/*         bSettings:tooltip in frame {&frame-name} =                       */
/*           bSettings:tooltip in frame {&frame-name} + " (" + std-ch + ")" */
        .

bConfig:load-image("images/settings.bmp").
bConfig:load-image-insensitive("images/settings-i.bmp").
std-ch = "".
publish "GetConfigFile" (output std-ch).
if std-ch > "" and search(std-ch) <> ? 
 then assign
        bConfig:sensitive in frame {&frame-name} = true
        bConfig:private-data = std-ch
/*         bConfig:tooltip in frame {&frame-name} =                        */
/*           bConfig:tooltip in frame {&frame-name} + " (" + std-ch + ")"  */
        .

bTask:load-image("images/s-task.bmp").
bTask:load-image-insensitive("images/s-task-i.bmp").
bData:load-image("images/s-data.bmp").

bEvents:load-image("images/s-lookup.bmp").
bEvents:load-image-insensitive("images/s-lookup-i.bmp").

bTempFiles:load-image("images/s-tempfiles.bmp").

bObjects:load-image("images/s-objects.bmp").
bPropath:load-image("images/s-propath.bmp").

bSession:load-image("images/s-session.bmp").
bOS:load-image("images/s-os.bmp").

bEsb:load-image("images/s-transmit.bmp").
bEsb:load-image-insensitive("images/s-transmit-i.bmp").
bAdmin:load-image("images/s-lock.bmp").

std-lo = false.
publish "GetAppTask" (output std-lo).
bTask:sensitive = std-lo.

std-lo = false.
publish "GetAppESB" (output std-lo).
bEsb:sensitive = std-lo.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tApp tVersion tRevised tSplash tDebug 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bHome bApacheMQ bAzure bCurl tApp bDotr tVersion tRevised bFamFam 
         bProgress tSplash tDebug bSharefile bWget bQueueAction bAdmin bServer 
         bData bPassword bEvents bOS bSession bObjects bPropath bTempFiles 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE HuntTree C-Win 
PROCEDURE HuntTree PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     This procedure walks the stack of the current widgets, discovering
               each window. Then it affixes to the window title the procedure file
               name that instantiated the window. This is done purly for the 
               convenience of the programmer and this routine should only be called
               upon some special button pressing, like F5 or something.

  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
    DEF INPUT PARAMETER root AS HANDLE.
    DEF INPUT PARAMETER level AS INT.
    def input parameter pShow as logical.
    def input-output parameter PPSChar as char.

    DEF VAR rH AS HANDLE.
    DEF VAR cH AS HANDLE.
    DEF VAR iH AS HANDLE.

    rH = root.
    DO WHILE rH <> ?:       
      /* Don't bother with these types, they have no children. */
      IF LOOKUP(rH:TYPE,"SLIDER,EDITOR,RADIO-SET,SELECTION-LIST,CONTROL-FRAME,IMAGE,RECTANGLE,TEXT,LITERAL,FILL-IN,COMBO-BOX,BUTTON,BROWSE,TOGGLE-BOX") = 0 
       THEN 
        DO:          
            IF rH:TYPE = "WINDOW" 
             THEN 
              DO:
                iH = rH:INSTANTIATING-PROCEDURE.
                IF VALID-HANDLE(iH) 
                 THEN 
                  DO:
                      IF pShow and INDEX(rH:TITLE, iH:FILE-NAME) = 0 
                       THEN assign
                              ppsChar = ppsChar + (if ppsChar = "" then "" else chr(10)) + ih:file-name
                              rH:TITLE = rH:TITLE + " (" + iH:FILE-NAME + ")". 
                      if not pShow and index(rh:title, ih:file-name) > 0 
                       then assign
                              rh:title = trim(replace(rh:title, "(" + ih:file-name + ")", "")).
                  END.
              END.

            cH = ?.
            cH = rH:FIRST-CHILD NO-ERROR. /* I may have missed a widget, or a new
                                             widget type could be invented. This should
                                             avoid problems down stream. */
            DO WHILE cH <> ?:               
             RUN HuntTree(cH, level + 1, pShow, input-output PPSChar).
             cH = cH:NEXT-SIBLING.  /* sibling attribute "almost" always there */      
            END.
        END. /* Traced widget. */    
      IF LOOKUP(rH:TYPE,"PSEUDO-WIDGET") = 0
       THEN rH = rH:NEXT-SIBLING.
       ELSE rH = ?.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsAboutOpen C-Win 
PROCEDURE IsAboutOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pOpen as logical init true.

{&window-name}:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openURL C-Win 
PROCEDURE openURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pURL as char.

 RUN ShellExecuteA in this-procedure (0,
                             "open",
                             pURL,
                             "",
                             "",
                             1,
                             OUTPUT std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION adjustWindowTitles C-Win 
FUNCTION adjustWindowTitles RETURNS LOGICAL PRIVATE
  ( pShowProgram as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 /* das: iterate through open windows and add/remove program-name(1) */
 def var level as int.
 def var ppsChar as char.

 ppsChar = "".
 level = 0.
 session:SET-WAIT-STATE ("General"). 
 RUN HuntTree in this-procedure (SESSION, 0, pShowProgram, input-output ppsChar).
 session:SET-WAIT-STATE (""). 
 
return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

