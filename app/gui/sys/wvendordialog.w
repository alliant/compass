&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wapvendor.w
   Window of all AP VENDORs
   3.3.2016
   */

CREATE WIDGET-POOL.

{tt/state.i}
{tt/vendor.i}
{lib/std-def.i}
{lib/count-rows.i}
define output parameter pVendorNbr as character no-undo.

/* {lib/winlaunch.i}  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwVendors

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES vendor

/* Definitions for BROWSE brwVendors                                    */
&Scoped-define FIELDS-IN-QUERY-brwVendors vendor.vendorID vendor.active vendor.name vendor.fulladdress vendor.acct   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwVendors   
&Scoped-define SELF-NAME brwVendors
&Scoped-define QUERY-STRING-brwVendors FOR EACH vendor by vendor.vendorID
&Scoped-define OPEN-QUERY-brwVendors OPEN QUERY {&SELF-NAME} FOR EACH vendor by vendor.vendorID.
&Scoped-define TABLES-IN-QUERY-brwVendors vendor
&Scoped-define FIRST-TABLE-IN-QUERY-brwVendors vendor


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwVendors}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport brwVendors chkActive cmbState ~
bRefresh bClear bFilter tSearch cmbDepartment RECT-37 
&Scoped-Define DISPLAYED-OBJECTS chkActive cmbState tSearch cmbDepartment 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE cmbDepartment AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dept" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "All","All"
     DROP-DOWN-LIST
     SIZE 29.6 BY 1 NO-UNDO.

DEFINE VARIABLE cmbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "All","All"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Vendor Name" 
     VIEW-AS FILL-IN 
     SIZE 24.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 152.4 BY 1.81.

DEFINE VARIABLE chkActive AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwVendors FOR 
      vendor SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwVendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwVendors C-Win _FREEFORM
  QUERY brwVendors DISPLAY
      vendor.vendorID column-label "Number" format "x(10)" width 15
      vendor.active column-label "Active" view-as toggle-box
      vendor.name column-label "Name" format "x(50)" width 50
      vendor.fulladdress column-label "Address" format "x(100)" width 70
      vendor.acct column-label "Account" format "x(20)" width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 168 BY 19.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bExport AT ROW 1.52 COL 9.2 WIDGET-ID 2 NO-TAB-STOP 
     brwVendors AT ROW 3.48 COL 2 WIDGET-ID 200
     chkActive AT ROW 2 COL 27.6 WIDGET-ID 72
     cmbState AT ROW 1.86 COL 38 COLON-ALIGNED WIDGET-ID 76
     bRefresh AT ROW 1.52 COL 2 WIDGET-ID 4 NO-TAB-STOP 
     bClear AT ROW 1.76 COL 158 WIDGET-ID 68
     bFilter AT ROW 1.76 COL 147 WIDGET-ID 64
     tSearch AT ROW 1.86 COL 119 COLON-ALIGNED WIDGET-ID 62
     cmbDepartment AT ROW 1.86 COL 72.2 COLON-ALIGNED WIDGET-ID 78
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.24 COL 19.8 WIDGET-ID 60
     "Active:" VIEW-AS TEXT
          SIZE 7 BY .95 AT ROW 1.91 COL 20.4 WIDGET-ID 74
     RECT-37 AT ROW 1.48 COL 17.8 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 170 BY 22.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Choose a Vendor"
         HEIGHT             = 22.62
         WIDTH              = 170
         MAX-HEIGHT         = 24.38
         MAX-WIDTH          = 206
         VIRTUAL-HEIGHT     = 24.38
         VIRTUAL-WIDTH      = 206
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         ALWAYS-ON-TOP      = yes
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwVendors 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwVendors:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwVendors:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwVendors
/* Query rebuild information for BROWSE brwVendors
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH vendor by vendor.vendorID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwVendors */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Choose a Vendor */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN APPLY "WINDOW-CLOSE":U TO C-Win.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Choose a Vendor */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Choose a Vendor */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  assign
    tSearch:screen-value = ""
    chkActive:screen-value = "YES"
    cmbState:screen-value = "ALL"
    cmbDepartment:screen-value = "ALL"
    .
  apply "CHOOSE":U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  dataSortBy = "".
  run sortData ("vendorID").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "LoadVendor".
  run GetData in this-procedure.
  apply "CHOOSE":U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwVendors
&Scoped-define SELF-NAME brwVendors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwVendors C-Win
ON DEFAULT-ACTION OF brwVendors IN FRAME fMain
DO:
  if available vendor
   then
    do:
      pVendorNbr = vendor.vendorID.
      apply "WINDOW-CLOSE":U to C-Win.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwVendors C-Win
ON ROW-DISPLAY OF brwVendors IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwVendors C-Win
ON START-SEARCH OF brwVendors IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME chkActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL chkActive C-Win
ON VALUE-CHANGED OF chkActive IN FRAME fMain
DO:
  apply "choose":U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbDepartment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbDepartment C-Win
ON VALUE-CHANGED OF cmbDepartment IN FRAME fMain /* Dept */
DO:
  apply "choose":U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbState C-Win
ON VALUE-CHANGED OF cmbState IN FRAME fMain /* State */
DO:
  apply "choose":U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSearch C-Win
ON RETURN OF tSearch IN FRAME fMain /* Vendor Name */
DO:
  apply "choose":U to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}
{lib/win-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").

/* load the data for the department filter */
run LoadDepartment in this-procedure.

/* load the data for the state filter */
{lib/get-state-list.i &combo=cmbState &addAll=true}

run windowResized in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  apply "CHOOSE":U to bClear in frame {&frame-name}.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY chkActive cmbState tSearch cmbDepartment 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bExport brwVendors chkActive cmbState bRefresh bClear bFilter tSearch 
         cmbDepartment RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if query brwVendors:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 &scoped-define ReportName "Vendors"

 {&window-name}:always-on-top = false.
 std-ch = "C".
 publish "GetExportType" (output std-ch).
 if std-ch = "X" 
  then run util/exporttoexcelbrowse.p (string(browse brwVendors:handle), {&ReportName}).
  else run util/exporttocsvbrowse.p (string(browse brwVendors:handle), {&ReportName}).
 {&window-name}:always-on-top = false.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData C-Win 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  empty temp-table vendor.
  publish "GetVendors" ("", output table vendor).
  if not can-find(first vendor)
   then publish "GetVendors" (output table vendor).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadDepartment C-Win 
PROCEDURE LoadDepartment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer vendor for vendor.

  run GetData in this-procedure.
  do with frame {&frame-name}:
    for each vendor no-lock 
       where vendor.dept > ""
       break by vendor.dept:
      if first-of(vendor.dept)
       then cmbDepartment:list-item-pairs = cmbDepartment:list-item-pairs + "," +
                                            vendor.deptname + 
                                            " [" + vendor.dept + "]" + "," + 
                                            vendor.dept.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as char no-undo.

  do with frame {&frame-name}:
    tWhereClause = "where " +
                   (if cmbState:screen-value <> "All" 
                   then "vendor.state = '" + cmbState:screen-value + "' "
                   else "true ") +
                   "and " + 
                   (if cmbDepartment:screen-value <> "All"
                   then "vendor.dept = '" + cmbDepartment:screen-value + "' "
                   else "true ") +
                   "and " +
                   (if tSearch:screen-value <> ""
                   then "vendor.name matches '*" + tSearch:screen-value + "*' "
                   else "true ") +
                   "and vendor.active = " + chkActive:screen-value + " ".
    {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

