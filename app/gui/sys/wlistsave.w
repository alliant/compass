&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
@name wlistsave.w
@description The screen is used to store all the previous saved lists for
             the user

@author John Oliver
@created 01/07/2016
----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

define input parameter pAppCalling as character no-undo.

{lib/std-def.i}
{lib/find-widget.i}
{lib/get-column.i}
{tt/list.i}
{tt/listentity.i}
{tt/listfield.i}
{tt/listfilter.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME frmList
&Scoped-define BROWSE-NAME brwList

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES list

/* Definitions for BROWSE brwList                                       */
&Scoped-define FIELDS-IN-QUERY-brwList list.displayName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwList   
&Scoped-define SELF-NAME brwList
&Scoped-define QUERY-STRING-brwList FOR EACH list by list.displayName
&Scoped-define OPEN-QUERY-brwList OPEN QUERY {&SELF-NAME} FOR EACH list by list.displayName.
&Scoped-define TABLES-IN-QUERY-brwList list
&Scoped-define FIRST-TABLE-IN-QUERY-brwList list


/* Definitions for FRAME frmList                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-frmList ~
    ~{&OPEN-QUERY-brwList}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bShare chkAuto cmbEntity brwList bDelete ~
bNew bModify 
&Scoped-Define DISPLAYED-OBJECTS chkAuto cmbEntity 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SensitizeButtons C-Win 
FUNCTION SensitizeButtons RETURNS LOGICAL
  ( input pEnable as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwList 
       MENU-ITEM m_View_Report  LABEL "View Quick List"
       RULE
       MENU-ITEM m_Open_Report  LABEL "Modify Quick List"
       MENU-ITEM m_Delete_Report LABEL "Delete Quick List"
       MENU-ITEM m_Share_Report LABEL "Share Quick List".


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected Quick List".

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected Quick List".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new Quick List".

DEFINE BUTTON bShare  NO-FOCUS
     LABEL "Share" 
     SIZE 7.2 BY 1.71 TOOLTIP "Share the selected Quick List with another user".

DEFINE VARIABLE cmbEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 37 BY 1 NO-UNDO.

DEFINE VARIABLE chkAuto AS LOGICAL INITIAL no 
     LABEL "Auto Run" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 TOOLTIP "Run the Quick List as soon as field(s) and filter(s) are loaded" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwList FOR 
      list SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwList C-Win _FREEFORM
  QUERY brwList DISPLAY
      list.displayName column-label "List Name" format "x(100)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS DROP-TARGET SIZE 44.4 BY 8.86
         TITLE "Quick Lists" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME frmList
     bShare AT ROW 1.14 COL 23 WIDGET-ID 116
     chkAuto AT ROW 1.57 COL 31.6 WIDGET-ID 120
     cmbEntity AT ROW 3.14 COL 6 COLON-ALIGNED WIDGET-ID 260
     brwList AT ROW 4.33 COL 1 WIDGET-ID 300
     bDelete AT ROW 1.14 COL 15.8 WIDGET-ID 46
     bNew AT ROW 1.14 COL 1.4 WIDGET-ID 44
     bModify AT ROW 1.14 COL 8.6 WIDGET-ID 52
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 44.4 BY 12.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Quick Lists"
         HEIGHT             = 12.19
         WIDTH              = 44.4
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME frmList
   FRAME-NAME                                                           */
/* BROWSE-TAB brwList cmbEntity frmList */
ASSIGN 
       bDelete:PRIVATE-DATA IN FRAME frmList     = 
                "Delete".

ASSIGN 
       bModify:PRIVATE-DATA IN FRAME frmList     = 
                "Email".

ASSIGN 
       bNew:PRIVATE-DATA IN FRAME frmList     = 
                "New".

ASSIGN 
       brwList:POPUP-MENU IN FRAME frmList             = MENU POPUP-MENU-brwList:HANDLE
       brwList:ALLOW-COLUMN-SEARCHING IN FRAME frmList = TRUE
       brwList:COLUMN-RESIZABLE IN FRAME frmList       = TRUE
       brwList:COLUMN-MOVABLE IN FRAME frmList         = TRUE.

ASSIGN 
       bShare:PRIVATE-DATA IN FRAME frmList     = 
                "Delete".

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwList
/* Query rebuild information for BROWSE brwList
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH list by list.displayName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwList */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Quick Lists */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Quick Lists */
DO:
  run WindowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME frmList /* Delete */
DO:
  run DeleteSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME frmList /* Modify */
DO:
  run OpenSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME frmList /* New */
DO:
  run NewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwList
&Scoped-define SELF-NAME brwList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON DEFAULT-ACTION OF brwList IN FRAME frmList /* Quick Lists */
DO:
  run ViewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON ROW-DISPLAY OF brwList IN FRAME frmList /* Quick Lists */
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON START-SEARCH OF brwList IN FRAME frmList /* Quick Lists */
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwList C-Win
ON VALUE-CHANGED OF brwList IN FRAME frmList /* Quick Lists */
DO:
  /* when the row is highlighted, display the name in the tooltip */
  if available list
   then
    /* determine if the column is big enough for the value */
    assign
      std-ha = getColumn(self:handle, "displayName")
      std-lo = (std-ha:width-chars < length(std-ha:screen-value))
      self:tooltip = (if std-lo then std-ha:screen-value else "")
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bShare
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bShare C-Win
ON CHOOSE OF bShare IN FRAME frmList /* Share */
DO:
  run ShareSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbEntity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbEntity C-Win
ON VALUE-CHANGED OF cmbEntity IN FRAME frmList /* Entity */
DO:
  run GetSavedLists in this-procedure (self:screen-value).
  apply "VALUE-CHANGED" to brwList.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Report C-Win
ON CHOOSE OF MENU-ITEM m_Delete_Report /* Delete Quick List */
DO:
  run DeleteSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open_Report C-Win
ON CHOOSE OF MENU-ITEM m_Open_Report /* Modify Quick List */
DO:
  run OpenSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Share_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Share_Report C-Win
ON CHOOSE OF MENU-ITEM m_Share_Report /* Share Quick List */
DO:
  run ShareSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Report C-Win
ON CHOOSE OF MENU-ITEM m_View_Report /* View Quick List */
DO:
  run ViewSavedList in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

std-ha = ?.
publish "GetListDataSrv" (output std-ha).
if not valid-handle(std-ha)
 then run sys/listdatasrv.p persistent.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  /* create the images for the buttons */
  do with frame {&frame-name}:
    bNew:load-image-up("images/new.bmp").
    bNew:load-image-insensitive("images/new-i.bmp").
    bModify:load-image-up("images/update.bmp").
    bModify:load-image-insensitive("images/update-i.bmp").
    bDelete:load-image-up("images/delete.bmp").
    bDelete:load-image-insensitive("images/delete-i.bmp").
    bShare:load-image-up("images/users.bmp").
    bShare:load-image-insensitive("images/users-i.bmp").
  end.
  RUN enable_UI.
  frame {&frame-name}:sensitive = false.
  run Initialize in this-procedure.
  frame {&frame-name}:sensitive = true.
      
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteSavedList C-Win 
PROCEDURE DeleteSavedList :
/*------------------------------------------------------------------------------
@description Deletes a previously saved list
------------------------------------------------------------------------------*/
  if available list
   then
    do:
      MESSAGE "Do you want to delete the report " + list.displayName + "?" 
        VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE std-lo AS LOGICAL.
      
      if not std-lo
       then return.
      
      publish "DeleteList" (list.displayName, output std-lo).
      
      /* delete the temp table entry */
      if std-lo
       then run GetSavedLists in this-procedure (cmbEntity:screen-value in frame {&frame-name}).
       else message "Could not delete the report." view-as alert-box error buttons ok.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY chkAuto cmbEntity 
      WITH FRAME frmList IN WINDOW C-Win.
  ENABLE bShare chkAuto cmbEntity brwList bDelete bNew bModify 
      WITH FRAME frmList IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frmList}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSavedLists C-Win 
PROCEDURE GetSavedLists :
/*------------------------------------------------------------------------------
@description Gets the user's saved lists
------------------------------------------------------------------------------*/
  define input parameter pEntity as character no-undo.
  
  publish "GetLists" (pEntity, output table list, output std-lo).
  if std-lo
   then {&OPEN-QUERY-brwList}
   else
    do:
      message "Failed to get the saved lists." view-as alert-box error buttons ok.
      SensitizeButtons(false).
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Initialize C-Win 
PROCEDURE Initialize :
/*------------------------------------------------------------------------------
@description Sets the window on the initial open
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    /* set the entity list */
    publish "GetEntities" (output table listentity,output std-lo).
    if std-lo
     then
      do:
        cmbEntity:delete(1).
        for each listentity no-lock:
          cmbEntity:add-last(listentity.displayName,listentity.entityName).
        end.
        if lookup(pAppCalling,cmbEntity:list-item-pairs) > 0
         then cmbEntity:screen-value = pAppCalling.
         else cmbEntity:screen-value = entry(2,cmbEntity:list-item-pairs).
        apply "VALUE-CHANGED":u to cmbEntity.
      end.
     else
      do:
        cmbEntity:delete(1).
        cmbEntity:add-last("ALL","A").
        cmbEntity:screen-value = "A".
      end.
  end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewSavedList C-Win 
PROCEDURE NewSavedList :
/*------------------------------------------------------------------------------
@description Create a new list
------------------------------------------------------------------------------*/
  /* create a new window to build the filters and fields */
  run sys/wlistbuild.w persistent (this-procedure, true).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenSavedList C-Win 
PROCEDURE OpenSavedList :
/*------------------------------------------------------------------------------
@description Opens a user's previously saved list      
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      run sys/wlistbuild.w persistent set std-ha (this-procedure, false).
      run BuildData in std-ha (list.displayName, list.reportName, logical(chkAuto:screen-value)).
    end.
   else MESSAGE "Please select a saved report" VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveSavedList C-Win 
PROCEDURE SaveSavedList :
/*------------------------------------------------------------------------------
@description Saves the list
------------------------------------------------------------------------------*/
  define input parameter pIsNew as logical no-undo.
  define input parameter pCurrentName as character no-undo.
  define input parameter pDisplayName as character no-undo.
  define input parameter table for listfield.
  define input parameter table for listfilter.
  
  if (pIsNew or pCurrentName <> pDisplayName) and can-find(first list where displayName = pDisplayName)
   then
    do:
      message "There is already a saved list named " + pDisplayName + ". Do you want to overwrite?" view-as alert-box question buttons yes-no update std-lo.
      if not std-lo
       then return.
    end.
  
  pIsNew = not can-find(first list where displayName = pDisplayName).
  publish "SaveList" (pIsNew,
                      pDisplayName,
                      table listfield,
                      table listfilter,
                      output std-lo).
                      
  if std-lo
   then message "Successfully saved the report" view-as alert-box information buttons ok.
   else message "Could not save the report" view-as alert-box error buttons ok.
          
  /* if we renamed the report, we need to delete the old report as saving will make a new one */
  if pCurrentName <> pDisplayName
   then publish "DeleteList" (pCurrentName, output std-lo).
   
  run GetSavedLists in this-procedure (cmbEntity:screen-value in frame {&frame-name}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShareSavedList C-Win 
PROCEDURE ShareSavedList :
/*------------------------------------------------------------------------------
@description Shares the selected list with other users  
------------------------------------------------------------------------------*/
  if available list
   then
    do:
      /* the output will be the user list */
      run sys/userselect.w ("", output std-ch).
      
      if std-ch > ""
       then
        do: 
          publish "ShareList" (list.displayName,
                               std-ch,
                               output std-ch).
      
          if std-ch > ""
           then message std-ch view-as alert-box error buttons ok.
           else message "Successfully shared the report" view-as alert-box information buttons ok.
        end.
    end.
   else message "Please select a saved report to share" view-as alert-box error buttons ok.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewSavedList C-Win 
PROCEDURE ViewSavedList :
/*------------------------------------------------------------------------------
@description View the selected list
------------------------------------------------------------------------------*/
  if available list
   then
    do with frame {&frame-name}:
      run sys/wlistresult.w persistent (list.displayName, list.reportName).
    end.
   else MESSAGE "Please select a saved report" VIEW-AS ALERT-BOX INFO BUTTONS OK.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized C-Win 
PROCEDURE WindowResized :
/*------------------------------------------------------------------------------
@description Executes when the window is resized, either bigger or smaller 
------------------------------------------------------------------------------*/
  frame frmList:width-pixels = {&window-name}:width-pixels.
  frame frmList:virtual-width-pixels = {&window-name}:width-pixels.
  frame frmList:height-pixels = {&window-name}:height-pixels.
  frame frmList:virtual-height-pixels = {&window-name}:height-pixels.

  /* browse components */
  std-ha = GetWidgetByName(frame frmList:handle,"brwList").
  if std-ha <> ?
   then
    assign
      std-ha:width-pixels = frame frmList:width-pixels
      std-ha:height-pixels = frame frmList:height-pixels - std-ha:y
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SensitizeButtons C-Win 
FUNCTION SensitizeButtons RETURNS LOGICAL
  ( input pEnable as logical ) :
/*------------------------------------------------------------------------------
@description Enables or disables the buttons
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    assign
      bNew:sensitive = pEnable
      bModify:sensitive = pEnable
      bDelete:sensitive = pEnable
      bShare:sensitive = pEnable
      chkAuto:sensitive = pEnable
      .
  end.

  
  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

