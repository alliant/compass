&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* compass.w
   display a startup COMPASS splash window
   D.Sinclair 1.1.2012
   
   The seemingly wierd presence of frame-A is due to how mksplash.p leaves a
   visible gap on the right-hand side of the borderless splash window.  In order
   to balance it, we created an empty (but color-filled) frame on the left
   so it seems to be on purpose.
 */

CREATE WIDGET-POOL.

def var tStartTime as datetime no-undo.
def var tCounter as int no-undo.

{lib/std-def.i}

tStartTime = now.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bLogo imgBkg 
&Scoped-Define DISPLAYED-OBJECTS tApplication tInitializing-2 tInitializing 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bLogo  NO-FOCUS FLAT-BUTTON
     LABEL "Logo" 
     SIZE 7.6 BY 1.81.

DEFINE VARIABLE tApplication AS CHARACTER FORMAT "X(256)":U INITIAL "Quality Assurance Management" 
      VIEW-AS TEXT 
     SIZE 37 BY .95
     FONT 6 NO-UNDO.

DEFINE VARIABLE tInitializing AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 25.6 BY .62
     FONT 6 NO-UNDO.

DEFINE VARIABLE tInitializing-2 AS CHARACTER FORMAT "X(256)":U INITIAL "Initializing" 
      VIEW-AS TEXT 
     SIZE 9.8 BY .62 NO-UNDO.

DEFINE IMAGE imgBkg
     FILENAME "adeicon/blank":U
     SIZE 52.4 BY 1.43.

DEFINE IMAGE imgLogo
     FILENAME "adeicon/blank":U
     SIZE 40 BY 2.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME FRAME-A
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         
         AT COL 1 ROW 1
         SIZE 3.8 BY 8.48 WIDGET-ID 200.

DEFINE FRAME fMain
     bLogo AT ROW 4.71 COL 7.4 WIDGET-ID 50 NO-TAB-STOP 
     tApplication AT ROW 5.14 COL 13.8 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     tInitializing-2 AT ROW 6.91 COL 6 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     tInitializing AT ROW 6.91 COL 15.6 COLON-ALIGNED NO-LABEL WIDGET-ID 58
     imgBkg AT ROW 7.95 COL 1 WIDGET-ID 54
     imgLogo AT ROW 1.43 COL 2 WIDGET-ID 56
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.8 ROW 1
         SIZE 52.6 BY 8.43
         BGCOLOR 15  WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Compass"
         HEIGHT             = 8.43
         WIDTH              = 53.4
         MAX-HEIGHT         = 12.76
         MAX-WIDTH          = 82.2
         VIRTUAL-HEIGHT     = 12.76
         VIRTUAL-WIDTH      = 82.2
         SMALL-TITLE        = yes
         SHOW-IN-TASKBAR    = no
         CONTROL-BOX        = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         ALWAYS-ON-TOP      = yes
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         MESSAGE-AREA       = no
         SENSITIVE          = no.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SENSITIVE        = FALSE.

/* SETTINGS FOR IMAGE imgLogo IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tApplication IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tInitializing IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tInitializing-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME FRAME-A
   UNDERLINE                                                            */
ASSIGN 
       FRAME FRAME-A:SENSITIVE        = FALSE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fMain
/* Query rebuild information for FRAME fMain
     _Query            is NOT OPENED
*/  /* FRAME fMain */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME FRAME-A
/* Query rebuild information for FRAME FRAME-A
     _Query            is NOT OPENED
*/  /* FRAME FRAME-A */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Compass */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Compass */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}
       .

/* Force frame/window realization to pass valid hwnd to Windows API */
if frame {&frame-name}:border-bottom > 0
 then .
run sys/MkSplash.p ({&WINDOW-NAME}:HWND, true).

subscribe to "SetSplashStatus" anywhere.
subscribe to "CloseSplash" anywhere.

imgLogo:load-image("images/ANcompass.jpg").
imgBkg:load-image("images/gradient.jpg").
publish "GetAppName" (output tApplication).
publish "GetLargeLogo" (output std-ch).
if search(std-ch) <> ? 
 then bLogo:load-image(std-ch).

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Dimensions need to be updated as a result of mksplash.p */
{&window-name}:height-pixels = imgBkg:y + imgBkg:height-pixels.

{&window-name}:hidden = false.
/* MESSAGE "9" skip                         */
/*         {&window-name}:width-pixels skip */
/*         imgBkg:width-pixels              */
/*         imgBkg:x                         */
/*  VIEW-AS ALERT-BOX INFO BUTTONS OK.      */
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseSplash C-Win 
PROCEDURE CloseSplash :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tElapsedTime as int no-undo.

 tElapsedTime = interval(now, tStartTime, "seconds").
 
 /* Increment to 2 (long enough to review splash screen) */
 do while tElapsedTime < 2:
  run SetSplashStatus in this-procedure.
  pause 1 no-message.
  tElapsedTime = tElapsedTime + 1.
 end.

 apply "Close" to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME FRAME-A IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-A}
  FRAME FRAME-A:SENSITIVE = NO.
  DISPLAY tApplication tInitializing-2 tInitializing 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bLogo imgBkg 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  FRAME fMain:SENSITIVE = NO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetSplashStatus C-Win 
PROCEDURE SetSplashStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 tCounter = tCounter + 1.
 if tCounter > 5 
  then tCounter = 0.
 tInitializing:screen-value in frame fMain = fill(" .", tCounter).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

