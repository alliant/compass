&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*
  sys/eventviewer.w
  server EVENT/calls Window for client-side debugging
  
  Modified :
  01/08/23     K.R     Task#: 102000 :Changed to support large response file (>16MB)    
 */

CREATE WIDGET-POOL.

{lib/std-def.i}
{lib/winlaunch.i}
{tt/process.i}

def var lResponseSelected as logical no-undo.
def var lIsResponseLarge  as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES processLog

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData processLog.logDate processLog.logProcess processLog.logDuration processLog.logStatus   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH processLog by processLog.logDate descending
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH processLog by processLog.logDate descending.
&Scoped-define TABLES-IN-QUERY-brwData processLog
&Scoped-define FIRST-TABLE-IN-QUERY-brwData processLog


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bViewHdr brwData tAuto tEntries bExport ~
bRefresh bViewCommand bViewConfig bViewLog bViewRequest bViewResponse ~
bViewStack bViewTrace bViewUrl tContents tLaunch 
&Scoped-Define DISPLAYED-OBJECTS tAuto tEntries tLabel tContents tLaunch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getResponseMimeType C-Win 
FUNCTION getResponseMimeType returns logical private
  ( output tMimeType as character ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export the list of application calls made to the server".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of application calls made to the server".

DEFINE BUTTON bViewCommand  NO-FOCUS
     LABEL "Cmd" 
     SIZE 10 BY 1.19 TOOLTIP "View the O/S level command".

DEFINE BUTTON bViewConfig  NO-FOCUS
     LABEL "Config" 
     SIZE 10 BY 1.19 TOOLTIP "View the configuration INI used to make the call to the server".

DEFINE BUTTON bViewHdr  NO-FOCUS
     LABEL "Headers" 
     SIZE 10 BY 1.19 TOOLTIP "View the server response headers".

DEFINE BUTTON bViewLog  NO-FOCUS
     LABEL "Debug" 
     SIZE 10 BY 1.19 TOOLTIP "View the call debugger output".

DEFINE BUTTON bViewRequest  NO-FOCUS
     LABEL "Request" 
     SIZE 10 BY 1.19 TOOLTIP "View the request payload sent to the server".

DEFINE BUTTON bViewResponse  NO-FOCUS
     LABEL "Response" 
     SIZE 10.6 BY 1.19 TOOLTIP "View the server response to the call".

DEFINE BUTTON bViewStack  NO-FOCUS
     LABEL "Stack" 
     SIZE 10 BY 1.19 TOOLTIP "View the internal calling stack".

DEFINE BUTTON bViewTrace  NO-FOCUS
     LABEL "Trace" 
     SIZE 10 BY 1.19 TOOLTIP "View the internal call trace".

DEFINE BUTTON bViewUrl  NO-FOCUS
     LABEL "Service" 
     SIZE 10 BY 1.19 TOOLTIP "View the call service address".

DEFINE VARIABLE tContents AS LONGCHAR 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL LARGE
     SIZE 90.4 BY 18.57 NO-UNDO.

DEFINE VARIABLE tEntries AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Entries" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 TOOLTIP "Enter the maximum number of requests to maintain" NO-UNDO.

DEFINE VARIABLE tLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Response" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tAuto AS LOGICAL INITIAL yes 
     LABEL "Auto-refresh" 
     VIEW-AS TOGGLE-BOX
     SIZE 16 BY .81 TOOLTIP "Select to refresh data when server requests are performed" NO-UNDO.

DEFINE VARIABLE tLaunch AS LOGICAL INITIAL no 
     LABEL "Startup" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.2 BY .81 TOOLTIP "Select to launch this window on application startup" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      processLog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      processLog.logDate label "Completed" 
 processLog.logProcess label "Request" format "x(20)"
 processLog.logDuration label "Duration" format "z,zzz,zz9" width 10
 processLog.logStatus label "STAT" format "S/F" width 5
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 70 BY 18.57 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bViewHdr AT ROW 1.24 COL 123.6 NO-TAB-STOP 
     brwData AT ROW 3.14 COL 2 WIDGET-ID 200
     tAuto AT ROW 1.38 COL 34 WIDGET-ID 130
     tEntries AT ROW 1.62 COL 23.6 COLON-ALIGNED WIDGET-ID 128 NO-TAB-STOP 
     bExport AT ROW 1.24 COL 9.4 WIDGET-ID 122 NO-TAB-STOP 
     bRefresh AT ROW 1.24 COL 2 WIDGET-ID 114 NO-TAB-STOP 
     bViewCommand AT ROW 1.24 COL 83 WIDGET-ID 108 NO-TAB-STOP 
     bViewConfig AT ROW 1.24 COL 73 WIDGET-ID 112 NO-TAB-STOP 
     bViewLog AT ROW 1.24 COL 133.6 WIDGET-ID 104 NO-TAB-STOP 
     bViewRequest AT ROW 1.24 COL 103 WIDGET-ID 94 NO-TAB-STOP 
     bViewResponse AT ROW 1.24 COL 113 WIDGET-ID 96 NO-TAB-STOP 
     bViewStack AT ROW 1.24 COL 153.6 WIDGET-ID 124 NO-TAB-STOP 
     bViewTrace AT ROW 1.24 COL 143.6 WIDGET-ID 92 NO-TAB-STOP 
     bViewUrl AT ROW 1.24 COL 93 WIDGET-ID 118 NO-TAB-STOP 
     tLabel AT ROW 2.43 COL 71 COLON-ALIGNED NO-LABEL WIDGET-ID 126
     tContents AT ROW 3.14 COL 73 NO-LABEL WIDGET-ID 110
     tLaunch AT ROW 2.1 COL 34 WIDGET-ID 132 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 163.4 BY 20.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Server Requests"
         HEIGHT             = 20.81
         WIDTH              = 163.4
         MAX-HEIGHT         = 22.91
         MAX-WIDTH          = 173.8
         VIRTUAL-HEIGHT     = 22.91
         VIRTUAL-WIDTH      = 173.8
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       tContents:RETURN-INSERTED IN FRAME fMain  = TRUE.

/* SETTINGS FOR FILL-IN tLabel IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH processLog by processLog.logDate descending.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Server Requests */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Server Requests */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Server Requests */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run ExportLog in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  apply "choose" to bViewResponse in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
   if tLabel:screen-value = "Stack"
    then run viewStack in this-procedure.
    else run viewfile in this-procedure (tLabel:screen-value) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewCommand
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewCommand C-Win
ON CHOOSE OF bViewCommand IN FRAME fMain /* Cmd */
DO:
  run viewfile in this-procedure ("Command").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewConfig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewConfig C-Win
ON CHOOSE OF bViewConfig IN FRAME fMain /* Config */
DO:
 run viewfile in this-procedure ("Configuration") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewHdr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewHdr C-Win
ON CHOOSE OF bViewHdr IN FRAME fMain /* Headers */
DO:
 run viewfile in this-procedure ("Headers") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewLog C-Win
ON CHOOSE OF bViewLog IN FRAME fMain /* Debug */
DO:
 run viewfile in this-procedure ("Debugger") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewRequest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewRequest C-Win
ON CHOOSE OF bViewRequest IN FRAME fMain /* Request */
DO:
 run viewfile in this-procedure ("Request") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewResponse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewResponse C-Win
ON CHOOSE OF bViewResponse IN FRAME fMain /* Response */
DO:
 run viewfile in this-procedure ("Response").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewStack
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewStack C-Win
ON CHOOSE OF bViewStack IN FRAME fMain /* Stack */
DO:
  run viewstack in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewTrace
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewTrace C-Win
ON CHOOSE OF bViewTrace IN FRAME fMain /* Trace */
DO:
  run viewfile in this-procedure ("Trace").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewUrl C-Win
ON CHOOSE OF bViewUrl IN FRAME fMain /* Service */
DO:
 run viewfile in this-procedure ("Service") .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuto C-Win
ON VALUE-CHANGED OF tAuto IN FRAME fMain /* Auto-refresh */
DO:
 publish "SetAppRequestRefresh" (self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




&Scoped-define SELF-NAME tContents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContents C-Win
ON MOUSE-SELECT-DBLCLICK OF tContents IN FRAME fMain
DO:
  if lResponseSelected and lIsResponseLarge then
    run openresponsefile. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&Scoped-define SELF-NAME tEntries
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEntries C-Win
ON LEAVE OF tEntries IN FRAME fMain /* Entries */
DO:
 publish "SetAppRequestLimit" (self:input-value).
 apply "CHOOSE" to bRefresh.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLaunch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLaunch C-Win
ON VALUE-CHANGED OF tLaunch IN FRAME fMain /* Startup */
DO:
 publish "SetAppRequestLaunch" (self:checked).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

publish "GetAppRequestLimit" (output tEntries).
publish "GetAppRequestRefresh" (output tAuto).
publish "GetAppRequestLaunch" (output tLaunch).

subscribe to "ProcessLog" anywhere.
subscribe to "IsEventViewerOpen" anywhere.
subscribe to "ProcessLogDataChanged" anywhere.

bRefresh:load-image("images/refresh.bmp").
bRefresh:load-image-insensitive("images/refresh-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

/* run ProcessLog in this-procedure ("", "", 0, false).  */

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  run getData.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAuto tEntries tLabel tContents tLaunch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bViewHdr brwData tAuto tEntries bExport bRefresh bViewCommand 
         bViewConfig bViewLog bViewRequest bViewResponse bViewStack bViewTrace 
         bViewUrl tContents tLaunch 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportLog C-Win 
PROCEDURE ExportLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 th = temp-table processLog:handle.

 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 std-ch = "".
 publish "GetReportDir" (output std-ch).

 run util/exporttable.p (table-handle th,
                         "processLog",
                         "for each processLog by processLog.logDate descending ",
                         "logDate,logProcess,logType,logDuration,logStatus,logMsg",
                         "Date,Action,Type,Duration,Success,Message",
                         std-ch,
                         "ServerRequestsAsOf" 
                           + string(year(today))
                           + string(month(today))
                           + string(day(today))
                           + "-" + replace(string(time, "HH:MM:SS"), ":", "")
                           + ".csv",
                         true,
                         output std-ch,
                         output std-in).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 close query brwData.
 empty temp-table processLog.

 publish "GetProcessLogs" (output table processlog).

 open query brwData for each processLog by processLog.logDate descending.
 apply "DEFAULT-ACTION" to brwData in frame fMain.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsEventViewerOpen C-Win 
PROCEDURE IsEventViewerOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pOpen as logical init true.

this-procedure:current-window:move-to-top().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openresponsefile C-Win 
PROCEDURE openresponsefile PRIVATE :
/*------------------------------------------------------------------------------
 Purpose:
 Notes:
------------------------------------------------------------------------------*/
 define variable tMimeType       as character no-undo.
 define variable tFile           as character no-undo.
 define variable tFileFullPath   as character no-undo.
 define variable tFileExtn       as character no-undo.
 define variable tCopyFileName   as character no-undo.
 
 std-lo = getResponseMimeType(output tMimeType).
 
 if not std-lo then
   return. 
 
 tFile = processLog.logProcess + "2".
 publish "GetTempFile" (tFile, output tFileFullPath).
 if tFileFullPath = ? or tFileFullPath = ""
 then 
  do: tContents:screen-value in frame fMain = "No content for " + processLog.logProcess.
      return.
  end.

  if tMimeType = "text/plain" then
    tFileExtn = ".txt".
  else
    tFileExtn = "." + substring(tMimeType,r-index(tMimeType,"/") + 1).
    
  tCopyFileName = tFileFullPath + tFileExtn.
  os-copy value(tFileFullPath) value(tCopyFileName).
  
  publish "AddTempFile" ("copiedFile", search(tCopyFileName)). 
  
  run ShellExecuteA in this-procedure (0,
                                       "open",
                                       tCopyFileName,
                                       "",
                                       "",
                                       1,
                                       OUTPUT std-in).
  

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ProcessLogDataChanged C-Win 
PROCEDURE ProcessLogDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     Refresh if any process is called while the window is open
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 publish "GetAppRequestRefresh" (output std-lo).
 if std-lo = no then
 return.

 run getData in this-procedure.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewFile C-Win 
PROCEDURE viewFile PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pType as char.

def var tFile as char no-undo.
def var tFileFullPath as char no-undo.
def var tUser as char no-undo.
def var tPw as char no-undo.

 assign
   lResponseSelected = false
   lIsResponseLarge  = false
   .
   
 tLabel:screen-value in frame {&frame-name} = pType.

 if not available processLog 
  then 
   do: tContents:screen-value in frame fMain = "No request".
       return.
   end.

 case pType:
  when "command" then tFile = processLog.logProcess + "1".
  when "response"
   then 
    do:
      tFile = processLog.logProcess + "2".
      lResponseSelected = true.
    end.
  when "debugger" 
   then
    do:
      if processlog.logType = "HTTP/GET"
       then tFile = processLog.logProcess + "8".
      else 
       tFile = processLog.logProcess + "3".
    end. 
  when "configuration" then tFile = processLog.logProcess + "4".
  when "service" then tFile = processLog.logProcess + "5".
  when "trace" then tFile = processLog.logProcess + "6".
  when "request" then tFile = processLog.logProcess + "7".
  when "headers" then tFile = processLog.logProcess +  "8".
 end case.

 publish "GetTempFile" (tFile, output tFileFullPath).

 if tFileFullPath = ? or tFileFullPath = ""
  then 
   do: tContents:screen-value in frame fMain = "No content for " + processLog.logProcess.
       return.
   end.

 /* To obfuscate them... 
    There is a bug in that this returns the "current" values and not
    the values as output in the file.  There is no good workaround. 
  */
 publish "GetCredentials" (output tUser, output tPw).

 file-info:file-name = tFileFullPath.
 /* read-file on editor fails for file size > 16MB (16777216B) */
 if file-info:file-size <= 16777216 or file-info:file-size = ? then
   tContents:read-file(tFileFullPath).
 else if lResponseSelected then
 do:
   tContents:screen-value in frame fMain = "File is too large to display. Double click here to open the file ".
   lIsResponseLarge = true.  
 end.
      
 tContents:replace("I1=" + tUser, "I1=[--]").
 tContents:replace("I2=" + tPw, "I2=[--]").
 tContents:replace("username: " + tUser, "username: [--]").
 tContents:replace("password: " + tPw, "password: [--]").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewStack C-Win 
PROCEDURE viewStack :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 tLabel:screen-value in frame {&frame-name} = "Stack".

 if not available processLog 
  then 
   do: 
       tContents:screen-value in frame fMain = "No request".
       return.
   end.

 tContents:screen-value in frame fMain = replace(processLog.logTrace, "|", chr(13)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 tContents:width-pixels = frame fmain:width-pixels - 365.
 tContents:height-pixels = frame fMain:height-pixels - 47.
 brwData:height-pixels = frame fMain:height-pixels - 47.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getResponseMimeType C-Win 
FUNCTION getResponseMimeType returns logical private
  ( output tMimeType as character ):
/*------------------------------------------------------------------------------
 Purpose:
 Notes:
------------------------------------------------------------------------------*/
 define variable tHeaderFileName     as character no-undo.
 define variable tHeaderFileFullPath as character no-undo.
 define variable tFileLine           as character no-undo.
 define variable tSuccess            as logical no-undo init false.
 
 tHeaderFileName = processLog.logProcess +  "8".
 publish "GetTempFile" (tHeaderFileName, output tHeaderFileFullPath).

  if tHeaderFileFullPath = ? or tHeaderFileFullPath = "" or search(tHeaderFileFullPath) = ?
   then 
     return tSuccess.
  
  input from value (tHeaderFileFullPath). 
  repeat:
    import unformatted tFileLine.
    if lookup("Content-Type:",tFileLine," ") > 0 
     then
      do:
        tMimeType = trim(substring(tFileLine,index(tFileLine,":") + 1)).
        tSuccess = true.
        leave.
      end.
  end.
        
  input close.
  return tSuccess.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

