&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME f
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS f 
/*********************************************************************
* Copyright (C) 2005 by Progress Software Corporation. All rights    *
* reserved.  Prior versions of this work may contain portions        *
* contributed by participants of Possenet.                           *
*                                                                    *
*********************************************************************/
/*------------------------------------------------------------------------

  File: _session.w

  Description: List connected databases (and let the user add/remove some).

    Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gerry Seidl

  Created: 08/09/93 -  7:37 am
  
  Modified on 11/06/96 gfs - Added adjustment to dialog for win95 font
              08/01/97 gfs - Updated attribute list
              01/23/98 gfs - Updated attribute list for v9
              12/07/98 gfs - Additional attr update for v9
              05/25/99 tsm - Added NUMERIC-DECIMAL-POINT and 
                             NUMERIC-SEPARATOR for v9.1
              06/11/99 tsm - Added CONTEXT-HELP-FILE for v9.1
              09/24/99 gfs - Added some other missing attrs.           
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/
/* { adecomm/commeng.i}   /* Help contexts */                        */
/* { adecomm/adestds.i}   /* Standard layout defines, colors etc. */ */
/* { protools/ptlshlp.i } /* help definitions */                     */
/* { adecomm/_adetool.i }                                            */

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE NEW SHARED STREAM rpt.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE DIALOG-BOX
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS attr-list 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE VARIABLE attr-list AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT 
     SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL 
     SIZE 54 BY 15.29
     FONT 2 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f
     attr-list AT ROW 1.57 COL 3 NO-LABEL
     SPACE(1.70) SKIP(0.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS THREE-D  SCROLLABLE 
         TITLE "Compass :|: Session Attributes":L.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: DIALOG-BOX
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX f
   FRAME-NAME UNDERLINE                                                 */
ASSIGN 
       FRAME f:SCROLLABLE       = FALSE.

/* SETTINGS FOR SELECTION-LIST attr-list IN FRAME f
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK f 


/* ***************************  Main Block  *************************** */

/* Add Trigger to equate WINDOW-CLOSE to END-ERROR                      */
ON WINDOW-CLOSE OF FRAME {&FRAME-NAME} APPLY "END-ERROR":U TO SELF.

/* Set Delimiter for the selection list. We cannot use ',' since
 * several numeric parameters are decimals and would mess up the
 * entries when using European format
 */
ASSIGN attr-list:DELIMITER = ";".

/* Get session Attributes. */
RUN get_attributes.

/* This adjustment is for when we are running with MS Sans Serif 8
 * as the default font
 */
IF SESSION:PIXELS-PER-COLUMN = 5 THEN
  ASSIGN FRAME {&FRAME-NAME}:WIDTH = FRAME {&FRAME-NAME}:WIDTH + 13
         attr-list:WIDTH           = attr-list:WIDTH + 14
 .
  
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI f  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI f  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE attr-list 
      WITH FRAME f.
  {&OPEN-BROWSERS-IN-QUERY-f}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE get_attributes f 
PROCEDURE get_attributes :
/* -----------------------------------------------------------
  Purpose:    Populate a list with the session attributes
              in the form "Attribute = value"     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE VARIABLE fproc  AS HANDLE NO-UNDO.
  DEFINE VARIABLE lproc  AS HANDLE NO-UNDO.
  DEFINE VARIABLE fchild AS HANDLE NO-UNDO.
  DEFINE VARIABLE lchild AS HANDLE NO-UNDO.
  
  ASSIGN fproc  = SESSION:FIRST-PROCEDURE
         lproc  = SESSION:LAST-PROCEDURE
         fchild = SESSION:FIRST-CHILD
         lchild = SESSION:LAST-CHILD.
           
  attr-list:LIST-ITEMS IN FRAME {&FRAME-NAME} =
    "APPL-ALERT-BOXES       = " + STRING(SESSION:APPL-ALERT-BOXES) + 
   ";BATCH-MODE             = " + STRING(SESSION:BATCH-MODE) + 
   ";CHARSET                = " + SESSION:CHARSET + 
   ";CONTEXT-HELP-FILE      = " + (IF SESSION:CONTEXT-HELP-FILE NE ? THEN SESSION:CONTEXT-HELP-FILE ELSE "?") +
   ";CPCASE                 = " + (IF SESSION:CPCASE NE ? THEN SESSION:CPCASE ELSE "?") + 
   ";CPCOLL                 = " + (IF SESSION:CPCOLL NE ? THEN SESSION:CPCOLL ELSE "?") +
   ";CPINTERNAL             = " + (IF SESSION:CPINTERNAL NE ? THEN SESSION:CPINTERNAL ELSE "?") + 
   ";CPLOG                  = " + (IF SESSION:CPLOG NE ? THEN SESSION:CPLOG ELSE "?") +
   ";CPPRINT                = " + (IF SESSION:CPPRINT NE ? THEN SESSION:CPPRINT ELSE "?") +
   ";CPRCODEIN              = " + (IF SESSION:CPRCODEIN NE ? THEN SESSION:CPRCODEIN ELSE "?") +
   ";CPRCODEOUT             = " + (IF SESSION:CPRCODEOUT NE ? THEN SESSION:CPRCODEOUT ELSE "?") + 
   ";CPSTREAM               = " + (IF SESSION:CPSTREAM NE ? THEN SESSION:CPSTREAM ELSE "?") +
   ";CPTERM                 = " + (IF SESSION:CPTERM NE ? THEN SESSION:CPTERM ELSE "?") +   
   ";DATA-ENTRY-RETURN      = " + STRING(SESSION:DATA-ENTRY-RETURN) + 
   ";DATE-FORMAT            = " + STRING(SESSION:DATE-FORMAT) + 
   ";DEBUG-ALERT            = " + (IF SESSION:DEBUG-ALERT THEN "TRUE" ELSE "FALSE") + 
   ";DISPLAY-TYPE           = " + SESSION:DISPLAY-TYPE  + 
   ";FIRST-ASYNC-REQUEST    = " + (IF SESSION:FIRST-ASYNC-REQUEST NE ? THEN STRING(SESSION:FIRST-ASYNC-REQUEST) ELSE "?") +
   ";FIRST-CHILD            = " + STRING(SESSION:FIRST-CHILD) + (IF CAN-QUERY(fchild,"TYPE") THEN " [" + fchild:TYPE + "]" ELSE "") +
   ";FIRST-PROCEDURE        = " + (IF VALID-HANDLE(fproc) THEN fproc:FILE-NAME ELSE "<none>") +
   ";FIRST-SERVER           = " + (IF SESSION:FIRST-SERVER NE ? THEN STRING(SESSION:FIRST-SERVER) ELSE "?") +
   ";FIRST-SOCKET           = " + (IF SESSION:FIRST-SOCKET NE ? THEN STRING(SESSION:FIRST-SOCKET) ELSE "?") +
   ";FRAME-SPACING          = " + STRING(SESSION:FRAME-SPACING) + 
   ";GET-PRINTERS()         = " + (IF SESSION:GET-PRINTERS() NE ? THEN STRING(SESSION:GET-PRINTERS()) ELSE "<none>") + 
   ";HEIGHT-CHARS           = " + STRING(SESSION:HEIGHT-CHARS) + 
   ";HEIGHT-PIXELS          = " + STRING(SESSION:HEIGHT-PIXELS) + 
   ";IMMEDIATE-DISPLAY      = " + STRING(SESSION:IMMEDIATE-DISPLAY) + 
   ";LAST-ASYNC-REQUEST     = " + (IF SESSION:LAST-ASYNC-REQUEST NE ? THEN STRING(SESSION:LAST-ASYNC-REQUEST) ELSE "?") +
   ";LAST-CHILD             = " + STRING(SESSION:LAST-CHILD) + (IF CAN-QUERY(lchild,"TYPE") THEN " [" + lchild:TYPE + "]" ELSE "") +
   ";LAST-PROCEDURE         = " + (IF VALID-HANDLE(lproc) THEN lproc:FILE-NAME ELSE "<none>") +
   ";LAST-SERVER            = " + (IF SESSION:LAST-SERVER NE ? THEN STRING(SESSION:LAST-SERVER) ELSE "?") +
   ";LAST-SOCKET            = " + (IF SESSION:LAST-SOCKET NE ? THEN STRING(SESSION:LAST-SOCKET) ELSE "?") +
   ";MULTITASKING-INTERVAL  = " + STRING(SESSION:MULTITASKING-INTERVAL) +
   ";NUMERIC-DECIMAL-POINT  = " + SESSION:NUMERIC-DECIMAL-POINT +
   ";NUMERIC-FORMAT         = " + SESSION:NUMERIC-FORMAT +
   ";NUMERIC-SEPARATOR      = " + SESSION:NUMERIC-SEPARATOR +
   ";OLE-INVOKE-LOCALE      = " + (IF SESSION:OLE-INVOKE-LOCALE NE ? THEN STRING(SESSION:OLE-INVOKE-LOCALE) ELSE "?") +
   ";OLE-NAMES-LOCALE       = " + (IF SESSION:OLE-NAMES-LOCALE NE ? THEN STRING(SESSION:OLE-NAMES-LOCALE) ELSE "?") +
   ";PARAMETER              = " + SESSION:PARAMETER +
   ";PIXELS-PER-COLUMN      = " + STRING(SESSION:PIXELS-PER-COLUMN) + 
   ";PIXELS-PER-ROW         = " + STRING(SESSION:PIXELS-PER-ROW)
   NO-ERROR.

attr-list:LIST-ITEMS IN FRAME {&FRAME-NAME} = attr-list:LIST-ITEMS IN FRAME {&FRAME-NAME} +
   ";PRINTER-CONTROL-HANDLE = " + STRING(SESSION:PRINTER-CONTROL-HANDLE) +
   ";PRINTER-HDC            = " + (IF SESSION:PRINTER-HDC = ? THEN "?" ELSE STRING(SESSION:PRINTER-HDC)) +
   ";PRINTER-NAME           = " + (IF SESSION:PRINTER-NAME NE ? THEN SESSION:PRINTER-NAME ELSE "?") +
   ";PRINTER-PORT           = " + (IF SESSION:PRINTER-PORT NE ? THEN SESSION:PRINTER-PORT ELSE "?") + 
   ";REMOTE                 = " + (IF SESSION:REMOTE THEN "TRUE" ELSE "FALSE") +
   ";STREAM                 = " + SESSION:STREAM + 
   ";SUPER-PROCEDURES       = " + (IF SESSION:SUPER-PROCEDURES = "" THEN "<none>" ELSE SESSION:SUPER-PROCEDURES) +
   ";SUPPRESS-WARNINGS      = " + STRING(SESSION:SUPPRESS-WARNINGS) + 
   ";SYSTEM-ALERT-BOXES     = " + STRING(SESSION:SYSTEM-ALERT-BOXES) + 
   ";TEMP-DIRECTORY         = " + SESSION:TEMP-DIRECTORY + 
   ";THREE-D                = " + STRING(SESSION:THREE-D) +
   ";TIME-SOURCE            = " + SESSION:TIME-SOURCE + 
   ";TOOLTIPS               = " + (IF SESSION:TOOLTIPS THEN "TRUE" ELSE "FALSE") +
   ";TYPE                   = " + SESSION:TYPE +
   ";V6DISPLAY              = " + STRING(SESSION:V6DISPLAY) + 
   ";WAIT-STATE             = " + (IF SESSION:GET-WAIT-STATE() = "" THEN "<none>" ELSE SESSION:GET-WAIT-STATE()) +
   ";WIDTH-CHARS            = " + STRING(SESSION:WIDTH-CHARS) + 
   ";WIDTH-PIXELS           = " + STRING(SESSION:WIDTH-PIXELS) + 
   ";WINDOW-SYSTEM          = " + SESSION:WINDOW-SYSTEM +
   ";WORK-AREA-HEIGHT-PIXELS= " + STRING(SESSION:WORK-AREA-HEIGHT-PIXELS) +
   ";WORK-AREA-WIDTH-PIXELS = " + STRING(SESSION:WORK-AREA-WIDTH-PIXELS) +
   ";WORK-AREA-X            = " + STRING(SESSION:WORK-AREA-X) +
   ";WORK-AREA-Y            = " + STRING(SESSION:WORK-AREA-Y) +
   ";YEAR-OFFSET            = " + STRING(SESSION:YEAR-OFFSET)
   NO-ERROR.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

