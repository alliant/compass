&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* authpwdialog.w
   present a DIALOG to change the user's PassWord
   D.Sinclair
   4.8.2013
   
   09.04.2015 jpo : Added a read-only fill-in to display either how many
                    days the user has before the password expires or that
                    the password has expired. The fill-in is hidden when
                    the window is open normally. The message provided 
                    is an input parameter.
   09.11.2015 jpo : Added new password criterion of at least one capital letter
 */

{lib/std-def.i}

def input parameter pMsg as char no-undo.
def var tCurrentMatch as logical no-undo init false.
def var tNewGood as logical no-undo init false.
def var tNewMatch as logical no-undo init false.
def var pos as integer no-undo.

{lib/text-align.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tOldPassword tNewPassword1 tNewPassword2 ~
bCancel bOk 
&Scoped-Define DISPLAYED-OBJECTS tOldPassword 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateCurrentPassword Dialog-Frame 
FUNCTION validateCurrentPassword RETURNS LOGICAL PRIVATE
  ( tPw as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateNewPassword Dialog-Frame 
FUNCTION validateNewPassword RETURNS LOGICAL PRIVATE
  ( tPw as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bOk 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tNewPassword1 AS CHARACTER FORMAT "X(20)":U 
     LABEL "New" 
     VIEW-AS FILL-IN 
     SIZE 31.8 BY 1 TOOLTIP "Enter your new password" NO-UNDO.

DEFINE VARIABLE tNewPassword2 AS CHARACTER FORMAT "X(20)":U 
     LABEL "Verify" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 TOOLTIP "Re-enter your new password" NO-UNDO.

DEFINE VARIABLE tOldPassword AS CHARACTER FORMAT "X(20)":U 
     LABEL "Current" 
     VIEW-AS FILL-IN 
     SIZE 31.8 BY 1 TOOLTIP "Enter your current password" NO-UNDO.

DEFINE VARIABLE txtPassword AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 68 BY 1 TOOLTIP "Please change your password"
     BGCOLOR 12 FGCOLOR 14  NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 68 BY 5.48.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     txtPassword AT ROW 1.24 COL 2 NO-LABEL WIDGET-ID 32
     tOldPassword AT ROW 2.67 COL 18 COLON-ALIGNED WIDGET-ID 2 PASSWORD-FIELD 
     tNewPassword1 AT ROW 4.29 COL 18 COLON-ALIGNED WIDGET-ID 4 PASSWORD-FIELD 
     tNewPassword2 AT ROW 5.43 COL 18 COLON-ALIGNED WIDGET-ID 6 PASSWORD-FIELD 
     bCancel AT ROW 13.14 COL 20
     bOk AT ROW 13.14 COL 36
     "- Must contain a capital letter" VIEW-AS TEXT
          SIZE 35.2 BY .62 AT ROW 7.91 COL 2.8 WIDGET-ID 34
     "Password Restrictions" VIEW-AS TEXT
          SIZE 26 BY .62 AT ROW 6.95 COL 3 WIDGET-ID 12
          FONT 6
     "- Must contain at least one special character" VIEW-AS TEXT
          SIZE 44.2 BY .62 AT ROW 10.81 COL 2.8 WIDGET-ID 18
     "- Must be between 8 and 20 characters in length" VIEW-AS TEXT
          SIZE 48.2 BY .62 AT ROW 9.33 COL 2.8 WIDGET-ID 14
     "- Must be different from your last four passwords" VIEW-AS TEXT
          SIZE 47.2 BY .62 AT ROW 10.05 COL 2.8 WIDGET-ID 22
     "- Must contain at least one number" VIEW-AS TEXT
          SIZE 45 BY .62 AT ROW 8.62 COL 2.8 WIDGET-ID 16
     "! @ $ % # & ^ * ( ) _ + - =" VIEW-AS TEXT
          SIZE 24 BY .62 AT ROW 11.57 COL 6 WIDGET-ID 36
     RECT-6 AT ROW 7.19 COL 2 WIDGET-ID 20
     SPACE(0.99) SKIP(2.03)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Set Password"
         DEFAULT-BUTTON bOk CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNewPassword1 IN FRAME Dialog-Frame
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN tNewPassword2 IN FRAME Dialog-Frame
   NO-DISPLAY                                                           */
/* SETTINGS FOR FILL-IN txtPassword IN FRAME Dialog-Frame
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
ASSIGN 
       txtPassword:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX Dialog-Frame
/* Query rebuild information for DIALOG-BOX Dialog-Frame
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX Dialog-Frame */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Set Password */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOk
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOk Dialog-Frame
ON CHOOSE OF bOk IN FRAME Dialog-Frame /* Save */
DO:    
  if not tNewGood or not tCurrentMatch or not tNewMatch
   then
    do:
        MESSAGE "Please correct the error"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        return.
    end.

  run server/setmypassword.p (input tNewPassword1:screen-value in frame {&frame-name},
                              output std-lo,
                              output std-ch).
  
  if not std-lo 
   then
    do:
        MESSAGE std-ch VIEW-AS ALERT-BOX warning BUTTONS OK.
        tNewPassword1:screen-value = "".
        tNewPassword2:screen-value = "".
        apply "ENTRY" to tNewPassword1.
        return.
    end.
   else
    do:
      publish "SetCredentialsPassword" (tNewPassword1:screen-value in frame {&frame-name}).
      publish "SaveCredentials".
      apply "WINDOW-CLOSE" to frame {&frame-name}.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNewPassword1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNewPassword1 Dialog-Frame
ON LEAVE OF tNewPassword1 IN FRAME Dialog-Frame /* New */
DO:
  std-lo = validateNewPassword(self:screen-value).
  if not std-lo
   then 
    do:
      assign
        tNewGood = false
        self:bgcolor = 14
        self:tooltip = "New password does not match the password requirements"
        txtPassword:hidden = false
        txtPassword:screen-value = self:tooltip
        .
      centerText(txtPassword:handle).
    end.
   else
    assign
      tNewGood = true
      self:bgcolor = ?
      self:tooltip = "Good"
      txtPassword:hidden = true
      txtPassword:screen-value = self:tooltip
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNewPassword2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNewPassword2 Dialog-Frame
ON LEAVE OF tNewPassword2 IN FRAME Dialog-Frame /* Verify */
DO:
  if self:screen-value <> tNewPassword1:screen-value in frame {&frame-name}
   then 
    do:
      assign
        tNewMatch = false
        self:bgcolor = 14
        self:tooltip = "The new password and verify password do not match"
        txtPassword:hidden = false
        txtPassword:screen-value = self:tooltip
        .
      centerText(txtPassword:handle).
    end.
   else
    assign
      tNewMatch = true
      self:bgcolor = ?
      self:tooltip = "Good"
      txtPassword:hidden = true
      txtPassword:screen-value = self:tooltip
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOldPassword
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOldPassword Dialog-Frame
ON LEAVE OF tOldPassword IN FRAME Dialog-Frame /* Current */
DO:
  std-lo = validateCurrentPassword(self:screen-value).
  if not std-lo
   then
    do:
      assign
        tCurrentMatch = false
        self:bgcolor = 14
        self:tooltip = "Your current password is not correct"
        txtPassword:hidden = false
        txtPassword:screen-value = self:tooltip
        txtPassword:read-only = true
        .
      centerText(txtPassword:handle).
    end.
   else
    assign
      tCurrentMatch = true
      self:bgcolor = ?
      self:tooltip = "Good"
      txtPassword:hidden = true
      txtPassword:screen-value = self:tooltip
      txtPassword:read-only = true
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* centers the window */
{lib/win-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* center and set the text for the password change box */
/* if the password is not being changed because the    */
/* password is expired or almost expiring, hide the box */

if length(pMsg) > 0
 then
  do:
        txtPassword:hidden = false.
        txtPassword:screen-value = pMsg.
        txtPassword:read-only = true.
        centerText(txtPassword:handle).
  end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&frame-name}.
  
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tOldPassword 
      WITH FRAME Dialog-Frame.
  ENABLE tOldPassword tNewPassword1 tNewPassword2 bCancel bOk 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateCurrentPassword Dialog-Frame 
FUNCTION validateCurrentPassword RETURNS LOGICAL PRIVATE
  ( tPw as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetCredentialsPassword" (output std-ch).

  return tPw = std-ch.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateNewPassword Dialog-Frame 
FUNCTION validateNewPassword RETURNS LOGICAL PRIVATE
  ( tPw as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var hasNumber as logical init false.
 def var hasSpecial as logical init false.
 def var isDifferent as logical init false.
 def var hasCapital as logical init false.

 if length(tPw) < 8 or length(tPw) > 20 
  then return false.
  
 if index(tPw, ":") > 0
  then return false.

 do std-in = 1 to length(tPw):
  std-ch = substring(tPw, std-in, 1).
  if index("0123456789", std-ch) > 0 
   then hasNumber = true.
  if index("!@$%#&^*()_+-=", std-ch) > 0 
   then hasSpecial = true.
  /* check for capital letter */
  if asc(upper(std-ch)) >= 65 and asc(upper(std-ch)) <= 90 and asc(std-ch) = asc(upper(std-ch))
   then hasCapital = true.
 end.

 isDifferent = tOldPassword:screen-value in frame {&frame-name} <> tPw.

 return (hasNumber and hasSpecial and isDifferent and hasCapital).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

