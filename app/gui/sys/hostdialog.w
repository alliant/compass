&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* sys/hostdialog.w
   Temporarily set the URLs to the compass servers during the current session
   @author D.Sinclair
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tHost tEsbHost Btn_Cancel Btn_OK 
&Scoped-Define DISPLAYED-OBJECTS tHost tEsbHost 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "These server addresses are only used for the current session / application"
     BGCOLOR 8 .

DEFINE VARIABLE tEsbHost AS CHARACTER FORMAT "X(256)":U 
     LABEL "Message Queue Server" 
     VIEW-AS FILL-IN 
     SIZE 74 BY 1 TOOLTIP "Enter the ESB server address to send/receive STOMP formatted messages" NO-UNDO.

DEFINE VARIABLE tHost AS CHARACTER FORMAT "X(256)":U 
     LABEL "Application Server" 
     VIEW-AS FILL-IN 
     SIZE 74 BY 1 TOOLTIP "Enter the Compass server URL (protocol://host:port/cgi/msg/service/command)" NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 103 BY 3.1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tHost AT ROW 1.95 COL 27 COLON-ALIGNED WIDGET-ID 2
     tEsbHost AT ROW 3.14 COL 5.4 WIDGET-ID 14
     Btn_Cancel AT ROW 5.05 COL 37
     Btn_OK AT ROW 5.05 COL 54
     "Session Endpoints" VIEW-AS TEXT
          SIZE 21 BY .62 AT ROW 1.24 COL 3 WIDGET-ID 18
          FONT 6
     RECT-6 AT ROW 1.48 COL 2 WIDGET-ID 16
     SPACE(0.99) SKIP(2.17)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Server Endpoints"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tEsbHost IN FRAME fMain
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX fMain
/* Query rebuild information for DIALOG-BOX fMain
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Server Endpoints */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetServiceAddress" (output tHost).
PUBLISH "GetEsbAddress" (OUTPUT tEsbHost).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if tHost:screen-value in frame fMain > "" 
   then publish "SetServiceAddress" (tHost:screen-value in frame fMain).
  if tEsbHost:screen-value in frame fMain > "" 
   then publish "SetEsbAddress" (tEsbHost:screen-value in frame fMain).
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tHost tEsbHost 
      WITH FRAME fMain.
  ENABLE tHost tEsbHost Btn_Cancel Btn_OK 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

