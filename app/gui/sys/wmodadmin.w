&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*
  wadm.w
  COMMunications Window for client-side debugging
 */

CREATE WIDGET-POOL.

{lib/std-def.i}
/* {lib/winlaunch.i} */
{tt/module.i}

def var tModuleFile as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES module

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData module.appCode module.appID module.appPort module.started   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH module by module.appCode
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH module by module.appCode.
&Scoped-define TABLES-IN-QUERY-brwData module
&Scoped-define FIRST-TABLE-IN-QUERY-brwData module


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFindString bExit bView bRefresh bTest ~
brwData bFind bData bPort 
&Scoped-Define DISPLAYED-OBJECTS tFindString 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshData C-Win 
FUNCTION refreshData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bData 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh active module list".

DEFINE BUTTON bExit 
     LABEL "Exit" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bFind 
     LABEL "Find" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bPort 
     LABEL "Ports" 
     SIZE 7.2 BY 1.71 TOOLTIP "Set Administrative Service Port Range".

DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bTest 
     LABEL "Test" 
     SIZE 8.6 BY 1.14.

DEFINE BUTTON bView 
     LABEL "View" 
     SIZE 8.6 BY 1.14.

DEFINE VARIABLE tFindString AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 50.8 BY 1 TOOLTIP "Enter the message to send to Find" NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 15.8 BY 2.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      module SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      module.appCode label "Module"
 module.appID label "ID" format "x(50)"
 module.appPort label "Port" format "zzzzz"
 module.started label "Launched"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 118.8 BY 11.52 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tFindString AT ROW 2.71 COL 50 COLON-ALIGNED NO-LABEL WIDGET-ID 146
     bExit AT ROW 1.48 COL 51.8 WIDGET-ID 116
     bView AT ROW 1.48 COL 34.6 WIDGET-ID 118
     bRefresh AT ROW 2.62 COL 34.6 WIDGET-ID 136
     bTest AT ROW 1.48 COL 43.2 WIDGET-ID 120
     brwData AT ROW 4.05 COL 2 WIDGET-ID 200
     bFind AT ROW 2.62 COL 43.2 WIDGET-ID 138
     bData AT ROW 1.19 COL 2 WIDGET-ID 140
     bPort AT ROW 1.19 COL 9.2 WIDGET-ID 144
     "Send Action:" VIEW-AS TEXT
          SIZE 13 BY .62 TOOLTIP "To selected Module" AT ROW 1.71 COL 21.4 WIDGET-ID 142
     RECT-5 AT ROW 1.1 COL 1.4 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 120.8 BY 14.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Module Administrative Service"
         HEIGHT             = 14.76
         WIDTH              = 120.8
         MAX-HEIGHT         = 20.33
         MAX-WIDTH          = 170.2
         VIRTUAL-HEIGHT     = 20.33
         VIRTUAL-WIDTH      = 170.2
         SHOW-IN-TASKBAR    = no
         MAX-BUTTON         = no
         ALWAYS-ON-TOP      = yes
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData bTest fMain */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH module by module.appCode..
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Module Administrative Service */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Module Administrative Service */
DO:
  /* This event will close the window and terminate the procedure.  */
  apply "CLOSE" to this-procedure.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bData C-Win
ON CHOOSE OF bData IN FRAME fMain /* Refresh */
DO:
  refreshData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExit C-Win
ON CHOOSE OF bExit IN FRAME fMain /* Exit */
DO:
  if available module 
   then run sys/modadmmsg.p (module.appCode, "EXIT", output std-lo, output std-ch).
   else apply "VALUE-CHANGED" to browse brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFind
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFind C-Win
ON CHOOSE OF bFind IN FRAME fMain /* Find */
DO:
 if available module 
  then run sys/modadmmsg.p (module.appCode, 
                            "FIND:" + tFindString:screen-value in frame {&frame-name}, 
                            output std-lo, 
                            output std-ch).
  else apply "VALUE-CHANGED" to browse brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPort
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPort C-Win
ON CHOOSE OF bPort IN FRAME fMain /* Ports */
DO:
  run sys/modadmdialog.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
 if available module 
  then 
   do: run sys/modadmmsg.p (module.appCode, "REFRESH", output std-lo, output std-ch).
       if not std-lo 
        then MESSAGE std-ch
              VIEW-AS ALERT-BOX warning BUTTONS OK.
   end.
  else apply "VALUE-CHANGED" to browse brwData.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if available module 
   then apply "choose" to bView in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  assign
    bView:sensitive in frame {&frame-name} = available module
    bRefresh:sensitive in frame {&frame-name} = available module
    bTest:sensitive in frame {&frame-name} = available module
    bFind:sensitive in frame {&frame-name} = available module
    bExit:sensitive in frame {&frame-name} = available module
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTest C-Win
ON CHOOSE OF bTest IN FRAME fMain /* Test */
DO:
 if available module 
  then 
   do: run sys/modadmmsg.p (module.appCode, "TEST", output std-lo, output std-ch).
       if std-lo 
        then MESSAGE "Success"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        else 
             MESSAGE std-ch
              VIEW-AS ALERT-BOX warning BUTTONS OK.
   end.
  else apply "VALUE-CHANGED" to browse brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bView C-Win
ON CHOOSE OF bView IN FRAME fMain /* View */
DO:

 if available module 
  then 
   do: run sys/modadmmsg.p (module.appCode, "VIEW", output std-lo, output std-ch).
       if not std-lo 
        then MESSAGE std-ch
              VIEW-AS ALERT-BOX warning BUTTONS OK.
   end.
  else apply "VALUE-CHANGED" to browse brwData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
tModuleFile = os-getenv("appdata") + "\Alliant\compass3.dat".
subscribe to "ExternalRefresh" anywhere.
subscribe to "ExternalView" anywhere.
subscribe to "IsModuleAdminOpen" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}

bData:load-image("images/sync.bmp").
bPort:load-image("images/config.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

refreshData().
apply "value-changed" to browse brwData.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFindString 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tFindString bExit bView bRefresh bTest brwData bFind bData bPort 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExternalRefresh C-Win 
PROCEDURE ExternalRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pDetails as char.
 refreshData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExternalView C-Win 
PROCEDURE ExternalView :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pDetails as char.
 current-window:window-state = 3.
 current-window:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsModuleAdminOpen C-Win 
PROCEDURE IsModuleAdminOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pYes as logical init true.

 this-procedure:current-window:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshData C-Win 
FUNCTION refreshData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer x-module for module.

 close query brwData.
 TEMP-TABLE module:READ-JSON("file", tModuleFile, "empty") NO-ERROR.
 open query brwData for each module by module.appCode.

 std-in = 0.
 for each x-module:
  std-in = std-in + 1.
 end.

 std-ch = string(std-in) + " active module" + 
  (if std-in = 1 then "" else "s") + " as of " + string(now).
 status default std-ch in window this-procedure:current-window.
 status input std-ch in window this-procedure:current-window.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

