/*------------------------------------------------------------------------
@name getactionnotes.p
@description Gets a list of the actionnotes for an action

@param ActionID;int;The action identifier

@returns The action note table

@author SC
@version 1.0
@created 07/28/2017
----------------------------------------------------------------------*/

{tt/actionnote.i}

def input  parameter piActionID as int no-undo.
def output parameter table for actionnote.

{lib/srvdefs.i}

/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

if piActionID = ? or piActionID = 0 then
do: pSuccess = false.
    pMsg = "Action ID cannot be zero or unknown".
    return.
end.

run httpget.p ("",
               "actionNotesGet",
               "ActionID=" + encodeUrl(string(piActionID)),
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
  if qName = "Actionnote" then
  do: 
    create actionnote.
    DO  i = 1 TO  attributes:NUM-ITEMS:
      fldvalue = attributes:get-value-by-index(i).
      CASE attributes:get-qname-by-index(i):
        when "ActionID"    then actionnote.ActionID    = integer(fldvalue) no-error.
        when "seq"         then actionnote.seq         = integer(fldvalue) no-error.
        when "category"    then actionnote.category    = fldvalue no-error.
        when "method"      then actionnote.method      = fldvalue.
        when "dateCreated" then actionnote.dateCreated = datetime(fldvalue) no-error.
        when "uid"         then actionnote.uid         = fldvalue.
        when "secured"     then actionnote.secured     = logical(fldvalue).
        when "takenBy"     then actionnote.takenBy     = fldvalue.
        when "stat"        then actionnote.stat        = fldvalue.
        when "comments"    then actionnote.comments    = fldvalue.
        when "UserName"    then actionnote.UserName    = fldvalue.
      END CASE.
    END.
    release actionnote.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
