&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file modifyclaimcode.p
@description Makes a server call to add a claim code to the table

@param ClaimID;int;The claim identifier
@param CodeType;char;The type of code
@param Code;char;The code
@param Seq;int;The sequence number
@param Direction;char;The direction to move the claim code

@returns The success or error message
@returns A logical value of whether the move was successful or not

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

def input parameter pClaimID as int no-undo.
def input parameter pCodeType as char no-undo.
def input parameter pCode as char no-undo.
def input parameter pSeq as int no-undo.
def input parameter pDirection as char no-undo.
{lib/srvdefs.i}

/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

if pClaimID = ? or pClaimID = 0
 then 
  do: pSuccess = false.
      pMsg = "Claim ID must be greater than zero.".
      return.
  end.
  
if pCodeType = ? or pCodeType = ""
 then 
  do: pSuccess = false.
      pMsg = "Code Type cannot be blank or unknown.".
      return.
  end.
  
if pCode = ? or pCode = ""
 then 
  do: pSuccess = false.
      pMsg = "Code cannot be blank or unknown.".
      return.
  end.

if pSeq = 0 or pSeq = ?
 then 
  do: pSuccess = false.
      pMsg = "Sequence must be greater than zero.".
      return.
  end.

if lookup(upper(pDirection), "UP,DOWN") = 0
 then
  do: pSuccess = false.
      pMsg = "Direction must be UP or DOWN.".
      return.
  end.

 run httpget.p 
   ("",
    "claimCodeModify",
    "claimID=" + encodeUrl(string(pClaimID))
    + "&codeType=" + encodeUrl(pCodeType)
    + "&code=" + encodeUrl(pCode)
    + "&seq=" + encodeUrl(string(pSeq))
    + "&direction=" + encodeUrl(upper(pDirection))
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

