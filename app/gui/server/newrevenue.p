/*------------------------------------------------------------------------
@name newrevenue.p
@description Creates revenue type

@author Rahul Sharma
@version 1.0
@created 01/06/2020

@notes
@param arrevenue; input table to create records in DB
----------------------------------------------------------------------*/

/* Temp-Table Definitions */
{tt/arrevenue.i}

/* Parameter definitions */
define input  parameter table          for arrevenue.
define output parameter opdtCreateDate as datetime  no-undo.

{lib/srvdefs.i}

define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
                                                                                                                                                
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

/* add to the xml document */
std-ch = os-getenv("appdata") + "\Alliant\" + "newarrevenue"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.


xDoc:create-node(xRoot, "Action", "ELEMENT").
xDoc:append-child(xRoot).
appendXml(xRoot, "arrevenue", 'for each arrevenue').

xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

run httppost.p 
   ("revenueNew",
    "text/xml",
    search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess then
  return.

if pSuccess then
  empty temp-table arrevenue. 
 
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
  if qName = "parameter"
   then
    do i = 1 to attributes:num-items:
      fldvalue = attributes:get-value-by-index(i).
      case attributes:get-qname-by-index(i):
        when "createddate"  then opdtCreateDate = datetime(fldvalue).       
      end case.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
