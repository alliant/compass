&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getagentstatements.p
@description Makes a server call to get the statements for the agent

@param Agent;char;The id of the agent

@returns The temp table for the files of the agent

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/doc.i}
{tt/file.i}
def input parameter agentID as char no-undo.
def output parameter table for document.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
    
run httpget.p 
   ("",
    "agentAccountingDocumentsGet",
    "Agent=" + encodeUrl(agentID),
    output pSuccess,
    output pMsg,
    output tResponseFile).   
    
if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  def buffer doc for document.

  {lib/srvstartelement.i}
  case qName:
   when "Document" then
    do:
      create doc.
      do i = 1 to  attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
				 when "identifier" then doc.identifier = fldvalue.
				 when "type" then doc.type = fldvalue.
				 when "name" then doc.name = fldvalue.
				 when "displayName" then doc.displayName = fldvalue.
				 when "fullpath" then doc.fullpath = fldvalue.
				 when "subdir" then doc.subdir = fldvalue.
				 when "filetype" then doc.filetype = fldvalue.
				 when "fileSize" then
          assign
            std-de = decimal(fldvalue) / 1024
            doc.fileSize = truncate((if std-de < 1 then 1 else std-de), 0)
            no-error
            .
				 when "createdDate" then doc.createdDate = fldvalue.
        end case.
      END.
      release doc.
    end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

