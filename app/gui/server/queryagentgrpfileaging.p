&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name queryagentgrpfileaging.p
@description Make a server call to get the aging for the list of agents

@param AgentIDList;char;The identifier to retrieve

@author Rahul Sharma
@version 1.0
@created 10/22/2020
@modified
Date         Name     Description
08/13/2021   SC       #83827 Change consolidation SP to return results for multiple agents instead of single
----------------------------------------------------------------------*/

{tt/araging.i}

define input  parameter pAsOfDate         as datetime  no-undo.
define input  parameter pReportType       as character no-undo.
define input  parameter pAllAgents        as logical   no-undo.
define input  parameter pStateID          as character no-undo.
define input  parameter pYearOfSign       as integer   no-undo.
define input  parameter pSoftware         as character no-undo.
define input  parameter pCompany          as character no-undo.
define input  parameter pOrganization     as character no-undo.
define input  parameter pManager          as character no-undo. 
define input  parameter pTagList          as character no-undo.
define input  parameter pAffiliationList  as character no-undo.
define output parameter table                   for araging.
define output parameter opcARContactEmail       as character no-undo.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 12.14
         WIDTH              = 73.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p 
   (input  "",
    input "agentGrpFileAgingQuery",
    input  "AsOfDate="                 + trim(encodeUrl(string(pAsOfDate)))        +
           "&ReportType="               + trim(encodeUrl(pReportType))             +
           "&IncludeAllAgents="         + trim(encodeUrl(string(pAllAgents)))      +
           "&StateID="                  + trim(encodeUrl(pStateID))                +
           "&YearOfSign="               + trim(encodeUrl(string(pYearOfSign)))     +
           "&Software="                 + trim(encodeUrl(pSoftware))               +
           "&Company="                  + trim(encodeUrl(string(pCompany)))        +
           "&Organization="             + trim(encodeUrl(pOrganization))           +
           "&Manager="                  + trim(encodeUrl(pManager))                +
           "&TagList="                  + trim(encodeUrl(pTagList))                +
           "&AffiliationList="          + trim(encodeUrl(pAffiliationList))             
           ,   
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
  
 pSuccess = parseXML(output std-ch).
 if not pSuccess
  then pMsg = std-ch.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=araging &setParam="'AgentGroupAging'"}
  
  if qName = "parameter"
   then
    do i = 1 to attributes:num-items:
      fldvalue = attributes:get-value-by-index(i).
      case attributes:get-qname-by-index(i):
       when "Message"         then pMsg              = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue). 
       when "ARContactEmail"  then opcARContactEmail = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).   
      end case.                  
    end.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

