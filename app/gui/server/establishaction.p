&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name establishaction.p
@description Changes state of action to opened.

@param pActionID;int;The ActionID of action to be opened(required)
@param dDueDate;datetime;Due date of the action
@param cOwnerID;char;Owner of action
@param psuccess;logical; success message.
@param pmsg;char;message returned by server.

@returns ActionID of the action.

@author SC
@version 1.0
@created 07/20/2017
----------------------------------------------------------------------*/
{tt/action.i}
def input         parameter pActionID   as int      no-undo.
def input         parameter dDueDate    as datetime no-undo.
def input         parameter cOwnerID    as char     no-undo.
def input         parameter dStartDate  as datetime no-undo.
def input         parameter dFollupDate as datetime no-undo.
def output parameter table for Action.
{lib/srvdefs.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* validate the actionID id */
if  pActionID = ? then
do:
  pSuccess = false.
  pMsg = "The ActionID is not valid".
  return.
end.

run httpget.p ("",
               "actionEstablish",
               "ActionID=" + encodeUrl(string(pActionID))    +
               "&DueDate=" + encodeUrl(string(dDueDate))     +
               "&OwnerID=" + encodeUrl(cOwnerID)             +
               "&StartDate=" + encodeUrl(string(dStartDate)) +
               "&FollowupDate=" + encodeUrl(string(dFollupDate)),
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess 
 then return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

