&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file queryRefunds.p
@description Makes a server call to get one or multiple record(s)

@returns The ttunappliedtran dataset

@author SA
@version 1.0
@created 04/21/2021
@modifications
----------------------------------------------------------------------*/

{tt/unappliedtran.i &tableAlias="ttunappliedtran"}

define input parameter ipchRefundID        as character no-undo. 
define input parameter ipcEntityID         as character  no-undo.
define input parameter ipdtFromPostDate    as date       no-undo. 
define input parameter ipdtToPostDate      as date       no-undo. 
define input parameter ipcSearchString     as character  no-undo.
define input parameter iploIncludeVoid     as logical    no-undo.


define  output parameter table for ttunappliedtran.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */    

run httpget.p 
  ("",
   "refundsQuery",     
   "RefundID=" + (if ipchRefundID = ?  then "" else String(ipchRefundID))              +
   "&EntityID="     + (if ipcEntityID      = ?  then "" else String(ipcEntityID))      +
   "&FromPostDate=" + (if ipdtFromPostDate = ?  then "" else String(ipdtFromPostDate)) +
   "&ToPostDate="   + (if ipdtToPostDate   = ?  then "" else String(ipdtToPostDate))   +
   "&SearchString=" + (if ipcSearchString  = ?  then "" else string(ipcSearchString))  +
   "&IncludeVoid="  + string(iploIncludeVoid),
   output pSuccess,
   output pMsg,
   output tResponseFile).
       
if not pSuccess 
 then
  do:
    empty temp-table ttunappliedtran.
    return.
  end.
  
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/  
  {lib/get-temp-table.i &t=ttunappliedtran &setParam="'RefundTran'"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

