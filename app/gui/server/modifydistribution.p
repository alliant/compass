&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file newdistibution.p
@action distributionNew
@description Used to create a new distribution list

@param Name;character;The name of the distribution
@param Contact;complex;The contacts to delete from the distribution
@retrun Contact;complex;The contacts for the distribution

@author John Oliver
@created 2024.07.09
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/distribution.i &tableAlias="distribution"}
{tt/distribution.i &tableAlias="out-distribution"}
define input  parameter pName as character no-undo.
define input  parameter table for distribution.
define output parameter table for out-distribution.

{lib/srvdefs.i}
{lib/xmlencode.i}
{lib/tt-xml.i}

define variable xDoc  as handle no-undo.
define variable xRoot as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

std-ch = os-getenv("appdata") + "\Alliant\" + "ModifyDistribution"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create X-DOCUMENT xDoc.
create x-noderef xRoot.

xDoc:CREATE-NODE(xRoot, "Import", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "Distribution", 'for each distribution').

xDoc:save("file", std-ch).
delete object xRoot.
delete object xDoc.

run httppostparam.p (input  "distributionModify",
                     input  "text/xml",
                     input  search(std-ch),
                     input  "Name=" + encodeUrl(pName),
                     output pSuccess,
                     output pMsg,
                     output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).
os-delete std-ch.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=out-distribution &setParam="'Contact'"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

