/*------------------------------------------------------------------------
@name preliminvoicevoidgl.p
@action invoiceVoidGlPrelim 

@returns glinvoice
@author Anjly
@version 1.0
@created 07/24/2020
Modification:
Date          Name      Description
----------------------------------------------------------------------*/

{tt/ledgerreport.i &tableAlias="glinvoice"}
define input parameter ipiInvoiceID as integer.
define output parameter table for glinvoice.
{lib/srvdefs.i}

run httpget.p 
   ("",
    "invoiceVoidGlPrelim",
    "invoiceID=" + string(ipiInvoiceID),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

procedure startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 define buffer glinvoice for glinvoice.
  {lib/get-temp-table.i &t=glinvoice}
end procedure.
