&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name queryagentunappliedtrans.p
@description Get unapplied cash records of agent
@author Rahul Sharma
@version 1.0
@created 01/09/2020
@notes
@modified 
12-22-2020  Shubham     Added type parameter for Payments(P) and Credits(C)
12/08/2022  Shefali     Task-#100854 Add status on Unapplied Transactions report
----------------------------------------------------------------------*/
{tt/agentunappliedCash.i}

define input  parameter ipcAgentID      as character no-undo.
define input  parameter ipcStateID      as character no-undo.
define input  parameter ipcType         as character no-undo.

define output parameter table           for agentunappliedCash.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p 
   ("",
    "agentUnappliedTransQuery",
    (if ipcStateID     <> ? and ipcStateID   <> ""  then "stateID="       + trim(encodeUrl(ipcAgentID))        else "stateID=")     + 
    (if ipcAgentID     <> ? and ipcAgentID   <> ""  then "&agentID="      + trim(encodeUrl(ipcStateID))        else "&agentID=")    + 
    (if ipcType        <> ? and ipcType      <> ""  then "&type="         + trim(encodeUrl(ipcType))           else "&type="),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
 if qName = "agentunappliedCash"
  then
    do: 
      create agentunappliedCash.
      do  i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "stateID"       then agentunappliedCash.stateID      = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).          
          when "agentID"       then agentunappliedCash.agentID      = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).          
          when "stat"          then agentunappliedCash.stat         = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).
          when "name"          then agentunappliedCash.name         = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).           
          when "manager"       then agentunappliedCash.manager      = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).
          when "tranID"        then agentunappliedCash.tranID       = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).          
          when "filenumber"    then agentunappliedCash.filenumber   = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue).           
          when "checknum"      then agentunappliedCash.checknum     = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue). 
          when "checkAmt"      then agentunappliedCash.checkAmt     = if (fldvalue = "?" or fldvalue = ?) then 0    else decimal(fldvalue)   no-error.
          when "artranID"      then agentunappliedCash.artranID     = if (fldvalue = "?" or fldvalue = ?) then 0    else integer(fldvalue)   no-error.
          when "remainingAmt"  then agentunappliedCash.remainingAmt = if (fldvalue = "?" or fldvalue = ?) then 0    else decimal(fldvalue)   no-error.          
          when "appliedAmt"    then agentunappliedCash.appliedAmt   = if (fldvalue = "?" or fldvalue = ?) then 0    else decimal(fldvalue)   no-error.
          when "refundedAmt"   then agentunappliedCash.refundedAmt  = if (fldvalue = "?" or fldvalue = ?) then 0    else decimal(fldvalue)   no-error.          
          when "checkDate"     then agentunappliedCash.checkDate    = if (fldvalue = "?" or fldvalue = ?) then ?    else datetime(fldvalue)  no-error.
          when "receiptDate"   then agentunappliedCash.receiptDate  = if (fldvalue = "?" or fldvalue = ?) then ?    else datetime(fldvalue)  no-error.
          when "postDate"      then agentunappliedCash.postDate     = if (fldvalue = "?" or fldvalue = ?) then ?    else datetime(fldvalue)  no-error.          
          when "depositRef"    then agentunappliedCash.depositRef   = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue). 
          when "arCashglRef"   then agentunappliedCash.arCashglRef  = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue). 
          when "arglref"       then agentunappliedCash.arglref      = if (fldvalue = "?" or fldvalue = ?) then ""   else decodexml(fldvalue). 
        end case.
      end.
      release agentunappliedCash.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

