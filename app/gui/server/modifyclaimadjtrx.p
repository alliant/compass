&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file newclaimadjtrx.p
@purpose Makes a server call to modify the claim adjustment transcation

@param claimadjtrx;table;The claimadjtrx input table

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/claimadjtrx.i}

def input parameter table for claimadjtrx.

{lib/srvdefs.i}

/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

std-in = 0.
for each claimadjtrx:
 std-in = std-in + 1.
end.

if std-in <> 1
 then
  do: pSuccess = false.
      pMsg = "One claim Adjustment Transaction structure must exist.".
      return.
  end.

find first claimadjtrx.

 run httpget.p 
   ("",
    "claimAdjTrxModify",
    "claimID=" + encodeUrl(string(claimadjtrx.claimID))
    + "&refCategory=" + encodeUrl(string(claimadjtrx.refCategory))
    + "&requestedAmount=" + encodeUrl(string(claimadjtrx.requestedAmount))
    + "&requestedBy=" + encodeUrl(string(claimadjtrx.requestedBy))
    + "&notes=" + encodeUrl(string(claimadjtrx.notes))
    + "&transDate=" + encodeUrl(string(claimadjtrx.transDate))
    + "&transAmount=" + encodeUrl(string(claimadjtrx.transAmount))
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

