&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file newclaim.p
@action claimNew
@description Makes a server call to create a claim

@param claim;table;The claim input table

@returns ClaimID;int;ClaimID for the new claim

@author John Oliver (joliver)
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/claim.i}

def input parameter table for claim.
def output parameter pNewClaimID as int no-undo.

{lib/srvdefs.i}

/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


std-in = 0.
for each claim:
 std-in = std-in + 1.
end.

if std-in <> 1
 then
  do: pSuccess = false.
      pMsg = "One Claim structure must exist.".
      return.
  end.

find first claim.

 run httpget.p 
   ("",
    "claimNew",
    "description=" + encodeUrl(claim.description)
    + "&stat=" + encodeUrl(claim.stat)
    + "&type=" + encodeUrl(claim.type)
    + "&stage=" + encodeUrl(claim.stage)
    + "&action=" + encodeUrl(claim.action)
    + "&difficulty=" + encodeUrl(string(claim.difficulty))
    + "&urgency=" + encodeUrl(string(claim.urgency))
    + "&effDate=" + encodeUrl(string(claim.effDate))
    + "&refYear=" + encodeUrl(string(claim.refYear))
    + "&yearFirstReport=" + encodeUrl(string(claim.yearFirstReport))
    + "&periodID=" + encodeUrl(string(claim.periodID))
    + "&stateID=" + encodeUrl(claim.stateID)
    + "&assignedTo=" + encodeUrl(claim.assignedTo)
    + "&agentID=" + encodeUrl(claim.agentID)
    + "&agentName=" + encodeUrl(claim.agentName)
    + "&agentStat=" + encodeUrl(claim.agentStat)
    + "&fileNumber=" + encodeUrl(claim.fileNumber)
    + "&insuredName=" + encodeUrl(claim.insuredName)
    + "&borrowerName=" + encodeUrl(claim.borrowerName)
    + "&hasReinsurance=" + encodeUrl(string(claim.hasReinsurance))
    + "&hasDeductible=" + encodeUrl(string(claim.hasDeductible))
    + "&deductibleAmount=" + encodeUrl(string(claim.deductibleAmount))
    + "&amountWaived=" + encodeUrl(string(claim.amountWaived))
    + "&seekingRecovery=" + encodeUrl(claim.seekingRecovery)
    + "&recoveryNotes=" + encodeUrl(claim.recoveryNotes)
    + "&eoPolicy=" + encodeUrl(claim.eoPolicy)
    + "&eoCarrier=" + encodeUrl(claim.eoCarrier)
    + "&eoEffDate=" + encodeUrl(string(claim.eoEffDate))
    + "&eoRetroDate=" + encodeUrl(string(claim.eoRetroDate))
    + "&eoCoverageAmount=" + encodeUrl(string(claim.eoCoverageAmount))
    + "&agentError=" + encodeUrl(claim.agentError)
    + "&attorneyError=" + encodeUrl(claim.attorneyError)
    + "&searcherError=" + encodeUrl(claim.searcherError)
    + "&searcher=" + encodeUrl(claim.searcher)
    + "&altaRisk=" + encodeUrl(string(claim.altaRisk))
    + "&altaResponsibility=" + encodeUrl(string(claim.altaResponsibility))
    + "&priorPolicyUsed=" + encodeUrl(claim.priorPolicyUsed)
    + "&priorPolicyID=" + encodeUrl(claim.priorPolicyID)
    + "&underwritingCompliance=" + encodeUrl(claim.underwritingCompliance)
    + "&sellerBreach=" + encodeUrl(claim.sellerBreach)
    + "&priorPolicyCarrier=" + encodeUrl(claim.priorPolicyCarrier)
    + "&priorPolicyAmount=" + encodeUrl(string(claim.priorPolicyAmount))
    + "&priorPolicyEffDate=" + encodeUrl(string(claim.priorPolicyEffDate))
    + "&summary=" + encodeUrl(claim.summary)
    + "&dateClmReceived=" + encodeUrl(string(claim.dateClmReceived))
    + "&dateTransferred=" + encodeUrl(string(claim.dateTransferred))
    + "&dateCreated=" + encodeUrl(string(claim.dateCreated))
    + "&userCreated=" + encodeUrl(claim.userCreated)
    + "&dateAssigned=" + encodeUrl(string(claim.dateAssigned))
    + "&userAssigned=" + encodeUrl(claim.userAssigned)
    + "&dateAcknowledged=" + encodeUrl(string(claim.dateAcknowledged))
    + "&userAcknowledged=" + encodeUrl(claim.userAcknowledged)
    + "&dateClosed=" + encodeUrl(string(claim.dateClosed))
    + "&userClosed=" + encodeUrl(claim.userClosed)
    + "&dateRecReceived=" + encodeUrl(string(claim.dateRecReceived))
    + "&dateReOpened=" + encodeUrl(string(claim.dateReOpened))
    + "&userReOpened=" + encodeUrl(claim.userReOpened)
    + "&dateResponded=" + encodeUrl(string(claim.dateResponded))
    + "&userResponded=" + encodeUrl(claim.userResponded)
    + "&dateInitResponded=" + encodeUrl(string(claim.dateInitResponded))
    + "&userInitResponded=" + encodeUrl(claim.userInitResponded)
    + "&dateReClosed=" + encodeUrl(string(claim.dateReClosed))
    + "&userReClosed=" + encodeUrl(claim.userReClosed)
    + "&lastActivity=" + encodeUrl(string(claim.lastActivity))
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 if qName = "parameter"
  then
    do: 
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "claimID" THEN pNewClaimID = integer(decodeXml(fldvalue)).
         END CASE.
        END.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

