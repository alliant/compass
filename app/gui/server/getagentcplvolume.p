&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name getalertcplvolume.p
@action alertCPLVolumeGet
@description Gets the CPL data from ARC and creates an alert based on the data

@param AgentID;char;The agent ID

@return Cplvolume;complex;The temp table for the cpl volume api call

@author John Oliver
@version 1.0
@created 2017/09/26
---------------------------------------------------------------------*/

{tt/cplvolume.i}
define input parameter pStateID as character no-undo.
define input parameter pAgentID as character no-undo.
define output parameter table for cplvolume.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p 
   ("",
    "agentCPLVolumeGet",
    "StateID=" + encodeUrl(pStateID) + 
    "&AgentID=" + encodeUrl(pAgentID),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).

if not pSuccess
 then empty temp-table cplvolume.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  define buffer cplvolume for cplvolume.

  if qName = "CPLVolume"
   then
     do: 
      create cplvolume.
      do  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
         when "agentID" then cplvolume.agentID = fldvalue.
         when "agentName" then cplvolume.agentName = fldvalue.
         when "stateID" then cplvolume.stateID = fldvalue.
         when "officeName" then cplvolume.officeName = fldvalue.
         when "stat" then cplvolume.stat = fldvalue.
         when "manager" then cplvolume.manager = fldvalue.
         when "month3" then cplvolume.month3 = integer(fldvalue) no-error.
         when "month2" then cplvolume.month2 = integer(fldvalue) no-error.
         when "month1" then cplvolume.month1 = integer(fldvalue) no-error.
         when "cplCount" then cplvolume.cplCount = integer(fldvalue) no-error.
         when "threeMonthAverage" then cplvolume.threeMonthAverage = decimal(fldvalue) no-error.
        end case.
      end.
      release cplvolume.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

