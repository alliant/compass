&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/deleteratecarddetail.p

@description Deletes the ratecard with specified CardID
@author Archana Gupta
@created 04.11.2018
@notes
@param  piCardID;      The cardID of ratecard
@return tempRateState; ratestate record with corresponding ratecard deleted.
*/

/* temp-table definitions */
{tt\ratecard.i}
{tt\ratestate.i  &tablealias=tempRateState}

/* parameter definitions  */
define input  parameter piCardID    as integer no-undo.
define output parameter table for tempRateState.
{lib\srvdefs.i}
{lib\xmlencode.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 if  piCardID = ? or  piCardID = 0 
  then
   do:
     pSuccess = false.
     pMsg     = "The CardID is not valid".
     return.
   end.

 run httpget.p 
   ("",
    "rateCardDelete",
    "cardID=" + encodeUrl(string(piCardID)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.

 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
  if qName = "rateState"
   then
    do:
      create tempRateState.
      do i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
           when "cardSetID"         then tempratestate.cardSetID         = if integer(fldvalue) = ?            then 0           else integer(fldvalue) no-error.
           when "version"           then tempratestate.version           = if integer(fldvalue) = ?            then 0           else integer(fldvalue) no-error.
           when "active"            then tempratestate.active            = if logical(fldvalue) = ?            then no          else logical(fldvalue) no-error.
           when "stateID"           then tempratestate.stateID           = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
           when "effectiveDate"     then tempratestate.effectiveDate     = if datetime(fldvalue) = ?           then datetime(?) else datetime(fldvalue) no-error.
           when "createdBy"         then tempratestate.createdBy         = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
           when "dateApproved"      then tempratestate.dateApproved      = if datetime(fldvalue) = ?           then datetime(?) else datetime(fldvalue) no-error.
           when "approvedBy"        then tempratestate.approvedBy        = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
           when "dateFiled"         then tempratestate.dateFiled         = if datetime(fldvalue) = ?           then datetime(?) else datetime(fldvalue) no-error.
           when "filingMethod"      then tempratestate.filingMethod      = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
           when "dateStateApproved" then tempratestate.dateStateApproved = if datetime(fldvalue) = ?           then datetime(?) else datetime(fldvalue) no-error.
           when "description"       then tempratestate.description       = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
           when "comments"          then tempratestate.comments          = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
           when "lastModifiedDate"  then tempratestate.lastModifiedDate  = if datetime(fldvalue) = ?           then datetime(?) else datetime(fldvalue) no-error.
           when "lastModifiedBy"    then tempratestate.lastModifiedBy    = if (fldvalue = ? or fldvalue = "?") then ""          else decodeFromXml(fldvalue).
        end case.
      end.
    end.  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

