&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------*/
/*    File        : getclaimsadjtrx.p                                   */
/*    Purpose     : Makes a server call to get one or all of the claim  */
/*                  adjustment transactions                             */
/*                                                                      */
/*    Syntax      : Progress (OpenEdge)                                 */
/*                                                                      */
/*    Parameters  : ClaimID;int;Claim number                            */
/*    Parameters  : RefCat;char;Refering Category (Optional)            */
/*                                                                      */
/*    Returns     : claimadjtrx;table;the claimadjtrx table             */
/*                                                                      */
/*    Author(s)   : John Oliver (joliver)                               */
/*    Created     : XX.XX.2015                                          */
/*    Notes       :                                                     */
/*----------------------------------------------------------------------*/

{tt/claimadjtrx.i}

def input parameter pClaimID as int no-undo.
def input parameter pRefCat as char no-undo.
def output parameter table for claimadjtrx.
{lib/srvdefs.i}

def var sQuery as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the claim ID must be there */
 if pClaimID = ? or pClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "Claim ID cannot be zero or unknown".
        return.
    end.
 sQuery = "claimID=" + encodeURL(string(pClaimID)).
 
 /* add the referring category to the querystring if available */
 if pRefCat <> ? and pRefCat <> ""
  then sQuery = sQuery + "&refCat=" + encodeUrl(pRefCat).
    
 run httpget.p 
   ("",
    "claimAdjTrxGet",
    sQuery,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table claimadjtrx.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer claimadjtrx for claimadjtrx.

 if qName = "ClaimAdjTrx"
  then
    do: create claimadjtrx.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
            when "claimID" then claimadjtrx.claimID = integer(fldvalue) no-error.
						when "refCategory" then claimadjtrx.refCategory = fldvalue.
						when "requestedAmount" then claimadjtrx.requestedAmount = decimal(fldvalue) no-error.
						when "requestedBy" then claimadjtrx.requestedBy = fldvalue.
						when "dateRequested" then claimadjtrx.dateRequested = datetime(fldvalue) no-error.
						when "notes" then claimadjtrx.notes = fldvalue.
						when "uid" then claimadjtrx.uid = fldvalue.
						when "transDate" then claimadjtrx.transDate = datetime(fldvalue) no-error.
						when "transAmount" then claimadjtrx.transAmount = decimal(fldvalue) no-error.
         END CASE.
        END.
        release claimadjtrx.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

