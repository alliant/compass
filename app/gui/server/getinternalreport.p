&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getinternaleport.p
@description Generates the internal audit report

@param AuditID;int;The identifier to modify
@param File;char;The file

@author John Oliver
@version 1.0
@created 11/30/2015
@modified 
@AG - added report delete functionality when application close 
----------------------------------------------------------------------*/

def input  parameter piAuditID as int no-undo.
def output parameter cFile     as char no-undo.

def var doSave     as logical init true.
def var tReportDir as char.

{lib/std-def.i}
{lib/srvdefs.i}
{lib/xmlencode.i}
{tt/file.i}

def var mFile as memptr no-undo.


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

if piAuditID = ? or piAuditID = 0 then
do:
  pSuccess = false.
  pMsg = "Audit ID cannot be zero or unknown".
  return.
end.

run httpget.p ("",
               "auditInternalReportGet",
               "auditID=" + encodeUrl(string(piAuditID)),
               output pSuccess,
               output pMsg,
               output tResponseFile).
    
if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).

if pSuccess
 then
  for first file no-lock:
    run server/rebuildfile.p (file.filename, table file, output mFile, output std-lo).
    publish "GetReportDir" (output tReportDir).
    if search(tReportDir) = ?
     then tReportDir = os-getenv("appdata") + "\Alliant".
    cFile =  tReportDir + "\InternalReport_" + string(piAuditID) + ".pdf".
    copy-lob from mFile to file cFile no-error.
  end.

publish "AddTempFile" ("Audit", cFile).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

  case qName:
    when "pdf" then
    do:
      create file.
      DO i = 1 TO attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        CASE attributes:get-qname-by-index(i):
         when "filename" then file.filename = decodeFromXml(fldvalue).
         when "base64" THEN file.base64 = decodeFromXml(fldvalue).
         when "order" then file.order = integer(fldvalue) no-error.
        END CASE.
      END.
      release file.
    end.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
