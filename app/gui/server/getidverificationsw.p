/*------------------------------------------------------------------------
@file getidverificationsw.p
@description return ID Verification SW

@author SRK
@created 11/29/2024

Name          Date       Note
------------- ---------- -----------------------------------------------
------------------------------------------------------------------------*/

{tt/idverificationsw.i}

def output parameter table for idverificationsw.
{lib/srvdefs.i}
{lib/rpt-defs.i}

{lib/compass-dataset-def.i &buffers="buffer idverificationsw:handle"}

run util/httpcall.p (input  "get",
                     input  "doweb",
                     input  "idVerificationSWGet",
                     input  "",
                     input  "",
                     input  "",
                     {lib/rpt-setparams.i},
                     output pSuccess,
                     output pMsg,
                     output clientResponse).

if not pSuccess
 then 
  return.

pSuccess = loadDataset(output pMsg).

if not pSuccess 
 then
  empty temp-table idverificationsw.



