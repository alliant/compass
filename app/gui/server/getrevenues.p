&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getrevenues.p
@description Makes a server call to get one or multiple record(s)

@returns The arrevenue dataset

@author Rahul Sharma
@version 1.0
@created 01/06/2020
----------------------------------------------------------------------*/

{tt/arrevenue.i}

define input  parameter ipcRevenueType  as  character no-undo.
define output parameter table           for arrevenue.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p 
   ("",
    "revenuesGet",
    (if ipcRevenueType <> ? and ipcRevenueType <> "" then "RevenueType=" + encodeUrl(ipcRevenueType) else "RevenueType="),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
  define buffer arrevenue for arrevenue.
  
  if qName = "ArRevenue"
   then
     do:
       create arrevenue.
       do i = 1 TO  attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).

        case attributes:get-qname-by-index(i):
          when "revenueType"      then arrevenue.revenueType      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "grossGLRef"       then arrevenue.grossGLRef       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "grossGLDesc"      then arrevenue.grossGLDesc      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "retainedGLRef"    then arrevenue.retainedGLRef    = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "retainedGLDesc"   then arrevenue.retainedGLDesc   = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).  
          when "netGLRef"         then arrevenue.netGLRef         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "netGLdesc"        then arrevenue.netGLdesc        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "notes"            then arrevenue.notes            = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "createdDate"      then arrevenue.createdDate      = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error. 
          when "createdBy"        then arrevenue.createdBy        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "modifiedDate"     then arrevenue.modifiedDate     = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error.
          when "modifiedBy"       then arrevenue.modifiedBy       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "cUsername"        then arrevenue.cUsername        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "mUsername"        then arrevenue.mUsername        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
        end case.
       end.
       release arrevenue.
     end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

