/*------------------------------------------------------------------------
@name import.p
@action batchFormImport
@description Calls the server via POST

@param Action;char;The action server call
@param ID;char;The ID of the parent record
@param Seq;int;The sequence of the parent record
@param Import;complex;The importdata temp-table

@author John Oliver
@version 1.0
@created 8/23/2017
----------------------------------------------------------------------*/

{tt/importdata.i}

define input parameter pAction as character no-undo.
define input parameter pID as character no-undo.
define input parameter pSeq as integer no-undo.
define input parameter table for importdata.

define variable xDoc as handle no-undo.
define variable xRoot as handle no-undo.
define variable cFile as character no-undo.

{lib/srvdefs.i}
{lib/xmlencode.i}
{lib/tt-xml.i}

 
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

if pAction = ? or pAction = ""
 then
  do: pSuccess = false.
      pMsg = "There must be an action".
      return.
  end.
  
cFile = os-getenv("appdata") + "\Alliant\" + string(pID) + ".xml".
create X-DOCUMENT xDoc.
create x-noderef xRoot.

xDoc:CREATE-NODE(xRoot, "Import", "ELEMENT").
xDoc:append-child(xRoot).
xRoot:set-attribute("ID", pID).
xRoot:set-attribute("Seq", string(pSeq)).

appendXml(xRoot, "importdata", 'for each importdata').

xDoc:save("file", cFile).
delete object xRoot.
delete object xDoc.

run httppost.p 
   (pAction,
    "text/xml",
    search(cFile),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).

os-delete value(cFile).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&endif
