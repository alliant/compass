&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
srvnewbatch.p
create a NEW BATCH within a period by calling the SeRVer action
D.Sinclair
2.16.2014
*/

{tt/batch.i}

def input-output parameter table for batch.
def output parameter pNewBatchID as int no-undo.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

find first batch
  where batch.batchID = 0 or batch.batchID = ? no-error.
if not available batch
 then
  do: pSuccess = false.
      pMsg = "Batch structure cannot be blank.".
      return.
  end.


if batch.agentID = "" or batch.agentID = ?
 then
  do: pSuccess = false.
      pMsg = "Agent ID cannot be blank or unknown.".
      return.
  end.

if batch.receivedDate = ?
 then batch.receivedDate = today.
 
 run httpget.p 
   ("",
    "batchCreate",
    "periodID=" + string(batch.periodID)
    + "&agentID=" + encodeUrl(batch.agentID)
    + "&stateID=" + encodeUrl(batch.stateID)
    + "&stat=" + encodeUrl(batch.stat)
    + "&createDate=" + encodeUrl(string(batch.createDate))
    + "&receivedDate=" + encodeUrl(string(batch.receivedDate))
    + "&startDate=" + encodeUrl(string(batch.startDate))
    + "&duration=" + encodeUrl(string(batch.duration))
    + "&endDate=" + encodeUrl(string(batch.endDate))
    + "&acctDate=" + encodeUrl(string(batch.acctDate))
    + "&liabilityAmount=" + encodeUrl(string(batch.liabilityAmount))
    + "&liabilityDelta=" + encodeUrl(string(batch.createDate))
    + "&grossPremiumReported=" + encodeUrl(string(batch.grossPremiumReported))
    + "&grossPremiumProcessed=" + encodeUrl(string(batch.grossPremiumProcessed))
    + "&grossPremiumDiff=" + encodeUrl(string(batch.grossPremiumDiff))
    + "&grossPremiumDelta=" + encodeUrl(string(batch.grossPremiumDelta))
    + "&netPremiumReported=" + encodeUrl(string(batch.netPremiumReported))
    + "&netPremiumProcessed=" + encodeUrl(string(batch.netPremiumProcessed))
    + "&netPremiumDiff=" + encodeUrl(string(batch.netPremiumDiff))
    + "&netPremiumDelta=" + encodeUrl(string(batch.netPremiumDelta))
    + "&retainedPremiumProcessed=" + encodeUrl(string(batch.retainedPremiumProcessed))
    + "&retainedPremiumDelta=" + encodeUrl(string(batch.retainedPremiumDelta))
    + "&fileCount=" + encodeUrl(string(batch.fileCount))
    + "&policyCount=" + encodeUrl(string(batch.policyCount))
    + "&endorsementCount=" + encodeUrl(string(batch.endorsementCount))
    + "&cplCount=" + encodeUrl(string(batch.cplCount))
    + "&periodMonth=" + encodeUrl(string(batch.periodMonth))
    + "&periodYear=" + encodeUrl(string(batch.periodYear))
    + "&agentName=" + encodeUrl(batch.agentName)
    + "&reference=" + encodeUrl(batch.reference)
    + "&cashReceived=" + encodeUrl(string(batch.cashReceived))
    + "&rcvdVia=" + encodeUrl(batch.rcvdVia)
    + "&processStat=" + encodeUrl(batch.processStat)
    + "&invoiceDate=" + encodeUrl(string(batch.invoiceDate))
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).
 
 assign batch.batchID = pNewBatchID.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 if qName = "parameter"
  then
    do: 
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "BatchID" THEN pNewBatchID = int(decodeXml(fldvalue)).
         END CASE.
        END.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

