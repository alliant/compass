&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/setclaimassignee.p
@desc Change the user assigned to a claim file
@author D.Sinclair
@date 12.31.2016
*/

def input parameter pClaimID as int.
def input parameter pUid as char.
def input parameter pNotes as char.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 73.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if pClaimID = 0 or pClaimID = ?
 then
  do: pSuccess = false.
      pMsg = "Claim ID cannot be blank or unknown.".
      return.
  end.
if pUid = "" or pUid = ? 
 then
  do: pSuccess = false.
      pMsg = "User assigned to a file cannot be blank or unknown.".
      return.
  end.

 run httpget.p 
   ("",
    "claimAssign",
    "claimID=" + encodeUrl(string(pClaimID))
     + "&uid=" + encodeurl(pUid)
     + "&note=" + encodeUrl(pNotes),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

