&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
server/getbatches.p
GET BATCHES by calling the SeRVer action
Created D.Sinclair 2.16.2014

Notes:
    pPeriodID   Pass null to get a single batch
    OR
    pBatchID    Pass null to get all batches for a period 

9.28.2014 D.Sinclair  Add four *count fields into startElement
*/

{tt/batch.i}
{tt/batchaction.i}

def input parameter pPeriodID as int no-undo.

def output parameter table for batch.
def output parameter table for batchaction.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if pPeriodID = ?
 then
  do: pSuccess = false.
      pMsg = "Period and Batch cannot both be unknown.".
      return.
  end.

std-lo = ?.
if pPeriodID <> ? and pPeriodID > 0
 then std-lo = true. /* Period */
 
if std-lo = ?
 then
  do: pSuccess = false.
      pMsg = "Period and Batch cannot both be blank.".
      return.
  end.
   
 run httpget.p 
   ("",
    "agentsForPeriodGet",
    "periodID=" + string(pPeriodID),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 /* Ensure data integrity */
 for each batchaction:
  if can-find(batch where batch.batchID = batchaction.batchID)
   then next.
  delete batchaction.
 end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer batch for batch.
 def buffer batchaction for batchaction.
 
 if qName = "Batch"
  then
    do: create batch.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "batchID" THEN batch.batchID = int(fldvalue).
          when "stat" THEN batch.stat = fldvalue.
          when "periodMonth" then batch.periodMonth = int(fldvalue) no-error.
          when "periodYear" then batch.periodYear = int(fldvalue) no-error.
          when "periodID" then batch.periodID = int(fldvalue) no-error.
          when "agentID" then batch.agentID = fldvalue.
          when "agentName" then batch.agentName = decodeXml(fldvalue).
          when "stateID" then batch.stateID = decodeXml(fldvalue).
          when "reference" then batch.reference = decodeXml(fldvalue).
          when "receivedDate" then batch.receivedDate = date(fldvalue) no-error.
          when "grossPremiumReported" then batch.grossPremiumReported = decimal(fldvalue) no-error.
          when "grossPremiumProcessed" then batch.grossPremiumProcessed = decimal(fldvalue) no-error.
          when "grossPremiumDiff" then batch.grossPremiumDiff = decimal(fldvalue) no-error.
          when "grossPremiumDelta" then batch.grossPremiumDelta = decimal(fldvalue) no-error.

          when "netPremiumReported" then batch.netPremiumReported = decimal(fldvalue) no-error.
          when "netPremiumProcessed" then batch.netPremiumProcessed = decimal(fldvalue) no-error.
          when "netPremiumDiff" then batch.netPremiumDiff = decimal(fldvalue) no-error.
          when "netPremiumDelta" then batch.netPremiumDelta = decimal(fldvalue) no-error.

          when "retainedPremiumProcessed" then batch.retainedPremiumProcessed = decimal(fldvalue) no-error.
          when "retainedPremiumDelta" then batch.retainedPremiumDelta = decimal(fldvalue) no-error.

          when "cashReceived" then batch.cashReceived = decimal(fldvalue) no-error.
          when "liabilityAmount" then batch.liabilityAmount = decimal(fldvalue) no-error.
          when "liabilityDelta" then batch.liabilityDelta = decimal(fldvalue) no-error.
          when "rcvdVia" then batch.rcvdVia = decodeXml(fldvalue).
          when "lockedBy" then batch.lockedBy = decodeXml(fldvalue).
          when "lockedDate" then batch.lockedDate = date(fldvalue) no-error.
          when "processStat" then batch.processStat = fldvalue.
          
        /*when "createdBy" then batch.createdBy = decodeXml(fldvalue).*/
          when "createDate" then batch.createDate = date(fldvalue) no-error.
        /*when "processedBy" then batch.processedBy = decodeXml(fldvalue).*/
        /*when "processDate" then batch.processDate = date(fldvalue) no-error.*/
        /*when "invoicedBy" then batch.invoicedBy = decodeXml(fldvalue).*/
          when "invoiceDate" then batch.invoiceDate = date(fldvalue) no-error.
        /*when "followupBy" then batch.followupBy = decodeXml(fldvalue).*/
        /*when "followupDate" then batch.followupDate = date(fldvalue) no-error.*/
          when "hasDocument" then batch.hasDocument = logical(fldvalue) no-error.

          when "fileCount" then batch.fileCount = int(fldvalue) no-error.
          when "policyCount" then batch.policyCount = int(fldvalue) no-error.
          when "endorsementCount" then batch.endorsementCount = int(fldvalue) no-error.
          when "cplCount" then batch.cplCount = int(fldvalue) no-error.

         END CASE.
        END.
        release batch.
    end.
 if qName = "BatchAction"
  then
    do: create batchaction.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "batchID" THEN batchaction.batchID = int(decodeXml(fldvalue)).
          when "seq" THEN batchaction.seq = int(fldvalue) no-error.
          when "postDate" then batchaction.postDate = date(fldvalue) no-error.
          when "postUser" then batchaction.postUser = decodeXml(fldvalue).
          when "postInitials" then batchaction.postInitials = decodeXml(fldvalue).
          when "comments" then batchaction.comments = decodeXml(fldvalue).
          when "secure" then batchaction.secure = (if fldvalue = "T" then true else false).
         END CASE.
        END.
        release batchaction.
    end.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

