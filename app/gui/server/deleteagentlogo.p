/*------------------------------------------------------------------------
@name gui/server/deleteagentlogo.p
@description Delete Agent Logo in DB

@author Archana 
@version 1.0
@created 06/19/2020
Modification:
Date        Name             Description
----------------------------------------------------------------------*/
 /* Parameter Definitions */
 define input  parameter ipcAgentID as character no-undo.

{lib/srvdefs.i}

run httpget.p 
   ("",
    "agentlogodelete",
    "AgentID=" + (if ipcAgentID <> ? and ipcAgentID <> "" then encodeUrl(ipcAgentID) else ""),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).
 
PROCEDURE startElement :
 /*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

END PROCEDURE.
