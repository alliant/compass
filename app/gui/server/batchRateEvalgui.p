{tt\batchProcessed.i}
{tt\calcParameters.i}
{tt\calcOutputParameters.i} 
{tt\batchProcessed.i  &tableAlias=batchreject}

define input parameter pbatchid as int.
define input parameter pseq as int.
define input parameter pRegion as char.
define input parameter pVersion as char.
define input parameter pPeriodID as int.

{lib/srvdefs.i}


define variable ttdataReport as handle.
 define variable bfdatareport as handle.
 define variable bfBatchProcessed as handle.
 define variable qBatchProcessed as handle.
 define variable fiBatchProcessed as handle.

 define var totalfilepremium as decimal.
 define var ownerPremium as decimal.
 define var loanPremium as decimal.
 define var scndPremium as decimal.
  define var ownerEndorsPremium as decimal.
 define var loanEndorsPremium as decimal.
 define var scndEndorsremium as decimal.

 define var fieldcnt as int.
 define var fieldData as char extent 100.
 define var iCount as int.
 define var cScenarioMatched as char.
 define var minScenarioPremium AS decimal initial 9999999999999999 .
 define VAR maxScenarioPremium as DECIMAL. 
 define variable dtattoexport as char.
define variable lPremiumMatched as logical.
define variable cScenarioNameMatched as char.
 define var filesummary as char extent 15.
 define variable deCa as decimal.
define variable deLCa as decimal.
 define var deSlCa as decimal.

/* ***************************  Main Block  *************************** */
             
run httpget.p 
   (input  "",
    input  "evalbatchRate",
    input  "BatchID="  + string(pbatchid) + 
           "&Seq="     + string(pseq) +
           "&Region="  + encodeUrl(pRegion)+
           "&Version=" + encodeUrl(pVersion) + 
           "&PeriodID=" + string(pPeriodID),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).
                                  
output to value("C:\antic\manual\batchProcessed" + string(pPeriodID) + ".csv") .
 for each  batchProcessed :
 export delimiter "," batchProcessed.
 end.
output close.

output to value("C:\antic\manual\calcParameters" + string(pPeriodID) + ".csv") .
 for each  calcParameters :
 export delimiter "," calcParameters.
 end.
output close.

output to value("C:\antic\manual\calcOutputParameters" + string(pPeriodID) + ".csv") .
 for each  calcOutputParameters :
 export delimiter "," calcOutputParameters.
 end.
output close.

output to value("C:\antic\manual\batchreject" + string(pPeriodID) + ".csv") .
export delimiter "," "BatchID" "AgentID" "FileID" "PolicyID" "FormIDs" "BatchPremium" "Comments".
 for each  batchreject :
 export delimiter "," batchreject.batchID batchreject.agentID batchreject.FileID  batchreject.PolicyID batchreject.formIDs batchreject.batchPremium batchreject.comments.
 end.
output close.
                      
output to value("C:\antic\manual\batchRateEvalResult" + string(pPeriodID) + ".csv") .
export delimiter "," "Scenario ID" "Batch ID"  "File ID"  "Polic ID" "Form IDs" "Region" "Version" "Scenario Name"  "Coverage Amount"  "Loan CoverageAmount" "Scnd Loan CoverageAmount"
   "Premium Owner" "Premium Owner Endors" "Premium Loan" "Premium Loan Endors" "Premium Scnd"  "Premium Scnd Endors" "Batch Premium" "Premium Matched".
for each  batchProcessed :
  for each calcParameters where calcParameters.scenarioID = batchProcessed.scenarioID:
     deCa  = 0.
     deLCa = 0.
     deSlCa = 0.
     assign  deCa = decimal(calcParameters.coverag)  no-error.
     assign deLCa = decimal(calcParameters.loanCoverageAmount)  no-error.
     assign deSlCa = decimal(calcParameters.secondloanCoverageAmount)  no-error.
     for each calcOutputParameters where calcOutputParameters.scenarioID = calcParameters.scenarioID :   
       export delimiter "," scenarioid batchid  fileid policyID formIDs calcParameters.region calcParameters.version calcParameters.scenarioName  deCa  deLCa deSlCa
                             ttpremiumOwner ttpremiumOEndors ttpremiumLoan ttpremiumLEndors ttpremiumScnd  ttpremiumSEndors batchPremium
                             (if decimal(ttpremiumOwner) + decimal(ttpremiumLoan) + decimal(ttpremiumScnd) + decimal(ttpremiumOEndors) + decimal(ttpremiumLEndors) + decimal(ttpremiumSEndors)= batchPremium then "yes" else "no" )
                             decimal(ttpremiumOwner) + decimal(ttpremiumLoan) + decimal(ttpremiumScnd) + decimal(ttpremiumOEndors) + decimal(ttpremiumLEndors) + decimal(ttpremiumSEndors).
     end.
  end.
end.
output close.

output to value("C:\antic\manual\batchRateEvalSummary" + string(pPeriodID) + ".csv") .

  export delimiter "," "Version" "BatchID" "AgentID" "FileID"  "PolicIDList " "Region"  
                        "Coverage(OwnerLiabilty)"  "LoanCoverage(LoanLiability)" "ScndLoanCoverage(Loan2Liability)"
                        "ScenarioMatched" "PremiumMatched" "BatchPremium"
                        "MinCalculatedPremium" "MaxCalculatedPremium" "PremiumWitnInRange" .
                        
  for each  batchProcessed  break  by batchProcessed.version by batchProcessed.batchid by batchProcessed.fileid :
   if first-of(batchProcessed.fileid) 
    then
     do:
        lPremiumMatched = no.
        cScenarioNameMatched = "".
        minScenarioPremium = 9999999999999999.
        totalFilepremium = 0.
        maxScenarioPremium  = 0.
        deCa  = 0.
        deLCa = 0.
        deSlCa = 0.
        
         
         filesummary[1] = batchProcessed.version.  
         filesummary[2] = string(batchProcessed.batchid).
         filesummary[3] = batchProcessed.agentid.
         filesummary[4] = batchProcessed.fileid.
         filesummary[5] = batchProcessed.policyid.
        
        for first calcParameters where calcParameters.scenarioID = batchProcessed.scenarioID:
          assign  deCa = decimal(calcParameters.coverag)  no-error.
          assign deLCa = decimal(calcParameters.loanCoverageAmount)  no-error.
          assign deSlCa = decimal(calcParameters.secondloanCoverageAmount)  no-error.
          
          filesummary[6] = calcParameters.region.
          
          filesummary[7] = string(deCa).
          filesummary[8] = string(deLCa).
          filesummary[9] = string(deSlCa).
         end.
      
      end.
           ownerPremium  = 0.
     loanPremium  = 0.
     scndPremium  = 0.
     ownerEndorsPremium  = 0.
     loanEndorsPremium  = 0.
     scndEndorsremium  = 0.
     
      for first calcOutputParameters where calcOutputParameters.scenarioID = batchProcessed.scenarioID:
         assign ownerPremium = decimal(calcOutputParameters.ttpremiumOwner)  no-error.
         assign      loanPremium  = decimal(calcOutputParameters.ttpremiumLoan)  no-error.
          assign     scndPremium  = decimal(calcOutputParameters.ttpremiumScnd)  no-error.
               assign ownerEndorsPremium  = decimal(calcOutputParameters.ttpremiumOEndors)  no-error.
            assign   loanEndorsPremium  = decimal(calcOutputParameters.ttpremiumLEndors) no-error.
            assign   scndEndorsremium  = decimal(calcOutputParameters.ttpremiumSEndors) no-error.
          totalFilepremium = ownerPremium + loanPremium + scndPremium + ownerEndorsPremium + loanEndorsPremium + scndEndorsremium.
          if totalFilepremium = batchProcessed.batchPremium 
           then
            assign
               lPremiumMatched = yes 
               cScenarioNameMatched = cScenarioNameMatched + "|" + calcOutputParameters.scenarioname
               no-error.
  
   
           if (minScenarioPremium > totalFilepremium)   and totalFilepremium <> 0
            then
             minScenarioPremium =  totalFilepremium.
             
           if maxScenarioPremium < totalFilepremium 
            then
             maxScenarioPremium =  totalFilepremium.
     end.
      if last-of(fileid) 
      then
      do:
          filesummary[10] = cScenarioNameMatched.
          filesummary[11] = string(lPremiumMatched) .
          filesummary[12] = string(batchProcessed.batchPremium).
          filesummary[13] = string(minScenarioPremium)  .
          filesummary[14] =  string(maxScenarioPremium).
          filesummary[15] = string(if (batchProcessed.batchPremium >= minScenarioPremium and batchProcessed.batchPremium <= maxScenarioPremium ) then yes else no ).

      export delimiter "," filesummary.
      end.
  end.
  
output close.

 

/*-----------------------creating dynamic temptable---------------------------------*/

 
 

 create temp-table ttdataReport.
 

 ttdataReport:add-new-field("scenarioID", "int"). 
 ttdataReport:add-new-field("batchID", "int"). 
 ttdataReport:add-new-field("FileID", "char"). 
 ttdataReport:add-new-field("PolicyID", "char"). 
 ttdataReport:add-new-field("Region", "char").
 ttdataReport:add-new-field("Version", "char").
 ttdataReport:add-new-field("batchPremium", "decimal"). 
 ttdataReport:add-new-field("premiumMatched", "logical"). 
 ttdataReport:add-new-field("comments", "char").
 ttdataReport:add-new-field("formIDs", "char").
 ttdataReport:add-new-field("matchedScenarioNameList", "char").
 ttdataReport:add-new-field("MinPremium", "decimal").
 ttdataReport:add-new-field("MaxPremium", "decimal").
 ttdataReport:add-new-field("withInRange", "logical").


  for each calcOutputParameters break by calcOutputParameters.scenarioName:
   if last-of (calcOutputParameters.scenarioName) then
   do:
      ttdataReport:add-new-field(calcOutputParameters.scenarioName, "character"). 
   end.
 end.

 ttdataReport:temp-table-prepare("batchevalreport").
 bfdatareport = ttdataReport:default-buffer-handle.
 
 bfBatchProcessed = buffer batchProcessed:handle.
 
 
 for each batchProcessed  break by batchProcessed.batchid by batchProcessed.version by batchProcessed.fileid :
 if first-of(batchProcessed.version) then
 do:
    totalfilepremium = 0.
    cScenarioMatched = "". 
    minScenarioPremium = 9999999999999999.
    maxScenarioPremium = 0.
 end.
     
   if first-of(fileid) 
    then
     do:
        
        
        bfdatareport:buffer-create.
        bfdatareport:buffer-field("scenarioID"):buffer-value()     = batchProcessed.scenarioID. 
        bfdatareport:buffer-field("batchID"):buffer-value()        = batchProcessed.batchID.
        bfdatareport:buffer-field("FileID"):buffer-value()         = batchProcessed.FileID.
        bfdatareport:buffer-field("PolicyID"):buffer-value()       = batchProcessed.PolicyID.
        bfdatareport:buffer-field("formIDs"):buffer-value()        = batchProcessed.formIDs.
        bfdatareport:buffer-field("comments"):buffer-value()       = batchProcessed.comments.
        bfdatareport:buffer-field("premiumMatched"):buffer-value() = batchProcessed.premiumMatched. 
        bfdatareport:buffer-field("Version"):buffer-value() = batchProcessed.Version. 

        
        for first calcParameters where calcParameters.scenarioID = batchProcessed.scenarioID: 
          bfdatareport:buffer-field("Region"):buffer-value()       = calcParameters.region. 
          bfdatareport:buffer-field("Version"):buffer-value()      = calcParameters.version.
        end.
     end.
     
     ownerPremium  = 0.
     loanPremium  = 0.
     scndPremium  = 0.
     ownerEndorsPremium  = 0.
     loanEndorsPremium  = 0.
     scndEndorsremium  = 0.
               
     for first calcOutputParameters where calcOutputParameters.scenarioID = batchProcessed.scenarioID:
         assign
               ownerPremium = decimal(calcOutputParameters.ttpremiumOwner)
               loanPremium  = decimal(calcOutputParameters.ttpremiumLoan)
               scndPremium  = decimal(calcOutputParameters.ttpremiumScnd)
               ownerEndorsPremium  = decimal(calcOutputParameters.ttpremiumOEndors)
               loanEndorsPremium  = decimal(calcOutputParameters.ttpremiumLEndors)
               scndEndorsremium  = decimal(calcOutputParameters.ttpremiumSEndors)
               totalFilepremium = ownerPremium +  loanPremium + scndPremium + ownerEndorsPremium + loanEndorsPremium + scndEndorsremium
               no-error.
         bfdatareport:buffer-field(calcOutputParameters.scenarioname):buffer-value() = string(totalFilepremium). 
      
          if totalFilepremium = batchProcessed.batchPremium 
           then
            assign
               bfdatareport:buffer-field("premiumMatched"):buffer-value() = yes 
               cScenarioMatched = cScenarioMatched + "," + calcOutputParameters.scenarioname
               no-error.
  
   
           if (minScenarioPremium > totalFilepremium)   and totalFilepremium <> 0 
            then
             minScenarioPremium =  totalFilepremium.
             
           if maxScenarioPremium < totalFilepremium 
            then
             maxScenarioPremium =  totalFilepremium.
     end.
     
     if last-of(fileid) 
      then
       assign
         bfdatareport:buffer-field("batchPremium"):buffer-value() = string(batchProcessed.batchPremium)
         bfdatareport:buffer-field("MinPremium"):buffer-value() = minScenarioPremium
         bfdatareport:buffer-field("MaxPremium"):buffer-value() = maxScenarioPremium
         bfdatareport:buffer-field("withInRange"):buffer-value() =(if (batchProcessed.batchPremium > minScenarioPremium and batchProcessed.batchPremium < maxScenarioPremium ) then yes else no )
         bfdatareport:buffer-field("matchedScenarioNameList"):buffer-value() = cScenarioMatched
         no-error.

 end.
 
  create query qBatchProcessed.
  qBatchProcessed:set-buffers(bfdatareport).
  qBatchProcessed:query-prepare("for each batchevalreport").
  qBatchProcessed:query-open().
  output to value("C:\antic\manual\batchdata" + string(pPeriodID) + ".csv").
  do iCount = 1 to bfdatareport:num-fields:
      fiBatchProcessed = bfdatareport:buffer-field(iCount).
      fielddata[iCount] = fiBatchProcessed:name.
  end.
  export delimiter "," fieldData.
  repeat:
    qBatchProcessed:get-next().
    if qBatchProcessed:query-off-end then leave.
    do iCount = 1 to bfdatareport:num-fields:
      fiBatchProcessed = bfdatareport:buffer-field(iCount).
      fielddata[iCount] = string(fiBatchProcessed:buffer-value()).
    end.
      export delimiter "," fieldData.
  end.
   output close.
   
PROCEDURE startElement :

  {lib/get-temp-table.i &t=calcParameters}
  {lib/get-temp-table.i &t=calcOutputParameters}
  {lib/get-temp-table.i &t=batchProcessed}
    {lib/get-temp-table.i &t=batchreject}


  
END PROCEDURE.





