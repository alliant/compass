&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name Compass
@action 
@description gets agents npr olap details

@return agentnpr details

@author Sagar K
@version 1.0
@created 19-04-2023
@notes 
---------------------------------------------------------------------*/


{tt/nprdetailsolap.i}
define output parameter table for nprdetailsolap.
def output parameter piMonth as int no-undo.
def output parameter piYear as int no-undo.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 run httpget.p 
   (input "",
    input "AgentNPROLAPGet",
    input "",
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
   

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).


 if not pSuccess
  then empty temp-table nprdetailsolap.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=nprdetailsolap}
   if qName = "parameter"
   then
    DO  i = 1 TO  attributes:NUM-ITEMS:
      fldvalue = attributes:get-value-by-index(i).
      CASE attributes:get-qname-by-index(i):
       when "Month" then piMonth = int(fldvalue) no-error.
       when "Year"  then piYear  = int(fldvalue) no-error.
      end case.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

