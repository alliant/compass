&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getaudit.p
@description Make a server call to get the audit XML file

@param AuditID;int;The identifier to retrieve

@author John Oliver
@version 1.0
@created 11/20/2015
 Modification:
   Date          Name      Description
   03/21/2017    AC        Modified xml parsing.
   04/27/2017    AG        Updated the score field. 
   08/09/2017    AG        Changes related to sectionID
----------------------------------------------------------------------*/
{tt/qaraudit.i}
{tt/qarauditsection.i}
{tt/qarauditquestion.i}
{tt/qarauditfile.i}
{tt/qarauditfinding.i}
{tt/qarauditaction.i}
{tt/qarauditbp.i}
{tt/qarbkgquestion.i}
{tt/qaraccount.i}

def input parameter piAuditID as int no-undo.
def output parameter table for audit.
def output parameter table for section.
def output parameter table for question.
def output parameter table for agentFile.
def output parameter table for finding.
def output parameter table for action.
def output parameter table for bestPractice.
def output parameter table for bkgQuestion.
def output parameter table for account.

{lib/srvdefs.i}


/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if piAuditID = 0 or piAuditID = ?
 then
  do: pSuccess = false.
      pMsg = "Audit ID cannot be zero or unknown".
      return.
  end.

run httpget.p 
   ("",
    "auditGet",
    "auditID=" + encodeUrl(string(piAuditID)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
{lib/srvstartelement.i}

  case qName:
    when "audit"
     then
      do:
        create audit.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then audit.qarID = int(fldvalue).
            when "agentID" then audit.agentID = decodeXml(fldvalue).
            when "name" then audit.name = decodeXml(fldvalue).
            when "addr" then audit.addr = decodeXml(fldvalue).
            when "city" then audit.city = decodeXml(fldvalue).
            when "state" then audit.state = decodeXml(fldvalue).
            when "zip" then audit.zip = decodeXml(fldvalue).
            when "parentName" then audit.parentName = decodeXml(fldvalue).
            when "parentAddr" then audit.parentAddr = decodeXml(fldvalue).
            when "parentCity" then audit.parentCity = decodeXml(fldvalue).
            when "parentState" then audit.parentState = decodeXml(fldvalue).
            when "parentZip" then audit.parentZip = decodeXml(fldvalue).
            when "contactName" then audit.contactName = decodeXml(fldvalue).
            when "contactPhone" then audit.contactPhone = decodeXml(fldvalue).
            when "contactFax" then audit.contactFax = decodeXml(fldvalue).
            when "contactOther" then audit.contactOther = decodeXml(fldvalue).
            when "contactEmail" then audit.contactEmail = decodeXml(fldvalue).
            when "contactPosition" then audit.contactPosition = decodeXml(fldvalue).
            when "auditDate" then audit.auditDate = datetime(fldvalue).
            when "auditscore" then audit.auditScore = integer(fldvalue).
            when "policyType" then audit.policyType = decodeXml(fldvalue).
            when "comments" then audit.comments = decodeXml(fldvalue).
            when "mainOffice" then audit.mainOffice = logical(fldvalue).
            when "numOffices" then audit.numOffices = integer(fldvalue).
            when "numEmployees" then audit.numEmployees = integer(fldvalue).
            when "numUnderwriters" then audit.numUnderwriters = integer(fldvalue).
            when "lastRevenue" then audit.lastRevenue = decimal(fldvalue).
            when "ranking" then audit.ranking = integer(fldvalue).
            when "auditor" then audit.auditor = decodeXml(fldvalue).
            when "deliveredTo" then audit.deliveredTo = decodeXml(fldvalue).
            when "services" then audit.services = decodeXml(fldvalue).
            when "escrowReviewer" then audit.escrowReviewer = decodeXml(fldvalue).
            when "escrowReviewDate" then audit.escrowReviewDate = datetime(fldvalue).
            when "stat" then audit.stat = decodeXml(fldvalue).
	    when "onsiteStartDate" then audit.onsiteStartDate = datetime(fldvalue).
            when "onsiteFinishDate" then audit.onsiteFinishDate = datetime(fldvalue).
          END CASE.
        end.
      end.
    when "section"
     then
      do:
        create section.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then section.qarID = int(fldvalue).
            when "sectionID" then section.sectionID = integer(fldvalue).
            when "description" then section.description = decodeXml(fldvalue).
            when "sectionScore" then section.sectionScore = integer(fldvalue).
            when "comments" then section.comments = decodeXml(fldvalue).
            when "weight" then section.weight = decimal(fldvalue).
            when "files" then section.files = logical(fldvalue).
          end case.
        end.
      end.
    when "question"
     then
      do:
        create question.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then question.qarID = int(fldvalue).
            when "seq" then question.seq = integer(fldvalue).
            when "questionID" then question.questionID = fldvalue.
            when "fileID" then question.fileID = integer(fldvalue).
            when "fileNumber" then question.fileNumber = decodeXml(fldvalue).
            when "description" then question.description = decodeXml(fldvalue).
            when "priority" then question.priority = integer(fldvalue).
            when "answer" then question.answer = decodeXml(fldvalue).
            when "notes" then question.notes = logical(fldvalue).
            when "uID" then question.uID = decodeXml(fldvalue).
            when "dateAnswered" then question.dateAnswered = datetime(fldvalue).
          end case.
        end.
      end.
    when "finding"
     then
      do:
        create finding.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then finding.qarID = int(fldvalue).
            when "findingID" then finding.findingID = integer(fldvalue).
            when "questionSeq" then finding.questionSeq = integer(fldvalue).
            when "comments" then finding.comments = decodeXml(fldvalue).
            when "reference" then finding.reference = decodeXml(fldvalue).
            when "files" then finding.files = decodeXml(fldvalue).
            when "priority" then finding.priority = integer(fldvalue).
            when "accounts" then finding.accounts = decodeXml(fldvalue) .
          end case.
        end.
      end.
    when "agentFile"
     then
      do:
        create agentFile.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then agentFile.qarID = int(fldvalue).
            when "fileID" then agentFile.fileID = integer(fldvalue).
            when "answered" then agentFile.answered = integer(fldvalue).
            when "fileNumber" then agentFile.fileNumber = decodeXml(fldvalue).
            when "state" then agentFile.state = decodeXml(fldvalue).
            when "county" then agentFile.county = decodeXml(fldvalue).
            when "ownerLiability" then agentFile.ownerLiability = decimal(fldvalue).
            when "ownerPolicy" then agentFile.ownerPolicy = integer(fldvalue).
            when "lender1Liability" then agentFile.lender1Liability = decimal(fldvalue).
            when "lender1Policy" then agentFile.lender1Policy = integer(fldvalue).
            when "lender2Liability" then agentFile.lender2Liability = decimal(fldvalue).
            when "lender2Policy" then agentFile.lender2Policy = integer(fldvalue).
            when "premiumStatement" then agentFile.premiumStatement = decimal(fldvalue).
            when "rate" then agentFile.rate = decimal(fldvalue).
            when "delta" then agentFile.delta = decimal(fldvalue).
            when "originalDate" then agentFile.originalDate = datetime(fldvalue).
            when "preliminaryDate" then agentFile.preliminaryDate = datetime(fldvalue).
            when "updateDate" then agentFile.updateDate = datetime(fldvalue).
            when "finalDate" then agentFile.finalDate = datetime(fldvalue).
            when "closingDate" then agentFile.closingDate = datetime(fldvalue).
            when "fundingDate" then agentFile.fundingDate = datetime(fldvalue).
            when "recordingDate" then agentFile.recordingDate = datetime(fldvalue).
            when "statutoryGapLimit" then AgentFile.statutoryGapLimit = integer(fldvalue).
            when "ownerDeliveryDate" then agentFile.ownerDeliveryDate = datetime(fldvalue).
            when "ownerDeliveryGapBusiness" then agentFile.ownerDeliveryGapBusiness = integer(fldvalue).
            when "ownerDeliveryGapCalendar" then agentFile.ownerDeliveryGapCalendar = integer(fldvalue).
            when "lender1DeliveryDate" then agentFile.lender1DeliveryDate = datetime(fldvalue).
            when "lender1DeliveryGapBusiness" then agentFile.lender1DeliveryGapBusiness = integer(fldvalue).
            when "lender1DeliveryGapCalendar" then agentFile.lender1DeliveryGapCalendar = integer(fldvalue).
            when "lender2DeliveryDate" then agentFile.lender2DeliveryDate = datetime(fldvalue).
            when "lender2DeliveryGapBusiness" then agentFile.lender2DeliveryGapBusiness = integer(fldvalue).
            when "lender2DeliveryGapCalendar" then agentFile.lender2DeliveryGapCalendar = integer(fldvalue).
            /*when "bringToDateGapCalendar" then agentFile.bringToDateGapCalendar = integer(fldvalue).
            when "bringToDateGapBusiness" then agentFile.bringToDateGapBusiness = integer(fldvalue).*/
            when "searchGapCalendar " then agentFile.searchGapCalendar = integer(fldvalue).
            when "searchGapBusiness " then agentFile.searchGapBusiness = integer(fldvalue).
            when "fundingGapCalendar" then agentFile.fundingGapCalendar = integer(fldvalue).
            when "fundingGapBusiness" then agentFile.fundingGapBusiness = integer(fldvalue).
            when "recordingGapCalendar" then agentFile.recordingGapCalendar = integer(fldvalue).
            when "recordingGapBusiness" then agentFile.recordingGapBusiness = integer(fldvalue).
          end case.
        end.
      end.
    when "bkgQuestion"
     then
      do:
        create bkgQuestion.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then bkgQuestion.qarID = int(fldvalue).
            when "seq" then bkgQuestion.seq = integer(fldvalue).
            when "questionID" then bkgQuestion.questionID = decodeXml(fldvalue).
            when "description" then bkgQuestion.description = decodeXml(fldvalue).
            when "answer" then bkgQuestion.answer = decodeXml(fldvalue).
            when "uID" then bkgQuestion.uID = decodeXml(fldvalue).
            when "dateAnswered" then bkgQuestion.dateAnswered = datetime(fldvalue).
          end case.
        end.
      end.
    when "action"
     then
      do:
        create action.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then action.qarID = int(fldvalue).
            when "actionID" then action.actionID = integer(fldvalue) no-error.
            when "questionSeq" then action.questionSeq = integer(fldvalue) no-error.
            when "description" then action.description = decodeXml(fldvalue).
            when "dueDate" then action.dueDate = datetime(fldvalue) no-error.
            when "stat" then action.stat = decodeXml(fldvalue).
            when "note" then action.note = decodeXml(fldvalue).
          end case.
        end.
        release action.
      end.
    when "bestPractice"
     then
      do:
        create bestPractice.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then bestPractice.qarID = int(fldvalue).
            when "bpID" then bestPractice.bpID = integer(fldvalue) no-error.
            when "questionSeq" then bestPractice.questionSeq = integer(fldvalue) no-error.
            when "comments" then bestPractice.comments = decodeXml(fldvalue).
          end case.
        end.
        release bestPractice.
      end.
     when "account"
     then
      do:
        create account.
        do i = 1 to attributes:num-items:
            fldvalue = attributes:get-value-by-index(i).
          CASE attributes:get-qname-by-index(i):
            when "qarID" then account.qarID = int(fldvalue).
            when "comments" then account.comments = decodeXml(fldvalue).
            when "trialBal" then account.trialBal = decimal(fldvalue).
            when "stmtBal" then account.stmtBal = decimal(fldvalue).
            when "reconciled" then account.reconciled = logical(fldvalue).
            when "reconTrialBal" then account.reconTrialBal = decimal(fldvalue).
            when "reconDate" then account.reconDate = datetime(fldvalue).
            when "reconCurrent" then account.reconCurrent = logical(fldvalue).
            when "reconCheckbookBal" then account.reconCheckbookBal = decimal(fldvalue).
            when "reconAdjBankBal" then account.reconAdjBankBal = decimal(fldvalue).
            when "outChecksBal" then account.outChecksBal = decimal(fldvalue).
            when "netNegAdj" then account.netNegAdj = decimal(fldvalue).
            when "netAdj" then account.netAdj = decimal(fldvalue).
            when "dipBal" then account.dipBal = decimal(fldvalue).
            when "calcBal" then account.calcBal = decimal(fldvalue).
            when "bankName" then account.bankName = decodeXml(fldvalue).
            when "bankID" then account.bankID = integer(fldvalue).
            when "actualCheckbookBal" then account.actualCheckbookBal = decimal(fldvalue).
            when "acctTitle" then account.acctTitle = decodeXml(fldvalue).
            when "acctNumber" then account.acctNumber = decodeXml(fldvalue).
          end case.
        end.
      end.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

