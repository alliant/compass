&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getcompliancenotes.p
@description Make a server call to get the comnotes data

@param entityID;char;The attorney ID to retrieve
@param entity;char;The entity to retrieve
@author Gurvindar
@version 1.0
@created 08/30/2018
@modified
date           Name          Description
10/29/2018    Sachin         Modified file name getcomnotes.p to getcompliancenotes.p
                             and changed action name        
----------------------------------------------------------------------*/

{tt/comnote.i}
define input  parameter ipcEntity   as  character no-undo.
define input  parameter ipcEntityID as  character no-undo.
define input  parameter ipiReqID    as  integer   no-undo.
define output parameter table       for comnote.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p ("",
               "compliancenotesGet",
               (if ipcEntity   <> ?  and ipcEntity   <> ""  then "Entity="        + trim(encodeUrl(ipcEntity))         else "Entity=") +
               (if ipcEntityID <> ?  and ipcEntityID <> ""  then "&EntityID="     + trim(encodeUrl(ipcEntityID))       else "&EntityID=") +
               (if ipiReqID    <> ?  and ipiReqID    <> 0   then "&Requirement="  + trim(encodeUrl(string(ipiReqID)))  else "&Requirement="),
               output pSuccess,
               output pMsg,
               output tResponseFile).


if not pSuccess 
 then
  return.
 
pSuccess = parseXML(output pMsg).

if not pSuccess 
 then
  empty temp-table comnote.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
  if qName = "comnote" 
   then
    do: 
      create comnote.
      do  i = 1 to  attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "entity"     then comnote.entity     = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "entityID"   then comnote.entityID   = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "reqID"      then comnote.reqID      = if integer(fldvalue) = ?            then 0            else integer(fldvalue)  no-error.
          when "seq"        then comnote.seq        = if integer(fldvalue) = ?            then 0            else integer(fldvalue)  no-error.
          when "stateID"    then comnote.stateID    = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "notetype"   then comnote.notetype   = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "subject"    then comnote.subject    = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "notes"      then comnote.notes      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "notedate"   then comnote.notedate   = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error.       
          when "UID"        then comnote.UID        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "username"   then comnote.username   = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        end case.
      end.
      release comnote.
    end. 
     
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

