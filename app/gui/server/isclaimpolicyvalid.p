&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/isclaimpolicyvalid.p (copied from canprocesspolicy.p)
   Determine if the policy is valid for a claim
   @author D.Sinclair / B. Johnson
   @created 1.5.2017
 */


def input parameter pClaimID as int no-undo.
def input parameter pPolicyID as char no-undo.
def input parameter pAgentID as char no-undo.
def output parameter pEffDate as datetime no-undo.
def output parameter pLiabilityAmount as decimal no-undo.
def output parameter pGrossPremium as decimal no-undo.
def output parameter pFormID as char no-undo.
def output parameter pStateID as char no-undo.
def output parameter pFileNumber as char no-undo.
def output parameter pStatCode as char no-undo.
def output parameter pNetPremium as dec no-undo.
def output parameter pRetention as dec no-undo.
def output parameter pCountyID as char no-undo.
def output parameter pResidential as log no-undo.
def output parameter pInvoiceDate as date no-undo.
def output parameter pStat as char no-undo.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

pStat = "E".

if pPolicyID = "" or pPolicyID = ?
 then
  do: pSuccess = false.
      pMsg = "Policy ID is blank or unknown.".
      return.
  end.

/*if pAgentID = "" or pAgentID = ?
 then
  do: pSuccess = true.
      pMsg = "Agent ID is blank or unknown.".
      return.
  end.*/

 run httpget.p 
   ("",
    "claimPolicyIsValid",
    "claimID=" + string(pClaimID)
    + "&policyID=" + encodeUrl(pPolicyID)
    + "&agentID=" + encodeUrl(pAgentID),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.

 pSuccess = parseXML(output pMsg). /* pMsg will contain parseMsg if fault */

/*  /* if we're able to parse, pStat = code */                             */
/*  case pStat:                                                            */
/*   when "2001" then pStat = "S".                                         */
/*   when "3067" then pStat = "W". /* Policy does not exist */             */
/*   when "3080" then pStat = "W". /* Policy has already been processed */ */
/*   otherwise pStat = "E". /* Any number of failures */                   */
/*  end case.                                                              */

 if pStat = "2001" then
 assign pStat = "S".
 else
 case parseCode:
  when "3028" or                /* Agent ID does not match on Policy */
  when "3066" or                /* Policy is blank or non-numeric */
  when "3067" or                /* Policy does not exist */
  when "3080" then pStat = "W". /* Policy has already been processed */
  otherwise pStat = "E". /* Any number of failures */
 end case.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
/* The handling of 'Fault' is done in lib/srvstartelement.i and it
   returns upon completion, so this code will never execute. */
/*  if qName = "Fault"                                    */
/*   then                                                 */
/*     do:                                                */
/*         DO  i = 1 TO  attributes:NUM-ITEMS:            */
/*          fldvalue = attributes:get-value-by-index(i).  */
/*          CASE attributes:get-qname-by-index(i):        */
/*           when "code" THEN pStat = fldvalue.           */
/*          END CASE.                                     */
/*         END.                                           */
/*     end.                                               */
/*   else                                                 */
 if qName = "Success" 
  then pStat = "2001".
  else
 if qName = "Parameter"
  then
   do:
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "EffDate" THEN pEffDate = date(fldvalue) no-error.
          when "LiabilityAmount" then pLiabilityAmount = decimal(fldvalue) no-error.
          when "GrossPremium" then pGrossPremium = decimal(fldvalue) no-error.
          when "FormID" then pFormID = fldvalue no-error.
          when "StateID" then pStateID = fldvalue no-error.
          when "FileNumber" then pFileNumber = fldvalue no-error.
          when "StatCode" then pStatCode = fldvalue no-error.
          when "NetPremium" then pNetPremium = decimal(fldvalue) no-error.
          when "Retention" then pRetention = decimal(fldvalue) no-error.
          when "CountyID" then pCountyID = fldvalue no-error.
          when "Residential" then pResidential = logical(fldvalue) no-error.
          when "InvoiceDate" then pInvoiceDate = date(fldvalue) no-error.
         END CASE.
        END.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

