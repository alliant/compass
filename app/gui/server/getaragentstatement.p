&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getaragentstatement.p
@description Makes a server call to get the agent statement report.

@param AsOfDate;datetime;The "as of date" for the report
@param AgentID;character;The agent identifier
@returns The table dataset

@author John Oliver
@version 1.0
@created 05.24.2017
----------------------------------------------------------------------*/

{tt/reportagentstatement.i}
define input parameter pAsOfDate as datetime no-undo.
define input parameter pAgentID as character no-undo.
define output parameter table for reportagentstatement.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
    
run httpget.p 
   ("",
    "arAgentStatementGet",
    "AsOfDate=" + encodeUrl(string(pAsOfDate)) +
    "&AgentName=" + encodeUrl(pAgentID),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

if not pSuccess
 then empty temp-table reportagentstatement.


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  define buffer ttReport for reportagentstatement.

  if qName = "Report"
   then
     do: 
      create ttReport.
      do  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
         when "claimID" then ttReport.claimID = integer(fldvalue) no-error.
         when "policyID" then ttReport.policyID = integer(fldvalue) no-error.
         when "fileNumber" then ttReport.fileNumber = fldvalue.
         when "category" then ttReport.category = fldvalue.
         when "stat" then ttReport.stat = fldvalue.
         when "dateClosed" then ttReport.dateClosed = datetime(fldvalue) no-error.
         when "stateID" then ttReport.stateID = fldvalue.
         when "laePaid" then ttReport.laePaid = decimal(fldvalue) no-error.
         when "lossPaid" then ttReport.lossPaid = decimal(fldvalue) no-error.
         when "totalPaid" then ttReport.totalPaid = decimal(fldvalue) no-error.
         when "deductible" then ttReport.deductible = decimal(fldvalue) no-error.
         when "unadjusted" then ttReport.unadjusted = decimal(fldvalue) no-error.
         when "billable" then ttReport.billable = decimal(fldvalue) no-error.
         when "billedAmount" then ttReport.billedAmount = decimal(fldvalue) no-error.
         when "billedDate" then ttReport.billedDate = datetime(fldvalue) no-error.
         when "invoiceNumber" then ttReport.invoiceNumber = fldvalue.
         when "paidAmount" then ttReport.paidAmount = decimal(fldvalue) no-error.
         when "paidDate" then ttReport.paidDate = datetime(fldvalue) no-error.
         when "age" then ttReport.age = integer(fldvalue) no-error.
         when "deMiniums" then ttReport.deMiniums = decimal(fldvalue) no-error.
         when "waivedAmount" then ttReport.waivedAmount = decimal(fldvalue) no-error.
        end case.
      end.
      release ttReport.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

