&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file buildlist.p
@description Makes a server call to build the dynamic list

@param Entity;char;The entity for the list
@param Action;char;The action for the server call
@param UserField;complex;The fields the user wants to show
@param UserFilter;complex;The filters the user has
@param ListField;The omplete list of fields for the entity

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/listfield.i}
{tt/listfield.i &tableAlias="userfield"}
{tt/listfilter.i &tableAlias="userfilter"}
define input parameter pEntity as character.
define input parameter pAction as character.
define input parameter table for userfield.
define input parameter table for userfilter.
define input parameter table for listfield.
define output parameter hTable as handle.
{lib/srvdefs.i}
{lib/add-delimiter.i}
{lib/build-table.i}

define variable hBuf as handle no-undo.
define variable cFilterList as character no-undo.
define variable cFieldList as character no-undo.
define variable hQuery as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the table must be there */
if pEntity = ? or pEntity = ""
 then
  do: pSuccess = false.
      pMsg = "The table must be present".
      return.
  end.
  
/* get the filter list */
for each userfilter:
  cFilterList = addDelimiter(cFilterList,",") + 
                userfilter.tableName {&msg-add} 
                userfilter.columnName {&msg-add} 
                userfilter.columnOperator + {&msg-dlm}.
  
  for first listfield no-lock
      where listfield.tableName = userfilter.tableName
        and listfield.columnName = userfilter.columnName:
      
    if listfield.dataType = "character"
     then cFilterList = cFilterList + "'" + userfilter.columnValue + "'".
     else cFilterList = cFilterList + userfilter.columnValue.
  end.
end.

/* get the fields */
std-in = 0.
for each userfield:
  cFieldList = addDelimiter(cFieldList,",") + userfield.columnBuffer.
end.

run httpget.p 
   ("",
    pAction,
    "table=" + encodeUrl(pEntity)
    + "&fields=" + encodeUrl(cFieldList)
    + (if cFilterList > "" then "&filters=" + encodeUrl(cFilterList) else ""),
    output pSuccess,
    output pMsg,
    output tResponseFile). 

pSuccess = true.
if not pSuccess 
 then return.

/* we must build the custom temp-table */
create temp-table hTable.
/* we need to add the keyfield column to the dynamic table */
create userfield.
assign
  userfield.entityName = pEntity
  userfield.columnBuffer = "keyfield"
  userfield.columnFormat = "x(8)"
  userfield.columnWidth = 10
  userfield.dataType = "character"
  userfield.queryFunction = "key"
  .
release userfield.
hTable = BuildTable(pEntity, hTable, temp-table userfield:handle).
hBuf = hTable:default-buffer-handle.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  define variable j as int no-undo.

  {lib/srvstartelement.i}
  if qName = "Report"
   then
    do:
      hBuf:buffer-create().
      /* we must loop through the XML attributes */
      do i = 1 to hBuf:num-fields:
        do j = 1 to attributes:num-items:
          if hBuf:buffer-field(i):name = attributes:get-qname-by-index(j)
           then
            do:
              fldvalue = attributes:get-value-by-index(j).
              case hBuf:buffer-field(i):data-type:
               when "character" then hBuf:buffer-field(i):buffer-value() = fldvalue.
               when "integer" then hBuf:buffer-field(i):buffer-value() = integer(fldvalue) no-error.
               when "decimal" then hBuf:buffer-field(i):buffer-value() = decimal(fldvalue) no-error.
               when "logical" then hBuf:buffer-field(i):buffer-value() = logical(fldvalue) no-error.
               when "datetime" then hBuf:buffer-field(i):buffer-value() = datetime(fldvalue) no-error.
              end case.
            end.
        end.
      end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

