&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getagentstatements.p
@description Makes a server call to get the statements for the agent

@param Agent;char;The id of the agent

@returns The temp table for the files of the agent

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/agentinv.i}
def input parameter pPaymentNumber as char no-undo.
def output parameter table for agentinv.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
    
run httpget.p 
   ("",
    "agentAccountingInvoicesGet",
    "PaymentNumber=" + encodeUrl(pPaymentNumber),
    output pSuccess,
    output pMsg,
    output tResponseFile).   
    
if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  def buffer agentinv for agentinv.

  {lib/srvstartelement.i}
  case qName:
   when "Invoice" then
    do:
      create agentinv.
      do i = 1 to  attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
				 when "agentID" then agentinv.agentID = fldvalue.
				 when "invoiceNumber" then agentinv.invoiceNumber = fldvalue.
				 when "paymentNumber" then agentinv.paymentNumber = fldvalue.
				 when "invoiceDate" then agentinv.invoiceDate = datetime(fldvalue) no-error.
				 when "fileNumber" then agentinv.fileNumber = fldvalue.
         when "batchNumber" then agentinv.batchNumber = fldvalue.
				 when "amount" then agentinv.amount = decimal(fldvalue) no-error.
				 when "amountApplied" then agentinv.amountApplied = decimal(fldvalue) no-error.
				 when "amountUnapplied" then agentinv.amountUnapplied = decimal(fldvalue) no-error.
        end case.
      END.
      release agentinv.
    end.
  end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

