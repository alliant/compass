&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name sys/setsysroleactions.p   
@action systemRoleActionsSet
@description Assign/Unassign role to action

@author Rahul Sharma
@paramp ipcRoleID;char;RoleID 
@paramp ipcActionList;char;Action List containing all the actions 
                           which needs to be assigned a role.
@version 1.0
@created 10/09/2018
----------------------------------------------------------------------*/

/* Parameter Definitions */
 define input  parameter ipcRoleID     as character no-undo.
 define input  parameter ipcActionList as character no-undo.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p ("",
               "systemRoleActionsSet",
               (if ipcRoleID     <> ? and ipcRoleID     <> "" then "RoleID="     + encodeUrl(ipcRoleID)     else "RoleID=") +
               (if ipcActionList <> ? and ipcActionList <> "" then "&ActionList=" + encodeUrl(ipcActionList) else "&ActionList="),
               output pSuccess,
               output pMsg,
               output tResponseFile).
               
if not pSuccess 
 then
  return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

