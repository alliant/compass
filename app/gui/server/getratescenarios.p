&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/getratescenarios.p

@description get all rate scenarios for a ratestate
@author Sachin Chaturvedi
@created 10.15.2019
@notes
@param piCardSetID;int;  Pass 0 for all card sets
@returns ratescenario   Temp-table for ratescenario.
*/

/* temp-table definitions */
{tt\ratescenario.i}
{tt\ratescenariocard.i}

/* parameter definitions */
define input  parameter piCardSetID as integer no-undo.
define input  parameter plCardsReq  as logical no-undo.
define output parameter table for ratescenario.
define output parameter table for ratescenariocard.
{lib\srvdefs.i}
{lib\xmlencode.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 5.19
         WIDTH              = 49.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 if  piCardSetID = ? 
  then
   do:
     pSuccess = false.
     pMsg     = "The CardSetID is not valid".
     return.
   end.

 run httpget.p 
   ("",
    "ratescenariosGet",
    "cardSetID=" + encodeUrl(string(piCardSetID)) +  
    "&returnCards=" + encodeUrl(string(plCardsReq)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.

 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 define buffer ratescenario     for ratescenario.
 define buffer ratescenariocard for ratescenariocard.
 
 if qName = "ratescenario"
  then
   do: 
       create ratescenario.
       do  i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "scenarioID"       then ratescenario.scenarioID       = if integer(fldvalue) = ?            then 0        else integer(fldvalue) no-error.
          when "cardSetID"        then ratescenario.cardSetID        = if integer(fldvalue) = ?            then 0        else integer(fldvalue) no-error.
          when "scenario"         then ratescenario.scenario         = if (fldvalue = ? or fldvalue = "?") then ""       else decodeFromXml(fldvalue).
          when "description"      then ratescenario.description      = if (fldvalue = ? or fldvalue = "?") then ""       else decodeFromXml(fldvalue).
          when "region"           then ratescenario.region           = if (fldvalue = ? or fldvalue = "?") then ""       else decodeFromXml(fldvalue).
          when "property"         then ratescenario.property         = if (fldvalue = ? or fldvalue = "?") then ""       else decodeFromXml(fldvalue).
          when "createdDate"      then ratescenario.createdDate      = if date(fldvalue) = ?               then date("") else date(fldvalue) no-error.            
        end case.
       end.
       release ratescenario.
   end.
   else if qName = "ratescenariocard"
    then
     do: 
       create ratescenariocard.
       do  i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "scenarioID"       then ratescenariocard.scenarioID    = if integer(fldvalue) = ?            then 0        else integer(fldvalue) no-error.
          when "cardID"           then ratescenariocard.cardID        = if integer(fldvalue) = ?            then 0        else integer(fldvalue) no-error.
          when "attribute"        then ratescenariocard.attribute     = if (fldvalue = ? or fldvalue = "?") then ""       else decodeFromXml(fldvalue).
          when "cardName"         then ratescenariocard.cardName      = if (fldvalue = ? or fldvalue = "?") then ""       else decodeFromXml(fldvalue).
          when "createdDate"      then ratescenariocard.createdDate   = if date(fldvalue) = ?               then date("") else date(fldvalue) no-error.            
        end case.
       end.
       release ratescenariocard.
     end. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

