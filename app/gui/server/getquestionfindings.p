/*------------------------------------------------------------------------
@name GetQuestionFindings.p
@description Gets a list of the question findings

@param questionSeq;int;The questionSeq of the audit (required)

@returns Audit;complex;The audit findingpareto table

@author sachin anthwal
@version 1.0
@created 10/28/2021

@modified
Date           Name          Description
20/02/2022     SD            Task#91131: Added new parameters to fetch the data accordingly.
29/07/2022     SD            Task#95509: Modified to support clob related changes
02/02/2023     KR            Task#10200: Changed the call of httpcall as per new framework
----------------------------------------------------------------------*/

{tt/qarfinding.i &tableAlias="ttQarfinding" &nodename="qarfinding"}

def input parameter piquestionseq as int no-undo.
def input parameter piyear as char no-undo.
def input parameter piauditor as char no-undo.
def input parameter piFindingType as int  no-undo.
def input parameter piState       as char no-undo.
def input parameter piAgentID     as char no-undo.
def output parameter table for ttQarfinding.

                  
{lib/srvdefs.i}
{lib/rpt-defs.i}
define variable dsHandle as handle no-undo.

create dataset dsHandle.
dsHandle:xml-node-name = 'data'.
dsHandle:set-buffers(buffer ttQarfinding:handle).
                       
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 
   
run util/httpcall.p (input "get",
                     input "DO",
                     input "questionFindingsGet",
                     input "",      /* content type */
                     input "",      /* data file */
                     input "questionSeq=" + encodeUrl(string(piquestionseq)) + 
                     (if piYear <> ? and piYear <> "ALL" then "&Year=" + encodeUrl(piYear) else "") +
                     (if piauditor <> ? and piauditor <> "ALL" then "&Auditor=" + encodeUrl(piauditor) else "") +
                     (if piState <> ? and piState <> "ALL" then "&State=" + encodeUrl(piState) else "") +
                     (if piAgentID <> ? and piAgentID <> "ALL" then "&Agent=" + encodeUrl(piAgentID) else "") +
                     (if piFindingType <> ? and piFindingType <> 0 then "&FindingType=" + encodeUrl(string(piFindingType)) else ""),
                     {lib/rpt-setparams.i},
                     output pSuccess,
                     output pMsg,
                     output clientResponse).

if not pSuccess 
 then return.
 
 if valid-object(clientResponse) then
    tResponseFile = clientResponse:bodyFilePath.
    
pSuccess = parseXML(output pMsg).  


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN
  
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/

 def buffer qarfinding for ttQarfinding.

 {lib/get-temp-table.i &t=qarfinding &setParam="'qarFinding'"}
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
