&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getorganizationrole.p
@description Make a server call to get the person data

@param organizationID;char;The identifier to retrieve

@author Gurvindar
@version 1.0
@created 09/16/2019
@modified
Date         Name           Description

----------------------------------------------------------------------*/

{tt/role.i}
{tt/reqfulfill.i}

define input  parameter ipiOrgRoleID as integer no-undo.
define output parameter table        for role.
define output parameter table        for reqfulfill.

{lib/std-def.i}
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 12.14
         WIDTH              = 73.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p ("",
               "organizationroleGet",
               (if ipiOrgRoleID  <> ?  and ipiOrgRoleID <> 0  then "orgroleID=" + trim(encodeUrl(string(ipiOrgRoleID))) else "orgroleID="),
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess 
 then
  return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

  case qName:     
    when "role" 
     then
      do:
        create role.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          case attributes:get-qname-by-index(i):
            when "roleID"     then role.roleID      = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "name"       then role.name        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "stat"       then role.stat        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "state"      then role.state       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "role"       then role.role        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "entityname" then role.entityname  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "entityID"   then role.entityID    = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            
          end case.
        end.
      end.
   
    when "reqfulfill" 
     then
      do:
        create reqfulfill.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          case attributes:get-qname-by-index(i):
            when "entity"              then reqfulfill.entity              = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "entityID"            then reqfulfill.entityID            = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "entityNPN"           then reqfulfill.entityNPN           = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "StateId"             then reqfulfill.StateId             = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "entityname"          then reqfulfill.entityname          = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "requirementID"       then reqfulfill.requirementID       = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "qualificationID"     then reqfulfill.qualificationID     = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "statereqqualID"      then reqfulfill.statereqqualID      = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "personRoleID"        then reqfulfill.personRoleID        = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "qualreqID"           then reqfulfill.qualreqID           = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "qualEntityId"        then reqfulfill.qualEntityId        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "orgRoleId"           then reqfulfill.orgRoleId           = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "url"                 then reqfulfill.url                 = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "expire"              then reqfulfill.expire              = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "expireMonth"         then reqfulfill.expireMonth         = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "expireDay"           then reqfulfill.expireDay           = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "expireYear"          then reqfulfill.expireYear          = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "expireDays"          then reqfulfill.expireDays          = if integer(fldvalue) = ?            then 0            else integer(fldvalue) no-error.
            when "irefID"              then reqfulfill.irefID              = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "authorizedBy"        then reqfulfill.authorizedBy        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "reqdesc"             then reqfulfill.reqdesc             = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "requirementQual"     then reqfulfill.requirementQual     = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "appliesTo"           then reqfulfill.appliesTo           = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "qualificationNumber" then reqfulfill.qualificationNumber = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "stat"                then reqfulfill.stat                = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "notes"               then reqfulfill.notes               = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "reqMet"              then reqfulfill.reqMet              = if logical(fldvalue) = ?            then no           else logical(fldvalue) no-error.
            when "effectiveDate"       then reqfulfill.effectiveDate       = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error.
            when "expirationDate"      then reqfulfill.expirationDate      = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error.
            when "activ"               then reqfulfill.activ               = if logical(fldvalue) = ?            then no           else logical(fldvalue) no-error.
            when "qualification"       then reqfulfill.qualification       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "role"                then reqfulfill.role                = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "comStat"             then reqfulfill.comStat             = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "description"         then reqfulfill.description         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          end case.
        end.     
      end.
  end case.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

