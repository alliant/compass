&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/getclaimreserveadjs.p
@desc GET CLaiM reserve adjustment data for reporting
@author D.Sinclair
@date 1.4.2017

@param claimID (or 0 for all)
@param Approver (or blank for all)
@param Stat (O)pen, (A)pproved, or blank for all
*/

{tt/clm06.i}

def input parameter pClaimID as int no-undo.
def input parameter pAdmin as char no-undo.
def input parameter pStat as char no-undo.
def output parameter table for clm06.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if pClaimID <> 0 
 then std-ch = "claim=" + string(pClaimID).

if pAdmin = "ALL" 
 then pAdmin = "".
if pAdmin = ? 
 then pAdmin = "".

if pAdmin > "" 
 then std-ch = std-ch + "&approver=" + encodeUrl(pAdmin).

if pStat > "" 
 then std-ch = std-ch + "&stat=" + encodeUrl(pStat).

 run httpget.p 
   ("",
    "claimReserveAdjustmentGet",
    "claim=" + encodeUrl(string(pClaimID)) + 
    "&approver=" + encodeUrl(pAdmin) +
    "&stat=" + encodeUrl(pStat),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer clm06 for clm06.
 
 if qName = "data"
  then
    do: create clm06.

        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             when "claimID" THEN clm06.claimID = int(fldvalue) no-error.
             when "stateID" then clm06.stateID = fldvalue.
             when "refCategory" THEN clm06.refCategory = fldvalue.
             when "stat" THEN clm06.stat = fldvalue no-error.

             when "dateRequested" THEN clm06.dateRequested = date(fldvalue) no-error.
             when "requestedAmount" THEN clm06.requestedAmount = decimal(fldvalue) no-error.
             when "requestedBy" THEN clm06.requestedBy = decodeXml(fldvalue) no-error.

             when "uid" THEN clm06.uid = decodeXml(fldvalue).
             when "notes" THEN clm06.notes = decodeXml(fldvalue) no-error.
         END CASE.
        END.
        release clm06.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

