&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getclaimlitigation.p
@description Makes a call to the server to get the claim litigation dataset

@param ClaimID;int;Claim number
@param LitigationID;char;Litigation ID (Optional)

@returns ClaimLitigation;dataset;The claim litigation dataset containing one or all claim litigations

@success 2005;The number of claim litigations found

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/claimlitigation.i}

def input parameter pClaimID as int no-undo.
def input parameter pLitigationID as int no-undo.
def output parameter table for claimlitigation.
{lib/srvdefs.i}

def var sQuery as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the claim ID must be there */
 if pClaimID = ? or pClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "Claim ID cannot be zero or unknown".
        return.
    end.
 sQuery = "ClaimID=" + encodeURL(string(pClaimID)).
 
 /* if there is a litigation ID, then add it to the querystring */
 if pLitigationID <> ? and pLitigationID <> 0
  then sQuery = sQuery + "&LitigationID=" + encodeUrl(string(pLitigationID)).
    
 run httpget.p 
   ("",
    "claimLitigationGet",
    sQuery,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table claimlitigation.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer claimlitigation for claimlitigation.

 if qName = "ClaimLitigation"
  then
    do: create claimlitigation.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
						when "litigationID" then claimlitigation.litigationID = integer(fldvalue) no-error.
						when "stat" then claimlitigation.stat = fldvalue.
						when "claimID" then claimlitigation.claimID = integer(fldvalue) no-error.
						when "caseNumber" then claimlitigation.caseNumber = fldvalue.
						when "dateFiled" then claimlitigation.dateFiled = datetime(fldvalue) no-error.
						when "courtName" then claimlitigation.courtName = fldvalue.
						when "addr1" then claimlitigation.addr1 = fldvalue.
						when "addr2" then claimlitigation.addr2 = fldvalue.
						when "city" then claimlitigation.city = fldvalue.
						when "stateID" then claimlitigation.stateID = fldvalue.
						when "zipcode" then claimlitigation.zipcode = fldvalue.
						when "judge" then claimlitigation.judge = fldvalue.
						when "dateServed" then claimlitigation.dateServed = datetime(fldvalue) no-error.
						when "isParty" then claimlitigation.isParty = logical(fldvalue) no-error.
						when "plaintiff" then claimlitigation.plaintiff = fldvalue.
						when "defendant" then claimlitigation.defendant = fldvalue.
						when "notes" then claimlitigation.notes = decodeXml(fldvalue).
         END CASE.
        END.
        release claimlitigation.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

