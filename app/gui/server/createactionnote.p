&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name newnote.p
@description Creates/Modifies an actionnote record in the actionnote table

@param ActionID;int;The ActionID for which new ActionNote is generated(required)
@param Category;char;Activity or Reminder
@param Method;char;User or System.
@param Notes;char;The Notes in text form (required)
@param Seq;int;The Seq of actionnote to be modified.( 0 in case of note creation)

@returns ActionID for which ActionNote created.

@author SC
@version 1.0
@created 07/20/2017
----------------------------------------------------------------------*/
{tt/actionnote.i &tablealias = "tactionnote"}
{tt/action.i}
def input  parameter pActionID  as  int     no-undo.
def input  parameter pCategory  as  char    no-undo.
def input  parameter pMethod    as  char    no-undo.
def input  parameter pNotes     as  char    no-undo.
def input  parameter piSeq      as  int     no-undo.
def output parameter table      for tActionnote.
def output parameter table      for Action.
{lib/srvdefs.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* validate the actionID id */
if  pActionID = ? then /* or  pActionID = "" */
do:
  pSuccess = false.
  pMsg = "The ActionID is not valid".
  return.
end.
  
/* validate Notes */
if pNotes = ? or pNotes = "" then
do:
  pSuccess = false.
  pMsg = "The Note is not valid".
  return.
end.

run httpget.p ("",
               "actionNoteCreate",
               "actionID=" + encodeUrl(string(pActionID)) +
               "&category=" + encodeUrl(pCategory)        +
               "&method=" + encodeUrl(pMethod)            +
               "&notes=" + encodeUrl(pNotes)              +
               "&seq=" + encodeUrl(string(piSeq)),
                output pSuccess,
                output pMsg,
                output tResponseFile). 

if not pSuccess 
 then return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

