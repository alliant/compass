&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getrequirementqualifications.p
@description Gets requirement and qualifications records for First Party/Third party 

@author Rahul Sharma

@version 1.0
@created 11.21.2018

@Modification
Date          Name             Description
03/30/20      Shubham          Added reqfor field for organization structure change.
----------------------------------------------------------------------*/

{tt/statereqqual.i}

/* Parameter definitions */
define input  parameter ipcStateID       as  character no-undo.
define input  parameter ipcEntity        as  character no-undo.
define input  parameter ipcAuthorizedBy  as  character no-undo.
define input  parameter ipiReqId         as integer    no-undo.
define output parameter table            for statereqqual.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p ("",
               "requirementQualsGet",
               (if ipcStateID       <> ? and ipcStateID       <> "" then "StateID="        + trim(encodeUrl(ipcStateID))       else "StateID=" + trim(encodeUrl("ALL"))) + 
               (if ipcEntity        <> ? and ipcEntity        <> "" then "&Entity="        + trim(encodeUrl(ipcEntity))        else "&Entity=" + trim(encodeUrl("ALL"))) +
               (if ipcAuthorizedBy  <> ? and ipcAuthorizedBy  <> "" then "&Authorizedby="  + trim(encodeUrl(ipcAuthorizedBy))  else "&Authorizedby=")                    +
               (if ipiReqId         <> ? and ipiReqId         <> 0  then "&RequirementID=" + trim(encodeUrl(string(ipiReqId))) else "&RequirementID="),
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess 
 then 
  return.
 
pSuccess = parseXML(output pMsg).

if not pSuccess
 then
  empty temp-table statereqqual.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

  if qName = "statereqqual" 
   then
    do: 
      create statereqqual.
      do  i = 1 to  attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "stateReqQualID"    then statereqqual.stateReqQualID    = if integer(fldvalue) = ?            then 0            else integer(fldvalue)  no-error.
          when "requirementID"     then statereqqual.requirementID     = if integer(fldvalue) = ?            then 0            else integer(fldvalue)  no-error. 
          when "description"       then statereqqual.description       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "reqappliesTo"      then statereqqual.reqappliesTo      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "refId"             then statereqqual.refId             = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "role"              then statereqqual.role              = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "qualification"     then statereqqual.qualification     = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "appliesTo"         then statereqqual.appliesTo         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "active"            then statereqqual.active            = if logical(fldvalue) = ?            then no           else logical(fldvalue)  no-error. 
          when "effectiveDate"     then statereqqual.effectiveDate     = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error. 
          when "expirationDate"    then statereqqual.expirationDate    = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error. 
          when "authorizedBy"      then statereqqual.authorizedBy      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "reqFor"            then statereqqual.reqFor            = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        end case.
      end.
      release statereqqual.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

