&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file approveapinvoice.p
@description Makes a server call to approve an invoice

@param ID;int;The row identifier
@param Seq;int;The sequence
@param Amount;decimal
@param RefCategory;char
@param Notes;char
@param RefType;char

@author John Oliver
@version 1.0
@created XX.XX.20XX
----------------------------------------------------------------------*/

{tt/apinva.i}.
define input parameter pID as int no-undo.
define input parameter pSeq as int no-undo.
define input parameter pAmount as decimal no-undo.
define input parameter pNotes as character no-undo.
define input parameter pRefType as character no-undo.
define output parameter pReserve as decimal no-undo.
{lib/srvdefs.i}

define variable cAction as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

if pID = ? or pID = 0
 then pMsg = "The ID cannot be unknown or zero~n".
 
if pSeq = ? or pSeq = 0
 then pMsg = "The sequence cannot be unknown or zero~n".
 
if pMsg > ""
 then
  do:
    pMsg = trim(pMsg,"~n").
    return.
  end.
  
if pAmount = ?
 then pAmount = 0.
 
case lower(pRefType):
 when "c" then cAction = "claimInvoiceApprove".
 otherwise cAction = "apInvoiceApprove".
end case.
    
run httpget.p 
   ("",
    cAction,
    "ID=" + encodeUrl(string(pID))
    + "&seq=" + encodeUrl(string(pSeq))
    + "&amount=" + encodeUrl(string(pAmount))
    + "&notes=" + encodeUrl(pNotes)
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
  if qName = "parameter"
   then
    DO  i = 1 TO  attributes:NUM-ITEMS:
     fldvalue = attributes:get-value-by-index(i).
     CASE attributes:get-qname-by-index(i):
      when "ReserveAdjustment" THEN pReserve = decimal(fldvalue) no-error.
     END CASE.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

