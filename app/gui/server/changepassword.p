&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name changepassword.p
@description File to change password of a sysuser

@author Rahul Sharma

@version 1.0
@created 10/03/2018
----------------------------------------------------------------------*/
/* Parameter definitions */
define input parameter ipcUserID   as character no-undo.
define input parameter ipcPassword as character no-undo.

/* Definitions           */
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p 
   ("",
    "SystemUserPasswordAdmin",
    (if ipcUserID    <> ? and ipcUserID    <> "" then "userid="    + encodeUrl(string(ipcUserID))    else "userid=") +
    (if ipcPassword  <> ? and ipcPassword  <> "" then "&password=" + encodeUrl(string(ipcPassword))  else "&password="),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.

 pSuccess = parseXML(output pMsg).
 pMsg = replace(pMsg, "; ", "~n").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 if qName = "parameter"
  then
    do: 
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "Msg" THEN parseMsg = decodeXml(fldvalue).
         END CASE.
        END.
    end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

