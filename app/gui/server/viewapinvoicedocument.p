&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file viewapinvoicedocument.p
@description Gets the invoice document from ShareFile

@param ID;int;The ID of the invoice
@param File;char;The path to the file

@author John Oliver
@created XX.XX.2016 
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/apinv.i &tableAlias="ttInvoice"}
def input parameter table for ttInvoice.
def output parameter pFile as char no-undo.
{lib/srvdefs.i}
{lib/account-repository.i}

def var cServerPath as char no-undo.
def var cFileName as char no-undo.
def var cLocalPath as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME




&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

std-lo = false.
for first ttInvoice:
  std-lo = true.
end.
if not std-lo
 then
  do: pSuccess = false.
      pMsg = "There must be one invoice record present".
      return.
  end.

for first ttInvoice:
  /* build the directory path */
  publish "GetTempDir" (output cLocalPath).
  cServerPath = getPayableServerPath(ttInvoice.vendorID,ttInvoice.invoiceDate).
  cFileName = ttInvoice.documentFilename.
  /* delete the file if it's there */
  pFile = cLocalPath + cFileName.
  if search(pFile) <> ?
   then os-delete value(pFile).
  /* download the file */
  run repository/repoFileDownload.p ("OPS",
                                  cServerPath + cFileName,
                                  pFile,
                                  output pSuccess,
                                  output std-ch
                                  ).
end.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
