/*------------------------------------------------------------------------
@name prelimhistorygl.p
@action historyGlPrelim 

@returns ledgerreport
@author Rahul Sharma
@version 1.0
@created 01/29/2021
Modification:
Date          Name      Description
----------------------------------------------------------------------*/

{tt/ledgerreport.i}
define input  parameter ipcAgentID      as  character no-undo.
define input  parameter ipcFileNo       as  character no-undo.
define input  parameter ipcReference    as  character no-undo.
define input  parameter ipcCheckNum     as  character no-undo.
define input  parameter ipcNotes        as  character no-undo.
define input  parameter ipdeAmount      as  decimal   no-undo.
define input  parameter ipcRevenueType  as  character no-undo.
define output parameter table           for ledgerreport.

{lib/srvdefs.i}

run httpget.p 
   ("",
    "historyGlPrelim",
    "AgentID="      + ipcAgentID +
    "&Filenumber="  + ipcFileNo +
    "&Reference="   + ipcReference +
    "&RevenueType=" + ipcRevenueType +
    "&CheckNum="    + ipcCheckNum +
    "&Notes="       + ipcNotes +
    "&TranAmt="     + string(ipdeAmount),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

procedure startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=ledgerreport}
end procedure.

