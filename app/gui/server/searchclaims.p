&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file server/searchclaims.p
@purpose Makes a server call to search for claims

@param SearchString;char;Search String (Mandatory)

@returns Claim;complex;the claims table

@author Bryan Johnson (bjohnson)
@created 01.20.2016
@notes
----------------------------------------------------------------------*/

{tt/claim.i}

def input parameter pSearchString as char no-undo.
def output parameter table for claim.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
  
 run httpget.p 
   ("",
    "claimsSearch",
    ("searchString=" + encodeUrl(pSearchString)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table claim.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer ttClaim for claim.

 if qName = "Claim"
  then
    do: 
      create ttClaim.
      DO  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        CASE attributes:get-qname-by-index(i):
          when "claimID" then ttClaim.claimID = integer(fldvalue) no-error.
          when "description" then ttClaim.description = fldvalue.
          when "stat" then ttClaim.stat = fldvalue.
          when "type" then ttClaim.type = fldvalue.
          when "stage" then ttClaim.stage = fldvalue.
          when "action" then ttClaim.action = fldvalue.
          when "difficulty" then ttClaim.difficulty = integer(fldvalue) no-error.
          when "urgency" then ttClaim.urgency = integer(fldvalue) no-error.
          when "effDate" then ttClaim.effDate = datetime(fldvalue) no-error.
          when "refYear" then ttClaim.refYear = integer(fldvalue) no-error.
          when "yearFirstReport" then ttClaim.yearFirstReport = integer(fldvalue) no-error.
          when "periodID" then ttClaim.periodID = integer(fldvalue) no-error.
          when "stateID" then ttClaim.stateID = fldvalue.
          when "assignedTo" then ttClaim.assignedTo = fldvalue.
          when "agentID" then ttClaim.agentID = fldvalue.
          when "agentName" then ttClaim.agentName = fldvalue.
          when "agentStat" then ttClaim.agentStat = fldvalue.
          when "fileNumber" then ttClaim.fileNumber = fldvalue.
          when "insuredName" then ttClaim.insuredName = fldvalue.
          when "borrowerName" then ttClaim.borrowerName = fldvalue.
          when "hasReinsurance" then ttClaim.hasReinsurance = logical(fldvalue) no-error.
          when "hasDeductible" then ttClaim.hasDeductible = logical(fldvalue) no-error.
          when "deductibleAmount" then ttClaim.deductibleAmount = decimal(fldvalue) no-error.
          when "amountWaived" then ttClaim.amountWaived = decimal(fldvalue) no-error.
          when "seekingRecovery" then ttClaim.seekingRecovery = fldvalue.
          when "eoPolicy" then ttClaim.eoPolicy = fldvalue.
          when "eoCarrier" then ttClaim.eoCarrier = fldvalue.
          when "eoEffDate" then ttClaim.eoEffDate = datetime(fldvalue) no-error.
          when "eoRetroDate" then ttClaim.eoRetroDate = datetime(fldvalue) no-error.
          when "eoCoverageAmount" then ttClaim.eoCoverageAmount = decimal(fldvalue) no-error.
          when "agentError" then ttClaim.agentError = fldvalue.
          when "searcherError" then ttClaim.searcherError = fldvalue.
          when "altaRisk" then ttClaim.altaRisk = fldvalue.
          when "altaResponsibility" then ttClaim.altaResponsibility = fldvalue.
          when "priorPolicyUsed" then ttClaim.priorPolicyUsed = fldvalue.
          when "priorPolicyID" then ttClaim.priorPolicyID = fldvalue.
          when "underwritingCompliance" then ttClaim.underwritingCompliance = fldvalue.
          when "sellerBreach" then ttClaim.sellerBreach = fldvalue.
          when "priorPolicyCarrier" then ttClaim.priorPolicyCarrier = fldvalue.
          when "priorPolicyAmount" then ttClaim.priorPolicyAmount = decimal(fldvalue) no-error.
          when "priorPolicyEffDate" then ttClaim.priorPolicyEffDate = datetime(fldvalue) no-error.
          when "summary" then ttClaim.summary = fldvalue.
          when "dateClmReceived" then ttClaim.dateClmReceived = datetime(fldvalue) no-error.
          when "dateTransferred" then ttClaim.dateTransferred = datetime(fldvalue) no-error.
          when "dateCreated" then ttClaim.dateCreated = datetime(fldvalue) no-error.
          when "userCreated" then ttClaim.userCreated = fldvalue.
          when "dateAssigned" then ttClaim.dateAssigned = datetime(fldvalue) no-error.
          when "userAssigned" then ttClaim.userAssigned = fldvalue.
          when "dateAcknowledged" then ttClaim.dateAcknowledged = datetime(fldvalue) no-error.
          when "userAcknowledged" then ttClaim.userAcknowledged = fldvalue.
          when "dateClosed" then ttClaim.dateClosed = datetime(fldvalue) no-error.
          when "userClosed" then ttClaim.userClosed = fldvalue.
          when "dateRecReceived" then ttClaim.dateRecReceived = datetime(fldvalue) no-error.
          when "dateReOpened" then ttClaim.dateReOpened = datetime(fldvalue) no-error.
          when "userReOpened" then ttClaim.userReOpened = fldvalue.
          when "dateResponded" then ttClaim.dateResponded = datetime(fldvalue) no-error.
          when "userResponded" then ttClaim.userResponded = fldvalue.
          when "dateReClosed" then ttClaim.dateReClosed = datetime(fldvalue) no-error.
          when "userReClosed" then ttClaim.userReClosed = fldvalue.
          when "lastActivity" then ttClaim.lastActivity = datetime(fldvalue) no-error.
          when "displayPropAddr" then ttClaim.displayPropAddr = fldvalue.
          when "displayAgentName" then ttClaim.displayAgentName = fldvalue.
        END CASE.
      END.
      release ttClaim.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

