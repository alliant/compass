&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
getholdopens.p
GET all hold open policies by calling the SeRVer action
B.Johnson
2.27.2015
*/

{tt/holdopen.i}

def output parameter table for holdopen.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 run httpget.p 
   ("",
    "holdopensGet",
    "",
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer holdopen for holdopen.

 if qName = "holdopen"
  then
    do: create holdopen.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "policyID" THEN holdopen.policyID = int(fldvalue) no-error.
          when "stat" THEN holdopen.stat = decodeXml(fldvalue).
          when "agentID" then holdopen.agentID = decodeXml(fldvalue).
          when "agentName" then holdopen.agentName = decodeXml(fldvalue).
          when "stateID" then holdopen.stateID = decodeXml(fldvalue).
          when "fileNumber" then holdopen.fileNumber = decodeXml(fldvalue).
          when "issueDate" then holdopen.issueDate = date(fldvalue) no-error.
          when "invoiceDate" then holdopen.invoiceDate = date(fldvalue) no-error.
          when "periodID" then holdopen.periodID = int(fldvalue) no-error.
          when "reprocessed" then holdopen.reprocessed = logical(fldvalue) no-error.
          when "formID" then holdopen.formID = decodeXml(fldvalue).
          when "statCode" then holdopen.statCode = decodeXml(fldvalue).
          when "effDate" then holdopen.effDate = date(fldvalue) no-error.
          when "liabilityAmount" then holdopen.liabilityAmount = dec(fldvalue) no-error.
          when "grossPremium" then holdopen.grossPremium = dec(fldvalue) no-error.
          when "netPremium" then holdopen.netPremium = dec(fldvalue) no-error.
          when "retentionPremium" then holdopen.retentionPremium = dec(fldvalue) no-error.
          when "residential" then holdopen.residential = logical(fldvalue) no-error.
          when "countyID" then holdopen.countyID = decodeXml(fldvalue).
         END CASE.
        END.
        release holdopen.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

