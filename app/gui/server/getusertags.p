&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
server/getusertags.p
GET Tags by calling the SeRVer action
Created 4/14/2020
Modified:

Date        Name   Comments
06/26/2023  SB     Task #104008 Modified to return createdDate for tag
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/tag.i}

define input  parameter pcEntity    as  character no-undo.
define input  parameter pcEntityID  as  character no-undo.

define output parameter table       for tag.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p 
   ("",
    "userTagsGet",    
    "Entity="    + (if pcEntity     <> "" then trim(encodeUrl(pcEntity))  else "") +  
    "&EntityID="  + (if pcEntityID   <> "" then trim(encodeUrl(pcEntityID)) else "") +
    "&getAllTags=true",
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
{lib/srvstartelement.i}

define buffer tag for tag.

if qName = "tag"
 then
   do:
     create tag.
     do i = 1 TO  attributes:num-items:
      fldvalue = attributes:get-value-by-index(i).

      case attributes:get-qname-by-index(i):        
        when "tagID"           then tag.tagID     = if integer(fldvalue) = ?            then 0            else integer(decodexml(fldvalue)).
        when "entity"          then tag.entity        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "entityID"        then tag.entityID      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "UID"             then tag.UID           = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "name"            then tag.name          = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "entityName"      then tag.entityName    = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "createddate"     then tag.createddate   = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue) no-error.
        when "username"        then tag.username      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).        
      end case.
     end.
     release tag.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

