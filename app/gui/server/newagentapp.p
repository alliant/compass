&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*--------------------------------------------------------------------------
@name newagentapp.p
@action agentAppNew
@description Create a new agent application by calling server action.

@param agentapp;complex;The agent application record(s)

@author Yoke Sam (modified by John Oliver)
version 1.1
@created 07.13.2017

Name          Date       Note
------------- ---------- -----------------------------------------------
John Oliver   05/21/2019 Modified to use POST rather than GET
--------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
/* parameters */
define input parameter pType as character no-undo.
define input parameter pSubtype as character no-undo.
define input parameter pAgentID as character no-undo.
define input parameter pUsername as character no-undo.
define input parameter pPassword as character no-undo.

/* temp tables */
{tt/agentapp.i}

/* variables */
{lib/srvdefs.i}
{lib/xmlencode.i}
{lib/tt-xml.i}
define variable xDoc as handle no-undo.
define variable xRoot as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.67
         WIDTH              = 64.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

create agentapp.
assign
  agentapp.agentID  = pAgentID
  agentapp.type     = pType
  agentapp.subtype  = pSubtype
  agentapp.username = pUsername
  agentapp.password = pPassword
  .

  
std-ch = os-getenv("appdata") + "\Alliant\" + "NewAgentApp"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create X-DOCUMENT xDoc.
create x-noderef xRoot.

xDoc:CREATE-NODE(xRoot, "Import", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "AgentApp", 'for each agentapp').

xDoc:save("file", std-ch).
delete object xRoot.
delete object xDoc.
  
run httppost.p 
   (input  "agentAppNew",
    input  "text/xml",
    input  search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

