&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@name modifyagentalert.p
@action agentAlertModify
@description Modifies an alert

@param AlertID;int;The alert ID
@param Status;char;The status of the alert
@param Owner;char;The owner of the alert
@param Severity;char;The severity

@author John Oliver
@version 1.0
@created 10/09/2017
----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/alert.i &tableAlias="data"}

define input parameter table for data.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

find first data no-error.
if not available data
 then
  do:
    pMsg = "There must be one alert".
    pSuccess = false.
    return.
  end.
  
run httpget.p 
   ("",
    "agentAlertModify",
    "AlertID=" + encodeUrl(string(data.alertID)) +
    "&Code=" + encodeUrl(data.processCode) +
    "&Status=" + encodeUrl(data.stat) +
    "&Owner=" + encodeUrl(data.owner) +
    "&Severity=" + encodeUrl(string(data.severity)) +
    "&Description=" + encodeUrl(data.description),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
 
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

