&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file queryagentgroupactivity.p
@description Gets activities for a group of agents

@param Year;integer;The year of the activity
@param CategoryList;char;The categories of the activities
@param AgentIDList;char;The agents for the activities
@return AgentActivity;complex;The agent activity dataset

@author Sachin Chaturvedi
@created 10/07/2020
@Modified  08/13/2021 SC #83827 Change consolidation SP to return results for multiple agents instead of single
@notes
------------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/activity.i &tableAlias="ttActivity"}
define input parameter pYear              as integer no-undo.
define input parameter pCategoryList      as character no-undo.
define input  parameter pStateID          as character no-undo.
define input  parameter pYearOfSign       as integer   no-undo.
define input  parameter pSoftware         as character no-undo.
define input  parameter pCompany          as character no-undo.
define input  parameter pOrganization     as character no-undo.
define input  parameter pManager          as character no-undo. 
define input  parameter pTagList          as character no-undo.
define input  parameter pAffiliationList  as character no-undo.
define output parameter table for ttActivity.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 74.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p
   (input "",
    input "agentGroupActivityQuery",
    input "Year=" + encodeUrl(string(pYear))                       +
           "&CategoryList=" + encodeUrl(pCategoryList)             +
           "&StateID="      + trim(encodeUrl(pStateID))            +
           "&YearOfSign="   + trim(encodeUrl(string(pYearOfSign))) +
           "&Software="     + trim(encodeUrl(pSoftware))           +
           "&Company="      + trim(encodeUrl(string(pCompany)))    +
           "&Organization="    + trim(encodeUrl(pOrganization))    +
           "&Manager="         + trim(encodeUrl(pManager))         +
           "&TagList="         + trim(encodeUrl(pTagList))         +
           "&AffiliationList=" + trim(encodeUrl(pAffiliationList))  
        ,
    output pSuccess,
    output pMsg,
    output tResponseFile
    ).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/get-temp-table.i &t=ttActivity &setParam="'AgentGroupActivity'"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

