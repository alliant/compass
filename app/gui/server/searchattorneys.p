&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name searchattorneys.p
@description Gets the attorneys from the Compass database

@author Sachin chaturvedi

@version 1.0
@created 04.16.2018

@notes
@param ipcStateID;      Identifier for state(FL,AL etc)
@param ipcSearchString; Identifier for user's Search String
@param attorney;        Output table for attorney records for specific criteria
@modified 
Date            Name               Description
03/17/2022      Shefali            Task# 86699 Search both(Attorney and attorney firm)
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/attorney.i &tableAlias="tempAttorney"}
define temp-table attorney like tempAttorney
 fields entity     as character
 fields entityID   as character
 fields entityName as character.

/* Parameter definitions */
define input  parameter ipcStateID      as character no-undo.
define input  parameter ipcSearchString as character no-undo.
define output parameter table for attorney.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p ("",
               "attorneysSearch",
               (if ipcStateID      <> ? and ipcStateID      <> "" then "stateID=" + encodeUrl(string(ipcStateID))    else "")
               + (if ipcSearchString <> ? and ipcSearchString <> "" then "&searchString=" + encodeUrl(ipcSearchString) else ""),
               output pSuccess,
               output pMsg,
               output tResponseFile).

            
 if not pSuccess 
  then
   return.
 
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=attorney}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

