/* server/addratestate.p

@description Add a ratestate record
@author Anjly Chanana
@created 03.09.2018
@notes
@param ratestate        The temp-table for ratestate record to be created.
@return ttAddRateState  The temp-table containing newly created ratestate record.
*/

/* temp-table definitions */
{tt\ratestate.i}
{tt\ratestate.i &tablealias=ttAddRateState}

/* parameter definitions */
define input  parameter table for ratestate.
define output parameter table for ttAddRateState.

{lib/srvdefs.i}
{lib/std-def.i}

/* local variable definitions */
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable piYear  as integer   no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

/* add to the xml document */
std-ch = os-getenv("appdata") + "\Alliant\" + "addRateState"  + "-" +  replace(String(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.

xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).
appendXml(xRoot, "ratestate", 'for each ratestate').
xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

run httppost.p 
  ("rateStateAdd",
   "text/xml",
   search(std-ch),
   output pSuccess,
   output pMsg,
   output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
procedure startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=ttAddRateState &setParam="'ratestate'"}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
