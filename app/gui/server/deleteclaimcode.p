&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file deleteclaimcode.p
@description Makes a server call to delete the claim code

@parameters ClaimID;int;Claim number (required)
@parameters CodeType;char;Code Type (optional)
@parameters Code;char;Code (optional)
@parameters Seq;int;Sequence (optional)

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

def input parameter pClaimID as int no-undo.
def input parameter pCodeType as char no-undo.
def input parameter pCode as char no-undo.
def input parameter pSeq as int no-undo.
{lib/srvdefs.i}

def var sQuery as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the claim ID must be there */
 if pClaimID = ? or pClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "Claim ID cannot be zero or unknown".
        return.
    end.
 sQuery = "claimID=" + encodeURL(string(pClaimID)).
 
 /* add the code type */
 if pCodeType <> ?
  then sQuery = sQuery + "&codeType=" + encodeUrl(pCodeType).
  
 /* add the code type */
 if pCode <> ?
  then sQuery = sQuery + "&code=" + encodeUrl(pCode).
  
 /* add the code type */
 if pSeq <> ? and pSeq <> 0
  then sQuery = sQuery + "&seq=" + encodeUrl(string(pSeq)).
    
 run httpget.p 
   ("",
    "claimCodeDelete",
    sQuery,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

