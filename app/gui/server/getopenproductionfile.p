/*----------------------------------------------------------------------
@file getopenproductionfile.p
@description Makes a server call to get one or multiple record(s)

@returns The artran dataset

@author Sagar R Koralli
@version 1.0
@created 05/10/2024
----------------------------------------------------------------------*/
{tt/artran.i}

define input parameter ipcEntityID      as character no-undo.
define input parameter ipcFileID        as character no-undo.
define input parameter ipiReferenceID   as integer   no-undo.
define output parameter table           for artran.


{lib/srvdefs.i}

/* ***************************  Main Block  *************************** */    
run httpget.p
  ("",
   "openProductionFilesGet",
   "EntityID="       + (if ipcEntityID    = ? then ""  else string(ipcEntityID))     +
   "&FileID="        + (if ipcFileID      = ? then ""  else string(ipcFileID)) +
   "&ReferenceID="   + (if ipiReferenceID = ? then "0" else string(ipiReferenceID)) ,
   output pSuccess,
   output pMsg,
   output tResponseFile).
   
if not pSuccess
 then return.

pSuccess = parseXML(output pMsg).


PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/  
  
 {lib/get-temp-table.i &t=artran &setParam="'ProductionFiles'"}
           
END PROCEDURE.
