&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/linkdocument.p
   Add a document link to an entity
   @author D.Sinclair
   @created 9.18.2014
 */

def input parameter pEntityType as char no-undo.
def input parameter pEntityID as char no-undo.
def input parameter pEntitySeq as int no-undo.
def input parameter pObjID as char no-undo.
def input parameter pObjAttr as char no-undo.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* Nothing to link to */
if pEntityType = ""
  or (pEntityID = "" and pEntitySeq = 0)
 then return.

run httpget.p 
   ("",
    "SystemDocumentNew",
    "Type=" + encodeUrl(pEntityType)
    + "&ID=" + encodeUrl(pEntityID)
    + "&Seq=" + string(pEntitySeq)
    + "&ObjID=" + encodeUrl(pObjID)
    + "&ObjAttr=" + encodeUrl(pObjAttr)
    ,
    output pSuccess, 
    output pMsg,
    output tResponseFile).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


