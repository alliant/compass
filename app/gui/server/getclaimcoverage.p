&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------*/
/*    File        : getclaimcoverage.p                                  */
/*    Purpose     : Makes a server call to get one or all of the claim  */
/*                  coverage records                                    */
/*                                                                      */
/*    Syntax      : Progress (OpenEdge)                                 */
/*                                                                      */
/*    Parameters  : ClaimID;int;Claim number                            */
/*    Parameters  : CoverageID;int;The coverage id (Optional)           */
/*                                                                      */
/*    Returns     : ClaimCoverage;table;The claimcoverage table         */
/*                                                                      */
/*    Author(s)   : John Oliver (joliver)                               */
/*    Created     : XX.XX.2015                                          */
/*    Notes       :                                                     */
/*----------------------------------------------------------------------*/

{tt/claimcoverage.i}

def input parameter pClaimID as int no-undo.
def input parameter pSeq as int no-undo.
def input parameter pCoverageID as char no-undo.
def output parameter table for claimcoverage.
{lib/srvdefs.i}

def var sQuery as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the claim ID must be there */
 if pClaimID = ? or pClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "Claim ID cannot be zero or unknown".
        return.
    end.
 sQuery = "ClaimID=" + encodeURL(string(pClaimID)).

 /* add the sequence number */
 if pSeq <> ? and pSeq <> 0
  then sQuery = sQuery + "&Seq=" + encodeUrl(string(pSeq)).

 /* add the coverage id to the querystring */
 if pCoverageID <> ? and pCoverageID <> ""
  then sQuery = sQuery + "&CoverageID=" + encodeUrl(pCoverageID).
    
 run httpget.p 
   ("",
    "claimCoverageGet",
    sQuery,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table claimcoverage.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer claimcoverage for claimcoverage.

 if qName = "ClaimCoverage"
  then
    do: create claimcoverage.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
            when "claimID" then claimcoverage.claimID = integer(fldvalue) no-error.
            when "seq" then claimcoverage.seq = integer(fldvalue) no-error.
						when "coverageID" then claimcoverage.coverageID = fldvalue.
						when "coverageType" then claimcoverage.coverageType = fldvalue.
						when "insuredType" then claimcoverage.insuredType = fldvalue.
						when "origLiabilityAmount" then claimcoverage.origLiabilityAmount = decimal(fldvalue) no-error.
						when "currentLiabilityAmount" then claimcoverage.currentLiabilityAmount = decimal(fldvalue) no-error.
						when "effDate" then claimcoverage.effDate = datetime(fldvalue) no-error.
            when "coverageYear" then claimcoverage.coverageYear = integer(fldvalue) no-error.
						when "insuredName" then claimcoverage.insuredName = fldvalue.
						when "claimant" then claimcoverage.claimant = fldvalue.
						when "isPrimary" then claimcoverage.isPrimary = logical(fldvalue) no-error.
         END CASE.
        END.
        release claimcoverage.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

