&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name newSysRole.p
@description Create new SysRole

@author Naresh Chopra
@version 1.0
@created 10/04/2018
@notes
@param SysRole; input table to create/update records in DB
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/SysRole.i}

/* Parameter definitions */
define input parameter table for SysRole.
{lib/srvdefs.i}

/* Local Variables */
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable tNewDir as character no-undo.
define variable piYear  as integer   no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
std-ch = os-getenv("appdata") + "\Alliant\" + "newsysrole"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.


xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "SysRole", 'for each SysRole').

xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

run httppost.p 
   ("SystemRoleNew",
    "text/xml",
    search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if pSuccess 
 then
  empty temp-table SysRole.

if not pSuccess 
 then
  return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

