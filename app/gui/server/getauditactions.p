&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getauditactions.p
@description Get the audit actions from the database

@param AuditID;int;The identifier to retrieve

@returns AuditAction;complex;The audit action table

@author John Oliver
@version 1.0
@created 11/20/2015
----------------------------------------------------------------------*/

{tt/auditaction.i}

def input parameter piAuditID as int no-undo.
def output parameter table for qaraction.

{lib/srvdefs.i}

/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

if piAuditID = ? or piAuditID = 0
 then
  do: pSuccess = false.
      pMsg = "Audit ID cannot be zero or unknown".
      return.
  end.


 run httpget.p 
   ("",
    "auditActionsGet",
    "auditID=" + encodeUrl(string(piAuditID)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
 def buffer ttAction for qaraction.

 if qName = "AuditAction"
  then
    do:
      create ttAction.
      DO  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        CASE attributes:get-qname-by-index(i):
					when "actionID" then ttAction.actionID = integer(fldvalue) no-error.
					when "findingID" then ttAction.findingID = integer(fldvalue) no-error.
					when "source" then ttAction.source = fldvalue.
					when "sourceID" then ttAction.sourceID = fldvalue.
					when "actionType" then ttAction.actionType = fldvalue.
					when "uid" then ttAction.uid = fldvalue.
					when "dueDate" then ttAction.dueDate = datetime(fldvalue) no-error.
					when "stat" then ttAction.stat = fldvalue.
					when "followupDate" then ttAction.followupDate = datetime(fldvalue) no-error.
					when "comments" then ttAction.comments = fldvalue.
        END CASE.
      END.
      release ttAction.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

