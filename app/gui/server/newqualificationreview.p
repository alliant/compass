&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name newQualificationReview.p
@description Creates new review in Compass database and updates the qualification record

@author Rahul Sharma
@version 1.0
@created 04/03/2018

@notes
@param review; input table to create records in DB
@modified 
Date           Name              Description
11/19/2018     Gurvindar         chnaged name from newreview to newQualificationReview
----------------------------------------------------------------------*/

/* Temp-Table Definitions */
{tt/review.i}

/* Parameter definitions */
define input  parameter table for review.

define output parameter iReviewID as integer no-undo.

{lib/srvdefs.i}
{lib/xmlencode.i}
{lib/tt-xml.i}
{lib/std-def.i}

define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable tNewDir as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* add to the xml document */
std-ch = os-getenv("appdata") + "\Alliant\" + "newreview"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.

xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "review", 'for each review').

xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

/* Server Call */
run httppost.p ("qualificationReviewNew",
                "text/xml",
                search(std-ch),
                output pSuccess,
                output pMsg,
                output tResponseFile).

if not pSuccess 
 then
  return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

  case qName:
    when "parameter"
     then
      do i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "reviewID" then ireviewID = integer(fldvalue) no-error.
        end case.
      end.
  end case.
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

