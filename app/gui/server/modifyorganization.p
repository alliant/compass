&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name modifyperson.p
@description Modifies existing Person record

@author Rahul Sharma
@version 1.0
@created 10/23/2018
@notes
@param Person; input table to update record in DB
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/Organization.i}

/* Parameter definitions */
define input parameter table FOR Organization.

{lib/srvdefs.i}

/* Local Variables */
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable tNewDir as character no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 81.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
std-ch = os-getenv("appdata") + "\Alliant\" + "modifyOrganization"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.


xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "organization", 'for each organization').


xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

/* Server Call */
run httppost.p ("organizationModify",
                "text/xml",
                search(std-ch),
                output pSuccess,
                output pMsg,
                output tResponseFile).

if not pSuccess
 then
  return.

empty temp-table organization.
  
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  case qName:
    when "organization" then
    do:
      create organization.
      do i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "orgID"       then organization.orgID        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "name"        then organization.name         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "addr1"       then organization.addr1        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "addr2"       then organization.addr2        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "city"        then organization.city         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "state"       then organization.state        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "zip"         then organization.zip          = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue). 
          when "email"       then organization.email        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "website"     then organization.website      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "fax"         then organization.fax          = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).         
          when "phone"       then organization.phone        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "mobile"      then organization.mobile       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).        
          when "stat"        then organization.stat         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "createdDate" then organization.createdDate  = if (fldvalue = "?" or fldvalue = ?) then datetime("") else datetime(decodexml(fldvalue)) no-error.
          when "createdBy"   then organization.createdBy    = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
         end case.
      end.
      release organization.
    end.
  end case.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

