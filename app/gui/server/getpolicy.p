&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/getpolicy.p
   GET POLICY details
   @created 2.19.2015 
   @author D.Sinclair
   @version 1.1 1.12.2020 D.Sinclair - renamed from getdetailspolicy.p for consistency
*/

{tt/policy.i}
{tt/batchform.i}

def input parameter pPolicyID as int no-undo.

def output parameter table for policy.
def output parameter table for batchform.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 9.14
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if pPolicyID = ? or pPolicyID <= 0
 then
  do: pSuccess = false.
      pMsg = "Policy number must be set.".
      return.
  end.
   
 run httpget.p 
   ("",
    "PolicyGet",
    "policyID=" + string(pPolicyID),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer policy for policy.
 def buffer batchform for batchform.

 if qName = "Policy"
  then
    do: create policy.

        do i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         case attributes:get-qname-by-index(i):
          when "policyID" THEN policy.policyID = int(fldvalue) no-error.
          when "agentID" then policy.agentID = decodeXml(fldvalue).
          when "stat" then policy.stat = fldvalue.
          when "issueDate" then policy.issueDate = datetime(fldvalue) no-error.
          when "effDate" then policy.effDate = datetime(fldvalue) no-error.
          when "voidDate" then policy.voidDate = datetime(fldvalue) no-error.
          when "invoiceDate" then policy.invoiceDate = datetime(fldvalue) no-error.
          when "fileNumber" then policy.fileNumber = decodeXml(fldvalue).
          when "stateID" then policy.stateID = decodeXml(fldvalue).
          when "liabilityAmount" then policy.liabilityAmount = dec(fldvalue) no-error.
          when "residential" then policy.residential = logical(fldvalue) no-error.
          when "reprocessed" then policy.reprocessed = logical(fldvalue) no-error.
          when "formID" then policy.formID = decodeXml(fldvalue).
          when "statCode" then policy.statCode = decodeXml(fldvalue).
          when "countyID" then policy.countyID = decodeXml(fldvalue).
          when "issuedEffDate" then policy.issuedEffDate = datetime(fldvalue) no-error.
          when "issuedLiabilityAmount" then policy.issuedLiabilityAmount = dec(fldvalue) no-error.
          when "issuedGrossPremium" then policy.issuedGrossPremium = dec(fldvalue) no-error.
           when "agentname" then policy.agentname = decodeXml(fldvalue).
          when "descstat" then policy.descstat = decodeXml(fldvalue).
          when "descformID" then policy.descformID = decodeXml(fldvalue).
          when "descstatCode" then policy.descstatCode = decodeXml(fldvalue).
          when "desccountyID" then policy.desccountyID = decodeXml(fldvalue).
         end case.
        end.
        release policy.
    end.
  else
 if qName = "BatchForm" 
  then
   do: create batchform.
       do  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
         when "batchID" then batchform.batchID = int(fldvalue) no-error.
         when "zipcode" then batchform.zipcode = fldvalue.
         when "seq" then batchform.seq = int(fldvalue) no-error.
         when "formType" then batchform.formType = decodeXml(fldValue).
         when "formID" then batchform.formID = decodeXml(fldvalue).
         when "statCode" then batchform.statCode = decodeXml(fldvalue).
         when "effDate" then batchform.effDate = date(fldvalue) no-error.
         when "liabilityDelta" then batchform.liabilityDelta = dec(fldvalue) no-error.
         when "grossDelta" then batchform.grossDelta = dec(fldvalue) no-error.
         when "netDelta" then batchform.netDelta = dec(fldvalue) no-error.
         when "grossGLRef" then batchform.grossGLRef = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
         when "grossGLDesc" then batchform.grossGLDesc = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
         when "retainedGLRef" then batchform.retainedGLRef = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
         when "retainedGLDesc" then batchform.retainedGLDesc = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
         when "netGLRef" then batchform.netGLRef = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
         when "netGLdesc" then batchform.netGLdesc = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
         when "notes" then batchform.notes = if fldvalue = ?  or fldvalue = "?" then "" else decodeXml(fldvalue).
        end case.
       end.
       release batchform.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

