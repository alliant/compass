&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
srvmodifystateform.p
MODIFY a STATE FORM by calling the SeRVer action
D.Sinclair
2.16.2014
Modification:
   Date          Name        Description     
   02/08/2019    Vikas Jain  Modified to send revenuyType for stateForm.
*/

def input parameter pStateID as char no-undo.
def input parameter pFormID as char no-undo.

def input parameter pDescription as char no-undo.
def input parameter pFormCode as char no-undo.
/* def input parameter pType as char no-undo.        */
/* def input parameter pInsuredType as char no-undo. */
def input parameter pActive as logical no-undo.
def input parameter pRateCheck as char no-undo.
def input parameter pRateMin as decimal no-undo.
def input parameter pRateMax as decimal no-undo.
def input parameter pOrgName as char no-undo.
def input parameter pOrgRev as char no-undo.
def input parameter pOrgRel as datetime no-undo.
def input parameter pOrgEff as datetime no-undo.
def input parameter pRevType as character no-undo.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* Key fields */
if pStateID = "" or pStateID = ?
 then
  do: pSuccess = false.
      pMsg = "State ID cannot be blank or unknown.".
      return.
  end.

if pFormID = "" or pFormID = ?
 then
  do: pSuccess = false.
      pMsg = "Form ID cannot be blank or unknown.".
      return.
  end.

/* Non-key fields */
if pDescription = ?
    and pFormCode = ? 
/*     and pType = ?         */
/*     and pInsuredType = ?  */
    and pActive = ?
    and pRateCheck = ?
    and pRateMin = ?
    and pRateMax = ?
    and pOrgName = ?
    and pOrgRev = ?
    and pOrgRel = ?
    and pOrgEff = ?
    and pRevType = ?
 then
  do: pSuccess = false.
      pMsg = "At least one attribute must be changed.".
      return.
  end.


       
 run httpget.p 
   ("",
    "stateFormModify",
    "stateID=" + encodeUrl(pStateID)
    + "&formID=" + encodeUrl(pFormID)
    + (if pDescription = ? then "" else "&description=" + encodeUrl(pDescription))
    + (if pFormCode = ? then "" else "&formCode=" + encodeUrl(pFormCode))
/*     + (if pType = ? then "" else "&type=" + encodeUrl(pType))                       */
/*     + (if pInsuredType = ? then "" else "&insuredType=" + encodeUrl(pInsuredType))  */
    + (if pActive = ? then "" else "&active=" + (if pActive then "T" else "F"))
    + (if pRateCheck = ? then "" else "&rateCheck=" + encodeUrl(pRateCheck))
    + (if pRateMin = ? then "" else "&rateMin=" + string(pRateMin))
    + (if pRateMax = ? then "" else "&rateMax=" + string(pRateMax))
    + (if pOrgName = ? then "" else "&orgName=" + encodeUrl(pOrgName))
    + (if pOrgRev = ? then "" else "&orgRev=" + encodeUrl(pOrgRev))
    + (if pOrgRel = ? then "" else "&orgRel=" + string(pOrgRel))
    + (if pOrgEff = ? then "" else "&orgEff=" + string(pOrgEff))
    + (if pRevType = ? then "" else "&RevType=" + encodeUrl(pRevType)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

