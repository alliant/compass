&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
srvnewstateform.p
create a NEW STATE FORM by calling the SeRVer action
D.Sinclair
2.16.2014
Modification:
   Date          Name        Description     
   02/08/2019    Vikas Jain  Modified to send revenuyType for stateForm.
*/

def input parameter pStateID as char no-undo.
def input parameter pFormID as char no-undo.
def input parameter pDescription as char no-undo.
def input parameter pFormCode as char no-undo.
def input parameter pType as char no-undo.
def input parameter pInsuredType as char no-undo.
def input parameter pActive as logical no-undo.
def input parameter pRateCheck as char no-undo.
def input parameter pRateMin as decimal no-undo.
def input parameter pRateMax as decimal no-undo.
def input parameter pOrgName as char no-undo.
def input parameter pOrgRev as char no-undo.
def input parameter pOrgRel as datetime no-undo.
def input parameter pOrgEff as datetime no-undo.
def input parameter pRevType as character no-undo.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if pStateID = "" or pStateID = ?
 then
  do: pSuccess = false.
      pMsg = "State ID cannot be blank or unknown.".
      return.
  end.

if pFormID = "" or pFormID = ?
 then
  do: pSuccess = false.
      pMsg = "Form ID cannot be blank or unknown.".
      return.
  end.

if pDescription = ? 
 then pDescription = "".

/* DAS 
if lookup(pType, "P,E,C,S") = 0
 then
  do: pSuccess = false.
      pMsg = "Form type must be one of P,E,C or S.".
      return.
  end.

*/
if pType = ?
 then pType = "".

if pInsuredType = ?
 then pInsuredType = "".
if pActive = ?
 then pActive = true.
if pRateCheck = ?
 then pRateCheck = "N".
if pRateMin = ?
 then pRateMin = 0.
if pRateMax = ?
 then pRateMax = 0.
if pOrgName = ?
 then pOrgName = "".
       
 run httpget.p 
   ("",
    "stateFormCreate",
    "stateID=" + encodeUrl(pStateID)
    + "&formID=" + encodeUrl(pFormID)
    + "&description=" + encodeUrl(pDescription)
    + "&formCode=" + encodeUrl(pFormCode)
    + "&type=" + encodeUrl(pType)
    + "&insuredType=" + encodeUrl(pInsuredType)
    + "&active=" + (if pActive then "T" else "F")
    + "&rateCheck=" + encodeUrl(pRateCheck)
    + "&rateMin=" + string(pRateMin)
    + "&rateMax=" + string(pRateMax)
    + "&orgName=" + encodeUrl(pOrgName)
    + "&orgRev=" + encodeUrl(pOrgRev)
    + (if pOrgRel = ? then "" else "&orgRel=" + string(pOrgRel))
    + (if pOrgEff = ? then "" else "&orgEff=" + string(pOrgEff))
    + "&RevType=" + encodeUrl(pRevType),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

