&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
getpolicies.p
GET list of policies, optionally filtered by various criteria.
B.Johnson
4.29.2014
Notes:
    Pass null (?) for any parameter to get entire range.
*/

{tt/policy.i}

DEF INPUT PARAM pAgentID        AS CHAR NO-UNDO.
DEF INPUT PARAM pFileNumber     AS CHAR NO-UNDO.
DEF INPUT PARAM pStartPolicy    AS CHAR NO-UNDO.
DEF INPUT PARAM pEndPolicy      AS CHAR NO-UNDO.
DEF INPUT PARAM pPolicyStat     AS CHAR NO-UNDO.
DEF INPUT PARAM pStartDate      as datetime NO-UNDO.
DEF INPUT PARAM pEndDate        as datetime NO-UNDO.

DEF OUTPUT PARAMETER TABLE FOR policy.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

if pAgentID = "" 
 then
  do: pSuccess = false.
      pMsg = "Agent ID cannot be blank.".
      return.
  end.

 run httpget.p 
   ("",
    "policiesGet",
    TRIM(
      (IF pAgentID = ? THEN "" ELSE "&AgentID=" + encodeUrl(pAgentID)) 
    + (IF pFileNumber = ? THEN "" ELSE "&FileNumber=" + encodeUrl(pFileNumber))
    + (IF pStartPolicy = ? THEN "" ELSE "&StartPolicy=" + encodeUrl(pStartPolicy))
    + (IF pEndPolicy = ? THEN "" ELSE "&EndPolicy=" + encodeUrl(pEndPolicy))
    + (IF pPolicyStat = ? THEN "" ELSE "&PolicyStat=" + encodeUrl(pPolicyStat))
    + (IF pStartDate = ? THEN "" ELSE "&StartDate=" + encodeUrl(STRING(pStartDate)))
    + (IF pEndDate = ? THEN "" ELSE "&EndDate=" + encodeUrl(STRING(pEndDate)))
    ,"&"),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table policy.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer policy for policy.

 IF qName = "Policy"
  THEN
    DO: CREATE policy.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "policyID"                THEN policy.policyID = INTEGER(fldvalue) NO-ERROR.
             WHEN "stat"                    THEN policy.stat = fldvalue.
             WHEN "agentID"                 THEN policy.agentID = fldvalue.
             WHEN "companyID"               THEN policy.companyID = fldvalue.
             WHEN "stateID"                 THEN policy.stateID = fldvalue.
             WHEN "fileNumber"              THEN policy.fileNumber = fldvalue.
             WHEN "issueDate"               THEN policy.issueDate = DATE(fldvalue) NO-ERROR.
             WHEN "voidDate"                THEN policy.voidDate = DATE(fldvalue) NO-ERROR.
             WHEN "invoiceDate"             THEN policy.invoiceDate = DATE(fldvalue) NO-ERROR.
             WHEN "periodID"                THEN policy.periodID = INTEGER(fldvalue) NO-ERROR.
             WHEN "reprocessed"             THEN policy.reprocessed = LOGICAL(fldvalue) NO-ERROR.
             WHEN "issuedFormID"            THEN policy.issuedFormID = fldvalue.
             WHEN "issuedLiabilityAmount"   THEN policy.issuedLiabilityAmount = DECIMAL(fldvalue) NO-ERROR.
             WHEN "issuedGrossPremium"      THEN policy.issuedGrossPremium = DECIMAL(fldvalue) NO-ERROR.
             WHEN "issuedResidential"       THEN policy.issuedResidential = LOGICAL(fldvalue) NO-ERROR.
             WHEN "issuedEffDate"           THEN policy.issuedEffDate = DATE(fldvalue) NO-ERROR.
             WHEN "formID"                  THEN policy.formID = fldvalue.
             WHEN "statcode"                THEN policy.statcode = fldvalue.
             WHEN "effDate"                 THEN policy.effDate = DATE(fldvalue) NO-ERROR.
             WHEN "liabilityAmount"         THEN policy.liabilityAmount = DECIMAL(fldvalue) NO-ERROR.
             WHEN "grossPremium"            THEN policy.grossPremium = DECIMAL(fldvalue) NO-ERROR.
             WHEN "netPremium"              THEN policy.netPremium = DECIMAL(fldvalue) NO-ERROR.
             WHEN "retention"               THEN policy.retention = DECIMAL(fldvalue) NO-ERROR.
             WHEN "residential"             THEN policy.residential = LOGICAL(fldvalue) NO-ERROR.
             WHEN "countyID"                THEN policy.countyID = fldvalue.
             WHEN "reportDate"              THEN policy.reportDate = DATE(fldvalue) NO-ERROR.
             WHEN "paidDate"                THEN policy.paidDate = DATE(fldvalue) NO-ERROR.
             WHEN "addr1"                   THEN policy.addr1 = decodeXml(fldvalue).
             WHEN "addr2"                   THEN policy.addr2 = decodeXml(fldvalue).
             WHEN "addr3"                   THEN policy.addr3 = decodeXml(fldvalue).
             WHEN "addr4"                   THEN policy.addr4 = decodeXml(fldvalue).
             WHEN "city"                    THEN policy.city = decodeXml(fldvalue).
             WHEN "county"                  THEN policy.county = fldvalue.
             WHEN "state"                   THEN policy.state = fldvalue.
             WHEN "zip"                     THEN policy.zip = fldvalue.
             WHEN "trxID"                   THEN policy.trxID = fldvalue.
             WHEN "trxDate"                 THEN policy.trxDate = DATE(fldvalue) NO-ERROR.
         END CASE.
        END.
        release policy.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

