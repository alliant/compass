&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name modifySysprop.p
@description Modify Sysprop table 

@author Sachin Chaturvedi
@version 1.0
@created 03/01/2019
@notes
@param
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/Sysprop.i}
/* Parameter definitions */
define input parameter table for Sysprop.
define output parameter dtlastModified as datetime  no-undo.
{lib/srvdefs.i}

/* Local Variables */
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.


{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
std-ch = os-getenv("appdata") + "\Alliant\" + "modifysysprop"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.


xDoc:create-node(xRoot, "Action", "ELEMENT").
xDoc:append-child(xRoot).
appendXml(xRoot, "Sysprop", 'for each Sysprop').

xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

run httppost.p 
   ("SystemPropertyModify",
    "text/xml",
    search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if pSuccess then
  empty temp-table Sysprop.

if not pSuccess then
  return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 if qName = "parameter"
     then
      DO i = 1 TO attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        CASE attributes:get-qname-by-index(i):
         when "lastModified"  then dtlastModified = datetime(fldvalue).
        END CASE.
      END.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

