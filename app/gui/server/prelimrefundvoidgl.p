/*------------------------------------------------------------------------
@name prelimrefundvoidgl.p
@action refundVoidGlPrelim 

@returns ledgerreport
@author Anjly
@version 1.0
@created 07/24/2020
Modification:
Date          Name      Description
----------------------------------------------------------------------*/

{tt/ledgerreport.i}
define input parameter ipcTypeID         as character  no-undo.
define input parameter ipiSelectedArTranID as integer    no-undo.
define input parameter ipcRefundAmt        as decimal    no-undo.
define output parameter table for ledgerreport.
{lib/srvdefs.i}

run httpget.p 
   ("",
    "refundVoidGlPrelim",
    "TypeID=" + ipcTypeID +
    "&arTranID=" + string(ipiSelectedArTranID) +
    "&RefundAmt=" + string(ipcRefundAmt),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

procedure startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 def buffer ledgerreport for ledgerreport.
  {lib/get-temp-table.i &t=ledgerreport}
end procedure.


