&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file getclaimcontact.p
@description Makes a server call to get one or all of the claim

@param ClaimID;int;Claim number
@param ContactNbr;char;Contact number Type (Optional)

@returns claimcontact;table;the claimcontact table

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

{tt/claimcontact.i}

def input parameter pClaimID as int no-undo.
def input parameter pContactID as int no-undo.
def output parameter table for claimcontact.
{lib/srvdefs.i}

def var sQuery as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the claim ID must be there */
 if pClaimID = ? or pClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "Claim ID cannot be zero or unknown".
        return.
    end.
 sQuery = "ClaimID=" + encodeURL(string(pClaimID)).
 
 /* check if there is a contact number added */
 if pContactID <> ? and pContactID <> 0
  then sQuery = sQuery + "&ContactID=" + encodeUrl(string(pContactID)).
    
 run httpget.p 
   ("",
    "claimContactGet",
    sQuery,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table claimcontact.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer claimcontact for claimcontact.

 if qName = "claimcontact"
  then
    do: create claimcontact.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
            when "claimID" then claimcontact.claimID = integer(fldvalue) no-error.
						when "contactID" then claimcontact.contactID = integer(fldvalue) no-error.
						when "role" then claimcontact.role = fldvalue.
						when "fname" then claimcontact.fname = fldvalue.
						when "lname" then claimcontact.lname = fldvalue.
						when "fullName" then claimcontact.fullName = fldvalue.
						when "jobTitle" then claimcontact.jobTitle = fldvalue.
						when "email" then claimcontact.email = fldvalue.
						when "phone" then claimcontact.phone = fldvalue.
						when "fax" then claimcontact.fax = fldvalue.
						when "mobile" then claimcontact.mobile = fldvalue.
						when "isActive" then claimcontact.isActive = logical(fldvalue) no-error.
						when "company" then claimcontact.company = fldvalue.
						when "addr1" then claimcontact.addr1 = fldvalue.
						when "addr2" then claimcontact.addr2 = fldvalue.
						when "city" then claimcontact.city = fldvalue.
						when "countyID" then claimcontact.countyID = fldvalue.
						when "stateID" then claimcontact.stateID = fldvalue.
						when "zipcode" then claimcontact.zipcode = fldvalue.
						when "notes" then claimcontact.notes = decodeXml(fldvalue).
         END CASE.
        END.
        release claimcontact.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

