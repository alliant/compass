&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@file getagentgrpclmcostssummary.p
@description Get the claim costs summary report for a group of agents

@author Sachin Chaturvedi
@date 2020.10.19
@modified
Date         Name     Description
08/13/2021   SC       #83827 Change consolidation SP to return results for multiple agents instead of single
  ----------------------------------------------------------------------*/

{tt/claimcostssummary.i}
define input  parameter pEndDate          as datetime  no-undo.
define input  parameter pStateID          as character no-undo.
define input  parameter pYearOfSign       as integer   no-undo.
define input  parameter pSoftware         as character no-undo.
define input  parameter pCompany          as character no-undo.
define input  parameter pOrganization     as character no-undo.
define input  parameter pManager          as character no-undo. 
define input  parameter pTagList          as character no-undo.
define input  parameter pAffiliationList  as character no-undo.
define output parameter table    for claimcostssummary.
{lib/srvdefs.i}

/* variables */
{lib/std-def.i}


/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

run httpget.p 
   (input  "",
    input  "AgentGrpClmCostsSummGet",
    input  "EndDate="          + encodeUrl(string(pEndDate))          +
           "&StateID="         + trim(encodeUrl(pStateID))            +
           "&YearOfSign="      + trim(encodeUrl(string(pYearOfSign))) +
           "&Software="        + trim(encodeUrl(pSoftware))           +
           "&Company="         + trim(encodeUrl(string(pCompany)))    +
           "&Organization="    + trim(encodeUrl(pOrganization))       +
           "&Manager="         + trim(encodeUrl(pManager))            +
           "&TagList="         + trim(encodeUrl(pTagList))            +
           "&AffiliationList=" + trim(encodeUrl(pAffiliationList))
           ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then 
  return.
 
pSuccess = parseXML(output pMsg).

if not pSuccess
 then 
  empty temp-table claimcostssummary.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/get-temp-table.i &t=claimcostssummary &setParam="'AgentGroupClaimCostsSummary'"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

