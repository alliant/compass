&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
srvcloseperiod.p
CLOSE a PERIOD by calling the SeRVer action
D.Sinclair
2.4.2014
*/

def input parameter pPeriodID as char.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.


{lib/std-def.i}

def var tPostFile as char no-undo.
def var tTraceFile as char no-undo.
def var tResponseFile as char no-undo.

{lib/xmlparse.i &exclude-startElement=true
                &traceFile=tTraceFile
                &xmlFile=tResponseFile}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 run httpget.p 
   ("",
    "periodClose",
    "periodID=" + pPeriodID,
    output pSuccess,
    output pMsg,
    output tResponseFile).


/* POST

 tPostFile = "<name>".
 output to value(tPostFile) page-size 0.
 put unformatted '<?xml version="1.0"?>' skip.
 put unformatted '<Action actionID="' string(pActionID) '"'.
 put unformatted ' comments="' encodeXml(pComments) '"/>' skip.
 output close.

 run httppost.p 
   ("",
    "<action>",
    search(tPostFile),
    output pSuccess,
    output pMsg,
    output tResponseFile).

*/


 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
  Notes:       This is not clean as it's all mushed together in this single
               procedure.  Ideally, each service should have it's own parser.
------------------------------------------------------------------------------*/
 DEFINE INPUT PARAMETER namespaceURI AS CHARACTER.
 DEFINE INPUT PARAMETER localName AS CHARACTER.
 DEFINE INPUT PARAMETER qName AS CHARACTER.
 DEFINE INPUT PARAMETER attributes AS HANDLE.

 DEF VAR i AS INT.
 DEF VAR fldvalue AS CHAR.

/*
 def buffer qaraudit for qaraudit.
 def buffer qaraction for qaraction.
 def buffer qarnote for qarnote.
 def buffer qaractionstat for qaractionstat.
 def buffer finding for finding.
 def buffer bestpractice for bestpractice.

 def buffer dbstate for dbstate.
 def buffer dbauditor for dbauditor.
*/

 def var responseCode as char no-undo.

 case qName: 
  when "Fault" 
   then 
    do:
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "faultCode" THEN
                 responseCode = fldvalue.
             WHEN "faultString" THEN
                 parseMsg = fldvalue.
         END CASE.
        END.
        parseStatus = false.
    end.

/*
  when "Action" or when "qaraction"
   then 
    do: create qaraction.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             WHEN "auditID" THEN
                 qaraction.qarID = fldvalue.
             WHEN "actionID" THEN
                 qaraction.actionID = int(fldvalue).
          when "priority" then qaraction.priority = int(fldvalue).
          when "actionType" then qaraction.actionType = fldvalue.
          when "comments" then qaraction.action = decodeXml(fldvalue).
          when "questionID" then qaraction.questionID = fldvalue.
          when "description" then qaraction.questionDesc = decodeXml(fldvalue).
          when "finding" then qaraction.finding = decodeXml(fldvalue).
          when "files" then qaraction.files = fldvalue.
          when "reference" then qaraction.reference = decodeXml(fldvalue).
          when "dueDate" then qaraction.dueDate = date(fldvalue) no-error.
          when "followupDate" then qaraction.followupDate = date(fldvalue) no-error.
          when "status" then qaraction.stat = fldvalue.
         END CASE.
        END.
        release qaraction.
    end.
  when "ActionNote" or when "qarnote"
   then 
    do: create qarnote.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          WHEN "actionID" THEN qarnote.actionId = int(fldvalue).
          when "seq" then qarnote.seq = int(fldvalue).
          when "comments" then qarnote.notes = decodeXml(fldvalue).
          WHEN "noteDate" THEN qarnote.noteDate = date(fldvalue) no-error.
          WHEN "takenBy" THEN qarnote.takenBy = decodeXml(fldvalue).
          when "secured" then qarnote.secured = (fldvalue = "yes").
         END CASE.
        END.
        release qarnote.
    end.
  when "ActionStatus"
   then 
    do: create qaractionstat.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          WHEN "actionID" THEN qaractionstat.actionId = int(fldvalue).
          when "seq" then qaractionstat.seq = int(fldvalue).
          when "status" then qaractionstat.stat = decodeXml(fldvalue).
          WHEN "statDate" THEN qaractionstat.statDate = date(fldvalue) no-error.
          WHEN "userid" THEN qaractionstat.uid = decodeXml(fldvalue).
          when "comments" then qaractionstat.comments = fldvalue.
         END CASE.
        END.
        release qaractionstat.
    end.
  
    
  when "Audit" or when "qaraudit"
   then
    do: create qaraudit.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "auditID" or when "qarID" then qaraudit.qarID = fldvalue.
          WHEN "agentID" THEN qaraudit.agentId = fldvalue.
          when "name" then qaraudit.name = decodeXml(fldvalue).
          WHEN "addr1" THEN qaraudit.addr1 = decodeXml(fldvalue).
          WHEN "addr2" THEN qaraudit.addr2 = decodeXml(fldvalue).
          when "city" then qaraudit.city = fldvalue.
          when "state" then qaraudit.state = fldvalue.
          when "zip" then qaraudit.zip = fldvalue.
          when "auditor" then qaraudit.auditor = decodeXml(fldvalue).
          when "auditDate" then qaraudit.auditDate = date(fldvalue) no-error.
          when "auditScore" or when "score" then qaraudit.score = int(fldvalue).
          when "contactName" then qaraudit.contactName = decodeXml(fldvalue).
          when "deliveredTo" then qaraudit.deliveredTo = decodeXml(fldvalue).
          when "comments" then qaraudit.comments = decodeXml(fldvalue).
          when "section1score" then qaraudit.section1score = int(fldvalue).
          when "section2score" then qaraudit.section2score = int(fldvalue).
          when "section3score" then qaraudit.section3score = int(fldvalue).
          when "section4score" then qaraudit.section4score = int(fldvalue).
          when "section5score" then qaraudit.section5score = int(fldvalue).
          when "section6score" then qaraudit.section6score = int(fldvalue).
          when "section7score" then qaraudit.section7score = int(fldvalue).
          when "section8score" then qaraudit.section8score = int(fldvalue).
          when "nextFollowupDate" then qaraudit.nextFollowupDate = date(fldvalue) no-error.
          when "nextDueDate" then qaraudit.nextDueDate = date(fldvalue) no-error.
          when "numMajor" then qaraudit.numMajor = int(fldvalue).
          when "numInter" then qaraudit.numInter = int(fldvalue).
          when "numMinor" then qaraudit.numMinor = int(fldvalue).
          when "numCorrective" then qaraudit.numCorrective = int(fldvalue).
          when "numRecommendations" then qaraudit.numRecommendations = int(fldvalue).
          when "numSuggestions" then qaraudit.numSuggestions = int(fldvalue).
          when "numOpen" then qaraudit.numOpen = int(fldvalue).
          when "numInProcess" then qaraudit.numInProcess = int(fldvalue).
          when "numCompleted" then qaraudit.numCompleted = int(fldvalue).
          when "numRejected" then qaraudit.numRejected = int(fldvalue).
          when "submittedBy" then qaraudit.submittedBy = decodeXml(fldvalue).
          when "submittedAt" then qaraudit.submittedAt = fldvalue.
         END CASE.
        END.
        release qaraudit.
    end.

  when "Finding"
   then
    do: create finding.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "auditID" then finding.qarID = fldvalue.
          when "sectionID" then finding.sectionID = fldvalue.
          WHEN "questionID" THEN finding.questionID = fldvalue.
          when "description" then finding.description = decodeXml(fldvalue).
          WHEN "priority" THEN finding.priority = int(fldvalue).
          WHEN "comments" THEN finding.comments = decodeXml(fldvalue).
         END CASE.
        END.
        release finding.
    end.

  when "BestPractice"
   then
    do: create bestpractice.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "auditID" then bestpractice.qarID = fldvalue.
          when "sectionID" then bestpractice.sectionID = fldvalue.
          WHEN "questionID" THEN bestpractice.questionID = fldvalue.
          when "description" then bestpractice.description = decodeXml(fldvalue).
          WHEN "comments" THEN bestpractice.comments = decodeXml(fldvalue).
         END CASE.
        END.
        release bestpractice.
    end.

  when "State" 
   then
    do: create dbstate.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "state" then dbstate.state = fldvalue.
          when "period" then dbstate.period = date(fldvalue).
          WHEN "numAudits" THEN dbstate.numAudits = int(fldvalue).
          WHEN "numMajor" THEN dbstate.numMajor = int(fldvalue).
          WHEN "numInter" THEN dbstate.numInter = int(fldvalue).
          WHEN "numMinor" THEN dbstate.numMinor = int(fldvalue).
          WHEN "numSuggestions" THEN dbstate.numSuggestions = int(fldvalue).
          WHEN "numRecommendations" THEN dbstate.numRecommendations = int(fldvalue).
          WHEN "numCorrective" THEN dbstate.numCorrective = int(fldvalue).
          WHEN "numOpen" THEN dbstate.numOpen = int(fldvalue).
          WHEN "numInProcess" THEN dbstate.numInProcess = int(fldvalue).
          WHEN "numComplete" THEN dbstate.numComplete = int(fldvalue).
          WHEN "numRejected" THEN dbstate.numRejected = int(fldvalue).
          WHEN "maxScore" THEN dbstate.maxScore = int(fldvalue).
          WHEN "minScore" THEN dbstate.minScore = int(fldvalue).
          WHEN "totalScore" THEN dbstate.totalScore = int(fldvalue).
         END CASE.
        END.
        release dbstate.
    end.
 
  when "Auditor" 
   then
    do: create dbauditor.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "auditor" then dbauditor.auditor = decodeXml(fldvalue).
          when "period" then dbauditor.period = date(fldvalue).
          WHEN "numAudits" THEN dbauditor.numAudits = int(fldvalue).
          WHEN "numMajor" THEN dbauditor.numMajor = int(fldvalue).
          WHEN "numInter" THEN dbauditor.numInter = int(fldvalue).
          WHEN "numMinor" THEN dbauditor.numMinor = int(fldvalue).
          WHEN "numSuggestions" THEN dbauditor.numSuggestions = int(fldvalue).
          WHEN "numRecommendations" THEN dbauditor.numRecommendations = int(fldvalue).
          WHEN "numCorrective" THEN dbauditor.numCorrective = int(fldvalue).
          WHEN "numOpen" THEN dbauditor.numOpen = int(fldvalue).
          WHEN "numInProcess" THEN dbauditor.numInProcess = int(fldvalue).
          WHEN "numComplete" THEN dbauditor.numComplete = int(fldvalue).
          WHEN "numRejected" THEN dbauditor.numRejected = int(fldvalue).
          WHEN "maxScore" THEN dbauditor.maxScore = int(fldvalue).
          WHEN "minScore" THEN dbauditor.minScore = int(fldvalue).
          WHEN "totalScore" THEN dbauditor.totalScore = int(fldvalue).
         END CASE.
        END.
        release dbauditor.
    end.
 */
 
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

