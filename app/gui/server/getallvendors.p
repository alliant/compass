/*------------------------------------------------------------------------
@file getallvendors.p
@description Gets all vendor

@param  
@return Vendor;complex;The Vendor table

@author Sagar K
@created 09-05-2023

Name          Date       Note
------------- ---------- -----------------------------------------------

------------------------------------------------------------------------*/

/* Temp Tables */
{tt/apvendor.i}

/* ***************************  Definitions  ************************** */
define input parameter pStatus as character no-undo.
define output parameter table for apvendor.

{lib/srvdefs.i}
{lib/rpt-defs.i}

{lib/compass-dataset-def.i &buffers="buffer apvendor:handle"}

/* Variables */

/* ***************************  Main Block  *************************** */

run util/httpcall.p (input  "get",
                     input  "doweb",
                     input  "allVendorsGet",
                     input  "",
                     input  "",
                     input  "Status=" + encodeUrl(pStatus),
                     {lib/rpt-setparams.i},
                     output pSuccess,
                     output pMsg,
                     output clientResponse).

if not pSuccess
 then return.
 
pSuccess = loadDataset(output pMsg).

