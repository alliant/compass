&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file modifyinvoiceapproval.p
@description Makes a server call to modify an invoice approval record

@param ID;int;The row identifier
@param Seq;int;The row sequence
@param Role;char
@param Uid;char
@param Amount;decimal
@param Notes;char

@returns The row identifier

@author John Oliver
@version 1.0
@created XX.XX.20XX
----------------------------------------------------------------------*/

{tt/apinva.i}.
def input parameter pID as int no-undo.
def input parameter pSeq as int no-undo.
def input parameter pRole as char no-undo.
def input parameter pUid as char no-undo.
def input parameter pAmount as decimal no-undo.
def input parameter pNotes as char no-undo.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */


if pID = ? or pID = 0
 then pMsg = "The ID cannot be unknown or zero~n".
 
if pSeq = ? or pSeq = 0
 then pMsg = "The seq cannot be unknown or blank~n".
 
if pRole = ? or pRole = ""
 then pMsg = "The role cannot be unknown or blank~n".
 
if pUid = ? or pUid = ""
 then pMsg = "The user cannot be unknown or blank~n".
 
if pMsg > ""
 then
  do:
    pMsg = trim(pMsg,"~n").
    return.
  end.
  
if pAmount = ?
 then pAmount = 0.
    
run httpget.p 
   ("",
    "apInvoiceApprovalModify",
		"ID=" + encodeUrl(string(pID))
    + "&seq=" + encodeUrl(string(pSeq))
		+ "&role=" + encodeUrl(pRole)
		+ "&uid=" + encodeUrl(pUid)
		+ "&amount=" + encodeUrl(string(pAmount))
		+ (if pID <> 0 then "notes=" + encodeUrl(string(pID)) else "")
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

