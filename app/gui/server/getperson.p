&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getperson.p
@description Make a server call to get the person data

@param ipcPersonID;char;The identifier to retrieve
       iplNeedAttorney;logical;To retrieve Attorney record or not
       iplNeedQualification;logical;To retrieve Qualification record or not
       iplNeedPersonRole;logical;To retrieve PersonRole record or not
       iplNeedAgentPerson;logical;To retrieve AgentPerson record or not
       iplNeedPersonContact;logical;To retrieve PersonContact record or not
@author Sachin chaturvedi
@version 1.0
@created 04/18/2018
@modified
Date          Name             Description
04/28/2020    Shubham          Modified for phase-2.
08/26/2020    VJ               Modified to add get-temp-table.i to parse XML data
12/06/2021    Shefali          Task# 86699 Modified to get fullfilment requirements for person role.
08/16/2022    SA               Task #96812 Modified to add functionality for person.
07/21/2023    SK               Add Agents tab in the person detail screen and make it default from the AMD APP.
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{lib/std-def.i}
{tt/person.i}
{tt/qualification.i}
{tt/personRole.i}
{tt/affiliation.i}
{tt/reqfulfill.i}
{tt/personcontact.i}
{tt/personagentdetails.i}

/* Parameter definitions */
define input  parameter ipcPersonID          as character no-undo.
define input  parameter iplNeedQualification as logical   no-undo.
define input  parameter iplNeedAffiliation   as logical   no-undo.
define input  parameter iplNeedPersonAgent   as logical   no-undo.
define input  parameter iplNeedPersonRole    as logical   no-undo.
define input  parameter iplNeedReqFulfill    as logical   no-undo.
define input  parameter iplNeedPersonContact as logical   no-undo. 
define input  parameter iplNeedPADetails     as logical   no-undo.

define output parameter table for person.    
define output parameter table for qualification.
define output parameter table for affiliation.      
define output parameter table for personrole.
define output parameter table for reqfulfill. 
define output parameter table for personcontact.
define output parameter table for personagentdetails.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p ("",
               "PersonGet",
               "personID="           + (if ipcPersonID <> ? and ipcPersonID <> "" then trim(encodeUrl(ipcPersonID))    else "")   +
               "&needQualification=" + (if iplNeedQualification <> ? then encodeUrl(string(iplNeedQualification))      else "no") +
               "&needPersonRole="    + (if iplNeedPersonRole <> ? then encodeUrl(string(iplNeedPersonRole))            else "no") +
               "&needAffiliation="   + (if iplNeedAffiliation <> ? then encodeUrl(string(iplNeedAffiliation))          else "no") +
               "&needPersonAgent="   + (if iplNeedPersonAgent <> ? then encodeUrl(string(iplNeedPersonAgent))          else "no") + 
               "&needReqFulfill="    + (if iplNeedReqFulfill <> ?     then encodeUrl(string(iplNeedReqFulfill))        else "no") +
               "&needPersonContact="  + (if iplNeedPersonContact <> ?  then encodeUrl(string(iplNeedPersonContact)) else "no") + 
               "&needPAgentsDetail="  + (if iplNeedPADetails <> ?     then encodeUrl(string(iplNeedPADetails)) else "no") ,
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess 
 then 
  return.
  
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
   {lib/get-temp-table.i &t=person}
   {lib/get-temp-table.i &t=qualification}
   {lib/get-temp-table.i &t=personrole}
   {lib/get-temp-table.i &t=affiliation}
   {lib/get-temp-table.i &t=reqfulfill} 
   {lib/get-temp-table.i &t=personcontact} 
   {lib/get-temp-table.i &t=personagentdetails}
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

