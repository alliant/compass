&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name newaudit.p
@description Creates a new audit record in the qar table

@param Year;int;The year of the audit (required)
@param AgentID;char;The agent the audit is regarding (required)
@param Auditor;char;The userid that is conducting the audit (optional)
@param AuditDate;char;The date of the Audit (required)

@returns The audit ID number

@author John Oliver
@version 1.0
@created 11/20/2015
 Modification:
   Date          Name      Description
   03/23/2017    AG        Removed month from the sharefile directory.
   05/18/2017    AC        Modified to handle server errors
   11/20/2017    RS        Modified to send reason code for QUR audits.
   11/20/24      SC        Task #116844 Add last completed audit note while creating/scheduling an audit
----------------------------------------------------------------------*/

def input parameter piYear as int no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pdtAuditDate as datetime no-undo.
def input parameter pcAuditor as char no-undo.
def input parameter pcAuditType as char no-undo.
def input parameter pcErrType as char no-undo.
def input parameter pcResncode as char no-undo.
def output parameter pAuditID as int no-undo.
def output parameter pcLastAuditNote as char no-undo.

{lib/srvdefs.i}
{lib/std-def.i}

/* def var tXmlFile as char no-undo.  */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


date(1, 1, piYear) no-error.
if error-status:error
 then 
  do:
    pSuccess = false.
    pMsg = "The Audit Year is not a valid year".
    return.
  end.
  
/* validate the agent id */
if pcAgentID = ? or pcAgentID = ""
 then
  do:
    pSuccess = false.
    pMsg = "The Agent is not valid".
    return.
  end.
  
/* validate the agent id */
date(pdtAuditDate) no-error.
if error-status:error
 then
  do:
    pSuccess = false.
    pMsg = "The Audit Date is not valid".
    return.
  end.

run httpget.p 
  ("",
   "auditNew",
   "year=" + encodeUrl(string(piYear))
   + "&agentID=" + encodeUrl(pcAgentID)
   + "&auditDate=" + encodeUrl(string(pdtAuditDate))
   + (if pcAuditor <> ? and pcAuditor <> "" then "&auditor=" + encodeUrl(pcAuditor) else "")
   + "&auditType=" + encodeUrl(pcAuditType) 
   + (if pcErrType <> ? and pcErrType <> "" then "&errType=" + encodeUrl(pcErrType) else "")     
   + (if pcResncode <> ? and pcResncode <> "" then "&reasonCode=" + encodeUrl(pcResncode) else "")     
   ,
   output pSuccess,
   output pMsg,
   output tResponseFile).

if not pSuccess 
 then return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 if qName = "parameter"
  then
    do: 
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "auditID" THEN pAuditID = integer(decodeXml(fldvalue)).
          when "LastAuditNote" then pcLastAuditNote = if (fldvalue = ? or fldvalue = "?") then "" else decodeXml(fldvalue).
         END CASE.
        END.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

