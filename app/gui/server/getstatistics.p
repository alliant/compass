&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getstatistics.p
@description Gets the Statistice

@author Rahul
@param ipcExcludeAction;char
@param ipdtStartDate;datetime
@param ipdtEnndDate;datetime

@version 1.0
@created
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/ttstatistics.i}

/* Parameter Definitions */
define input  parameter ipcExcludeAction      as character no-undo.
define input  parameter ipdtStartDate         as datetime  no-undo.
define input  parameter ipdtEndDate           as datetime  no-undo.

define output parameter table for ttstatistics.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 59.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p ("",
               "systemStatistics",
               "ExceptAction="    + (if ipcExcludeAction    <> "" then trim(encodeUrl(ipcExcludeAction))        else "") +
               "&StartDate="      + (if ipdtStartDate       <> ?  then trim(encodeUrl(string(ipdtStartDate)))   else "") +
               "&EndDate="        + (if ipdtEndDate         <> ?  then trim(encodeUrl(string(ipdtEndDate)))     else "") , 
               output pSuccess,
               output pMsg,
               output tResponseFile).
               

if not pSuccess 
 then return.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
 
  if qName = "ttstatistics" then
  do: 
    create ttstatistics.
    do  i = 1 to  attributes:num-items:
      fldvalue = attributes:get-value-by-index(i).
      case attributes:get-qname-by-index(i):
        when "type"              then ttstatistics.type          = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "action"            then ttstatistics.action        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "uName"             then ttstatistics.uName         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "uid"               then ttstatistics.uid           = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "calls"             then ttstatistics.calls         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "error"             then ttstatistics.error         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "avgDuration"       then ttstatistics.avgDuration   = if integer(fldvalue) = ?            then integer("")  else integer(fldvalue) no-error.
        when "noAction"          then ttstatistics.noAction      = if integer(fldvalue) = ?            then integer("")  else integer(fldvalue) no-error.
        when "roles"             then ttstatistics.roles         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "description"       then ttstatistics.description   = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        when "active"            then ttstatistics.active        = if integer(fldvalue) = ?            then integer("")  else integer(fldvalue) no-error.
        when "inactive"          then ttstatistics.inactive      = if integer(fldvalue) = ?            then integer("")  else integer(fldvalue) no-error.
      end case.
    end.
    release ttstatistics.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

