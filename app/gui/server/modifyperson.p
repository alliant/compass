&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name modifyperson.p
@description Modifies existing Person record

@author Rahul Sharma
@version 1.0
@created 10/23/2018
@notes
@param Person; input table to update record in DB
@modified
Date         Name      Description
04/04/2024   SB        Task# 111689 Modified to remove the reference of title
04/05/2024   SR        Task# 111689 Deleted case statement on person.
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/Person.i}

/* Parameter definitions */
define input parameter table for Person.
{lib/srvdefs.i}

/* Local Variables */
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable tNewDir as character no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
std-ch = os-getenv("appdata") + "\Alliant\" + "modifyPerson"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.


xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "person", 'for each person').


xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

/* Server Call */
run httppost.p ("personModify",
                "text/xml",
                search(std-ch),
                output pSuccess,
                output pMsg,
                output tResponseFile).

if not pSuccess 
 then
  return.

empty temp-table person.
  
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

