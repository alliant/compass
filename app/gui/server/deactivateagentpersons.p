&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
deactivateagentpersons.p
MODIFY a notes and date in personagent table
sachin anthwal
08.24.2022
*/

def input  parameter pPersonAgentID as int     no-undo.
def input  parameter pExpDate       as date    no-undo.
def output parameter olpersinactive as logical no-undo.


{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p 
   ("",
    "agentpersonsdeactivate",
    (if string(pPersonAgentID) = ? then '' else "PersonAgentID=" + encodeUrl(string(pPersonAgentID)))
    + (if string(pExpDate) = ? then "" else "&ExpDate=" + encodeUrl(string(pExpDate))),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
 if qName = "parameter"
   then
    do  i = 1 to  attributes:num-items:
      fldvalue = attributes:get-value-by-index(i).
      case attributes:get-qname-by-index(i):
        when "PersonActive" then olpersinactive = logical(fldvalue) no-error.
      end case.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

