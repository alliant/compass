&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*---------------------------------------------------------------------
@name getcplreport.p
@description Server call to get the CPLs by batch number

@param Batch ID;integer;The batch number

@author John Oliver
@version 1.0
@created 07/29/2016
@notes 
---------------------------------------------------------------------*/

{tt/cplreport.i}
{tt/cpl.i}

DEFINE INPUT PARAMETER pBatchId AS INTEGER NO-UNDO.
DEFINE OUTPUT PARAMETER TABLE FOR cplReport.
DEFINE OUTPUT PARAMETER TABLE FOR cpl.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

IF pBatchId = 0 
 then
  do: pSuccess = false.
      pMsg = "Batch ID cannot be zero.".
      return.
  end.

 run httpget.p 
   ("",
    "cplReportByBatch",
    (IF pBatchId = ? THEN "" ELSE "BatchId=" + encodeUrl(STRING(pBatchId))),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 if not pSuccess
  then empty temp-table cpl.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

  CASE qName:
   WHEN "cpl" THEN
    DO:
      CREATE cpl.
      DO i = 1 TO attributes:NUM-ITEMS:
        fldvalue = attributes:GET-VALUE-BY-INDEX(i).
        CASE attributes:GET-QNAME-BY-INDEX(i):
         WHEN "cplID" THEN cpl.cplID = fldvalue.
         WHEN "formID" THEN cpl.formID = fldvalue.
         WHEN "stateID" THEN cpl.stateID = fldvalue.
         WHEN "statCode" THEN cpl.statCode = fldvalue.
         WHEN "agentID" THEN cpl.agentID = fldvalue.
         WHEN "fileNumber" THEN cpl.fileNumber = fldvalue.
         WHEN "cleanFileNumber" THEN cpl.cleanFileNumber = fldvalue.
         WHEN "name" THEN cpl.name = fldvalue.
         WHEN "addr1" THEN cpl.addr1 = fldvalue.
         WHEN "addr2" THEN cpl.addr2 = fldvalue.
         WHEN "addr3" THEN cpl.addr3 = fldvalue.
         WHEN "addr4" THEN cpl.addr4 = fldvalue.
         WHEN "city" THEN cpl.city = fldvalue.
         WHEN "county" THEN cpl.county = fldvalue.
         WHEN "state" THEN cpl.state = fldvalue.
         WHEN "zip" THEN cpl.zip = fldvalue.
         WHEN "stat" THEN cpl.stat = fldvalue.
         WHEN "issueDate" THEN cpl.issueDate = DATETIME(fldvalue) NO-ERROR.
         WHEN "voidDate" THEN cpl.voidDate = DATETIME(fldvalue) NO-ERROR.
         WHEN "closeDate" THEN cpl.closeDate = DATETIME(fldvalue) NO-ERROR.
         WHEN "liabilityAmount" THEN cpl.liabilityAmount = DECIMAL(fldvalue) NO-ERROR.
        END CASE.
      END.
      RELEASE cpl.
    END.
   WHEN "cplReport" THEN
    DO:
      CREATE cplReport.
      DO i = 1 TO attributes:NUM-ITEMS:
        fldvalue = attributes:GET-VALUE-BY-INDEX(i).
        CASE attributes:GET-QNAME-BY-INDEX(i):
         WHEN "batchID" THEN cplReport.batchID = INTEGER(fldvalue) NO-ERROR.
         WHEN "fileNumber" THEN cplReport.fileNumber = fldvalue.
         WHEN "cleanFileNumber" THEN cplReport.cleanFileNumber = fldvalue.
         WHEN "effDate" THEN cplReport.effDate = fldvalue.
         WHEN "policyID" THEN cplReport.policyID = fldvalue.
         WHEN "numberOfForms" THEN cplReport.numberOfForms = INTEGER(fldvalue) NO-ERROR.
         WHEN "numberOfEndorsements" THEN cplReport.numberOfEndorsements = INTEGER(fldvalue) NO-ERROR.
         WHEN "numberOfPolicies" THEN cplReport.numberOfPolicies = INTEGER(fldvalue) NO-ERROR.
         WHEN "numberOfCPLs" THEN cplReport.numberOfCPLs = INTEGER(fldvalue) NO-ERROR.
        END CASE.
      END.
      RELEASE cplReport.
    END.
  END CASE.

END.
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

