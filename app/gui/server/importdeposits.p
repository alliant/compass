/*------------------------------------------------------------------------
@name importdeposits.p
@description Creates new AR payment records
@param ArPmt; input table to create records in DB
@author Vikas Jain
@version 1.0
@created 02/07/20
@notes
@modified
Name       Date          Comments
SRK        05/09/2024    Modified to Production related changes
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/arpmt.i}

/* Parameter definitions */
define input parameter iplProduction   as logical   no-undo.
define input-output  parameter table for ArPmt.
define input parameter iplWarning    as logical   no-undo.

/* Local Variables */
{lib/srvdefs.i}
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable xParameter  as handle    no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-O
   NLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 

/* add to the xml document */
std-ch = os-getenv("appdata") + "\Alliant\" + "importdeposit"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef  xRoot.
create x-noderef xParameter.

xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

xDoc:create-node(xParameter, "Parameter", "ELEMENT").

xParameter:set-attribute("WarningMsg",string(iplWarning)).

xParameter:set-attribute("Production",string(iplProduction)).

xRoot:append-child(xParameter).

appendXml(xRoot, "ArPmt", 'for each ArPmt').

xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

run httppost.p 
   ("depositsImport",
    "text/xml",
    search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess then
  return.

if pSuccess then
  empty temp-table ArPmt.

pSuccess = parseXML(output pMsg).

&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
procedure startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  if qName = "errarpmt"
   then
    do: 
      create arpmt.
      DO  i = 1 TO  attributes:NUM-ITEMS:
        fldvalue = attributes:get-value-by-index(i).
        CASE attributes:get-qname-by-index(i):                    
          when "entityID"       then arpmt.entityID        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "checknum"       then arpmt.checknum        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "receiptdate"    then arpmt.receiptdate     = if datetime(fldvalue) = ?           then  datetime(?) else datetime(decodexml(fldvalue)).                             
          when "checkamt"       then arpmt.checkamt        = if decimal(fldvalue) = ?            then 0            else decimal(decodexml(fldvalue)).                    
          when "filenumber"     then arpmt.filenumber      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).                    
          when "entityname"     then arpmt.entityname      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue) no-error.                    
          when "depositRef"     then arpmt.depositRef      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
          when "errorMsg"       then arpmt.errorMsg        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).          
        END CASE.
       END.
       release arpmt.
     end. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
