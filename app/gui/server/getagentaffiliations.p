&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name getagentaffiliations.p
@description Make a server call to get the data for peers of an agent role

@param stateID;char;The state of agent role
@param AgentID;char;

@author Sachin Chaturvedi
@version 1.0
@created 04/17/2020
@modified
date          Name           description
----------------------------------------------------------------------*/

{lib/std-def.i}

/* Temp-Table Definitions */
{tt/affiliation.i}

/* Parameter definitions */
define input  parameter ipcStateID           as character no-undo.
define input  parameter ipcAgentID           as character no-undo.
define output parameter table for affiliation.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p ("",
               "agentAffiliationsGet",
               "stateID="            + (if ipcStateID <> ""   then trim(encodeUrl(ipcStateID)) else "")   +
               "&agentID="           + (if ipcAgentID  <> ""  then trim(encodeUrl(ipcAgentID)) else "") ,
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess 
 then 
  return.
  
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  case qName:
   when "affiliation"
     then
      do:
        create affiliation.
        do i = 1 to attributes:num-items:
          fldvalue = attributes:get-value-by-index(i).
          case attributes:get-qname-by-index(i):
            when "affiliationID" then affiliation.affiliationID = if integer(fldvalue) = ?            then 0            else integer(fldvalue)    no-error.
            when "partnerA"      then affiliation.partnerA      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "partnerAType"  then affiliation.partnerAType  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "partnerB"      then affiliation.partnerB      = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "partnerBType"  then affiliation.partnerBType  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "natureOfAtoB"  then affiliation.natureOfAtoB  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "natureOfBtoA"  then affiliation.natureOfBtoA  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "isCtrlPerson"  then affiliation.isCtrlPerson  = if logical(fldvalue) = ?            then no           else logical(fldvalue)   no-error.
            when "notes"         then affiliation.notes         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue) no-error.           
            when "effdate"       then affiliation.effdate       = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue)  no-error.
            when "expDate"       then affiliation.expDate       = if datetime(fldvalue) = ?           then datetime("") else datetime(fldvalue)  no-error.
            when "partnerAName"  then affiliation.partnerAName  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "partnerBName"  then affiliation.partnerBName  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "tag"           then affiliation.tag           = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "email"         then affiliation.email         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
            when "phone"         then affiliation.phone         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          end case.
        end.
      end.
        
  end case.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

