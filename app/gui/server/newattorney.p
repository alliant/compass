&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name newAttorney.p
@description Creates new attorney

@author Naresh Chopra
@version 1.0
@created 04/19/2018
@notes
@param attorney; input table to create record in DB
@param personId; input id to crreate record or personrole as attorney for perosn
@Modified
Date         Name               Description
04/13/20     Shubham            Added new parameter
12/06/21     Shefali            Task# 86699 Added output parameter for new structure. 
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/attorney.i}

/* Parameter definitions */
define input  parameter table for attorney.
define output parameter opcAttorneyID  as character no-undo.
define output parameter opcOrgID       as character no-undo.
define output parameter opcPersonID    as character no-undo.
define output parameter opcComStatus   as character no-undo.
define output parameter opdtCreateDate as datetime  no-undo.

{lib/srvdefs.i}

/* Local Variables */
define variable xDoc    as handle    no-undo.
define variable xRoot   as handle    no-undo.
define variable tNewDir as character no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
std-ch = os-getenv("appdata") + "\Alliant\" + "newAttorney"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.


xDoc:create-node(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

appendXml(xRoot, "attorney", 'for each attorney').

xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.

/* Server Call */

run httppost.p ("attorneyNew",
                "text/xml",
                search(std-ch),
                output pSuccess,
                output pMsg,
                output tResponseFile).

if not pSuccess 
 then
  return.

empty temp-table attorney.

pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}

  if qName = "parameter" 
   then
    do i = 1 to attributes:num-items:
      fldvalue = attributes:get-value-by-index(i).
      case attributes:get-qname-by-index(i):
        when "attorneyID" then opcAttorneyID  = decodeXml(fldvalue) no-error.
        when "orgID"      then opcOrgID       = decodeXml(fldvalue) no-error.
        when "personID"   then opcPersonID    = decodeXml(fldvalue) no-error.
        when "ComStatus"  then opcComStatus   = decodeXml(fldvalue) no-error.       
        when "createDate" then opdtCreateDate = if datetime(fldvalue) = ? then datetime("") else datetime(fldvalue) no-error.       
      end case.
    end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

