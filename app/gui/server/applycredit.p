&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
@name applycredit.p
@description Apply credit to corrosponding invoice

@author Anjly    
@version 1.0
@created 08/30/2019
@notes
@param arpmtID; input parameter for ArTran id
       artran; input-output ArTran to create/update records in DB
@modified  
Date         Name              Description
----------------------------------------------------------------------*/
/* Temp-Table Definitions */
{tt/artran.i}

/* Parameter definitions */
define input parameter  ipcArTranID  as character no-undo.
define input parameter  iplValidate  as logical   no-undo.
define input parameter  table        for artran.

{lib/srvdefs.i}

/* Local Variables */
define variable xDoc        as handle    no-undo.
define variable xRoot       as handle    no-undo.
define variable tNewDir     as character no-undo.
define variable xParameter  as handle    no-undo.

{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */    
/* add to the xml document */
std-ch = os-getenv("appdata") + "\Alliant\" + "applycredit"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create x-document xDoc.
create x-noderef xRoot.
create x-noderef xParameter.

xDoc:create-node(xRoot, "DATA", "ELEMENT").
xDoc:append-child(xRoot).
xDoc:create-node(xParameter, "Parameter", "ELEMENT").
xParameter:set-attribute("artranID",ipcArTranID).
xParameter:set-attribute("ValidateFlag" ,string(iplValidate)).
xRoot:append-child(xParameter).
appendXml(xRoot, "artran", 'for each artran').
xDoc:save("file", std-ch).

delete object xRoot.
delete object xDoc.
    
/* Server Call */
run httppost.p
   ("creditApply",
    "text/xml",
    search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).


if not pSuccess then
  return.
 
if pSuccess 
 then
  empty temp-table artran.


pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/  
  {lib/srvstartelement.i}
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

