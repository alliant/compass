&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file deleteclaimlink.p
@description Makes a server call to delete a claim relation

@param FromClaimID;int;Claim number
@param ToClaimID;int;Claim number

@returns claimlink;table;the claimlink table

@author John Oliver
@created XX.XX.2015
@notes
----------------------------------------------------------------------*/

def input parameter pFromClaimID as int no-undo.
def input parameter pToClaimID as int no-undo.

{lib/srvdefs.i}

def var sQuery as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
 /* the from claim ID must be there */
 if pFromClaimID = ? or pFromClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "From Claim ID cannot be zero or unknown".
        return.
    end.
 sQuery = "FromClaimID=" + encodeURL(string(pFromClaimID)).
 
 /* add the toClaimID */
 if pToClaimID = ? or pToClaimID = 0
  then
    do: pSuccess = false.
        pMsg = "From Claim ID cannot be zero or unknown".
        return.
    end.
  sQuery = sQuery + "&ToClaimID=" + encodeUrl(string(pToClaimID)).
    
 run httpget.p 
   ("",
    "claimLinkDelete",
    sQuery,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

