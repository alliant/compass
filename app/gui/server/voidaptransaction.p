&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*----------------------------------------------------------------------
@file appTemplateModify.p
@description Makes a server call to void the transaction

@param ID;int;The invoice ID
@param Notes;char
@param RefType;char

@author John Oliver
@version 1.0
@created XX.XX.20XX
----------------------------------------------------------------------*/

{tt/aptrx.i}.
def input parameter pID as int no-undo.
def input parameter pNote as char no-undo.
{lib/srvdefs.i}

def var cAction as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

if pID = ? or pID = 0
 then
  do: pSuccess = false.
      pMsg = "Transaction ID cannot be unknown or zero.".
      return.
  end.

if pNote = ? or pNote = ""
 then
  do: pSuccess = false.
      pMsg = "Note cannot be unknown or blank when voiding a transaction.".
      return.
  end.
  
run httpget.p 
   ("",
    "apTransactionVoid",
    "ID=" + encodeUrl(string(pID))
    + "&note=" + encodeUrl(pNote)
    ,
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

