&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/getperiodstatestatcodes.p
   GET data aggregated by period/state/type/statcode
   Used by OPS16 report
   Created 12.14.2014 D.Sinclair

   Parameter:  Month (integer 1 to 12)
               Year (four-digit year > 2000)
               State (optional; blank = all)
*/

{tt/periodstatestatcode.i &tableAlias=ops16}

def input parameter pMonth as int no-undo.
def input parameter pYear as int no-undo.
def input parameter pState as char no-undo.
def output parameter table for ops16.

{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


if pMonth < 1 or pMonth > 12
 then
  do: pMsg = "Month must be between 1 (Jan) and 12 (Dec)".
      return.
  end.

if pYear < 2000 or pYear > 2999 
 then
  do: pMsg = "Year must be between 2000 and 2999".
      return.
  end.

if pState = ?
 then pState = "".

 run httpget.p 
   ("",
    "PeriodStateStatCodesGet",
    "month=" + encodeUrl(string(pMonth))
    + "&year=" + encodeUrl(string(pYear))
    + (if pState > "" then "&state=" + encodeUrl(pState) else ""),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer ops16 for ops16.
 
 if qName = "data"
  then
    do: create ops16.

        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
             when "stateID" THEN ops16.stateID = fldvalue no-error.
             when "formType" THEN ops16.formType = fldvalue no-error.
             when "statCode" THEN ops16.statCode = decodeXml(fldvalue).
             when "description" then ops16.description = decodeXml(fldvalue).
             when "cnt" then ops16.cnt = integer(fldvalue) no-error.
             when "grossPremium" THEN ops16.grossPremium = decimal(fldvalue) no-error.
             when "retainedPremium" THEN ops16.retainedPremium = decimal(fldvalue) no-error.
             when "netPremium" THEN ops16.netPremium = decimal(fldvalue) no-error.
             when "liabilityAmount" THEN ops16.liabilityAmount = decimal(fldvalue) no-error.
             when "liabilityDelta" THEN ops16.liabilityDelta = decimal(fldvalue) no-error.
             when "grossDelta" THEN ops16.grossDelta = decimal(fldvalue) no-error.
             when "retainedDelta" THEN ops16.retainedDelta = decimal(fldvalue) no-error.
             when "netDelta" THEN ops16.netDelta = decimal(fldvalue) no-error.
         END CASE.
        END.
        release ops16.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

