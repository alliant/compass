/*------------------------------------------------------------------------
@name getFindings.p
@description Gets a list of the audit findings

@param Year;int;The year of the audit (required)
@param auditID;int;The Audit ID (optional)
@param State;char;The state the audit is in (optional)
@param AgentID;char;The agent the audit is regarding (optional)
@param UID;char;The userid that is conducting the audit (optional)

@returns Audit;complex;The audit finding table
@author Anjly Chanana
@version 1.0
@created 06/23/2017
@modified 07/15/2021 - SA  - Modified to use get-temp-table.i 
                             to parse XML data 
          29/07/2022 - SD  - Task#95509: Modified to support clob related changes
          02/03/2023 - SR  - Task #: 102000 :Changed the call of httpcall as per new framework
----------------------------------------------------------------------*/
{tt/findingreport.i &tableAlias="qarfinding"}

def input parameter piYear    as int no-undo.
def input parameter piAuditID as int no-undo.
def input parameter pcState   as char no-undo.
def input parameter pcAgentID as char no-undo.
def input parameter pcUid     as char no-undo.
def output parameter table for qarfinding.

{lib/srvdefs.i}
{lib/rpt-defs.i}


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 14.81
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run util/httpcall.p (input "get",
                     input "DO",
                     input "auditFindingsGet",
                     input "",      /* content type */
                     input "",      /* data file */
                     input "year=" + encodeUrl(string(piYear))
                       + (if piAuditID <> ? and piAuditID <> 0 then "&AuditID=" + encodeUrl(string(piAuditID)) else "")
                       + (if pcState <> ? and pcState <> "ALL" then "&State=" + encodeUrl(pcState) else "")
                       + (if pcAgentID <> ? and pcAgentID <> "ALL" then "&Agent=" + encodeUrl(pcAgentID) else "")
                       + (if pcUid <> ? and pcUid <> "ALL" then "&Auditor=" + encodeUrl(pcUid) else ""),
                     {lib/rpt-setparams.i},
                     output pSuccess,
                     output pMsg,
                     output clientResponse).

if not pSuccess 
 then 
  return.

if valid-object(clientResponse) 
 then
  tResponseFile = clientResponse:bodyFilePath.
 
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 def buffer qarfinding for qarfinding .
  
 {lib/get-temp-table.i &t=qarfinding &setParam="'finding'"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
