&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
srvnewpolicy.p
create a NEW POLICY by calling the SeRVer action
D.Sinclair
2.16.2014
*/

{tt/policy.i}
def input-output parameter table for policy.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


find first policy
  where policy.policyID > 0 no-error.
if not available policy
 then
  do: pSuccess = false.
      pMsg = "Policy structure cannot be blank.".
      return.
  end.

if policy.agentID = "" or policy.agentID = ?
 then
  do: pSuccess = false.
      pMsg = "Agent ID cannot be blank or unknown.".
      return.
  end.

if policy.formID = "" or policy.formID = ?
 then
  do: pSuccess = false.
      pMsg = "Form ID cannot be blank or unknown.".
      return.
  end.

 run httpget.p 
   ("",
    "policyCreate",
    "policyID=" + string(policy.policyID)
    + "&agentID=" + encodeUrl(policy.agentID)
    + "&formID=" + encodeUrl(policy.formID)
    + "&fileNumber=" + encodeUrl(policy.fileNumber)
    + "&liability=" + string(policy.liability)
    + "&residential=" + (if policy.residential then "T" else "F")
    + "&grossPremium=" + string(policy.grossPremium)
    + (if policy.effDate = ? then "" else "&effDate=" + encodeUrl(string(policy.effDate))),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

