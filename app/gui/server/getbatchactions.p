&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*
srvgetbatchactions.p
GET BATCH ACTIONS for a batch by calling the SeRVer action
D.Sinclair
2.16.2014
*/

{tt/batchaction.i}

def input parameter pBatchID as int no-undo.
def output parameter table for batchaction.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

if pBatchID = 0 or pBatchID = ?
 then
  do: pSuccess = false.
      pMsg = "Batch ID cannot be zero or unknown.".
      return.
  end.
  
 run httpget.p 
   ("",
    "batchActionsGet",
    "batchID=" + encodeUrl(string(pBatchID)),
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

 /* Ensure data integrity */
 for each batchaction
   where batchaction.batchID <> pBatchID:
  delete batchaction.
 end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer batchaction for batchaction.
 
 if qName = "BatchAction"
  then
    do: create batchaction.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "batchID" THEN batchaction.batchID = int(decodeXml(fldvalue)).
          when "seq" THEN batchaction.seq = int(fldvalue) no-error.
          when "postDate" then batchaction.postDate = datetime(fldvalue) no-error.
          when "postUserID" then batchaction.postUserID = decodeXml(fldvalue).
          when "postInitials" then batchaction.postInitials = decodeXml(fldvalue).
          when "comments" then batchaction.comments = decodeXml(fldvalue).
          when "secure" then batchaction.secure = logical(decodeXml(fldvalue)) no-error.
         END CASE.
        END.
        release batchaction.
    end.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

