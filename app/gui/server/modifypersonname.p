/*------------------------------------------------------------------------
@name modifypersonname.p
@description modify person name
@param personid,firstname,lastname,middlename,title,displayname
@returns Success;log;Success indicator
@author Sagar Koralli
@version 
@notes
@modified
Date         Name      Description
04/04/2024   SB        Task# 111689 Modified to remove the reference of title
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameter definitions */
def input  parameter  pcFirstname       as char .
def input  parameter  pcPersonID        as char .
def input  parameter  pcLastname        as char .
def input  parameter  pcMiddlename      as char .
def input  parameter  pcDisplayname     as char .

{lib/srvdefs.i}

/* ***************************  Main Block  *************************** */
/* Server Call */
run httpget.p ("",
               "personNameModify",
               "Firstname="   + encodeUrl(pcFirstname) +
               "&Lastname="    + encodeUrl(pcLastname) +
               "&Middlename="  + encodeUrl(pcMiddlename) +
               "&PersonID="    + encodeUrl(pcPersonID) +
               "&Displayname=" + encodeUrl(pcDisplayname),
               output pSuccess,
               output pMsg,
               output tResponseFile).

if not pSuccess then
  return.

pSuccess = parseXML(output pMsg).

procedure startElement:
 {lib/srvstartelement.i}
end procedure.



