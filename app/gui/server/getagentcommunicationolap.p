/*------------------------------------------------------------------------
@name getagentcommunicationolap.p
@description get agent communication olap
@param table agentcommunication
@returns Success;log;Success indicator
@author Sagar Koralli
@version 
@notes
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
/* Temp-Table Definitions */
{tt/agentcommunication.i}

/* Parameter definitions */
define output  parameter table for agentcommunication.
{lib/srvdefs.i}
{lib/rpt-defs.i}

{lib/compass-dataset-def.i &buffers="buffer agentcommunication:handle"}

/* ***************************  Main Block  *************************** */

run util/httpcall.p (input  "get",
                     input  "doweb",
                     input  "agentCommunicationolapGet",
                     input  "",
                     input  "",
                     input  "",
                     {lib/rpt-setparams.i},
                     output pSuccess,
                     output pMsg,
                     output clientResponse).

if not pSuccess
 then return.
 
pSuccess = loadDataset(output pMsg).
