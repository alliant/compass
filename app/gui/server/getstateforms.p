&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/getstateforms.p
@description GET all STATE FORMS by calling the SeRVer action
@author D.Sinclair
@created 2.4.2014
@notes
@param pStateID    Pass blank to get all states
@param pFormID     Pass blank for all stateforms for a specific state or all states
Modification:
   Date          Name        Description     
   02/08/2019    Vikas Jain  Modified to parse revenuyType for stateForm.
*/

{tt/stateform.i}
def input parameter pStateID as char no-undo.
def input parameter pFormID as char no-undo.
def output parameter table for stateform.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

 std-ch = "".
 if pStateID <> ? and pStateID > "" 
  then std-ch = "stateID=" + encodeUrl(pStateID).
 if pFormID <> ? and pFormID > "" 
  then std-ch = std-ch + (if std-ch > "" then "&" else "")
                  + "formID=" + encodeUrl(pFormID).

 run httpget.p 
   ("",
    "stateFormsGet",
    std-ch,
    output pSuccess,
    output pMsg,
    output tResponseFile).

 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}

 def buffer stateform for stateform.

 if qName = "stateform"
  then
    do: create stateform.
        DO  i = 1 TO  attributes:NUM-ITEMS:
         fldvalue = attributes:get-value-by-index(i).
         CASE attributes:get-qname-by-index(i):
          when "stateID" THEN stateform.stateID = decodeXml(fldvalue).
          when "formID" then stateform.formID = decodeXml(fldvalue).
          when "active" then stateform.active = (if fldvalue = "no" then false else true).
          when "effDate" then stateform.effDate = date(fldvalue) no-error.
          when "fileDate" then stateform.fileDate = date(fldvalue) no-error.
          when "minGross" then stateform.minGross = decimal(fldvalue) no-error.
          when "maxGross" then stateform.maxGross = decimal(fldvalue) no-error.
          when "formType" then stateform.formType = decodeXml(fldvalue).
          when "rateCheck" then stateform.rateCheck = fldvalue.
          when "rateMin" then stateform.rateMin = decimal(fldvalue) no-error.
          when "rateMax" then stateform.rateMax = decimal(fldvalue) no-error.
          when "description" then stateform.description = decodeXml(fldvalue).
          when "formCode" then stateform.formCode = decodeXml(fldvalue).
          when "insuredType" then stateform.insuredType = decodeXml(fldvalue).
          when "orgName" then stateform.orgName = decodeXml(fldvalue).
          when "orgRev" then stateform.orgRev = decodeXml(fldvalue) no-error.
          when "orgRelDate" then stateform.orgRelDate = date(fldvalue) no-error.
          when "orgEffDate" then stateform.orgEffDate = date(fldvalue) no-error.
          when "revenueType" then stateform.revenueType = (if fldvalue = "?" then "" else fldvalue).
         END CASE.
        END.
        release stateform.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

