&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* server/getsysindexes.p
Get Indexes (sysIndex table records) by calling the Server action
Created 27.09.2018 Rahul Sharma
*/

{tt/sysindex.i}

define input parameter ipcObjType as character no-undo.
define input parameter ipcObjID   as character no-undo.
define output parameter table     for sysindex.
{lib/srvdefs.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


run httpget.p 
   ("",
    "SysIndexesGet",
    (if ipcObjType <> ? and ipcObjType <> "" then "ObjType="  + trim(encodeUrl(ipcObjType))  else "ObjType=") +
    (if ipcObjID   <> ? and ipcObjID   <> "" then "&ObjId="   + trim(encodeUrl(ipcObjID))    else "&ObjId="),
    output pSuccess,
    output pMsg,
    output tResponseFile).
    
 if not pSuccess 
  then return.
 pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     SAX Parser standard interface method
  Parameters:  
------------------------------------------------------------------------------*/
 {lib/srvstartelement.i}
 
 if qName = "ttSysIndex"
  then
    do: 
      create sysindex.
      do  i = 1 to attributes:num-items:
        fldvalue = attributes:get-value-by-index(i).
        case attributes:get-qname-by-index(i):
          when "objID"          then sysindex.objID         = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "objType"        then sysindex.objType       = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "objAttribute"   then sysindex.objAttribute  = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
          when "objKey"         then sysindex.objKey        = if (fldvalue = "?" or fldvalue = ?) then ""           else decodexml(fldvalue).
        end case.
      end.
      release sysindex.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

