/*------------------------------------------------------------------------
@name newcontactsysdest.p
@description create system destinations.      

@param sysdest, entityId; input table and variable to create records in DB
@author AG
@version 1.0
@created 07/24/20
@notes

----------------------------------------------------------------------*/
{tt/sysdest.i}

define input parameter ipcEntityId as character  no-undo.
define input parameter table       for sysdest.
define output parameter opiDestID  as integer    no-undo.

{lib/srvdefs.i}

/* Local Variables ---                                                  */
define variable xDoc as handle no-undo.
define variable xRoot as handle no-undo.
define variable xParameter  as handle    no-undo.

/* Functions and Procedures ---                                         */
{lib/xmlencode.i}
{lib/tt-xml.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

std-ch = os-getenv("appdata") + "\Alliant\" + "NewContactSysDest"  + "-" +  replace(string(today,"99/99/9999"),"/","-") + "-" + replace(string(time,"HH:MM:SS AM"),":","-") + ".xml".
create X-DOCUMENT xDoc.
create x-noderef xRoot.
create x-noderef xParameter.

xDoc:CREATE-NODE(xRoot, "data", "ELEMENT").
xDoc:append-child(xRoot).

xDoc:create-node(xParameter, "Parameter", "ELEMENT").

xParameter:set-attribute("EntityID",string(ipcEntityId)).

xRoot:append-child(xParameter).

appendXml(xRoot, "SysDest", 'for each sysdest').

xDoc:save("file", std-ch).
delete object xRoot.
delete object xDoc.
  
run httppost.p 
   (input  "contactSystemDestinationNew",
    input  "text/xml",
    input  search(std-ch),
    output pSuccess,
    output pMsg,
    output tResponseFile).

if not pSuccess 
 then return.
pSuccess = parseXML(output pMsg).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-startElement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startElement Procedure 
PROCEDURE startElement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/srvstartelement.i}
  
  if qName = "parameter"
   then
    do:
     do i = 1 to attributes:num-items:
       fldvalue = attributes:get-value-by-index(i).
       case attributes:get-qname-by-index(i):
         when "DestID" then opiDestID = int(fldvalue) no-error.         
       end case.
     end.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
