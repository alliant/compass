&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fFrameWin 
/*------------------------------------------------------------------------
  File: toolbar.w (smart frame)
  Description: 14-button Toolbar
  
  Usage:  
  1. Drop this toolbar.w as a smart object in a smart container.
  2. Identify the handle assigned to the smart frame, default: h_toolbar
  3. Call add* procedures in h_toolbar in the main block of the container:
      run addTransactionSet in h_toolbar.
      run addCancelButton in h_toolbar.
      run addButton in h_toolbar (...).
  4. Buttons are added in the order created--no override.
  5. Create internal procedure that are public in the container that follow
     the naming convention of the action OR subscribe to the named events.
     For example, if you don't want an internal procedure called "add" and 
     instead have a public internal procedure called addItem, and you are 
     using the default transaction set of buttons, add the following
     subscription in the main block of the smart container:
       subscribe this-procedure to "add" in h_toolbar run-procedure "addItem".
  6. Call enableActions and disableActions as desired.  Standard enable and
     disable activity is performed prior to a publish.  This may result in 
     flashing.  To prevent flashing, define the buttons manually using
     addButton and disable default sensitivity behaviour.     
------------------------------------------------------------------------*/

CREATE WIDGET-POOL.

{src/adm2/widgetprto.i}

def var tToolbarActions as char init ",,,,,,,,,,,,,," no-undo.
def var tLastButtonUsed as int init 0 no-undo.
def var std-ch as char no-undo.
def var std-in as int no-undo.
def var std-lo as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartFrame
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fToolbar

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B1  NO-FOCUS
     LABEL "1" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B10  NO-FOCUS
     LABEL "10" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B11  NO-FOCUS
     LABEL "11" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B12  NO-FOCUS
     LABEL "12" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B13  NO-FOCUS
     LABEL "13" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B14  NO-FOCUS
     LABEL "14" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B15  NO-FOCUS
     LABEL "15" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B2  NO-FOCUS
     LABEL "2" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B3  NO-FOCUS
     LABEL "3" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B4  NO-FOCUS
     LABEL "4" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B5  NO-FOCUS
     LABEL "5" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B6  NO-FOCUS
     LABEL "6" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B7  NO-FOCUS
     LABEL "7" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B8  NO-FOCUS
     LABEL "8" 
     SIZE 7.2 BY 1.71.

DEFINE BUTTON B9  NO-FOCUS
     LABEL "9" 
     SIZE 7.2 BY 1.71.

DEFINE RECTANGLE rButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 109.4 BY 2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fToolbar
     B1 AT ROW 1.14 COL 1.6 WIDGET-ID 2
     B15 AT ROW 1.14 COL 102.4 WIDGET-ID 32
     B14 AT ROW 1.14 COL 95.2 WIDGET-ID 30
     B11 AT ROW 1.14 COL 73.6 WIDGET-ID 24
     B12 AT ROW 1.14 COL 80.8 WIDGET-ID 26
     B13 AT ROW 1.14 COL 88 WIDGET-ID 28
     B10 AT ROW 1.14 COL 66.4 WIDGET-ID 20
     B2 AT ROW 1.14 COL 8.8 WIDGET-ID 4
     B3 AT ROW 1.14 COL 16 WIDGET-ID 6
     B4 AT ROW 1.14 COL 23.2 WIDGET-ID 8
     B5 AT ROW 1.14 COL 30.4 WIDGET-ID 10
     B6 AT ROW 1.14 COL 37.6 WIDGET-ID 12
     B7 AT ROW 1.14 COL 44.8 WIDGET-ID 14
     B8 AT ROW 1.14 COL 52 WIDGET-ID 16
     B9 AT ROW 1.14 COL 59.2 WIDGET-ID 18
     rButtons AT ROW 1 COL 1 WIDGET-ID 22
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER NO-HELP 
         NO-LABELS NO-UNDERLINE THREE-D NO-AUTO-VALIDATE 
         AT COL 1 ROW 1 SCROLLABLE  WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartFrame
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target
   Other Settings: PERSISTENT-ONLY APPSERVER
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW fFrameWin ASSIGN
         HEIGHT             = 2.24
         WIDTH              = 128.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB fFrameWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW fFrameWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fToolbar
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME fToolbar:SCROLLABLE       = FALSE
       FRAME fToolbar:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON B1 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B1:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B10 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B10:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B11 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B11:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B12 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B12:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B13 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B13:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B14 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B14:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B15 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B15:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B2 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B2:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B3 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B3:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B4 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B4:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B5 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B5:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B6 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B6:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B7 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B7:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B8 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B8:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR BUTTON B9 IN FRAME fToolbar
   NO-ENABLE                                                            */
ASSIGN 
       B9:HIDDEN IN FRAME fToolbar           = TRUE.

/* SETTINGS FOR RECTANGLE rButtons IN FRAME fToolbar
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fToolbar
/* Query rebuild information for FRAME fToolbar
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME fToolbar */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B1 fFrameWin
ON CHOOSE OF B1 IN FRAME fToolbar /* 1 */
or "choose" of b2
 or "choose" of b3
 or "choose" of b4
 or "choose" of b5
 or "choose" of b6
 or "choose" of b7
 or "choose" of b8
 or "choose" of b9
 or "choose" of b10
 or "choose" of b11
 or "choose" of b12
 or "choose" of b13
 or "choose" of b14
 or "choose" of b15
DO:
 run buttonClicked in this-procedure
   (input entry(1, self:private-data),
    input entry(2, self:private-data)).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fFrameWin 


/* ***************************  Main Block  *************************** */


&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN
   /* Now enable the interface  if in test mode - otherwise this happens when
      the object is explicitly initialized from its container. */
   RUN initializeObject.
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAttachButton fFrameWin 
PROCEDURE addAttachButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("attach", "Attachments", "Attachments", "images/attach.bmp", "", "images/attach-i.bmp", 
    false, true, source-procedure, pMenuWidget).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addButton fFrameWin 
PROCEDURE addButton :
/*------------------------------------------------------------------------------
  Purpose:     Visualize the next button in the toolbar.
  
  Parameters:  Action:  named action published back to controller on click
               Label:   <see button widget definition>
               Tooltip: <see button widget definition>
               ImageUp: <see button widget definition>
               ImageDown: <see button widget definition>
               ImageInsensitive: <see button widget definition>
               Enable:  Starting state of the button
               Default: Should related actions be disabled/enabled on click
               Subscriber: Handle of procedure to receive the event (optional)
               Widget:  Handle of a widget that support sensitive attribute (optional)
               
  Notes:       There is currently a 14 button limit.
  
               Although the action doesn't need to be unique, in the default 
               click handler, only the first button for an action will have
               its sensitive state change. 
------------------------------------------------------------------------------*/
 def input parameter pAction as char.
 def input parameter pLabel as char.
 def input parameter pTooltip as char.
 def input parameter pImageUp as char.
 def input parameter pImageDown as char.
 def input parameter pImageInsensitive as char.
 def input parameter pEnable as logical.
 def input parameter pDefaultBehavior as logical.
 def input parameter pSubscriber as handle.
 def input parameter pMenuItem as handle.

 def var thisButton as widget-handle no-undo.
 
 if tLastButtonUsed = 15 
  then return.
 tLastButtonUsed = tLastButtonUsed + 1.

 case tLastButtonUsed:
  when 1 then thisButton = b1:handle in frame fToolbar.
  when 2 then thisButton = b2:handle in frame fToolbar.
  when 3 then thisButton = b3:handle in frame fToolbar. 
  when 4 then thisButton = b4:handle in frame fToolbar. 
  when 5 then thisButton = b5:handle in frame fToolbar. 
  when 6 then thisButton = b6:handle in frame fToolbar. 
  when 7 then thisButton = b7:handle in frame fToolbar. 
  when 8 then thisButton = b8:handle in frame fToolbar. 
  when 9 then thisButton = b9:handle in frame fToolbar. 
  when 10 then thisButton = b10:handle in frame fToolbar. 
  when 11 then thisButton = b11:handle in frame fToolbar. 
  when 12 then thisButton = b12:handle in frame fToolbar. 
  when 13 then thisButton = b13:handle in frame fToolbar. 
  when 14 then thisButton = b14:handle in frame fToolbar. 
  when 15 then thisButton = b15:handle in frame fToolbar. 
 end case.


 std-ch = search(pImageUp).
 if std-ch <> ? 
  then thisButton:load-image-up(std-ch).

 std-ch = search(pImageDown).
 if std-ch <> ? 
  then thisButton:load-image-down(std-ch).

 std-ch = search(pImageInsensitive).
 if std-ch <> ? 
  then thisButton:load-image-insensitive(std-ch).
  
 assign
  thisButton:label = pLabel
  thisButton:tooltip = pTooltip
  thisButton:hidden = false
  thisButton:sensitive = pEnable
  rButtons:width-pixels = min((tLastButtonUsed * 37) + (7 - tLastButtonUsed),
                              frame fToolbar:width-pixels)
  .

 thisButton:private-data = 
  pAction 
  + ","
  + (if pDefaultBehavior then "Y" else "N")
  + ","
  + (if valid-handle(pMenuItem) then "Y" else "N")
  + ","
  + (if valid-handle(pMenuItem) then string(pMenuItem) else "")
  + ","
  .

 entry(tLastButtonUsed, tToolbarActions) = pAction.

 if valid-handle(pSubscriber)
    and lookup("Action" + pAction, pSubscriber:internal-entries) > 0
  then subscribe procedure pSubscriber to ("Action" + pAction) in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addCancelButton fFrameWin 
PROCEDURE addCancelButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("cancel", "Cancel", "Cancel", "images/cancel.bmp", "", "images/cancel-i.bmp", 
    false, true, source-procedure, pMenuWidget).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addCloseButton fFrameWin 
PROCEDURE addCloseButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("close", "Close", "Close", "images/exit.bmp", "", "images/exit-i.bmp", 
    false, true, source-procedure, pMenuWidget).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addCommentButton fFrameWin 
PROCEDURE addCommentButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("comment", "Comment", "Comment", "images/comment.bmp", "", "images/comment-i.bmp", 
    false, true, source-procedure, pMenuWidget).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addCopyButton fFrameWin 
PROCEDURE addCopyButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("copy", "Copy", "Copy", "images/copy.bmp", "", "images/copy-i.bmp",
     true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addDeleteButton fFrameWin 
PROCEDURE addDeleteButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("delete", "Delete", "Delete", "images/delete.bmp", "", "images/delete-i.bmp",
     true, true, source-procedure, pMenuWidget).              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addExitButton fFrameWin 
PROCEDURE addExitButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("exit", "Exit", "Exit", "images/exit.bmp", "", "images/exit-i.bmp", 
    false, true, source-procedure, pMenuWidget).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addFilterButton fFrameWin 
PROCEDURE addFilterButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("filter", "Filter", "Filter", "", "", "", true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addFindButton fFrameWin 
PROCEDURE addFindButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("find", "Find", "Find", "images/find.bmp", "", "images/find-i.bmp",
     true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addFirstButton fFrameWin 
PROCEDURE addFirstButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("first", "First", "First", "images/first.bmp", "", "images/first-i.bmp",
     true, true, source-procedure, pMenuWidget).                      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addHelpButton fFrameWin 
PROCEDURE addHelpButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("help", "Help", "Help", "images/help.bmp", "", "images/help.bmp", 
    false, true, source-procedure, pMenuWidget).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addLastButton fFrameWin 
PROCEDURE addLastButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("last", "Last", "Last", "images/last.bmp", "", "images/last-i.bmp",
     true, true, source-procedure, pMenuWidget).              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addModifyButton fFrameWin 
PROCEDURE addModifyButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as handle.
 run addButton in this-procedure
   ("modify", "Modify", "Modify", "images/update.bmp", "", "images/update-i.bmp",
     true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addNavigationButtons fFrameWin 
PROCEDURE addNavigationButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run addButton in this-procedure
   ("first", "First", "First", "images/first.bmp", "", "images/first-i.bmp", true, true, source-procedure, ?).                      
 run addButton in this-procedure
   ("previous", "Previous", "Previous", "images/previous-i.bmp", "", "", true, true, source-procedure, ?).       
 run addButton in this-procedure
   ("next", "Next", "Next", "images/next.bmp", "", "images/next-i.bmp", true, true, source-procedure, ?).       
 run addButton in this-procedure
   ("last", "Last", "Last", "images/last.bmp", "", "images/last-i.bmp", true, true, source-procedure, ?).              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addNewButton fFrameWin 
PROCEDURE addNewButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("new", "New", "New", "images/new.bmp", "", "images/new-i.bmp", true, true, source-procedure, pMenuWidget).                      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addNextButton fFrameWin 
PROCEDURE addNextButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("next", "Next", "Next", "images/next.bmp", "", "images/next-i.bmp", 
      true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addNoteButton fFrameWin 
PROCEDURE addNoteButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.

 run addButton in this-procedure
   ("note", "Note", "Note", "images/note.bmp", "", "images/note-i.bmp", 
    false, true, source-procedure, pMenuWidget).  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addPreviousButton fFrameWin 
PROCEDURE addPreviousButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("previous", "Previous", "Previous", "images/previous.bmp", "", "images/previous-i.bmp",
     true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addQueryButton fFrameWin 
PROCEDURE addQueryButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("query", "Query", "Query", "images/query.bmp", "", "images/query-i.bmp",
     true, true, source-procedure, pMenuWidget).       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addSaveButton fFrameWin 
PROCEDURE addSaveButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMenuWidget as widget-handle.
 run addButton in this-procedure
   ("save", "Save", "Save", "images/save.bmp", "", "images/save-i.bmp",
     false, true, source-procedure, pMenuWidget).                
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addSubscriber fFrameWin 
PROCEDURE addSubscriber :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pSubscriber as handle.

 if lookup("toolbarButtonClicked", pSubscriber:internal-entries) > 0
  then subscribe procedure pSubscriber to "toolbarButtonClicked" in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addTransactionButtons fFrameWin 
PROCEDURE addTransactionButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 run addButton in this-procedure
   ("new", "New", "New", "images/new.bmp", "", "images/new-i.bmp",
     true, true, source-procedure, ?).                      
 run addButton in this-procedure
   ("modify", "Modify", "Modify", "images/update.bmp", "", "images/update-i.bmp",
     true, true, source-procedure, ?).       
 run addButton in this-procedure
   ("copy", "Copy", "Copy", "images/copy.bmp", "", "images/copy-i.bmp",
     true, true, source-procedure, ?).       
 run addButton in this-procedure
   ("delete", "Delete", "Delete", "images/delete.bmp", "", "images/delete-i.bmp",
     true, true, source-procedure, ?).              
 run addButton in this-procedure
   ("save", "Save", "Save", "images/save.bmp", "", "images/save-i.bmp",
     false, true, source-procedure, ?).                
 run addButton in this-procedure
   ("cancel", "Cancel", "Cancel", "images/cancel.bmp", "", "images/cancel-i.bmp",
     false, true, source-procedure, ?).  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects fFrameWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE buttonClicked fFrameWin 
PROCEDURE buttonClicked PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     Perform standard disable/enable actions on common buttons;
               Publish the action clicked for the controlling procedure.
  Parameters:  Action (i.e. button) clicked; whether default behavior should occur
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAction as char.
 def input parameter pDefault as char.

 def var subscriberError as logical init false.

 publish "Action" + pAction (output subscriberError).
 publish "toolbarButtonClicked" (pAction, output subscriberError).

 if subscriberError 
  then return.

 if pDefault = "Y"
  then
   case pAction:
    when "new" 
     then
      do: run disableActions ("new,update,copy,delete,find,filter,first,previous,next,last").
          run enableActions  ("save,cancel").
      end.
    when "update" 
     then
      do: run disableActions ("new,update,copy,delete,find,filter,first,previous,next,last").
          run enableActions  ("save,cancel").
      end.
    when "find" 
     then
      do: run disableActions ("new,update,copy,delete,find,filter,first,previous,next,last,save").
          run enableActions  ("cancel").
      end.
    when "filter" 
     then
      do: run disableActions ("new,update,copy,delete,find,filter,first,previous,next,last,save").
          run enableActions  ("cancel").
      end.
    when "copy" 
     then
      do: run disableActions ("new,update,copy,delete,find,filter,first,previous,next,last").
          run enableActions  ("save,cancel").
      end.
    when "save" 
     then
      do: run disableActions ("save,cancel").
          run enableActions  ("new,update,copy,delete,find,filter,first,previous,next,last").
      end.
    when "cancel" 
     then
      do: run disableActions ("save,cancel").
          run enableActions  ("new,update,copy,delete,find,filter,first,previous,next,last").
      end.
   end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeActionState fFrameWin 
PROCEDURE changeActionState PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pActions as char.
 def input parameter pState as logical.

 def var tPos as int no-undo.
 def var tButtonHandle as handle no-undo.
 def var tMenuHandle as handle no-undo.

 do std-in = 1 to num-entries(pActions):
  std-ch = entry(std-in, pActions).
  tPos = lookup(std-ch, tToolbarActions).
  tButtonHandle = ?.
  tMenuHandle = ?.
  case tPos:
   when 1 then tButtonHandle = b1:handle in frame fToolbar.
   when 2 then tButtonHandle = b2:handle in frame fToolbar.
   when 3 then tButtonHandle = b3:handle in frame fToolbar.
   when 4 then tButtonHandle = b4:handle in frame fToolbar.
   when 5 then tButtonHandle = b5:handle in frame fToolbar.
   when 6 then tButtonHandle = b6:handle in frame fToolbar.
   when 7 then tButtonHandle = b7:handle in frame fToolbar.
   when 8 then tButtonHandle = b8:handle in frame fToolbar.
   when 9 then tButtonHandle = b9:handle in frame fToolbar.
   when 10 then tButtonHandle = b10:handle in frame fToolbar.
   when 11 then tButtonHandle = b11:handle in frame fToolbar.
   when 12 then tButtonHandle = b12:handle in frame fToolbar.
   when 13 then tButtonHandle = b13:handle in frame fToolbar.
   when 14 then tButtonHandle = b14:handle in frame fToolbar.
   when 15 then tButtonHandle = b15:handle in frame fToolbar.
  end case.
  if valid-handle(tButtonHandle) 
   then
    do: tButtonHandle:sensitive = pState.
        if entry(3, tButtonHandle:private-data) = "Y"
         then tMenuHandle = handle(entry(4, tButtonHandle:private-data)).
        if valid-handle(tMenuHandle) 
         then tMenuHandle:sensitive = pState.
    end.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ChangeActionUI fFrameWin 
PROCEDURE ChangeActionUI :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAction as char.
 def input parameter pSensitiveImage as char.
 def input parameter pInsensitiveImage as char.
 def input parameter pTooltip as char.

 def var tPos as int no-undo.
 def var tButtonHandle as handle no-undo.
 def var tMenuHandle as handle no-undo.

 if pSensitiveImage <> ? and search(pSensitiveImage) = ? 
  then pSensitiveImage = ?.
 if pInsensitiveImage <> ? and search(pInsensitiveImage) = ? 
  then pInsensitiveImage = ?.
 if pSensitiveImage = ?
    and pInsensitiveImage = ? 
  then return.

  tPos = lookup(pAction, tToolbarActions).
  tButtonHandle = ?.
  tMenuHandle = ?.
  case tPos:
   when 1 then tButtonHandle = b1:handle in frame fToolbar.
   when 2 then tButtonHandle = b2:handle in frame fToolbar.
   when 3 then tButtonHandle = b3:handle in frame fToolbar.
   when 4 then tButtonHandle = b4:handle in frame fToolbar.
   when 5 then tButtonHandle = b5:handle in frame fToolbar.
   when 6 then tButtonHandle = b6:handle in frame fToolbar.
   when 7 then tButtonHandle = b7:handle in frame fToolbar.
   when 8 then tButtonHandle = b8:handle in frame fToolbar.
   when 9 then tButtonHandle = b9:handle in frame fToolbar.
   when 10 then tButtonHandle = b10:handle in frame fToolbar.
   when 11 then tButtonHandle = b11:handle in frame fToolbar.
   when 12 then tButtonHandle = b12:handle in frame fToolbar.
   when 13 then tButtonHandle = b13:handle in frame fToolbar.
   when 14 then tButtonHandle = b14:handle in frame fToolbar.
   when 15 then tButtonHandle = b15:handle in frame fToolbar.
  end case.
  if valid-handle(tButtonHandle) 
   then
    do: if pSensitiveImage <> ?
          then tButtonHandle:load-image(pSensitiveImage).
        if pInsensitiveImage <> ? 
         then tButtonHandle:load-image-insensitive(pInsensitiveImage).
        tButtonHandle:tooltip = pTooltip.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DisableActions fFrameWin 
PROCEDURE DisableActions :
/*------------------------------------------------------------------------------
  Purpose:     Disable buttons based on its assigned actions
  Parameters:  Comma-delimited list of buttons (actions) to disable
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pActions as char.
 
/*das DisableActions in toolbar testing*/
/*  MESSAGE "DisableActions in toolbar: " pActions skip(2) */
/*          " from " program-name(2)                      */
/*   VIEW-AS ALERT-BOX INFO BUTTONS OK.                   */

 run changeActionState in this-procedure (pActions, false).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fFrameWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fToolbar.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EnableActions fFrameWin 
PROCEDURE EnableActions :
/*------------------------------------------------------------------------------
  Purpose:     Enable buttons based on its assigned actions
  Parameters:  Comma-delimited list of buttons (actions) to enable
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pActions as char.
 
 /*das EnableActions in toolbar testing*/
/*  MESSAGE "EnableActions in toolbar: " pActions skip(2) */
/*          " from " program-name(2)                      */
/*   VIEW-AS ALERT-BOX INFO BUTTONS OK.                   */

 run changeActionState in this-procedure (pActions, true).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fFrameWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  {&OPEN-BROWSERS-IN-QUERY-fToolbar}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDisabledActions fFrameWin 
PROCEDURE GetDisabledActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pList as char.

 run listActionState (false, output pList).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetEnabledActions fFrameWin 
PROCEDURE GetEnabledActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pList as char.

 run listActionState (true, output pList).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE listActionState fFrameWin 
PROCEDURE listActionState PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pState as logical no-undo.
 def output parameter pActionList as char no-undo.

 def var tPos as int no-undo.
 def var tButtonHandle as handle no-undo.
 def var tMenuHandle as handle no-undo.

 do tPos = 1 to num-entries(tToolbarActions):
  std-ch = entry(tPos, tToolbarActions).
  if std-ch = "" 
   then leave.
  
  tButtonHandle = ?.
  case tPos:
   when 1 then tButtonHandle = b1:handle in frame fToolbar.
   when 2 then tButtonHandle = b2:handle in frame fToolbar.
   when 3 then tButtonHandle = b3:handle in frame fToolbar.
   when 4 then tButtonHandle = b4:handle in frame fToolbar.
   when 5 then tButtonHandle = b5:handle in frame fToolbar.
   when 6 then tButtonHandle = b6:handle in frame fToolbar.
   when 7 then tButtonHandle = b7:handle in frame fToolbar.
   when 8 then tButtonHandle = b8:handle in frame fToolbar.
   when 9 then tButtonHandle = b9:handle in frame fToolbar.
   when 10 then tButtonHandle = b10:handle in frame fToolbar.
   when 11 then tButtonHandle = b11:handle in frame fToolbar.
   when 12 then tButtonHandle = b12:handle in frame fToolbar.
   when 13 then tButtonHandle = b13:handle in frame fToolbar.
   when 14 then tButtonHandle = b14:handle in frame fToolbar.
   when 15 then tButtonHandle = b15:handle in frame fToolbar.
  end case.
  if valid-handle(tButtonHandle) 
    and tButtonHandle:sensitive = pState
   then pActionList = pActionList + (if pActionList > "" then "," else "")
          + std-ch.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetTooltip fFrameWin 
PROCEDURE SetTooltip :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAction as char.
 def input parameter pTip as char.

 def var tPos as int no-undo.
 def var tButtonHandle as handle no-undo.

 tPos = lookup(pAction, tToolbarActions).
 tButtonHandle = ?.
 case tPos:
  when 1 then tButtonHandle = b1:handle in frame fToolbar.
  when 2 then tButtonHandle = b2:handle in frame fToolbar.
  when 3 then tButtonHandle = b3:handle in frame fToolbar.
  when 4 then tButtonHandle = b4:handle in frame fToolbar.
  when 5 then tButtonHandle = b5:handle in frame fToolbar.
  when 6 then tButtonHandle = b6:handle in frame fToolbar.
  when 7 then tButtonHandle = b7:handle in frame fToolbar.
  when 8 then tButtonHandle = b8:handle in frame fToolbar.
  when 9 then tButtonHandle = b9:handle in frame fToolbar.
  when 10 then tButtonHandle = b10:handle in frame fToolbar.
  when 11 then tButtonHandle = b11:handle in frame fToolbar.
  when 12 then tButtonHandle = b12:handle in frame fToolbar.
  when 13 then tButtonHandle = b13:handle in frame fToolbar.
  when 14 then tButtonHandle = b14:handle in frame fToolbar.
  when 15 then tButtonHandle = b15:handle in frame fToolbar.
 end case.
 if valid-handle(tButtonHandle) 
  then tButtonHandle:tooltip = pTip.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

