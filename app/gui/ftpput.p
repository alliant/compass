&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : ftpput.p
    Purpose     : FTP put functionality to interact with AES FTP Service 
                  using cURL.exe
    Description :
    Author(s)   : D.Sinclair
    Created     : 4.30.2012
    Notes       :
  ----------------------------------------------------------------------*/
def input parameter pProcess as char no-undo.
def input parameter pLocalFile as char no-undo.
def input parameter pServerFile as char no-undo.

def output parameter responseStatus as logical init false.
def output parameter responseMsg as char.

def var pHost as char no-undo.
def var pUser as char no-undo.
def var pPasswd as char no-undo.

def var ftpTool as char no-undo.
&scoped-define ftptool wput.exe

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-parseResponse) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD parseResponse Procedure 
FUNCTION parseResponse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

etime(true).

os-delete value(pProcess + ".bat").
os-delete value(pProcess + "-srv.dat").
os-delete value(pProcess + "-trc.dat").

publish "GetDocumentAddress" (output pHost).
if pHost = "" or pHost = ?
 then
  do: trace("Document Service was not set: ftp").
      responseMsg = "Document Service not set.  Contact the System Administrator.".
      publish "ProcessLog" (pProcess, "FTP/PUT", etime, responseStatus).
      return.
  end.

publish "GetCredentials" (output pUser, output pPasswd).


DO-BLOCK:
do on error undo, leave:
 ftpTool = search("{&ftptool}").
 if ftpTool = ? 
  then 
   do: trace("{&ftptool} was not found.").
       responseMsg = "Windows command not configured properly.  Contact the System Administrator.".
       leave DO-BLOCK.
   end.

 if pProcess = "" or pProcess = ? or index(pProcess, ".") > 0 
  then 
   do: trace("Process not set appropriately: " + (if pProcess = ? then "Unknown" else pProcess)).
       responseMsg = "Process not set correctly.  Contact the System Administrator.".
       leave DO-BLOCK.
   end.

 file-info:file-name = pLocalFile.
 if file-info:full-pathname = ? 
  then
   do: trace("File: " + pLocalFile + " does not exist: " + pLocalFile).
       responseMsg = "File " + pLocalFile + " does not exist.".
       leave DO-BLOCK.
   end.

 /* BATch file to perform the ftp put command using WPUT */
 output to value(pProcess + ".bat") page-size 0.
 put unformatted ftpTool.
 put unformatted " --output-file=" + pProcess + "-srv.dat".
 put unformatted " --debug ".
 put unformatted '"' file-info:full-pathname '"'.
 put unformatted " ftp://" + pUser + ":" + pPasswd 
                    + "@" + pHost + "/" + pServerFile skip.
 put unformatted "exit" skip.
 output close.

 /* Run the batch file */
 os-command silent value(pProcess + ".bat").

 /* Parse the FTP response file (basically) */
 responseStatus = parseResponse().
end.

publish "ProcessLog" (pProcess, "FTP/PUT", etime, responseStatus).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-parseResponse) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION parseResponse Procedure 
FUNCTION parseResponse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def var tLine as char no-undo.
  def var tCnt as int no-undo.

  file-info:file-name = pProcess + "-srv.dat".
  
  if index(file-info:file-type, "F") = 0 or file-info:file-type = ?
   then
    do: trace("Unable to locate response file: " + pProcess + "-srv.dat").
        responseMsg = "Unable to locate response.  Contact the System Administrator.".
        return false.
    end.
  if index(file-info:file-type, "R") = 0
   then
    do: trace("Unable to read response file: " + pProcess + "-srv.dat").
        responseMsg = "Unable to read response.  Contact the System Administrator.".
        return false.
    end.
  
  input from value(pProcess + "-srv.dat").
  repeat on error undo, leave
     on endkey undo, leave:
   import unformatted tLine.
   tCnt = tCnt + 1.
  end.
  input close.

  trace("Line " + string(tCnt) + " of log file: " + tLine).

  if not tLine matches "Transfered*"
   then
    do: trace("Transfer failed; check process files: " + pProcess).
        responseMsg = "Unable to upload document.  Contact the System Administrator.".
        return false.
    end.
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-trace) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION trace Procedure 
FUNCTION trace RETURNS LOGICAL PRIVATE
  ( input pError as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 output to value(pProcess + "-trc.dat") append.
 put unformatted pError skip.
 output close.

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

