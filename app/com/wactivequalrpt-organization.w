&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wactivequalrpt-organization.w

  Description: Screen for Qualification Status Report for Organizations.
----------------------------------------------------------------------*/

create widget-pool.

{tt/organization.i}
{tt/qualstatus.i}
{tt/qualstatus.i &tableAlias=tResults}

{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}

define variable dColumnWidth as decimal              no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQualification

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tResults

/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification tResults.entityid "Organization ID" tResults.name "Name" tResults.Qualification "Qualification" tResults.inUse tResults.stat "Status" tResults.EffectiveDate "Effective" tResults.ExpirationDate "Expiration" tResults.lastReviewDate "Last Reviewed" tResults.lastReviewBy "Reviewed By"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification   
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification for each tResults by tResults.entityid
&Scoped-define OPEN-QUERY-brwQualification open query {&SELF-NAME} for each tResults by tResults.entityid.
&Scoped-define TABLES-IN-QUERY-brwQualification tResults
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification tResults


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bOrgLookup cbState tgLinked btGet tgActive ~
fOrganization RECT-54 
&Scoped-Define DISPLAYED-OBJECTS cbState tgLinked tgActive cbQual cbStatus ~
tgExpired fiExp fOrganization 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-Win 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bOrgLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset filter".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cbQual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 51 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 31.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE fiExp AS INTEGER FORMAT "zz9":U INITIAL 0 
     LABEL "in" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 6.2 BY 1 NO-UNDO.

DEFINE VARIABLE fOrganization AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Organization" 
     VIEW-AS FILL-IN 
     SIZE 56 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86.2 BY 5.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 5.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9 BY 5.

DEFINE VARIABLE tgActive AS LOGICAL INITIAL no 
     LABEL "Get Qualifications Only For Active Roles" 
     VIEW-AS TOGGLE-BOX
     SIZE 42 BY .81 TOOLTIP "Display qualifications only for active roles" NO-UNDO.

DEFINE VARIABLE tgExpired AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 5.8 BY .81 NO-UNDO.

DEFINE VARIABLE tgLinked AS LOGICAL INITIAL no 
     LABEL "Get Only In Use Qualifications" 
     VIEW-AS TOGGLE-BOX
     SIZE 39.8 BY .81 TOOLTIP "Display only qualifications fulfilling at least one requirement" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQualification FOR 
      tResults SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification C-Win _FREEFORM
  QUERY brwQualification DISPLAY
      tResults.entityid       label        "Organization ID"     format "x(12)"    width 18
tResults.name           label        "Name"          format "x(80)"    width 35
tResults.Qualification  label        "Qualification" format "x(50)"    width 33
tResults.inUse          column-label "In Use"        width 9 view-as toggle-box
tResults.stat           label        "Status"        format "x(15)"    width 15
tResults.EffectiveDate  label        "Effective"     format "99/99/99" width 12
tResults.ExpirationDate label        "Expiration"    format "99/99/99" width 12
tResults.lastReviewDate label        "Last Reviewed" format "99/99/99" width 18
tResults.lastReviewBy   label        "Reviewed By"   format "x(50)"    width 25
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 184.8 BY 17.95
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bOrgLookup AT ROW 3 COL 72.8 WIDGET-ID 270 NO-TAB-STOP 
     btExport AT ROW 2.91 COL 163.2 WIDGET-ID 244 NO-TAB-STOP 
     btFilter AT ROW 2.91 COL 154.4 WIDGET-ID 262 NO-TAB-STOP 
     cbState AT ROW 1.86 COL 14.6 COLON-ALIGNED WIDGET-ID 242
     tgLinked AT ROW 4.14 COL 16.8 WIDGET-ID 254
     btGet AT ROW 2.91 COL 78.8 WIDGET-ID 248 NO-TAB-STOP 
     tgActive AT ROW 4.95 COL 16.8 WIDGET-ID 272
     cbQual AT ROW 2.05 COL 100.8 COLON-ALIGNED WIDGET-ID 246
     cbStatus AT ROW 3.24 COL 100.8 COLON-ALIGNED WIDGET-ID 30
     tgExpired AT ROW 4.57 COL 102.8 WIDGET-ID 252
     fiExp AT ROW 4.43 COL 106.8 COLON-ALIGNED WIDGET-ID 238
     brwQualification AT ROW 6.43 COL 2 WIDGET-ID 200
     fOrganization AT ROW 3.05 COL 3.6 WIDGET-ID 268 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.05 COL 89 WIDGET-ID 152
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 163.4 WIDGET-ID 250
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.05 COL 3.2 WIDGET-ID 28
     "Expires:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 4.62 COL 94.6 WIDGET-ID 258
     "Days" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 4.57 COL 115.6 WIDGET-ID 260
     RECT-2 AT ROW 1.29 COL 2.2 WIDGET-ID 8
     RECT-54 AT ROW 1.29 COL 162.4 WIDGET-ID 92
     RECT-3 AT ROW 1.29 COL 88 WIDGET-ID 94
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 270.2 BY 23.48
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Qualifications Report"
         HEIGHT             = 23.48
         WIDTH              = 185.8
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwQualification fiExp fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BROWSE brwQualification IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwQualification:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwQualification:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON btExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFilter IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbQual IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbStatus IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiExp IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fOrganization IN FRAME fMain
   ALIGN-L                                                              */
ASSIGN 
       fOrganization:READ-ONLY IN FRAME fMain        = TRUE
       fOrganization:PRIVATE-DATA IN FRAME fMain     = 
                "ALL".

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tgExpired IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query {&SELF-NAME} for each tResults by tResults.entityid.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Qualifications Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Qualifications Report */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Qualifications Report */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup C-Win
ON CHOOSE OF bOrgLookup IN FRAME fMain /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON DEFAULT-ACTION OF brwQualification IN FRAME fMain
do:
  if not available tResults 
   then 
    return.
    
  publish "OpenWindow"  (input {&organization}, 
                         input tResults.entityID, 
                         input "worganizationdetail.w", 
                         input "character|input|" + tResults.entityID + "^integer|input|2" + "^character|input|",                                   
                         input this-procedure).      
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON ROW-DISPLAY OF brwQualification IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON START-SEARCH OF brwQualification IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter C-Win
ON CHOOSE OF btFilter IN FRAME fMain /* Filter */
do:
  do with frame {&frame-name}: 
    /* Initialise filters to default values */
    assign
        cbQual:screen-value      = {&ALL}
        cbStatus:screen-value    = {&ALL}
        tgExpired:checked        = false
        fiExp:screen-value       = "0"
        fiExp:sensitive          = false
        .
  end.
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* get */
do:
  resetScreen().
  run getdata in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbQual C-Win
ON VALUE-CHANGED OF cbQual IN FRAME fMain /* Qualification */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  resetScreen().

  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus C-Win
ON VALUE-CHANGED OF cbStatus IN FRAME fMain /* Status */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiExp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiExp C-Win
ON VALUE-CHANGED OF fiExp IN FRAME fMain /* in */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgActive C-Win
ON VALUE-CHANGED OF tgActive IN FRAME fMain /* Get Qualifications Only For Active Roles */
do:
  resetScreen().
  resultsChanged(false).  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgExpired
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgExpired C-Win
ON VALUE-CHANGED OF tgExpired IN FRAME fMain
do:
  assign 
      fiExp:sensitive    = (self:checked = true)
      fiExp:screen-value = ""
      .
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgLinked
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgLinked C-Win
ON VALUE-CHANGED OF tgLinked IN FRAME fMain /* Get Only In Use Qualifications */
do:
  resetScreen().
  resultsChanged(false).  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to terminate it. */
on close of this-procedure 
  run disable_UI.

{lib/win-main.i}
{lib/win-status.i}

/* Best default for GUI applications is...                              */
pause 0 before-hide.

subscribe to "closeWindow" anywhere.

btGet:load-image ("images/completed.bmp").              
btGet:load-image-insensitive("images/completed-i.bmp").
btExport:load-image("images/excel.bmp").
btExport:load-image-insensitive("images/excel-i.bmp").
btFilter:load-image("images/filtererase.bmp").
btFilter:load-image-insensitive("images/filtererase-i.bmp").
bOrgLookup:load-image("images/s-lookup.bmp").
bOrgLookup:load-image-insensitive("images/s-lookup-i.bmp").

publish "GetSearchStates" (output std-ch).
if std-ch > "" 
 then 
  std-ch = "," + std-ch.

/* Add item-pair "ALL," */
cbState:list-item-pairs in frame {&frame-name} = {&ALL} + "," +  std-ch.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  resetScreen(). 
  run enable_UI.
  /* create the Organization combo */

  {lib/get-column-width.i &col="'qualification'" &var=dColumnWidth} 
   
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  run windowResized in this-procedure.
  
  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState tgLinked tgActive cbQual cbStatus tgExpired fiExp 
          fOrganization 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bOrgLookup cbState tgLinked btGet tgActive fOrganization RECT-54 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable    as handle    no-undo.
  
  if query brwqualification:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  
  htable = temp-table tResults:handle.
  run util/exporttable.p (table-handle htable,
                          "tResults",
                          "for each tResults ",
                          "stateID,entityid,name,Qualification,inUse,stat,EffectiveDate,ExpirationDate,lastReviewDate,lastReviewBy",
                          "State,Organization ID,Name,Qualification,In Use,Status,Effective Date,Expiration Date,Last Reviewed Date,Last Reviewed by",
                          std-ch,                  
                          "OrganizationActiveQualifications" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
                          
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterdata C-Win 
PROCEDURE filterdata PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  define buffer qualstatus  for qualstatus.
  
  close query brwQualification.
  empty temp-table tResults.

  do with frame {&frame-name}:
    for each qualstatus 
      where qualstatus.qualification = (if cbQual:input-value   = {&ALL} then qualstatus.qualification else cbQual:input-value)
        and qualstatus.stat          = (if cbStatus:input-value = {&ALL} then qualstatus.stat          else cbStatus:input-value)
        and if tgExpired:checked then date(qualstatus.ExpirationDate) <= (today + fiExp:input-value)  else true:
      create tResults.
      buffer-copy qualstatus to tResults.
      tResults.stat = getQualificationDesc(qualstatus.stat).
    end.
  end.

  open query brwqualification preselect each tResults by tResults.entityid.

  /* Display no. of records on status bar */
  setStatusCount(query brwqualification:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer qualstatus  for qualstatus.
  
  /* Server Call */
  run server/queryqualificationsstatus.p (input cbState:input-value in frame {&frame-name},
                                          input {&OrganizationCode},
                                          input fOrganization:private-data,
                                          input tgLinked:checked,
                                          input tgActive:checked,
                                          input 0,   /* QualificationID 0 - for ALL */
                                          output table qualstatus,
                                          output std-lo,
                                          output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
                                                                       
  /* Set the filters based on the data returned, with ALL as the first option */
  for each qualstatus 
    break by qualstatus.qualification:
    if not first-of(qualstatus.qualification) 
     then 
      next.
    cbQual:add-last(qualstatus.qualification).
  end.

  for each qualstatus
    break by qualstatus.stat:
    if not first-of(qualstatus.stat) or qualstatus.stat = ""
     then 
      next.

    publish "GetSysPropDesc" ("COM", "Qualification", "Status", qualstatus.stat, output std-ch).
    cbStatus:add-last(std-ch,qualstatus.stat).
  end.

  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwQualification:num-results > 0 
   then
    assign 
        browse brwQualification:sensitive = true
               btExport:sensitive         = true
               cbQual:sensitive           = true
               cbStatus:sensitive         = true
               tgExpired:sensitive        = true
               btFilter:sensitive         = true
        .  
  else
   assign 
       browse brwQualification:sensitive = false
              btExport:sensitive         = false
              cbQual:sensitive           = false
              cbStatus:sensitive         = false
              tgExpired:sensitive        = false
              btFilter:sensitive         = false
       .
  
  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwqualification:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog C-Win 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable cPrevOrgID as character              no-undo.
  define variable cOrgID     as character              no-undo.
  define variable cName      as character              no-undo.
  
  if fOrganization:input-value <> ""
   then
    cPrevOrgID = fOrganization:private-data. 
    
  run dialogorganizationlookup.w(input  cPrevOrgID,
                                 input  false,        /* True if add "New" */
                                 input  false,        /* True if add "Blank" */
                                 input  true,         /* True if add "ALL" */
                                 output cOrgID,
                                 output cName,
                                 output std-lo).
   
  if not std-lo 
   then
    return no-apply.

  if cName = {&ALL}
   then
    do:
      fOrganization:screen-value = cName.
      fOrganization:private-data = cName.
    end.
  else
   do:
     fOrganization:screen-value = if cOrgID = "" then "" else cName.
     fOrganization:private-data = if cOrgID = "" then "" else cOrgID.
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 4
      .

  {lib/resize-column.i &col="'qualification'" &var=dColumnWidth}

  run ShowScrollBars(browse brwQualification:handle, no, yes).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-Win 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}: 
    /* Initialise filters to default values */
    assign
        cbQual:list-items        = {&ALL}
        cbStatus:list-item-pairs = "ALL,ALL"
        cbQual:screen-value      = {&ALL}
        cbStatus:screen-value    = {&ALL}
        tgExpired:checked        = false
        fiExp:screen-value       = "0"
        fiExp:sensitive          = false
        .
    disable cbQual cbStatus tgExpired fiExp btFilter btExport. 
  end.

  setStatusMessage("").
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}).
  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

