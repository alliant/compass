&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wstaterequirement.w

  Description: Displays state requirements and 
               state requirement qualifications for an entity. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Sachin Chaturvedi

  Created: 01.04.2018
  Modification;
  Date            Name           Description
  10/16/2018      Rahul          Fixed bug related to enable/disbale of butons.
  04/02/2020      Archana Gupta  Modified code according to new organization structure
  12/06/2021      Shefali        Task# 86699  Modified to show fulfillable column in state requirement grid.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

{lib/std-def.i}
{lib/com-def.i}
{lib/brw-multi-def.i}

/* Temp-table definition*/
{tt/staterequirement.i}
{tt/statereqqual.i}

define temp-table tstateRequirement like stateRequirement.
define temp-table tstateReqQual     like stateReqQual.

/* ***************************  Definitions  ************************** */

/* Local Variable Definitions ---                                       */

define variable activeRowid     as rowid     no-undo.
define variable iStateReqID     as integer   no-undo.
define variable iStateReqQualID as integer   no-undo.
define variable hpopupMenu      as handle    no-undo.
define variable hpopupMenuItem  as handle    no-undo.
define variable hpopupMenuItem1 as handle    no-undo.
define variable ireqid          as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwstateReq

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES staterequirement StateReqQual

/* Definitions for BROWSE brwstateReq                                   */
&Scoped-define FIELDS-IN-QUERY-brwstateReq getreqfor( staterequirement.reqfor ) @ staterequirement.reqfor "Applies To" staterequirement.appliesTo "Role" staterequirement.refId "Entity ID" staterequirement.description "Requirement" getauth(staterequirement.authorizedBy) @ staterequirement.authorizedBy staterequirement.fulfillable "Fulfillable"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwstateReq   
&Scoped-define SELF-NAME brwstateReq
&Scoped-define QUERY-STRING-brwstateReq for each staterequirement by staterequirement.reqfor
&Scoped-define OPEN-QUERY-brwstateReq open query {&SELF-NAME} for each staterequirement by staterequirement.reqfor .
&Scoped-define TABLES-IN-QUERY-brwstateReq staterequirement
&Scoped-define FIRST-TABLE-IN-QUERY-brwstateReq staterequirement


/* Definitions for BROWSE brwstateReqQual                               */
&Scoped-define FIELDS-IN-QUERY-brwstateReqQual StateReqQual.active getreqfor(StateReqQual.appliesTo) @ stateReqQual.appliesTo StateReqQual.Qualification StateReqQual.EffectiveDate StateReqQual.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwstateReqQual   
&Scoped-define SELF-NAME brwstateReqQual
&Scoped-define QUERY-STRING-brwstateReqQual for each StateReqQual where StatereqQual.requirementID = iReqID
&Scoped-define OPEN-QUERY-brwstateReqQual open query {&SELF-NAME} for each StateReqQual where StatereqQual.requirementID = iReqID.
&Scoped-define TABLES-IN-QUERY-brwstateReqQual StateReqQual
&Scoped-define FIRST-TABLE-IN-QUERY-brwstateReqQual StateReqQual


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwstateReq}~
    ~{&OPEN-QUERY-brwstateReqQual}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwstateReq bActivateStateReqQual ~
bDeleteStateReq bDeleteStateReqQual bEditStateReq bEditStateReqQual bExport ~
bNewStateReq bNewStateReqQual bRefresh cbState rsFulffiledBy2 cbAppliesTo ~
flDetails brwstateReqQual 
&Scoped-Define DISPLAYED-OBJECTS cbState rsFulffiledBy2 cbAppliesTo ~
flDetails 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getauth C-Win 
FUNCTION getauth returns character ( cauth as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getreqfor C-Win 
FUNCTION getreqfor returns character ( cauth as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bActivateStateReqQual  NO-FOCUS
     LABEL "Activate" 
     SIZE 4.8 BY 1.14 TOOLTIP "Activate state requirement qualification".

DEFINE BUTTON bDeleteStateReq  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete state requirement".

DEFINE BUTTON bDeleteStateReqQual  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete state requirement qualification".

DEFINE BUTTON bEditStateReq  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify state requirement".

DEFINE BUTTON bEditStateReqQual  NO-FOCUS
     LABEL "Edit" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify state requirement qualification".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bNewStateReq  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "New state requirement".

DEFINE BUTTON bNewStateReqQual  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "New state requirement qualification".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE VARIABLE cbAppliesTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "All" 
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 30.6 BY 1 NO-UNDO.

DEFINE VARIABLE flDetails AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 132.8 BY 1 NO-UNDO.

DEFINE VARIABLE rsFulffiledBy2 AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 29 BY 1.29 TOOLTIP "Select state requirement for organization or person" NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 40 BY 2.19.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65.6 BY 2.19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwstateReq FOR 
      staterequirement SCROLLING.

DEFINE QUERY brwstateReqQual FOR 
      StateReqQual SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwstateReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwstateReq C-Win _FREEFORM
  QUERY brwstateReq DISPLAY
      getreqfor( staterequirement.reqfor ) @ staterequirement.reqfor     label        "Applies To"       format "x(15)"        width 12
staterequirement.appliesTo              label        "Role"             format "x(25)"        width 15      
staterequirement.refId                  label        "Entity ID"                              width 12      
staterequirement.description            label        "Requirement"      format "x(50)"        width 50
getauth(staterequirement.authorizedBy)   @ staterequirement.authorizedBy label "Authorized By" format "x(15)" width 15
staterequirement.fulfillable            column-label        "Fulfillable"      view-as toggle-box
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 132.8 BY 11.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "State Requirements" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwstateReqQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwstateReqQual C-Win _FREEFORM
  QUERY brwstateReqQual DISPLAY
      StateReqQual.active column-label "Active":C width 12    view-as toggle-box 
getreqfor(StateReqQual.appliesTo)  @ stateReqQual.appliesTo         label "Fulfilled by"  format "x(15)"    width 15 
StateReqQual.Qualification       label "Qualification" format "x(60)"    width 60
StateReqQual.EffectiveDate       label "Effective"     format 99/99/9999 width 20
StateReqQual.expirationDate      label "Expiration"    format 99/99/9999 width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 132.8 BY 6.52
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Qualifications that can fulfill/meet selected state requirement" ROW-HEIGHT-CHARS .76 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwstateReq AT ROW 3.91 COL 8 WIDGET-ID 200
     bActivateStateReqQual AT ROW 20.91 COL 2.4 WIDGET-ID 266 NO-TAB-STOP 
     bDeleteStateReq AT ROW 6.14 COL 2.4 WIDGET-ID 252 NO-TAB-STOP 
     bDeleteStateReqQual AT ROW 19.76 COL 2.4 WIDGET-ID 260 NO-TAB-STOP 
     bEditStateReq AT ROW 5 COL 2.4 WIDGET-ID 96 NO-TAB-STOP 
     bEditStateReqQual AT ROW 18.62 COL 2.4 WIDGET-ID 262 NO-TAB-STOP 
     bExport AT ROW 7.29 COL 2.4 WIDGET-ID 2 NO-TAB-STOP 
     bNewStateReq AT ROW 3.86 COL 2.4 WIDGET-ID 250 NO-TAB-STOP 
     bNewStateReqQual AT ROW 17.48 COL 2.4 WIDGET-ID 264 NO-TAB-STOP 
     bRefresh AT ROW 1.33 COL 2.4 WIDGET-ID 268 NO-TAB-STOP 
     cbState AT ROW 1.95 COL 13.8 COLON-ALIGNED WIDGET-ID 90
     rsFulffiledBy2 AT ROW 1.76 COL 50.6 NO-LABEL WIDGET-ID 358
     cbAppliesTo AT ROW 1.91 COL 84.6 COLON-ALIGNED WIDGET-ID 254
     flDetails AT ROW 16.1 COL 6 COLON-ALIGNED NO-LABEL WIDGET-ID 258 NO-TAB-STOP 
     brwstateReqQual AT ROW 17.48 COL 8 WIDGET-ID 300
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.14 COL 9.2 WIDGET-ID 28
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.1 COL 48.8 WIDGET-ID 152
     RECT-2 AT ROW 1.38 COL 8 WIDGET-ID 8
     RECT-3 AT ROW 1.38 COL 47.6 WIDGET-ID 94
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 141.6 BY 23.31 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Requirements Setup"
         HEIGHT             = 23.38
         WIDTH              = 141.6
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwstateReq 1 fMain */
/* BROWSE-TAB brwstateReqQual RECT-3 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwstateReq:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwstateReq:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       brwstateReqQual:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwstateReqQual:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

ASSIGN 
       flDetails:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwstateReq
/* Query rebuild information for BROWSE brwstateReq
     _START_FREEFORM
open query {&SELF-NAME} for each staterequirement by staterequirement.reqfor .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwstateReq */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwstateReqQual
/* Query rebuild information for BROWSE brwstateReqQual
     _START_FREEFORM
open query {&SELF-NAME} for each StateReqQual where StatereqQual.requirementID = iReqID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwstateReqQual */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Requirements Setup */
or endkey of {&window-name} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Requirements Setup */
do:
  /* This event will close the window and terminate the procedure.  */
  apply "close":u to this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bActivateStateReqQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bActivateStateReqQual C-Win
ON CHOOSE OF bActivateStateReqQual IN FRAME fMain /* Activate */
do:
  run changeStatus in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteStateReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteStateReq C-Win
ON CHOOSE OF bDeleteStateReq IN FRAME fMain /* Delete */
do:
  run deleteRequirement  in this-procedure.
  run actionValueChanged in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteStateReqQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteStateReqQual C-Win
ON CHOOSE OF bDeleteStateReqQual IN FRAME fMain /* Delete */
do:
  run deleteRequirementQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditStateReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditStateReq C-Win
ON CHOOSE OF bEditStateReq IN FRAME fMain /* Edit */
do:
  run modifyRequirement in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditStateReqQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditStateReqQual C-Win
ON CHOOSE OF bEditStateReqQual IN FRAME fMain /* Edit */
do:
  run modifyRequirementQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewStateReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewStateReq C-Win
ON CHOOSE OF bNewStateReq IN FRAME fMain /* Add */
do:
  run newrequirement in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewStateReqQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewStateReqQual C-Win
ON CHOOSE OF bNewStateReqQual IN FRAME fMain /* Add */
do:
  run newRequirementQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshRequirements in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwstateReq
&Scoped-define SELF-NAME brwstateReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReq C-Win
ON DEFAULT-ACTION OF brwstateReq IN FRAME fMain /* State Requirements */
do:
  apply "choose":U  to bEditStateReq.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReq C-Win
ON MOUSE-MENU-CLICK OF brwstateReq IN FRAME fMain /* State Requirements */
do:
  /*/* Need to check why this is used. */
  if not can-find(first staterequirement) 
   then
    return no-apply.
    */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReq C-Win
ON ROW-DISPLAY OF brwstateReq IN FRAME fMain /* State Requirements */
do:
  {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReq C-Win
ON START-SEARCH OF brwstateReq IN FRAME fMain /* State Requirements */
do:
  define variable tWhereClause as character no-undo.
  
  tWhereClause = "where staterequirement.reqfor = " + "'" + rsFulffiledBy2:input-value  + "'" +  
   
   " and staterequirement.appliesTo = " +                                               
                  (if cbAppliesto:input-value = "ALL"
                    then "staterequirement.appliesTo"   
                   else "'" + cbAppliesto:input-value + "'" ).
     
  {lib/brw-startSearch-multi.i &browseName="brwstateReq" &whereClause="tWhereClause"}
 

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReq C-Win
ON VALUE-CHANGED OF brwstateReq IN FRAME fMain /* State Requirements */
do:
  iReqID = 0 . 
  find current staterequirement no-error.
  if available staterequirement
   then
    iReqID =  staterequirement.requirementID .
  run actionValueChanged in this-procedure.
 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwstateReqQual
&Scoped-define SELF-NAME brwstateReqQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReqQual C-Win
ON DEFAULT-ACTION OF brwstateReqQual IN FRAME fMain /* Qualifications that can fulfill/meet selected state requirement */
do:
  if stateReqQual.AuthorizedBy = {&CompanyCode} 
   then
    return no-apply.

  apply "choose" to bEditStateReqQual. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReqQual C-Win
ON MOUSE-MENU-CLICK OF brwstateReqQual IN FRAME fMain /* Qualifications that can fulfill/meet selected state requirement */
do:
  if not can-find(first stateRequirement) 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReqQual C-Win
ON ROW-DISPLAY OF brwstateReqQual IN FRAME fMain /* Qualifications that can fulfill/meet selected state requirement */
do:
  {lib/brw-rowDisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReqQual C-Win
ON START-SEARCH OF brwstateReqQual IN FRAME fMain /* Qualifications that can fulfill/meet selected state requirement */
do:
  define variable tWhereClause as character no-undo.

  tWhereClause = "where StateReqQual.requirementID = "  + string(iReqID).


  {lib/brw-startSearch-multi.i &browseName ="brwstateReqQual" &whereClause="tWhereClause" }

  run SetWidgetsState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwstateReqQual C-Win
ON VALUE-CHANGED OF brwstateReqQual IN FRAME fMain /* Qualifications that can fulfill/meet selected state requirement */
do:
  run setWidgetsState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAppliesTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAppliesTo C-Win
ON VALUE-CHANGED OF cbAppliesTo IN FRAME fMain /* Role */
do:
  run showstatus         in this-procedure.
  run displayRequirement in this-procedure.
  run actionValueChanged in this-procedure.
  apply 'value-changed' to browse brwstateReq.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do: 
  /* Once a state is selected, delete the "--Select State-" item from the list. */ 
  {lib/modifylist.i &cbhandle=cbState:handle &list=list-item-pairs &delimiter = "," &item = '{&SelectState}'}
  run getRequirements in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsFulffiledBy2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsFulffiledBy2 C-Win
ON VALUE-CHANGED OF rsFulffiledBy2 IN FRAME fMain
do:
  iReqID = 0.
  run changerole         in this-procedure.
  run displayRequirement in this-procedure.
  find current staterequirement no-error.
  if available staterequirement
   then
    iReqID =  staterequirement.requirementID .
  run displayRequirementQualification in this-procedure.
  run showstatus                      in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwstateReq
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

{lib/win-main.i}
{lib/win-status.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Deleting the handles on close */
on close of this-procedure 
do:
  run disable_UI.
  if valid-handle(hpopupMenuItem) 
   then
    delete object hpopupMenuItem.

  if valid-handle(hpopupMenuItem1) 
   then
    delete object hpopupMenuItem1.

  if valid-handle(hpopupMenu) 
   then
    delete object hpopupMenu.
end.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

bDeleteStateReq      :load-image("images/s-delete.bmp").
bDeleteStateReq      :load-image-insensitive("images/s-delete-i.bmp").
bDeleteStateReqQual  :load-image("images/s-delete.bmp").
bDeleteStateReqQual  :load-image-insensitive("images/s-delete-i.bmp").
bNewStateReq         :load-image("images/s-add.bmp").
bNewStateReq         :load-image-insensitive("images/s-add-i.bmp").
bNewStateReqQual     :load-image("images/s-add.bmp").
bNewStateReqQual     :load-image-insensitive("images/s-add-i.bmp").
bEditStateReq        :load-image("images/s-update.bmp").
bEditStateReq        :load-image-insensitive("images/s-update-i.bmp").
bEditStateReqQual    :load-image("images/s-update.bmp").
bEditStateReqQual    :load-image-insensitive("images/s-update-i.bmp").
bRefresh             :load-image("images/s-Refresh.bmp").
bRefresh             :load-image-insensitive("images/s-Refresh-i.bmp").
bActivateStateReqQual:load-image("images/s-flag-i").
bActivateStateReqQual:load-image-insensitive("images/s-flag-i").
bExport              :load-image("images/s-excel").
bExport              :load-image-insensitive("images/s-excel-i").

{lib/brw-main-multi.i &browse-list="brwstateReq,brwstateReqQual"}

/* Getting states from config. Also adding National here */
publish "getSearchStates" (output std-ch).
if std-ch > "" 
 then
  std-ch = "," + {&NationalState} + "," + std-ch.

cbState:list-item-pairs in frame {&frame-name} = {&SelectState} + "," + std-ch.                           

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  run changerole in this-procedure.
  assign
      cbState:screen-value     = ""
      cbAppliesTo:screen-value = {&ALL}
      .
  
  /* Setting widgets sensitivity on launch. */
  run setWidgetsState in this-procedure.
  setStatusMessage("").

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionValueChanged C-Win 
PROCEDURE actionValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Creating dynamic popup menu with respective to entity i.e Agent,attorney,role      
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  std-lo = false.
  std-ch = "".
  define variable pList as character no-undo. 

  if available staterequirement  /* available  */
   then
    do:
      /* opening query for specific requirement in requirementqual. */
    
      flDetails:screen-value = if refId <> {&ALL} then staterequirement.name + "," + staterequirement.address else "".
      
      if valid-handle(hpopupMenuItem) 
       then
        delete object hpopupMenuItem.
      
      if valid-handle(hpopupMenuItem1) 
       then
        delete object hpopupMenuItem1.
      
      if valid-handle(hpopupMenu) 
       then
        delete object hpopupMenu.
    end.
  else
   assign
       flDetails:screen-value = ""
       iReqID = 0
       .
  
  run displayRequirementQualification in this-procedure.
  /* Enabling/disabling widgets . */
  run setWidgetsState                 in this-procedure.
  run showStatus                      in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changerole C-Win 
PROCEDURE changerole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.
  std-lo = false.
  std-ch = "".
 
  define variable pList as character no-undo. 
  
  if rsFulffiledBy2:input-value = {&OrganizationCode}
   then
     do:
       publish "getComplianceCodesList" ({&OrganizationRole},
                                          input ",",
                                          output pList,
                                          output std-lo,
                                          output std-ch).
     end.
  
  else if rsfulffiledBy2:input-value = {&PersonCode}
   then 
    do:
      publish "getComplianceCodesList" ({&PersonRole},
                                       input ",",
                                       output pList,
                                       output std-lo,
                                       output std-ch).
     
    end.
  
  if not std-lo
   then
     do:
       message std-ch
         view-as alert-box error buttons ok.
       return.
     end.
                              
  /* Assigning the list */                             
  pList = trim (pList,",").
  cbAppliesTo:list-items = {&ALL} + "," + pList.
  cbAppliesto:screen-value = cbAppliesTo:list-items.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeStatus C-Win 
PROCEDURE changeStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Its for activate/deactivate staterequirementqualification
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not available StateReqQual
   then
    return.

  assign 
      iStateReqQualID = StateReqQual.statereqqualID
      iStateReqID     = StateReqQual.requirementID 
      .

  if StateReqQual.active  
   then
    do:
       /* deactivates the requirement qualification and update data model. */
       publish "deactivateRequirementQual" (input iStateReqQualID,
                                            input  yes,
                                            output std-lo,
                                            output std-ch).
       
       if not std-lo 
        then
         do:
          if std-ch <> {&InUse}
           then
            do:
              message std-ch
                  view-as alert-box error buttons ok.
              return.
            end. 
         
          std-lo = false.
          message "This state requirement qualification mapping is still in use. "   skip
                   "Are yor sure you want to deactivate ?"                                              
            view-as alert-box question buttons yes-no update std-lo.
      
          if not std-lo /* if dont want to change the status */
           then
            return.
    
          publish "deactivateRequirementQual" (input  iStateReqQualID,
                                               input  not std-lo,
                                               output std-lo,
                                               output std-ch).
         
        end.
    end.
    else
     /* activates the requirement qualification and update data modal. */
     publish "activateRequirementQual"(input iStateReqQualID,
                                      output std-lo ,
                                       output std-ch).


    if not std-lo 
     then
      do:
        message std-ch
          view-as alert-box error buttons ok.
        return.
      end.   

    find first staterequirement where staterequirement.requirementID = iStateReqID no-error.
    if available staterequirement 
     then
      activeRowID = rowid(staterequirement).
      
    run actionValueChanged in this-procedure.
    reposition brwstateReq to rowid activeRowid no-error.
    
    find first stateReqQual where stateReqQual.requirementID = iStatereqID and stateReqQual.statereqQUalID = iStatereqQualID no-error.
    if available statereqqual 
     then
      do:
        StateReqQual.active = not statereqQual.active .
        activeRowID = rowid(statereqqual).
      end.

    reposition brwstateReqQual to rowid activeRowID no-error.
    apply "value-changed" to brwStatereqQual.
    brwstateReqQual:refresh().
        
    if not can-find(first stateReqQual where stateReqQual.requirementID = iStatereqID and StateReqQual.active = true and staterequirement.fulfillable) then
       staterequirement.fulfillable = not staterequirement.fulfillable.
       
    apply "value-changed" to brwStatereq.
    brwstateReq:refresh().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
  return no-apply.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRequirement C-Win 
PROCEDURE deleteRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable isSuccess  as logical   no-undo.
  define variable chErrorMsg as character no-undo.

  define buffer tStateRequirement for tStateRequirement.
  define buffer tStateReqQual     for tStateReqQual.
  
  if not available staterequirement 
   then
    return.
  
  message "Highlighted State Requirement will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete State Requirement"
    update lContinue as logical.

  if not lContinue  
   then
    return.

  /* Delete requirement record and update same in data model. */
  publish "deleterequirement"(input staterequirement.requirementID, /* requirementiD*/
                              output std-lo,                        /* Success or not */
                              output std-ch).                       /* Error msg from server */


  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
  
  run getRequirements in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRequirementQualification C-Win 
PROCEDURE deleteRequirementQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tStateRequirement for tStateRequirement.
  define buffer tStateReqQual     for tStateReqQual.

  if not available stateReqqual
   then
    return.

  message "Highlighted State Requirement Qualification will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete State Requirement Qualification"
    update lContinue as logical.
  
  if not lContinue
   then
    return.

  /* delete the requirement qualification and update data model. */
  publish "deleterequirementqual" (input stateReqQual.statereqqualId, /* StatereqqualID */
                                   output std-lo,                     /* Success or not */
                                   output std-ch) .                   /* Error msg from server */
  
  if not std-lo  
   then
    do:
      message std-ch
         view-as alert-box error buttons yes-no.
      return.
    end.
      
  /* On success and no error msg from server we are updating the data */
  
  close query brwstateReqQual.
  empty temp-table tStateReqQual.

  /* Get update data from data model. */
  publish "getRequirementQuals" (cbState:input-value in frame {&frame-name},
                                 {&ALL},
                                 {&ALL},
                                  0 ,
                                 output table tStateReqQual,
                                 output std-lo,
                                 output std-ch).

  if not std-lo  /* if not return */
   then
    do:
      message std-ch
            view-as alert-box error buttons ok.
      return.
    end.


  empty temp-table StateReqQual.

  for each tstateReqQual:
    create statereqqual.
    buffer-copy tStateReqQual to stateReqQual.
  end.

  run displayRequirementQualification in this-procedure.
  run showStatus                      in this-procedure.
  run setWidgetsState                 in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayRequirement C-Win 
PROCEDURE displayRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwstateReq for each staterequirement
        where staterequirement.reqFor = (if rsFulffiledBy2:input-value in frame {&frame-name} = {&OrganizationCode} then {&OrganizationCode} else {&PersonCode}) and
                  staterequirement.appliesTo = (if cbAppliesTo:input-value in frame {&frame-name} = {&ALL} then staterequirement.appliesto  else cbAppliesTo:input-value) .
      
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayRequirementQualification C-Win 
PROCEDURE displayRequirementQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwstateReqQual for each stateReqQual where stateReqQual.requirementID = iReqID.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState rsFulffiledBy2 cbAppliesTo flDetails 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwstateReq bActivateStateReqQual bDeleteStateReq bDeleteStateReqQual 
         bEditStateReq bEditStateReqQual bExport bNewStateReq bNewStateReqQual 
         bRefresh cbState rsFulffiledBy2 cbAppliesTo flDetails brwstateReqQual 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hTableHandle as handle no-undo.
  
  define buffer stateRequirement  for stateRequirement.
  define buffer tStateRequirement for tStateRequirement.

  empty temp-table tStateRequirement.

  do with frame {&frame-name}:
  end.

  if query brwstateReq:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  for each stateRequirement 
    where stateRequirement.appliesTo = (if cbAppliesTo:screen-value = {&ALL} then stateRequirement.appliesTo else cbAppliesTo:screen-value) and 
         (staterequirement.reqfor = {&OrganizationCode} or staterequirement.reqfor = {&PersonCode}):
    create tStateRequirement.
    buffer-copy stateRequirement to tStateRequirement.
    assign
        tStateRequirement.authorizedBy = getauth(stateRequirement.authorizedBy).
        tStateRequirement.reqfor = getreqfor(stateRequirement.reqfor).
        tStateRequirement.fulfilledBy = getreqfor(stateRequirement.fulfilledBy).
  end.
  
  publish "GetReportDir" (output std-ch).
  
  hTableHandle = temp-table tStateRequirement:handle.
  
  run util/exporttable.p (table-handle hTableHandle,
                          "tStateRequirement",
                          "for each tStateRequirement ",
                          "reqfor,appliesTO,refId,stateID,description,effectiveDate,expirationDate,authorizedBy,fulfillable,qualification,fulfilledBy,contactName,contactAddress,contactcity,contactstate,contactzip,url,method,notificationApproval,notificationApprovalDays,notificationCancellation,notificationCancellationDays,renewalrequired,expire,expireMonth,expireDay,expireYear,expireDays,notes,name,address",
                          "Applies To,Role,Entity ID,State ID,Requirement,Effective Date,Expiration Date,Authorized By,Fulfillable,Qualification,Fulfilled By,Contact Name,Contact Address,Contact City,Contact State,Contact Zip,URL,Method,Notification Approval,Notification Approval Days,Notification Cancellation,Notification Cancellation Days,Renewal Required,Expire,Expire Month,Expire Day,Expire Year,Expire Days,Notes,Entity Name,Entity Address",
                          std-ch,
                          "stateRequirements-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRequirements C-Win 
PROCEDURE getRequirements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer staterequirement for staterequirement.
  define buffer stateReqQual     for stateReqQual.

  if cbState:input-value in frame {&frame-name} eq "" 
   then
    do:
      close query brwStateReq.
      close query brwStateReqQual.
      run setWidgetsState in this-procedure.
      return.
    end.  
 
  empty temp-table staterequirement.
  empty temp-table stateReqQual.

  /* get the requirements from server and updates the data model. */
  publish "getRequirements" (input  cbState:input-value,    /* stateID  */   
                             input  0,                      /* reqID = 0 for ALL */
                             output table staterequirement , 
                             output table stateReqQual,
                             output std-lo ,              /* Success or not */           
                             output std-ch).              /* Error msg from server */    


  if not std-lo  /* Return if not success*/
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
      
  /* populate the main tables here copy content of GetRequirementsData here */
  close query brwStatereq.
  close query brwstatereqqual.

  for each stateRequirement:

   /* Copy the Requirement Qualification records to main temp-table table. */
    for each StateReqQual where StateReqQual.requirementID = stateRequirement.requirementID:
      assign
          stateReqQual.description       = stateRequirement.description
          StateReqQual.reqappliesTo      = staterequirement.appliesto
          StateReqQual.AuthorizedBy      = stateRequirement.authorizedBy
          stateRequirement.qualification = (if stateRequirement.authorizedby = {&CompanyCode} then 
                                             stateReqQual.qualification else "")
          stateReqQual.reqfor            = stateRequirement.reqfor
          stateReqQual.refid             = stateRequirement.refid
          .                                   
    end.
  end.
    
  /* Display the data on the screen. */
  run displayrequirement in this-procedure.

  /* Enabling disabling buttons. */
  run setWidgetsState    in this-procedure.

  /* Showing counts for fulfillable requirements and total requirements. */
  run showStatus         in this-procedure.

  apply "value-changed" to brwStatereq.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyRequirement C-Win 
PROCEDURE modifyRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tStateRequirement for tStateRequirement.

  empty temp-table tStateRequirement.
  
  if not available staterequirement
   then
    return.
   
  create tStateRequirement.
  buffer-copy staterequirement to tStateRequirement.
  
  /*tStateRequirement.qualification = staterequirement.qualification.*/
  
  if tStateRequirement.refId = "?" 
   then
    tStateRequirement.refId = {&ALL}.

  /* Update call for dialog */
  run dialogrequirement.w(input  cbState:input-value in frame {&frame-name} ,
                          input  table tStateRequirement,   /* non-blank in case of edit */
                          output iStateReqId,               /* StaterequirementID */           
                          output istatereqqualID,           /* stateReqqualID */           
                          output std-lo).                  /* Updated record or not.  */    

  /* if not record is updated then return */
  if not std-lo 
   then
    return.

  run getRequirements in this-procedure.

  /* Reposition to the record. */
  find last staterequirement where staterequirement.requirementID = iStateReqId no-error.
  
  if available staterequirement 
   then
    activeRowid = rowid(staterequirement).

  reposition brwstateReq to rowid activeRowid no-error.
  apply "value-changed" to brwStatereq in frame {&frame-name}.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyRequirementQualification C-Win 
PROCEDURE modifyRequirementQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table tStateReqQual.

  /* We are not allowing users to edit the active requirement qualification 
     mapping record. */
  if not available StateReqQual or statereqqual.active
   then
    return.
  
  assign
      iStateReqQualID = StateReqQual.stateReqQualID
      iStateReqID     = StateReqQual.requirementID
      .

  create tStateReqQual.
  buffer-copy StateReqQual to tStateReqQual.
  
  run dialogrequirementqual.w(input {&ActionEdit} ,                              /* edit reqQual .*/
                              input cbState:input-value in frame {&frame-name}, /* StateID  */
                              input table tStateReqQual,                         /* Non-blank for edit .*/
                              output iStateReqQualID,                            /* StatereqqualID */           
                              output std-lo).                                    /* Success or not */           
                                                            
  if not std-lo
   then
    return.

  close query brwstateReqQual.
  
  empty temp-table tStateReqQual.

  /* Get update data from data model. */
  publish "getRequirementQuals" (cbState:input-value,
                                 {&ALL},
                                 {&ALL},
                                  0 ,
                                 output table tStateReqQual,
                                 output std-lo,
                                 output std-ch).

  if not std-lo
   then
    do:
      message std-ch
            view-as alert-box error buttons ok.
      return.
    end.

  empty temp-table StateReqQual.
    
  for each tstateReqQual:
    create statereqqual.
    buffer-copy tStateReqQual to stateReqQual.
  end.
  
  run displayRequirementQualification in this-procedure.

  /* Reposition to the record. */
  find first statereqqual where statereqqual.stateReqQualID = iStateReqQualID no-error.
  if available statereqqual 
   then
    activeRowid = rowid(statereqqual).
  else
   activeRowid = ?.
  
  if string(activeRowid) <> ? 
   then
    reposition brwstateReqQual to rowid activeRowid no-error.
  run showStatus in this-procedure.
  apply "value-changed" to brwStatereqqual.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newRequirement C-Win 
PROCEDURE newRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tStateRequirement for tStateRequirement.
  define buffer tStateReqQual     for tStateReqQual.

  empty temp-table tStateRequirement.
  empty temp-table tStateReqQual.

  /* Makes the server call to save the record. Also cache the info in data model. */
  run dialogrequirement.w (input  cbState:input-value in frame {&frame-name} ,
                           input  table tStateRequirement,    /* blank in case of new */
                           output iStateReqId,                /* return requirementid from server*/
                           output istatereqqualID,            /* return statereqqualid from server*/
                           output std-lo).                    /* Success or not */

  /* if no change in the record then return. */
  if not std-lo
   then
    return.

  run getRequirements in this-procedure.

  find first staterequirement where staterequirement.requirementID =  iStateReqId no-error.
  if available staterequirement 
   then
    activeRowid = rowid(staterequirement).
     
  reposition brwstateReq to rowid activeRowid no-error.

  apply "value-changed" to brwStatereq.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newRequirementQualification C-Win 
PROCEDURE newRequirementQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
  define buffer tStateRequirement for tStateRequirement.
  define buffer tStateReqQual     for tStateReqQual.

  empty temp-table tStateReqQual.
      
  /* Creating table for new because we need this prior data from staterequirement to create record. */
  create tStateReqQual.

  assign 
      tStateReqQual.reqfor         = staterequirement.reqfor /* organization or person*/
      tStateReqQual.requirementID  = staterequirement.requirementID
      tStateReqQual.description    = staterequirement.description
      tStateReqQual.reqappliesTo   = staterequirement.appliesTo
      tStateReqQual.role           = staterequirement.role /* role will be empty always*/
      tStateReqQual.refId          = staterequirement.refId
      tStateReqQual.AuthorizedBy   = staterequirement.authorizedBy
      tStateReqQual.effectiveDate  = staterequirement.effectiveDate
      tStateReqQual.expirationDate = staterequirement.expirationDate
      
      iStateReqID                  = staterequirement.requirementID.

  run dialogrequirementqual.w(input {&ActionNew},                                 /* (N)ew */
                              input cbState:input-value in frame {&frame-name},   /* State ID */
                              input table tStateReqQual,                          /* input statereqqual */
                              output iStateReqQualID,                             /* return the new StateReqqualID from server. */
                              output std-lo).                                     /* Return success or not */


  if not std-lo
   then
    return.

  /* Closing query and empty temp-table */
  close query brwstateReqQual.
  
  empty temp-table tStateReqQual.

  /* Get update data from data model. */
  publish "getRequirementQuals" (cbState:input-value in frame {&frame-name},
                                 {&ALL},
                                 {&ALL},
                                  0 ,
                                 output table tStateReqQual,
                                 output std-lo,
                                 output std-ch).

  if not std-lo
   then
    do:
      message std-ch
            view-as alert-box error buttons ok.
      return.
    end.

  empty temp-table StateReqQual.

  for each tstateReqQual:
    create statereqqual.
    buffer-copy tStateReqQual to stateReqQual.
  end.

  run displayRequirementQualification in this-procedure.
  
  apply "value-changed" to brwStatereq.

  find last StateReqQual 
    where StateReqQual.stateReqQualID = iStateReqQualID no-error.
  if available StateReqQual 
   then
    activeRowid = rowid(StateReqQual).
  
  reposition brwstateReqQual to rowid activeRowid no-error.
  
  /* Setting widgets state */
  run setWidgetsState in this-procedure.
  run showStatus      in this-procedure.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshRequirements C-Win 
PROCEDURE refreshRequirements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* get the requirements from server and updates data model. */
  publish "refreshRequirements" (input  cbState:input-value in frame fMain, 
                                 input  0,
                                 output std-lo,
                                 output std-ch).

  if not std-lo  /* Return if not success*/
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
     
  run getRequirements in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetsState C-Win 
PROCEDURE setWidgetsState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Setting widgets states w.r.t to data in browse    
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  /* if cbstate:input-value = "" */
  if cbState:input-value <> "" 
   then
    do:
      assign
          bRefresh              :sensitive = true 
          cbAppliesTo           :sensitive = true
          bNewStateReq          :sensitive = true
          brwstateReq           :sensitive = true
          bExport               :sensitive = true
          bEditStateReq         :sensitive = true
          bDeleteStateReq       :sensitive = true
          flDetails             :sensitive = true
          bNewStateReqQual      :sensitive = true
          brwstateReqQual       :sensitive = true
          bEditStateReqQual     :sensitive = true
          bDeleteStateReqQual   :sensitive = true
          bActivateStateReqQual :sensitive = true
          rsFulffiledBy2        :sensitive = true
          .
    end.
   else
    do:
      assign
          bRefresh              :sensitive = false
          cbAppliesTo           :sensitive = false
          bNewStateReq          :sensitive = false
          brwstateReq           :sensitive = false
          bExport               :sensitive = false
          bEditStateReq         :sensitive = false
          bDeleteStateReq       :sensitive = false
          flDetails             :sensitive = false
          bNewStateReqQual      :sensitive = false
          brwstateReqQual       :sensitive = false
          bEditStateReqQual     :sensitive = false
          bDeleteStateReqQual   :sensitive = false
          bActivateStateReqQual :sensitive = false
          rsFulffiledBy2        :sensitive = false.
    end.

  if query brwstateReq:num-results = 0 
   then
    assign
        bEditStateReq   :sensitive = false
        bDeleteStateReq :sensitive = false
        bExport         :sensitive = false
        bNewStateReqQual:sensitive = false 
        .
   else
    do:
      /* For first party requirements, disabled the new button in qualreq browse.
         This is because, qualreq are created while creating the requirements. And there is
         one to one mapping between the requirement and qualifications.  */
      if not available staterequirement 
       then 
        return.
  
      if staterequirement.authorizedby = {&FirstParty} 
       then
        bNewStateReqQual:sensitive = false.
      else
       bNewStateReqQual:sensitive = true.
       
      if can-find(first stateReqQual where stateReqQual.requirementID = staterequirement.requirementID
                                       and stateReqQual.active      = true) 
       then
        bDeleteStateReq:sensitive = false.
      else 
       bDeleteStateReq:sensitive = true.
    end.
   
  if can-find(first stateReqQual where stateReqQual.requirementID = staterequirement.requirementID) 
   then
    do:
      assign  
          bEditStateReqQual    :sensitive = true
          bDeleteStateReqQual  :sensitive = true
          bActivateStateReqQual:sensitive = true 
          .

      if not available stateReqQual 
       then
        return.

      /* For first party requirements, edit and delete buttons in qualreq browse are disabled. */
      if stateReqQual.AuthorizedBy = {&FirstParty}  
       then
        assign 
            bEditStateReqQual   :sensitive = false
            bDeleteStateReqQual :sensitive = false
            .
  
      if StateReqQual.active 
       then
        do:
          assign 
              bDeleteStateReqQual:sensitive = false
              bEditStateReqQual  :sensitive = false
              .
         
          bActivateStateReqQual:load-image("images/s-flag_red").
          bActivateStateReqQual:tooltip = "Deactivate state requirement qualification".
       end.
       else
       do:
          if staterequirement.authorizedby = {&firstparty} 
           then
             bEditStateReqQual:sensitive = false.
          else
            assign
                bEditStateReqQual  :sensitive = true
                bDeleteStateReqQual:sensitive = true
                .

          bActivateStateReqQual:load-image("images/s-flag_green").
          bActivateStateReqQual:tooltip = "Activate state requirement qualification".
        end.
    end.

  else 
   assign 
       bEditStateReqQual    :sensitive = false
       bDeleteStateReqQual  :sensitive = false
       bActivateStateReqQual:sensitive = false 
       .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showStatus C-Win 
PROCEDURE showStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Showing the count of total requirements and how many are fulfillable   
------------------------------------------------------------------------------*/
  define variable cStatus     as character no-undo.
  define variable iTotReq     as integer   no-undo.
  define variable iActReq     as integer   no-undo.
  
  define buffer staterequirement for staterequirement.
  define buffer stateReqqual     for stateReqqual.
  do with frame {&frame-name}:
  end.
  assign
       iTotReq = 0
       iActReq = 0.
  
  for each stateRequirement where staterequirement.reqfor = rsFulffiledBy2:input-value and 
    staterequirement.appliesto = (if cbappliesto:input-value = {&ALL} then staterequirement.appliesto else cbappliesto:input-value ) :
    iTotReq = iTotReq + 1.
    for first stateReqqual where stateReqqual.requirementId = stateRequirement.requirementId
                           and stateReqqual.active:
      iActReq = iActReq + 1.
    end.
  end.
  
  cStatus = string(iTotReq) + " Total Requirements, " + string(iActReq) + " are Fulfillable.".

  /* if cbAppliesTo filter does not have any record then we are setting this status message as 0 records. */
  if query brwstateReq:num-results = 0 and query brwstateReqQual:num-results = 0 
   then
    cStatus = "0 Records".

  setStatusMessage(cStatus).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 {lib/brw-sortData-multi.i }
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getauth C-Win 
FUNCTION getauth returns character ( cauth as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cauth = {&ThirdParty}
   then
    return {&Third-Party}.   /* Function return value. */
  else
   return {&First-Party}.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getreqfor C-Win 
FUNCTION getreqfor returns character ( cauth as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cauth = {&OrganizationCode} 
   then
    return {&Organization}.   /* Function return value. */
  else
   return {&Person}.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

