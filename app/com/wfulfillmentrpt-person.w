&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wfulfillmentrpt-person.w

  Description: Screen for Requirement Vs Qualification Report.
  
  Modified:
  
  03.29.2022    Shefali        Task# 86699  Modified in exportData IP.
  06.27.2024    Sachin         Task# 113997 Recommitting the removed changes related to Task #111982 
                               (Show 'Fulfilled By' in person and organization fulfillment reports)
  11/28/2024    Shefali        Task #117467 Modify to get agent fulfillments record as per requirementID 
                               for Agent Fulfillment Report in AMD
  ----------------------------------------------------------------------*/   

create widget-pool.
  
/*Parameter Definition*/
define input parameter ipcentity as character no-undo.

/* Include Files Definition */
{tt/reqqual.i}
{tt/reqqual.i &tableAlias=ttresult}
{tt/person.i}

{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}


/* Variables Definition */
define variable dColumnWidth     as decimal   no-undo.
define variable iBgColor         as integer   no-undo.
define variable plastEntityid    as character no-undo.
define variable chAction         as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQualification

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttresult

/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification ttresult.stateID ttresult.entityId ttresult.entityName ttresult.role ttresult.roleName ttresult.roleID ttresult.requirementDesc ttresult.qualificationDesc ttresult.stat "Status" ttresult.effDate "Effective" ttresult.expDate ttresult.reviewDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification   
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification for each ttresult
&Scoped-define OPEN-QUERY-brwQualification open query {&SELF-NAME} for each ttresult.
&Scoped-define TABLES-IN-QUERY-brwQualification ttresult
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification ttresult


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwQualification}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btGet cbState cbrole brwQualification ~
RECT-55 RECT-56 
&Scoped-Define DISPLAYED-OBJECTS cbState cbrole fPerson cbrequirement ~
cbauthorized cbmet 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData returns logical private ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getreqfor C-Win 
FUNCTION getreqfor returns character ( cauth as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged returns logical private
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bPersonLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Person lookup".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON btFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reset Filter".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cbauthorized AS CHARACTER FORMAT "X(256)":U 
     LABEL "Authorized by" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEM-PAIRS "Third Party","T",
                     "First Party","C",
                     "ALL","B"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cbmet AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement Status" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEM-PAIRS "Met","M",
                     "Unmet","U",
                     "ALL","B"
     DROP-DOWN-LIST
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE cbrequirement AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "ALL" 
     DROP-DOWN-LIST
     SIZE 66.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbrole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE fPerson AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Person" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 61.6 BY 1.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 92.8 BY 3.81.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42.2 BY 3.81.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 11 BY 3.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQualification FOR 
      ttresult SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification C-Win _FREEFORM
  QUERY brwQualification DISPLAY
      ttresult.stateID           column-label "StateID"         format "x(8)"       width 9      
ttresult.entityId          column-label "Person ID"       format "x(10)"      width 12         
ttresult.entityName        column-label "Person Name"     format "x(80)"      width 30
ttresult.role              column-label "Role"            format "x(15)"      width 15
ttresult.roleName          column-label "Name"            format "x(80)"      width 22
ttresult.roleID            column-label "ID"              format "x(10)"      width 10
ttresult.requirementDesc   column-label "Requirement"     format "x(50)"      width 50
ttresult.qualificationDesc column-label "Qualification"   format "x(50)"      width 31
ttresult.stat              label        "Status"          format "x(16)"      width 16
ttresult.effDate           label        "Effective"       format "99/99/9999" width 12         
ttresult.expDate           column-label "Expiration"      format "99/99/9999" width 14
ttresult.reviewDate        column-label "Review"          format "99/99/9999" width 14
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 229.2 BY 16.81
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     btGet AT ROW 2.52 COL 35 WIDGET-ID 262 NO-TAB-STOP 
     btFilter AT ROW 2.43 COL 127.6 WIDGET-ID 274 NO-TAB-STOP 
     cbState AT ROW 2.24 COL 8.8 COLON-ALIGNED WIDGET-ID 260
     bPersonLookup AT ROW 2.71 COL 121.8 WIDGET-ID 282 NO-TAB-STOP 
     cbrole AT ROW 3.38 COL 8.8 COLON-ALIGNED WIDGET-ID 278
     fPerson AT ROW 2.81 COL 58 COLON-ALIGNED WIDGET-ID 280 NO-TAB-STOP 
     cbrequirement AT ROW 1.67 COL 58 COLON-ALIGNED WIDGET-ID 100
     btExport AT ROW 2.43 COL 138 WIDGET-ID 2 NO-TAB-STOP 
     cbauthorized AT ROW 3.95 COL 58 COLON-ALIGNED WIDGET-ID 270
     cbmet AT ROW 3.95 COL 109.2 COLON-ALIGNED WIDGET-ID 272
     brwQualification AT ROW 5.48 COL 1.8 WIDGET-ID 200
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.14 COL 45 WIDGET-ID 242
     "Parameters" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 1.19 COL 3 WIDGET-ID 252
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.14 COL 137.2 WIDGET-ID 266
     RECT-2 AT ROW 1.43 COL 43.8 WIDGET-ID 8
     RECT-55 AT ROW 1.43 COL 2 WIDGET-ID 250
     RECT-56 AT ROW 1.43 COL 136.2 WIDGET-ID 264
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 230.6 BY 22.24
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Fulfillment Report"
         HEIGHT             = 22.48
         WIDTH              = 230.6
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwQualification cbmet fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bPersonLookup IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwQualification:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwQualification:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON btExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btFilter IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbauthorized IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbmet IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbrequirement IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fPerson IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fPerson:READ-ONLY IN FRAME fMain        = TRUE
       fPerson:PRIVATE-DATA IN FRAME fMain     = 
                "ALL".

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query {&SELF-NAME} for each ttresult.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Fulfillment Report */
or endkey of {&window-name} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Fulfillment Report */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Fulfillment Report */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPersonLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPersonLookup C-Win
ON CHOOSE OF bPersonLookup IN FRAME fMain /* Lookup */
do:
  run openDialog in this-procedure. 
  apply "value-changed":U to fPerson.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON DEFAULT-ACTION OF brwQualification IN FRAME fMain
do:
  if not available ttresult 
   then
    return.

  publish "OpenWindow" (input {&person}, 
                        input ttresult.entityID, 
                        input "wpersondetail.w", 
                        input "character|input|" + ttresult.entityID + "^integer|input|2" + "^character|input|",                                   
                        input this-procedure).                                           
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON ROW-DISPLAY OF brwQualification IN FRAME fMain
do:
  /* In this report, we cannot use the standard brw-rowdisplay.i.
     This is beacuse we need to show all the fulfillment records of 
     an entity in same color. */
    
  if plastEntityid = "" 
   then
    assign
        plastEntityid = ttresult.entityId
        iBgColor      = 17
        .
  if plastEntityid ne ttresult.entityId 
   then
    do:
      if iBgColor = 17 then
        iBgColor = 15.
      else
        iBgColor = 17.
    end.

  assign 
      ttresult.entityId:bgcolor in browse brwqualification          = iBgcolor
      ttresult.entityName:bgcolor in browse brwqualification        = iBgcolor
      ttresult.role:bgcolor in browse brwqualification              = iBgcolor
      ttresult.requirementDesc:bgcolor in browse brwqualification   = iBgcolor
      ttresult.qualificationDesc:bgcolor in browse brwqualification = iBgcolor
      ttresult.stat:bgcolor in browse brwqualification              = iBgcolor
      ttresult.effDate:bgcolor in browse brwqualification           = iBgcolor
      ttresult.expDate:bgcolor in browse brwqualification           = iBgcolor
      plastEntityid                                                 = ttresult.entityId
      .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON START-SEARCH OF brwQualification IN FRAME fMain
do:
  assign
      hSortColumn = browse {&browse-name}:current-column
      chAction    = hSortColumn:name
      .

  if chAction = "PersonId" or
     chAction = "PersonName" 
   then
    do:
      {lib\brw-startSearch.i}
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFilter C-Win
ON CHOOSE OF btFilter IN FRAME fMain /* Filter */
do:
  /* Reset filter widgets */
  assign 
      cbrequirement:screen-value = {&ALL}
      fPerson:private-data       = {&ALL}
      fPerson:screen-value       = {&ALL}
      cbauthorized:screen-value  = {&RadioBoth}
      cbmet:screen-value         = {&RadioBoth}
      .

  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* Get */
do:
  clearData().
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbauthorized
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbauthorized C-Win
ON VALUE-CHANGED OF cbauthorized IN FRAME fMain /* Authorized by */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbmet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbmet C-Win
ON VALUE-CHANGED OF cbmet IN FRAME fMain /* Requirement Status */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbrequirement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbrequirement C-Win
ON VALUE-CHANGED OF cbrequirement IN FRAME fMain /* Requirement */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  clearData().
  resultsChanged(false).

  btGet:sensitive = (self:screen-value <> "").
  
  /* Delete Select State from state combo-box */
  {lib/modifylist.i &cbhandle=cbState:handle &list=list-item-pairs &delimiter = "," &item = '{&SelectState}'} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPerson
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPerson C-Win
ON VALUE-CHANGED OF fPerson IN FRAME fMain /* Person */
DO:
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
   run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

btExport:load-image("images/excel.bmp").
btExport:load-image-insensitive("images/excel-i.bmp").
btGet:load-image("images/Completed.bmp").
btGet:load-image-insensitive("images/Completed-i.bmp").
btFilter:load-image("images/filtererase.bmp").
btFilter:load-image-insensitive("images/filtererase-i.bmp").
bPersonLookup:load-image("images/s-lookup.bmp").
bPersonLookup:load-image-insensitive("images/s-lookup-i.bmp").

{lib/win-main.i}
{lib/win-status.i}

C-Win:title = getreqfor(ipcentity) + " Fulfillment Report" .

publish "GetSearchStates" (output std-ch).
if std-ch > "" then
   std-ch = "," + std-ch.
cbState:list-item-pairs in frame {&frame-name} = {&ALL} + "," + {&ALL} + std-ch.
cbState:screen-value = {&ALL}.
 
/* Function to reset default values and clear filters */
clearData().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  {lib/get-column-width.i &col="'entityName'"        &var=dColumnWidth} 
  {lib/get-column-width.i &col="'requirementDesc'"   &var=dColumnWidth}
  {lib/get-column-width.i &col="'qualificationDesc'" &var=dColumnWidth}

  run changerole in this-procedure.
 
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  run windowResized in this-procedure.

  if not this-procedure:persistent
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changerole C-Win 
PROCEDURE changerole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.
  
  define variable capplies as character no-undo.
  define variable pList    as character no-undo.
    
  std-lo = false.
  std-ch = "".
  capplies = "".
     
  publish "getComplianceCodesList" ({&PersonRole},
                                     input ",",
                                     output pList,
                                     output std-lo,
                                     output std-ch).
  capplies = {&Person}.

  if not std-lo
   then
     do:
       message std-ch
         view-as alert-box error buttons ok.
       return.
     end.
                           
  /* Assigning the list */                            
  assign
      pList = trim (pList,",")
      cbrole:list-items = "ALL" + "," + pList
      cbrole:screen-value = cbrole:list-items.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState cbrole fPerson cbrequirement cbauthorized cbmet 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE btGet cbState cbrole brwQualification RECT-55 RECT-56 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable as handle no-undo.

  if query brwQualification:num-results = 0
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  publish "GetReportDir" (output std-ch).
  htable = temp-table ttresult:handle.
  
  run util/exporttable.p (table-handle htable,
                          "ttresult",
                          "for each ttresult ",
                          "stateID,entityId,entityName,role,fulfilledBy,requirementDesc,qualificationDesc,stat,effDate,expDate,reviewDate",
                          "StateID,Person ID,Person Name,Role,Fulfilled By,Requirement,Qualification,Status,Effective,Expiration,Review",
                          std-ch,
                          "EntityRequirementsVsQualifications" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).                           
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  close query brwQualification.
  empty temp-table ttresult.

  for each reqQual where reqQual.requirementDesc = (if cbrequirement:screen-value  = "ALL" then reqQual.requirementDesc else cbrequirement:screen-value)
                     and reqQual.requirementType = (if cbauthorized:screen-value = {&CompanyCode} or cbauthorized:screen-value = {&ThirdParty} then cbauthorized:input-value else reqQual.requirementType)
                    and reqQual.entityId         = (if ( fPerson:private-data     = "ALL" or  fPerson:private-data = ?) then reqQual.entityId else fPerson:private-data) :
    if cbmet:screen-value = {&RadioMet} 
     then
       do:
         if reqQual.qualificationId <> 0 
          then
           do:
             create ttresult.
             buffer-copy reqQual to ttresult.
           end.
       end.
    else if cbmet:screen-value = {&RadioUnmet} 
     then
      do:
        if reqQual.qualificationId = 0 
         then
          do:
            create ttresult.
            buffer-copy reqQual to ttresult.
          end.
      end.
    else
     do:
       create ttresult.
       buffer-copy reqQual to ttresult.
     end.
  end.
   
  for each ttresult:
    ttresult.stat = getQualificationDesc(ttresult.stat).
  end.
  
  open query brwqualification preselect each ttresult.

  /* Display no. of records on status bar */
  setStatusCount(query brwqualification:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define buffer reqQual for reqQual. 
  do with frame {&FRAME-NAME} :
  end.

  /* client Server Call */
  run server/queryrequirementsqualifications.p (input cbState:input-value,
                                                input ipcentity,
                                                input cbrole:input-value,
                                                input "",
                                                output table reqQual,
                                                output std-lo,
                                                output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
     
  /* Set the filters based on the data returned, with ALL as the first option */
  for each reqQual 
    break by reqQual.requirementDesc:
    if not first-of(reqQual.requirementDesc) 
     then next.
    cbrequirement:add-last(reqQual.requirementDesc) in frame {&frame-name}.
  end.
  
  for each reqQual
    break by reqQual.entityId:
    if not first-of(reqQual.entityId) 
     then next.   
   /* cbPerson:add-last(replace(reqQual.entityName, ",", " ") + " (" + replace(reqQual.entityId, ",", " ") + ")", replace(reqQual.entityId, ",", " ")) in frame {&frame-name}.   
    cbOrg:add-last(replace(reqQual.entityName, ",", " ") + " (" + replace(reqQual.entityId, ",", " ") + ")", replace(reqQual.entityId, ",", " ")) in frame {&frame-name}.   
 */ end. 

  /* This will use the screen-value of the filters which is ALL */
  run filterData in this-procedure.
  
  /* Makes widget enable-disable based on the data */ 
  if query brwQualification:num-results > 0 
   then
    assign 
        browse brwQualification:sensitive = true
               btExport:sensitive         = true
               cbrequirement:sensitive    = true
               fPerson:sensitive          = true
               bPersonLookup:sensitive    = true
               cbauthorized:sensitive     = true
               cbmet:sensitive            = true
               btFilter:sensitive         = true
               .
  else
   assign 
       browse brwQualification:sensitive = false
       btExport:sensitive                = false
       cbrequirement:sensitive           = false
       fPerson:sensitive                 = false
       bPersonLookup:sensitive           = false
       cbauthorized:sensitive            = false
       cbmet:sensitive                   = false
       btFilter:sensitive                = false
       .
  
  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwqualification:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog C-Win 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable cPrevPersonID as character              no-undo.
  define variable cPersonID     as character              no-undo.
  define variable cName         as character              no-undo.
  
  if fPerson:private-data <> ""
   then
    cPrevPersonID = fPerson:private-data. 
    
  run dialogpersonlookup.w(input  cPrevPersonID,
                           input  false,        /* True if add "New" */
                           input  false,        /* True if add "Blank" */
                           input  true,         /* True if add "ALL" */
                           output cPersonID,
                           output cName,
                           output std-lo).
   
  if not std-lo 
   then
    return no-apply.

  if cName = {&ALL}
   then
    do:
      fPerson:screen-value = cName.
      fPerson:private-data = cName.
    end.
  else
   do:
     fPerson:screen-value = if cPersonID = "" then "" else cName.
     fPerson:private-data = if cPersonID = "" then "" else cPersonID.
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.
  
  if chAction = "entityName" then
    tWhereClause = tWhereClause + " by ttresult.entityId ".
  else 
    tWhereClause = "".
 
  {lib/brw-sortData.i &post-by-clause=" + tWhereClause"}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.
 
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 7
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 2
      .
  
  {lib/resize-column.i &col="'entityName,requirementDesc,qualificationDesc'" &var=dColumnWidth}
  run ShowScrollBars(browse brwQualification:handle, no, yes). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData returns logical private ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
  /* Initialise filters to default values */
  assign 
      cbrequirement:list-items in frame {&frame-name} = "ALL"
      .

  /* Set default values for the client side filters */
  assign
      cbrequirement:screen-value = "ALL"
      cbauthorized:screen-value  = {&RadioBoth}
      cbmet:screen-value         = {&RadioBoth}
      .

  /* disable widgets on launch of report and everytime different state is selected */
  disable fPerson bPersonLookup cbrequirement cbauthorized cbmet
          btExport btFilter with frame {&frame-name}.

  setStatusMessage("").
  return true. /* Function return value */
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  RETURN std-ch.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getreqfor C-Win 
FUNCTION getreqfor returns character ( cauth as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cauth = {&OrganizationCode} 
   then
    return {&Organization}.   /* Function return value. */
  else
   return {&Person}.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged returns logical private
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 return true.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

