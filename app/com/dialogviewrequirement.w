&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogviewrequirement.w

  Description: UI to view state requirement record.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Gurvindar 

  Created:12/12/2018
  Modified:  04/02/2020  Archana Gupta  Modified code according to new organization structure
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/*------Temp-table definitons-------------------------------------------*/
{tt/staterequirement.i} 

/* Parameters definitions ---    */
define input parameter table for StateRequirement.

{lib/std-def.i}
{lib/com-def.i}
{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BtnUrl fState flAppliesTo flRole ~
fldescription fpersonName tParentAddr tParentCity fWebsite RECT-3 feffDate ~
RECT-4 fNotifApprDays fexpDate fNotifCancDays flQualification flExpiry ~
fexpiryMonth fexpiryDays fexpiryDay fexpiryYear Enotes tNotes 
&Scoped-Define DISPLAYED-OBJECTS fState flAppliesTo flRole fldescription ~
fpersonName tParentAddr tParentCity tParentZip tParentState fWebsite ~
feffDate fNotifApprDays fexpDate rsFulffiledBy2 fNotifCancDays tfirstparty ~
flQualification flExpiry fexpiryMonth rsFulffiledBy fexpiryDays fexpiryDay ~
fexpiryYear Enotes tNotifAppr tNotifCanc tNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON BtnUrl DEFAULT  NO-FOCUS
     LABEL "URL" 
     SIZE 4.6 BY 1.14 TOOLTIP "Open URL"
     BGCOLOR 8 .

DEFINE VARIABLE tParentState AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 8.8 BY 1 NO-UNDO.

DEFINE VARIABLE Enotes AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 168.8 BY 4.05 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fexpiryDay AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Day" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE fexpiryDays AS INTEGER FORMAT ">>>>":U INITIAL 0 
     LABEL "Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE fexpiryMonth AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Month" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE fexpiryYear AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE flAppliesTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Applies To" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE fldescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE flExpiry AS CHARACTER FORMAT "X(256)":U 
     LABEL "Method" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE flQualification AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE flRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE fNotifApprDays AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE fNotifCancDays AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE fpersonName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE VARIABLE fWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER FORMAT "X(256)":U INITIAL "Notes:" 
      VIEW-AS TEXT 
     SIZE 6.6 BY .62 NO-UNDO.

DEFINE VARIABLE tParentAddr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tParentCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 34.8 BY 1 TOOLTIP "Enter the Issuer city" NO-UNDO.

DEFINE VARIABLE tParentZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14.8 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE VARIABLE rsFulffiledBy AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 25.6 BY .95 NO-UNDO.

DEFINE VARIABLE rsFulffiledBy2 AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 29.8 BY .95 TOOLTIP "Select state requirement for organization or person" NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 72.8 BY 9.43.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 95 BY 9.43.

DEFINE VARIABLE tfirstparty AS LOGICAL INITIAL no 
     LABEL "Company Qualification" 
     VIEW-AS TOGGLE-BOX
     SIZE 29 BY .81
     FONT 6 NO-UNDO.

DEFINE VARIABLE tNotifAppr AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 NO-UNDO.

DEFINE VARIABLE tNotifCanc AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     BtnUrl AT ROW 12.05 COL 105.2 WIDGET-ID 92 NO-TAB-STOP 
     fState AT ROW 1.67 COL 15.8 COLON-ALIGNED WIDGET-ID 94
     flAppliesTo AT ROW 3.86 COL 15.8 COLON-ALIGNED WIDGET-ID 306
     flRole AT ROW 4.95 COL 15.8 COLON-ALIGNED WIDGET-ID 308
     fldescription AT ROW 6.05 COL 15.8 COLON-ALIGNED WIDGET-ID 310
     fpersonName AT ROW 8.81 COL 27.2 COLON-ALIGNED WIDGET-ID 24
     tParentAddr AT ROW 9.91 COL 27.2 COLON-ALIGNED WIDGET-ID 54
     tParentCity AT ROW 11 COL 27.2 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     tParentZip AT ROW 11 COL 72.4 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     tParentState AT ROW 11 COL 62.8 COLON-ALIGNED NO-LABEL WIDGET-ID 288 NO-TAB-STOP 
     fWebsite AT ROW 12.1 COL 27.2 COLON-ALIGNED WIDGET-ID 62
     feffDate AT ROW 14.57 COL 38.4 COLON-ALIGNED WIDGET-ID 6
     fNotifApprDays AT ROW 14.57 COL 92.2 COLON-ALIGNED WIDGET-ID 68
     fexpDate AT ROW 15.67 COL 38.4 COLON-ALIGNED WIDGET-ID 22
     rsFulffiledBy2 AT ROW 2.86 COL 18.2 NO-LABEL WIDGET-ID 358 NO-TAB-STOP 
     fNotifCancDays AT ROW 15.67 COL 92.2 COLON-ALIGNED WIDGET-ID 72
     tfirstparty AT ROW 7.52 COL 116 WIDGET-ID 88 NO-TAB-STOP 
     flQualification AT ROW 9.81 COL 126.8 COLON-ALIGNED WIDGET-ID 312
     flExpiry AT ROW 12.24 COL 139.6 COLON-ALIGNED WIDGET-ID 314
     fexpiryMonth AT ROW 13.33 COL 139.6 COLON-ALIGNED WIDGET-ID 78
     rsFulffiledBy AT ROW 8.62 COL 129 NO-LABEL WIDGET-ID 290 NO-TAB-STOP 
     fexpiryDays AT ROW 13.33 COL 157.6 COLON-ALIGNED WIDGET-ID 76
     fexpiryDay AT ROW 14.43 COL 139.6 COLON-ALIGNED WIDGET-ID 80
     fexpiryYear AT ROW 15.52 COL 139.6 COLON-ALIGNED WIDGET-ID 304
     Enotes AT ROW 17.81 COL 17.8 NO-LABEL WIDGET-ID 34
     tNotifAppr AT ROW 14.71 COL 83.8 WIDGET-ID 66 NO-TAB-STOP 
     tNotifCanc AT ROW 15.81 COL 83.8 WIDGET-ID 70 NO-TAB-STOP 
     tNotes AT ROW 17.81 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 82
     "Issued To:" VIEW-AS TEXT
          SIZE 10.4 BY .62 AT ROW 8.76 COL 118.2 WIDGET-ID 294
     "Issued To:" VIEW-AS TEXT
          SIZE 10.4 BY .62 AT ROW 3.1 COL 7.4 WIDGET-ID 362
     "Authority" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 7.57 COL 19.8 WIDGET-ID 338
          FONT 6
     "Default Expiration Date:" VIEW-AS TEXT
          SIZE 23 BY .62 TOOLTIP "Method for setting the expiration date on a newly issued Qualification" AT ROW 11.29 COL 128.8 WIDGET-ID 316
     "Cancellation:" VIEW-AS TEXT
          SIZE 12.8 BY .62 AT ROW 15.95 COL 70.4 WIDGET-ID 300
     "Approval:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 14.81 COL 73.6 WIDGET-ID 298
     "Enforceable:" VIEW-AS TEXT
          SIZE 13.2 BY .62 TOOLTIP "Informational only - when is the Requirement in effect?" AT ROW 13.62 COL 29.2 WIDGET-ID 340
     "Notification Required On:" VIEW-AS TEXT
          SIZE 23.6 BY .62 AT ROW 13.62 COL 64.8 WIDGET-ID 324
     RECT-3 AT ROW 7.86 COL 114 WIDGET-ID 342
     RECT-4 AT ROW 7.86 COL 17.8 WIDGET-ID 336
     SPACE(75.19) SKIP(5.17)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "View Requirement" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       Enotes:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       feffDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fexpDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fexpiryDay:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fexpiryDays:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fexpiryMonth:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fexpiryYear:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flAppliesTo:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fldescription:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flExpiry:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flQualification:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       flRole:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fNotifApprDays:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fNotifCancDays:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fpersonName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fState:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       fWebsite:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR RADIO-SET rsFulffiledBy IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET rsFulffiledBy2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tfirstparty IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tNotifAppr IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tNotifCanc IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       tParentAddr:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

ASSIGN 
       tParentCity:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR COMBO-BOX tParentState IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentZip IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* View Requirement */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnUrl Dialog-Frame
ON CHOOSE OF BtnUrl IN FRAME Dialog-Frame /* URL */
do:
  if fWebsite:screen-value <> "" 
   then
    run openURL in this-procedure (fWebsite:screen-value).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fWebsite
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fWebsite Dialog-Frame
ON MOUSE-SELECT-DBLCLICK OF fWebsite IN FRAME Dialog-Frame /* Website */
do:
  apply "choose" to BtnUrl.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsFulffiledBy2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsFulffiledBy2 Dialog-Frame
ON VALUE-CHANGED OF rsFulffiledBy2 IN FRAME Dialog-Frame
do:
  run changerole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? then
  frame {&frame-name}:parent = active-window.

  /* Now enable the interface and wait for the exit condition.            */

  btnurl:load-image ("images/s-url.bmp").
  btnurl:load-image-insensitive("images/s-url-i.bmp").

/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
  MAIN-BLOCK:
  do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
     on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

    run enable_UI.

    /* diaplaying data on screen. */
    run displayData.

    wait-for go of frame {&frame-name}.
  end.
  
  run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  /* Setting data on screen and showing */

  find first StateRequirement no-error.
  if not available StateRequirement 
   then
    return.
  if staterequirement.appliesto = {&Agent} 
   then
    flRole:label = {&Agent} + ":"   .
  else if staterequirement.appliesto = {&Attorney} 
   then
    flRole:label = {&Attorney} + ":" .
    
  assign                                 
      fState:screen-value           = staterequirement.stateID
      rsFulffiledBy2:screen-value   = staterequirement.reqfor
      flAppliesTo:screen-value      = StateRequirement.appliesTo 
      flRole:screen-value           = StateRequirement.refid  
      flDescription:screen-value    = StateRequirement.description
      fpersonName:screen-value      = StateRequirement.contactName
      tParentAddr:screen-value      = StateRequirement.contactAddress
      tParentCity:screen-value      = StateRequirement.contactCity  
      tParentState:list-items       = StateRequirement.contactState
      tParentState:screen-value     = StateRequirement.contactState     
      tParentZip:screen-value       = string(StateRequirement.contactZip)
      fWebsite:screen-value         = StateRequirement.url
      tNotifAppr:screen-value       = string(StateRequirement.notificationApproval)
      fNotifApprDays:screen-value   = string(StateRequirement.notificationApprovalDays)
      tNotifCanc:screen-value       = string(StateRequirement.notificationCancellation)
      fNotifCancDays:screen-value   = string(StateRequirement.notificationCancellationDays)
      flexpiry:screen-value         = StateRequirement.expire 
      fexpiryDays:screen-value      = string(StateRequirement.expireDays)
      fexpiryMonth:screen-value     = string(StateRequirement.expireMonth)
      fexpiryDay:screen-value       = string(StateRequirement.expireDay)
      fexpiryYear:screen-value      = if StateRequirement.expireYear = 0 and flexpiry:screen-value = {&ExpireDate} then "1" else string(StateRequirement.expireYear)
      feffDate:screen-value         = string(StateRequirement.effectiveDate)
      fexpDate:screen-value         = string(StateRequirement.expirationDate)
      flQualification:screen-value  = string(StateRequirement.qualification)
      Enotes:screen-value           = StateRequirement.notes
      rsFulffiledBy:screen-value    = StateRequirement.fulfilledBy 
      flQualification:screen-value  = StateRequirement.qualification
      tfirstparty:checked           = if StateRequirement.authorizedby = {&firstParty} then true else false
      .
  
  if StateRequirement.fulfilledBy = ""
   then
    tfirstparty:checked = no.
  else
   tfirstparty:checked = yes.
  
  if tfirstparty:checked = yes 
   then
    flqualification:screen-value = StateRequirement.qualification.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fState flAppliesTo flRole fldescription fpersonName tParentAddr 
          tParentCity tParentZip tParentState fWebsite feffDate fNotifApprDays 
          fexpDate rsFulffiledBy2 fNotifCancDays tfirstparty flQualification 
          flExpiry fexpiryMonth rsFulffiledBy fexpiryDays fexpiryDay fexpiryYear 
          Enotes tNotifAppr tNotifCanc tNotes 
      WITH FRAME Dialog-Frame.
  ENABLE BtnUrl fState flAppliesTo flRole fldescription fpersonName tParentAddr 
         tParentCity fWebsite RECT-3 feffDate RECT-4 fNotifApprDays fexpDate 
         fNotifCancDays flQualification flExpiry fexpiryMonth fexpiryDays 
         fexpiryDay fexpiryYear Enotes tNotes 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openUrl Dialog-Frame 
PROCEDURE openUrl :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pURL as character no-undo.
 
  run ShellExecuteA in this-procedure (0,
                                       "open",
                                       pURL,
                                       "",
                                       "",
                                       1,
                                       output std-in).
end procedure

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

