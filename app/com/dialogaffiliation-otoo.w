&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogaffiliation-otoo.w 

  Description:Dialog to create/edit an affiliation from organization to organization

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Shubham 

  Created:04.23.2020 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/
/* ***************************  Definitions  ************************** */
/* Temp-table Definitions ---                                           */
{tt/affiliation.i}
{lib/std-def.i}
{lib/com-def.i}

/* Parameters Definitions ---    */ 
define input  parameter table for affiliation.
define input  parameter ipcOrgID    as character no-undo.
define input  parameter ipcOrgName  as character no-undo.
define input  parameter iphData     as handle    no-undo.
define output parameter oplSuccess  as logical   no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable chTrackOrganization  as character no-undo.
define variable chTrackOrganizationB as character no-undo.
define variable chTrackAtoB          as character no-undo.
define variable chTrackBtoA          as character no-undo.
define variable chTrackNotes         as character no-undo.
define variable cList                as character no-undo.

define variable cAtoB                as character no-undo.
define variable cBtoA                as character no-undo.
define variable iCount               as integer   no-undo.
define variable iAtoB                as integer   no-undo.
define variable iBtoA                as integer   no-undo.
define variable lAtoB                as logical   no-undo.
define variable lBtoA                as logical   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbNatureOfAToB cbNatureOfBToA edNotes ~
BtnCancel bOrgLookup 
&Scoped-Define DISPLAYED-OBJECTS cbNatureOfAToB fOrganization ~
fOrganizationB cbNatureOfBToA edNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bOrgLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE BUTTON btnTurnDown  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "to" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON btnTurnLeft  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "is a(n)" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON btnTurnRight  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "is a(n)" 
     SIZE 4.6 BY 1.14.

DEFINE BUTTON btnTurnUp  NO-FOCUS FLAT-BUTTON NO-CONVERT-3D-COLORS
     LABEL "is a(n)" 
     SIZE 4.6 BY 1.14.

DEFINE VARIABLE cbNatureOfAToB AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbNatureOfBToA AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE VARIABLE edNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 74.8 BY 2.14 NO-UNDO.

DEFINE VARIABLE fOrganization AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fOrganizationB AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 25 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbNatureOfAToB AT ROW 1.71 COL 26.8 COLON-ALIGNED NO-LABEL WIDGET-ID 222
     btnTurnDown AT ROW 1.86 COL 50 WIDGET-ID 304 NO-TAB-STOP 
     fOrganization AT ROW 3 COL 2 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     fOrganizationB AT ROW 3 COL 47.8 COLON-ALIGNED NO-LABEL WIDGET-ID 214
     btnTurnLeft AT ROW 4 COL 50 WIDGET-ID 306 NO-TAB-STOP 
     cbNatureOfBToA AT ROW 4.24 COL 26.8 COLON-ALIGNED NO-LABEL WIDGET-ID 224
     edNotes AT ROW 7 COL 4.2 NO-LABEL WIDGET-ID 314
     btnTurnRight AT ROW 1.86 COL 24.2 WIDGET-ID 300 NO-TAB-STOP 
     BtnOK AT ROW 10.29 COL 25.4 WIDGET-ID 208
     BtnCancel AT ROW 10.29 COL 42 WIDGET-ID 204
     btnTurnUp AT ROW 4 COL 24.2 WIDGET-ID 308 NO-TAB-STOP 
     bOrgLookup AT ROW 2.91 COL 75.2 WIDGET-ID 292 NO-TAB-STOP 
     "of" VIEW-AS TEXT
          SIZE 3 BY .62 AT ROW 2.1 COL 55 WIDGET-ID 294
     "is" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 4.24 COL 55.2 WIDGET-ID 296
     "Partner A" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 2.29 COL 4 WIDGET-ID 312
     "Partner B" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 2.29 COL 74 RIGHT-ALIGNED WIDGET-ID 318
     "Notes:" VIEW-AS TEXT
          SIZE 6.4 BY .62 AT ROW 6.33 COL 4.2 WIDGET-ID 316
     "of" VIEW-AS TEXT
          SIZE 2.2 BY .62 AT ROW 4.24 COL 22 WIDGET-ID 298
     "is" VIEW-AS TEXT
          SIZE 2 BY .62 AT ROW 2.05 COL 22.2 WIDGET-ID 302
     SPACE(58.19) SKIP(10.03)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Affiliation - Organization to Organization"
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON BtnOK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnDown IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnLeft IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnRight IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btnTurnUp IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edNotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fOrganization IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fOrganization:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fOrganizationB IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fOrganizationB:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TEXT-LITERAL "Partner B"
          SIZE 9 BY .62 AT ROW 2.29 COL 74 RIGHT-ALIGNED                */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Affiliation - Organization to Organization */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup Dialog-Frame
ON CHOOSE OF bOrgLookup IN FRAME Dialog-Frame /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK Dialog-Frame
ON CHOOSE OF BtnOK IN FRAME Dialog-Frame /* Create */
do:
  run validateaffiliation in this-procedure (output std-ch).
  if std-ch <> ""
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return no-apply.
    end.
  
  run addaffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbNatureOfAToB
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbNatureOfAToB Dialog-Frame
ON VALUE-CHANGED OF cbNatureOfAToB IN FRAME Dialog-Frame
do:
  define variable cValue as character no-undo.
  iAtoB = 0.
  do iCount = 1 to num-entries(cList):
    if iCount mod 2 <> 0 
     then
      do:      
        cValue = entry(iCount,cList,",").
        if cValue = cbNatureOfAToB:input-value
         then
          cbNatureOfBToA:screen-value = entry(iCount + 1,cList,",").  
      end.
  end.
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbNatureOfBToA
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbNatureOfBToA Dialog-Frame
ON VALUE-CHANGED OF cbNatureOfBToA IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNotes Dialog-Frame
ON VALUE-CHANGED OF edNotes IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then 
  frame {&frame-name}:parent = active-window.
  
btnTurnRight:load-image            ("images/s-arrow-turn-right.bmp"). 
btnTurnRight:load-image-insensitive("images/s-arrow-turn-right.bmp"). 
btnTurnDown:load-image             ("images/s-arrow-turn-down.bmp"). 
btnTurnDown:load-image-insensitive ("images/s-arrow-turn-down.bmp"). 
btnTurnLeft:load-image             ("images/s-arrow-turn-left.bmp"). 
btnTurnLeft:load-image-insensitive ("images/s-arrow-turn-left.bmp").
btnTurnUp:load-image               ("images/s-arrow-turn-up.bmp"). 
btnTurnUp:load-image-insensitive   ("images/s-arrow-turn-up.bmp"). 
bOrgLookup:load-image              ("images/s-lookup.bmp").
bOrgLookup:load-image-insensitive  ("images/s-lookup-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  run setNature   in this-procedure.
  run displayData in this-procedure.
  
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAffiliation Dialog-Frame 
PROCEDURE addAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame {&frame-name}:
  end.
  
  find first affiliation no-error.
  
  if not available affiliation 
   then
    create affiliation.
  
  assign
      affiliation.partnerA       = fOrganization:private-data
      affiliation.partnerAType   = {&OrganizationCode}
      affiliation.partnerB       = fOrganizationB:private-data
      affiliation.partnerBType   = {&OrganizationCode}
      affiliation.natureOfAToB   = cbNatureOfAToB:input-value
      affiliation.natureOfBToA   = cbNatureOfBToA:input-value  
      affiliation.notes          = edNotes:input-value
      .
      
  if not valid-handle(iphData) 
   then
    do:
      message "Data Model not found."
          view-as alert-box error buttons ok.
      return.
    end.
  
  if affiliation.affiliationId = 0 
   then
    do:
      /* Save the affiliations. */
      run newaffiliation in iphData (input  table affiliation,
                                     output oplSuccess,
                                     output std-ch).    
      if not oplSuccess 
       then 
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end.
    end.
  else
   do:
     run modifyaffiliation in iphData (input  table affiliation,
                                       output oplSuccess,
                                       output std-ch).
     if not oplSuccess 
      then 
       do:
         message std-ch
             view-as alert-box error buttons ok.
         return.
       end. 
   end.
   
   if oplSuccess
    then
     do:
       run newSysCode in this-procedure.
       publish "modifiedAffiliation" (input fOrganizationB:private-data).
     end.
     
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  fOrganization:screen-value in frame Dialog-Frame = ipcOrgName .
  fOrganization:private-data = ipcOrgID.
  
  for first affiliation:
    frame Dialog-Frame:title = "Modify Affiliation - Organization to Organization".
    BtnOK:label              = "Save".
    bOrgLookup:sensitive     = false.
    
    assign
        fOrganization:screen-value in frame Dialog-Frame  = if affiliation.partnerB = ipcOrgID then affiliation.partnerAName else ipcOrgName
        fOrganization:private-data                        = if affiliation.partnerB = ipcOrgID then affiliation.partnerA     else ipcOrgID
        fOrganizationB:screen-value                       = if affiliation.partnerB = ipcOrgID then ipcOrgName               else affiliation.partnerBName 
        fOrganizationB:private-data                       = if affiliation.partnerB = ipcOrgID then ipcOrgID                 else affiliation.partnerB
        cbNatureOfAToB:screen-value                       = affiliation.natureOfAToB    
        cbNatureOfBToA:screen-value                       = affiliation.natureOfBToA
        edNotes:screen-value                              = affiliation.notes
        .
  end.
  assign
      chTrackOrganization  = fOrganization:private-data
      chTrackOrganizationB = fOrganizationB:private-data 
      chTrackAtoB          = cbNatureOfAToB:input-value
      chTrackBtoA          = cbNatureOfBToA:input-value 
      chTrackNotes         = edNotes:input-value
      no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  BtnOK:sensitive = not (chTrackOrganization  = fOrganization:input-value  and
                         chTrackOrganizationB = fOrganizationB:private-data      and
                         chTrackAtoB          = cbNatureOfAToB:input-value and
                         chTrackBtoA          = cbNatureOfBToA:input-value and
                         chTrackNotes         = edNotes:input-value)
                         no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbNatureOfAToB fOrganization fOrganizationB cbNatureOfBToA edNotes 
      WITH FRAME Dialog-Frame.
  ENABLE cbNatureOfAToB cbNatureOfBToA edNotes BtnCancel bOrgLookup 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysCode Dialog-Frame 
PROCEDURE newSysCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  lAtoB = logical(lookup(cbNatureOfAToB:input-value,cList)).
  lBtoA = logical(lookup(cbNatureOfBtoA:input-value,cList)).

  if not lAtoB or not lBtoA 
   then
    do:
      publish "newCode" (input "OtoO",
                         input cbNatureOfAToB:input-value + "," + cbNatureOfBtoA:input-value,
                         output std-lo,
                         output std-ch).
      if not std-lo
       then
        message std-ch
             view-as alert-box information buttons ok.
    end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog Dialog-Frame 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable cOrgID            as character no-undo.
  define variable cName             as character no-undo.
  define variable caffiliationOrgID as character no-undo.
  
  caffiliationOrgID = fOrganization:private-data.
    
  run dialogorganizationlookup.w(input  caffiliationOrgID,
                                 input  false,         /* True if add "New" */
                                 input  false,        /* True if add "Blank" */
                                 input  false,        /* True if add "ALL" */
                                 output cOrgID,
                                 output cName,
                                 output std-lo).
   
  if not std-lo 
   then
    return no-apply.

  if caffiliationOrgID = cOrgID
   then
    do:
      message "Cannot affiliate to same Organization"
          view-as alert-box information buttons ok.
      return no-apply.
    end.
    
  fOrganizationB:screen-value = if cOrgID = "" then "" else cName .
  fOrganizationB:private-data = cOrgID.
  
  if fOrganizationB:private-data ne cOrgID
   then
    run enableDisableSave in this-procedure. 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNature Dialog-Frame 
PROCEDURE setNature :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  publish "GetNatureList" (input "OtoO",
                           output cList).
                        
  do iCount = 1 to num-entries(cList): 
    if iCount mod 2 = 0 
     then
      do:
        if logical(lookup(entry(iCount,cList,","),cBtoA)) 
         then 
          next.
        cBtoA = if cBToA = "" then entry(iCount,cList,",") else cBToA + "," + entry(iCount,cList,",").
      end.
    else
     do:
       if logical(lookup(entry(iCount,cList,","),cAtoB)) 
        then 
         next.
       cAToB = if cAToB = "" then entry(iCount,cList,",") else cAToB + "," + entry(iCount,cList,",").
     end.
  end.

  cbNatureOfAToB:list-items = cAToB.
  cbNatureOfBToA:list-items = cBtoA.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateAffiliation Dialog-Frame 
PROCEDURE validateAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter opcError as character no-undo.

  do with frame Dialog-Frame:
  end.

  if fOrganizationB:private-data = ?
   then
    opcError = "Partner B cannot be blank".  
    
  if cbNatureOfAToB:input-value = "" or cbNatureOfAToB:input-value = ? or 
     cbNatureOfBToA:input-value = "" or cbNatureOfBToA:input-value = ?
   then
    opcError = "Please enter nature of affiliation".
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

