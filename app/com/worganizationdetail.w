&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: worganizationdetail.w

  Description: Show Organization and its linking with others.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created:09.23.2019 
  
  Modified:
  Date          Name           Description
  03/17/2022    Shefali        Task# 86699 Cancelled the attorney while deletion of attorney firm orgrole
  04/26/2022    Shefali        Task #93684 Modified to refresh the fulfillment data properly.
  11/08/2022    AG             Task #96812 Modified to add organization level document feature
  03/08/2023    S Chandu      Task #102073 Added Renew Qualification feature.
  12/31/2024    SRK           Modified to fix raido-set filter issue
          
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* Temp-table Definitions ---                                           */
{tt/reqfulfill.i}
{tt/reqfulfill.i &tableAlias="tReqfulfill"}


{tt/orgrole.i &tableAlias="tOrgRole"}
{tt/orgrole.i &tableAlias="ttOrgRole"}

{tt/organization.i }
{tt/organization.i &tableAlias="tOrganization"}

{tt/affiliation.i}
{tt/affiliation.i &tableAlias="tAffiliation"}

{tt/qualification.i}
{tt/qualification.i &tableAlias="tQualification"}
{tt/qualification.i &tableAlias="ttQualification"}

/* Parameters Definitions ---                                           */

define input parameter ipcOrgID    as character no-undo.
define input parameter ipiPageNo   as integer   no-undo.
define input parameter ipcEntityID as character no-undo.

{src/adm2/widgetprto.i}

define temp-table orgRole like tOrgRole
field totfulfillreq as integer.

{lib/std-def.i}
{lib/winlaunch.i}
{lib/com-def.i} 
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/brw-multi-def.i}

/* Local Variable Definitions ---                                       */

define variable hOrganizationDataSrv  as handle    no-undo.
define variable hComLog               as handle    no-undo.
define variable hDocument             as handle    no-undo.
define variable cValuePair            as character no-undo.
define variable cstat                 as character no-undo.
define variable dAffiliation          as decimal   no-undo.
define variable dQualification        as decimal   no-undo.
define variable dQualificationNum     as decimal   no-undo.
define variable dRole                 as decimal   no-undo.
define variable dColumnWidth          as decimal   no-undo.
define variable dColumnWidthq         as decimal   no-undo.
define variable inooffulfillreq       as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAffiliations

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES affiliation reqfulfill orgRole qualification

/* Definitions for BROWSE brwAffiliations                               */
&Scoped-define FIELDS-IN-QUERY-brwAffiliations affiliation.isCtrlPerson getEntityName(affiliation.partnerB, affiliation.partnerBType, affiliation.partnerAName, affiliation.partnerBName) @ affiliation.partnerBName getEntityID(affiliation.partnerBType, affiliation.partnerA, affiliation.partnerB) @ affiliation.partnerB getEntity(affiliation.partnerBType) @ affiliation.partnerBType getNature(affiliation.partnerB, affiliation.partnerBType, affiliation.natureOfAtoB, affiliation.natureOfBtoA) @ affiliation.natureOfAtoB   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAffiliations   
&Scoped-define SELF-NAME brwAffiliations
&Scoped-define QUERY-STRING-brwAffiliations for each affiliation
&Scoped-define OPEN-QUERY-brwAffiliations open query {&SELF-NAME} for each affiliation.
&Scoped-define TABLES-IN-QUERY-brwAffiliations affiliation
&Scoped-define FIRST-TABLE-IN-QUERY-brwAffiliations affiliation


/* Definitions for BROWSE brwFulfillment                                */
&Scoped-define FIELDS-IN-QUERY-brwFulfillment reqfulfill.reqMet reqfulfill.reqdesc "Requirement" getQualificationDesc(reqfulfill.stat) @ reqfulfill.stat "Status"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFulfillment   
&Scoped-define SELF-NAME brwFulfillment
&Scoped-define QUERY-STRING-brwFulfillment for each reqfulfill
&Scoped-define OPEN-QUERY-brwFulfillment open query {&SELF-NAME} for each reqfulfill.
&Scoped-define TABLES-IN-QUERY-brwFulfillment reqfulfill
&Scoped-define FIRST-TABLE-IN-QUERY-brwFulfillment reqfulfill


/* Definitions for BROWSE brwOrgRoles                                   */
&Scoped-define FIELDS-IN-QUERY-brwOrgRoles orgrole.comstat orgrole.stateID orgrole.source orgrole.sourceID getRoleStatus(orgrole.active) @ orgrole.active orgrole.totfulfillreq   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwOrgRoles   
&Scoped-define SELF-NAME brwOrgRoles
&Scoped-define QUERY-STRING-brwOrgRoles for each orgRole
&Scoped-define OPEN-QUERY-brwOrgRoles open query {&SELF-NAME} for each orgRole.
&Scoped-define TABLES-IN-QUERY-brwOrgRoles orgRole
&Scoped-define FIRST-TABLE-IN-QUERY-brwOrgRoles orgRole


/* Definitions for BROWSE brwQualifications                             */
&Scoped-define FIELDS-IN-QUERY-brwQualifications qualification.activ qualification.stateID qualification.Qualification qualification.QualificationNumber getQualificationDesc(qualification.stat) @ qualification.stat qualification.effectiveDate qualification.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualifications   
&Scoped-define SELF-NAME brwQualifications
&Scoped-define QUERY-STRING-brwQualifications for each qualification where Qualification.entityId = ipcOrgID by Qualification.QualificationID
&Scoped-define OPEN-QUERY-brwQualifications open query {&SELF-NAME} for each qualification where Qualification.entityId = ipcOrgID by Qualification.QualificationID.
&Scoped-define TABLES-IN-QUERY-brwQualifications qualification
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualifications qualification


/* Definitions for FRAME FRAME-C                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-C ~
    ~{&OPEN-QUERY-brwAffiliations}

/* Definitions for FRAME FRAME-D                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-D ~
    ~{&OPEN-QUERY-brwQualifications}

/* Definitions for FRAME FRAME-E                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-E ~
    ~{&OPEN-QUERY-brwFulfillment}~
    ~{&OPEN-QUERY-brwOrgRoles}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bOrganizationDocuments fName bEmail fNPN ~
bOrganizationDelete fAltaUID bRefresh bUrl fAddress fAddress2 fCity cbState ~
fZip fWebsite fEmail fPhone fMobile bViewLog RECT-64 
&Scoped-Define DISPLAYED-OBJECTS fName fNPN fAltaUID fAddress fAddress2 ~
fCity cbState fZip fWebsite fEmail fPhone fMobile fOrgID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityID wWin 
FUNCTION getEntityID RETURNS CHARACTER
  ( cPartnerBType as character,
    cPartnerA     as character,
    cPartnerB     as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityName wWin 
FUNCTION getEntityName RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cPartnerAName as character,
    cPartnerBName as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getNature wWin 
FUNCTION getNature RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cNatureAtoB   as character,
    cNatureBtoA   as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getRoleStatus wWin 
FUNCTION getRoleStatus RETURNS CHARACTER
  ( cStat as logical /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VARIABLE wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwAffiliations 
       MENU-ITEM m_View_affiliation LABEL "View affiliation".

DEFINE MENU POPUP-MENU-brwOrgRoles 
       MENU-ITEM m_View_role_fulfillments LABEL "View fulfillments".


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel".

DEFINE BUTTON bEmail  NO-FOCUS
     LABEL "Email" 
     SIZE 4.8 BY 1.14 TOOLTIP "Send email".

DEFINE BUTTON bOrganizationDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete organization".

DEFINE BUTTON bOrganizationDocuments  NO-FOCUS
     LABEL "Documents" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save".

DEFINE BUTTON bUrl  NO-FOCUS
     LABEL "Url" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open URL".

DEFINE BUTTON bViewLog  NO-FOCUS
     LABEL "ViewLog" 
     SIZE 7.2 BY 1.71 TOOLTIP "View logs".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 8.4 BY 1 NO-UNDO.

DEFINE VARIABLE fAddress AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE fAddress2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE fAltaUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "ALTA UID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.8 BY 1 NO-UNDO.

DEFINE VARIABLE fCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE fEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE fMobile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Mobile" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE fNPN AS CHARACTER FORMAT "X(10)":U 
     LABEL "NPN" 
     VIEW-AS FILL-IN 
     SIZE 16.8 BY 1 NO-UNDO.

DEFINE VARIABLE fOrgID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Organization ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.8 BY 1 NO-UNDO.

DEFINE VARIABLE fPhone AS CHARACTER FORMAT "X(50)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 16.8 BY 1 NO-UNDO.

DEFINE VARIABLE fWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN 
     SIZE 48 BY 1 NO-UNDO.

DEFINE VARIABLE fZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 44 BY 2.14.

DEFINE BUTTON bAffiliationAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "Affiliate person".

DEFINE BUTTON bAffiliationDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete affiliation".

DEFINE BUTTON bAffiliationExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bAffiliationOrg  NO-FOCUS
     LABEL "Affiliation Organization" 
     SIZE 4.8 BY 1.14 TOOLTIP "Affiliate organization".

DEFINE BUTTON bAffiliationUpdate  NO-FOCUS
     LABEL "Update" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify affiliation".

DEFINE BUTTON bAffiliationView  NO-FOCUS
     LABEL "View" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open affiliation".

DEFINE BUTTON bQualificationActive  NO-FOCUS
     LABEL "Active" 
     SIZE 4.8 BY 1.14 TOOLTIP "Activate qualification".

DEFINE BUTTON bQualificationAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "New qualification".

DEFINE BUTTON bQualificationCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 4.8 BY 1.14 TOOLTIP "Copy qualification".

DEFINE BUTTON bQualificationDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete qualification".

DEFINE BUTTON bQualificationDocuments  NO-FOCUS
     LABEL "Documents" 
     SIZE 4.8 BY 1.14 TOOLTIP "Documents".

DEFINE BUTTON bQualificationExport  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON bQualificationRenew  NO-FOCUS
     LABEL "Renew" 
     SIZE 4.8 BY 1.14 TOOLTIP "Renew qualification".

DEFINE BUTTON bQualificationUpdate  NO-FOCUS
     LABEL "Update" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify qualification".

DEFINE BUTTON bQualificationVerify  NO-FOCUS
     LABEL "Verify" 
     SIZE 4.8 BY 1.14 TOOLTIP "Verify qualification".

DEFINE VARIABLE QualType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
          "Inactive", "I":U,
          "Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

DEFINE BUTTON bRoleAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 4.8 BY 1.14 TOOLTIP "New role".

DEFINE BUTTON bRoleDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete role".

DEFINE BUTTON bRoleLog  NO-FOCUS
     LABEL "ViewRoleLog" 
     SIZE 4.8 BY 1.14 TOOLTIP "View logs".

DEFINE BUTTON bRoleUpdate  NO-FOCUS
     LABEL "Update" 
     SIZE 4.8 BY 1.14 TOOLTIP "Modify role".

DEFINE BUTTON bRoleView  NO-FOCUS
     LABEL "view" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open fulfillments".

DEFINE VARIABLE roleType AS CHARACTER INITIAL "B" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Active", "A":U,
          "Inactive", "I":U,
          "Both", "B":U
     SIZE 35 BY 1.38 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAffiliations FOR 
      affiliation SCROLLING.

DEFINE QUERY brwFulfillment FOR 
      reqfulfill SCROLLING.

DEFINE QUERY brwOrgRoles FOR 
      orgRole SCROLLING.

DEFINE QUERY brwQualifications FOR 
      qualification SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAffiliations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAffiliations wWin _FREEFORM
  QUERY brwAffiliations DISPLAY
      affiliation.isCtrlPerson                                    column-label "Control Person"                   width 18 view-as toggle-box 
getEntityName(affiliation.partnerB,
              affiliation.partnerBType,
              affiliation.partnerAName,
              affiliation.partnerBName) @ affiliation.partnerBName       label "Name"           format "x(30)"    width 30 
getEntityID(affiliation.partnerBType,
            affiliation.partnerA,
            affiliation.partnerB)       @ affiliation.partnerB           label "ID"             format "999999"   width 10 
getEntity(affiliation.partnerBType)     @ affiliation.partnerBType       label "Type"           format "x(15)"    width 15
getNature(affiliation.partnerB,
          affiliation.partnerBType,
          affiliation.natureOfAtoB,
          affiliation.natureOfBtoA)     @ affiliation.natureOfAtoB       label "Nature"         format "x(30)"    width 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 150.6 BY 14.29
         BGCOLOR 15  ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.

DEFINE BROWSE brwFulfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFulfillment wWin _FREEFORM
  QUERY brwFulfillment DISPLAY
      reqfulfill.reqMet                                          column-label "Met"         width 6    view-as toggle-box
reqfulfill.reqdesc                                           label        "Requirement"   format "x(100)"      width 53
getQualificationDesc(reqfulfill.stat) @ reqfulfill.stat      label        "Status"        format "x(16)"      width 18
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 81 BY 12.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Requirements of Selected Role" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Requirements and Fulfillments".

DEFINE BROWSE brwOrgRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwOrgRoles wWin _FREEFORM
  QUERY brwOrgRoles DISPLAY
      orgrole.comstat     label ""              format "x(2)"     width 2   
orgrole.stateID           label "State"         format "x(5)"     width 8
orgrole.source            label "Role"          format "x(15)"    width 15
orgrole.sourceID          label "ID"            format "x(8)"     width 10
getRoleStatus(orgrole.active) @  orgrole.active          label "Status"        format "x(8)"     width 8
orgrole.totfulfillreq     column-label "# Fulfillable ! Requirements "            format ">>>"     width 5
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 67.6 BY 12.91
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Roles" ROW-HEIGHT-CHARS .85 FIT-LAST-COLUMN.

DEFINE BROWSE brwQualifications
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualifications wWin _FREEFORM
  QUERY brwQualifications DISPLAY
      qualification.activ          column-label "Active"                                       width 9 view-as toggle-box   
qualification.stateID                     label "State ID"              format "x(5)"          width 10
qualification.Qualification               label "Qualification"         format "x(50)"         width 42
qualification.QualificationNumber         label "Number"                format "x(20)"         width 22
getQualificationDesc(qualification.stat)  
@ qualification.stat                      label "Status"                format "x(15)"         width 16
qualification.effectiveDate               label "Effective"             format "99/99/9999"    width 14
qualification.expirationDate              label "Expiration"            format "99/99/9999"    width 14
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 150.6 BY 12.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bOrganizationDocuments AT ROW 1.67 COL 37.8 WIDGET-ID 360 NO-TAB-STOP 
     fName AT ROW 4.95 COL 18 COLON-ALIGNED WIDGET-ID 214
     bEmail AT ROW 5.95 COL 128.8 WIDGET-ID 318 NO-TAB-STOP 
     fNPN AT ROW 8.24 COL 78.6 COLON-ALIGNED WIDGET-ID 358
     bOrganizationDelete AT ROW 1.67 COL 23.8 WIDGET-ID 334 NO-TAB-STOP 
     fAltaUID AT ROW 8.24 COL 109.8 COLON-ALIGNED WIDGET-ID 356
     bRefresh AT ROW 1.67 COL 2.8 WIDGET-ID 342 NO-TAB-STOP 
     bUrl AT ROW 4.86 COL 128.8 WIDGET-ID 320 NO-TAB-STOP 
     fAddress AT ROW 6.05 COL 18 COLON-ALIGNED WIDGET-ID 216
     fAddress2 AT ROW 7.14 COL 18 COLON-ALIGNED NO-LABEL WIDGET-ID 236
     fCity AT ROW 8.24 COL 18 COLON-ALIGNED NO-LABEL WIDGET-ID 230
     bCancel AT ROW 1.67 COL 16.8 WIDGET-ID 246 NO-TAB-STOP 
     cbState AT ROW 8.24 COL 42.6 COLON-ALIGNED NO-LABEL WIDGET-ID 298
     fZip AT ROW 8.24 COL 51.6 COLON-ALIGNED NO-LABEL WIDGET-ID 240
     fWebsite AT ROW 4.95 COL 78.6 COLON-ALIGNED WIDGET-ID 220
     fEmail AT ROW 6.05 COL 78.6 COLON-ALIGNED WIDGET-ID 218
     fPhone AT ROW 7.14 COL 78.6 COLON-ALIGNED WIDGET-ID 222
     fMobile AT ROW 7.14 COL 104.6 COLON-ALIGNED WIDGET-ID 238
     bSave AT ROW 1.67 COL 9.8 WIDGET-ID 244 NO-TAB-STOP 
     bViewLog AT ROW 1.67 COL 30.8 WIDGET-ID 232 NO-TAB-STOP 
     fOrgID AT ROW 3.86 COL 18 COLON-ALIGNED WIDGET-ID 212 NO-TAB-STOP 
     RECT-64 AT ROW 1.48 COL 2 WIDGET-ID 338
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 161.4 BY 25.76 WIDGET-ID 100.

DEFINE FRAME FRAME-E
     bRoleAdd AT ROW 2.67 COL 2.2 WIDGET-ID 350 NO-TAB-STOP 
     roleType AT ROW 1.05 COL 22.2 NO-LABEL WIDGET-ID 358
     brwFulfillment AT ROW 2.67 COL 77 WIDGET-ID 400
     brwOrgRoles AT ROW 2.71 COL 7.4 WIDGET-ID 700
     bRoleView AT ROW 7.24 COL 2.2 WIDGET-ID 354 NO-TAB-STOP 
     bRoleLog AT ROW 6.1 COL 2.2 WIDGET-ID 356 NO-TAB-STOP 
     bRoleDelete AT ROW 4.95 COL 2.2 WIDGET-ID 352 NO-TAB-STOP 
     bRoleUpdate AT ROW 3.81 COL 2.2 WIDGET-ID 348 NO-TAB-STOP 
     "Show Status :" VIEW-AS TEXT
          SIZE 14 BY .86 AT ROW 1.33 COL 8.2 WIDGET-ID 362
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 11.52
         SIZE 158 BY 14.76 WIDGET-ID 1000.

DEFINE FRAME FRAME-C
     brwAffiliations AT ROW 1.29 COL 7.4 WIDGET-ID 900
     bAffiliationAdd AT ROW 2.43 COL 2 WIDGET-ID 342 NO-TAB-STOP 
     bAffiliationOrg AT ROW 3.57 COL 2 WIDGET-ID 350 NO-TAB-STOP 
     bAffiliationDelete AT ROW 5.86 COL 2 WIDGET-ID 346 NO-TAB-STOP 
     bAffiliationExport AT ROW 1.29 COL 2 WIDGET-ID 326 NO-TAB-STOP 
     bAffiliationUpdate AT ROW 4.71 COL 2 WIDGET-ID 344 NO-TAB-STOP 
     bAffiliationView AT ROW 7 COL 2 WIDGET-ID 348 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 11.52
         SIZE 157.6 BY 14.76 WIDGET-ID 300.

DEFINE FRAME FRAME-D
     QualType AT ROW 1.05 COL 22.2 NO-LABEL WIDGET-ID 358
     bQualificationRenew AT ROW 7.24 COL 2 WIDGET-ID 364 NO-TAB-STOP 
     brwQualifications AT ROW 2.71 COL 7.4 WIDGET-ID 800
     bQualificationCopy AT ROW 8.38 COL 2 WIDGET-ID 348 NO-TAB-STOP 
     bQualificationVerify AT ROW 10.67 COL 2 WIDGET-ID 254 NO-TAB-STOP 
     bQualificationActive AT ROW 11.81 COL 2 WIDGET-ID 320 NO-TAB-STOP 
     bQualificationAdd AT ROW 3.81 COL 2 WIDGET-ID 342 NO-TAB-STOP 
     bQualificationDelete AT ROW 6.1 COL 2 WIDGET-ID 346 NO-TAB-STOP 
     bQualificationDocuments AT ROW 9.52 COL 2 WIDGET-ID 188 NO-TAB-STOP 
     bQualificationExport AT ROW 2.67 COL 2 WIDGET-ID 326 NO-TAB-STOP 
     bQualificationUpdate AT ROW 4.95 COL 2 WIDGET-ID 344 NO-TAB-STOP 
     "Show Status :" VIEW-AS TEXT
          SIZE 14 BY .81 AT ROW 1.33 COL 8.2 WIDGET-ID 362
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.4 ROW 11.52
         SIZE 157.6 BY 14.76 WIDGET-ID 600.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Organization Detail"
         HEIGHT             = 25.81
         WIDTH              = 161.4
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME FRAME-C:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-D:FRAME = FRAME fMain:HANDLE
       FRAME FRAME-E:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fOrgID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fOrgID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FRAME FRAME-C
                                                                        */
/* BROWSE-TAB brwAffiliations 1 FRAME-C */
/* SETTINGS FOR BUTTON bAffiliationDelete IN FRAME FRAME-C
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAffiliationExport IN FRAME FRAME-C
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAffiliationUpdate IN FRAME FRAME-C
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAffiliationView IN FRAME FRAME-C
   NO-ENABLE                                                            */
ASSIGN 
       brwAffiliations:POPUP-MENU IN FRAME FRAME-C             = MENU POPUP-MENU-brwAffiliations:HANDLE
       brwAffiliations:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-C = TRUE
       brwAffiliations:COLUMN-RESIZABLE IN FRAME FRAME-C       = TRUE.

/* SETTINGS FOR FRAME FRAME-D
                                                                        */
/* BROWSE-TAB brwQualifications bQualificationRenew FRAME-D */
/* SETTINGS FOR BUTTON bQualificationUpdate IN FRAME FRAME-D
   NO-ENABLE                                                            */
ASSIGN 
       brwQualifications:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-D = TRUE
       brwQualifications:COLUMN-RESIZABLE IN FRAME FRAME-D       = TRUE.

/* SETTINGS FOR FRAME FRAME-E
                                                                        */
/* BROWSE-TAB brwFulfillment roleType FRAME-E */
/* BROWSE-TAB brwOrgRoles brwFulfillment FRAME-E */
ASSIGN 
       brwFulfillment:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-E = TRUE
       brwFulfillment:COLUMN-RESIZABLE IN FRAME FRAME-E       = TRUE
       brwFulfillment:COLUMN-MOVABLE IN FRAME FRAME-E         = TRUE.

ASSIGN 
       brwOrgRoles:POPUP-MENU IN FRAME FRAME-E             = MENU POPUP-MENU-brwOrgRoles:HANDLE
       brwOrgRoles:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-E = TRUE
       brwOrgRoles:COLUMN-RESIZABLE IN FRAME FRAME-E       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAffiliations
/* Query rebuild information for BROWSE brwAffiliations
     _START_FREEFORM
open query {&SELF-NAME} for each affiliation.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAffiliations */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFulfillment
/* Query rebuild information for BROWSE brwFulfillment
     _START_FREEFORM
open query {&SELF-NAME} for each reqfulfill.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFulfillment */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwOrgRoles
/* Query rebuild information for BROWSE brwOrgRoles
     _START_FREEFORM
open query {&SELF-NAME} for each orgRole.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwOrgRoles */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualifications
/* Query rebuild information for BROWSE brwQualifications
     _START_FREEFORM
open query {&SELF-NAME} for each qualification where Qualification.entityId = ipcOrgID by Qualification.QualificationID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualifications */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Organization Detail */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent 
   then 
    return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Organization Detail */
do:
  /* This event will close the window and terminate the procedure.  */  
  run closeWindow in this-procedure.
  return no-apply.         
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Organization Detail */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME bAffiliationAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliationAdd wWin
ON CHOOSE OF bAffiliationAdd IN FRAME FRAME-C /* Add */
do:
  run addAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAffiliationDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliationDelete wWin
ON CHOOSE OF bAffiliationDelete IN FRAME FRAME-C /* Delete */
do:
  run deleteAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAffiliationExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliationExport wWin
ON CHOOSE OF bAffiliationExport IN FRAME FRAME-C /* Export */
do:
  run exportData in this-procedure ({&AffiliationCode}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAffiliationOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliationOrg wWin
ON CHOOSE OF bAffiliationOrg IN FRAME FRAME-C /* Affiliation Organization */
do:
  run affiliateOrganization in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAffiliationUpdate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliationUpdate wWin
ON CHOOSE OF bAffiliationUpdate IN FRAME FRAME-C /* Update */
do:
  run modifyAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAffiliationView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAffiliationView wWin
ON CHOOSE OF bAffiliationView IN FRAME FRAME-C /* View */
do:
  run openAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel wWin
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
do:
  run cancelOrganization in this-procedure. 
  run enableDisableSave  in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEmail wWin
ON CHOOSE OF bEmail IN FRAME fMain /* Email */
do:
  if fEmail:input-value eq "" 
   then
    return.

  run openURL("mailto:" + femail:input-value).   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrganizationDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrganizationDelete wWin
ON CHOOSE OF bOrganizationDelete IN FRAME fMain /* Delete */
do:
  run deleteOrganization in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrganizationDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrganizationDocuments wWin
ON CHOOSE OF bOrganizationDocuments IN FRAME fMain /* Documents */
do:
  publish "OpenDocument" (input {&Organization},
                          input fOrgId:input-value,
                          input this-procedure).
   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME bQualificationActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationActive wWin
ON CHOOSE OF bQualificationActive IN FRAME FRAME-D /* Active */
do:
  run changeStatus in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationAdd wWin
ON CHOOSE OF bQualificationAdd IN FRAME FRAME-D /* Add */
do:
  run addQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationCopy wWin
ON CHOOSE OF bQualificationCopy IN FRAME FRAME-D /* Copy */
do:
  run copyQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationDelete wWin
ON CHOOSE OF bQualificationDelete IN FRAME FRAME-D /* Delete */
do:
  run deleteQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationDocuments wWin
ON CHOOSE OF bQualificationDocuments IN FRAME FRAME-D /* Documents */
do:
  run openDocument in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationExport wWin
ON CHOOSE OF bQualificationExport IN FRAME FRAME-D /* Export */
do:
  run exportData in this-procedure ({&QualificationCode}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationRenew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationRenew wWin
ON CHOOSE OF bQualificationRenew IN FRAME FRAME-D /* Renew */
do:
  run renewQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationUpdate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationUpdate wWin
ON CHOOSE OF bQualificationUpdate IN FRAME FRAME-D /* Update */
do:
  run modifyQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bQualificationVerify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bQualificationVerify wWin
ON CHOOSE OF bQualificationVerify IN FRAME FRAME-D /* Verify */
do:
  run openReview in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh wWin
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure (yes,  /*  NeedQualification  */          
                                     yes,  /*  NeedAffiliation */        
                                     yes,  /*  NeedOrgRole */ 
                                     yes). /*  NeedReqfulfill */      
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME bRoleAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRoleAdd wWin
ON CHOOSE OF bRoleAdd IN FRAME FRAME-E /* Add */
do:
  run addOrgRole in this-procedure.   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRoleDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRoleDelete wWin
ON CHOOSE OF bRoleDelete IN FRAME FRAME-E /* Delete */
do:
  run deleteOrgRole in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRoleLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRoleLog wWin
ON CHOOSE OF bRoleLog IN FRAME FRAME-E /* ViewRoleLog */
do:
  run openRoleLog in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRoleUpdate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRoleUpdate wWin
ON CHOOSE OF bRoleUpdate IN FRAME FRAME-E /* Update */
do:
  run modifyOrgRole in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRoleView
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRoleView wWin
ON CHOOSE OF bRoleView IN FRAME FRAME-E /* view */
do:
  run openOrganizationRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAffiliations
&Scoped-define FRAME-NAME FRAME-C
&Scoped-define SELF-NAME brwAffiliations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON DEFAULT-ACTION OF brwAffiliations IN FRAME FRAME-C
do:
  run openAffiliation in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON ROW-DISPLAY OF brwAffiliations IN FRAME FRAME-C
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffiliations wWin
ON START-SEARCH OF brwAffiliations IN FRAME FRAME-C
do:
  {lib/brw-startSearch-multi.i &browse-name = brwAffiliations}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFulfillment
&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME brwFulfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment wWin
ON ROW-DISPLAY OF brwFulfillment IN FRAME FRAME-E /* Requirements of Selected Role */
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment wWin
ON START-SEARCH OF brwFulfillment IN FRAME FRAME-E /* Requirements of Selected Role */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwFulfillment}

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define BROWSE-NAME brwOrgRoles
&Scoped-define SELF-NAME brwOrgRoles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwOrgRoles wWin
ON DEFAULT-ACTION OF brwOrgRoles IN FRAME FRAME-E /* Roles */
do:
  run openOrganizationRole in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwOrgRoles wWin
ON ROW-DISPLAY OF brwOrgRoles IN FRAME FRAME-E /* Roles */
do:
  cstat = "".
  {lib/brw-rowdisplay-multi.i}
    
  if orgrole.source = {&Agent}
   then
    do:
      publish "getAgentStatus" (input orgrole.sourceID, output cstat).
      if cstat ne {&ActiveStat} 
       then
        orgrole.comstat = "" .
    end.         
   
   
 case orgrole.comstat:
    when {&Rcode} 
     then  
      orgrole.comstat:bgcolor in browse brwOrgRoles = 12.

    when {&Ycode} 
     then 
      orgrole.comstat:bgcolor in browse brwOrgRoles = 14.

    when {&Gcode} 
     then
      orgrole.comstat:bgcolor in browse brwOrgRoles =  2.
  end case.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwOrgRoles wWin
ON START-SEARCH OF brwOrgRoles IN FRAME FRAME-E /* Roles */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwOrgRoles}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwOrgRoles wWin
ON VALUE-CHANGED OF brwOrgRoles IN FRAME FRAME-E /* Roles */
DO:  
  if can-find(first orgrole) 
   then
    do:
      run refreshfulfillmentData in this-procedure.  
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualifications
&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME brwQualifications
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON DEFAULT-ACTION OF brwQualifications IN FRAME FRAME-D
do:
  run modifyQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON ROW-DISPLAY OF brwQualifications IN FRAME FRAME-D
do:
  {lib/brw-rowdisplay-multi.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON START-SEARCH OF brwQualifications IN FRAME FRAME-D
do:
  {lib/brw-startSearch-multi.i &browse-name = brwQualifications}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualifications wWin
ON VALUE-CHANGED OF brwQualifications IN FRAME FRAME-D
do:
  run setButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave wWin
ON CHOOSE OF bSave IN FRAME fMain /* Save */
do:
  run saveOrganization in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUrl wWin
ON CHOOSE OF bUrl IN FRAME fMain /* Url */
do:
  if fWebsite:input-value <> "" 
   then
    run openURL in this-procedure (fWebsite:input-value).  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewLog wWin
ON CHOOSE OF bViewLog IN FRAME fMain /* ViewLog */
do:
  run openLog in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fName wWin
ON VALUE-CHANGED OF fName IN FRAME fMain /* Name */
or 'value-changed' of cbState
or 'value-changed' of fNPN
or 'value-changed' of fAltaUID
or 'value-changed' of fAddress
or 'value-changed' of fAddress2
or 'value-changed' of fCity
or 'value-changed' of fEmail
or 'value-changed' of fMobile
or 'value-changed' of fWebsite
or 'value-changed' of fPhone
or 'value-changed' of fZip
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_affiliation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_affiliation wWin
ON CHOOSE OF MENU-ITEM m_View_affiliation /* View affiliation */
DO:
  run openAffiliation in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_role_fulfillments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_role_fulfillments wWin
ON CHOOSE OF MENU-ITEM m_View_role_fulfillments /* View fulfillments */
DO:
  run openOrganizationRole in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-D
&Scoped-define SELF-NAME QualType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL QualType wWin
ON VALUE-CHANGED OF QualType IN FRAME FRAME-D
DO:
  empty temp-table qualification. 
  if qualType:input-value = "A" then
  do:
    for each tqualification where tqualification.activ = true :
    create qualification. 
    buffer-copy tqualification to qualification.
    end.
  end.
 else if qualType:input-value = "I" then
  do:
    for each tqualification where tqualification.activ = false :
    create qualification. 
    buffer-copy tqualification to qualification.
    end.
  end.
  else
  do:
    for each tqualification:
    create qualification. 
    buffer-copy tqualification to qualification.
    end.
  end.
  open query brwQualifications for each qualification.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-E
&Scoped-define SELF-NAME roleType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL roleType wWin
ON VALUE-CHANGED OF roleType IN FRAME FRAME-E
DO:
  empty temp-table orgrole. 
  if roleType:input-value = "A" then
  do:
    for each torgrole where torgrole.active = true :
      create orgrole. 
      buffer-copy torgrole to orgrole.
      inooffulfillreq  = 0.
      for each treqfulfill where treqfulfill.orgroleID = tOrgRole.orgroleID:
        inooffulfillreq = inooffulfillreq + 1.
      end.
      assign orgrole.totfulfillreq = inooffulfillreq.
    end.
  end.
 else if roleType:input-value = "I" then
  do:
    for each torgrole where torgrole.active = false :
      create orgrole. 
      buffer-copy torgrole to orgrole.
      inooffulfillreq  = 0.
      for each treqfulfill where treqfulfill.orgroleID = tOrgRole.orgroleID:
        inooffulfillreq = inooffulfillreq + 1.
      end.
      assign orgrole.totfulfillreq = inooffulfillreq.
    end.
  end.
  else
  do:
    for each torgrole:
      create orgrole. 
      buffer-copy torgrole to orgrole.
      inooffulfillreq  = 0.
      for each treqfulfill where treqfulfill.orgroleID = tOrgRole.orgroleID:
        inooffulfillreq = inooffulfillreq + 1.
      end.
      assign orgrole.totfulfillreq = inooffulfillreq.
    end.
  end.
  open query brworgRoles for each orgrole.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwAffiliations
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

{lib/win-main.i}
{lib/win-status.i}

bSave            :load-image ("images/Save.bmp").
bSave            :load-image-insensitive("images/Save-i.bmp").
bRefresh         :load-image("images/Refresh.bmp").
bRefresh         :load-image-insensitive("images/Refresh-i.bmp").
bCancel          :load-image ("images/Cancel.bmp").
bCancel          :load-image-insensitive("images/Cancel-i.bmp").
bViewLog         :load-image("images/log.bmp").
bViewLog         :load-image-insensitive("images/log-i.bmp").

bEmail           :load-image ("images/s-email.bmp").
bEmail           :load-image-insensitive("images/s-email-i.bmp").
bUrl             :load-image ("images/s-url.bmp").
bUrl             :load-image-insensitive("images/s-url-i.bmp").  
bRoleAdd         :load-image ("images/s-add.bmp").
bRoleAdd         :load-image-insensitive("images/s-add-i.bmp").
bRoleUpdate      :load-image ("images/s-update.bmp").
bRoleUpdate      :load-image-insensitive("images/s-update-i.bmp").
bRoleDelete      :load-image ("images/s-delete.bmp").
bRoleDelete      :load-image-insensitive("images/s-delete-i.bmp").
bRoleLog         :load-image ("images/s-log.bmp").
bRoleLog         :load-image-insensitive("images/s-log-i.bmp").
bRoleView        :load-image ("images/s-magnifier.bmp").
bRoleView        :load-image-insensitive("images/s-magnifier-i.bmp").

bAffiliationExport    :load-image("images/s-excel.bmp").
bAffiliationExport    :load-image-insensitive("images/s-excel-i.bmp"). 
bAffiliationAdd       :load-image("images/s-group-link.bmp").
bAffiliationAdd       :load-image-insensitive("images/s-group-link-i.bmp").
bAffiliationUpdate    :load-image ("images/s-update.bmp").
bAffiliationUpdate    :load-image-insensitive("images/s-update-i.bmp").
bAffiliationDelete    :load-image("images/s-delete.bmp").
bAffiliationDelete    :load-image-insensitive("images/s-delete-i.bmp").
bAffiliationView      :load-image ("images/s-magnifier.bmp").
bAffiliationView      :load-image-insensitive("images/s-magnifier-i.bmp").

bQualificationExport    :load-image("images/s-excel.bmp").
bQualificationExport    :load-image-insensitive("images/s-excel-i.bmp"). 
bQualificationAdd       :load-image ("images/s-add.bmp").
bQualificationAdd       :load-image-insensitive("images/s-add-i.bmp").
bQualificationUpdate    :load-image ("images/s-update.bmp").
bQualificationUpdate    :load-image-insensitive("images/s-update-i.bmp").
bQualificationDelete    :load-image("images/s-delete.bmp").
bQualificationDelete    :load-image-insensitive("images/s-delete-i.bmp").
bQualificationCopy      :load-image("images/s-copy.bmp").
bQualificationCopy      :load-image-insensitive("images/s-copy-i.bmp").
bQualificationDocuments :load-image("images/s-attach.bmp").
bQualificationDocuments :load-image-insensitive("images/s-attach-i.bmp").  
bQualificationVerify    :load-image("images/s-star.bmp").
bQualificationVerify    :load-image-insensitive("images/s-star-i.bmp").
bQualificationActive    :load-image("images/s-flag_red.bmp").
bQualificationActive    :load-image-insensitive("images/s-flag-i.bmp").
bOrganizationDelete     :load-image("images/delete.bmp").
bOrganizationDelete     :load-image-insensitive("images/delete-i.bmp").
bOrganizationDocuments  :load-image("images/attach.bmp").
bOrganizationDocuments  :load-image-insensitive("images/attach-i.bmp").
bQualificationRenew     :load-image("images/s-award.bmp").
bQualificationRenew     :load-image-insensitive("images/s-award-i.bmp").

bAffiliationOrg         :load-image("images/s-building-link.bmp").
bAffiliationOrg         :load-image-insensitive("images/s-building-link-i.bmp").

{lib/brw-main-multi.i &browse-list="brwQualifications,brwAffiliations,brwOrgRoles,brwFulfillment"}  

/* Subscribe to the events, so that qualification can refresh 
   in case fulfillments are modified. */
subscribe to "fulFillmentModified"        anywhere.

/* Subscribe to the events, so that qualification screen can refresh 
    in case review is modified  are modified. */
subscribe to "qualificationReviewModified" anywhere run-procedure "refreshQualifications".

/* Subscribe to the event in case state requirement qualification record
  is activated or deactivated. This also results in change of fulfillments
  grid. */
subscribe to "modifiedAffiliation"        anywhere.
subscribe to "requirementModified"        anywhere.
subscribe to "organizationStatusModified" anywhere.
subscribe to "roleComplianceModified"     anywhere.

setStatusMessage("").

run initializeObject in this-procedure.

/* Create data model for agent Detail */
run organizationdatasrv.p persistent set hOrganizationDataSrv (input ipcOrgID).

run getData in this-procedure.

if not can-find(first organization where organization.orgId = ipcOrgID) 
 then
  do:
    message "The Organization with ID: " + string(ipcOrgID) + " does not exist."  
      view-as alert-box error buttons ok.
    
    delete procedure hOrganizationDataSrv.
    delete procedure this-procedure.
    return.
  end.
 
run displayOrganizationRole in this-procedure.

/* Select the role when called from main window Agent tab */
if ipcEntityID <> ""
 then.
  do: 
    find first orgRole where orgRole.sourceID = ipcEntityID no-error.
    if available orgRole
     then 
      do:
       reposition brwOrgRoles to rowid rowid(orgRole).
       apply "value-changed" to brwOrgRoles.
      end.
  end. 
 
{lib/get-column-width.i &col="'partnerBName'"  &var=dAffiliation   &browse-name=brwAffiliations }
{lib/get-column-width.i &col="'source'"        &var=dRole          &browse-name=brwOrgRoles    }
{lib/get-column-width.i &col="'reqdesc'"        &var=dColumnWidth  &browse-name=brwFulfillment} 
{lib/get-column-width.i &col="'qualification'"  &var=dColumnWidthq &browse-name=brwFulfillment}
{lib/get-column-width.i &col="'qualification'"  &var=dQualification &browse-name=brwQualifications}
{lib/get-column-width.i &col="'QualificationNumber'"  &var=dQualificationNum &browse-name=brwQualifications}

run displayQualification      in this-procedure.
run displayAffiliation        in this-procedure.
run displayOrganization       in this-procedure.
run setButtons                in this-procedure.

/* This procedure restores the window and move it to top */
run showWindow                in this-procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addAffiliation wWin 
PROCEDURE addAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table tAffiliation.
  
  do with frame {&frame-name}:
  end.
                                          
  run dialogaffiliation-otop.w(input table tAffiliation,
                               input ipcOrgID,
                               input fname:input-value,
                               input hOrganizationDataSrv,
                               output std-lo).                                          
  if not std-lo 
   then
    return.

  /* Get updated data of affiliated persons with an organization */
  run getAffiliations in hOrganizationDataSrv(output table tAffiliation).  
  
  /* Clearing Browser */
  close query brwaffiliations.
  empty temp-table affiliation.

  for each tAffiliation:
    create affiliation.
    buffer-copy tAffiliation to affiliation.
  end.

  run displayAffiliation in this-procedure.
  run setButtons         in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addOrgRole wWin 
PROCEDURE addOrgRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable opiOrgRoleID as integer no-undo.

  define buffer tOrgRole for tOrgRole.
  define buffer orgrole  for orgrole.

  do with frame fMain:
  end.

  empty temp-table ttOrgRole.
 
  run dialogneworgrole.w (input hOrganizationDataSrv,
                          input integer(fOrgID:input-value),
                          input fName:input-value,
                          input table ttOrgRole,
                          output opiOrgRoleID,
                          output std-lo).
                                                        
  if not std-lo 
   then
    return.
  
  empty temp-table tOrgRole.
  run getOrganizationRole in hOrganizationDataSrv( output table tOrgRole).

  empty temp-table orgrole.

  for each tOrgRole:
    create orgrole.
    buffer-copy tOrgRole to orgrole.
  end.

  run displayOrganizationRole in this-procedure.

  for first orgrole
    where orgrole.orgroleID = opiOrgRoleID :
    std-ro = rowid(orgrole).
  end.

  reposition brwOrgRoles to rowid std-ro.
  brwOrgRoles:get-repositioned-row() in frame FRAME-E.

  apply "value-changed" to brwOrgRoles.
  run setButtons in this-procedure.
                                
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addQualification wWin 
PROCEDURE addQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tQualification for tQualification.
  define buffer qualification  for qualification.

  define variable iQualID   as integer  no-undo.
  define variable iReviewID as integer  no-undo.
  
  do with frame {&frame-name}:
  end.
 
  empty temp-table ttQualification.
  
  run dialogqualification.w (input hOrganizationDataSrv,
                             input {&OrganizationNew},
                             input {&OrganizationCode},
                             input organization.orgID,
                             input organization.state,
                             input table ttQualification,
                             output std-lo,
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.
  empty temp-table tQualification.
  run getQualifications in hOrganizationDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.
  
  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  reposition brwqualifications to rowid std-ro.
  brwqualifications:get-repositioned-row() in frame FRAME-D.
  
  run setButtons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Affiliations|Qualifications|Roles' + 'FolderTabWidth30FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 10.29 , 2.00 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 16.19 , 159.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjustTabOrder ( h_folder ,
             fMobile:HANDLE IN FRAME fMain , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE affiliateOrganization wWin 
PROCEDURE affiliateOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  empty temp-table taffiliation.

  run dialogaffiliation-OtoO.w(input table tAffiliation,
                               input ipcOrgID,
                               input fname:input-value,
                               input hOrganizationDataSrv,
                               output std-lo).                                          
  if not std-lo 
   then
    return.
                                         
  /* Get updated data of affiliated persons with an organization */
  run getAffiliations in hOrganizationDataSrv(output table tAffiliation).  
  
  /* Clearing Browser */
  close query brwaffiliations.
  empty temp-table affiliation.

  for each tAffiliation:
    create affiliation.
    buffer-copy tAffiliation to affiliation.
  end.

  run displayAffiliation in this-procedure.
  run setButtons         in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE affiliationforCtrlPersonModified wWin 
PROCEDURE affiliationforCtrlPersonModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: if Affiliations are modified by person or organization than person and its
         role gets affected so need to refresh    
------------------------------------------------------------------------------*/
  define input parameter ipcEntityID       as character no-undo.
  define input parameter ipiAffiliationID  as integer   no-undo.

  define variable iCountAffiliated  as integer no-undo.
  define variable iAffiliationID    as integer no-undo.

  /* Match if this is concerned person. */
  if ipcEntityID eq ipcOrgID
   then
    return.

  /* Match if this is concerned agent. */
  if not can-find( first Affiliation where Affiliation.AffiliationID = ipiAffiliationID) /* affiliation ,modify by person */ 
   then
    return.

  do with frame FRAME-C:
  end.

  find current Affiliation no-error.
  if available Affiliation 
   then
    iAffiliationID = Affiliation.AffiliationID.

  do iCountAffiliated = 1 to brwAffiliations:num-iterations:
    if brwAffiliations:is-row-selected(iCountAffiliated)
     then
      leave.
  end.

  run refreshData(no,       /* iplNeedQualification */
                  yes,      /* iplNeedAffiliation   */
                  yes,      /* iplNeedOrgRole       */ 
                  yes).     /* iplNeedReqfulfill    */


  for first Affiliation 
    where Affiliation.AffiliationID = iAffiliationID :
    std-ro = rowid(Affiliation).
  end.

  brwAffiliations:set-repositioned-row(iCountAffiliated,"ALWAYS").
  reposition brwAffiliations to rowid std-ro no-error.

  run setButtons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelOrganization wWin 
PROCEDURE cancelOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer organization for organization.
  
  do with frame {&frame-name}:
  end.

  /* Undo all changes of organization table */
  for first organization 
    where organization.orgID = ipcOrgID :
    assign 
        fOrgID:screen-value        =  string(organization.orgID)
        fname:screen-value         =  organization.name
        fNPN:screen-value          =  organization.NPN
        fAltaUID:screen-value      =  organization.altaUID
        fPhone:screen-value        =  organization.phone
        fEmail:screen-value        =  organization.email
        fMobile:screen-value       =  organization.mobile
        fAddress:screen-value      =  organization.addr1
        fAddress2:screen-value     =  organization.addr2
        fCity:screen-value         =  organization.city
        cbstate:screen-value       =  if lookup(organization.state,cValuePair) = 0 then "" else organization.state
        fZip:screen-value          =  organization.zip
        fWebsite:screen-value      =  organization.website
          . 
  end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeRoleStatus wWin 
PROCEDURE changeRoleStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changeStatus wWin 
PROCEDURE changeStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount   as integer     no-undo.
  define variable iQualID  as integer     no-undo.
  define variable lCheck   as logical     no-undo.
 
  do with frame {&frame-name}:
  end.

  if not available qualification
   then
    return.

  iQualID = qualification.qualificationID.

  do iCount = 1 to brwqualifications:num-iterations in frame FRAME-D:
    if brwqualifications:is-row-selected(iCount)
     then
      leave.
  end.

  if qualification.active
   then
    do:
      run deactivateQualification in hOrganizationDataSrv (input iQualID,
                                                           input  yes, /* Yes - validate if the qualification is in use */
                                                           output std-lo,
                                                           output std-ch).  
     
      if std-ch <> "" 
       then
        do:
          if std-ch = {&InUse} 
           then
            message "Fulfillment record exists for this qualification. Do you still want to deactivate?"
                view-as alert-box question buttons yes-no update lupdate as logical.
          else 
           return.
          
          if not lUpdate
           then 
            return.
          
          run deActivateQualification in hOrganizationDataSrv (input  iQualID,
                                                               input  no, /* No - do not validate if the qualification is in use */
                                                               output std-lo,
                                                               output std-ch).
          
          if not std-lo 
           then
            do:
              message std-ch 
                  view-as alert-box error buttons ok.
              return.
            end.
        end. /* if std-ch <> ""... */

    end. /* if qualification.active */
  
  else
   do:
     if qualification.effectiveDate = ?
      then
       do:
         message "Effective Date must be set to activate the Qualification." 
             view-as alert-box error buttons ok.
         return.
       end.
     
     if qualification.stat ne {&GoodStanding} and qualification.stat ne {&waived}
      then
       do:
         message "Only a qualification in good standing or waved status can fully meet a requirement. Are you sure you want to continue?"
             view-as alert-box question buttons yes-no update lCheck.
          
         if not lCheck
          then
           return.
       end.
     
     run activateQualification in hOrganizationDataSrv(input iQualID,
                                                       output std-lo,
                                                       output std-ch).
     
     if not std-lo
      then
       do:
         message std-ch
             view-as alert-box error buttons ok.
         return.
       end.
   end.

  run getQualifications in hOrganizationDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.

  run displayQualification in this-procedure.

  run roleComplianceModified (input ipcOrgID ) .  
  run setButtons in this-procedure.
  
  publish "fulfillmentModifiedRole" .
  
  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  brwqualifications:set-repositioned-row(iCount,"ALWAYS") in frame FRAME-D no-error.
  reposition brwqualifications to rowid std-ro no-error.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow wWin 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hOrganizationDataSrv)
   then
    delete procedure hOrganizationDataSrv.
   
  hOrganizationDataSrv = ?. 
       
  if valid-handle(hComLog)
   then
    run closeWindow in hComLog.
  hComLog = ?.
  
  if valid-handle(hDocument)
   then
    run closeWindow in hDocument.
  hDocument = ?.
      
  /* Close parent and child windows.
     Also clear the temp-table in comstart.w  */
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyQualification wWin 
PROCEDURE copyQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define buffer tQualification for tQualification.

  define variable iQualID   as integer  no-undo.
  define variable iReviewID as integer  no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if not available qualification 
   then
    return.
 
  empty temp-table tQualification.
  create tQualification.
  buffer-copy qualification to tQualification.
  tQualification.qualificationID = 0.
  
  run dialogqualification.w (input hOrganizationDataSrv,
                             input {&OrganizationCopy},
                             input {&OrganizationCode},
                             input organization.orgID,
                             input organization.state,
                             input table tQualification,
                             output std-lo,
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.

  run getQualifications in hOrganizationDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.
  
  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  reposition brwqualifications to rowid std-ro.
  brwqualifications:get-repositioned-row() in frame FRAME-D.
  
  run setButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteAffiliation wWin 
PROCEDURE deleteAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lcheck     as logical no-undo. 
  
  define buffer taffiliation for taffiliation.
  
  if not available affiliation 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
    
  message "Highlighted affiliation will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete affiliation" update lcheck.
   
  if not lcheck 
   then
    return.
  
  /* Server call to delete the affiliated person with an agent */
  run deleteAffiliation in hOrganizationDataSrv (input  affiliation.affiliationId,
                                                 input  true,
                                                 output std-lo,
                                                 output std-ch).
 
  if not std-lo 
   then
    do:
      if std-ch = {&InUse} 
       then
        do:
          message "The Qualification of this person are used to fulfill the requirement of the Organization. Do you still want to delete?"
            view-as alert-box question buttons yes-no update lupdate as logical.
         
         
          if not lUpdate
           then
            return.
   
          /* Server call to delete the affiliated person with an agent */       
          run deleteAffiliation in hOrganizationDataSrv (input  affiliation.affiliationId, 
                                                         input  false,            
                                                         output std-lo,
                                                         output std-ch).   
                                           

        end.
      else
       do:
         message std-ch 
           view-as alert-box error buttons ok.
         return.
       end.
    end.
    
    if std-lo
     then
      if affiliation.partnerBType = 'O' 
       then
        do:
          if affiliation.partnerA = ipcOrgID 
           then
            publish "modifiedAffiliation" (input affiliation.partnerB).
          else
           publish "modifiedAffiliation" (input affiliation.partnerA).
        end.
       else
        publish "modifiedAffiliation" (input affiliation.partnerB).
      
     run getAffiliations in hOrganizationDataSrv (output table taffiliation).
   
     empty temp-table affiliation.
   
     for each taffiliation:
       create affiliation.
       buffer-copy taffiliation to affiliation.
     end.
   
     run displayAffiliation in this-procedure.
     run setButtons         in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteOrganization wWin 
PROCEDURE deleteOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  message "Are you sure you want to delete the selected record?"
    view-as alert-box question buttons yes-no update std-lo.
  
  if not std-lo 
   then
    return.

  run "deleteOrganization" in hOrganizationDataSrv (output std-lo,
                                                    output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  /* Once organization is deleted, close the window. */
  run closeWindow in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteOrgRole wWin 
PROCEDURE deleteOrgRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lSuccess     as logical  no-undo.
  define variable dtStatusDate as datetime no-undo.
  
  do with frame {&frame-name}:
  end.
 
  if not available orgrole
   then
    return.

  message "Highlighted Role will be deleted." skip "Do you want to continue?" 
      view-as alert-box question buttons yes-no title "Delete OrgRole" update std-lo.
    
  if not std-lo 
   then
    return.
      
  run deleteorgrole in hOrganizationDataSrv(input  orgrole.orgroleID,
                                            output std-lo,                      
                                            output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  publish "deactivateAttorney" (orgrole.sourceID , output lSuccess).
  run getOrganizationRole in hOrganizationDataSrv(output table tOrgRole). 
  
  empty temp-table orgrole.

  for each tOrgRole:
    create orgrole.
    buffer-copy tOrgRole to orgrole.
  end.
  
  run displayOrganizationRole in this-procedure.
  run setButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteQualification wWin 
PROCEDURE deleteQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.
 
  if available qualification
   then
    message "Highlighted Organization Qualification will be deleted." skip "Do you want to continue?" 
        view-as alert-box question buttons yes-no title "Delete Organization Qualification" update std-lo.
    
  if not std-lo 
   then
    return.

  run deleteQualification in hOrganizationDataSrv(input qualification.qualificationID,
                                                  output std-lo,                      
                                                  output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  run getQualifications in hOrganizationDataSrv(output table tQualification). 
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tQualification to qualification.
  end.
  
  run displayQualification   in this-procedure.
  run setButtons             in this-procedure. 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayAffiliation wWin 
PROCEDURE displayAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwAffiliations for each affiliation.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayOrganization wWin 
PROCEDURE displayOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer organization for organization.
 
  do with frame {&frame-name}:
  end.

  find first organization where organization.orgID = ipcOrgID no-error.
  if not available organization 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.

  assign 
      fOrgID:screen-value        =  string(organization.orgID)
      fname:screen-value         =  organization.name
      fPhone:screen-value        =  organization.phone
      fEmail:screen-value        =  organization.email
      fMobile:screen-value       =  organization.mobile
      fAddress:screen-value      =  organization.addr1
      fAddress2:screen-value     =  organization.addr2
      fCity:screen-value         =  organization.city
      cbstate:screen-value       =  if lookup(organization.state,cValuePair) = 0 then "" else organization.state
      fZip:screen-value          =  organization.zip
      fWebsite:screen-value      =  organization.website
      fNPN:screen-value          =  organization.NPN
      fAltaUID:screen-value      =  organization.altaUID
      .
    {&window-name}:title = "Organization - " + organization.name + "(" + string(ipcOrgID) + ")".   
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayOrganizationRole wWin 
PROCEDURE displayOrganizationRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frame-E:
  end.
  open query brwOrgRoles for each orgRole by stateID.
  apply "value-changed" to brwOrgRoles.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayQualification wWin 
PROCEDURE displayQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame FRAME-D :
  end.
  open query brwQualifications for each qualification.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave wWin 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first organization where organization.orgID = ipcOrgID no-error.
 
  if not available organization 
   then
    return.

  /* Enable the save button as soon as any organization related information is changed. */
  bSave:sensitive = not    (organization.name      = fName     :input-value     and 
                            organization.NPN       = fNPN      :input-value     and
                            organization.altaUID   = faltaUID  :input-value     and
                            organization.mobile    = fMobile   :input-value     and
                            organization.website   = fWebsite  :input-value     and
                            organization.phone     = fPhone    :input-value     and
                            organization.email     = fEmail    :input-value     and
                            organization.addr1     = fAddress  :input-value     and
                            organization.addr2     = fAddress2 :input-value     and
                            organization.city      = fcity     :input-value     and
                            organization.state     = cbState   :input-value     and 
                            organization.zip       = fZip      :input-value)
                            no-error.  

  bCancel:sensitive = bSave:sensitive.
  
  bEmail:sensitive = not (fEmail    :input-value = "").
  bUrl  :sensitive = not (fWebsite  :input-value = "").
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fName fNPN fAltaUID fAddress fAddress2 fCity cbState fZip fWebsite 
          fEmail fPhone fMobile fOrgID 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bOrganizationDocuments fName bEmail fNPN bOrganizationDelete fAltaUID 
         bRefresh bUrl fAddress fAddress2 fCity cbState fZip fWebsite fEmail 
         fPhone fMobile bViewLog RECT-64 
      WITH FRAME fMain IN WINDOW wWin.
  VIEW FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  ENABLE brwAffiliations bAffiliationAdd bAffiliationOrg 
      WITH FRAME FRAME-C IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-C}
  DISPLAY QualType 
      WITH FRAME FRAME-D IN WINDOW wWin.
  ENABLE QualType bQualificationRenew brwQualifications bQualificationCopy 
         bQualificationVerify bQualificationActive bQualificationAdd 
         bQualificationDelete bQualificationDocuments bQualificationExport 
      WITH FRAME FRAME-D IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-D}
  DISPLAY roleType 
      WITH FRAME FRAME-E IN WINDOW wWin.
  ENABLE bRoleAdd roleType brwFulfillment brwOrgRoles bRoleView bRoleLog 
         bRoleDelete bRoleUpdate 
      WITH FRAME FRAME-E IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-E}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
  return.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData wWin 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cType as character no-undo.

  define variable hTableHandle as handle no-undo.
 
  define buffer affiliation    for affiliation.
  define buffer tAffiliation   for tAffiliation.
  define buffer qualification  for qualification.
  define buffer tQualification for tQualification.

  empty temp-table tAffiliation.
  empty temp-table tQualification.  

  do with frame frame-C:
  end.

  do with frame frame-D:
  end.
  
  if cType = "A" 
   then
    do:
      if query brwAffiliations:num-results = 0 
       then
        do: 
          message "There is nothing to export"
            view-as alert-box warning buttons ok.
          return.
        end.

      for each affiliation no-lock:
        create taffiliation.
        buffer-copy affiliation to taffiliation.
        taffiliation.partnerAName = getEntityName(affiliation.partnerB,
                                                 affiliation.partnerBType,
                                                 affiliation.partnerAName,
                                                 affiliation.partnerBName).
        taffiliation.partnerB     = getEntityID(affiliation.partnerBType,
                                                affiliation.partnerA,
                                                affiliation.partnerB).
        taffiliation.partnerBType = getEntity(affiliation.partnerBType). 
        taffiliation.natureOfAToB = getNature(affiliation.partnerB,
                                              affiliation.partnerBType,
                                              affiliation.natureOfAtoB,
                                              affiliation.natureOfBtoA).
      end.
      
      publish "GetReportDir" (output std-ch).
      
      hTableHandle = temp-table taffiliation:handle.
      
      run util/exporttable.p (table-handle hTableHandle,
                              "taffiliation",
                              "for each taffiliation ",
                              "isCtrlPerson,partnerAName,partnerB,partnerBType,natureOfAToB",
                              "Control Person,Name,ID,Type,Nature",
                              std-ch,
                              "affiliations-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                              true,
                              output std-ch,
                              output std-in).
    end.
  else
   do:
     if query brwQualifications:num-results = 0 
      then
       do: 
         message "There is nothing to export"
           view-as alert-box warning buttons ok.
         return.
       end.
     
     for each qualification no-lock:
       create tQualification.
       buffer-copy qualification to tQualification.
       tQualification.stat =  getQualificationDesc(qualification.stat).
     end.
     
     publish "GetReportDir" (output std-ch).
     
     hTableHandle = temp-table tQualification:handle.
     
     run util/exporttable.p (table-handle hTableHandle,
                             "tQualification",
                             "for each tQualification ",
                             "active,stateID,Qualification,qualificationNumber,stat,effectiveDate,expirationDate",
                             "Active,State ID,Qualification,Number,Status,Effective,Expiration",
                             std-ch,
                             "qualifications-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                             true,
                             output std-ch,
                             output std-in).
                             
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fulFillmentModified wWin 
PROCEDURE fulFillmentModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:   if fullfilment are modified then qualification will also get modified 
           fullfilment can be modified by agent,company or attorney
           so person need to refresh if any of then update fullfilments
------------------------------------------------------------------------------*/
  define input parameter ipiEntityID     as integer no-undo.
  
  if (ipiEntityID ne 0) and  
     (ipiEntityID ne forgId:input-value in frame fMain) and /* do not reresh if not concerned person*/
     (not can-find (first qualification where qualification.inUseCompany and qualification.entityID eq string(ipiEntityID))) /*  do not refresh if company update the fulfilment but person does not have any qualification used by compny */
   then
    return.
 
 if forgId:input-value in frame fMain = string(ipiEntityID)
   then
    run refreshData(yes,    /* iplNeedQualification */
                    no,     /* iplNeedAffiliation   */
                    yes,    /* iplNeedOrgRole       */ 
                    yes).   /* iplNeedReqfulfill    */ 

  apply "value-changed" to brwQualifications in frame frame-d.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData wWin 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwQualifications.  
  close query brwAffiliations.
  close query brwOrgRoles.

  empty temp-table tOrganization.      
  empty temp-table tQualification.
  empty temp-table tAffiliation.  
  empty temp-table tOrgRole.   
  empty temp-table tReqfulfill.

  /* Get person details information in data modal from server. */
  run getOrganizationDetails in hOrganizationDataSrv(output table tOrganization,
                                                     output table tQualification,
                                                     output table tAffiliation,
                                                     output table tOrgRole,
                                                     output table tReqfulfill,
                                                     output std-lo,
                                                     output std-ch). 
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  /* Remove client data. */
  empty temp-table organization.      
  empty temp-table qualification.
  empty temp-table affiliation.  
  empty temp-table orgrole.
  empty temp-table reqfulfill.

  /* Update the local copy of data. */
  for each tOrganization:
    create organization.
    buffer-copy tOrganization to organization.
  end.
 
  for each tAffiliation:
    create affiliation.
    buffer-copy tAffiliation to affiliation.
  end.

  for each tQualification:
    create qualification.
    buffer-copy tQualification to qualification.
  end.
 
  for each tOrgRole: 
    create orgrole.
    buffer-copy tOrgRole to orgrole.
    inooffulfillreq  = 0.
    for each treqfulfill where treqfulfill.orgroleID = tOrgRole.orgroleID:
       inooffulfillreq = inooffulfillreq + 1.
    end.
    assign orgrole.totfulfillreq = inooffulfillreq.
  end.
 
  for each treqfulfill: 
    create reqfulfill.
    buffer-copy treqfulfill to reqfulfill.
  end.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  /* Code placed here will execute PRIOR to standard behavior. */
  {&window-name}:window-state = 2. /* Minimized */

  run super.
  
  {&window-name}:max-width-pixels  = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.
 
  publish "GetStateIDList"(input ",",
                           output cValuePair,
                           output std-lo,
                           output std-ch).
                           
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  cbstate:list-items in frame {&frame-name} = trim(cValuePair).
  
  run selectPage in this-procedure(if ipiPageNo > 0 then ipiPageNo else 1).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifiedAffiliation wWin 
PROCEDURE modifiedAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcPartnerB as character no-undo.
 
  if ipcPartnerB = ipcOrgID 
   then
    do:
      run refreshData in this-procedure (no,       /* iplNeedQualification */
                                         yes,      /* iplNeedAffiliation   */
                                         yes,      /* iplNeedOrgRole       */ 
                                         yes).     /* iplNeedReqfulfill    */
  
    
      run setButtons in this-procedure.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyAffiliation wWin 
PROCEDURE modifyAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iAffiliationId as integer no-undo.

  do with frame {&frame-name}:
  end.
  
  if not available Affiliation
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.

  iAffiliationId = Affiliation.AffiliationID.

  empty temp-table tAffiliation.

  create tAffiliation.
  buffer-copy Affiliation to tAffiliation no-error.
  
  if affiliation.partnerBType = {&OrganizationCode} 
   then
    run dialogaffiliation-otoo.w(input table tAffiliation,
                                 input ipcOrgID,
                                 input fname:input-value,
                                 input hOrganizationDataSrv,
                                 output std-lo).
  else
   run dialogaffiliation-otop.w(input table tAffiliation,
                                input ipcOrgID,
                                input fname:input-value,
                                input hOrganizationDataSrv,
                                output std-lo).   

  if not std-lo 
   then
    return.
  
  /* Get updated data of affiliated persons with an agent */
  run getAffiliations in hOrganizationDataSrv (output table tAffiliation).
  
  /* Clearing Browser */
  close query brwAffiliations.
  empty temp-table Affiliation.

  for each tAffiliation:
    create Affiliation.
    buffer-copy tAffiliation to Affiliation.
  end.
  
  run displayAffiliation in this-procedure.

  find first Affiliation where Affiliation.AffiliationID = iAffiliationId no-error.
  if not available Affiliation 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
  
  std-ro = rowid(Affiliation).
  
  reposition brwAffiliations to rowid std-ro no-error.
  run setbuttons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyOrgRole wWin 
PROCEDURE modifyOrgRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount           as integer     no-undo.
  define variable opiOrgRoleID     as integer     no-undo.
  define variable iOrgRoleID       as integer     no-undo.

  empty temp-table ttOrgRole.
 
  if not available orgrole 
   then
    return.

  do with frame {&frame-name}:
  end.
   
  do iCount = 1 to brwOrgRoles:num-iterations in frame FRAME-E:
    if brwOrgRoles:is-row-selected(iCount)
     then
      leave.
  end.

  create ttOrgRole.
  buffer-copy orgrole to ttOrgRole.
  iOrgRoleID = orgrole.orgroleID.
                           
  run dialogneworgrole.w (input hOrganizationDataSrv,
                          input integer(fOrgID:input-value),
                          input fName:input-value,
                          input table ttOrgRole,
                          output opiOrgRoleID,
                          output std-lo).
  
  if not std-lo 
   then
    return.

  empty temp-table tOrgRole.
  run getOrganizationRole in hOrganizationDataSrv(output table tOrgRole).
 
  empty temp-table orgrole.

  for each tOrgRole:
    create orgrole.
    buffer-copy tOrgRole to orgrole.
  end.
  
  for first orgrole
    where orgrole.orgRoleID = iOrgRoleID :
    std-ro = rowid(orgrole).
  end.

  brwOrgRoles:set-repositioned-row(iCount,"ALWAYS").
  reposition brwOrgRoles to rowid std-ro.  

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyQualification wWin 
PROCEDURE modifyQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount           as integer     no-undo.
  define variable iQualID          as integer     no-undo.
  define variable iReviewID        as integer     no-undo.
  define variable iQualificationID as integer     no-undo.

  define buffer tQualification for tQualification.

  empty temp-table ttQualification.
 
  if not available qualification 
   then
    return.

  do with frame  {&FRAME-NAME}:
  end.
   
  do iCount = 1 to brwqualifications:num-iterations in frame FRAME-D:
    if brwqualifications:is-row-selected(iCount)
     then
      leave.
  end.

  create ttQualification.
  buffer-copy qualification to ttQualification.
  iQualificationID = qualification.qualificationID.
  
  run dialogqualification.w (input hOrganizationDataSrv,
                             input {&OrganizationModify},
                             input {&OrganizationCode},             
                             input fOrgId:input-value, 
                             input qualification.stateId , 
                             input table ttQualification,
                             output std-lo,  
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.
  empty temp-table tQualification.
  run getQualifications in hOrganizationDataSrv(output table tQualification).
 
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.

  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualificationID :
    std-ro = rowid(qualification).
  end.
  
  brwqualifications:set-repositioned-row(iCount,"ALWAYS") in frame FRAME-D.
  reposition brwqualifications to rowid std-ro.  
  
  run roleComplianceModified (input ipcOrgID ) .  
  run setButtons in this-procedure.
  
  publish "fulfillmentModifiedRole" .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openAffiliation wWin 
PROCEDURE openAffiliation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available affiliation 
   then
    return.
    
  if affiliation.partnerBtype = {&OrganizationCode} 
   then
    do:
      if affiliation.partnerB = ipcOrgID 
       then
        publish "OpenWindow" (input {&Organization},                                /*childtype*/
                              input affiliation.partnerA,                           /*childid*/
                              input "worganizationdetail.w",                        /*window*/
                              input "character|input|" + affiliation.partnerA + "^integer|input|1" + "^character|input|",                                   
                              input this-procedure).
      else
       publish "OpenWindow" (input {&organization},                                /*childtype*/ 
                             input affiliation.partnerB,                           /*childid*/
                             input "worganizationdetail.w",                        /*window*/
                             input "character|input|" + affiliation.partnerB + "^integer|input|1" + "^character|input|",                                   
                             input this-procedure).
    end.
  else
   publish "OpenWindow" (input {&person},                                   /*childtype*/
                         input affiliation.partnerB,                        /*childid*/
                         input "wpersondetail.w",                           /*window*/
                         input "character|input|" + affiliation.partnerB + "^integer|input|1" + "^character|input|",                                   
                         input this-procedure).                             /*currentProcedure handle*/                         
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDocument wWin 
PROCEDURE openDocument :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available qualification 
   then
    return.
  
 /* Open child window and maintain its instance */
  publish "OpenDocument" (input "Compliance",
                          input qualification.qualificationId,
                          input this-procedure).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openLog wWin 
PROCEDURE openLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fMain:
  end.

  if valid-handle(hComLog)  
   then
    run showWindow in hComLog.
  else
   run wcompliancelog.w persistent set hComLog 
       (input {&OrganizationCode},   /* Entity = o */
        input fOrgID:input-value ,   /* Organization Id */
        input "All",                 /* State ID */           
        input "All",                 /* role */
        input "All",                 /* roleID */
        input fname:input-value).    /* Entity Name */

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openOrganizationRole wWin 
PROCEDURE openOrganizationRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable iCount      as integer     no-undo.
  define variable iOrgRoleID  as integer     no-undo.                   
  
  empty temp-table torgRole.
 
  if not available orgRole
   then
    return.
   
  do iCount = 1 to brwOrgRoles:num-iterations in frame FRAME-E:
    if brwOrgRoles:is-row-selected(iCount)
     then
      leave.
  end.

  create torgRole.
  buffer-copy orgRole to torgRole.
  iOrgRoleID = orgRole.orgRoleID .

  publish "OpenWindow" (input "organizationrole",      /*childtype*/
                        input string(iOrgRoleID),      /*childid*/
                        input "worganizationrole.w" ,  /*window*/
                        input "handle|input|" + string(hOrganizationDataSrv) + "^integer|input|" + string(iOrgRoleID),
                        input this-procedure).         /*currentProcedure handle*/ 

                       
  run getOrganizationRole in hOrganizationDataSrv(output table torgRole).
 
  empty temp-table orgRole.

  for each torgrole:
    create orgrole.
    buffer-copy torgrole to orgrole.
    inooffulfillreq  = 0.
    for each treqfulfill where treqfulfill.orgroleID = tOrgRole.orgroleID:
      inooffulfillreq = inooffulfillreq + 1.
    end.
    assign orgrole.totfulfillreq = inooffulfillreq.
  end.
  
  apply 'value-changed' to roleType.
  
  for first orgRole
    where orgRole.orgRoleID = iorgRoleID:
    std-ro = rowid(orgRole).
  end.

  brwOrgRoles:set-repositioned-row(iCount,"ALWAYS").
  reposition brwOrgRoles to rowid std-ro.  

  run setButtons in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openReview wWin 
PROCEDURE openReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available qualification 
   then
    return.
    
  /* Open Child window and maintain its instance */ 
  publish "OpenWindow" (input "review", 
                        input string(qualification.qualificationID), 
                        input "wqualificationreviews.w", 
                        input "handle|input|" + string(hOrganizationDataSrv) + "^integer|input|" + string(qualification.qualificationID),                                   
                        input this-procedure).                      
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openRoleLog wWin 
PROCEDURE openRoleLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not available orgrole
   then return.

  publish "OpenWindow" (input "wcompliancelog",                           /*childtype*/
                        input orgrole.sourceID,                           /*childid*/
                        input "wcompliancelog.w",                         /*window*/
                        input "character|input|" + {&OrganizationCode} + "^character|input|" + orgrole.orgID + "^character|input|" + orgrole.stateID + "^character|input|" + orgrole.source +
                              "^character|input|" + (if orgrole.source = {&Agent} or  orgrole.source = {&AttorneyFirm} then orgrole.sourceID else orgrole.orgID) + "^character|input|" + organization.name,                                   
                        input this-procedure).                           /* parent window handle */ 
           
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openURL wWin 
PROCEDURE openURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcURL as character no-undo.

 run ShellExecuteA in this-procedure (0,
                                      "open",
                                      ipcURL,
                                      "",
                                      "",
                                      1,
                                      output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE organizationStatusModified wWin 
PROCEDURE organizationStatusModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcOrgIDLocal as character no-undo.

  if ipcOrgIDLocal ne ipcOrgID 
   then
    return.

  run getData               in this-procedure.
  run displayOrganization   in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData wWin 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iplNeedQualification as logical.
  define input parameter iplNeedAffiliation   as logical.
  define input parameter iplNeedOrgRole       as logical.
  define input parameter iplNeedReqfulfill    as logical.

  run refreshOrganizationDetails in hOrganizationDataSrv (input  iplNeedQualification, /*  NeedQualification  */         
                                                          input  iplNeedAffiliation,   /*  NeedAffiliation */       
                                                          input  iplNeedOrgRole,       /*  NeedOrgRole */     
                                                          input  iplNeedReqfulfill,    /*  NeedReqfulfill */
                                                          output std-lo,
                                                          output std-ch).
  if not std-lo            
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end. 

  /* Delete all local data and get updated data from server */
  run getData                 in this-procedure.
  run displayOrganization     in this-procedure.
  run displayAffiliation      in this-procedure.   
  run displayOrganizationRole in this-procedure.  
  run displayQualification    in this-procedure.
  run setButtons              in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshFulfillmentData wWin 
PROCEDURE refreshFulfillmentData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE variable iOrgRoleID  as integer NO-UNDO.
  
  assign inooffulfillreq = 0.
  
  empty temp-table reqfulfill.
  
  for each treqfulfill where treqfulfill.orgroleID = orgrole.orgroleID:
    create reqfulfill. 
    buffer-copy treqfulfill to reqfulfill.
    inooffulfillreq = inooffulfillreq + 1.
  end.
  
  assign orgrole.totfulfillreq = inooffulfillreq
         iOrgRoleID = orgrole.orgroleID.
         
  for first orgrole
    where orgrole.orgRoleID = iOrgRoleID :
    std-ro = rowid(orgrole).
  end.

  open query brwFulfillment for each reqfulfill.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshQualifications wWin 
PROCEDURE refreshQualifications :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameter Definition */
  define input parameter ipiQualificationID as integer no-undo.
  
  /* Local Variable */
  define variable iCount  as integer no-undo.

  if not can-find(first qualification where qualification.qualificationID = ipiQualificationID)
   then
    return.  
  
  do with frame {&frame-name}:
  end.
  
  do iCount = 1 to brwQualifications:num-iterations in frame FRAME-D:
    if brwQualifications:is-row-selected(iCount)
     then
      leave.
  end.

  run refreshData in this-procedure (yes, /*  NeedQualification */  
                                     no,  /*  NeedAffiliation   */                
                                     no,  /*  NeedOrgrole    */
                                     no). /*  NeedReqFulfill    */

  for first qualification 
    where qualification.qualificationID = ipiQualificationID:
    std-ro = rowid(qualification).
  end.

  brwQualifications:set-repositioned-row(iCount,"ALWAYS") no-error.
  reposition brwQualifications to rowid std-ro no-error.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE renewQualification wWin 
PROCEDURE renewQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define buffer tQualification for tQualification.

  define variable iQualID   as integer  no-undo.
  define variable iReviewID as integer  no-undo.
  define variable iPrevQualID  as integer  no-undo.
  
  do with frame {&frame-name}:
  end.
  
  if not available qualification 
   then
    return.
 
  empty temp-table tQualification.
  create tQualification.
  buffer-copy qualification to tQualification.
  iPrevQualID = tQualification.qualificationID.
  
  run dialogqualification.w (input hOrganizationDataSrv,
                             input {&OrganizationRenew},
                             input {&OrganizationCode},
                             input organization.orgID,
                             input organization.state,
                             input table tQualification,
                             output std-lo,
                             output iQualID,
                             output iReviewID).
  
  if not std-lo 
   then
    return.

  run getQualifications in hOrganizationDataSrv(output table tQualification).
  
  empty temp-table qualification.

  for each tQualification:
    create qualification.
    buffer-copy tqualification to qualification.
  end.
  
  run refreshQualifications in this-procedure (input iPrevQualID).
  run displayQualification in this-procedure.

  for first qualification
    where qualification.qualificationID = iQualID:
    std-ro = rowid(qualification).
  end.

  reposition brwqualifications to rowid std-ro.
  brwqualifications:get-repositioned-row() in frame FRAME-D.
  
  run setButtons in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE requirementModified wWin 
PROCEDURE requirementModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcStateID  as character.
  define input parameter ipcEntity   as character.
  define input parameter ipcRole     as character.

  do with frame {&frame-name}:
  end. 
 
  if (ipcEntity = {&OrganizationCode} and not can-find(first orgRole where orgRole.stateId = ipcStateID and orgRole.source = ipcRole)) 
   then
    return.

  run refreshData(yes,   /* iplNeedQualification */
                  yes,   /* iplaffiliation       */
                  yes,   /* iplNeedOrgRole       */ 
                  yes).  /* iplNeedReqfulfill    */
                   
  publish "fulfillmentModifiedRole" .                     

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE roleComplianceModified wWin 
PROCEDURE roleComplianceModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: 
------------------------------------------------------------------------------*/
  define input parameter ipcEntityID as character no-undo.
  
  if ipcEntityID ne ipcOrgID
   then
    return.

  run refreshData(no,    /* iplNeedQualification    */
                  no,    /* iplNeedAffiliation      */
                  yes,   /* iplNeedOrgRole          */ 
                  yes).  /* iplNeedReqfulfill       */

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveOrganization wWin 
PROCEDURE saveOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer torganization for torganization.
  define buffer organization  for organization.

  empty temp-table torganization.

  find first organization where organization.orgID = ipcOrgID no-error.
  if not available organization 
   then
    return.

  create torganization.
  assign
      torganization.orgID   =  ipcOrgID
      torganization.name    =  fName:input-value 
      torganization.addr1   =  fAddress:input-value
      torganization.addr2   =  fAddress2:input-value
      torganization.city    =  fCity:input-value
      torganization.state   =  cbState:input-value
      torganization.zip     =  fZip:input-value  
      torganization.email   =  fEmail:input-value
      torganization.website =  fWebsite:input-value
      torganization.phone   =  fPhone:input-value
      torganization.mobile  =  fMobile:input-value
      torganization.NPN     =  fNPN:input-value
      torganization.altaUID =  fAltaUID:input-value
      torganization.stat    =  organization.stat
      .

  run modifyOrganization in horganizationDataSrv (input table tOrganization,
                                                  output std-lo,
                                                  output std-ch).
  if not std-lo            
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  empty temp-table organization.

  run getOrganization in hOrganizationDataSrv (output table torganization).

  for each torganization:
    create organization.
    buffer-copy torganization to organization.
  end.
  
  run displayOrganization in this-procedure.
  run enableDisableSave   in this-procedure.
  run setButtons          in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectPage wWin 
PROCEDURE selectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter piPageNum as integer no-undo.

  /* Code placed here will execute PRIOR to standard behavior. */
  run super(input piPageNum).
  
  if piPageNum = 1 
   then
    do:
      assign 
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-E:visible   = false
          frame FRAME-E:sensitive = false
          frame FRAME-C:visible   = true
          frame FRAME-C:sensitive = true
          .
    end.
   else if piPageNum = 2 
    then
     do:
       assign 
           frame FRAME-C:visible   = false
           frame FRAME-C:sensitive = false
           frame FRAME-E:visible   = false
           frame FRAME-E:sensitive = false
           frame FRAME-D:visible   = true
           frame FRAME-D:sensitive = true
           .
     end.
   else
    do:
      assign 
          frame FRAME-C:visible   = false
          frame FRAME-C:sensitive = false
          frame FRAME-D:visible   = false
          frame FRAME-D:sensitive = false
          frame FRAME-E:visible   = true
          frame FRAME-E:sensitive = true
          .
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons wWin 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  do with frame FRAME-D:
  end.
  
  if query brwQualifications:num-results = 0 or query brwQualifications:num-results = ?
   then
    assign 
        bQualificationExport:sensitive    in frame FRAME-D = false
        bQualificationUpdate:sensitive    in frame FRAME-D = false
        bQualificationDelete:sensitive    in frame FRAME-D = false
        bQualificationDocuments:sensitive in frame FRAME-D = false
        bQualificationVerify:sensitive    in frame FRAME-D = false
        bQualificationActive:sensitive    in frame FRAME-D = false
        bQualificationCopy:sensitive      in frame FRAME-D = false
        bQualificationRenew:sensitive     in frame FRAME-D = false
        .
  else
   do:
     assign 
         bQualificationExport:sensitive    in frame FRAME-D    = true
         bQualificationUpdate:sensitive    in frame FRAME-D    = true    
         bQualificationDocuments:sensitive in frame FRAME-D    = true
         bQualificationVerify:sensitive    in frame FRAME-D    = true
         bQualificationActive:sensitive    in frame FRAME-D    = true
         bQualificationDelete:sensitive    in frame FRAME-D    = true
         bQualificationCopy:sensitive      in frame FRAME-D    = true
         bQualificationRenew:sensitive     in frame FRAME-D    = true
         .
                
  if available qualification
   then
    do:
      if Qualification.activ 
       then
        do:
          bQualificationActive:load-image("images/s-flag_red.bmp").
          bQualificationActive:tooltip   = "Deactivate qualification".
          bQualificationDelete:sensitive = false.
        end.
      else
       do:
         bQualificationActive:load-image("images/s-flag_green.bmp").
         bQualificationActive:tooltip   = "Activate qualification".
         bQualificationDelete:sensitive = true.
       end.
    end. 
  end.
 
  if query brwAffiliations:num-results = 0 or query brwAffiliations:num-results = ? 
   then
    assign  
        bAffiliationExport:sensitive in frame FRAME-C = false
        bAffiliationUpdate:sensitive in frame FRAME-C = false
        bAffiliationDelete:sensitive in frame FRAME-C = false
        bAffiliationView  :sensitive in frame FRAME-C = false
        .   
  else    
   assign  
       bAffiliationExport:sensitive in frame FRAME-C = true
       bAffiliationUpdate:sensitive in frame FRAME-C = true
       bAffiliationDelete:sensitive in frame FRAME-C = true
       bAffiliationView  :sensitive in frame FRAME-C = true
       .
  
  do with frame FRAME-E:
  end.
  
  if query brwOrgRoles:num-results = 0 or query brwOrgRoles:num-results = ? 
   then
    assign
        bRoleUpdate:sensitive   = false
        bRoleDelete:sensitive   = false
        bRoleLog:sensitive      = false
        bRoleView:sensitive     = false
        .
  else
   assign
       bRoleUpdate:sensitive   = true
       bRoleDelete:sensitive   = true
       bRoleLog:sensitive      = true
       bRoleView:sensitive     = true
       .
        
  if fEmail:input-value = "" 
   then
    bEmail:sensitive = false.
  else
   bEmail:sensitive = true. 
   
  if fWebsite:input-value = "" 
   then
    bUrl:sensitive = false.
  else
   bUrl:sensitive = true.
  
  run enableDisableSave in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow wWin 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  assign
      frame fmain:width-pixels                              = {&window-name}:width-pixels
      frame fmain:virtual-width-pixels                      = {&window-name}:width-pixels
      frame fmain:height-pixels                             = {&window-name}:height-pixels
      frame fmain:virtual-height-pixels                     = {&window-name}:height-pixels
      .
      
  run resizeObject in h_folder (frame fMain:height - 3, frame fMain:width - 2). 

  assign
      
      frame frame-C:width-pixels                        = {&window-name}:width-pixels  - 20 
      frame frame-C:virtual-width-pixels                = {&window-name}:width-pixels  - 20  
      frame frame-C:height-pixels                       = {&window-name}:height-pixels - 103.90     
      frame frame-C:virtual-height-pixels               = {&window-name}:height-pixels - 103.90     
      
      
      frame frame-E:width-pixels                              = frame fmain:width-pixels    - 20 
      frame frame-E:virtual-width-pixels                      = frame fmain:width-pixels    - 20  
      frame frame-E:height-pixels                             = frame fmain:height-pixels   - 103.90 
      frame frame-E:virtual-height-pixels                     = frame fmain:height-pixels   - 103.90 
      
      
      frame frame-D:width-pixels                              = frame fmain:width-pixels  - 20 
      frame frame-D:virtual-width-pixels                      = frame fmain:width-pixels  - 20  
      frame frame-D:height-pixels                             = frame fmain:height-pixels - 103.90 
      frame frame-D:virtual-height-pixels                     = frame fmain:height-pixels - 103.90 
      
      brwAffiliations:width-pixels    in frame frame-C    = frame frame-C:width-pixels - 32
      brwAffiliations:height-pixels   in frame frame-C    = frame frame-C:height-pixels - brwAffiliations:y - 10
      brwOrgRoles:height-pixels       in frame frame-E    = frame frame-E:height-pixels - brwOrgRoles:y - 10
      brwFulfillment:width-pixels     in frame frame-E    = frame frame-E:width-pixels - brwOrgRoles:width-pixels - 43
      brwFulfillment:height-pixels    in frame frame-E    = frame frame-E:height-pixels - brwFulfillment:y - 10
      brwQualifications:width-pixels  in frame frame-D    = frame frame-D:width-pixels - 32
      brwQualifications:height-pixels in frame frame-D    = frame frame-D:height-pixels - brwQualifications:y - 10
      .

 /* resize the description column in the claims browse */
  {lib/resize-column.i &col="'name'" &var=dAffiliation &browse-name=brwAffiliations}
  {lib/resize-column.i &col="'Qualification,QualificationNumber'" &var=dAffiliation &var=dQualificationNum &browse-name=brwQualifications}
  {lib/resize-column.i &col="'reqdesc,qualification'" &var=dColumnWidth &var=dColumnWidthQ}    
     
  run ShowScrollBars(frame fmain:handle, no, no).
  run ShowScrollBars(frame frame-C:handle, no, no).
  run ShowScrollBars(frame frame-E:handle, no, no).
  run ShowScrollBars(frame frame-D:handle, no, no).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&OrganizationCode}
   then
    cStat = {&Organization}.
  else
   cStat = {&Person}.
  
  return  cStat.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityID wWin 
FUNCTION getEntityID RETURNS CHARACTER
  ( cPartnerBType as character,
    cPartnerA     as character,
    cPartnerB     as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cPartnerBType = {&OrganizationCode} 
   then
    do:
      if cPartnerB = ipcOrgID 
       then
        return cPartnerA.
      else
       return cPartnerB.
    end.
  else
   return cPartnerB.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityName wWin 
FUNCTION getEntityName RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cPartnerAName as character,
    cPartnerBName as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if cPartnerBType = {&OrganizationCode} 
   then
    do:
      if cPartnerB = ipcOrgID 
       then
        return cPartnerAName.
      else
       return cPartnerBName.
    end.
  else
   return cPartnerBName.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getNature wWin 
FUNCTION getNature RETURNS CHARACTER
  ( cPartnerB     as character,
    cPartnerBType as character,
    cNatureAtoB   as character,
    cNatureBtoA   as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cPartnerBType = {&OrganizationCode} 
   then
    do:
      if cPartnerB = ipcOrgID 
       then
        return cNatureAtoB.
      else
       return cNatureBtoA.
    end.
  else
   return cNatureBtoA.
   
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:   
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getRoleStatus wWin 
FUNCTION getRoleStatus RETURNS CHARACTER
  ( cStat as logical /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cVar as character no-undo.
  
  if cStat
   then
    cVar = {&Active}.
  else
   cVar = {&InActive}.
  
  return  cVar.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

