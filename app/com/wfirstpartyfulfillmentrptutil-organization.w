&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wfirstpartyfulfillment-role.w 

  Description: First Party Requirement Fulfillment Utility for Role 

  Author: Rahul Sharma 

  Created: 01/15/2019 
   
  Modified
  Date          Name           Description
  08/09/2019    Gurvindar      Removed progress error while populating combo-box  
  04/06/2021    SA             Task#93007 Bug fix in first-party fulfillment utility report.
  05/09/2023    S Chandu       Task#104481 Bug fix in first-party fulfillment utility 
                               while doing fulfillment.
  06/27/2023    S Chandu       Task#104582 Refresh issue in First-party fulfilment utility.							   
  10/13/2023    Shefali        Task#107737 Modified to show requirements which is for ALL entity.
------------------------------------------------------------------------*/

create widget-pool.

/*Parameters*/
define input parameter ipcentity as character no-undo.

{tt/cmpfulfillments.i}
{tt/cmpfulfillments.i &tableAlias = "tcmpfulfillments"}
{tt/cmpfulfillments.i &tableAlias = "tSelectedRecords"}
{tt/cmpfulfillments.i &tableAlias = "ttcmpfulfillments"}
{tt/statereqqual.i}  /* Just a place holder */
{tt/statereqqual.i &tableAlias = "tstatereqqual"}
{tt/qualification.i} /* Just a place holder */
{tt/fulfillment.i}   /* Just a place holder */

/* Local Variable Definitions -----                                      */
{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}

define variable cExpire           as character  no-undo.
define variable iExpireMonth      as integer    no-undo.
define variable iExpireDay        as integer    no-undo.
define variable iExpireYear       as integer    no-undo.
define variable iExpireDays       as integer    no-undo.
define variable dColumnWidth      as decimal    no-undo.
define variable cRequirement      as character  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwAffentity

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tcmpfulfillments

/* Definitions for BROWSE brwAffentity                                  */
&Scoped-define FIELDS-IN-QUERY-brwAffentity tcmpfulfillments.lSelect tcmpfulfillments.entityID "ID" tcmpfulfillments.entityName "Name" tcmpfulfillments.entity "Role" tcmpfulfillments.roleName "RoleName" tcmpfulfillments.roleID "RoleID" getQualificationDesc(tcmpfulfillments.stat) @ tcmpfulfillments.stat "Status" tcmpfulfillments.effectiveDate tcmpfulfillments.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAffentity tcmpfulfillments.lSelect   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwAffentity tcmpfulfillments
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwAffentity tcmpfulfillments
&Scoped-define SELF-NAME brwAffentity
&Scoped-define QUERY-STRING-brwAffentity for each tcmpfulfillments
&Scoped-define OPEN-QUERY-brwAffentity open query {&SELF-NAME} for each tcmpfulfillments.
&Scoped-define TABLES-IN-QUERY-brwAffentity tcmpfulfillments
&Scoped-define FIRST-TABLE-IN-QUERY-brwAffentity tcmpfulfillments


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwAffentity}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState brwAffentity bStart bCancel RECT-63 ~
RECT-65 
&Scoped-Define DISPLAYED-OBJECTS cbState flfulfill flQual cbRole ~
cbRequirement cbExpired 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen returns logical private
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged returns logical private
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bStart 
     LABEL "Start" 
     SIZE 15 BY 1.14.

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Search for Agents based on entered terms".

DEFINE VARIABLE cbExpired AS CHARACTER FORMAT "X(256)":U 
     LABEL "Expired" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "ALL","Yes","No","None" 
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE cbRequirement AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)" 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE flfulfill AS CHARACTER FORMAT "X(256)":U 
     LABEL "Applies To" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE flQual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 40 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 115.8 BY 4.38.

DEFINE RECTANGLE RECT-65
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 52.4 BY 4.38.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAffentity FOR 
      tcmpfulfillments SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAffentity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAffentity C-Win _FREEFORM
  QUERY brwAffentity DISPLAY
      tcmpfulfillments.lSelect        column-label "Select"                  width 8   view-as toggle-box
tcmpfulfillments.entityID       label        "ID"   format "x(8)"            width 8
tcmpfulfillments.entityName     label        "Name" format "x(80)"           width 45
tcmpfulfillments.entity         label        "Role" format "x(24)"           width 15
tcmpfulfillments.roleName       label        "RoleName" format "x(80)"       width 20
tcmpfulfillments.roleID         label        "RoleID"   format "x(24)"       width 20
getQualificationDesc(tcmpfulfillments.stat)
 @ tcmpfulfillments.stat        label        "Status"      format "x(20)"    width 16
tcmpfulfillments.effectiveDate  column-label "Effective"   format "99/99/99" width 12
tcmpfulfillments.expirationDate column-label "Expiration"  format "99/99/99" width 12
enable tcmpfulfillments.lSelect
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 168 BY 15.86
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btGet AT ROW 3.48 COL 108 WIDGET-ID 42 NO-TAB-STOP 
     cbState AT ROW 2.43 COL 9.2 COLON-ALIGNED WIDGET-ID 90
     flfulfill AT ROW 4.14 COL 53 COLON-ALIGNED WIDGET-ID 278 NO-TAB-STOP 
     flQual AT ROW 3 COL 53 COLON-ALIGNED WIDGET-ID 276 NO-TAB-STOP 
     cbRole AT ROW 3.57 COL 9.2 COLON-ALIGNED WIDGET-ID 244
     cbRequirement AT ROW 1.86 COL 53 COLON-ALIGNED WIDGET-ID 18
     cbExpired AT ROW 3 COL 131.6 COLON-ALIGNED WIDGET-ID 268
     brwAffentity AT ROW 6 COL 2.2 WIDGET-ID 200
     bStart AT ROW 22.1 COL 67.2 WIDGET-ID 238
     bCancel AT ROW 22.1 COL 89.2 WIDGET-ID 204
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.05 COL 119 WIDGET-ID 262
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.05 COL 3.2 WIDGET-ID 266
     RECT-63 AT ROW 1.33 COL 2.2 WIDGET-ID 236
     RECT-65 AT ROW 1.33 COL 117.6 WIDGET-ID 260
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 170.4 BY 22.38
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "First-Party Fulfillment Utility"
         HEIGHT             = 22.38
         WIDTH              = 170.2
         MAX-HEIGHT         = 33.86
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.86
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAffentity cbExpired DEFAULT-FRAME */
ASSIGN 
       brwAffentity:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwAffentity:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON btGet IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbExpired IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbRequirement IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbRole IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flfulfill IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flQual IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAffentity
/* Query rebuild information for BROWSE brwAffentity
     _START_FREEFORM
open query {&SELF-NAME} for each tcmpfulfillments.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAffentity */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* First-Party Fulfillment Utility */
or endkey of {&window-name} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* First-Party Fulfillment Utility */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* First-Party Fulfillment Utility */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME DEFAULT-FRAME /* Cancel */
do:
  apply "close":u to this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAffentity
&Scoped-define SELF-NAME brwAffentity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffentity C-Win
ON DEFAULT-ACTION OF brwAffentity IN FRAME DEFAULT-FRAME
do:
  if not available tcmpfulfillments
   then 
    return.
  if flfulfill:screen-value = {&Organization}
   then
    publish "OpenWindow" (input {&organization}, 
                          input tcmpfulfillments.entityID, 
                          input "worganizationdetail.w", 
                          input "character|input|" + tcmpfulfillments.entityID + "^integer|input|2" + "^character|input|",                                   
                          input this-procedure).   
  else
   publish "OpenWindow" (input {&person}, 
                        input tcmpfulfillments.entityID, 
                        input "wpersondetail.w", 
                        input "character|input|" + tcmpfulfillments.entityID + "^integer|input|2" + "^character|input|",                                   
                        input this-procedure).
                        
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffentity C-Win
ON ROW-DISPLAY OF brwAffentity IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffentity C-Win
ON START-SEARCH OF brwAffentity IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bStart C-Win
ON CHOOSE OF bStart IN FRAME DEFAULT-FRAME /* Start */
do:
  run actionStart in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME DEFAULT-FRAME /* Get */
do:
  resetScreen().
  /* Get all the Person or organization Roles */
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbExpired
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbExpired C-Win
ON VALUE-CHANGED OF cbExpired IN FRAME DEFAULT-FRAME /* Expired */
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRequirement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRequirement C-Win
ON VALUE-CHANGED OF cbRequirement IN FRAME DEFAULT-FRAME /* Requirement */
do:
  resetScreen().
  resultsChanged(false).

  run setQualification in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole C-Win
ON VALUE-CHANGED OF cbRole IN FRAME DEFAULT-FRAME /* Role */
do:
  close query brwAffentity.
  resetScreen().
  resultsChanged(false).
 
  run getRequirementQualifications in this-procedure.
  run getRequirementCombo in this-procedure.
  run setWidgetsState in this-procedure.
   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME DEFAULT-FRAME /* State */
do:
   close query brwAffentity.
   resetScreen().
   resultsChanged(false).
   
   cbRole:sensitive = (self:input-value <> ""). 

   resetScreen().
   resultsChanged(false).
  
   if self:screen-value <> "" 
    then 
     run getRequirementQualifications in this-procedure.

   run getRequirementCombo in this-procedure.
   
   run setWidgetsState in this-procedure.
   
   /* removing select state once other state is selected */
  {lib/modifylist.i &cbhandle=cbState:handle &list=list-item-pairs &delimiter = "," &item = '{&SelectState}'}

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

{lib/brw-main.i}
{lib/win-main.i}
{lib/win-status.i}

/* Removed the update events for utility, as the usability is very rare. */
/*-----------------event subscription----------------------------*/
subscribe to "inUseQualificationModified"  anywhere.
subscribe to "requirementModified"         anywhere run-procedure "requirementsModified".
subscribe to "qualificationReviewModified" anywhere run-procedure "inUseQualificationModified".
subscribe to "affiliationModified"         anywhere.
subscribe to "fulFillmentModified"         anywhere run-procedure "affiliationModified".

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

run getrole in this-procedure.

/*  It allows multiple selection of attorney when requirement is met by different person 
    and single selection of attorney when requirement is fulfilled by the same person */

on value-changed of tcmpfulfillments.lSelect in browse brwAffentity
do:
  assign
       std-ch = tcmpfulfillments.roleID
       std-ro = rowid(tcmpfulfillments)
       .
  
  for each tcmpfulfillments where tcmpfulfillments.roleID = std-ch:
    if rowid(tcmpfulfillments) = std-ro 
     then
      tcmpfulfillments.lSelect = not tcmpfulfillments.lSelect.  
    else 
      tcmpfulfillments.lSelect = false.                 
  end. /* for each tcmpfulfillments */
  
  browse brwAffentity:refresh().
  bstart:sensitive in frame {&frame-name} = can-find(first tcmpfulfillments where tcmpfulfillments.lSelect) .
  
end. 


  
pause 0 before-hide.

btGet:load-image ("images/completed.bmp").              
btGet:load-image-insensitive("images/completed-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  /* Getting states from config */
  publish "GetSearchStates" (output std-ch).
  if std-ch > "" then
   std-ch = "," + std-ch. 
   
  cbState:list-item-pairs = {&SelectState} + "," + std-ch.  
  run enable_UI.

  resetScreen().

  {lib/get-column-width.i &col="'Name'"  &var=dColumnWidth} 
 
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  if not this-procedure:persistent then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionStart C-Win 
PROCEDURE actionStart PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if cbRequirement:screen-value = "" or cbRequirement:screen-value = ?
   then
    do:
      message "Please select the requirement."
            view-as alert-box info buttons ok.
      return.
    end.
    
  if flQual:screen-value = "" or flQual:screen-value = ?
   then
    do:
      message "Please select the Qualification."
            view-as alert-box info buttons ok.
      return.      
    end.
  
  define buffer tSelectedRecords  for tSelectedRecords.
  define buffer tcmpfulfillments  for tcmpfulfillments.

  empty temp-table tSelectedRecords.
  
  std-lo = false.

  for each tcmpfulfillments:
    if tcmpfulfillments.lSelect 
     then
      do:
        std-lo = true.
        create tSelectedRecords.
        buffer-copy tcmpfulfillments to tSelectedRecords.
        assign
            tSelectedRecords.entityID = tcmpfulfillments.roleID
            .         
      end.
  end.
  
  if not std-lo 
   then
    do:
      message "Please select at least one organization for requirement fulfillment."
            view-as alert-box info buttons ok.
      return.
    end.

  /* Fetch Qualification Review Period from datasrv */
  publish "getQualReviewPeriod" (input {&Qualification},
                                 input flQual:input-value,
                                 output std-in,
                                 output std-lo,
                                 output std-ch).

  if not std-lo
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
    
  /* Dialog to fulfill first party requirement of person or organization */
  run dialogfulfillments.w (input ipcentity,           
                            input cbrequirement:input-value,
                            input flqual:input-value,
                            input cExpire,
                            input iExpireMonth,
                            input iExpireDay,
                            input iExpireYear,
                            input iExpireDays,
                            input std-in, /* Review Period */
                            input-output table tSelectedRecords,
                            output std-lo).

  /* Fulfillment Failed */
  if not std-lo  
   then
    return.

  for each tSelectedRecords:
    for each cmpfulfillments where cmpfulfillments.entityID = tSelectedRecords.fulfillByID
                                /*and cmpfulfillments.PersonID = tSelectedRecords.PersonID:*/ :
      buffer-copy tSelectedRecords except tSelectedRecords.role tSelectedRecords.roleName to cmpfulfillments.
    end.
    
    /* Update the entity specific screens which are currently open */ 
    publish "fulfillmentmodified" (tSelectedRecords.fulfillByID).
    publish "fulfillmentmodifiedrole" .
    
  end.
  
  browse brwAffentity:refresh().      
   
  /* Procedure to filter data accroding to the applied filters */
  run filterData in this-procedure.
     
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE affiliationModified C-Win 
PROCEDURE affiliationModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiAgentID as integer.

  for each tcmpfulfillments where tcmpfulfillments.entityID = string(ipiAgentID) :
    std-lo = true.
    leave.
  end.
  if not std-lo
   then
    return.

  apply 'choose' to btGet in frame {&frame-name}.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "close":u to this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayfulfillments C-Win 
PROCEDURE displayfulfillments :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  open query brwAffentity preselect each tcmpfulfillments .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState flfulfill flQual cbRole cbRequirement cbExpired 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE cbState brwAffentity bStart bCancel RECT-63 RECT-65 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:IP for filtering data on the condition on combo-box i.e cbexpired      
------------------------------------------------------------------------------*/
  close query brwAffentity.
  empty temp-table tcmpfulfillments.

  if cbExpired:screen-value in frame {&frame-name} = "ALL" 
   then
    for each cmpfulfillments:
      create tcmpfulfillments.
      buffer-copy cmpfulfillments to tcmpfulfillments.
    end.
  else if cbExpired:screen-value = "Yes" 
   then
    for each cmpfulfillments 
      where cmpfulfillments.qualificationID  <> 0
        and date(cmpfulfillments.expirationDate) < today:
      create tcmpfulfillments.
      buffer-copy cmpfulfillments to tcmpfulfillments.
    end.
  else if cbExpired:screen-value = "No" 
   then
    for each cmpfulfillments 
      where cmpfulfillments.qualificationID         <> 0
        and ( (date(cmpfulfillments.expirationDate) >= today) or (cmpfulfillments.expirationDate = ?) ) :
      create tcmpfulfillments.
      buffer-copy cmpfulfillments to tcmpfulfillments.
    end.
  else
   for each cmpfulfillments where cmpfulfillments.qualificationID = 0 : 
        create tcmpfulfillments.
        buffer-copy cmpfulfillments to tcmpfulfillments.
   end.
  
  run displayfulFillments in this-procedure.

  setStatusCount(query brwAffentity:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FirstPartyfulfillmentModified C-Win 
PROCEDURE FirstPartyfulfillmentModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.
  define input parameter ipcEntityID as character   no-undo.
 
  if can-find (first tcmpfulfillments where tcmpfulfillments.entityID = ipcEntityID) 
   then
    do:
      /* Making server call to get the data from server */
      run server/getfulfillments.p ( input 0,                                             /* ReqID */
                                     input cbRole:input-value,                            /* entity */
                                     input "ALL",                                         /* Entity ID */
                                     input cbState:input-value in frame {&frame-name},    /* StateID */
                                     input flfulfill:input-value,                         /* fulfilled by */  
                                     input cbrequirement:input-value,                     /* statreqqualId */
                                     input {&organization},                          
                                     input {&CompanyCode},                                /* ActionType */
                                     output cExpire,
                                     output iExpireMonth,
                                     output iExpireDay,
                                     output iExpireYear,
                                     output iExpireDays,
                                     output table qualification,
                                     output table fulfillment,
                                     output table tcmpfulfillments,
                                     output std-lo,    
                                     output std-ch).
     
      run displayfulfillments in this-procedure.
      
      setStatusRecords(query brwAffentity:num-results).
    end.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer cmpfulfillments for cmpfulfillments.
  
  empty temp-table qualification.
  empty temp-table fulfillment.
  empty temp-table ttcmpfulfillments.
  
  do with frame {&frame-name}:
  end.
    
  /* Making server call to get the data from server */
  run server/getfulfillments.p ( input 0,                                             /* RequirementID */             
                                 input cbRole:screen-value,                           /* Entity (org or person role) */     
                                 input "ALL",                                         /* EntityId (org or person roleid) */         
                                 input cbState:input-value in frame {&frame-name},    /* StateId */          
                                 input flfulfill:input-value,                         /* FulfilledBy Organization/Person */       
                                 input cbRequirement:input-value,                     /* StateReqQualId */       
                                 input {&Organization},                               /* Roletype organization or person */
                                 input {&CompanyCode},                                /* Action Type (firstParty/thirdParty) */
                                 output cExpire,                                      /* Expire */     
                                 output iExpireMonth,                                 /* Expire in months */
                                 output iExpireDay,                                   /* Expire in date */
                                 output iExpireYear,                                  /* Expire in Year */
                                 output iExpireDays,                                  /* Expire in days */
                                 output table qualification,                          /* Not Required */
                                 output table fulfillment,                            /* Not Required */
                                 output table ttcmpfulfillments ,                         
                                 output std-lo,                                      
                                 output std-ch).
                                 
                                 
                                 

  if not std-lo  /* if not then return */
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
  empty temp-table cmpfulfillments.

   for each ttcmpfulfillments:
     create cmpfulfillments.
     buffer-copy ttcmpfulfillments except ttcmpfulfillments.fulfillbyID to cmpfulfillments.
     cmpfulfillments.fulfillByID = ttcmpfulfillments.entityId. 
   end.
   
  /* Filter data based on the filter section */
  run filterData in this-procedure.

  if query brwAffentity:num-results <> 0
   then
    cbexpired:sensitive  = true.
  else
   cbexpired:sensitive  = false.
    
  setStatusRecords(query brwAffentity:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRequirementCombo C-Win 
PROCEDURE getRequirementCombo PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  std-ch = "".
  /* Creating requirement list and removing duplicate requirements.
     refid = "" means, exclude entity specific requirements.  */
  for each statereqqual where (statereqqual.refId = "" or statereqqual.refId = {&all}) and statereqqual.active :
    if lookup(statereqqual.description,std-ch) = 0 
     then
      std-ch = std-ch + "," + statereqqual.description + 
                        "," + string(statereqqual.statereqqualID).
  end.

  if std-ch <> "" 
   then
    do:
      assign 
          cbRequirement:list-item-pairs = trim(std-ch,",")
          cbRequirement:screen-value    = entry(2,cbRequirement:list-item-pairs)
          .
      apply 'value-changed' to cbRequirement.
    end.  
  else 
   cbRequirement:list-item-pairs = ?.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRequirementQualifications C-Win 
PROCEDURE getRequirementQualifications PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define buffer StateReqQual for StateReqQual.
 
  empty temp-table tstatereqqual.
  empty temp-table statereqqual.
  /* Calling procedure from comdatasrv */
  publish "getRequirementQuals" (input cbState:input-value in frame {&frame-name},   /* stateid */
                                 input cbRole:input-value,                           /* entity */
                                 input {&CompanyCode},                               /* authorizedby */
                                 input 0,                                            /* ReqId Not Needed Here */
                                 output table tstatereqqual ,
                                 output std-lo,
                                 output std-ch).
 
  if not std-lo  /* if not success then return */
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
  
  for each tstatereqqual where (tstatereqqual.appliesto = {&OrganizationCode} or tstatereqqual.appliesto = {&PersonCode}) :
    create statereqqual.
    buffer-copy tstatereqqual to statereqqual .
  end.
    
  if cbRequirement = "" 
   then
    assign
       flQual:screen-value    = ""
       flfulFill:screen-value = "" .
       
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRole C-Win 
PROCEDURE getRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.
  
  define variable capplies as character no-undo.
  define variable pList    as character no-undo.
    
  std-lo = false.
  std-ch = "".
   
  publish "getComplianceCodesListItems" ({&OrganizationRole},
                                         input ",",
                                         output pList,
                                         output std-lo,
                                         output std-ch).
       
  if not std-lo
   then
     do:
       message std-ch
         view-as alert-box error buttons ok.
       return.
     end.
 
  cbState:list-item-pairs = {&SelectState} + "," + std-ch.  
 
 
  if pList ne ""
   then
    cbRole:list-item-pairs = /*cbRole:list-item-pairs + "," +*/  pList.
 
  cbRole:screen-value    = entry(1,plist).
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inUseQualificationModified C-Win 
PROCEDURE inUseQualificationModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiQualificationID as integer.

  if not can-find(first tcmpfulfillments
                    where tcmpfulfillments.qualificationID = ipiQualificationID)
   then
    return.

  apply 'choose' to btGet in frame {&frame-name}.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE requirementsModified C-Win 
PROCEDURE requirementsModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcStateID  as character.
  define input parameter ipcEntity   as character.
  define input parameter ipcRole     as character.
  
  do with frame {&frame-name}:
  end.
    
  cRequirement = cbRequirement:screen-value. /* screen-value */ 
    
  if cbState:screen-value = ipcStateID and cbRole:screen-value = ipcRole
   then
    do:
      run getRequirementQualifications in this-procedure.  
      std-lo = true.
    end.
      
  if not std-lo
   then
    return.
    
 std-ch = "". 
 /* Creating requirement list and removing duplicate requirements. */
 for each statereqqual where (statereqqual.refId = "" or statereqqual.refId = {&all}) and statereqqual.active :
    if lookup(statereqqual.description,std-ch) = 0 
     then
      std-ch = std-ch + "," + statereqqual.description + 
                        "," + string(statereqqual.statereqqualID).
  end.  
 
  if std-ch <> ""
   then
    do:
      assign
          cbRequirement:list-item-pairs = trim(std-ch,",").
      if cRequirement = ? or cRequirement = ""  /* when requirement added is first */
       then
        cbRequirement:screen-value    = entry(2,cbRequirement:list-item-pairs).   
    end.
  else 
   cbRequirement:list-item-pairs = ?.
   
  /* to set exisiting value to the screen  */
  if cRequirement ne "" and lookup(cRequirement,cbRequirement:list-item-pairs) > 0 
   then
    cbRequirement:screen-value = cRequirement. 
  else                    
   do:
     close query brwAffentity.
     empty temp-table tcmpfulfillments.
     resetScreen().
   end. 
 
  run setQualification in this-procedure.
  run setWidgetsState in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setQualification C-Win 
PROCEDURE setQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  find first statereqqual where statereqqual.stateReqQualID = cbRequirement:input-value  no-error.
   if not available statereqqual
    then
     return.
        
  if cbRequirement:screen-value ne ""
   then
    assign
        flQual:screen-value    = statereqqual.qualification
        flfulFill:screen-value = (if statereqqual.appliesto = {&OrganizationCode} then {&organization} else {&person} ).
        .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetsState C-Win 
PROCEDURE setWidgetsState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if cbrequirement:list-item-pairs ne "" and cbrequirement:list-item-pairs ne ?
   then
    assign 
         cbRequirement:sensitive = true
         btGet:sensitive         = true
         .
   else
    assign
        cbRequirement:sensitive = false
        btGet:sensitive         = false
        .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable deButtonGap as decimal no-undo.
 
  do with frame {&frame-name}:
  end.
 
  deButtonGap = bCancel:col - (bStart:col + bStart:width).
 
  assign
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 41.50
      bStart:row                                = {&browse-name}:row + {&browse-name}:height-chars + 0.4
      bStart:col                                = {&browse-name}:width-chars / 2 - 20
      bCancel:row                               = {&browse-name}:row + {&browse-name}:height-chars + 0.4
      bCancel:col                               = bStart:col + bStart:width + deButtonGap
      .
 
  {lib/resize-column.i &col="'Name'" &var=dColumnWidth} 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  RETURN std-ch.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen returns logical private
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  assign
      cbExpired:screen-value  = "ALL"
      cbexpired:sensitive     = false
      bstart:sensitive        = false
      .

  setStatusMessage("").
  return true.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged returns logical private
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 return true.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

