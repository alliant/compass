&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wattorneys.w

  Description:Attorney Maintainance

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Shubham

  Created: 12.04.2019

------------------------------------------------------------------------*/
create widget-pool.

{tt/syscode.i}
{tt/syscode.i &tableAlias="tSysCode"}
{tt/syscode.i &tableAlias="ttSysCode"}

define variable cCodeType      as character  no-undo.
define variable cCode          as character  no-undo.
define variable opcCode        as character  no-undo.
define variable cType          as character  no-undo.
define variable dColumnWidth   as decimal    no-undo.

/* track status change to set status in filter*/
define variable cStatusDateTime  as logical   no-undo. 

{lib/std-def.i}
{lib/com-def.i}

{lib/get-column.i}
{lib/winshowscrollbars.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES syscode

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData syscode.code syscode.type syscode.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData preselect each syscode
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} preselect each syscode.
&Scoped-define TABLES-IN-QUERY-brwData syscode
&Scoped-define FIRST-TABLE-IN-QUERY-brwData syscode


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bdelete bEdit bNew RECT-61 bRefresh RECT-62 ~
cbType brwData bExport 
&Scoped-Define DISPLAYED-OBJECTS cbType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete nature of affiliation".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify nature of affiliation".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New nature of affiliation".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)" 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37.4 BY 2.14.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 41 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      syscode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      syscode.code    format "9999" column-label "Code"         width 8
syscode.type          format "x(10)"       label "Type"         width 8
syscode.description   format "x(60)"       label "Description"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 78 BY 11 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bdelete AT ROW 1.62 COL 71.6 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 64.6 WIDGET-ID 8 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 57.6 WIDGET-ID 6 NO-TAB-STOP 
     bRefresh AT ROW 1.62 COL 43.6 WIDGET-ID 4 NO-TAB-STOP 
     cbType AT ROW 2 COL 7.6 COLON-ALIGNED WIDGET-ID 48
     brwData AT ROW 3.95 COL 2 WIDGET-ID 200
     bExport AT ROW 1.62 COL 50.6 WIDGET-ID 2 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1 COL 43.2 WIDGET-ID 52
     "Filter" VIEW-AS TEXT
          SIZE 5.2 BY .62 AT ROW 1.14 COL 2.8 WIDGET-ID 56
     RECT-61 AT ROW 1.43 COL 42.6 WIDGET-ID 50
     RECT-62 AT ROW 1.43 COL 2 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 160.8 BY 17.29 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Nature of Affiliation"
         HEIGHT             = 14.19
         WIDTH              = 79.8
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData cbType fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} preselect each syscode.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Nature of Affiliation */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Nature of Affiliation */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Nature of Affiliation */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run actionDelete in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run actionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbType C-Win
ON VALUE-CHANGED OF cbType IN FRAME fMain /* Type */
do:
  run filterData in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i }

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").
bnew    :load-image("images/add.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/update-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run getData in this-procedure.
  
  run enable_UI.
  
  {lib/get-column-width.i &col="'description'" &var=dColumnWidth}

  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  {&window-name}:visible = true.

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lChoice as logical no-undo.
  
  if not available syscode 
   then
    return.
  
  cCodeType = syscode.codetype.
  cCode     = syscode.code.
 
  lchoice = false.

  message "Highlighted Record will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete Nature of Affiliation" update lChoice.
 
  if not lChoice 
   then
    return.
    
  publish "deleteSyscode" (input  cCodeType,
                           input  cCode,
                           output std-lo,
                           output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box info buttons ok.
      return.
    end.

  publish "GetSysCodes" ({&NatureOfAffiliation},output table tsyscode).
  
  run filterData in this-procedure.  
  setStatusRecords(query brwData:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable iCount as integer no-undo.
  
  do iCount = 1 to brwdata:num-iterations:
    if brwdata:is-row-selected(iCount) 
     then  
      leave.
  end.
  
  if not available syscode 
   then
    return.
  
  empty temp-table ttSysCode.
  
  create ttSysCode.
  buffer-copy syscode to ttSysCode no-error.
    
  run dialogsyscode.w (input  {&ActionEdit},
                       input  table ttsyscode,
                       input  cType,
                       output opcCode,
                       output std-lo).
                       
  if not std-lo 
   then
    return.
 
  publish "GetSysCodes" ({&NatureOfAffiliation},output table tsyscode).
  
  run filterData in this-procedure.
  
  for first ttSysCode:
    for first syscode 
      where syscode.codeType = ttSysCode.codeType
        and syscode.code     = ttSysCode.code:
      std-ro = rowid(syscode).
    end.
  end.     
  brwdata:set-repositioned-row(iCount,"ALWAYS").
  reposition brwdata to rowid std-ro.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  def var icount as int no-undo.
  empty temp-table ttSysCode.
              
  run dialogsyscode.w (input  {&ActionNew},
                       input  table ttsyscode,
                       input  cType,
                       output opcCode,
                       output std-lo).
  
  if not std-lo  
   then
    return.
   
  /* Update the data from the data model. */
  publish "GetSysCodes" ({&NatureOfAffiliation},output table tsyscode).
  
  run filterData in this-procedure.

  find first syscode 
    where syscode.codeType = {&NatureOfAffiliation}
      and syscode.code     = opcCode
    no-error.

  if available syscode and (cbType:input-value = syscode.type or cbType:input-value = "ALL" )
   then
    std-ro = rowid(syscode).
  else 
   std-ro = ?.
                                                     
  if std-ro <> ? 
   then
    do:
      do iCount = 1 TO brwdata:num-iterations:
        if brwdata:is-row-selected(iCount)
         then
          leave.
      end.   
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata TO rowid std-ro.
      brwdata:get-repositioned-row().

      message "New SysCode is created."
        view-as alert-box info buttons ok.       
    end.  
  setStatusRecords(query brwData:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "close":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bdelete bEdit bNew RECT-61 bRefresh RECT-62 cbType brwData bExport 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htableHandle as handle no-undo.
  
  empty temp-table tsyscode.
 
  for each syscode where syscode.type = (if cbType:input-value in frame {&frame-name} = "ALL"
                                          then
                                           syscode.type 
                                          else 
                                           cbType:input-value):
    create tsyscode.
    buffer-copy syscode to tsyscode.
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  htableHandle = temp-table tsyscode:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "tSyscode",
                          "for each tSyscode ",
                          "codetype,code,type,description,comment",
                          "Code Type,Code,Type,Description,Comments",
                          std-ch,
                          "Syscode.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  close query brwData.
  
  empty temp-table syscode.
  
  for each tSysCode:
    if not tSysCode.type = (if cbType:input-value = {&ALL} then tSysCode.type else cbType:input-value) 
     then 
      next.
     
    create syscode.
    buffer-copy tSysCode to syscode.
  end.
   
  open query brwData preselect each syscode.
  
  /* Display the record count after applying the filter. */
  setStatusCount(query brwData:num-results).
  run setButtons in this-procedure. 
        
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not can-find (first syscode)
   then
    publish "GetSysCodes" ({&NatureOfAffiliation},output table tsyscode).
  
  for each tsyscode break by tsyscode.type:
    if first-of(tsyscode.type) 
     then
      cType = if cType = "" then tsyscode.type else cType + "," + tsyscode.type.
  end.
  
  cbType:list-items = {&ALL} + "," + cType.
  cbType:screen-value = {&ALL}.

  run filterData in this-procedure.
  
  setStatusRecords(query brwData:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "sysCodeChanged" ({&NatureOfAffiliation}).
  
  publish "GetSysCodes" ({&NatureOfAffiliation},output table tsyscode).
        
  run filterData in this-procedure.
  setStatusRecords(query brwData:num-results).
end  procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtons C-Win 
PROCEDURE setButtons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if query brwData:num-results > 0 
   then
    assign 
        bExport       :sensitive  = true
        bEdit         :sensitive  = true
        bDelete       :sensitive  = true
        .
   else
    assign
        bExport        :sensitive = false
        bEdit          :sensitive = false
        bDelete        :sensitive = false
        .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}                                                          
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixel
      /* fMain Components */
      brwData:width-pixels              = frame fmain:width-pixels - 12
      
      brwData:height-pixels             = frame fmain:height-pixels - brwData:y - 4
      .
  {lib/resize-column.i &col="'description'" &var=dColumnWidth}
  run ShowScrollBars(browse brwData:handle, no, yes).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 cStatusDateTime = false.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

