&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------

  File: qualificationdatasrv.p

  Description: 
  This procedure is the "data model" for Qualifications Appointments and Reviews (COM).
  It is responsible for saving, loading, and managing all requests for data
  elements.  It is the data master.  There are no visual components within
  this procedure.  It is not responsible for making sure there is no data
  loss.  In other words, a controller is responsible to make sure the
  appropriate methods are called on this data model to save its state.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created:          
  
  Modification:
  Date            Name        Description
  05/10/2022      Shefali     Task # 93684 Modified to update qualification record when qualification
                              review record is added or modified. 
          
------------------------------------------------------------------------*/

define input parameter ipiQualificationID as integer.

{lib/std-def.i}
{lib/com-def.i}

/* Temp-table definitions  */
{tt/review.i}
{tt/review.i &tableAlias=tReview}

{tt/qualification.i}
{tt/qualification.i &tableAlias=tQualification}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15.67
         WIDTH              = 72.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-deleteQualificationReview) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteQualificationReview Procedure 
PROCEDURE deleteQualificationReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Parameters Definition */
 define input  parameter ipiReviewID as integer   no-undo.
 define output parameter oplSuccess  as logical   no-undo.
 define output parameter opcMsg      as character no-undo.
 
 define buffer review for review.

 /* Client Server Call */
 run server/deletequalificationreview.p (input  ipiReviewID,
                                         output oplSuccess,
                                         output opcMsg).

 if not oplSuccess 
  then
   return.
 
 find first review where review.reviewID = ipiReviewID no-error.
 if not available review
  then
   do: 
      assign 
        opcMsg     = "Internal Error of missing data."  
        oplSuccess = false
        . 
      return.
   end.
   
 /* Publidh the event, so that the parent qualificaton of this review 
    can refresh itself. For example agentdetail screen, firstparty 
    fulfillments utility, qualification screen. */ 
 publish "qualificationReviewModified"(input ipiQualificationID).
 
 delete review.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getQualification) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getQualification Procedure 
PROCEDURE getQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
 define output parameter table for tQualification.
 define output parameter oplSuccess         as logical   no-undo.
 define output parameter opcMsg             as character no-undo.

 define buffer qualification  for qualification.
 define buffer tQualification for tQualification.
 
 empty temp-table tQualification.

 /* client Server Call */
  if not can-find(first qualification)
  then
   do:
      run refreshQualifications in this-procedure (output oplSuccess,           
                                                   output opcMsg).              


      if not oplSuccess
       then
        return.
   end.

  for first qualification:
    create tQualification.
    buffer-copy qualification to tQualification.
  end.

  oplSuccess = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getQualificationReviews) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getQualificationReviews Procedure 
PROCEDURE getQualificationReviews :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Parameters Definition */
 define input  parameter ipiReviewID        as integer   no-undo.
 define output parameter table              for tReview.
 define output parameter oplSuccess         as logical   no-undo.
 define output parameter opcMsg             as character no-undo.

 define buffer review  for review.
 define buffer tReview for tReview.

 empty temp-table tReview.

 if (ipiReviewID <> 0 and not can-find(first review where review.reviewID = ipiReviewID)) or
    (ipiReviewID = 0  and not can-find(first review where review.qualificationID = ipiQualificationID))
  then
   do:
      run refreshQualificationReviews in this-procedure (input  ipiReviewID,
                                                         output oplSuccess,
                                                         output opcMsg).

      if not oplSuccess
       then
        return.
   end.
 
 for each review where review.reviewID = ipiReviewID or review.qualificationID = ipiQualificationID:
   create tReview.
   buffer-copy review to tReview.
 end.
                      
 assign oplSuccess = true.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-modifyQualificationReview) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyQualificationReview Procedure 
PROCEDURE modifyQualificationReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Parameters Definition */
 define input parameter table for tReview.
 define output parameter oplSuccess   as logical   no-undo.
 define output parameter opcMsg       as character no-undo.

 define buffer review  for review.
 define buffer tReview for tReview.

 /* check for availability of record before making the server call. */
 find first tReview no-error.
 if not available tReview
  then
   return.

 /* Client Server Call */
 run server/modifyqualificationreview.p (input table tReview,
                                         output oplSuccess,
                                         output opcMsg).

 if not oplSuccess 
  then
   return.

 find first review where review.reviewID = tReview.reviewID no-error.
 if not available review
  then
   do: 
      assign 
        opcMsg     = "Internal Error of missing data."  
        oplSuccess = false
        . 
      empty temp-table tReview.
      return.
   end.

 buffer-copy tReview to review.
 
 find first Qualification where qualification.qualificationID = review.qualificationID no-error.
 if available Qualification
  then
   assign
       Qualification.stat           = review.stat   
       Qualification.effectiveDate  = review.effdate
       Qualification.expirationDate = review.expdate
       no-error.
 
 empty temp-table tReview.
 
 /* Publidh the event, so that the parent qualificaton of this review 
    can refresh itself. For example agentdetail screen, firstparty 
    fulfillments utility, qualification screen. */ 

 publish "qualificationReviewModified"(input ipiQualificationID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-myInstance) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE myInstance Procedure 
PROCEDURE myInstance :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter opiInstanceID as int.
 opiInstanceID = ipiQualificationID.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newQualificationReview) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newQualificationReview Procedure 
PROCEDURE newQualificationReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Parameters Definition */
 define input parameter table for tReview.
 define output parameter opiReviewID  as integer   no-undo.
 define output parameter oplSuccess   as logical   no-undo.
 define output parameter opcMsg       as character no-undo.

 define buffer review  for review.
 define buffer tReview for tReview.

 /* check for availability of record before making the server call. */
 find first tReview no-error.
 if not available tReview
  then
   return.

 /* Client Server Call */
 run server/newqualificationreview.p (input table tReview,
                                      output opiReviewID,
                                      output oplSuccess,
                                      output opcMsg).

 if not oplSuccess 
  then
   return.

 create review.
 buffer-copy tReview except tReview.reviewID to review.
 assign review.reviewID = opiReviewID.
 
 find first Qualification where qualification.qualificationID = review.qualificationID no-error.
  if available Qualification
   then
    assign
        Qualification.stat           = review.stat   
        Qualification.effectiveDate  = review.effdate
        Qualification.expirationDate = review.expdate
        no-error.

 empty temp-table tReview.

 /* Publidh the event, so that the parent qualificaton of this review 
    can refresh itself. For example agentdetail screen, firstparty 
    fulfillments utility, qualification screen. */ 

 publish "qualificationReviewModified"(input ipiQualificationID).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshQualificationReviews) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshQualificationReviews Procedure 
PROCEDURE refreshQualificationReviews :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Parameters Definition */
 define input  parameter ipiReviewID        as integer   no-undo.
 define output parameter oplSuccess         as logical   no-undo.
 define output parameter opcMsg             as character no-undo.

 define buffer tReview for tReview.
 define buffer review  for review.

 empty temp-table tReview.

 /* Client Server Call */
 run server/getqualificationreviews.p (input ipiReviewID,
                                       input ipiQualificationID,
                                       output table tReview,
                                       output oplSuccess,
                                       output opcMsg).
 
 if not oplSuccess 
  then
   return.

  empty temp-table review.
  for each tReview by treview.reviewID:
    create review.
    buffer-copy tReview to review.
  end.

  empty temp-table tReview.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshQualifications) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshQualifications Procedure 
PROCEDURE refreshQualifications :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
 define output parameter oplSuccess  as logical    no-undo.
 define output parameter opcMsg      as character  no-undo.

  define buffer tQualification for tQualification.
  define buffer qualification  for qualification.
  
  empty temp-table tQualification.
 
 /* client Server Call */
 run server/getqualifications.p (input "",
                                 input "",
                                 input "",
                                 input ipiQualificationID,
                                 output table tQualification,
                                 output oplSuccess,
                                 output opcMsg).

 if not oplSuccess 
  then
   return.

  for each tQualification :
    find first qualification where qualification.qualificationID = tQualification.qualificationID no-error.
     if not available qualification
      then
       create qualification.
     buffer-copy tQualification to qualification.
  end.

  empty temp-table tQualification.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

