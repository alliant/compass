&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

  /* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                           */
define input  parameter iphPersonDataSrv  as handle     no-undo.
define input  parameter ipiPersonRoleID   as integer    no-undo.

create widget-pool.

{lib/std-def.i}
{lib/com-def.i}   
{lib/get-column.i}
{lib/brw-multi-def.i}
                                                                                                                 
/* Temp-table Definitions ---*/

{tt/role.i }
{tt/role.i &tablealias = trole}
{tt/reqfulfill.i}
{tt/reqfulfill.i &tablealias = treqfulfill}  
{tt/staterequirement.i}
{tt/statereqqual.i}
 
/* Local Variable Definitions ---                                       */
define variable hNote          as handle     no-undo.
define variable dColumnWidth   as decimal    no-undo.
define variable dColumnWidthQ  as decimal    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwFulfillment

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES reqfulfill

/* Definitions for BROWSE brwFulfillment                                */
&Scoped-define FIELDS-IN-QUERY-brwFulfillment reqfulfill.reqMet reqfulfill.reqdesc "Requirement" reqfulfill.qualification "Qualification" reqfulfill.qualificationNumber "Number" getQualificationDesc(reqfulfill.stat) @ reqfulfill.stat "Status" reqfulfill.entityname "Fulfilled By" reqfulfill.effectiveDate "Effective" reqfulfill.expirationDate "Expiration"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFulfillment   
&Scoped-define SELF-NAME brwFulfillment
&Scoped-define QUERY-STRING-brwFulfillment for each reqfulfill
&Scoped-define OPEN-QUERY-brwFulfillment open query {&SELF-NAME} for each reqfulfill.
&Scoped-define TABLES-IN-QUERY-brwFulfillment reqfulfill
&Scoped-define FIRST-TABLE-IN-QUERY-brwFulfillment reqfulfill


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwFulfillment}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bNotes Bdelete bfulfill bRefresh bViewReq ~
brwFulfillment bComstatus RECT-65 RECT-68 
&Scoped-Define DISPLAYED-OBJECTS fpersonID attName frole fstate tAttorneyID ~
tAttorneyName fStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-win 
FUNCTION getQualificationDesc return character
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getreqfor C-win 
FUNCTION getreqfor returns character ( cauth as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatus C-win 
FUNCTION getStatus returns character ( cAuth as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON Bdelete  NO-FOCUS
     LABEL "delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete fulfillment".

DEFINE BUTTON bfulfill  NO-FOCUS
     LABEL "Fulfill" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fulfillment".

DEFINE BUTTON bNotes  NO-FOCUS
     LABEL "Notes" 
     SIZE 7.2 BY 1.71 TOOLTIP "Fulfillment notes".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bViewReq  NO-FOCUS
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View requirement".

DEFINE VARIABLE attName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Person Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 54.2 BY 1 TOOLTIP "Name" NO-UNDO.

DEFINE VARIABLE fpersonID AS CHARACTER FORMAT "x(8)":U 
     LABEL "Person ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.2 BY 1 TOOLTIP "ID" NO-UNDO.

DEFINE VARIABLE frole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.2 BY 1 TOOLTIP "Role" NO-UNDO.

DEFINE VARIABLE fstate AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 TOOLTIP "State" NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 TOOLTIP "Status" NO-UNDO.

DEFINE VARIABLE tAttorneyID AS CHARACTER FORMAT "x(8)":U 
     LABEL "Attorney ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16.2 BY 1 TOOLTIP "Attorney ID" NO-UNDO.

DEFINE VARIABLE tAttorneyName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Attorney Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 54.2 BY 1 TOOLTIP "Attorney name" NO-UNDO.

DEFINE IMAGE bComstatus
     SIZE 5 BY .95.

DEFINE RECTANGLE RECT-65
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.2 BY 2.24.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.4 BY 2.24.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwFulfillment FOR 
      reqfulfill SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwFulfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFulfillment C-win _FREEFORM
  QUERY brwFulfillment DISPLAY
      reqfulfill.reqMet                                          column-label "Met"         width 6    view-as toggle-box
reqfulfill.reqdesc                                           label        "Requirement"   format "x(80)"      width 23
reqfulfill.qualification                                     label        "Qualification" format "x(80)"      width 30
reqfulfill.qualificationNumber                               label        "Number"        format "x(15)"      width 12
getQualificationDesc(reqfulfill.stat) @ reqfulfill.stat      label        "Status"        format "x(16)"      width 15
reqfulfill.entityname                                        label        "Fulfilled By"  format "x(80)"      width 24
reqfulfill.effectiveDate                                     label        "Effective"     format "99/99/9999" width 12
reqfulfill.expirationDate                                    label        "Expiration"    format "99/99/9999" width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 144 BY 9.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Requirements and Fulfillments" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN TOOLTIP "Requirements and Fulfillments".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bNotes AT ROW 1.62 COL 33.8 WIDGET-ID 334 NO-TAB-STOP 
     fpersonID AT ROW 3.86 COL 13 COLON-ALIGNED WIDGET-ID 98
     Bdelete AT ROW 1.62 COL 17.8 WIDGET-ID 140 NO-TAB-STOP 
     attName AT ROW 3.81 COL 59.2 COLON-ALIGNED WIDGET-ID 100
     bfulfill AT ROW 1.62 COL 10.8 WIDGET-ID 228 NO-TAB-STOP 
     frole AT ROW 5 COL 13 COLON-ALIGNED WIDGET-ID 160
     bRefresh AT ROW 1.62 COL 3.8 WIDGET-ID 322 NO-TAB-STOP 
     fstate AT ROW 5 COL 59.2 COLON-ALIGNED WIDGET-ID 332
     tAttorneyID AT ROW 6.14 COL 13 COLON-ALIGNED WIDGET-ID 340
     bViewReq AT ROW 1.62 COL 24.8 WIDGET-ID 328 NO-TAB-STOP 
     tAttorneyName AT ROW 6.14 COL 59.2 COLON-ALIGNED WIDGET-ID 342
     fStatus AT ROW 6.14 COL 131 COLON-ALIGNED WIDGET-ID 174
     brwFulfillment AT ROW 7.33 COL 3 WIDGET-ID 200
     "Notes" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1 COL 33.8 WIDGET-ID 338
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1 COL 4.2 WIDGET-ID 52
     bComstatus AT ROW 5.05 COL 31.8 WIDGET-ID 326
     RECT-65 AT ROW 1.38 COL 3 WIDGET-ID 330
     RECT-68 AT ROW 1.38 COL 32.8 WIDGET-ID 336
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS TOP-ONLY NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 147.8 BY 16.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-win ASSIGN
         HIDDEN             = YES
         TITLE              = "Fulfillments"
         HEIGHT             = 16.52
         WIDTH              = 147.8
         MAX-HEIGHT         = 37.57
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 37.57
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwFulfillment fStatus DEFAULT-FRAME */
/* SETTINGS FOR FILL-IN attName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwFulfillment:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwFulfillment:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwFulfillment:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN fpersonID IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN frole IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fstate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fStatus IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAttorneyID IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAttorneyName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-win)
THEN C-win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFulfillment
/* Query rebuild information for BROWSE brwFulfillment
     _START_FREEFORM
open query {&SELF-NAME} for each reqfulfill.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFulfillment */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-win C-win
ON END-ERROR OF C-win /* Fulfillments */
or endkey of {&window-name} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent then return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-win C-win
ON WINDOW-CLOSE OF C-win /* Fulfillments */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-win C-win
ON WINDOW-RESIZED OF C-win /* Fulfillments */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Bdelete C-win
ON CHOOSE OF Bdelete IN FRAME DEFAULT-FRAME /* delete */
do:
  /* Deletes the fulfillment of a requirement */
  run deleteFulfillment in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bfulfill
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bfulfill C-win
ON CHOOSE OF bfulfill IN FRAME DEFAULT-FRAME /* Fulfill */
do:
  run fulfillRequirement in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNotes C-win
ON CHOOSE OF bNotes IN FRAME DEFAULT-FRAME /* Notes */
do:
  run openNotes in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
do:
  run refreshData(yes).   /* NeedFulFillment */ 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFulfillment
&Scoped-define SELF-NAME brwFulfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment C-win
ON DEFAULT-ACTION OF brwFulfillment IN FRAME DEFAULT-FRAME /* Requirements and Fulfillments */
do:
  if bfulfill:sensitive
   then
    apply "choose" to bfulfill.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment C-win
ON ROW-DISPLAY OF brwFulfillment IN FRAME DEFAULT-FRAME /* Requirements and Fulfillments */
do:
  {lib/brw-rowdisplay.i} 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment C-win
ON START-SEARCH OF brwFulfillment IN FRAME DEFAULT-FRAME /* Requirements and Fulfillments */
do:
  {lib/brw-startSearch-multi.i &browse-name = brwFulfillment}

  /* Apply trigger so that buttons are properly enabled/disbaled depending on the currently selected row in the browse. */
  apply 'value-changed' to browse brwFulfillment.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFulfillment C-win
ON VALUE-CHANGED OF brwFulfillment IN FRAME DEFAULT-FRAME /* Requirements and Fulfillments */
do:
  run setButtons in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bViewReq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bViewReq C-win
ON CHOOSE OF bViewReq IN FRAME DEFAULT-FRAME /* View */
do:
  run viewRequirement in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-win 


/* ***************************  Main Block  *************************** */
 {lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Subscribe to the events, so that fulfillment can refresh 
   in case fulfillments are modified by the affiliated person. */
subscribe to "fulfillmentModifiedRole"  anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
 run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.

bRefresh    :load-image("images/Refresh.bmp").
bRefresh    :load-image-insensitive("images/Refresh-i.bmp").
bdelete     :load-image("images/disconnect.bmp").
bdelete     :load-image-insensitive("images/disconnect-i.bmp").
bfulfill    :load-image("images/connect.bmp").
bfulfill    :load-image-insensitive("images/connect-i.bmp").
bViewreq    :load-image("images/magnifier.bmp").
bViewreq    :load-image-insensitive("images/magnifier-i.bmp").
bNotes      :load-image("images/note.bmp").
bNotes      :load-image-insensitive("images/note-i.bmp").

{lib/win-main.i}
{lib/win-status.i}

run setComplianceStatus in this-procedure({&Gcode}).
run getdata  in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.  
  
  {lib/get-column-width.i &col="'reqdesc'"           &var=dColumnWidth} 
  {lib/get-column-width.i &col="'qualification'"     &var=dColumnWidthq}
  
  run displayrole        in this-procedure. 
  run displayfulfillment in this-procedure.
  run setbuttons         in this-procedure.
  run showWindow         in this-procedure. 
  
  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hNote)
   then
    run closeWindow in hNote.
  hNote = ?.
      
  apply "close":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteFulfillment C-win 
PROCEDURE deleteFulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable ireqID          as integer     no-undo.
  define variable ifulfillmentID  as integer     no-undo.
  define variable istatereqqualID as integer     no-undo.
 
  define buffer treqfulfill for treqfulfill.

  if not available reqfulfill
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.                                                                                     
    end.
    
  message "Highlighted fullfilment will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no title "Delete Qualification Requirement" update std-lo.
  
  if not std-lo 
   then
    return.
 
  assign 
      ifulfillmentID  = reqfulfill.qualReqID
      ireqID          = reqfulfill.requirementId     
      istatereqqualID = reqfulfill.statereqqualID
      .

  /* Server call to delete the fulfillment record and Update datamodal. */
  run "deleteFulfillment" in iphPersonDataSrv (input  ifulfillmentID,                               
                                               output std-lo,
                                               output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
  
    /* Get the updated Person role compliance status from data model */
  run getComplianceStatus in iphPersonDataSrv (input ipiPersonRoleID ,output std-ch).
  
  /* Set the organization role complaince status on the organization role screen */
  run setComplianceStatus in this-procedure (input std-ch).
      
  /* Get the updated records from datamodel */
  run getRequirementsFulfillment in iphPersonDataSrv (output table treqfulfill).
  
  /* Clearing browser */
  close query brwFulfillment.
  
  /* Update local copy of data. */
  empty temp-table reqfulfill.

  for each treqfulfill where treqfulfill.personRoleID = ipiPersonRoleID:
    create reqfulfill.
    buffer-copy treqfulfill to reqfulfill.
  end.

  run displayFulfillment in this-procedure. 

  find first reqfulfill where reqfulfill.requirementId   = ireqID
                          and reqfulfill.statereqqualID  = istatereqqualID no-error.
  if not available reqfulfill 
   then
    find first reqfulfill where reqfulfill.requirementID = ireqid no-error.
    
  if not available reqfulfill 
   then
    do:
      message "Internal Error of missing data."
         view-as alert-box error buttons ok.
      return.
    end.
    
  std-ro = rowid(reqfulfill).
  
  reposition brwFulfillment to rowid std-ro no-error. 
 
  run setButtons in this-procedure.       
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-win)
  THEN DELETE WIDGET C-win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayfulfillment C-win 
PROCEDURE displayfulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first reqfulfill no-error.
  if not available reqfulfill
   then 
    return.
  else
   open query brwFulfillment for each reqfulfill.   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayRole C-win 
PROCEDURE displayRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.

  find first role no-error.
  if not available role
   then
    return.
    
  assign
      fStatus:visible       = false 
      tAttorneyID:visible   = false  
      tAttorneyName:visible = false 
      .
      
  if role.role = {&attorney}
   then
    assign
        fStatus       :visible = true 
        tAttorneyID   :visible = true
        tAttorneyName :visible = true
        tAttorneyID   :screen-value  = role.entityID
        tAttorneyName :screen-value  = role.entityname
        .
  
  assign
      fPersonID:screen-value   = string(role.roleId )
      attName:screen-value  = role.name
      fStatus:screen-value  = getStatus(role.stat)
      fstate:screen-value   = role.state
      frole:screen-value    = role.role      
      .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fpersonID attName frole fstate tAttorneyID tAttorneyName fStatus 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-win.
  ENABLE bNotes Bdelete bfulfill bRefresh bViewReq brwFulfillment bComstatus 
         RECT-65 RECT-68 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fulfillmentModifiedRole C-win 
PROCEDURE fulfillmentModifiedRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:   if fulfillment are modified then qualification will also get modified 
           fullfilment can be modified by affiliated person
           so agent need to refresh if any of them update fullfilments      
------------------------------------------------------------------------------*/
  run refreshData(yes).  /* NeedFulFillment */    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fulfillRequirement C-win 
PROCEDURE fulfillRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  define variable iRequirementId as integer no-undo.
  
  define buffer treqfulfill for treqfulfill.

  do with frame {&frame-name}:
  end.

  if not available reqfulfill 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.

  if reqfulfill.authorizedBy = {&ThirdParty} 
   then
    do:
      iRequirementId = reqfulfill.requirementId.
      
      run dialogthirdpartyfulfillment.w (input iphPersonDataSrv,                                                        /* Handle of PersonDataSrv */
                                         input reqfulfill.requirementId,                                                /* Requirement Id */
                                         input reqfulfill.reqdesc,                                                      /* Requirement Description */
                                         input role.role, /* attorneyID or personID */  
                                         input if role.role = {&attorney} then role.entityID else string(role.roleID), /* PersonID */  
                                         input {&Person},                                                               /*Role Type*/
                                         input role.state,                                                              /*State ID*/
                                         input getreqfor(reqfulfill.appliesTo),                                         /*fulfilled by  reqfor*/
                                         input reqfulfill.statereqqualId,                                               /*StatereqqualID*/
                                         output std-lo).                                                                /* Fulfillment changed or not */
      
      if not std-lo 
       then
        return.      
    end.
    
  else if reqfulfill.authorizedBy = {&FirstParty} 
   then
    do:
    
      iRequirementId = reqfulfill.requirementId.

      empty temp-table treqFulfill.
           
      create treqFulfill.
      buffer-copy reqFulfill to treqFulfill.
      
      assign
          treqFulfill.qualification = reqFulfill.requirementQual
          treqFulfill.entity        = role.role /*reqFulfill.appliesTo */
          treqfulfill.stateId       = reqFulfill.stateID
          .                                                              
    
 
      run dialogfirstpartyfulfillment.w (input iphPersonDataSrv,      /* Handle of PersonDataSrv */
                                         input {&Person},             /* Entity */
                                         input table treqFulfill,         
                                         output std-lo).              /* Fulfillment changed or not */
      
      if not std-lo 
       then
        return.      
    end.
    
   
  /* Get the updated Person compliance status from data model */
  run getComplianceStatus in iphPersonDataSrv ( input ipiPersonRoleID, output std-ch ).

  /* Set the Person complaince status on the Person screen */
  run setComplianceStatus in this-procedure(input std-ch) .
  
  /* Get the updated records for fulfillment from datamodel. */
  run getRequirementsFulfillment in iphPersonDataSrv (output table treqfulfill).

  /* Clearing browser */
  close query brwFulfillment.

  /* Update local copy of data. */
  empty temp-table reqfulfill.
  
  for each treqfulfill:
    create reqfulfill.
    buffer-copy treqfulfill to reqfulfill.
  end.
  
  run displayFulfillment in this-procedure. 
        
  find first reqfulfill where reqfulfill.requirementId = iRequirementId no-error.
  if not available reqfulfill 
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.

  std-ro = rowid(reqfulfill).
  reposition brwFulfillment to rowid std-ro no-error.
  
  run setButtons  in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getdata C-win 
PROCEDURE getdata :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Clearing browser */
  close query brwFulfillment. 
  
  define buffer trole for trole.
  define buffer treqfulfill for treqfulfill.
  
 /*Remove Person role data*/      
  empty temp-table role.
  empty temp-table reqfulfill.
  
 /*Get Person role information in data modal from server*/
 run getPersonRoleFulfillments in iphPersondatasrv(input  ipiPersonRoleID,
                                 output table trole,
                                 output table treqfulfill,
                                 output std-lo,
                                 output std-ch).
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
    end.
    
  /*update the local copy of data*/
  for each trole:
    create role.
    buffer-copy trole to role.
  end.

  for each treqfulfill :
    create reqfulfill.
    buffer-copy treqfulfill to reqfulfill.
  end.
 
  std-ch = "".
 /* Get the updated organization compliance status from data model */
 run getComplianceStatus in iphPersonDataSrv ( input ipiPersonRoleID, output std-ch ).

  /* Set the Org complaince status on the Org screen */
  run setComplianceStatus in this-procedure(input std-ch) .
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openNotes C-win 
PROCEDURE openNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.
  
  if valid-handle(hNote)
   then
    run showWindow in hNote.
  else
    run wentityNotes.w persistent set hNote (input fstate:screen-value,
                                             input {&PersonCode},
                                             input fPersonID:screen-value,
                                             input attName:screen-value,
                                             input frole:screen-value,
                                             input 0 ). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter iplneedPersonrole as logical. 

/* Refresh all the data on the screen */
  run getData            in this-procedure.
  run displayRole        in this-procedure.
  run displayFulfillment in this-procedure.
  run setButtons         in this-procedure. 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setbuttons C-win 
PROCEDURE setbuttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.

  if query brwFulfillment:num-results = 0  
   then
    assign 
        Bdelete:sensitive  = false
        bfulfill:sensitive = false
        bViewReq:sensitive = false
        bNotes:sensitive   = false
        .
  else
   do:
     if not available reqfulfill
      then
       do:
         return.
       end.

     if reqfulfill.qualificationID = 0  
      then
       Bdelete:sensitive = false.
       
     else
      Bdelete:sensitive = true.

     assign 
         bViewReq:sensitive = true
         bfulfill:sensitive = true
         bNotes:sensitive   = true
         .
     if reqfulfill.authorizedBy = {&FirstParty}
      then
       bfulfill:tooltip = "First party requirement fulfillment".
     else
      bfulfill:tooltip = "Third party requirement fulfillment".
   end.

              
  if fstatus:screen-value ne {&Active} 
   then
    do: 
      bfulfill:sensitive = false.    
      bdelete:sensitive = false.
      
      assign
          bComStatus:hidden  = true
          bComstatus:visible = false
          .
    end.

  run showWindow in this-procedure.  
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setComplianceStatus C-win 
PROCEDURE setComplianceStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cstat as character no-undo. 
  do with frame {&FRAME-NAME}:
  end.
 
  case cstat:
     when {&Gcode}
      then
       do:
         bComStatus:load-image ("images/s-smiley-green.png").
         bComStatus:tooltip = {&InCompliance}.
       end.

     when {&Ycode}
      then
       do:
         bComStatus:load-image ("images/s-smiley-yellow.png").
         bComStatus:tooltip = {&NeedsAttention}.
       end.
     /* in case of Red or Blank ComStatus */
     otherwise
       do:
         bComStatus:load-image ("images/s-smiley-red.png").
         bComStatus:tooltip = {&OutOfCompliance}.
       end.
  end case.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewRequirement C-win 
PROCEDURE viewRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
 
  if not available reqfulfill
   then
    do:
      message "Internal Error of missing data."
          view-as alert-box error buttons ok.
      return.
    end.
    
  publish "getRequirement" (input reqfulfill.requirementId,
                            output table StateRequirement,
                            output std-lo,
                            output std-ch).  
  if not std-lo 
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.

  run dialogviewrequirement.w(input table StateRequirement).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      brwFulfillment:width-pixels               = frame {&frame-name}:width-pixels - 20
      brwFulfillment:height-pixels              = frame {&frame-name}:height-pixels - brwFulfillment:y - 9
      .
      
  {lib/resize-column.i &col="'reqdesc,qualification'" &var=dColumnWidth &var=dColumnWidthQ}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-win 
FUNCTION getQualificationDesc return character
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getreqfor C-win 
FUNCTION getreqfor returns character ( cauth as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cauth = {&OrganizationCode}
   then
    return {&Organization}.   /* Function return value. */
  else if cauth = {&personcode}
   then
    return {&Person}.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatus C-win 
FUNCTION getStatus returns character ( cAuth as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cAuth = "yes" 
   then
    return "Active".   
  else if cAuth = "no"
   then
    return "cancelled".
 
  else if cAuth = {&ActiveStat} 
   then
    return "Active".   
  
  else if cAuth = {&ClosedStat} 
    then
     return "Closed".
  else if cAuth = {&InActiveStat} 
    then
     return "InActiveStat".
  else if cAuth = {&ProspectStat} 
   then
    return "Prospect".  
  
  else if cAuth = {&WithDrawnStat}
   then
    return "WithDrawn".
  else
    return "cancelled". 
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

