&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------

  File: dialogcompliancecode.w

  Description: UI for compliance codes

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma

  Created: 04.25.2018
  @Modified
  Date         Name            Description
  04/11/19     Rahul           Add mandatory flag '*' in code and discription fillIn
  11/11/2021   Shefali         Task #88683 Bug Fix user cannot able to save blank description
  03/14/2022   Shefali         Task #86699 Remove the code combo-box.
------------------------------------------------------------------------*/

{tt/comcode.i &tableAlias=tComCode} 

define input  parameter table        for tComCode.
define input  parameter ipccomCode   as character.
define output parameter opicomCodeID as integer.
define output parameter oplSuccess   as logical.

{lib/std-def.i}
{lib/com-def.i}

define variable iComID        as integer     no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable chCode        as character no-undo.
define variable chDescription as character no-undo.
define variable inValiddays   as integer   no-undo.
define variable chComments    as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tDescription validdays tComments Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tDescription validdays tComments ~
fMarkMandatory2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 80 BY 4.76 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 80 BY 1 NO-UNDO.

DEFINE VARIABLE validdays AS INTEGER FORMAT ">>>9":U INITIAL 0 
     LABEL "Default Review In" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 6.6 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tDescription AT ROW 1.29 COL 20.8 COLON-ALIGNED WIDGET-ID 4
     validdays AT ROW 2.48 COL 20.8 COLON-ALIGNED WIDGET-ID 154
     tComments AT ROW 3.67 COL 22.8 NO-LABEL WIDGET-ID 44
     Btn_OK AT ROW 8.71 COL 41 WIDGET-ID 152
     Btn_Cancel AT ROW 8.71 COL 57.6 WIDGET-ID 150
     fMarkMandatory2 AT ROW 1.52 COL 101 COLON-ALIGNED NO-LABEL WIDGET-ID 54 NO-TAB-STOP 
     "Comments:" VIEW-AS TEXT
          SIZE 11 BY 1.19 AT ROW 3.38 COL 8.6 WIDGET-ID 158
     "Days" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 2.71 COL 30.4 WIDGET-ID 308
     SPACE(71.99) SKIP(6.66)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Compliance Code"
         DEFAULT-BUTTON Btn_OK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX fMain
/* Query rebuild information for DIALOG-BOX fMain
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Compliance Code */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fMain
ON CHOOSE OF Btn_Cancel IN FRAME fMain /* Cancel */
do:
  oplSuccess = false.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fMain
ON CHOOSE OF Btn_OK IN FRAME fMain /* Save */
do:
  /* If temp-table is available, then edit, else add */
  if iComID ne 0
   then
    run editComplianceCode.
  else
   run addComplianceCode. 

  /* Set in one of the above two procedures. */
  if not oplSuccess
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tComments fMain
ON VALUE-CHANGED OF tComments IN FRAME fMain
do:
  run enableDisableSave in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDescription fMain
ON VALUE-CHANGED OF tDescription IN FRAME fMain /* Description */
do:
  run enableDisableSave in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME validdays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL validdays fMain
ON VALUE-CHANGED OF validdays IN FRAME fMain /* Default Review In */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  run enable_UI.

  run displaydata in this-procedure.

  assign
      chDescription = tDescription:input-value
      inValiddays   = validdays:input-value   
      chComments    = tComments:input-value
      .  
  run setWidgetState in this-procedure.
   
  run enableDisableSave in this-procedure. 
  
  for first tComCode:
    iComID = comCodeID.
  end.
  
  wait-for go of frame {&frame-name}.
  
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addComplianceCode fMain 
PROCEDURE addComplianceCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table tComCode. 

  create tComCode.

  assign  
      tComCode.code         = ipccomCode
      tComCode.description  = replace(tDescription:input-value,",","")
      tComCode.comments     = tComments:input-value
      tComCode.reviewPeriod = validdays:input-value
      .
  
  publish "newComplianceCode" (input table tComCode,
                               output opiComCodeID,
                               output oplSuccess,
                               output std-ch).
                                              
  if oplSuccess = false 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData fMain 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first tComCode no-error.
  
  if available tComCode 
   then
    assign 
        frame fMain:title             = "Modify " + (if ipccomcode = "OrganizationRole" then "Organization Role"
                                                else if ipccomcode = "PersonRole"       then "Person Role" 
                                                else ipccomcode)
        tDescription:screen-value     = tComCode.description
        validdays:screen-value        = string(tComCode.reviewPeriod)
        tComments:screen-value        = tComCode.comments
        .
   else
    assign 
        frame fMain:title             = "New " + (if ipccomcode = "OrganizationRole" then "Organization Role"
                                             else if ipccomcode = "PersonRole"       then "Person Role" 
                                             else ipccomcode)
        Btn_OK:label                  = "Create"
        .
  assign       
      fMarkMandatory2:hidden        = false
      fMarkMandatory2:screen-value  = {&mandatory}
      .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE editComplianceCode fMain 
PROCEDURE editComplianceCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table tComCode.

  create tComCode.

  assign 
      tComCode.code         = ipccomCode
      tComCode.description  = replace(tDescription:input-value,",","")
      tComCode.comments     = tComments:input-value
      tComCode.reviewPeriod = integer(validdays:input-value)
      tComCode.comCodeID    = iComID
      .
  
  publish "modifyComplianceCode" (input table tComCode,
                                  output oplSuccess,
                                  output std-ch).
  if not oplSuccess 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      empty temp-table tComCode. 
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave fMain 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  /* Enable/Disable the save button
     Edit: enable only if any value is changed. 
     New:  enable as soon as mandatory fields are filled. */
  if can-find(first tComCode)
   then
    btn_ok:sensitive = not((tDescription:input-value = "" or tDescription:input-value = ? or tDescription:input-value = chDescription)  and
                           inValiddays               = validdays:input-value    and
                           (tDescription:input-value = "" or tDescription:input-value  = ? or chComments = tComments:input-value))   no-error.
  else
   btn_ok:sensitive = not (tDescription:input-value = "" or tDescription:input-value = ?) no-error.  

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDescription validdays tComments fMarkMandatory2 
      WITH FRAME fMain.
  ENABLE tDescription validdays tComments Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState fMain 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  /* validdays is available for user inputs only for "Qualification" */
  validdays:sensitive = (ipccomCode = {&qualification}).
  
  /* Set the default to 0, if not sensitive. */
  validdays:screen-value = string(if not(validdays:sensitive) 
                            then 0
                           else validdays:input-value).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

