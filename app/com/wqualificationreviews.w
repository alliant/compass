&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wqualificationreviews.w 

  Description:User Interface for review of Qualifications. 

  Input Parameters:

  Output Parameters:
    
  Author: Rahul 

  Created:04.11.2018 

  Modified: 10-22-18    Rahul          Modified to include win-main.i.
            04-02-2020  Archana Gupta  Modified code according to new organization structure.
            05-04-2022  Shefali        Task #93684 Modified to refresh qualification data.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* Parameters Definitions ---                                           */
define input parameter iphEntityData      as handle  no-undo.
define input parameter ipiQualificationID as integer no-undo.

{tt/qualification.i}
{tt/review.i}
{tt/review.i &tablealias=tReview}

{lib/std-def.i}

/* Local Variable Definitions ---                                       */
define variable cStateID         as character no-undo.
define variable cQualStatus      as character no-undo.
define variable daEffectiveDate  as datetime  no-undo.
define variable daExpirationDate as datetime  no-undo.
define variable hQual            as handle    no-undo.
define variable iLatestReviewID  as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwQualReview

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES review

/* Definitions for BROWSE brwQualReview                                 */
&Scoped-define FIELDS-IN-QUERY-brwQualReview review.reviewDate review.username review.issue review.nextreviewDueDate getQualificationDesc(review.stat) @ review.stat review.effdate review.expdate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualReview   
&Scoped-define SELF-NAME brwQualReview
&Scoped-define QUERY-STRING-brwQualReview preselect each review by review.reviewID desc
&Scoped-define OPEN-QUERY-brwQualReview open query {&SELF-NAME} preselect each review by review.reviewID desc.
&Scoped-define TABLES-IN-QUERY-brwQualReview review
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualReview review


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwQualReview}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-62 brwQualReview edComments bNew ~
bRefresh 
&Scoped-Define DISPLAYED-OBJECTS flQualNum flQualification edComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character
  (  cStat as character  )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete review".

DEFINE BUTTON bEditreview  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify review".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New review".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE edComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 135 BY 2 NO-UNDO.

DEFINE VARIABLE flQualification AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 50 BY 1 NO-UNDO.

DEFINE VARIABLE flQualNum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Number" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36.6 BY 2.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQualReview FOR 
      review SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQualReview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualReview C-Win _FREEFORM
  QUERY brwQualReview DISPLAY
      review.reviewDate               label "Review"                   format "99/99/99" width 14
review.username                 label "Review By"                format "x(20)"    width 30
review.issue             column-label "Issues"                                     width 9 view-as toggle-box            
review.nextreviewDueDate column-label "Next Review!Due"          format "99/99/99" width 16
getQualificationDesc(review.stat)
  @ review.stat          column-label "Qualification!Status"     format "x(20)"    width 20
review.effdate           column-label "Qualification!Effective"  format "99/99/99" width 16
review.expdate           column-label "Qualification!Expiration" format "99/99/99" width 14
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS NO-TAB-STOP SIZE 135 BY 13.86
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bEditreview AT ROW 1.67 COL 23.6 WIDGET-ID 298 NO-TAB-STOP 
     flQualNum AT ROW 2 COL 47.8 COLON-ALIGNED WIDGET-ID 310 NO-TAB-STOP 
     flQualification AT ROW 2 COL 85 COLON-ALIGNED WIDGET-ID 302 NO-TAB-STOP 
     brwQualReview AT ROW 4.05 COL 2 WIDGET-ID 500
     bExport AT ROW 1.67 COL 9.6 WIDGET-ID 2 NO-TAB-STOP 
     edComments AT ROW 18.95 COL 2 NO-LABEL WIDGET-ID 312 NO-TAB-STOP 
     bNew AT ROW 1.67 COL 16.6 WIDGET-ID 204 NO-TAB-STOP 
     bRefresh AT ROW 1.67 COL 2.6 WIDGET-ID 158 NO-TAB-STOP 
     bDelete AT ROW 1.67 COL 30.6 WIDGET-ID 206 NO-TAB-STOP 
     "Comments" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 18.24 COL 2.4 WIDGET-ID 314
     RECT-62 AT ROW 1.52 COL 2 WIDGET-ID 308
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 139 BY 20.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Qualification Reviews"
         HEIGHT             = 19.95
         WIDTH              = 137
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         SHOW-IN-TASKBAR    = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQualReview flQualification DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bDelete IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bDelete:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

/* SETTINGS FOR BUTTON bEditreview IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bNew:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "A".

ASSIGN 
       brwQualReview:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwQualReview:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       edComments:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flQualification IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flQualification:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

/* SETTINGS FOR FILL-IN flQualNum IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       flQualNum:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualReview
/* Query rebuild information for BROWSE brwQualReview
     _START_FREEFORM
open query {&SELF-NAME} preselect each review by review.reviewID desc.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualReview */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Qualification Reviews */
or endkey of {&WINDOW-NAME} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  if we did not, the
     application would exit. */
  if this-procedure:persistent 
   then 
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Qualification Reviews */
do: 
  /* This event will close the window and terminate the procedure. */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Qualification Reviews */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME DEFAULT-FRAME /* Delete */
do:
  run deleteReview in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEditreview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEditreview C-Win
ON CHOOSE OF bEditreview IN FRAME DEFAULT-FRAME /* Edit */
do: 
  run modifyReview in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME DEFAULT-FRAME /* New */
do: 
  run addReview in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME DEFAULT-FRAME /* Refresh */
do:
  run refreshData in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualReview
&Scoped-define SELF-NAME brwQualReview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualReview C-Win
ON DEFAULT-ACTION OF brwQualReview IN FRAME DEFAULT-FRAME
do:
  if (available review and (iLatestReviewID = review.reviewID)) 
   then
    run modifyReview in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualReview C-Win
ON ROW-DISPLAY OF brwQualReview IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualReview C-Win
ON START-SEARCH OF brwQualReview IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualReview C-Win
ON VALUE-CHANGED OF brwQualReview IN FRAME DEFAULT-FRAME
do:
  /* display data on UI for the review which is currrently selected in browse*/
  run displayData    in this-procedure no-error.
  /* set widget state on UI based on the currently selecetd review record */
  run setWidgetState in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
run qualificationdatasrv.p persistent set hQual (input ipiQualificationID).

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

{lib/win-status.i}
{lib/brw-main.i}

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
  run disable_UI.

/* Best default for GUI applications is...                              */
pause 0 before-hide.
/* The qualification edit dialog cause the change in review record, so
   review screen needs to be updated. Therefore, review screen has subscribed to
   qualification update event. */
subscribe to "qualificationModified"                anywhere.
subscribe to "qualificationReviewModifiedByUtility" anywhere. 
subscribe to "closeWindow"                          anywhere.

/* Get the qualification information from the qualification data model
   and updated the local variable with the new data */
run getQualification in this-procedure no-error.
/* Getting all reviews from data model for corressponding qualification id passed
   and fill the browse with data */
run getReviews       in this-procedure no-error.
run getEntityName    in iphEntityData (output std-ch).

C-Win:title = "Qualification Reviews - " + std-ch.

bRefresh   :load-image ("images/refresh.bmp").
bRefresh   :load-image-insensitive("images/refresh-i.bmp").
bExport    :load-image("images/excel.bmp").
bExport    :load-image-insensitive("images/excel-i.bmp").
bNew       :load-image("images/add.bmp").
bNew       :load-image-insensitive("images/add-i.bmp").
bEditreview:load-image("images/update.bmp").
bEditreview:load-image-insensitive("images/update-i.bmp").
bdelete    :load-image("images/delete.bmp").
bdelete    :load-image-insensitive("images/delete-i.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERRor and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  find first Qualification no-error.
  if available Qualification 
   then
    assign 
        cStateID                     = Qualification.stateID          
        flQualNum:screen-value       = Qualification.qualificationNumber
        flQualification:screen-value = Qualification.qualification no-error. 

  /* set the status in taskbar */
  setStatusRecords(query brwQualReview:num-results).  

  /* display data on UI for the review which is currrently selected in browse*/
  run displayData    in this-procedure no-error.
  
  /* set widget state on Ui based on the currently selecetd review */
  run setWidgetState in this-procedure no-error.  
  {lib/win-main.i}
  
  /* This procedure restores the window and move it to top */
  run showWindow     in this-procedure. 

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addReview C-Win 
PROCEDURE addReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iReviewId as integer no-undo.    
  
  empty temp-table tReview.
  
  run dialogqualificationreview.w (input hQual,
                                   input cStateID,
                                   input ipiQualificationID,
                                   input cQualStatus,
                                   input daEffectiveDate,
                                   input daExpirationDate,
                                   input table tReview,
                                   output iReviewId,
                                   output std-lo).   
  if not std-lo 
   then
    return.  

  /* getting all reviews from data model for corressponding qualification id
     passed and fill the browse with data */
  run getReviews       in this-procedure no-error.
  
  /* get the qualification information from the qualification data model and
     updated the local variable with the new data */
  run getQualification in this-procedure no-error.   

  for first review 
    where review.reviewID = iReviewId:
    std-ro = rowid(review).   
  end.
     
  reposition brwQualReview to rowid std-ro no-error.
  
  /* display data on UI for the review which is currrently selected in browse */
  run displayData    in this-procedure no-error.
  
  /* set widget state on Ui based on the currently selecetd review */
  run setWidgetState in this-procedure no-error.
  
  /* set the status in taskbar */
  setStatusCount(query brwQualReview:num-results). 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(hQual) 
   then
    delete procedure hQual.

  hQual = ?.
  
  apply "CLOSE":U to this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteReview C-Win 
PROCEDURE deleteReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iReviewId as integer no-undo.  

  message "Highlighted Qualification Review will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete Qualification Review"
    update std-lo as logical.
  
  if not std-lo 
   then
    return.
  
  if available review 
   then
    do:
      iReviewId = review.reviewID.
      run deleteQualificationReview in hQual (input review.reviewID,
                                              output std-lo,
                                              output std-ch).
      if not std-lo 
       then 
        do:
          message std-ch
              view-as alert-box error buttons ok.
          return.
        end.   
    end.
   
  /* getting all reviews from data model for corressponding qualification
     id passed and fill the browse with data */
  run getReviews       in this-procedure no-error.
  
  /* get the qualification information from the qualification data
     model and updated the local variable with the new data */
  run getQualification in this-procedure no-error.               

  /* display data on UI for the review which is currrently selected in browse*/
  run displayData      in this-procedure no-error.
  
  /* set widget state on Ui based on the currently selecetd review */
  run setWidgetState   in this-procedure no-error.

  /* set the status in taskbar */
  setStatusCount(query brwQualReview:num-results). 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.  

  edComments:screen-value = if available review then review.Comments else "".

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flQualNum flQualification edComments 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-62 brwQualReview edComments bNew bRefresh 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable      as  handle no-undo.
  define buffer   review      for review.
  define buffer   tReview     for tReview.  

  if query brwQualReview:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  empty temp-table tReview.

  for each review:
    create tReview.
    buffer-copy review to tReview.
    tReview.stat = getQualificationDesc(review.stat).
  end.
  
  publish "GetReportDir" (output std-ch).

  htable = temp-table tReview:handle.
  run util/exporttable.p (table-handle htable,
                          "tReview",
                          "for each tReview by tReview.reviewID desc",
                          "reviewID,reviewDate,stat,issue,reviewBy,nextreviewDueDate,numValidDays,effdate,expdate,Comments,enforcement",
                          "Review ID,Review Date,Status,Issue,Review By,Next Review Due Date,Number of Valid Days,Effective Date,Expiry Date,Comments,Enforcement",
                          std-ch,
                          "review-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in). 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getQualification C-Win 
PROCEDURE getQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* get the qualification information from the qualification data model. */
  run getQualification in hQual  (output table Qualification,
                                  output std-lo,
                                  output std-ch). 
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  find first Qualification no-error.
  if available Qualification 
   then
    assign
        cQualStatus      = Qualification.stat
        daEffectiveDate  = Qualification.effectiveDate
        daExpirationDate = Qualification.expirationDate
        .       
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getReviews C-Win 
PROCEDURE getReviews :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwQualReview.
  empty temp-table review.

  define buffer tReview for review.

  /* getting all reviews from data model for corressponding qualification id passed */
  run getQualificationReviews in hQual (input 0,
                                        output table review,
                                        output std-lo,
                                        output std-ch).
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  open query brwQualReview preselect each review by review.reviewID descending.        

  for last tReview by tReview.reviewID descending:  /* track the latest one review */
    iLatestReviewID = tReview.reviewID.     
  end.


end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyReview C-Win 
PROCEDURE modifyReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iReviewId as integer no-undo.

  empty temp-table tReview.

  if available review 
   then
    buffer-copy review to tReview.
  
  run dialogqualificationreview.w(input hQual,
                                  input cStateID,
                                  input ipiQualificationID,
                                  input cQualStatus,
                                  input daEffectiveDate,
                                  input daExpirationDate,
                                  input table tReview,
                                  output iReviewId,
                                  output std-lo).
  if not std-lo 
   then
    return.
  /* getting all reviews from data model for corressponding qualification
     id passed and fill the browse with data */
  run getReviews       in this-procedure no-error.
  
  /* get the qualification information from the qualification data model
     and updated the local variable with the new data */
  run getQualification in this-procedure no-error. 

  for first review 
    where review.reviewID = iLatestReviewID:
    std-ro = rowid(review).                 
  end.
     
  reposition brwQualReview to rowid std-ro no-error.
  
  /* display data on UI for the review which is currrently selected in browse*/
  run displayData in this-procedure no-error. 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE qualificationModified C-Win 
PROCEDURE qualificationModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcLocalQualID as integer no-undo.

  define variable iCount                as integer no-undo.
  define variable iReviewID             as integer no-undo.

  do with frame {&frame-name}:
  end.
  
  if ipiQualificationID ne ipcLocalQualID 
   then
    return.
  
  /* Keeping the row number of the currently selected record in the 
     browse. */
  find current review no-error.
  if available review 
   then
    iReviewID = review.reviewID.

  do iCount = 1 to brwQualReview:num-iterations:
    if brwQualReview:is-row-selected(iCount)
     then
      leave.
  end.

  run refreshQualificationReviews in hQual  (input 0,     /* Review ID */
                                             output std-lo,
                                             output std-ch) no-error.
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.


  run getReviews in this-procedure no-error.
  run getQualifications in iphEntityData (output table Qualification) .

  find first Qualification where Qualification.qualificationID = review.qualificationID no-error.
  if available Qualification
   then
    assign
        cStateID                     = Qualification.stateID
        flQualNum:screen-value       = Qualification.qualificationNumber
        flQualification:screen-value = Qualification.qualification 
        cQualStatus                  = Qualification.stat
        daEffectiveDate              = Qualification.effectiveDate
        daExpirationDate             = Qualification.expirationDate
        no-error. 

  /* set the status in taskbar */
  setStatusRecords(query brwQualReview:num-results).  

  /* display data on UI for the review which is currrently selected in browse*/
  run displayData in this-procedure no-error.
  
  /* After data refresh, reposition to the same previously selected record
     in the browse. */
  for first review 
    where review.reviewID = iReviewID:
    std-ro = rowid(review).
  end.

  brwQualReview:set-repositioned-row(iCount,"ALWAYS").
  reposition brwQualReview to rowid std-ro no-error.

  /* set widget state on Ui based on the currently selecetd review */
  run setWidgetState in this-procedure no-error. 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE qualificationReviewModifiedByUtility C-Win 
PROCEDURE qualificationReviewModifiedByUtility :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter ipcLocalQualID as integer no-undo.

  define variable iCount                as integer no-undo.
  define variable iReviewID             as integer no-undo.

  if not can-find(first qualification where qualification.qualificationID = ipcLocalQualID) 
   then
    return.

  do with frame {&frame-name}:
  end.
  
  /* Keeping the row number of the currently selected record in the 
     browse. */
  find current review no-error.
  if available review 
   then
    iReviewID = review.reviewID.

  do iCount = 1 to brwQualReview:num-iterations:
    if brwQualReview:is-row-selected(iCount)
     then
      leave.
  end.

  run getQualification in this-procedure no-error.
  run getReviews       in this-procedure no-error.

  /* set the status in taskbar */
  setStatusRecords(query brwQualReview:num-results).  

  /* display data on UI for the review which is currrently selected in browse*/
  run displayData in this-procedure no-error.
  
  /* After data refresh, reposition to the same previously selected record
     in the browse. */
  for first review 
    where review.reviewID = iReviewID:
    std-ro = rowid(review).
  end.

  brwQualReview:set-repositioned-row(iCount,"ALWAYS").
  reposition brwQualReview to rowid std-ro no-error.

  /* set widget state on Ui based on the currently selecetd review */
  run setWidgetState in this-procedure no-error. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* refresh all reviews in data model from server for corressponding qualification id passed */
  run refreshQualificationReviews in hQual  (input 0,     /* Review ID */
                                             output std-lo,
                                             output std-ch) no-error.
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
         
  /* getting all reviews from data model for corressponding qualification
     id passed and fill the browse with data */
  run getReviews     in this-procedure no-error.

  run setWidgetState in this-procedure no-error.
  /* set the status in taskbar */
  setStatusRecords(query brwQualReview:num-results). 
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  assign 
      bExport    :sensitive  = ((query brwQualReview:num-results) > 0)
      bEditreview:sensitive  = available review and (iLatestReviewID = review.reviewID)
      bDelete    :sensitive  = available review and (iLatestReviewID = review.reviewID).
      .
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
  run displayData    in this-procedure no-error.
  run setWidgetState in this-procedure no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 10
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 5
      .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-Win 
FUNCTION getQualificationDesc returns character
  (  cStat as character  ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).

  return std-ch.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

