&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogsyscode.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Gurvindar Singh

  Created:09/26/18
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{lib/com-def.i}

{tt/syscode.i}
{tt/syscode.i &tableAlias=ttsyscode}

/* Parameters Definitions ---                                           */
define input  parameter ipcAction     as  character no-undo.
define input  parameter table         for ttsyscode.
define input  parameter ipcType       as  character no-undo.
define output parameter opcCode       as  character no-undo.
define output parameter oplSuccess    as  logical   no-undo.


/* Local Variable Definitions ---                                       */
define variable cTrackType     as character no-undo.
define variable cTrackDesc     as character no-undo.
define variable cTrackComment  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbType fDesc eComment Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cbType fDesc eComment 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 18.8 BY 1 NO-UNDO.

DEFINE VARIABLE eComment AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 62.6 BY 2.81 NO-UNDO.

DEFINE VARIABLE fDesc AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 62.6 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbType AT ROW 1.48 COL 12.2 COLON-ALIGNED WIDGET-ID 40
     fDesc AT ROW 2.67 COL 12.2 COLON-ALIGNED WIDGET-ID 42
     eComment AT ROW 3.86 COL 14.2 NO-LABEL WIDGET-ID 26
     Btn_OK AT ROW 7.29 COL 24.8
     Btn_Cancel AT ROW 7.29 COL 41.4
     "Comment:" VIEW-AS TEXT
          SIZE 9.4 BY .62 AT ROW 4.81 COL 4.6 WIDGET-ID 28
     SPACE(64.99) SKIP(3.66)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System Code"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       eComment:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* System Code */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveSystemCode in this-procedure.
  if not oplSuccess 
   then
    return no-apply.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbType Dialog-Frame
ON VALUE-CHANGED OF cbType IN FRAME Dialog-Frame /* Type */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eComment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eComment Dialog-Frame
ON VALUE-CHANGED OF eComment IN FRAME Dialog-Frame
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDesc Dialog-Frame
ON VALUE-CHANGED OF fDesc IN FRAME Dialog-Frame /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&FRAME-NAME}:parent eq ?
 then 
  frame {&FRAME-NAME}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.

  run displayData in this-procedure.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  cbType:list-items = ipcType.
  cbType:screen-value = entry(1,ipcType).
  
  case ipcAction:
    when {&ActionNew}
     then
      assign 
          frame dialog-frame:title = "New - Nature of Affiliation"
          Btn_OK :label            = "Create" 
          Btn_OK :tooltip          = "Create"
          .
    when {&ActionEdit} 
     then
      do:
        find first ttsyscode no-error.
        if available ttsysCode 
         then            
           assign
             frame dialog-frame:title   = "Modify - Nature of Affiliation" 
             cbType:sensitive           = false
             Btn_OK :label              = "Save" 
             Btn_OK :tooltip            = "Save"  
             cbType:screen-value        = ttsyscode.type
             fdesc:screen-value         = ttsyscode.description            
             ecomment:screen-value      = ttsyscode.comments
             .            
      end.       

  end case.  

  assign
      cTrackType     = trim(cbType:input-value)
      cTrackDesc     = trim(fdesc:input-value)
      cTrackComment  = trim(ecomment:input-value)
      .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if ipcAction = {&ActionNew}
   then
    Btn_OK:sensitive = (cbType:input-value <> "" and  
                        cbType:input-value <> ?  and
                        fDesc:input-value  <> "")
                        .
  else                    
    Btn_OK:sensitive = not(cTrackType     = trim(cbType:input-value)   and    
                           cTrackDesc     = trim(fdesc:input-value)   and    
                           cTrackComment  = trim(ecomment:input-value)
                           ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbType fDesc eComment 
      WITH FRAME Dialog-Frame.
  ENABLE cbType fDesc eComment Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveSystemCode Dialog-Frame 
PROCEDURE saveSystemCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.   
  
  if not available ttsyscode 
   then
    create ttsyscode.
    
  assign
      ttsyscode.type          = cbType    :input-value
      ttsyscode.description   = fdesc     :input-value
      ttsyscode.comments      = ecomment  :input-value
      .

  case ipcAction:
    when {&ActionEdit} 
     then
      publish "modifySysCode" (input table ttsyscode,
                               output oplSuccess,
                               output std-ch).

    when {&ActionNew} 
     then
      publish "newSysCode" (input table ttsyscode,
                            output opcCode,
                            output oplSuccess,
                            output std-ch).

  end case.  

  if not oplSuccess
   then 
    message std-ch
      view-as alert-box info buttons ok.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

