&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------
  File: wCom.w
  Description: Main window for Com application
  Author: Rahul
  Created: 11.22.2017
  Modified: 05/02/2020  Archana Gupta  Modified code according to new organization structure
            03/16/2022  Shefali        Task #86699 - Attorney Firms can be searched from �Attorney� tab 
                                       in COM Main screen and modified to update linking of agent and org.
            04/26/2022  Shefali        Task #93684 Modified to fix attorney binaculer.
            08/16/2022  SA             Task #96812 Modified to add functionality for personagent.
            11/07/2022  S Chandu       Added Roles filter to Organization And people tab. 
            11/11/2022  S Chandu       Task #99797 Modified to Use Open Window.
            02/23/2023  S Chandu       Task #102719 Modifed refreshpersondetails call
            02/24/2023  SB             Task# 102649 Parameterize the Person Screen.
            04/04/2024  SB             Task# 111689 Modified to remove the reference of title and county
	        04/04/2024  SR             Task# 111689 Modified fields to contactPhone,contactEmail and contactMobile.
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* Temp-table definitions*/
{tt/importdata.i}

{tt/organization.i }
{tt/organization.i &tableAlias="tOrganization"}
{tt/organization.i &tableAlias="ttOrganization"}

{tt/openwindow.i}

{tt/agent.i}
{tt/agent.i &tableAlias="tAgent"}
{tt/agent.i &tableAlias="ttAgent"}

{tt/person.i}
{tt/person.i &tableAlias="tPerson"}
{tt/person.i &tableAlias="ttPerson"}
{tt/person.i &tablealias="tempPerson"}

{tt/attorney.i &tableAlias="tAttorney"}

/* Parameters Definitions ---                                           */
{src/adm2/widgetprto.i}

{lib/std-def.i}
{lib/com-def.i}
{lib/winlaunch.i}
{lib/winshowscrollbars.i}
{lib/get-column.i}
{lib/brw-multi-def.i}


define temp-table tErrorPerson like person
 fields tempError as character.    
  
/* Local Variable Definitions ---                                       */
define variable hOrganization as handle no-undo.
define variable hAgent        as handle no-undo.
define variable hPerson       as handle no-undo. 
define variable hAttorney     as handle no-undo.
define variable hpersondatasrv as handle no-undo.
define variable hOrganizationDataSrv as handle no-undo.
define variable cValuePair      as character no-undo.
define variable cStateId        as character  no-undo.
define variable cStatusDateTime as logical   no-undo.
define variable ipageNo         as integer no-undo.
define variable dOrganization   as decimal no-undo.
define variable dAgent          as decimal no-undo.
define variable dPeople         as decimal no-undo.
define variable dAttorney       as decimal no-undo.
define variable cOrgSearch      as character no-undo.
define variable cAgentSearch    as character no-undo.
define variable cPeopleSearch   as character no-undo.
define variable cAttorneySearch as character no-undo.
define variable cAgentStatList  as character no-undo.
define variable cAttorneyStatList  as character no-undo.
define variable cAgentStat     as character no-undo.
define variable cAttorneyStat  as character no-undo.
define variable cList          as character no-undo init "Active,A,InActive,I".
define variable cStat          as character no-undo.
define variable tBadSearchChars as char init "/?<>\:*|~"~~~'~`!#$%^~{~}[];,+=_()" no-undo. /* refrence from " buildsysindex.p " */
define variable cRoles          as character no-undo.
define variable cPersonRoles    as character no-undo.
define variable cOrgRole        as character no-undo.
define variable cPeopleRole     as character no-undo.

define temp-table ttAttorney like tAttorney
 fields entity     as character
 fields entityID   as character
 fields entityName as character.
define temp-table tempAttorney like ttAttorney.
define temp-table attorney like ttAttorney.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwDataAgent

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttAgent ttAttorney ttOrganization ttPerson

/* Definitions for BROWSE brwDataAgent                                  */
&Scoped-define FIELDS-IN-QUERY-brwDataAgent getComStat(ttagent.comseq) @ ttagent.comseq ttAgent.agentID getStatDesc(ttagent.stat) @ ttagent.stat ttagent.legalname ttagent.address ttagent.stateID ttagent.orgID ttagent.orgname ttagent.matchPercent   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDataAgent   
&Scoped-define SELF-NAME brwDataAgent
&Scoped-define QUERY-STRING-brwDataAgent preselect each ttAgent
&Scoped-define OPEN-QUERY-brwDataAgent open query {&SELF-NAME} preselect each ttAgent.
&Scoped-define TABLES-IN-QUERY-brwDataAgent ttAgent
&Scoped-define FIRST-TABLE-IN-QUERY-brwDataAgent ttAgent


/* Definitions for BROWSE brwDataAttorney                               */
&Scoped-define FIELDS-IN-QUERY-brwDataAttorney getComStat(ttattorney.Comseq) @ ttattorney.Comseq ttAttorney.attorneyID getStatDesc(ttattorney.stat) @ ttAttorney.stat ttAttorney.entityname ttAttorney.address ttAttorney.stateID ttAttorney.entityID ttAttorney.matchPercent   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDataAttorney   
&Scoped-define SELF-NAME brwDataAttorney
&Scoped-define QUERY-STRING-brwDataAttorney preselect each ttAttorney
&Scoped-define OPEN-QUERY-brwDataAttorney open query {&SELF-NAME} preselect each ttAttorney.
&Scoped-define TABLES-IN-QUERY-brwDataAttorney ttAttorney
&Scoped-define FIRST-TABLE-IN-QUERY-brwDataAttorney ttAttorney


/* Definitions for BROWSE brwDataOrg                                    */
&Scoped-define FIELDS-IN-QUERY-brwDataOrg ttOrganization.orgID ttOrganization.name ttOrganization.address ttOrganization.phone ttOrganization.website ttOrganization.roles ttorganization.matchPercent   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDataOrg   
&Scoped-define SELF-NAME brwDataOrg
&Scoped-define QUERY-STRING-brwDataOrg preselect each ttOrganization
&Scoped-define OPEN-QUERY-brwDataOrg open query {&SELF-NAME} preselect each ttOrganization.
&Scoped-define TABLES-IN-QUERY-brwDataOrg ttOrganization
&Scoped-define FIRST-TABLE-IN-QUERY-brwDataOrg ttOrganization


/* Definitions for BROWSE brwDataPeople                                 */
&Scoped-define FIELDS-IN-QUERY-brwDataPeople ttPerson.personID getStatDesc(string(ttperson.active)) @ ttPerson.active ttPerson.dispname ttPerson.address ttPerson.roles ttPerson.matchPercent   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDataPeople   
&Scoped-define SELF-NAME brwDataPeople
&Scoped-define QUERY-STRING-brwDataPeople preselect each ttPerson
&Scoped-define OPEN-QUERY-brwDataPeople open query {&SELF-NAME} preselect each ttPerson.
&Scoped-define TABLES-IN-QUERY-brwDataPeople ttPerson
&Scoped-define FIRST-TABLE-IN-QUERY-brwDataPeople ttPerson


/* Definitions for FRAME frAgents                                       */

/* Definitions for FRAME frAttorneys                                    */

/* Definitions for FRAME frOrganization                                 */

/* Definitions for FRAME frPeople                                       */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbRoleTypes btClear btFindOrg btFindPeople ~
btSearch btFindAgent btFindAttorney cbState flSearch flFindPeople ~
flFindAgent flFindAttorney flFindOrg SearchSepRectangle ViewAtt 
&Scoped-Define DISPLAYED-OBJECTS cbRoleTypes cbState flSearch flFindPeople ~
flFindAgent flFindAttorney flFindOrg 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData wWin 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( input ipcEntity as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStat wWin 
FUNCTION getComStat returns character PRIVATE
  ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStatDesc wWin 
FUNCTION getComStatDesc returns character
  ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getEntityName wWin 
FUNCTION getEntityName RETURNS character
  ( input ipageNo as integer /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getStatDesc wWin 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgentAction wWin 
FUNCTION openWindowForAgentAction returns handle
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultschanged wWin 
FUNCTION resultschanged RETURNS LOGICAL
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveSearchValue wWin 
FUNCTION saveSearchValue RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateSearch wWin 
FUNCTION validateSearch returns character
  ( cKeyWord as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_File 
       MENU-ITEM m_Configuration LABEL "Configure"     
       MENU-ITEM m_About        LABEL "About"         
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_Organization 
       MENU-ITEM m_O_Qualifications_Report LABEL "Qualifications Report"
       MENU-ITEM m_O_Pending_Reviews_Report LABEL "Qualifications Pending Review Utility"
       RULE
       MENU-ITEM m_O_Fulfillment_Report LABEL "Fulfillment Report"
       MENU-ITEM m_O_Fullfillment_Utility LABEL "First-Party Fullfillment Utility"
       MENU-ITEM m_O_Annual_First_Party_Report LABEL "Annual First-Party Qualifications Report"
       RULE
       MENU-ITEM m_O_Compliance_Status_Report LABEL "Compliance Status Report"
       RULE
       MENU-ITEM m_Add_New_Organization LABEL "Add New Organization".

DEFINE SUB-MENU m_Person 
       MENU-ITEM m_P_Qualifications_Report LABEL "Qualifications Report"
       MENU-ITEM m_P_Pending_Review_Report LABEL "Qualifications Pending Review Utility"
       RULE
       MENU-ITEM m_P_Fulfillment_Report LABEL "Fulfillment Report"
       MENU-ITEM m_P_First-Party_Fullfillment LABEL "First-Party Fullfillment Utility"
       MENU-ITEM m_P_Annual_First-Party_Report LABEL "Annual First-Party Qualifications Report"
       RULE
       MENU-ITEM m_P_Compliance_Status_Report LABEL "Compliance Status Report"
       RULE
       MENU-ITEM m_Add_New_Person LABEL "Add New Person"
       RULE
       MENU-ITEM m_Import_People_from_File LABEL "Import People from File".

DEFINE SUB-MENU m_States 
       MENU-ITEM States         LABEL "State Maintenance"
       RULE
       MENU-ITEM m_Requirements_Setup LABEL "State Requirements".

DEFINE SUB-MENU m_References 
       MENU-ITEM m_View_Types   LABEL "Setup Codes"   
       MENU-ITEM m_Nature_of_Affiliations LABEL "Nature of Affiliations"
       RULE
       MENU-ITEM m_All_Organization LABEL "All Organizations"
       MENU-ITEM m_All_Person   LABEL "All People"    
       RULE
       MENU-ITEM m_All_Agents   LABEL "All Agents"    
       MENU-ITEM m_All_Attorneys LABEL "All Attorneys" .

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_File         LABEL "Module"        
       SUB-MENU  m_Organization LABEL "Organization"  
       SUB-MENU  m_Person       LABEL "Person"        
       SUB-MENU  m_States       LABEL "States"        
       SUB-MENU  m_References   LABEL "References"    .

DEFINE MENU POPUP-MENU-brwDataAgent 
       MENU-ITEM m_View_Agent   LABEL "View Agent"    
       MENU-ITEM m_View_Organization LABEL "View Organization"
       MENU-ITEM m_View_Fulfillments LABEL "View Fulfillments"
       RULE
       MENU-ITEM m_View_Search_Key_Ag LABEL "View Search Keys".

DEFINE MENU POPUP-MENU-brwDataAttorney 
       MENU-ITEM m_View_Attorney LABEL "View Attorney" 
       MENU-ITEM m_View_Person  LABEL "View Person"   
       MENU-ITEM m_View_Person_Fulfillments LABEL "View Fulfillments"
       RULE
       MENU-ITEM m_View_Search_Key_Att LABEL "View Search Keys".

DEFINE MENU POPUP-MENU-brwDataOrg 
       MENU-ITEM m_View_Org     LABEL "View Organization"
       RULE
       MENU-ITEM m_View_Search_Key_Org LABEL "View Search Keys".

DEFINE MENU POPUP-MENU-brwDataPeople 
       MENU-ITEM m_View_Per     LABEL "View Person"   
       RULE
       MENU-ITEM m_View_Search_Key_Person LABEL "View Search Keys".


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 7.2 BY 1.71 TOOLTIP "Clear data".

DEFINE BUTTON btFindAgent  NO-FOCUS
     LABEL "Ag" 
     SIZE 4.8 BY 1.14 TOOLTIP "View agent".

DEFINE BUTTON btFindAttorney  NO-FOCUS
     LABEL "At" 
     SIZE 4.8 BY 1.14 TOOLTIP "View Attorney".

DEFINE BUTTON btFindOrg  NO-FOCUS
     LABEL "O" 
     SIZE 4.8 BY 1.14 TOOLTIP "View organization".

DEFINE BUTTON btFindPeople  NO-FOCUS
     LABEL "P" 
     SIZE 4.8 BY 1.14 TOOLTIP "View person".

DEFINE BUTTON btSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cbRoleTypes AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 26.8 BY 1 TOOLTIP "Select a State to limit search results" NO-UNDO.

DEFINE VARIABLE flSearch AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN AUTO-COMPLETION
     SIZE 80 BY 1 TOOLTIP "Enter criteria (A=Active; I=InActive)" NO-UNDO.

DEFINE VARIABLE flEnterCriteria AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 80 BY 1 NO-UNDO.

DEFINE VARIABLE flFindAgent AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Enter an Agent  ID to view it directly" NO-UNDO.

DEFINE VARIABLE flFindAttorney AS CHARACTER FORMAT "X(10)":U 
     LABEL "Attorney" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Enter an Attorney ID to view it directly" NO-UNDO.

DEFINE VARIABLE flFindOrg AS CHARACTER FORMAT "X(10)":U 
     LABEL "Organization" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Enter an Organization ID to view it directly" NO-UNDO.

DEFINE VARIABLE flFindPeople AS CHARACTER FORMAT "X(10)":U 
     LABEL "Person" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 TOOLTIP "Enter an Person ID to view it directly" NO-UNDO.

DEFINE RECTANGLE searchRectangleAtt
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 112.6 BY 4.

DEFINE RECTANGLE SearchSepRectangle
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .4 BY 3.52.

DEFINE RECTANGLE ViewAtt
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 80.4 BY 4.

DEFINE BUTTON btExportAgent  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON btNewAgent  NO-FOCUS
     LABEL "New Organization" 
     SIZE 4.8 BY 1.14 TOOLTIP "New agent".

DEFINE BUTTON btOpenAgent  NO-FOCUS
     LABEL "Open" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open detail".

DEFINE BUTTON btRefreshAgent  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE BUTTON btExportAttorney  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON btNewAttorney  NO-FOCUS
     LABEL "New Attorney" 
     SIZE 4.8 BY 1.14 TOOLTIP "New Attorney".

DEFINE BUTTON btOpenAttorney  NO-FOCUS
     LABEL "Open" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open detail".

DEFINE BUTTON btRefreshAttorney  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE VARIABLE attorneyType AS CHARACTER INITIAL "A" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O":U,
"Person", "P":U,
"None", "N":U,
"All", "A":U
     SIZE 44.8 BY 1.38 NO-UNDO.

DEFINE BUTTON btExportOrg  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON btNewOrg  NO-FOCUS
     LABEL "New Organization" 
     SIZE 4.8 BY 1.14 TOOLTIP "New organization".

DEFINE BUTTON btOpenOrg  NO-FOCUS
     LABEL "Open" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open detail".

DEFINE BUTTON btRefreshOrg  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

DEFINE BUTTON btExportPeople  NO-FOCUS
     LABEL "Export" 
     SIZE 4.8 BY 1.14 TOOLTIP "Export data".

DEFINE BUTTON btNewPeople  NO-FOCUS
     LABEL "New People" 
     SIZE 4.8 BY 1.14 TOOLTIP "New person".

DEFINE BUTTON btOpenPeople  NO-FOCUS
     LABEL "Open" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open detail".

DEFINE BUTTON btRefreshPeople  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reload data".

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDataAgent FOR 
      ttAgent SCROLLING.

DEFINE QUERY brwDataAttorney FOR 
      ttAttorney SCROLLING.

DEFINE QUERY brwDataOrg FOR 
      ttOrganization SCROLLING.

DEFINE QUERY brwDataPeople FOR 
      ttPerson SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDataAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDataAgent wWin _FREEFORM
  QUERY brwDataAgent DISPLAY
      getComStat(ttagent.comseq)  @ ttagent.comseq        format "x(5)"      label "" width 5
ttAgent.agentID           format "999999"       label "Agent ID"            width 12
 getStatDesc(ttagent.stat) 
 @ ttagent.stat           format "x(25)"        label "Status"              width 14
ttagent.legalname         format "x(100)"       label "Agent Legal Name"    width 40
ttagent.address           format "x(100)"       label "Address"             width 40 
ttagent.stateID           format "x(10)"        label "State ID"            width 10
ttagent.orgID             format "x(10)"        label "Organization ID"     width 14
ttagent.orgname           format "x(100)"       label "Organization Name"   width 30
ttagent.matchPercent      format "ZZZ"          label "%"                   width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 182.6 BY 18.52 ROW-HEIGHT-CHARS .82 FIT-LAST-COLUMN.

DEFINE BROWSE brwDataAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDataAttorney wWin _FREEFORM
  QUERY brwDataAttorney DISPLAY
      getComStat(ttattorney.Comseq) @ ttattorney.Comseq        label ""         width 5
ttAttorney.attorneyID format "x(20)"       label "Attorney ID" width 15
getStatDesc(ttattorney.stat)
 @ ttAttorney.stat       format "x(15)"       label "Status"       width 13
ttAttorney.entityname    format "x(100)"       label "Name"        width 48
ttAttorney.address       format "x(100)"       label "Address"     width 53
ttAttorney.stateID       format "x(10)"       label "State ID"     width 10
ttAttorney.entityID      format "x(10)"       label "Org ID/Person ID"    width 21
ttAttorney.matchPercent  format "ZZZ"         label "%"            width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 182.6 BY 17.71 ROW-HEIGHT-CHARS .82 FIT-LAST-COLUMN.

DEFINE BROWSE brwDataOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDataOrg wWin _FREEFORM
  QUERY brwDataOrg DISPLAY
      ttOrganization.orgID      format "999999"  label "Organization ID"  width 18
ttOrganization.name         format "x(100)"      label "Name"             width 30
ttOrganization.address      format "x(100)"      label "Address"          width 49
ttOrganization.phone        format "x(40)"       label "Phone"            width 20
ttOrganization.website      format "x(30)"       label "Website"          width 20
ttOrganization.roles        format "x(50)"       label "Roles"            width 28
ttorganization.matchPercent format "ZZZ"         label "%"                width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 182.6 BY 18.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwDataPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDataPeople wWin _FREEFORM
  QUERY brwDataPeople DISPLAY
      ttPerson.personID    format "x(20)"       label "Person ID"   width 15
 getStatDesc(string(ttperson.active))
 @ ttPerson.active          format "x(15)"       label "Status"     width 15
ttPerson.dispname           format "x(100)"      label "Name"       width 50
ttPerson.address            format "x(100)"      label "Address"    width 55
ttPerson.roles              format "x(50)"       label "Roles"      width 30
ttPerson.matchPercent       format "ZZZ"         label "%"          width 10
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 182.6 BY 18.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbRoleTypes AT ROW 1.71 COL 49 COLON-ALIGNED WIDGET-ID 380
     btClear AT ROW 2.57 COL 105.4 WIDGET-ID 374 NO-TAB-STOP 
     btFindOrg AT ROW 2.24 COL 149.6 WIDGET-ID 326 NO-TAB-STOP 
     btFindPeople AT ROW 2.24 COL 186.2 WIDGET-ID 346 NO-TAB-STOP 
     btSearch AT ROW 2.57 COL 98 WIDGET-ID 352 NO-TAB-STOP 
     btFindAgent AT ROW 3.43 COL 149.6 WIDGET-ID 366 NO-TAB-STOP 
     btFindAttorney AT ROW 3.43 COL 186.2 WIDGET-ID 370 NO-TAB-STOP 
     cbState AT ROW 1.71 COL 10.6 COLON-ALIGNED WIDGET-ID 354
     flSearch AT ROW 2.91 COL 12.6 NO-LABEL WIDGET-ID 358
     flFindPeople AT ROW 2.29 COL 165.6 COLON-ALIGNED WIDGET-ID 356
     flFindAgent AT ROW 3.48 COL 129 COLON-ALIGNED WIDGET-ID 368
     flFindAttorney AT ROW 3.48 COL 165.6 COLON-ALIGNED WIDGET-ID 372
     flFindOrg AT ROW 2.29 COL 129 COLON-ALIGNED WIDGET-ID 320
     flEnterCriteria AT ROW 4.14 COL 10.8 COLON-ALIGNED NO-LABEL WIDGET-ID 378
     "View" VIEW-AS TEXT
          SIZE 4.6 BY .62 AT ROW 1.14 COL 115.4 WIDGET-ID 332
     "Search" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.1 COL 3.2 WIDGET-ID 58
     searchRectangleAtt AT ROW 1.43 COL 2.2 WIDGET-ID 364
     SearchSepRectangle AT ROW 1.67 COL 95.4 WIDGET-ID 362
     ViewAtt AT ROW 1.43 COL 114.4 WIDGET-ID 318
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 1
         SIZE 195.4 BY 26.95 WIDGET-ID 100.

DEFINE FRAME frAttorneys
     attorneyType AT ROW 1.1 COL 8.2 NO-LABEL WIDGET-ID 362
     brwDataAttorney AT ROW 2.52 COL 8 WIDGET-ID 900
     btRefreshAttorney AT ROW 2.48 COL 2.2 WIDGET-ID 322 NO-TAB-STOP 
     btExportAttorney AT ROW 3.62 COL 2.2 WIDGET-ID 344 NO-TAB-STOP 
     btNewAttorney AT ROW 4.76 COL 2.2 WIDGET-ID 348 NO-TAB-STOP 
     btOpenAttorney AT ROW 5.91 COL 2.2 WIDGET-ID 350 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 6.91
         SIZE 191.2 BY 20.49
         TITLE "Attorneys" WIDGET-ID 500.

DEFINE FRAME frPeople
     brwDataPeople AT ROW 1.29 COL 8 WIDGET-ID 800
     btExportPeople AT ROW 2.38 COL 2.2 WIDGET-ID 344 NO-TAB-STOP 
     btNewPeople AT ROW 3.52 COL 2.2 WIDGET-ID 348 NO-TAB-STOP 
     btOpenPeople AT ROW 4.67 COL 2.2 WIDGET-ID 350 NO-TAB-STOP 
     btRefreshPeople AT ROW 1.24 COL 2.2 WIDGET-ID 322 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 6.91
         SIZE 191.1 BY 20.49
         TITLE "People" WIDGET-ID 400.

DEFINE FRAME frOrganization
     brwDataOrg AT ROW 1.29 COL 8 WIDGET-ID 600
     btExportOrg AT ROW 2.38 COL 2.2 WIDGET-ID 340 NO-TAB-STOP 
     btNewOrg AT ROW 3.52 COL 2.2 WIDGET-ID 342 NO-TAB-STOP 
     btOpenOrg AT ROW 4.67 COL 2.2 WIDGET-ID 344 NO-TAB-STOP 
     btRefreshOrg AT ROW 1.24 COL 2.2 WIDGET-ID 322 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 6.91
         SIZE 191.2 BY 20.49
         TITLE "Organizations" WIDGET-ID 200.

DEFINE FRAME frAgents
     brwDataAgent AT ROW 1.29 COL 8 WIDGET-ID 1000
     btExportAgent AT ROW 2.38 COL 2.2 WIDGET-ID 344 NO-TAB-STOP 
     btNewAgent AT ROW 3.52 COL 2.2 WIDGET-ID 348 NO-TAB-STOP 
     btOpenAgent AT ROW 4.67 COL 2.2 WIDGET-ID 350 NO-TAB-STOP 
     btRefreshAgent AT ROW 1.24 COL 2.2 WIDGET-ID 322 NO-TAB-STOP 
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 6.91
         SIZE 191.2 BY 20.49
         TITLE "Agents" WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Compliance Management"
         HEIGHT             = 26.86
         WIDTH              = 195
         MAX-HEIGHT         = 36.24
         MAX-WIDTH          = 307.2
         VIRTUAL-HEIGHT     = 36.24
         VIRTUAL-WIDTH      = 307.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frAgents:FRAME = FRAME fMain:HANDLE
       FRAME frAttorneys:FRAME = FRAME fMain:HANDLE
       FRAME frOrganization:FRAME = FRAME fMain:HANDLE
       FRAME frPeople:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* SETTINGS FOR FILL-IN flEnterCriteria IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       flEnterCriteria:AUTO-RESIZE IN FRAME fMain      = TRUE
       flEnterCriteria:RESIZABLE IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX flSearch IN FRAME fMain
   ALIGN-L                                                              */
/* SETTINGS FOR RECTANGLE searchRectangleAtt IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frAgents
                                                                        */
/* BROWSE-TAB brwDataAgent 1 frAgents */
ASSIGN 
       brwDataAgent:POPUP-MENU IN FRAME frAgents             = MENU POPUP-MENU-brwDataAgent:HANDLE
       brwDataAgent:ALLOW-COLUMN-SEARCHING IN FRAME frAgents = TRUE
       brwDataAgent:COLUMN-RESIZABLE IN FRAME frAgents       = TRUE.

/* SETTINGS FOR BUTTON btExportAgent IN FRAME frAgents
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btOpenAgent IN FRAME frAgents
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefreshAgent IN FRAME frAgents
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frAttorneys
                                                                        */
/* BROWSE-TAB brwDataAttorney attorneyType frAttorneys */
ASSIGN 
       brwDataAttorney:POPUP-MENU IN FRAME frAttorneys             = MENU POPUP-MENU-brwDataAttorney:HANDLE
       brwDataAttorney:ALLOW-COLUMN-SEARCHING IN FRAME frAttorneys = TRUE
       brwDataAttorney:COLUMN-RESIZABLE IN FRAME frAttorneys       = TRUE.

/* SETTINGS FOR BUTTON btExportAttorney IN FRAME frAttorneys
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btOpenAttorney IN FRAME frAttorneys
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefreshAttorney IN FRAME frAttorneys
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frOrganization
                                                                        */
/* BROWSE-TAB brwDataOrg 1 frOrganization */
ASSIGN 
       FRAME frOrganization:RESIZABLE        = TRUE.

ASSIGN 
       brwDataOrg:POPUP-MENU IN FRAME frOrganization             = MENU POPUP-MENU-brwDataOrg:HANDLE
       brwDataOrg:ALLOW-COLUMN-SEARCHING IN FRAME frOrganization = TRUE
       brwDataOrg:COLUMN-RESIZABLE IN FRAME frOrganization       = TRUE.

/* SETTINGS FOR BUTTON btExportOrg IN FRAME frOrganization
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btOpenOrg IN FRAME frOrganization
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefreshOrg IN FRAME frOrganization
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frPeople
                                                                        */
/* BROWSE-TAB brwDataPeople 1 frPeople */
ASSIGN 
       brwDataPeople:POPUP-MENU IN FRAME frPeople             = MENU POPUP-MENU-brwDataPeople:HANDLE
       brwDataPeople:ALLOW-COLUMN-SEARCHING IN FRAME frPeople = TRUE
       brwDataPeople:COLUMN-RESIZABLE IN FRAME frPeople       = TRUE.

/* SETTINGS FOR BUTTON btExportPeople IN FRAME frPeople
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btOpenPeople IN FRAME frPeople
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRefreshPeople IN FRAME frPeople
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDataAgent
/* Query rebuild information for BROWSE brwDataAgent
     _START_FREEFORM
open query {&SELF-NAME} preselect each ttAgent.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDataAgent */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDataAttorney
/* Query rebuild information for BROWSE brwDataAttorney
     _START_FREEFORM
open query {&SELF-NAME} preselect each ttAttorney.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDataAttorney */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDataOrg
/* Query rebuild information for BROWSE brwDataOrg
     _START_FREEFORM
open query {&SELF-NAME} preselect each ttOrganization.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDataOrg */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDataPeople
/* Query rebuild information for BROWSE brwDataPeople
     _START_FREEFORM
open query {&SELF-NAME} preselect each ttPerson.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDataPeople */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Compliance Management */
or endkey of {&window-name} anywhere do:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  if this-procedure:persistent 
   then 
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Compliance Management */
do:
  /* This event will close the window and terminate the procedure.  */  
  publish "GetConfirmExit" (output std-lo).
  
  if std-lo 
   then
    do:
      message "Are you sure you want to exit?"
          view-as alert-box question buttons ok-cancel update std-lo as logical.
      if not std-lo 
       then 
        return no-apply.
    end.
  
  apply "CLOSE":U to this-procedure.
  publish "ExitApplication".
  return no-apply.         
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Compliance Management */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAttorneys
&Scoped-define SELF-NAME attorneyType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL attorneyType wWin
ON VALUE-CHANGED OF attorneyType IN FRAME frAttorneys
DO:
  if attorneyType:input-value = "A" or attorneyType:input-value = "N" then
    assign btNewAttorney:sensitive = false.
  else 
    assign btNewAttorney:sensitive = true.
      
  run filterData in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDataAgent
&Scoped-define FRAME-NAME frAgents
&Scoped-define SELF-NAME brwDataAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAgent wWin
ON DEFAULT-ACTION OF brwDataAgent IN FRAME frAgents
DO:  
  if not available ttagent
   then return.
  
  if ttagent.orgid <> "" and ttagent.orgid <> "0"
   then
    run openOrganization in this-procedure (input ttagent.orgid,
                                            input 3,                /* tab no */
                                            input ttagent.agentID). /* agent ID */ 
   else
    openWindowForAgentAction(ttagent.agentID, "detail", "wagent.w", "E").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAgent wWin
ON ROW-DISPLAY OF brwDataAgent IN FRAME frAgents
DO:
  {lib/brw-rowdisplay-multi.i}
  ttagent.ComSeq:bgcolor 
      in browse brwDataAgent  =    if      (ttagent.ComSeq = "3") then 12 
                                   else if (ttagent.ComSeq = "1") then 2 
                                   else if (ttagent.ComSeq = "2") then 14 
                                   else 15.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAgent wWin
ON START-SEARCH OF brwDataAgent IN FRAME frAgents
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwDataAgent &sortClause = "'by legalname by agentID desc'" }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAgent wWin
ON VALUE-CHANGED OF brwDataAgent IN FRAME frAgents
DO:
  if not available ttagent
   then
    do:
      assign
          MENU-ITEM m_View_Agent:sensitive          IN MENU POPUP-MENU-brwDataAgent = false
          MENU-ITEM m_View_Organization:sensitive   IN MENU POPUP-MENU-brwDataAgent = false
          MENU-ITEM m_View_Fulfillments:sensitive   IN MENU POPUP-MENU-brwDataAgent = false
          MENU-ITEM m_View_Search_Key_Ag:sensitive  IN MENU POPUP-MENU-brwDataAgent = false
          .
      
      return.
    end.
 
   assign
       MENU-ITEM m_View_Agent:sensitive         IN MENU POPUP-MENU-brwDataAgent = true
       MENU-ITEM m_View_Organization:sensitive  IN MENU POPUP-MENU-brwDataAgent = true
       MENU-ITEM m_View_Fulfillments:sensitive  IN MENU POPUP-MENU-brwDataAgent = true
       MENU-ITEM m_View_Search_Key_Ag:sensitive IN MENU POPUP-MENU-brwDataAgent = true
       .
 
 if ttagent.OrgId = ""
  then
   assign
     MENU-ITEM m_View_Organization:sensitive IN MENU POPUP-MENU-brwDataAgent = false
     MENU-ITEM m_View_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAgent = false
     .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDataAttorney
&Scoped-define FRAME-NAME frAttorneys
&Scoped-define SELF-NAME brwDataAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAttorney wWin
ON DEFAULT-ACTION OF brwDataAttorney IN FRAME frAttorneys
DO:  
  if not available ttattorney
   then return.
  
  if entityID ne ""
   then
    do:
      if ttattorney.personID ne ""
       then
        run openPerson in this-procedure (input ttattorney.personID,
                                          input 3,                      /* tab no */
                                          input ttattorney.attorneyID). /* attorney ID */
      else if ttattorney.orgID ne ""
       then
        run openOrganization in this-procedure (input ttattorney.orgID,
                                                input 3,                      /* tab no */
                                                input ttattorney.attorneyID). /* attorney ID */
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAttorney wWin
ON ROW-DISPLAY OF brwDataAttorney IN FRAME frAttorneys
DO:
  {lib/brw-rowdisplay-multi.i}
  
   ttattorney.ComSeq:bgcolor 
      in browse brwDataAttorney =  if      (ttattorney.ComSeq = "3") then 12 
                                   else if (ttattorney.ComSeq = "1") then 2 
                                   else if (ttattorney.ComSeq = "2") then 14 
                                   else 15.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAttorney wWin
ON START-SEARCH OF brwDataAttorney IN FRAME frAttorneys
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwDataAttorney &sortClause = "'by name1 by attorneyID desc'" }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataAttorney wWin
ON VALUE-CHANGED OF brwDataAttorney IN FRAME frAttorneys
DO:
   if not available ttattorney
    then
     do:
       assign
           MENU-ITEM m_View_Attorney:sensitive            IN MENU POPUP-MENU-brwDataAttorney = false
           MENU-ITEM m_View_Person:sensitive              IN MENU POPUP-MENU-brwDataAttorney = false
           MENU-ITEM m_View_Person_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAttorney = false
           MENU-ITEM m_View_Search_Key_Att:sensitive      IN MENU POPUP-MENU-brwDataAttorney = false
           .
       return.
     end.
  
   assign
        MENU-ITEM m_View_Attorney:sensitive            IN MENU POPUP-MENU-brwDataAttorney = true
        MENU-ITEM m_View_Person:sensitive              IN MENU POPUP-MENU-brwDataAttorney = true
        MENU-ITEM m_View_Person_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAttorney = true
        MENU-ITEM m_View_Search_Key_Att:sensitive      IN MENU POPUP-MENU-brwDataAttorney = true
        .
 
   if ttAttorney.entity = {&OrganizationCode} 
    then
     assign menu-item m_View_Person:label IN MENU POPUP-MENU-brwDataAttorney = "View Organization".
   else if ttAttorney.entity = {&PersonCode} 
    then 
     assign menu-item m_View_Person:label IN MENU POPUP-MENU-brwDataAttorney = "View Person".
   else
    do:
      MENU-ITEM m_View_Person:sensitive IN MENU POPUP-MENU-brwDataAttorney = false.
      MENU-ITEM m_View_Person_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAttorney = false.
      MENU-ITEM m_View_Attorney:sensitive IN MENU POPUP-MENU-brwDataAttorney = false.
    end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDataOrg
&Scoped-define FRAME-NAME frOrganization
&Scoped-define SELF-NAME brwDataOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataOrg wWin
ON DEFAULT-ACTION OF brwDataOrg IN FRAME frOrganization
DO:
  if not available ttorganization or ttorganization.orgid = "" or ttorganization.orgid = "0"
   then return.
    
  run openOrganization in this-procedure (input ttorganization.orgid,
                                          input 2,              /* tab no */
                                          input "").            /* agent ID */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataOrg wWin
ON ROW-DISPLAY OF brwDataOrg IN FRAME frOrganization
DO:
  {lib/brw-rowdisplay-multi.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataOrg wWin
ON START-SEARCH OF brwDataOrg IN FRAME frOrganization
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwDataOrg}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataOrg wWin
ON VALUE-CHANGED OF brwDataOrg IN FRAME frOrganization
DO:
  if not available ttorganization
   then
    do:
      MENU-ITEM m_View_Org:sensitive             IN MENU POPUP-MENU-brwDataOrg = false.
      MENU-ITEM m_View_Search_Key_Org:sensitive  IN MENU POPUP-MENU-brwDataOrg = false.
      return.
    end.
                                    
  MENU-ITEM m_View_Org:sensitive             IN MENU POPUP-MENU-brwDataOrg = true.
  MENU-ITEM m_View_Search_Key_Org:sensitive  IN MENU POPUP-MENU-brwDataOrg = true.                 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDataPeople
&Scoped-define FRAME-NAME frPeople
&Scoped-define SELF-NAME brwDataPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataPeople wWin
ON DEFAULT-ACTION OF brwDataPeople IN FRAME frPeople
DO:
  if not available ttperson or ttperson.personID = ""
   then return.
  
  run openPerson in this-procedure (input ttperson.personID,
                                    input 2,    /* tab no */
                                    input "").  /* attorney ID */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataPeople wWin
ON ROW-DISPLAY OF brwDataPeople IN FRAME frPeople
DO:
  {lib/brw-rowdisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataPeople wWin
ON START-SEARCH OF brwDataPeople IN FRAME frPeople
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwDataPeople}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDataPeople wWin
ON VALUE-CHANGED OF brwDataPeople IN FRAME frPeople
DO:
  if not available ttperson
   then
    do:
      MENU-ITEM m_View_Per:sensitive                IN MENU POPUP-MENU-brwDataPeople = false.
      MENU-ITEM m_View_Search_Key_Person:sensitive  IN MENU POPUP-MENU-brwDataPeople = false.
      return.
    end.
  
  MENU-ITEM m_View_Per:sensitive                    IN MENU POPUP-MENU-brwDataPeople = true.
  MENU-ITEM m_View_Search_Key_Person:sensitive      IN MENU POPUP-MENU-brwDataPeople = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear wWin
ON CHOOSE OF btClear IN FRAME fMain /* Clear */
do:
  cbState:screen-value in frame fmain = "ALL" .
  clearData(input getEntityName(ipageNo) ).
  flSearch:screen-value  = "".
  resultsChanged(true).

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAgents
&Scoped-define SELF-NAME btExportAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExportAgent wWin
ON CHOOSE OF btExportAgent IN FRAME frAgents /* Export */
do:
  empty temp-table tAgent.
  for each ttagent:
     create tAgent.
     buffer-copy ttagent to tagent .
     assign
         tagent.comseq = getComStatDesc(getcomStat(ttagent.comSeq))
         tagent.stat   = getStatDesc(ttagent.stat)
         .
   end.
   
  run exportData in this-procedure (input browse brwDataAgent:handle, /* Borwser handle */
                                   input temp-table tAgent:handle,   /* Temp-table handle */
                                   input "tAgent",                    /* Table name */
                                   input "Agent",                     /* CSV filename */
                                   input "for each tAgent").          /* Query */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAttorneys
&Scoped-define SELF-NAME btExportAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExportAttorney wWin
ON CHOOSE OF btExportAttorney IN FRAME frAttorneys /* Export */
do:
  empty temp-table tempAttorney.
  for each ttattorney:
    create tempAttorney.
    buffer-copy ttattorney to tempAttorney .
    assign
        tempAttorney.comseq = getComStatDesc(getcomStat(ttattorney.comSeq))
        tempAttorney.stat   = getStatDesc(ttattorney.stat)
        .
  end.

  run exportData in this-procedure (input browse brwDataAttorney:handle,   /* Borwser handle */
                                   input temp-table tempAttorney:handle,      /* Temp-table handle */
                                   input "tempAttorney",                      /* Table name */
                                   input "Attorney",                       /* CSV filename */
                                   input "for each tempAttorney").            /* Query */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frOrganization
&Scoped-define SELF-NAME btExportOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExportOrg wWin
ON CHOOSE OF btExportOrg IN FRAME frOrganization /* Export */
do:
  run exportData in this-procedure (input browse brwDataOrg:handle,   /* Borwser handle */
                                   input temp-table ttOrganization:handle, /* Temp-table handle */
                                   input "ttorganization",                 /* Table name */
                                   input "Organization",             /* CSV filename */
                                   input "for each ttorganization").       /* Query */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frPeople
&Scoped-define SELF-NAME btExportPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExportPeople wWin
ON CHOOSE OF btExportPeople IN FRAME frPeople /* Export */
do:
  run exportData in this-procedure (input browse brwDataPeople:handle,   /* Borwser handle */
                                    input temp-table ttperson:handle, /* Temp-table handle */
                                    input "ttPerson",                 /* Table name */
                                    input "Person",             /* CSV filename */
                                    input "for each ttPerson").       /* Query */
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME btFindAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFindAgent wWin
ON CHOOSE OF btFindAgent IN FRAME fMain /* Ag */
do:
  apply "return" to flFindAgent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFindAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFindAttorney wWin
ON CHOOSE OF btFindAttorney IN FRAME fMain /* At */
do:
  apply "return" to flFindAttorney .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFindOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFindOrg wWin
ON CHOOSE OF btFindOrg IN FRAME fMain /* O */
do:
  apply "return" to flFindOrg .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btFindPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btFindPeople wWin
ON CHOOSE OF btFindPeople IN FRAME fMain /* P */
do:
  apply "return" to flFindPeople .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAgents
&Scoped-define SELF-NAME btNewAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNewAgent wWin
ON CHOOSE OF btNewAgent IN FRAME frAgents /* New Organization */
do:
  run ActionNewAgent in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAttorneys
&Scoped-define SELF-NAME btNewAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNewAttorney wWin
ON CHOOSE OF btNewAttorney IN FRAME frAttorneys /* New Attorney */
do:
  run ActionNewAttorney in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frOrganization
&Scoped-define SELF-NAME btNewOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNewOrg wWin
ON CHOOSE OF btNewOrg IN FRAME frOrganization /* New Organization */
do:
  run ActionNewOrganization in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frPeople
&Scoped-define SELF-NAME btNewPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNewPeople wWin
ON CHOOSE OF btNewPeople IN FRAME frPeople /* New People */
do:
  run ActionNewPerson in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAgents
&Scoped-define SELF-NAME btOpenAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOpenAgent wWin
ON CHOOSE OF btOpenAgent IN FRAME frAgents /* Open */
DO:
   if not available ttagent 
    then return.
     
   openWindowForAgentAction(ttagent.agentID, "detail", "wagent.w", "E").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAttorneys
&Scoped-define SELF-NAME btOpenAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOpenAttorney wWin
ON CHOOSE OF btOpenAttorney IN FRAME frAttorneys /* Open */
do:
  if not available ttattorney
   then
    return.
    
  run OpenAttorney in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frOrganization
&Scoped-define SELF-NAME btOpenOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOpenOrg wWin
ON CHOOSE OF btOpenOrg IN FRAME frOrganization /* Open */
DO:
  if not available ttorganization or ttorganization.orgid = "" or ttorganization.orgid = "0"
   then
    return.
    
  run openOrganization in this-procedure (input ttOrganization.orgID,
                                          input 2,              /* tab no */
                                          input "").            /* agent ID */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frPeople
&Scoped-define SELF-NAME btOpenPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOpenPeople wWin
ON CHOOSE OF btOpenPeople IN FRAME frPeople /* Open */
DO:
  if not available ttperson or ttperson.personID = ""
   then return.
  
  run openPerson in this-procedure (input ttperson.personID,
                                    input 2,    /* tab no */
                                    input "").  /* attorney ID */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAgents
&Scoped-define SELF-NAME btRefreshAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefreshAgent wWin
ON CHOOSE OF btRefreshAgent IN FRAME frAgents /* Refresh */
DO:
  if btRefreshAgent:sensitive 
   then
    run getData in this-procedure (input getEntityName(ipageNo), input cAgentSearch ) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frAttorneys
&Scoped-define SELF-NAME btRefreshAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefreshAttorney wWin
ON CHOOSE OF btRefreshAttorney IN FRAME frAttorneys /* Refresh */
DO:
  if btRefreshAttorney:sensitive 
   then
    run getData in this-procedure (input getEntityName(ipageNo), input cAttorneySearch ) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frOrganization
&Scoped-define SELF-NAME btRefreshOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefreshOrg wWin
ON CHOOSE OF btRefreshOrg IN FRAME frOrganization /* Refresh */
DO:
  if btRefreshOrg:sensitive 
   then
    run getData in this-procedure (input getEntityName(ipageNo), input cOrgSearch ) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frPeople
&Scoped-define SELF-NAME btRefreshPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefreshPeople wWin
ON CHOOSE OF btRefreshPeople IN FRAME frPeople /* Refresh */
do:
  if btRefreshPeople:sensitive 
   then
    run getData in this-procedure (input getEntityName(ipageNo), input cPeopleSearch ) . 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME btSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSearch wWin
ON CHOOSE OF btSearch IN FRAME fMain /* Search */
do:
  apply "return" to flSearch in frame fmain.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState wWin
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFindAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFindAgent wWin
ON RETURN OF flFindAgent IN FRAME fMain /* Agent */
DO:  
  if self:screen-value = ""
   then return no-apply.
   
  if not can-find(first ttagent where ttagent.agentID = self:input-value)
   then
    do:
      empty temp-table tagent. /* Not needed, but feels correct */
        
      run server/searchagents.p (input "ALL",
                                 input self:input-value,
                                 output table tagent,
                                 output std-lo,
                                 output std-ch).
      if not std-lo 
       then return no-apply.
        
      for first tagent where tagent.agentID = self:input-value:
        create ttagent.
        buffer-copy tagent to ttagent.
      end.
    end.
      
  find first ttagent where ttagent.agentID = self:input-value no-error.
  if not available ttagent
   then return no-apply.
    
  if ttagent.orgid <> "" and ttagent.orgid <> "0"
   then
    run openOrganization in this-procedure (input ttagent.orgid,
                                            input 3,                /* tab no */
                                            input ttagent.agentID). /* agent ID */ 
   else
    openWindowForAgentAction(ttagent.agentID, "detail", "wagent.w", "E").

  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFindAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFindAttorney wWin
ON RETURN OF flFindAttorney IN FRAME fMain /* Attorney */
DO:  
  if flFindAttorney:input-value = ""
   then return no-apply.
   
  if not can-find(first ttattorney where ttattorney.attorneyID = flFindAttorney:input-value)
   then
    do:
      empty temp-table tempattorney. /* Not needed, but feels correct */
        
      run server/searchattorneys.p (input "ALL",
                                    input flFindAttorney:input-value,
                                    output table tempattorney,
                                    output std-lo,
                                    output std-ch).
      if not std-lo      
       then return no-apply.
        
      for first tempattorney where tempattorney.attorneyID = flFindAttorney:input-value:
        create ttattorney.
        buffer-copy tempattorney to ttattorney.
      end.
    end.
      
  find first ttattorney where ttattorney.attorneyID = flFindAttorney:input-value no-error.    
  if not available ttattorney
   then return no-apply.
  
  if  ttAttorney.orgID ne "" and  ttAttorney.orgID ne ?   
   then
    do:
      if not available ttattorney or ttattorney.orgID = ""
       then return.
  
     run openOrganization in this-procedure (input ttattorney.orgID,
                                             input 4,                      /* tab no */
                                             input ttattorney.attorneyID). /* attorney ID */
   end.
  else 
   do:
     if not available ttattorney or ttattorney.personID = ""
       then return.
  
      run openPerson in this-procedure (input ttattorney.personID,
                                        input 4,                      /* tab no */
                                        input ttattorney.attorneyID). /* attorney ID */
  end.
  
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFindOrg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFindOrg wWin
ON RETURN OF flFindOrg IN FRAME fMain /* Organization */
DO:  
  if self:screen-value <> ""
   then
    run openOrganization in this-procedure (input self:input-value,
                                            input 2,              /* tab no */
                                            input "").            /* agent ID */                              
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flFindPeople
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flFindPeople wWin
ON RETURN OF flFindPeople IN FRAME fMain /* Person */
do:  
  if self:screen-value <> ""
   then
    run openPerson in this-procedure (input self:screen-value,
                                      input 2,    /* tab no */
                                      input "").  /* attorney ID */
 
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flSearch wWin
ON RETURN OF flSearch IN FRAME fMain
DO:
  if flSearch:input-value = "" or flSearch:input-value = ?
   then
    return no-apply.
    
  run getData in this-procedure (input getEntityName(ipageNo), input flsearch:input-value ) .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flSearch wWin
ON VALUE-CHANGED OF flSearch IN FRAME fMain
do:
   resultsChanged(false).                
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About wWin
ON CHOOSE OF MENU-ITEM m_About /* About */
do:
  publish "AboutApplication".
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_New_Organization
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_New_Organization wWin
ON CHOOSE OF MENU-ITEM m_Add_New_Organization /* Add New Organization */
do:
  do with frame frOrganization:
    apply "choose" to btNewOrg.
  end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Add_New_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Add_New_Person wWin
ON CHOOSE OF MENU-ITEM m_Add_New_Person /* Add New Person */
do:
  do with frame frPeople:
    apply "choose" to btNewPeople.
  end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_All_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_All_Agents wWin
ON CHOOSE OF MENU-ITEM m_All_Agents /* All Agents */
do: 
   publish "openWindow" (input "ReferenceAgent",
                         input "",
                         input "referenceagent.w",
                         input ?,
                         input this-procedure).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_All_Attorneys
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_All_Attorneys wWin
ON CHOOSE OF MENU-ITEM m_All_Attorneys /* All Attorneys */
do:   
  publish "openWindow" (input "Attorneys",
                        input "",
                        input "wattorneys.w",
                        input ?,
                        input this-procedure).

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_All_Organization
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_All_Organization wWin
ON CHOOSE OF MENU-ITEM m_All_Organization /* All Organizations */
do:
   publish "openWindow" (input "Organization",
                         input "",
                         input "worganizations.w",
                         input ?,
                         input this-procedure).

    
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_All_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_All_Person wWin
ON CHOOSE OF MENU-ITEM m_All_Person /* All People */
do:
   publish "openWindow" (input "Persons",
                         input "",
                         input "wPersons.w",
                         input ?,
                         input this-procedure).

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configuration
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configuration wWin
ON CHOOSE OF MENU-ITEM m_Configuration /* Configure */
do:
  run ActionConfig in this-procedure (output std-lo).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit wWin
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
do:
  apply "WINDOW-CLOSE" to {&window-name}.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Import_People_from_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Import_People_from_File wWin
ON CHOOSE OF MENU-ITEM m_Import_People_from_File /* Import People from File */
do:
  run actionImportPeople in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Nature_of_Affiliations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Nature_of_Affiliations wWin
ON CHOOSE OF MENU-ITEM m_Nature_of_Affiliations /* Nature of Affiliations */
DO:
  publish "openWindow" (input "Sys Codes",
                        input "",
                        input "wsyscodes.w",
                        input ?,
                        input this-procedure).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_O_Annual_First_Party_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_O_Annual_First_Party_Report wWin
ON CHOOSE OF MENU-ITEM m_O_Annual_First_Party_Report /* Annual First-Party Qualifications Report */
do:
  run wfirstpartyfulfillmentrpt-organization.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_O_Compliance_Status_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_O_Compliance_Status_Report wWin
ON CHOOSE OF MENU-ITEM m_O_Compliance_Status_Report /* Compliance Status Report */
do:
  run wcompliancestatusrpt-organization.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_O_Fulfillment_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_O_Fulfillment_Report wWin
ON CHOOSE OF MENU-ITEM m_O_Fulfillment_Report /* Fulfillment Report */
do:
  run wfulfillmentrpt-organization.w persistent(input {&OrganizationCode}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_O_Fullfillment_Utility
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_O_Fullfillment_Utility wWin
ON CHOOSE OF MENU-ITEM m_O_Fullfillment_Utility /* First-Party Fullfillment Utility */
do:
  publish "openWindow" (input "FirstPartyOrganizationUtility",
                        input "", 
                        input "wfirstpartyfulfillmentrptutil-organization.w",
                        input "character|input|" + {&OrganizationCode},
                        input this-procedure).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_O_Pending_Reviews_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_O_Pending_Reviews_Report wWin
ON CHOOSE OF MENU-ITEM m_O_Pending_Reviews_Report /* Qualifications Pending Review Utility */
do:
  run wpendingreviews-organization.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_O_Qualifications_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_O_Qualifications_Report wWin
ON CHOOSE OF MENU-ITEM m_O_Qualifications_Report /* Qualifications Report */
do:
  run wactivequalrpt-organization.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_P_Annual_First-Party_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_P_Annual_First-Party_Report wWin
ON CHOOSE OF MENU-ITEM m_P_Annual_First-Party_Report /* Annual First-Party Qualifications Report */
do:
  run wfirstpartyfulfillmentrpt-person.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_P_Compliance_Status_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_P_Compliance_Status_Report wWin
ON CHOOSE OF MENU-ITEM m_P_Compliance_Status_Report /* Compliance Status Report */
do:
  run wcompliancestatusrpt-person.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_P_First-Party_Fullfillment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_P_First-Party_Fullfillment wWin
ON CHOOSE OF MENU-ITEM m_P_First-Party_Fullfillment /* First-Party Fullfillment Utility */
do:
  publish "openWindow" (input "FirstPartyPersonUtility",
                        input "", 
                        input "wfirstpartyfulfillmentrptutil-person.w",
                        input "character|input|" + {&PersonCode},
                        input this-procedure).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_P_Fulfillment_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_P_Fulfillment_Report wWin
ON CHOOSE OF MENU-ITEM m_P_Fulfillment_Report /* Fulfillment Report */
do:
  run wfulfillmentrpt-person.w persistent(input {&PersonCode}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_P_Pending_Review_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_P_Pending_Review_Report wWin
ON CHOOSE OF MENU-ITEM m_P_Pending_Review_Report /* Qualifications Pending Review Utility */
do:
  run wpendingreviews-person.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_P_Qualifications_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_P_Qualifications_Report wWin
ON CHOOSE OF MENU-ITEM m_P_Qualifications_Report /* Qualifications Report */
do:
  run wactivequalrpt-person.w persistent.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Requirements_Setup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Requirements_Setup wWin
ON CHOOSE OF MENU-ITEM m_Requirements_Setup /* State Requirements */
do:
  run wstaterequirement.w persistent.   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Agent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Agent wWin
ON CHOOSE OF MENU-ITEM m_View_Agent /* View Agent */
DO:
  if not available ttagent 
   then return.
    
  openWindowForAgentAction(ttagent.AgentID, "detail", "wagent.w", "E").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Attorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Attorney wWin
ON CHOOSE OF MENU-ITEM m_View_Attorney /* View Attorney */
DO:
  if not available ttattorney
   then
    return.
  
  empty temp-table tAttorney.
  
  if ttAttorney.orgID ne "" and  ttAttorney.orgID ne ? 
   then
    run OpenAttorneyFirm in this-procedure.
   else 
    run OpenAttorney in this-procedure.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Fulfillments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Fulfillments wWin
ON CHOOSE OF MENU-ITEM m_View_Fulfillments /* View Fulfillments */
DO:
  if available ttagent
   then
    run OpenAgentRole  in this-procedure (input ttagent.agentID).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Org
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Org wWin
ON CHOOSE OF MENU-ITEM m_View_Org /* View Organization */
DO:
  if not available ttorganization or ttorganization.orgid = "" or ttorganization.orgid = "0"
   then return.
    
  run openOrganization in this-procedure (input ttorganization.orgid,
                                          input 2,     /* tab no */
                                          input "").   /* agent ID */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Organization
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Organization wWin
ON CHOOSE OF MENU-ITEM m_View_Organization /* View Organization */
DO:
  if not available ttagent or ttagent.orgid = "" or ttagent.orgid = "0"
   then return.
  
  run openOrganization in this-procedure (input ttagent.orgid,
                                          input 3,                /* tab no */
                                          input ttagent.agentID). /* agent ID */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Per
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Per wWin
ON CHOOSE OF MENU-ITEM m_View_Per /* View Person */
DO:
  if available ttperson
   then
    apply "default-action" to brwdatapeople in frame frPeople.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Person wWin
ON CHOOSE OF MENU-ITEM m_View_Person /* View Person */
DO:
  if  ttAttorney.orgID ne "" and  ttAttorney.orgID ne ?   
   then
    do:
      if not available ttattorney or ttattorney.orgID = ""
       then return.
  
     run openOrganization in this-procedure (input ttattorney.orgID,
                                             input 4,                      /* tab no */
                                             input ttattorney.attorneyID). /* attorney ID */
   end.
  else 
   do:
     if not available ttattorney or ttattorney.personID = ""
       then return.
  
      run openPerson in this-procedure (input ttattorney.personID,
                                        input 4,                      /* tab no */
                                        input ttattorney.attorneyID). /* attorney ID */
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Person_Fulfillments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Person_Fulfillments wWin
ON CHOOSE OF MENU-ITEM m_View_Person_Fulfillments /* View Fulfillments */
DO:
  if available ttattorney
   then
    do:
      if ttAttorney.orgId = ""  
       then
        run OpenAttorneyRole  in this-procedure (input ttattorney.attorneyID).
      else if ttAttorney.personId = "" 
       then
        run OpenAttorneyFirmRole  in this-procedure (input ttattorney.attorneyID).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Search_Key_Ag
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Search_Key_Ag wWin
ON CHOOSE OF MENU-ITEM m_View_Search_Key_Ag /* View Search Keys */
DO:
  if available ttagent
   then
    run dialogcomplianceindex.w (input {&Agent} ,
                                 input ttagent.agentId).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Search_Key_Att
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Search_Key_Att wWin
ON CHOOSE OF MENU-ITEM m_View_Search_Key_Att /* View Search Keys */
DO:
  if available ttattorney
   then
    run dialogcomplianceindex.w (input {&Attorney} ,
                                 input ttattorney.attorneyId).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Search_Key_Org
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Search_Key_Org wWin
ON CHOOSE OF MENU-ITEM m_View_Search_Key_Org /* View Search Keys */
DO:
  if available ttorganization
   then
    run dialogcomplianceindex.w( input {&Organization} ,
                                 input  ttorganization.orgid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Search_Key_Person
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Search_Key_Person wWin
ON CHOOSE OF MENU-ITEM m_View_Search_Key_Person /* View Search Keys */
DO:
  if available ttperson
   then
    run dialogcomplianceindex.w (input {&Person},
                                 input ttperson.Personid).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Types
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Types wWin
ON CHOOSE OF MENU-ITEM m_View_Types /* Setup Codes */
do:
  publish "openWindow" (input "ComplianceCodes",
                        input "",
                        input "wcompliancecodes.w",
                        input ?,
                        input this-procedure).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME States
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL States wWin
ON CHOOSE OF MENU-ITEM States /* State Maintenance */
do:
  publish "openWindow" (input "ReferenceState",
                        input "",
                        input "referencestate.w",
                        input ?,
                        input this-procedure).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDataAgent
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main-multi.i &browse-list="brwDataOrg,brwDataAgent,brwDataPeople,brwDataAttorney"}

   
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
 on close of this-procedure  
  run disable_UI.                                                               

    btSearch        :load-image ("images/completed.bmp").             
    btSearch        :load-image-insensitive("images/completed-i.bmp").
    btClear         :load-image ("images/erase.bmp").
    btClear         :load-image-insensitive("images/erase-i.bmp"). 
    btFindOrg       :load-image("images/s-lookup.bmp").  
    btFindOrg       :load-image-insensitive("images/s-lookup-i.bmp").
    btFindAgent     :load-image("images/s-lookup.bmp").  
    btFindAgent     :load-image-insensitive("images/s-lookup-i.bmp").
    btFindAttorney  :load-image("images/s-lookup.bmp").  
    btFindAttorney  :load-image-insensitive("images/s-lookup-i.bmp").
    btFindPeople    :load-image("images/s-lookup.bmp").  
    btFindPeople    :load-image-insensitive("images/s-lookup-i.bmp").
                   
    
    /* Best default for GUI applications is...                              */
  pause 0 before-hide.

  subscribe to "filepath" anywhere .
  subscribe to "fulfillmentModified" anywhere .
  subscribe to "statusModified"      anywhere .
  subscribe to "ActionWindowForAgent" anywhere.
  
  publish "GetSysPropList" ("AMD", "Agent", "Status", output std-ch).
  
  std-ch =     replace(std-ch,'^',',').
  cAgentStatList = std-ch.
  do std-in = 1 to num-entries(std-ch):
    cAgentStat = cAgentStat + " " + entry(std-in + 1,std-ch) + "=" + entry(std-in, std-ch ) + ";" no-error.
    std-in = std-in + 1.
  end.
  
  publish "GetSysPropList" ("COM", "Attorney", "Status", output std-ch).
  std-ch =     replace(std-ch,'^',',').
  cAttorneyStatList=std-ch.
  do std-in = 1 to num-entries(std-ch):
    cAttorneyStat = cAttorneyStat + " " + entry(std-in + 1, std-ch) + "=" + entry(std-in, std-ch ) + ";" no-error.
    std-in = std-in + 1.
  end.
  
  do std-in = 1 to num-entries(clist):
    cStat = cStat + " " + entry(std-in + 1, clist) + "=" + entry(std-in, clist ) + ";" no-error.
    std-in = std-in + 1.
  end.
      
  run enable_UI.
  
 
  {lib/get-column-width.i &col="'name'"      &var=dOrganization  &browse-name=brwdataOrg        }
  {lib/get-column-width.i &col="'legalname'" &var=dAgent         &browse-name=brwdataAgent        }
  {lib/get-column-width.i &col="'name'"      &var=dPeople        &browse-name=brwdataPeople        }
  {lib/get-column-width.i &col="'name'"      &var=dAttorney      &browse-name=brwdataAttorney        }
  

  run initializeObject in this-procedure.
  run selectPage       in this-procedure (1). 
  run showWindow       in this-procedure.

  setStatusMessage("").

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionConfig wWin 
PROCEDURE ActionConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pError  as logical initial true no-undo.

  run dialogconfig.w.
  
  if return-value = "no" 
   then
    return.
  
  /* display the screen based on changed config settings. */
  run displayConfigurations in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionImportPeople wWin 
PROCEDURE ActionImportPeople :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Procedure to call dialog import which loads person data from a file
         and send it to server.       
------------------------------------------------------------------------------*/
  run util/importdialog.w 
      (input "NPN,ALTAUID,Title,First Name,Middle Name,Last Name,Display Name,Address1,Address2,City,State,County,Zip,Email,Phone,Mobile,Notes",
       input "CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH,CH",
       input "",
       input "ActionImportPerson"). /* Internal procedure called to import each record */

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionImportPerson wWin 
PROCEDURE ActionImportPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table     for importdata.
  define output parameter pSuccess  as logical initial true.
  define output parameter pErrorMsg as char    initial "".

  define variable  opiPersonID   as integer   no-undo.
  define variable  lCheck        as logical   no-undo.
 
  define buffer person for person.

  empty temp-table tempperson.

  for each importData:
    create tempPerson.
    assign 
        tempPerson.NPN           = importData.data[1]
        tempPerson.altaUID       = importData.data[2]
        tempPerson.firstName     = importData.data[4]
        tempPerson.middleName    = importData.data[5]
        tempPerson.lastName      = importData.data[6]
        tempPerson.dispName      = importData.data[7]
        tempPerson.address1      = importData.data[8]
        tempPerson.address2      = importData.data[9]
        tempPerson.city          = importData.data[10]
        tempPerson.state         = importData.data[11]
        tempPerson.zip           = importData.data[13]
        tempPerson.contactEmail  = importData.data[14]
        tempPerson.contactPhone  = importData.data[15]
        tempPerson.contactMobile = importData.data[16]
        tempPerson.notes         = importData.data[17]
        .
  end.   
  
  for each tempPerson:
    empty temp-table person.
    create person.
    buffer-copy tempPerson to person.
    /* Server Call */

    publish "newPerson" (input-output table person,
                         input-output lCheck, /* Duplicate Check */
                         output opiPersonID,  /* New PersonId */
                         output std-lo,
                         output std-ch).

    if std-ch <> "" or not std-lo or opiPersonID = 0
     then
      do:
        create tErrorPerson.
        buffer-copy tempPerson to tErrorPerson.
        tErrorPerson.tempError = std-ch.
      end.
  end. /* for each tempPerson */
                                 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewAgent wWin 
PROCEDURE ActionNewAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
openWindowForAgentAction("", "agentDetails", "dialogagent.w", "N").

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewAttorney wWin 
PROCEDURE ActionNewAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frAttorneys:
  end.
                       
  define variable iCount  as integer no-undo.
  define variable cAttorneyId as character no-undo.
  
  empty temp-table tattorney.
 
  if attorneyType:input-value = {&OrganizationCode}
   then
     run dialogattorneyfirm.w(input table tattorney,
                              input {&ActionAdd},
                              input "",
                              input "",
                              output cAttorneyId,
                              output std-lo).
   else 
    run dialogattorney.w(input table tattorney,
                         input {&ActionAdd},
                         input "",
                         input "",
                         output cAttorneyId,
                         output std-lo).
                       
  if not std-lo  
   then
    return.
          
  message "New Attorney created with ID: " + cAttorneyId
    view-as alert-box info buttons ok.       
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewOrganization wWin 
PROCEDURE ActionNewOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable opcOrgID as character no-undo.

  run dialogneworganization.w (output opcOrgID,
                               output std-lo).
                                                        
  if not std-lo 
   then
    return.
    
  message "New Organization created with ID: " + opcOrgID
      view-as alert-box info buttons ok.                              

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewPerson wWin 
PROCEDURE ActionNewPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define variable cPersonId as character no-undo.  

  run dialognewperson.w(input '',
                        output cPersonId,
                        output std-lo).   
  
  if not std-lo 
   then
    return.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent wWin 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'adm2/folder.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'FolderLabels':U + 'Organizations|Agents|People|Attorneys' + 'FolderTabWidth30FolderFont-1HideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_folder ).
       RUN repositionObject IN h_folder ( 5.76 , 2.00 ) NO-ERROR.
       RUN resizeObject IN h_folder ( 21.91 , 193.00 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN addLink ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjustTabOrder ( h_folder ,
             flFindOrg:HANDLE IN FRAME fMain , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow wWin 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.
  return no-apply.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayConfigurations wWin 
PROCEDURE displayConfigurations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStateId as character  no-undo.

  do with frame {&frame-name}:
  end.

  /* Get the selected states from the config settings. */
  publish "GetSearchStates" (output std-ch).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbRoleTypes cbState flSearch flFindPeople flFindAgent flFindAttorney 
          flFindOrg 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE cbRoleTypes btClear btFindOrg btFindPeople btSearch btFindAgent 
         btFindAttorney cbState flSearch flFindPeople flFindAgent 
         flFindAttorney flFindOrg SearchSepRectangle ViewAtt 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  ENABLE brwDataAgent btNewAgent 
      WITH FRAME frAgents IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAgents}
  DISPLAY attorneyType 
      WITH FRAME frAttorneys IN WINDOW wWin.
  ENABLE attorneyType brwDataAttorney btNewAttorney 
      WITH FRAME frAttorneys IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frAttorneys}
  ENABLE brwDataOrg btNewOrg 
      WITH FRAME frOrganization IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frOrganization}
  ENABLE brwDataPeople btNewPeople 
      WITH FRAME frPeople IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-frPeople}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/
  apply "CLOSE":U to this-procedure.
  return.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData wWin 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter haBrowser  as handle    no-undo.
  define input parameter haTable    as handle    no-undo.
  define input parameter cTableName as character no-undo.
  define input parameter cFileName  as character no-undo.
  define input parameter cQuery     as character no-undo.
  
  define variable cFields    as character no-undo.
  define variable cFieldName as character no-undo.
  
  /* Get column and field name of a browser */
  do std-in = 1 to haBrowser:num-columns:
    std-ha = haBrowser:get-browse-column(std-in).
    
    cFields    = cFields    + (if cFields    > "" then "," else "")  + std-ha:name.
    if  cFileName = {&Agent} or cFileName = {&Attorney}
     then
      cFieldName = cFieldName + (if cFieldName > "" then "," else "ComStatus")  + std-ha:label.
    else
     cFieldName = cFieldName + (if cFieldName > "" then "," else "")  + std-ha:label.
    
  end. 
    
  if haBrowser:query:num-results = ? or haBrowser:query:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.
 

  publish "GetReportDir" (output std-ch).
 
  run util/exporttable.p (table-handle haTable,
                          cTableName,
                          cQuery,
                          cFields,
                          cFieldName,
                          std-ch,
                          cFileName + "-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData wWin 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frAttorneys:
  end.
  
  define variable tAddress as character no-undo.
  
  close query brwDataAttorney.
  
  empty temp-table ttattorney.
  std-in = 0.
  
  for each Attorney by attorney.seq:
   if attorneyType:input-value = "O" then
    do:
      if (Attorney.entityID eq "" or attorney.entityID eq ?) or (Attorney.personID ne "" and attorney.personID ne ?)
       then
        next.
    end.
   else if attorneyType:input-value = "P" then
    do:
      if (Attorney.entityID eq "" or attorney.entityID eq ?) or (Attorney.orgID ne "" and attorney.orgID ne ?) 
       then
        next.
    end.
   else if attorneyType:input-value = "N" then
    do:
      if (Attorney.entityID ne "" and attorney.entity ne ?)
       then
        next.
    end.
    
    tAddress = "" . 
    create ttAttorney.
    buffer-copy attorney to ttAttorney.
    assign
        ttAttorney.ComSeq =  if ttAttorney.comstat = {&Gcode} then "1"
                        else if ttAttorney.comstat = {&Ycode} then "2"
                        else if ttAttorney.comstat = {&Rcode} then "3"
                        else "4"
        tAddress          =  if (attorney.addr1 = "?") or (attorney.addr1 = "") then "" else attorney.addr1
                tAddress  =  tAddress + if (attorney.addr2 = "?") or (attorney.addr2 = "") then "" else ", " + attorney.addr2
                tAddress  =  tAddress + if (attorney.city  = "?") or (attorney.city = "")  then "" else ", " + attorney.city
                tAddress  =  tAddress + if (attorney.state = "?") or (attorney.state = "") then "" else ", " +  attorney.state
                tAddress  =  tAddress + if (attorney.zip   = "?") or (attorney.zip = "")   then "" else " " +  attorney.zip
                tAddress  =  trim(tAddress,',')
         ttAttorney.address  =  trim(tAddress).  
         std-in = std-in + 1.
  end.
  
  open query brwDataAttorney preselect each ttattorney.
  
  /* Display the record count after applying the filter. */
  setStatusCount(query brwDataAttorney:num-results).  
  apply "value-changed"  to brwDataAttorney.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fulfillmentModified wWin 
PROCEDURE fulfillmentModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:   if fullfilment are modified then qualification will also get modified 
           fullfilment can be modified by agent,company or attorney
           so person need to refresh if any of then update fullfilments
------------------------------------------------------------------------------*/
  define input parameter ipcEntityID  as character no-undo.

  if (if available ttattorney then ttattorney.personID = ipcEntityID  else no)
   then
    do:
      if not valid-handle(hPersonDataSrv) then
        run persondatasrv.p persistent set hPersonDataSrv(input ttattorney.personID).
        
      run refreshPersonDetails in hPersonDataSrv (input  yes, /*  NeedQualification   */ 
                                                  input  yes, /*  NeedPerson          */  
                                                  input  no,  /*  Need personagent    */
                                                  input  yes, /*  NeedPersonrole      */
                                                  input  yes, /*  NeedReqfulfillments */
                                                  input  no,  /*  NeedPersonContact   */
                                                  input  no,  /*  NeedPersonagent     */
                                                  output std-lo,
                                                  output std-ch).
      if not std-lo 
       then
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end. 
       
   end. 
   else if (if available ttattorney then ttattorney.orgID = ipcEntityID  else no)
   then
    do:
      if not valid-handle(hOrganizationDataSrv) then
        run organizationdatasrv.p persistent set hOrganizationDataSrv(input ttattorney.orgID).
        
      run refreshOrganizationDetails in hOrganizationDataSrv (input  yes, /*  NeedQualification  */ 
                                                  input  yes,   /*  NeedPerson  */        
                                                  input  yes,    /*  NeedPersonrole */
                                                  input  yes,     /*  NeedReqfulfillments*/
                                                  output std-lo,
                                                  output std-ch).
      if not std-lo 
       then
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end. 
       
   end. 
   else if ( if available ttagent then ttagent.OrgId = ipcEntityID else no )
    then
     do:
       if not valid-handle(hOrganizationDataSrv) then
        run organizationdatasrv.p persistent set hOrganizationDataSrv(input ttagent.orgID).
        
       run refreshOrganizationDetails in hOrganizationDataSrv (input  yes, /*  NeedQualification  */ 
                                                               input  yes,   /*  NeedPerson  */        
                                                               input  yes,    /*  NeedPersonrole */
                                                               input  yes,     /*  NeedReqfulfillments*/
                                                               output std-lo,
                                                               output std-ch).
       if not std-lo 
        then
         do:
           message std-ch
             view-as alert-box error buttons ok.
           return.
         end. 

     end.
         
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData wWin 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcEntity as character no-undo.
  define input parameter ipcSearch as character no-undo.
  
  do with frame {&frame-name}:
  end.

  do with frame frPeople:
  end.
  do with frame frOrganization:
  end.
  do with frame frAgents:
  end.
  do with frame frAttorneys:
  end.
  
  define variable tAddress as character no-undo.
  
  /* Save the recently search string. */
     saveSearchValue().
  
  
  if ipcEntity = {&OrganizationCode}
   then
    do:
      clearData(input {&OrganizationCode}).

      empty temp-table ttOrganization.

      assign
          std-in = 0
          cOrgSearch = ipcSearch.
      empty temp-table organization. /* Not needed, but feels correct */
  
      run server/searchorganizations.p (validatesearch(ipcSearch),
                                        cbState:input-value,
                                        cbRoleTypes:screen-value,
                                        output table organization,
                                        output std-lo,
                                        output std-ch).
      if std-lo 
       then 
        do:
          for each organization by organization.seq:
            tAddress = "" .
            create ttOrganization.
            buffer-copy organization to ttOrganization.
            assign
                tAddress =  if (ttorganization.addr1 = "?") or (ttorganization.addr1 = "") then "" else ttorganization.addr1
                tAddress =  tAddress + if (ttorganization.addr2 = "?") or (ttorganization.addr2 = "") then "" else ", " + ttorganization.addr2
                tAddress =  tAddress + if (ttorganization.city  = "?") or (ttorganization.city = "")  then "" else ", " + ttorganization.city
                tAddress =  tAddress + if (ttorganization.state = "?") or (ttorganization.state = "") then "" else ", " +  ttorganization.state
                tAddress =  tAddress + if (ttorganization.zip   = "?") or (ttorganization.zip = "")   then "" else " " +  ttorganization.zip
                tAddress  =  trim(tAddress,',')
                ttorganization.Address  =  trim(tAddress)
                .  
            std-in = std-in + 1.
          end. /* for each organization by organization.seq: */
        end. /* if std-lo */
      else 
       if std-ch <> "" 
        then
         message std-ch view-as alert-box error buttons ok.
    
      close query brwDataOrg.

      open query brwDataOrg preselect each ttOrganization  by ttOrganization.matchPercent desc by ttorganization.name .
      apply "value-changed"  to brwDataOrg .
      assign
           btOpenOrg:sensitive in frame frOrganization     = (query brwDataOrg:num-results > 0)
           btRefreshOrg:sensitive in frame frOrganization  = (query brwDataOrg:num-results > 0)
           btExportOrg:sensitive in frame frOrganization   = (query brwDataOrg:num-results > 0)
           brwDataOrg :sensitive in frame frOrganization   = (query brwDataOrg:num-results > 0)
           frame frOrganization :Title                     =  if can-do(cList,getStatDesc(validatesearch(ipcSearch))) 
                                                              then 
                                                               getStatDesc(validatesearch(ipcSearch)) + " Organizations"
                                                             else
                                                              "Organizations that match " + '"' + validatesearch(ipcSearch) + '"'.
 
 /* Display no. of records with date and time on status bar */
      setStatusRecords(query brwDataOrg:num-results).
      cOrgRole = cbRoleTypes:screen-value. 
   end. /* if ipcEntity = {&OrganizationCode} */
  else if ipcEntity = {&AgentCode}
   then
    do:
      clearData(input {&AgentCode}).

      empty temp-table ttAgent.

      assign
          std-in = 0
          cAgentSearch = ipcSearch.
          
      empty temp-table agent. /* Not needed, but feels correct */
  
      run server/searchagents.p (cbState:input-value,
                                 validatesearch(ipcSearch),
                                 output table agent,
                                 output std-lo,
                                 output std-ch).
      if std-lo 
       then 
        do:
          for each agent by agent.seq:
            tAddress = "".
            create ttAgent.
            buffer-copy agent to ttAgent.
            assign
                ttAgent.ComSeq =          if ttAgent.comstat = {&Gcode} then "1"
                                 else if ttAgent.comstat = {&Ycode} then "2"
                                 else if ttAgent.comstat = {&Rcode} then "3"
                                 else "4"
            
                
                tAddress =  if (ttagent.addr1 = "?") or (ttagent.addr1 = "") then "" else ttagent.addr1
                tAddress =  tAddress + if (ttagent.addr2 = "?") or (ttagent.addr2 = "") then "" else  ", " + ttagent.addr2
                tAddress =  tAddress + if (ttagent.addr3 = "?") or (ttagent.addr3 = "") then "" else  ", " + ttagent.addr3
                tAddress =  tAddress + if (ttagent.addr4 = "?") or (ttagent.addr4 = "") then "" else  ", " + ttagent.addr4
                tAddress =  tAddress + if (ttagent.city = "?")  or (ttagent.city = "")  then "" else  ", " + ttagent.city
                tAddress =  tAddress + if (ttagent.state = "?") or (ttagent.state = "") then "" else  ", " +  ttagent.state
                tAddress =  tAddress + if (ttagent.zip = "?")   or (ttagent.zip = "")   then "" else  " " +  ttagent.zip
                tAddress =  trim(tAddress,',')
                ttagent.Address  =  trim(tAddress) 
                . 
                                          
            std-in = std-in + 1.
          end. /* for each agent by agent.seq: */
        end. /* if std-lo */
      else 
       if std-ch <> "" 
        then
         message std-ch view-as alert-box error buttons ok.
    
      close query brwDataAgent.

      open query brwDataAgent preselect each ttAgent by ttagent.matchPercent desc by ttagent.legalName .
      apply "value-changed"  to  brwDataAgent.
      
      assign
           btRefreshAgent:sensitive in frame frAgents = (query brwDataAgent:num-results > 0)
           btOpenAgent  :sensitive in frame frAgents  = (query brwDataAgent:num-results > 0)
           btExportAgent:sensitive in frame frAgents  = (query brwDataAgent:num-results > 0)
           brwDataAgent :sensitive in frame frAgents  = (query brwDataAgent:num-results > 0)
           frame frAgents:Title                       =  if can-do(cAgentStatList,getStatDesc(validatesearch(ipcSearch)))  
                                                          then  
                                                           getStatDesc(validatesearch(ipcSearch)) + " Agents" 
                                                         else 
                                                         "Agents that match " + '"' + validatesearch(ipcSearch) + '"'
           .

      /* Display no. of records with date and time on status bar */
      setStatusRecords(query brwDataAgent:num-results).
      
    end. /* else if ipcEntity = {&AgentCode} */
  else if ipcEntity = {&PersonCode}
   then
    do:
      
  
      clearData(input {&PersonCode}).

      empty temp-table ttPerson.

      assign
          std-in = 0
          cPeopleSearch = ipcSearch.
          
      empty temp-table person. /* Not needed, but feels correct */
  
      run server/searchPersons.p(cbState:input-value,
                                 validatesearch(ipcSearch),
                                 cbRoleTypes:screen-value, /*personRole type is taken from screen-value otherwise it should be "" */
                                 "",
                                 "",
                                 output table person,
                                 output std-lo,
                                 output std-ch).
  
      if std-lo 
       then 
        do:
          for each person by person.seq:
            tAddress = "" .
            create ttPerson.
            buffer-copy person to ttPerson.
            assign
                tAddress =  if (ttperson.address1 = "?")            or (ttperson.address1 = "") then "" else ttperson.address1
                tAddress =  tAddress + if (ttperson.address2 = "?") or (ttperson.address2 = "") then "" else ", " + ttperson.address2
                tAddress =  tAddress + if (ttperson.city = "?")     or (ttperson.city = "")     then "" else ", " + ttperson.city
                tAddress =  tAddress + if (ttperson.state = "?")    or (ttperson.state = "")    then "" else ", " +  ttperson.state
                tAddress =  tAddress + if (ttperson.zip = "?")      or (ttperson.zip = "")      then "" else " " +  ttperson.zip
                tAddress =  trim(tAddress,',')
                ttperson.Address  =  trim(tAddress)
                .
            std-in = std-in + 1.
          end. /* for each person by person.seq: */
        end. /* if std-lo */
      else 
       if std-ch <> "" 
        then
         message std-ch view-as alert-box error buttons ok.
    
      close query brwDataPeople.

      open query brwDataPeople preselect each ttPerson by ttperson.matchPercent desc by ttperson.dispname .
      assign
           btRefreshPeople:sensitive in frame frPeople  = (query brwDataPeople:num-results > 0)
           btOpenPeople  :sensitive in frame frPeople   = (query brwDataPeople:num-results > 0)
           btExportPeople:sensitive in frame frPeople   = (query brwDataPeople:num-results > 0)
           brwDataPeople :sensitive in frame frPeople   = (query brwDataPeople:num-results > 0)
           frame frPeople:Title                         =  if can-do(cList,getStatDesc(validatesearch(ipcSearch))) 
                                                            then 
                                                             getStatDesc(validatesearch(ipcSearch)) + " People"
                                                           else
                                                            "People that match " + '"' + validatesearch(ipcSearch) + '"'
           .
      
      apply "value-changed"  to brwDataPeople .
     /* Display no. of records with date and time on status bar */
      setStatusRecords(query brwDataPeople:num-results).
      cPeopleRole = cbRoleTypes:screen-value.  
  
    end. /* else if ipcEntity = {&PersonCode} */
  else if ipcEntity = {&AttorneyCode}
   then
    do:
      clearData(input {&AttorneyCode}).

      empty temp-table ttAttorney.

      assign
          std-in = 0
          cAttorneySearch = ipcSearch.
          
      empty temp-table attorney. /* Not needed, but feels correct */
  
      run server/searchAttorneys.p(cbState:input-value,
                                   validatesearch(ipcSearch),           
                                   output table attorney,
                                   output std-lo,
                                   output std-ch).
      for each attorney:
        assign
            attorney.entity     = if (attorney.orgID ne "" and attorney.orgID ne ?) then {&OrganizationCode} else 
                                  if (attorney.personID ne "" and attorney.personID ne ?) then {&PersonCode} else ""
            attorney.entityID   = if (attorney.orgID ne "" and attorney.orgID ne ?) then attorney.orgID      else 
                                  if (attorney.personID ne "" and attorney.personID ne ?) then attorney.personID else ""
            attorney.entityName = if attorney.entity = {&OrganizationCode} then attorney.firmname else 
                                  if attorney.entity = {&PersonCode}       then attorney.name1 else (if (attorney.name1 ne "" and attorney.name1 ne ?) then attorney.name1   
                                                                                                     else attorney.firmname) 
            .
      end. 
      if std-lo 
       then 
        do:
          for each attorney by attorney.seq:
            tAddress = "" .
            create ttAttorney.
            buffer-copy attorney to ttAttorney.
            assign
                ttAttorney.ComSeq =       if ttAttorney.comstat = {&Gcode} then "1"
                                     else if ttAttorney.comstat = {&Ycode} then "2"
                                     else if ttAttorney.comstat = {&Rcode} then "3"
                                     else "4"
                 
                tAddress =  if (ttAttorney.addr1 = "?") or (ttAttorney.addr1 = "") then "" else ttAttorney.addr1
                tAddress =  tAddress + if (ttAttorney.addr2 = "?") or (ttAttorney.addr2 = "") then "" else ", " + ttAttorney.addr2
                tAddress =  tAddress + if (ttAttorney.city = "?")  or (ttAttorney.city = "")  then "" else ", " + ttAttorney.city
                tAddress =  tAddress + if (ttAttorney.state = "?") or (ttAttorney.state = "") then "" else ", " +  ttAttorney.state
                tAddress =  tAddress + if (ttAttorney.zip = "?")   or (ttAttorney.zip = "")   then "" else " "  +  ttAttorney.zip
                tAddress =  trim(tAddress,',')
                ttAttorney.Address  =  trim(tAddress)
                . 
            
         std-in = std-in + 1.
          end. /* for each attorney by attorney.seq: */
        end. /* if std-lo */
      else 
       if std-ch <> "" 
        then
         message std-ch view-as alert-box error buttons ok.
     
      close query brwDataAttorney.

      open query brwDataAttorney preselect each ttAttorney by ttattorney.matchPercent desc by ttattorney.name1.
      apply "value-changed"  to  brwDataAttorney.
      
      assign
           btRefreshAttorney:sensitive in frame frAttorneys  = (query brwDataAttorney:num-results > 0)
           btOpenAttorney  :sensitive in frame frAttorneys   = (query brwDataAttorney:num-results > 0)
           btExportAttorney:sensitive in frame frAttorneys   = (query brwDataAttorney:num-results > 0)
           brwDataAttorney :sensitive in frame frAttorneys   = (query brwDataAttorney:num-results > 0)
           frame frAttorneys:Title = if can-do(cAttorneyStatList,getStatDesc(validatesearch(ipcSearch)))  
                                      then 
                                       getStatDesc(validatesearch(ipcSearch)) + " Attorneys"
                                     else
                                      "Attorneys that match " + '"' + validatesearch(validatesearch(ipcSearch)) + '"'
           .
  
      /* Display no. of records with date and time on status bar */
      setStatusRecords(query brwDataAttorney:num-results).
    end.     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeObject wWin 
PROCEDURE initializeObject :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  /* Code placed here will execute PRIOR to standard behavior. */
  {&window-name}:window-state = 2. /* Minimized */

  run super.

  {&window-name}:max-width-pixels  = session:width-pixels.
  {&window-name}:max-height-pixels = session:height-pixels.
  {&window-name}:min-width-pixels  = {&window-name}:width-pixels.
  {&window-name}:min-height-pixels = {&window-name}:height-pixels.

  do with frame frOrganization:
    btOpenOrg               :load-image("images/s-magnifier.bmp").
    btOpenOrg               :load-image-insensitive("images/s-magnifier-i.bmp").
    btExportOrg             :load-image("images/s-excel.bmp").
    btExportOrg             :load-image-insensitive("images/s-excel-i.bmp").
    btNewOrg                :load-image("images/s-add.bmp").
    btNewOrg                :load-image-insensitive("images/s-add-i.bmp").
    btRefreshOrg            :load-image("images/s-refresh.bmp").
    btRefreshOrg            :load-image-insensitive("images/s-refresh-i.bmp").
  end.

  do with frame frAgents:
    btOpenAgent             :load-image("images/s-magnifier.bmp").
    btOpenAgent             :load-image-insensitive("images/s-magnifier-i.bmp").
    btExportAgent           :load-image("images/s-excel.bmp").
    btExportAgent           :load-image-insensitive("images/s-excel-i.bmp").
    btNewAgent              :load-image("images/s-add.bmp").
    btNewAgent              :load-image-insensitive("images/s-add-i.bmp").
    btRefreshAgent          :load-image("images/s-refresh.bmp").
    btRefreshAgent          :load-image-insensitive("images/s-refresh-i.bmp").
    
  end.
  do with frame frPeople:
    btOpenPeople             :load-image("images/s-magnifier.bmp").
    btOpenPeople             :load-image-insensitive("images/s-magnifier-i.bmp").
    btExportPeople           :load-image("images/s-excel.bmp").
    btExportPeople           :load-image-insensitive("images/s-excel-i.bmp").
    btNewPeople              :load-image("images/s-add.bmp").
    btNewPeople              :load-image-insensitive("images/s-add-i.bmp").
    btRefreshPeople          :load-image("images/s-refresh.bmp").
    btRefreshPeople          :load-image-insensitive("images/s-refresh-i.bmp").
 
  end.
  
  do with frame frAttorneys:
    btOpenAttorney             :load-image("images/s-magnifier.bmp").
    btOpenAttorney             :load-image-insensitive("images/s-magnifier-i.bmp").
    btExportAttorney           :load-image("images/s-excel.bmp").
    btExportAttorney           :load-image-insensitive("images/s-excel-i.bmp").
    btNewAttorney              :load-image("images/s-add.bmp").
    btNewAttorney              :load-image-insensitive("images/s-add-i.bmp").
    btRefreshAttorney          :load-image("images/s-refresh.bmp").
    btRefreshAttorney          :load-image-insensitive("images/s-refresh-i.bmp").     
  end.
 
  do with frame {&frame-name}:
  end.
  run selectPage in this-procedure(1) .
  
  /* Get the selected states from the config settings. */
  publish "GetSearchStates" (output std-ch).
 

  if std-ch > "" 
   then
    std-ch = "," + std-ch.

  assign
      cbState:list-item-pairs in frame {&frame-name} = "ALL,ALL" + std-ch
      cbState:screen-value     = /*if lookup(cStateId,cbState:list-item-pairs) > 0 then cStateId else */ "ALL" 
      . 
 
  run showWindow    in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAgentRole wWin 
PROCEDURE OpenAgentRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define input parameter cagentId as character no-undo.
   
   if not can-find(first ttagent where ttagent.agentID = cagentID )
    then
      do:
        empty temp-table tagent. /* Not needed, but feels correct */
        
        run server/searchagents.p (input "ALL",
                                   input cagentId,
                                   output table tagent,
                                   output std-lo,
                                   output std-ch).
        if not std-lo 
         then 
          return.
        
        for first tagent where tagent.agentID = cagentID :
          create ttagent.
          buffer-copy tagent to ttagent.
        end.
      end.
      
   find first ttagent where ttagent.agentID = cagentID no-error.
   if available ttagent
    then
     do:
       if ttagent.orgId = "" or ttagent.orgRoleID = 0
        then
         do:
           openWindowForAgentAction(ttagent.AgentID, "detail", "wagent.w", "E") .
           return.
         end.
         
       run organizationdatasrv.p persistent set horganizationdatasrv (input ttagent.orgID).
        
       run fulFillmentModified in this-procedure (input ttagent.orgID).
        
       publish "OpenWindow" (input "Organizationrole",            /*childtype*/
                             input string(ttagent.orgroleid),   /*childid*/
                             input "worganizationrole.w" ,        /*window*/
                             input "handle|input|" + string(horganizationdatasrv) + "^integer|input|" + string(ttagent.orgroleid),
                             input this-procedure).         /*currentProcedure handle*/ 
     end.
   else
     do:
       message "The Agent with ID: " + cagentID + " does not exist." view-as alert-box error.
       return. 
     end.       
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAttorney wWin 
PROCEDURE OpenAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frAttorneys:
  end.
  
  define variable iCount  as integer no-undo.
  define variable cAttorneyID as character no-undo.

  run dialogattorney.w (input table tattorney,
                        input {&ActionEdit},
                        input ttattorney.attorneyID,
                        input string(ttattorney.personID),
                        output cAttorneyID,
                        output std-lo ).

  if not std-lo 
   then
    return.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAttorneyFirm wWin 
PROCEDURE OpenAttorneyFirm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frAttorneys:
  end.

  define variable iCount  as integer no-undo.
  define variable cAttorneyID as character no-undo.

  run dialogAttorneyFirm.w (input table tAttorney,
                            input {&ActionEdit},
                            input ttattorney.attorneyID,
                            input string(ttattorney.orgID),
                            output cAttorneyID,
                            output std-lo ).

  if not std-lo
   then
    return.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAttorneyFirmRole wWin 
PROCEDURE OpenAttorneyFirmRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cattorneyId as character no-undo.
  
  if not can-find(first ttattorney where ttattorney.attorneyID = cattorneyId )
    then
      do:
        empty temp-table tempattorney. /* Not needed, but feels correct */
        
        run server/searchattorneys.p (input "ALL",
                                      input cattorneyId,
                                      output table tempattorney,
                                      output std-lo,
                                      output std-ch).
        if not std-lo      
         then 
          return.
          
        for first tempattorney where tempattorney.attorneyID = cattorneyID :
          create ttattorney.
          buffer-copy tempattorney to ttattorney.
        end.
      end.
      
   find first ttattorney where ttattorney.attorneyID = cattorneyId no-error.
   if available ttattorney
    then
     do:
       if ttattorney.orgId = "" or ttattorney.orgroleID = 0 
        then
         do:
           run OpenAttorneyFirm in this-procedure.
           return.
         end.

       run organizationdatasrv.p persistent set hOrganizationDataSrv(input ttattorney.orgID).
       
       run fulFillmentModified in this-procedure (input ttattorney.orgID).
       
       publish "OpenWindow" (input "organizationrole",            /*childtype*/
                               input string(ttattorney.orgroleid),   /*childid*/
                               input "worganizationrole.w" ,        /*window*/
                               input "handle|input|" + string(hOrganizationDataSrv) + "^integer|input|" + string(ttattorney.orgroleid),
                               input this-procedure).         /*currentProcedure handle*/ 
     end.  

    else
     do:
       message "The Attorney with ID: " + cattorneyId + " does not exist." view-as alert-box error .
       return.
     end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenAttorneyRole wWin 
PROCEDURE OpenAttorneyRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cattorneyId as character no-undo.
  
  if not can-find(first ttattorney where ttattorney.attorneyID = cattorneyId )
    then
      do:
        empty temp-table tempattorney. /* Not needed, but feels correct */
        
        run server/searchattorneys.p (input "ALL",
                                      input cattorneyId,
                                      output table tempattorney,
                                      output std-lo,
                                      output std-ch).
        if not std-lo      
         then 
          return.
        
        for first tempattorney where tempattorney.attorneyID = cattorneyID :
          create ttattorney.
          buffer-copy tempattorney to ttattorney.
        end.
      end.
      
   find first ttattorney where ttattorney.attorneyID = cattorneyId no-error.
   if available ttattorney
    then
     do:
       if ttattorney.personId = "" or ttattorney.personroleID = 0 
        then
         do:
           run OpenAttorney in this-procedure.
           return.
         end.
   
       run persondatasrv.p persistent set hpersondatasrv(input ttattorney.personID).
       
       run fulFillmentModified in this-procedure (input ttattorney.personID).
       
       publish "OpenWindow" (input "personrole",            /*childtype*/
                               input string(ttattorney.personroleid),   /*childid*/
                               input "wpersonrole.w" ,        /*window*/
                               input "handle|input|" + string(hPersonDataSrv) + "^integer|input|" + string(ttattorney.personroleid),
                               input this-procedure).         /*currentProcedure handle*/ 
     end.  

    else
     do:
       message "The Attorney with ID: " + cattorneyId + " does not exist." view-as alert-box error .
       return.
     end.
     
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openOrganization wWin 
PROCEDURE openOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcOrgID   as character no-undo.
  define input parameter ipiPageNo  as integer   no-undo.
  define input parameter ipcAgentID as character no-undo.
     
  publish "OpenWindow" (input {&organization},
                        input ipcOrgID,
                        input "worganizationdetail.w",
                        input "character|input|" + ipcOrgID + "^integer|input|" + string(ipiPageNo) + "^character|input|" + ipcAgentID,
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openPerson wWin 
PROCEDURE openPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcPersonID    as character no-undo.
  define input parameter ipiPageNo      as integer   no-undo.
  define input parameter ipcAttorneyID  as character no-undo.
  
  publish "OpenWindow" (input {&person},
                        input ipcPersonID,
                        input "wpersondetail.w",
                        input "character|input|" + ipcPersonID + "^integer|input|" + string(ipiPageNo) + "^character|input|" + ipcAttorneyID,
                        input this-procedure).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectPage wWin 
PROCEDURE selectPage :
/*------------------------------------------------------------------------------
  Purpose:     Super Override
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter piPageNum as integer no-undo.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  run super(input piPageNum).
  do with frame fmain:
  end.
  
  case piPageNum:
   when 1 
    then
     do: 
       flSearch:tooltip = "Enter Criteria:" 
       + chr(10) + "Organization:" + cStat .
       flEnterCriteria:screen-value = "Shortcut: " + trim(cStat,';').
       ipageNo = 1.
       
       publish "getComplianceCodesList"(input {&OrganizationRole},
                                        input  "," ,
                                        output cRoles,
                                        output std-lo,
                                        output std-ch).
                                        
       if not std-lo
        then
         do:
           message std-ch 
             view-as alert-box error buttons ok.
         end.
                                   
       assign
           cbRoleTypes:hidden = false
           cbRoleTypes:list-items in frame {&frame-name} = "ALL" + "," + cRoles
           . 
           
       if cOrgRole = "" 
        then
         cbRoleTypes:screen-value     = "ALL".
        else
         cbRoleTypes:screen-value     = cOrgRole.
       
      if (query brwDataOrg:num-results in frame frOrganizations )  eq  0  or (query brwDataOrg:num-results in frame frOrganizations )  eq ? 
       then
        do:
          MENU-ITEM m_View_Search_Key_Org:sensitive IN MENU POPUP-MENU-brwDataOrg = false.
          MENU-ITEM m_View_Org:sensitive            IN MENU POPUP-MENU-brwDataOrg = false.
        end.
       view frame frOrganization.
       
       hide frame frAgents.
       hide frame frPeople.
       hide frame frAttorneys.
   
     end.
   when 2 
    then
     do:
      flSearch:tooltip = "Enter Criteria:" 
      + chr(10) + "Agent:" + cAgentStat. 
      flEnterCriteria:screen-value = "Shortcut: " + trim(cAttorneyStat,';').
      ipageNo = 2.
      
      if ipageNo = 2
       then
        cbRoleTypes:hidden = true.
          
      if  (query brwDataAgent:num-results in frame frAgents )  eq  0  or (query brwDataAgent:num-results in frame frAgents )  eq ? 
       then
        do:
          MENU-ITEM m_View_Agent:sensitive         IN MENU POPUP-MENU-brwDataAgent = false.
          MENU-ITEM m_View_Organization:sensitive  IN MENU POPUP-MENU-brwDataAgent = false.
          MENU-ITEM m_View_Fulfillments:sensitive  IN MENU POPUP-MENU-brwDataAgent = false.
          MENU-ITEM m_View_Search_Key_Ag:sensitive IN MENU POPUP-MENU-brwDataAgent = false.
        end.
      view frame frAgents .
   
      hide frame frOrganization.
      hide frame frPeople.
      hide frame frAttorneys.
      
     end.
   when 3
    then
     do:       
        flSearch:tooltip = "Enter Criteria:" 
        + chr(10) + "Person:" +  cStat .
       flEnterCriteria:screen-value = "Shortcut: " + trim(cStat,';') .
       ipageNo = 3.
       
       publish "getComplianceCodesList"(input {&PersonRole},
                                        input  "," ,
                                        output cPersonRoles,
                                        output std-lo,
                                        output std-ch).
                                        
       if not std-lo
        then
         do:
           message std-ch 
             view-as alert-box error buttons ok.
         end.                                   

       assign
           cbRoleTypes:hidden = false
           cbRoleTypes:list-items in frame {&frame-name} = "ALL" + "," + cPersonRoles 
           .
           
       if cPeopleRole = "" 
        then
         cbRoleTypes:screen-value     = "ALL".
        else
         cbRoleTypes:screen-value     = cPeopleRole.
     
     if not std-lo
      then
       do:
         message std-ch 
          view-as alert-box error buttons ok.
         return.
       end.
       
       if  (query brwDataPeople:num-results in frame frPeople )  eq  0  or  (query brwDataPeople:num-results in frame frPeople )  eq ? 
        then
         do:
           MENU-ITEM m_View_Search_Key_Person:sensitive IN MENU POPUP-MENU-brwDataPeople = false.
           MENU-ITEM m_View_Per:sensitive               IN MENU POPUP-MENU-brwDataPeople = false.
         end.
       view frame frPeople .
       
       hide frame frOrganization.        
       hide frame frAgents.
       hide frame frAttorneys.
       
     end.
     
   when 4
    then
     do:
       flSearch:tooltip = "Enter Criteria:" 
       + chr(10) + "Attorney:" + cAttorneyStat . 
       flEnterCriteria:screen-value = "Shortcut: " + trim(cAttorneyStat,';').
       ipageNo = 4.
       if ipageNo = 4
        then
         cbRoleTypes:hidden = true. 
         
       if ( query brwDataAttorney:num-results in frame frAttorney) eq 0   or ( query brwDataAttorney:num-results in frame frAttorney) eq ?
        then
         do:
           MENU-ITEM m_View_Attorney:sensitive            IN MENU POPUP-MENU-brwDataAttorney = false.
           MENU-ITEM m_View_Person:sensitive              IN MENU POPUP-MENU-brwDataAttorney = false.
           MENU-ITEM m_View_Person_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAttorney = false.
           MENU-ITEM m_View_Search_Key_Att:sensitive      IN MENU POPUP-MENU-brwDataAttorney = false.
         end.
       
       view frame frAttorneys .
      
       if attorneyType:input-value = "A" or attorneyType:input-value ="N" then
         assign btNewAttorney:sensitive = false.
       else 
         assign btNewAttorney:sensitive = true.
        
       hide frame frOrganization.        
       hide frame frAgents.
       hide frame frPeople.   
       
    end.
  end case.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow wWin 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
  {lib/brw-sortData-multi.i}
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE statusModified wWin 
PROCEDURE statusModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcentity as character no-undo.
  
  if ipcentity = {&Attorney}
   then
    apply "choose" to btRefreshAttorney in frame frAttorneys.
  else if ipcentity = {&Agent} 
   then
    apply "choose" to btRefreshAgent in frame frAgents.
  else
   return.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  assign
      frame fmain:width-pixels                              = {&window-name}:width-pixels
      frame fmain:virtual-width-pixels                      = {&window-name}:width-pixels
      frame fmain:height-pixels                             = {&window-name}:height-pixels
      frame fmain:virtual-height-pixels                     = {&window-name}:height-pixels
      .
      
  run resizeObject in h_folder (frame fMain:height - 3, frame fMain:width - 2). 

  assign
      
      frame frOrganization:width-pixels                        = {&window-name}:width-pixels  - 20 
      frame frOrganization:virtual-width-pixels                = {&window-name}:width-pixels  - 20  
      frame frOrganization:height-pixels                       = {&window-name}:height-pixels - 103.90     
      frame frOrganization:virtual-height-pixels               = {&window-name}:height-pixels - 103.90     
      
      
      frame frAgents:width-pixels                              = frame fmain:width-pixels    - 20 
      frame frAgents:virtual-width-pixels                      = frame fmain:width-pixels    - 20  
      frame frAgents:height-pixels                             = frame fmain:height-pixels   - 103.90 
      frame frAgents:virtual-height-pixels                     = frame fmain:height-pixels   - 103.90 
            
      frame frPeople:width-pixels                              = frame fmain:width-pixels  - 20 
      frame frPeople:virtual-width-pixels                      = frame fmain:width-pixels  - 20  
      frame frPeople:height-pixels                             = frame fmain:height-pixels - 103.90 
      frame frPeople:virtual-height-pixels                     = frame fmain:height-pixels - 103.90 
      
      
      frame frAttorneys:width-pixels                              = frame fmain:width-pixels  - 20 
      frame frAttorneys:virtual-width-pixels                      = frame fmain:width-pixels  - 20  
      frame frAttorneys:height-pixels                             = frame fmain:height-pixels - 103.90 
      frame frAttorneys:virtual-height-pixels                     = frame fmain:height-pixels - 103.90 
      
      brwDataOrg:width-pixels    in frame frOrganization    = frame frOrganization:width-pixels - 45
      brwDataOrg:height-pixels   in frame frOrganization    = frame frOrganization:height-pixels - brwDataOrg:y - 24
      brwDataAgent:width-pixels  in frame frAgents          = frame frAgents:width-pixels - 45
      brwDataAgent:height-pixels in frame frAgents          = frame frAgents:height-pixels - brwDataAgent:y - 24
      brwDataPeople:width-pixels in frame frPeople          = frame frPeople:width-pixels - 45
      brwDataPeople:height-pixels in frame frPeople         = frame frPeople:height-pixels - brwDataPeople:y - 24
      brwDataAttorney:width-pixels  in frame frAttorneys    = frame frAttorneys:width-pixels - 45
      brwDataAttorney:height-pixels in frame frAttorneys    = frame frAttorneys:height-pixels - brwDataAttorney:y - 24
      .

 
 /* resize the description column in the claims browse */
  {lib/resize-column.i &col="'name,address,roles,website'" &var=dorganization &browse-name=brwdataOrg}
  {lib/resize-column.i &col="'legalname,address'" &var=dAgent &browse-name=brwdataAgent}
  {lib/resize-column.i &col="'name,address'" &var=dPeople &browse-name=brwdataPeople}
  {lib/resize-column.i &col="'name,address'" &var=dAttorney &browse-name=brwdataAttorney}
  
  run ShowScrollBars(frame fmain:handle, no, no).
  run ShowScrollBars(frame frOrganization:handle, no, no).
  run ShowScrollBars(frame frPeople:handle, no, no).
  run ShowScrollBars(frame frAttorneys:handle, no, no).
  run ShowScrollBars(frame frAgents:handle, no, no).
        
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData wWin 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( input ipcEntity as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  case ipcEntity:
   when {&OrganizationCode} 
    then
     do:
       frame frOrganization  :Title =  "Organizations".
       close query brwDataOrg.
       empty temp-table ttOrganization.
  
       disable btOpenOrg btExportOrg btRefreshOrg with frame frOrganization.
  
       setStatusMessage("").
       return true.
     end.
   when {&AgentCode} 
    then
     do:
       frame frAgents:Title =  "Agents" .
       close query brwDataAgent.
       empty temp-table ttAgent.
  
       disable btOpenAgent btExportAgent btRefreshAgent with frame frAgents.
       if ( query brwDataAgent:num-results in frame frAgent) eq 0   or ( query brwDataAgent:num-results in frame frAgent) eq ?
       then
        do:
          MENU-ITEM m_View_Agent:sensitive        IN MENU POPUP-MENU-brwDataAgent = false.
          MENU-ITEM m_View_Organization:sensitive IN MENU POPUP-MENU-brwDataAgent = false.
          MENU-ITEM m_View_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAgent = false.
        end.
       setStatusMessage("").
       return true.
     end.
   when {&PersonCode} 
    then
     do:
       frame frPeople        :Title =  "People".
       close query brwDataPeople.
       empty temp-table ttPerson.
  
       disable btOpenPeople btExportPeople btRefreshPeople with frame frPeople.
  
       setStatusMessage("").
       return true.
     end.
   when {&AttorneyCode} 
    then
     do:
       frame frAttorneys     :Title =  "Attorneys".
       close query brwDataAttorney.
       empty temp-table ttAttorney.
  
       disable btOpenAttorney btExportAttorney btRefreshAttorney with frame frAttorneys.
       if ( query brwDataAttorney:num-results in frame frAttorney) eq 0   or ( query brwDataAttorney:num-results in frame frAttorney) eq ?
       then
        do:
          MENU-ITEM m_View_Attorney:sensitive            IN MENU POPUP-MENU-brwDataAttorney = false.
          MENU-ITEM m_View_Person:sensitive              IN MENU POPUP-MENU-brwDataAttorney = false.
          MENU-ITEM m_View_Person_Fulfillments:sensitive IN MENU POPUP-MENU-brwDataAttorney = false.
        end.
       setStatusMessage("").
       return true.
     end.
  end case.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStat wWin 
FUNCTION getComStat returns character PRIVATE
  ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cstat = "1" 
   then 
    return {&Gcode}.
  else if cstat = "2" 
   then 
    return {&Ycode}.
  else if cstat = "3" 
   then 
    return {&Rcode}.
  else 
   return "N".
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStatDesc wWin 
FUNCTION getComStatDesc returns character
  ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cstat = {&Gcode} 
   then 
    return {&Green}.
  else if cstat = {&Ycode} 
   then 
    return {&Yellow}.
  else if cstat = {&Rcode} 
   then 
    return {&Red}.
  else
   return "N/A".
   
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntity wWin 
FUNCTION getEntity RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&OrganizationCode}
   then
    cStat = {&Organization}.
  else
   cStat = {&Person}.
  
  return  cStat.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getEntityName wWin 
FUNCTION getEntityName RETURNS character
  ( input ipageNo as integer /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable cvalue as character no-undo.
  if ipageNo = 1
   then
     cvalue = {&OrganizationCode}.
  else if ipageNo = 2
   then
    cvalue = {&AgentCode}.
  else if ipageNo = 3
   then
    cvalue = {&PersonCode}.
  else
   cvalue = {&AttorneyCode}.
  
  RETURN cvalue.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc wWin 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:   
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getStatDesc wWin 
FUNCTION getStatDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cStat = {&ActiveStat} or cStat = "yes" 
   then  
    cStat = {&Active}.
  else if cStat = {&InActiveStat} or cStat = "no"
   then
    cStat = {&InActive}.
  else if cStat = {&ClosedStat}
   then  
    cStat = {&Closed}.
  else if cStat = {&ProspectStat}
   then  
    cStat = {&Prospect}.
  else if cStat = {&WithdrawnStat}
   then  
    cStat = {&Withdrawn}.
  else if cStat = {&CancelledStat}
   then  
    cStat = {&Cancelled}.
  else
   cstat = "N/A" .
 
  return cStat.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgentAction wWin 
FUNCTION openWindowForAgentAction returns handle
  ( input pAgentID as character,
    input pType    as character,
    input pFile    as character,
    input pAction  as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow      as handle    no-undo.
  define variable hFileDataSrv as handle    no-undo.
  define variable cAgentWindow as character no-undo.

  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAction <> "N" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).
  
  if pAction <> "C" then
  do:
    for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
      hWindow = openwindow.procHandle.
    end.                                                
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv,pAction).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultschanged wWin 
FUNCTION resultschanged RETURNS LOGICAL
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage({&ResultNotMatch}). 
/*  cStatusDateTime = false. */
  return true.                         
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveSearchValue wWin 
FUNCTION saveSearchValue RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 
 assign 
      flSearch:screen-value in frame {&frame-name} = trim(replace(replace(flSearch:input-value, flSearch:delimiter, " "), "  ", " "))
      std-ch                                      = flSearch:input-value
      std-in                                      = lookup(flSearch:input-value, flSearch:list-items, flSearch:delimiter)
      .
  
  if (std-in = 0 or std-in = ?) and flSearch:input-value <> "" and flSearch:input-value <> ?  /* Add it to the top of the list */
   then
    do: 
      if flSearch:num-items > 20 
       then 
        flSearch:delete(flSearch:num-items). /* Delete the oldest */

      flSearch:add-first(std-ch).
  
      publish "SetPeopleSearchHistory" (flSearch:list-items).
    end.

  return true.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateSearch wWin 
FUNCTION validateSearch returns character
  ( cKeyWord as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  /* Remove all punctuation and non-searchable characters from search keyword */
  define variable cvar as character no-undo.
  define variable ivar as integer   no-undo.
  
  cvar = "".
  
  do ivar = 1 to length(cKeyWord):
    if index(tBadSearchChars, substring(cKeyWord,ivar,1)) = 0 
     then
      cvar = cvar + substring(cKeyWord,ivar,1).
  end.
  
  cKeyWord = trim(cvar).
   
  /* Trim any leading or trailing asterisks 
     (they are ok in email addresses though) */
  cKeyWord = trim(cKeyWord,"@").
  
  return cKeyWord.   /* Function return value. */

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

