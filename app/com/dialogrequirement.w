&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogrequirement.w

  Description: UI to add/edit a state requirement record.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Naresh Chopra

  Created:04.10.2018 
  Modification :
  Date        Name        Description
  11/14/2018  Gurvindar   Fixed UI related changes and fetched state from config
  04/09/2020  Shubham     Modified code according to new organization structure
  11/11/2021  Shefali     Task #88683 BugFix User cannot able to save blank 
                          first-party requirement qualification
  03/31/2022  Shefali     Task# 86699  Modified to get attorney list.
  04/07/2022  Shefali     Task# 93008  Update layout.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/*------Temp-table definitons-------------------------------------------*/
{tt/agent.i}
{tt/attorney.i &tableAlias=tAttorney}
{tt/staterequirement.i} 

/* Parameters definitions ---    */
define input   parameter  ipcStateID        as character no-undo.
define input   parameter  table             for StateRequirement.
define output  parameter  opiReqID          as integer   no-undo.
define output  parameter  opiStateReqQualID as integer   no-undo.
define output  parameter  oplRecordUpdated  as logical   no-undo.

{lib/std-def.i}
{lib/com-def.i}
{lib/winlaunch.i}

/* Local Variable definiitions ---  */
define variable cValueQual      as character no-undo.
define variable pList           as character no-undo.
define variable cValuePair      as character no-undo.
define variable cAction         as character no-undo.

/* Variables used in enableDisableSave button. */
define variable chTrackAppliesTo      as character no-undo.
define variable chTrackEntityType     as character no-undo.
define variable chTrackDescription    as character no-undo.
define variable chTrackFulffiledBy    as character no-undo.
define variable chTrackQual           as character no-undo.
define variable loTrackFirstParty     as character no-undo.
define variable daTrackEffDate        as date      no-undo.
define variable chTrackExpDate        as date      no-undo.
define variable chTrackPersonName     as character no-undo.
define variable chTrackParentAddr     as character no-undo.
define variable chTrackParentCity     as character no-undo.
define variable chTrackParentState    as character no-undo.
define variable chTracktParentZip     as character no-undo.
define variable chTrackWebsite        as character no-undo.
define variable chTrackExpiry         as character no-undo.
define variable loTrackNotifAppr      as logical   no-undo.
define variable inTrackExpiryMonth    as integer   no-undo.
define variable inTrackExpiryDays     as integer   no-undo.
define variable inTrackNotifApprDays  as integer   no-undo.
define variable inTrackExpiryDay      as integer   no-undo.
define variable loTrackNotifCanc      as logical   no-undo.
define variable inTrackExpiryYear     as integer   no-undo.
define variable inTrackNotifCancDays  as integer   no-undo.
define variable chTrackEnotes         as character no-undo.
define variable hSelectionAgent       as handle    no-undo.
define variable hTextAgent            as handle    no-undo.
define variable hSelectionAttorney    as handle    no-undo.
define variable hTextAttorney         as handle    no-undo.
define variable ivar                  as integer   no-undo initial 0.
define variable ivar2                 as integer   no-undo initial 0.

define temp-table attorney like tAttorney
 fields entity     as character
 fields entityID   as character
 fields entityName as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS authorisedBy rsFulffiledBy2 cbAppliesTo ~
cbDescription fpersonName tParentAddr tParentCity tParentState tParentZip ~
fWebsite BtnUrl feffDate fexpDate tNotifAppr fNotifApprDays tNotifCanc ~
fNotifCancDays Enotes rsFulffiledBy cbqual cbexpiry fexpiryMonth fexpiryDay ~
fexpiryYear fexpiryDays BtnCancel tNotes fmarkMandatory-2 fmarkMandatory-3 ~
fmarkMandatory RECT-4 RECT-3 RECT-7 
&Scoped-Define DISPLAYED-OBJECTS authorisedBy fState rsFulffiledBy2 ~
cbAppliesTo cbDescription fpersonName tParentAddr tParentCity tParentState ~
tParentZip fWebsite feffDate fexpDate tNotifAppr fNotifApprDays tNotifCanc ~
fNotifCancDays Enotes rsFulffiledBy cbqual cbexpiry fexpiryMonth fexpiryDay ~
fexpiryYear fexpiryDays tNotes fmarkMandatory-2 fmarkMandatory-3 ~
fmarkMandatory 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel Requirement".

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save Requirement".

DEFINE BUTTON BtnUrl DEFAULT 
     LABEL "URL" 
     SIZE 4.8 BY 1.14 TOOLTIP "Open URL"
     BGCOLOR 8 .

DEFINE VARIABLE cbAgent AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE cbAppliesTo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE cbAttorney AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE cbDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE cbexpiry AS CHARACTER FORMAT "X(256)":U 
     LABEL "Method" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Does not expire","On a date","After number of days" 
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE cbqual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 55.8 BY 1 TOOLTIP "Select the company issued Qualification that will be newly assigned" NO-UNDO.

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE tParentState AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE Enotes AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 194 BY 4.05 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fexpiryDay AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Day" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 TOOLTIP "Default a newly assigned Qualification this day for the Expiration Date" NO-UNDO.

DEFINE VARIABLE fexpiryDays AS INTEGER FORMAT ">>>>":U INITIAL 0 
     LABEL "Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 8 BY 1 TOOLTIP "Default a newly assigned Qualification this number of days forward for the Expi" NO-UNDO.

DEFINE VARIABLE fexpiryMonth AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Month" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 TOOLTIP "Default a newly assigned Qualification this month for the Expiration Date" NO-UNDO.

DEFINE VARIABLE fexpiryYear AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Years" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 TOOLTIP "Default a newly assigned Qualification this number of years forward for the Exp" NO-UNDO.

DEFINE VARIABLE fiAppliesTo AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE VARIABLE fiAppliesTo2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 6 BY .62 NO-UNDO.

DEFINE VARIABLE fmarkMandatory AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 3 BY .62 TOOLTIP "Qualification is mandatory"
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fmarkMandatory-2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 3 BY .62 TOOLTIP "Qualification is mandatory"
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fmarkMandatory-3 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 3 BY .62 TOOLTIP "Qualification is mandatory"
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fNotifApprDays AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 7 BY 1 TOOLTIP "Number of days after approval to notify Issuer" NO-UNDO.

DEFINE VARIABLE fNotifCancDays AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Days" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 7 BY 1 TOOLTIP "Number of days after cancellation to notify Issuer" NO-UNDO.

DEFINE VARIABLE fpersonName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 TOOLTIP "Organization that enforces the Requirement" NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21.2 BY 1 NO-UNDO.

DEFINE VARIABLE fStateID AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE .2 BY .1 NO-UNDO.

DEFINE VARIABLE fWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 75 BY 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER FORMAT "X(256)":U INITIAL "Notes" 
      VIEW-AS TEXT 
     SIZE 6.6 BY .62 NO-UNDO.

DEFINE VARIABLE tParentAddr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 60 BY 1 NO-UNDO.

DEFINE VARIABLE tParentCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 34.8 BY 1 TOOLTIP "Enter the Issuer city" NO-UNDO.

DEFINE VARIABLE tParentZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14.8 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE VARIABLE authorisedBy AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Third-Party", "T",
          "First-Party", "C"
     SIZE 198.8 BY .95 NO-UNDO.

DEFINE VARIABLE rsFulffiledBy AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 27.6 BY .95 TOOLTIP "Select which entity will have a newly assigned Qualification" NO-UNDO.

DEFINE VARIABLE rsFulffiledBy2 AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 29.8 BY .95 TOOLTIP "Select state requirement for organization or person" NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 95 BY 9.43.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 95 BY 9.43.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 39 BY 5.48.

DEFINE VARIABLE tNotifAppr AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 NO-UNDO.

DEFINE VARIABLE tNotifCanc AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME dialog-Frame
     authorisedBy AT ROW 7.67 COL 18.2 NO-LABEL WIDGET-ID 374
     fStateID AT ROW 1.67 COL 43.6 COLON-ALIGNED NO-LABEL WIDGET-ID 310
     fState AT ROW 1.67 COL 15.8 COLON-ALIGNED WIDGET-ID 94
     rsFulffiledBy2 AT ROW 2.81 COL 18.2 NO-LABEL WIDGET-ID 358
     cbAppliesTo AT ROW 3.91 COL 16 COLON-ALIGNED WIDGET-ID 30
     cbRole AT ROW 5 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 316
     cbDescription AT ROW 6.1 COL 16 COLON-ALIGNED WIDGET-ID 18
     fpersonName AT ROW 10.19 COL 28.4 COLON-ALIGNED WIDGET-ID 24
     tParentAddr AT ROW 11.29 COL 28.4 COLON-ALIGNED WIDGET-ID 54
     tParentCity AT ROW 12.38 COL 28.4 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     tParentState AT ROW 12.38 COL 66.4 NO-LABEL WIDGET-ID 288
     tParentZip AT ROW 12.38 COL 73.6 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     fWebsite AT ROW 13.48 COL 28.4 COLON-ALIGNED WIDGET-ID 62
     BtnUrl AT ROW 13.43 COL 105.8 WIDGET-ID 92
     feffDate AT ROW 15.95 COL 39.6 COLON-ALIGNED WIDGET-ID 6
     fexpDate AT ROW 17.05 COL 39.6 COLON-ALIGNED WIDGET-ID 22
     tNotifAppr AT ROW 16.14 COL 83.8 WIDGET-ID 66
     fNotifApprDays AT ROW 15.95 COL 93.2 COLON-ALIGNED WIDGET-ID 68
     tNotifCanc AT ROW 17.24 COL 83.8 WIDGET-ID 70
     fNotifCancDays AT ROW 17.05 COL 93.2 COLON-ALIGNED WIDGET-ID 72
     Enotes AT ROW 19.76 COL 18 NO-LABEL WIDGET-ID 34
     rsFulffiledBy AT ROW 10.14 COL 131 NO-LABEL WIDGET-ID 290
     cbqual AT ROW 11.33 COL 128.8 COLON-ALIGNED WIDGET-ID 96
     cbexpiry AT ROW 13.71 COL 139 COLON-ALIGNED WIDGET-ID 302
     fexpiryMonth AT ROW 14.81 COL 139 COLON-ALIGNED WIDGET-ID 78
     fexpiryDay AT ROW 15.91 COL 139 COLON-ALIGNED WIDGET-ID 80
     fexpiryYear AT ROW 17 COL 139 COLON-ALIGNED WIDGET-ID 304
     fexpiryDays AT ROW 14.81 COL 157 COLON-ALIGNED WIDGET-ID 76
     BtnOK AT ROW 24.33 COL 99.6 WIDGET-ID 14
     BtnCancel AT ROW 24.33 COL 116 WIDGET-ID 16
     cbAgent AT ROW 5 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 86
     cbAttorney AT ROW 5 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 312
     fiAppliesTo AT ROW 5.19 COL 6.4 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     tNotes AT ROW 18.86 COL 16.2 COLON-ALIGNED NO-LABEL WIDGET-ID 82
     fmarkMandatory-2 AT ROW 5.24 COL 92.2 COLON-ALIGNED NO-LABEL WIDGET-ID 346
     fmarkMandatory-3 AT ROW 6.33 COL 92 COLON-ALIGNED NO-LABEL WIDGET-ID 344
     fmarkMandatory AT ROW 11.52 COL 185.8 COLON-ALIGNED NO-LABEL WIDGET-ID 330
     fiAppliesTo2 AT ROW 5.19 COL 6.4 COLON-ALIGNED NO-LABEL WIDGET-ID 366
     "Applies To:" VIEW-AS TEXT
          SIZE 10.4 BY .62 AT ROW 2.95 COL 6.4 WIDGET-ID 294
     "Notification Required On:" VIEW-AS TEXT
          SIZE 23.6 BY .62 AT ROW 15 COL 66 WIDGET-ID 324
     "Expiration of Qualification" VIEW-AS TEXT
          SIZE 25 BY .62 TOOLTIP "Method for setting the expiration date on a newly issued Qualification" AT ROW 12.67 COL 131 WIDGET-ID 308
     "Enforceable:" VIEW-AS TEXT
          SIZE 13.2 BY .62 TOOLTIP "Informational only - when is the Requirement in effect?" AT ROW 15 COL 30.4 WIDGET-ID 340
     "Authority" VIEW-AS TEXT
          SIZE 10.6 BY .62 TOOLTIP "The organization that established this requirement" AT ROW 9 COL 20.4 WIDGET-ID 338
          FONT 6
     "Authorized By:" VIEW-AS TEXT
          SIZE 14.2 BY .62 AT ROW 7.71 COL 3.8 WIDGET-ID 378
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME dialog-Frame
     "Company issued Qualification" VIEW-AS TEXT
          SIZE 35.6 BY .62 AT ROW 9 COL 118.4 WIDGET-ID 370
          FONT 6
     "Issued To:" VIEW-AS TEXT
          SIZE 10.4 BY .62 AT ROW 10.29 COL 120 WIDGET-ID 356
     "Cancellation:" VIEW-AS TEXT
          SIZE 12.8 BY .62 AT ROW 17.29 COL 70.4 WIDGET-ID 300
     "Approval:" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 16.24 COL 73.6 WIDGET-ID 298
     RECT-4 AT ROW 9.29 COL 19 WIDGET-ID 336
     RECT-3 AT ROW 9.33 COL 117 WIDGET-ID 342
     RECT-7 AT ROW 12.91 COL 130 WIDGET-ID 372
     SPACE(59.59) SKIP(7.46)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "State Requirement"
         DEFAULT-BUTTON BtnOK CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME dialog-Frame:SCROLLABLE       = FALSE.

/* SETTINGS FOR BUTTON BtnOK IN FRAME dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAgent IN FRAME dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbAttorney IN FRAME dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX cbRole IN FRAME dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       Enotes:RETURN-INSERTED IN FRAME dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fiAppliesTo IN FRAME dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       fiAppliesTo:AUTO-RESIZE IN FRAME dialog-Frame      = TRUE
       fiAppliesTo:RESIZABLE IN FRAME dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fiAppliesTo2 IN FRAME dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       fiAppliesTo2:AUTO-RESIZE IN FRAME dialog-Frame      = TRUE
       fiAppliesTo2:RESIZABLE IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fmarkMandatory:READ-ONLY IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fmarkMandatory-2:READ-ONLY IN FRAME dialog-Frame        = TRUE.

ASSIGN 
       fmarkMandatory-3:READ-ONLY IN FRAME dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fState IN FRAME dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fState:READ-ONLY IN FRAME dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fStateID IN FRAME dialog-Frame
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       fStateID:HIDDEN IN FRAME dialog-Frame           = TRUE.

ASSIGN 
       rsFulffiledBy:HIDDEN IN FRAME dialog-Frame           = TRUE.

/* SETTINGS FOR COMBO-BOX tParentState IN FRAME dialog-Frame
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dialog-Frame dialog-Frame
ON WINDOW-CLOSE OF FRAME dialog-Frame /* State Requirement */
do:
  oplRecordUpdated = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME authorisedBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL authorisedBy dialog-Frame
ON VALUE-CHANGED OF authorisedBy IN FRAME dialog-Frame
DO:
  run setwidgetsState   in this-procedure.
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME dialog-Frame /* Cancel */
do:
  oplRecordUpdated = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK dialog-Frame
ON CHOOSE OF BtnOK IN FRAME dialog-Frame /* Save */
do:
  /* Checking validations if fails then dialog do not get close. */
  run validationcheck (output std-ch).

  if std-ch ne "" then
  do:
    message std-ch
      view-as alert-box info buttons ok.
    return no-apply.
  end.

  run saveRequirement in this-procedure.

  if not oplRecordUpdated
   then 
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnUrl dialog-Frame
ON CHOOSE OF BtnUrl IN FRAME dialog-Frame /* URL */
do:
  if fWebsite:input-value <> "" 
   then
    run openURL in this-procedure (fWebsite:input-value).  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAgent dialog-Frame
ON VALUE-CHANGED OF cbAgent IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAppliesTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAppliesTo dialog-Frame
ON VALUE-CHANGED OF cbAppliesTo IN FRAME dialog-Frame /* Role */
do:
  find first stateRequirement no-error.

  if not available StateRequirement 
   then
    do:
      ivar = 0.
      run actionValueChanged in this-procedure. 
      run enableDisableSave  in this-procedure.
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAttorney
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAttorney dialog-Frame
ON VALUE-CHANGED OF cbAttorney IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbDescription dialog-Frame
ON VALUE-CHANGED OF cbDescription IN FRAME dialog-Frame /* Requirement */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbexpiry
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbexpiry dialog-Frame
ON VALUE-CHANGED OF cbexpiry IN FRAME dialog-Frame /* Method */
do:
  run setWidgetsState   in this-procedure.
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbqual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbqual dialog-Frame
ON VALUE-CHANGED OF cbqual IN FRAME dialog-Frame /* Qualification */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole dialog-Frame
ON VALUE-CHANGED OF cbRole IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Enotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Enotes dialog-Frame
ON VALUE-CHANGED OF Enotes IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME feffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL feffDate dialog-Frame
ON VALUE-CHANGED OF feffDate IN FRAME dialog-Frame /* Effective */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpDate dialog-Frame
ON VALUE-CHANGED OF fexpDate IN FRAME dialog-Frame /* Expiration */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpiryDay
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpiryDay dialog-Frame
ON LEAVE OF fexpiryDay IN FRAME dialog-Frame /* Day */
do:
  if fexpiryDay:input-value gt 31 
   then
    do:
      message "Expire Day must be 1 - 31."
        view-as alert-box info buttons ok.
      return no-apply.
    end.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpiryDay dialog-Frame
ON VALUE-CHANGED OF fexpiryDay IN FRAME dialog-Frame /* Day */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpiryDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpiryDays dialog-Frame
ON VALUE-CHANGED OF fexpiryDays IN FRAME dialog-Frame /* Days */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpiryMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpiryMonth dialog-Frame
ON VALUE-CHANGED OF fexpiryMonth IN FRAME dialog-Frame /* Month */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpiryYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpiryYear dialog-Frame
ON VALUE-CHANGED OF fexpiryYear IN FRAME dialog-Frame /* Years */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNotifApprDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNotifApprDays dialog-Frame
ON VALUE-CHANGED OF fNotifApprDays IN FRAME dialog-Frame /* Days */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNotifCancDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNotifCancDays dialog-Frame
ON VALUE-CHANGED OF fNotifCancDays IN FRAME dialog-Frame /* Days */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fpersonName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fpersonName dialog-Frame
ON VALUE-CHANGED OF fpersonName IN FRAME dialog-Frame /* Name */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fWebsite
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fWebsite dialog-Frame
ON MOUSE-SELECT-DBLCLICK OF fWebsite IN FRAME dialog-Frame /* Website */
do:
  apply "choose" to BtnUrl.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fWebsite dialog-Frame
ON VALUE-CHANGED OF fWebsite IN FRAME dialog-Frame /* Website */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsFulffiledBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsFulffiledBy dialog-Frame
ON VALUE-CHANGED OF rsFulffiledBy IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsFulffiledBy2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsFulffiledBy2 dialog-Frame
ON VALUE-CHANGED OF rsFulffiledBy2 IN FRAME dialog-Frame
do:
  find first stateRequirement no-error.
  if not available StateRequirement 
   then
    do:
      ivar = 0.
      run changerole in this-procedure.
      run actionValueChanged in this-procedure. 
    end. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotifAppr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotifAppr dialog-Frame
ON VALUE-CHANGED OF tNotifAppr IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotifCanc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotifCanc dialog-Frame
ON VALUE-CHANGED OF tNotifCanc IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentAddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentAddr dialog-Frame
ON VALUE-CHANGED OF tParentAddr IN FRAME dialog-Frame /* Address */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentCity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentCity dialog-Frame
ON VALUE-CHANGED OF tParentCity IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentState dialog-Frame
ON VALUE-CHANGED OF tParentState IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentZip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentZip dialog-Frame
ON VALUE-CHANGED OF tParentZip IN FRAME dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */

btnurl:load-image ("images/s-url.bmp").
btnurl:load-image-insensitive("images/s-url-i.bmp").

/* Gets the requirement list from compliance codes using data model . */
publish "getComplianceCodesList" ({&Requirement},
                                  input ",",
                                  output pList,
                                  output std-lo,
                                  output std-ch ).

/* Assigning the list */
assign
    pList = trim (pList,",")
    cbDescription:list-items = pList
    .

/* Gets the qualification list from compliance codes using data model. */
pList = "".
publish "getComplianceCodesList" ({&Qualification},
                                  input ",",
                                  output pList,
                                  output std-lo,
                                  output std-ch).

if not std-lo
 then
  do:
    message std-ch
        view-as alert-box error buttons ok.
    return.
  end.

/* assigning the list */
assign
    pList = trim (pList,",")
    cbqual:list-items = pList
    cValueQual = pList
    .

/* getting stateIDs from data model. */
publish "GetStateIDList"(input ",",
                         output cValuePair,
                         output std-lo,
                         output std-ch).

if not std-lo and std-ch <> ""
 then
  do:
    message std-ch
          view-as alert-box error buttons ok.
    return.
  end.
 
tParentState:list-items = trim(cValuePair).

/* Getting state from config.*/
publish "GetSearchStates" (output plist).
if plist > ""
 then
  plist = "," + {&NationalState} + "," + plist.
    
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
   
   run enable_UI. 
  /* Created a hidden widget, that contains the state ID. State ID widget 
     is required by agent combo-box. */
  fStateID:screen-value = ipcStateID.
 
  fState:screen-value = entry(lookup(ipcStateID, pList) - 1, pList) .
 
  std-lo = not can-find(first StateRequirement).
  
  /* create the agent combo */
  {lib/get-agent-list.i &combo=cbAgent &state=fStateID &addAll=true &setEnable=std-lo}
  
  /* create the attorney combos */
  {lib/get-attorney-list.i &combo=cbAttorney &state=fStateID &attorneyType=rsFulffiledBy2 &addAll=true &setEnable=std-lo}
  

  /* Both the get-agent-list.i and get-attorney-list.i defines the "on entry anywhere" trigger, the later 
     definition overrides the initial definition. So we have to redefine the "on entry anywhere" trigger
     including code for all 4 widgets of agent and attorney lists and fill-ins.  */      
  on entry anywhere
  do: 
    hSelectionAgent    = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentSelection").
    hTextAgent         = GetWidgetByName(cbAgent   :frame in frame {&frame-name}, "cbAgentAgentText").
    hSelectionAttorney = GetWidgetByName(cbAttorney:frame in frame {&frame-name}, "cbAttorneyAttorneySelection").
    hTextAttorney      = GetWidgetByName(cbAttorney:frame in frame {&frame-name}, "cbAttorneyAttorneyText").
    
    if valid-handle(hSelectionAgent) and valid-handle(hTextAgent)
     then
      if not self:name = "cbAgentAgentText" and not self:name = "cbAgentAgentSelection"
       then
        hSelectionAgent:visible = false.
      else
      do:
        if hSelectionAgent:list-item-pairs > "" and not hSelectionAgent:visible
         then 
          hSelectionAgent:visible = true.
      end.
    
    if valid-handle(hSelectionAttorney) and valid-handle(hTextAttorney)
     then
      if not self:name = "cbAttorneyAttorneyText" and not self:name = "cbAttorneyAttorneySelection"
       then
        hSelectionAttorney:visible = false.
      else
       do:
         if hSelectionAttorney:list-item-pairs > "" and not hSelectionAttorney:visible
          then
           hSelectionAttorney:visible = true.
       end.
  end.
  
  /* Set the default values. */
  assign
      cbAgent:screen-value = ""
      cbAttorney:screen-value = ""
      cbexpiry:screen-value = {&ExpireNot}
      .
  
      
  /* diaplaying data on screen. */
  run changerole         in this-procedure.
  run actionValueChanged in this-procedure.
  run displayData        in this-procedure.
  run setWidgetsState    in this-procedure.
 
  on 'value-changed':U of cbAttorney or
     'value-changed':U of cbAgent
  do:
    run enableDisableSave in this-procedure.
  end.
  
  wait-for go of frame {&frame-name}.
end.

run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionValueChanged dialog-Frame 
PROCEDURE actionValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable chagentlist as character no-undo.
  define variable chRoleList  as character no-undo.

  do with frame {&frame-name} :
  end.
  
  ivar = ivar + 1.
    
  do with frame {&frame-name}:
  end.

  /* Hide both the combo-boxes. Depending on the 
     value of cbAppliesTo combo-box, the label and 
     combo-boxes will be shown. */
  assign 
      cbRole:visible           = false
      cbRole:sensitive         = false
      fiAppliesTo2:visible     = false
      fiAppliesTo:visible      = false
      .
      
  run agentComboHide      in this-procedure (true).
  run attorneyComboHide   in this-procedure (true).
  run agentComboEnable    in this-procedure (false).
  run attorneyComboEnable in this-procedure (false).
  /* Populating the combo box here for what the new requirement going to be made
     its for Agent, Attorney */
     
  fiAppliesTo:screen-value = "" .
  fiAppliesTo2:screen-value = "".
  fmarkMandatory-2:visible = false.
  /* Populating for Agent */
  if cbAppliesTo:input-value = {&Agent}
   then
    do:
     if ivar = 1 
      then
       do:
         assign
             fiAppliesTo2:visible         = false
             fiAppliesTo:visible          = true
             fiAppliesTo:width-pixel      = 30
             fiAppliesTo:screen-value     = cbAppliesTo:input-value + ":" 
             fiAppliesTo:x                = cbAgent:x - fiAppliesTo:width-pixel - 4 
             .
       end.
     assign
         cbRole:visible               = false                      
         cbRole:sensitive             = false
         rsFulffiledBy:sensitive      = true
         fmarkMandatory-2:visible     = true
         .

     run AttorneyComboHide   in this-procedure (true).
     run AttorneyComboEnable in this-procedure (false).
     run AgentComboHide      in this-procedure (false).
     run AgentComboEnable    in this-procedure (true).
    end.
  else if cbAppliesTo:input-value = {&AttorneyFirm}
   then
    do:
     ivar2 = ivar2 + 1.
      if ivar = 1 
       then
        do:
          assign
              fiAppliesTo:visible        = false
              fiAppliesTo2:visible       = true
              fiAppliesTo2:screen-value  = cbAppliesTo:input-value + ":" 
              fiAppliesTo2:width-pixel   = if (ivar = 1 and ivar2 = 1)  then (fiAppliesTo2:width-pixel + 35) else fiAppliesTo2:width-pixel
              fiAppliesTo2:x             = cbAttorney:x - fiAppliesTo:width-pixel - 35
              .
        end.

     assign
          cbRole:visible            = false                      
          cbRole:sensitive          = false
          fmarkMandatory-2:visible  = true
          .

      run AgentComboHide      in this-procedure (true).
      run AgentComboEnable    in this-procedure (false).
      run AttorneyComboHide   in this-procedure (false).
      run AttorneyComboEnable in this-procedure (true).
      run AttorneyComboClear  in this-procedure.
    end.
   /* Populating for attorney */
  else if cbAppliesTo:input-value = {&Attorney} 
   then
    do:
      ivar2 = ivar2 + 1.
      if ivar = 1 
       then
        do:
          assign
              fiAppliesTo:visible        = false
              fiAppliesTo2:visible       = true
              fiAppliesTo2:screen-value  = cbAppliesTo:input-value + ":" 
              fiAppliesTo2:width-pixel   = if (ivar = 1 and ivar2 = 1)  then (fiAppliesTo2:width-pixel + 12) else fiAppliesTo2:width-pixel
              fiAppliesTo2:x             = cbAttorney:x - fiAppliesTo:width-pixel - 15
              .
        end.
   

        
      assign
          cbRole:visible            = false                      
          cbRole:sensitive          = false
          fmarkMandatory-2:visible  = true
          .

      run AgentComboHide      in this-procedure (true).
      run AgentComboEnable    in this-procedure (false).
      run AttorneyComboHide   in this-procedure (false).
      run AttorneyComboEnable in this-procedure (true).
      run AttorneyComboClear  in this-procedure.
      
    end.

  else 
    do:
      assign 
          fiAppliesTo:screen-value    = "" 
          cbAgent:screen-value        = ""
          cbAttorney:screen-value     = ""       
          cbRole:screen-value         = ""
          cbRole:visible              = true
          cbAgent:visible             = false
          cbAttorney:visible          = false
          fmarkMandatory-2:visible    = false
          .
  
    end.
   
  assign
      chTrackAppliesTo     = cbAppliesTo:input-value
      chTrackEntityType    = if cbAttorney:input-value <> "" then cbAttorney:input-value else (if cbRole:input-value <> "" then cbRole:input-value else cbAgent:input-value)
      chTrackDescription   = cbDescription:input-value
      chTrackFulffiledBy   = rsFulffiledBy:input-value
      chTrackQual          = cbqual:input-value
      loTrackFirstParty    = authorisedBy:screen-value /* firstParty name nd value */
      daTrackEffDate       = feffDate:input-value
      chTrackExpDate       = fexpDate:input-value
      chTrackPersonName    = fpersonName:input-value
      chTrackParentAddr    = tParentAddr:input-value
      chTrackParentCity    = tParentCity:input-value
      chTrackParentState   = tParentState:input-value
      chTracktParentZip    = tParentZip:input-value
      chTrackWebsite       = fWebsite:input-value
      chTrackExpiry        = cbexpiry:input-value
      loTrackNotifAppr     = tNotifAppr:input-value
      inTrackExpiryMonth   = fexpiryMonth:input-value
      inTrackExpiryDays    = fexpiryDays:input-value
      inTrackNotifApprDays = fNotifApprDays:input-value
      inTrackExpiryDay     = fexpiryDay:input-value
      loTrackNotifCanc     = tNotifCanc:input-value
      inTrackExpiryYear    = fexpiryYear:input-value
      inTrackNotifCancDays = fNotifCancDays:input-value
      chTrackEnotes        = Enotes:input-value
      .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE changerole dialog-Frame 
PROCEDURE changerole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.
  assign 
      std-lo = false
      std-ch = ""
      pList  = ""
      .

  find first staterequirement no-error.
  if available staterequirement
   then 
    rsfulffiledBy2:screen-value = staterequirement.reqfor.
   
  if rsFulffiledBy2:input-value = {&OrganizationCode}
    then
     publish "getComplianceCodesList" ({&OrganizationRole},
                                        input ",",
                                        output pList,
                                        output std-lo,
                                        output std-ch).

  else
   publish "getComplianceCodesList" ({&PersonRole},
                                      input ",",
                                      output pList,
                                      output std-lo,
                                      output std-ch).

     
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
 
  /* Assigning the list */
  assign
      pList = trim (pList,",")
      cbAppliesTo:list-items = pList
      cbAppliesto:screen-value = entry(1,pList)
      .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer StateRequirement for StateRequirement.
 
 find first StateRequirement no-error.
 if not available StateRequirement 
  then
   do:
     assign
         cAction                  = {&ActionNew}
         frame Dialog-Frame:title = "Add Requirement"
         btnOk:label              = "Create"
         btnok:tooltip            = "Create Requirement"
         .
     return.
   end.
   
  else
  do:

    btnok:tooltip = "Save Requirement".
    frame dialog-Frame:title = "Edit Requirement". 
    assign                                 
        cbAppliesTo:screen-value      = StateRequirement.appliesTo 
        feffDate:screen-value         = string(StateRequirement.effectiveDate)
        fexpDate:screen-value         = string(StateRequirement.expirationDate) 
        cbqual:screen-value           = StateRequirement.qualification
        fpersonName:screen-value      = StateRequirement.contactName
        tParentAddr:screen-value      = StateRequirement.contactAddress
        tParentCity:screen-value      = StateRequirement.contactCity   
        tParentState:screen-value     = StateRequirement.contactState     
        tParentZip:screen-value       = string(StateRequirement.contactZip)
        fWebsite:screen-value         = StateRequirement.url
        tNotifAppr:screen-value       = string(StateRequirement.notificationApproval)
        fNotifApprDays:screen-value   = string(StateRequirement.notificationApprovalDays)
        tNotifCanc:screen-value       = string(StateRequirement.notificationCancellation)
        fNotifCancDays:screen-value   = string(StateRequirement.notificationCancellationDays)
        cbexpiry:screen-value         = StateRequirement.expire 
        fexpiryDays:screen-value      = string(StateRequirement.expireDays)
        fexpiryMonth:screen-value     = string(StateRequirement.expireMonth)
        fexpiryDay:screen-value       = string(StateRequirement.expireDay)
        fexpiryYear:screen-value      = if StateRequirement.expireYear = 0 and cbexpiry:input-value = {&ExpireDate} then "1" else string(StateRequirement.expireYear)
        Enotes:screen-value           = StateRequirement.notes
        cbDescription:screen-value    = StateRequirement.description
        opiReqID                      = stateRequirement.RequirementID
        rsFulffiledBy:screen-value    = StateRequirement.fulfilledBy 
        rsFulffiledBy2:screen-value   = StateRequirement.reqFor
        rsFulffiledBy2:sensitive      = false
        btnOk:label                   = "Save" 
        no-error.
 
    authorisedBy:screen-value = if StateRequirement.authorizedby = {&FirstParty} then {&FirstParty} else {&ThirdParty}. /* firstParty */
   
    /* Populating combo box for entityID */
    run actionValueChanged in this-procedure.
    
    if StateRequirement.appliesTo = {&Agent} and StateRequirement.reqFor ={&OrganizationCode}
     then
      do:   
        run AgentComboHide in this-procedure (true) .
        run AgentComboEnable in this-procedure (false).
        if stateRequirement.name <> "" 
         then      
          cbAgent:add-first(stateRequirement.name + "(" + stateRequirement.refid + ")" , stateRequirement.refID). /* Showing agent name  and agentID */
        else   
         cbAgent:add-first(stateRequirement.refID, stateRequirement.refID).

        cbAgent:screen-value = stateRequirement.refID.
        cbagent:move-to-top().
      end.
   
    else if StateRequirement.appliesTo = {&Attorney} and StateRequirement.reqFor ={&PersonCode}
     then
      do:
        run AttorneyComboHide in this-procedure (true).
        run AttorneyComboEnable in this-procedure (false).
        cbattorney:visible = true.
        if stateRequirement.name <> "" 
         then      
          cbAttorney:add-first(stateRequirement.name + "(" + stateRequirement.refid + ")" , stateRequirement.refID). /* Showing agent name  and agentID */
        else   
         cbAttorney:add-first(stateRequirement.refID, stateRequirement.refID).
             
        cbAttorney:screen-value  = StateRequirement.refId.
        cbAttorney:move-to-top().
        
      end.
      
    else if StateRequirement.appliesTo = {&AttorneyFirm} and StateRequirement.reqFor ={&OrganizationCode}
     then
      do:
        run AttorneyComboHide in this-procedure (true).
        run AttorneyComboEnable in this-procedure (false).
        cbattorney:visible = true.
        if stateRequirement.name <> ""
         then
          cbAttorney:add-first(stateRequirement.name + "(" + stateRequirement.refid + ")" , stateRequirement.refID). /* Showing agent name  and agentID */
        else
         cbAttorney:add-first(stateRequirement.refID, stateRequirement.refID).

        cbAttorney:screen-value  = StateRequirement.refId.
        cbAttorney:move-to-top().

      end.
          
    if authorisedBy:input-value = {&FirstParty}  /* firstParty true */
     then
      cbqual:screen-value = StateRequirement.qualification.
  end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name}:
  end.

  if cAction = {&ActionNew} 
   then 
    BtnOK:sensitive =  if cbDescription:input-value = "" or cbDescription:input-value = ? then false else true. 
                     
  else
    BtnOK:sensitive = not ((chTrackAppliesTo    = cbAppliesTo:input-value     or cbAppliesTo:input-value   = "")  and
                           (chTrackEntityType   = cbAttorney:input-value      or cbAttorney:input-value    = "")  and 
                           (chTrackEntityType   = cbRole:input-value          or cbRole:input-value        = "")  and
                           (chTrackDescription  = cbDescription:input-value   or cbDescription:input-value = "")  and
                           chTrackFulffiledBy   = rsFulffiledBy:input-value      and
                           chTrackQual          = cbqual:input-value             and 
                           loTrackFirstParty    = authorisedBy:input-value       and
                           daTrackEffDate       = feffDate:input-value           and
                           chTrackExpDate       = fexpDate:input-value           and
                           chTrackPersonName    = fpersonName:input-value        and
                           chTrackParentAddr    = tParentAddr:input-value        and              
                           chTrackParentCity    = tParentCity:input-value        and                                   
                           chTrackParentState   = tParentState:input-value       and        
                           chTracktParentZip    = tParentZip:input-value         and
                           chTrackWebsite       = fWebsite:input-value           and 
                           chTrackExpiry        = cbexpiry:input-value           and
                           loTrackNotifAppr     = tNotifAppr:input-value         and
                           inTrackExpiryMonth   = fexpiryMonth:input-value       and
                           inTrackExpiryDays    = fexpiryDay:input-value         and
                           inTrackNotifApprDays = fNotifApprDays:input-value     and
                           inTrackExpiryDay     = fexpiryDays:input-value        and
                           loTrackNotifCanc     = tNotifCanc:input-value         and
                           inTrackExpiryYear    = fexpiryYear:input-value        and
                           inTrackNotifCancDays = fNotifCancDays:input-value     and
                           chTrackEnotes        = Enotes:input-value             and
                          (authorisedBy:screen-value = {&FirstParty} or cbqual:input-value = "")
                          ) no-error. 
       
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY authorisedBy fState rsFulffiledBy2 cbAppliesTo cbDescription 
          fpersonName tParentAddr tParentCity tParentState tParentZip fWebsite 
          feffDate fexpDate tNotifAppr fNotifApprDays tNotifCanc fNotifCancDays 
          Enotes rsFulffiledBy cbqual cbexpiry fexpiryMonth fexpiryDay 
          fexpiryYear fexpiryDays tNotes fmarkMandatory-2 fmarkMandatory-3 
          fmarkMandatory 
      WITH FRAME dialog-Frame.
  ENABLE authorisedBy rsFulffiledBy2 cbAppliesTo cbDescription fpersonName 
         tParentAddr tParentCity tParentState tParentZip fWebsite BtnUrl 
         feffDate fexpDate tNotifAppr fNotifApprDays tNotifCanc fNotifCancDays 
         Enotes rsFulffiledBy cbqual cbexpiry fexpiryMonth fexpiryDay 
         fexpiryYear fexpiryDays BtnCancel tNotes fmarkMandatory-2 
         fmarkMandatory-3 fmarkMandatory RECT-4 RECT-3 RECT-7 
      WITH FRAME dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openURL dialog-Frame 
PROCEDURE openURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pURL as character no-undo.
 
  run ShellExecuteA in this-procedure (0,
                                       "open",
                                       pURL,
                                       "",
                                       "",
                                       1,
                                       output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveRequirement dialog-Frame 
PROCEDURE saveRequirement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME} :
  end.
  define buffer staterequirement for staterequirement.

  find first StateRequirement no-error.
  if not available StateRequirement
   then  
    create StateRequirement. 

  /*  filling table for new or modify record */

  assign
      StateRequirement.stateID                      = ipcStateID
      StateRequirement.reqFor                       = rsFulffiledBy2:input-value 
      StateRequirement.appliesTo                    = cbAppliesTo:input-value
      StateRequirement.description                  = cbDescription:input-value in frame {&frame-name}     
      StateRequirement.contactName                  = fpersonName:input-value
      StateRequirement.contactAddress               = tParentAddr:input-value
      StateRequirement.contactCity                  = tParentCity:input-value
      StateRequirement.contactState                 = tParentState:input-value        
      StateRequirement.contactZip                   = tParentZip:input-value
      StateRequirement.url                          = fWebsite:input-value
      StateRequirement.role                         = ""
      StateRequirement.notificationApproval         = tNotifAppr:checked      /* tNotifAppr:input-value */
      StateRequirement.notificationApprovalDays     = fNotifApprDays:input-value
      StateRequirement.notificationCancellation     = tNotifCanc:checked
      StateRequirement.notificationCancellationDays = fNotifCancDays:input-value
      StateRequirement.effectiveDate                = feffDate:input-value
      StateRequirement.expirationDate               = fexpDate:input-value
      StateRequirement.expire                       = cbexpiry:input-value
      StateRequirement.expireDays                   = fexpiryDays:input-value
      StateRequirement.expireMonth                  = fexpiryMonth:input-value
      StateRequirement.expireDay                    = fexpiryDay:input-value
      StateRequirement.expireYear                   = fexpiryYear:input-value
      StateRequirement.notes                        = enotes:input-value
      StateRequirement.refId                        = if (cbAppliesTo:input-value = {&Agent}) then cbAgent:input-value 
                                                      else if (cbAppliesTo:input-value = {&Attorney} or cbAppliesTo:input-value = {&AttorneyFirm}) then cbAttorney:input-value
                                                      else {&ALL}
      StateRequirement.refId                        = if StateRequirement.refId = {&ALL} then {&ALL} else StateRequirement.refId . 
      
  if authorisedBy:input-value = {&ThirdParty} /* Third-Party */
   then
    StateRequirement.authorizedBy = {&ThirdParty}.  /*(T)hirdParty */
  else /*first party*/
    do:
      assign 
          StateRequirement.authorizedBy   = {&CompanyCode}. /*(C)ompanyCode */
          StateRequirement.fulfilledBy    =  rsFulffiledBy:input-value. 
          StateRequirement.qualification  =  cbqual:input-value. 
    end.
    
  /* new requirement record and update same in data modal. */                                      
  if cAction = {&ActionNew}
   then
    publish "newRequirement" (input table StateRequirement,
                              output opiReqID,                  /* returns requirementID.  */
                              output opiStateReqQualID ,        /* returns statereqqualID. */
                              output oplRecordUpdated,          /* returns success or fail */
                              output std-ch).                   /* returns error msg from server */
  else
   /* modify requirement record and update same in data modal. */
   publish "modifyRequirement" (input table StateRequirement,
                                output oplRecordUpdated,        /* returns success or fail */
                                output std-ch).                 /* returns error msg from server */

  if not oplRecordUpdated /* if not success return. */
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetsState dialog-Frame 
PROCEDURE setWidgetsState PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define  variable hQualhandle as handle      no-undo.
  do with frame {&frame-name}:
  end.
  fmarkMandatory:screen-value   = "*".
  fmarkMandatory-3:screen-value = "*".
  
  if cbAppliesTo:input-value eq {&Agent} or cbAppliesTo:input-value eq {&Attorney} or cbAppliesTo:input-value eq {&AttorneyFirm}
   then
    fmarkMandatory-2:screen-value = "*".
  else
   fmarkMandatory-2:screen-value = "".

  if authorisedBy:input-value = {&FirstParty} /* firstParty true */
   then
    do:
      if cbexpiry:input-value  = {&ExpireDate} /* On a date */
       then
        do:
          assign
              fexpiryDay:sensitive        = true
              fexpiryMonth:sensitive      = true
              fexpiryYear:sensitive       = true
              fexpiryDays:sensitive       = false
              fexpiryDays:screen-value    = ""
              fmarkMandatory-2:hidden     = false
              .
           
          if fexpiryYear:input-value = "" 
           then
            fexpiryYear:screen-value = "1".
            
        end.

      else if cbexpiry:input-value = {&ExpireDays} /* After no of days */
       then
        do:
          assign
              fexpiryDays:sensitive     = true
              fexpiryMonth:sensitive    = false
              fexpiryDay:sensitive      = false
              fexpiryYear:sensitive     = false
              fexpiryMonth:screen-value = ""
              fexpiryDay:screen-value   = ""
              fexpiryYear:screen-value  = ""
              .
        end.
     
      else
       do:
         assign
             fexpiryDay:sensitive      = false
             fexpiryMonth:sensitive    = false
             fexpiryDays:sensitive     = false
             fexpiryYear:sensitive     = false
             fexpiryMonth:screen-value = ""
             fexpiryDay:screen-value   = ""
             fexpiryDays:screen-value  = ""
             fexpiryYear:screen-value  = ""
             .
       end.
     
      assign 
          cbqual         :sensitive    = true
          cbexpiry       :sensitive    = true
          fexpiryMonth   :sensitive    = true
          fexpiryDay     :sensitive    = true
          fexpiryDays    :sensitive    = true
          fexpiryYear    :sensitive    = true            
          rsFulffiledBy  :sensitive    = true 
          fpersonname    :sensitive    = false
          tparentaddr    :sensitive    = false
          tParentCity    :sensitive    = false
          tParentState   :sensitive    = false
          tParentZip     :sensitive    = false
          fWebsite       :sensitive    = false
          BtnUrl         :sensitive    = false
          feffDate       :sensitive    = false
          tNotifAppr     :sensitive    = false
          fNotifApprDays :sensitive    = false
          fexpDate       :sensitive    = false
          tNotifCanc     :sensitive    = false
          fNotifCancDays :sensitive    = false
          fpersonname    :screen-value = ""
          tparentaddr    :screen-value = ""
          tParentCity    :screen-value = ""
          tParentState   :list-items   = ?
          tParentZip     :screen-value = ""
          fWebsite       :screen-value = ""
          feffDate       :screen-value = ""
          tNotifAppr     :checked      = false
          fNotifApprDays :screen-value = ""
          fexpDate       :screen-value = ""
          tNotifCanc     :checked      = false
          fNotifCancDays :screen-value = ""
          .  
          
      /* Populate the qualifications drop down for first party requirements.
         This has to be done in case of "New" action only. */
     
      std-ch = cbqual:input-value.
      if not can-find(staterequirement) 
       then
        assign
          cbqual:list-items = cValueQual
          cbqual:screen-value = std-ch
          std-ch = "". 
     
       fmarkMandatory:hidden   = false.
            
    end.

  else
   assign 
       cbqual           :sensitive         = false
       fpersonname      :sensitive         = true
       tparentaddr      :sensitive         = true
       tParentCity      :sensitive         = true
       tParentState     :sensitive         = true
       tParentZip       :sensitive         = true
       cbQual           :list-items        = ""
       tParentState     :list-items        = trim(cValuePair)
       rsFulffiledBy    :sensitive         = true
       cbexpiry         :sensitive         = false
       fexpiryMonth     :sensitive         = false
       fexpiryDay       :sensitive         = false
       fexpiryDays      :sensitive         = false
       fexpiryYear      :sensitive         = false
       rsFulffiledBy    :sensitive         = false 
       fWebsite         :sensitive         = true
       BtnUrl           :sensitive         = true
       feffDate         :sensitive         = true
       tNotifAppr       :sensitive         = true
       fNotifApprDays   :sensitive         = true
       fexpDate         :sensitive         = true
       tNotifCanc       :sensitive         = true
       fNotifCancDays   :sensitive         = true
       fexpiryYear      :screen-value      = ""
       fexpiryMonth     :screen-value      = ""
       fexpiryDay       :screen-value      = ""
       fexpiryDays      :screen-value      = ""
       cbexpiry         :screen-value      = {&ExpireNot} 
       fmarkMandatory   :hidden            = true
       fmarkMandatory-2 :hidden            = false
       fmarkMandatory-3 :hidden            = false
       .                                   
                                           
  if can-find (first staterequirement)     
   then                                    
    do:                                    
      assign                               
          cbAppliesTo   :sensitive  = false
          cbDescription :sensitive  = false
          cbAgent       :sensitive  = false
          cbAttorney    :sensitive  = false
          cbRole        :sensitive  = false
          authorisedBy  :sensitive  = false /* firstParty change name */
          rsFulffiledBy :sensitive  = false
          cbqual        :sensitive  = false
          fmarkMandatory-2:hidden   = true
          fmarkMandatory-3:hidden   = true
          fmarkMandatory:hidden     = true
          tParentState:screen-value = StateRequirement.contactState
          .
    end.  

  if cbexpiry:input-value = {&ExpireNot}
   then
    assign
        fexpiryMonth :sensitive = false
        fexpiryDay   :sensitive = false
        fexpiryDays  :sensitive = false
        fexpiryYear  :sensitive = false.

  else if cbexpiry:input-value = {&ExpireDays}
   then
    assign
        fexpiryDays  :sensitive  = true
        fexpiryMonth :sensitive  = false
        fexpiryDay   :sensitive  = false
        fexpiryYear  :sensitive  = false.
     
  else
   assign
       fexpiryMonth :sensitive = true
       fexpiryDay   :sensitive = true
       fexpiryDays  :sensitive = false
       fexpiryYear  :sensitive = true
       .
     
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ValidationCheck dialog-Frame 
PROCEDURE ValidationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: checking all the validations here     
------------------------------------------------------------------------------*/
  define output parameter chMsg as character no-undo.

  define variable dTempDate as date no-undo. 

  if cbAppliesto:input-value in frame {&frame-name} = ?
   then
    do:
      chMsg = "Applies To cannot be blank". 
      return chMsg.
    end.

  
  if cbAppliesto:input-value = {&Agent} and 
        (cbAgent:input-value = ? or cbAgent:input-value = "")
   then
    do:
      chMsg = "Agent cannot be blank". 
      return chMsg.
    end.

  if (cbAppliesto:input-value = {&Attorney} or cbAppliesto:input-value = {&AttorneyFirm}) and 
        (cbAttorney:input-value = ? or cbAttorney:input-value = "")
   then
    do:
      chMsg = "Attorney cannot be blank". 
      return chMsg.
    end.

  if cbDescription:input-value = ?
   then
    do:
      chMsg = "Requirement cannot be blank".
      return chMsg.
    end.
  
  if authorisedBy:input-value = {&FirstParty} and (cbqual:input-value = ? or cbqual:input-value = "") /* firstParty true */
   then
    do:
      chMsg = "Qualification cannot be blank".
      return chmsg.
    end.
   
  if authorisedBy:input-value = {&FirstParty}  /* firstParty true */
   then
    do:
      if fexpiryDay:input-value gt 31
       then
        do:
          chMsg = "Expire Day must be 1 - 31.".   
          return chmsg.
        end.
        
      if fExpiryMonth:input-value gt 12 
       then
        do:
          chMsg = "Expire Month must be 1 - 12".
          return chmsg.
        end.
       
      if cbexpiry:input-value = ? or cbexpiry:input-value = "" 
       then
        do:
          chMsg = "Select Expire type".
          return chmsg.
        end.
       
      if cbexpiry:input-value = {&ExpireDate}  /* (Expiredate) on a date */
       then
        do:
          dTempDate = date(fexpiryMonth:input-value,fexpiryDay:input-value,year(today) + fexpiryDay:input-value) no-error.
          if error-status:error 
           then
            do:
              chMsg = "Expire day or Expire month is invalid".
              return chmsg.
            end.
        end.
    end.  

    /* Expiration date check it must be greater then effective date */
  if date(fexpdate:input-value) < date(feffdate:input-value)
   then
    do:
      chmsg = "Expiration date must be greater than effective date".
      return chmsg.
    end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

