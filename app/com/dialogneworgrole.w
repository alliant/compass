&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File:dialogneworgrole.w 

  Description:Dialog to create and edit OrgRole record. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Shubham 

  Created:05.12.2018 
  @Modified
  Date         Name            Description
  03/16/2022   Shefali         Task #86699 Drop down of roles is fixed.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Temp-table Definitions ---                                           */
{tt/organization.i}
{tt/organization.i &tableAlias=tOrganization}

{tt/orgrole.i}

{tt/comcode.i}

/* Parameters Definitions ---                                           */
define input  parameter iphOrganizationData  as handle    no-undo.
define input  parameter ipcOrgID             as character no-undo.
define input  parameter ipcOrganizationName  as character no-undo.
define input  parameter table for orgrole.
define output parameter opiOrgRoleId         as character no-undo.
define output parameter oplSuccess           as logical   no-undo.

/* Local Variable Definitions ---                                       */

define variable cValuePair    as character no-undo. 
define variable cRoles        as character no-undo.
define variable cNewList      as character no-undo.
define variable iCount        as integer no-undo.

/* Library Files  */
{lib/com-def.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState cbRole fEffDate fExpDate btnOK ~
btnCancel 
&Scoped-Define DISPLAYED-OBJECTS fiName cbState fMarkMandatory1 cbRole ~
fMarkMandatory2 tActive fEffDate fExpDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON btnCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON btnOK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(30)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(30)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE fEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 12.6 BY 1 NO-UNDO.

DEFINE VARIABLE fExpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN 
     SIZE 12.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.6 BY .67 TOOLTIP "Status is always active" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fiName AT ROW 1.38 COL 12 COLON-ALIGNED WIDGET-ID 50
     cbState AT ROW 2.57 COL 12 COLON-ALIGNED WIDGET-ID 298
     fMarkMandatory1 AT ROW 2.76 COL 22.8 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     cbRole AT ROW 3.76 COL 12 COLON-ALIGNED WIDGET-ID 300
     fMarkMandatory2 AT ROW 4 COL 42 COLON-ALIGNED NO-LABEL WIDGET-ID 54
     tActive AT ROW 4.95 COL 14 WIDGET-ID 312
     fEffDate AT ROW 5.76 COL 12 COLON-ALIGNED WIDGET-ID 214
     fExpDate AT ROW 6.95 COL 12 COLON-ALIGNED WIDGET-ID 304
     btnOK AT ROW 8.1 COL 10 WIDGET-ID 294
     btnCancel AT ROW 8.1 COL 26.6 WIDGET-ID 292
     SPACE(10.19) SKIP(0.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Organization Role"
         DEFAULT-BUTTON btnOK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fiName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fiName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tActive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Organization Role */
do:
  apply "END-ERROR":u to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnCancel Dialog-Frame
ON CHOOSE OF btnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnOK Dialog-Frame
ON CHOOSE OF btnOK IN FRAME Dialog-Frame /* Save */
do:
  if cbRole:input-value = {&SelectRole}  
   then
    do:
      message "Please select a valid Role" 
            view-as alert-box info buttons ok.     
      return no-apply.    
    end.
  
  if fExpDate:input-value < fEffDate:input-value 
   then
    do:
      message "Expiration Date cannot be before Effective Date" 
          view-as alert-box info buttons ok.
      
      apply "entry" to fExpDate.
      return no-apply.
    end.
  run saveOragnizationRole in this-procedure.
  
  /* Variable oplSuccess gets updated in savepersonRole procedure. */
  if not oplSuccess 
   then
    return no-apply.    
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole Dialog-Frame
ON VALUE-CHANGED OF cbRole IN FRAME Dialog-Frame /* Role */
do:      
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState Dialog-Frame
ON VALUE-CHANGED OF cbState IN FRAME Dialog-Frame /* State */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEffDate Dialog-Frame
ON VALUE-CHANGED OF fEffDate IN FRAME Dialog-Frame /* Effective */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fExpDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fExpDate Dialog-Frame
ON VALUE-CHANGED OF fExpDate IN FRAME Dialog-Frame /* Expiration */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActive Dialog-Frame
ON VALUE-CHANGED OF tActive IN FRAME Dialog-Frame /* Active */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
   
  run getData           in this-procedure.
  run enableDisableSave in this-procedure.
  
  wait-for go of frame {&frame-name}.
end.

run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first orgRole no-error.
  
  /* Edit: enable save, if user changes anything. */
  if available orgRole and 
     orgRole.orgRoleID ne 0
   then
    btnOK:sensitive = not(fEffDate:input-value = orgRole.effdate and fExpDate:input-value = orgRole.expDate and tActive:input-value = orgRole.active) no-error.
  /* New: enable save, mandatory fields (state and role) is filled. */
  else
   btnOK:sensitive = not((cbState:input-value = {&SelectState} or cbState:input-value = "" or cbState:input-value = ?) or 
                         (cbRole:input-value  = {&SelectRole}  or cbRole:input-value  = "" or cbRole:input-value  = ?)).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fiName cbState fMarkMandatory1 cbRole fMarkMandatory2 tActive fEffDate 
          fExpDate 
      WITH FRAME Dialog-Frame.
  ENABLE cbState cbRole fEffDate fExpDate btnOK btnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  publish "GetStateIDList"(input ",",
                           output cValuePair,
                           output std-lo,
                           output std-ch).                         
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
    
  cbstate:list-items = {&National} + "," + cValuePair.
 
  publish "getComplianceCodesList"(input {&OrganizationRole},
                                   input  "," ,
                                   output cRoles,
                                   output std-lo,
                                   output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
      
  find first orgrole no-error.
  if available orgrole
   then
    do:
      assign
          cbRole:list-items in frame {&frame-name} = {&SelectRole} + "," + cRoles
          frame dialog-frame:title = "Modify Organization Role"
          cbState:sensitive = false
          cbRole :sensitive = false
          cbstate:screen-value = orgrole.stateID
          cbRole :screen-value = orgrole.source
          tActive:tooltip      = if orgrole.source = {&Agent} then "Status is based on agent status" else "Status is always active"
          tActive:checked      = orgrole.active
          fiName:screen-value  = ipcOrganizationName
          fMarkMandatory1 :hidden = true
          fMarkMandatory2 :hidden = true
          .
      
      if orgrole.effDate ne ? 
       then 
        fEffDate:screen-value = string(orgrole.effDate).
      if orgrole.expDate ne ? 
       then
        fExpDate:screen-value = string(orgrole.expDate).
    end.
  else 
   do:
     do iCount = 1 to num-entries(cRoles):
       if entry(iCount,cRoles) <> "Agent" and entry(iCount,cRoles) <> "Attorney Firm" 
        then
         cNewList = if cNewList = "" then entry(iCount,cRoles) else cNewList + "," + entry(iCount,cRoles). 
     end. /* do iCount = 1 to num-entries(cList): */
     
    assign 
         cbRole:list-items in frame {&frame-name} = {&SelectRole} + "," + cNewList
         cbstate:screen-value = cbstate:entry(1)
         cbRole:screen-value  = {&SelectRole}
         btnOK:label          = "Create"
         tActive:checked      = true
         fiName:screen-value  = ipcOrganizationName
         fMarkMandatory1   :screen-value   = {&mandatory}
         fMarkMandatory1   :hidden         = false
         fMarkMandatory2   :screen-value   = {&mandatory}
         fMarkMandatory2   :hidden         = false
         .
   end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveOragnizationRole Dialog-Frame 
PROCEDURE saveOragnizationRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not available orgrole
   then
    do:
      create orgrole. 
      orgrole.orgID = ipcOrgID. 
    end.

  assign
      orgrole.stateID        = cbState  :input-value
      orgrole.source         = cbRole   :input-value 
      orgrole.sourceID       = if orgrole.source = {&Agent} then orgrole.sourceID else ipcOrgID
      orgrole.effdate        = fEffDate :input-value
      orgrole.expDate        = fExpDate :input-value 
      orgrole.active         = tActive  :input-value no-error 
      .

  if not valid-handle(iphOrganizationdata) 
   then
    do:
      message "Data Model not found."
          view-as alert-box error buttons ok.
      return.
    end.
  
  if orgrole.orgRoleID = 0
   then   
    run newOrganizationRole    in iphOrganizationdata (input table orgrole,
                                                       output opiOrgRoleId,
                                                       output oplSuccess,
                                                       output std-ch).
  else 
   run modifyOrganizationRole in iphOrganizationdata (input table orgrole,
                                                      output oplSuccess,
                                                      output std-ch).     
  if not oplSuccess 
   then
    message std-ch
      view-as alert-box error buttons ok.
      
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

