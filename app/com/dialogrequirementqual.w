&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogrequirementqual.w

  Description: User Interface to add/edit state requirement qualification record

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul

  Created: 02.16.2018
  
  Modifications:
  Date          Name           Description
  11/20/2018    Rahul Sharma   Modified publish internal procedure names
  04/11/2019    Rahul          Add mandatory flag '*' for qualification fillIn    
  04/02/2020    Archana Gupta  Modified code according to new organization structure
  12/06/2021    Shefali        Task# 86699 Add Notes field in DB as well as in UI
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */


/* temp-table definition */
{tt/statereqqual.i}

/* Parameter Definition */
define input parameter ipchAction           as character no-undo.
define input parameter ipchStateID          as character no-undo.
define input parameter table for stateReqQual.
define output parameter opiStateReqQualID   as integer   no-undo.
define output parameter oplUpdateRecord     as logical   no-undo.

{lib/std-def.i}
{lib/com-def.i}

/* Local Variables */
define variable chQualList    as character   no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable chTrackCheck   as character no-undo.
define variable chTrackQual    as character no-undo.
define variable dtTrackEffdate as date      no-undo.
define variable dtTrackExpDate as date      no-undo.
define variable chTrackNotes   as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tCheck cbQual fnotes effdate expDate bcancel ~
tNotes RECT-58 RECT-60 
&Scoped-Define DISPLAYED-OBJECTS fState fAppto fRole fEntityId fReq tCheck ~
cbQual fMarkMandatory1 fnotes effdate expDate tNotes 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bcancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bSave AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE VARIABLE cbQual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE fnotes AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 58.5 BY 4.05 NO-UNDO.

DEFINE VARIABLE effdate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE expDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fAppto AS CHARACTER FORMAT "X(256)":U 
     LABEL "Applies To" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE fEntityId AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 2.5 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fReq AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE fRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER FORMAT "X(256)":U INITIAL "Notes:" 
      VIEW-AS TEXT 
     SIZE 6.6 BY .62 NO-UNDO.

DEFINE VARIABLE tCheck AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 28 BY .81 NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 9.29.

DEFINE RECTANGLE RECT-60
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 66 BY 6.57.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fState AT ROW 1.86 COL 18 COLON-ALIGNED WIDGET-ID 172
     fAppto AT ROW 3.1 COL 18 COLON-ALIGNED WIDGET-ID 192
     fRole AT ROW 4.33 COL 18 COLON-ALIGNED WIDGET-ID 174
     fEntityId AT ROW 5.57 COL 18 COLON-ALIGNED WIDGET-ID 176
     fReq AT ROW 6.81 COL 18 COLON-ALIGNED WIDGET-ID 178
     tCheck AT ROW 9.24 COL 20 NO-LABEL WIDGET-ID 162
     cbQual AT ROW 10.29 COL 18 COLON-ALIGNED WIDGET-ID 166
     fMarkMandatory1 AT ROW 10.62 COL 64 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     fnotes AT ROW 12.24 COL 7.4 NO-LABEL WIDGET-ID 34
     effdate AT ROW 16.57 COL 14.8 COLON-ALIGNED WIDGET-ID 188
     expDate AT ROW 16.57 COL 50 COLON-ALIGNED WIDGET-ID 190
     bSave AT ROW 18.19 COL 21.2 WIDGET-ID 146
     bcancel AT ROW 18.19 COL 40 WIDGET-ID 138
     tNotes AT ROW 11.57 COL 5.2 COLON-ALIGNED NO-LABEL WIDGET-ID 82
     "Qualification" VIEW-AS TEXT
          SIZE 12.2 BY .62 TOOLTIP "Qualification" AT ROW 8.29 COL 5.4 WIDGET-ID 182
     "Requirement" VIEW-AS TEXT
          SIZE 12.4 BY .62 TOOLTIP "Requirement" AT ROW 1.29 COL 5.2 WIDGET-ID 186
     "Fullfilled by :" VIEW-AS TEXT
          SIZE 12 BY .62 AT ROW 9.29 COL 8 WIDGET-ID 160
     RECT-58 AT ROW 8.62 COL 3.8 WIDGET-ID 180
     RECT-60 AT ROW 1.57 COL 3.8 WIDGET-ID 184
     SPACE(1.79) SKIP(11.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "State Requirement Qualification"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bcancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fAppto IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fEntityId IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fnotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fReq IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRole IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fState IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* State Requirement Qualification */
do:
  oplUpdateRecord = false.
  apply "end-error":u to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bcancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bcancel Dialog-Frame
ON CHOOSE OF bcancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplUpdateRecord = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:
  run validationCheck (output std-ch).

  if std-ch ne ""
   then
    do:
      MESSAGE std-ch
          VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return no-apply.
   end.

  run saveRequirementQualification in this-procedure.

  /* checking if server calls fails then not to close the dialog. */
  if not oplUpdateRecord
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbQual Dialog-Frame
ON VALUE-CHANGED OF cbQual IN FRAME Dialog-Frame /* Qualification */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME effdate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL effdate Dialog-Frame
ON VALUE-CHANGED OF effdate IN FRAME Dialog-Frame /* Effective */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME expDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL expDate Dialog-Frame
ON VALUE-CHANGED OF expDate IN FRAME Dialog-Frame /* Expiration */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fnotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fnotes Dialog-Frame
ON VALUE-CHANGED OF fnotes IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCheck Dialog-Frame
ON VALUE-CHANGED OF tCheck IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
  if valid-handle(active-window) and frame {&frame-name}:parent eq ?
   then
    frame {&frame-name}:parent = active-window.

  
  /* Getting the qualification compliance codes list from data modal. */
  publish "getComplianceCodesList" ({&Qualification},
                                    input ",",
                                    output chQualList,
                                    output std-lo,
                                    output std-ch).
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  /* assigning the qualification list */
  assign 
      chQualList = trim(chQualList,",")
      cbQual:list-items in frame {&frame-name} = chQualList 
      .

  MAIN-BLOCK:
  do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
     on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
    run enable_UI.

    /* display the data on dialog screen. */
    run displayData in this-procedure.
    run enableDisableSave in this-procedure.

    wait-for go of frame {&frame-name}.
  end.
  run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable chRole as character no-undo.
  
  do with frame {&frame-name}:
  end.
 
  chQualList = "".
   
  find first stateReqQual no-error.
  if not available stateReqQual 
   then 
    return.
 
  fMarkMandatory1:screen-value   = {&mandatory}.
  /* checking its for new or modify. */
  if ipchAction = {&ActionEdit}  /*(A)ctionAdd , (E)ActionEdit . */
   then 
    assign
        frame Dialog-Frame:title = "Edit State Requirement Qualification"
        bSave:label              = "Save"
        cbQual:screen-value      = stateReqQual.qualification
        bSave:tooltip            = "Save"
        tcheck:screen-value      = stateReqQual.appliesTo
        effdate   :screen-value   = string(stateReqQual.effectiveDate)
        expDate   :screen-value   = string(stateReqQual.expirationDate) 
        fnotes    :screen-value   = string(stateReqQual.notes) no-error
        .
  else
    assign
        frame Dialog-Frame:title      = "Add State Requirement Qualification"
        bSave:label                   = "Create"
        bSave:tooltip                 = "Create"
        .
        
  assign 
      fState    :screen-value   = ipchStateID
      fAppto    :screen-value   = if stateReqQual.reqFor = {&OrganizationCode} then {&Organization}  else  {&person}
      fRole     :screen-value   = stateReqQual.reqappliesTo
      fEntityId :screen-value   = stateReqQual.refID
      fReq      :screen-value   = stateReqQual.description
      opiStateReqQualID         = stateReqQual.stateReqQualID
      .

  /* Keep the initial Values, required in enableDisableSave. */
  assign
      chTrackCheck   = tCheck:input-value
      chTrackQual    = cbQual:input-value
      dtTrackEffdate = effdate:input-value
      dtTrackExpDate = expDate:input-value
      chTrackNotes   = fnotes:input-value
      .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  bSave:sensitive = if ipchAction = {&ActionEdit} 
                     then
                      not(tCheck:input-value = chTrackCheck   and
                         cbQual:input-value  = chTrackQual    and
                         effdate:input-value = dtTrackEffdate and
                         expDate:input-value = dtTrackExpDate and
                         fnotes:input-value  = chTrackNotes)
                    else
                      (tCheck:input-value <> "" and 
                       cbQual:input-value <> "")
                    no-error.
                    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fState fAppto fRole fEntityId fReq tCheck cbQual fMarkMandatory1 
          fnotes effdate expDate tNotes 
      WITH FRAME Dialog-Frame.
  ENABLE tCheck cbQual fnotes effdate expDate bcancel tNotes RECT-58 RECT-60 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveRequirementQualification Dialog-Frame 
PROCEDURE saveRequirementQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  define buffer stateReqQual for stateReqQual.

  /*  Filling table for new or modify record  as it will have data because 
      we need prior information for creating new statereqqual */

  find first stateReqQual no-error.
  if not available stateReqQual
   then 
    return.

  assign 
      stateReqQual.appliesTo      = if tCheck:screen-value = ? then "" else tCheck:screen-value 
      stateReqQual.qualification  = if cbQual:screen-value = ? then "" else cbQual:screen-value
      stateReqQual.expirationDate = expdate:input-value
      stateReqQual.effectiveDate  = effdate:input-value
      stateReqQual.notes          = fnotes:input-value
      . 

  /* new and edit requirement qualification record and update same in data modal. */
  if ipchAction = {&ActionEdit} 
   then
    publish "modifyRequirementQual" (input table stateReqQual ,
                                     output oplUpdateRecord,
                                     output std-ch).
  else  
   publish "newRequirementQual" (input table stateReqQual,
                                 output opiStateReqQualID, /* StateReqQualID returns from server . */
                                 output oplUpdateRecord,   /* return succes or fail */
                                 output std-ch).           /* error msg from server if fails */

  if not oplUpdateRecord  /* if not success return. */
   then
    do:
      message std-ch
         view-as alert-box error buttons ok.
    end.
  
end procedure .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validationCheck Dialog-Frame 
PROCEDURE validationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter chMSg as character no-undo.

  do with frame {&frame-name}:
  end.

  if date(expdate:screen-value) < date (effdate:screen-value) 
   then
    do:
      chmsg = "Expiration date must be greater than effective date.".
      return chmsg.
    end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

