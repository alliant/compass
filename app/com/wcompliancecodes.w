&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: wcompliancecodes.w

  Description:Compliance code's window

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Naresh Chopra

  Created: 03.23.2018
  
  Modified: 04.02.2020  Archana Gupta  Modified code according to new organization structure
            03/14/2022  Shefali        Task #86699 Replace the code combo-box with the radio-buttons.
------------------------------------------------------------------------*/
create widget-pool.

{tt/comcode.i}
{tt/comcode.i &tableAlias="tComCode"}

{lib/std-def.i}
{lib/com-def.i}

define variable icodeID      as integer   no-undo.
define variable dColumnWidth as decimal   no-undo.

{lib/get-column.i}
{lib/winshowscrollbars.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES comCode

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData comCode.code comCode.description comCode.isSecure   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each comCode by comCode.code
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each comCode by comCode.code.
&Scoped-define TABLES-IN-QUERY-brwData comCode
&Scoped-define FIRST-TABLE-IN-QUERY-brwData comCode


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh RECT-61 RECT-62 codeType brwData ~
bExport bdelete bEdit bNew 
&Scoped-Define DISPLAYED-OBJECTS codeType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete code".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify code".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New code".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE codeType AS CHARACTER INITIAL "OrganizationRole" 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization Role", "OrganizationRole":U,
          "Person Role", "PersonRole":U,
          "Requirement", "Requirement":U,
          "Qualification", "Qualification":U
     SIZE 69.8 BY 1.38 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37 BY 2.14.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 72 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      comCode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      comCode.code             label "Code"        format "x(25)"  
comCode.description            label "Description" format "x(255)" width 65
comCode.isSecure        column-label "Secure"      view-as toggle-box
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 108.4 BY 13.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bRefresh AT ROW 1.62 COL 74.4 WIDGET-ID 4 NO-TAB-STOP 
     codeType AT ROW 1.81 COL 3.2 NO-LABEL WIDGET-ID 358
     brwData AT ROW 3.81 COL 2 WIDGET-ID 200
     bExport AT ROW 1.62 COL 81.4 WIDGET-ID 2 NO-TAB-STOP 
     bdelete AT ROW 1.62 COL 102.4 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.62 COL 95.4 WIDGET-ID 8 NO-TAB-STOP 
     bNew AT ROW 1.62 COL 88.4 WIDGET-ID 6 NO-TAB-STOP 
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 74.4 WIDGET-ID 52
     "Filter" VIEW-AS TEXT
          SIZE 4.4 BY .62 AT ROW 1.1 COL 2.8 WIDGET-ID 56
     RECT-61 AT ROW 1.43 COL 73.6 WIDGET-ID 50
     RECT-62 AT ROW 1.43 COL 2 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 153.6 BY 17.81 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Setup Codes"
         HEIGHT             = 16.91
         WIDTH              = 110.4
         MAX-HEIGHT         = 32.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.52
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData codeType fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each comCode by comCode.code.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Setup Codes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Setup Codes */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  RETURN NO-APPLY.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Setup Codes */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do:
  run actionDelete in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
do:
  run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
do:
  run actionNew in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  run refreshData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  if comCode.isSecure = no 
   then
    run actionModify in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startsearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME codeType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL codeType C-Win
ON VALUE-CHANGED OF codeType IN FRAME fMain
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i }

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

on close of this-procedure 
  run disable_UI.

pause 0 before-hide.

bExport :load-image("images/excel.bmp").
bExport :load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").
bnew    :load-image("images/add.bmp").
bnew    :load-image-insensitive("images/add-i.bmp").
bEdit   :load-image("images/update.bmp").
bEdit   :load-image-insensitive("images/update-i.bmp").
bDelete :load-image("images/delete.bmp").
bDelete :load-image-insensitive("images/delete-i.bmp").


MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  run enable_UI.

  run getData in this-procedure.
 
  setStatusRecords(query brwdata:num-results).
 
  {lib/get-column-width.i &col="'description'" &var=dColumnWidth}

  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
 
  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable icomCodeID as integer no-undo.
  define variable lChoice    as logical no-undo.
  
  if not available comCode 
   then
    return.
 
  lchoice = false.

  message "Highlighted Compliance Code will be deleted." skip "Do you want to continue?"
      view-as alert-box question buttons yes-no title "Delete Compliance Code" update lChoice.
 
  if not lChoice 
   then
    return.

  iComCodeID = comCode.comCodeID.
 
  publish "deleteComplianceCode"(input  icomCodeID,
                                 output std-lo,
                                 output std-ch).

  if not std-lo 
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.

  run getData in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iComID  as integer    no-undo.
  define variable iCount  as integer    no-undo.
  
  empty temp-table tComCode.
 
  if not available comCode 
   then
    return.
 
  do with frame {&frame-name}:
  end.
 
  do iCount = 1 TO brwdata:num-iterations:
    if brwdata:is-row-selected(iCount) 
     then  
      leave.
  end.
 
  create tComCode.
  assign 
      tComCode.code         = comCode.code
      tComCode.description  = comCode.description
      tComCode.comments     = comCode.Comments
      tComCode.comCodeID    = comCode.comCodeID
      tComCode.reviewPeriod = comCode.reviewPeriod
      iComId                = comCode.comCodeID
      .
 
  run dialogcompliancecode.w(input table tComCode,
                             input codeType:input-value in frame {&frame-name},
                             output icodeID,
                             output std-lo).
 
  if not std-lo 
   then
    return.
 
  run getData in this-procedure.

  for first comCode 
    where comCode.comCodeID = iComID:
    std-ro = rowid(comCode).
  end.
       
  brwdata:set-repositioned-row(iCount,"ALWAYS").
  reposition brwdata to rowid std-ro.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount  as integer no-undo.
 
  define buffer bfComCode for comCode.
 
  empty temp-table tComCode.
 
  run dialogcompliancecode.w(input table tComCode, /* Blank for new */
                             input codeType:input-value in frame {&frame-name},
                             output iCodeID,       /* Code ID of newly created code. */
                             output std-lo).       /* true if the records is created successfully. */
  
  if not std-lo  
   then
    return.
 
  /* Update the data from the data model. */
  run getData in this-procedure.
 
  do with frame {&frame-name}:
  end.
 
  find first comCode 
    where comCode.comCodeID = iCodeID 
    no-error.

  if available comCode and (codeType:input-value = comCode.code)
   then
    std-ro = rowid(comCode).
  else 
   std-ro = ?.
    
  if std-ro <> ? 
   then
    do:
      do iCount = 1 TO brwdata:num-iterations:
        if brwdata:is-row-selected(iCount)
         then
          leave.
      end.
      brwdata:set-repositioned-row(iCount,"ALWAYS").
      reposition brwdata TO rowid std-ro.
      brwdata:get-repositioned-row().
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/      
  apply "close":U to this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY codeType 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh RECT-61 RECT-62 codeType brwData bExport bdelete bEdit bNew 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htableHandle as handle no-undo.
  
  empty temp-table tComCode.
 
  for each comCode where comCode.code = codeType:input-value in frame {&frame-name}:
    create tComCode.
    buffer-copy comCode to tComCode.
  end.
  
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
         view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
 
  htableHandle = temp-table tComCode:handle.
  run util/exporttable.p (table-handle htableHandle,
                          "tComCode",
                          "for each tComCode ",
                          "code,description,isSecure,reviewPeriod",
                          "Code,Description,Secure,Review Period",
                          std-ch,
                          "ComplianceCodes.csv-"+ replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwdata.
  
  open query brwdata 
    preselect each comCode 
      where comCode.code = codeType:input-value in frame {&frame-name} 
      by comCode.code.
  
  setStatusCount(query brwdata:num-results).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table comCode.

  publish "getComplianceCodes" (input {&ALL},
                                output table comCode,
                                output std-lo,
                                output std-ch). 
 
  if not std-lo 
   then
    do:
      message std-ch 
          view-as alert-box error buttons ok.
      return.
    end.
   
  run filterData in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshData C-Win 
PROCEDURE refreshData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  publish "refreshComplianceCodes"(input  {&ALL},
                                   output std-lo,
                                   output std-ch).
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.

  run getData in this-procedure.

  setStatusRecords(query brwdata:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tWhereClause as character no-undo.

  tWhereClause =  "where comCode.code = " + "'" + codeType:input-value in frame {&frame-name} + "'" .

  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame fMain:width-pixels          = {&window-name}:width-pixels
      frame fMain:virtual-width-pixels  = {&window-name}:width-pixels
      frame fMain:height-pixels         = {&window-name}:height-pixels
      frame fMain:virtual-height-pixels = {&window-name}:height-pixel
      /* fMain Components */
      brwData:width-pixels              = frame fmain:width-pixels  - 13
      brwData:height-pixels             = frame fMain:height-pixels - 67.5
       .
  {lib/resize-column.i &col="'Description'" &var=dColumnWidth}

  run ShowScrollBars(frame fMain:handle, no, no).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

