&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogqualificationreview.w

  Description: Dialog to add or edit a qualification review.

  Input Parameters:
      iphQualDataSrv      :Qualification DataSrv handle.
      ipcStateID          :state ID
      ipiQualificationID  :Qualification ID
      ipcQualStatus       :Qualification Status
      ipdtEffectiveDate   :Effective Date
      ipdtExpirationDate  :Expiration Date
      review              :review temp-table

  Author:Sachin Chaturvedi 

  Created:05.05.2018 
  @Modified
  Date         Name       Description  
  04/11/19     Rahul      Add mandatory flag '*' for nextReviewDate fillIn  
  08/09/19     Gurvindar  Removed progress error while populating combo-box    
  04/02/2020   Shubham    Modified code according to new organization structure
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/review.i} 

/* Parameters Definitions ---                                           */
define input  parameter iphQualDataSrv      as handle    no-undo.
define input  parameter ipcStateID          as character no-undo.
define input  parameter ipiQualificationID  as integer   no-undo.
define input  parameter ipcQualStatus       as character no-undo.
define input  parameter ipdtEffectiveDate   as datetime  no-undo.
define input  parameter ipdtExpirationDate  as datetime  no-undo.
define input  parameter table for review.
define output parameter opiReviewID         as integer   no-undo.
define output parameter oplSuccess          as logical   no-undo.

/* Library Files  */
{lib/com-def.i}
{lib/std-def.i}

/* Local Variable Definitions ---                                       */
define variable cMode       as character no-undo.
define variable cCurrUser   as character no-undo.
define variable cCurrentUID as character no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable loTrackIssues   as logical   no-undo.
define variable inTrackValidfor as integer   no-undo.
define variable daNextRevDt     as date      no-undo.
define variable chStatus        as character no-undo.
define variable daEffDate       as date      no-undo.
define variable daExpDate       as date      no-undo.
define variable chNewComm       as character no-undo.
define variable chEnforcements  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bCancel tIssues fValidfor fNextRevDt ~
cbStatus feffDate fexpDate edNewComm edEnforcements 
&Scoped-Define DISPLAYED-OBJECTS flReview flReviewBy tIssues fValidfor ~
fNextRevDt cbStatus feffDate fexpDate edNewComm edEnforcements ~
fMarkMandatory1 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bSave AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save".

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE edEnforcements AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65.4 BY 2.14 NO-UNDO.

DEFINE VARIABLE edNewComm AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 65.4 BY 2.14 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Qualification Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Qualification Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE flReview AS DATE FORMAT "99/99/99":U 
     LABEL "Review" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flReviewBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Review By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fNextRevDt AS DATE FORMAT "99/99/99":U 
     LABEL "Next Review" 
     VIEW-AS FILL-IN 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fValidfor AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Valid For Day(s)" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tIssues AS LOGICAL INITIAL no 
     LABEL "Issues" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bCancel AT ROW 14.76 COL 49.4 NO-TAB-STOP 
     bSave AT ROW 14.76 COL 32.8 NO-TAB-STOP 
     flReview AT ROW 1.33 COL 23.6 COLON-ALIGNED WIDGET-ID 36
     flReviewBy AT ROW 1.33 COL 68.6 COLON-ALIGNED WIDGET-ID 38
     tIssues AT ROW 2.38 COL 25.6 WIDGET-ID 6
     fValidfor AT ROW 3.24 COL 23.6 COLON-ALIGNED WIDGET-ID 16
     fNextRevDt AT ROW 3.24 COL 68.6 COLON-ALIGNED WIDGET-ID 18
     cbStatus AT ROW 4.76 COL 23.6 COLON-ALIGNED WIDGET-ID 30
     feffDate AT ROW 6.33 COL 23.6 COLON-ALIGNED WIDGET-ID 32
     fexpDate AT ROW 6.33 COL 68.6 COLON-ALIGNED WIDGET-ID 34
     edNewComm AT ROW 8.62 COL 25.6 NO-LABEL WIDGET-ID 12
     edEnforcements AT ROW 12.05 COL 25.6 NO-LABEL WIDGET-ID 20
     fMarkMandatory1 AT ROW 3.48 COL 89.2 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 7.86 COL 25.8 WIDGET-ID 14
     "Enforcements:" VIEW-AS TEXT
          SIZE 14 BY .62 AT ROW 11.29 COL 25.8 WIDGET-ID 22
     SPACE(56.39) SKIP(4.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Verify Qualification"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       edEnforcements:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

ASSIGN 
       edNewComm:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN flReview IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flReview:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flReviewBy IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flReviewBy:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Verify Qualification */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:  
  define variable oplError as logical no-undo.

  run validateReview in this-procedure (output oplError).
  if not oplError 
   then
    return no-apply.

  run saveQualReview in this-procedure.
  if not oplSuccess 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus Dialog-Frame
ON VALUE-CHANGED OF cbStatus IN FRAME Dialog-Frame /* Qualification Status */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edEnforcements
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edEnforcements Dialog-Frame
ON VALUE-CHANGED OF edEnforcements IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME edNewComm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL edNewComm Dialog-Frame
ON VALUE-CHANGED OF edNewComm IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME feffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL feffDate Dialog-Frame
ON VALUE-CHANGED OF feffDate IN FRAME Dialog-Frame /* Qualification Effective */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpDate Dialog-Frame
ON VALUE-CHANGED OF fexpDate IN FRAME Dialog-Frame /* Qualification Expiration */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNextRevDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNextRevDt Dialog-Frame
ON VALUE-CHANGED OF fNextRevDt IN FRAME Dialog-Frame /* Next Review */
do:
  run valChngNxtRevDt   in this-procedure.
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fValidfor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fValidfor Dialog-Frame
ON VALUE-CHANGED OF fValidfor IN FRAME Dialog-Frame /* Valid For Day(s) */
do:  
  fNextRevDt:screen-value = string(today + fValidfor:input-value) no-error.
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tIssues
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tIssues Dialog-Frame
ON VALUE-CHANGED OF tIssues IN FRAME Dialog-Frame /* Issues */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
  
  publish "GetSysPropListMinusID" ("COM", "Qualification", "Status", {&NeedsReview}, output std-ch).

  if std-ch ne "" 
   then
    cbStatus:list-item-pairs = replace(std-ch,"^",",").
  else
   cbStatus:list-item-pairs = ",".
                                        
  publish "GetCredentialsName" (output cCurrUser).
  publish "GetCredentialsID"   (output cCurrentUID).

  run displayData in this-procedure.
    
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hcmdhandle as handle no-undo.

  assign 
      flReview  :screen-value in frame {&frame-name} = string(today)
      flReviewBy:screen-value                        = cCurrUser
      fMarkMandatory1:screen-value                   = {&mandatory}
      .

  find first review no-error.

  /* If temp-table has a record, this is a case of "Modify" */
  if available review 
   then
    do:
      if review.stat ne "" and
         not can-do(cbStatus:list-item-pairs,review.stat)
       then
        do:
          hcmdhandle = cbStatus:handle.
          std-ch = "".
          publish "GetSysPropDesc" ("COM", "Qualification", "Status", review.stat, output std-ch).

          hcmdhandle:add-last(std-ch,review.stat).
        end.

      assign 
          fValidfor     :screen-value  = string(review.numValidDays)
          fNextRevDt    :screen-value  = string(review.nextreviewDueDate)
          feffDate      :screen-value  = string(review.effdate)
          fexpDate      :screen-value  = string(review.expdate)
          tIssues       :screen-value  = string(review.issue)
          cbStatus      :screen-value  = if cbstatus:list-item-pairs = "," then "" else review.stat
          edNewComm     :screen-value  = review.Comments
          edEnforcements:screen-value  = review.enforcement 
          bSave:label                  = "Save"  
          bSave:tooltip                = "Save"  
          cMode                        = {&ActionEdit} no-error
          .  
    end.
                                     

  /* If empty temp-table is passed, this is a case of "New" */
  else         
   assign 
       cbStatus:screen-value   = if cbstatus:list-item-pairs = "," then "" else ipcQualStatus
       feffDate:screen-value   = string(ipdtEffectiveDate)                                     
       fexpDate:screen-value   = string(ipdtExpirationDate) 
       bSave:label             = "Create"  
       bSave:tooltip           = "Create"  
       cMode                   = {&ActionNew} no-error
       .  
  /* Keep the initial values, as required in enableDisableSave. */
  assign
      loTrackIssues   = tIssues:input-value
      inTrackValidfor = fValidfor:input-value
      daNextRevDt     = fNextRevDt:input-value
      chStatus        = cbStatus:input-value
      daEffDate       = feffDate:input-value
      daExpDate       = fexpDate:input-value
      chNewComm       = edNewComm:input-value
      chEnforcements  = edEnforcements:input-value
      .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  bSave:sensitive = if cMode = {&ActionNew} 
                     then (fNextRevDt:input-value <> ?)                          
                    else
                     not (loTrackIssues   = tIssues:input-value         and
                          inTrackValidfor = fValidfor:input-value       and
                          daNextRevDt     = fNextRevDt:input-value      and
                          chStatus        = cbStatus:input-value        and
                          daEffDate       = feffDate:input-value        and
                          daExpDate       = fexpDate:input-value        and
                          chNewComm       = edNewComm:input-value       and 
                          chEnforcements  = edEnforcements:input-value) and
                          (fNextRevDt:input-value <> ?)                  
                          no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flReview flReviewBy tIssues fValidfor fNextRevDt cbStatus feffDate 
          fexpDate edNewComm edEnforcements fMarkMandatory1 
      WITH FRAME Dialog-Frame.
  ENABLE bCancel tIssues fValidfor fNextRevDt cbStatus feffDate fexpDate 
         edNewComm edEnforcements 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveQualReview Dialog-Frame 
PROCEDURE saveQualReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Set the success flag to false, so that dialog will halt until
     success is returned from server. */
  oplSuccess = false.
  do with frame {&frame-name}:
  end.
  
  find first review no-error.
  if not available review then
    create review.
 
  assign 
      review.stateID           = ipcStateID
      review.qualificationID   = ipiQualificationID
      review.reviewDate        = datetime(today)
      review.reviewBY          = cCurrentUID
      review.username          = cCurrUser
      review.enforcement       = edEnforcements:input-value
      review.nextreviewDueDate = fNextRevDt:input-value
      review.numValidDays      = fValidfor:input-value
      review.issue             = tIssues:checked
      review.Comments          = edNewComm:input-value
      review.stat              = cbStatus:screen-value
      review.effdate           = feffDate:input-value 
      review.expdate           = fexpDate:input-value no-error
      .

  if cMode = {&ActionNew} 
   then
    run newqualificationreview in iphQualDataSrv (input  table review,
                                                  output opiReviewID,
                                                  output oplSuccess,
                                                  output std-ch).
  else 
   run modifyqualificationreview in iphQualDataSrv (input  table review,
                                                    output oplSuccess,
                                                    output std-ch).
  if not oplSuccess 
   then 
    message std-ch
      view-as alert-box error buttons ok.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChngNxtRevDt Dialog-Frame 
PROCEDURE valChngNxtRevDt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  assign
      bSave:sensitive in frame {&frame-name} = true
      fValidfor:screen-value = string(fNextRevDt:input-value - today) no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateReview Dialog-Frame 
PROCEDURE validateReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter oplUpdate as logical no-undo.

  if fNextRevDt:input-value in frame {&frame-name} = ? 
   then
    do:
      message "Next Review date cannot be blank."
         view-as alert-box info buttons ok.
      oplUpdate = false.
      return.
    end.

  if tIssues:checked        = true and 
     edNewComm:input-value  = ""       
   then
    do:
      message "Comments cannot be blank."
          view-as alert-box info buttons ok.
      oplUpdate = false.
      return.
    end.

  if not valid-handle(iphQualDataSrv) 
   then
    do:
      message "Data Model not found."
          view-as alert-box error buttons ok.
      oplUpdate = false.
      return.
    end.

  /* Review date should be greater than effective date. */
  if not feffDate:input-value = ? and 
    fNextRevDt:input-value < feffDate:input-value 
   then
    do:
      message "Next Review date cannot be less than effective date"
        view-as alert-box info buttons ok.
      oplUpdate = false.
      return.
    end. /* if date(fNextRevDt:screen-value) < today then */

  /* Review date should be greater than effective date. */
  if not fexpDate:input-value = ? and 
    fNextRevDt:input-value > fexpDate:input-value 
   then
    do:
      message "Next Review date cannot be greater than expiration date"
        view-as alert-box info buttons ok.
      oplUpdate = false.
      return.
    end. /* if date(fNextRevDt:screen-value) < today then */


  /* expiration date should not be less then Effective date. */
  if fexpDate:input-value < feffDate:input-value 
   then
    do:
      message "Expiration date cannot be before qualification effective date."
        view-as alert-box info buttons ok.

      fexpDate:screen-value = "".
      oplUpdate = false.
      return.
    end.

  oplUpdate = true. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

