&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogqualification.w

  Description: User Interface to add or edit a qualification.

  Input Parameters:
  
  Output Parameters:

  Author: Naresh Chopra 

  Created: 03.11.2018
  Modification:
  Date          Name          Description
  12/21/2018    Sachin        Modified to remove review temptable and use 
                              qualification temptable to create review record
  04/11/19      Rahul         Add mandatory flag '*' in stateId,qualification fillIn 
                              and add validations.
  08/09/2019    Gurvindar     Removed progress error while populating combo-box  
  12/12/2019    Archana Gupta Modified for the new structure of organization.
  03/16/2022    Shefali       Task #86699 Implememt copy feature of qualification record.                     
  04/26/2022    Shefali       Task #93684 Modified to add qualification state if it is not in state combo..                     
  03/08/2023    S Chandu      Task #102073 Added Renew Qualification changes.
  04/24/2023    S Chandu      Task #103750 Populating EffectiveDate as per old qualification.
  12/03/2024    SRK           TasK #117466 Implement Identity Verification Service per file rate and tax. 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt/qualification.i &tableAlias="tqualification"}
{tt/idverificationsw.i}
{lib/add-delimiter.i}

/* Parameters Definitions ---                                           */
define input  parameter iphentitydata   as handle    no-undo.
define input  parameter ipcAction       as character no-undo.
define input  parameter ipcEntity       as character no-undo.
define input  parameter ipcEntityID     as character no-undo.
define input  parameter ipcStateID      as character no-undo.
define input  parameter table for tQualification.
define output parameter oplSuccess      as logical   no-undo.
define output parameter opiQualID       as integer   no-undo.
define output parameter opiReviewID     as integer   no-undo.

/* Library Files   */
{lib/std-def.i}
{lib/com-def.i} 

/* Local Variables */
define variable cCurrUser as character no-undo.
define variable cList     as character no-undo.
define variable cQualList as character no-undo.
define variable lIsNew    as logical   no-undo.

/*  Renew variables */
define variable lIsRenew     as logical   no-undo.
define variable cRenewQualID as character no-undo.

/* Variables defined to be used in IP enableDisableSave. */
define variable chQual            as character no-undo.
define variable chLicensenum      as character no-undo.
define variable chStatus          as character no-undo.
define variable chProvider        as character no-undo.
define variable chAuth            as character no-undo.
define variable daEffDate         as date      no-undo.
define variable daExpDate         as date      no-undo.
define variable inCoverageAmt     as integer   no-undo.
define variable inAggregateAmt    as integer   no-undo.
define variable daRetroDate       as date      no-undo.
define variable inDeductAmt       as integer   no-undo.
define variable chInsuranceBroker as character no-undo.
define variable chEnotes          as character no-undo.
define variable inValidDays       as integer   no-undo.
define variable daNxtReviewDate   as date      no-undo.
define variable crate             as character no-undo.
define variable ctax              as character no-undo.
define variable cservicename      as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS CbState cbQual fLicensenum cbAuth feffDate ~
fProvider fexpDate tgOther tgInsurance fcoverageAmt RetroDate fdeductAmt ~
fAggregateAmt InsuranceBroker tgIdentity cbservicename flrate fltax Enotes ~
cbStatus validDays nxtReviewDate Btn_Cancel RECT-1 RECT-61 
&Scoped-Define DISPLAYED-OBJECTS CbState fMarkMandatory1 cbQual ~
fMarkMandatory2 fLicensenum cbAuth feffDate fProvider fexpDate tgOther ~
tgInsurance fcoverageAmt RetroDate fdeductAmt fAggregateAmt InsuranceBroker ~
tgIdentity cbservicename flrate fltax Enotes tactive cbStatus reviewDate ~
reviewby validDays nxtReviewDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateQual Dialog-Frame 
FUNCTION validateQual returns logic
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbAuth AS CHARACTER FORMAT "X(256)":U 
     LABEL "Authorized by" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "First-Party","Third-Party" 
     DROP-DOWN-LIST
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE cbQual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 57.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbservicename AS CHARACTER FORMAT "X(256)":U 
     LABEL "Service Name" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE CbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN-LIST
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE Enotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 93 BY 2.29 NO-UNDO.

DEFINE VARIABLE fAggregateAmt AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Aggregate Amount $" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fcoverageAmt AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Coverage Amount $" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 DROP-TARGET NO-UNDO.

DEFINE VARIABLE fdeductAmt AS INTEGER FORMAT "Z,ZZZ,ZZ9":U INITIAL 0 
     LABEL "Deductible Amount $" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE fLicensenum AS CHARACTER FORMAT "X(256)":U 
     LABEL "Number/ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE flrate AS DECIMAL FORMAT "ZZ,ZZ9.99":U INITIAL 0 
     LABEL "Rate $" 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 NO-UNDO.

DEFINE VARIABLE fltax AS DECIMAL FORMAT "ZZ,ZZ9.99":U INITIAL 0 
     LABEL "Tax $" 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fProvider AS CHARACTER FORMAT "X(256)":U 
     LABEL "Issuer" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 38 BY 1 NO-UNDO.

DEFINE VARIABLE InsuranceBroker AS CHARACTER FORMAT "X(256)":U 
     LABEL "Broker" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 59.4 BY 1 NO-UNDO.

DEFINE VARIABLE nxtReviewDate AS DATE FORMAT "99/99/99":U 
     LABEL "Next Review Date" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE RetroDate AS DATE FORMAT "99/99/99":U 
     LABEL "Retro Respective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE reviewby AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewed By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 28.2 BY 1 NO-UNDO.

DEFINE VARIABLE reviewDate AS DATE FORMAT "99/99/99":U 
     LABEL "Review date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE validDays AS INTEGER FORMAT "->>>>":U INITIAL 0 
     LABEL "Valid for Days" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 94 BY 12.76.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 93 BY 4.43.

DEFINE VARIABLE tactive AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 3.4 BY .81 NO-UNDO.

DEFINE VARIABLE tgIdentity AS LOGICAL INITIAL no 
     LABEL "Identity Verification" 
     VIEW-AS TOGGLE-BOX
     SIZE 22 BY .81 NO-UNDO.

DEFINE VARIABLE tgInsurance AS LOGICAL INITIAL no 
     LABEL "Insurance" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.

DEFINE VARIABLE tgOther AS LOGICAL INITIAL no 
     LABEL "Other" 
     VIEW-AS TOGGLE-BOX
     SIZE 9 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     CbState AT ROW 1.14 COL 18.6 COLON-ALIGNED WIDGET-ID 314
     fMarkMandatory1 AT ROW 1.38 COL 42.6 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     cbQual AT ROW 2.24 COL 18.6 COLON-ALIGNED WIDGET-ID 218
     fMarkMandatory2 AT ROW 2.48 COL 76.2 COLON-ALIGNED NO-LABEL WIDGET-ID 330
     fLicensenum AT ROW 3.33 COL 18.6 COLON-ALIGNED WIDGET-ID 42
     cbAuth AT ROW 4.43 COL 18.6 COLON-ALIGNED WIDGET-ID 220
     feffDate AT ROW 4.43 COL 72 COLON-ALIGNED WIDGET-ID 6
     fProvider AT ROW 5.52 COL 18.6 COLON-ALIGNED WIDGET-ID 236
     fexpDate AT ROW 5.52 COL 72 COLON-ALIGNED WIDGET-ID 22
     tgOther AT ROW 8.86 COL 9 WIDGET-ID 354
     tgInsurance AT ROW 10.19 COL 9 WIDGET-ID 356
     fcoverageAmt AT ROW 11.1 COL 51.2 RIGHT-ALIGNED WIDGET-ID 222
     RetroDate AT ROW 11.1 COL 77.4 COLON-ALIGNED WIDGET-ID 226
     fdeductAmt AT ROW 12.19 COL 51.2 RIGHT-ALIGNED WIDGET-ID 224
     fAggregateAmt AT ROW 12.19 COL 77.4 COLON-ALIGNED WIDGET-ID 328
     InsuranceBroker AT ROW 13.29 COL 34.2 COLON-ALIGNED WIDGET-ID 234
     tgIdentity AT ROW 15.38 COL 9 WIDGET-ID 358
     cbservicename AT ROW 16.48 COL 34 COLON-ALIGNED WIDGET-ID 338
     flrate AT ROW 17.52 COL 34 COLON-ALIGNED WIDGET-ID 340
     fltax AT ROW 18.57 COL 34 COLON-ALIGNED WIDGET-ID 342
     Enotes AT ROW 21.67 COL 6 NO-LABEL WIDGET-ID 34
     tactive AT ROW 25.67 COL 22.6 WIDGET-ID 244
     cbStatus AT ROW 25.38 COL 64 COLON-ALIGNED WIDGET-ID 30
     reviewDate AT ROW 26.52 COL 20.6 COLON-ALIGNED WIDGET-ID 320
     reviewby AT ROW 26.52 COL 64 COLON-ALIGNED WIDGET-ID 326
     validDays AT ROW 27.62 COL 20.6 COLON-ALIGNED WIDGET-ID 324
     nxtReviewDate AT ROW 27.67 COL 64 COLON-ALIGNED WIDGET-ID 322
     Btn_OK AT ROW 29.71 COL 35
     Btn_Cancel AT ROW 29.71 COL 50
     "Review" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 24.67 COL 7 WIDGET-ID 318
          FONT 6
     "Notes:" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 20.95 COL 6.4 WIDGET-ID 242
     "Active:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 25.71 COL 15.4 WIDGET-ID 246
     "Qualification Type" VIEW-AS TEXT
          SIZE 22 BY .62 AT ROW 7.52 COL 7 WIDGET-ID 336
          FONT 6
     RECT-1 AT ROW 7.76 COL 6 WIDGET-ID 238
     RECT-61 AT ROW 25 COL 6 WIDGET-ID 316
     SPACE(3.99) SKIP(1.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Qualifications"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       Enotes:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN fcoverageAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       fcoverageAmt:AUTO-RESIZE IN FRAME Dialog-Frame      = TRUE.

/* SETTINGS FOR FILL-IN fdeductAmt IN FRAME Dialog-Frame
   ALIGN-R                                                              */
/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN reviewby IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       reviewby:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN reviewDate IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       reviewDate:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR TOGGLE-BOX tactive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Qualifications */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplSuccess = false.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
do:
  if validateQual() 
   then 
    do:
      run newQualification in this-procedure.
      /* If there was any error then do not close the dialog. */
      if not oplSuccess
       then
        return no-apply.
    end.   
  else
   return no-apply. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAuth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAuth Dialog-Frame
ON VALUE-CHANGED OF cbAuth IN FRAME Dialog-Frame /* Authorized by */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbQual
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbQual Dialog-Frame
ON VALUE-CHANGED OF cbQual IN FRAME Dialog-Frame /* Qualification */
do:
  run valChgQual in this-procedure.
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbservicename
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbservicename Dialog-Frame
ON VALUE-CHANGED OF cbservicename IN FRAME Dialog-Frame /* Service Name */
DO:
  run enableDisableSave in this-procedure no-error.
  if cbservicename:screen-value = 'SMT'
   then
    assign
       flrate:sensitive  = true
       fltax:sensitive   = true.
   else
    assign
       flrate:sensitive  = false
       fltax:sensitive   = false
       flrate:screen-value = string(0)
       fltax:screen-value  = string(0).
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CbState Dialog-Frame
ON VALUE-CHANGED OF CbState IN FRAME Dialog-Frame /* State */
do:
  run valChgState in this-procedure.
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus Dialog-Frame
ON VALUE-CHANGED OF cbStatus IN FRAME Dialog-Frame /* Status */
do:
  run setWidgetState in this-procedure.
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Enotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Enotes Dialog-Frame
ON VALUE-CHANGED OF Enotes IN FRAME Dialog-Frame
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAggregateAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAggregateAmt Dialog-Frame
ON VALUE-CHANGED OF fAggregateAmt IN FRAME Dialog-Frame /* Aggregate Amount $ */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fcoverageAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fcoverageAmt Dialog-Frame
ON VALUE-CHANGED OF fcoverageAmt IN FRAME Dialog-Frame /* Coverage Amount $ */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fdeductAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fdeductAmt Dialog-Frame
ON VALUE-CHANGED OF fdeductAmt IN FRAME Dialog-Frame /* Deductible Amount $ */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME feffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL feffDate Dialog-Frame
ON VALUE-CHANGED OF feffDate IN FRAME Dialog-Frame /* Effective */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fexpDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpDate Dialog-Frame
ON LEAVE OF fexpDate IN FRAME Dialog-Frame /* Expiration */
do:
  run expDtLeave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fexpDate Dialog-Frame
ON VALUE-CHANGED OF fexpDate IN FRAME Dialog-Frame /* Expiration */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fLicensenum
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fLicensenum Dialog-Frame
ON VALUE-CHANGED OF fLicensenum IN FRAME Dialog-Frame /* Number/ID */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flrate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flrate Dialog-Frame
ON VALUE-CHANGED OF flrate IN FRAME Dialog-Frame /* Rate $ */
DO:
  run enableDisableSave in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fltax
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fltax Dialog-Frame
ON VALUE-CHANGED OF fltax IN FRAME Dialog-Frame /* Tax $ */
DO:
  run enableDisableSave in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fProvider
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fProvider Dialog-Frame
ON VALUE-CHANGED OF fProvider IN FRAME Dialog-Frame /* Issuer */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME InsuranceBroker
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL InsuranceBroker Dialog-Frame
ON VALUE-CHANGED OF InsuranceBroker IN FRAME Dialog-Frame /* Broker */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME nxtReviewDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL nxtReviewDate Dialog-Frame
ON VALUE-CHANGED OF nxtReviewDate IN FRAME Dialog-Frame /* Next Review Date */
do:
  run valChgNxtRevDt in this-procedure.
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RetroDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RetroDate Dialog-Frame
ON VALUE-CHANGED OF RetroDate IN FRAME Dialog-Frame /* Retro Respective */
do:
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgIdentity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgIdentity Dialog-Frame
ON VALUE-CHANGED OF tgIdentity IN FRAME Dialog-Frame /* Identity Verification */
DO: 
  tgIdentity:checked = true.
  if tgIdentity:checked
   then
    do:
      assign
          fAggregateAmt:sensitive      = false
          RetroDate:sensitive          = false
          InsuranceBroker:sensitive    = false
          fcoverageAmt:sensitive       = false
          fdeductAmt:sensitive         = false
          fltax:sensitive              = true
          flrate:sensitive             = true
          cbservicename:sensitive      = true
          tgOther:checked              = false
          tgInsurance:checked          = false
          fAggregateAmt:screen-value   = ''
          RetroDate:screen-value       = ''
          InsuranceBroker:screen-value = ''
          fcoverageAmt:screen-value    = ''
          fdeductAmt:screen-value      = ''.
    end.
    else
     assign
        fltax:sensitive              = false
        flrate:sensitive             = false
        cbservicename:sensitive      = false 
        fltax:screen-value           = ''
        flrate:screen-value          = ''
        cbservicename:screen-value   = 'None'.
        
   run enableDisableSave in this-procedure no-error.     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgInsurance
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgInsurance Dialog-Frame
ON VALUE-CHANGED OF tgInsurance IN FRAME Dialog-Frame /* Insurance */
DO:
  tgInsurance:checked = true.
  if tgInsurance:checked
   then
    do:
      assign
          fAggregateAmt:sensitive      = true
          RetroDate:sensitive          = true
          InsuranceBroker:sensitive    = true
          fcoverageAmt:sensitive       = true
          fdeductAmt:sensitive         = true
          fltax:sensitive              = false
          flrate:sensitive             = false
          cbservicename:sensitive      = false
          tgOther:checked              = false
          tgIdentity:checked           = false
          fltax:screen-value           = ''
          flrate:screen-value          = ''
          cbservicename:screen-value   = 'None'.
    end.
    else
     assign
        fAggregateAmt:sensitive      = false
        RetroDate:sensitive          = false
        InsuranceBroker:sensitive    = false
        fcoverageAmt:sensitive       = false
        fdeductAmt:sensitive         = false
        fAggregateAmt:screen-value   = ''
        RetroDate:screen-value       = ''
        InsuranceBroker:screen-value = ''
        fcoverageAmt:screen-value    = ''
        fdeductAmt:screen-value      = ''.
        
   run enableDisableSave in this-procedure no-error.     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgOther
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgOther Dialog-Frame
ON VALUE-CHANGED OF tgOther IN FRAME Dialog-Frame /* Other */
DO: 
  tgOther:checked = true.
  assign
     fAggregateAmt:sensitive      = false
     RetroDate:sensitive          = false
     InsuranceBroker:sensitive    = false
     fcoverageAmt:sensitive       = false
     fdeductAmt:sensitive         = false
     fltax:sensitive              = false
     flrate:sensitive             = false
     cbservicename:sensitive      = false
     fAggregateAmt:screen-value   = ''
     RetroDate:screen-value       = ''
     InsuranceBroker:screen-value = ''
     fcoverageAmt:screen-value    = ''
     fdeductAmt:screen-value      = ''
     fltax:screen-value           = ''
     flrate:screen-value          = ''
     cbservicename:screen-value   = 'None'.
  
  if tgOther:checked
   then
    do:
      tgInsurance:checked = false.
      tgIdentity:checked  = false.
    end.
   
  run enableDisableSave in this-procedure no-error. 
    
  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME validDays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL validDays Dialog-Frame
ON VALUE-CHANGED OF validDays IN FRAME Dialog-Frame /* Valid for Days */
do:
  nxtReviewDate:screen-value = string(today + Validdays:input-value).
  run enableDisableSave in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

publish "getCredentialsName" (output cCurrUser).

/* Getting states from config. Also adding National here */
publish "getSearchStates" (output cList).
if cList > "" 
 then
  cList = "," + {&NationalState} + "," + cList.

/* Adding a non-configured state to 'state' combobox to create/modify qualification for that state */
if lookup(ipcStateID,cList) = 0 
 then
  do:
    publish "getStateName" (input ipcStateID, 
                            output std-ch).
    cList = cList + "," + std-ch + "," + ipcStateID.
  end.
 
find first tQualification no-error.
if avail(tQualification) then
do:
  if lookup(tQualification.StateID,cList) = 0
   then
    do:
      publish "getStateName" (input tQualification.StateID, 
                              output std-ch).
      cList = cList + "," + std-ch + "," + tQualification.StateID.
    end.  
end.

/* Add item-pair "--Select State--, " */

cbState:list-item-pairs in frame {&frame-name} = {&SelectState} + ",".
if clist ne ""
 then
  cbState:list-item-pairs in frame {&frame-name} = cbState:list-item-pairs  + cList.
    
publish "getComplianceCodesList"(input {&Qualification},
                                 input ",",             
                                 output cQualList,
                                 output std-lo,              
                                 output std-ch).
if not std-lo 
 then
  do:
    message std-ch
      view-as alert-box error buttons ok.
    return.
  end.

cbqual:list-items in frame {&frame-name} = cQualList.

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.
 
  Frame {&FRAME-NAME}:title = ipcentity.
  publish "GetSysPropListMinusID" ("COM", "Qualification", "Status", {&NeedsReview}, output std-ch).
  
  if std-ch ne ""
   then
    cbStatus:list-item-pairs = replace(std-ch,"^",",").
  else
   cbStatus:list-item-pairs = ",". 
  
  run displayData.
   
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE assignValue Dialog-Frame 
PROCEDURE assignValue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter centity as character no-undo.
  do with frame {&frame-name}:
  end.
  find first tQualification where tQualification.entity = centity and tQualification.entityId = ipcEntityID no-error.
        if available tQualification 
         then
          do:
            if tQualification.stat ne "" and
               not can-do(cbStatus:list-item-pairs,tQualification.stat) 
             then
              run updateStatusCombo (input tQualification.stat).
           
            if lookup(tQualification.Qualification,cQualList) = 0
              then
               cbQual:add-last(tQualification.Qualification).
 
            assign
                reviewDate     :screen-value = string( tQualification.reviewDate)
                reviewby       :screen-value = tQualification.reviewBy 
                nxtreviewDate  :screen-value = string(tQualification.nextReviewDueDate)
                validdays      :screen-value = string(tQualification.numvaliddays)
                cbQual         :screen-value = string(tQualification.Qualification)
                cbStatus       :screen-value = if cbStatus:list-item-pairs = "," then "" else tQualification.stat
                cbAuth         :screen-value = if string(tQualification.authorizedBy) = {&CompanyCode} then {&First-Party} else {&Third-Party}
                feffDate       :screen-value = string(tQualification.EffectiveDate)
                fexpDate       :screen-value = string(tQualification.ExpirationDate)
                Enotes         :screen-value = string(tQualification.notes)
                fLicensenum    :screen-value = tQualification.QualificationNum
                cbState        :screen-value = if clist = "" or lookup(tqualification.stateid,cbState:list-item-pairs) = 0 then "" else tQualification.stateid 
                fProvider      :screen-value = tQualification.provider
                tactive        :screen-value = string(tQualification.activ)
                .
                case tQualification.QualificationType :
                when "Identity Verification"  
                 then
                  do:
                    assign
                       tgIdentity:checked              = true
                       tgIdentity:sensitive            = false
                       tgInsurance:sensitive           = false
                       tgOther:sensitive               = false
                       fcoverageAmt:screen-value       = ''
                       fdeductAmt:screen-value         = ''
                       RetroDate:screen-value          = ''
                       fAggregateAmt:screen-value      = ''
                       InsuranceBroker:screen-value    = ''
                       flrate:screen-value             = string(tQualification.coverageAmt)         
                       fltax:screen-value              = string(tQualification.deductibleAmt)        
                       cbservicename:screen-value      = string(tQualification.insuranceBroker).
                    apply "VALUE-changed" to  tgIdentity.    
                  end.
                when "Insurance"
                 then
                  do:
                    assign
                       tgInsurance:checked             = true
                       tgIdentity:sensitive            = false
                       tgInsurance:sensitive           = false
                       tgOther:sensitive               = false
                       fcoverageAmt:screen-value       =  string(tQualification.coverageAmt) 
                       fdeductAmt:screen-value         =  string(tQualification.deductibleamt)
                       insurancebroker:screen-value    = tQualification.insurancebroker 
                       fAggregateAmt:screen-value      = string(tQualification.aggregateamt)
                       RetroDate:screen-value          = string(tQualification.retrodate)
                       fltax:screen-value              = ''
                       flrate:screen-value             = ''
                       cbservicename:screen-value      = 'None'.
                    apply "VALUE-changed" to  tgInsurance.       
                  end.
                when "Other" then
                 do:
                   assign
                       tgOther:checked                 = true
                       tgIdentity:sensitive            = false
                       tgInsurance:sensitive           = false
                       tgOther:sensitive               = false 
                       fAggregateAmt:screen-value      = ''
                       RetroDate:screen-value          = ''
                       InsuranceBroker:screen-value    = ''
                       fcoverageAmt:screen-value       = ''
                       fdeductAmt:screen-value         = ''
                       fltax:screen-value              = ''
                       flrate:screen-value             = ''
                       cbservicename:screen-value      = 'None'.
                   apply "VALUE-changed" to  tgOther.        
                 end.
              end case.  
                 
          end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable idverificationlist as character no-undo.
  do with frame {&frame-name}:
  end.
  
  publish "getidverificationsw" (output table idverificationsw ).
  for each idverificationsw no-lock by code :
    idverificationlist = addDelimiter(idverificationlist,",") + (idverificationsw.description + ',' + idverificationsw.code).
  end.
  cbservicename:list-item-pairs  = idverificationlist.
 
  case ipcAction:
    when {&PersonNew} 
     then
      do:
       assign
            cbState:sensitive              = true
            cbQual:sensitive               = true
            cbState:screen-value           = ""
            cbAuth:screen-value            = {&Third-Party}
            cbStatus:screen-value          = if cbStatus:list-item-pairs = "," then "" else {&GoodStanding}
            frame dialog-frame:title       = "New Person Qualification"
            reviewBy:screen-value          = cCurrUser
            reviewDate:screen-value        = string(today)
            lIsNew                         = true
            Btn_OK:label                   = "Create"
            Btn_OK:tooltip                 = "Create" 
            fMarkMandatory1:hidden         = false
            fMarkMandatory1:screen-value   = {&mandatory}
            fMarkMandatory2:hidden         = false
            fMarkMandatory2:screen-value   = {&mandatory}
            tgOther:checked                = true
            .
            apply "VALUE-changed" to  tgOther. 
      end.
    when {&PersonModify} 
     then
      do:
        assign
            cbState:sensitive        = false
            cbQual:sensitive         = false
            cbState:screen-value     = ipcStateID 
            frame dialog-frame:title = "Modify Person Qualification"
            Btn_OK:label             = "Save"
            Btn_OK:tooltip           = "Save" 
            fMarkMandatory1:hidden   = true
            fMarkMandatory2:hidden   = true
            .
       run assignValue(input {&PersonCode} ).     

        
      end.  
      
    when   {&OrganizationNew} 
     then
      do:
        assign
            cbState:sensitive              = true
            cbQual:sensitive               = true
            frame Dialog-Frame:title       = "New Organization Qualification"
            cbState:screen-value           = ""
            cbAuth:screen-value            = {&Third-Party}
            cbStatus:screen-value          = if cbStatus:list-item-pairs = "," then "" else {&GoodStanding}
            reviewBy:screen-value          = cCurrUser
            reviewDate:screen-value        = string(today)
            lIsNew                         = true
            Btn_OK:label                   = "Create" 
            Btn_OK:tooltip                 = "Create" 
            fMarkMandatory1:hidden         = false
            fMarkMandatory1:screen-value   = {&mandatory}
            fMarkMandatory2:hidden         = false
            fMarkMandatory2:screen-value   = {&mandatory}
            tgOther:checked                = true
            .
            apply "VALUE-changed" to  tgOther. 
          
      end.    
    when {&OrganizationModify} 
     then
      do:
        assign
            frame Dialog-Frame:title = "Modify Organization Qualification" 
            cbState:sensitive        = false
            cbQual:sensitive         = false
            cbState:screen-value     = ipcStateID
            Btn_OK:label             = "Save" 
            Btn_OK:tooltip           = "Save" 
            fMarkMandatory1:hidden   = true
            fMarkMandatory2:hidden   = true
            .
        run assignValue(input {&OrganizationCode}  ).         
        
      end.
    when {&OrganizationCopy} 
     then
      do:
        assign
            frame Dialog-Frame:title       = "New Organization Qualification" 
            cbState:sensitive              = true
            cbQual:sensitive               = true
            cbState:screen-value           = ipcStateID
            Btn_OK:label                   = "Create" 
            Btn_OK:tooltip                 = "Create" 
            fMarkMandatory1:hidden         = false
            fMarkMandatory1:screen-value   = {&mandatory}
            fMarkMandatory2:hidden         = false
            fMarkMandatory2:screen-value   = {&mandatory}
            lIsNew                         = true
            tgOther:checked                = true
            .
            apply "VALUE-changed" to  tgOther. 
         run assignValue(input {&OrganizationCode}  ).         

      end.
    when {&PersonCopy} 
     then
      do:
        assign
            frame Dialog-Frame:title       = "New Person Qualification" 
            cbState:sensitive              = true
            cbQual:sensitive               = true
            cbState:screen-value           = ipcStateID
            Btn_OK:label                   = "Create" 
            Btn_OK:tooltip                 = "Create" 
            fMarkMandatory1:hidden         = false
            fMarkMandatory1:screen-value   = {&mandatory}
            fMarkMandatory2:hidden         = false
            fMarkMandatory2:screen-value   = {&mandatory}
            lIsNew                         = true
            .
        run assignValue(input {&PersonCode} ).     

      end.
    when {&PersonRenew} 
     then
      do:
        assign
            frame Dialog-Frame:title       = "Renew Person Qualification" 
            cbState:sensitive              = false
            cbQual:sensitive               = false
            cbAuth:sensitive               = false
            cbState:screen-value           = ipcStateID
            Btn_OK:label                   = "Create" 
            Btn_OK:tooltip                 = "Create" 
            fMarkMandatory1:hidden         = false
            fMarkMandatory1:screen-value   = {&mandatory}
            fMarkMandatory2:hidden         = false
            fMarkMandatory2:screen-value   = {&mandatory}
            lIsRenew                       = true
            .
        
        find first tQualification no-error.
        if available tQualification 
         then
          do:
            if tQualification.stat ne "" and
               not can-do(cbStatus:list-item-pairs,tQualification.stat) 
             then
              run updateStatusCombo (input tQualification.stat).
            
            if lookup(tQualification.Qualification,cQualList) = 0
             then
              cbQual:add-last(tQualification.Qualification).
              
            cRenewQualID = string(tQualification.qualificationID).            
            assign 
                reviewBy       :screen-value = cCurrUser
                reviewDate     :screen-value = string(today)
                nxtreviewDate  :screen-value = string(tQualification.nextReviewDueDate)
                validdays      :screen-value = string(tQualification.numvaliddays)
                cbQual         :screen-value = string(tQualification.Qualification)
                cbStatus       :screen-value = 'G' /* if cbStatus:list-item-pairs = "," then "" else tQualification.stat */
                cbAuth         :screen-value = if string(tQualification.authorizedBy) = {&CompanyCode} then {&First-Party} else {&Third-Party}
                feffDate       :screen-value = string(tQualification.EffectiveDate)
                fexpDate       :screen-value = string(tQualification.ExpirationDate)
                Enotes         :screen-value = string(tQualification.notes)
                fLicensenum    :screen-value = tQualification.QualificationNum
                cbState        :screen-value = if clist = "" then "" else tQualification.stateid
                fProvider      :screen-value = tQualification.provider
                tactive        :screen-value = string(true)
                .
            case tQualification.QualificationType :
                when "Identity Verification"  
                 then
                  do:
                    assign
                       tgIdentity:checked              = true
                       tgIdentity:sensitive            = false
                       tgInsurance:sensitive           = false
                       tgOther:sensitive               = false
                       fcoverageAmt:screen-value       = ''
                       fdeductAmt:screen-value         = ''
                       RetroDate:screen-value          = ''
                       fAggregateAmt:screen-value      = ''
                       InsuranceBroker:screen-value    = ''
                       flrate:screen-value             = string(tQualification.coverageAmt)         
                       fltax:screen-value              = string(tQualification.deductibleAmt)        
                       cbservicename:screen-value      = string(tQualification.insuranceBroker).
                    apply "VALUE-changed" to  tgIdentity.    
                  end.
                when "Insurance"
                 then
                  do:
                    assign
                       tgInsurance:checked             = true
                       tgIdentity:sensitive            = false
                       tgInsurance:sensitive           = false
                       tgOther:sensitive               = false
                       fcoverageAmt:screen-value       =  string(tQualification.coverageAmt) 
                       fdeductAmt:screen-value         =  string(tQualification.deductibleamt)
                       insurancebroker:screen-value    = tQualification.insurancebroker 
                       fAggregateAmt:screen-value      = string(tQualification.aggregateamt)
                       RetroDate:screen-value          = string(tQualification.retrodate)
                       fltax:screen-value              = ''
                       flrate:screen-value             = ''
                       cbservicename:screen-value      = 'None'.
                    apply "VALUE-changed" to  tgInsurance.       
                  end.
                when "Other" then
                 do:
                   assign
                       tgOther:checked                 = true
                       tgIdentity:sensitive            = false
                       tgInsurance:sensitive           = false
                       tgOther:sensitive               = false 
                       fAggregateAmt:screen-value      = ''
                       RetroDate:screen-value          = ''
                       InsuranceBroker:screen-value    = ''
                       fcoverageAmt:screen-value       = ''
                       fdeductAmt:screen-value         = ''
                       fltax:screen-value              = ''
                       flrate:screen-value             = ''
                       cbservicename:screen-value      = 'None'.
                   apply "VALUE-changed" to  tgOther.        
                 end.
              end case.
          end.
      end.     
    when {&OrganizationRenew}
     then    
      run displayOrgRenewData in this-procedure.
  
  end case.

  /* Keep the copy of initial values. Required in enableDisableSave. */ 
  assign
      chQual            = cbQual     :input-value
      chLicensenum      = fLicensenum:input-value
      chStatus          = cbStatus   :input-value
      chProvider        = fProvider  :input-value
      chAuth            = cbAuth     :input-value
      daEffDate         = feffDate   :input-value
      daExpDate         = fexpDate   :input-value
      inCoverageAmt     = fcoverageAmt   :input-value
      daRetroDate       = RetroDate      :input-value
      inDeductAmt       = fdeductAmt     :input-value
      chInsuranceBroker = InsuranceBroker:input-value
      chEnotes          = Enotes         :input-value
      inValidDays       = validDays      :input-value
      daNxtReviewDate   = nxtReviewDate  :input-value
      inAggregateAmt    = fAggregateAmt  :input-value
      cservicename      = cbservicename  :input-value
      crate             = flrate         :input-value
      ctax              = fltax          :input-value
      no-error.
      
   apply "VALUE-changed" to  cbservicename. 
   
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayOrgRenewData Dialog-Frame 
PROCEDURE displayOrgRenewData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
        assign
            frame Dialog-Frame:title       = "Renew Organization Qualification" 
            cbState:sensitive              = false
            cbQual:sensitive               = false
            cbAuth:sensitive               = false
            cbState:screen-value           = ipcStateID
            Btn_OK:label                   = "Create" 
            Btn_OK:tooltip                 = "Create" 
            fMarkMandatory1:hidden         = false
            fMarkMandatory1:screen-value   = {&mandatory}
            fMarkMandatory2:hidden         = false
            fMarkMandatory2:screen-value   = {&mandatory}
            lIsRenew                       = true
            .
        
        find first tQualification no-error.
        if available tQualification 
         then
          do:
            if tQualification.stat ne "" and
               not can-do(cbStatus:list-item-pairs,tQualification.stat) 
             then
              run updateStatusCombo (input tQualification.stat).
            
            if lookup(tQualification.Qualification,cQualList) = 0
             then
              cbQual:add-last(tQualification.Qualification).
              
            cRenewQualID = string(tQualification.qualificationID).
            assign 
                reviewBy       :screen-value = cCurrUser
                reviewDate     :screen-value = string(today)
                nxtreviewDate  :screen-value = string(tQualification.nextReviewDueDate)
                validdays      :screen-value = string(tQualification.numvaliddays)
                cbQual         :screen-value = string(tQualification.Qualification)
                cbStatus       :screen-value =  'G'/* if cbStatus:list-item-pairs = "," then "" else tQualification.stat */
                cbAuth         :screen-value = if string(tQualification.authorizedBy) = {&CompanyCode} then {&First-Party} else {&Third-Party}
                feffDate       :screen-value = string(tQualification.EffectiveDate)
                fexpDate       :screen-value = string(tQualification.ExpirationDate)
                Enotes         :screen-value = string(tQualification.notes)
                fLicensenum    :screen-value = tQualification.QualificationNum
                insurancebroker:screen-value = tQualification.insurancebroker
                cbState        :screen-value = if clist = "" then "" else tQualification.stateid
                fProvider      :screen-value = tQualification.provider
                fcoverageamt   :screen-value = string(tQualification.coverageamt)
                fdeductAmt     :screen-value = string(tQualification.deductibleamt)
                retrodate      :screen-value = string(tQualification.retrodate)
                tactive        :screen-value = string(true)
                fAggregateAmt  :screen-value = string(tQualification.aggregateamt)
                .
          end.
      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lenable as logical no-undo. 
  do with frame {&frame-name}:
  end.
  if (tgOther:checked or tgInsurance:checked or tgIdentity:checked)
   then
    lenable = true.
   else
    lenable = false.

  if lIsNew
   then
    do:
      Btn_OK:sensitive =  (cbstate:input-value ne "" and cbQual:input-value ne "" and
                           (if ipcAction = {&PersonNew} or ipcAction = {&OrganizationNew}
                             then (CbState:screen-value ne "")
                             else true ) and lenable) no-error.
    end.    
  else             
   Btn_OK:sensitive = not (chLicensenum      = fLicensenum:input-value     and
                           chStatus          = cbStatus:input-value        and
                           chProvider        = fProvider:input-value       and
                           chAuth            = cbAuth:input-value          and
                           daEffDate         = feffDate:input-value        and
                           daExpDate         = fexpDate:input-value        and
                           inCoverageAmt     = fcoverageAmt:input-value    and
                           daRetroDate       = RetroDate:input-value       and
                           inDeductAmt       = fdeductAmt:input-value      and
                           chInsuranceBroker = InsuranceBroker:input-value and
                           chEnotes          = Enotes:input-value          and
                           inValidDays       = validDays:input-value       and
                           daNxtReviewDate   = nxtReviewDate:input-value   and
                           crate             = flrate:input-value          and
                           ctax              = fltax:input-value           and
                           cservicename      = cbservicename:input-value   and
                           inAggregateAmt    = fAggregateAmt:input-value)
                           no-error.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY CbState fMarkMandatory1 cbQual fMarkMandatory2 fLicensenum cbAuth 
          feffDate fProvider fexpDate tgOther tgInsurance fcoverageAmt RetroDate 
          fdeductAmt fAggregateAmt InsuranceBroker tgIdentity cbservicename 
          flrate fltax Enotes tactive cbStatus reviewDate reviewby validDays 
          nxtReviewDate 
      WITH FRAME Dialog-Frame.
  ENABLE CbState cbQual fLicensenum cbAuth feffDate fProvider fexpDate tgOther 
         tgInsurance fcoverageAmt RetroDate fdeductAmt fAggregateAmt 
         InsuranceBroker tgIdentity cbservicename flrate fltax Enotes cbStatus 
         validDays nxtReviewDate Btn_Cancel RECT-1 RECT-61 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE expDtLeave Dialog-Frame 
PROCEDURE expDtLeave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if (nxtreviewDate:input-value in frame {&frame-name}) gt (fexpDate:input-value) 
   then
    do:
      if (date(fexpDate:input-value) - 14) gt today 
       then
        nxtreviewDate:screen-value = string(date(fexpDate:input-value) - 14).
      else
        nxtreviewDate:screen-value = string(today + 1).
    end.
  
  apply "value-changed" to nxtreviewDate.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newQualification Dialog-Frame 
PROCEDURE newQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  find first tQualification no-error.
  if not available tQualification 
   then
    create tQualification.
 
  assign 
      tQualification.entity               = ipcEntity 
      tQualification.entityID             = ipcEntityID
      tQualification.qualification        = cbqual:input-value in frame {&frame-name}
      tQualification.stat                 = cbstatus:input-value
      tQualification.qualificationNumber  = fLicensenum:input-value
      tQualification.provider             = fprovider:input-value
      tQualification.authorizedby         = if cbauth:input-value = {&First-Party} then {&CompanyCode} else {&ThirdParty}
      tQualification.effectiveDate        = feffDate:input-value
      tQualification.expirationdate       = fexpDate:input-value
      tQualification.notes                = enotes:input-value
      tQualification.retrodate            = retrodate:input-value
      tQualification.coverageAmt          = 0.00
      tQualification.deductibleAmt        = 0.00
      tQualification.insuranceBroker      = ''
      tQualification.stateID              = cbState:input-value
      tQualification.reviewDate           = reviewDate:input-value
      tQualification.reviewBy             = reviewby:input-value
      tQualification.nextReviewDueDate    = nxtreviewDate:input-value
      tQualification.numvaliddays         = validDays:input-value
      tQualification.aggregateAmt         = fAggregateAmt:input-value
      tQualification.active               = tactive:input-value  .
      if tgOther:checked
       then
        tQualification.QualificationType = 'Other'.
      
      if tgInsurance:checked
       then
        do:
          assign
             tQualification.coverageAmt          = fcoverageAmt:input-value
             tQualification.deductibleAmt        = fdeductAmt:input-value
             tQualification.insuranceBroker      = insuranceBroker:input-value 
             tQualification.QualificationType    = "Insurance". 
        end.
       
       if tgIdentity:checked
        then
         do:
           assign
              tQualification.coverageAmt          = flrate:input-value
              tQualification.deductibleAmt        = fltax:input-value
              tQualification.insuranceBroker      = cbservicename:input-value
              tQualification.QualificationType    = "Identity verification".
         end.

  if not valid-handle(iphentitydata) 
   then
    do:
      message "Data Model not found."
          view-as alert-box error buttons ok.
      return.
    end.

  if lIsNew 
   then
    run NewQualification in iphentitydata (input table tQualification,
                                             output opiQualID,
                                             output opiReviewID,
                                             output oplSuccess,
                                             output std-ch).
                                             
 else if lIsRenew
  then
   run RenewQualification in iphentitydata(input  cRenewQualID,
                                           input table tQualification,
                                           output opiQualID,
                                           output opiReviewID,
                                           output oplSuccess,
                                           output std-ch).
     

   
  else
   run ModifyQualification in iphentitydata (input table tQualification,
                                               output oplSuccess,
                                               output std-ch).
  if not oplSuccess 
   then 
    message std-ch
      view-as alert-box error buttons ok.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable cAuthByVal as character no-undo.

 if cbstatus:input-value in frame {&frame-name} = {&Waived} 
  then
   do:
      assign 
          fLicenseNum    :sensitive      = false
          fProvider      :sensitive      = false
          cbAuth         :sensitive      = false
          fCoverageAmt   :sensitive      = false
          fDeductAmt     :sensitive      = false
          fAggregateAmt  :sensitive      = false
          insuranceBroker:sensitive      = false
          retroDate      :sensitive      = false
          fLicenseNum    :screen-value   = ""
          fProvider      :screen-value   = ""
          cbAuth         :list-items     = ""
          fCoverageAmt   :screen-value   = ""
          fDeductAmt     :screen-value   = ""
          InsuranceBroker:screen-value   = ""
          retroDate      :screen-value   = ""
          fAggregateAmt  :screen-value   = ""
          .
     if lIsNew 
      then
       cbQual:sensitive  = false.
   end.
  else 
   do:
     assign  
         cAuthByVal                     = cbAuth:input-value 
         cbAuth         :list-items     = {&first-party} + "," + {&Third-Party} 
         cbAuth         :screen-value   = cAuthByVal 
         fLicenseNum    :sensitive      = true 
         fProvider      :sensitive      = true 
         cbAuth         :sensitive      = true 
         fCoverageAmt   :sensitive      = true 
         fDeductAmt     :sensitive      = true 
         insuranceBroker:sensitive      = true 
         retroDate      :sensitive      = true  
         fAggregateAmt  :sensitive      = true 
         . 
     if lIsNew  
      then 
       cbQual:sensitive  = true. 
   end.

  apply "value-changed" to cbQual.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE updateStatusCombo Dialog-Frame 
PROCEDURE updateStatusCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>                                                     rre
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcQualStat as character  no-undo.
  
  define variable hcmdhandle  as handle     no-undo.

  do with frame {&frame-name}:
  end.

  hcmdhandle = cbStatus:handle.
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", ipcQualStat, output std-ch).

  hcmdhandle:add-last(std-ch,ipcQualStat).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChgNxtRevDt Dialog-Frame 
PROCEDURE valChgNxtRevDt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable myDate as date no-undo.

  myDate = date(nxtReviewDate:input-value in frame {&frame-name}) no-error.

  if not error-status:error and not myDate = ? 
   then
    Validdays:screen-value = string(date(nxtReviewDate:input-value) - today).
  else 
   Validdays:screen-value = "0".
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChgQual Dialog-Frame 
PROCEDURE valChgQual :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iReviewPeriod as integer   no-undo.
  
  if lIsNew 
   then
    do:
      if cbStatus:input-value in frame {&frame-name} = {&GoodStanding} 
       then
        do:
          publish "getQualReviewPeriod" (input {&Qualification},
                                         input cbQual:input-value, 
                                         output iReviewPeriod,
                                         output std-lo,
                                         output std-ch).
          
          if std-lo and iReviewPeriod ne 0 
           then
            nxtreviewDate:screen-value = string(today + iReviewPeriod).      
        
          if (fexpDate:input-value ne ?) and
             (nxtreviewDate:input-value gt fexpDate:input-value) 
           then
            do:
              if (date(fexpDate:screen-value) - 14) lt today 
               then
                nxtreviewDate:screen-value = string(today + 1).
              else
               nxtreviewDate:screen-value = string(date(fexpDate:input-value) - 14 ).
            end.
        end.
      else
       nxtreviewDate:screen-value = "".
    end.
  
  apply "value-changed" to nxtreviewDate.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE valChgState Dialog-Frame 
PROCEDURE valChgState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* removing select state once other state is selected */
  do with frame {&frame-name}:
  end.

  {lib/modifylist.i &cbhandle=cbState:handle &list=list-item-pairs &delimiter = "," &item = '{&SelectState}'}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateQual Dialog-Frame 
FUNCTION validateQual returns logic
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  define variable iReviewPeriod as integer  no-undo.

  /* Validations */
  if cbState:input-value in frame {&frame-name} = "" 
   then
    do:
      message "State cannot be blank."
        view-as alert-box info buttons ok.
      return false.
    end.
  
  if cbqual:input-value = "" 
   then
    do:
      message "Qualification cannot be blank"
        view-as alert-box info buttons ok.
      return false.
    end.
    
  if fexpDate:input-value < feffDate:input-value 
   then
    do:
      message "Expiration date cannot be before qualification effective date."
        view-as alert-box info buttons ok.
      return false.
    end.
  
  publish "getQualReviewPeriod" (input {&Qualification},
                                 input  cbQual:input-value,
                                 output iReviewPeriod,
                                 output std-lo,
                                 output std-ch).

  if cbStatus:input-value = {&GoodStanding}  and 
     iReviewPeriod        <> 0               and 
     nxtreviewDate:input-value = ? 
   then
    do:  
      message "Next Review date cannot be blank."
        view-as alert-box info buttons ok.
      assign nxtreviewDate:screen-value = string(today + iReviewPeriod).
      apply "value-changed" to nxtreviewDate. 
      return false.
    end. /* if nxtreviewDate:input-value       = "" or ...*/


  if tActive:checked = true    and 
      (feffDate:input-value = ?)
   then
    do:
      message "Please enter Effective Date as qualification is active."
          view-as alert-box info buttons ok.
      return false.
    end.

  if date(feffDate:input-value) gt today
   then
    do:
      message "Effective Date is in future. Continue?" 
          view-as alert-box warning buttons yes-no update std-lo.
      if not std-lo 
       then
        return false.
    end.

  if date(fexpDate:input-value) lt today and
     cbStatus:screen-value      eq {&GoodStanding}
   then
    do:
      message "The Expiration Date is in past and the status is Good Standing. Continue?" 
          view-as alert-box warning buttons yes-no update std-lo.
      if not std-lo 
       then
        return false.
    end.

  if not (date(fexpDate:input-value) = ?) and (nxtreviewDate:input-value > fexpDate:input-value) 
   then
    do:
      message "New Review Due Date cannot be after the qualification expiration date."
        view-as alert-box info buttons ok.
                
      if (date(fexpDate:input-value) - 14) < today 
       then
        nxtreviewDate:screen-value = string(today + 1). 
       else
        nxtreviewDate:screen-value = string(date(fexpDate:input-value) - 14). 
      apply "value-changed" to nxtreviewDate.
      return false.
    end. /* if not (date(fexpDate:input-value) = ?) and...*/      
  
  if not (date(feffDate:input-value) = ?) and (nxtreviewDate:input-value < feffDate:input-value) 
   then
    do:
      message "Next Review date cannot be less than effective date"
        view-as alert-box info buttons ok.
      return false.
    end. /* if not (date(feffDate:input-value) = ?) and... */


  return true.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

