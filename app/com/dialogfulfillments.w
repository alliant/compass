&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogfulfillments.w 

  Description: Fullfill first party requirement fulfillment for more than
               one entity. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  Modified:  04/02/2020      Archana Gupta  Modified code according to new organization structure
             04/06/2021      SA             Task#93007 Bug fix in first-party fulfillment utility report.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{tt/cmpfulfillments.i}
{tt/cmpfulfillments.i &tableAlias="tcmpfulfillments"}

/* Parameters Definitions ---                                           */
define input parameter ipcEntity         as character no-undo. 
define input parameter ipiStateReqQualID as integer   no-undo.
define input parameter ipcQualification  as character no-undo. 
define input parameter ipcExpire         as character no-undo. 
define input parameter ipiExpireMonth    as integer   no-undo. 
define input parameter ipiExpireDay      as integer   no-undo. 
define input parameter ipiExpireYear     as integer   no-undo. 
define input parameter ipiExpireDays     as integer   no-undo. 
define input parameter ipiReviewPeriod   as integer   no-undo. 
define input-output parameter table      for cmpfulfillments.
define output parameter oplSuccess       as logical   no-undo.

{tt/fulfillment.i}
{lib/std-def.i}
{lib/com-def.i}

/* Local Variable Definitions ---                                       */
define variable cCurrUser  as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbStatus feffDate fexpDate edNewComm ~
fValidfor fNextRevDt bExport bNext bCancel RECT-66 RECT-67 
&Scoped-Define DISPLAYED-OBJECTS cbStatus feffDate fexpDate edNewComm ~
flReview flReviewBy fValidfor fNextRevDt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc Dialog-Frame 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getreqfor Dialog-Frame 
FUNCTION getreqfor returns character ( cauth as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel"
     BGCOLOR 8 .

DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 15 BY 1.14 TOOLTIP "Generate Report"
     BGCOLOR 8 .

DEFINE BUTTON bNext AUTO-GO 
     LABEL "Next" 
     SIZE 15 BY 1.14 TOOLTIP "Next".

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE edNewComm AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 64.6 BY 2.14 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE flReview AS DATE FORMAT "99/99/99":U 
     LABEL "Review" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE flReviewBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Review By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fNextRevDt AS DATE FORMAT "99/99/99":U 
     LABEL "Next Review Date" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fValidfor AS INTEGER FORMAT "->,>>9":U INITIAL 0 
     LABEL "Valid For Day(s)" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-66
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 8.1.

DEFINE RECTANGLE RECT-67
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 4.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbStatus AT ROW 2 COL 12.2 COLON-ALIGNED WIDGET-ID 30
     feffDate AT ROW 3.43 COL 12.2 COLON-ALIGNED WIDGET-ID 32
     fexpDate AT ROW 4.86 COL 12.2 COLON-ALIGNED WIDGET-ID 34
     edNewComm AT ROW 6.95 COL 14.2 NO-LABEL WIDGET-ID 12
     flReview AT ROW 10.81 COL 18.4 COLON-ALIGNED WIDGET-ID 46
     flReviewBy AT ROW 10.81 COL 56 COLON-ALIGNED WIDGET-ID 48
     fValidfor AT ROW 12.71 COL 18.4 COLON-ALIGNED WIDGET-ID 16
     fNextRevDt AT ROW 12.67 COL 56 COLON-ALIGNED WIDGET-ID 50
     bExport AT ROW 14.57 COL 18.2
     bNext AT ROW 14.57 COL 34 WIDGET-ID 36
     bCancel AT ROW 14.57 COL 49.8
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 6.29 COL 14 WIDGET-ID 14
     "Qualification Details" VIEW-AS TEXT
          SIZE 19.2 BY .62 AT ROW 1.24 COL 4.8 WIDGET-ID 40
     "Review Details" VIEW-AS TEXT
          SIZE 14.8 BY .62 AT ROW 9.91 COL 4.8 WIDGET-ID 44
     RECT-66 AT ROW 1.52 COL 3 WIDGET-ID 38
     RECT-67 AT ROW 10.19 COL 3 WIDGET-ID 42
     SPACE(1.19) SKIP(1.85)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add Qualification Details"
         DEFAULT-BUTTON bExport CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       edNewComm:RETURN-INSERTED IN FRAME Dialog-Frame  = TRUE.

/* SETTINGS FOR FILL-IN flReview IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flReview:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flReviewBy IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flReviewBy:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Add Qualification Details */
DO:
  oplsuccess = false.
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  oplSuccess = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport Dialog-Frame
ON CHOOSE OF bExport IN FRAME Dialog-Frame /* Export */
DO:
  run populateData in this-procedure.
  run exportData   in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNext
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNext Dialog-Frame
ON CHOOSE OF bNext IN FRAME Dialog-Frame /* Next */
DO:
   define variable chreqQualID as character no-undo.
   define variable  chComstat  as character no-undo.

   run validationCheck (output std-ch).
   if std-ch <> "" 
    then
     do:
       message std-ch
           view-as alert-box info buttons ok.
       return no-apply.
     end.

   run populateData in this-procedure.
   
   message "Qualification will now be added to " + getreqfor(ipcEntity) +  ". Click Ok to confirm." 
     view-as alert-box info buttons ok-cancel title "Confirm" update std-lo.
  
   if not std-lo
    then 
     return no-apply.
  
   /* Create/Delete fulfillment Record. */
   run server/newfulfillment.p(input-output table fulfillment,          /* Not Required */ 
                               input-output table cmpfulfillments,  /* Create new qualreq and qualification record */
                               output chreqQualID,                  /* Not Required */
                               output chComStat,                    /* Return updated compliance status. */
                               output oplSuccess,
                               output std-ch).
  
   if not oplSuccess /* if not success return */
    then
     do:
       message std-ch
           view-as alert-box error buttons ok.
       return.
     end.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus Dialog-Frame
ON VALUE-CHANGED OF cbStatus IN FRAME Dialog-Frame /* Status */
DO:
  assign 
       bExport:sensitive in frame {&frame-name} = true
       fValidfor:screen-value                   = ""
       fNextRevDt:screen-value                  = ""
       .

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNextRevDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNextRevDt Dialog-Frame
ON VALUE-CHANGED OF fNextRevDt IN FRAME Dialog-Frame /* Next Review Date */
do:
  define variable myDate as date no-undo.
  
  assign myDate = date (fNextRevDt:screen-value) no-error.
  
  if not error-status:error then
    assign fValidfor:screen-value = string(date(fNextRevDt:screen-value) - today).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fValidfor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fValidfor Dialog-Frame
ON VALUE-CHANGED OF fValidfor IN FRAME Dialog-Frame /* Valid For Day(s) */
do:
  assign
       fNextRevDt:screen-value = string(today + fValidfor:input-value)
       .
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/win-main.i}
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.
  
  run getData.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbStatus feffDate fexpDate edNewComm flReview flReviewBy fValidfor 
          fNextRevDt 
      WITH FRAME Dialog-Frame.
  ENABLE cbStatus feffDate fexpDate edNewComm fValidfor fNextRevDt bExport 
         bNext bCancel RECT-66 RECT-67 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData Dialog-Frame 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable hTable         as  handle no-undo.
 define buffer tcmpfulfillments for tcmpfulfillments.
 define buffer cmpfulfillments  for cmpfulfillments.
 
 empty temp-table tcmpfulfillments.

 for each cmpfulfillments:
   create tcmpfulfillments.
   buffer-copy cmpfulfillments to tcmpfulfillments.

   tcmpfulfillments.stat  = getQualificationDesc(cmpfulfillments.stat).
 end.

 publish "GetReportDir" (output std-ch).
 hTable = temp-table tcmpfulfillments:handle.

 run util/exporttable.p (table-handle hTable,
                         "tcmpfulfillments",
                         "for each tcmpfulfillments",
                         "entityID,entityName,entity,qualification,stat,effectiveDate,expirationDate,notes",
                         "Entity ID,Entity Name,Role,Qualification,Last Status,Last Effective,Last Expiration,Notes",
                         std-ch,
                         "newcmpfulfillments-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output std-ch,
                         output std-in).
                         
                                                                             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable dtexpiration as date  no-undo.
  
  do with frame {&frame-name}:
  end.
  
  publish "GetSysPropListMinusID" ("COM", "Qualification", "Status",{&NeedsReview},output std-ch).
  /*cbStatus:list-item-pairs = std-ch.*/
  
 if std-ch ne ""
 then
  cbStatus:list-item-pairs = replace(std-ch,"^",",").
 else
 cbStatus:list-item-pairs = ","  .
 
  cbStatus:screen-value  = {&GoodStanding}. 
  
  /* Getting logged in current user */
  publish "GetCredentialsName" (output cCurrUser).
      
  assign 
      flReview  :screen-value = string(today)
      flReviewBy:screen-value = cCurrUser
      .
  
  if cbStatus:screen-value = {&GoodStanding} and 
     ipiReviewPeriod      <> 0               and 
     (fNextRevDt:screen-value = "" or date(fNextRevDt:screen-value) = ?)
   then
    do: 
      assign 
          fNextRevDt:screen-value = string(today + ipiReviewPeriod)
          .
      apply "value-changed" to fNextRevDt. 

    end. /* if fNextRevDt:screen-value = "" or ...*/

  if ipcExpire = {&ExpireDate} 
   then
    assign dtexpiration = date(ipiExpireMonth,ipiExpireDay,year(today) + ipiExpireYear) no-error.
   else 
    if ipcExpire = {&ExpireDays} 
     then
      assign 
          dtexpiration = ipiExpireDays + today no-error
          .
     else
      assign 
          dtexpiration = ?
        .

  assign 
      fexpDate:screen-value = string(dtexpiration)
      feffDate:screen-value = string(today)
      .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE populateData Dialog-Frame 
PROCEDURE populateData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: 
------------------------------------------------------------------------------*/
 for each cmpfulfillments:

   assign 
       cmpfulfillments.stateReqQualID     = ipiStateReqQualID
       cmpfulfillments.qualification      = ipcQualification
       cmpfulfillments.stat               = cbStatus:input-value in frame {&frame-name}
       cmpfulfillments.effectiveDate      = datetime(date(feffDate:input-value),mtime)
       cmpfulfillments.expirationDate     = datetime(date(fexpDate:input-value),mtime)
       cmpfulfillments.notes              = edNewComm:input-value
       cmpfulfillments.reviewDate         = datetime(today, mtime)
       cmpfulfillments.reviewBy           = cCurrUser
       cmpfulfillments.numValidDays       = fValidfor:input-value
       cmpfulfillments.nextreviewDueDate  = datetime(date(fNextRevDt:input-value),mtime)
       .

 end. /* for each cmpfulfillments */ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validationCheck Dialog-Frame 
PROCEDURE validationCheck :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter cMsg as character no-undo.

  do with frame {&frame-name}:
  end.
  
  if feffDate:screen-value       = "" or 
     date(feffDate:screen-value) = ? 
   then
    do:
      cMsg = "Effective date cannot be blank." .
      assign 
          feffDate:screen-value = string(today)
          .
      return cMsg.
    end.
   
  if date(fexpDate:screen-value) < date(feffDate:screen-value) 
   then
    do:
      cMsg = "Expiration date cannot be before qualification effective date.".
      assign 
          fexpDate:screen-value = ""
          .
      return cMsg.
    end.
  
  if cbStatus:screen-value = {&GoodStanding} and 
     ipiReviewPeriod      <> 0               and 
     (fNextRevDt:screen-value = "" or date(fNextRevDt:screen-value) = ?)
   then
    do: 
      cMsg = "Next Review date cannot be blank." .
      assign 
          fNextRevDt:screen-value = string(today + ipiReviewPeriod)
          .
      apply "value-changed" to fNextRevDt. 

      return cMsg.

    end. /* if fNextRevDt:screen-value       = "" or ...*/
 
  if not (date(fexpDate:screen-value) = ?) and (date(fNextRevDt:screen-value) > date(fexpDate:screen-value))
   then
    do:
      cMsg = "New Review Due Date cannot be after the qualification expiration date.".
     
      if (date(fexpDate:screen-value) - 14) < today 
       then
        fNextRevDt:screen-value = string(today + 1). 
       else
        fNextRevDt:screen-value = string(date(fexpDate:input-value) - 14). 

      apply "value-changed" to fNextRevDt.

      return cMsg.

    end. /* if not (date(fexpDate:screen-value) = ?) and...*/      
  
  if not (date(feffDate:screen-value) = ?) and (date(fNextRevDt:screen-value) < date(feffDate:screen-value)) 
   then
    do:
      cMsg = "Next Review date cannot be less than effective date".
      return cMsg.
    end. /* if not (date(feffDate:screen-value) = ?) and... */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc Dialog-Frame 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  std-ch = "".
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  RETURN std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getreqfor Dialog-Frame 
FUNCTION getreqfor returns character ( cauth as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cauth = {&OrganizationCode}
   then
    return {&Organization}.   /* Function return value. */
  else if cauth = {&personcode}
   then
    return {&Person}.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

