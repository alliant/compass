&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wpendingreviews-person.w 

  Description:Pending Reviews Report for qualifications of
              person. 
 
  Author:Sachin Chaturvedi

  Created:07.12.2018

  Modified:
  11/01/2018    Rahul Sharma    Modified to create sharefolder once user
                                select documents button.
  03.29.2022    Shefali         Task# 86699  Modified in exportData IP.
  05.10.2022    Shefali         Task# 93684 Modified to show username instead of userID in grid. 
------------------------------------------------------------------------*/
CREATE WIDGET-POOL.


/*-----------------Temptable definition----*/
{tt/qualification.i}
{tt/qualification.i &tableAlias="ttResult"}
{tt/review.i} 
{tt/review.i &tableAlias="tReview"}

/* Standard Library */
{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}


/*----Local variables----*/
define variable dColumnWidth as decimal no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwQualification

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttresult

/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification ttresult.entityID ttresult.Name ttresult.qualification ttresult.qualificationNumber ttresult.EffectiveDate ttresult.ExpirationDate ttresult.reviewDate ttresult.reviewBy ttresult.nextReviewDueDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification   
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification for each ttresult by ttresult.name
&Scoped-define OPEN-QUERY-brwQualification open query {&SELF-NAME} for each ttresult by ttresult.name.
&Scoped-define TABLES-IN-QUERY-brwQualification ttresult
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification ttresult


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bget cbState tgLinked brwQualification ~
tgActive RECT-54 RECT-55 
&Scoped-Define DISPLAYED-OBJECTS cbState tgLinked flEntity tgExpired fiExp ~
tgActive 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDoc  NO-FOCUS
     LABEL "Doc" 
     SIZE 7.2 BY 1.71 TOOLTIP "Documents".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON bget  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE BUTTON bVerify  NO-FOCUS
     LABEL "Verify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Verify qualification".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE fiExp AS INTEGER FORMAT "zz9":U INITIAL 0 
     LABEL "in" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 6.2 BY 1 NO-UNDO.

DEFINE VARIABLE flEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualifications of" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77.2 BY 3.52.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.4 BY 3.52.

DEFINE RECTANGLE RECT-55
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29 BY 3.52.

DEFINE VARIABLE tgActive AS LOGICAL INITIAL no 
     LABEL "Get Qualifications only for Active Roles" 
     VIEW-AS TOGGLE-BOX
     SIZE 42 BY .81 TOOLTIP "Display qualifications only for active roles" NO-UNDO.

DEFINE VARIABLE tgExpired AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 5.8 BY .81 NO-UNDO.

DEFINE VARIABLE tgLinked AS LOGICAL INITIAL no 
     LABEL "Include Unused Qualifications" 
     VIEW-AS TOGGLE-BOX
     SIZE 32 BY .81 TOOLTIP "Display unused qualifications" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQualification FOR 
      ttresult SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification C-Win _FREEFORM
  QUERY brwQualification DISPLAY
      ttresult.entityID    label "Person ID"                                             width 15 
ttresult.Name              label "Name"                             format "x(55)"      width 30 
ttresult.qualification     label "Qualification"                    format "x(50)"      width 24 
ttresult.qualificationNumber  label "Number"                        format "x(50)"      width 24 
ttresult.EffectiveDate     column-label "Qualification!Effective"   format "99/99/9999" width 16 
ttresult.ExpirationDate    column-label "Qualification!Expiration"  format "99/99/9999" width 17
ttresult.reviewDate        label "Last Review"                      format "99/99/9999" width 18
ttresult.reviewBy          label "Last Review By"                   format "x(30)"      width 18
ttresult.nextReviewDueDate label "Next Review Due"                  format "99/99/9999" width 20
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 188.8 BY 18.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bget AT ROW 2.29 COL 70.8 WIDGET-ID 110 NO-TAB-STOP 
     bDoc AT ROW 2.24 COL 115.6 WIDGET-ID 96 NO-TAB-STOP 
     cbState AT ROW 2.05 COL 8.4 COLON-ALIGNED WIDGET-ID 98
     tgLinked AT ROW 3.14 COL 10.4 WIDGET-ID 254
     bExport AT ROW 2.24 COL 122.6 WIDGET-ID 2 NO-TAB-STOP 
     bVerify AT ROW 2.24 COL 108.6 WIDGET-ID 94 NO-TAB-STOP 
     flEntity AT ROW 2.05 COL 48 COLON-ALIGNED WIDGET-ID 258 NO-TAB-STOP 
     tgExpired AT ROW 2.71 COL 88.4 WIDGET-ID 252
     fiExp AT ROW 2.62 COL 92.4 COLON-ALIGNED WIDGET-ID 238
     brwQualification AT ROW 5.33 COL 2 WIDGET-ID 200
     tgActive AT ROW 3.86 COL 10.4 WIDGET-ID 264
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.1 COL 3 WIDGET-ID 108
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1.1 COL 79.8 WIDGET-ID 112
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.1 COL 108.6 WIDGET-ID 256
     "Expires:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 2.81 COL 80.2 WIDGET-ID 262
     "Days" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 2.81 COL 101.2 WIDGET-ID 260
     RECT-2 AT ROW 1.38 COL 2 WIDGET-ID 8
     RECT-54 AT ROW 1.38 COL 107.6 WIDGET-ID 92
     RECT-55 AT ROW 1.38 COL 78.8 WIDGET-ID 106
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 191 BY 23.29
         DEFAULT-BUTTON bget WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Qualifications Pending Review Utility"
         HEIGHT             = 23.24
         WIDTH              = 191
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwQualification fiExp fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bDoc IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwQualification:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwQualification:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwQualification:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bVerify IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiExp IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flEntity IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flEntity:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tgExpired IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query {&SELF-NAME} for each ttresult by ttresult.name.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Qualifications Pending Review Utility */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Qualifications Pending Review Utility */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Qualifications Pending Review Utility */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDoc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDoc C-Win
ON CHOOSE OF bDoc IN FRAME fMain /* Doc */
do:
  run openDocument in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bget
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bget C-Win
ON CHOOSE OF bget IN FRAME fMain /* Get */
do:
  resetScreen().
  run getData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON DEFAULT-ACTION OF brwQualification IN FRAME fMain
do:
  run actionOpen in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON ROW-DISPLAY OF brwQualification IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification C-Win
ON START-SEARCH OF brwQualification IN FRAME fMain
do:
  {lib\brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bVerify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bVerify C-Win
ON CHOOSE OF bVerify IN FRAME fMain /* Verify */
do:
  run openDialogReview in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  resetScreen().
  resultsChanged(false).
                              
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiExp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiExp C-Win
ON VALUE-CHANGED OF fiExp IN FRAME fMain /* in */
or "RETURN" of fiExp
do:
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgActive C-Win
ON VALUE-CHANGED OF tgActive IN FRAME fMain /* Get Qualifications only for Active Roles */
do:
  resetScreen().
  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgExpired
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgExpired C-Win
ON VALUE-CHANGED OF tgExpired IN FRAME fMain
do:
  assign 
      fiExp:sensitive    = (self:checked = true)
      fiExp:screen-value = ""
      .
  run filterData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgLinked
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgLinked C-Win
ON VALUE-CHANGED OF tgLinked IN FRAME fMain /* Include Unused Qualifications */
do:
  resetScreen().
  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign current-window                = {&window-name} 
       this-procedure:current-window = {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to terminate it. */
on close of this-procedure 
  run disable_UI.

{lib/brw-main.i}
{lib/win-status.i}

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


subscribe to "closeWindow" anywhere.

subscribe to "qualificationReviewModified" anywhere run-procedure "qualificationModified".

/* Subscribe to the update events. */
subscribe to "qualificationModified"       anywhere.
subscribe to "fulfillmentModified"         anywhere.

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bget:load-image ("images/Completed.bmp").
bget:load-image-insensitive("images/Completed-i.bmp").
bVerify:load-image ("images/star.bmp").
bVerify:load-image-insensitive("images/star-i.bmp").
bDoc:load-image ("images/attach.bmp").
bDoc:load-image-insensitive("images/attach-i.bmp").

publish "GetSearchStates" (output std-ch).
if std-ch > "" 
 then
  std-ch = "," + std-ch.
assign 
      cbState:list-item-pairs in frame {&frame-name} = {&ALL} + "," + {&ALL} +  std-ch
      cbState:screen-value = {&ALL}
      .

/* Function to reset default values and clear filters */
resetScreen().

/* Changes browse columns according to the entity */
run resetBrowse in this-procedure.

/* Sets title of the window according to the standard */
{lib/win-main.i}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  run enable_UI. 
   
  /* Entity Type to the fill-in based on the input parameter */
  flEntity:screen-value = {&Person}.

  {lib/get-column-width.i &col="'name'"          &var=dColumnWidth}
  {lib/get-column-width.i &col="'reviewBy'"      &var=dColumnWidth}
  {lib/get-column-width.i &col="'qualification'" &var=dColumnWidth}
    
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  run windowResized in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionOpen C-Win 
PROCEDURE actionOpen PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if not available ttresult
   then 
    return.

  publish "OpenWindow" (input {&person}, 
                        input ttresult.entityid, 
                        input "wpersondetail.w", 
                        input "character|input|" + ttresult.entityid + "^integer|input|2" + "^character|input|",                                   
                        input this-procedure).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CloseWindow C-Win 
PROCEDURE CloseWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Close parent and child windows and delete their instances */
  publish "WindowClosed" (input this-procedure).

  apply "CLOSE":U to this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState tgLinked flEntity tgExpired fiExp tgActive 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bget cbState tgLinked brwQualification tgActive RECT-54 RECT-55 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cFileName  as character   no-undo.
  define variable cTableList as character   no-undo.
  define variable cFieldList as character   no-undo.

  if query brwqualification:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
  
  /* Export list is same for Organization and Person. */
  assign 
        cTableList = "entityID,name,qualification,qualificationNumber,EffectiveDate,ExpirationDate,reviewDate,reviewBy,nextReviewDueDate"
        cFieldList = "Person ID,Name,Qualification,Number,Qualification Effective,Qualification Expiration,Last Review,Last Review By,Next Review Due"
        .
    
  publish "GetReportDir" (output std-ch).
  std-ha = temp-table ttresult:handle.
  run util/exporttable.p (table-handle std-ha,
                          "ttresult",
                          "for each ttresult",
                          cTableList,
                          cFieldList,
                          std-ch,
                          "PersonPendingReviews" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 

  close query brwQualification.
  empty temp-table ttresult.

  do with frame {&frame-name}:
  end.
  
  for each qualification:

    if tgExpired:checked and
       date(qualification.nextReviewDueDate) ge (today + fiExp:input-value) 
     then
      next.      

    create ttresult.
    buffer-copy qualification to ttresult.

  end. /* for each qualification...*/

  open query brwqualification preselect each ttresult by ttresult.name.

  /* Display no. of records on status bar */
  setStatusCount(query brwqualification:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE fulfillmentModified C-Win 
PROCEDURE fulfillmentModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcEntityID  as character no-undo.

  define variable iCount              as integer no-undo.
  define variable iqualificationID    as integer no-undo.

  for each ttresult where ttresult.entityID = ipcEntityID : 
    std-lo = true.
    leave.
  end.
  if not std-lo 
   then
    return.

  do with frame {&frame-name}:
  end.
  
  find current ttresult no-error.
  if available ttresult 
   then
    iqualificationID = ttresult.qualificationID.

  do iCount = 1 to brwQualification:num-iterations:
    if brwQualification:is-row-selected(iCount)
     then
      leave.
  end.

  resetScreen().
  run getData in this-procedure.
  
  for first ttresult 
    where ttresult.qualificationID = iqualificationID:
    std-ro = rowid(ttresult).
  end.
  
  brwQualification:set-repositioned-row(iCount,"ALWAYS") no-error.
  reposition brwQualification to rowid std-ro no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer qualification for qualification.

 do with frame {&frame-name}:
 end.

 /* Calling client side script to call server file */
 run server/querypendingreviews.p (input "ALL",                /* EntityID */
                                   input cbState:input-value,
                                   input {&PersonCode},            /* P */
                                   input tgLinked:checked,
                                   input tgActive:checked,
                                   output table qualification,
                                   output std-lo,
                                   output std-ch).
 
 if not std-lo 
  then
   do:
     message std-ch
       view-as alert-box error buttons ok.
     return.
   end. 
 
 /* Based on the filter screen-value data will be shown in the browser */
 run filterData in this-procedure.

/* Makes widget enable-disable based on the data */
 if query brwqualification:num-results > 0 
  then
   assign 
         bVerify:sensitive    = true
         bdoc:sensitive       = true
         bExport:sensitive    = true
         tgExpired:sensitive  = true
         .
 else
   assign
         bVerify:sensitive   = false
         bdoc:sensitive      = false
         bExport:sensitive   = false
         tgExpired:sensitive = false
         .

 /* Display no. of records with date and time on status bar */
 setStatusRecords(query brwqualification:num-results).
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialogReview C-Win 
PROCEDURE openDialogReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable hProcedure      as handle     no-undo.
  define variable cClientProcs    as character  no-undo.
  define variable iInstanceID     as integer    no-undo.
  define variable success         as logical    no-undo.

   if not available ttresult 
    then 
     return.

   std-ro = rowid(ttresult).

   hProcedure = session:first-procedure.
   do while valid-handle(hProcedure):
     cClientProcs = cClientProcs + hProcedure:file-name + ",".
     hProcedure = hProcedure:next-sibling.
     run myinstance in hProcedure (output iInstanceID) no-error.
   
     if ttresult.QualificationID eq iInstanceID 
      then
       do:
         success = true.
         leave.
       end.
     success = false.
   end.

 if not success   
  then
   run qualificationdatasrv.p persistent set hProcedure (input ttresult.QualificationID).
  
  run getQualificationReviews in hProcedure (input 0,
                                             output table review,
                                             output std-lo,
                                             output std-ch).
  
  find last review 
    where review.qualificationID = ttresult.QualificationID 
    no-error.
  if not available review 
   then 
    return.
  
  empty temp-table treview.

  create treview.
  buffer-copy review to treview.

  run dialogqualificationreview.w (input hProcedure,     /* qualificationdatasrv handle */
                                   input ttresult.stateId,
                                   input ttresult.QualificationID,
                                   input ttresult.stat,
                                   input ttresult.EffectiveDate,
                                   input ttresult.ExpirationDate,
                                   input table treview,
                                   output std-in,  /* ReviewId */
                                   output std-lo). 
  
  if not std-lo 
   then 
    return.
  
  run getQualificationReviews in hProcedure (input treview.reviewID,
                                             output table review,
                                             output std-lo,
                                             output std-ch).
  
  find last review 
    where review.qualificationID = ttresult.QualificationID 
    no-error.
  if not available review 
   then 
    return.

  assign 
        ttresult.effectiveDate     = review.effdate
        ttresult.expirationDate    = review.expdate
        ttresult.expirationDate    = review.expdate
        ttresult.reviewDate        = review.reviewDate
        ttresult.reviewBy          = review.userName
        ttresult.nextreviewDueDate = review.nextreviewDueDate
        .

  publish "qualificationReviewModifiedByUtility" (input ttresult.QualificationID).

  open query brwqualification preselect each ttresult by ttresult.name.
  
  reposition brwqualification to rowid std-ro.

  if valid-handle(hProcedure) and not success
   then
    delete procedure hProcedure.
  hProcedure =?.
  iInstanceID = 0.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDocument C-Win 
PROCEDURE openDocument :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available ttResult 
  then return.
 
  publish "OpenDocument" (input "Compliance",
                          input ttResult.qualificationId,
                          input this-procedure).


end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE qualificationModified C-Win 
PROCEDURE qualificationModified :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcLocalQualID as integer   no-undo.

  define variable iCount                as integer   no-undo.
  define variable iqualificationID      as integer   no-undo.

  for each ttresult where ttresult.qualificationID = ipcLocalQualID : 
    std-lo = true.
    leave.
  end.
  if not std-lo 
   then
    return.
  
  do with frame {&frame-name}:
  end.
  
  find current ttresult no-error.
  if available ttresult 
   then
    iqualificationID = ttresult.qualificationID.

  do iCount = 1 to brwQualification:num-iterations:
    if brwQualification:is-row-selected(iCount)
     then
      leave.
  end.

  resetScreen().
  run getData in this-procedure.
  
  for first ttresult 
    where ttresult.qualificationID = iqualificationID:
    std-ro = rowid(ttresult).
  end.
  
  brwQualification:set-repositioned-row(iCount,"ALWAYS") no-error.
  reposition brwQualification to rowid std-ro no-error.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE resetBrowse C-Win 
PROCEDURE resetBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Resets Browse columns and set the window title according to the entity       
------------------------------------------------------------------------------*/
  define variable hEntityId      as handle  no-undo.
  define variable hEntityName    as handle  no-undo.
  define variable hQualification as handle  no-undo.

  do with frame {&frame-name}:
  end. 
  
  assign 
        hEntityId      = brwQualification:get-browse-column(1)
        hEntityName    = brwQualification:get-browse-column(2)
        hQualification = brwQualification:get-browse-column(3)
        .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i}  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} Components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 7.
  

 {lib/resize-column.i &col="'name,qualification'" &var=dColumnWidth}

 run ShowScrollBars(browse brwQualification:handle, no, yes).

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
    
  /* Initialise filters to default values */
  assign 
        bVerify:sensitive   = false
        bdoc:sensitive      = false
        bExport:sensitive   = false
        fiExp:screen-value  = "0"
        fiExp:sensitive     = false
        .

  setStatusMessage("").
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage("Results may not match current parameters.").
 return true.
 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

