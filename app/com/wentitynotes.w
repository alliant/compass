&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wentitynotes.w

  Description:Window to create notes for Entity
              (Agent,Attorney,PersonRole,Company) 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Rahul Sharma

  Created:02.08.2018 
  
  Modified:
  Date          Name           Description
  08/09/2019    Gurvindar      Removed progress error while populating combo-box   
  04/02/2020    Archana Gupta  Modified code according to new organization structure
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

{lib/winlaunch.i}
{lib/std-def.i}
{lib/com-def.i} 

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* Parameters Definitions ---                                           */
define input parameter pcStateID    as character no-undo.
define input parameter pcEntity     as character no-undo.
define input parameter pcEntityId   as character no-undo.
define input parameter pcName       as character no-undo.
define input parameter pcRoleType   as character no-undo.
define input parameter pcReqID      as character no-undo.


/* Temp-table Definitions ---                                           */
{tt/comnote.i}
{tt/comnote.i &tablealias=filtercomnote}
{tt/comnote.i &tablealias=tcomnote}

&scoped-define addNote        "Add" 
&scoped-define modifyNote     "Modify" 
&scoped-define default        "" 
&scoped-define selReq         "--Select Requirement--,0"  

/* Local Variable Definitions ---                                       */
define variable cCurrUser      as character no-undo.
define variable cCurrentUID    as character no-undo.
define variable cAction        as character no-undo.
define variable cTrackSubject  as character no-undo.
define variable cTrackNoteText as character no-undo.
define variable cRequimentList as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES filtercomnote

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData filtercomnote.notedate filtercomnote.username filtercomnote.subject   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData preselect each filtercomnote
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} preselect each filtercomnote.
&Scoped-define TABLES-IN-QUERY-brwData filtercomnote
&Scoped-define FIRST-TABLE-IN-QUERY-brwData filtercomnote


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bFilter bClear bAddNote trequirement fSearch ~
brwData bRefresh tSubject tNoteText RECT-36 RECT-37 RECT-65 
&Scoped-Define DISPLAYED-OBJECTS flStateID trequirement fSearch tEntityId ~
tname tEntity tDateUser 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddNote  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add Note".

DEFINE BUTTON bCancel  NO-FOCUS
     LABEL "Cancel" 
     SIZE 7.2 BY 1.71 TOOLTIP "Cancel".

DEFINE BUTTON bClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 4.8 BY 1.14 TOOLTIP "Clear".

DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete Note".

DEFINE BUTTON bFilter  NO-FOCUS
     LABEL "Filter" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search Notes".

DEFINE BUTTON bModifyNote  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify Note".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 4.8 BY 1.14 TOOLTIP "Refresh notes".

DEFINE BUTTON bSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save".

DEFINE BUTTON bSpellCheck  NO-FOCUS
     LABEL "Spell Check" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check spelling".

DEFINE VARIABLE treq AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Requirement--","0"
     DROP-DOWN-LIST
     SIZE 45 BY 1 TOOLTIP "Select Requirement" NO-UNDO.

DEFINE VARIABLE trequirement AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN-LIST
     SIZE 52 BY 1 TOOLTIP "Select the type of notes to view" NO-UNDO.

DEFINE VARIABLE tNoteText AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 107 BY 13.24
     BGCOLOR 15 FONT 5 NO-UNDO.

DEFINE VARIABLE flStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 6 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.8 BY 1 TOOLTIP "Search Criteria (Subject,Note)" NO-UNDO.

DEFINE VARIABLE tDateUser AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 39.8 BY 1 NO-UNDO.

DEFINE VARIABLE tEntity AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15.8 BY 1 NO-UNDO.

DEFINE VARIABLE tEntityId AS CHARACTER FORMAT "X(256)":U 
     LABEL "Entity ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15.8 BY 1 NO-UNDO.

DEFINE VARIABLE tname AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 52.4 BY 1 TOOLTIP "Name of the entity" NO-UNDO.

DEFINE VARIABLE tSubject AS CHARACTER FORMAT "X(256)":U 
     LABEL "Subject" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 85.6 BY 1 TOOLTIP "Subject" NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73 BY 3.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.4 BY 3.

DEFINE RECTANGLE RECT-65
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16.2 BY 3.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      filtercomnote SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      filtercomnote.notedate label "Date" format "99/99/9999" width 14
filtercomnote.username label "User"    format "x(50)"  width 15
filtercomnote.subject  label "Subject" format "x(200)" width 50
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 73 BY 15.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bFilter AT ROW 4.48 COL 65 WIDGET-ID 308 NO-TAB-STOP 
     bClear AT ROW 4.48 COL 70.2 WIDGET-ID 68 NO-TAB-STOP 
     bCancel AT ROW 3.57 COL 106.6 WIDGET-ID 318 NO-TAB-STOP 
     bdelete AT ROW 3.57 COL 90.8 WIDGET-ID 350 NO-TAB-STOP 
     bAddNote AT ROW 3.57 COL 76.4 WIDGET-ID 224 NO-TAB-STOP 
     flStateID AT ROW 1.48 COL 10 COLON-ALIGNED WIDGET-ID 342 NO-TAB-STOP 
     bModifyNote AT ROW 3.57 COL 83.6 WIDGET-ID 306 NO-TAB-STOP 
     trequirement AT ROW 3.29 COL 15.4 COLON-ALIGNED WIDGET-ID 334
     bSave AT ROW 3.57 COL 99.4 WIDGET-ID 316 NO-TAB-STOP 
     fSearch AT ROW 4.57 COL 15.4 COLON-ALIGNED WIDGET-ID 310
     brwData AT ROW 6.48 COL 3 WIDGET-ID 200
     bRefresh AT ROW 3.24 COL 70.2 WIDGET-ID 348 NO-TAB-STOP 
     bSpellCheck AT ROW 6.48 COL 176.2 WIDGET-ID 326 NO-TAB-STOP 
     tSubject AT ROW 7.48 COL 88 COLON-ALIGNED WIDGET-ID 320 NO-TAB-STOP 
     tNoteText AT ROW 9.05 COL 76 NO-LABEL WIDGET-ID 312 NO-TAB-STOP 
     tEntityId AT ROW 1.48 COL 54.6 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     tname AT ROW 1.48 COL 81 COLON-ALIGNED WIDGET-ID 336 NO-TAB-STOP 
     tEntity AT ROW 1.48 COL 25.8 COLON-ALIGNED WIDGET-ID 338 NO-TAB-STOP 
     tDateUser AT ROW 6.33 COL 133.8 COLON-ALIGNED NO-LABEL WIDGET-ID 340 NO-TAB-STOP 
     treq AT ROW 6.33 COL 88 COLON-ALIGNED WIDGET-ID 344 NO-TAB-STOP 
     RECT-36 AT ROW 2.95 COL 3 WIDGET-ID 298
     RECT-37 AT ROW 2.95 COL 75.6 WIDGET-ID 314
     RECT-65 AT ROW 2.95 COL 98.6 WIDGET-ID 356
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 183.2 BY 21.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Notes"
         HEIGHT             = 21.43
         WIDTH              = 183
         MAX-HEIGHT         = 47.86
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.86
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fSearch fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bCancel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bdelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModifyNote IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSpellCheck IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flStateID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       flStateID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tDateUser IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tDateUser:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEntity IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tEntity:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEntityId IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tEntityId:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tname IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tname:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR EDITOR tNoteText IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNoteText:RETURN-INSERTED IN FRAME fMain  = TRUE
       tNoteText:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX treq IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tSubject IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tSubject:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} preselect each filtercomnote.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Notes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Notes */
do:
  /* This event will close the window */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Notes */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddNote C-Win
ON CHOOSE OF bAddNote IN FRAME fMain /* Add */
do:
  cAction = {&addNote}.

  /* display data on UI */
  run displayData    in this-procedure no-error.

  /* set widget state on UI */
  run setWidgetState in this-procedure({&addNote}) no-error.   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
do:
  cAction = {&default}.

  /* display data on UI */
  run displayData    in this-procedure no-error.     

  /* set widget state on UI */
  run setWidgetState in this-procedure({&default}) no-error.   

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
do: 
  fSearch:screen-value = "".
  /* clear all the data in browse */
  run setStatusMsg in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
do :
  /* delete the selected notes in browse and then filter the data*/
  run deleteNote     in this-procedure no-error.
   
  /* display data on UI */
  run displayData    in this-procedure  no-error.
  
  /* set widget state on UI */
  run setWidgetState in this-procedure ({&default}) no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
do:
  /* filter data and fill the browse with the filtered data and set status in taskbar */
  run filterData     in this-procedure.
  
  /* display data on UI */
  run displayData    in this-procedure no-error.
  
  /* set widget state on UI */
  run setWidgetState in this-procedure ({&default}) no-error.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyNote
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyNote C-Win
ON CHOOSE OF bModifyNote IN FRAME fMain /* Modify */
do:  
  cAction = {&modifyNote}.   
  /* display data on UI */
  run displayData    in this-procedure no-error.
  
  /* set widget state on UI */
  run setWidgetState in this-procedure({&modifyNote}) no-error.     
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
do:
  /* We are not caching notes data in data model, so instead of implementing the
     2 step process (refreshData and then getData), we directly call getData which 
     in turn refresh the data from server. */
  run getData in this-procedure no-error.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
do:
  apply "choose" to bModifyNote.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
do:
  /* display data on UI */
  run displayData    in this-procedure no-error.

  /* set widget state on UI */
  run setWidgetState in this-procedure ({&default}) no-error.  
  
  /* Store the current values of the slected rows of the browse. */
  cTrackSubject  = tSubject:input-value.
  cTrackNoteText = tNoteText:input-value.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fMain /* Save */
do:
  /* save note after adding a new note or modifying a note */
  run saveNote       in this-procedure (output std-lo) no-error.   
  if not std-lo
   then 
    return no-apply.
     
  cAction = {&default}.
  /* dsplay data on UI */
  run displayData    in this-procedure  no-error.
  
  /* set widget state on UI */
  run setWidgetState in this-procedure ({&default}) no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSpellCheck C-Win
ON CHOOSE OF bSpellCheck IN FRAME fMain /* Spell Check */
do:
  run checkSpell in this-procedure no-error.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON VALUE-CHANGED OF fSearch IN FRAME fMain /* Search */
do:
  run setStatusMsg in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNoteText
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNoteText C-Win
ON VALUE-CHANGED OF tNoteText IN FRAME fMain
do:
  bSave:sensitive =  (cAction = {&addNote}) or     /* in case of adding a new note */
   
                     /* in case of modifying an existing note */
                     (cTrackSubject <> tSubject:input-value or cTrackNoteText <> tNoteText:input-value).   
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME trequirement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL trequirement C-Win
ON VALUE-CHANGED OF trequirement IN FRAME fMain /* Requirement */
do:
  /* filter data and fill the browse with the filtered data and set status in taskbar */
  run filterData     in this-procedure no-error.      

  /* display data on UI */
  run displayData    in this-procedure  no-error.

  /*setwidget state on UI */
  run setWidgetState in this-procedure ({&default}) no-error. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSubject
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSubject C-Win
ON VALUE-CHANGED OF tSubject IN FRAME fMain /* Subject */
do:
  bSave:sensitive =  (cAction = {&addNote}) or    /* in case of adding a new note */
   
                     /* in case of modifying an existing note */  
                     (cTrackSubject <> tSubject:input-value or cTrackNoteText <> tNoteText:input-value).

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main.i}
{lib/win-status.i} 

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  =  {&window-name}:width-pixels.
{&window-name}:max-height-pixels =  session:height-pixels.
{&window-name}:max-width-pixels  =   session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

publish "getRequirementListItems" (input pcStateID,
                                   input pcEntity,
                                   input pcRoleType,
                                   input "0" ,  /*pcReqID*/
                                   input "" , /*This one is for authorized by, blank means 'authorized by' check will not be considered */
                                   input ",", /* delimiter */
                                   output cRequimentList,
                                   output std-lo,
                                   output std-ch).
if not std-lo 
 then
  do:
    message std-ch
        view-as alert-box error buttons ok.
    return.
  end.
 assign
     trequirement:list-item-pairs = "ALL,ALL" 
     treq:list-item-pairs = {&selReq}.


 if cRequimentList ne "" 
  then
   assign
       trequirement:list-item-pairs = trim( trequirement:list-item-pairs  + "," + cRequimentList,",")
       treq:list-item-pairs         = trim(treq:list-item-pairs + "," + cRequimentList,",")
       . 

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
on close of this-procedure 
   run disable_UI.
     
/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

bRefresh:load-image("images/s-refresh.bmp").
bRefresh:load-image-insensitive("images/s-refresh-i.bmp").
bClear:load-image("images/s-erase.bmp").
bClear:load-image-insensitive("images/s-erase-i.bmp").
bfilter:load-image("images/s-magnifier.bmp").
bfilter:load-image-insensitive("images/s-magnifier-i.bmp").
bAddNote:load-image("images/blank-add.bmp").
bAddNote:load-image-insensitive("images/blank-i.bmp").
bModifyNote:load-image("images/update.bmp").
bModifyNote:load-image-insensitive("images/update-i.bmp").
bdelete:load-image("images/delete.bmp").
bdelete:load-image-insensitive("images/delete-i.bmp").
bSave:load-image("images/save.bmp").
bSave:load-image-insensitive("images/save-i.bmp").
bCancel:load-image("images/cancel.bmp").
bCancel:load-image-insensitive("images/cancel-i.bmp").
bSpellCheck:load-image("images/spellcheck.bmp").
bSpellCheck:load-image-insensitive("images/spellcheck-i.bmp").

publish "GetCredentialsName" (output cCurrUser).
publish "GetCredentialsID" (output cCurrentUID).

/* get notes form data model, filter data, display data
   and set Widget state on ui and set status on taskbar */
run getData in this-procedure no-error. 

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:  
  run enable_UI.  

  /* Setting the static items on the screen. */
  assign 
      flStateID:screen-value    = pcStateID
      tEntity  :screen-value    = pcRoleType
      tEntityId:screen-value    = pcEntityId
      tname    :screen-value    = pcName   
      .

  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkSpell C-Win 
PROCEDURE checkSpell :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable noteText as character no-undo.
 
  noteText = tNoteText:input-value in frame {&frame-name}.

  run util/spellcheck.p ({&window-name}:handle, input-output noteText, output std-lo).
  
  tNoteText:screen-value in frame {&frame-name} = noteText. 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Save is enabled, that means, there is something which is not yet saved. */
  if bSave:sensitive     
   then
    do:
      message "Changes to the note will be lost. Continue?"
         view-as alert-box buttons yes-no set std-lo.
      
      if not std-lo 
       then 
        return no-apply.
    end.
  
   /* This event will close the window and terminate the procedure.  */
   apply "CLOSE":U to this-procedure.
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteNote C-Win 
PROCEDURE deleteNote :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pentity   as character no-undo.
  define variable pentityID as character no-undo.
  define variable preqid    as integer   no-undo.
  define variable iseq      as integer   no-undo.  

  if not available filtercomnote 
   then
    return.   

  message "Highlighted Compliance Note will be deleted." skip "Do you want to continue?"
    view-as alert-box question buttons yes-no
    title "Delete Compliance Note"
    update std-lo as logical.
     
  if not std-lo 
   then
    return.

  assign
      pentity   = filtercomnote.entity
      pentityID = filtercomnote.entityID
      preqid    = filtercomnote.reqid
      iseq      = filtercomnote.seq
      .
  
  publish "deleteComplianceNote" (input pentity,
                                  input pentityID,
                                  input preqid,
                                  input iseq,
                                  output std-lo,
                                  output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
       
  find first comnote where comnote.entity     = pentity 
                         and comnote.entityId = pentityID 
                         and comnote.reqid    = preqid 
                         and comnote.seq      = iseq 
                         no-error.
  if available comnote 
   then
    delete comnote.

  /* filter data and fill the browse with the filtered data and set status in taskbar */
  run filterdata in this-procedure.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData C-Win 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose: 
  pcInputValues:  
  Notes:       
------------------------------------------------------------------------------*/
  do with frame fMain:
  end.   
  
  /* if this is Add Action then empty the widgets. */
  if cAction = {&addNote} or not available filtercomnote
   then
    assign
        tNoteText:screen-value = ""
        tSubject :screen-value = "" 
        tReq     :screen-value = (if   trequirement:input-value = {&ALL} then "0"
                                  else trequirement:input-value )
        tDateUser:screen-value = cCurrUser
        .
   /* Display the current record in widgets. */
   else if available filtercomnote 
    then
     do:
       assign
           tNoteText:screen-value = filtercomnote.notes
           tSubject :screen-value = filtercomnote.subject
           tReq     :screen-value = if cRequimentList = "" then "0" else string(filtercomnote.reqID)                                
           tDateUser:screen-value = string(filtercomnote.noteDate, "99/99/9999 HH:MM AM") + 
                                    "  " + filtercomnote.username
           .
     end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flStateID trequirement fSearch tEntityId tname tEntity tDateUser 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bFilter bClear bAddNote trequirement fSearch brwData bRefresh tSubject 
         tNoteText RECT-36 RECT-37 RECT-65 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable th as handle no-undo. 
 
  if query brwData:num-results = 0 
   then
    do: 
      message "There is nothing to export"
        view-as alert-box warning buttons ok.
      return.
    end.
 
  publish "GetReportDir" (output std-ch).
  
  th = temp-table filtercomnote:handle.
  
  run util/exporttable.p (table-handle th,
                          "filtercomnote",
                          "for each filtercomnote",
                          "entity,entityID,reqID,stateID,notedate,subject,uid,notes",
                          "Entity,EntityID,Req. ID,StateID,Date,Subject,User,Notes",
                          std-ch,
                          "ComNotes-" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table filtercomnote.
  
  do with frame {&frame-name}:
  end.
 
  for each comnote where comnote.reqId = (if (trequirement:input-value = {&ALL} or trequirement:input-value = "") then comnote.reqId else integer(trequirement:input-value)):
    /* test if the record contains the search text */  
    if ((fSearch:input-value = "") or
       ((comnote.notes matches "*" + fSearch:input-value + "*") or (comnote.subject matches "*" + fSearch:input-value + "*")))
     then
      do:
        create filtercomnote.
        buffer-copy comnote to filtercomnote.
      end.    
  end.    
 
  open query brwData preselect each filtercomnote. 
   
  setStatusCount(query brwData:num-results). 
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table comnote.
 
  /* Getting notes for given entity, entityId and all requirements */
  publish "getComplianceNotes" (input pcEntity,
                                input pcEntityId,
                                input 0,         
                                output table comnote,
                                output std-lo,
                                output std-ch).
  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
  
  /* filter data and fill the browse with the filtered data and set status in taskbar */
  run filterData     in this-procedure no-error.      

  /* display data on UI */
  run displayData    in this-procedure  no-error.

  /*setwidget state on UI */
  run setWidgetState in this-procedure ({&default}) no-error. 
  
  setStatusRecords(query brwData:num-results). 
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveNote C-Win 
PROCEDURE saveNote PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define output parameter loSuccess as logical   no-undo.

  define variable iSeq              as integer   no-undo. 
  define variable dtNoteDate        as datetime  no-undo. 
  define variable chNoteType        as character no-undo.
 
  do with frame {&frame-name}:
  end.

  /*----------client validations------------*/
  if treq:input-value = "?" or treq:input-value = "0" 
   then
    do:
      message "Please Select Requirement"
          view-as alert-box info buttons ok.
      loSuccess = false.
      return.
    end.
  
  empty temp-table tcomnote.

  /*-----------fill tt to pass to server-----------*/ 
  create tcomnote.                                              
  assign 
      tcomnote.entity   = pcEntity
      tcomnote.entityID = pcEntityId
      tcomnote.reqID    = treq:input-value
      tcomnote.stateID  = pcStateID
      tcomnote.username = cCurrUser
      tcomnote.uid      = cCurrentUID
      tcomnote.subject  = tSubject:input-value
      tcomnote.notes    = tNoteText:input-value
      . 

  if cAction = {&modifyNote}
   then
    do:
      tcomnote.seq = filtercomnote.seq. 
      publish "modifyComplianceNote" (input table tcomnote,                                                          
                                      output std-lo,
                                      output std-ch).
    end.    
  else
   publish "newComplianceNote" (input table tcomnote,
                                output iSeq,
                                output dtNoteDate,
                                output chNoteType,
                                output std-lo,
                                output std-ch).

  /*----------if not success then return----------*/
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
 
  /*-----------in case of edit the data id updated. Incase of add, new record ID is created-------------*/ 
  find first tcomnote no-error.
  if available tcomnote 
   then
    do:
      find first comnote where comnote.stateID  = tcomnote.stateID
                           and comnote.entity   = tcomnote.entity
                           and comnote.entityID = tcomnote.entityID
                           and comnote.reqID    = tcomnote.reqID
                           and comnote.seq      = tcomnote.seq no-error.
      if available comnote 
       then
        assign 
            comnote.subject = tcomnote.subject
            comnote.notes   = tcomnote.notes
            iseq            = comnote.seq
            .
      else
       do:
         create comnote.
         buffer-copy tcomnote to comnote.
         assign 
             comnote.seq      = iSeq
             comnote.notedate = dtNoteDate
             comnote.notetype = chNoteType
             .
       end.

      /* Filtering data acording to UI filters */
      run filterData in this-procedure no-error.
      /* repositioning to updated record */
      find first filtercomnote where filtercomnote.stateID  = tcomnote.stateID
                                 and filtercomnote.entity   = tcomnote.entity
                                 and filtercomnote.entityID = tcomnote.entityID
                                 and filtercomnote.reqID    = tcomnote.reqID
                                 and filtercomnote.seq      = iseq no-error.
      if available filtercomnote 
       then
        reposition brwData to rowid rowid(filtercomnote) no-error.
  end. 
  loSuccess = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setStatusMsg C-Win 
PROCEDURE setStatusMsg :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  setStatusMessage({&ResultNotMatch}). 

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose: 
  pcInputValues:  
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAction as character no-undo.
  
  do with frame fMain:
  end.
  
  case pAction:  
    when {&addNote}     /* in case of adding a new note */
     then        
      assign
          treq:sensitive         = true          
          tSubject:read-only     = false
          tNoteText:read-only    = false
          bSave:sensitive        = true
          bCancel:sensitive      = true
          bSpellCheck:sensitive  = true       
          trequirement:sensitive = false
          bRefresh:sensitive     = false
          bFilter:sensitive      = false 
          bClear:sensitive       = false
          fSearch:sensitive      = false
          bAddNote:sensitive     = false 
          bModifyNote:sensitive  = false
          bdelete:sensitive      = false
          brwData:sensitive      = false
          .

    when {&modifyNote}      /* in case of modifying an existing note */
     then
      assign
          tSubject:read-only     = false
          tNoteText:read-only    = false       
          bCancel:sensitive      = true
          bSpellCheck:sensitive  = true
          treq:sensitive         = true
          trequirement:sensitive = false
          bRefresh:sensitive     = false
          bFilter:sensitive      = false
          bClear:sensitive       = false
          fSearch:sensitive      = false
          bAddNote:sensitive     = false
          bModifyNote:sensitive  = false
          bdelete:sensitive      = false                             
          brwData:sensitive      = false
          .                              

    otherwise           /* this covers all the cases except above 2 */
     assign
         trequirement:sensitive = true
         bRefresh:sensitive     = true
         bFilter:sensitive      = true
         bClear:sensitive       = true
         fSearch:sensitive      = true
         bAddNote:sensitive     = true
         bModifyNote:sensitive  = (available filtercomnote and filtercomnote.uid = cCurrentUID)
         bdelete:sensitive      = (available filtercomnote and filtercomnote.uid = cCurrentUID)
         brwData:sensitive      = true
         bSave:sensitive        = false
         bCancel:sensitive      = false
         treq:sensitive         = false
         bSpellCheck:sensitive  = false
         tSubject:read-only     = true
         tNoteText:read-only    = true
         .
  end case.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized  
   then
    {&window-name}:window-state = window-normal .
  
  c-Win:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.
  
  tNoteText:width-pixels       = {&window-name}:width-pixels - 378.
  tNoteText:height-pixels      = frame {&frame-name}:height-pixels - tNoteText:y - 5.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  tSubject:width-pixels        = frame {&frame-name}:width-pixels - 486.
  bSpellCheck:x                = frame {&frame-name}:width-pixels - 40.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

