&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------
 File: dialogconfig.w

 Description: Create a configuration file to select states at beginning
 
 Input Parameters:
     <none>
 
 Output Parameters:
     <none>
 
 Author: Sachin Chaturvedi
 
 Created:03/27/2018 
 @Modified
 Date         Name            Description
 11/21/18     Rahul           Modified to remove inactive people checkbox
 10/15/2019   Archana Guptda  Modifies for the Phase 2.
------------------------------------------------------------------------*/
{lib/std-def.i}

&scoped-define  defaultStateDesc           "Colorado"
&scoped-define  defaultStateCode           "CO"
&scoped-define  addAllState                "addAllState"
&scoped-define  addState                   "addState"
&scoped-define  removeAllState             "removeAllState"
&scoped-define  removeState                "removeState"

define variable cAvailableStates           as character no-undo.
define variable cSelectedStates            as character no-undo.
define variable cList                      as character no-undo.
define variable cTrackStates               as character no-undo.
define variable cTrackReportDir            as character no-undo.
define variable lTrackConfirmExit          as logical   no-undo.
define variable lSaveConfig                as logical   no-undo.
define variable cTrackExportType           as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tOtherStates tStates tConfirmExit ~
tReportsSearch tReportDir Btn_Cancel tExportType 
&Scoped-Define DISPLAYED-OBJECTS tOtherStates tStates tConfirmExit ~
tReportDir tExportType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddAll  NO-FOCUS
     LABEL "ALL-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add all States to the Search options".

DEFINE BUTTON bAddState  NO-FOCUS
     LABEL "-->" 
     SIZE 4.8 BY 1.14 TOOLTIP "Add a State to the Search options".

DEFINE BUTTON bRemoveAll  NO-FOCUS
     LABEL "<-- ALL" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove All States from Search options".

DEFINE BUTTON bRemoveState  NO-FOCUS
     LABEL "<--" 
     SIZE 4.8 BY 1.14 TOOLTIP "Remove a State from the Search options".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON Btn_OK AUTO-GO  NO-FOCUS
     LABEL "Save" 
     SIZE 15 BY 1.14.

DEFINE BUTTON tReportsSearch 
     LABEL "..." 
     SIZE 4.8 BY 1.14.

DEFINE VARIABLE tReportDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Reports Directory" 
     VIEW-AS FILL-IN 
     SIZE 58 BY 1 TOOLTIP "Enter the default directory to save report PDF documents and CSV export files" NO-UNDO.

DEFINE VARIABLE tExportType AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "XLSX", "X",
"CSV", "C"
     SIZE 20.8 BY .62 TOOLTIP "Select the type of Excel export to use" NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 82.8 BY 14.57.

DEFINE VARIABLE tOtherStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     SIZE 24 BY 9.48 NO-UNDO.

DEFINE VARIABLE tStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SORT SCROLLBAR-VERTICAL 
     SIZE 25 BY 9.48 NO-UNDO.

DEFINE VARIABLE tConfirmExit AS LOGICAL INITIAL no 
     LABEL "Confirm Application Exit" 
     VIEW-AS TOGGLE-BOX
     SIZE 26.8 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     Btn_OK AT ROW 16.14 COL 27.4
     bAddAll AT ROW 5.33 COL 40.4 WIDGET-ID 90 NO-TAB-STOP 
     tOtherStates AT ROW 2.71 COL 12.2 NO-LABEL WIDGET-ID 82 NO-TAB-STOP 
     tStates AT ROW 2.71 COL 49.4 NO-LABEL WIDGET-ID 74 NO-TAB-STOP 
     tConfirmExit AT ROW 12.71 COL 20.4 WIDGET-ID 132
     tReportsSearch AT ROW 13.62 COL 79 WIDGET-ID 30
     tReportDir AT ROW 13.71 COL 18.4 COLON-ALIGNED WIDGET-ID 2
     Btn_Cancel AT ROW 16.14 COL 44
     bAddState AT ROW 6.43 COL 40.4 WIDGET-ID 78 NO-TAB-STOP 
     bRemoveState AT ROW 7.52 COL 40.4 WIDGET-ID 80 NO-TAB-STOP 
     bRemoveAll AT ROW 8.62 COL 40.4 WIDGET-ID 88 NO-TAB-STOP 
     tExportType AT ROW 14.95 COL 20.4 NO-LABEL WIDGET-ID 208
     "Options" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.14 COL 3.4 WIDGET-ID 70
     "Available States" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 1.86 COL 14 WIDGET-ID 134
          FONT 6
     "Selected States" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 1.86 COL 51.8 WIDGET-ID 136
          FONT 6
     "Export To:" VIEW-AS TEXT
          SIZE 10 BY .62 TOOLTIP "Export to Excel as Spreadsheet or CSV file" AT ROW 14.95 COL 10.2 WIDGET-ID 214
     RECT-31 AT ROW 1.43 COL 2 WIDGET-ID 68
     SPACE(0.59) SKIP(1.47)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddAll IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAddState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemoveAll IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemoveState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn_OK IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Configuration */
do:
  lSaveConfig = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAll fMain
ON CHOOSE OF bAddAll IN FRAME fMain /* ALL--> */
do:
  run addRemoveStates in this-procedure(input {&addAllState}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddState fMain
ON CHOOSE OF bAddState IN FRAME fMain /* --> */
do:
  run addRemoveStates  in this-procedure(input {&addState}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll fMain
ON CHOOSE OF bRemoveAll IN FRAME fMain /* <-- ALL */
do:
  run addRemoveStates  in this-procedure(input {&removeAllState}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveState fMain
ON CHOOSE OF bRemoveState IN FRAME fMain /* <-- */
do:
  run addRemoveStates in this-procedure(input {&removeState}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fMain
ON CHOOSE OF Btn_Cancel IN FRAME fMain /* Cancel */
do:
  lSaveConfig = false. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fMain
ON CHOOSE OF Btn_OK IN FRAME fMain /* Save */
do:
  run saveConfiguration in this-procedure.
  
  /* If there was any error then do not close the dialog. */
  if not lSaveConfig
   then
    return no-apply.
 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tConfirmExit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tConfirmExit fMain
ON VALUE-CHANGED OF tConfirmExit IN FRAME fMain /* Confirm Application Exit */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tExportType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tExportType fMain
ON VALUE-CHANGED OF tExportType IN FRAME fMain
do:
  run enableDisableSave in this-procedure.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOtherStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fMain
ON DEFAULT-ACTION OF tOtherStates IN FRAME fMain
do:
  run addRemoveStates in this-procedure(input {&addState}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fMain
ON VALUE-CHANGED OF tOtherStates IN FRAME fMain
do:
  tStates:screen-value = ?.
  run setWidgetState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportDir fMain
ON VALUE-CHANGED OF tReportDir IN FRAME fMain /* Reports Directory */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tReportsSearch fMain
ON CHOOSE OF tReportsSearch IN FRAME fMain /* ... */
do:
  run getReportDirectory in this-procedure.
  run enableDisableSave  in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fMain
ON DEFAULT-ACTION OF tStates IN FRAME fMain
do:
  /* Selected state list should not be empty therefore we only allow
     removal of a state from this list when there is more than one state in it. */

  if tStates:num-items > 1
   then
    run addRemoveStates in this-procedure(input {&removeState}).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fMain
ON VALUE-CHANGED OF tStates IN FRAME fMain
do:
  tOtherStates:screen-value = ?.
  run setWidgetState in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then 
  frame {&frame-name}:parent = active-window.
       
bAddState:load-image("images/s-next.bmp").
bRemoveState:load-image("images/s-previous.bmp").
bAddAll:load-image("images/s-nextpg.bmp").
bRemoveAll:load-image("images/s-previouspg.bmp").
bAddState:load-image-insensitive("images/s-next-i.bmp").
bRemoveState:load-image-insensitive("images/s-previous-i.bmp").
bAddAll:load-image-insensitive("images/s-nextpg-i.bmp").
bRemoveAll:load-image-insensitive("images/s-previouspg-i.bmp").

publish "GetReportDir"(output tReportDir).    
publish "GetConfirmExit"(output tConfirmExit).


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  run enable_UI.

  /* Getting statelists from comdatasrv */
  publish "getStatesListItems" (input  "," ,
                                output cList,
                                output std-lo,
                                output std-ch).

  if not std-lo         
   then
    do:
       message std-ch
         view-as alert-box error buttons ok.
       return string(std-lo).
    end.

  tOtherStates:list-item-pairs = if cList <> "" and cList <> "?" then cList else ?.

  /* Getting Last saved statelists from Options/Settings */
  publish "GetSearchStates" (output std-ch). 

  tStates:list-item-pairs = if std-ch <> "" and std-ch <> "?" and std-ch <> ? 
                             then std-ch else ({&defaultStateDesc} + "," + {&defaultStateCode}).

  if (tOtherStates:list-item-pairs = ? or
     lookup({&defaultStateCode}, tOtherStates:list-item-pairs) = 0) and 
     (std-ch = ? or
     lookup({&defaultStateCode}, std-ch) = 0 )
   then
    tOtherStates:add-last({&defaultStateDesc}, {&defaultStateCode}).

  do std-in = 2 to num-entries(std-ch) by 2:
    tOtherStates:delete(entry(std-in, std-ch)).
  end.
  
  run setWidgetState in this-procedure.

  /* Setting initial values of widgets in variable to be used for enable/disable save */
  assign
        cTrackStates               = tStates:list-item-pairs
        lTrackConfirmExit          = tConfirmExit:checked
        cTrackReportDir            = tReportDir:input-value
        cTrackExportType           = tExportType:input-value
        .

  /* Toggles tSearchAttorneys tSearchPeople tSearchInactivePeople are temporarily disabled
     as part of ongoing developments.*/

  wait-for go of frame {&frame-name}.
end.
               
run disable_UI.

return string(lSaveConfig).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addRemoveStates fMain 
PROCEDURE addRemoveStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcAction as character no-undo.
  
  do with frame {&frame-name}:
  end.

  case ipcAction:

    when {&addState} then          /* Add a State to the Search options */
     do:
       if tOtherStates:input-value = ?
        then 
         return.

       tStates:add-last(entry(lookup(tOtherStates:input-value, tOtherStates:list-item-pairs) - 1, tOtherStates:list-item-pairs), 
                            tOtherStates:input-value).
       tStates:list-item-pairs = trim(tStates:list-item-pairs,",").      
       tOtherStates:delete(tOtherStates:input-value).      
     end.
         
    when {&addAllState} then       /* Add all States to the Search options */
     assign  
           cAvailableStates             = if tOtherStates:list-item-pairs = ? or tOtherStates:list-item-pairs = "?" then "" else tOtherStates:list-item-pairs
           cSelectedStates              = if tStates:list-item-pairs = ? or tStates:list-item-pairs = "?" then "" else tStates:list-item-pairs
           tStates:list-item-pairs      = trim(cAvailableStates + "," + cSelectedStates,",")
           tOtherStates:list-item-pairs = ?
           cAvailableStates             = ""
           cSelectedStates              = ""
           .

    when {&removeState} then       /* Remove a State from the Search options */
     do:
       if tStates:input-value = ? 
        then
         return.

       tOtherStates:add-last(entry(lookup(tStates:input-value, tStates:list-item-pairs) - 1, tStates:list-item-pairs), 
                             tStates:input-value).
       tOtherStates:list-item-pairs = trim(tOtherStates:list-item-pairs,",").       
       tStates:delete(tStates:input-value).
     end.
 
    when {&removeAllState}         /* Remove all States from the Search options */
     then
      do:
        assign  
              cAvailableStates             = if tOtherStates:list-item-pairs = ? or tOtherStates:list-item-pairs = "?" then "" else tOtherStates:list-item-pairs
              cSelectedStates              = if tStates:list-item-pairs = ? or tStates:list-item-pairs = "?" then "" else tStates:list-item-pairs
              tOtherStates:list-item-pairs = trim(cAvailableStates + "," + cSelectedStates,",")
              tStates:list-item-pairs      = {&defaultStateDesc} + "," + {&defaultStateCode}
              cAvailableStates             = ""
              cSelectedStates              = ""
              .
        tOtherStates:delete({&defaultStateCode}). 
      end.
  end case.

  run setWidgetState    in this-procedure.
  run enableDisableSave in this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave fMain 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Enabling the save button only when user made any modification on this dialog after its loading. */
  Btn_OK:sensitive = not (cTrackStates               = tStates:list-item-pairs  and
                          lTrackConfirmExit          = tConfirmExit:checked     and
                          cTrackReportDir            = tReportDir:input-value   and
                          cTrackExportType           = tExportType:input-value ) no-error.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tOtherStates tStates tConfirmExit tReportDir tExportType 
      WITH FRAME fMain.
  ENABLE tOtherStates tStates tConfirmExit tReportsSearch tReportDir Btn_Cancel 
         tExportType 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getReportDirectory fMain 
PROCEDURE getReportDirectory :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  std-ch = tReportDir:input-value.
  
  if tReportDir:input-value > "" 
   then
    system-dialog get-dir std-ch initial-dir std-ch. 
  else
   system-dialog get-dir std-ch.
  
  if std-ch > "" and std-ch <> ? 
   then
    tReportDir:screen-value = std-ch.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveConfiguration fMain 
PROCEDURE saveConfiguration :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  publish "SetReportDir" (tReportDir:input-value).
  if error-status:error 
   then
    do:
      message "Invalid Directory"
        view-as alert-box error buttons ok.
      lSaveConfig = false.
      run enableDisableSave in this-procedure.
      apply "ENTRY" to tReportDir.
      return.
    end.

  publish "SetSearchStates" (tStates:list-item-pairs). 
  publish "SetReportDir"    (tReportDir:input-value).
  publish "SetConfirmExit"  (tConfirmExit:checked).
  
  if cTrackExportType <> tExportType:input-value
   then   
    publish "SetExportType" (tExportType:input-value).
  
  lSaveConfig = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState fMain 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  /* Enable/Disable the Add/Add All/Remove/Remove All buttons based on 
     the list items in their respective selection lists.*/
  assign
        bAddState:sensitive    = tOtherStates:input-value <> ?
        bRemoveAll:sensitive   = tStates:num-items > 1
        bAddAll:sensitive      = tOtherStates:num-items > 0
        bRemoveState:sensitive = tStates:num-items > 1 and tStates:input-value <> ? 
        .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

