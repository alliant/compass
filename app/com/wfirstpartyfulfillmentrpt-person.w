&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File:wfirstpartyfulfillmentrpt-person.w 

  Description: Annual First-Party Qualifications Report for Person

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma 

  Created: 06/20/2018
  
  Modified:
    03.29.2022    Shefali        Task# 86699  Modified in exportData IP.

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

create widget-pool.

/* ***************************  Definitions  ************************** */

/* Standard Library Files */
{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}

/* Temp-table */
{tt/reqfulfill.i}
               
/* Local Variable Definitions -----                                */   
define variable dColumnWidth  as decimal   no-undo.
define variable cReqList      as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwComReport

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES reqfulfill

/* Definitions for BROWSE brwComReport                                  */
&Scoped-define FIELDS-IN-QUERY-brwComReport reqfulfill.entityID "Person ID" reqfulfill.entityNPN "NPN" reqfulfill.entityname "Name" reqfulfill.activ reqfulfill.qualification "Qualification" reqfulfill.qualificationNumber "Number" reqfulfill.stat "Status" reqfulfill.effectiveDate reqfulfill.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwComReport   
&Scoped-define SELF-NAME brwComReport
&Scoped-define QUERY-STRING-brwComReport for each reqfulfill
&Scoped-define OPEN-QUERY-brwComReport open query {&SELF-NAME} for each reqfulfill.
&Scoped-define TABLES-IN-QUERY-brwComReport reqfulfill
&Scoped-define FIRST-TABLE-IN-QUERY-brwComReport reqfulfill


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwComReport}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btGet btExport cbState cbRequirement cbYear ~
brwComReport RECT-63 RECT-56 
&Scoped-Define DISPLAYED-OBJECTS cbState cbRequirement cbYear 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getQualificationDesc C-Win 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "Get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cbRequirement AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 57.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.4 BY 2.43.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 140.6 BY 2.43.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwComReport FOR 
      reqfulfill SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwComReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwComReport C-Win _FREEFORM
  QUERY brwComReport DISPLAY
      reqfulfill.entityID            label         "Person ID"                              width 15
reqfulfill.entityNPN                label         "NPN"                  format "x(12)"
reqfulfill.entityname               label         "Name"                 format "x(80)"     width 36
reqfulfill.activ                    column-label  "Active" width 9 view-as toggle-box  
reqfulfill.qualification            label         "Qualification"        format "x(50)"     width 25
reqfulfill.qualificationNumber      label         "Number"               format "x(10)"
reqfulfill.stat                     label         "Status"               format "x(20)"     width 16 
reqfulfill.effectiveDate            column-label  "Effective"            format "99/99/99"  width 13
reqfulfill.expirationDate           column-label  "Expiration"           format "99/99/99"  width 13
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 158.8 BY 19.29
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btGet AT ROW 1.67 COL 134.2 WIDGET-ID 42 NO-TAB-STOP 
     btExport AT ROW 1.67 COL 143.4 WIDGET-ID 2 NO-TAB-STOP 
     cbState AT ROW 2.05 COL 8.4 COLON-ALIGNED WIDGET-ID 90
     cbRequirement AT ROW 2.05 COL 47.8 COLON-ALIGNED WIDGET-ID 18
     cbYear AT ROW 2.05 COL 115 COLON-ALIGNED WIDGET-ID 276
     brwComReport AT ROW 4.1 COL 2.2 WIDGET-ID 200
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 143.4 WIDGET-ID 278
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1.05 COL 3.2 WIDGET-ID 266
     RECT-63 AT ROW 1.33 COL 2.2 WIDGET-ID 236
     RECT-56 AT ROW 1.33 COL 142.4 WIDGET-ID 264
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 169 BY 26.2
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Person Annual First-Party Qualifications Report"
         HEIGHT             = 23.38
         WIDTH              = 161
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwComReport cbYear DEFAULT-FRAME */
ASSIGN 
       brwComReport:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwComReport:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwComReport
/* Query rebuild information for BROWSE brwComReport
     _START_FREEFORM
open query {&SELF-NAME} for each reqfulfill.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwComReport */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Person Annual First-Party Qualifications Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Person Annual First-Party Qualifications Report */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Person Annual First-Party Qualifications Report */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwComReport
&Scoped-define SELF-NAME brwComReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComReport C-Win
ON DEFAULT-ACTION OF brwComReport IN FRAME DEFAULT-FRAME
do:
  run openEntity in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComReport C-Win
ON ROW-DISPLAY OF brwComReport IN FRAME DEFAULT-FRAME
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComReport C-Win
ON START-SEARCH OF brwComReport IN FRAME DEFAULT-FRAME
do:
  {lib/brw-startSearch.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME DEFAULT-FRAME /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME DEFAULT-FRAME /* Get */
do:    
  resetScreen().
  run getdata in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRequirement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRequirement C-Win
ON VALUE-CHANGED OF cbRequirement IN FRAME DEFAULT-FRAME /* Requirement */
do:
  resetScreen().
  resultsChanged(false). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME DEFAULT-FRAME /* State */
do:
  resetScreen().
  resultsChanged(false).
  
  /* Delete Select State from state combo-box */
  {lib/modifylist.i &cbhandle=cbState:handle &list=list-item-pairs &delimiter = "," &item = '{&SelectState}'} 

  if cbState:input-value <> "" 
   then
    run getRequirementCombo in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbYear C-Win
ON VALUE-CHANGED OF cbYear IN FRAME DEFAULT-FRAME /* Year */
do:
  resetScreen().
  resultsChanged(false). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
    current-window                = {&window-name} 
    this-procedure:current-window = {&window-name}
    .

/* The CLOSE event can be used from inside or outside the procedure to terminate it. */
on close of this-procedure 
  run disable_UI.

{lib/win-status.i}

/* Best default for GUI applications is...                              */
pause 0 before-hide.

subscribe to "closeWindow" anywhere.

btExport:load-image            ("images/excel.bmp").
btExport:load-image-insensitive("images/excel-i.bmp").
btGet   :load-image            ("images/Completed.bmp").              
btGet   :load-image-insensitive("images/Completed-i.bmp").

publish "GetSearchStates" (output std-ch).
if std-ch > "" 
 then
  std-ch = "," + std-ch.

cbState:list-item-pairs in frame {&frame-name} = {&SelectState} + "," + std-ch.

/* Populate Year combo */ 
run getYearCombo in this-procedure.

/* Function to reset default values and clear filters */
resetScreen().

/* Sets title of the window according to the standard */
{lib/win-main.i}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
    
  run enable_UI.

  {lib/get-column-width.i &col="'entityname'"    &var=dColumnWidth} 
  {lib/get-column-width.i &col="'qualification'" &var=dColumnWidth}
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.

  run windowResized in this-procedure.

  disable cbRequirement btExport btGet cbYear with frame {&frame-name}.

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState cbRequirement cbYear 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE btGet btExport cbState cbRequirement cbYear brwComReport RECT-63 
         RECT-56 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable     as handle    no-undo.

  if query brwComReport:num-results = 0 then
  do: 
    message "There is nothing to export"
        view-as alert-box warning buttons ok.
    return.
  end.

  publish "GetReportDir" (output std-ch).
  htable = temp-table reqfulfill:handle.
  
  run util/exporttable.p (table-handle htable,
                          "reqfulfill",
                          "for each reqfulfill",
                          "entityID,entityNPN,entityname,activ,qualification,qualificationNumber,stat,effectiveDate,expirationDate",
                          "Person ID,NPN,Name,Active,Qualification,Number,Status,Effective,Expiration",
                          std-ch,
                          "PersonAnnualFirstPartyQualificationsReport" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  /* Server Call */
  run server/querycompanyfulfillment.p (input cbState:input-value in frame {&frame-name},
                                        input {&PersonCode},
                                        input cbYear:input-value,
                                        input cbRequirement:input-value, 
                                        output table reqfulfill,
                                        output std-lo,
                                        output std-ch).
  
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  for each reqfulfill:
    reqfulfill.stat = getQualificationDesc(reqfulfill.stat).
  end.
                                                                         
  open query brwComReport preselect each reqfulfill.

  /* Makes widget enable-disable based on the data */ 
  if query brwComReport:num-results > 0 
   then
    assign
        browse brwComReport:sensitive = true
               btExport    :sensitive = true
        .  
  else
   assign 
       browse brwComReport:sensitive = false
              btExport    :sensitive = false
       .
  
  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwComReport:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRequirementCombo C-Win 
PROCEDURE getRequirementCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  /* Get the requirements from server and updates the data model. */
  publish "getAllRequirementListItems" (input cbState:input-value in frame {&frame-name},
                                        input {&PersonCode},                                   
                                        input {&CompanyCode},
                                        input "," ,   /* Delimiter */
                                        output cReqList,                             
                                        output std-lo,
                                        output std-ch).
 
  if not std-lo /* Return if not success*/
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
 
  if cReqList <> ""
   then
    assign 
        cbRequirement:list-item-pairs = trim(cReqList,",")
        cbRequirement:screen-value    = entry(2,cbRequirement:list-item-pairs)
        cbRequirement:sensitive       = true
        cbYear:sensitive              = true
        btGet:sensitive               = true
        .
  else
   assign 
       cbRequirement:list-item-pairs = ?
       cbRequirement:sensitive       = false
       cbYear:sensitive              = false
       btGet:sensitive               = false
       .

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getYearCombo C-Win 
PROCEDURE getYearCombo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  std-ch = "".
 
  do std-in = - 1 to 7 with frame {&frame-name}:
    std-ch = std-ch + "," + string(year(today) - std-in).
  end.
  
  assign 
      cbYear:list-items    = {&ALL} + "," + trim(std-ch,",")
      cbYear:screen-value  = {&ALL}
      .
 
  std-ch = "".
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openEntity C-Win 
PROCEDURE openEntity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
 
  if not available reqfulfill 
   then 
    return.

  publish "OpenWindow" (input {&person}, 
                        input reqfulfill.entityid, 
                        input "wpersondetail.w", 
                        input "character|input|" + reqfulfill.entityid + "^integer|input|2" + "^character|input|",                                   
                        input this-procedure).
                                                
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
      frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
      frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
      frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
      frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels
      {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 12
      {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 7.5
      .

  {lib/resize-column.i &col="'entityname'" &var=dColumnWidth}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getQualificationDesc C-Win 
FUNCTION getQualificationDesc RETURNS CHARACTER
  ( cStat as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  publish "GetSysPropDesc" ("COM", "Qualification", "Status", cStat, output std-ch).
  return std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  disable btExport with frame {&frame-name}. 
  
  setStatusMessage("").
  
  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  setStatusMessage("Results may not match current parameters.").
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

