&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
  File: wcompliancestatusrpt-organization.w

  Description: Screen for Compliance Status Report for Organization.
  
  Modified: 
  Date          Name           Description
  08/09/2019    Gurvindar      Removed progress error while populating combo-box
  03.29.2022    Shefali        Task# 86699  Modified in exportData IP.
----------------------------------------------------------------------*/
create widget-pool.
{tt/compliance.i}

{tt/comcode.i}
{tt/organization.i}

{lib/std-def.i}
{lib/com-def.i}
{lib/get-column.i}
{lib/winshowscrollbars.i}

define temp-table tResults no-undo like Compliance
  field sequence as character.

define variable dColumnWidth   as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwComplianceRpt

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tResults

/* Definitions for BROWSE brwComplianceRpt                              */
&Scoped-define FIELDS-IN-QUERY-brwComplianceRpt getComStat(tResults.sequence) @ tResults.sequence tResults.stateID "State ID" tResults.currentdate tResults.refID "Organization ID" tResults.name "Organization Name" tResults.role "Role" tResults.roleName "Name" tResults.roleID "ID" tResults.Comstat "Status As Of" tResults.logdate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwComplianceRpt   
&Scoped-define SELF-NAME brwComplianceRpt
&Scoped-define QUERY-STRING-brwComplianceRpt for each tResults
&Scoped-define OPEN-QUERY-brwComplianceRpt open query {&SELF-NAME} for each tResults.
&Scoped-define TABLES-IN-QUERY-brwComplianceRpt tResults
&Scoped-define FIRST-TABLE-IN-QUERY-brwComplianceRpt tResults


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwComplianceRpt}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState flDate btGet brwComplianceRpt ~
fOrganization bOrgLookup RECT-64 RECT-65 
&Scoped-Define DISPLAYED-OBJECTS cbState flDate tglgreen tglred tglyellow ~
tglna fOrganization 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStat C-Win 
FUNCTION getComStat returns character PRIVATE
  ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getComStatDesc C-Win 
FUNCTION getComStatDesc returns character
  ( input cStat as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bOrgLookup  NO-FOCUS
     LABEL "Lookup" 
     SIZE 4.6 BY 1.14 TOOLTIP "Organization lookup".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export data".

DEFINE BUTTON btGet  NO-FOCUS
     LABEL "get" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 22.4 BY 1 TOOLTIP "Select State" NO-UNDO.

DEFINE VARIABLE flDate AS DATE FORMAT "99/99/99":U 
     LABEL "Status As Of" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 13 BY 1 TOOLTIP "Display the status as of this date" NO-UNDO.

DEFINE VARIABLE fOrganization AS CHARACTER FORMAT "X(256)":U INITIAL "ALL" 
     LABEL "Organization" 
     VIEW-AS FILL-IN 
     SIZE 66.8 BY 1 TOOLTIP "Display the status as of this date".

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98.2 BY 4.

DEFINE RECTANGLE RECT-64
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26.6 BY 4.

DEFINE RECTANGLE RECT-65
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.4 BY 4.

DEFINE VARIABLE tglgreen AS LOGICAL INITIAL no 
     LABEL "Green" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

DEFINE VARIABLE tglna AS LOGICAL INITIAL no 
     LABEL "N/A" 
     VIEW-AS TOGGLE-BOX
     SIZE 8 BY .81 NO-UNDO.

DEFINE VARIABLE tglred AS LOGICAL INITIAL no 
     LABEL "Red" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

DEFINE VARIABLE tglyellow AS LOGICAL INITIAL no 
     LABEL "Yellow" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwComplianceRpt FOR 
      tResults SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwComplianceRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwComplianceRpt C-Win _FREEFORM
  QUERY brwComplianceRpt DISPLAY
      getComStat(tResults.sequence)  @ tResults.sequence label ""                       format "x(2)"     width 2   
tResults.stateID                                         label        "State ID"        format "x(15)"    width 10  
tResults.currentdate                                   column-label "Current Log"       format "99/99/99" width 14      
tResults.refID                                         label        "Organization ID"   format "x(15)"    width 17
tResults.name                                          label        "Organization Name" format "x(80)"    width 40
tResults.role                                          label        "Role"              format "x(20)"    width 15
tResults.roleName                                      label        "Name"             format "x(80)"     width 25
tResults.roleID                                        label        "ID"               format "x(20)"     width 10
tResults.Comstat                                       label        "Status As Of"      format "x(16)"    width 15
tResults.logdate                                       column-label "Last Activity"     format "99/99/99" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 174.4 BY 17.95
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbState AT ROW 2.1 COL 17.2 COLON-ALIGNED WIDGET-ID 242
     flDate AT ROW 2.1 COL 71 COLON-ALIGNED WIDGET-ID 252
     tglgreen AT ROW 1.81 COL 105 WIDGET-ID 284
     tglred AT ROW 2.86 COL 105 WIDGET-ID 286
     tglyellow AT ROW 3.86 COL 105 WIDGET-ID 288
     tglna AT ROW 2.86 COL 116 WIDGET-ID 290
     btGet AT ROW 2.76 COL 91.8 WIDGET-ID 266 NO-TAB-STOP 
     btExport AT ROW 2.43 COL 127.2 WIDGET-ID 262 NO-TAB-STOP 
     brwComplianceRpt AT ROW 5.52 COL 2.2 WIDGET-ID 200
     fOrganization AT ROW 3.29 COL 17.2 COLON-ALIGNED WIDGET-ID 280 NO-TAB-STOP 
     bOrgLookup AT ROW 3.19 COL 86.4 WIDGET-ID 282 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 1 COL 3.2 WIDGET-ID 28
     "Filters" VIEW-AS TEXT
          SIZE 5.4 BY .62 AT ROW 1 COL 101 WIDGET-ID 270
     "Actions" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 1.05 COL 127.2 WIDGET-ID 274
     RECT-2 AT ROW 1.29 COL 2.2 WIDGET-ID 8
     RECT-64 AT ROW 1.29 COL 100 WIDGET-ID 268
     RECT-65 AT ROW 1.29 COL 126.2 WIDGET-ID 272
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 176.6 BY 22.62
         DEFAULT-BUTTON btGet WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Organization Compliance Status Report"
         HEIGHT             = 22.57
         WIDTH              = 176.6
         MAX-HEIGHT         = 34.48
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 34.48
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwComplianceRpt btExport fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwComplianceRpt:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwComplianceRpt:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR BUTTON btExport IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       fOrganization:READ-ONLY IN FRAME fMain        = TRUE
       fOrganization:PRIVATE-DATA IN FRAME fMain     = 
                "ALL".

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tglgreen IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tglna IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tglred IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tglyellow IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwComplianceRpt
/* Query rebuild information for BROWSE brwComplianceRpt
     _START_FREEFORM
open query {&SELF-NAME} for each tResults.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwComplianceRpt */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Organization Compliance Status Report */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Organization Compliance Status Report */
do:
  /* This event will close the window and terminate the procedure.  */
  run closeWindow in this-procedure.
  return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Organization Compliance Status Report */
do:
  run windowResized in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bOrgLookup
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bOrgLookup C-Win
ON CHOOSE OF bOrgLookup IN FRAME fMain /* Lookup */
do:
  run openDialog in this-procedure. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwComplianceRpt
&Scoped-define SELF-NAME brwComplianceRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComplianceRpt C-Win
ON DEFAULT-ACTION OF brwComplianceRpt IN FRAME fMain
do:
  run actionOpen in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComplianceRpt C-Win
ON ROW-DISPLAY OF brwComplianceRpt IN FRAME fMain
do:
  {lib/brw-rowDisplay.i}

  tResults.sequence:bgcolor 
      in browse brwComplianceRpt = if      (tResults.sequence = "3") then 12 
                                   else if (tResults.sequence = "1") then 2 
                                   else if (tResults.sequence = "2") then 14 
                                   else 15.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwComplianceRpt C-Win
ON START-SEARCH OF brwComplianceRpt IN FRAME fMain
do:
  define variable hsort as handle no-undo.
  hsort = browse {&browse-name}:current-column.

  /* stateSearch will not work, if the column label is blank.
     Therefore, we are setting the column label before 
     calling startsearch .i  */

  if hsort:label = "" 
   then
    hsort:label = "Comstat".

  {lib/brw-startSearch.i}

  if hsort:label = "Comstat" 
   then
    hsort:label = "".
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME fMain /* Export */
do:
  run exportData in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btGet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btGet C-Win
ON CHOOSE OF btGet IN FRAME fMain /* get */
do:
  resetScreen().
  run getdata in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
do:
  resetScreen().
  resultsChanged(false).

  assign
        flDate:sensitive        = (self:input-value <> "")
        btGet:sensitive         = (self:input-value <> "")
        .
                                               
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME flDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL flDate C-Win
ON VALUE-CHANGED OF flDate IN FRAME fMain /* Status As Of */
do:
  resetScreen().
  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fOrganization
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fOrganization C-Win
ON VALUE-CHANGED OF fOrganization IN FRAME fMain /* Organization */
do:
  resetScreen().
  resultsChanged(false).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tglgreen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tglgreen C-Win
ON VALUE-CHANGED OF tglgreen IN FRAME fMain /* Green */
DO:
  run filterdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tglna
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tglna C-Win
ON VALUE-CHANGED OF tglna IN FRAME fMain /* N/A */
DO:
   run filterdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tglred
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tglred C-Win
ON VALUE-CHANGED OF tglred IN FRAME fMain /* Red */
DO:
   run filterdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tglyellow
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tglyellow C-Win
ON VALUE-CHANGED OF tglyellow IN FRAME fMain /* Yellow */
DO:
   run filterdata in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels  = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
assign 
      current-window                = {&window-name} 
      this-procedure:current-window = {&window-name}
      .

/* The CLOSE event can be used from inside or outside the procedure to terminate it. */
on close of this-procedure 
  run disable_UI.

{lib/win-status.i}

/* Best default for GUI applications is...                              */
pause 0 before-hide.
subscribe to "closeWindow" anywhere.

btGet:load-image("images/Completed.bmp").              
btGet:load-image-insensitive("images/Completed-i.bmp").
btExport:load-image("images/excel.bmp").
btExport:load-image-insensitive("images/excel-i.bmp").
bOrgLookup:load-image("images/s-lookup.bmp").
bOrgLookup:load-image-insensitive("images/s-lookup-i.bmp").

publish "GetSearchStates" (output std-ch).
if std-ch > "" 
 then 
  std-ch = "," + std-ch.

cbState:list-item-pairs in frame {&frame-name} = "ALL,All" + std-ch.

/*assign all state when screen is launched*/
cbState:screen-value = {&ALL}.
      
/* Function to reset default values and clear filters */
resetScreen().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI. 

  {lib/get-column-width.i &col="'name'" &var=dColumnWidth}
  
  /* This procedure restores the window and move it to top */
  run showWindow in this-procedure.
  run windowResized in this-procedure.
  
  /* assign current system date when screen is launched */
  flDate:screen-value = string(today).
  
  {lib/win-main.i}

  if not this-procedure:persistent 
   then
    wait-for close of this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionOpen C-Win 
PROCEDURE actionOpen PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available tResults 
  then 
   return.

 publish "OpenWindow" (input {&organization}, 
                       input tResults.refID, 
                       input "worganizationdetail.w", 
                       input "character|input|" + tResults.refID + "^integer|input|2" + "^character|input|",                                   
                       input this-procedure).                                                         
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow C-Win 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  publish "WindowClosed" (input this-procedure).
  apply "CLOSE":U to this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState flDate tglgreen tglred tglyellow tglna fOrganization 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE cbState flDate btGet brwComplianceRpt fOrganization bOrgLookup RECT-64 
         RECT-65 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable htable     as handle    no-undo.

  if query brwComplianceRpt:num-results = 0 
   then
    do: 
      message "There is nothing to export"
          view-as alert-box warning buttons ok.
      return.
    end.

  publish "GetReportDir" (output std-ch).
 
  for each tResults:
    tresults.currentstat = getComStatDesc(tresults.currentstat).
  end.
 
  htable = temp-table tResults:handle.
  run util/exporttable.p (table-handle htable,
                          "tResults",
                          "for each tResults ",
                          "currentstat,stateID,currentdate,refID,name,role,roleName,roleID,Comstat,logdate",
                          "Current Status,State ID,Current Log,Organization ID,Organization Name,Role,Name,ID,Status As Of,Last Activity",
                          std-ch,
                          "OrganizationComplianceReport" + replace(string(now,"99-99-99"),"-","") + replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output std-ch,
                          output std-in).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*   define variable cval as character no-undo initial "". */
  
  close query brwComplianceRpt.
  empty temp-table tResults.
  
  do with frame {&frame-name}:
  end.

  if  (tglgreen:checked = true) or (tglred:checked = true) or (tglyellow:checked = true) or (tglna:checked = true)
   then
    for each Compliance :
      
      if compliance.comstat = {&Gcode} and tglgreen:checked = true
       then
       do:
         create tResults.                                      
         buffer-copy Compliance to tResults.
       end.
       
       else if compliance.comstat = {&Rcode} and tglred:checked = true
       then
       do:
         create tResults.                                      
         buffer-copy Compliance to tResults.
       end.
       
       else if compliance.comstat = {&Ycode} and tglYellow:checked = true
       then
       do:
         create tResults.                                      
         buffer-copy Compliance to tResults.
       end.
       
       else if compliance.comstat = "N" and tglna:checked = true
       then
       do:
         create tResults.                                      
         buffer-copy Compliance to tResults.
       end.
       
       else
        next.
    
      assign
            tResults.Comstat   = getComStatDesc(tResults.Comstat)
            tResults.sequence  = if      tResults.currentstat = {&Gcode} then "1"
                                 else if tResults.currentstat = {&Ycode} then "2"
                                 else if tResults.currentstat = {&Rcode} then "3"
                                 else "4"
          .                                    
    end.
  
  else
   for each compliance :
      create tResults.                                      
       buffer-copy Compliance to tResults.
       
    assign
          tResults.Comstat   = getComStatDesc(tResults.Comstat)
          tResults.sequence  = if      tResults.currentstat = {&Gcode} then "1"
                               else if tResults.currentstat = {&Ycode} then "2"
                               else if tResults.currentstat = {&Rcode} then "3"
                               else "4"
          .                                    
    end.
   
    
  open query brwComplianceRpt preselect each tResults.

  /* Display no. of records on status bar */
  setStatusCount(query brwComplianceRpt:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer compliance for compliance.

 empty temp-table compliance.

 do with frame {&frame-name}:
 end.

 run server/queryCompliance.p (input {&OrganizationCode},
                               input fOrganization:private-data,                              
                               input cbState:screen-value,
                               input date(flDate:screen-value),
                               output table Compliance,
                               output std-lo,
                               output std-ch).
 if not std-lo
  then
   do:
     message std-ch
       view-as alert-box error buttons ok.
     return.
   end.
  for each compliance where compliance.comstat = "N" :
    compliance.currentstat = "".
  end.
 /* This will use the screen-value of the filters which is ALL */
 run filterData in this-procedure. 
 
 /* Makes widget enable-disable based on the data */ 
 if query brwComplianceRpt:num-results > 0 
  then
   assign 
         browse brwComplianceRpt:sensitive = true
         btExport:sensitive  = true
         tglgreen:sensitive  = true
         tglred:sensitive    = true
         tglyellow:sensitive = true
         tglna:sensitive     = true             
         .  
 else
   assign 
         browse brwComplianceRpt:sensitive = false
         btExport:sensitive  = false
         tglgreen:sensitive  = false
         tglred:sensitive    = false
         tglyellow:sensitive = false
         tglna:sensitive     = false
         .
   
  /* Display no. of records with date and time on status bar */
  setStatusRecords(query brwComplianceRpt:num-results).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openDialog C-Win 
PROCEDURE openDialog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&FRAME-NAME}:
  end.
  
  define variable cPrevOrgID as character              no-undo.
  define variable cOrgID     as character              no-undo.
  define variable cName      as character              no-undo.
  
  if fOrganization:input-value <> ""
   then
    cPrevOrgID = fOrganization:private-data. 
    
  run dialogorganizationlookup.w(input  cPrevOrgID,
                                 input  false,        /* True if add "New" */
                                 input  false,        /* True if add "Blank" */
                                 input  true,         /* True if add "ALL" */
                                 output cOrgID,
                                 output cName,
                                 output std-lo).
   
  if not std-lo 
   then
    return no-apply.

  if cName = {&ALL}
   then
    do:
      fOrganization:screen-value = cName.
      fOrganization:private-data = cName.
    end.
  else
   do:
     fOrganization:screen-value = if cOrgID = "" then "" else cName.
     fOrganization:private-data = if cOrgID = "" then "" else cOrgID.
   end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if {&window-name}:window-state eq window-minimized 
   then
    {&window-name}:window-state = window-normal .

  {&window-name}:move-to-top().
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  assign 
        frame {&frame-name}:width-pixels          = {&window-name}:width-pixels
        frame {&frame-name}:virtual-width-pixels  = {&window-name}:width-pixels
        frame {&frame-name}:height-pixels         = {&window-name}:height-pixels
        frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels         
        {&browse-name}:width-pixels               = frame {&frame-name}:width-pixels - 11        
        {&browse-name}:height-pixels              = frame {&frame-name}:height-pixels - {&browse-name}:y - 3
        .

  {lib/resize-column.i &col="'name'" &var=dColumnWidth}

  run ShowScrollBars(browse brwComplianceRpt:handle, no, yes).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStat C-Win 
FUNCTION getComStat returns character PRIVATE
  ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cstat = "1" 
   then 
    return {&Gcode}.
  else if cstat = "2" 
   then 
    return {&Ycode}.
  else if cstat = "3" 
   then 
    return {&Rcode}.
  else 
   return "N".
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getComStatDesc C-Win 
FUNCTION getComStatDesc returns character
  ( input cStat as character) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cstat = {&Gcode} 
   then 
    return {&Green}.
  else if cstat = {&Ycode} 
   then 
    return {&Yellow}.
  else if cstat = {&Rcode} 
   then 
    return {&Red}.
  else
   return "N/A".
   
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resetScreen C-Win 
FUNCTION resetScreen RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
  do with frame {&frame-name}:
  end.
  
  assign 
      tglgreen:checked      = false      
      tglred:checked        = false  
      tglyellow:checked     = false  
      tglna:checked         = false  
      btExport:sensitive    = false
      tglgreen:sensitive    = false
      tglred:sensitive      = false
      tglyellow:sensitive   = false
      tglna:sensitive       = false
      .
  
  setStatusMessage("").

  return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resultsChanged C-Win 
FUNCTION resultsChanged RETURNS LOGICAL PRIVATE
  ( pValid as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 setStatusMessage({&ResultNotMatch}).
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

