&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File:dialogneworganization.w 

  Description:Dialog to create new Organization record. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author:Shubham 

  Created:03.04.2020
  @Modified
  Date         Name            Description
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Temp-table Definitions ---                                           */
{tt/organization.i}
{tt/organization.i &tableAlias=tOrganization}

/* Parameters Definitions ---                                           */
define output parameter opcOrgId    as character no-undo.
define output parameter oplSuccess  as LOGICAL   no-undo.

/* Local Variable Definitions ---                                       */

define variable cValuePair    as character no-undo.

/* Library Files  */
{lib/com-def.i}
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fName fNPN fAltaUID fAddress fAddress2 fCity ~
cbState fZip fPhone fFax fMobile fEmail fWebsite btnCancel 
&Scoped-Define DISPLAYED-OBJECTS fName fNPN fAltaUID fAddress fAddress2 ~
fCity cbState fZip fPhone fFax fMobile fEmail fWebsite 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON btnCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14.

DEFINE BUTTON btnOK AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10.2 BY 1 NO-UNDO.

DEFINE VARIABLE fAddress AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 71.2 BY 1 NO-UNDO.

DEFINE VARIABLE fAddress2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN NATIVE 
     SIZE 71.2 BY 1 NO-UNDO.

DEFINE VARIABLE fAltaUID AS CHARACTER FORMAT "X(256)":U 
     LABEL "ALTA UID" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fCity AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 46.2 BY 1 NO-UNDO.

DEFINE VARIABLE fEmail AS CHARACTER FORMAT "X(256)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 71.2 BY 1 NO-UNDO.

DEFINE VARIABLE fFax AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 NO-UNDO.

DEFINE VARIABLE fMobile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Mobile" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 46.2 BY 1 NO-UNDO.

DEFINE VARIABLE fName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 71.2 BY 1 NO-UNDO.

DEFINE VARIABLE fNPN AS CHARACTER FORMAT "X(256)":U 
     LABEL "NPN" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fPhone AS CHARACTER FORMAT "X(256)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 46.2 BY 1 NO-UNDO.

DEFINE VARIABLE fWebsite AS CHARACTER FORMAT "X(256)":U 
     LABEL "Website" 
     VIEW-AS FILL-IN 
     SIZE 71.2 BY 1 NO-UNDO.

DEFINE VARIABLE fZip AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 13.6 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fName AT ROW 1.33 COL 9 COLON-ALIGNED WIDGET-ID 214
     fNPN AT ROW 2.52 COL 9 COLON-ALIGNED WIDGET-ID 300
     fAltaUID AT ROW 2.52 COL 55.2 COLON-ALIGNED WIDGET-ID 302
     fAddress AT ROW 3.71 COL 9 COLON-ALIGNED WIDGET-ID 216
     fAddress2 AT ROW 4.91 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 236
     fCity AT ROW 6.1 COL 9 COLON-ALIGNED NO-LABEL WIDGET-ID 230
     cbState AT ROW 6.1 COL 55.8 COLON-ALIGNED NO-LABEL WIDGET-ID 298
     fZip AT ROW 6.1 COL 66.6 COLON-ALIGNED NO-LABEL WIDGET-ID 240
     fPhone AT ROW 7.29 COL 9 COLON-ALIGNED WIDGET-ID 222
     fFax AT ROW 7.29 COL 66.6 COLON-ALIGNED WIDGET-ID 296
     fMobile AT ROW 8.48 COL 9 COLON-ALIGNED WIDGET-ID 238
     fEmail AT ROW 9.67 COL 9 COLON-ALIGNED WIDGET-ID 218
     fWebsite AT ROW 10.86 COL 9 COLON-ALIGNED WIDGET-ID 220
     btnOK AT ROW 12 COL 26 WIDGET-ID 294
     btnCancel AT ROW 12 COL 42.6 WIDGET-ID 292
     SPACE(24.99) SKIP(0.28)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Organization"
         DEFAULT-BUTTON btnOK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON btnOK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Organization */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnCancel Dialog-Frame
ON CHOOSE OF btnCancel IN FRAME Dialog-Frame /* Cancel */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btnOK Dialog-Frame
ON CHOOSE OF btnOK IN FRAME Dialog-Frame /* Create */
do:
  run saveOragnization in this-procedure.
    
  if not oplSuccess 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fName Dialog-Frame
ON VALUE-CHANGED OF fName IN FRAME Dialog-Frame /* Name */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.


publish "GetStateIDList"(input ",",
                         output cValuePair,
                         output std-lo,
                         output std-ch).

if not std-lo 
 then
  do:
    message std-ch
      view-as alert-box error buttons ok.
    return.
  end.     

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  run enable_UI.

  cbstate:list-items   = trim(cValuePair).
  cbstate:screen-value = cbstate:entry(1).
  wait-for go of frame {&frame-name}.
  
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  BtnOK:sensitive = not(fName:input-value = "").

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fName fNPN fAltaUID fAddress fAddress2 fCity cbState fZip fPhone fFax 
          fMobile fEmail fWebsite 
      WITH FRAME Dialog-Frame.
  ENABLE fName fNPN fAltaUID fAddress fAddress2 fCity cbState fZip fPhone fFax 
         fMobile fEmail fWebsite btnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveOragnization Dialog-Frame 
PROCEDURE saveOragnization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  empty temp-table organization.
  create organization.
  assign
      organization.name    =  fName:input-value  
      organization.NPN     =  fNPN:input-value  
      organization.ALTAUID =  fAltaUID:input-value  
      organization.addr1   =  fAddress:input-value 
      organization.addr2   =  fAddress2:input-value 
      organization.city    =  fCity:input-value      
      organization.state   =  cbState:input-value 
      organization.zip     =  fZip:input-value 
      organization.email   =  fEmail:input-value 
      organization.website =  fWebsite:input-value 
      organization.fax     =  fFax:input-value 
      organization.phone   =  fPhone:input-value 
      organization.mobile  =  fMobile:input-value 
      organization.stat    =  {&InActiveStat}
      .

  for first organization:
    create tOrganization.
    buffer-copy organization to tOrganization no-error.
  end.

  publish "neworganization" (input table tOrganization,
                             output opcOrgId,
                             output std-lo,
                             output std-ch).

  /* If there is any error during the server call, then show error, set flag and return. */
  if not std-lo 
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      oplSuccess = false.
      return.
    end.
  oplSuccess = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

