&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogfirstpartyfulfillment.w 

  Description: Agent, Attorney or role specific first party requirement fulfillment 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Rahul Sharma 

  Created: 06/20/2018 
  @modified
  Date          Name           Description
  11/20/18      Gurvindar      Modified to change publish procedure names 
  08/09/2019    Gurvindar      Removed progress error while populating combo-box  
  01/05/2020    Shubham        Modifies for the new structure of organization.
  11/11/2021    Shefali        Task #88683 BugFix Don't let user to met the requirement
                               with blank requirement qualification and creates blank 
                               first party qualification 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

{tt/reqfulfill.i}
{tt/cmpfulfillments.i}
{tt/cmpfulfillments.i &tableAlias=tcmpfulfillments}

/* Parameters Definitions ---      */ 
define input    parameter iphData    as  handle      no-undo.
define input    parameter ipcEntity  as  character   no-undo.
define input    parameter table      for reqfulfill.
define output   parameter oplUpdate  as  logical     no-undo.

/* ***************************  Definitions  ************************** */
{lib/com-def.i}
{lib/std-def.i}

/* Local Variable Definitions ---  */
define variable cCurrUser         as character no-undo.
define variable iReviewPeriod     as integer   no-undo.
define variable deHeightWithBr    as decimal   no-undo.
define variable deHeightWithOutBr as decimal   no-undo.
define variable deBrowseRow       as decimal   no-undo.
define variable deSaveCancelRow   as decimal   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAffPersons

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES cmpfulfillments

/* Definitions for BROWSE brwAffPersons                                 */
&Scoped-define FIELDS-IN-QUERY-brwAffPersons cmpfulfillments.lSelect cmpfulfillments.stateReqQualID "StateReqQualID" cmpfulfillments.entityName "Name" cmpfulfillments.entityID "ID"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAffPersons cmpfulfillments.lSelect   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwAffPersons cmpfulfillments
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwAffPersons cmpfulfillments
&Scoped-define SELF-NAME brwAffPersons
&Scoped-define QUERY-STRING-brwAffPersons for each cmpfulfillments by cmpfulfillments.entityName
&Scoped-define OPEN-QUERY-brwAffPersons open query {&SELF-NAME} for each cmpfulfillments by cmpfulfillments.entityName.
&Scoped-define TABLES-IN-QUERY-brwAffPersons cmpfulfillments
&Scoped-define FIRST-TABLE-IN-QUERY-brwAffPersons cmpfulfillments


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAffPersons}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwAffPersons bCancel cbStatus Enotes ~
feffDate fexpDate fValidfor fNextRevDt 
&Scoped-Define DISPLAYED-OBJECTS flQualType flRequirement fqual cbStatus ~
fAuth Enotes feffDate fexpDate flReview flReviewBy fValidfor fNextRevDt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getreqfor Dialog-Frame 
FUNCTION getreqfor returns character ( cauth as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bSave AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Save".

DEFINE VARIABLE cbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE Enotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 70 BY 1.91 NO-UNDO.

DEFINE VARIABLE fAuth AS CHARACTER FORMAT "X(256)":U 
     LABEL "Authorized by" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 22.4 BY 1 NO-UNDO.

DEFINE VARIABLE feffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flQualType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Fulfilled By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE flRequirement AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 70 BY 1 NO-UNDO.

DEFINE VARIABLE flReview AS DATE FORMAT "99/99/99":U 
     LABEL "Review" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE flReviewBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Review By" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 28.8 BY 1 NO-UNDO.

DEFINE VARIABLE fNextRevDt AS DATE FORMAT "99/99/99":U 
     LABEL "Next Review" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fqual AS CHARACTER FORMAT "X(256)":U 
     LABEL "Qualification" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 70 BY 1 NO-UNDO.

DEFINE VARIABLE fValidfor AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Valid For Day(s)" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAffPersons FOR 
      cmpfulfillments SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAffPersons
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAffPersons Dialog-Frame _FREEFORM
  QUERY brwAffPersons DISPLAY
      cmpfulfillments.lSelect      column-label "Select"   view-as toggle-box 
cmpfulfillments.stateReqQualID  label        "StateReqQualID"   width 20
cmpfulfillments.entityName      label        "Name"           format "x(80)"  width 40
cmpfulfillments.entityID        label        "ID"             format "x(20)"  width 20
enable cmpfulfillments.lSelect
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 92.4 BY 10.81
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Qualification" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     brwAffPersons AT ROW 12.33 COL 3 WIDGET-ID 200
     bCancel AT ROW 23.33 COL 50.2 WIDGET-ID 204 NO-TAB-STOP 
     bSave AT ROW 23.33 COL 33.6 WIDGET-ID 208 NO-TAB-STOP 
     flQualType AT ROW 1.52 COL 19.8 COLON-ALIGNED WIDGET-ID 236
     flRequirement AT ROW 2.76 COL 19.8 COLON-ALIGNED WIDGET-ID 224
     fqual AT ROW 4 COL 19.8 COLON-ALIGNED WIDGET-ID 222
     cbStatus AT ROW 5.24 COL 19.8 COLON-ALIGNED WIDGET-ID 30
     fAuth AT ROW 5.24 COL 67.4 COLON-ALIGNED WIDGET-ID 42
     Enotes AT ROW 6.48 COL 21.8 NO-LABEL WIDGET-ID 34
     feffDate AT ROW 8.62 COL 19.8 COLON-ALIGNED WIDGET-ID 6
     fexpDate AT ROW 9.86 COL 19.8 COLON-ALIGNED WIDGET-ID 22
     flReview AT ROW 9.86 COL 74.8 COLON-ALIGNED WIDGET-ID 46
     flReviewBy AT ROW 8.62 COL 61 COLON-ALIGNED WIDGET-ID 48
     fValidfor AT ROW 11.1 COL 19.8 COLON-ALIGNED WIDGET-ID 16
     fNextRevDt AT ROW 11.1 COL 74.8 COLON-ALIGNED WIDGET-ID 50
     "Notes:" VIEW-AS TEXT
          SIZE 6.6 BY .62 AT ROW 6.38 COL 14.8 WIDGET-ID 36
     SPACE(75.99) SKIP(17.94)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "First Party Requirement Fulfillment"
         DEFAULT-BUTTON bSave WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAffPersons 1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwAffPersons:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAffPersons:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fAuth IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fAuth:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flQualType IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN flRequirement IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flRequirement:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flReview IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flReview:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN flReviewBy IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       flReviewBy:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fqual IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fqual:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAffPersons
/* Query rebuild information for BROWSE brwAffPersons
     _START_FREEFORM
open query {&SELF-NAME} for each cmpfulfillments by cmpfulfillments.entityName.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAffPersons */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* First Party Requirement Fulfillment */
do:
  oplUpdate = no.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAffPersons
&Scoped-define SELF-NAME brwAffPersons
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAffPersons Dialog-Frame
ON ROW-DISPLAY OF brwAffPersons IN FRAME Dialog-Frame /* Qualification */
do:
  {lib/brw-rowDisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:
  run validateFulfillment in this-procedure.
  
  if not oplUpdate
   then
    return no-apply.

  run saveFulfillment in this-procedure.   
  
  if not oplUpdate 
   then
     return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbStatus Dialog-Frame
ON VALUE-CHANGED OF cbStatus IN FRAME Dialog-Frame /* Status */
do:
  run setNextReviewDate in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fNextRevDt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNextRevDt Dialog-Frame
ON VALUE-CHANGED OF fNextRevDt IN FRAME Dialog-Frame /* Next Review */
do:
  run setValidForDays in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fValidfor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fValidfor Dialog-Frame
ON VALUE-CHANGED OF fValidfor IN FRAME Dialog-Frame /* Valid For Day(s) */
do:
  fNextRevDt:screen-value = string(today + fValidfor:input-value).
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */
{lib/brw-main.i}

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Get name of currently login user. */
publish "GetCredentialsName" (output cCurrUser).

/* Now enable the interface and wait for the exit condition.            */

/* Populate Qualifications status combobox. */
publish "GetSysPropListMinusID" ("COM", "Qualification", "Status", {&NeedsReview}, output std-ch).

if std-ch ne ""
 then
  cbStatus:list-item-pairs = replace(std-ch,"^",",").
else
 cbStatus:list-item-pairs = "," .
  
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  run enable_UI. 
  /* For the requirements fulfilled by persons or organization qualifications, we need to show the browse. the browse will not be diaplyed.
     Therefore, dynamically we need to change the height of the frame. */
  assign     
      deSaveCancelRow   = bSave:row
      deBrowseRow       = brwAffPersons:row
      deHeightWithBr    = frame Dialog-Frame:height-char
      deHeightWithOutBr = (frame Dialog-Frame:height-char - brwAffPersons:height).

  run displayFulfillment in this-procedure.
  
  /* Show/Hide browse. In case we need to show the browse, then 
     get the data from the server. */
  run showBrowse in this-procedure.
  
  /* Customized trigger to select/Deselect record in browse. */
  on value-changed of cmpfulfillments.lSelect in browse brwAffPersons
  do:
    run selectRecord      in this-procedure.
    run enableDisableSave in this-procedure.
  end.

  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayFulfillment Dialog-Frame 
PROCEDURE displayFulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  

  do with frame Dialog-Frame:
  end.
    
  find first reqfulfill no-error.
  if not available reqfulfill 
   then
    return.
  assign
      flQualType:screen-value    = getreqfor(reqfulfill.appliesTo)
      flRequirement:screen-value = reqfulfill.reqdesc
      fqual     :screen-value    = reqfulfill.qualification    
      cbStatus  :screen-value    = if cbStatus:list-item-pairs = ","  then "" else  {&GoodStanding}
      fAuth     :screen-value    = {&First-Party} /* First Party */    
      flReview  :screen-value    = string(today)
      flReviewBy:screen-value    = cCurrUser
    .
  apply "value-changed" to cbStatus.

  /* When expire type is expireDate then create expiration date from expireMonth ,expireDay and expireYear
     when expire type is expireDays then create expiration date from expiredays.
  */
  if reqfulfill.expire = {&ExpireDate} /* On a date */
   then
    std-dt = date(reqfulfill.expireMonth,reqfulfill.expireDay,year(today) + reqfulfill.expireYear) no-error.   
  
  else if reqfulfill.expire = {&ExpireDays} /* After number of days */
   then
    std-dt = reqfulfill.expireDays + today no-error.
  
  else
    std-dt = ?.
  
  assign 
      fexpDate:screen-value = string(std-dt)
      feffDate:screen-value = string(today)
      .       
   
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if (not can-find (first cmpfulfillments) or
     can-find (first cmpfulfillments where cmpfulfillments.lSelect)) and
     not (fqual:input-value = "" or fqual:input-value = ?)
   then
    bSave:sensitive = true.
  else 
   bSave:sensitive = false.
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY flQualType flRequirement fqual cbStatus fAuth Enotes feffDate fexpDate 
          flReview flReviewBy fValidfor fNextRevDt 
      WITH FRAME Dialog-Frame.
  ENABLE brwAffPersons bCancel cbStatus Enotes feffDate fexpDate fValidfor 
         fNextRevDt 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveFulfillment Dialog-Frame 
PROCEDURE saveFulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/    
  
  empty temp-table tcmpfulfillments.                          
   
  find first reqfulfill no-error.
  if not available reqfulfill 
   then
    return.

  create tcmpfulfillments.
  assign 
      tcmpfulfillments.entity             = reqfulfill.role
      tcmpfulfillments.entityID           = reqfulfill.entityID
      tcmpfulfillments.entityName         = reqfulfill.entityname
      tcmpfulfillments.StateId            = reqfulfill.stateID
      tcmpfulfillments.requirementID      = reqfulfill.requirementID
      tcmpfulfillments.stateReqQualID     = reqfulfill.statereqqualID
      tcmpfulfillments.qualificationID    = reqfulfill.qualificationID
      tcmpfulfillments.qualification      = reqfulfill.qualification  
      tcmpfulfillments.qualReqID          = reqfulfill.qualreqID
      tcmpfulfillments.authorizedBy       = {&CompanyCode}
      tcmpfulfillments.stat               = cbStatus:input-value in frame {&frame-name}
      tcmpfulfillments.effectiveDate      = feffDate:input-value
      tcmpfulfillments.expirationDate     = fexpDate:input-value
      tcmpfulfillments.notes              = Enotes:input-value
      tcmpfulfillments.reviewDate         = datetime(today, mtime)
      tcmpfulfillments.reviewBy           = cCurrUser
      tcmpfulfillments.numValidDays       = fValidfor:input-value
      tcmpfulfillments.nextreviewDueDate  = datetime(fNextRevDt:input-value,mtime)
      tcmpfulfillments.lselect            = true
      tcmpfulfillments.fulfillByID        = reqfulfill.entityID.
    
 
  if brwAffPersons:visible = true
   then
    do :
      find first cmpfulfillments where cmpfulfillments.lSelect = true  no-error.
      if not available cmpfulfillments
       then
        return.
      if ipcEntity = {&Organization} and reqfulfill.appliesto = {&personCode}
       then
        tcmpfulfillments.fulfillByID  = cmpfulfillments.entityID.
      else if ipcEntity = {&Person} and reqfulfill.appliesto = {&OrganizationCode}
       then
        tcmpfulfillments.fulfillByID = cmpfulfillments.entityID.
    end.
     
  /* Create new fulfillment record and update datasrv. */
  run newFirstPartyFulfillment in iphData (input table tcmpfulfillments,
                                           output oplUpdate,
                                           output std-ch ).
  if not oplUpdate 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
  
  oplUpdate = true.  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectRecord Dialog-Frame 
PROCEDURE selectRecord :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  if not available cmpfulfillments
   then
    return.

  std-ro = rowid(cmpfulfillments).
   
  for each cmpfulfillments:
    cmpfulfillments.lSelect = if (rowid(cmpfulfillments) = std-ro) 
                               then not(cmpfulfillments.lSelect)
                              else false.
  end.

  browse brwAffPersons:refresh().

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNextReviewDate Dialog-Frame 
PROCEDURE setNextReviewDate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame Dialog-Frame:
  end.

  /* Set the initial value */
  fNextRevDt:screen-value = "".

  if cbStatus:input-value = {&goodStanding} 
   then
    do:
      
      /* Get the default value of review period set in the qualifications record. */
      publish "getQualReviewPeriod" (input {&Qualification} , 
                                     input fqual:input-value,
                                     output std-in,
                                     output std-lo,
                                     output std-ch).
      if not std-lo
       then
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end.

      if std-in <> 0
       then
        assign
            iReviewPeriod = std-in /* This Variable is later used in validateFulfillment IP */
            fNextRevDt:screen-value = string(today + iReviewPeriod)
            .
    end.

  apply "value-changed" to fNextRevDt.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setValidForDays Dialog-Frame 
PROCEDURE setValidForDays :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  do with frame Dialog-Frame:
  end.

  std-da = fNextRevDt:input-value no-error.  

  fValidfor:screen-value = if (error-status:error or std-da = ?)
                             then "0"
                           else
                             string(fNextRevDt:input-value - today).                           

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showBrowse Dialog-Frame 
PROCEDURE showBrowse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  empty temp-table cmpfulfillments.
  
  
  if not available reqfulfill 
   then
    return.
  
  /* Show browse when Organization or person requirement  is fulfilled by person or organization. */
  if ipcEntity = {&Organization} and reqfulfill.appliesTo = {&PersonCode}   
   then
    do:
      /* Get cmpfulfillments records from organizationdatasrv */
      run getFirstPartyFulfillment in iphData (input  reqfulfill.statereqqualID,
                                               output table cmpfulfillments,
                                               output std-lo,
                                               output std-ch).
      if not std-lo
       then
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end.
      assign
          brwAffPersons:title       = {&Person}
          frame Dialog-Frame:height = deHeightWithBr
          bSave:row                 = deSaveCancelRow
          bCancel:row               = deSaveCancelRow
          brwAffPersons:visible     = true
          .
      for first cmpfulfillments where cmpfulfillments.entityName = reqfulfill.entityname :
        cmpfulfillments.lSelect = true.
      end.
       
      open query brwAffPersons for each cmpfulfillments by cmpfulfillments.personname.      
    end.
  else if ipcEntity = {&Person} and reqfulfill.appliesTo = {&OrganizationCode} 
   then
    do:
      /* Get cmpfulfillments records from organizationdatasrv */
      run getFirstPartyFulfillment in iphData (input  reqfulfill.statereqqualID,
                                               output table cmpfulfillments,
                                               output std-lo,
                                               output std-ch).
      if not std-lo
       then
        do:
          message std-ch
            view-as alert-box error buttons ok.
          return.
        end.
      
      assign
          brwAffPersons:title       = {&Organization}
          frame Dialog-Frame:height = deHeightWithBr
          bSave:row                 = deSaveCancelRow
          bCancel:row               = deSaveCancelRow
          brwAffPersons:visible     = true
          .
      
      for first cmpfulfillments where cmpfulfillments.entityName = reqfulfill.entityname :
        cmpfulfillments.lSelect = true.
      end.

      open query brwAffPersons for each cmpfulfillments by cmpfulfillments.entity.      
    end.
    else
     assign     
         brwAffPersons:visible     = false
         bSave:row                 = deBrowseRow
         bCancel:row               = deBrowseRow
         frame Dialog-Frame:height = deHeightWithOutBr.
   
  run enableDisablesave in this-procedure.
  
  if brwAffPersons:visible and (query brwAffPersons:num-results  eq 0 or query brwAffPersons:num-results  eq ?) 
   then
    bSave:sensitive = false.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i}
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateFulfillment Dialog-Frame 
PROCEDURE validateFulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  do with frame Dialog-Frame:
  end.

  /* Effective date cannot be blank. */
  if feffDate:input-value = ? 
   then
    do:
      message "Effective date cannot be blank."
        view-as alert-box info buttons ok.

      feffDate:screen-value = string(today ).
      oplUpdate = false.
      return.
    end.

  /* expiration date should not be less then Effective date. */
  if fexpDate:input-value < feffDate:input-value 
   then
    do:
      message "Expiration date cannot be before qualification effective date."
        view-as alert-box info buttons ok.

      fexpDate:screen-value = "".
      oplUpdate = false.
      return.
    end.

  if cbStatus:input-value = {&GoodStanding} and 
     iReviewPeriod <> 0                     and 
     fNextRevDt:input-value = ? 
   then
    do:  
      message "Next Review date cannot be blank."
        view-as alert-box info buttons ok.

      fNextRevDt:screen-value = string(today + iReviewPeriod).
      oplUpdate = false.
      apply "value-changed" to fNextRevDt. 
      return.
    end. /* if fNextRevDt:screen-value = "" or ...*/

  /* New review date should not be greater than expiration date. */
  if not fexpDate:input-value = ? and 
    fNextRevDt:input-value > fexpDate:input-value 
   then
    do:
      message "New Review Due Date cannot be after the qualification expiration date."
        view-as alert-box info buttons ok.

      if (fexpDate:input-value - 14) < today then
        fNextRevDt:screen-value = string(today + 1). 
      else
       fNextRevDt:screen-value = string(fexpDate:input-value - 14). 

      apply "value-changed" to fNextRevDt.
      oplUpdate = false.
      return.
    end. /* if not (date(fexpDate:screen-value) = ?) and...*/      
  
  /* New review date should be greater than effective date. */
  if not feffDate:input-value = ? and 
    fNextRevDt:input-value < feffDate:input-value 
   then
    do:
      message "Next Review date cannot be less than effective date"
        view-as alert-box info buttons ok.
      oplUpdate = false.
      return.
    end. /* if date(fNextRevDt:screen-value) < today then */

  find first cmpfulfillments no-error.
  if available cmpfulfillments 
   then
    do:
      if flQualType:input-value = {&Person} and reqfulfill.appliesTo = {&OrganizationCode}  
       then
        do:
          if not can-find (first cmpfulfillments where cmpfulfillments.lselect)
           then
            do:
              message "Please select at least one organization role for requirement fulfillment"
                view-as alert-box info buttons ok.
              oplUpdate = false.
              return.
            end.
        end.
     
      if flQualType:input-value = {&Organization} and reqfulfill.appliesTo = {&PersonCode}
       then
        do:
          if not can-find (first cmpfulfillments where cmpfulfillments.lselect)
           then
            do:
              message "Please select at least one person role for requirement fulfillment"
                view-as alert-box info buttons ok.
              oplUpdate = false.
              return.
            end.
        end.
    end.
  oplUpdate = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getreqfor Dialog-Frame 
FUNCTION getreqfor returns character ( cauth as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cauth = {&OrganizationCode}
   then
    return {&Organization}.   /* Function return value. */
  else if cauth = {&personcode}
   then
    return {&Person}.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

