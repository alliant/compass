&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:dialogaddrole.w

  Description:Dialog to create relation between Person and Role. 

  Input Parameters:
      <none>

  Output Parameters:
      <none>
      
  Author:Rahul Sharma

  Created:04.26.2018 
  @Modified
  Date         Name            Description
  04/11/19     Rahul           Add mandatory flag '*' in state and role fillIn  
  29/11/19     Shubham         Added code to delete Attorney from the personrole lsit
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* --Temp-Table Definitions ---                                         */
{tt/personrole.i}

/* --Parameters Definitions ---                                         */
define input  parameter iphPersondata       as handle    no-undo.
define input  parameter ipcPersonID         as character no-undo.
define input  parameter ipcPersonName       as character no-undo.
define input  parameter table               for personRole.
define output parameter opiPersonRoleID     as integer   no-undo.
define output parameter oplSuccess          as logical   no-undo.

{lib/std-def.i}
{lib/com-def.i}

/* --Local Variable Definitions ---                                     */
define variable cList         as character no-undo.
define variable cValuePair    as character no-undo. 
define variable cNewList      as character no-undo.
define variable iCount        as integer no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbState cbRole fieffDate fiexpDate bCancel 
&Scoped-Define DISPLAYED-OBJECTS fiName cbState fMarkMandatory1 cbRole ~
fMarkMandatory2 tActive fieffDate fiexpDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Cancel".

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14 TOOLTIP "Create".

DEFINE VARIABLE cbRole AS CHARACTER FORMAT "X(256)":U 
     LABEL "Role" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE fieffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiexpDate AS DATE FORMAT "99/99/99":U 
     LABEL "Expiration" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 12.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fMarkMandatory1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE fMarkMandatory2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 3 BY .62
     FGCOLOR 12 FONT 10 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.6 BY .67 TOOLTIP "Status is always active" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fiName AT ROW 1.38 COL 12 COLON-ALIGNED WIDGET-ID 50
     cbState AT ROW 2.57 COL 12 COLON-ALIGNED WIDGET-ID 40
     fMarkMandatory1 AT ROW 2.81 COL 22.8 COLON-ALIGNED NO-LABEL WIDGET-ID 52
     cbRole AT ROW 3.76 COL 8.4 WIDGET-ID 30
     fMarkMandatory2 AT ROW 4 COL 42 COLON-ALIGNED NO-LABEL WIDGET-ID 54
     tActive AT ROW 4.95 COL 14 WIDGET-ID 310
     fieffDate AT ROW 5.76 COL 4.4 WIDGET-ID 32
     fiexpDate AT ROW 6.95 COL 12 COLON-ALIGNED WIDGET-ID 34
     bSave AT ROW 8.1 COL 10
     bCancel AT ROW 8.1 COL 26.6
     SPACE(10.19) SKIP(0.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Role"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbRole IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fieffDate IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fiName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       fiName:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN fMarkMandatory1 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMarkMandatory2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tActive IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Role */
do:
  oplSuccess = false.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplSuccess = false.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Create */
do:
  if fiexpDate:input-value < fieffDate:input-value 
   then
    do:
      message "Expiration Date cannot be before Effective Date" 
          view-as alert-box info buttons ok.
      
      apply "entry" to fiexpDate.
      return no-apply.
    end.

  run savePersonRole in this-procedure.

  /* Variable oplSuccess gets updated in savepersonRole procedure. */
  if not oplSuccess 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRole
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRole Dialog-Frame
ON VALUE-CHANGED OF cbRole IN FRAME Dialog-Frame /* Role */
do:
  /* To delete "Select Role" from list */
  {lib/modifylist.i &cbhandle=cbRole:handle &list=list-items &delimiter = "," &item = '{&SelectRole}'}

  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState Dialog-Frame
ON VALUE-CHANGED OF cbState IN FRAME Dialog-Frame /* State */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fieffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fieffDate Dialog-Frame
ON VALUE-CHANGED OF fieffDate IN FRAME Dialog-Frame /* Effective */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiexpDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiexpDate Dialog-Frame
ON VALUE-CHANGED OF fiexpDate IN FRAME Dialog-Frame /* Expiration */
do:
  run enableDisableSave in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tActive
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActive Dialog-Frame
ON VALUE-CHANGED OF tActive IN FRAME Dialog-Frame /* Active */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  publish "GetStateIDList"(input ",",
                           output cValuePair,
                           output std-lo,
                           output std-ch).                         
  if not std-lo 
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.
    
  cbstate:list-items = {&National} + "," + cValuePair.

  publish "getComplianceCodesList"(input {&PersonRole},
                                   input  "," ,
                                   output cList,
                                   output std-lo,
                                   output std-ch).
  if not std-lo
   then
    do:
      message std-ch 
        view-as alert-box error buttons ok.
      return.
    end.
  
  run enable_UI.
  
  run displayData       in this-procedure.
  run setWidgetState    in this-procedure.
  run enableDisableSave in this-procedure.
  
  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayData Dialog-Frame 
PROCEDURE displayData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  fiName:screen-value = ipcPersonName.
 
  find first personRole no-error.

  if available personRole 
   then
    do:
      cbRole:list-items in frame {&frame-name} = {&SelectRole} + "," + cList.
      cbRole:screen-value = {&SelectRole}.    
  
      assign 
          frame Dialog-Frame:title          = "Modify Person Role"
          bSave             :label          = "Save"
          bSave             :tooltip        = "Save"
          cbState           :screen-value   = personRole.stateID
          cbRole            :screen-value   = personRole.role
          fieffDate         :screen-value   = string(personRole.effectivedate)
          fiexpDate         :screen-value   = string(personRole.expirationDate)
          fMarkMandatory1   :hidden         = true
          fMarkMandatory2   :hidden         = true
          tActive           :tooltip        = if personrole.role = {&Attorney} then "Status is based on attorney status" else "Status is always active"
          tActive           :checked        = personrole.active
          .
    end.  /* if available personRole */
  else 
   do:
     do iCount = 1 to num-entries(cList):
       if entry(iCount,cList) <> "Attorney" 
        then
         cNewList = if cNewList = "" then entry(iCount,cList) else cNewList + "," + entry(iCount,cList). 
     end. /* do iCount = 1 to num-entries(cList): */
     cbRole:list-items in frame {&frame-name} = {&SelectRole} + "," + cNewList.
     cbRole:screen-value = {&SelectRole}.  
     cbstate:screen-value = cbstate:entry(1).
     
     assign
         frame Dialog-Frame:title          = "New Person Role"
         bSave             :label          = "Create"  
         bSave             :tooltip        = "Create" 
         fMarkMandatory1   :screen-value   = {&mandatory}
         fMarkMandatory1   :hidden         = false
         fMarkMandatory2   :screen-value   = {&mandatory}
         fMarkMandatory2   :hidden         = false
         tActive           :checked        = true
         .  
   end. /* else */
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first personRole no-error.
  
  /* Edit: enable save, if user changes anything. */
  if available personRole  and 
     personRole.personRoleID ne 0
   then
    bSave:sensitive = not(fieffDate:input-value = personRole.effectivedate and fiexpDate:input-value = personRole.expirationDate) no-error. 
    
    
  /* New: enable save, mandatory fields (state and role) is filled. */
  else
   bSave:sensitive = not((cbState:input-value = {&SelectState} or cbState:input-value = "" or cbState:input-value = ?) or 
                         (cbRole:input-value = {&SelectRole} or cbRole:input-value = "" or cbRole:input-value = ?)).
  
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fiName cbState fMarkMandatory1 cbRole fMarkMandatory2 tActive 
          fieffDate fiexpDate 
      WITH FRAME Dialog-Frame.
  ENABLE cbState cbRole fieffDate fiexpDate bCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE savePersonRole Dialog-Frame 
PROCEDURE savePersonRole :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  find first personRole no-error.  
  if not available personRole
   then
    do:
      create personRole. 
      personRole.PersonID = ipcPersonID. 
    end.

  assign
      personRole.stateID        = cbState  :input-value
      personRole.role           = cbRole   :input-value
      personRole.effectivedate  = fieffDate:input-value
      personRole.expirationDate = fiexpDate:input-value 
      personRole.roleID         = if personRole.role = {&Attorney} then personRole.roleID else ipcPersonID          
      personRole.active         = logical(tActive:screen-value)
      no-error .

  if not valid-handle(iphPersondata) 
   then
    do:
      message "Data Model not found."
          view-as alert-box error buttons ok.
      return.
    end.

  if personRole.personRoleID = 0
   then   
    run newPersonRole in iphPersondata (input table personRole,
                                        output opiPersonRoleID,
                                        output oplSuccess,
                                        output std-ch).
  else 
    run modifyPersonRole in iphPersondata (input table personRole,
                                           output oplSuccess,
                                           output std-ch).
  if not oplSuccess 
   then
    message std-ch
      view-as alert-box error buttons ok.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState Dialog-Frame 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if can-find(first personRole)
   then
    assign 
        cbState:sensitive = false
        cbRole:sensitive  = false
        .
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

