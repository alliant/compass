&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogThirdPartyFulfillment.w

  Description: Create/Delete fulfillment records for third party requirements.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  @modified
  Date            Name                 Description
  11/21/2018      Gurvindar            Changed IP calls for get,new fulfillment
  04/02/2020      Archana Gupta        Modified code according to new organization structure
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/


/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
 define input  parameter iphData           as handle    no-undo.
 define input  parameter ipireqID          as integer   no-undo.
 define input  parameter ipcreqDesc        as character no-undo.
 define input  parameter ipcEntity         as character no-undo.
 define input  parameter ipcEntityId       as character no-undo.
 define input  parameter ipcRoleType       as character no-undo. 
 define input  parameter ipcStateID        as character no-undo.
 define input  parameter ipcFulfilledBy    as character no-undo.
 define input  parameter ipiStateReqQualid as integer   no-undo.
 define output parameter oplChange         as logical   no-undo.

/* Local Variable Definitions ---                                       */
{tt/fulfillment.i &tablealias="tfulfillment"}
{tt/qualification.i}
{tt/qualification.i &tablealias="tqualification" } 

{lib/std-def.i}
{lib/com-def.i}
{lib/winlaunch.i}

define variable chQualType    as character no-undo.
define variable hColumn       as handle    no-undo.
define variable hColumn1      as handle    no-undo.
define variable icount        as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwQualification

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tqualification

/* Definitions for BROWSE brwQualification                              */
&Scoped-define FIELDS-IN-QUERY-brwQualification tqualification.qualmet tqualification.active tqualification.entityID tqualification.name tqualification.qualification string(tqualification.qualificationNumber) tqualification.effectiveDate tqualification.expirationDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwQualification tqualification.qualmet   
&Scoped-define ENABLED-TABLES-IN-QUERY-brwQualification tqualification
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-brwQualification tqualification
&Scoped-define SELF-NAME brwQualification
&Scoped-define QUERY-STRING-brwQualification preselect each tqualification
&Scoped-define OPEN-QUERY-brwQualification open query {&SELF-NAME} preselect each tqualification.
&Scoped-define TABLES-IN-QUERY-brwQualification tqualification
&Scoped-define FIRST-TABLE-IN-QUERY-brwQualification tqualification


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwQualification}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bCancel rQualType brwQualification 
&Scoped-Define DISPLAYED-OBJECTS frequirement rQualType 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getauth Dialog-Frame 
FUNCTION getauth returns character ( cAuth as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY DEFAULT  NO-FOCUS
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Add Agent Qualification".

DEFINE BUTTON bSave AUTO-GO DEFAULT 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Add Agent Qualification".

DEFINE VARIABLE frequirement AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requirement" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.8 BY 1 NO-UNDO.

DEFINE VARIABLE rQualType AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Organization", "O",
"Person", "P"
     SIZE 27.4 BY .62 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwQualification FOR 
      tqualification SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwQualification Dialog-Frame _FREEFORM
  QUERY brwQualification DISPLAY
      tqualification.qualmet                                column-label "Select" view-as toggle-box
tqualification.active                                       column-label "Active" view-as toggle-box 
tqualification.entityID                                            label "Entity ID"       format "x(20)"  width 15
tqualification.name                                                label "Entity Name"     format "x(60)"  width 26
tqualification.qualification                                       label "Qualification"   format "x(60)"  width 36
string(tqualification.qualificationNumber)                         label "Number"          format "x(12)"  width 12
tqualification.effectiveDate                                       label "Effective"       format 99/99/99 width 11
tqualification.expirationDate                                      label "Expiration"      format 99/99/99 width 11
enable tqualification.qualmet
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 133 BY 9.81
         BGCOLOR 15  ROW-HEIGHT-CHARS .71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bCancel AT ROW 13.48 COL 70.2 WIDGET-ID 220 NO-TAB-STOP 
     frequirement AT ROW 1.19 COL 14 COLON-ALIGNED WIDGET-ID 228
     rQualType AT ROW 2.48 COL 26.4 NO-LABEL WIDGET-ID 222
     brwQualification AT ROW 3.52 COL 2.6 WIDGET-ID 200
     bSave AT ROW 13.48 COL 53.6 WIDGET-ID 218 NO-TAB-STOP 
     "Show Qualifications of :" VIEW-AS TEXT
          SIZE 23.8 BY .62 AT ROW 2.48 COL 2.6 WIDGET-ID 226
     SPACE(110.09) SKIP(11.65)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Third Party Requirement Fulfillments"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bSave WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwQualification rQualType Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwQualification:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwQualification:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON bSave IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN frequirement IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwQualification
/* Query rebuild information for BROWSE brwQualification
     _START_FREEFORM
open query {&SELF-NAME} preselect each tqualification.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwQualification */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Third Party Requirement Fulfillments */
do:
  oplChange = yes.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel Dialog-Frame
ON CHOOSE OF bCancel IN FRAME Dialog-Frame /* Cancel */
do:
  oplChange = no.
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwQualification
&Scoped-define SELF-NAME brwQualification
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwQualification Dialog-Frame
ON ROW-DISPLAY OF brwQualification IN FRAME Dialog-Frame
do:
  {lib/brw-rowdisplay.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave Dialog-Frame
ON CHOOSE OF bSave IN FRAME Dialog-Frame /* Save */
do:
  run saveFulfillment in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rQualType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rQualType Dialog-Frame
ON VALUE-CHANGED OF rQualType IN FRAME Dialog-Frame
do:
  run actionValueChanged in this-procedure.
  run enableDisableSave  in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&frame-name}:parent eq ? 
 then
  frame {&frame-name}:parent = active-window.

/* Get fulfilment data from server to data modal */
run getQualification in this-procedure.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  run enable_UI.

  frequirement:screen-value in frame Dialog-Frame = ipcreqDesc.

  run displayQualification in this-procedure.
  
  /* If a record gets selected or unselected in the browse, then Enable/Disable
     save button. */
  on value-changed of tqualification.qualmet in browse brwQualification
  do:
    /* Select only one qualification for one statereqQual */ 
   run selectRecord in this-procedure.
   run enableDisableSave in this-procedure.
  end.

  wait-for go of frame {&frame-name}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionValueChanged Dialog-Frame 
PROCEDURE actionValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tqualification  for tqualification.
  define buffer qualification   for qualification.

  $BLOCK$:
  /* On value change of radio button, Check if record has been changed
     by users in the browse. */
  
  for each qualification:
    for first tqualification
      where tqualification.entity          =  qualification.entity 
        and tqualification.entityID        =  qualification.entityId
        and tqualification.qualificationID =  qualification.qualificationID
        and tqualification.qualmet         <> qualification.qualmet:

      message "Unsaved changes exist. Do you want to continue?"
        view-as alert-box question buttons yes-no update lChoice as logical.
  
      if not lChoice 
       then
        do:
          rQualType:screen-value in frame Dialog-Frame = chQualType.
          return.
        end.
      else
       leave $BLOCK$.
    end.
  end.
 
  run displayQualification in this-procedure.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayQualification Dialog-Frame 
PROCEDURE displayQualification :
/*------------------------------------------------------------------------------
      Purpose:     
      Parameters:  <none>
      Notes:       
------------------------------------------------------------------------------*/
  do with frame Dialog-Frame:
  end.
  
  close query brwqualification.
  empty temp-table tqualification.
  
  /* Display data in browse depending on radio buttons */
  if ipcRoleType = {&Organization}  /* Organizatoin*/
   then
    do:
      if  rQualType:screen-value = {&OrganizationCode} 
       then
        do:
          assign 
              hColumn  = brwqualification:get-browse-column(4)
              hColumn1 = brwqualification:get-browse-column(3).
          
          if valid-handle(hColumn) and valid-handle(hColumn1) 
           then
            assign
                hColumn:label  = "Name"
                hColumn1:label = "ID"
                .
        
          chQualType = rQualType:screen-value.
          for each qualification where qualification.entity = {&Organizationcode} and qualification.authorizedby = {&Thirdparty}:
            create tqualification.  
            buffer-copy qualification to tqualification.
          end.
        end.
      else /*person*/ 
       do:
          assign 
              hColumn  = brwqualification:get-browse-column(4)
              hColumn1 = brwqualification:get-browse-column(3)
              .
         
          if valid-handle(hColumn) and valid-handle(hColumn1) 
           then
            assign
                hColumn:label    = "Name"
                hColumn1:label   = "ID"
                hColumn:visible  = true
                hColumn1:visible = true
                .     
         chQualType = rQualType:screen-value.
         for each qualification where qualification.entity = {&Personcode} and qualification.authorizedby = {&Thirdparty} :
           create tqualification.  
           buffer-copy qualification to tqualification.
         end.
       end.
       
       open query brwqualification preselect each tqualification.
    end.

  else 
   do:
     if  rQualType:screen-value = {&OrganizationCode} 
      then
       do:
         assign 
             hColumn  = brwqualification:get-browse-column(4)
             hColumn1 = brwqualification:get-browse-column(3).
         
         if valid-handle(hColumn) and valid-handle(hColumn1) 
          then
           assign
               hColumn:label  = "Name"
               hColumn1:label = "ID"
               hColumn:visible  = true
               hColumn1:visible = true
               .
               
         chQualType = rQualType:screen-value.
         for each qualification where qualification.entity = {&Organizationcode} and qualification.authorizedby = {&Thirdparty}:
           create tqualification.  
           buffer-copy qualification to tqualification.
         end.
       end.
     else /*person*/ 
      do:
        assign 
            hColumn  = brwqualification:get-browse-column(4)
            hColumn1 = brwqualification:get-browse-column(3)
            .
        
        if valid-handle(hColumn) and valid-handle(hColumn1) 
         then
          assign
              hColumn:label    = "Name"
              hColumn1:label   = "ID"
              hColumn:visible  = true
              hColumn1:visible = true
              .     
        
        chQualType = rQualType:screen-value.
        for each qualification where qualification.entity = {&Personcode} and qualification.authorizedby = {&Thirdparty}:
          create tqualification.  
          buffer-copy qualification to tqualification.
        end.
      end.
   
   open query brwqualification preselect each tqualification.
   end.   

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave Dialog-Frame 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define buffer tqualification for tqualification.
  define buffer qualification  for qualification.

  std-lo = false.
  QUALBLK:
  for each tqualification:
    if can-find(first qualification 
                where qualification.entity          = tqualification.entity
                  and qualification.entityId        = tqualification.entityID
                  and qualification.qualificationID = tqualification.qualificationID
                  and qualification.qualmet         <> tqualification.qualmet)
     then
      do:
        std-lo = true.
        leave QUALBLK.
      end.   
   
  end.
  bSave:sensitive = std-lo.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY frequirement rQualType 
      WITH FRAME Dialog-Frame.
  ENABLE bCancel rQualType brwQualification 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getQualification Dialog-Frame 
PROCEDURE getQualification :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer qualification for qualification.
 
  /* Get Qualification data from server. */
  run getThirdPartyFulfillments in iphData (input ipireqID,               /* RequirementId */
                                            input ipcEntity,  
                                            input ipcEntityId,
                                            input ipcRoleType,
                                            input ipcStateID,
                                            input ipcFulfilledBy,
                                            input ipiStateReqQualid,
                                            output table qualification,
                                            output std-lo,
                                            output std-ch).

  if not std-lo
   then
    do:
      message std-ch
          view-as alert-box error buttons ok.
      return.
    end.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveFulfillment Dialog-Frame 
PROCEDURE saveFulfillment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer tqualification  for tqualification.
  define buffer qualification   for qualification.

  empty temp-table tfulfillment.

  /* Check if Qualification table contains the user changes. */
  for each qualification:
    for first tqualification
        where tqualification.entity          =  qualification.entity 
          and tqualification.entityID        =  qualification.entityId
          and tqualification.qualificationID =  qualification.qualificationID
          and tqualification.qualmet         <> qualification.qualmet:

      /* Create tfulfillment record for all changed(selet/unselect) tqualifications. */   
      create tfulfillment.
      assign 
          tfulfillment.fulfillmentID   = tqualification.qualreqid
          tfulfillment.requirementID   = ipireqID
          tfulfillment.entity          = ipcEntity 
          tfulfillment.entityID        = ipcEntityID /*qualification.entityId */
          tfulfillment.qualificationID = tqualification.qualificationID
          tfulfillment.activatedBy     = tqualification.userCreated
          tfulfillment.activatedDate   = tqualification.effectiveDate
          icount                       = icount + 1
          .
    end.
  end.
  
  if icount = 0 
   then
    do:
      message  "No changes to save."
          view-as alert-box info buttons ok.
      return no-apply.
    end.
  
  /* Create/Delete fulfillment record and update data modal */
  run newThirdPartyFulfillment in iphData(input  table tfulfillment,
                                          output std-lo,
                                          output std-ch).
  if not std-lo
   then
    do:
      message std-ch
        view-as alert-box error buttons ok.
      return.
    end.

  oplChange = yes.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE selectRecord Dialog-Frame 
PROCEDURE selectRecord :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable chQualification as character   no-undo.
  
  if not available tQualification
   then
    return.

  std-ro          = rowid(tQualification).
  chQualification = tQualification.Qualification.

  /* If no qualification record is selected for the statereqQual, then select qualification
     else deselect perviously selected qualification and select new qualfication */
  for each tQualification where tqualification.qualification = chQualification:
    tQualification.qualmet = if (rowid(tQualification) = std-ro)  then not( tQualification.qualmet ) else tQualification.qualmet  .                    
  end.

  browse brwQualification:refresh().
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getauth Dialog-Frame 
FUNCTION getauth returns character ( cAuth as character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if cAuth = {&ThirdParty} 
   then
    return {&Third-Party}.   /* Function return value. */
  else if cAuth = {&CompanyCode} 
   then
    return {&First-Party}.
  else
   return cAuth.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

