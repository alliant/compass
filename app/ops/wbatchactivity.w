&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wbatchactivity.w
   Window of BATCH ACTIVITY
   4.23.2012
 */

CREATE WIDGET-POOL.

{lib/std-def.i}

DEFINE VARIABLE activeBatchID AS CHARACTER   NO-UNDO.

def var tSyncSave as log no-undo.

{tt/batchaction.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwCodes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batchaction

/* Definitions for BROWSE brwCodes                                      */
&Scoped-define FIELDS-IN-QUERY-brwCodes batchAction.postDate batchAction.postUserID batchAction.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCodes   
&Scoped-define SELF-NAME brwCodes
&Scoped-define QUERY-STRING-brwCodes FOR EACH batchaction
&Scoped-define OPEN-QUERY-brwCodes OPEN QUERY {&SELF-NAME} FOR EACH batchaction.
&Scoped-define TABLES-IN-QUERY-brwCodes batchaction
&Scoped-define FIRST-TABLE-IN-QUERY-brwCodes batchaction


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwCodes}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwCodes fComments tSync 
&Scoped-Define DISPLAYED-OBJECTS fComments tBatch tSync 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getData C-Win 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resizeFrame C-Win 
FUNCTION resizeFrame RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDeleteComment  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected activity".

DEFINE BUTTON bNewComment  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add an activity".

DEFINE BUTTON bPrintNotes  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View PDF summary of activity".

DEFINE VARIABLE fComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 136 BY 4 NO-UNDO.

DEFINE VARIABLE tBatch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Batch" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1 NO-UNDO.

DEFINE VARIABLE tSync AS LOGICAL INITIAL yes 
     LABEL "Auto-refresh" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 TOOLTIP "Change with the batch records" NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwCodes FOR 
      batchaction SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCodes C-Win _FREEFORM
  QUERY brwCodes DISPLAY
      batchAction.postDate label "Date/Time" format "99/99/9999 HH:MM:SS" width 25
batchAction.postUserID label "By" format "x(12)"
batchAction.comments label "Action/Comments" format "x(200)" WIDTH 48
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 136 BY 17.97 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwCodes AT ROW 3.62 COL 2 WIDGET-ID 200
     fComments AT ROW 21.86 COL 2 NO-LABEL WIDGET-ID 90 NO-TAB-STOP 
     tBatch AT ROW 1.86 COL 31.6 COLON-ALIGNED WIDGET-ID 88
     tSync AT ROW 1.95 COL 54.8 WIDGET-ID 86
     bDeleteComment AT ROW 1.48 COL 10.4 WIDGET-ID 66 NO-TAB-STOP 
     bNewComment AT ROW 1.48 COL 3 WIDGET-ID 64 NO-TAB-STOP 
     bPrintNotes AT ROW 1.48 COL 17.8 WIDGET-ID 84 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 138 BY 25.1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Batch Activity"
         HEIGHT             = 25.1
         WIDTH              = 138
         MAX-HEIGHT         = 25.33
         MAX-WIDTH          = 173
         VIRTUAL-HEIGHT     = 25.33
         VIRTUAL-WIDTH      = 173
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwCodes 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bDeleteComment IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bNewComment IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bPrintNotes IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwCodes:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwCodes:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       fComments:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tBatch IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCodes
/* Query rebuild information for BROWSE brwCodes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchaction.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwCodes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Batch Activity */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Batch Activity */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Batch Activity */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteComment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteComment C-Win
ON CHOOSE OF bDeleteComment IN FRAME fMain /* Delete */
DO:
  run ActionDeleteComment in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewComment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewComment C-Win
ON CHOOSE OF bNewComment IN FRAME fMain /* New */
DO:
  run ActionNewComment in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrintNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrintNotes C-Win
ON CHOOSE OF bPrintNotes IN FRAME fMain /* Print */
DO:
  run ops02-r.p (activeBatchID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCodes
&Scoped-define SELF-NAME brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON ROW-DISPLAY OF brwCodes IN FRAME fMain
DO:
{lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON START-SEARCH OF brwCodes IN FRAME fMain
DO:
{lib/brw-startSearch.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON VALUE-CHANGED OF brwCodes IN FRAME fMain
DO:
  IF NOT AVAILABLE batchaction
   THEN assign
          bDeleteComment:SENSITIVE = false
          fComments:screen-value = "".
   else
    do:
        IF batchaction.secure 
         THEN bDeleteComment:SENSITIVE = false.
         ELSE bDeleteComment:SENSITIVE = true.
        fComments:screen-value = batchaction.comments.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSync
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSync C-Win
ON VALUE-CHANGED OF tSync IN FRAME fMain /* Auto-refresh */
DO:
  if not self:checked 
    then return.
  publish "GetCurrentValue" ("BatchID", output activeBatchID).
  run ActiveBatchChanged in this-procedure (activeBatchID).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/win-main.i}
{lib/brw-main.i}

subscribe to "IsBatchActivityOpen" anywhere.
subscribe to "BatchChanged" anywhere.
subscribe to "ActiveBatchChanged" anywhere.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

bNewComment:load-image("images/comment_add.bmp").
bNewComment:load-image-insensitive("images/comment_add-i.bmp").
bDeleteComment:load-image("images/comment_delete.bmp").
bDeleteComment:load-image-insensitive("images/comment_delete-i.bmp").
bPrintNotes:load-image("images/pdf.bmp").
bPrintNotes:load-image-insensitive("images/pdf-i.bmp").

PUBLISH "GetCurrentValue" ("BatchID", OUTPUT activeBatchID).
/* PUBLISH "GetBatchActions" (int(activeBatchID), OUTPUT TABLE batchaction). */

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  /*RUN ActiveBatchChanged IN THIS-PROCEDURE (activeBatchID).*/
  getData().
  bNewComment:SENSITIVE = (activeBatchID <> "").
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDeleteComment C-Win 
PROCEDURE ActionDeleteComment PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.

 if not available batchaction 
   or (available batchaction
        and batchaction.secure)
  then 
   do: bDeleteComment:sensitive in frame fMain = false.
       return.
   end.

 C-Win:always-on-top = yes.

 std-lo = no.
 MESSAGE "Comment will be permanently deleted.  Continue?"
  VIEW-AS ALERT-BOX warning BUTTONS yes-no update std-lo.
 
 C-Win:always-on-top = no.

 if not std-lo 
  then return.

 publish "DeleteBatchAction" (batchaction.batchID,
                              batchaction.seq,
                              output std-lo,
                              output std-ch).
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 getData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewComment C-Win 
PROCEDURE ActionNewComment PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

def var tFlag as char.
DEF VAR tStat AS CHAR.
def var tComment as char.
def var tBatchChanged as log.

if activeBatchID = "" 
 then
  do: bNewComment:sensitive in frame fMain = false.
      return.
  end.

PUBLISH "GetBatchAttribute" (int(activeBatchID), "ProcessingStatus", OUTPUT tFlag).
PUBLISH "GetBatchAttribute" (int(activeBatchID), "Status", OUTPUT tStat).

if tStat <> "P" 
 then
  run dialognewcomment.w (output pError,
                          output tComment).
 else 
  run dialognewcomment2.w (input-output tFlag,
                           output pError,
                           output tComment).

if pError or tComment = ""
 then return.

C-Win:always-on-top = yes.
tSyncSave = tSync:input-value.
tSync = no.
tSync:checked = no.

publish "NewBatchAction" (int(activeBatchID),
                          tComment,
                          tFlag,
                          false,
                          output std-in,
                          output tBatchChanged,
                          output std-lo,
                          output std-ch).

tSync:screen-value = string(tSyncSave).
tSync:checked = tSyncSave.
C-Win:always-on-top = no.

if not std-lo 
 then
  do:
      MESSAGE std-ch
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.

 if not tBatchChanged then
 getData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActiveBatchChanged C-Win 
PROCEDURE ActiveBatchChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEF INPUT PARAMETER pActiveBatch AS CHAR.

 if not tSync:checked in frame {&frame-name}
  then return.

 activeBatchID = pActiveBatch.

 getData().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BatchChanged C-Win 
PROCEDURE BatchChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 getData().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fComments tBatch tSync 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwCodes fComments tSync 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsBatchActivityOpen C-Win 
PROCEDURE IsBatchActivityOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pOpen as logical init true.
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 /* Check if the window is getting larger since the components of the frame
    need to be moved around before the frame is resized */
 std-lo = (frame fMain:width-pixels < {&window-name}:width-pixels and
           frame fMain:height-pixels < {&window-name}:height-pixels)
       or (frame fMain:width-pixels < {&window-name}:width-pixels and
           frame fMain:height-pixels = {&window-name}:height-pixels)
       or (frame fMain:width-pixels = {&window-name}:width-pixels and
           frame fMain:height-pixels < {&window-name}:height-pixels)
           .
 if std-lo 
  then resizeFrame().
 
 /* All components are positioned relative to the window since the window
    dimensions have already changed */
 brwCodes:width-pixels = {&window-name}:width-pixels - 10.
 brwCodes:height-pixels = {&window-name}:height-pixels - 147.
 fComments:Y = {&window-name}:height-pixels - 89.
 fComments:width-pixels = {&window-name}:width-pixels - 10.

 if not std-lo 
  then resizeFrame().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getData C-Win 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
 tBatch:screen-value in frame fMain = "".
  
 CLOSE QUERY {&browse-name}.
 EMPTY TEMP-TABLE batchaction.
 PUBLISH "GetBatchActions" (int(activeBatchID), OUTPUT TABLE batchaction).

 dataSortBy = "".
 dataSortDesc = no.
 run sortData("postdate").

 tBatch:screen-value in frame fMain = activeBatchID.
 apply "VALUE-CHANGED" to {&browse-name}.

 RETURN TRUE.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resizeFrame C-Win 
FUNCTION resizeFrame RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

