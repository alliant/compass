&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialogMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialogMain 
/*
 dialogmodifypolicy.w
 DIALOG to allow MODIFY of a POLICY
 4.25.2012 D.Sinclair
 */

def input parameter pPolicy as int.
def input-output parameter pFile as char.
def input-output parameter pForm as char.
def input-output parameter pSTAT as char.
def input-output parameter pEffDate as datetime.
def input-output parameter pCounty as char.
def input-output parameter pResidential as logical.

def input parameter pFormList as char.
def input parameter pStatCodeList as char.
def input parameter pCountyList as char.
def input parameter pErrMsg as char.

def output parameter pAction as char init "UNDO".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialogMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFile tEffDate tStat tForm tResidential ~
tCounty bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tFile tEffDate tStat tForm tResidential ~
tCounty tPolicy tErrMsg 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tCounty AS CHARACTER 
     LABEL "County" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN AUTO-COMPLETION
     SIZE 32.2 BY 1 NO-UNDO.

DEFINE VARIABLE tForm AS CHARACTER 
     LABEL "Form" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN AUTO-COMPLETION
     SIZE 113.4 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER 
     LABEL "STAT" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN AUTO-COMPLETION
     SIZE 113.4 BY 1 NO-UNDO.

DEFINE VARIABLE tEffDate as datetime FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tErrMsg AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 123.2 BY .62 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tResidential AS LOGICAL INITIAL no 
     LABEL "Residential" 
     VIEW-AS TOGGLE-BOX
     SIZE 15.2 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialogMain
     tFile AT ROW 3.14 COL 9.6 COLON-ALIGNED WIDGET-ID 2
     tEffDate AT ROW 4.24 COL 9.8 COLON-ALIGNED WIDGET-ID 10
     tStat AT ROW 5.43 COL 9.6 COLON-ALIGNED WIDGET-ID 32
     tForm AT ROW 6.52 COL 9.6 COLON-ALIGNED WIDGET-ID 30
     tResidential AT ROW 3.1 COL 45.8 WIDGET-ID 38
     tCounty AT ROW 4.33 COL 43.2 COLON-ALIGNED WIDGET-ID 34
     tPolicy AT ROW 2.05 COL 9.6 COLON-ALIGNED WIDGET-ID 4 NO-TAB-STOP 
     bSave AT ROW 8.62 COL 39
     bCancel AT ROW 8.57 COL 58.6
     tErrMsg AT ROW 10.14 COL 1.4 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     SPACE(1.39) SKIP(0.00)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify Policy"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialogMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fDialogMain:SCROLLABLE       = FALSE
       FRAME fDialogMain:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tErrMsg IN FRAME fDialogMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tPolicy IN FRAME fDialogMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialogMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialogMain fDialogMain
ON WINDOW-CLOSE OF FRAME fDialogMain /* Modify Policy */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fDialogMain
ON CHOOSE OF bSave IN FRAME fDialogMain /* Save */
DO:
  if tFile:screen-value in frame fDialogMain = "" 
   then
    do:
     MESSAGE "File cannot be blank."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return no-apply.
    end.
  pAction = "SAVE".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialogMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

tCounty:list-item-pairs in frame fDialogMain = pCountyList.
tForm:list-item-pairs in frame fDialogMain = pFormList.
tStat:list-item-pairs in frame fDialogMain = pStatCodeList.

ASSIGN
  tFile = pFile
  tPolicy = pPolicy
  tForm = pForm
  tSTAT = pSTAT
  tEffDate = pEffDate
  tCounty = pCounty
  tResidential = pResidential
  tErrMsg = pErrMsg
  .

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if pAction = "SAVE" 
   then assign
          pFile = tFile:input-value in frame fDialogMain
          pForm = tForm:input-value in frame fDialogMain
          pSTAT = tSTAT:input-value in frame fDialogMain
          pEffDate = tEffDate:input-value in frame fDialogMain
          pCounty = tCounty:input-value in frame fDialogMain
          pResidential = tResidential:checked in frame fDialogMain
          .
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialogMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialogMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialogMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFile tEffDate tStat tForm tResidential tCounty tPolicy tErrMsg 
      WITH FRAME fDialogMain.
  ENABLE tFile tEffDate tStat tForm tResidential tCounty bSave bCancel 
      WITH FRAME fDialogMain.
  VIEW FRAME fDialogMain.
  {&OPEN-BROWSERS-IN-QUERY-fDialogMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

