&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* dialogagentappcancel.w
   Window for adding the new agent application
   Yoke Sam 08.09.2017

 */
 
{tt/policyhoi.i}
{tt/agent.i}
{tt/county.i}
{tt/sysprop.i}
{tt/policy.i}
{tt/batchform.i}


def input parameter table for policyhoi.
def output parameter pCancel as logical init true.

def var countylist as char no-undo.


{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tFileNumber tCounty tAgentID tAgentName ~
tIssueDate tPolicy tLiability tGrossPremium tAddr1 tDIPStatus tCoopAgentID ~
tAddr2 bCancel tRequestAgentID 
&Scoped-Define DISPLAYED-OBJECTS tFileNumber tCounty tAgentID tAgentName ~
tIssueDate tPolicy tAddr1 tDIPStatus tCoopAgentID tAddr2 tRequestAgentID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetCountyList fMain 
FUNCTION GetCountyList RETURNS CHARACTER
  ( input stID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SetModify fMain 
FUNCTION SetModify RETURNS logical
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidateEntry fMain 
FUNCTION ValidateEntry RETURNS logical
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bModify AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS COMBO-BOX INNER-LINES 30
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE tCoopAgentID AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     LABEL "Cooperating Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tIssueDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Policy Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 22.8 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter policy number and press <Return> to search"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tRequestAgentID AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     LABEL "Requesting Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDIPStatus AS INTEGER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "MultiCounty (0)", 0,
"Best Evidence (1)", 1,
"Out of County (2)", 2
     SIZE 21.2 BY 3 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tFileNumber AT ROW 3.38 COL 15.4 COLON-ALIGNED WIDGET-ID 14
     tCounty AT ROW 4.57 COL 15.4 COLON-ALIGNED WIDGET-ID 190
     tAgentID AT ROW 1.48 COL 71.6 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     tAgentName AT ROW 1.48 COL 86.4 COLON-ALIGNED NO-LABEL WIDGET-ID 70 NO-TAB-STOP 
     tIssueDate AT ROW 5.91 COL 15.4 COLON-ALIGNED WIDGET-ID 184
     tPolicy AT ROW 1.52 COL 15.4 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tLiability AT ROW 7.1 COL 15.4 COLON-ALIGNED WIDGET-ID 32
     tGrossPremium AT ROW 8.33 COL 15.4 COLON-ALIGNED WIDGET-ID 44
     tAddr1 AT ROW 2.62 COL 86.4 COLON-ALIGNED NO-LABEL WIDGET-ID 18 NO-TAB-STOP 
     tDIPStatus AT ROW 10.05 COL 18 NO-LABEL WIDGET-ID 192
     tCoopAgentID AT ROW 7.05 COL 71.6 COLON-ALIGNED WIDGET-ID 188
     tAddr2 AT ROW 3.76 COL 86.4 COLON-ALIGNED NO-LABEL WIDGET-ID 20 NO-TAB-STOP 
     bModify AT ROW 13.1 COL 54.8
     bCancel AT ROW 13.1 COL 70.8
     tRequestAgentID AT ROW 5.91 COL 71.6 COLON-ALIGNED WIDGET-ID 186 NO-TAB-STOP 
     "Status:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 10.19 COL 9 WIDGET-ID 196
     SPACE(121.79) SKIP(3.99)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify Home Issued Policy"
         CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAddr1:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAddr2:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAgentID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAgentName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tIssueDate:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tLiability:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPolicy:READ-ONLY IN FRAME fMain        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Modify Home Issued Policy */
DO:
    std-lo = true.
    if bModify:sensitive in frame {&frame-name}
       then message "You've not saved yet; cancel?" view-as alert-box buttons yes-no update std-lo.
    
    if std-lo then
     do:
        APPLY "END-ERROR":U TO SELF.
     end.
     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fMain
ON CHOOSE OF bCancel IN FRAME fMain /* Cancel */
DO:
  apply "WINDOW-CLOSE" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify fMain
ON CHOOSE OF bModify IN FRAME fMain /* Save */
DO:
  if  ValidateEntry() then 
  do:
      SetModify(true).
      run ModifyPolicyHoi in this-procedure.
      
      .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAddr1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr1 fMain
ON ENTRY OF tAddr1 IN FRAME fMain
DO:
  apply "entry":U to tFileNumber in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAddr2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr2 fMain
ON ENTRY OF tAddr2 IN FRAME fMain
DO:
  apply "entry":U to tFileNumber in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID fMain
ON ENTRY OF tAgentID IN FRAME fMain /* Agent */
DO:
  apply "entry":U to tFileNumber in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentName fMain
ON ENTRY OF tAgentName IN FRAME fMain
DO:
  apply "entry":U to tFileNumber in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPolicy fMain
ON ENTRY OF tPolicy IN FRAME fMain /* Policy */
DO:
  apply "entry":U to tFileNumber in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPolicy fMain
ON RETURN OF tPolicy IN FRAME fMain /* Policy */
DO:
/*   run SetScreenvalues in this-procedure. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRequestAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRequestAgentID fMain
ON ENTRY OF tRequestAgentID IN FRAME fMain /* Requesting Agent */
DO:
  apply "entry":U to tFileNumber in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

setModify(true).

ON 'VALUE-CHANGED':U anywhere 
DO:
    bModify:sensitive in frame {&frame-name} = true.
END.

frame {&frame-name}:title = "Modify Home Issued Policy".

std-ch = "".
publish "GetCounties" (output table county).
/* for each county where stateID = {&statehoi} by county.countyID:                                                                     */
/*     std-ch = (if std-ch > "" then std-ch + "," else std-ch) + county.description + " [" + county.countyID + "]," + county.countyID. */
/* end.                                                                                                                                */
/*                                                                                                                                     */
/* tCounty:list-item-pairs in frame {&frame-name} =  std-ch.                                                                           */
countylist = std-ch.

publish "GetSysProps" (output table sysprop).

/* publish "GetAgent" (output table agent). */

RUN enable_UI.
run SetScreenvalues in this-procedure.



MAIN-BLOCK:

repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   WAIT-FOR GO OF FRAME {&FRAME-NAME}.

END.



/* repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK */
/*    ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:  */
/*   WAIT-FOR GO OF FRAME {&FRAME-NAME}.             */
/*                                                   */
/*   if tCode:input-value in frame fMain = ""        */
/*   or tCode:input-value in frame fMain = ? then    */
/*   do:                                             */
/*     message                                       */
/*       "Code cannot be blank"                      */
/*       view-as alert-box error.                    */
/*     next.                                         */
/*   end.                                            */
/*                                                   */
/*   assign                                          */
/*     pCode = tCode:input-value in frame fMain      */
/*     pCancel = false                               */
/*     .                                             */
/*   leave MAIN-BLOCK.                               */
/* END.                                              */
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFileNumber tCounty tAgentID tAgentName tIssueDate tPolicy tAddr1 
          tDIPStatus tCoopAgentID tAddr2 tRequestAgentID 
      WITH FRAME fMain.
  ENABLE tFileNumber tCounty tAgentID tAgentName tIssueDate tPolicy tLiability 
         tGrossPremium tAddr1 tDIPStatus tCoopAgentID tAddr2 bCancel 
         tRequestAgentID 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyPolicyHoi fMain 
PROCEDURE ModifyPolicyHoi PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
empty temp-table policyhoi.
do with frame {&frame-name}:
    bModify:sensitive = false.
    create policyhoi.

    assign
        policyhoi.policyID = tPolicy:input-value
        policyhoi.DIPStatus = tDIPStatus:input-value
        policyhoi.liabilityAmount = tLiability:input-value
        policyhoi.grossPremium = tGrossPremium:input-value
        policyhoi.policyDate = tIssueDate:input-value
        policyhoi.fileNumber = tFileNumber:input-value
        policyhoi.countyID = tCounty:input-value
        policyhoi.coopAgentID = tCoopAgentID:input-value
        policyhoi.requestAgentID = tRequestAgentID:input-value
        .

    publish "ModifyPolicyHoi" (table policyhoi, output std-lo, output std-ch).

    if std-lo then
       apply "window-close" to frame {&frame-name}.


end.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetScreenValues fMain 
PROCEDURE SetScreenValues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:

for first policyhoi no-lock
    by policyhoi.policyID desc:
   
/*     message "lookup:" entry(lookup(policyhoi.countyID,countylist) - 1, countylist) */
/*   + chr(13) + "countyid:" policyhoi.countyID                                       */
/*      view-as alert-box.                                                            */

/*    frame {&frame-name}:title = agentapplication.agentName. */
    
    publish "GetPolicy" (policyhoi.policyID, output table policy, output table batchform).
    for first policy no-lock:

        publish "GetAgent" (policy.agentID, output table agent).
        
/*         message "policy id:" string(policy.policyID) + chr(13) + */
/*                "agentid:" policy.agentID                         */
/*             view-as alert-box.                                   */
        for first agent where agent.agentID = policy.agentID no-lock:

        tCounty:list-item-pairs = GetCountyList(agent.stateID).
        assign
           tAgentID:screen-value = policy.agentID
           tAgentName:screen-value = agent.name
           tAddr1:screen-value = agent.addr1
           tAddr2:screen-value = agent.addr2
           .
        end.
   end.

    
    assign
     tPolicy:screen-value = string(policyhoi.policyID)
     tRequestAgentID:screen-value = policyhoi.requestAgentID
     tCoopAgentID:screen-value = policyhoi.coopAgentID
     tFileNumber:screen-value = policyhoi.fileNUmber
     tIssueDate:screen-value = string(policyhoi.policyDate)
     tLiability:screen-value = string(policyhoi.liabilityAmount)
     tGrossPremium:screen-value = string(policyhoi.grossPremium)
     tCounty:screen-value = policyhoi.countyID
     tDIPStatus:screen-value = policyhoi.DIPStatus
     .

leave.
end.
end.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetCountyList fMain 
FUNCTION GetCountyList RETURNS CHARACTER
  ( input stID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
std-ch = "".
  for each county where stateID = stID by county.countyID:                                                                     
     std-ch = (if std-ch > "" then std-ch + "," else std-ch) + county.description + " [" + county.countyID + "]," + county.countyID. 
  end.         
  
  RETURN std-ch.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SetModify fMain 
FUNCTION SetModify RETURNS logical
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
    
     if pModify
        then 
         bModify:label = "Save".
   
     assign 
       tFileNumber:read-only = not pModify
       tCoopAgentID:read-only = not pModify
       tCounty:sensitive = pModify
       tDIPStatus:sensitive = pModify
       tIssueDate:read-only = not pModify
       tLiability:read-only = not pModify
       tGrossPremium:read-only = not pModify
       
     . 
    
 end.

  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidateEntry fMain 
FUNCTION ValidateEntry RETURNS logical
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
std-lo = true.

std-ch = "".

do with frame {&frame-name}:


  /* Check for File Number */
     if tFileNumber:screen-value = "" then
     do:
        std-ch = std-ch + "File Number cannot be blank.".
     end.

     /* Check for Agent IDs */
     if tCoopAgentID:screen-value = ""  then
         std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "Cooperating Agent ID cannot be blank.".
     /* Check for future date of policy  */
     if date(tIssueDate:screen-value) > today then 
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) +  "Policy Date cannot be in the future.".
    
     if date(tIssueDate:input-value) = ? then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) +  "Policy Date invalid.".

     /* Check for liability and gross premium  */
     if tLiability:input-value = 0 then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "Liability amount cannot be zero.".

     if tGrossPremium:input-value = 0 then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "Gross Premium cannot be zero.".

     if tCounty:input-value = "" then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "County cannot be blank.".


end.     

if std-ch > "" then
   do: 
     message std-ch view-as alert-box.
     std-lo = false.
   end.

RETURN std-lo.   /* Function return value. */

            
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

