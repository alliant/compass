&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* wops.w
   Main Windows of the Operations Processing application that is started
   by opsstart.p
   D.Sinclair 1.29.2013
   09.28.2014 D.Sinclair Restructured setActionStatus() to be the central
                         location to set menu-items and toolbar actions for
                         the various batch-specific actions.
   11.26.2014 D.Sinclair Added notion of "NeedToRefresh" for both Period and
                         Batches to prevent window moving to top on redisplay.
                         This relies on brwBatch being the only widget on the
                         main screen with focus.
   03.12.2020 AG         changed batch.state to batch.stateID
*/   

CREATE WIDGET-POOL.

{src/adm2/widgetprto.i}

{lib/std-def.i}
{lib/winlaunch.i}
{lib/get-column.i}
{tt/openwindow.i}

/* def var hRowCnt as handle no-undo.  */
def var rowCnt as int no-undo.

def var activeType as char no-undo.
def var activeMonth as int no-undo.
def var activeYear as int no-undo.
def var activePeriodID as int no-undo.
def var activeAction as char no-undo. /* Batch Processing */
def var activeBatchID as int no-undo.
def var processBatchID as int no-undo.
def var activeAgentID as char no-undo.
def var tDisplayBatch as log no-undo init yes.

def var tColumnWidth as decimal no-undo.

def var tNeedToRefreshBatches as log no-undo init false.
def var tNeedToRefreshPeriod as log no-undo init false.
def var tNeedToRefreshMsg as char no-undo.

def var maxMonth as int no-undo.
def var maxYear as int no-undo.

{tt/state.i}
{tt/batch.i}
{tt/batchaction.i}
/* Used to return subsets of the original datasets for various calls */
{tt/batch.i &tableAlias="tempBatch"}
{tt/batchaction.i &tableAlias="tempBatchAction"}


def var batchByClause as char init " by batch.batchID " no-undo.
def var batchWhereClause as char no-undo.

def var tEnabledActions as char no-undo.
def var tDisabledActions as char no-undo.

def var tConfirmExit as log no-undo.

/* Handles to subordinate windows */
def var hProcessing as handle no-undo.
def var hStatCode as handle no-undo.
def var hState as handle no-undo.
def var hCounty as handle no-undo.
def var hForm as handle no-undo.
def var hAgent as handle no-undo.
def var hPeriod as handle no-undo.
def var hDocs as handle no-undo.
DEF VAR hActivity AS HANDLE NO-UNDO.
DEF VAR hHOI AS HANDLE NO-UNDO.
DEF VAR hConfig as handle no-undo.
define variable hRevenueType  as handle no-undo.
define variable hAttorney as handle no-undo.

/* Save button state when a dependent child window is open so we can
   disable the buttons and then re-enable them when the child window(s)
   is closed.  OpenWindow and CloseWindow */
def var saveEnabledActions as char no-undo.
def var saveEnabledButtons as logical extent 3 no-undo.

define menu batchpopmenu title "Actions"
 menu-item m_PopModify label "Modify"
 menu-item m_PopProcessing label "Processing Data Entry"
 rule 
 menu-item m_PopValidate  label "Validate..."
 menu-item m_PopViewReport label "View Validation Report"
 rule
 menu-item m_PopBatchDetails label "Batch Details"
 menu-item m_PopBatchCounts label "Batch Counts"
 menu-item m_PopBatchActivity label "Batch Activity"
 menu-item m_PopViewAttachment label "View Documents"
 rule
 menu-item m_PopReassignPolicies label "Reassign Policies..."
 .

ON 'choose':U OF menu-item m_PopModify in menu batchpopmenu
DO:
 run ActionModify in this-procedure (output std-lo).
 RETURN.
END.

ON 'choose':U OF menu-item m_PopViewAttachment in menu batchpopmenu
DO:
 run ActionAttachment in this-procedure (output std-lo).
 RETURN.
END.

ON 'choose':U OF menu-item m_PopProcessing in menu batchpopmenu
DO:
 run ActionProcessing in this-procedure (output std-lo).
 RETURN.
END.

ON 'choose':U OF menu-item m_PopValidate in menu batchpopmenu
DO:
 run ActionValidate in this-procedure (output std-lo).
 RETURN.
END.

ON 'choose':U OF menu-item m_PopViewReport in menu batchpopmenu
DO:
 run ActionViewReport in this-procedure (output std-lo).
 RETURN.
END.

ON 'choose':U OF menu-item m_PopBatchDetails in menu batchpopmenu
DO:
 run ActionBatchDetails in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopBatchCounts in menu batchpopmenu
DO:
 run ActionBatchCounts in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopBatchActivity in menu batchpopmenu
DO:
 run ActionBatchActivity in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopReassignPolicies in menu batchpopmenu
DO:
 run ActionBatchReassignPolicies in this-procedure.
 RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwBatch

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batch

/* Definitions for BROWSE brwBatch                                      */
&Scoped-define FIELDS-IN-QUERY-brwBatch batch.stat batch.hasDocument batch.batchID batch.agentID batch.agentName batch.stateID batch.receivedDate batch.reference batch.grossPremiumReported batch.netPremiumReported batch.grossPremiumProcessed batch.netPremiumProcessed batch.grossPremiumDelta batch.netPremiumDelta /* entry(index("NPIC", batch.stat), "New,Processing,Invoicing,Complete") */   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBatch   
&Scoped-define SELF-NAME brwBatch
&Scoped-define QUERY-STRING-brwBatch FOR EACH batch. rowCnt = 0
&Scoped-define OPEN-QUERY-brwBatch OPEN QUERY {&SELF-NAME} FOR EACH batch. rowCnt = 0.
&Scoped-define TABLES-IN-QUERY-brwBatch batch
&Scoped-define FIRST-TABLE-IN-QUERY-brwBatch batch


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fState tTotalGrossRevenue ~
tTotalGrossRevenueDelta tTotalGrossReported tTotalGrossProcessed ~
tTotalGrossDelta tTotalBatchCnt tTotalNetRevenue tTotalNetRevenueDelta ~
tTotalNetReported tTotalNetProcessed tTotalNetDelta brwBatch 
&Scoped-Define DISPLAYED-OBJECTS fState tTotalBatchCnt ~
tReportedPremiumLabel tProcessedPremiumLabel tVarianceLabel ~
tRevenuePremiumLabel tVarianceLabel2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calcTotals wWin 
FUNCTION calcTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayBatch wWin 
FUNCTION displayBatch RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayPeriod wWin 
FUNCTION displayPeriod RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals wWin 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getPeriodName wWin 
FUNCTION getPeriodName RETURNS CHARACTER PRIVATE
  (input pMonth as int,
   input pYear as int)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD hideUpdateMessage wWin 
FUNCTION hideUpdateMessage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setActionStatus wWin 
FUNCTION setActionStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showUpdateMessage wWin 
FUNCTION showUpdateMessage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Application 
       MENU-ITEM m_Configure    LABEL "Configure..."   ACCELERATOR "CTRL-O"
       RULE
       MENU-ITEM m_About        LABEL "About..."       ACCELERATOR "CTRL-A"
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"           ACCELERATOR "CTRL-Q".

DEFINE SUB-MENU m_View 
       MENU-ITEM m_GoToPeriod   LABEL "Period..."     
       RULE
       MENU-ITEM m_Open         LABEL "New"           
              TOGGLE-BOX
       MENU-ITEM m_Processing   LABEL "Processing"    
              TOGGLE-BOX
       MENU-ITEM m_Invoicing    LABEL "Reviewing"     
              TOGGLE-BOX
       MENU-ITEM m_Complete     LABEL "Complete"      
              TOGGLE-BOX
       MENU-ITEM m_All          LABEL "All"           
              TOGGLE-BOX
       RULE
       MENU-ITEM m_ActionRefresh LABEL "Refresh"        ACCELERATOR "CTRL-R".

DEFINE SUB-MENU m_Batch 
       MENU-ITEM m_ActionNew    LABEL "Add new..."    
       MENU-ITEM m_ActionModify LABEL "Modify details..."
       MENU-ITEM m_LockUnlock   LABEL "Lock/Unlock"   
       RULE
       MENU-ITEM m_ActionToProcess LABEL "Start processing"
       MENU-ITEM m_ActionProcessing LABEL "Processing data entry"
       MENU-ITEM m_ActionToReview LABEL "Review batch"  
       RULE
       MENU-ITEM m_ActionValidate LABEL "Validate..."   
       MENU-ITEM m_ActionSplit  LABEL "Split..."      
       MENU-ITEM m_ActionInvoice LABEL "Invoice..."    
       RULE
       MENU-ITEM m_ActionMoveToPeriod LABEL "Move to another period..."
       MENU-ITEM m_ActionDelete LABEL "Complete empty...".

DEFINE SUB-MENU m_Files 
       MENU-ITEM m_Agents       LABEL "Agents"        
       MENU-ITEM m_Attorneys    LABEL "Attorneys"     
       MENU-ITEM m_Forms        LABEL "State Products"
       MENU-ITEM m_Counties     LABEL "Counties"      
       MENU-ITEM m_States       LABEL "States"        
       MENU-ITEM m_STAT_Codes   LABEL "STAT Codes"    
       MENU-ITEM m_Periods      LABEL "Periods"       
       MENU-ITEM m_Default_Endorsements LABEL "Default Endorsements"
       MENU-ITEM m_Processing_Error_Codes LABEL "Processing Error Codes"
       MENU-ITEM m_Revenue      LABEL "Revenue Types" .

DEFINE SUB-MENU m_Lookup 
       MENU-ITEM m_Batch_Activity LABEL "Batch Activity" ACCELERATOR "CTRL-T"
       MENU-ITEM m_ActionAttachment LABEL "View Documents" ACCELERATOR "CTRL-D"
       RULE
       MENU-ITEM m_Agent_File   LABEL "File Details"   ACCELERATOR "CTRL-F"
       MENU-ITEM m_Policy       LABEL "Policy Details" ACCELERATOR "CTRL-P"
       RULE
       MENU-ITEM m_Hold_Open_Policies LABEL "Hold Open Policies"
       MENU-ITEM m_HOI_Policies LABEL "HOI Policies"  .

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_Batch_Details LABEL "Batch Details" 
       MENU-ITEM m_Batch_Counts LABEL "Batch Counts"  
       MENU-ITEM m_Period_Summary LABEL "Batches for Period"
       RULE
       MENU-ITEM m_Agent_Batch_Activity LABEL "Agent Batch Activity"
       MENU-ITEM m_Agent_For_Period LABEL "Agents for Period"
       MENU-ITEM m_Agent_Data_Call LABEL "Agent Data Call"
       MENU-ITEM m_Agent_Accounting LABEL "Agent Accounting"
       RULE
       MENU-ITEM m_State_Batch_Activity LABEL "State Batch Activity"
       MENU-ITEM m_State_Data_Call LABEL "State Data Call"
       RULE
       MENU-ITEM m_Data_Call    LABEL "Business Mix (Data Call)"
       MENU-ITEM m_Period_Details LABEL "Period Details"
       MENU-ITEM m_Statutory_Liability LABEL "Statutory Liability"
       MENU-ITEM m_Transaction_Summary LABEL "Transaction Summary"
       MENU-ITEM m_Reprocessed_Forms LABEL "Reprocessed Forms"
       MENU-ITEM m_Reinsurance  LABEL "Reinsurance"   
       MENU-ITEM m_Pipeline     LABEL "Revenue Pipeline"
       MENU-ITEM m_Unreported_Policies LABEL "Unreported Policies"
       RULE
       MENU-ITEM m_Paid_Files_with_a_Positive_ LABEL "Paid Files with a Positive Balance"
       MENU-ITEM m_Paid_Files_with_a_Negative_ LABEL "Paid Files with a Negative Balance".

DEFINE MENU MENU-BAR-wWin MENUBAR
       SUB-MENU  m_Application  LABEL "Module"        
       SUB-MENU  m_View         LABEL "Select"        
       SUB-MENU  m_Batch        LABEL "Batch"         
       SUB-MENU  m_Files        LABEL "References"    
       SUB-MENU  m_Lookup       LABEL "View"          
       SUB-MENU  m_Reports      LABEL "Reports"       .


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_toolbar AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE fStatus AS CHARACTER FORMAT "X(256)":U INITIAL "A" 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","A",
                     "New","N",
                     "Processing","P",
                     "Reviewing","R",
                     "Complete","C"
     DROP-DOWN-LIST
     SIZE 20.4 BY 1 NO-UNDO.

DEFINE VARIABLE tPeriodClosed AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 14 BY .62
     FGCOLOR 12 FONT 6 NO-UNDO.

DEFINE VARIABLE tPeriodDesc AS CHARACTER FORMAT "X(256)":U INITIAL "December 2012" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tProcessedPremiumLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Processed" 
      VIEW-AS TEXT 
     SIZE 10.6 BY .62 NO-UNDO.

DEFINE VARIABLE tReportedPremiumLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Reported" 
      VIEW-AS TEXT 
     SIZE 9.2 BY .62 NO-UNDO.

DEFINE VARIABLE tRevenuePremiumLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Revenue" 
      VIEW-AS TEXT 
     SIZE 10.6 BY .62 NO-UNDO.

DEFINE VARIABLE tTotalBatchCnt AS INTEGER FORMAT "z,zz9":U INITIAL 9999 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tTotalGrossDelta AS DECIMAL FORMAT "$(zz,zzz,zz9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23.8 BY 1 TOOLTIP "The difference between total gross Reported and Processed" NO-UNDO.

DEFINE VARIABLE tTotalGrossProcessed AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalGrossReported AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 22.8 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalGrossRevenue AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalGrossRevenueDelta AS DECIMAL FORMAT "$(zz,zzz,zz9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23.8 BY 1 TOOLTIP "The difference between total gross Revenue and Processed" NO-UNDO.

DEFINE VARIABLE tTotalNetDelta AS DECIMAL FORMAT "$(zz,zzz,zz9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23.8 BY 1 TOOLTIP "The difference between total net Reported and Processed" NO-UNDO.

DEFINE VARIABLE tTotalNetProcessed AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalNetReported AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 22.8 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalNetRevenue AS DECIMAL FORMAT "$-zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tTotalNetRevenueDelta AS DECIMAL FORMAT "$(zz,zzz,zz9.99)":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23.8 BY 1 TOOLTIP "The difference between total net Revenue and Processed" NO-UNDO.

DEFINE VARIABLE tVarianceLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Variance" 
      VIEW-AS TEXT 
     SIZE 9.2 BY .62 NO-UNDO.

DEFINE VARIABLE tVarianceLabel2 AS CHARACTER FORMAT "X(256)":U INITIAL "Variance" 
      VIEW-AS TEXT 
     SIZE 9.2 BY .62 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 109.4 BY 3.33.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65 BY 3.33.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwBatch FOR 
      batch SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBatch wWin _FREEFORM
  QUERY brwBatch DISPLAY
      batch.stat label "Stat" format "x(6)"
 batch.hasDocument column-label "Has!Doc" view-as toggle-box
 batch.batchID label "Batch ID" format " >>>>>>>>>9  "
 batch.agentID label "Agent ID" format "x(11)" width 12
 batch.agentName label "Agent" format "x(200)" width 32
 batch.stateID label "State" format "x(7)"
 batch.receivedDate column-label "Received!Date" format "99/99/9999  "
 batch.reference column-label "Reference" format "x(40)" width 40
 batch.grossPremiumReported column-label "Reported!Gross" format "$->>,>>>,>>9.99"
 batch.netPremiumReported column-label "Reported!Net" format "$->>,>>>,>>9.99"
 batch.grossPremiumProcessed column-label "Processed!Gross" format "$->>,>>>,>>9.99"
 batch.netPremiumProcessed column-label "Processed!Net" format "$->>,>>>,>>9.99"
 batch.grossPremiumDelta column-label "Accounting!Gross" format "$->>,>>>,>>9.99"
 batch.netPremiumDelta column-label "Accounting!Net" format "$->>,>>>,>>9.99"

/*  entry(index("NPIC", batch.stat), "New,Processing,Invoicing,Complete") label "Status" format "x(10)" */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 236.4 BY 20.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fState AT ROW 1.14 COL 7 COLON-ALIGNED WIDGET-ID 68 NO-TAB-STOP 
     fStatus AT ROW 2.19 COL 7 COLON-ALIGNED WIDGET-ID 70 NO-TAB-STOP 
     tTotalGrossRevenue AT ROW 4.19 COL 153 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     tTotalGrossRevenueDelta AT ROW 4.19 COL 178.4 COLON-ALIGNED NO-LABEL WIDGET-ID 96 NO-TAB-STOP 
     tTotalGrossReported AT ROW 4.24 COL 55.2 COLON-ALIGNED WIDGET-ID 24 NO-TAB-STOP 
     tTotalGrossProcessed AT ROW 4.24 COL 87.4 COLON-ALIGNED WIDGET-ID 32 NO-TAB-STOP 
     tTotalGrossDelta AT ROW 4.24 COL 112.8 COLON-ALIGNED NO-LABEL WIDGET-ID 60 NO-TAB-STOP 
     tPeriodDesc AT ROW 4.29 COL 5 COLON-ALIGNED NO-LABEL WIDGET-ID 10 NO-TAB-STOP 
     tTotalBatchCnt AT ROW 4.43 COL 35 COLON-ALIGNED NO-LABEL WIDGET-ID 80 NO-TAB-STOP 
     tTotalNetRevenue AT ROW 5.38 COL 153 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     tTotalNetRevenueDelta AT ROW 5.38 COL 178.4 COLON-ALIGNED NO-LABEL WIDGET-ID 100 NO-TAB-STOP 
     tTotalNetReported AT ROW 5.43 COL 55.2 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tTotalNetProcessed AT ROW 5.43 COL 87.4 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     tTotalNetDelta AT ROW 5.43 COL 112.8 COLON-ALIGNED NO-LABEL WIDGET-ID 62 NO-TAB-STOP 
     brwBatch AT ROW 6.95 COL 1.6 WIDGET-ID 200
     tReportedPremiumLabel AT ROW 3.52 COL 62 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     tProcessedPremiumLabel AT ROW 3.57 COL 93.8 COLON-ALIGNED NO-LABEL WIDGET-ID 38
     tVarianceLabel AT ROW 3.57 COL 120.2 COLON-ALIGNED NO-LABEL WIDGET-ID 94
     tRevenuePremiumLabel AT ROW 3.57 COL 159.8 COLON-ALIGNED NO-LABEL WIDGET-ID 104
     tVarianceLabel2 AT ROW 3.57 COL 185.4 COLON-ALIGNED NO-LABEL WIDGET-ID 106
     tPeriodClosed AT ROW 5.29 COL 8.4 COLON-ALIGNED NO-LABEL WIDGET-ID 78
     "Accounting" VIEW-AS TEXT
          SIZE 13.8 BY .62 TOOLTIP "The accounting totals for the selected state and status" AT ROW 3.14 COL 142.8 WIDGET-ID 110
          FONT 6
     "Batches" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 5.48 COL 37 WIDGET-ID 82
     "Processing" VIEW-AS TEXT
          SIZE 13.4 BY .62 TOOLTIP "The processing totals for the selected state and status" AT ROW 3.19 COL 32.6 WIDGET-ID 88
          FONT 6
     RECT-1 AT ROW 3.43 COL 30.8 WIDGET-ID 86
     RECT-3 AT ROW 3.43 COL 141 WIDGET-ID 108
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 237.6 BY 26.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 26.71
         WIDTH              = 237.6
         MAX-HEIGHT         = 47.95
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.95
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-wWin:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwBatch tTotalNetDelta fMain */
ASSIGN 
       brwBatch:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwBatch:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR COMBO-BOX fStatus IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tPeriodClosed IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tPeriodDesc IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tProcessedPremiumLabel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tReportedPremiumLabel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRevenuePremiumLabel IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tTotalBatchCnt:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalGrossDelta IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalGrossDelta:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalGrossProcessed IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalGrossProcessed:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalGrossReported IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalGrossReported:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalGrossRevenue IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalGrossRevenue:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalGrossRevenueDelta IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalGrossRevenueDelta:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalNetDelta IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalNetDelta:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalNetProcessed IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalNetProcessed:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalNetReported IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalNetReported:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalNetRevenue IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalNetRevenue:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tTotalNetRevenueDelta IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tTotalNetRevenueDelta:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tVarianceLabel IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tVarianceLabel2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBatch
/* Query rebuild information for BROWSE brwBatch
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batch.
rowCnt = 0.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fMain
/* Query rebuild information for FRAME fMain
     _Query            is NOT OPENED
*/  /* FRAME fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin
DO:
  publish "GetConfirmExit" (output tConfirmExit).
  
  if tConfirmExit = yes then
  do:
    std-lo = no.
    message "Are you sure you want to exit?"
        view-as alert-box question buttons yes-no update std-lo.
    if not (std-lo = yes) then
    return no-apply.
  end.
  
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  apply "CLOSE" to this-procedure.
  publish "ExitApplication".
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBatch
&Scoped-define SELF-NAME brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON DEFAULT-ACTION OF brwBatch IN FRAME fMain
DO:
  if fStatus:screen-value = "P" 
   then run ActionProcessing in this-procedure (output std-lo).
   else
    do:
      std-ch = "".
      publish "GetDoubleClick" (output std-ch).
      case std-ch:
       when "M" then run ActionModify in this-procedure (output std-lo).
       when "D" then run wops01-r.w persistent.
      end case.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON ENTRY OF brwBatch IN FRAME fMain
DO:
    /* ActionRefresh will display the Period as well */
    if tNeedToRefreshBatches 
     then run ActionRefresh in this-procedure (output std-lo).
     else
    if tNeedToRefreshPeriod 
     then displayPeriod().
    RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON ROW-DISPLAY OF brwBatch IN FRAME fMain
DO:
 {lib/brw-rowDisplay.i}
 
 if activeType = "P" 
  then
   case batch.processStat:
    when "R" then 
     assign
       batch.batchID:bgcolor in browse brwBatch = 12
       batch.batchID:fgcolor in browse brwBatch = 15
       .
    when "G" then 
     assign
       batch.batchID:bgcolor in browse brwBatch = 2
       batch.batchID:fgcolor in browse brwBatch = 15
       .
    when "Y" then 
     assign
       batch.batchID:bgcolor in browse brwBatch = 14
       batch.batchID:fgcolor in browse brwBatch = ?
       .

    otherwise 
    assign
      batch.batchID:bgcolor in browse brwBatch = ?
      batch.batchID:fgcolor in browse brwBatch = ?
      .
   end case.

 if batch.grossPremiumDelta <> batch.grossPremiumProcessed 
  then assign
         batch.grossPremiumDelta:bgcolor in browse brwBatch = 14
         .
  else assign
         batch.grossPremiumDelta:bgcolor in browse brwBatch = ?
         .

 if batch.netPremiumDelta <> batch.netPremiumProcessed 
  then assign
         batch.netPremiumDelta:bgcolor in browse brwBatch = 14
         .
  else assign
         batch.netPremiumDelta:bgcolor in browse brwBatch = ?
         .


 batch.stat:screen-value = batch.stat 
    + (if batch.lockedBy <> "" then "L" else "U")
    + batch.processStat.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON START-SEARCH OF brwBatch IN FRAME fMain
DO:
{lib/brw-startSearch.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBatch wWin
ON VALUE-CHANGED OF brwBatch IN FRAME fMain
DO:
  displayBatch().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState wWin
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
 publish "SetCurrentValue" ("ViewState", self:screen-value).
 calcTotals().
 run filterBatches in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStatus wWin
ON VALUE-CHANGED OF fStatus IN FRAME fMain /* Status */
DO:
  publish "SetCurrentValue" ("ViewStatus", self:screen-value).
  calcTotals().
  run filterBatches in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About wWin
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionAttachment
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionAttachment wWin
ON CHOOSE OF MENU-ITEM m_ActionAttachment /* View Documents */
DO:
  run ActionAttachment in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionDelete wWin
ON CHOOSE OF MENU-ITEM m_ActionDelete /* Complete empty... */
DO:
 run ActionDelete in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionInvoice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionInvoice wWin
ON CHOOSE OF MENU-ITEM m_ActionInvoice /* Invoice... */
DO:
  run ActionToComplete in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionModify wWin
ON CHOOSE OF MENU-ITEM m_ActionModify /* Modify details... */
DO:
  run ActionModify in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionMoveToPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionMoveToPeriod wWin
ON CHOOSE OF MENU-ITEM m_ActionMoveToPeriod /* Move to another period... */
DO:
  run ActionMovePeriod in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionNew wWin
ON CHOOSE OF MENU-ITEM m_ActionNew /* Add new... */
DO:
  run ActionNew in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionProcessing
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionProcessing wWin
ON CHOOSE OF MENU-ITEM m_ActionProcessing /* Processing data entry */
DO:
  run ActionProcessing in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionRefresh wWin
ON CHOOSE OF MENU-ITEM m_ActionRefresh /* Refresh */
DO:
  run ActionSvrRefresh in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionSplit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionSplit wWin
ON CHOOSE OF MENU-ITEM m_ActionSplit /* Split... */
DO:
 run ActionSplit in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionToProcess
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionToProcess wWin
ON CHOOSE OF MENU-ITEM m_ActionToProcess /* Start processing */
DO:
  run ActionToProcess in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionToReview
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionToReview wWin
ON CHOOSE OF MENU-ITEM m_ActionToReview /* Review batch */
DO:
  run ActionToReview in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_ActionValidate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_ActionValidate wWin
ON CHOOSE OF MENU-ITEM m_ActionValidate /* Validate... */
DO:
  run ActionValidate in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents wWin
ON CHOOSE OF MENU-ITEM m_Agents /* Agents */
DO:
  if valid-handle(hAgent) 
   then run ShowWindow in hAgent no-error.
   else run referenceagent.w persistent set hAgent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Accounting
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Accounting wWin
ON CHOOSE OF MENU-ITEM m_Agent_Accounting /* Agent Accounting */
DO:
 run rpt/agentaccounting.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Batch_Activity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Batch_Activity wWin
ON CHOOSE OF MENU-ITEM m_Agent_Batch_Activity /* Agent Batch Activity */
DO:
 run rpt/agentbatchactivity.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Data_Call
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Data_Call wWin
ON CHOOSE OF MENU-ITEM m_Agent_Data_Call /* Agent Data Call */
DO:
 run rpt/agentdatacall.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_File
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_File wWin
ON CHOOSE OF MENU-ITEM m_Agent_File /* File Details */
DO:
 if available batch 
  then publish "SetCurrentValue" ("AgentID", batch.agentID).
  else publish "SetCurrentValue" ("AgentID", "").
 publish "SetCurrentValue" ("FileNumber", "").
 run wfile.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_For_Period
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_For_Period wWin
ON CHOOSE OF MENU-ITEM m_Agent_For_Period /* Agents for Period */
DO:
 run rpt/agentforperiod.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_All
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_All wWin
ON VALUE-CHANGED OF MENU-ITEM m_All /* All */
DO:
 if activeType = "A"
  then 
   do: self:checked = true.
       return.
   end.
 fStatus:screen-value in frame fMain = "A".
 apply "value-changed" to fStatus in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Attorneys
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Attorneys wWin
ON CHOOSE OF MENU-ITEM m_Attorneys /* Attorneys */
do:
  if valid-handle(hAttorney) 
   then 
    run ShowWindow in hAttorney no-error.
  else
   run wattorneys.w persistent set hAttorney.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Batch_Activity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Batch_Activity wWin
ON CHOOSE OF MENU-ITEM m_Batch_Activity /* Batch Activity */
DO:
    if valid-handle(hActivity) 
     then run ShowWindow in hActivity no-error.
     else run wbatchactivity.w persistent set hActivity.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Batch_Counts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Batch_Counts wWin
ON CHOOSE OF MENU-ITEM m_Batch_Counts /* Batch Counts */
DO:
  RUN ActionBatchCounts IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Batch_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Batch_Details wWin
ON CHOOSE OF MENU-ITEM m_Batch_Details /* Batch Details */
DO:
  run wops01-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Complete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Complete wWin
ON VALUE-CHANGED OF MENU-ITEM m_Complete /* Complete */
DO:
 if activeType = "C"
  then 
   do: self:checked = true.
       return.
   end.
 fStatus:screen-value in frame fMain = "C".
 apply "value-changed" to fStatus in frame fMain.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure wWin
ON CHOOSE OF MENU-ITEM m_Configure /* Configure... */
DO:
  if valid-handle(hConfig) 
   then run ShowWindow in hConfig no-error.
   else run dialogopsconfig.w persistent set hConfig.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Counties
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Counties wWin
ON CHOOSE OF MENU-ITEM m_Counties /* Counties */
DO:
  if valid-handle(hCounty) 
   then run ShowWindow in hCounty no-error.
   else run referencecounty.w persistent set hCounty.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Data_Call
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Data_Call wWin
ON CHOOSE OF MENU-ITEM m_Data_Call /* Business Mix (Data Call) */
DO:
  run rpt/businessmix.w persistent.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Default_Endorsements
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Default_Endorsements wWin
ON CHOOSE OF MENU-ITEM m_Default_Endorsements /* Default Endorsements */
DO:
  run wdefend.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit wWin
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Forms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Forms wWin
ON CHOOSE OF MENU-ITEM m_Forms /* State Products */
DO:
  if valid-handle(hForm) 
   then run ShowWindow in hForm no-error.
   else run wform.w persistent set hForm.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_GoToPeriod
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_GoToPeriod wWin
ON CHOOSE OF MENU-ITEM m_GoToPeriod /* Period... */
DO:
  run ActionGoToPeriod in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_HOI_Policies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_HOI_Policies wWin
ON CHOOSE OF MENU-ITEM m_HOI_Policies /* HOI Policies */
DO:
  if valid-handle(hHOI) 
   then run ShowWindow in hHOI no-error.
   else run whoi.w persistent set hHOI.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Hold_Open_Policies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Hold_Open_Policies wWin
ON CHOOSE OF MENU-ITEM m_Hold_Open_Policies /* Hold Open Policies */
DO:
  run wholdopen.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Invoicing
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Invoicing wWin
ON VALUE-CHANGED OF MENU-ITEM m_Invoicing /* Reviewing */
DO:
 if activeType = "R"
  then 
   do: self:checked = true.
       return.
   end.
 fStatus:screen-value in frame fMain = "R".
 apply "value-changed" to fStatus in frame fMain.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_LockUnlock
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_LockUnlock wWin
ON CHOOSE OF MENU-ITEM m_LockUnlock /* Lock/Unlock */
DO:
  run ActionLock in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open wWin
ON VALUE-CHANGED OF MENU-ITEM m_Open /* New */
DO:
  if activeType = "N"
   then 
    do: self:checked = true.
        return.
    end.
  fStatus:screen-value in frame fMain = "N".
  apply "value-changed" to fStatus in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Paid_Files_with_a_Negative_
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Paid_Files_with_a_Negative_ wWin
ON CHOOSE OF MENU-ITEM m_Paid_Files_with_a_Negative_ /* Paid Files with a Negative Balance */
DO:
  run woverpaymentfile.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Paid_Files_with_a_Positive_
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Paid_Files_with_a_Positive_ wWin
ON CHOOSE OF MENU-ITEM m_Paid_Files_with_a_Positive_ /* Paid Files with a Positive Balance */
DO:
  run wunderpaidfile.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Periods
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Periods wWin
ON CHOOSE OF MENU-ITEM m_Periods /* Periods */
DO:
  if valid-handle(hPeriod) 
   then run ShowWindow in hPeriod no-error.
   else run referenceperiod.w persistent set hPeriod.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_Details
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_Details wWin
ON CHOOSE OF MENU-ITEM m_Period_Details /* Period Details */
DO:
  run wops08-r.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Period_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Period_Summary wWin
ON CHOOSE OF MENU-ITEM m_Period_Summary /* Batches for Period */
DO:
 run wops02-r.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Pipeline
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Pipeline wWin
ON CHOOSE OF MENU-ITEM m_Pipeline /* Revenue Pipeline */
DO:
  run wops09e-r.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Policy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Policy wWin
ON CHOOSE OF MENU-ITEM m_Policy /* Policy Details */
DO:
    publish "SetCurrentValue" ("PolicyID", "").
    run wpolicy.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Processing
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Processing wWin
ON VALUE-CHANGED OF MENU-ITEM m_Processing /* Processing */
DO:
 if activeType = "P"
  then 
   do: self:checked = true.
       return.
   end.
 fStatus:screen-value in frame fMain = "P".
 apply "value-changed" to fStatus in frame fMain.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Processing_Error_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Processing_Error_Codes wWin
ON CHOOSE OF MENU-ITEM m_Processing_Error_Codes /* Processing Error Codes */
DO:
    std-lo = false.
    publish "IsProcessingErrorsOpen" (output std-lo).
    if std-lo 
     then return.
    run wproerr.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reinsurance
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reinsurance wWin
ON CHOOSE OF MENU-ITEM m_Reinsurance /* Reinsurance */
DO:
  run rpt/reinsurance.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Reprocessed_Forms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Reprocessed_Forms wWin
ON CHOOSE OF MENU-ITEM m_Reprocessed_Forms /* Reprocessed Forms */
DO:
  run wops17-r.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Revenue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Revenue wWin
ON CHOOSE OF MENU-ITEM m_Revenue /* Revenue Types */
DO:
  run openRevenue in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_States
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_States wWin
ON CHOOSE OF MENU-ITEM m_States /* States */
DO:
    if valid-handle(hState) 
     then run ShowWindow in hState no-error.
     else run referencestate.w persistent set hState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_State_Batch_Activity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_State_Batch_Activity wWin
ON CHOOSE OF MENU-ITEM m_State_Batch_Activity /* State Batch Activity */
DO:
 run rpt/statebatchactivity.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_State_Data_Call
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_State_Data_Call wWin
ON CHOOSE OF MENU-ITEM m_State_Data_Call /* State Data Call */
DO:
  run wops13e-r.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Statutory_Liability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Statutory_Liability wWin
ON CHOOSE OF MENU-ITEM m_Statutory_Liability /* Statutory Liability */
DO:
 run rpt/statliab.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_STAT_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_STAT_Codes wWin
ON CHOOSE OF MENU-ITEM m_STAT_Codes /* STAT Codes */
DO:
 if valid-handle(hStatCode) 
  then run ShowWindow in hStatCode no-error.
  else run wstatcode.w persistent set hStatCode.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Transaction_Summary
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Transaction_Summary wWin
ON CHOOSE OF MENU-ITEM m_Transaction_Summary /* Transaction Summary */
DO:
  run rpt/transactionsummary.w persistent.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unreported_Policies
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unreported_Policies wWin
ON CHOOSE OF MENU-ITEM m_Unreported_Policies /* Unreported Policies */
DO:
  run rpt/unreportedpolicies.w persistent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */


{lib/win-main.i}
{lib/brw-main.i}

subscribe to "TIMER" anywhere.
subscribe to "WindowOpened" anywhere.
subscribe to "WindowClosed" anywhere.
subscribe to "BatchChanged" anywhere.
subscribe to "BatchesChanged" anywhere.
subscribe to "BatchTotalsChanged" anywhere.
subscribe to "ActionWindowForAgent" anywhere.

/* The *DataChanged events result from ESB messages */
subscribe to "BatchDataChanged" anywhere.
subscribe to "PeriodDataChanged" anywhere.

subscribe to "ActionNewHoldOpen" anywhere.

/* double-clicking a dynamic browse */
subscribe to "AgentSelected" anywhere.

{&window-name}:max-width-pixels = session:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.

browse brwBatch:POPUP-MENU = MENU batchpopmenu:HANDLE.


/* 1/12/2015 (BEJ) Tried to force a refresh when the user resumes focus
on this window, but trapping the mouse events is flaky.  So, I just
added a refresh button to the window */

/*ON 'entry':U anywhere*/
/* on 'mouse-select-up':U anywhere                                       */
/* DO:                                                                   */
/* /*   hide message no-pause.                                       */  */
/* /*   message last-event:label focus:type focus:name string(now).  */  */
/*                                                                       */
/*     /* ActionRefresh will display the Period as well */               */
/*   if tNeedToRefreshBatches                                            */
/*   then run ActionRefresh in this-procedure (output std-lo).           */
/*   else                                                                */
/*   if tNeedToRefreshPeriod                                             */
/*   then displayPeriod().                                               */
/* END.                                                                  */


PAUSE 0 BEFORE-HIDE. /* Best default for GUI applications */

{src/adm2/windowmn.i}

 publish "SetSplashStatus".

 run addButton in h_toolbar
   ("gotoperiod", "period", "View a different period", "images/cal.bmp", "", "images\cal-i.bmp", 
    false, true, this-procedure, menu-item m_GoToPeriod:handle in menu m_View).  
 run addButton in h_toolbar
   ("new", "New", "Add a new batch", "images/add.bmp", "", "images/add-i.bmp", 
    false, false, this-procedure, menu-item m_ActionNew:handle in menu m_Batch).  
 run addButton in h_toolbar 
   ("modify", "modify", "Modify batch details", "images/update.bmp", "", "images/update-i.bmp",
    false, false, this-procedure, menu-item m_ActionModify:handle in menu m_Batch).
 run addButton in h_toolbar
   ("lock", "", "Lock the batch (prevent status changes)", "images/lock.bmp", "", "images/lock-i.bmp", 
    false, true, this-procedure, menu-item m_LockUnlock:handle in menu m_Batch).  
 run addButton in h_toolbar
   ("attachment", "Attachment", "View documents", "images/attach.bmp", "", "images/attach-i.bmp", 
    false, true, this-procedure, menu-item m_ActionAttachment:handle in menu m_Lookup).  
 run addButton in h_toolbar
   ("toProcess", "ToProcessing", "Start processing (change status to Processing", "images/process.bmp", "", "images/process-i.bmp", 
    false, true, this-procedure, menu-item m_ActionToProcess:handle in menu m_Batch).  
 run addButton in h_toolbar
   ("processing", "Processing", "Processing data entry", "images/keyboard.bmp", "", "images/keyboard-i.bmp", 
    false, true, this-procedure, menu-item m_ActionProcessing:handle in menu m_Batch).  
 run addButton in h_toolbar
   ("toReview", "ToReviewing", "Review batch (change status to Reviewing)", "images/check.bmp", "", "images/check-i.bmp", 
    false, true, this-procedure, menu-item m_ActionToReview:handle in menu m_Batch).

 run addButton in h_toolbar
   ("validate", "Validate", "Validate processing", "images/spellcheck.bmp", "", "images/spellcheck-i.bmp",
    false, true, this-procedure, menu-item m_ActionValidate:handle in menu m_Batch).
 
 run addButton in h_toolbar
   ("split", "Split", "Split errors into a new batch", "images/split.bmp", "", "images/split-i.bmp", 
    false, true, this-procedure, menu-item m_ActionSplit:handle in menu m_Batch).

 run addButton in h_toolbar
   ("toComplete", "ToComplete", "Complete batch (change status to Complete)", "images/dollar.bmp", "", "images/dollar-i.bmp", 
    false, true, this-procedure, menu-item m_ActionInvoice:handle in menu m_Batch).  
 run addButton in h_toolbar
   ("moveperiod", "MovePeriod", "Move batch to another period", "images/movedate.bmp", "", "images/movedate-i.bmp", 
    false, true, this-procedure, menu-item m_ActionMoveToPeriod:handle in menu m_Batch).  
 run addButton in h_toolbar
   ("delete", "Delete", "Cancel the empty batch (change status to Complete)�.", "images/delete.bmp", "", "images/delete-i.bmp", 
    false, true, this-procedure, menu-item m_ActionDelete:handle in menu m_Batch).  
 
 run addButton in h_toolbar
   ("export", "Export to Excel", "Export batches to a CSV file", "images/excel.bmp", "", "images/excel-i.bmp",
    false, true, this-procedure, ?).
 run addButton in h_toolbar
   ("svrRefresh", "Refresh Data", "Refresh data", "images/refresh.bmp", "", "images/refresh-i.bmp",
    false, true, this-procedure, ?).

 publish "SetSplashStatus".

 {&window-name}:window-state = 2.
 run initializeObject.

 /* Set default period */
 publish "GetStartPeriod" (output std-ch).
 if std-ch = "U" 
  then publish "GetLastPeriod" (output activeMonth, output activeYear).

 /* "C" option or invalid values in config */
 if activeMonth < 0 
    or activeMonth > 12
    or activeYear < 2012
    or activeYear > 9999 /* Wow, that's optimistic! */
  then assign
         activeMonth = month(today)
         activeYear = year(today)
         .

 publish "GetPeriodID" (activeMonth, activeYear, output activePeriodID).
 publish "SetLastPeriod" (activeMonth, activeYear).
 publish "SetCurrentValue" ("PeriodMonth", string(activeMonth)).
 publish "SetCurrentValue" ("PeriodYear", string(activeYear)).
 publish "SetCurrentValue" ("PeriodID", string(activePeriodID)).

 if activePeriodID > 0 then
 publish "LoadBatches" (activePeriodID). 

  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-state-list.i &combo=fState &addAll=true}
    /* set the values */
    {lib/set-current-value.i &state=fState}
  end.

 std-ch = "".
 publish "GetCurrentValue" ("ViewStatus", output std-ch).
 if index("ANPRC", std-ch) = 0 
  then std-ch = "A".
 fStatus:screen-value in frame fMain = std-ch.

 publish "SetSplashStatus".
 displayPeriod().

 publish "SetSplashStatus".
 run filterBatches.
 run sortData("batchID").
 run enableActions in h_toolbar ("gotoperiod,svrRefresh,config").
 

  std-ha = getColumn(browse {&browse-name}:handle, "agentName").
  IF VALID-HANDLE(std-ha)
   THEN tColumnWidth = std-ha:width * session:pixels-per-column.

 hideUpdateMessage().

 {&window-name}:window-state = 3.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAttachment wWin 
PROCEDURE ActionAttachment :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def output parameter pError as logical init false.

  if not available batch 
   then return.

  if not valid-handle(hDocs)
   then run repository-lite.w persistent set hDocs ("Batch", string(batch.batchID)).
   else run ShowWindow in hDocs.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionBatchActivity wWin 
PROCEDURE ActionBatchActivity :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
    if valid-handle(hActivity) 
     then run ShowWindow in hActivity no-error.
     else run wbatchactivity.w persistent set hActivity.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionBatchCounts wWin 
PROCEDURE ActionBatchCounts :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  RUN wops21-r.w PERSISTENT.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionBatchDetails wWin 
PROCEDURE ActionBatchDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run wops01-r.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionBatchReassignPolicies wWin 
PROCEDURE ActionBatchReassignPolicies :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tComments as char no-undo.
 def var tGo as logical no-undo.

 if not available batch 
  then return.

 run dialogreassignpolicies.w (batch.batchID,
                               batch.agentID,
                               output tComments,
                               output tGo).

 if not tGo 
  then return.

 std-lo = false.
 std-ch = "".
 publish "ReassignPolicies" (batch.batchID,
                             tComments,
                             output std-lo,
                             output std-ch).

 if not std-lo 
  then
   do: 
       MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 message 
  "Policies reassigned successfully." skip
  "Please re-run validation on the batch."
  view-as alert-box info.

 run ActionRefresh in this-procedure (output std-lo).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete wWin 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 def var tComments as char no-undo.
 def var tDelete as logical no-undo.

 if not available batch 
  then return.

 run dialogdeletebatch.w (batch.batchID,
                          output tComments,
                          output tDelete).

 if not tDelete 
  then return.

 publish "DeleteBatch" (batch.batchID,
                        tComments,
                        output std-lo,
                        output std-ch).

 if not std-lo 
  then
   do: 
     message std-ch
         view-as alert-box error buttons ok.
     return.
   end.

 batch.stat = "C".
 browse brwBatch:delete-selected-rows().

 message "Batch cancelled successfully."
   view-as alert-box info buttons ok.
 
 run ActionRefresh in this-procedure (output std-lo).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionExport wWin 
PROCEDURE ActionExport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.

 def var th as handle.
 th = temp-table batch:handle.

 run util/exporttable.p
   (table-handle th,
    "batch",
    "for each batch " + batchWhereClause + batchByClause,
    "",
    "",
    "",
    "",
    yes,
    output std-ch,
    output std-in).
 if std-ch = ""
  then
   MESSAGE std-in " records exported."
    VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionGoToPeriod wWin 
PROCEDURE ActionGoToPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pSelect as logical init false.
 def var tYear as int.
 def var tMonth as int.

 assign
   tMonth = activeMonth
   tYear = activeYear
   .

 run dialogperiod.w (input-output tMonth,
                     input-output tYear,
                     output pSelect).
 if not pSelect or 
   (pSelect and tMonth = activeMonth and tYear = activeYear)
  then return.

 assign
   activeYear = tYear
   activeMonth = tMonth
   .
 
 publish "GetPeriodID" (activeMonth, activeYear, output activePeriodID).
 publish "SetLastPeriod" (activeMonth, activeYear).
 publish "SetCurrentValue" ("PeriodMonth", string(activeMonth)).
 publish "SetCurrentValue" ("PeriodYear", string(activeYear)).
 publish "SetCurrentValue" ("PeriodID", string(activePeriodID)).
 
 publish "LoadBatches" (activePeriodID).  

 displayPeriod().
 run filterBatches.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionLock wWin 
PROCEDURE ActionLock :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 if not available batch 
  then return.

 if batch.lockedBy = ""
  then
   do:
       publish "LockBatch" (batch.batchID,
                            output std-lo,
                            output std-ch).
       if std-lo 
        then 
         do: publish "GetCredentialsID" (output std-ch).
             batch.lockedBy = std-ch.
             displayBatch().
         end.
        else
         do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX warning BUTTONS OK.
         end.
   end.
  else
   do:
       publish "UnlockBatch" (batch.batchID,
                              output std-lo,
                              output std-ch).
       if std-lo 
        then 
         do: batch.lockedBy = "".
             displayBatch().
         end.
        else
         do:
          MESSAGE std-ch
           VIEW-AS ALERT-BOX warning BUTTONS OK.
         end.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify wWin 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 def var tDateRcvd as datetime init now no-undo.
 def var tAgentID as char no-undo.
 def var tGrossPremium as deci no-undo.
 def var tNetPremium as decimal no-undo.
 def var tCash as decimal no-undo.
 def var tRcvdVia as char init "E" no-undo.
 def var tReference as char no-undo.

 def buffer x-batch for batch.

 if not available batch 
  then return.

 assign
   tDateRcvd = batch.receivedDate
   tAgentID = batch.agentID
   tGrossPremium = batch.grossPremiumReported
   tNetPremium = batch.netPremiumReported
   tCash = batch.cashReceived
   tRcvdVia = batch.rcvdVia
   tReference = batch.reference
   .

 run dialogmodifybatch.w (input batch.batchID,
                          input batch.stat,
                          input-output tDateRcvd,
                          input-output tAgentID,
                          input-output tGrossPremium,
                          input-output tNetPremium,
                          input-output tCash,
                          input-output tRcvdVia,
                          input-output tReference,
                          output pError).
 if pError 
  then return.

  publish "ModifyBatch"
    (input batch.batchID,
     input tDateRcvd,
     input tAgentID,
     input tGrossPremium,
     input tNetPremium,
     input tCash,
     input tRcvdVia,
     input tReference,
     output std-lo,
     output std-ch).
  if not std-lo 
   then
    do: 
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.
   
/*  /*das ActionModify testing*/                    */
/*  find x-batch                                    */
/*    where rowid(x-batch) = rowid(batch).          */
/*  assign                                          */
/*    x-batch.receivedDate = tDateRcvd              */
/*    x-batch.agentID = tAgentID                    */
/*    x-batch.grossPremiumReported = tGrossPremium  */
/*    x-batch.netPremiumReported = tNetPremium      */
/*    x-batch.cashReceived = tCash                  */
/*    x-batch.rcvdVia = tRcvdVia                    */
/*    x-batch.reference = tReference                */
/*    .                                             */

 run ActionRefresh in this-procedure (output std-lo).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionMovePeriod wWin 
PROCEDURE ActionMovePeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pSelect as logical init false.
 
 def var tMonth as int no-undo.
 def var tYear as int no-undo.
 def var tPeriod as char no-undo.
 
 def var tComments as char no-undo.
 def var tDirection as char no-undo.
 def var tMoved as logical no-undo.
 def var tMsg as char no-undo.
 def var tNewBatchID as int no-undo.

 if not available batch 
  then return.

/*das:ActionMovePeriod*/

 run dialogmovebatch.w (input batch.batchID,
                        input batch.periodMonth,
                        input batch.periodYear,
                        output tDirection,
                        output tComments,
                        output std-lo).
 if not std-lo
  then return.

 publish "MoveBatch" (batch.batchID,
                      tDirection,
                      tComments,
                      output tMoved,
                      output tMsg,
                      output tNewBatchID).

 if not tMoved 
  then
   do: 
       MESSAGE tMsg
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.

 MESSAGE "Batch" batch.batchID "was moved to batch" tNewBatchID
  VIEW-AS ALERT-BOX info BUTTONS OK.
 
 delete batch.

 displayPeriod().
 run openBatchQuery in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew wWin 
PROCEDURE ActionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 def var tBatchID as int.

 def var tDateRcvd as datetime init now no-undo.
 def var tAgentID as char no-undo.
 def var tGrossPremium as deci no-undo.
 def var tNetPremium as decimal no-undo.
 def var tCash as decimal no-undo.
 def var tRcvdVia as char init "E" no-undo.
 def var tAttachment as char no-undo.
 def var tReference as char no-undo.
 def var dtPeriodFirstDate as date no-undo.
 def var dtPeriodLastDate as date no-undo.

 def buffer x-batch for batch.

dtPeriodFirstDate = date(activeMonth,1,activeYear).
 
if activeMonth <> 12
 then
  dtPeriodLastDate  = date(activeMonth + 1,1, activeYear) - 1.
 else
  dtPeriodLastDate  = date(1,1, activeYear + 1) - 1.
   
if dtPeriodFirstDate < today  and 
   dtPeriodLastDate > today
 then
  tDateRcvd = today.
 else
  tDateRcvd = dtPeriodLastDate.

publish "setcurrentvalue" ("selectedMonth" , string(activeMonth)).
publish "setcurrentvalue" ("selectedYear" , string(activeYear)).
  
run dialognewbatch.w (input-output tDateRcvd,
                       input-output tAgentID,
                       input-output tGrossPremium,
                       input-output tNetPremium,
                       input-output tCash,
                       input-output tRcvdVia,
                       input-output tAttachment,
                       input-output tReference,
                       output pError).
 if pError 
  then return.
  
 publish "NewBatch"
    (input activeMonth,
     input activeYear,
     input tDateRcvd,
     input tAgentID,
     input tGrossPremium,
     input tNetPremium,
     input tCash,
     input tRcvdVia,
     input tAttachment,
     input tReference,
     output std-lo,
     output std-ch,
     output tBatchID).
  if not std-lo 
   then
    do: 
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.
    
/*  MESSAGE "Batch created with ID: " tBatchID  */
/*   VIEW-AS ALERT-BOX INFO BUTTONS OK.         */
 
 /* Refresh data set and browse */
 run ActionRefresh in this-procedure (output std-lo).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNewHoldOpen wWin 
PROCEDURE ActionNewHoldOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def output parameter pError as logical init false.
 
 def var tAgentID as char no-undo.
 def var tPolicyID as int no-undo.

 def buffer x-batch for batch.

 run dialognewholdopen.w (input-output tAgentID,
                          output pError).
 if pError 
  then return.
  
 publish "NewHoldOpenPolicy"
    (input tAgentID,
     output std-lo,
     output std-ch,
     output tPolicyID).
  if not std-lo 
   then
    do: 
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        return.
    end.

 message
  "New Hold Open Policy created for Agent #" + tAgentID + " with ID: " + string(tPolicyID) 
  view-as alert-box info.

 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionProcessing wWin 
PROCEDURE ActionProcessing :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def output parameter pError as logical init false.

def var tBatchID as int no-undo.
def var tState as char no-undo.
def var tAgentID as char no-undo.
def var tAgentName as char no-undo.

 if not available batch 
  then return.
 
 assign
   tBatchID = batch.batchID
   tState = batch.stateID
   tAgentID = batch.agentID
   tAgentName = batch.agentName
   .

 if valid-handle(hProcessing) 
  then 
   do: run ShowWindow in hProcessing no-error.
       return.
   end.

 if batch.stat <> "P"
  then
   do: publish "ToProcessBatch" (tBatchID,
                                 output std-lo,
                                 output std-ch).

       if not std-lo
        then
         do:
             MESSAGE std-ch
              VIEW-AS ALERT-BOX warning BUTTONS OK.
             return.
         end.
   end.
 
 publish "LockBatch" (tBatchID,
                      output std-lo,
                      output std-ch).
 
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.
 
 if batch.stat <> "P"
  then
   do: batch.stat = "P".
       run ActionRefresh in this-procedure (output pError).
   end.

 activeBatchID = tBatchID. /* So reporting works as expected */
 activeAgentID = tAgentID.
 
 processBatchID = tBatchID. /* For proper refresh of browse upon
                               return from processing screen */
 
 publish "SetCurrentValue" ("BatchID", string(tBatchID)).
 publish "SetCurrentValue" ("AgentID", tAgentID).

 do with frame fMain:
     if fStatus:input-value <> "P" then
     do:
        fStatus:screen-value = "P".
        assign fStatus.
        apply "value-changed" to fStatus.
     end.
 end.
 
 run wprocessing.w persistent set hProcessing
    (tBatchID,
     tState,
     tAgentID).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionRefresh wWin 
PROCEDURE ActionRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.

 def var tBatchID as int.
 def var i as int init 0.
 def var r as rowid.
 def var f as int.
 
 def buffer xbatch for batch.

 do with frame {&frame-name}:
     
     /* Flag to disable 'displayBatch()' during reposition */
     tDisplayBatch = no.
     
     /* Grab currently selected row for repositioning. */ 
     if avail batch then
     do:
         tBatchID = batch.batchID no-error.
         r = rowid(batch) no-error.
         do i = 1 to {&browse-name}:num-iterations:
            if {&browse-name}:is-row-selected(i) then leave.
         end.
     end.
     
     displayPeriod().
     run filterBatches in this-procedure.
    
     /* Reposition the browse to previously selected record. */
     find xbatch where xbatch.batchID = tBatchID no-lock no-error.
     if avail xbatch then
     r = rowid(xbatch).

     {&browse-name}:set-repositioned-row(i,"ALWAYS") no-error.
     reposition {&browse-name} to rowid r no-error.
     f = {&browse-name}:get-repositioned-row() no-error.
     
     /* Turn 'displayBatch()' back on */
     tDisplayBatch = yes.
     
     /* Value-changed calls 'displayBatch()' */
     apply "value-changed" to brwBatch in frame fMain.
 
     tNeedToRefreshBatches = false.
     hideUpdateMessage().
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSplit wWin 
PROCEDURE ActionSplit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

 def var tMonth as int no-undo.
 def var tYear as int no-undo.
 def var tPeriod as char no-undo.
 
 def var tComments as char no-undo.
 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.
 def var tNewBatchID as int no-undo.

 if not available batch 
  then return.

/*das:ActionSplitPeriod*/

 run dialogsplitbatch.w (input batch.batchID,
                        input batch.periodMonth,
                        input batch.periodYear,
                        output tComments,
                        output std-lo).
 if not std-lo
  then return.

 publish "SplitBatch" (batch.batchID,
                       tComments,
                       output tSuccess,
                       output tMsg,
                       output tNewBatchID).

 if not tSuccess 
  then
   do: 
       MESSAGE tMsg
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.

 MESSAGE "Batch" batch.batchID "was split into batch" tNewBatchID
  VIEW-AS ALERT-BOX info BUTTONS OK.
 
 displayPeriod().
 run openBatchQuery in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSvrRefresh wWin 
PROCEDURE ActionSvrRefresh :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.

 if activePeriodID > 0 then
 publish "LoadBatches" (activePeriodID).

 run ActionRefresh in this-procedure (output std-lo).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionToComplete wWin 
PROCEDURE ActionToComplete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

if not available batch 
 then return.
 
publish "GetConfirmInvoice" (output std-lo).
if std-lo 
 then
  do: 
      MESSAGE "Batch will be completed and cannot be undone.  Continue?"
       VIEW-AS ALERT-BOX question BUTTONS Yes-No title "Confirm Completion"
        set std-lo.
      if not std-lo 
       then return.
  end.

publish "InvoiceBatch" (batch.batchID,
                         output std-lo,
                         output std-ch).

if not std-lo 
 then
  do:
      MESSAGE std-ch
       VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      return.
  end.

batch.stat = "C".
browse brwBatch:delete-selected-rows().

message "Batch completed successfully."
 view-as alert-box info buttons ok.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionToProcess wWin 
PROCEDURE ActionToProcess :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

if not available batch 
 then return.

publish "ToProcessBatch" (batch.batchID,
                          output std-lo,
                          output std-ch).

if not std-lo 
 then
  do:
      MESSAGE std-ch
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return.
  end.

  batch.stat = "P".

  browse brwBatch:delete-selected-rows().

MESSAGE "Batch is now in Processing."
 VIEW-AS ALERT-BOX INFO BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionToReview wWin 
PROCEDURE ActionToReview :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

if not available batch 
 then return.

if batch.grossPremiumReported <> batch.grossPremiumProcessed
  or batch.netPremiumReported <> batch.netPremiumProcessed
 then
  do: 
      MESSAGE "Batch totals do not match.  Continue?"
       VIEW-AS ALERT-BOX question BUTTONS Yes-No 
       title "Confirm Close Batch"
        set std-lo.
      if not std-lo
       then return.
  end.

publish "ToReviewBatch" (batch.batchID,
                         output std-lo,
                         output std-ch).

if not std-lo 
 then
  do:
      MESSAGE std-ch 
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return.
  end.

batch.stat = "R".

browse brwBatch:delete-selected-rows().

MESSAGE "Batch is now in Reviewing."
 VIEW-AS ALERT-BOX INFO BUTTONS OK.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionUnlock wWin 
PROCEDURE ActionUnlock :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 if not available batch 
  then return.

 publish "GetCredentialsID" (output std-ch).
 if batch.lockedBy <> std-ch 
  then publish "UnlockBatch" (batch.batchID,
                              output std-lo,
                              output std-ch).
  else 
   do: 
       MESSAGE "Batch is locked by " batch.lockedBy skip(2)
               "Continue?" 
        VIEW-AS ALERT-BOX question 
        BUTTONS YES-NO update std-lo.
       if not std-lo 
        then return.

       publish "UnlockBatchMgr" (batch.batchID,
                                 output std-lo,
                                 output std-ch).
   end.

 if std-lo 
  then 
   do: batch.lockedBy = "".
       displayBatch().
   end.
  else MESSAGE std-ch
         VIEW-AS ALERT-BOX warning BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionValidate wWin 
PROCEDURE ActionValidate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

def var tErrForms as int.
def var tWarnForms as int.
def var tGoodForms as int.

if not available batch 
 then return.

session:set-wait-state("general").

publish "ValidateBatch" (batch.batchID,
                         output std-lo,
                         output std-ch,
                         output tErrForms,
                         output tWarnForms,
                         output tGoodForms).

session:set-wait-state("").

if std-lo <> yes 
 then
    message std-ch view-as alert-box error.    
else
if tErrForms > 0 or tWarnForms > 0
 then 
    do:
        wWin:always-on-top = yes.   
        /* force window to top because validation report was opened in adobe */
        MESSAGE tErrForms " errors found" skip(1)
              tWarnForms " warnings found" skip(2)
              "Please review the validation report"
            VIEW-AS ALERT-BOX warning BUTTONS OK title "Validation".
        wWin:always-on-top = no.
    end.
else 
    MESSAGE "Batch validated with no errors or warnings"
       VIEW-AS ALERT-BOX info BUTTONS OK.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionViewReport wWin 
PROCEDURE ActionViewReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

def var tFile as char no-undo.

if not available batch 
 then return.

publish "GetTempFile" (input string(batch.batchID) + "validation",
                       output tFile).

if search(tFile) = ? 
 then
  do: 
      MESSAGE "Validation report does not exist" skip(1)
              "Please run validation again"
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.

RUN ShellExecuteA in this-procedure (0,
                            "open",
                            tFile,
                            "",
                            "",
                            1,
                            OUTPUT std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent wWin 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'toolbar.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'LogicalObjectNamePhysicalObjectNameDynamicObjectnoRunAttributeHideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_toolbar ).
       RUN repositionObject IN h_toolbar ( 1.14 , 30.80 ) NO-ERROR.
       /* Size in AB:  ( 2.00 , 109.40 ) */

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AgentSelected wWin 
PROCEDURE AgentSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  
  publish "ActionAgentModify" (pAgentID).  
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BatchChanged wWin 
PROCEDURE BatchChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

run ActionRefresh in this-procedure (output std-lo).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BatchDataChanged wWin 
PROCEDURE BatchDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
    Refresh batches on screen if processing screen is NOT open and the 
    user is looking at the main window.  The purpose of the second condition
    is to prevent Progress from bringing the window to top to update display.
------------------------------------------------------------------------------*/
 tNeedToRefreshBatches = true.

 if lookup("PROCESSING", activeAction) = 0 
   and focus = brwBatch:handle in frame {&frame-name}
  then run ActionRefresh in this-procedure (output std-lo).
  else showUpdateMessage().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BatchesChanged wWin 
PROCEDURE BatchesChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 if activeAction = "" 
  then
   do: publish "GetAutoUpdate" (output std-lo).
       if not std-lo
        then return.
       run ActionRefresh in this-procedure (output std-lo).
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE BatchTotalsChanged wWin 
PROCEDURE BatchTotalsChanged :
/*------------------------------------------------------------------------------
  Purpose:     Respond to changes in the data model batch totals
  Parameters:  <none>
  Notes:       Called after a publish event
------------------------------------------------------------------------------*/
def var tGross as decimal no-undo.
def var tNet as decimal no-undo.

 /* Called by data model when the batch form / batch aggregations change */
 if activeAction = "PROCESSING" 
    and available batch
  then 
   do: 
       publish "GetBatchAttribute" (batch.batchID,
                                    "GrossPremiumProcessed",
                                    output std-ch).
       tGross = decimal(std-ch).
       publish "GetBatchAttribute" (batch.batchID,
                                    "NetPremiumProcessed",
                                    output std-ch).
       tNet = decimal(std-ch).
       display tGross @ batch.grossPremiumProcessed 
               tNet @ batch.netPremiumProcessed 
         with browse brwBatch.
       batch.grossPremiumProcessed = tGross.
       batch.netPremiumProcessed = tNet.
       calcTotals().
       displayTotals().
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fState tTotalBatchCnt tReportedPremiumLabel tProcessedPremiumLabel 
          tVarianceLabel tRevenuePremiumLabel tVarianceLabel2 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE fState tTotalGrossRevenue tTotalGrossRevenueDelta tTotalGrossReported 
         tTotalGrossProcessed tTotalGrossDelta tTotalBatchCnt tTotalNetRevenue 
         tTotalNetRevenueDelta tTotalNetReported tTotalNetProcessed 
         tTotalNetDelta brwBatch 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterBatches wWin 
PROCEDURE filterBatches PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var pState as char no-undo.
/*  def var pType as char no-undo. */

 pState = fState:input-value in frame fMain.
 activeType = fStatus:input-value in frame fMain.

 assign
   menu-item m_Open:checked in menu m_View = false
   menu-item m_Processing:checked in menu m_View = false
   menu-item m_Invoicing:checked in menu m_View = false
   menu-item m_Complete:checked in menu m_View = false
   menu-item m_All:checked in menu m_View = false
   .

 case activeType:
  when "N" then assign
                batchWhereClause = " where batch.stat = 'N' "
                menu-item m_Open:checked in menu m_View = true
/*                 tEnabledActions = "new"                                  */
/*                 tDisabledActions = "validate,toreview,tocomplete,split"  */
                . 
  when "P" then assign
                batchWhereClause = " where batch.stat = 'P'  "
                menu-item m_Processing:checked in menu m_View = true
/*                 tEnabledActions = "split"                             */
/*                 tDisabledActions = "new,delete,toprocess,tocomplete"  */
                . 
  when "R" then assign
                batchWhereClause = " where batch.stat = 'R'  "
                menu-item m_Invoicing:checked in menu m_View = true
/*                 tEnabledActions = ""                                   */
/*                 tDisabledActions = "new,delete,modify,toreview,split"  */
                . 
  when "C" then assign
                batchWhereClause = " where batch.stat = 'C'  " 
                menu-item m_Complete:checked in menu m_View = true
/*                 tEnabledActions = ""                                                                                            */
/*                 tDisabledActions = "new,modify,delete,toprocess,processing,split,validate,toreview,tocomplete,moveperiod,lock"  */
                . 
  when "A" then assign
                batchWhereClause = ""
                menu-item m_All:checked in menu m_View = true
/*                 tEnabledActions = ""                                                                                            */
/*                 tDisabledActions = "new,modify,delete,toprocess,processing,split,validate,toreview,tocomplete,moveperiod,lock"  */
                . 
 end case.

 if pState <> "ALL" 
  then batchWhereClause = (if batchWhereClause > "" 
                            then batchWhereClause + "and " 
                            else "where ")
                         + "batch.stateID = '" + pState + "' ".
 
/*  run enableActions in h_toolbar (tEnabledActions).    */
/*  run disableActions in h_toolbar (tDisabledActions).  */
/*  activeType = pType. */

 if query brwBatch:is-open
  then query brwBatch:query-close().

 empty temp-table batch.
 empty temp-table batchaction.
 
 publish "GetBatches" (input activeMonth,
                       input activeYear,
                       output table batch).

 calcTotals().

 run openBatchQuery in this-procedure.

 displayTotals().

 setActionStatus().
 
 apply 'entry' to brwBatch.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openBatchQuery wWin 
PROCEDURE openBatchQuery :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var hQuery as handle.
 def var tWhere as char init "preselect each batch " no-undo.

 hQuery = query brwBatch:handle.
 if hQuery:is-open
  then hQuery:query-close().

 tWhere = tWhere + batchWhereClause + batchByClause.
 hQuery:query-prepare(tWhere).
 hQuery:query-open().
 
 tTotalBatchCnt:screen-value in frame fMain = string(hQuery:num-results).
 tTotalBatchCnt = hQuery:num-results.
 apply "value-changed" to brwBatch in frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openRevenue wWin 
PROCEDURE openRevenue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not valid-handle(hRevenueType) 
   then
    run wrevenuetype.w persistent set hRevenueType.
   else
    run showWindow in hRevenueType.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PeriodDataChanged wWin 
PROCEDURE PeriodDataChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
    Refresh period on screen if processing screen is NOT open and the 
    user is looking at the main window.  The purpose of the second condition
    is to prevent Progress from bringing the window to top to update display.
------------------------------------------------------------------------------*/
 tNeedToRefreshPeriod = true.
 if lookup("PROCESSING", activeAction) = 0 
   and active-window = this-procedure:current-window
  then displayPeriod().
  else showUpdateMessage().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 {lib/brw-sortData.i &ExcludeOpenQuery="yes"} 
 
 batchByClause = if pName <> ""
                 then " by " + pName + (if dataSortDesc then " descending" else "")
                 else "".
 
 run openBatchQuery in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowClosed wWin 
PROCEDURE WindowClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pWinAction as char.

 def var tGrossAmt as decimal.
 def var tNetAmt as decimal.
 def var tStat as char.

 if lookup(pWinAction, activeAction) > 0 
  then activeAction = trim(replace(activeAction, pWinAction, ""), ",").
 activeAction = replace(activeAction, ",,", ",").

 if available batch 
  then
   do:
       publish "GetBatchAttribute" (activeBatchID,
                                    "ProcessingStatus",
                                    output std-ch).
       batch.processStat = std-ch.
       
       publish "GetBatchAttribute" (activeBatchID,
                                    "LockedBy",
                                    output std-ch).
       batch.lockedBy = std-ch.
       
       publish "GetBatchAttribute" (activeBatchID,
                                    "Status",
                                    output std-ch).
       batch.stat = std-ch.
       
       displayBatch().
   end.

 if activeAction = "" 
  then 
   do: run EnableActions in h_toolbar (saveEnabledActions).
       saveEnabledActions = "".
       assign
         fState:sensitive in frame fMain = true
         fStatus:sensitive in frame fMain = true
         frame fMain:sensitive = true
         menu m_View:sensitive = true
         menu m_Batch:sensitive = true
         .
   end.

 /* Refresh browse if batch was moved to reviewing from processing screen */
 if pWinAction = "PROCESSING" then
 do:
    publish "GetBatchAttribute" (processBatchID,
                                 "Status",
                                 output std-ch).
    if std-ch <> "P" then
    apply 'value-changed' to fStatus in frame fMain.
 end.
   
 publish "TIMER".   /* Refresh data if messages received via ESB from other users */

 run ActionSvrRefresh in this-procedure (output std-lo).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowOpened wWin 
PROCEDURE WindowOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pWinAction as char.

 if lookup(pWinAction, activeAction) = 0 
  then activeAction = activeAction 
                       + (if activeAction > "" then "," else "")
                       + pWinAction.

 assign
   fState:sensitive in frame fMain = false
   fStatus:sensitive in frame fMain = false
   frame fMain:sensitive = false
   menu m_View:sensitive = false
   menu m_Batch:sensitive = false
   .
 if saveEnabledActions = "" 
  then
   do: run GetEnabledActions in h_toolbar (output saveEnabledActions).
       run DisableActions in h_toolbar (saveEnabledActions).
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized wWin 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = max(244, {&window-name}:height-pixels).
 frame {&frame-name}:virtual-height-pixels = max(244, {&window-name}:height-pixels).

 /* Changing browse dimensions caused a reopen of the query so initialize the counters to zero */
/*  assign                      */
/*    rowCnt = 0                */
/*    tTotalGrossReported = 0   */
/*    tTotalNetReported = 0     */
/*    tTotalGrossProcessed = 0  */
/*    tTotalNetProcessed = 0    */
/*    tTotalGrossDelta = 0      */
/*    tTotalNetDelta = 0        */
/*    .                         */
 
 brwBatch:width-pixels = max(50, frame {&frame-name}:width-pixels - 6).
 brwBatch:height-pixels = max(100, frame {&frame-name}:height-pixels - 131).

/*  display                   */
/*    tPeriodDesc             */
/*    tReportedPremiumLabel   */
/*    tProcessedPremiumLabel  */
/*                            */
/*    tPeriodClosed           */
/*    tTotalBatchCnt          */
/*    tTotalGrossReported     */
/*    tTotalNetReported       */
/*    tTotalGrossProcessed    */
/*    tTotalNetProcessed      */
/*    tTotalGrossDelta        */
/*    tTotalNetDelta          */
/*   with frame fMain.        */
 pause 0.
 
  std-ha = getColumn({&browse-name}:handle, "agentName").
  IF VALID-HANDLE(std-ha) 
   then std-ha:width-pixels = tColumnWidth + (frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels).
   
 apply "value-changed" to brwBatch.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calcTotals wWin 
FUNCTION calcTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 publish "GetPeriodTotals" (activeMonth,
                            activeYear,
                            fState:input-value in frame fMain,
                            fStatus:input-value in frame fMain,
                            output tTotalGrossReported,
                            output tTotalNetReported,
                            output tTotalGrossProcessed,
                            output tTotalNetProcessed,
                            output tTotalGrossRevenue,
                            output tTotalNetRevenue,
                            output tTotalBatchCnt).
 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayBatch wWin 
FUNCTION displayBatch RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
 if not tDisplayBatch then
 return true.
 
 if not available batch 
  then
   do: run disableActions in h_toolbar("lock").
       activeBatchID = 0.
       activeAgentID = "".
       publish "SetCurrentValue" ("BatchID", "").
       publish "SetCurrentValue" ("AgentID", "").
       return true.
   end.

 activeBatchID = batch.batchID.
 activeAgentID = batch.agentID.
 publish "SetCurrentValue" ("BatchID", string(batch.batchID)).

 
/* das: Should this publish be here?  It seems like it should be in an event
        that actually changes the batch with focus.  This function may be called
        even though the active batch hasn't changed.
 */
 publish "ActiveBatchChanged" (string(batch.batchID)).


 if activeAction = "" /* No pseudo-modal child windows open */
  then
   do:
       if activeType <> "A" and batch.stat <> "C"                            
        then 
         do: run enableActions in h_toolbar("lock").   
             if batch.lockedBy > "" 
              then run ChangeActionUI in h_toolbar ("lock", "images/unlock.bmp", ?, "Unlock the batch (allow status changes)").
              else run ChangeActionUI in h_toolbar ("lock", "images/lock.bmp", ?, "Lock the batch (prevent status changes)").
         end.
        else run disableActions in h_toolbar("lock").  
   end.

  if activeType = "P" 
   then
    case batch.processStat:
     when "R" then 
      assign
        batch.batchID:bgcolor in browse brwBatch = 12
        batch.batchID:fgcolor in browse brwBatch = 15
        .
     when "G" then 
      assign
        batch.batchID:bgcolor in browse brwBatch = 2
        batch.batchID:fgcolor in browse brwBatch = 15
        .
     when "Y" then 
      assign
        batch.batchID:bgcolor in browse brwBatch = 14
        batch.batchID:fgcolor in browse brwBatch = ?
        .

     otherwise 
     assign
       batch.batchID:bgcolor in browse brwBatch = batch.stat:bgcolor in browse brwBatch
       batch.batchID:fgcolor in browse brwBatch = batch.stat:fgcolor in browse brwBatch
       .
    end case.

 batch.stat:screen-value = batch.stat
    + (if batch.lockedBy > "" then "L" else "U")
    + batch.processStat.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayPeriod wWin 
FUNCTION displayPeriod RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if query brwBatch:is-open
  then close query brwBatch.

 empty temp-table batch.

 tPeriodDesc = getPeriodName(activeMonth, activeYear).
 tPeriodDesc:screen-value in frame fMain = tPeriodDesc.

 publish "IsPeriodOpen" (input activeMonth,
                         input activeYear,
                         output std-lo).

 if std-lo 
  then 
   do: 
/*        run enableActions in h_toolbar ("new,modify"). */
/*        activeType = 1.                               */
/*        fStatus:screen-value = "1".                    */
       assign
        fStatus:sensitive in frame fMain = true
        menu-item m_Open:sensitive in menu m_View = true
        menu-item m_Processing:sensitive in menu m_View = true
        menu-item m_Invoicing:sensitive in menu m_View = true
        menu-item m_Complete:sensitive in menu m_View = true
        menu-item m_All:sensitive in menu m_View = true
        tPeriodClosed:screen-value in frame fMain = ""
        tPeriodClosed = ""
        .
   end.
  else 
   do: run disableActions in h_toolbar ("new,modify").
       activeType = "C".
       fStatus:screen-value = "A".
       assign
        fStatus:sensitive in frame fMain = false
        menu-item m_Open:sensitive in menu m_View = false
        menu-item m_Processing:sensitive in menu m_View = false
        menu-item m_Invoicing:sensitive in menu m_View = false
        menu-item m_Complete:sensitive in menu m_View = false
        menu-item m_All:sensitive in menu m_View = true
        tPeriodClosed:screen-value in frame fMain = "Closed"
        tPeriodClosed = "Closed"
        .
   end.

  publish "GetBatches" (input activeMonth,
                        input activeYear,
                        output table batch).

  calcTotals().
  tNeedToRefreshPeriod = false.
  hideUpdateMessage().
  RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals wWin 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
 assign
  tTotalGrossDelta = (tTotalGrossProcessed - tTotalGrossReported)
  tTotalNetDelta = (tTotalNetProcessed - tTotalNetReported)
  tTotalGrossRevenueDelta = (tTotalGrossRevenue - tTotalGrossProcessed)
  tTotalNetRevenueDelta = (tTotalNetRevenue - tTotalNetProcessed)
  .

 if tTotalGrossDelta >= 0 
  then tTotalGrossDelta:fgcolor in frame fMain = 2.
  else tTotalGrossDelta:fgcolor in frame fMain = 12.
 if tTotalNetDelta >= 0 
  then tTotalNetDelta:fgcolor in frame fMain = 2.
  else tTotalNetDelta:fgcolor in frame fMain = 12.

 if tTotalGrossRevenueDelta >= 0 
  then tTotalGrossRevenueDelta:fgcolor in frame fMain = 2.
  else tTotalGrossRevenueDelta:fgcolor in frame fMain = 12.
 if tTotalNetRevenueDelta >= 0 
  then tTotalNetRevenueDelta:fgcolor in frame fMain = 2.
  else tTotalNetRevenueDelta:fgcolor in frame fMain = 12.
 
 display 
   tTotalGrossReported
   tTotalNetReported
   tTotalGrossProcessed
   tTotalNetProcessed
   tTotalGrossRevenue
   tTotalNetRevenue
   tTotalGrossDelta
   tTotalNetDelta
   tTotalGrossRevenueDelta
   tTotalNetRevenueDelta
   with frame fMain.
 
 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getPeriodName wWin 
FUNCTION getPeriodName RETURNS CHARACTER PRIVATE
  (input pMonth as int,
   input pYear as int) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if pMonth < 1 or pMonth > 12 
  then return "Unknown Month".
 if pYear < 2000 or pYear > 3000 
  then return "Unknown Year".

 return
    entry(pMonth, "January,February,March,April,May,June,July,August,September,October,November,December")
               + " " + string(pYear, "9999").
/*  return                                                                                                                                      */
/*     entry(pMonth, "  January,  February,    March,     April,     May,     June,     July,   August,September, October, November, December") */
/*                + " " + string(pYear, "9999").                                                                                                */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION hideUpdateMessage wWin 
FUNCTION hideUpdateMessage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 tNeedToRefreshMsg = "".
 status input "" in window {&window-name}.
 status default "" in window {&window-name}.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent wWin 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setActionStatus wWin 
FUNCTION setActionStatus RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 tDisabledActions = "".

 if query brwBatch:num-results > 0 
  then
   do:
       case activeType:
        when "N" then assign
                        menu-item m_PopModify:sensitive in menu batchpopmenu = true
                        menu-item m_PopProcessing:sensitive in menu batchpopmenu = true
                        menu-item m_PopValidate:sensitive in menu batchpopmenu = false
                        menu-item m_PopViewReport:sensitive in menu batchpopmenu = false
                        menu-item m_PopBatchDetails:sensitive in menu batchpopmenu = false
                        menu-item m_PopReassignPolicies:sensitive in menu batchpopmenu = false

                        tDisabledActions = "split,toreview,validate,tocomplete"
                        tEnabledActions = "new,modify,delete,attachment,toprocess,processing,moveperiod,export,svrRefresh"
                        .

        when "P" then assign
                        menu-item m_PopModify:sensitive in menu batchpopmenu = true
                        menu-item m_PopProcessing:sensitive in menu batchpopmenu = true
                        menu-item m_PopValidate:sensitive in menu batchpopmenu = true
                        menu-item m_PopViewReport:sensitive in menu batchpopmenu = true
                        menu-item m_PopBatchDetails:sensitive in menu batchpopmenu = true
                        menu-item m_PopReassignPolicies:sensitive in menu batchpopmenu = true

                        tDisabledActions = "new,delete,toprocess,tocomplete"
                        tEnabledActions = "modify,attachment,processing,validate,split,toreview,moveperiod,export,svrRefresh"
                        .
        when "R" then assign
                        menu-item m_PopModify:sensitive in menu batchpopmenu = false
                        menu-item m_PopProcessing:sensitive in menu batchpopmenu = true
                        menu-item m_PopValidate:sensitive in menu batchpopmenu = false
                        menu-item m_PopViewReport:sensitive in menu batchpopmenu = false
                        menu-item m_PopBatchDetails:sensitive in menu batchpopmenu = true
                        menu-item m_PopReassignPolicies:sensitive in menu batchpopmenu = false

                        tDisabledActions = "new,delete,split,validate,toreview"
                        tEnabledActions = "attachment,toprocess,processing,tocomplete,moveperiod,export,svrRefresh"
                        .
        otherwise assign
                        menu-item m_PopModify:sensitive in menu batchpopmenu = false
                        menu-item m_PopProcessing:sensitive in menu batchpopmenu = false
                        menu-item m_PopValidate:sensitive in menu batchpopmenu = false
                        menu-item m_PopViewReport:sensitive in menu batchpopmenu = false
                        menu-item m_PopBatchDetails:sensitive in menu batchpopmenu = true
                        menu-item m_PopReassignPolicies:sensitive in menu batchpopmenu = false

                        tDisabledActions = "new,modify,delete,toprocess,processing,validate,split,toreview,tocomplete,moveperiod,lock"
                        tEnabledActions = "attachment,export,svrRefresh"
                        .
       end case.
       assign
         menu-item m_PopBatchActivity:sensitive in menu batchpopmenu = true
         menu-item m_PopViewAttachment:sensitive in menu batchpopmenu = true
         .
   end.
  else assign
         tEnabledActions = (if activeType = "N" then "new" else "")
         tDisabledActions = "modify,delete,attachment,toprocess,processing,split,validate,toreview,tocomplete,moveperiod,lock,unlock,export,svrRefresh"
                                + (if activeType <> "N" then ",new" else "")
         menu-item m_PopModify:sensitive in menu batchpopmenu = false
         menu-item m_PopProcessing:sensitive in menu batchpopmenu = false
         menu-item m_PopValidate:sensitive in menu batchpopmenu = false
         menu-item m_PopViewReport:sensitive in menu batchpopmenu = false
         menu-item m_PopBatchDetails:sensitive in menu batchpopmenu = false
         menu-item m_PopBatchActivity:sensitive in menu batchpopmenu = false
         menu-item m_PopViewAttachment:sensitive in menu batchpopmenu = false
         menu-item m_PopReassignPolicies:sensitive in menu batchpopmenu = false
         .


 run enableActions in h_toolbar (tEnabledActions).
 run disableActions in h_toolbar (tDisabledActions).

 RETURN FALSE.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showUpdateMessage wWin 
FUNCTION showUpdateMessage RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if tNeedToRefreshMsg = ""
  then tNeedToRefreshMsg = "Update pending since " + string(now, "99/99/9999 HH:MM:SS").
        
 status input tNeedToRefreshMsg in window {&window-name}.
 status default tNeedToRefreshMSg in window {&window-name}.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

