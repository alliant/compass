&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wholdopen.w
   Window of Hold Open policies
   2.27.2015
   */

CREATE WIDGET-POOL.

{lib/std-def.i}

{tt/holdopen.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwCodes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES holdopen

/* Definitions for BROWSE brwCodes                                      */
&Scoped-define FIELDS-IN-QUERY-brwCodes holdopen.policyID holdopen.stat holdopen.agentID holdopen.agentName holdopen.stateID holdopen.fileNumber holdopen.issueDate holdopen.invoiceDate holdopen.periodID holdopen.reprocessed holdopen.formID holdopen.statcode holdopen.effDate holdopen.liabilityAmount holdopen.grossPremium holdopen.netPremium holdopen.retentionPremium holdopen.residential holdopen.countyID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCodes   
&Scoped-define SELF-NAME brwCodes
&Scoped-define QUERY-STRING-brwCodes FOR EACH holdopen
&Scoped-define OPEN-QUERY-brwCodes OPEN QUERY {&SELF-NAME} FOR EACH holdopen.
&Scoped-define TABLES-IN-QUERY-brwCodes holdopen
&Scoped-define FIRST-TABLE-IN-QUERY-brwCodes holdopen


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwCodes bNew 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New Hold Open Policy".

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwCodes FOR 
      holdopen SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCodes C-Win _FREEFORM
  QUERY brwCodes DISPLAY
      holdopen.policyID column-label "PolicyID" format ">>>>>>>9"
holdopen.stat column-label "Status" format "x(4)"
holdopen.agentID column-label "AgentID" format "x(8)"
holdopen.agentName column-label "Agent Name" format "x(40)"
holdopen.stateID column-label "State" format "x(4)"
holdopen.fileNumber column-label "File Number" format "x(30)" width 20
holdopen.issueDate column-label "Issue!Date" format "99/99/99"
holdopen.invoiceDate column-label "Invoice!Date" format "99/99/99"
holdopen.periodID column-label "PeriodID" format ">>>>>>>9"
holdopen.reprocessed column-label "Reprocess" format "Yes/No"
holdopen.formID column-label "FormID" format "x(12)"
holdopen.statcode column-label "STAT!Code" format "x(8)"
holdopen.effDate column-label "Effective!Date" format "99/99/99"
holdopen.liabilityAmount column-label "Liability!Amount" format "->>,>>>,>>9.99"
holdopen.grossPremium column-label "Gross!Premium" format "->>,>>>,>>9.99"
holdopen.netPremium column-label "Net!Premium" format "->>,>>>,>>9.99"
holdopen.retentionPremium column-label "Retention!Premium" format "->>,>>>,>>9.99"
holdopen.residential column-label "Res" format "Yes/No"
holdopen.countyID column-label "County" format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 259 BY 20.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwCodes AT ROW 3.14 COL 2 WIDGET-ID 200
     bNew AT ROW 1.19 COL 1.8 WIDGET-ID 48 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 260.8 BY 22.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Hold Open Policies"
         HEIGHT             = 22.86
         WIDTH              = 260.8
         MAX-HEIGHT         = 25.24
         MAX-WIDTH          = 260.8
         VIRTUAL-HEIGHT     = 25.24
         VIRTUAL-WIDTH      = 260.8
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwCodes 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwCodes:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwCodes:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCodes
/* Query rebuild information for BROWSE brwCodes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH holdopen.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwCodes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Hold Open Policies */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Hold Open Policies */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Hold Open Policies */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:

  def var tLoadedHoldOpens as logical init false no-undo.

  publish "ActionNewHoldOpen" (output std-lo).
  
  if not std-lo then
  do:
      tLoadedHoldOpens = true.
     
     run getData in this-procedure (tLoadedHoldOpens).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCodes
&Scoped-define SELF-NAME brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON ROW-DISPLAY OF brwCodes IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON START-SEARCH OF brwCodes IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.


/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bNew:load-image("images/new.bmp").

run getData (false).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE brwCodes bNew 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer holdopen for holdopen.
 def var tFile as char no-undo.

 def input parameter tLoadedHoldOpens as logical no-undo.

 close query {&browse-name}.
 empty temp-table holdopen.

if tLoadedHoldOpens then
do:
   publish "LoadHoldOpens".
   publish "GetHoldOpens" (output table holdopen).
end.

else
 
 publish "GetHoldOpens" (output table holdopen).

 dataSortBy = "".
 run sortData ("policyID").
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{lib/brw-sortData.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwCodes:width-pixels = frame fmain:width-pixels - 10.
 brwCodes:height-pixels = frame fMain:height-pixels - 10.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

