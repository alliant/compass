&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: 

  Description: from cntnrwin.w - ADM SmartWindow Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: New V9 Version - January 15, 1998
          
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input parameter pPolicyID as int.


/* Local Variable Definitions ---                                       */
CREATE WIDGET-POOL.


{src/adm2/widgetprto.i}

{lib/std-def.i}

{tt/batch.i}
{tt/batchform.i}
{tt/policy.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwBatch

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batch batchform

/* Definitions for BROWSE brwBatch                                      */
&Scoped-define FIELDS-IN-QUERY-brwBatch batch.batchID batch.receivedDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBatch   
&Scoped-define SELF-NAME brwBatch
&Scoped-define QUERY-STRING-brwBatch FOR EACH batch by batch.batchID
&Scoped-define OPEN-QUERY-brwBatch OPEN QUERY {&SELF-NAME} FOR EACH batch by batch.batchID.
&Scoped-define TABLES-IN-QUERY-brwBatch batch
&Scoped-define FIRST-TABLE-IN-QUERY-brwBatch batch


/* Definitions for BROWSE brwForm                                       */
&Scoped-define FIELDS-IN-QUERY-brwForm batchform.formType batchform.formID batchform.statcode batchform.effDate batchform.grossPremium batchform.netPremium   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwForm   
&Scoped-define SELF-NAME brwForm
&Scoped-define QUERY-STRING-brwForm FOR EACH batchform by batchform.seq
&Scoped-define OPEN-QUERY-brwForm OPEN QUERY {&SELF-NAME} FOR EACH batchform by batchform.seq.
&Scoped-define TABLES-IN-QUERY-brwForm batchform
&Scoped-define FIRST-TABLE-IN-QUERY-brwForm batchform


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwBatch}~
    ~{&OPEN-QUERY-brwForm}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwForm bEdit bDelete bView brwBatch 
&Scoped-Define DISPLAYED-OBJECTS tPolicyID tAgentID tStatus tAddr1 ~
tFileNumber tAddr-2 tFormID tStatCode tEffectiveDate tResidential tCountyID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_toolbar AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDelete 
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the Endorsement".

DEFINE BUTTON bEdit 
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the Endorsement".

DEFINE BUTTON bView 
     LABEL "View" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Batch Details".

DEFINE VARIABLE tAddr-2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.6 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49.6 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tCountyID AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 85.4 BY 1 NO-UNDO.

DEFINE VARIABLE tEffectiveDate as datetime FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 28.4 BY 1 NO-UNDO.

DEFINE VARIABLE tFormID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Form" 
     VIEW-AS FILL-IN 
     SIZE 85.6 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "$z,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium-2 AS DECIMAL FORMAT "$z,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "$>>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "$z,zzz,zz9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium-2 AS DECIMAL FORMAT "$z,zzz,zz9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicyID AS INTEGER FORMAT ">>>>>>>":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tRetention AS DECIMAL FORMAT "$z,zzz,zz9.99":U INITIAL 0 
     LABEL "Retention" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tRetention-2 AS DECIMAL FORMAT "$z,zzz,zz9.99":U INITIAL 0 
     LABEL "Retention" 
     VIEW-AS FILL-IN 
     SIZE 19.8 BY 1 NO-UNDO.

DEFINE VARIABLE tStatCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "STAT Code" 
     VIEW-AS FILL-IN 
     SIZE 85.4 BY 1 NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 4.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37.6 BY 3.71.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37.6 BY 3.71.

DEFINE VARIABLE tResidential AS LOGICAL INITIAL no 
     LABEL "Residential" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwBatch FOR 
      batch SCROLLING.

DEFINE QUERY brwForm FOR 
      batchform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBatch wWin _FREEFORM
  QUERY brwBatch DISPLAY
      batch.batchID label "Batch" format ">>>>>>>9"
batch.receivedDate label "Date"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 99.6 BY 5.71 FIT-LAST-COLUMN.

DEFINE BROWSE brwForm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwForm wWin _FREEFORM
  QUERY brwForm DISPLAY
      batchform.formType label "Type"
batchform.formID label "FormID" format "x(20)"
batchform.statcode label "STAT" format "x(10)"
batchform.effDate label "Eff Date"
batchform.grossPremium label "Gross"
batchform.netPremium label "Net"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 99.6 BY 5.71 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tPolicyID AT ROW 3.1 COL 14.2 COLON-ALIGNED WIDGET-ID 12
     tAgentID AT ROW 4 COL 50.2 COLON-ALIGNED WIDGET-ID 16
     tStatus AT ROW 4.29 COL 14.6 COLON-ALIGNED WIDGET-ID 74
     tAddr1 AT ROW 5.14 COL 50.2 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     tFileNumber AT ROW 6.24 COL 14.2 COLON-ALIGNED WIDGET-ID 14
     tAddr-2 AT ROW 6.24 COL 50.2 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     tFormID AT ROW 7.43 COL 14 COLON-ALIGNED WIDGET-ID 24
     tStatCode AT ROW 8.62 COL 14 COLON-ALIGNED WIDGET-ID 30
     tEffectiveDate AT ROW 9.76 COL 14.2 COLON-ALIGNED WIDGET-ID 34
     tLiability AT ROW 9.76 COL 45.6 COLON-ALIGNED WIDGET-ID 32
     tResidential AT ROW 9.81 COL 73.2 WIDGET-ID 36
     tCountyID AT ROW 10.86 COL 14.2 COLON-ALIGNED WIDGET-ID 38
     tGrossPremium-2 AT ROW 12.52 COL 69 COLON-ALIGNED WIDGET-ID 50
     tGrossPremium AT ROW 12.62 COL 32.4 COLON-ALIGNED WIDGET-ID 44
     tNetPremium-2 AT ROW 13.62 COL 69 COLON-ALIGNED WIDGET-ID 52
     tNetPremium AT ROW 13.71 COL 32.4 COLON-ALIGNED WIDGET-ID 46
     tRetention-2 AT ROW 14.76 COL 69 COLON-ALIGNED WIDGET-ID 54
     tRetention AT ROW 14.86 COL 32.4 COLON-ALIGNED WIDGET-ID 48
     brwForm AT ROW 17 COL 2.2 WIDGET-ID 200
     bEdit AT ROW 17 COL 102.6 WIDGET-ID 68
     bDelete AT ROW 18.67 COL 102.6 WIDGET-ID 70
     bView AT ROW 23.86 COL 102.6 WIDGET-ID 72
     brwBatch AT ROW 23.91 COL 2.2 WIDGET-ID 300
     "File" VIEW-AS TEXT
          SIZE 4.8 BY .62 AT ROW 12 COL 59.6 WIDGET-ID 56
          FONT 6
     "Endorsements" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 16.29 COL 2.2 WIDGET-ID 60
          FONT 6
     "Policy" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 12.05 COL 21.4 WIDGET-ID 66
          FONT 6
     "Batches" VIEW-AS TEXT
          SIZE 16.2 BY .62 AT ROW 23.19 COL 2.4 WIDGET-ID 62
          FONT 6
     RECT-4 AT ROW 12.29 COL 58.4 WIDGET-ID 58
     RECT-5 AT ROW 12.33 COL 20 WIDGET-ID 64
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 109.6 BY 28.86 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Policy"
         HEIGHT             = 28.86
         WIDTH              = 109.6
         MAX-HEIGHT         = 48.91
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.91
         VIRTUAL-WIDTH      = 384
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwForm tRetention fMain */
/* BROWSE-TAB brwBatch bView fMain */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-5 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAddr-2 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAddr1 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCountyID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tEffectiveDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tFileNumber IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tFormID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tGrossPremium-2 IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tLiability IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tNetPremium IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tNetPremium-2 IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tPolicyID IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tResidential IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRetention IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tRetention-2 IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tStatCode IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBatch
/* Query rebuild information for BROWSE brwBatch
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batch by batch.batchID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwBatch */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwForm
/* Query rebuild information for BROWSE brwForm
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchform by batchform.seq.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwForm */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Policy */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Policy */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBatch
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

 run initializeObject.

/*  run addButton in h_toolbar                                        */
/*    ("new", "New", "New", "images/new.bmp", "", "images/new-i.bmp", */
/*      true, true, source-procedure, ?).                             */
 run addButton in h_toolbar
   ("modify", "Modify", "Modify", "images/update.bmp", "", "images/update-i.bmp",
     true, true, source-procedure, ?).       
 run addButton in  h_toolbar
   ("save", "Save", "Save", "images/save.bmp", "", "images/save-i.bmp",
     false, true, source-procedure, ?).                
 run addButton in  h_toolbar
   ("cancel", "Cancel", "Cancel", "images/cancel.bmp", "", "images/cancel-i.bmp",
     false, true, source-procedure, ?).

 run addButton in  h_toolbar
   ("replace", "Replace", "Replace with a different Policy", "images/process.bmp", "", "images/process-i.bmp",
     false, true, source-procedure, ?).
 run addButton in  h_toolbar
   ("void", "Void", "Void", "images/rejected.bmp", "", "images/rejected-i.bmp",
     false, true, source-procedure, ?).

 bEdit:load-image("images/update.bmp").
 bEdit:load-image-insensitive("images/update-i.bmp").
 bDelete:load-image("images/delete.bmp").
 bDelete:load-image-insensitive("images/delete-i.bmp").

 bView:load-image("images/copy.bmp").
 bView:load-image-insensitive("images/copy-i.bmp").

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'toolbar.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'LogicalObjectNamePhysicalObjectNameDynamicObjectnoRunAttributeHideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_toolbar ).
       RUN repositionObject IN h_toolbar ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in AB:  ( 2.00 , 102.00 ) */

       /* Adjust the tab order of the smart objects. */
       RUN adjustTabOrder ( h_toolbar ,
             tPolicyID:HANDLE IN FRAME fMain , 'BEFORE':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tPolicyID tAgentID tStatus tAddr1 tFileNumber tAddr-2 tFormID 
          tStatCode tEffectiveDate tResidential tCountyID 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE brwForm bEdit bDelete bView brwBatch 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

