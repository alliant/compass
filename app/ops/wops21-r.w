&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wBatchCounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wBatchCounts 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

{tt/cpl.i}
{tt/cpl.i &tableAlias="allCPL"}
{tt/cplreport.i}
{tt/period.i}
{tt/batch.i &tableAlias="ttBatch"}
{tt/agent.i &tableAlias="ttAgent"}
{tt/state.i}
{lib/std-def.i}
{lib/get-column.i}
{lib/add-delimiter.i}
{lib/brw-multi-def.i}
{lib/winlaunch.i}
{lib/getstatename.i}
{lib/do-wait.i}
     
DEFINE VARIABLE iCounter AS INTEGER NO-UNDO INITIAL 0.
DEFINE VARIABLE iCounterMax AS INTEGER NO-UNDO.
DEFINE VARIABLE pCurrMonth AS INTEGER NO-UNDO INITIAL 0.
DEFINE VARIABLE pCurrYear AS INTEGER NO-UNDO INITIAL 0.
DEFINE VARIABLE pCurrBatch AS INTEGER NO-UNDO INITIAL 0.

/* resize variables */
DEFINE VARIABLE dMinTextAgentWidth AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinRectParamWidth AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinRectAgentWidth AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinBrowseCPLWidth AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinBrowseCPLHeight AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinBrowseCPLColFile AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinBrowseCountsWidth AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinBrowseCountsColPolicy AS DECIMAL NO-UNDO.
DEFINE VARIABLE dMinBrowseCountsColFile AS DECIMAL NO-UNDO.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fBatchCounts
&Scoped-define BROWSE-NAME brwCounts

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES cplReport cpl

/* Definitions for BROWSE brwCounts                                     */
&Scoped-define FIELDS-IN-QUERY-brwCounts cplReport.fileNumber cplReport.policyID cplReport.effDate cplReport.numberOfEndorsements cplReport.numberOfPolicies cplReport.numberOfCPLs   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCounts   
&Scoped-define SELF-NAME brwCounts
&Scoped-define QUERY-STRING-brwCounts FOR EACH cplReport NO-LOCK BY cplReport.fileNumber INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwCounts OPEN QUERY {&SELF-NAME} FOR EACH cplReport NO-LOCK BY cplReport.fileNumber INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwCounts cplReport
&Scoped-define FIRST-TABLE-IN-QUERY-brwCounts cplReport


/* Definitions for BROWSE brwCPL                                        */
&Scoped-define FIELDS-IN-QUERY-brwCPL cpl.cplid cpl.stat cpl.fileNumber cpl.NAME cpl.state cpl.issueDate cpl.voidDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCPL   
&Scoped-define SELF-NAME brwCPL
&Scoped-define QUERY-STRING-brwCPL FOR EACH cpl NO-LOCK BY cpl.issueDate INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brwCPL OPEN QUERY {&SELF-NAME} FOR EACH cpl NO-LOCK BY cpl.issueDate INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brwCPL cpl
&Scoped-define FIRST-TABLE-IN-QUERY-brwCPL cpl


/* Definitions for FRAME fBatchCounts                                   */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fBatchCounts ~
    ~{&OPEN-QUERY-brwCounts}~
    ~{&OPEN-QUERY-brwCPL}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rAgent rParam cmbMonth cmbYear bGo bExport ~
cmbState cmbStatus cmbBatch bFilterFileGo bFilterFileClear ~
tFilterFileNumber brwCounts brwCPL 
&Scoped-Define DISPLAYED-OBJECTS cmbMonth cmbYear cmbState cmbStatus ~
cmbBatch tAgentID tAgentState tAgentStatus tAgentName tFilterFileNumber ~
chkVoid 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData wBatchCounts 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus wBatchCounts 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus wBatchCounts 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitizeBrowseFilters wBatchCounts 
FUNCTION sensitizeBrowseFilters RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD sensitizeReportButtons wBatchCounts 
FUNCTION sensitizeReportButtons RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wBatchCounts AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bFilterFileClear 
     LABEL "Clear" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bFilterFileGo 
     LABEL "Go" 
     SIZE 4.8 BY 1.14.

DEFINE BUTTON bGo 
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE cmbBatch AS CHARACTER INITIAL "0" 
     LABEL "Batch" 
     VIEW-AS COMBO-BOX INNER-LINES 8
     LIST-ITEMS "Please select a batch id" 
     DROP-DOWN AUTO-COMPLETION
     SIZE 47 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE cmbMonth AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Month" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "Item 1",0
     DROP-DOWN-LIST
     SIZE 19 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE cmbState AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "State","-1"
     DROP-DOWN-LIST
     SIZE 19 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE cmbStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "ALL","ALL",
                     "New","N",
                     "Processing","P",
                     "Reviewing","R",
                     "Complete","C"
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE cmbYear AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 7
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 20 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "ID" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 108 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentStatus AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE tFilterFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE RECTANGLE rAgent
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 119 BY 3.33.

DEFINE RECTANGLE rParam
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 119 BY 5.

DEFINE VARIABLE chkVoid AS LOGICAL INITIAL no 
     LABEL "Show Voided" 
     VIEW-AS TOGGLE-BOX
     SIZE 16 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwCounts FOR 
      cplReport SCROLLING.

DEFINE QUERY brwCPL FOR 
      cpl SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwCounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCounts wBatchCounts _FREEFORM
  QUERY brwCounts NO-LOCK DISPLAY
      cplReport.fileNumber FORMAT "x(50)" WIDTH 30.3 LABEL "File Number"
    cplReport.policyID FORMAT "x(50)" WIDTH 25 LABEL "Policy ID"
    cplReport.effDate FORMAT "x(50)" WIDTH 20 LABEL "Effective Date"
          cplReport.numberOfEndorsements FORMAT "ZZ" WIDTH 15 COLUMN-LABEL "No. of!Endorsements"
          cplReport.numberOfPolicies FORMAT "ZZ" WIDTH 10 COLUMN-LABEL "No. of!Policies"
          cplReport.numberOfCPLs FORMAT "ZZ" WIDTH 10 COLUMN-LABEL "No. of!CPLs"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 119 BY 7.29
         TITLE "Counts".

DEFINE BROWSE brwCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCPL wBatchCounts _FREEFORM
  QUERY brwCPL NO-LOCK DISPLAY
      cpl.cplid FORMAT "x(10)" WIDTH 10 LABEL "CPL ID"
      cpl.stat FORMAT "x(15)" WIDTH 10 LABEL "Status"
      cpl.fileNumber FORMAT "x(50)" WIDTH 19.6 LABEL "File Number"
          cpl.NAME FORMAT "x(10)" WIDTH 15 LABEL "Type"
          cpl.state FORMAT "x(30)" WIDTH 15 LABEL "State"
          cpl.issueDate FORMAT "99/99/99" WIDTH 20 LABEL "Issue Date"
          cpl.voidDate FORMAT "99/99/99" WIDTH 20 LABEL "Void Date"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 119 BY 7.29
         TITLE "CPL".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fBatchCounts
     cmbMonth AT ROW 2.43 COL 11 COLON-ALIGNED WIDGET-ID 6
     cmbYear AT ROW 2.43 COL 38 COLON-ALIGNED WIDGET-ID 8
     bGo AT ROW 3.38 COL 62 WIDGET-ID 106
     bExport AT ROW 3.38 COL 70 WIDGET-ID 104
     cmbState AT ROW 3.62 COL 11 COLON-ALIGNED WIDGET-ID 84
     cmbStatus AT ROW 3.62 COL 38 COLON-ALIGNED WIDGET-ID 118
     cmbBatch AT ROW 4.81 COL 11 COLON-ALIGNED WIDGET-ID 88
     tAgentID AT ROW 7.67 COL 10 COLON-ALIGNED WIDGET-ID 126
     tAgentState AT ROW 7.67 COL 29 COLON-ALIGNED WIDGET-ID 130
     tAgentStatus AT ROW 7.67 COL 54 COLON-ALIGNED WIDGET-ID 132
     tAgentName AT ROW 8.86 COL 10 COLON-ALIGNED WIDGET-ID 128
     bFilterFileGo AT ROW 10.71 COL 47 WIDGET-ID 136
     bFilterFileClear AT ROW 10.71 COL 52.4 WIDGET-ID 134
     tFilterFileNumber AT ROW 10.76 COL 14 COLON-ALIGNED WIDGET-ID 110
     brwCounts AT ROW 12.05 COL 3 WIDGET-ID 200
     chkVoid AT ROW 19.67 COL 4 WIDGET-ID 120
     brwCPL AT ROW 20.62 COL 3 WIDGET-ID 300
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 4
     "Agent Information" VIEW-AS TEXT
          SIZE 17 BY .62 AT ROW 6.71 COL 4 WIDGET-ID 124
     rAgent AT ROW 6.95 COL 3 WIDGET-ID 122
     rParam AT ROW 1.48 COL 3 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 123 BY 27.43 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wBatchCounts ASSIGN
         HIDDEN             = YES
         TITLE              = "Batch Counts"
         HEIGHT             = 27.43
         WIDTH              = 123
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wBatchCounts
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fBatchCounts
   FRAME-NAME                                                           */
/* BROWSE-TAB brwCounts tFilterFileNumber fBatchCounts */
/* BROWSE-TAB brwCPL chkVoid fBatchCounts */
/* SETTINGS FOR TOGGLE-BOX chkVoid IN FRAME fBatchCounts
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentID IN FRAME fBatchCounts
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentName IN FRAME fBatchCounts
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentState IN FRAME fBatchCounts
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentStatus IN FRAME fBatchCounts
   NO-ENABLE                                                            */
ASSIGN 
       tFilterFileNumber:READ-ONLY IN FRAME fBatchCounts        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wBatchCounts)
THEN wBatchCounts:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCounts
/* Query rebuild information for BROWSE brwCounts
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH cplReport NO-LOCK BY cplReport.fileNumber INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwCounts */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCPL
/* Query rebuild information for BROWSE brwCPL
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH cpl NO-LOCK BY cpl.issueDate INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE brwCPL */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fBatchCounts:HANDLE
       ROW             = 3.86
       COLUMN          = 78
       HEIGHT          = .71
       WIDTH           = 42
       WIDGET-ID       = 108
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(cmbStatus:HANDLE IN FRAME fBatchCounts).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wBatchCounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wBatchCounts wBatchCounts
ON END-ERROR OF wBatchCounts /* Batch Counts */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wBatchCounts wBatchCounts
ON WINDOW-CLOSE OF wBatchCounts /* Batch Counts */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wBatchCounts wBatchCounts
ON WINDOW-RESIZED OF wBatchCounts /* Batch Counts */
DO:
  RUN WindowResized IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport wBatchCounts
ON CHOOSE OF bExport IN FRAME fBatchCounts /* Export */
DO:
  sensitizeReportButtons(false).
  run ExportData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterFileClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterFileClear wBatchCounts
ON CHOOSE OF bFilterFileClear IN FRAME fBatchCounts /* Clear */
DO:
  RUN FilterBatchData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilterFileGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilterFileGo wBatchCounts
ON CHOOSE OF bFilterFileGo IN FRAME fBatchCounts /* Go */
DO:
  RUN FilterBatchData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo wBatchCounts
ON CHOOSE OF bGo IN FRAME fBatchCounts /* Go */
DO:
  sensitizeReportButtons(false).
  run GetData in this-procedure.
  sensitizeReportButtons(true).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCounts
&Scoped-define SELF-NAME brwCounts
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCounts wBatchCounts
ON ROW-DISPLAY OF brwCounts IN FRAME fBatchCounts /* Counts */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCounts wBatchCounts
ON START-SEARCH OF brwCounts IN FRAME fBatchCounts /* Counts */
DO:
  RUN FilterBatchData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCounts wBatchCounts
ON VALUE-CHANGED OF brwCounts IN FRAME fBatchCounts /* Counts */
DO:
  IF NOT AVAILABLE cplReport
   THEN RETURN.
  
  /* get the cpl details from the clean file number */
  RUN GetCPLDetails IN THIS-PROCEDURE (cplReport.cleanFileNumber).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCPL
&Scoped-define SELF-NAME brwCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCPL wBatchCounts
ON ROW-DISPLAY OF brwCPL IN FRAME fBatchCounts /* CPL */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCPL wBatchCounts
ON START-SEARCH OF brwCPL IN FRAME fBatchCounts /* CPL */
DO:
  RUN FilterCPLData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME chkVoid
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL chkVoid wBatchCounts
ON VALUE-CHANGED OF chkVoid IN FRAME fBatchCounts /* Show Voided */
DO:
  RUN FilterCPLData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbBatch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbBatch wBatchCounts
ON VALUE-CHANGED OF cmbBatch IN FRAME fBatchCounts /* Batch */
DO:
  std-in = INTEGER(SELF:SCREEN-VALUE) NO-ERROR.
  IF NOT ERROR-STATUS:ERROR
   THEN RUN GetAgentInfo IN THIS-PROCEDURE (std-in).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbMonth wBatchCounts
ON VALUE-CHANGED OF cmbMonth IN FRAME fBatchCounts /* Month */
DO:
  RUN GetBatchList IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbState wBatchCounts
ON VALUE-CHANGED OF cmbState IN FRAME fBatchCounts /* State */
DO:
  RUN GetBatchList IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbStatus
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbStatus wBatchCounts
ON VALUE-CHANGED OF cmbStatus IN FRAME fBatchCounts /* Status */
DO:
  RUN GetBatchList IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cmbYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cmbYear wBatchCounts
ON VALUE-CHANGED OF cmbYear IN FRAME fBatchCounts /* Year */
DO:
  RUN GetBatchList IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFilterFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFilterFileNumber wBatchCounts
ON LEAVE OF tFilterFileNumber IN FRAME fBatchCounts /* File Number */
DO:
  RUN FilterBatchData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFilterFileNumber wBatchCounts
ON RETURN OF tFilterFileNumber IN FRAME fBatchCounts /* File Number */
DO:
  RUN FilterBatchData IN THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCounts
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wBatchCounts 


/* ***************************  Main Block  *************************** */

SUBSCRIBE to "SetProgressCounter" ANYWHERE.
SUBSCRIBE TO "SetProgressStatus" ANYWHERE.
SUBSCRIBE TO "SetProgressEnd" ANYWHERE.

ASSIGN
  {&window-name}:MIN-HEIGHT-PIXELS = {&window-name}:HEIGHT-PIXELS
  {&window-name}:MIN-WIDTH-PIXELS = {&window-name}:WIDTH-PIXELS
  {&window-name}:MAX-HEIGHT-PIXELS = SESSION:HEIGHT-PIXELS
  {&window-name}:MAX-WIDTH-PIXELS = SESSION:WIDTH-PIXELS
  .

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* setup the browse widgets */
{lib/brw-main-multi.i &browse-list="brwCounts,brwCPL"}

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  do with FRAME {&frame-name}:
    /* get the agent data */
    PUBLISH "GetAgents" (OUTPUT TABLE ttAgent).
    
    /* create the combos */
    {lib/get-state-list.i &combo=cmbState}
    {lib/get-period-list.i &mth=cmbMonth &yr=cmbYear}
    /* set the values */
    {lib/set-current-value.i &state=cmbState &mth=cmbMonth &yr=cmbYear &stat=cmbStatus}
  end.
  RUN Initialize IN THIS-PROCEDURE.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load wBatchCounts  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wops21-r.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wops21-r.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wBatchCounts  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wBatchCounts)
  THEN DELETE WIDGET wBatchCounts.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wBatchCounts  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY cmbMonth cmbYear cmbState cmbStatus cmbBatch tAgentID tAgentState 
          tAgentStatus tAgentName tFilterFileNumber chkVoid 
      WITH FRAME fBatchCounts IN WINDOW wBatchCounts.
  ENABLE rAgent rParam cmbMonth cmbYear bGo bExport cmbState cmbStatus cmbBatch 
         bFilterFileGo bFilterFileClear tFilterFileNumber brwCounts brwCPL 
      WITH FRAME fBatchCounts IN WINDOW wBatchCounts.
  {&OPEN-BROWSERS-IN-QUERY-fBatchCounts}
  VIEW wBatchCounts.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData wBatchCounts 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
@description Exports the browse widgets to Excel
------------------------------------------------------------------------------*/
  DEFINE VARIABLE cBrowseList AS CHARACTER NO-UNDO INITIAL "".
  DEFINE VARIABLE cReportName AS CHARACTER NO-UNDO.
  ASSIGN
    cBrowseList = addDelimiter(cBrowseList, ",") + STRING(BROWSE brwCounts:HANDLE)
    cBrowseList = addDelimiter(cBrowseList, ",") + STRING(BROWSE brwCPL:HANDLE)
    cReportName = "Batch_Counts_" + cmbBatch:SCREEN-VALUE IN FRAME {&frame-name} + "_Report"
    .
  RUN util/exporttoexcelbrowse.p (cBrowseList, cReportName).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterBatchData wBatchCounts 
PROCEDURE FilterBatchData :
/*------------------------------------------------------------------------------
@description Filters the batch count browse by the criteria the user selects
------------------------------------------------------------------------------*/
  DEFINE VARIABLE cWhere AS CHARACTER NO-UNDO INITIAL "".
  DEFINE VARIABLE cSort AS CHARACTER NO-UNDO INITIAL "by cplReport.fileNumber".
  
  DO WITH FRAME {&frame-name}:
    std-ch = tFilterFileNumber:SCREEN-VALUE.
  END.
  
  if std-ch <> ""
   then cWhere = "where fileNumber matches '*" + std-ch + "*'".

  {lib/brw-startSearch-multi.i &browseName="brwCounts" &whereClause="cWhere" &sortClause="cSort"}
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FilterCPLData wBatchCounts 
PROCEDURE FilterCPLData :
/*------------------------------------------------------------------------------
@description Filters the batch count browse by the criteria the user selects
------------------------------------------------------------------------------*/
  DEFINE VARIABLE cWhere AS CHARACTER NO-UNDO INITIAL "where voidDate = ?".
  DEFINE VARIABLE cSort AS CHARACTER NO-UNDO INITIAL "by cpl.issueDate".
  
  DO WITH FRAME {&frame-name}:
    std-lo = LOGICAL(chkVoid:SCREEN-VALUE) NO-ERROR.
  END.
  
  IF std-lo
   THEN cWhere = "".

  {lib/brw-startSearch-multi.i &browseName="brwCPL" &whereClause="cWhere" &sortClause="cSort"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAgentInfo wBatchCounts 
PROCEDURE GetAgentInfo :
/*------------------------------------------------------------------------------
@description Filters the batch count browse by the criteria the user selects
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pBatchID AS INTEGER NO-UNDO.
  
  /* set the current batch ID */
  pCurrBatch = pBatchID.
  
  /* set the agent information */
  IF CAN-FIND(FIRST ttAgent)
   THEN
    FOR EACH ttBatch, EACH ttAgent
        WHERE ttBatch.agentID = ttAgent.agentID
          AND ttBatch.batchID = pBatchID:
          
      DO WITH FRAME {&frame-name}:
        ASSIGN
          tAgentID:SCREEN-VALUE = ttAgent.agentID
          tAgentName:SCREEN-VALUE = ttAgent.name
          tAgentStatus:SCREEN-VALUE = ttAgent.stat
          tAgentState:SCREEN-VALUE = getStateName(ttAgent.stateID)
          .
      END.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatchList wBatchCounts 
PROCEDURE GetBatchList :
/*------------------------------------------------------------------------------
@description Populates the batch combo box with a filtered list of batch ids 
------------------------------------------------------------------------------*/
  DEFINE VARIABLE iMonth AS INTEGER NO-UNDO.
  DEFINE VARIABLE iYear AS INTEGER NO-UNDO.
  DEFINE VARIABLE cState AS CHARACTER NO-UNDO.
  DEFINE VARIABLE cStatus AS CHARACTER NO-UNDO.
  DEFINE VARIABLE cBatchList AS CHARACTER NO-UNDO INITIAL "".
  
  DO WITH FRAME {&frame-name}:
    ASSIGN
      iMonth = cmbMonth:INPUT-VALUE
      iYear = cmbYear:INPUT-VALUE
      cState = cmbState:INPUT-VALUE
      cStatus = cmbStatus:INPUT-VALUE
      .
  END.
  
  /* clear the screen if the month or year does not match the last selection */
  IF iMonth <> pCurrMonth OR iYear <> pCurrYear
   THEN
    DO:
      ASSIGN
        pCurrMonth = iMonth
        pCurrYear = iYear
        .
      /* clear the screen and get the batch list */
      sensitizeReportButtons(false).
      PUBLISH "GetBatches" (pCurrMonth, pCurrYear, OUTPUT TABLE ttBatch).
      sensitizeReportButtons(true).
    END.
  /* set the batch information */
  FOR EACH ttBatch NO-LOCK:
    
    IF cState <> "ALL" AND cState <> ttBatch.stateID
     THEN NEXT.
     
    IF cStatus <> "ALL" AND cStatus <> ttBatch.stat
     THEN NEXT.
     
    cBatchList = addDelimiter(cBatchList,",") + STRING(ttBatch.batchID).
  END.
  
  /* if no rows were found after filtering */
  IF cBatchList = ""
   THEN cBatchList = "No Batches Found".
    
  ASSIGN
    cmbBatch:LIST-ITEMS = cBatchList
    cmbBatch:SCREEN-VALUE = (IF pCurrBatch > 0 THEN STRING(pCurrBatch) ELSE ENTRY(1,cmbBatch:LIST-ITEMS))
    .
  IF cBatchList MATCHES STRING(pCurrBatch)
   THEN cmbBatch:SCREEN-VALUE = STRING(pCurrBatch).
   
  APPLY "VALUE-CHANGED" TO cmbBatch.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCPLDetails wBatchCounts 
PROCEDURE GetCPLDetails :
/*------------------------------------------------------------------------------
@description Gets the CPL details for the file number
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER pFileNumber AS CHARACTER NO-UNDO.
  
  /* get the correct CPL for the file number */
  EMPTY TEMP-TABLE cpl.
  FOR EACH allCPL
     WHERE allCPL.cleanFileNumber = pFileNumber:
  
    CREATE cpl.
    BUFFER-COPY allCPL TO cpl.
  END.
  RUN FilterCPLData IN THIS-PROCEDURE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetData wBatchCounts 
PROCEDURE GetData :
/*------------------------------------------------------------------------------
@description Gets the report data from the server    
------------------------------------------------------------------------------*/
  DEFINE VARIABLE iBatchID AS INTEGER NO-UNDO.
  
  ASSIGN
    iBatchID = INTEGER(cmbBatch:SCREEN-VALUE IN FRAME {&frame-name})
    NO-ERROR
    .
    
  IF ERROR-STATUS:ERROR
   THEN
    DO:
      MESSAGE "Please choose a valid batch number" VIEW-AS ALERT-BOX INFO BUTTONS OK.
      RETURN.
    END.
  
  /* cleanup before getting data */
  publish "SetProgressCounter" (2).
  pbMinStatus().
  clearData().
  /* pause the screen to let the combo widgets get sensitive */
  sensitizeBrowseFilters(false).
  PAUSE 1 NO-MESSAGE.
  PUBLISH "SetProgressStatus".
  /* get the data */
  RUN SERVER/getcplreport.p (iBatchID,
                             OUTPUT TABLE cplReport,
                             OUTPUT TABLE allCPL, 
                             OUTPUT std-lo,
                             OUTPUT std-ch).
                             
  IF NOT std-lo
   THEN
    DO:
      IF std-ch > ""
       THEN MESSAGE std-ch VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      PUBLISH "SetProgressCounter" (0).
      RETURN.
    END.
  PUBLISH "SetProgressStatus".
  /* filter the batch data */
  RUN FilterBatchData IN THIS-PROCEDURE.
  /* get the cpl details */
  APPLY "VALUE-CHANGED" TO brwCounts.
  PUBLISH "SetProgressEnd".
  sensitizeBrowseFilters(true).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initialize wBatchCounts 
PROCEDURE initialize :
/*------------------------------------------------------------------------------
@description Sets the screen during the initial load
------------------------------------------------------------------------------*/
  DoWait(true).
  DEFINE VARIABLE iMonth AS INTEGER NO-UNDO.
  DEFINE VARIABLE iYear AS INTEGER NO-UNDO.
  DEFINE VARIABLE cState AS CHARACTER NO-UNDO INITIAL "".
  DEFINE VARIABLE cStatus AS CHARACTER NO-UNDO INITIAL "".
  DO WITH FRAME {&frame-name}:
    bGo:LOAD-IMAGE("images/completed.bmp").
    bGo:LOAD-IMAGE-INSENSITIVE("images/completed-i.bmp").
    bExport:LOAD-IMAGE("images/excel.bmp").
    bExport:LOAD-IMAGE-INSENSITIVE("images/excel-i.bmp").
    bFilterFileGo:load-image("images/s-completed.bmp").
    bFilterFileGo:load-image-insensitive("images/s-completed-i.bmp").
    bFilterFileClear:load-image("images/s-erase.bmp").
    bFilterFileClear:load-image-insensitive("images/s-erase-i.bmp").
    
    ASSIGN
      /* minimum width and heights for resize */
      dMinTextAgentWidth = tAgentName:WIDTH-PIXELS
      dMinRectParamWidth = rParam:WIDTH-PIXELS
      dMinRectAgentWidth = rAgent:WIDTH-PIXELS
      dMinBrowseCPLWidth = brwCPL:WIDTH-PIXELS
      dMinBrowseCPLHeight = brwCPL:HEIGHT-PIXELS
      dMinBrowseCountsWidth = brwCounts:WIDTH-PIXELS
      .
      
    /* set the minimum width of the file number column on the CPL browse widget */
    std-ha = getColumn(brwCPL:HANDLE, "fileNumber").
    IF VALID-HANDLE(std-ha)
     THEN dMinBrowseCPLColFile = std-ha:WIDTH-CHARS.
     
    /* set the minimum width of the file number column on the counts browse widget */
    std-ha = getColumn(brwCounts:HANDLE, "fileNumber").
    IF VALID-HANDLE(std-ha)
     THEN dMinBrowseCountsColFile = std-ha:WIDTH-CHARS.
    
    /* set the minimum width of the file number column on the counts browse widget */
    std-ha = getColumn(brwCounts:HANDLE, "policyID").
    IF VALID-HANDLE(std-ha)
     THEN dMinBrowseCountsColPolicy = std-ha:WIDTH-CHARS.
  END.
  /* setting the current month and year to 0 ensures that we will refresh the batch list */
  ASSIGN
    pCurrMonth = 0
    pCurrYear = 0
    .
  RUN GetBatchList IN THIS-PROCEDURE.
  DoWait(false).
  
  /* run the report if there is a batch and the user wishes it */
  PUBLISH "GetAutoView" (OUTPUT std-lo).
  IF std-lo AND pCurrBatch > 0
   THEN APPLY "CHOOSE" TO bGo.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressCounter wBatchCounts 
PROCEDURE SetProgressCounter :
/*------------------------------------------------------------------------------
@description Sets the progress bar count
------------------------------------------------------------------------------*/
  DEF INPUT PARAMETER pCounter AS INT NO-UNDO.
  ASSIGN
    iCounterMax = pCounter
    iCounter = 0
    .
  pbMinStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressEnd wBatchCounts 
PROCEDURE SetProgressEnd :
/*------------------------------------------------------------------------------
@description Makes the progress bar minimum after setting to 100
------------------------------------------------------------------------------*/
  pbUpdateStatus(100, 1).
  pbMinStatus().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetProgressStatus wBatchCounts 
PROCEDURE SetProgressStatus :
/*------------------------------------------------------------------------------
@description Updates the progress bar
------------------------------------------------------------------------------*/
  iCounter = iCounter + 1.
  pbUpdateStatus(int(iCounter / iCounterMax * 100), 0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wBatchCounts 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
@description Sorts the data in the browse widgets
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowResized wBatchCounts 
PROCEDURE WindowResized PRIVATE :
/*-----------------------------------------------------------------------------
@description Resize the window  
------------------------------------------------------------------------------*/  
  DEFINE VARIABLE dDiffHeight AS DECIMAL NO-UNDO.
  DEFINE VARIABLE dDiffWidth AS DECIMAL NO-UNDO.

  FRAME {&frame-name}:WIDTH-PIXELS = {&window-name}:WIDTH-PIXELS.
  FRAME {&frame-name}:VIRTUAL-WIDTH-PIXELS = {&window-name}:WIDTH-PIXELS.
  FRAME {&frame-name}:HEIGHT-PIXELS = {&window-name}:HEIGHT-PIXELS.
  FRAME {&frame-name}:VIRTUAL-HEIGHT-PIXELS = {&window-name}:HEIGHT-PIXELS.
  
  /* get the difference between the old and new width\height */
  ASSIGN 
    dDiffWidth = FRAME {&frame-name}:WIDTH-PIXELS - {&window-name}:MIN-WIDTH-PIXELS
    dDiffHeight = FRAME {&frame-name}:HEIGHT-PIXELS - {&window-name}:MIN-HEIGHT-PIXELS
    .
    
  DO WITH FRAME {&frame-name}:
    ASSIGN
      tAgentName:WIDTH-PIXELS = dMinTextAgentWidth + dDiffWidth
      rParam:WIDTH-PIXELS = dMinRectParamWidth + dDiffWidth
      rAgent:WIDTH-PIXELS = dMinRectAgentWidth + dDiffWidth
      brwCPL:WIDTH-PIXELS = dMinBrowseCPLWidth + dDiffWidth
      brwCPL:HEIGHT-PIXELS = dMinBrowseCPLHeight + dDiffHeight
      brwCounts:WIDTH-PIXELS = dMinBrowseCountsWidth + dDiffWidth
      .
      
    /* set the width of the file number column on the CPL browse widget */
    std-ha = getColumn(brwCPL:HANDLE, "fileNumber").
    IF VALID-HANDLE(std-ha)
     THEN std-ha:WIDTH-PIXELS = (dMinBrowseCPLColFile * SESSION:PIXELS-PER-COLUMN) + dDiffWidth.
     
    /* set the width of the file number column on the counts browse widget */
    std-ha = getColumn(brwCounts:HANDLE, "fileNumber").
    IF VALID-HANDLE(std-ha)
     THEN std-ha:WIDTH-PIXELS = (dMinBrowseCountsColFile * SESSION:PIXELS-PER-COLUMN) + (dDiffWidth / 2).
    
    /* set the width of the file number column on the counts browse widget */
    std-ha = getColumn(brwCounts:HANDLE, "policyID").
    IF VALID-HANDLE(std-ha)
     THEN std-ha:WIDTH-PIXELS = (dMinBrowseCountsColPolicy * SESSION:PIXELS-PER-COLUMN) + (dDiffWidth / 2) - 1.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData wBatchCounts 
FUNCTION clearData RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Closes the queries for the browsers and resets the filters 
------------------------------------------------------------------------------*/
  /* clear the brwCounts browse */
  CLOSE QUERY brwCounts.
  EMPTY TEMP-TABLE cplReport.
  /* clear the brwCPL browse */
  CLOSE QUERY brwCPL.
  EMPTY TEMP-TABLE cpl.
  /* clear the allCPL temp table */
  EMPTY TEMP-TABLE allCPL.

  RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus wBatchCounts 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
@description Sets the progress bar back to the minimum value
------------------------------------------------------------------------------*/
  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus wBatchCounts 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( INPUT pPercentage AS INT, 
    INPUT pPauseSeconds AS INT ) :
/*------------------------------------------------------------------------------
@description: Sets the progress bar to pPercentage
------------------------------------------------------------------------------*/
  {&WINDOW-NAME}:move-to-top().
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0
     then pause pPauseSeconds no-message.
  end.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitizeBrowseFilters wBatchCounts 
FUNCTION sensitizeBrowseFilters RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
@description Enables or disables the filters for both browse widgets
------------------------------------------------------------------------------*/
  DO WITH FRAME {&frame-name}:
    ASSIGN
      bFilterFileGo:SENSITIVE = pEnable
      bFilterFileClear:SENSITIVE = pEnable
      tFilterFileNumber:READ-ONLY = NOT pEnable
      chkVoid:SENSITIVE = pEnable
      .
  END.
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION sensitizeReportButtons wBatchCounts 
FUNCTION sensitizeReportButtons RETURNS LOGICAL
  ( INPUT pEnable AS LOGICAL ) :
/*------------------------------------------------------------------------------
@description Enables or disables the buttons within the parameters
------------------------------------------------------------------------------*/
  DO WITH FRAME {&frame-name}:
    ASSIGN
      cmbMonth:SENSITIVE = pEnable
      cmbYear:SENSITIVE = pEnable
      cmbStatus:SENSITIVE = pEnable
      cmbState:SENSITIVE = pEnable
      cmbBatch:SENSITIVE = pEnable
      bGo:SENSITIVE = pEnable
      bExport:SENSITIVE = pEnable
      .
  END.
   
  DoWait(NOT pEnable).
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

