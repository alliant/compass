&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wpolicies.w
   Window of POLICIES for an agent
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/cpl.i}

{lib/std-def.i}

def var hData as handle no-undo.

{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES cpl

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData cpl.stat cpl.cplID cpl.issueDate cpl.fileNumber cpl.formID cpl.grossPremium cpl.netPremium cpl.retention   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH cpl
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH cpl.
&Scoped-define TABLES-IN-QUERY-brwData cpl
&Scoped-define FIRST-TABLE-IN-QUERY-brwData cpl


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fAgentID fFileNumber fStartCPL fEndCPL ~
fStartDate fEndDate fPolicyStat bRefresh bPrint bExport brwData 
&Scoped-Define DISPLAYED-OBJECTS fAgentID fFileNumber fStartCPL fEndCPL ~
fStartDate tStatus fEndDate fPolicyStat 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bPrint 
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View CPL details as PDF".

DEFINE BUTTON bRefresh 
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get CPL details".

DEFINE VARIABLE fAgentID AS CHARACTER FORMAT "X(20)":U 
     LABEL "AgentID" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE VARIABLE fEndCPL AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter ending CPL number; blank for all" NO-UNDO.

DEFINE VARIABLE fEndDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "To" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter latest policy issue date; blank (?) for all" NO-UNDO.

DEFINE VARIABLE fFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE fStartCPL AS CHARACTER FORMAT "X(256)":U 
     LABEL "CPL" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter starting CPL number; blank for all" NO-UNDO.

DEFINE VARIABLE fStartDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Issued" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter earliest policy issue date; blank (?) for all" NO-UNDO.

DEFINE VARIABLE tStatus AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 142.4 BY 1 NO-UNDO.

DEFINE VARIABLE fPolicyStat AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Issued", "I",
"Processed", "P",
"Voided", "V",
"All", "A"
     SIZE 45 BY 1.05 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      cpl SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      cpl.stat label "S" format "x(2)"      
 cpl.cplID label "CPL" format "x(15)"
 cpl.issueDate label "Issued"

 cpl.fileNumber label "File" format "x(30)" width 20
 
 cpl.formID label "Form" format "x(25)"
 cpl.grossPremium column-label "Gross!Premium"
 cpl.netPremium column-label "Net!Premium"
 cpl.retention label "Retention"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 141.4 BY 11.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fAgentID AT ROW 1.19 COL 10.8 COLON-ALIGNED WIDGET-ID 10
     fFileNumber AT ROW 2.24 COL 10.8 COLON-ALIGNED WIDGET-ID 26
     fStartCPL AT ROW 1.19 COL 35.8 COLON-ALIGNED WIDGET-ID 28
     fEndCPL AT ROW 2.24 COL 36 COLON-ALIGNED WIDGET-ID 30
     fStartDate AT ROW 1.19 COL 57.6 COLON-ALIGNED WIDGET-ID 22
     tStatus AT ROW 15.38 COL 1 NO-LABEL WIDGET-ID 12 NO-TAB-STOP 
     fEndDate AT ROW 2.24 COL 57.6 COLON-ALIGNED WIDGET-ID 24
     fPolicyStat AT ROW 1.91 COL 75.8 NO-LABEL WIDGET-ID 16
     bRefresh AT ROW 1.33 COL 121.4 WIDGET-ID 4
     bPrint AT ROW 1.33 COL 128.4 WIDGET-ID 14
     bExport AT ROW 1.33 COL 135.4 WIDGET-ID 2
     brwData AT ROW 3.43 COL 2 WIDGET-ID 200
     "Status" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.29 COL 75 WIDGET-ID 32
          FONT 6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 170.2 BY 15.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "CPLs for Agent"
         HEIGHT             = 15.43
         WIDTH              = 143
         MAX-HEIGHT         = 16
         MAX-WIDTH          = 170.2
         VIRTUAL-HEIGHT     = 16
         VIRTUAL-WIDTH      = 170.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData bExport fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN tStatus IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       tStatus:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH cpl.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* CPLs for Agent */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* CPLs for Agent */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* CPLs for Agent */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run ViewCPL in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgentID C-Win
ON LEAVE OF fAgentID IN FRAME fMain /* AgentID */
DO:
 if self:modified 
  then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEndCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEndCPL C-Win
ON LEAVE OF fEndCPL IN FRAME fMain /* To */
DO:
 if self:modified 
  then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fEndDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fEndDate C-Win
ON LEAVE OF fEndDate IN FRAME fMain /* To */
DO:
 if self:modified 
  then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fFileNumber C-Win
ON LEAVE OF fFileNumber IN FRAME fMain /* File */
DO:
  if self:modified 
   then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPolicyStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPolicyStat C-Win
ON VALUE-CHANGED OF fPolicyStat IN FRAME fMain
DO:
  tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStartCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStartCPL C-Win
ON LEAVE OF fStartCPL IN FRAME fMain /* CPL */
DO:
  if self:modified 
   then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStartDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStartDate C-Win
ON LEAVE OF fStartDate IN FRAME fMain /* Issued */
DO:
 if self:modified 
  then tStatus:screen-value in frame fMain = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bPrint:load-image("images/pdf.bmp").
bExport:load-image("images/excel.bmp").

publish "GetCurrentValue" ("AgentID", output fAgentID).

/* run getData in this-procedure. */

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fAgentID fFileNumber fStartCPL fEndCPL fStartDate tStatus fEndDate 
          fPolicyStat 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fAgentID fFileNumber fStartCPL fEndCPL fStartDate fEndDate fPolicyStat 
         bRefresh bPrint bExport brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer x-cpl for cpl.
 def var exportFilename as char no-undo.
 def var doSave as logical no-undo.

 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).

 system-dialog get-file exportFilename
  filters "CSV Files" "*.csv"
  initial-dir std-ch
  ask-overwrite
  create-test-file
  default-extension ".csv"
  use-filename
  save-as
 update doSave.

 if not doSave 
  then return.

 std-in = 0.
 output to value(exportFilename).
 export delimiter ","
   "CPL"
   "Issued"
   "File"
   "Form"
   "Gross"
   "Net"
   "Retained"
   .

 for each x-cpl:
  export delimiter "," 
    x-cpl.cplID
    x-cpl.issueDate
    x-cpl.fileNumber
    x-cpl.formID 
    x-cpl.grossPremium
    x-cpl.netPremium
    x-cpl.retention
    .
  std-in = std-in + 1.
 end.
 output close.

 RUN ShellExecuteA in this-procedure (0,
                             "open",
                             exportFilename,
                             "",
                             "",
                             1,
                             OUTPUT std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer policy for cpl.
 def var tFile as char no-undo.
 
 if fAgentID:screen-value in frame fMain = "" 
  then
   do:
       MESSAGE "Agent ID cannot be blank"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 close query brwData.
 empty temp-table cpl.

 publish "GetCPLs" (input fAgentID:screen-value in frame fMain,
                        input fFileNumber:screen-value in frame fMain,
                        input fStartCPL:input-value in frame fMain,
                        input fEndCPL:input-value in frame fMain,
                        input fPolicyStat:screen-value in frame fMain,
                        input fStartDate:input-value in frame fMain,
                        input fEndDate:input-value in frame fMain,
                        output table cpl).

 dataSortBy = "".
 run sortData ("cplID").

 tStatus:screen-value in frame fMain = string(query brwData:num-results, ">>,>>>,>>9") 
    + " CPLs for agent " 
    + fAgentID:screen-value in frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if query brwData:num-results = 0 or not available cpl
  then
   do: 
    MESSAGE "There is nothing to print"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 run ops05-r.p ("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewCPL C-Win 
PROCEDURE ViewCPL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  if available cpl                                   */
/*   then run wcpl.w persistent (cpl.cplID) no-error.  */
/* das:ViewCPL:need to implement */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwData:width-pixels = frame fmain:width-pixels - 10.
 brwData:height-pixels = frame fMain:height-pixels - 75.
 tStatus:Y in frame fMain = frame fMain:height-pixels - 23.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

