&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input parameter pBatchID as int.
def input parameter pStat as char.
def input-output parameter pDateRcvd as datetime.
def input-output parameter pAgentID as char.
def input-output parameter pGrossPremium as deci.
def input-output parameter pNetPremium as deci.
def input-output parameter pCash as deci.
def input-output parameter pRcvdVia as char.
def input-output parameter pReference as char.
def output parameter pError as logical init true.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
def var tName as char no-undo.

def var activeMonth       as char no-undo.
def var activeYear        as char no-undo.
def var dtPeriodFirstDate as date no-undo.
def var dtPeriodLastDate  as date no-undo.

{tt/agent.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tDateRcvd tAgentID tReference ~
tGrossPremium tNetPremium tCash tRcvdVia fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 27.6 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN AUTO-COMPLETION
     SIZE 91.8 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 61.6 BY .62 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 61.6 BY .62 NO-UNDO.

DEFINE VARIABLE tCash AS DECIMAL FORMAT "$zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Cash Received" 
     VIEW-AS FILL-IN 
     SIZE 24.4 BY 1 NO-UNDO.

DEFINE VARIABLE tDateRcvd AS DATETIME FORMAT "99/99/99":U 
     LABEL "Date Received" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "$zz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "$zz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tReference AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference" 
     VIEW-AS FILL-IN 
     SIZE 52.8 BY 1 TOOLTIP "Agent reference" NO-UNDO.

DEFINE VARIABLE tRcvdVia AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Electronic", "E",
"Fax", "F",
"Mail", "M"
     SIZE 16 BY 3 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     tDateRcvd AT ROW 1.95 COL 16 COLON-ALIGNED WIDGET-ID 12
     tAgentID AT ROW 4.33 COL 16 COLON-ALIGNED WIDGET-ID 80
     tReference AT ROW 7.43 COL 16 COLON-ALIGNED WIDGET-ID 90
     tGrossPremium AT ROW 8.62 COL 16 COLON-ALIGNED WIDGET-ID 8
     tNetPremium AT ROW 9.81 COL 16 COLON-ALIGNED WIDGET-ID 10
     tCash AT ROW 11 COL 16 COLON-ALIGNED WIDGET-ID 20
     tRcvdVia AT ROW 8.62 COL 58.4 NO-LABEL WIDGET-ID 14
     Btn_OK AT ROW 12.76 COL 40.2
     tAddr1 AT ROW 5.48 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 86
     Btn_Cancel AT ROW 12.76 COL 58.2
     tAddr2 AT ROW 6.14 COL 16 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     fState AT ROW 3.14 COL 16 COLON-ALIGNED WIDGET-ID 82
     SPACE(66.19) SKIP(10.33)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify Batch"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fState IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAddr1 IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tAddr2 IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX tAgentID IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCash IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tDateRcvd IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNetPremium IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET tRcvdVia IN FRAME fNew
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tReference IN FRAME fNew
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* Modify Batch */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState fNew
ON VALUE-CHANGED OF fState IN FRAME fNew /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID fNew
ON VALUE-CHANGED OF tAgentID IN FRAME fNew /* Agent */
DO:
 find agent
   where agent.agentID = self:input-value no-error.
 if not available agent 
  then assign
         tAddr1:screen-value in frame fNew = ""
         tAddr2:screen-value in frame fNew = ""
         .
  else assign
         tAddr1:screen-value in frame fNew = agent.addr1 + "  " + agent.addr2
         tAddr2:screen-value in frame fNew = agent.city 
                 + (if agent.city > "" and agent.state > "" then ", " else "")
                 + agent.state + "  " + agent.zip
         tAgentID = self:screen-value
         .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCash
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCash fNew
ON LEAVE OF tCash IN FRAME fNew /* Cash Received */
DO:
  tCash = self:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDateRcvd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDateRcvd fNew
ON LEAVE OF tDateRcvd IN FRAME fNew /* Date Received */
DO:
  if self:input-value = ? or self:input-value > today 
   then self:bgcolor = 14.
   else assign
          tDateRcvd = self:input-value
          self:bgcolor = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tGrossPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tGrossPremium fNew
ON LEAVE OF tGrossPremium IN FRAME fNew /* Gross Premium */
DO:
  tGrossPremium = self:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNetPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNetPremium fNew
ON LEAVE OF tNetPremium IN FRAME fNew /* Net Premium */
DO:
  tNetPremium = self:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

{lib/get-state-list.i &combo=fState &addAll=true}
{lib/get-agent-list.i &combo=tAgentID &state=fState}
publish "SetCurrentValue" ("AgentID", pAgentID).
for first agent no-lock
    where agent.agentID = pAgentID:
  
  publish "SetCurrentValue" ("StateID", agent.stateID).
end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
assign
  tDateRcvd = pDateRcvd
  tGrossPremium = pGrossPremium
  tNetPremium = pNetPremium
  tCash = pCash
  tRcvdVia = pRcvdVia
  tReference = pReference
  .

RUN enable_UI.
{lib/set-current-value.i &state=fState &agent=tAgentID}
publish "getcurrentvalue" ("selectedMonth" , output activeMonth).
publish "getcurrentvalue" ("selectedYear" , output activeYear).
frame fNew:title = "Modify Batch #" + string(pBatchID).

if index("NP", pStat) > 0 
 then enable tDateRcvd
             fState
             tAgentID
             tReference
             tGrossPremium
             tNetPremium
             tCash
             tRcvdVia
             Btn_OK
             with frame fNew.
 else frame fNew:title = "Batch".
 
run AgentComboEnable in this-procedure (index("NP", pStat) > 0 ).

publish "SetCurrentValue" ("AgentID", "").
publish "SetCurrentValue" ("StateID", "").

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if tDateRcvd:input-value in frame fNew = ? or
     tDateRcvd:input-value > today 
   then
  do: 
      MESSAGE "Date Received cannot be blank or in the future"
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      next.
  end.
/*   if tAgentID:input-value in frame fNew = "NONE" */
/*     or tAgentID:input-value in frame fNew = ?    */
/*     or tAgentID:input-value in frame fNew = ""   */
  if not can-find(first agent where agent.agentID = tAgentID:input-value)
   then
    do: 
        MESSAGE "Invalid Agent"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        next.
    end.
  
  if pDateRcvd ne tDateRcvd:input-value in frame fNew 
   then
    do:
       dtPeriodFirstDate = date(integer(activeMonth),1,integer(activeYear)) no-error.
 
       if integer(activeMonth) <> 12
        then
         dtPeriodLastDate  = date(integer(activeMonth) + 1,1, integer(activeYear)) - 1 no-error.
       else
         dtPeriodLastDate  = date(1,1, integer(activeYear) + 1) - 1 no-error.
   
       if dtPeriodFirstDate > date(tDateRcvd:screen-value in frame fNew)  or 
          dtPeriodLastDate < date(tDateRcvd:screen-value in frame fNew)
        then
         do:
            MESSAGE "Date Received must lie in Batch Period."
              VIEW-AS ALERT-BOX warning BUTTONS ok .
            next.
         end.   
    end.

  /* Force screen values to variables */
  assign
    tDateRcvd
    tAgentID
    tGrossPremium
    tNetPremium
    tCash
    tRcvdVia
    tReference.

  assign
    pDateRcvd = tDateRcvd
    pAgentID = tAgentID
    pGrossPremium = tGrossPremium
    pNetPremium = tNetPremium
    pCash = tCash
    pRcvdVia = tRcvdVia
    pReference = tReference
    pError = false
    .

  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tDateRcvd tAgentID tReference tGrossPremium tNetPremium tCash tRcvdVia 
          fState 
      WITH FRAME fNew.
  ENABLE Btn_Cancel 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

