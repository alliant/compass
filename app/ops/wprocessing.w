&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/* wprocessing.w
   main window for PROCESSING a batch
   Created 1.30.2013 D.Sinclair
   11.26.2014 D.Sinclair    Added pop-menu for default endorsements which involved
                            restructuring addDefaultEndorsement procedure with
                            parameters rather than assuming it can use ui fields.
   Modified: 
   Date          Name        Comments
   08/21/2019    Vikas Jain  Added code to handle two new fields formCount and revenueType 
   02/15/2021    MK          Modified for commitment processing.  
   06/10/2021    MK          Updated for framework changes.
   02/17/2022    Vignesh R   Increased the below fields UI format 
                             (i)  tLiability field  - from "zzz,zzz,zz9.99" to "zzzz,zzz,zz9.99"  
                             (ii) In browse 'brwForms' field 'batchForm.liabilityAmount' - from 
                                  "-zz,zzz,zz9.99" to "-zzz,zzz,zz9.99" .
   */

CREATE WIDGET-POOL.


def input parameter pBatchID as int.
def input parameter pStateID as char.
def input parameter pAgentID as char.

define variable pZipCode as character no-undo.

/* def var pBatchID as char init "Batch-1".       */
/* def var pState as char init "TX" no-undo.      */
/* def var pAgentID as char init "TX 1000".       */
/* def var pAgentName as char init "Texas Agent". */

{lib/std-def.i}
{lib/set-button-def.i}
{src/adm2/widgetprto.i}
{lib/rpt-defs.i}

{tt/agent.i}
{tt/county.i}
{tt/stateform.i}
{tt/statcode.i}
{tt/policy.i}

{tt/batch.i}
{tt/batchaction.i}
{tt/batchform.i}
{tt/importdata.i}
{tt/proerr.i}

{tt/defaultform.i &tableAlias=defaultEnd}

def var tFileActive as logical init false no-undo.
def var tSearchActive as logical init false no-undo.

def var tLastPolicy as int no-undo.
def var tLastEffDate as datetime no-undo.
def var tLastCounty as char no-undo.

def var tLastSeq as int no-undo.
def var tSortBy as int init 0 no-undo.

def var tRevenueType as char no-undo.
def var tFormsList as char no-undo.
def var tAllFormsList as char no-undo.
def var tStatCodesList as char no-undo.
def var tCountiesList as char no-undo.

def var xEffDate as datetime.
def var xLiabilityAmount as decimal.
def var xGrossPremium as decimal.
def var xFormID as char.
def var xStateID as char.
def var xFileNumber as char.
def var xStatCode as char.
def var xNetPremium as dec.
def var xRetention as dec.
def var xCountyID as char.
def var xResidential as log.
def var xInvoiceDate as date.
def var xZipCode as logical.

def var tMsgStat as char no-undo.

def var hDocs as handle no-undo.


DEFINE MENU formpopmenu TITLE "Actions"
 MENU-ITEM m_PopModify LABEL "Modify"
 MENU-ITEM m_PopDelete label "Delete"
 RULE
 MENU-ITEM m_PopDefaults label "Add Endorsements"
/*  menu-item m_PopSearch label "Search" */
 .


publish "GetAgent" (pAgentID, output table agent).
find first agent no-error.
if not available agent 
 then
  do:
      MESSAGE "Invalid AgentID " pAgentID skip
              "Contact the System Administrator"
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      if this-procedure:persistent
       then delete procedure this-procedure.
      return.
  end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwForms

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batchform

/* Definitions for BROWSE brwForms                                      */
&Scoped-define FIELDS-IN-QUERY-brwForms batchForm.seq no-label batchform.reprocess batchForm.formType batchForm.fileNumber batchForm.policyID batchForm.formID batchForm.statCode batchForm.effDate batchForm.liabilityAmount batchForm.grossPremium batchForm.netPremium   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwForms   
&Scoped-define SELF-NAME brwForms
&Scoped-define QUERY-STRING-brwForms FOR EACH batchform
&Scoped-define OPEN-QUERY-brwForms OPEN QUERY {&SELF-NAME} FOR EACH batchform.
&Scoped-define TABLES-IN-QUERY-brwForms batchform
&Scoped-define FIRST-TABLE-IN-QUERY-brwForms batchform


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bDeleteBrowse tFileNumber bSearch ~
tResidential tZipCode tFilePoliciesCnt tSearchErrors tSearchFile ~
tSearchPolicy tBatchID brwForms tFileGrossPremium tFileNetPremium ~
tFileRetention tTotalCounts tTotalGrossPremium tTotalNetPremium tAgentID ~
tAgentName tFileEndorsementsCnt RECT-3 RECT-5 
&Scoped-Define DISPLAYED-OBJECTS tFileNumber tResidential tZipCode ~
tFormType tRateCode tStatCode tForm tSearchType tCounty tSearchTitle ~
tSearchErrors tSearchFile tSearchPolicy tTotalCounts tTotalGrossPremium ~
tTotalNetPremium 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearAddForm wWin 
FUNCTION clearAddForm RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearFileSummary wWin 
FUNCTION clearFileSummary RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD deleteValidationReport wWin 
FUNCTION deleteValidationReport RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayActiveFile wWin 
FUNCTION displayActiveFile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayFileSummary wWin 
FUNCTION displayFileSummary RETURNS LOGICAL PRIVATE
  ( pFile as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayForms wWin 
FUNCTION displayForms RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals wWin 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD doSearch wWin 
FUNCTION doSearch RETURNS LOGICAL PRIVATE
  ( pSearchFile as char,
    pSearchPolicy as char,
    pSearchErrors as char,
    pSearchCondition as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getData wWin 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( pSearchFile as char,
    pSearchPolicy as char,
    pSearchErrors as char,
    pSearchCondition as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD resizeWindow wWin 
FUNCTION resizeWindow RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveCommitment wWin 
FUNCTION saveCommitment RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveCPL wWin 
FUNCTION saveCPL RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveEndorsement wWin 
FUNCTION saveEndorsement RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveForm wWin 
FUNCTION saveForm RETURNS CHARACTER PRIVATE
  ( pFileNumber as char,
    pResidential as logical,
    pFormType as char,
    pPolicy as int,
    pRateCode as char,
    pSTAT as char,
    pForm as char,
    pEffDate as datetime,
    pLiability as decimal,
    pCounty as char,
    pGross as decimal,
    pNet as decimal,
    pFormCount as int,
    pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveMisc wWin 
FUNCTION saveMisc RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD savePolicy wWin 
FUNCTION savePolicy RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveSearch wWin 
FUNCTION saveSearch RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setAddFormState wWin 
FUNCTION setAddFormState RETURNS LOGICAL PRIVATE
  ( pSensitive as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setAllForms wWin 
FUNCTION setAllForms RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setCounties wWin 
FUNCTION setCounties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setForms wWin 
FUNCTION setForms RETURNS LOGICAL PRIVATE
  ( input pType as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setStatCodes wWin 
FUNCTION setStatCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD translateErrorCodes wWin 
FUNCTION translateErrorCodes RETURNS CHARACTER PRIVATE
  ( tValidMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_toolbar AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "Process" 
     SIZE 16 BY 2.19 TOOLTIP "Process form in this batch".

DEFINE BUTTON bDeleteBrowse 
     LABEL "Delete" 
     SIZE 4.8 BY 1.14 TOOLTIP "Delete the shown batch forms".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 4.8 BY 1.14 TOOLTIP "Search batch for forms".

DEFINE BUTTON bSearchClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 4.8 BY 1.14 TOOLTIP "Clear search".

DEFINE VARIABLE tCounty AS CHARACTER 
     LABEL "County" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN AUTO-COMPLETION
     SIZE 33.8 BY 1 NO-UNDO.

DEFINE VARIABLE tForm AS CHARACTER 
     LABEL "Form" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN AUTO-COMPLETION
     SIZE 90.8 BY 1 NO-UNDO.

DEFINE VARIABLE tFormType AS CHARACTER FORMAT "X(256)":U INITIAL "P" 
     LABEL "Form Type" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     LIST-ITEM-PAIRS "Policy","P",
                     "Endorsement","E",
                     "CPL","C",
                     "Commitment","T"
     DROP-DOWN-LIST
     SIZE 29.8 BY 1 NO-UNDO.

DEFINE VARIABLE tRateCode AS CHARACTER FORMAT "X(256)":U INITIAL "P" 
     LABEL "Rate Code" 
     VIEW-AS COMBO-BOX INNER-LINES 9
     LIST-ITEM-PAIRS "Purchase","P",
                     "Loan","L",
                     "Loan Simultaneous","S",
                     "2nd Loan Simultaneous","N",
                     "Home Equity","H",
                     "Construction","C",
                     "Guarantee","G",
                     "Leasehold","E",
                     "Foreclosure","F"
     DROP-DOWN-LIST
     SIZE 29.8 BY 1 TOOLTIP "Select the nature of the policy (rate)" NO-UNDO.

DEFINE VARIABLE tStatCode AS CHARACTER 
     LABEL "STAT" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     DROP-DOWN AUTO-COMPLETION
     SIZE 108 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 62 BY 1 NO-UNDO.

DEFINE VARIABLE tBatchID AS INTEGER FORMAT ">>>>>>>9":U INITIAL 99999999 
     VIEW-AS FILL-IN 
     SIZE 13.2 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 14 BY 1 TOOLTIP "Effective Date should be no more than 6 months old" NO-UNDO.

DEFINE VARIABLE tFileEndorsementsCnt AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Endorsements" 
     VIEW-AS FILL-IN 
     SIZE 6.6 BY 1 NO-UNDO.

DEFINE VARIABLE tFileGrossPremium AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Gross" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNetPremium AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Net" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(20)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 TOOLTIP "Enter the Agent's File Number (cannot be blank)"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tFilePoliciesCnt AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Policies" 
     VIEW-AS FILL-IN 
     SIZE 6.6 BY 1 NO-UNDO.

DEFINE VARIABLE tFileRetention AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Retention" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE tFormCount AS INTEGER FORMAT "Z":U INITIAL 1 
     LABEL "Form Count" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "zz,zz9.99":U INITIAL 0 
     LABEL "Gross $" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "zzz,zzz,zz9.99":U INITIAL 999999999.99 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 21.2 BY 1 TOOLTIP "Liability amount typically does not exceeds $1,000,000" NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "zz,zz9.99":U INITIAL 0 
     LABEL "Net $" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 20 BY 1 TOOLTIP "Policy number cannot be blank" NO-UNDO.

DEFINE VARIABLE tRetention AS DECIMAL FORMAT "zz,zz9.99-":U INITIAL 0 
     LABEL "Retention $" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tSearchFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 32.8 BY 1 NO-UNDO.

DEFINE VARIABLE tSearchPolicy AS INTEGER FORMAT ">>>>>>>>":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tSearchTitle AS CHARACTER FORMAT "X(256)":U INITIAL "Search" 
      VIEW-AS TEXT 
     SIZE 7.2 BY .62 NO-UNDO.

DEFINE VARIABLE tSearchType AS CHARACTER FORMAT "X(256)":U INITIAL "View:" 
      VIEW-AS TEXT 
     SIZE 5.4 BY .81 NO-UNDO.

DEFINE VARIABLE tTotalCounts AS CHARACTER FORMAT "X(256)":U 
     LABEL "Batch Counts" 
     VIEW-AS FILL-IN 
     SIZE 73.8 BY 1 TOOLTIP "Counts in the batch (Files/Policies/Endorsements)" NO-UNDO.

DEFINE VARIABLE tTotalGrossPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Batch Totals" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 TOOLTIP "Total Gross Premium in the batch"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tTotalNetPremium AS DECIMAL FORMAT "-z,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 17.4 BY 1 TOOLTIP "Total Net Premium in the batch"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tZipCode AS INTEGER FORMAT ">>>>>":U INITIAL 0 
     LABEL "Zip Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tSearchErrors AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "All", "A",
"Errors", "E",
"Warnings", "W",
"Both", "B"
     SIZE 40.6 BY .81 TOOLTIP "Display All forms, Errors only, Warnings only, or both Errors and Warnings" NO-UNDO.

DEFINE RECTANGLE AddFormRectangle
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 141.6 BY 6.91.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25 BY 2.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56.4 BY 4.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 7.8 BY 1.91.

DEFINE RECTANGLE SearchRect
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 133 BY 1.91.

DEFINE VARIABLE tResidential AS LOGICAL INITIAL yes 
     LABEL "Residential" 
     VIEW-AS TOGGLE-BOX
     SIZE 14.4 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwForms FOR 
      batchform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwForms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwForms wWin _FREEFORM
  QUERY brwForms DISPLAY
      batchForm.seq no-label format "zzzz9"   
 batchform.reprocess label "R" format "Y/"
 batchForm.formType label "Type" format "x(2)" 
 batchForm.fileNumber label "File" format "x(30)" width 24
 batchForm.policyID label "Policy" format "zzzzzzzz9" width 12.6
 batchForm.formID label "Form" format "x(14)" width 15
 batchForm.statCode label "STAT" format "x(10)"
 batchForm.effDate column-label "Effective!Date" format "99/99/9999"
 batchForm.liabilityAmount label "Liability" format "-zzz,zzz,zz9.99" 
 batchForm.grossPremium column-label "Gross!Premium" format "-zz,zzz,zz9.99"
 batchForm.netPremium column-label "Net!Premium" format "-zz,zzz,zz9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 141.6 BY 8.81 ROW-HEIGHT-CHARS .71 TOOLTIP "Double-click to modify".


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bDeleteBrowse AT ROW 24.57 COL 137.2 WIDGET-ID 146
     tFileNumber AT ROW 5.29 COL 7 COLON-ALIGNED WIDGET-ID 6
     bSearch AT ROW 24.57 COL 123.2 WIDGET-ID 92 NO-TAB-STOP 
     tResidential AT ROW 5.38 COL 43 WIDGET-ID 112
     tZipCode AT ROW 5.29 COL 69 COLON-ALIGNED WIDGET-ID 142
     bSearchClear AT ROW 24.57 COL 128.2 WIDGET-ID 84 NO-TAB-STOP 
     tFormType AT ROW 8.33 COL 12.2 COLON-ALIGNED WIDGET-ID 68
     tPolicy AT ROW 9.57 COL 12.2 COLON-ALIGNED WIDGET-ID 10
     tRateCode AT ROW 10.71 COL 12.2 COLON-ALIGNED WIDGET-ID 140
     tStatCode AT ROW 11.86 COL 12.2 COLON-ALIGNED WIDGET-ID 104
     tForm AT ROW 13.05 COL 12.2 COLON-ALIGNED WIDGET-ID 102
     tEffDate AT ROW 8.33 COL 58.6 COLON-ALIGNED WIDGET-ID 12
     tSearchType AT ROW 24.81 COL 72 COLON-ALIGNED NO-LABEL WIDGET-ID 134
     tLiability AT ROW 9.52 COL 58.6 COLON-ALIGNED WIDGET-ID 16
     tCounty AT ROW 10.67 COL 58.6 COLON-ALIGNED WIDGET-ID 106
     tSearchTitle AT ROW 23.86 COL 1.6 COLON-ALIGNED NO-LABEL WIDGET-ID 130
     tGrossPremium AT ROW 8.29 COL 123.2 COLON-ALIGNED WIDGET-ID 18
     tNetPremium AT ROW 9.48 COL 123.2 COLON-ALIGNED WIDGET-ID 20
     bAdd AT ROW 11.86 COL 125.2 WIDGET-ID 28
     tFilePoliciesCnt AT ROW 5 COL 101 COLON-ALIGNED WIDGET-ID 124 NO-TAB-STOP 
     tSearchErrors AT ROW 24.81 COL 80.4 NO-LABEL WIDGET-ID 116 NO-TAB-STOP 
     tSearchFile AT ROW 24.71 COL 6.2 COLON-ALIGNED WIDGET-ID 80 NO-TAB-STOP 
     tSearchPolicy AT ROW 24.71 COL 48 COLON-ALIGNED WIDGET-ID 82 NO-TAB-STOP 
     tBatchID AT ROW 1.67 COL 9.6 COLON-ALIGNED NO-LABEL WIDGET-ID 66 NO-TAB-STOP 
     tRetention AT ROW 10.67 COL 123.2 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     brwForms AT ROW 14.71 COL 2 WIDGET-ID 200
     tFileGrossPremium AT ROW 3.81 COL 119 COLON-ALIGNED WIDGET-ID 54 NO-TAB-STOP 
     tFileNetPremium AT ROW 6.14 COL 119 COLON-ALIGNED WIDGET-ID 56 NO-TAB-STOP 
     tFileRetention AT ROW 4.95 COL 119 COLON-ALIGNED WIDGET-ID 60 NO-TAB-STOP 
     tTotalCounts AT ROW 26.43 COL 14.2 COLON-ALIGNED WIDGET-ID 34 NO-TAB-STOP 
     tTotalGrossPremium AT ROW 26.38 COL 103.6 COLON-ALIGNED WIDGET-ID 40 NO-TAB-STOP 
     tTotalNetPremium AT ROW 26.38 COL 124 COLON-ALIGNED NO-LABEL WIDGET-ID 42 NO-TAB-STOP 
     tAgentID AT ROW 3.67 COL 7.2 COLON-ALIGNED WIDGET-ID 2 NO-TAB-STOP 
     tAgentName AT ROW 3.67 COL 21 COLON-ALIGNED NO-LABEL WIDGET-ID 4 NO-TAB-STOP 
     tFileEndorsementsCnt AT ROW 6.14 COL 101 COLON-ALIGNED WIDGET-ID 126 NO-TAB-STOP 
     tFormCount AT ROW 13.05 COL 116 COLON-ALIGNED WIDGET-ID 144 NO-TAB-STOP 
     "Process Form" VIEW-AS TEXT
          SIZE 15.6 BY .62 AT ROW 7.29 COL 3.4 WIDGET-ID 62
          FONT 6
     "Batch:" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.81 COL 3.8 WIDGET-ID 108
          FONT 6
     "File Totals" VIEW-AS TEXT
          SIZE 10.2 BY .62 AT ROW 3.24 COL 88.8 WIDGET-ID 138
     AddFormRectangle AT ROW 7.62 COL 2 WIDGET-ID 24
     RECT-3 AT ROW 1.24 COL 2 WIDGET-ID 114
     SearchRect AT ROW 24.19 COL 2 WIDGET-ID 86
     RECT-4 AT ROW 3.48 COL 87.2 WIDGET-ID 136
     RECT-5 AT ROW 24.19 COL 135.8 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 143.6 BY 26.67 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "Batch Processing"
         HEIGHT             = 26.67
         WIDTH              = 143.6
         MAX-HEIGHT         = 33.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwForms tRetention fMain */
/* SETTINGS FOR RECTANGLE AddFormRectangle IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bAdd IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwForms:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwForms:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bSearchClear IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-4 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE SearchRect IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgentID IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tAgentName IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tAgentName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tBatchID IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tBatchID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX tCounty IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tEffDate IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tFileEndorsementsCnt IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFileEndorsementsCnt:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFileGrossPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFileGrossPremium:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFileNetPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFileNetPremium:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFilePoliciesCnt IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFilePoliciesCnt:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFileRetention IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFileRetention:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX tForm IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tFormCount IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX tFormType IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tLiability IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tNetPremium IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tPolicy IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR COMBO-BOX tRateCode IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRetention IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tSearchTitle IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tSearchType IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tStatCode IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tTotalCounts:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tTotalGrossPremium:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tTotalNetPremium:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tZipCode:HIDDEN IN FRAME fMain           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwForms
/* Query rebuild information for BROWSE brwForms
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchform.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwForms */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* Batch Processing */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* Batch Processing */
DO:
/*   run ActionSave in this-procedure (output std-lo). */
/*   if not std-lo                                     */
/*    then return no-apply.                            */

  deleteValidationReport().
  publish "WindowClosed" ("PROCESSING").

  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-RESIZED OF wWin /* Batch Processing */
DO:
 resizeWindow().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd wWin
ON CHOOSE OF bAdd IN FRAME fMain /* Process */
DO:
  deleteValidationReport().

  std-lo = false.

  tResidential:sensitive = false.
  
  case tFormType:screen-value in frame fMain:
   when "P" then std-lo = savePolicy(true).
   when "E" then std-lo = saveEndorsement(true).
   when "C" then std-lo = saveCPL(true).
   when "T" then std-lo = saveCommitment(true).
   when "S" then std-lo = saveSearch(true).
   when "M" then std-lo = saveMisc(true).
  end case.


  if std-lo 
   then 
    do: if tFormType:screen-value in frame fMain = "P"
         then run addDefaultEndorsements in this-procedure
                (tFileNumber:screen-value in frame fMain,
                 tResidential:checked,
                 tLastPolicy,
                 tRateCode:screen-value,
                 tEffDate:input-value,
                 tLiability:input-value,
                 tForm:screen-value).

        clearAddForm().
        displayActiveFile().
        displayTotals().
        
        run DisableActions in h_toolbar ("viewreport").
        apply "tab" to self.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteBrowse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteBrowse wWin
ON CHOOSE OF bDeleteBrowse IN FRAME fMain /* Delete */
DO:
  {lib/confirm-delete.i "All shown forms"}
  
  for each batchform exclusive-lock:
    publish "DeleteBatchForm" (batchform.batchID, batchform.seq, output std-lo, output std-ch).
    if std-lo
     then delete batchform.
  end.
  browse brwForms:refresh().
  displayTotals().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwForms
&Scoped-define SELF-NAME brwForms
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForms wWin
ON DEFAULT-ACTION OF brwForms IN FRAME fMain
DO:
 run ActionModify in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForms wWin
ON RIGHT-MOUSE-CLICK OF brwForms IN FRAME fMain
DO:
  if available batchform and not tFileActive
   then 
    do: tFileNumber:screen-value in frame fMain = batchform.fileNumber.
        tZipCode:screen-value in frame fMain = batchform.zipCode.
        displayFileSummary(batchform.fileNumber).
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForms wWin
ON ROW-DISPLAY OF brwForms IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwForms wWin
ON START-SEARCH OF brwForms IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch wWin
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
 if tSearchFile:screen-value in frame fMain = "" 
   and tSearchPolicy:screen-value in frame fMain = ""
   and tSearchErrors:input-value in frame fMain = "A"
  then return.

/*   searchRect:fgcolor in frame fMain = 4. */
/*   bAdd:sensitive in frame fMain = false. */
  assign
    tSearchFile = tSearchFile:screen-value in frame fMain
    tSearchPolicy = tSearchPolicy:input-value in frame fMain
    tSearchErrors = tSearchErrors:input-value in frame fMain

    bSearchClear:sensitive in frame fMain = true
    tFileNumber:read-only in frame fMain = true
    tFileNumber:sensitive in frame fMain = false
    tResidential:sensitive in frame fMain = false
    tZipCode:read-only in frame fMain = true
    tZipCode:sensitive in frame fMain = false
    .
  run disableActions in h_toolbar("startFile,purge,import,importall,print,details,validate,save,attach").
  
  tSearchActive = true.

  doSearch(tSearchFile:screen-value in frame fMain + "*",
           tSearchPolicy:screen-value in frame fMain + "*",
           tSearchErrors:input-value in frame fMain,
           "matches").
  if tSearchFile > "" 
   then displayFileSummary(tSearchFile).
   else clearFileSummary().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearchClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearchClear wWin
ON CHOOSE OF bSearchClear IN FRAME fMain /* Clear */
DO:
  self:sensitive = false.

  close query brwForms. /* No data should remain in the forms browse */
  clearFileSummary().

  assign
    bSearch:sensitive in frame fMain = true
    tFileNumber:read-only in frame fMain = false
    tFileNumber:sensitive in frame fMain = true
    tResidential:sensitive in frame fMain = true
    tZipCode:read-only in frame fMain = false
    tZipCode:sensitive in frame fMain = true
    .

  run enableActions in h_toolbar("startFile,purge,import,importall,print,details,validate,save,attach").
  tSearchActive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEffDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEffDate wWin
ON LEAVE OF tEffDate IN FRAME fMain /* Effective */
DO:
  if tFormType:screen-value = "C" or tFormType:screen-value = "T"
   then
    return.
    
  if self:input-value > today or self:input-value < today - 270
   then 
    do: self:bgcolor = 14.
        bell.
    end.
   else self:bgcolor = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFileNumber wWin
ON LEAVE OF tFileNumber IN FRAME fMain /* File */
DO:
  if self:screen-value = "" 
   then .
   else self:bgcolor = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFileNumber wWin
ON RETURN OF tFileNumber IN FRAME fMain /* File */
DO:
 run ActionStartFile in this-procedure (output std-lo).
 return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tForm
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tForm wWin
ON VALUE-CHANGED OF tForm IN FRAME fMain /* Form */
DO:
  /* das:wprocessing:ValueChanged Is this correct? */
  if agent.remitType <> "P" 
   then return.

  /* only here if remitType = P)ercentage */
  find stateform
    where stateform.stateID = pStateID
      and stateform.formID = self:screen-value no-error.
  if not available stateform 
   then return.
   
  if tGrossPremium:input-value = 0 and stateform.rateMin > 0 then
  tGrossPremium:screen-value in frame fMain = string(stateform.rateMin).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFormType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFormType wWin
ON LEAVE OF tFormType IN FRAME fMain /* Form Type */
DO:
 if self:input-value = "P" or
    self:input-value = "C" or
    self:input-value = "T"
  then return.
 
 if tEffDate:input-value in frame fMain = ? 
  then tEffDate:screen-value in frame fMain = string(tLastEffDate).

 if tPolicy:screen-value in frame fMain = ""
  then tPolicy:screen-value in frame fMain = string(tLastPolicy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFormType wWin
ON VALUE-CHANGED OF tFormType IN FRAME fMain /* Form Type */
DO:
    assign
      /*tPolicy:sensitive in frame fMain = (index("CS", self:input-value) = 0)*/
      tPolicy:sensitive in frame fMain = yes
      tLiability:sensitive in frame fMain = self:input-value = "P"
      tCounty:sensitive in frame fMain = self:input-value <> "E"
      tFormCount:screen-value = "1"
                
      tEffDate = ?
      tEffDate:screen-value = ""
      tLiability:screen-value = ""
      tGrossPremium:screen-value = ""
      .
    
    if tFormType:input-value = "C" or tFormType:input-value = "T"
     then     
      assign
        tFormCount:sensitive  = true
        tPolicy:sensitive     = false
        tEffDate:sensitive    = false
        tPolicy:screen-value  = ""
        tEffDate:screen-value = ?
        tPolicy:bgcolor       = ?
        tEffDate:bgcolor      = ?
        .
    else
      assign
        tFormCount:sensitive  = false
        tPolicy:sensitive     = true
        tEffDate:sensitive    = true        
        .
    
              
    setForms(self:input-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tGrossPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tGrossPremium wWin
ON LEAVE OF tGrossPremium IN FRAME fMain /* Gross $ */
DO:
  if agent.remitType = "P"
    and tNetPremium:input-value in frame {&frame-name} = 0
   then
    do: std-de = round(self:input-value * agent.remitValue, 2).
        tNetPremium:screen-value in frame {&frame-name} = string(std-de).
    end.
  std-de = self:input-value - tNetPremium:input-value in frame {&frame-name}.
  tRetention:screen-value in frame {&frame-name} = string(std-de).

  self:bgcolor = ?.
  
  find stateform
    where stateform.stateID = pStateID
      and stateform.formID = tForm:input-value no-error.
  if available stateform
    and stateform.rateCheck = "F"
    and (self:input-value < stateform.rateMin
      or self:input-value > stateform.rateMax)
   then self:bgcolor = 14.
  if available stateform
    and stateform.rateCheck = "L"
    and (self:input-value < ((tLiability:input-value in frame {&frame-name} / 1000) * stateform.rateMin)
      or self:input-value > ((tLiability:input-value in frame {&frame-name} / 1000) * stateform.rateMax))
   then self:bgcolor = 14.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLiability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLiability wWin
ON LEAVE OF tLiability IN FRAME fMain /* Liability */
DO:
  if agent.remitType = "L" 
    and agent.remitValue > 0
    and tNetPremium:input-value in frame fMain = 0
   then
    do:
        std-de = round((self:input-value / 1000) * agent.remitValue, 2).
        tNetPremium:screen-value in frame fMain = string(std-de).
    end.

  if self:input-value >= 1000000 
   then 
    do: self:bgcolor = 14.
        bell.
    end.
   else self:bgcolor = ?.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNetPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNetPremium wWin
ON LEAVE OF tNetPremium IN FRAME fMain /* Net $ */
DO:
 if agent.remitType = "P"
   and agent.remitValue > 0
   and tGrossPremium:input-value in frame {&frame-name} = 0 
 then
  do: std-de = round(self:input-value / agent.remitValue, 2).
      tGrossPremium:screen-value in frame {&frame-name} = string(std-de).
  end.
 std-de = tGrossPremium:input-value in frame {&frame-name} - self:input-value.
 tRetention:screen-value in frame {&frame-name} = string(std-de).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPolicy wWin
ON LEAVE OF tPolicy IN FRAME fMain /* Policy */
DO:
  if input tFormType:screen-value = "C" or input tFormType:screen-value = "T"
   then
    return.
    
  if self:screen-value = "" 
   then 
    do: self:bgcolor = 14.
        bell.
    end.
   else 
    do: self:bgcolor = ?.
        publish "IsPolicyValid" (input pAgentID,
                                 input self:input-value,
                                 output xEffDate,
                                 output xLiabilityAmount,
                                 output xGrossPremium,
                                 output xFormID,
                                 output xStateID,
                                 output xFileNumber,
                                 output xStatCode,
                                 output xNetPremium,
                                 output xRetention,
                                 output xCountyID,
                                 output xResidential,
                                 output xInvoiceDate,
                                 output tMsgStat,
                                 output std-ch).
                                 
        if tMsgStat = "S" then  /* Success */
        do:
            if tFormType:screen-value = "P" then
            do:
                if tEffDate:input-value = ? and xEffDate <> ? then
                assign tEffDate:screen-value = string(xEffDate).
                
                if tLiability:input-value = 0 and xLiabilityAmount <> 0 then
                assign tLiability:screen-value = string(xLiabilityAmount).
                
                if tGrossPremium:input-value = 0 and xGrossPremium <> 0 then
                assign tGrossPremium:screen-value = string(xGrossPremium).
                
                if (tCounty:screen-value = "" or tCounty:screen-value = ?) and xCountyID <> "" then
                do:
                    for first county
                        where county.stateID = pStateID
                        and (county.countyID = xCountyID or county.description = xCountyID)
                        no-lock:
                        
                        assign tCounty:screen-value = county.countyID.
                    end.
                end.
                
                if (tStatCode:screen-value = "" or tStatCode:screen-value = ?) 
                and xStatCode <> "" and xStatCode <> ? and xStatCode <> "?" then
                assign tStatCode:screen-value = xStatCode.
                
                if (tForm:screen-value = "" or tForm:screen-value = ?) 
                and xFormID <> "" and xFormID <> ? and xFormID <> "?" then
                assign tForm:screen-value = xFormID.
            end.
            
/*             if tFormType:screen-value = "T" then                   */
/*             do:                                                    */
/*                 if tEffDate:input-value = ? and xEffDate <> ? then */
/*                 assign tEffDate:screen-value = string(xEffDate).   */
/*             end.                                                   */
        end.
        else
        if tMsgStat = "E" then  /* Error */
        do:
            self:bgcolor = 14.
            MESSAGE std-ch
              VIEW-AS ALERT-BOX error BUTTONS OK.
        end.
        else
        if tMsgStat = "W" then  /* Warning */
        do:
            if tFormType:screen-value = "P" then
            do:
                if tEffDate:input-value = ? and xEffDate <> ? then
                assign tEffDate:screen-value = string(xEffDate).
                
                if tLiability:input-value = 0 and xLiabilityAmount <> 0 then
                assign tLiability:screen-value = string(xLiabilityAmount).
                
                if tGrossPremium:input-value = 0 and xGrossPremium <> 0 then
                assign tGrossPremium:screen-value = string(xGrossPremium).
                
                if (tCounty:screen-value = "" or tCounty:screen-value = ?) and xCountyID <> "" then
                do:
                    for first county
                        where county.stateID = pStateID
                        and (county.countyID = xCountyID or county.description = xCountyID)
                        no-lock:
                        
                        assign tCounty:screen-value = county.countyID.
                    end.
                end.
                
                if (tStatCode:screen-value = "" or tStatCode:screen-value = ?) 
                and xStatCode <> "" and xStatCode <> ? then
                assign tStatCode:screen-value = xStatCode.
                
                if (tForm:screen-value = "" or tForm:screen-value = ?) 
                and xFormID <> "" and xFormID <> ? then
                assign tForm:screen-value = xFormID.
            end.

/*             if tFormType:screen-value = "T" then                   */
/*             do:                                                    */
/*                 if tEffDate:input-value = ? and xEffDate <> ? then */
/*                 assign tEffDate:screen-value = string(xEffDate).   */
/*             end.                                                   */
           
            self:bgcolor = 14.
            MESSAGE std-ch
              VIEW-AS ALERT-BOX warning BUTTONS OK.
        end.
        else                    /* Anything else - just in case */
           message tMsgStat skip std-ch
            view-as alert-box error buttons ok.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRateCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRateCode wWin
ON VALUE-CHANGED OF tRateCode IN FRAME fMain /* Rate Code */
DO:
  if self:input-value = "S" then
  do:
    if tEffDate:input-value = ? 
    then tEffDate:screen-value in frame fMain = string(tLastEffDate).
  
    if tCounty:screen-value = "" or tCounty:screen-value = ?
    then tCounty:screen-value in frame fMain = tLastCounty.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tResidential
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tResidential wWin
ON RETURN OF tResidential IN FRAME fMain /* Residential */
DO:
 run ActionStartFile in this-procedure (output std-lo).
 return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSearchFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSearchFile wWin
ON RETURN OF tSearchFile IN FRAME fMain /* File */
DO:
  if tFileActive 
   then return no-apply.
  apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tSearchPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tSearchPolicy wWin
ON RETURN OF tSearchPolicy IN FRAME fMain /* Policy */
DO:
 if tFileActive 
  then return no-apply.
 apply "CHOOSE" to bSearch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStatCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStatCode wWin
ON VALUE-CHANGED OF tStatCode IN FRAME fMain /* STAT */
DO:
  find statcode 
    where statcode.stateID = pStateID
      and statcode.statcode = self:screen-value no-error.
  if not available statcode 
   then return.
   
  if can-find(stateform
    where stateform.stateID = pStateID
      and stateform.formID = statcode.formID
      and stateform.formID <> ""
      and stateform.formType = tFormType:screen-value in frame fMain)
   then 
    do: tForm:screen-value in frame fMain = statcode.formID.
        apply "VALUE-CHANGED" TO tForm in frame fMain.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tZipCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tZipCode wWin
ON LEAVE OF tZipCode IN FRAME fMain /* Zip Code */
DO:
  if self:screen-value = "" 
   then .
   else self:bgcolor = ?.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tZipCode wWin
ON RETURN OF tZipCode IN FRAME fMain /* Zip Code */
DO:
 run ActionStartFile in this-procedure (output std-lo).
 return no-apply.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

{src/adm2/windowmn.i}

{lib/win-main.i}
{lib/brw-main.i}

run initializeObject in this-procedure.

ON 'choose':U OF menu-item m_PopModify in menu formpopmenu
 DO:
     if not available batchform 
      then return.
     run ActionModify in this-procedure (output std-lo).
     RETURN.
 END.

ON 'choose':U OF menu-item m_PopDelete in menu formpopmenu
 DO:
     if not available batchform 
      then return.
     run ActionDelete in this-procedure (output std-lo).
     RETURN.
 END.

ON 'choose':U OF MENU-ITEM m_PopDefaults in menu formpopmenu
 DO:
     if not available batchform
        or (available batchform and batchform.formType <> "P")
      then RETURN.
    
     /* Same sequence as in choose of Process button */  
     run addDefaultEndorsements in this-procedure
        (batchform.fileNumber,
         batchform.residential,
         batchform.policyID,
         batchform.rateCode,
         batchform.effDate,
         batchform.liabilityAmount,
         batchform.formID).
    
     clearAddForm(). 
     displayActiveFile().
     displayTotals().
            
     run DisableActions in h_toolbar ("viewreport").
 END.

/* ON 'choose':U OF menu-item m_PopSearch in menu formpopmenu                */
/* DO:                                                                       */
/*  if not available batchform                                               */
/*   then return.                                                            */
/*                                                                           */
/*  tSearchFile:screen-value in frame fMain = batchform.fileNumber.          */
/*  tSearchPolicy:screen-value in frame fMain = "".                          */
/*                                                                           */
/*  bSearchClear:sensitive in frame fMain = true.                            */
/*  run disableActions in h_toolbar("startFile,import,print,validate,save"). */
/*  tSearchActive = true.                                                    */
/*  self:sensitive = false.                                                  */
/*                                                                           */
/*  tFileNumber:screen-value in frame fMain = batchform.fileNumber.          */
/*  displayActiveFile().                                                     */
/*                                                                           */
/*  RETURN.                                                                  */
/* END.                                                                      */

ON 'ALT-S':U anywhere
 DO:
     run ActionStartFile in this-procedure (output std-lo).
     RETURN.
 END.

ON 'ALT-C':U anywhere
 DO:
     run ActionStopFile in this-procedure (output std-lo).
     RETURN.
 END.


{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Don't let the first column move around... */
batchform.seq:movable in browse brwForms = false.

browse brwForms:POPUP-MENU = MENU formpopmenu:HANDLE.

run addButton in h_toolbar
  ("startFile", "Start File", "Start File (Alt-S)", "images/start.bmp", "", "images/start-i.bmp", 
   false, true, this-procedure, ?).  
run addButton in h_toolbar
  ("stopFile", "Completed File", "Completed File (Alt-C)", "images/stop.bmp", "", "images/stop-i.bmp", 
   false, true, this-procedure, ?).  
/* run addButton in h_toolbar                                                                     */
/*   ("modify", "Modify", "Modify Selected Form", "images/update.bmp", "", "images/update-i.bmp", */
/*    false, true, this-procedure, menu-item m_PopModify:handle in menu formpopmenu).             */
run addButton in h_toolbar
  ("purge", "", "Purge all forms from the batch", "images/trash.bmp", "", "images/trash-i.bmp", 
   false, true, this-procedure, ?).  
run addButton in h_toolbar
  ("import", "", "Import forms from a file", "images/import.bmp", "", "images/import-i.bmp", 
   false, true, this-procedure, ?).  
run addButton in h_toolbar
  ("importall", "", "Bulk import forms from a file", "images/importred.bmp", "", "images/import-i.bmp", 
   false, true, this-procedure, ?).  
run addButton in h_toolbar
  ("attach", "Attach", "View Batch Attachment(s)", "images/attach.bmp", "", "images/attach-i.bmp", 
   false, true, this-procedure, ?).  
run addButton in h_toolbar
  ("details", "Details", "Batch Details", "images/report.bmp", "", "images/report-i.bmp",
   false, true, this-procedure, ?).
/*  run addButton in h_toolbar                                                              */
/*    ("print", "Print", "Print Batch Details", "images/pdf.bmp", "", "images/pdf-i.bmp", */
/*     false, true, this-procedure, ?).                                                     */
/*  run addButton in h_toolbar                                                                                */
/*    ("validate", "Validate", "Validate Processing", "images/spellcheck.bmp", "", "images/spellcheck-i.bmp", */
/*     false, true, this-procedure, ?).                                                                       */
/*  run addButton in h_toolbar                                                                                */
/*    ("viewReport", "ViewReport", "View Validation PDF", "images/pdfcheck.bmp", "", "images/pdfcheck-i.bmp", */
/*     false, true, this-procedure, ?).                                                                       */
run addButton in h_toolbar
  ("errors", "Errors", "View Error Codes", "images/pageerror.bmp", "", "images/pageerror-i.bmp",
   false, true, this-procedure, ?).
run addButton in h_toolbar
  ("save", "Save", "Close Batch", "images/check.bmp", "", "images/check-i.bmp",
   false, true, this-procedure, ?).
/*  run addButton in h_toolbar                                                                  */
/*    ("save", "Save", "Processing Complete", "images/process.bmp", "", "images/process-i.bmp", */
/*     false, true, this-procedure, ?).                                                         */
 
 publish "GetBatchFormZipCode" (pStateID, output xZipCode).

 assign
  tBatchID:screen-value in frame fMain = string(pBatchID)
  tAgentID:screen-value in frame fMain = pAgentID
  tAgentName:screen-value in frame fMain = agent.name
  tBatchID = pBatchID
  tAgentID = pAgentID
  tAgentName = agent.name
  tSearchTitle = "Search"
  tZipCode:hidden = not xZipCode
  .

  {lib/set-button.i &label="Search"       &image="images/s-magnifier.bmp" &inactive="images/s-magnifier-i.bmp"}
  {lib/set-button.i &label="SearchClear"  &image="images/s-cancel.bmp"    &inactive="images/s-cancel-i.bmp"}
  {lib/set-button.i &label="DeleteBrowse" &image="images/s-delete.bmp"    &inactive="images/s-delete-i.bmp"}
  setButtons().
  
  bDeleteBrowse:sensitive = false.

 publish "GetCounties" (output table county).
 setCounties().

 publish "GetStatCodes" (output table statcode).
 setStatCodes().

 publish "GetStateForms" (output table stateform).
 setForms("P").

 setAllForms().
 
 publish "GetProcessingErrors" (output table proerr).

 publish "WindowOpened" ("PROCESSING").

 /* getData().*/
 displayTotals().

 run EnableActions in h_toolbar ("startFile,attach,purge,import,importall,details,errors,save").

 apply "ENTRY" TO tFileNumber in frame fMain.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionAttach wWin 
PROCEDURE ActionAttach :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def output parameter pError as logical init false.

  def buffer batch for batch.

  publish "GetBatch" (pBatchID, output table batch).
  find first batch no-error.
  if not available batch 
   then return.

  if not valid-handle(hDocs)
   then run repository-lite.w persistent set hDocs ("Batch", string(batch.batchID)).
   else run ShowWindow in hDocs.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete wWin 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 def buffer x-batchform for batchform.

 if not available batchform 
  then return.

 session:set-wait-state("general").
 
 find x-batchform
   where rowid(x-batchform) = rowid(batchform).

 publish "DeleteBatchForm" (batchform.batchID,
                            batchform.seq,
                            output std-lo,
                            output std-ch).
 
 session:set-wait-state("").
 
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 delete x-batchform.
 browse brwForms:delete-selected-rows().

 if tFileActive
  then displayFileSummary(tFileNumber).
  else 
 if tSearchActive 
  then displayFileSummary(tSearchFile).
 
 displayTotals().
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDetails wWin 
PROCEDURE ActionDetails :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

publish "SetCurrentValue" ("BatchID", string(pBatchID)).
run wops01-r.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionErrors wWin 
PROCEDURE ActionErrors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

std-lo = false.
publish "IsProcessingErrorsOpen" (output std-lo).
if std-lo 
 then return.
run wproerr.w persistent.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionImport wWin 
PROCEDURE ActionImport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.

 run util/importdialog.w 
  ("File,Residential,FormType,Policy,RateCode,STAT,Form,Effective,Liability,County,Gross,Net,FormCount",
   "CH,LO,CH,IN,CH,CH,CH,DA,DE,CH,DE,DE,IN",
   "Column 1 is File Number: must not be blank||Column 2 is Residential: required for Policy||Column 3 is FormType: P)olicy or E)ndorsement||Column 4 is Policy: must not be blank||Column 5 is RateCode: should not be blank||Column 6 is STAT Code: if applicable or blank||Column 7 is Form: should not be blank||Column 8 is Effective Date: required||Column 9 is Liability: required for Policy, ignored for Endorsement||Column 10 is County: if applicable or blank, ignored for Endorsement||Column 11 is Gross Premium||Column 12 is Net Premium||Column 13 is Form Count: if applicable",
   "ActionImportRecord" /* Internal procedure called to import each record */).
 
 clearFileSummary().
 clearAddForm().
 displayTotals().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionImportAll wWin 
PROCEDURE ActionImportAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def output parameter pError as logical init false.
 
/*   do with frame {&frame-name}:                                                  */
/*     if not tZipCode:hidden and tZipCode:screen-value = ""                       */
/*      then                                                                       */
/*       do:                                                                       */
/*         message "Please enter a zip code" view-as alert-box warning buttons ok. */
/*         apply "ENTRY" to tZipCode.                                              */
/*         return.                                                                 */
/*       end.                                                                      */
/*   end.                                                                          */

  run util/importdialognew.w 
    ("File,Residential,FormType,Policy,RateCode,STAT,Form,Effective,Liability,County,Gross,Net,FormCount",
     "CH,LO,CH,IN,CH,CH,CH,DA,DE,CH,DE,DE,IN",
     "Column 1 is File Number: must not be blank||Column 2 is Residential: required for Policy||Column 3 is FormType: P)olicy or E)ndorsement||Column 4 is Policy: must not be blank||Column 5 is RateCode: should not be blank||Column 6 is STAT Code: if applicable or blank||Column 7 is Form: should not be blank||Column 8 is Effective Date: required||Column 9 is Liability: required for Policy, ignored for Endorsement||Column 10 is County: if applicable or blank, ignored for Endorsement||Column 11 is Gross Premium||Column 12 is Net Premium||Column 13 is Form Count: if applicable",
     "ActionImportRecordAll" /* Internal procedure called to import each record */).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionImportRecord wWin 
PROCEDURE ActionImportRecord :
/*------------------------------------------------------------------------------
  Purpose:     Callback routine used by the generic Import Dialog
  Parameters:  <none>
  Notes:       
               [1]  - File number
               [2]  - Residential (Y/N)
               [3]  - Form Type - (P)olicy/(E)ndorsement
               [4]  - Policy number
               [5]  - Rate Code
               [6]  - STAT code
               [7]  - FormID
               [8]  - Effective date
               [9]  - Liability amount
               [10] - County code
               [11] - Gross Premium
               [12] - Net Premium
               [13] - FormCount
------------------------------------------------------------------------------*/
 def input parameter table for importdata.
 def output parameter pSuccess as logical init false.
 def output parameter pErrorMsg as char init "".

 /* Data will be available if called by the select file dialog */
 find first importdata no-error.
 if not available importdata 
  then return.  

 /* Checking of datatypes already performed by the select file dialog */
 std-in = integer(importdata.data[4]) no-error.
 if error-status:error 
  then return.


 if importdata.data[3] = "P" or importdata.data[3] = ""
  then std-da = date(importdata.data[8]) no-error.
 if error-status:error 
  then return.

 if importdata.data[3] = "P" or importdata.data[3] = ""
  then std-de = decimal(importdata.data[9]) no-error.
 if error-status:error 
  then return.

 /* If type is not defined, and effective date and liability were set correctly = "P"olicy else "E"ndorsement */
 if importdata.data[3] = "" and std-da <> ? and std-de > 0
  then importdata.data[3] = "P".
 if importdata.data[3] = "" 
  then importdata.data[3] = "E".

std-in = integer(importdata.data[13]) no-error.
 if error-status:error or 
    std-in < 1
  then 
   importdata.data[13] = "1".

 std-de = decimal(importdata.data[11]) no-error.
 if error-status:error 
  then return.

 std-de = decimal(importdata.data[12]) no-error.
 if error-status:error 
  then return.

 pErrorMsg = saveForm(importdata.data[1],
                      if lookup(importdata.data[2], "Y,y,Yes,yes,T,t,True,true") > 0 then true else false,
                      importdata.data[3],
                      int(importdata.data[4]),
                      importdata.data[5],
                      importdata.data[6],
                      importdata.data[7],
                      if importdata.data[3] = "P"
                       then date(importdata.data[8]) else ?,
                      if importdata.data[3] = "P"
                       then decimal(importdata.data[9]) else 0,
                      if importdata.data[3] = "P"
                       then importdata.data[10] else "",
                      decimal(importdata.data[11]),
                      decimal(importdata.data[12]),
                      int(importdata.data[13]),
                      false).
 if pErrorMsg = "" 
  then pSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionImportRecordAll wWin 
PROCEDURE ActionImportRecordAll :
/*------------------------------------------------------------------------------
  Purpose:     Callback routine used by the generic Import Dialog to pass all
               the records to the server
  Parameters:  <none>
  Notes:       [1]  - File number
               [2]  - Residential (Y/N)
               [3]  - Form Type - (P)olicy/(E)ndorsement
               [4]  - Policy number
               [5]  - Rate Code
               [6]  - STAT code
               [7]  - FormID
               [8]  - Effective date
               [9]  - Liability amount
               [10] - County code
               [11] - Gross Premium
               [12] - Net Premium
               [13]  - FormCount
------------------------------------------------------------------------------*/
  define input parameter table for importdata.
  define output parameter pSuccess as logical initial true.
  define output parameter pErrorMsg as character initial "".
  
  for each importdata no-lock:
    if not pSuccess
     then next.
     
    /* Checking of datatypes already performed by the select file dialog */
    std-in = integer(importdata.data[4]) no-error.
    if error-status:error 
     then pSuccess = false.
    
    if importdata.data[3] = "P" or importdata.data[3] = ""
     then std-da = date(importdata.data[8]) no-error.
    if error-status:error 
     then pSuccess = false.
    
    if importdata.data[3] = "P" or importdata.data[3] = ""
     then std-de = decimal(importdata.data[9]) no-error.
    if error-status:error 
     then pSuccess = false.
    
    /* If type is not defined, and effective date and liability were set correctly = "P"olicy else "E"ndorsement */
    if importdata.data[3] = "" and std-da <> ? and std-de > 0
     then importdata.data[3] = "P".
    if importdata.data[3] = "" 
     then importdata.data[3] = "E".
    
    std-de = decimal(importdata.data[11]) no-error.
    if error-status:error 
     then pSuccess = false.
    
    std-de = decimal(importdata.data[12]) no-error.
    if error-status:error 
     then pSuccess = false.
     
   std-in = integer(importdata.data[13]) no-error.
    if error-status:error or
       std-in < 1
     then 
      importdata.data[13] = "1".
      
    if pSuccess
     then importdata.data[14] = tZipCode:screen-value in frame {&frame-name}.  
  end.
  
  pErrorMsg = "".
  if pSuccess
   then 
    do:
      assign
       std-ch = ""
       rpt-behavior = "Q".
       
      run server/importbatchform.p (pBatchID,                /* batch number */
                                    input table importdata,  /* the import records */
                                    {lib/rpt-setparams.i},
                                    output std-ch,
                                    output pSuccess,
                                    output pErrorMsg
                                    ).
      if pSuccess
       then
        do:
          message "Batch Import submitted successfully." view-as alert-box information buttons ok.
          if search(std-ch) <> ?
           then
            do:
              std-lo = true.
              message "Would you like to view the validation file?" view-as alert-box question buttons yes-no update std-lo.
              if std-lo
               then publish "OpenTempFile" ("BatchValidation").
            end.
        end.
        apply "WINDOW-CLOSE" to {&window-name}.
    end.
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify wWin 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

def var pFormType as char.
def var pFile as char.
def var tFile as char.
def var pPolicy as int.
def var tPolicy as int.
def var pForm as char.
def var pFormCount as int.
def var pRateCode as char.
def var pSTAT as char.
def var pEffDate as datetime.
def var pLiability as dec.
def var pCounty as char.
def var pResidential as logical no-undo.
def var pGrossPremium as dec.
def var pNetPremium as dec.
def var pZipCode as char.
def var pAction as char init "UNDO".

def buffer x-batchform for batchform.
def var tRedo as logical no-undo.

if not available batchform 
 then return.

assign
  pFormType = batchform.formType
  pFile = batchform.fileNumber
  tFile = batchform.fileNumber
  pPolicy = batchform.policyID
  tPolicy = batchform.policyID
  pForm = batchform.formID
  pFormCount = batchform.formCount
  pRateCode = batchform.rateCode
  pSTAT = batchform.statCode
  pEffDate = batchform.effDate
  pLiability = batchform.liabilityAmount
  pCounty = batchform.countyID
  pResidential = batchform.residential
  pGrossPremium = batchform.grossPremium
  pNetPremium = batchform.netPremium
  pZipCode = tZipCode:input-value in frame fMain
  .
  
case pFormType:
 when "P" then run dialogmodifybpolicy.w
                 (input-output pFile,
                  input-output pPolicy,
                  input-output pForm,
                  input-output pRateCode,
                  input-output pSTAT,
                  input-output pEffDate,
                  input-output pLiability,
                  input-output pCounty,
                  input-output pResidential,
                  input-output pGrossPremium,
                  input-output pNetPremium,
                  input-output pZipCode,
                  input "Policy",
                  input tAllFormslist, /* We need to give the complete list, not type specific */
                  input tStatCodesList,
                  input tCountiesList,
                  input batchform.validmsg,
                  input pStateID,
                  input table agent,
                  input table stateform,
                  output pAction).

 when "E" then run dialogmodifybend.w
                 (input-output pPolicy,
                  input-output pForm,
                  input-output pRateCode,
                  input-output pSTAT,
                  input-output pEffDate,
                  input-output pGrossPremium,
                  input-output pNetPremium,
                  input tAllFormslist, /* We need to give the complete list, not type specific */
                  input tStatCodesList,
                  input batchform.validmsg,
                  input table agent,
                  output pAction).

 when "C" then run dialogmodifybcpl.w
                 (input-output pPolicy,
                  input-output pForm,
                  input-output pRateCode,
                  input-output pSTAT,
                  input-output pEffDate,
                  input-output pCounty,
                  input-output pGrossPremium,
                  input-output pNetPremium,
                  input-output pFormCount,
                  input tAllFormslist, /* We need to give the complete list, not type specific */
                  input tStatCodesList,
                  input tCountiesList,
                  input batchform.validmsg,
                  input table agent,
                  output pAction).

 when "T" then run dialogmodifybcom.w
                 (input-output pPolicy,
                  input-output pForm,
                  input-output pRateCode,
                  input-output pSTAT,
                  input-output pEffDate,
                  input-output pCounty,
                  input-output pGrossPremium,
                  input-output pNetPremium,
                  input-output pFormCount,
                  input tAllFormslist, /* We need to give the complete list, not type specific */
                  input tStatCodesList,
                  input tCountiesList,
                  input batchform.validmsg,
                  input table agent,
                  output pAction).
                  
 when "S" then run dialogmodifybsearch.w
                 (input-output pFile,
                  input-output pForm,
                  input-output pRateCode,
                  input-output pSTAT,
                  input-output pEffDate,
                  input-output pCounty,
                  input-output pGrossPremium,
                  input-output pNetPremium,
                  input tAllFormslist, /* We need to give the complete list, not type specific */
                  input tStatCodesList,
                  input tCountiesList,
                  input batchform.validmsg,
                  output pAction).

 when "M" then run dialogmodifybmisc.w
                 (input-output pFile,
                  input-output pPolicy,
                  input-output pForm,
                  input-output pRateCode,
                  input-output pSTAT,
                  input-output pEffDate,
                  input-output pCounty,
                  input-output pGrossPremium,
                  input-output pNetPremium,
                  input tAllFormslist, /* We need to give the complete list, not type specific */
                  input tStatCodesList,
                  input tCountiesList,
                  input batchform.validmsg,
                  output pAction).
end case.

tZipCode:screen-value in frame fMain = pZipCode.

/* run dialogmodifyform.w                                                              */
/*   (input-output pFormtype,                                                          */
/*    input-output pFile,                                                              */
/*    input-output pPolicy,                                                            */
/*    input-output pForm,                                                              */
/*    input-output pSTAT,                                                              */
/*    input-output pEffDate,                                                           */
/*    input-output pLiability,                                                         */
/*    input-output pCounty,                                                            */
/*    input-output pResidential,                                                       */
/*    input-output pGrossPremium,                                                      */
/*    input-output pNetPremium,                                                        */
/*    input tAllFormslist, /* We need to give the complete list, not type specific */  */
/*    input tStatCodesList,                                                            */
/*    input tCountiesList,                                                             */
/*    input batchform.validmsg,                                                        */
/*    output pAction).                                                                 */

case pAction:
  when "UNDO" then return.
  when "SAVE" 
   then 
    do: 
      if pFormType = "P" and pFile <> tFile
       then
        for each x-batchform exclusive-lock
           where x-batchform.fileNumber = tFile
             and x-batchform.policyID = tPolicy:
           
          if x-batchform.formType = "P"
           then publish "ModifyBatchForm" (x-batchform.batchID,
                                           x-batchform.seq,
                                           pFormType,
                                           pFile,
                                           pPolicy,
                                           pForm,
                                           pFormCount,
                                           pRateCode,
                                           pSTAT,
                                           pEffDate,
                                           pLiability,
                                           pCounty,
                                           pResidential,
                                           pGrossPremium,
                                           pNetPremium,
                                           pZipCode,
                                           output tRevenueType,
                                           output tRedo,
                                           output std-lo,
                                           output std-ch).
           else publish "ModifyBatchForm" (x-batchform.batchID,
                                           x-batchform.seq,
                                           x-batchform.formType,
                                           pFile,
                                           x-batchform.policyID,
                                           x-batchform.formID,
                                           x-batchform.formCount,
                                           x-batchform.rateCode,
                                           x-batchform.statCode,
                                           x-batchform.effDate,
                                           x-batchform.liabilityAmount,
                                           x-batchform.countyID,
                                           x-batchform.residential,
                                           x-batchform.grossPremium,
                                           x-batchform.netPremium,
                                           pZipCode,
                                           output tRevenueType,
                                           output tRedo,
                                           output std-lo,
                                           output std-ch).

          if not std-lo 
           then
            do:
                MESSAGE std-ch
                 VIEW-AS ALERT-BOX error BUTTONS OK.
                return.
            end.

          assign
            x-batchform.formType = pFormType
            x-batchform.fileNumber = pFile
            x-batchform.policyID = pPolicy
            x-batchform.formID = pForm
            x-batchform.formCount = pFormCount         
            x-batchform.rateCode = pRateCode
            x-batchform.statCode = pSTAT
            x-batchform.effDate = pEffDate
            x-batchform.liabilityAmount = pLiability
            x-batchform.countyID = pCounty
            x-batchform.residential = pResidential
            x-batchform.grossPremium = pGrossPremium
            x-batchform.netPremium = pNetPremium
            x-batchform.reprocess = tRedo
            x-batchform.revenueType = tRevenueType
            .
        end.
       else
        do:
          find x-batchform where rowid(x-batchform) = rowid(batchform).
          publish "ModifyBatchForm" (x-batchform.batchID,
                                     x-batchform.seq,
                                     pFormType,
                                     pFile,
                                     pPolicy,
                                     pForm,
                                     pFormCount,
                                     pRateCode,
                                     pSTAT,
                                     pEffDate,
                                     pLiability,
                                     pCounty,
                                     pResidential,
                                     pGrossPremium,
                                     pNetPremium,
                                     pZipCode,
                                     output tRevenueType,
                                     output tRedo,
                                     output std-lo,
                                     output std-ch).

          if not std-lo 
           then
            do:
                MESSAGE std-ch
                 VIEW-AS ALERT-BOX error BUTTONS OK.
                return.
            end.

          assign
            x-batchform.formType = pFormType
            x-batchform.fileNumber = pFile
            x-batchform.policyID = pPolicy
            x-batchform.formID = pForm
            x-batchform.formCount = pFormCount         
            x-batchform.rateCode = pRateCode
            x-batchform.statCode = pSTAT
            x-batchform.effDate = pEffDate
            x-batchform.liabilityAmount = pLiability
            x-batchform.countyID = pCounty
            x-batchform.residential = pResidential
            x-batchform.grossPremium = pGrossPremium
            x-batchform.netPremium = pNetPremium
            x-batchform.reprocess = tRedo
            x-batchform.revenueType = tRevenueType
            .
          release x-batchform.
        end.

       if tFileActive 
        then displayActiveFile().
        else
         do: 
             clearFileSummary().
/*              displayForms().  */
         end.
       displayTotals().
   end.
 when "DELETE" 
  then run ActionDelete in this-procedure (output pError).
end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionPrint wWin 
PROCEDURE ActionPrint :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

 run ops01-r.p (pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionPurge wWin 
PROCEDURE ActionPurge :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 std-lo = false.
 MESSAGE "All forms will be permanently removed from the batch." skip
         "This action cannot be undone." skip(2)
         "Continue?"
  VIEW-AS ALERT-BOX warning BUTTONS YES-NO update std-lo.
 
 if not std-lo 
  then return.
 
 publish "PurgeAllBatchForms" (pBatchID, output std-lo, output std-ch).
 
 if std-lo 
  then
    MESSAGE "All forms purged from the batch"
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
  else
    MESSAGE std-ch
     VIEW-AS ALERT-BOX error BUTTONS OK.
 displayTotals().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionSave wWin 
PROCEDURE ActionSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.

def var tStat as char.
def var tComments as char.
def var tBatchChanged as log.

run dialogsavebatch.w (output pError,
                       output tStat,
                       output tComments).
if pError 
 then return.
 

if tStat = "G" then /* No Issues / Move to Revieiwing */
do:
    publish "GetBatch" (pBatchID, output table batch).
    find first batch no-error.
    if not available batch then
    do:
        message "Batch record for batch " + string(pBatchID) + " not found."
            view-as alert-box error.
        return.
    end.
     
    if batch.grossPremiumReported <> batch.grossPremiumProcessed
    or batch.netPremiumReported <> batch.netPremiumProcessed then
    do: 
        MESSAGE "Batch totals do not match.  Continue?"
           VIEW-AS ALERT-BOX question BUTTONS Yes-No 
           title "Confirm Close Batch"
           set std-lo.
        if not std-lo
        then return.
    end.

    session:set-wait-state("general").
    
    publish "UnlockBatch" (pBatchID,
                           output std-lo,
                           output std-ch).

    publish "ToReviewBatch" (batch.batchID,
                             output std-lo,
                             output std-ch).
    
    session:set-wait-state("").
    
    if not std-lo 
     then
      do:
          MESSAGE std-ch 
            skip(1)
            "Review validation report."
           VIEW-AS ALERT-BOX INFO BUTTONS OK.
          return.
      end.
    
    batch.stat = "R".
    
    MESSAGE "Batch is now in Reviewing."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
end.
else
if tStat = "R" then /* Unresolved Issue(s) / Remain in Processing */
do:
    publish "NewBatchAction" (pBatchID,
                              tComments,
                              tStat,
                              false,
                              output std-in,
                              output tBatchChanged,
                              output std-lo,
                              output std-ch).
    
    publish "UnlockBatch" (pBatchID,
                           output std-lo,
                           output std-ch).
end.
else
do:
    message "Invalid Action. Cannot close batch"
        view-as alert-box error.
    return.
end.

/*das:ActionSave in wprocessing*/
apply "WINDOW-CLOSE" to wWin.
/*                                        */
/* publish "WindowClosed" ("PROCESSING"). */
/* APPLY "CLOSE":U TO THIS-PROCEDURE.     */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionStartFile wWin 
PROCEDURE ActionStartFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.

 if tFileActive or tSearchActive
  then return.

 if tFileNumber:screen-value in frame fMain = ""
  then 
   do: tFileNumber:bgcolor in frame fMain = 14.
       MESSAGE "File Number cannot be blank"
        VIEW-AS ALERT-BOX error BUTTONS OK.
       apply 'entry' to tFileNumber.
       return.
   end.
   
/*  if tZipCode:visible and (tZipCode:screen-value = "" or tZipCode:screen-value = ?) then */
/*    do: tZipCode:bgcolor in frame fMain = 14.                                            */
/*        MESSAGE "Zip Code cannot be blank"                                               */
/*         VIEW-AS ALERT-BOX error BUTTONS OK.                                             */
/*        apply 'entry' to tZipCode.                                                       */
/*        return.                                                                          */
/*    end.                                                                                 */
 
 /* Hold these values for unusual redisplay */
 assign
   tFileNumber:screen-value in frame fMain = trim(tFileNumber:screen-value in frame fMain)
   tFileNumber = tFileNumber:screen-value in frame fMain
   tResidential = tResidential:checked in frame fMain
   tZipCode:screen-value in frame fMain = trim(tZipCode:screen-value in frame fMain)
   tZipCode = tZipCode:input-value in frame fMain
   .

 /* Cannot change File Number at this time */
 tFileNumber:read-only in frame fMain = true.
 tFileNumber:sensitive in frame fMain = false.
 tFileNumber:bgcolor in frame fMain = ?.
 tResidential:sensitive in frame fMain = true.
/*  tZipCode:read-only in frame fMain = true.  */
/*  tZipCode:sensitive in frame fMain = false. */
/*  tZipCode:bgcolor in frame fMain = ?.       */

 assign
   tSearchFile:sensitive in frame fMain = false
   tSearchPolicy:sensitive in frame fMain = false
   tSearchErrors:sensitive in frame fMain = false
   bSearch:sensitive in frame fMain = false
   .

 displayActiveFile().

 run disableActions in h_toolbar ("startFile,purge,import,importall,save").
 run enableActions in h_toolbar ("stopFile").

 bSearch:sensitive in frame fMain = false.
 bDeleteBrowse:sensitive in frame fMain = true.
/*  menu-item m_PopSearch:sensitive in menu formpopmenu = false. */

 setAddFormState(true). /* Enable AddForm fields */

 tFormType:screen-value = "P".
 tFormCount:screen-value = "1".
 tLastPolicy = 0.
 tLastEffDate = ?.
 tLastCounty = "".

 setForms("P").

 tFormCount:sensitive = (tFormType:input-value = "C" or tFormType:input-value = "T").
 tFileActive = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionStopFile wWin 
PROCEDURE ActionStopFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pError as logical init false.
 
 if not tFileActive or tSearchActive
  then return.

 clearFileSummary().
 clearAddForm().
 setAddFormState(false). /* Disable AddForm fields */
/*  displayForms().  */
 close query brwForms.

 run disableActions in h_toolbar ("stopFile").
 run enableActions in h_toolbar ("startFile,purge,import,importall,save").
 
 /* Set search field to last file handled for ease of looking up what we just did */
 tSearchFile:screen-value in frame fMain = tFileNumber:screen-value in frame fMain.
 bSearch:sensitive in frame fMain = true.
/*  menu-item m_PopSearch:sensitive in menu formpopmenu = false. */
 
 /* Can change File data at this time */
 assign
   tFileNumber:read-only in frame fMain = false
   tFileNumber:sensitive in frame fMain = true
   tFileNumber:screen-value in frame fMain = ""
   tFileNumber = ""
   tResidential:sensitive in frame fMain = true
   tResidential:checked in frame fMain = true
   tResidential = true
   tZipCode:read-only in frame fMain = false
   tZipCode:sensitive in frame fMain = true
   tZipCode:screen-value in frame fMain = ""
   tZipCode = ?
   .

 /* Enable Search */
 assign
   tSearchFile:sensitive in frame fMain = true
   tSearchPolicy:sensitive in frame fMain = true
   tSearchErrors:sensitive in frame fMain = true
   bSearch:sensitive in frame fMain = true
   bDeleteBrowse:sensitive in frame fMain = false
   .

 tFileActive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionValidate wWin 
PROCEDURE ActionValidate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/* THIS PROCEDURE IS CURRENTLY NOT CALLED ANYWHERE */

def output parameter pError as logical init false.

def var tErr as int no-undo.
def var tWarn as int no-undo.
def var tGood as int no-undo.

frame fMain:sensitive = false.
publish "ValidateBatch" (input pBatchID,
                         output std-lo,
                         output std-ch,
                         output tErr,
                         output tWarn,
                         output tGood).
frame fMain:sensitive = true.

if std-lo and tErr = 0
 then MESSAGE "Batch validated with no errors."
       VIEW-AS ALERT-BOX info BUTTONS OK
        title "Batch Validated Successfully".
 else
if not std-lo 
 then MESSAGE std-ch
       VIEW-AS ALERT-BOX warning BUTTONS OK
        title "Batch Validation Failed".
 else MESSAGE  tErr "errors" skip
               tWarn "warnings" skip(2)
              "Review the batch validation report"
       VIEW-AS ALERT-BOX warning BUTTONS OK
        title "Batch Validated With Errors".

 if not std-lo 
  then return. /* Major failure (network, etc.) */

 /* Reload the batch to capture validation flags */
/*  getData().  */
 run EnableActions in h_toolbar ("viewreport").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionViewReport wWin 
PROCEDURE ActionViewReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter pError as logical init false.
def var tValidationReport as char init "Blank.Compass.None" no-undo.

publish "GetTempFile" (string(pBatchID) + "validation",
                       output tValidationReport).

if search(tValidationReport) = ? 
 then
  do: 
      MESSAGE "Validation report does not exist" skip(1)
              "Please run validation again"
       VIEW-AS ALERT-BOX warning BUTTONS OK.
      return.
  end.

run util/openfile.p (tValidationReport).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addDefaultEndorsements wWin 
PROCEDURE addDefaultEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pFileNumber as char.
def input parameter pResidential as logical.
def input parameter pPolicyID as int.
def input parameter pRateCode as char.
def input parameter pEffDate as date.
def input parameter pLiability as dec.
def input parameter pFormID as char.

def buffer x-batchform for batchform.
def var xFileNumber as char init "" no-undo.
def var xEffDate as datetime no-undo.
def var xCounty as char no-undo.

def var tRedo as logical no-undo.
def var tAdded as logical no-undo init false.

def buffer defaultEnd for defaultEnd.
def buffer stateform for stateform.

find first stateform
  where stateform.stateID = pStateID
    and stateform.formID = pFormID no-error.
if not available stateform 
 then return.

empty temp-table defaultEnd.
publish "GetDefaultEndorsements" (pAgentID,
                                  pStateID,
                                  stateform.insuredType,
                                  output table defaultEnd).
                                  
if not can-find(first defaultEnd where defaultEnd.type = "D")
 then return.

run dialogdefend.w (input-output table defaultEnd,
                    input table agent,
                    input table stateform,
                    input pStateID,
                    input pLiability,
                    output std-lo).
if not std-lo 
 then return.

for each defaultEnd
  where defaultEnd.include = true
    and defaultEnd.formID > "":

 /* das:addDefaultEndorsements:input-output changeable fields */
 xFileNumber = pFileNumber.
 xEffDate = pEffDate.
 xCounty = "".
 publish "NewBatchForm" (pBatchID,
                        "E",
                        input-output xFileNumber, /* FileNumber */
                        pPolicyID,
                        pRateCode,
                        input-output defaultEnd.formID,
                        1,                        /* Number of forms (formCount)*/
                        input-output defaultEnd.statcode, 
                        input-output xEffDate,
                        0.0, /* Liability */
                        input-output xCounty,
                        pResidential,
                        defaultEnd.grossPremium,
                        defaultEnd.netPremium,
                        tZipCode:input-value in frame fMain,
                        output tRevenueType,
                        output tLastSeq,
                        output std-lo,
                        output std-ch,
                        output tRedo).

 if not std-lo 
  then
   do: MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
       next.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "E"
   x-batchform.fileNumber = xFileNumber /* Set to policy FileNumber */
   x-batchform.policyID = pPolicyID
   x-batchform.rateCode = pRateCode
   x-batchform.formID = defaultEnd.formID
   x-batchform.formCount = 1
   x-batchform.statCode = defaultEnd.statcode 
   x-batchform.effDate = xEffDate /* May be set to policy effDate */
   x-batchform.liabilityAmount = 0.0
   x-batchform.countyID = xCounty
   x-batchform.residential = pResidential
   x-batchform.grossPremium = defaultEnd.grossPremium
   x-batchform.netPremium = defaultEnd.netPremium 
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.reprocess = tRedo
   x-batchform.revenueType = tRevenueType   
   x-batchform.createdDate = now

   tAdded = true.
   .
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE currentPage  AS INTEGER NO-UNDO.

  ASSIGN currentPage = getCurrentPage().

  CASE currentPage: 

    WHEN 0 THEN DO:
       RUN constructObject (
             INPUT  'toolbar.w':U ,
             INPUT  FRAME fMain:HANDLE ,
             INPUT  'LogicalObjectNamePhysicalObjectNameDynamicObjectnoRunAttributeHideOnInitnoDisableOnInitnoObjectLayout':U ,
             OUTPUT h_toolbar ).
       RUN repositionObject IN h_toolbar ( 1.24 , 26.60 ) NO-ERROR.
       /* Size in AB:  ( 2.00 , 109.40 ) */

       /* Adjust the tab order of the smart objects. */
       RUN adjustTabOrder ( h_toolbar ,
             bAdd:HANDLE IN FRAME fMain , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFileNumber tResidential tZipCode tFormType tRateCode tStatCode tForm 
          tSearchType tCounty tSearchTitle tSearchErrors tSearchFile 
          tSearchPolicy tTotalCounts tTotalGrossPremium tTotalNetPremium 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE bDeleteBrowse tFileNumber bSearch tResidential tZipCode 
         tFilePoliciesCnt tSearchErrors tSearchFile tSearchPolicy tBatchID 
         brwForms tFileGrossPremium tFileNetPremium tFileRetention tTotalCounts 
         tTotalGrossPremium tTotalNetPremium tAgentID tAgentName 
         tFileEndorsementsCnt RECT-3 RECT-5 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData wWin 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearAddForm wWin 
FUNCTION clearAddForm RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
        assign
/*           tFileNumber:screen-value in frame fMain = "" */
          tPolicy:screen-value in frame fMain = ""
          
          tForm:screen-value in frame fMain = ""
          tFormCount:screen-value in frame fMain = "1"
     
          tStatCode:screen-value in frame fMain = ""
     
          tEffDate:screen-value in frame fMain = ""
          tLiability:screen-value in frame fMain = ""
          tCounty:screen-value in frame fMain = ""
     
/*           tResidential:checked in frame fMain = true  */
     
          tGrossPremium:screen-value in frame fMain = ""
          tNetPremium:screen-value in frame fMain = ""
          tRetention:screen-value in frame fMain = ""
           
/*           tFileNumber:bgcolor in frame fMain = ? */
          tPolicy:bgcolor in frame fMain = ?
          tForm:bgcolor in frame fMain = ?
          tEffDate:bgcolor in frame fMain = ?
          tLiability:bgcolor in frame fMain = ?
          tGrossPremium:bgcolor in frame fMain = ?
          .

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearFileSummary wWin 
FUNCTION clearFileSummary RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 assign
   tFilePoliciesCnt:screen-value in frame fMain = ""
   tFileEndorsementsCnt:screen-value in frame fMain = ""
   tFileRetention:screen-value in frame fMain = ""
   tFileGrossPremium:screen-value in frame fMain = ""
   tFileNetPremium:screen-value in frame fMain = ""
   tFilePoliciesCnt = 0
   tFileEndorsementsCnt = 0
   tFileRetention = 0
   tFileGrossPremium = 0
   tFileNetPremium = 0
   .

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION deleteValidationReport wWin 
FUNCTION deleteValidationReport RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 publish "DeleteTempFile" (string(pBatchID) + "validation", output std-lo).
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayActiveFile wWin 
FUNCTION displayActiveFile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  doSearch(tFileNumber,
           "",
           "A",
           "").
  displayFileSummary(tFileNumber).
  
  if pZipCode <> "" 
   then
    tZipCode:screen-value in frame {&frame-name} = pZipCode.
    
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayFileSummary wWin 
FUNCTION displayFileSummary RETURNS LOGICAL PRIVATE
  ( pFile as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def buffer x-batchform for batchform.

 assign
   tFileGrossPremium = 0
   tFileNetPremium = 0
   tFileRetention = 0
   tFilePoliciesCnt = 0
   tFileEndorsementsCnt = 0
   .
 for each x-batchform
   where x-batchform.fileNumber = pFile:
  assign
    tFileGrossPremium = tFileGrossPremium + x-batchform.grossPremium
    tFileNetPremium = tFileNetPremium + x-batchform.netPremium
    tFileRetention = tFileRetention + x-batchForm.retentionPremium
    .
  if x-batchform.formType = "P" 
   then tFilePoliciesCnt = tFilePoliciesCnt + 1.
   else 
  if x-batchform.formType = "E" 
   then tFileEndorsementsCnt = tFileEndorsementsCnt + 1.
 end.

 display
   tFileGrossPremium
   tFileNetPremium
   tFileRetention
   tFilePoliciesCnt
   tFileEndorsementsCnt
  with frame fMain.

 RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayForms wWin 
FUNCTION displayForms RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
close query brwForms.
case tSortBy:
 when 1 then open query brwForms for each batchform by batchform.fileNumber by batchform.seq.
 when 2 then open query brwForms for each batchform by batchform.policyID by batchform.seq.
 otherwise open query brwForms for each batchform by batchform.seq descending.
end case.
 
if available batchform
 then run EnableActions in h_toolbar ("modify,print,validate").
 else run DisableActions in h_toolbar ("modify,print,validate,viewreport").

assign
  menu-item m_PopModify:sensitive in menu formpopmenu = available batchform
  menu-item m_PopDelete:sensitive in menu formpopmenu = available batchform
/*   menu-item m_PopSearch:sensitive in menu formpopmenu = available batchform */
  .

return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals wWin 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def var tTotalFiles as int no-undo.
 def var tTotalPolicies as int no-undo.
 def var tTotalEndorsements as int no-undo.
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
  
 def buffer batch for batch.
 def buffer batchaction for batchaction.

 assign
   tTotalFiles = 0
   tTotalPolicies = 0
   tTotalEndorsements = 0
   tTotalGrossPremium = 0
   tTotalNetPremium = 0
   .

 if pBatchID = 0 or pBatchID = ? 
  then return false.

 run server/getbatches.p (0, /* periodID, 0=Any */
                          pBatchID,
                          output table batch,
                          output table batchaction,
                          output tSuccess,
                          output tMsg).
 
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.
   end.

 find batch
   where batch.batchID = pBatchID no-error.
 
 if avail batch then
 assign
   tTotalFiles = batch.fileCount
   tTotalPolicies = batch.policyCount
   tTotalEndorsements = batch.endorsementCount
   tTotalGrossPremium = batch.grossPremiumProcessed
   tTotalNetPremium = batch.netPremiumProcessed
   .

 tTotalCounts = "Files: " + string(tTotalFiles) + " / " 
              + "Policies: " + string(tTotalPolicies) + " / "
              + "Endorsements: " + string(tTotalEndorsements).
 display
   tTotalCounts
   tTotalGrossPremium
   tTotalNetPremium
  with frame fMain.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION doSearch wWin 
FUNCTION doSearch RETURNS LOGICAL PRIVATE
  ( pSearchFile as char,
    pSearchPolicy as char,
    pSearchErrors as char,
    pSearchCondition as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var hQuery as handle.
 def var tWhere as char.
 def var tAnd as logical init false no-undo.

 getData(input pSearchFile,
         input pSearchPolicy,
         input pSearchErrors,
         input pSearchCondition).

 hQuery = query brwForms:handle.
 hQuery:query-close().
 browse brwForms:clear-sort-arrows().

 tWhere = 'preselect each batchform by batchform.fileNumber by batchform.seq '.

 hQuery:query-prepare(tWhere).
 hQuery:query-open().
 
 if hQuery:num-results > 0 then
 tResidential:sensitive in frame {&frame-name} = false.
 
 
return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getData wWin 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( pSearchFile as char,
    pSearchPolicy as char,
    pSearchErrors as char,
    pSearchCondition as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwForms.
 empty temp-table batchform.
 publish "SearchBatchForms" (input pBatchID,
                             input pSearchFile,
                             input pSearchPolicy,
                             input pSearchErrors,
                             input pSearchCondition, 
                             output pZipCode,
                             output table batchform).
                  
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION resizeWindow wWin 
FUNCTION resizeWindow RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 /* If getting bigger, make the frame bigger */
 if {&window-name}:width-pixels > frame fMain:width-pixels 
  then assign
         frame fMain:width-pixels = {&window-name}:width-pixels
         frame fMain:virtual-width-pixels = {&window-name}:width-pixels
         .
 
 if {&window-name}:height-pixels > frame fMain:height-pixels 
  then assign
         frame fMain:height-pixels = {&window-name}:height-pixels
         frame fMain:virtual-height-pixels = {&window-name}:height-pixels
         .

 assign
   /* Bottom line */
   tTotalCounts:side-label-handle:Y in frame fMain = {&window-name}:height-pixels - 22
   tTotalCounts:Y in frame fMain = {&window-name}:height-pixels - 22
   tTotalGrossPremium:side-label-handle:Y in frame fMain = {&window-name}:height-pixels - 22
   tTotalGrossPremium:Y in frame fMain = {&window-name}:height-pixels - 22
   tTotalNetPremium:Y in frame fMain = {&window-name}:height-pixels - 22

   /* Search box */
   tSearchFile:side-label-handle:Y in frame fMain = {&window-name}:height-pixels - 58
   tSearchFile:Y in frame fMain = {&window-name}:height-pixels - 58
   
   tSearchPolicy:side-label-handle:Y in frame fMain = {&window-name}:height-pixels - 58
   tSearchPolicy:Y in frame fMain = {&window-name}:height-pixels - 58
  
   tSearchType:Y in frame fMain = {&window-name}:height-pixels - 58
   tSearchErrors:Y in frame fMain = {&window-name}:height-pixels - 58
   
   bSearch:Y in frame fMain = {&window-name}:height-pixels - 60
   bSearchClear:Y in frame fMain = {&window-name}:height-pixels - 60
   
   tSearchTitle:Y in frame fMain = {&window-name}:height-pixels - 76
   SearchRect:Y in frame fMain = {&window-name}:height-pixels - 69

   /* Forms browse */
   brwForms:width-pixels = {&window-name}:width-pixels - 10
   brwForms:height-pixels = {&window-name}:height-pixels - 370
   .

 if {&window-name}:width-pixels < frame fMain:width-pixels 
  then assign
         frame fMain:width-pixels = {&window-name}:width-pixels
         frame fMain:virtual-width-pixels = {&window-name}:width-pixels
         .
 
 if {&window-name}:height-pixels < frame fMain:height-pixels 
  then assign
         frame fMain:height-pixels = {&window-name}:height-pixels
         frame fMain:virtual-height-pixels = {&window-name}:height-pixels
         .

 /* A reduction change in frame size causes "clear" behavior */
 display
   tBatchID
   tAgentID
   tAgentName
   tFileNumber
   tResidential
   tSearchTitle
   
   tFileGrossPremium
   tFileNetPremium
   tFileRetention
   tFilePoliciesCnt
   tFileEndorsementsCnt

   tSearchFile 
   tSearchPolicy
   tSearchErrors

   tTotalCounts
   tTotalGrossPremium
   tTotalNetPremium
  with frame fMain.
 pause 0.

 RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveCommitment wWin 
FUNCTION saveCommitment RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var xFileNumber as char no-undo.
def var xEffDate as date no-undo.
def var xFormID as char no-undo.
def var xStatCode as char no-undo.
def var xCountyID as char no-undo.
def var tRevenueType as character no-undo.
def var tRedo as logical no-undo.

 if tFileNumber:screen-value in frame fMain = ""
   then tMsg = "File Number cannot be blank.".

 if tForm:screen-value in frame fMain = "" 
   or tForm:screen-value in frame fMain = ?
  then tMsg = "  " + "Form Number cannot be blank or invalid.".

 if tMsg > "" 
  then 
   do: if pShowMsg
        then MESSAGE trim(tMsg)
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 assign
   xFileNumber = tFileNumber:screen-value in frame fMain
   xEffDate = tEffDate:input-value in frame fMain
   xStatCode = tStatCode:screen-value in frame fMain
   xFormID = tForm:screen-value in frame fMain
   xCountyID = tCounty:screen-value in frame fMain
   .

 /* das:saveCommitment:input-output changeable fields */
 publish "NewBatchForm" (pBatchID,
                         "T",
                         input-output xFileNumber,
                         tPolicy:input-value in frame fMain,
                         tRateCode:input-value in frame fMain,
                         input-output xFormID,
                         input tFormCount:input-value in frame fMain,
                         input-output xStatCode,
                         input-output xEffDate,
                         0.0,
                         input-output xCountyID,
                         tResidential:checked in frame fMain,
                         tGrossPremium:input-value in frame fMain,
                         tNetPremium:input-value in frame fMain,
                         tZipCode:input-value in frame fMain,
                         output tRevenueType,
                         output tLastSeq,
                         output std-lo,
                         output std-ch,
                         output tRedo).

 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "T"
   x-batchform.fileNumber = xFileNumber
   x-batchform.policyID = 0
   x-batchform.rateCode = tRateCode:input-value in frame fMain
   x-batchform.formID = xFormID
   x-batchform.formCount = tFormCount:input-value in frame fMain
   x-batchform.statCode = xStatCode
   x-batchform.effDate = ?
   x-batchform.liabilityAmount = 0.0
   x-batchform.countyID = xCountyID
   x-batchform.residential = tResidential:checked in frame fMain
   x-batchform.grossPremium = tGrossPremium:input-value in frame fMain
   x-batchform.netPremium = tNetPremium:input-value in frame fMain
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.reprocess = tRedo
   x-batchform.revenueType = tRevenueType
   x-batchform.createdDate = now

/*    tLastPolicy = tPolicy:input-value in frame fMain   */
/*    tLastEffDate = tEffDate:input-value in frame fMain */
   tLastCounty = tCounty:input-value in frame fMain
   .

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveCPL wWin 
FUNCTION saveCPL RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var xFileNumber as char no-undo.
def var xEffDate as date no-undo.
def var xFormID as char no-undo.
def var xStatCode as char no-undo.
def var xCountyID as char no-undo.
def var tRevenueType as char no-undo.
def var tRedo as logical no-undo.

 if tFileNumber:screen-value in frame fMain = ""
   then tMsg = "File Number cannot be blank.".

 if tForm:screen-value in frame fMain = "" 
   or tForm:screen-value in frame fMain = ?
  then tMsg = "  " + "Form Number cannot be blank or invalid.".

 if tMsg > "" 
  then 
   do: if pShowMsg
        then MESSAGE trim(tMsg)
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 assign
   xFileNumber = tFileNumber:screen-value in frame fMain
   xEffDate = tEffDate:input-value in frame fMain
   xStatCode = tStatCode:screen-value in frame fMain
   xFormID = tForm:screen-value in frame fMain
   xCountyID = tCounty:screen-value in frame fMain
   .

 /* das:saveCPL:input-output changeable fields */
 publish "NewBatchForm" (pBatchID,
                         "C",
                         input-output xFileNumber,
                         tPolicy:input-value in frame fMain,
                         tRateCode:input-value in frame fMain,
                         input-output xFormID,
                         input tFormCount:input-value in frame fMain,
                         input-output xStatCode,
                         input-output xEffDate,
                         0.0,
                         input-output xCountyID,
                         tResidential:checked in frame fMain,
                         tGrossPremium:input-value in frame fMain,
                         tNetPremium:input-value in frame fMain,
                         tZipCode:input-value in frame fMain,
                         output tRevenueType,
                         output tLastSeq,
                         output std-lo,
                         output std-ch,
                         output tRedo).

 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "C"
   x-batchform.fileNumber = xFileNumber
   x-batchform.policyID = 0
   x-batchform.rateCode = tRateCode:input-value in frame fMain
   x-batchform.formID = xFormID
   x-batchform.formCount = tFormCount:input-value in frame fMain
   x-batchform.statCode = xStatCode
   x-batchform.effDate = ?
   x-batchform.liabilityAmount = 0.0
   x-batchform.countyID = xCountyID
   x-batchform.residential = tResidential:checked in frame fMain
   x-batchform.grossPremium = tGrossPremium:input-value in frame fMain
   x-batchform.netPremium = tNetPremium:input-value in frame fMain
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.reprocess = tRedo
   x-batchform.revenueType = tRevenueType
   x-batchform.createdDate = now

/*    tLastPolicy = tPolicy:input-value in frame fMain   */
/*    tLastEffDate = tEffDate:input-value in frame fMain */
   tLastCounty = tCounty:input-value in frame fMain
   .

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveEndorsement wWin 
FUNCTION saveEndorsement RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var xFileNumber as char no-undo.
def var xEffDate as datetime no-undo.
def var xFormID as char no-undo.
def var xStatCode as char no-undo.
def var xCountyID as char no-undo.
def var tRevenueType as char no-undo.
def var tRedo as logical no-undo.
def var tValid as char no-undo.
def var tValidMsg as char no-undo.

 publish "ValidateBatchForm" (pBatchID,
                              pAgentID,
                              tFileNumber:screen-value in frame fMain,
                              tResidential:checked in frame fMain,
                              "E",
                              tPolicy:input-value in frame fMain,
                              tRateCode:input-value in frame fMain,
                              tStatCode:screen-value in frame fMain,
                              tForm:screen-value in frame fMain,
                              tEffDate:input-value in frame fMain,
                              tLiability:input-value in frame fMain,
                              tCounty:screen-value in frame fMain,
                              tGrossPremium:input-value in frame fMain,
                              tNetPremium:input-value in frame fMain,
                              output tValid,
                              output tValidMsg).

 /* If don't show messages, go ahead and try to add to the batch even when errors */
 if tValid <> "G" and tValidMsg > "" and pShowMsg
  then
   do: tMsg = translateErrorCodes(tValidMsg).
       std-lo = false.
       if tValid = "E" 
        then MESSAGE tMsg skip(2)
                     "Continue?"
              VIEW-AS ALERT-BOX error BUTTONS Yes-No update std-lo.
        else MESSAGE tMsg skip(2)
                     "Continue?"
              VIEW-AS ALERT-BOX warning BUTTONS Yes-No update std-lo.
       if not std-lo 
        then return false.
   end.

 /* das:saveEndorsement:input-output changeable fields */
 
 assign
  xFileNumber = tFileNumber:screen-value in frame fMain
  xEffDate = tEffDate:input-value in frame fMain
  xFormID = tForm:screen-value in frame fMain
  xStatCode = tStatCode:screen-value in frame fMain
  xCountyID = tCounty:screen-value in frame fMain
  .
 
 publish "NewBatchForm" (pBatchID,
                        "E",
                        input-output xFileNumber, /* FileNumber */
                        tPolicy:input-value in frame fMain,
                        tRateCode:input-value in frame fMain,
                        input-output xFormID,
                        input tFormCount:input-value in frame fMain,
                        input-output xStatCode,
                        input-output xEffDate,
                        0.0, /* Liability */
                        input-output xCountyID,  /* County */
                        tResidential:checked in frame fMain,
                        tGrossPremium:input-value in frame fMain,
                        tNetPremium:input-value in frame fMain,
                        tZipCode:input-value in frame fMain,
                        output tRevenueType,
                        output tLastSeq,
                        output std-lo,
                        output std-ch,
                        output tRedo).

 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "E"
   x-batchform.fileNumber = xFileNumber /* Set to policy FileNumber */
   x-batchform.policyID = tPolicy:input-value in frame fMain
   x-batchform.rateCode = tRateCode:input-value in frame fMain
   x-batchform.formID = xFormID
   x-batchform.formCount = tFormCount:input-value in frame fMain
   x-batchform.statCode = xStatCode
   x-batchform.effDate = xEffDate /* May be set to policy effDate */
   x-batchform.liabilityAmount = 0.0
   x-batchform.countyID = xCountyID
   x-batchform.residential = tResidential:checked in frame fMain
   x-batchform.grossPremium = tGrossPremium:input-value in frame fMain
   x-batchform.netPremium = tNetPremium:input-value in frame fMain
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.revenueType = tRevenueType
   x-batchform.reprocess = tRedo
   x-batchform.createdDate = now
   
   tLastPolicy = tPolicy:input-value in frame fMain
   tLastEffDate = tEffDate:input-value in frame fMain
   tLastCounty = tCounty:input-value in frame fMain
   .

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveForm wWin 
FUNCTION saveForm RETURNS CHARACTER PRIVATE
  ( pFileNumber as char,
    pResidential as logical,
    pFormType as char,
    pPolicy as int,
    pRateCode as char,
    pSTAT as char,
    pForm as char,
    pEffDate as datetime,
    pLiability as decimal,
    pCounty as char,
    pGross as decimal,
    pNet as decimal,
    pFormCount as int,
    pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var tRedo as logical no-undo.
def var tValid as char no-undo.
def var tValidMsg as char no-undo.
def var tRevenueType as char no-undo.

 publish "ValidateBatchForm" (pBatchID,
                              pAgentID,
                              pFileNumber,
                              pResidential,
                              pFormType,
                              pPolicy,
                              pRateCode,
                              pSTAT,
                              pForm,
                              pEffDate,
                              pLiability,
                              pCounty,
                              pGross,
                              pNet,
                              output tValid,
                              output tValidMsg).

 /* If don't show messages, go ahead and try to add to the batch even when errors */
 if tValid <> "G" and tValidMsg > "" and pShowMsg
  then
   do: tMsg = translateErrorCodes(tValidMsg).
       if tValid = "E"
        then MESSAGE tMsg VIEW-AS ALERT-BOX error.
        else MESSAGE tMsg VIEW-AS ALERT-BOX warning.
   end.

 /* das:saveForm:input-output changeable fields */
 publish "NewBatchForm" (pBatchID,
                         pFormType,
                         input-output pFileNumber,
                         pPolicy,
                         pRateCode,
                         input-output pForm,
                         input pFormCount,
                         input-output pStat,
                         input-output pEffDate,
                         pLiability,
                         input-output pCounty,
                         pResidential,
                         pGross,
                         pNet,
                         tZipCode:input-value in frame fMain,
                         output tRevenueType,
                         output tLastSeq,
                         output std-lo,
                         output std-ch,
                         output tRedo).

 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch VIEW-AS ALERT-BOX error BUTTONS OK.
       
       /* show the proper message including any validation warnings/errors */
       if tValid <> "G" and tValidMsg > ""
        then std-ch = replace(std-ch, chr(10), ";").
        
       return std-ch.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID
   x-batchform.seq = tLastSeq
   x-batchform.formType = pFormType
   x-batchform.fileNumber = pFileNumber
   x-batchform.policyID = pPolicy
   x-batchform.rateCode = pRateCode
   x-batchform.formID = pForm
   x-batchform.formCount = pFormCount
   x-batchform.statCode = pStat
   x-batchform.effDate = pEffDate
   x-batchform.liabilityAmount = pLiability
   x-batchform.countyID = pCounty
   x-batchform.residential = pResidential
   x-batchform.grossPremium = pGross
   x-batchform.netPremium = pNet
   x-batchform.retentionPremium = (pGross - pNet)
   x-batchform.reprocess = tRedo
   x-batchform.revenueType = tRevenueType
   x-batchform.createdDate = now
   .
   
   if pFormType <> "C" or pFormType <> "T"
    then
     assign
       tLastPolicy = pPolicy
       tLastEffDate = pEffDate
       .
   
   tLastCounty = pCounty.

 return "".
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveMisc wWin 
FUNCTION saveMisc RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var xFileNumber as char init "" no-undo.
def var xEffDate as datetime no-undo.
def var xFormID as char no-undo.
def var xStatCode as char no-undo.
def var xCountyID as char no-undo.
def var tRevenueType as char no-undo.
def var tRedo as logical no-undo.

 if tFileNumber:screen-value in frame fMain = ""
   then tMsg = "File Number cannot be blank.".

 if tForm:screen-value in frame fMain = "" 
   or tForm:screen-value in frame fMain = ?
  then tMsg = "  " + "Form Number cannot be blank or invalid.".

 if tEffDate:input-value in frame fMain > today 
  then tMsg = "  " + "Effective Date cannot be in the future.".

 if tMsg > "" 
  then 
   do: if pShowMsg
        then MESSAGE trim(tMsg)
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 /* das:saveForm:input-output changeable fields */
 assign
  xFileNumber = tFileNumber:screen-value in frame fMain
  xEffDate = tEffDate:input-value in frame fMain
  xStatCode = tStatCode:screen-value in frame fMain
  xFormID = tForm:screen-value in frame fMain
  xCountyID = tCounty:screen-value in frame fMain
  .
 publish "NewBatchForm" (pBatchID,
                         "M",
                         input-output xFileNumber,
                         tPolicy:input-value in frame fMain,
                         tRateCode:input-value in frame fMain,
                         input-output xFormID,
                         input tFormCount:input-value in frame fMain,
                         input-output xStatCode,
                         input-output xEffDate,
                         0.0,
                         input-output xCountyID,
                         tResidential:checked in frame fMain,
                         tGrossPremium:input-value in frame fMain,
                         tNetPremium:input-value in frame fMain,
                         tZipCode:input-value in frame fMain,
                         output tRevenueType,
                         output tLastSeq,
                         output std-lo,
                         output std-ch,
                         output tRedo).

 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "M"
   x-batchform.fileNumber = xFileNumber
   x-batchform.policyID = tPolicy:input-value in frame fMain
   x-batchform.rateCode = tRateCode:input-value in frame fMain
   x-batchform.formID = xFormID
   x-batchform.formCount = tFormCount:input-value in frame fMain
   x-batchform.statCode = xStatCode
   x-batchform.effDate = xEffDate
   x-batchform.liabilityAmount = 0.0
   x-batchform.countyID = xCountyID
   x-batchform.residential = tResidential:checked in frame fMain
   x-batchform.grossPremium = tGrossPremium:input-value in frame fMain
   x-batchform.netPremium = tNetPremium:input-value in frame fMain
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.reprocess = tRedo
   x-batchform.revenueType = tRevenueType
   x-batchform.createdDate = now
   
   tLastPolicy = tPolicy:input-value in frame fMain
   tLastEffDate = tEffDate:input-value in frame fMain
   tLastCounty = tCounty:input-value in frame fMain
   .
 
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION savePolicy wWin 
FUNCTION savePolicy RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var xFileNumber as char no-undo.
def var xEffDate as datetime no-undo.
def var xFormID as char no-undo.
def var xStatCode as char no-undo.
def var xCountyID as char no-undo.
def var tRevenueType as character no-undo.
def var tRedo as logical no-undo.
def var tValid as char no-undo.
def var tValidMsg as char no-undo.

  if xZipCode and tZipCode:screen-value in frame fMain = ""
   then
    do:
      MESSAGE "Please enter a valid zip code" VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      return false.
    end.

 publish "ValidateBatchForm" (pBatchID,
                              pAgentID,
                              tFileNumber:screen-value in frame fMain,
                              tResidential:checked in frame fMain,
                              "P",
                              tPolicy:input-value in frame fMain,
                              tRateCode:input-value in frame fMain,
                              tStatCode:screen-value in frame fMain,
                              tForm:screen-value in frame fMain,
                              tEffDate:input-value in frame fMain,
                              tLiability:input-value in frame fMain,
                              tCounty:screen-value in frame fMain,
                              tGrossPremium:input-value in frame fMain,
                              tNetPremium:input-value in frame fMain,
                              output tValid,
                              output tValidMsg).

 /* If don't show messages, go ahead and try to add to the batch even when errors */
 if tValid <> "G" and tValidMsg > "" and pShowMsg
  then
   do: tMsg = translateErrorCodes(tValidMsg).
       std-lo = false.
       if tValid = "E" 
        then MESSAGE tMsg skip(2)
                     "Continue?"
              VIEW-AS ALERT-BOX error BUTTONS Yes-No update std-lo.
        else MESSAGE tMsg skip(2)
                     "Continue?"
              VIEW-AS ALERT-BOX warning BUTTONS Yes-No update std-lo.
       if not std-lo 
        then return false.
   end.

 /* das:savePolicy:input-output changeable fields */
 assign
  xFileNumber = tFileNumber:screen-value in frame fMain
  xEffDate = tEffDate:input-value in frame fMain
  xFormID = tForm:screen-value in frame fMain
  xStatCode = tStatCode:screen-value in frame fMain
  xCountyID = tCounty:screen-value in frame fMain
  .
 
 publish "NewBatchForm" (pBatchID,
                         "P",
                         input-output xFileNumber,
                         tPolicy:input-value in frame fMain,
                         tRateCode:input-value in frame fMain,
                         input-output xFormID,
                         input tFormCount:input-value in frame fMain,
                         input-output xStatCode,
                         input-output xEffDate,
                         tLiability:input-value in frame fMain,
                         input-output xCountyID,
                         tResidential:checked in frame fMain,
                         tGrossPremium:input-value in frame fMain,
                         tNetPremium:input-value in frame fMain,
                         tZipCode:input-value in frame fMain,
                         output tRevenueType,
                         output tLastSeq,
                         output std-lo,
                         output std-ch,
                         output tRedo).


 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "P"
   x-batchform.fileNumber = xFileNumber
   x-batchform.policyID = tPolicy:input-value in frame fMain
   x-batchform.rateCode = tRateCode:input-value in frame fMain
   x-batchform.formID = xFormID
   x-batchform.formCount = tFormCount:input-value in frame fMain
   x-batchform.statCode = xStatCode
   x-batchform.effDate = xEffDate
   x-batchform.liabilityAmount = tLiability:input-value in frame fMain
   x-batchform.countyID = xCountyID
   x-batchform.residential = tResidential:checked in frame fMain
   x-batchform.grossPremium = tGrossPremium:input-value in frame fMain
   x-batchform.netPremium = tNetPremium:input-value in frame fMain
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.revenueType = tRevenueType
   x-batchform.reprocess = tRedo
   x-batchform.createdDate = now
   
   tLastPolicy = tPolicy:input-value in frame fMain
   tLastEffDate = tEffDate:input-value in frame fMain
   tLastCounty = tCounty:input-value in frame fMain
   .

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveSearch wWin 
FUNCTION saveSearch RETURNS LOGICAL PRIVATE
  ( pShowMsg as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
def buffer x-batchform for batchform.
def var tMsg as char no-undo.
def var xFileNumber as char no-undo.
def var xEffDate as datetime no-undo.
def var xFormID as char no-undo.
def var xStatCode as char no-undo.
def var xCountyID as char no-undo.
def var tRevenueType as char no-undo.
def var tRedo as logical no-undo.

 if tFileNumber:screen-value in frame fMain = ""
   then tMsg = "File Number cannot be blank.".

 if tForm:screen-value in frame fMain = "" 
   or tForm:screen-value in frame fMain = ?
  then tMsg = "  " + "Form Number cannot be blank or invalid.".

 if tEffDate:input-value in frame fMain > today 
  then tMsg = "  " + "Effective Date cannot be in the future.".

 if tMsg > "" 
  then 
   do: if pShowMsg
        then MESSAGE trim(tMsg)
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 /* das:saveSearch:input-output changeable fields */
 assign
   xFileNumber = tFileNumber:screen-value in frame fMain
   xEffDate = tEffDate:input-value in frame fMain
   xFormID = tForm:screen-value in frame fMain
   xStatCode = tStatCode:screen-value in frame fMain
   xCountyID = tCounty:screen-value in frame fMain
   .
 publish "NewBatchForm" (pBatchID,
                         "S",
                         input-output xFileNumber,
                         0,
                         tRateCode:input-value in frame fMain,
                         input-output xFormID,
                         input tFormCount:input-value in frame fMain,
                         input-output xStatCode,
                         input-output xEffDate,
                         0.0,
                         input-output xCountyID,
                         tResidential:checked in frame fMain,
                         tGrossPremium:input-value in frame fMain,
                         tNetPremium:input-value in frame fMain,
                         tZipCode:input-value in frame fMain,
                         output tRevenueType,
                         output tLastSeq,
                         output std-lo,
                         output std-ch,
                         output tRedo).

 if not std-lo 
  then
   do: if pShowMsg
        then MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK.
       return false.
   end.

 create x-batchform.
 assign
   x-batchform.batchID = pBatchID /* Global parameter */
   x-batchform.seq = tLastSeq
   x-batchform.formType = "P"
   x-batchform.fileNumber = xFileNumber
   x-batchform.policyID = 0
   x-batchform.rateCode = tRateCode:input-value in frame fMain
   x-batchform.formID = xFormID
   x-batchform.formCount = tFormCount:input-value in frame fMain
   x-batchform.statCode = xStatCode
   x-batchform.effDate = xEffDate
   x-batchform.liabilityAmount = 0.0
   x-batchform.countyID = xCountyID
   x-batchform.residential = tResidential:checked in frame fMain
   x-batchform.grossPremium = tGrossPremium:input-value in frame fMain
   x-batchform.netPremium = tNetPremium:input-value in frame fMain
   x-batchform.retentionPremium = (x-batchform.grossPremium - x-batchform.netPremium)
   x-batchform.revenueType = tRevenueType
   x-batchform.reprocess = tRedo
   x-batchform.createdDate = now
   
   tLastPolicy = 0
   tLastEffDate = ?
   tLastCounty = ""
   .
 
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setAddFormState wWin 
FUNCTION setAddFormState RETURNS LOGICAL PRIVATE
  ( pSensitive as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 assign
   tFormType:sensitive in frame fMain = pSensitive
   tPolicy:sensitive in frame fMain = pSensitive
   tRateCode:sensitive in frame fMain = pSensitive
   tStatCode:sensitive in frame fMain = pSensitive
   tForm:sensitive in frame fMain = pSensitive
   tFormCount:sensitive in frame fMain = pSensitive
   tEffDate:sensitive in frame fMain = pSensitive
   tLiability:sensitive in frame fMain = pSensitive
   tCounty:sensitive in frame fMain = pSensitive
/*    tResidential:sensitive in frame fMain = pSensitive */
   tGrossPremium:sensitive in frame fMain = pSensitive
   tNetPremium:sensitive in frame fMain = pSensitive
   tRetention:sensitive in frame fMain = pSensitive
   bAdd:sensitive in frame fMain = pSensitive
   .

 if pSensitive = yes then
 do:
   if tStatCodesList = "None," 
   then tStatCode:sensitive in frame fMain = no.
   else tStatCode:sensitive in frame fMain = yes.
 end.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setAllForms wWin 
FUNCTION setAllForms RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer stateform for stateform.
 
 std-ch = "".
 for each stateform
   where stateform.stateID = pStateID
     and stateform.formID > ""
   by stateform.formID
   by stateform.description:
   
  
  std-ch = std-ch + "," + stateform.formID
                        + "  :  " + replace(stateform.description, ",", " ")
                        + "," + stateform.formID. 
 end.
 tAllFormsList = trim(std-ch, ",").
 if num-entries(tAllFormsList) <= 1 
  then tAllFormsList = "None,".

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setCounties wWin 
FUNCTION setCounties RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 std-ch = "".
 for each county
   where county.stateID = pStateID
     and county.countyID > ""
   by county.description
   by county.countyID:
   
  std-ch = std-ch + "," + replace(county.description, ",", " ")
                        + (if county.countyID <> county.description
                            then " (" + county.countyID + ")"
                            else "")
                        + "," + county.countyID.
 end.
 
 tCountiesList = trim(std-ch, ",").
 if num-entries(tCountiesList) <= 1 
  then tCountiesList = "None,".
  
 tCounty:list-item-pairs in frame fMain = tCountieslist.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setForms wWin 
FUNCTION setForms RETURNS LOGICAL PRIVATE
  ( input pType as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer stateform for stateform.

 std-ch = "".
 
 for each stateform
   where stateform.stateID = pStateID
     and stateform.formID > ""
     and stateform.formType = pType
   by stateform.formID
   by stateform.description:
  
  std-ch = std-ch + "," + stateform.formID
                        + "  :  " + replace(stateform.description, ",", " ")
                        + "," + stateform.formID. 
                                
 end.
 tFormsList = trim(std-ch, ",").
 if num-entries(tFormsList) <= 1 
  then tFormsList = "None,".
 tForm:list-item-pairs in frame fMain = tFormsList.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setStatCodes wWin 
FUNCTION setStatCodes RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 std-ch = "".
 for each statcode
   where statcode.stateID = pStateID
     and statcode.statcode > ""
   by statcode.statcode
   by statcode.description:
   
  std-ch = std-ch + "," + statcode.statcode
                        + "  :  " + replace(statcode.description, ",", " ")
                        + "," + statcode.statcode.
                                      .
 end.
 tStatCodesList = trim(std-ch, ",").
 if num-entries(tStatCodesList) <= 1 
  then tStatCodesList = "None,".
 tStatCode:list-item-pairs in frame fMain = tStatCodesList.

 RETURN FALSE.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION translateErrorCodes wWin 
FUNCTION translateErrorCodes RETURNS CHARACTER PRIVATE
  ( tValidMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  
  std-ch = "".
  do std-in = 1 to num-entries(tValidMsg):

   find first proerr where proerr.err = entry(std-in, tValidMsg) no-lock no-error.
   if avail proerr then
   std-ch = std-ch + proerr.description.
   else
   std-ch = std-ch + "Undefined issue".
   
   std-ch = std-ch + " (" + entry(std-in, tValidMsg) + ")." + chr(10).
  end.
  std-ch = trim(std-ch).

  RETURN std-ch.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

