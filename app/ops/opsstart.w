&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* opsstart.w
   Startup routine for the remittance OPerationS client
   Author(s)     : D.Sinclair / B.Johnson
   Created       : 05.02.2014

   11.26.2014 D.Sinclair    Restructured call to lib/appstart.i for splash
 */

CREATE WIDGET-POOL.

/* Stores persistent window instances */
define temp-table openWindow
 field entityType    as character
 field entityID      as character
 field childHandle   as handle
 field isBusy        as logical
 field parentHandle  as handle
 .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 58.8 BY 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* SUPPRESS Window definition (used by the UIB) 
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "OPS Startup"
         HEIGHT             = 1
         WIDTH              = 58.8
         MAX-HEIGHT         = 16
         MAX-WIDTH          = 80
         VIRTUAL-HEIGHT     = 16
         VIRTUAL-WIDTH      = 80
         SHOW-IN-TASKBAR    = no
         CONTROL-BOX        = no
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         MESSAGE-AREA       = no
         SENSITIVE          = no.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
                                                                        */
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME
ASSIGN C-Win = CURRENT-WINDOW.




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  NOT-VISIBLE,                                                          */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* OPS Startup */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* OPS Startup */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

subscribe to "openWindow"    anywhere.

/* Event published when any window is closed. */
subscribe to "windowClosed"  anywhere.

{lib/appstart.i 
   &config="opsconfig.p"
   &startup=startup
   &timeout=5
   &splash=20
   &subscribe-esb="Batch,BatchAsync,Period,State,County,Agent,StatCode,StateForm,DefaultEndorsement"
  }

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME DEFAULT-FRAME.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openWindow C-Win 
PROCEDURE openWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
 define input  parameter  pType         as character.
 define input  parameter  pID           as character.
 define input  parameter  pProg         as character.
 define input  parameter  pParam        as character. /* Pass-through when running value(pProg) */
 define input  parameter  pParentHandle as handle.
 
 define variable hCall     as handle.
 define variable icount    as integer.
 define variable datatype  as character.
 define variable inout     as character.
 define variable varname   as character. 
 
 
 def buffer openWindow for openWindow.

 if pType = "" or pProg = "" 
  then return.

 assign
     std-ha = ?
     std-lo = false
     .
   
 for each openWindow
   where openWindow.entityType = pType
     and openWindow.entityID   = pID:
  if valid-handle(openWindow.childHandle) and not std-lo 
   then 
    do: 
      assign
          std-ha        = openWindow.childHandle
          std-lo        = true   /* found one */
          .
    end.
   else delete openWindow.
 end.

 if not std-lo 
  then
   do: 
      create call hCall.
      hCall:call-name  = pProg.
      hCall:persistent = true.
      hCall:call-type  = procedure-call-type.
      
      if num-entries(pParam, "^") > 0 
       then
        do:
          hCall:num-parameters = num-entries(pParam, "^").
          do icount = 1 to num-entries(pParam, "^"):
            datatype = entry(1,entry(icount,pParam,"^"),"|").
            inout    = entry(2,entry(icount,pParam,"^"),"|").
            varname  = entry(3,entry(icount,pParam,"^"),"|").
            hCall:set-parameter(icount, datatype, inout,varname).
          end.
        end.

      hCall:invoke.

     if not valid-handle(hCall) 
      then
       return. 
     
     create openWindow.
     assign
         openWindow.entityType   = pType
         openWindow.entityID     = pID 
         
         /* Store the handle of current window opened. */
         openWindow.childHandle  = hCall:in-handle.
         std-ha = hCall:in-handle.
         /* Store the handle of parent window which is 
            responsible for opening the child window. */
         openWindow.parentHandle = pParentHandle 
         .
   end.
   
  if valid-handle(std-ha) 
   then
    run showWindow in std-ha.
 
 std-ha = ?.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startup C-Win 
PROCEDURE startup :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 std-ch = "".
 /* Data model default from configuration */
 publish "GetDataSource" (output std-ch).
  
 run opsdatasrv.p persistent.
  
 publish "SetSplashStatus".
 run wops.w persistent.      /* Main window */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowClosed C-Win 
PROCEDURE windowClosed :
/*---------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: This procedure closes all the child windows and parent window and 
         deletes there record from the temp-table while closing the parent window.
---------------------------------------------------------------------------------*/  
  define input parameter pParentHandle as handle no-undo.
  
  define buffer openWindow for openWindow.
  define buffer bfOpenWindow for openWindow.

  /* Close all the childs associated with the input parent window's handle. */
  for each openWindow
    where openWindow.parentHandle = pParentHandle:

       std-ha = openWindow.childHandle.
    
       /* Deleting the child window instance from the temp-table before calling 
          closeWindow procedure. This has been done because the closeWindow 
          procedure of the child screen also calls WindowClosed procedure of 
          opstart.w. This leads to circular execution. Since it tries to 
          deletes the same instance, this generates error. */
       delete openWindow.
    
      /* Close child window created in the window*/
      if valid-handle(std-ha) 
       then
        run closeWindow in std-ha.
    
  end.
  
  /* Deleting windows instance when it is closed from 
     the cross button */
  for each openWindow
    where openWindow.childHandle = pParentHandle:
    delete openWindow.
  end.

  std-ha = ?.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

