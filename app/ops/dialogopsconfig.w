&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
define variable cOption as character no-undo.
{lib/std-def.i}

/* Functions ---                                                        */
{lib/dialog-config-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fConfig

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rStartup rOptions tStartPeriod tDoubleClick ~
bSave bCancel 
&Scoped-Define DISPLAYED-OBJECTS tStartPeriod tDoubleClick 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 17 BY 1.19
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 17 BY 1.19
     BGCOLOR 8 .

DEFINE VARIABLE tDoubleClick AS CHARACTER FORMAT "X(256)":U INITIAL "D" 
     LABEL "Double Click" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Batch Details","D",
                     "Modify Batch","M"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE tLastPeriod AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 11.2 BY .62 NO-UNDO.

DEFINE VARIABLE tStartPeriod AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Open in Current Period", "C",
"Open in Last Period Viewed", "U"
     SIZE 30.4 BY 1.81 NO-UNDO.

DEFINE RECTANGLE rOptions
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100 BY 7.62.

DEFINE RECTANGLE rStartup
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 64 BY 12.86.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fConfig
     tStartPeriod AT ROW 9.81 COL 40 NO-LABEL WIDGET-ID 94
     tDoubleClick AT ROW 11.95 COL 29.8 WIDGET-ID 102
     bSave AT ROW 14.81 COL 68 WIDGET-ID 222
     bCancel AT ROW 14.81 COL 86 WIDGET-ID 220
     tLastPeriod AT ROW 10.86 COL 70 COLON-ALIGNED NO-LABEL WIDGET-ID 98 NO-TAB-STOP 
     "Load on Startup" VIEW-AS TEXT
          SIZE 15.6 BY .62 AT ROW 1.24 COL 106 WIDGET-ID 4
     "Options" VIEW-AS TEXT
          SIZE 7.6 BY .62 AT ROW 1.24 COL 4 WIDGET-ID 184
     rStartup AT ROW 1.48 COL 105 WIDGET-ID 2
     rOptions AT ROW 1.48 COL 3 WIDGET-ID 182
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 170.2 BY 15.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Configuration"
         HEIGHT             = 15.48
         WIDTH              = 170.2
         MAX-HEIGHT         = 24.05
         MAX-WIDTH          = 170.2
         VIRTUAL-HEIGHT     = 24.05
         VIRTUAL-WIDTH      = 170.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fConfig
   FRAME-NAME L-To-R,COLUMNS                                            */
/* SETTINGS FOR COMBO-BOX tDoubleClick IN FRAME fConfig
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN tLastPeriod IN FRAME fConfig
   NO-DISPLAY NO-ENABLE                                                 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Configuration */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Configuration */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel C-Win
ON CHOOSE OF bCancel IN FRAME fConfig /* Cancel */
DO:
  apply "WINDOW-CLOSE" to {&WINDOW-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave C-Win
ON CHOOSE OF bSave IN FRAME fConfig /* Save */
DO:
  saveConfig().
  publish "SetStartPeriod" (tStartPeriod:input-value).
  publish "SetDoubleClick" (tDoubleClick:screen-value).
  
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-close.i}
{lib/win-show.i}

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 before-hide.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/dialog-config-add-load.i     &label="Agents"                                                                                                                              &mandatory=true}
  {lib/dialog-config-add-load.i     &label="States"                                                                                                                              &mandatory=true}
  {lib/dialog-config-add-load.i     &label="System Codes"         &loadProc="LoadSysCodes"  &modProc="SysCodeDataChanged"   &setProc="SetLoadSysCodes"  &getProc="GetLoadSysCodes"}
  {lib/dialog-config-add-load.i     &label="System Properties"    &loadProc="LoadSysProps"  &modProc="SysPropDataChanged"   &setProc="SetLoadSysProps"  &getProc="GetLoadSysProps" &mandatory=true}
  {lib/dialog-config-add-load.i     &label="State Forms"}
  {lib/dialog-config-add-load.i     &label="Default Endorsements"}
  {lib/dialog-config-add-load.i     &label="STAT Codes"          &loadProc="LoadStatCodes" &modProc="StatCodeDataChanged"  &setProc="SetLoadStatCodes" &getProc="GetLoadStatCodes"}
  {lib/dialog-config-add-load.i     &label="Counties"                                      &modProc="CountyDataChanged"}
  {lib/dialog-config-add-load.i     &label="Periods"                                                                                                                             &mandatory=true}
  {lib/dialog-config-add-load.i     &label="System Users"        &loadProc="LoadSysUsers"  &modProc="UserDataChanged"      &setProc="SetLoadSysUsers"  &getProc="GetLoadSysUsers" &mandatory=true}
  {lib/dialog-config-add-file.i     &label="Temporary Directory"                                                           &setProc="SetTempDir"       &getProc="GetTempDir"}
  {lib/dialog-config-add-file.i     &label="Report Directory"                                                              &setProc="SetReportDir"     &getProc="GetReportDir"}
  {lib/dialog-config-add-checkbox.i &label="Confirm Close"}
  {lib/dialog-config-add-checkbox.i &label="Confirm File Upload"}
  {lib/dialog-config-add-checkbox.i &label="Confirm Delete"}
  {lib/dialog-config-add-checkbox.i &label="Confirm Exit"}
  {lib/dialog-config-add-checkbox.i &label="Confirm Invoice"}
  {lib/dialog-config-add-checkbox.i &label="Refresh Data Immediately"                                                      &setProc="SetRefresh"       &getProc="GetRefresh"      &tooltip="If checked, the data will be reloaded from the server immediately rather than in the background"}
  {lib/dialog-config-add-checkbox.i &label="Auto-View Data"                                                                &setProc="SetAutoView"      &getProc="GetAutoView"}
  {lib/dialog-config-add-radio.i    &label="Export Type"          &options="XLSX,X,CSV,C"                                  &setProc="SetExportType"    &getProc="GetExportType"}
  setConfig(frame {&frame-name}:handle, rStartup:handle, rOptions:handle).
  
  /* start period */
  std-ch = "".
  publish "GetStartPeriod" (output std-ch).
  if lookup(std-ch, "C,U") = 0 
   then tStartPeriod:screen-value = "C".
   else tStartPeriod:screen-value = std-ch.
   
  /* last period */
  define variable tMonth as integer no-undo.
  define variable tYear  as integer no-undo.
  publish "GetLastPeriod" (output tMonth, output tYear).
  tLastPeriod:screen-value = string(tMonth) + "/" + string(tYear).
  
  /* double click */
  std-ch = "".
  publish "GetDoubleClick" (output std-ch).
  if std-ch = "" or lookup(std-ch, tDoubleClick:list-item-pairs) = 0
   then std-ch = entry(2, tDoubleClick:list-item-pairs).
  tDoubleClick:screen-value = std-ch.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tStartPeriod tDoubleClick 
      WITH FRAME fConfig IN WINDOW C-Win.
  ENABLE rStartup rOptions tStartPeriod tDoubleClick bSave bCancel 
      WITH FRAME fConfig IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fConfig}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

