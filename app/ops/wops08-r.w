&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wops08-r.w
   Window of Period Details report.
*/

CREATE WIDGET-POOL.

{tt/period.i}
{tt/perdet.i}

{lib/std-def.i}

def var hData as handle no-undo.

def var tStatus as char no-undo.
def var tNumTasks as int no-undo.
def var tCurTask as int no-undo.

def var tGettingData as logical.
def var tKeepGettingData as logical.

{lib/winlaunch.i}
{lib/set-button-def.i}
{lib/set-filter-def.i &tableName="perdet"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES perdet

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData perdet.agentID perdet.name perdet.formID perdet.policyID perdet.issueDate perdet.paidDate perdet.fileNumber perdet.postYear perdet.postMonth perdet.batchID perdet.formType perdet.statCode perdet.rateCode perdet.liabilityAmount perdet.reprocess perdet.grossPremium perdet.retentionPremium perdet.netPremium perdet.stateID perdet.countyID perdet.propType perdet.effDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH perdet
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH perdet.
&Scoped-define TABLES-IN-QUERY-brwData perdet
&Scoped-define FIRST-TABLE-IN-QUERY-brwData perdet


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh bExport fState brwData fMonth fYear ~
RECT-36 RECT-37 
&Scoped-Define DISPLAYED-OBJECTS fState fMonth fYear 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV file".

DEFINE BUTTON bRefresh 
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE fMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","None"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE tOpenClosed AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 50 BY 4.29.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 4.29.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      perdet SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      perdet.agentID width 12
perdet.name width 35
perdet.formID width 12
perdet.policyID width 12
perdet.issueDate width 12
perdet.paidDate width 12
perdet.fileNumber width 20
perdet.postYear 
perdet.postMonth
perdet.batchID 
perdet.formType
perdet.statCode
perdet.rateCode
perdet.liabilityAmount
perdet.reprocess
perdet.grossPremium
perdet.retentionPremium
perdet.netPremium
perdet.stateID
perdet.countyID width 20
perdet.propType
perdet.effDate width 12
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 254 BY 23.57 ROW-HEIGHT-CHARS .86 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bRefresh AT ROW 3.62 COL 36 WIDGET-ID 4
     bExport AT ROW 3.62 COL 43 WIDGET-ID 2
     fState AT ROW 2.19 COL 59.2 COLON-ALIGNED WIDGET-ID 42
     brwData AT ROW 5.91 COL 2 WIDGET-ID 200
     fMonth AT ROW 2.19 COL 9 COLON-ALIGNED WIDGET-ID 64
     fYear AT ROW 2.19 COL 26 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     tOpenClosed AT ROW 2.19 COL 38 COLON-ALIGNED NO-LABEL WIDGET-ID 50 NO-TAB-STOP 
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 54.2 WIDGET-ID 60
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-37 AT ROW 1.48 COL 53.2 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 288.6 BY 28.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Period Details"
         HEIGHT             = 28.71
         WIDTH              = 288.6
         MAX-HEIGHT         = 29.52
         MAX-WIDTH          = 319
         VIRTUAL-HEIGHT     = 29.52
         VIRTUAL-WIDTH      = 319
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fState fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR FILL-IN tOpenClosed IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH perdet.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4.24
       COLUMN          = 4
       HEIGHT          = .48
       WIDTH           = 31
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(bExport:HANDLE IN FRAME fMain).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Period Details */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Period Details */
DO:
  if tGettingData 
   then return no-apply.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Period Details */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMonth C-Win
ON VALUE-CHANGED OF fMonth IN FRAME fMain /* Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  dataSortDesc = not dataSortDesc.
  run sortData (dataSortBy).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fYear C-Win
ON VALUE-CHANGED OF fYear IN FRAME fMain
DO:
 clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/set-button.i &label="Refresh" &toggle=false}
{lib/set-button.i &label="Export"}
{lib/set-filter.i &label="State" &id="stateID" &value="stateDesc"}
setButtons().

subscribe to "DataError" anywhere.

publish "GetPeriods" (output table period).
std-ch = "".
for each period
 break by period.periodYear:
 if not first-of(period.periodYear) 
  then next.
 std-ch = std-ch + (if std-ch > "" then "," else "") + string(period.periodYear).
end.
if std-ch = "" 
 then std-ch = string(year(today)).
fYear:list-items in frame fMain = std-ch.

std-ch = "".
publish "GetCurrentValue" ("PeriodYear", output std-ch).
if std-ch = "" 
 then std-ch = string(year(today)).
fYear = int(std-ch).

std-ch = "".
publish "GetCurrentValue" ("PeriodMonth", output std-ch).
if std-ch = "" 
 then std-ch = string(month(today)).
fMonth = int(std-ch).

session:immediate-display = yes.

run windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  clearData().
  enableButtons(false).
  enableFilters(false).
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wops08-r.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wops08-r.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input param pMsg as char.

  /* Since Progress is single-threaded, this works */
  if not tGettingData 
   then return.

  tKeepGettingData = false.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the systems administrator if the problem persists."
    view-as alert-box error.

  clearData().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY fState fMonth fYear 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh bExport fState brwData fMonth fYear RECT-36 RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if query brwData:num-results = 0 
   then
    do: 
     MESSAGE "There is nothing to export" VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.
   
  &scoped-define ReportName "Period_Details"
 
  run util/exporttoexcelbrowse.p (string(browse {&browse-name}:handle), {&ReportName}).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def buffer perdet for perdet.
  
  def var tSuccess as logical init false no-undo.
  def var tMsg as char no-undo.
  
  close query brwData.
  empty temp-table perdet.
  clearData().
  
  tNumTasks = 2.
  tCurTask = 1.
  pbMinStatus().
  
  tGettingData = true.
  tkeepGettingData = true.
  
  hide message no-pause.
  message "Getting data...".
  
  GETTING-DATA:
  do with frame {&frame-name}:
    pbUpdateStatus(tCurTask, 0).
    
    find period
      where period.periodMonth = fMonth:input-value
        and period.periodYear = fYear:input-value no-error.
    if not available period
     then 
      do:
        tGettingData = false.
        clearData().
        return.
      end.
     
    run server/getperdet.p (period.periodID,
                            output table perdet,
                            output tSuccess,
                            output tMsg).
      
     if not tSuccess
      then 
       do: std-lo = false.
           publish "GetAppDebug" (output std-lo).
           if std-lo 
            then message "GetPerDet failed: " tMsg view-as alert-box warning.
       end.
  
    process events.
       
    if not tKeepGettingData 
     then leave GETTING-DATA.
    
    pbUpdateStatus(int((tCurTask / tNumTasks) * 100), 0).
  end. /* GETTING-DATA */
  
  if not tKeepGettingData 
   then
    do:
        close query brwData.
        empty temp-table perdet.
        clearData().
  
        hide message no-pause.
        message "Cancelled (" + string(now,"99/99/99 HH:MM:SS") + ")".
        pbUpdateStatus(100, 1).
        pbMinStatus().
        tGettingData = false.
        return.
    end.
  
  dataSortBy = "".
  run sortData ("agentID").
  setFilterCombos("ALL").
  enableButtons(can-find(first perdet)).
  enableFilters(can-find(first perdet)).
  
  pbUpdateStatus(100, 1).
  pbMinStatus().
  tGettingData = false. 
  hide message no-pause.
  message "Report Complete for Period " + 
          fMonth:screen-value in frame fMain + "/" +
          fYear:screen-value in frame fMain + "  " +
          "(" + string(now,"99/99/99 HH:MM:SS") + ")".
  message.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData.i &pre-by-clause="getWhereClause() +"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    close query brwData.
    hide message no-pause.
    fState:screen-value = "".
    
    find first period where period.periodMonth = fMonth:input-value
                        and period.periodYear = fYear:input-value
                        no-lock no-error.
    tOpenClosed = if avail period 
                  then if period.active then "(Open)" else "(Closed)"
                  else "".
    display tOpenClosed.
  end.
  message "".
                  
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

  {&WINDOW-NAME}:move-to-top().
  
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

