&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialog 
/* dialogmovebatch.w
   request confirmation to move the batch
   2.6.2013 D.Sinclair
   */

def input parameter pBatch as int.
def input parameter pMonth as int.
def input parameter pYear as int.
def output parameter pDirection as char init "".
def output parameter pComments as char.
def output parameter pMove as logical init false.

def var tNextOpen as logical no-undo.
def var tPrevOpen as logical no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tDirection tComments 
&Scoped-Define DISPLAYED-OBJECTS tBatch tDirection tComments tAction1 ~
tAction2 tAction3 tAction4 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bMove AUTO-GO 
     LABEL "Apply Move" 
     SIZE 15 BY 1.14 TOOLTIP "The period must be open for this to be active"
     BGCOLOR 8 .

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 77.6 BY 4 NO-UNDO.

DEFINE VARIABLE tAction1 AS CHARACTER FORMAT "X(256)":U INITIAL "1.  The current batch will be completed with zero value." 
     LABEL "Effects" 
      VIEW-AS TEXT 
     SIZE 68 BY .62 TOOLTIP "The following effects will occur as a result of the move" NO-UNDO.

DEFINE VARIABLE tAction2 AS CHARACTER FORMAT "X(256)":U INITIAL "2.  A new batch will be created in the appropriate period." 
      VIEW-AS TEXT 
     SIZE 68 BY .62 NO-UNDO.

DEFINE VARIABLE tAction3 AS CHARACTER FORMAT "X(256)":U INITIAL "3.  All forms from the current batch will be moved to the new batch." 
      VIEW-AS TEXT 
     SIZE 75.2 BY .62 NO-UNDO.

DEFINE VARIABLE tAction4 AS CHARACTER FORMAT "X(256)":U INITIAL "4.  All attachments for the current batch will be copied to the new batch." 
      VIEW-AS TEXT 
     SIZE 76.2 BY .62 NO-UNDO.

DEFINE VARIABLE tBatch AS INTEGER FORMAT ">>>>>>>9":U INITIAL 0 
     LABEL "Batch" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE tDirection AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Next", "N",
"Previous", "P"
     SIZE 20.8 BY 1.05 TOOLTIP "Select to move the batch forward or backward a period" NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.4 BY 1.33.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialog
     tBatch AT ROW 1.76 COL 14.2 COLON-ALIGNED WIDGET-ID 8
     tDirection AT ROW 3.1 COL 17.4 NO-LABEL WIDGET-ID 16
     tComments AT ROW 4.48 COL 15.8 NO-LABEL WIDGET-ID 2
     bMove AT ROW 3.05 COL 50.2
     tAction1 AT ROW 8.81 COL 13.8 COLON-ALIGNED WIDGET-ID 10
     tAction2 AT ROW 9.48 COL 13.8 COLON-ALIGNED NO-LABEL WIDGET-ID 12
     tAction3 AT ROW 10.14 COL 13.8 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     tAction4 AT ROW 10.81 COL 13.8 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     "Period" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 3.33 COL 39.8 WIDGET-ID 22
     "Comments:" VIEW-AS TEXT
          SIZE 10.8 BY .62 AT ROW 4.52 COL 5 WIDGET-ID 4
     "Move To:" VIEW-AS TEXT
          SIZE 9.6 BY .62 AT ROW 3.33 COL 6.4 WIDGET-ID 20
     RECT-3 AT ROW 2.95 COL 16 WIDGET-ID 26
     SPACE(56.79) SKIP(7.76)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Move Batch" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialog
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fDialog:SCROLLABLE       = FALSE
       FRAME fDialog:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bMove IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-3 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAction1 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAction2 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAction3 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAction4 IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tBatch IN FRAME fDialog
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialog fDialog
ON WINDOW-CLOSE OF FRAME fDialog /* Move Batch */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDirection
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDirection fDialog
ON VALUE-CHANGED OF tDirection IN FRAME fDialog
DO:
  bMove:sensitive in frame fDialog = (self:input-value = "N" and tNextOpen 
                                      or self:input-value = "P" and tPrevOpen).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialog 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

tBatch = pBatch.
publish "IsPeriodOpen" (if pMonth = 12 then 1 else pMonth + 1,
                        if pMonth = 12 then pYear + 1 else pYear,
                        output tNextOpen).
publish "IsPeriodOpen" (if pMonth = 1 then 12 else pMonth - 1,
                        if pMonth = 1 then pYear - 1 else pYear,
                        output tPrevOpen).

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  apply "VALUE-CHANGED" to tDirection in frame fDialog.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  pDirection = tDirection:input-value in frame fDialog.
  pComments = tComments:screen-value in frame fDialog.
  pMove = true.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tBatch tDirection tComments tAction1 tAction2 tAction3 tAction4 
      WITH FRAME fDialog.
  ENABLE tDirection tComments 
      WITH FRAME fDialog.
  VIEW FRAME fDialog.
  {&OPEN-BROWSERS-IN-QUERY-fDialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

