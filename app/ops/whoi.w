&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*  wagentapp.w
 *  @description - Agent Application Window
    @date - 07.07.2017
*/  

CREATE WIDGET-POOL.

{tt/policyhoi.i}
{tt/policyhoi.i &tableAlias="ttpolicyhoi"}
{tt/county.i}
{tt/sysprop.i}
{tt/period.i}
{tt/state.i}

{tt/openwindow.i}
    
{lib/get-column.i}
{lib/std-def.i}

/* def var hDocWindow as handle no-undo.    */
   def var dFilterWidth as decimal no-undo. 
   def var todayMonth as int no-undo.
   def var todayYear as int no-undo.
  

   def var dpolicyidcolumn as decimal no-undo.
   def var dfilenumcolumn as decimal no-undo.

def var hFileDataSrv as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwHOI

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES policyhoi

/* Definitions for BROWSE brwHOI                                        */
&Scoped-define FIELDS-IN-QUERY-brwHOI policyhoi.policyID policyhoi.fileNumber policyhoi.grossPremium policyhoi.liabilityAmount policyhoi.policyDate policyhoi.countyID policyhoi.requestAgentID policyhoi.coopAgentID policyhoi.DIPStatus   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwHOI   
&Scoped-define SELF-NAME brwHOI
&Scoped-define QUERY-STRING-brwHOI FOR EACH policyhoi by policyhoi.policyID
&Scoped-define OPEN-QUERY-brwHOI OPEN QUERY {&SELF-NAME} FOR EACH policyhoi by policyhoi.policyID.
&Scoped-define TABLES-IN-QUERY-brwHOI policyhoi
&Scoped-define FIRST-TABLE-IN-QUERY-brwHOI policyhoi


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwHOI}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh bGo rFilter RECT-38 RECT-50 RECT-51 ~
fState fStat tBeginMonth tBeginYear fCounty bFilter bClear tEndMonth ~
tEndYear fSearch brwHOI 
&Scoped-Define DISPLAYED-OBJECTS fState fStat tBeginMonth tBeginYear ~
fCounty tEndMonth tEndYear fSearch 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ClearData C-Win 
FUNCTION ClearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SetActionBtns C-Win 
FUNCTION SetActionBtns RETURNS character
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ShowOpenWindow C-Win 
FUNCTION ShowOpenWindow RETURNS handle private
  ( input pFile as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ShowOpenWindowForAgentAppStat C-Win 
FUNCTION ShowOpenWindowForAgentAppStat RETURNS handle
  ( input pAgentID as char,
    input pType as char,
    input pWindow as char)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidPeriod C-Win 
FUNCTION ValidPeriod RETURNS logical
  ( input beginPeriod as int,
    input endPeriod as int)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwHOI TITLE "Actions"
       MENU-ITEM m_Popup_New    LABEL "New"           
       MENU-ITEM m_Popup_Delete LABEL "Delete"        
       MENU-ITEM m_Popup_Update LABEL "Update"        .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 8 BY 1.19.

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 8 BY 1.19.

DEFINE BUTTON bGo  NO-FOCUS
     LABEL "GO" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get HOI policy".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE BUTTON bUpdate  NO-FOCUS
     LABEL "Update" 
     SIZE 7.2 BY 1.71 TOOLTIP "Update".

DEFINE VARIABLE fCounty AS CHARACTER 
     LABEL "County" 
     VIEW-AS COMBO-BOX INNER-LINES 25
     LIST-ITEM-PAIRS "ALL","ALL"
     DROP-DOWN AUTO-COMPLETION
     SIZE 41.6 BY 1 NO-UNDO.

DEFINE VARIABLE fStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 26.8 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 23.8 BY 1 NO-UNDO.

DEFINE VARIABLE tBeginMonth AS INTEGER FORMAT ">>>>>9":U INITIAL 0 
     LABEL "Beginning" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "Item 1",0
     DROP-DOWN-LIST
     SIZE 23.8 BY 1 TOOLTIP "Beginning Policy Effective Date" NO-UNDO.

DEFINE VARIABLE tBeginYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 TOOLTIP "Beginning Policy Effective Date" NO-UNDO.

DEFINE VARIABLE tEndMonth AS INTEGER FORMAT ">>>>>9":U INITIAL 0 
     LABEL "Ending" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "Item 1",0
     DROP-DOWN-LIST
     SIZE 23.8 BY 1 TOOLTIP "Ending Policy Effective Date" NO-UNDO.

DEFINE VARIABLE tEndYear AS INTEGER FORMAT "9999":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEMS "0" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 TOOLTIP "Ending Policy Effective Date" NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 41.6 BY 1 TOOLTIP "Search for policy or file number" NO-UNDO.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 43 BY 4.

DEFINE RECTANGLE RECT-50
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE .4 BY 1.67.

DEFINE RECTANGLE RECT-51
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 63.2 BY 4.

DEFINE RECTANGLE rFilter
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71.4 BY 4.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwHOI FOR 
      policyhoi SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwHOI
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwHOI C-Win _FREEFORM
  QUERY brwHOI DISPLAY
      policyhoi.policyID column-label "Policy ID" format "zzzzzzzzz" width 20
 policyhoi.fileNumber column-label "File Number" format "x(30)" width 40
 policyhoi.grossPremium column-label "Gross!Premium" format "(z,zzz,zz9.99)" width 15
 policyhoi.liabilityAmount column-label "Liability" format "(zzz,zzz,zz9.99)" width 15
 policyhoi.policyDate column-label "Policy Date" format "99/99/9999" width 20
 policyhoi.countyID column-label "County!Code" format "x(15)" width 15
 policyhoi.requestAgentID column-label "Requesting Agent!State ID" format "x(20)" width 30
 policyhoi.coopAgentID column-label "Cooperating Agent!State ID" format "x(20)" width 30
 policyhoi.DIPStatusDesc column-label "Status" format "x(40)" width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-TAB-STOP SIZE 217 BY 21.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bDelete AT ROW 2.62 COL 80.8 WIDGET-ID 302 NO-TAB-STOP 
     bExport AT ROW 2.62 COL 98.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 2.62 COL 91.2 WIDGET-ID 4 NO-TAB-STOP 
     bGo AT ROW 2.62 COL 55.8 WIDGET-ID 320 NO-TAB-STOP 
     fState AT ROW 1.95 COL 11.8 COLON-ALIGNED WIDGET-ID 308
     bNew AT ROW 2.62 COL 66.4 WIDGET-ID 10 NO-TAB-STOP 
     fStat AT ROW 1.95 COL 116.4 COLON-ALIGNED WIDGET-ID 62
     tBeginMonth AT ROW 3.05 COL 11.8 COLON-ALIGNED WIDGET-ID 312
     bUpdate AT ROW 2.62 COL 73.6 WIDGET-ID 88 NO-TAB-STOP 
     tBeginYear AT ROW 3.05 COL 36 COLON-ALIGNED NO-LABEL WIDGET-ID 314
     fCounty AT ROW 3.05 COL 116.4 COLON-ALIGNED WIDGET-ID 64
     bFilter AT ROW 4.05 COL 160.6 WIDGET-ID 66
     bClear AT ROW 4.05 COL 169.2 WIDGET-ID 86
     tEndMonth AT ROW 4.14 COL 11.8 COLON-ALIGNED WIDGET-ID 322
     tEndYear AT ROW 4.14 COL 36 COLON-ALIGNED NO-LABEL WIDGET-ID 324
     fSearch AT ROW 4.14 COL 116.4 COLON-ALIGNED WIDGET-ID 84
     brwHOI AT ROW 5.76 COL 1.8 WIDGET-ID 200
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.14 COL 109.2 WIDGET-ID 60
     "Parameters" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.14 COL 3.2 WIDGET-ID 318
     "Actions" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.14 COL 66.4 WIDGET-ID 70
     rFilter AT ROW 1.52 COL 107.6 WIDGET-ID 58
     RECT-38 AT ROW 1.52 COL 64.8 WIDGET-ID 68
     RECT-50 AT ROW 2.67 COL 89.4 WIDGET-ID 304
     RECT-51 AT ROW 1.52 COL 1.8 WIDGET-ID 310
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 239.4 BY 26.71 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Home Office Issued Policies"
         HEIGHT             = 26.57
         WIDTH              = 218.6
         MAX-HEIGHT         = 48.43
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 48.43
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwHOI fSearch fMain */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bExport IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bNew IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwHOI:POPUP-MENU IN FRAME fMain             = MENU POPUP-MENU-brwHOI:HANDLE
       brwHOI:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwHOI:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR BUTTON bUpdate IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwHOI
/* Query rebuild information for BROWSE brwHOI
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH policyhoi by policyhoi.policyID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwHOI */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Home Office Issued Policies */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Home Office Issued Policies */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Home Office Issued Policies */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  fSearch:screen-value = "".
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
    run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run ExportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  run filterData.
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bGo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bGo C-Win
ON CHOOSE OF bGo IN FRAME fMain /* GO */
DO:
   
    apply "choose" to bRefresh.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
   run ActionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwHOI
&Scoped-define SELF-NAME brwHOI
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwHOI C-Win
ON DEFAULT-ACTION OF brwHOI IN FRAME fMain
DO:
    run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwHOI C-Win
ON ROW-DISPLAY OF brwHOI IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwHOI C-Win
ON START-SEARCH OF brwHOI IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwHOI C-Win
ON VALUE-CHANGED OF brwHOI IN FRAME fMain
DO:
  
/*   if not available policyhoi                                */
/*    then                                                     */
/*   do:                                                       */
/*       message "policy hoi NOT AVAIL!!!!" view-as alert-box. */
/*                                                             */
/*   end.                                                      */

  do with frame {&frame-name}:
  
    assign
      bRefresh:sensitive = avail policyhoi
      bExport:sensitive = avail policyhoi
      bNew:sensitive = true
      bUpdate:sensitive =  avail policyhoi
      bDelete:sensitive = avail policyhoi 
    .

end.

  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUpdate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUpdate C-Win
ON CHOOSE OF bUpdate IN FRAME fMain /* Update */
DO:
    run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCounty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCounty C-Win
ON VALUE-CHANGED OF fCounty IN FRAME fMain /* County */
DO:

    assign {&SELF-NAME}.
    
/*     dataSortBy = "".          */
/*     run sortData("countyID"). */
/*     SetActionBtns().          */
 apply "choose" to bFilter.   

/*     run filterData. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON LEAVE OF fSearch IN FRAME fMain /* Search */
DO:
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fStat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fStat C-Win
ON VALUE-CHANGED OF fStat IN FRAME fMain /* Status */
DO:
     assign {&SELF-NAME}. 
/*      dataSortBy = "".          */
/*      run sortData(dataSortBy). */
/*      SetActionBtns().          */
   apply "choose" to bFilter. 
     
/*run filterData.*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  ClearData().
  {lib/get-county-list.i &combo=fCounty &state=self:screen-value &addAll=true}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Delete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Delete C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Delete /* Delete */
DO:
  run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_New C-Win
ON CHOOSE OF MENU-ITEM m_Popup_New /* New */
DO:
  run ActionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Popup_Update
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Popup_Update C-Win
ON CHOOSE OF MENU-ITEM m_Popup_Update /* Update */
DO:
  run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tBeginMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tBeginMonth C-Win
ON VALUE-CHANGED OF tBeginMonth IN FRAME fMain /* Beginning */
DO:
  ClearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tBeginYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tBeginYear C-Win
ON VALUE-CHANGED OF tBeginYear IN FRAME fMain
DO:
  ClearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEndMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEndMonth C-Win
ON VALUE-CHANGED OF tEndMonth IN FRAME fMain /* Ending */
DO:
  ClearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tEndYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tEndYear C-Win
ON VALUE-CHANGED OF tEndYear IN FRAME fMain
DO:
  ClearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "PolicyHOIChanged" anywhere.

/* hBrowse = {&browse-name}:handle. */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/brw-main.i}
{lib/win-main.i}
{&window-name}:window-state = 2.

{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.


SetActionBtns().

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/sync-i.bmp").
bNew:load-image("images/new.bmp").
bNew:load-image-insensitive("images/new-i.bmp").
bUpdate:load-image("images/update.bmp").
bUpdate:load-image-insensitive("images/update-i.bmp").
bDelete:load-image-up("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").
bGo:load-image("images/completed.bmp").
bGo:load-image-insensitive("images/completed-i.bmp").

fStat:tooltip = "Directly Issued Policies".

std-ha = getColumn({&browse-name}:handle,"policyID").
if valid-handle(std-ha)
     then dpolicyidcolumn = std-ha:width-pixels.

std-ha = getcolumn({&browse-name}:handle, "fileNumber").
if valid-handle(std-ha) 
    then dfilenumcolumn = std-ha:width-pixels.

assign
   dFilterWidth = rFilter:width-pixels
   .

{lib/get-state-list.i &combo=fState &addAll=true}
publish "GetCounties" (output table county).

/* message "getagentappstatus " std-ch view-as alert-box information. */

{lib/get-sysprop-list.i &combo=fStat &appCode="'OPS'" &objAction="'DIPStatus'" &objProperty="'DIPStatus'" &addAll=true}

publish "GetPeriods" (output table period).
  

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  do with frame {&frame-name}:
    {lib/get-period-list.i &mth=tBeginMonth &yr=tBeginYear}
    {lib/get-period-list.i &mth=tEndMonth &yr=tEndYear}
    
    {lib/set-current-value.i &yr=tBeginYear}
    {lib/set-current-value.i &yr=tEndYear}
    
    tBeginMonth:screen-value = "1".
    for last period no-lock
       where period.periodYear = tBeginYear:input-value
          by period.periodMonth:
      
      tEndMonth:screen-value = string(period.periodMonth).
    end.
    fState:screen-value = "TX".
    fStat:screen-value = "ALL".
    apply "VALUE-CHANGED" to fState.
  end.
  {&window-name}:window-state = 3.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete C-Win 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if avail policyhoi 
 then
  do:
    std-lo = no.
    message
         "Are you sure you want to delete the HOI policy (" + string(policyhoi.policyID)  + ")?"
         view-as alert-box buttons yes-no update std-lo.
   if not std-lo then return.
/*    message "policy ID to delete:" string(policyhoi.policyID) view-as alert-box. */
   
   publish "DeletePolicyHoi" (input policyhoi.policyID, output std-lo, output std-ch).

   if not std-lo then
       message std-ch view-as alert-box error.

  end.
  else
     message "No Home Issued Policy is available." view-as alert-box.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify C-Win 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if not avail policyhoi 
  then
  do:
    message "No Home Issued Policy is available." view-as alert-box.
    return no-apply.
  end.

  empty temp-table ttpolicyhoi.
  create ttpolicyhoi.
  buffer-copy policyhoi to ttpolicyhoi.

  run dialoghoimodify.w (table ttpolicyhoi, output std-lo).

  if not std-lo
   then return.
                                                    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew C-Win 
PROCEDURE ActionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
run dialoghoinew.w (output std-lo).
  if not std-lo 
   then return.
   else apply "CHOOSE" to bRefresh in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fState fStat tBeginMonth tBeginYear fCounty tEndMonth tEndYear fSearch 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh bGo rFilter RECT-38 RECT-50 RECT-51 fState fStat tBeginMonth 
         tBeginYear fCounty bFilter bClear tEndMonth tEndYear fSearch brwHOI 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExportData C-Win 
PROCEDURE ExportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    def var th as handle no-undo.

    def var tbeginPeriod as int no-undo.
    def var tendPeriod as int no-undo.

/*     &scoped-define ReportName "AgentApplications" */
/*                                                   */

    th = temp-table policyhoi:handle.
    

  do with frame {&frame-name}:
      tbeginPeriod = integer(tBeginYear:screen-value + (if int(tBeginMonth:screen-value) < 10 then string(tBeginMonth:screen-value,"09") else tBeginMonth:screen-value)).
      tendPeriod = integer(tEndYear:screen-value + (if int(tEndMonth:screen-value) < 10 then string(tEndMonth:screen-value,"09") else tEndMonth:screen-value)).
  end.

    if query brwHOI:num-results = 0
   then
    do:
     MESSAGE "There is nothing to export"
      VIEW-AS ALERT-BOX warning BUTTONS OK.
     return.
    end.

   publish "GetReportDir" (output std-ch).

   run util/exporttable.p(table-handle th,
                          "policyhoi",
                          "for each policyhoi ",
                          "fileNumber,grossPremium,liabilityAmount,policyDate,countyID,requestAgentID,coopAgentID,DIPStatus",
                          "Transaction ID,Gross Premium,Limits of Liability,Policy Date,County Code,Requesting Agent ID,Cooperating Agent ID, DIP Status",
                          std-ch,
                          "Directly Issued Policies_" 
/*                           + string(tBeginYear:screen-value in frame {&frame-name},"9999") */
/*                           + string(tBeginMonth:screen-value in frame {&frame-name},"99")  */
                          + string(tbeginPeriod)
                          + "_" 
/*                           + string(tEndYear:screen-value in frame {&frame-name},"9999") */
/*                           + string(tEndMonth:screen-value in frame {&frame-name},"99")  */
                          + string(tendPeriod)
                          + ".csv",
                          true,
                          output std-ch,
                          output std-in).

/*                                      */
/*                                                                                    */
/*   run util/exporttocsvbrowse.p (string(browse brwAgentApp:handle), {&ReportName}). */
/*                                                                                    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("PolicyID").
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def buffer policyhoi for policyhoi.
def var tbeginPeriod as int no-undo.
def var tendPeriod as int no-undo.

/*   close query brwHOI.           */
/*   empty temp-table ttpolicyhoi. */
/*   empty temp-table policyhoi.   */
/*                                 */

ClearData().

do with frame {&frame-name}:
  /* get the beginning period */
  for first period no-lock
      where period.periodMonth = tBeginMonth:input-value
        and period.periodYear = tBeginYear:input-value:
    
    tbeginPeriod = period.periodID.
  end.
  /* get the beginning period */
  for first period no-lock
      where period.periodMonth = tEndMonth:input-value
        and period.periodYear = tEndYear:input-value:
    
    tendPeriod = period.periodID.
  end.
end.

if not ValidPeriod(tbeginPeriod, tendPeriod) then return.
/* message "tbeginperiod:" string(tbeginPeriod) view-as alert-box. */

publish "SearchPolicyHOI" (fState:screen-value,tbeginPeriod, tendPeriod, output table policyhoi).

/* for each ttpolicyhoi where true */
/*            and year(ttpolicyhoi.policyDate) >= integer(tBeginYear:screen-value in frame {&frame-name})   */
/*            and year(ttpolicyhoi.policyDate) <= integer(tEndYear:screen-value in frame {&frame-name})     */
/*            and month(ttpolicyhoi.policyDate) >= integer(tBeginMonth:screen-value in frame {&frame-name}) */
/*            and month(ttpolicyhoi.policyDate) <= integer(tEndMonth:screen-value in frame {&frame-name})   */
/*                                       (if month(ttpolicyhoi.policyDate < 10 then string(month(ttpolicyhoi.policyDate),"09") else string(month(ttpolicyhoi.policyDate)))) <= tendPeriod */
/*                            no-lock: */
/*                           and integer(string(year(ttpolicyhoi.policyDate))  +                                                                                                             */
/*                                       (if month(ttpolicyhoi.policyDate) < 10 then string(month(ttpolicyhoi.policyDate),"09") else string(month(ttpolicyhoi.policyDate)))) >= tbeginPeriod */
                          
                          
        

/*     message "policy date:" string(month(ttpolicyhoi.policyDate)) view-as alert-box. */
/*                                                                                     */
/*    create policyhoi.                                                                */
/*    buffer-copy ttpolicyhoi to policyhoi.                                            */
/*                                                                                     */
/* end.                                                                                */

SetActionBtns().

/* for each policyhoi:                                           */
/*     message "policy ID:" string(policyhoi.policyID)           */
/*       + chr(13) +                                             */
/*         "LIABILITY AMOUNT:" string(policyhoi.liabilityAmount) */
/*     view-as alert-box.                                        */
/* end.                                                          */


DataSortDesc = yes.
run SortData("policyID").

/* apply "VALUE-CHANGED" to browse brwAgentApp. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PolicyHOIChanged C-Win 
PROCEDURE PolicyHOIChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table policyhoi.
  publish "GetPolicyhoi" (output table policyhoi).
  DataSortDesc = yes.
  run SortData("policyID").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SortData C-Win 
PROCEDURE SortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var twhereClause as char no-undo.

  do with frame {&frame-name}:
      twhereClause = "where " + 
          
           (if fStat:screen-value <> "ALL"
            then "policyhoi.DIPStatus = '" + fStat:screen-value + "'"
            else "true") + 
           " and " + 
           (if fCounty:screen-value <> "ALL" and fCounty:screen-value <> "NoState"
            then "policyhoi.countyID = '" + fCounty:screen-value  + "'"
            else "true") +
           " and " +
          (if fSearch:screen-value > ""
            then "(string(policyhoi.policyID) matches '*" + fSearch:screen-value + "*' or "
            else "true and ") +
            
          (if fSearch:screen-value > ""
           then "policyhoi.fileNumber matches '*" + fSearch:screen-value + "*') "
           else "true")
/*                 + "or policyhoi.coopAgentID matches '*" + fSearch:screen-value + "*' "     */
/*                 + "or policyhoi.requestAgentID matched '*" + fSearch:screen-value + "*') " */
/*            else "true")                                                                    */
          .
      {lib/brw-sortData.i &pre-by-clause="twhereClause +"}
  end.

/*   message "where clause:" twhereClause view-as alert-box. */
  apply "VALUE-CHANGED" to brwHOI.

  std-ch = string(num-results("{&browse-name}")) + " record(s) shown".
  status input std-ch in window {&window-name}.
  status default std-ch in window {&window-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define  variable  dDiffWidth as decimal no-undo.
  
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.


  /* get the difference between the old and new width\height */
  dDiffWidth = frame {&frame-name}:width-pixels - {&window-name}:min-width-pixels.

 
  /* {&frame-name} components */
  {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
  {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
  
  std-ha = getColumn({&browse-name}:handle, "policyID").
  if valid-handle(std-ha) 
    then std-ha:width-pixels = dpolicyidcolumn + (dDiffWidth / 2).

  std-ha = getColumn({&browse-name}:handle, "fileNumber").
  if valid-handle(std-ha) 
    then std-ha:width-pixels = dfilenumcolumn + (dDiffWidth / 2).


  do with frame {&frame-name}:
      rFilter:width-pixels = dFilterWidth + dDiffWidth.
  end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ClearData C-Win 
FUNCTION ClearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 status input "" in window {&window-name}.
 status default "" in window {&window-name}.
/*   hide message no-pause. */
  close query brwHOI.
  clear frame fMain.
  empty temp-table policyhoi.
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SetActionBtns C-Win 
FUNCTION SetActionBtns RETURNS character
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
if avail policyhoi then
    message "policyhoi available." view-as alert-box.

do with frame {&frame-name}:

    assign
      bRefresh:sensitive = can-find(first policyhoi) 
      bExport:sensitive = can-find(first policyhoi) 
      bNew:sensitive = true
      bUpdate:sensitive =  can-find(first policyhoi) 
      bDelete:sensitive = can-find(first policyhoi) 
    .

end.




  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ShowOpenWindow C-Win 
FUNCTION ShowOpenWindow RETURNS handle private
  ( input pFile as character ) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo. 
  define buffer openWindow for openWindow.

  for each openWindow:
    if not valid-handle(openWindow.procHandle) 
     then delete openWindow.
  end.
  
  hWindow = ?.
  for first openwindow no-lock
      where openwindow.procFile = pFile:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow)
   then
    do:
      run value(pFile) persistent set hWindow.
      create openwindow.
      assign
        openwindow.procFile = pFile
        openwindow.procHandle = hWindow
        .
    end.

  run ShowWindow in hWindow no-error.
  return hWindow.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ShowOpenWindowForAgentAppStat C-Win 
FUNCTION ShowOpenWindowForAgentAppStat RETURNS handle
  ( input pAgentID as char,
    input pType as char,
    input pWindow as char) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def var hwindow as handle no-undo.
  def var cAgentWindow as char no-undo.
  def buffer openWindow for openWindow.
  

  for each openWindow:
      if not valid-handle(openWindow.procHandle) 
        then delete openWindow.
  end.
  assign
      hwindow = ?
      cAgentWindow = pAgentID + " " + pType
      .

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  

  
  
  
/*   for each agentapplication                                                     */
/*       where agentapplication.agentID = pAgentID no-lock:                        */
/*       buffer-copy agentapplication to agentIDApp.                               */
/*   end.                                                                          */
/*                                                                                 */
/*   for each agentIDApp                                                           */
/*       where agentIDApp.agentID = pAgentID:                                      */
/*       message 'agentIDapp type of app:' agentIDapp.typeofapp view-as alert-box. */
/*   end.                                                                          */
 

if not valid-handle(hWindow)
   then
    do:
      run value(pWindow) persistent set hWindow (pAgentID, output std-lo).
      create openwindow.
      assign
        openwindow.procFile = cAgentWindow
        openwindow.procHandle = hWindow
        .
    end.

  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidPeriod C-Win 
FUNCTION ValidPeriod RETURNS logical
  ( input beginPeriod as int,
    input endPeriod as int) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 std-lo = true.

 std-ch = "".
  
 if endPeriod < beginPeriod then
 do:
     std-ch = std-ch +  "end period is less than beginning period.".
     
 end.

if std-ch > "" then
do:
   std-lo = false.
   message std-ch view-as alert-box.
end.

  RETURN std-lo.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

