&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input-output parameter pAgentID as char.
def output parameter pError as logical init true.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
def var tName as char no-undo.

{tt/agent.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tAgentID Btn_OK Btn_Cancel fState 
&Scoped-Define DISPLAYED-OBJECTS tAgentID fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 27.6 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN AUTO-COMPLETION
     SIZE 93 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 61.6 BY .62 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 61.6 BY .62 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     tAgentID AT ROW 2.91 COL 11 COLON-ALIGNED WIDGET-ID 84
     tAddr1 AT ROW 4.05 COL 11 COLON-ALIGNED NO-LABEL WIDGET-ID 86
     Btn_OK AT ROW 6.48 COL 39
     Btn_Cancel AT ROW 6.48 COL 57
     tAddr2 AT ROW 4.71 COL 11 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     fState AT ROW 1.71 COL 11 COLON-ALIGNED WIDGET-ID 92
     SPACE(70.59) SKIP(5.67)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Hold Open Policy"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tAddr1 IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tAddr2 IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* New Hold Open Policy */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState fNew
ON VALUE-CHANGED OF fState IN FRAME fNew /* State */
DO:
  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID fNew
ON VALUE-CHANGED OF tAgentID IN FRAME fNew /* Agent */
DO:
  find agent
    where agent.agentID = self:screen-value no-error.
  if not available agent 
   then assign
          tAddr1:screen-value in frame fNew = ""
          tAddr2:screen-value in frame fNew = ""
          .
   else assign
          tAddr1:screen-value in frame fNew = agent.addr1 + "  " + agent.addr2
          tAddr2:screen-value in frame fNew = agent.city 
                  + (if agent.city > "" and agent.state > "" then ", " else "")
                  + agent.state + "  " + agent.zip
          .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


publish "GetAgents" (output table agent).
tAgentID:delimiter = {&msg-dlm}.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  {lib/get-state-list.i &combo=fState &addAll=true}
  {lib/get-agent-list.i &combo=tAgentID &state=fState}
  {lib/set-current-value.i &state=fState &agent=tAgentID}
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if not can-find(first agent where agent.agentID = tAgentID:input-value)
   then
    do: 
        MESSAGE "Invalid Agent"
         VIEW-AS ALERT-BOX warning BUTTONS OK.
        next.
    end.

  /* Force screen values to variables */
  assign
    tAgentID.

  assign
    pAgentID = tAgentID
    pError = false
    .
  
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAgentID fState 
      WITH FRAME fNew.
  ENABLE tAgentID Btn_OK Btn_Cancel fState 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */


