&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* opsdatasrv.p
   DATA model for the OPerationS module that interacts with the applicationSeRVer
Created 5.20.2012 D.Sinclair

 9.28.2014 D.Sinclair  Added "DataDump" procedure to assist debugging.
11.26.2014 D.Sinclair  Added Splash publishes for user feedback.
 3.12.2015 D.Sinclair  Added check to load datasets on startup
 Modified: 
  Date          Name        Comments
  08/21/2019    Vikas Jain  Added code in two IPs modifyBatchForm and newBatchForm to handle two new fields formCount and revenueType 
  03.12.2020    Archana     Changed batch.state to batch.stateID
  03.12.2022    Shefali     Task #86699 Modified to cache attorney firm records and modified to update linking of agent and org.
  04.26.2022    Shefali     Task #93684 Modified to get correct attorney name.
 */ 
   
{tt/statcode.i}
{tt/stateform.i}
{tt/defaultform.i}
define variable lModifyOrganization  as logical  initial false no-undo.

{tt/policy.i}
{tt/cpl.i}
{tt/endorsement.i}
{tt/organization.i}
{tt/organization.i   &tableAlias=tcacheOrganization}
{tt/organization.i   &tableAlias=tOrganization}
{tt/endorsement.i &tableAlias="tempendorsement"}

{tt/batch.i}
{tt/batchaction.i}
{tt/batchform.i}

{tt/syscode.i &tableAlias="tempproerr"}
{tt/proerr.i}

{tt/holdopen.i}
{tt/esbmsg.i}

{tt/policyhoi.i}
{tt/policyhoi.i &tableAlias="temppolicyhoi"}

/* Used to return subsets of the original datasets for various calls */
{tt/statcode.i &tableAlias="tempStatCode"}
{tt/stateform.i &tableAlias="tempStateForm"}
{tt/defaultform.i &tableAlias="tempDefaultForm"}

{tt/policy.i &tableAlias="tempPolicy"}
{tt/cpl.i &tableAlias="tempCPL"}

{tt/batch.i &tableAlias="tempBatch"}
{tt/batchaction.i &tableAlias="tempBatchAction"}
{tt/batchform.i &tableAlias="tempBatchForm"}

{tt/repository.i &tableAlias="batchitem"}
{tt/repository.i &tableAlias="tempbatchitem"}

{tt/attorney.i &tableAlias=tAttorney}
{tt/attorney.i &tableAlias=ttAttorney}

{tt/person.i   &tableAlias=tperson}
{tt/person.i   &tableAlias=tcachePerson}

{lib/std-def.i}
{lib/add-delimiter.i}
{lib/datasource.i &name="SERVER"}
{lib/alertdatasrv.i}
{lib/com-def.i}

{tt/syscode.i  &tableAlias=ttsyscode}

{tt/arrevenue.i}
{tt/arrevenue.i  &tableAlias=ttarrevenue}

{tt/agentmanager.i}

def var tLoadedStatCodes as logical init false no-undo.
def var tLoadedStateForms as logical init false no-undo.
def var tLoadedDefaultEndorsements as logical init false no-undo.
def var tLoadedProcessingErrors as logical init false no-undo.
def var tLoadedHoldOpens as logical init false no-undo.
def var tLoadedPolicyhoi as logical initial false no-undo.

define variable tLastSync as datetime no-undo.

define variable tAccountUser as character             no-undo.
define variable tAccountPass as character             no-undo.
define variable cCurrentUser as character             no-undo.
define variable loadPersons  as logical initial false no-undo.

def stream s1.

define temp-table attorney like tAttorney
 fields entity     as character
 fields entityID   as character
 fields entityName as character.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-addFormToBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD addFormToBatch Procedure 
FUNCTION addFormToBatch RETURNS LOGICAL PRIVATE
 (
  input pBatchID as int,
  input pSeq as int,

  input pFormType as char,
  input pFileNumber as char,
 
  input pPolicyID as int,
  input pFormID as char,
  input pStatCode as char,
 
  input pEffDate as datetime,
  input pLiabilityAmount as decimal,
 
  input pCountyID as char,
  input pResidential as logical,

  input pGrossPremium as decimal,
  input pNetPremium as decimal,
 
  input pValidForm as char,
  input pValidMsg as char,

  output pRedo as logical
 ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getNextBatchID) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getNextBatchID Procedure 
FUNCTION getNextBatchID RETURNS CHARACTER PRIVATE
  ( input pMonth as int,
    input pYear as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBatch Procedure 
FUNCTION refreshBatch RETURNS LOGICAL PRIVATE
  ( pBatchID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatchActions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBatchActions Procedure 
FUNCTION refreshBatchActions RETURNS LOGICAL PRIVATE
  ( pBatchID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatchForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBatchForm Procedure 
FUNCTION refreshBatchForm RETURNS LOGICAL PRIVATE
  ( pBatchID as int,
    pSeq as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatchForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBatchForms Procedure 
FUNCTION refreshBatchForms RETURNS LOGICAL PRIVATE
  ( pBatchID as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshDefaultEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshDefaultEndorsements Procedure 
FUNCTION refreshDefaultEndorsements RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshStatCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshStatCode Procedure 
FUNCTION refreshStatCode RETURNS LOGICAL PRIVATE
  ( pStateID as char,
    pStatCode as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshStateForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshStateForm Procedure 
FUNCTION refreshStateForm RETURNS LOGICAL PRIVATE
  ( pStateID as char,
    pFormID as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeFormFromBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeFormFromBatch Procedure 
FUNCTION removeFormFromBatch RETURNS LOGICAL PRIVATE
 (pBatchID as int,
  pSeq as int) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.76
         WIDTH              = 59.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

{lib/agentdatasrv.i}
{lib/statedatasrv.i}
{lib/countydatasrv.i}
{lib/perioddatasrv.i}
{lib/regiondatasrv.i}
{lib/syscodedatasrv.i &syscode="'processing'"}
{lib/syspropdatasrv.i}
{lib/sysuserdatasrv.i}

publish "SetSplashStatus".
publish "GetLoadStatCodes" (output std-lo).
if std-lo 
 then run LoadStatCodes in this-procedure.

publish "SetSplashStatus".
publish "GetLoadStateForms" (output std-lo).
if std-lo 
 then run LoadStateForms in this-procedure.

publish "SetSplashStatus".
publish "GetLoadDefaultEndorsements" (output std-lo).
if std-lo 
 then run LoadDefaultEndorsements in this-procedure.

tLastSync = now. /* Time we retrieved the current data set */

publish "GetCredentialsName" (output cCurrentUser).
publish "GetCredentialsID"   (output tAccountUser).
publish "GetCredentialsPassword" (output tAccountPass).


/* Procedures to refresh the whole dataset from the server */
subscribe to "LoadStateForms" anywhere.
subscribe to "LoadDefaultEndorsements" anywhere.
subscribe to "LoadStatCodes" anywhere.
subscribe to "LoadBatches" anywhere.
subscribe to "LoadPolicies" anywhere.
subscribe to "LoadCPLs" anywhere.
subscribe to "LoadProcessingErrors" anywhere.

SUBSCRIBE TO "GetStateForms" anywhere.
SUBSCRIBE TO "NewStateForm" anywhere.
SUBSCRIBE TO "ModifyStateForm" anywhere.
SUBSCRIBE TO "DeleteStateForm" anywhere.

SUBSCRIBE TO "GetStatCodes" anywhere.
SUBSCRIBE TO "NewStatCode" anywhere.
SUBSCRIBE TO "ModifyStatCode" anywhere.
SUBSCRIBE TO "DeleteStatCode" anywhere.

SUBSCRIBE TO "GetPeriodTotals" anywhere.

SUBSCRIBE TO "GetAgents" anywhere.
subscribe to "GetAgent" anywhere.

SUBSCRIBE TO "GetBatches" anywhere.
SUBSCRIBE TO "GetBatchesTemp" anywhere.

SUBSCRIBE TO "GetBatch" anywhere.
SUBSCRIBE TO "GetBatchAttribute" anywhere.

subscribe to "GetServerBatches" anywhere.

SUBSCRIBE TO "NewBatch" anywhere.
SUBSCRIBE TO "MoveBatch" anywhere.
SUBSCRIBE TO "ModifyBatch" anywhere.
SUBSCRIBE TO "DeleteBatch" anywhere.
SUBSCRIBE TO "LockBatch" anywhere.
SUBSCRIBE TO "UnlockBatch" anywhere.
SUBSCRIBE TO "InvoiceBatch" anywhere.
SUBSCRIBE TO "ToReviewBatch" anywhere.
SUBSCRIBE TO "ToProcessBatch" anywhere.

SUBSCRIBE TO "ValidateBatch" anywhere.
SUBSCRIBE TO "SplitBatch" anywhere.

SUBSCRIBE TO "ValidateBatchForm" anywhere.

SUBSCRIBE TO "GetBatchActions" anywhere.
SUBSCRIBE TO "NewBatchAction" anywhere.
SUBSCRIBE TO "DeleteBatchAction" anywhere.

SUBSCRIBE TO "GetBatchForms" anywhere.
SUBSCRIBE TO "NewBatchForm" anywhere.
SUBSCRIBE TO "ModifyBatchForm" anywhere.
SUBSCRIBE TO "DeleteBatchForm" anywhere.
SUBSCRIBE TO "PurgeAllBatchForms" anywhere.
SUBSCRIBE TO "SearchBatchForms" anywhere.
subscribe to "GetBatchFormZipCode" anywhere.

SUBSCRIBE TO "GetPolicies" anywhere.
SUBSCRIBE TO "GetCPLs" anywhere.

SUBSCRIBE TO "GetDetailsFile" anywhere.

subscribe to "IsPolicyValid" anywhere.

subscribe to "GetDefaultEndorsements" anywhere.
subscribe to "NewDefaultEndorsement" anywhere.
subscribe to "DeleteDefaultEndorsement" anywhere.
subscribe to "MoveDefaultEndorsement" anywhere.

subscribe to "NewPolicy" anywhere.
subscribe to "VoidPolicy" anywhere.
subscribe to "IssuePolicy" anywhere.
subscribe to "SwapPolicy" anywhere.
subscribe to "NewHoldOpenPolicy" anywhere.

subscribe to "GetProcessingErrors" anywhere.

subscribe to "GetLastSync" anywhere.

subscribe to "ReassignPolicies" anywhere.

subscribe to "GetHoldOpens" anywhere.
subscribe to "LoadHoldOpens" anywhere.

subscribe to "GetPolicyhoi" anywhere.
subscribe to "DeletePolicyHoi" anywhere.
subscribe to "LoadPolicyhoiID" anywhere.
subscribe to "GetPolicy" anywhere.
subscribe to "NewPolicyHoi" anywhere.
subscribe to "ModifyPolicyHoi" anywhere.
subscribe to "SearchPolicyHOI" anywhere.
subscribe to "getOrganizations" anywhere.
subscribe to "GetEndorsements" anywhere.
subscribe to "getPersons" anywhere.

/* Called by esbdata.p/NewStompMessage when another user changes 
   something we're interested in (as specified in param to lib/appstart.i) */
subscribe to "ExternalEsbMessage" anywhere.

/* Published by lib/appstart.i when the user is idle */
subscribe to "TIMER" anywhere.
subscribe to "SetLastSync" anywhere.

/* Published by lib/appstart.i just before quitting; used for testing */
subscribe to "QUIT" anywhere.

/* Published by sys/about.w to allow trouble-shooting of data contents */
subscribe to "DataDump" anywhere.

/*-------------------------Agents---------------------------*/
subscribe to "getManagerName"      anywhere.

/*-------------------------System Codes----------------------*/
subscribe to "refreshSysCodes"         anywhere.
subscribe to "newSysCode"              anywhere.
subscribe to "modifySysCode"           anywhere.
subscribe to "deleteSysCode"           anywhere.
subscribe to "getSysCodeTypeListItems" anywhere.
subscribe to "getSysCodeListItems"     anywhere.
subscribe to "getSysCodeDescs"         anywhere.

/*------------------Revenue Types------------------*/
subscribe to "newRevenue"           anywhere.
subscribe to "modifyRevenue"        anywhere.
subscribe to "deleteRevenue"        anywhere.
subscribe to "getRevenues"          anywhere.
subscribe to "refreshRevenues"      anywhere.

/*----------------------Persons----------------*/
subscribe to "getPersons"               anywhere.
subscribe to "getPersonName"            anywhere.
subscribe to "validPerson"              anywhere.
subscribe to "newPersonAttorney"        anywhere.

/*----------------------States----------------*/
subscribe to "getStatesListItems"       anywhere.
subscribe to "GetStateIDList"           anywhere.

/*--------------------Organizations-------------*/
subscribe to "getOrganizationName"      anywhere.
subscribe to "newOrganizationAttorney"  anywhere.
subscribe to "newOrganizationAgent"     anywhere.

/*---------------------Attorneys-------------------------*/
subscribe to "newAttorney"                        anywhere.
subscribe to "modifyAttorney"                     anywhere.
subscribe to "getAttorneys"                       anywhere.
subscribe to "getAttorney"                        anywhere.
subscribe to "refreshAttorneys"                   anywhere.
subscribe to "getStatusList"                      anywhere.
subscribe to "setAttorneyStatus"                  anywhere.
subscribe to "getAttorneyStatus"                  anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-DataDumpEXT) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataDumpEXT Procedure 
PROCEDURE DataDumpEXT :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tPrefix as char no-undo.

 publish "DeleteTempFile" ("DataStatCode", output std-lo).
 publish "DeleteTempFile" ("DataStateForm", output std-lo).
 publish "DeleteTempFile" ("DataDefaultForm", output std-lo).
 publish "DeleteTempFile" ("DataBatch", output std-lo).
 publish "DeleteTempFile" ("DataBatchForm", output std-lo).
 publish "DeleteTempFile" ("DataBatchAction", output std-lo).
 publish "DeleteTempFile" ("DataPolicy", output std-lo).
 publish "DeleteTempFile" ("DataEndorsement", output std-lo).
 publish "DeleteTempFile" ("DataCPL", output std-lo).
 publish "DeleteTempFile" ("DataProErr", output std-lo).

 publish "DeleteTempFile" ("DataTempStatCode", output std-lo).
 publish "DeleteTempFile" ("DataTempStateForm", output std-lo).
 publish "DeleteTempFile" ("DataTempDefaultForm", output std-lo).
 publish "DeleteTempFile" ("DataTempBatch", output std-lo).
 publish "DeleteTempFile" ("DataTempBatchForm", output std-lo).
 publish "DeleteTempFile" ("DataTempBatchAction", output std-lo).
 publish "DeleteTempFile" ("DataTempPolicy", output std-lo).
 publish "DeleteTempFile" ("DataTempCPL", output std-lo).

 tPrefix = "data" + string(year(today), "9999")
                  + string(month(today), "99")
                  + string(day(today), "99") 
                  + string(etime).

 /* Active temp-tables */

 temp-table statcode:write-xml("file", tPrefix + "5").
 publish "AddTempFile" ("DataStatCode", tPrefix + "5").

 temp-table stateform:write-xml("file", tPrefix + "6").
 publish "AddTempFile" ("DataStateForm", tPrefix + "6").

 temp-table defaultform:write-xml("file", tPrefix + "7").
 publish "AddTempFile" ("DataDefaultForm", tPrefix + "7").
 
 temp-table batch:write-xml("file", tPrefix + "8").
 publish "AddTempFile" ("DataBatch", tPrefix + "8").

 temp-table batchform:write-xml("file", tPrefix + "9").
 publish "AddTempFile" ("DataBatchForm", tPrefix + "9").

 temp-table batchaction:write-xml("file", tPrefix + "10").
 publish "AddTempFile" ("DataBatchAction", tPrefix + "10").

 temp-table policy:write-xml("file", tPrefix + "11").
 publish "AddTempFile" ("DataPolicy", tPrefix + "11").

 temp-table endorsement:write-xml("file", tPrefix + "12").
 publish "AddTempFile" ("DataEndorsement", tPrefix + "12").

 temp-table cpl:write-xml("file", tPrefix + "13").
 publish "AddTempFile" ("DataCPL", tPrefix + "13").

 temp-table proerr:write-xml("file", tPrefix + "14").
 publish "AddTempFile" ("DataProErr", tPrefix + "14").


 /* "Temp" temp-tables */

 temp-table tempstatcode:write-xml("file", tPrefix + "20").
 publish "AddTempFile" ("DataTempStatCode", tPrefix + "20").

 temp-table tempstateform:write-xml("file", tPrefix + "21").
 publish "AddTempFile" ("DataTempStateForm", tPrefix + "21").

 temp-table tempdefaultform:write-xml("file", tPrefix + "22").
 publish "AddTempFile" ("DataTempDefaultForm", tPrefix + "22").
 
 temp-table tempbatch:write-xml("file", tPrefix + "23").
 publish "AddTempFile" ("DataTempBatch", tPrefix + "23").

 temp-table tempbatchform:write-xml("file", tPrefix + "24").
 publish "AddTempFile" ("DataTempBatchForm", tPrefix + "24").

 temp-table tempbatchaction:write-xml("file", tPrefix + "25").
 publish "AddTempFile" ("DataTempBatchAction", tPrefix + "25").

 temp-table temppolicy:write-xml("file", tPrefix + "26").
 publish "AddTempFile" ("DataTempPolicy", tPrefix + "26").

 temp-table tempcpl:write-xml("file", tPrefix + "27").
 publish "AddTempFile" ("DataTempCPL", tPrefix + "27").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteBatch Procedure 
PROCEDURE DeleteBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pComments as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 
 run server/deletebatch.p (pBatchID,
                           pComments,
                           output pSuccess,
                           output pMsg).

 if not pSuccess
  then return.

 refreshBatch(pBatchID). 
 refreshBatchActions(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteBatchAction) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteBatchAction Procedure 
PROCEDURE DeleteBatchAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pSeq as int.
 
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batchaction for batchaction.

 run server/deletebatchaction.p (pBatchID,
                                 pSeq,
                                 output pSuccess,
                                 output pMsg).

 if not pSuccess
  then return.

 find batchaction
   where batchaction.batchID = pBatchID
     and batchaction.seq = pSeq no-error.
 if available batchaction 
  then delete batchaction.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteBatchForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteBatchForm Procedure 
PROCEDURE DeleteBatchForm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pSeq as int.
 
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batchform for batchform.

 run server/deletebatchform.p (pBatchID,
                               pSeq,
                               output pSuccess,
                               output pMsg).
                           
 if not pSuccess
  then return.
 
 find batchform
   where batchform.batchID = pBatchID
     and batchform.seq = pSeq no-error.
 if available batchform
  then delete batchform.
  
 refreshBatch(pBatchID).
 /* refreshBatchForms(pBatchID). <-- takes too long for large batches */  
   
 PUBLISH "BatchTotalsChanged".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteDefaultEndorsement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteDefaultEndorsement Procedure 
PROCEDURE DeleteDefaultEndorsement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pIdType as char.      /* A)gent or S)tate */
 def input parameter pId as char.          /* AgentID or StateID */
 def input parameter pInsuredType as char. /* O)wner or L)ender */
 def input parameter pSeq as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/deletedefaultform.p (pIdType,
                                 pId,
                                 pInsuredType,
                                 pSeq,
                                 output pSuccess,
                                 output pMsg).
 if not pSuccess
  then return.
            
 refreshDefaultEndorsements().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeletePolicyHoi) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeletePolicyHoi Procedure 
PROCEDURE DeletePolicyHoi :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pPolicyID as int.
def output parameter pSuccess as logical init false.
def output parameter pMsg as char.
 
 def buffer policyhoi for policyhoi.

 run server/deletepolicyhoi.p (pPolicyID,
                               output pSuccess,
                               output pMsg).
 if not pSuccess
  then return.                          

 find policyhoi
   where policyhoi.policyID = pPolicyID no-error.
 if available policyhoi
  then delete policyhoi.

 publish "PolicyHOIChanged".


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-deleteRevenue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRevenue Procedure 
PROCEDURE deleteRevenue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter ipcRevenueType  as character no-undo.
  define output parameter oplSuccess      as logical   no-undo.
  define output parameter opcMsg          as character no-undo.
 
  define buffer arrevenue for arrevenue.

  /* client Server Call */
  run server/deleterevenue.p (input  ipcRevenueType,
                              output oplSuccess,
                              output opcMsg).
  
  if not oplSuccess 
   then
    return.

  /* Update Cache records */
  find first arrevenue where arrevenue.revenueType = ipcRevenueType no-error.
  if not available arrevenue
   then
    do:
      assign
          opcMsg     = "Internal Error of missing data."
          oplSuccess = false
          .
      return.
    end.

  delete arrevenue.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteStatCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteStatCode Procedure 
PROCEDURE DeleteStatCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStateID as char.
 def input parameter pStatCode as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer statcode for statcode.

 run server/deletestatcode.p (pStateID,
                              pStatCode,
                              output pSuccess,
                              output pMsg).
                          
 if not pSuccess
  then return.
                           
 find statcode
   where statcode.stateID = pStateID
     and statcode.statcode = pStatCode no-error.
 if available statcode 
  then delete statcode.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteStateForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteStateForm Procedure 
PROCEDURE DeleteStateForm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStateID as char.
 def input parameter pFormID as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer stateform for stateform.

 run server/deletestateform.p (pStateID,
                               pFormID,
                               output pSuccess,
                               output pMsg).
 if not pSuccess
  then return.                          

 find stateform
   where stateform.stateID = pStateID
     and stateform.formID = pFormID no-error.
 if available stateform 
  then delete stateform.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-deleteSysCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteSysCode Procedure 
PROCEDURE deleteSysCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input parameter  cType      as character no-undo.
  define input parameter  cCode      as character no-undo.

  define output parameter oplSuccess as logical   no-undo.
  define output parameter opcMsg     as character no-undo.

  define buffer sysCode for sysCode.

  /* client Server Call */
  run server/deletesyscode.p (input  cType, 
                              input  cCode,
                              output oplSuccess,
                              output opcMsg).

  if not oplSuccess 
   then
    return.

  find first sysCode where sysCode.codeType = cType 
                       and sysCode.code     = cCode no-error.
      
  if not available sysCode 
   then
    do: 
      assign 
          opcMsg     = "Internal Error of missing data."  
          oplSuccess = false
          . 
      return.
    end.
    
  delete sysCode.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getAllAgentManagers) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAllAgentManagers Procedure 
PROCEDURE getAllAgentManagers :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer agentmanager for agentmanager.
  
  empty temp-table agentmanager.
  
  /* Server Call */
  run server/getallagentmanagers.p (output table agentmanager,
                                    output std-lo,
                                    output std-ch).
  if not std-lo
   then 
    do: 
       std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then
         message "LoadAgentManagers failed: " std-ch view-as alert-box warning.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getAttorney) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAttorney Procedure 
PROCEDURE getAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for attorney.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getAttorneys) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAttorneys Procedure 
PROCEDURE getAttorneys :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for attorney. 
 
  /* Buffer */
  define buffer attorney for attorney.
  define buffer tAttorney for tAttorney.

  /* Client Server Call */
  if not can-find(first tattorney)
   then
    do:  
      run server/getAttorneys.p (input  "" ,   /* stateID      */
                                 input "" ,  /* Person  */
                                 output table tattorney,
                                 output std-lo,
                                 output std-ch
                                )
                                .
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "LoadAttorneys failed: " std-ch
                 view-as alert-box error.
      
      empty temp-table attorney.
        
      for each tAttorney:
        create attorney.
        buffer-copy tAttorney to attorney.
        assign
             attorney.entity     = if (tAttorney.orgID ne "" and tAttorney.orgID ne ?) then {&OrganizationCode} else 
                                   if (tAttorney.personID ne "" and tAttorney.personID ne ?) then {&PersonCode} else ""
             attorney.entityID   = if (tAttorney.orgID ne "" and tAttorney.orgID ne ?) then tAttorney.orgID  else
                                   if (tAttorney.personID ne "" and tAttorney.personID ne ?) then tAttorney.personID else ""
             attorney.entityName = if attorney.entity = {&OrganizationCode} then attorney.firmname else 
                                   if attorney.entity = {&PersonCode}       then attorney.name1 else (if (attorney.name1 ne "" and attorney.name1 ne ?) then attorney.name1   
                                                                                                      else attorney.firmname)
             .
      end.
    end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getAttorneyStatus) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getAttorneyStatus Procedure 
PROCEDURE getAttorneyStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter ipAttorneyId as character no-undo.
 define output parameter opstatus     as character no-undo.
 
 define buffer attorney for attorney.
 
 if not can-find (first attorney where attorney.attorneyID = ipAttorneyId)
  then
   run getAttorneys in this-procedure (output table attorney).
 
 for first attorney where attorney.attorneyID = ipattorneyID :
   opstatus = attorney.stat.
 end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatch Procedure 
PROCEDURE GetBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatch as int.
 def output parameter table for tempbatch.

 def buffer batch for batch.
 def buffer tempbatch for tempbatch.

 empty temp-table tempbatch.

 find batch 
   where batch.batchID = pBatch no-error.
 if not available batch 
  then return.

 create tempbatch.
 buffer-copy batch to tempbatch.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatchActions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatchActions Procedure 
PROCEDURE GetBatchActions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pBatchID as int.
def output parameter table for tempBatchAction.

def buffer batchaction for batchAction.
def buffer tempBatchAction for tempBatchAction.

refreshBatchActions(pBatchID).

empty temp-table tempBatchAction.

for each batchAction
   where batchAction.batchID = pBatchID:
 create tempBatchAction.
 buffer-copy batchAction to tempBatchAction.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatchAttribute) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatchAttribute Procedure 
PROCEDURE GetBatchAttribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatch as int.
 def input parameter pAttribute as char.
 def output parameter pValue as char.

 def buffer batch for batch.
 
 find batch 
   where batch.batchID = pBatch no-error.
 if not available batch 
  then return.

 case pAttribute:
  when "AgentID" 
   then pValue = batch.agentID.
  when "LiabilityAmount" 
   then pValue = string(batch.liabilityAmount).
  when "GrossPremiumProcessed" 
   then pValue = string(batch.grossPremiumProcessed).
  when "NetPremiumProcessed" 
   then pValue = string(batch.netPremiumProcessed).
  when "RetainedPremiumProcessed" 
   then pValue = string(batch.retainedPremiumProcessed).
  when "Status" 
   then pValue = batch.stat.
  when "ProcessingStatus" 
   then pValue = batch.processStat.
  when "LockedBy" 
   then pValue = batch.lockedBy.
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatches) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatches Procedure 
PROCEDURE GetBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pMonth as int.
def input parameter pYear as int.
def output parameter table for tempBatch.

def buffer batch for batch.
def buffer period for period.

empty temp-table tempBatch.

find period
  where period.periodMonth = pMonth
    and period.periodYear = pYear no-error.
if not available period
 then return.

find first batch
  where batch.periodID = period.periodID no-error.

if not available batch
 then run LoadBatches in this-procedure (period.periodID).
  
for each batch
  where batch.periodID = period.periodID:
 create tempBatch.
 buffer-copy batch to tempBatch.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatchesTemp) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatchesTemp Procedure 
PROCEDURE GetBatchesTemp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pMonth as int.
def input parameter pYear as int.
def output parameter table for tempBatch.

def var tSuccess as logical no-undo.
def var tMsg as char no-undo.

def buffer period for period.

empty temp-table tempbatch.
empty temp-table tempbatchform.
empty temp-table tempbatchaction.

find period
  where period.periodMonth = pMonth
    and period.periodYear = pYear no-error.
if not available period
 then return.

run server/getbatches.p (period.periodID,
                         0, /* BatchID (zero=All) */
                         output table tempbatch,
                         output table tempbatchaction,
                         output tSuccess,
                         output tMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatchForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatchForms Procedure 
PROCEDURE GetBatchForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pBatchID as int.
def output parameter table for tempBatchForm.

def buffer batchform for batchform.
def buffer tempBatchForm for tempBatchForm.

refreshBatchForms(pBatchID).

empty temp-table tempBatchForm.

for each batchform
   where batchform.batchID = pBatchID:
 create tempBatchForm.
 buffer-copy batchform to tempBatchform.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetBatchFormZipCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetBatchFormZipCode Procedure 
PROCEDURE GetBatchFormZipCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter pState as char no-undo.
  def output parameter pShowZipCode as logical no-undo.

  def buffer sysprop for sysprop.

  pShowZipCode = false.
  for each sysprop no-lock
     where sysprop.appCode = "OPS"
       and sysprop.objAction = "BatchForm"
       and sysprop.objProperty = "ZipCode"
       and sysprop.objID = "State"
       and sysprop.objValue = pState:
       
    pShowZipCode = true.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCPLs) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCPLs Procedure 
PROCEDURE GetCPLs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAgentID as char.
 def input parameter pFileNumber as char.
 def input parameter pStartCPL as char.
 def input parameter pEndCPL as char.
 def input parameter pCPLStat as char.
 def input parameter pStartDate as datetime.
 def input parameter pEndDate as datetime.
 def output parameter table for tempCPL.
 
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
 
 empty temp-table tempCPL.

 run server/getcpls.p (pAgentID,
                   pFileNumber,
                   pStartCPL,
                   pEndCPL,
                   pCPLStat,
                   pStartDate,
                   pEndDate,
                   output table tempCPL,
                   output tSuccess,
                   output tMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetDefaultEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDefaultEndorsements Procedure 
PROCEDURE GetDefaultEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAgentID as char.
 def input parameter pStateID as char.
 def input parameter pInsuredType as char.
 def output parameter table for tempdefaultform.

 def buffer agent for agent.
 def buffer defaultform for defaultform.
 def buffer stateform for stateform.

 def var tRemitType as char no-undo.
 def var tRemitValue as decimal no-undo.
 def var tStateID as char no-undo.

 empty temp-table tempdefaultform.


 /* Lenders or Owners */
 if index("LOB", pInsuredType) = 0
  then return.

 /* Verify we have the required datasets */
 if not tLoadedDefaultEndorsements 
  then run LoadDefaultEndorsements in this-procedure.
 if not tLoadedAgents 
  then run LoadAgents in this-procedure.
 if not tLoadedStateForms 
  then run LoadStateForms in this-procedure.
 if not tLoadedStatCodes
  then run LoadStatCodes in this-procedure.

 find agent
   where agent.agentID = pAgentID 
   and agent.agentID <> "" no-error.
 if available agent
  then assign
         tRemitType = agent.remitType
         tRemitValue = agent.remitValue
         tStateID = agent.stateID
         .
  else assign
         tRemitType = "P"
         tRemitValue = .15
         tStateID = pStateID
         .

 std-in = 0.
 if pAgentID <> "" and avail agent then
 for each defaultform
   where defaultform.ID = pAgentID
     and defaultform.insuredType begins pInsuredType:
  
  find stateform
    where stateform.stateID = tStateID
      and stateform.formID = defaultform.formID no-error.

  create tempdefaultform.
  tempdefaultform.ID = defaultform.ID.
  if available stateform 
   then assign
          tempdefaultform.formID = stateform.formID
          tempdefaultform.description = stateform.description + " (" + stateform.insuredType + ")"
          .
  if available stateform
    and tRemitType = "P" 
   then
    assign
      tempdefaultform.grossPremium = stateform.rateMin
      tempdefaultform.netPremium = round(stateform.rateMin * tRemitValue, 2)
      .

  tempdefaultform.type = "D".  /* Default */
  tempdefaultform.include = true.
  tempdefaultform.statcode = defaultform.statcode.
  tempdefaultform.seq = defaultform.seq.

  std-in = std-in + 1.
  if std-in = 10 
   then leave.
 end.
 
/*  if std-in > 0 */
/*   then return. */

 if std-in = 0 then
 do:
   /* Default forms for a state */
   if pStateID <> "" then
   for each defaultform
     where defaultform.ID = tStateID
       and defaultform.insuredType begins pInsuredType:
    
    find stateform
      where stateform.stateID = tStateID
        and stateform.formID = defaultform.formID no-error.
  
    create tempdefaultform.
    tempdefaultform.ID = defaultform.ID.
    if available stateform
     then assign
            tempdefaultform.formID = stateform.formID
            tempdefaultform.description = stateform.description + " (" + stateform.insuredType + ")"
            .
    if available stateform
      and tRemitType = "P" 
     then
      assign
        tempdefaultform.grossPremium = stateform.rateMin
        tempdefaultform.netPremium = round(stateform.rateMin * tRemitValue, 2)
        .
  
    tempdefaultform.type = "D".  /* Default */
    tempdefaultform.include = true.
    tempdefaultform.statcode = defaultform.statcode.
    tempdefaultform.seq = defaultform.seq.
  
    std-in = std-in + 1.
    if std-in = 10 
     then leave.
   end.
 end.
 
 /* Add remaining (non-default) forms to the list */
 std-in = 0.
 for each tempdefaultform by tempdefaultform.seq:
  std-in = tempdefaultform.seq.
 end.

 for each stateform where stateform.stateID = tStateID by stateform.formID:
  if can-find(first tempdefaultform where tempdefaultform.formID = stateform.formID) then
  next.
  
  std-in = std-in + 1.
  create tempdefaultform.
  assign
    tempdefaultform.ID = tStateID
    tempdefaultform.type = "N"    /* Non-Default */
    tempdefaultform.insuredType = stateform.insuredType
    tempdefaultform.formID = stateform.formID
    tempdefaultform.seq = std-in
    tempdefaultform.description = stateform.description
    tempdefaultform.include = false
    tempdefaultform.statCode = ""
    tempdefaultform.grossPremium = stateform.rateMin
    tempdefaultform.netPremium = round(stateform.rateMin * tRemitValue, 2).
    
  for first statcode
    where statcode.stateID = tStateID
      and statcode.formID = tempdefaultform.formID:
        tempdefaultform.statCode = statcode.statCode.
  end.
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetDetailsFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetDetailsFile Procedure 
PROCEDURE GetDetailsFile :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAgentID as char.
def input parameter pFileNumber as char.
def output parameter pLiability as decimal init 0.0.
def output parameter pGross as decimal init 0.0.
def output parameter pNet as decimal init 0.0.
def output parameter pRetention as decimal init 0.0.
def output parameter table for tempBatchForm.

def var pSuccess as logical init false.
def var pMsg as char no-undo.

empty temp-table tempBatchForm.

run server/getfileforms.p (pAgentID,
                       pFileNumber,
                       output pLiability,
                       output pGross,
                       output pNet,
                       output pRetention,
                       output table tempbatchform,
                       output pSuccess,
                       output pMsg).

/*                       
for each policy
  where policy.agentID = pAgentID
    and policy.fileNumber = pFileNumber:
 create tempBatchForm.
 assign
   tempBatchForm.formType = "P"
   tempBatchForm.policyID = policy.policyID
   tempbatchform.fileNumber = pFileNumber
   tempbatchform.formID = policy.formID
   tempbatchform.grossPremium = policy.grossPremium
   tempbatchform.netPremium = policy.netPremium
   tempbatchform.retentionPremium = policy.retention
   tempbatchform.residential = policy.residential
   tempbatchform.effDate = policy.effDate
   tempbatchform.liabilityAmount = policy.liabilityAmount
   .

 assign
   pLiability = max(pLiability, policy.liabilityAmount)
   pGross = pGross + policy.grossPremium
   pNet = pNet + policy.netPremium
   pRetention = pRetention + policy.retention
   .

 for each endorsement
   where endorsement.policyID = policy.policyID:
  create tempBatchForm.
  assign
    tempBatchForm.formType = "E"
    tempBatchForm.policyID = endorsement.policyID
    tempbatchform.fileNumber = pFileNumber
    tempbatchform.formID = endorsement.formID
    tempbatchform.effDate = endorsement.effDate
    tempbatchform.grossPremium = endorsement.grossPremium
    tempbatchform.netPremium = endorsement.netPremium
    tempbatchform.retentionPremium = endorsement.retention
    .
  assign
    pGross = pGross + endorsement.grossPremium
    pNet = pNet + endorsement.netPremium
    pRetention = pRetention + endorsement.retention
    .
 end.
end.
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetEndorsements Procedure 
PROCEDURE GetEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pPolicyID as integer no-undo.
  define output parameter table for tempendorsement.
 
  if not can-find(first endorsement where policyID = pPolicyID)
   then
    do:
      empty temp-table tempendorsement.
      run server/getendorsements.p (input  pPolicyID,
                                    output table tempendorsement,
                                    output std-lo,
                                    output std-ch).
                                    
      if not std-lo
       then message std-ch view-as alert-box error buttons ok.
       else
        for each tempendorsement no-lock:
          create endorsement.
          buffer-copy tempendorsement to endorsement.
        end.
    end.
    
  empty temp-table tempendorsement.
  for each endorsement no-lock
     where endorsement.policyID = pPolicyID:
      
    create tempendorsement.
    buffer-copy endorsement to tempendorsement.
  end.
                       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetHoldOpens) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetHoldOpens Procedure 
PROCEDURE GetHoldOpens :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def output parameter table for holdopen.

if not tLoadedHoldOpens 
 then run LoadHoldOpens in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getManagerName) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getManagerName Procedure 
PROCEDURE getManagerName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcType  as character no-undo.
  define input  parameter ipcID    as character no-undo.
  define output parameter opcName  as character no-undo.
  
  define buffer agentManager for agentManager.
  
  if not can-find(first agentManager)
   then
    run getAllAgentManagers in this-procedure.
    
  if ipcType = "U"
   then
    do:
      for first agentManager where agentmanager.UID = ipcID:
        opcName =  agentmanager.UIDDesc.
      end.  
    end.
   else
    do:
      for first agentManager where agentmanager.agentID = ipcID:
        opcName =  agentmanager.UID.
      end.  
    end.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getOrganizationName) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getOrganizationName Procedure 
PROCEDURE getOrganizationName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcOrgID    as character no-undo.
  define output parameter  opcName    as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.

  define buffer organization for organization.
  
  /* Client Server Call */
  if not can-find(first organization)
   then
    do:
      run server/getorganizations.p (input {&ALL},  /* organizationStatus */
                                     output table organization,
                                     output std-lo,
                                     output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Loadorganizations failed: " std-ch
                 view-as alert-box error.
    end.

  find first organization where organization.orgID = ipcOrgID no-error.
  if available organization
   then
    do:
      opcName = organization.name.
      oplSuccess   = true.
    end.    
  else
   oplSuccess = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getOrganizations) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getOrganizations Procedure 
PROCEDURE getOrganizations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for organization.
 
  /* Buffer */
  define buffer organization for organization.

  /* Client Server Call */
  if not can-find(first organization) or lModifyOrganization
   then
    do:
      run server/getorganizations.p ("ALL",  /* organizationStatus */
                              output table organization,
                              output std-lo,
                              output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Loadorganizations failed: " std-ch
                 view-as alert-box error.
                 
      IF lModifyOrganization 
       THEN
        lModifyOrganization = FALSE.
    end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPeriodTotals) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPeriodTotals Procedure 
PROCEDURE GetPeriodTotals :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMonth as int.
 def input parameter pYear as int.
 def input parameter pState as char.
 def input parameter pStatus as char.

 def output parameter pGrossReported as decimal.
 def output parameter pNetReported as decimal.
 def output parameter pGrossProcessed as decimal.
 def output parameter pNetProcessed as decimal.
 def output parameter pGrossRevenue as decimal.
 def output parameter pNetRevenue as decimal.
 def output parameter pBatches as int.

 def var tPeriodID as int no-undo.
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.

 empty temp-table tempbatch.
  
/*  run server/getperiodtotals.p (pMonth,                  */
/*                                pYear,                   */
/*                                output tPeriodID,        */
/*                                output table tempbatch,  */
/*                                output tSuccess,         */
/*                                output tMsg).            */
                           
 publish "GetPeriodID" (pMonth, pYear, output tPeriodID).

 for each batch
   where batch.periodID = tPeriodID:

  if pState <> "ALL" and batch.stateID <> pState
   then next.
  if pStatus <> "A" and batch.stat <> pStatus 
   then next.

  assign
    pGrossReported = pGrossReported + batch.grossPremiumReported
    pNetReported = pNetReported + batch.netPremiumReported
    pGrossProcessed = pGrossProcessed + batch.grossPremiumProcessed
    pNetProcessed = pNetProcessed + batch.netPremiumProcessed
    pGrossRevenue = pGrossRevenue + batch.grossPremiumDelta
    pNetRevenue = pNetRevenue + batch.netPremiumDelta
    pBatches = pBatches + 1
    .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getPersonName) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPersonName Procedure 
PROCEDURE getPersonName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter  ipcPersonID as character no-undo.
  define output parameter  opcName     as character no-undo.
  define output parameter  oplSuccess  as logical   no-undo.

  /* Buffer */
  define buffer tcachePerson for tcachePerson.

  /* Client Server Call */
  if not can-find(first tcachePerson)
   then
    do:    
      run server/getPersons.p (output table tcachePerson,
                               output std-lo,
                               output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Load Persons failed: " std-ch
                 view-as alert-box error.
    end.

  find first tcachePerson where tcachePerson.personID = ipcPersonID no-error.
  if available tcachePerson
   then
    do:
      assign
          opcName      = tcachePerson.dispName
          oplSuccess   = true
          .
    end.    
  else
   oplSuccess = false.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getPersons) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getPersons Procedure 
PROCEDURE getPersons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter table for tcachePerson.
 
 /* Buffer */
  define buffer tcachePerson for tcachePerson.

  /* Client Server Call */
  if not loadPersons
   then
    do:    
      run server/getPersons.p (output table tcachePerson,
                               output std-lo,
                               output std-ch).
      if not std-lo 
       then 
        /* message not sent back to UI, so displayed here. */
        message "Load Persons failed: " std-ch
                 view-as alert-box error.
      
      loadPersons  = std-lo.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPolicies) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPolicies Procedure 
PROCEDURE GetPolicies :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAgentID as char.
 def input parameter pFileNumber as char.
 def input parameter pStartPolicy as int.
 def input parameter pEndPolicy as int.
 def input parameter pPolicyStat as char.
 def input parameter pStartDate as datetime.
 def input parameter pEndDate as datetime.
 def output parameter table for tempPolicy.
 
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
 
 empty temp-table tempPolicy.
 
 run server/getpolicies.p (pAgentID,
                       pFileNumber,
                       pStartPolicy,
                       pEndPolicy,
                       pPolicyStat,
                       pStartDate,
                       pEndDate,
                       output table tempPolicy,
                       output tSuccess,
                       output tMsg).
                       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPolicy Procedure 
PROCEDURE GetPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pPolicyID as int.
def output parameter table for tempPolicy.
def output parameter table for tempBatchForm.

 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
 
 empty temp-table tempPolicy.
 empty temp-table tempBatchForm.
 
 run server/getdetailspolicy.p (pPolicyID,
                         output table tempPolicy,
                         output table tempBatchForm,
                         output tSuccess,
                         output tMsg).
                       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPolicyhoi) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPolicyhoi Procedure 
PROCEDURE GetPolicyhoi :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for policyhoi.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetProcessingErrors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetProcessingErrors Procedure 
PROCEDURE GetProcessingErrors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def output parameter table for proerr.

  empty temp-table tempproerr.
  publish "GetSysCodes" ("processing", output table tempproerr).

  empty temp-table proerr.
  for each tempproerr no-lock:
    create proerr.
    assign
      proerr.err = tempproerr.code
      proerr.description = tempproerr.description
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getRevenues) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getRevenues Procedure 
PROCEDURE getRevenues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameter Definition */
  define output parameter table      for ttarrevenue.
  define output parameter oplSuccess as logical   no-undo.
  define output parameter opcMsg     as character no-undo.
  
  /* Buffer Definition */
  define buffer arrevenue   for arrevenue.
  define buffer ttarrevenue for ttarrevenue.
 
  empty temp-table ttarrevenue.
  
  if not can-find(first arrevenue)
   then
    do:
      run refreshRevenues (output oplSuccess,
                           output opcMsg)
                           .
      
      if not oplSuccess
       then
        return.
    end.

  for each arrevenue:
    create ttarrevenue.
    buffer-copy arrevenue to ttarrevenue.
  end.
 
  oplSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetServerBatches) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetServerBatches Procedure 
PROCEDURE GetServerBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def input parameter pMonth as int.
def input parameter pYear as int.
def output parameter table for tempbatch.

def buffer period for period.
def buffer tempbatch for tempbatch.
def buffer tempbatchform for tempbatchform.
def buffer tempbatchaction for tempbatchaction.

def var tSuccess as logical init false no-undo.
def var tMsg as char no-undo.

empty temp-table tempbatch.
empty temp-table tempbatchform.
empty temp-table tempbatchaction.

find period
  where period.periodMonth = pMonth
    and period.periodYear = pYear no-error.
if not available period then 
do:
  publish "DataError" (input "Period not found for " + string(pMonth) + "/" + string(pYear)).
  return.
end.

run server/getbatches.p (period.periodID,
                         0, /* BatchID (zero=All) */
                         output table tempbatch,
                         output table tempbatchaction,
                         output tSuccess,
                         output tMsg).
 
 if not tSuccess
  then 
   do: std-lo = false.
       publish "DataError" (input tMsg).
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "GetServerBatches failed: " tMsg view-as alert-box warning.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStatCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStatCodes Procedure 
PROCEDURE GetStatCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for statcode.

if not tLoadedStatCodes 
 then run LoadStatCodes in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStateForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateForms Procedure 
PROCEDURE GetStateForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def output parameter table for stateform.

if not tLoadedStateForms 
 then run LoadStateForms in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getSysCodeDescs) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysCodeDescs Procedure 
PROCEDURE getSysCodeDescs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter pCodeType    as character no-undo.
  define input  parameter pCode        as character no-undo.
  define output parameter opCodeDesc   as character no-undo.
  define output parameter oplSuccess   as logical   no-undo.
  define output parameter opcMsg       as character no-undo.

  std-ch = "".
  run getSysCodeListItems in this-procedure (input  pCodeType, 
                                             output std-ch,
                                             output oplSuccess,
                                             output opcMsg).

  if not oplSuccess 
   then
    return.

  if std-ch > ""
   then
    do:
      if lookup(pCode,std-ch) = 0 
       then
        opCodeDesc = pCode.
       else 
        opCodeDesc = entry(lookup(pCode,std-ch) - 1, std-ch) no-error.
    end.

  oplSuccess = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getSysCodeListItems) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysCodeListItems Procedure 
PROCEDURE getSysCodeListItems :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCodeType   as character no-undo.
  define output parameter pList       as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.
  define output parameter opcMsg      as character no-undo.
  
  define buffer syscode for syscode.

  if not can-find(first syscode where syscode.codeType = pCodeType)
   then
    do:
      run refreshSysCodes in this-procedure (input pCodeType,
                                             output oplSuccess,
                                             output opcMsg).
      if not oplSuccess
       then
        return.
    end.
   
  for each syscode no-lock
    where syscode.codeType = pCodeType
    by syscode.description:    
    pList = addDelimiter(pList,",") + syscode.description + "," + syscode.code.
  end.

  oplSuccess = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getSysCodess) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysCodess Procedure 
PROCEDURE getSysCodess :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input parameter  ipcType    as  character  no-undo.
  define output parameter table      for ttSysCode.
  define output parameter oplSuccess as  logical   no-undo.
  define output parameter opcMsg     as  character no-undo.  

  /* Buffers */
  define buffer sysCode   for sysCode.
  define buffer ttsysCode for ttsysCode.

  empty temp-table ttSysCode.
  
  publish "SetCurrentValue" ("RefreshCodes", false).

  /* Check if data is available in local cache. */
  if not can-find(first sysCode)
   then
    do:
      /* if not, then refresh the data from server. */
      run refreshSysCodes in this-procedure (input  ipcType,                                             
                                             output oplSuccess,
                                             output opcMsg).
      if not oplSuccess
       then
        return.
    end.
     
  /* Populate the return temp-table, so that data is returned to calling procedure.  */
  for each sysCode:
    create ttSysCode.
    buffer-copy sysCode to ttSysCode.    
  end.    

  oplSuccess = true.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getSysCodeTypeListItems) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getSysCodeTypeListItems Procedure 
PROCEDURE getSysCodeTypeListItems :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter opcCodeTypeList as character no-undo.
  define output parameter oplSuccess      as logical   no-undo.
  define output parameter opcMsg          as character no-undo.

  define buffer sysCode for sysCode.
  
  /* Check if data is available in local cache. */
  if not can-find(first sysCode)
   then
    do:
      /* if not, then refresh the data from server. */
      run refreshSysCodes in this-procedure (input  "",     /* Blank means for all codeType */                                             
                                             output oplSuccess,
                                             output opcMsg).
      if not oplSuccess
       then
        return.
    end.

  for each sysCode break by sysCode.codetype:
    if first-of(sysCode.codetype) 
     then
      opcCodeTypeList = opcCodeTypeList + "," + sysCode.codetype.    
  end.

  opcCodeTypeList = trim(opcCodeTypeList,",").
  oplSuccess = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStateIDList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateIDList Procedure 
PROCEDURE GetStateIDList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters */
  define input  parameter ipcDelimiter   as character             no-undo.
  define output parameter opcList        as character initial ""  no-undo.
  define output parameter oplSuccess     as logical               no-undo.
  define output parameter opcMsg         as character             no-undo.
    
  /* Buffers */
  define buffer tempstate      for tempstate.

  run getStates in this-procedure(output table tempstate). /* Not used here */
     
  for each state by state.stateId:
    opcList = opcList + ipcDelimiter + state.stateID.
  end.
  
  opcList = trim(opcList, ipcDelimiter).

  oplSuccess = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getStatesListItems) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getStatesListItems Procedure 
PROCEDURE getStatesListItems :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters: state <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters */
  define input  parameter ipcDelimiter as character             no-undo.
  define output parameter opcList      as character initial ""  no-undo.
  define output parameter oplSuccess   as logical               no-undo.
  define output parameter opcMsg       as character             no-undo.

  /* Buffers */
  define buffer tempstate      for tempstate.
  
  run getStates in this-procedure(output table tempstate). 

  for each tempstate by tempstate.stateId:
    opcList = opcList + ipcDelimiter + tempstate.description + ipcDelimiter + tempstate.stateid. 
  end.

  opcList = trim(opcList, ipcDelimiter).
  oplSuccess = true.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getStatusList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getStatusList Procedure 
PROCEDURE getStatusList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pAppCode  as character no-undo.
  define input  parameter pAction   as character no-undo.
  define input  parameter pProperty as character no-undo.
  define input  parameter pStat     as character no-undo.
  define output parameter pList     as character no-undo.

  define  variable chExcludeList as character   no-undo.
  
  /* buffers */
  define buffer tempsysprop for tempsysprop.

  /* get the system property */
  empty temp-table tempsysprop.
  run GetSysProp (pAppCode, pAction, pProperty, output table tempsysprop).

  case pStat:
    when "A" 
     then
      chExcludeList = "A,P,W".
    when "C" 
     then
      chExcludeList = "P,W,C,X,I".
    when "P" 
     then
      chExcludeList = "C,X,P,I".
    when "W" 
     then
      chExcludeList = "A,W,C,X,I".  
    when "X" 
     then
      chExcludeList = "P,W,C,X,I".       
    when "I" 
     then
      chExcludeList = "P,W,I".       
    otherwise   
      chExcludeList = "A,W,C,X".  
  end case.
 
  
  /* create a list of system properties */
  pList = "".
  for each tempsysprop no-lock
    where tempsysprop.appCode = pAppCode
      and tempsysprop.objAction = pAction
      and tempsysprop.objProperty = pProperty
      and not can-do(chExcludeList,tempsysProp.objID)
       by tempsysprop.objRef
       by tempsysprop.objID:
    
    pList = addDelimiter(pList, {&msg-dlm}) + tempsysprop.objValue {&msg-add} tempsysprop.objID.
  end.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-InvoiceBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InvoiceBatch Procedure 
PROCEDURE InvoiceBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batch for batch.

 run server/invoicebatch.p (pBatchID,
                            output pSuccess,
                            output pMsg).
 if not pSuccess
  then return.
                         
 find batch
   where batch.batchID = pBatchID no-error.
 if available batch 
  then batch.stat = "C".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IsPolicyValid) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IsPolicyValid Procedure 
PROCEDURE IsPolicyValid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAgentID as char.
 def input parameter pPolicyID as int.
 def output parameter pEffDate as datetime.
 def output parameter pLiabilityAmount as decimal.
 def output parameter pGrossPremium as decimal.
 def output parameter pFormID as char.
 def output parameter pStateID as char.
 def output parameter pFileNumber as char.
 def output parameter pStatCode as char.
 def output parameter pNetPremium as dec.
 def output parameter pRetention as dec.
 def output parameter pCountyID as char.
 def output parameter pResidential as log.
 def output parameter pInvoiceDate as date.
 def output parameter pStat as char.
 def output parameter pMsg as char.

 run server/canprocesspolicy.p (pAgentID,
                                pPolicyID,
                                output pEffDate,
                                output pLiabilityAmount,
                                output pGrossPremium,
                                output pFormID,
                                output pStateID,
                                output pFileNumber,
                                output pStatCode,
                                output pNetPremium,
                                output pRetention,
                                output pCountyID,
                                output pResidential,
                                output pInvoiceDate,
                                output pStat,
                                output std-lo,
                                output pMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-IssuePolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IssuePolicy Procedure 
PROCEDURE IssuePolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pPolicyID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/issuepolicy.p (pPolicyID,
                           ?, ?, ?, ?, ?, ?, ?,
                           output pSuccess,
                           output pMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadBatches) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadBatches Procedure 
PROCEDURE LoadBatches :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pPeriodID as int no-undo.

def buffer batch for batch.
def var tSuccess as logical init false no-undo.
def var tMsg as char no-undo.


empty temp-table batch.
empty temp-table batchform.
empty temp-table batchaction.
empty temp-table tempbatch.
empty temp-table tempbatchform.
empty temp-table tempbatchaction.

run server/getbatches.p (pPeriodID,
                         0, /* BatchID (zero=All) */
                         output table batch,
                         output table batchaction,
                         output tSuccess,
                         output tMsg).
 
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadBatches failed: " tMsg view-as alert-box warning.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadCPLs) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadCPLs Procedure 
PROCEDURE LoadCPLs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*DAS*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadDefaultEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadDefaultEndorsements Procedure 
PROCEDURE LoadDefaultEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as char no-undo.
 def buffer defaultform for defaultform.
 def var tMsg as char no-undo.
 
 empty temp-table defaultform.
 empty temp-table tempdefaultform.

 run server/getdefaultforms.p (output table defaultform,
                               output tLoadedDefaultEndorsements,
                               output tMsg).
                               
 if not tLoadedDefaultEndorsements
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadDefaultEndorsements failed: " tMsg view-as alert-box warning.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadHoldOpens) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadHoldOpens Procedure 
PROCEDURE LoadHoldOpens :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer holdopen for holdopen.
 def var tMsg as char no-undo.

 empty temp-table holdopen.

 run server/getholdopens.p (output table holdopen,
                            output tLoadedHoldOpens,
                            output tMsg).
 if not tLoadedHoldOpens
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadHoldOpens failed: " tMsg view-as alert-box warning.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadPolicies) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadPolicies Procedure 
PROCEDURE LoadPolicies :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*DAS*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadPolicyhoiID) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadPolicyhoiID Procedure 
PROCEDURE LoadPolicyhoiID :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pPolicyID as int no-undo.
def output parameter table for temppolicyhoi.
  
  
def buffer policyhoi for policyhoi.

  empty temp-table temppolicyhoi.
  for first policyhoi no-lock
      where policyhoi.policyID = pPolicyID:
      
      create temppolicyhoi.
      buffer-copy policyhoi to temppolicyhoi.
  end.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadStatCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadStatCodes Procedure 
PROCEDURE LoadStatCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer statcode for statcode.
 def var tMsg as char no-undo.

 empty temp-table statcode.
 empty temp-table tempstatcode.

 run server/getstatcodes.p ("", /* stateID, blank=All */
                            "", /* statcode, blank=All */
                            output table statcode,
                            output tLoadedStatCodes,
                            output tMsg).
 if not tLoadedStatCodes
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadStatCodes failed: " tMsg view-as alert-box warning.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadStateForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadStateForms Procedure 
PROCEDURE LoadStateForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer stateform for stateform.
 def var tMsg as char no-undo.
 
 empty temp-table stateform.
 empty temp-table tempstateform.
 
 run server/getstateforms.p ("", /* stateID, blank=All */
                             "", /* formID, blank=All */
                             output table stateform,
                             output tLoadedStateForms,
                             output tMsg).
 if not tLoadedStateForms
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadStateForms failed: " tMsg view-as alert-box warning.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LockBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockBatch Procedure 
PROCEDURE LockBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/lockbatch.p (pBatchID,
                     output pSuccess,
                     output pMsg).
 if not pSuccess
  then return.

 refreshBatch(pBatchID).
 refreshBatchActions(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-modifyAttorney) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyAttorney Procedure 
PROCEDURE modifyAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tattorney.
  define output parameter opcOrgID     as character no-undo.
  define output parameter opcPersonID  as character no-undo.
  define output parameter lSuccess     as logical   no-undo.
  define output parameter cMsg         as character no-undo.

  define variable cComStat         as character no-undo.
  define variable ipcAttorneyID    as character no-undo.
  

  define buffer attorney for attorney.

  /* Client Server Call */
  run server/modifyattorney.p (input table tattorney,
                               output opcOrgID,
                               output opcPersonID,
                               output lSuccess,
                               output cMsg).
  if not lSuccess 
   then
    return.
  
  for first tattorney:
    for first attorney where attorney.attorneyID = tattorney.attorneyID:
      buffer-copy tattorney to attorney.
      if tattorney.personID = "-99" 
       then
        do:
          tattorney.personID = opcPersonID.
          attorney.personID  = opcPersonID.
        end.
       if tattorney.orgID = "-99" 
         then
          do:
            tattorney.orgID  = opcOrgID.
            attorney.orgID   = opcOrgID.
          end.
       assign
            attorney.entity     = if (attorney.orgID ne "" and attorney.orgID ne ?) then {&OrganizationCode} else 
                                  if (attorney.personID ne "" and attorney.personID ne ?) then {&PersonCode} else ""
            attorney.entityID   = if (attorney.orgID ne "" and attorney.orgID ne ?) then attorney.orgID  else
                                  if (attorney.personID ne "" and attorney.personID ne ?) then attorney.personID else ""
            attorney.entityName = if attorney.entity = {&OrganizationCode} then attorney.firmname else 
                                  if attorney.entity = {&PersonCode}       then attorney.name1 else (if (attorney.name1 ne "" and attorney.name1 ne ?) then attorney.name1                                                                                               else attorney.firmname)
             .
    end.
  end.

  empty temp-table tattorney.
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyBatch Procedure 
PROCEDURE ModifyBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pDateRcvd as datetime.
 def input parameter pAgentID as char.
 def input parameter pGross as deci.
 def input parameter pNet as deci.
 def input parameter pCash as deci.
 def input parameter pRcvdVia as char.
 def input parameter pReference as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer tempbatch for tempbatch.
 
 find batch
   where batch.batchID = pBatchID no-error.
 if not available batch
  then
   do: pSuccess = false.
       pMsg = "Batch " + string(pBatchID) + " does not exist.".
       return.
   end.
   
 empty temp-table tempbatch.
 
 create tempbatch.
 buffer-copy batch to tempbatch.
 
 assign
   tempbatch.agentID = pAgentID
   tempbatch.reference = pReference
   tempbatch.receivedDate = pDateRcvd
   tempbatch.grossPremiumReported = pGross
   tempbatch.netPremiumReported = pNet
   tempbatch.cashReceived = pCash
   tempbatch.rcvdVia = pRcvdVia
   .
   
 run server/modifybatch.p (input-output table tempbatch,
                           output pSuccess,
                           output pMsg).
 if not pSuccess
  then return.

 refreshBatch(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyBatchForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyBatchForm Procedure 
PROCEDURE ModifyBatchForm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pSeq as int.
 
 def input parameter pFormType as char.
 def input parameter pFileNumber as char.
 def input parameter pPolicyID as int.
 def input parameter pFormID as char.
 def input parameter pFormCount as int.
 def input parameter pRateCode as char.
 def input parameter pStatCode as char.
 def input parameter pEffDate as datetime.
 def input parameter pLiabilityAmount as decimal.
 def input parameter pCountyID as char.
 def input parameter pResidential as logical.
 def input parameter pGrossPremium as decimal.
 def input parameter pNetPremium as decimal.
 def input parameter pZipCode as char.
 def output parameter pRevenueType as character.
 def output parameter pRedo as logical.

 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batchform for batchform.
 def buffer tempbatchform for tempbatchform.

 find batchform 
   where batchform.batchID = pBatchID 
     and batchform.seq = pSeq no-error.
 if not available batchform 
  then
   do: pMsg = "Form with sequence " + string(pSeq) + " does not exist".
       return.
   end.

 empty temp-table tempbatchform.
 create tempbatchform.
 buffer-copy batchform to tempbatchform.
 assign
    tempbatchform.formType = pFormType
    tempbatchform.fileNumber = pFileNumber
    tempbatchform.policyID = pPolicyID
    tempbatchform.formID = pFormID
    tempbatchform.formCount = pFormCount
    tempbatchform.rateCode = pRateCode
    tempbatchform.statCode = pStatCode
    tempbatchform.effDate = pEffDate
    tempbatchform.liabilityAmount = pLiabilityAmount
    tempbatchform.countyID = pCountyID
    tempbatchform.residential = pResidential
    tempbatchform.grossPremium = pGrossPremium
    tempbatchform.netPremium = pNetPremium
    tempbatchform.zipcode = pZipCode
    .
   
 run server/modifybatchform.p (input-output table tempbatchform,
                               output pSuccess,
                               output pMsg).
 
 if not pSuccess
  then return.

 find tempbatchform
   where tempbatchform.batchID = pBatchID
     and tempbatchform.seq = pSeq.
 buffer-copy tempbatchform to batchform.
 pRevenueType = tempbatchform.revenueType.  
 
 refreshBatch(pBatchID).
 
 PUBLISH "BatchTotalsChanged".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyPolicyHoi) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyPolicyHoi Procedure 
PROCEDURE ModifyPolicyHoi :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for temppolicyhoi.
  define output parameter pSuccess as log no-undo.
  define output parameter pMsg as char no-undo.

  run server/modifypolicyhoi.p(input table temppolicyhoi,
                               output pSuccess,
                               output pMsg).

  if not pSuccess and pMsg > "" 
   then message pMsg view-as alert-box error.
   else
    for first temppolicyhoi no-lock:
      for first policyhoi exclusive-lock
          where policyhoi.policyID = temppolicyhoi.policyID:
        
        buffer-copy temppolicyhoi to policyhoi.
      end.
      publish "PolicyHOIChanged".
    end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-modifyRevenue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyRevenue Procedure 
PROCEDURE modifyRevenue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input parameter table       for ttarrevenue.
  define output parameter oplSuccess as logical   no-undo.
  define output parameter opcMsg     as character no-undo.

  define variable dtModifyDate as datetime  no-undo.
  
  define buffer ttarrevenue for ttarrevenue.

  /* check for availability of record before making the server call. */
  if not can-find (first ttarrevenue)
   then 
    return.

  /* Client Server Call */
  run server/modifyrevenue.p (input table ttarrevenue,
                              output dtModifyDate,
                              output oplSuccess,
                              output opcMsg).
 
  if not oplSuccess 
   then
    return.

  /* Caching of data */
  for first ttarrevenue:
    find first arrevenue where 
               arrevenue.revenueType = ttarrevenue.revenueType no-error.
    if not available arrevenue
     then
      do:
        assign 
            opcMsg     = "Internal Error of missing data."  
            oplSuccess = false
            . 
        return.
      end.
    
    buffer-copy ttarrevenue except ttarrevenue.createddate ttarrevenue.createdby 
    ttarrevenue.cUsername ttarrevenue.modifiedDate ttarrevenue.modifiedBy ttarrevenue.mUsername to arrevenue.
    assign
        arrevenue.modifiedDate = dtModifyDate
        arrevenue.modifiedBy   = tAccountUser
        arrevenue.mUsername    = cCurrentUser
        .
    
  end. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyStatCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyStatCode Procedure 
PROCEDURE ModifyStatCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStateID as char.
 def input parameter pStatCode as char.
 def input parameter pFormID as char.
 def input parameter pDescription as char.
 def input parameter pRegulation as char.
 def input parameter pActive as logical.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer statcode for statcode.

 run server/modifystatcode.p (pStateID,
                              pStatCode,
                              pFormID,
                              pDescription,
                              pRegulation,
                              pActive,
                              output pSuccess,
                              output pMsg).
 
 if not pSuccess
  then return.

 refreshStatCode(pStateID, pStatCode).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyStateForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyStateForm Procedure 
PROCEDURE ModifyStateForm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStateID as char.
 def input parameter pFormID as char.
 def input parameter pDescription as char.
 def input parameter pFormCode as char.
 def input parameter pActive as logical.
 def input parameter pRateCheck as char.
 def input parameter pRateMin as decimal.
 def input parameter pRateMax as decimal.
 def input parameter pOrgName as char.
 def input parameter pOrgRev as char.
 def input parameter pOrgRel as datetime.
 def input parameter pOrgEff as datetime.
 def input parameter pRevType as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer stateform for stateform.

 run server/modifystateform.p (pStateID,
                               pFormID,
                               pDescription,
                               pFormCode,
                               pActive,
                               pRateCheck,
                               pRateMin,
                               pRateMax,
                               pOrgName,
                               pOrgRev,
                               pOrgRel,
                               pOrgEff,
                               pRevType,
                               output pSuccess,
                               output pMsg).
                           
 if not pSuccess
  then return.
          
 refreshStateForm(pStateID, pFormID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-modifySysCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifySysCode Procedure 
PROCEDURE modifySysCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter table for ttSysCode.

  define output parameter oplSuccess  as logical    no-undo.
  define output parameter opcMsg      as character  no-undo.

  define buffer sysCode   for sysCode.
  define buffer ttSysCode for ttSysCode.

  /* check for availability of record before making the server call. */
  find first ttSysCode no-error.
  if not available ttSysCode
   then
    return.

  /* Client Server Call */
  run server/modifysyscode.p (input  table ttSysCode,
                              output oplSuccess,
                              output opcMsg).

  if not oplSuccess
   then
    return.

  find first sysCode where sysCode.codeType = ttSysCode.codeType 
                       and sysCode.code     = ttSysCode.code no-error.
  if not available sysCode
   then
    do: 
      assign 
          opcMsg     = "Internal Error of missing data."  
          oplSuccess = false
          . 
      empty temp-table ttSysCode.
      return.
    end.

  buffer-copy ttSysCode to sysCode.

  empty temp-table ttSysCode.
 
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MoveBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MoveBatch Procedure 
PROCEDURE MoveBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pDirection as char.
 def input parameter pComments as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 def output parameter pNewBatchID as int.

 def buffer batch for batch.

 def var tUser as char no-undo.
 def var tFile as char no-undo.
 
 define variable tNewYear  as integer no-undo.
 define variable tNewMonth as integer no-undo.
 
 run server/movebatch.p (pBatchID,
                         pDirection,
                         pComments,
                         output pNewBatchID,
                         output pSuccess,
                         output pMsg).
 if not pSuccess
  then return.
  
 refreshBatch(pBatchID).
 refreshBatchActions(pBatchID).

 run server/getarcrepositoryfiles.p ("Batch", pBatchID, false, output table batchitem, output std-lo, output std-ch).
 if not std-lo
  then return.
 
 for each batchitem no-lock:
  /* we need to download the file first then upload */
  tFile = batchitem.displayName + "." + batchitem.filetype.
  run server/downloadarcrepositoryfile.p (batchitem.identifier, input-output tFile, output std-lo, output std-ch).
  run server/uploadarcrepositoryfile.p (tAccountUser, false, "Batch", pNewBatchID, batchitem.category, tFile, output table tempbatchitem, output pSuccess, output std-ch).
  
  if pSuccess
   then
    do:
     run server/unlinkdocument.p ("Batch", string(pBatchID), 0, output std-lo, output std-ch).
     run server/linkdocument.p ("Batch", string(pNewBatchID), 0, output std-lo, output std-ch).
    end.
 end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-MoveDefaultEndorsement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE MoveDefaultEndorsement Procedure 
PROCEDURE MoveDefaultEndorsement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pIdType as char.      /* A)gent or S)tate */
 def input parameter pId as char.          /* AgentID or StateID */
 def input parameter pInsuredType as char. /* O)wner or L)ender */
 def input parameter pSeq as int.
 def input parameter pDirection as char.   /* UP or DOWN */
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/movedefaultform.p (pIdType,
                               pId,
                               pInsuredType,
                               pSeq,
                               pDirection,
                               output pSuccess,
                               output pMsg).
                          
 refreshDefaultEndorsements().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newAttorney) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newAttorney Procedure 
PROCEDURE newAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tAttorney.
  define output parameter opcAttorneyID as character no-undo.
  define output parameter opcOrgID      as character no-undo.
  define output parameter opcPersonID   as character no-undo.
  define output parameter oplSuccess    as logical   no-undo.
  define output parameter opcMsg        as character no-undo.

  define variable cComStatus   as character no-undo.
  define variable dtCreateDate as datetime  no-undo.

  define buffer attorney  for attorney.
  define buffer tAttorney for tAttorney.

  /*Checking availability of record before making the server call. */
  if not can-find (first tAttorney)
   then
    return.

  /* Server Call */
  run server/newattorney.p (input  table tAttorney,
                            output       opcAttorneyID,
                            output       opcOrgID,
                            output       opcPersonID,
                            output       cComStatus,
                            output       dtCreateDate,
                            output       oplSuccess,
                            output       opcMsg).

  if not oplSuccess
   then
    return.

  /* Caching of data */
  for each tAttorney:
    create attorney.
    buffer-copy tAttorney except tAttorney.attorneyID to attorney.
    assign
        attorney.attorneyID = opcAttorneyID
        attorney.comstat    = cComStatus
        attorney.createDate = dtCreateDate
        attorney.entity     = if (tAttorney.orgID ne "" and tAttorney.orgID ne ?) then {&OrganizationCode}  else 
                              if (tAttorney.personID ne "" and tAttorney.personID ne ?) then {&PersonCode} else ""
        attorney.entityID   = if (tAttorney.orgID ne "" and tAttorney.orgID ne ?) then tAttorney.orgID else
                              if (tAttorney.personID ne "" and tAttorney.personID ne ?) then tAttorney.personID else ""
        attorney.entityName = if attorney.entity = {&OrganizationCode} then attorney.firmname else 
                              if attorney.entity = {&PersonCode}       then attorney.name1 else (if (attorney.name1 ne "" and attorney.name1 ne ?) then attorney.name1   
                                                                                                 else attorney.firmname)
        .
    case attorney.stat:
      when {&ActiveStat}    then attorney.activeDate    = dtCreateDate.
      when {&ProspectStat}  then attorney.prospectDate  = dtCreateDate.
      when {&ClosedStat}    then attorney.closedDate    = dtCreateDate.
      when {&CancelledStat} then attorney.cancelDate    = dtCreateDate.
      when {&WithdrawnStat} then attorney.withdrawnDate = dtCreateDate.
    end case.
    
    if tAttorney.orgID ne ""
     then 
      do:
        find first organization where organization.orgID = tattorney.orgID no-lock no-error.
         if avail organization 
          then
           assign attorney.addr1   = organization.addr1
                  attorney.addr2   = organization.addr2
                  attorney.city    = organization.city
                  attorney.state   = organization.state
                  attorney.zip     = organization.zip
                  attorney.phone   = organization.phone
                  attorney.website = organization.website
                  attorney.state   = organization.state
                  .
      end.

    if opcOrgID <> "" and opcOrgID <> ?
     then
      attorney.orgID    = opcOrgID.
    else if opcPersonID <> "" and opcPersonID <> ? then
      attorney.personID = opcPersonID.
      
    assign  
        attorney.entityID   = if (attorney.orgID ne "" and attorney.orgID ne ?) then attorney.orgID else
                              if (attorney.personID ne "" and attorney.personID ne ?) then attorney.personID else "".
  end.
  empty temp-table tattorney.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewBatch Procedure 
PROCEDURE NewBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter pMonth as int.
  def input parameter pYear as int.
  def input parameter pDateRcvd as datetime.
  def input parameter pAgentID as char.
  def input parameter pGross as deci.
  def input parameter pNet as deci.
  def input parameter pCash as deci.
  def input parameter pRcvdVia as char.
  def input parameter pAttachment as char.
  def input parameter pReference as char.
  def output parameter pSuccess as logical init false.
  def output parameter pMsg as char.
  def output parameter pBatchID as int.
  
  def buffer batch for batch.
  def buffer tempbatch for tempbatch.
  
  def var tFileID as char no-undo.
  def var tNewDir as char no-undo.
  def var tNumTasks as int no-undo.
  
  empty temp-table tempbatch no-error.
  create tempbatch.
  assign   
    tempbatch.stat = "N"
    tempbatch.processStat = "B" /* default to B)lack */
  /*tempbatch.periodID = period.periodID*/
    tempbatch.periodMonth = pMonth
    tempbatch.periodYear = pYear
    tempbatch.agentID = pAgentID
    tempbatch.reference = pReference
    tempbatch.createDate = today
    tempbatch.receivedDate = pDateRcvd
    tempbatch.grossPremiumReported = pGross
    tempbatch.netPremiumReported = pNet
    tempbatch.cashReceived = pCash
    tempbatch.rcvdVia = pRcvdVia
    .
  
  tNumTasks = if search(pAttachment) <> ? then 3 else 2.
 
  {lib/pbshow.i "'Creating batch, please wait...'"}
  {lib/pbupdate.i "'Creating new batch...'" 0}
 
  run server/newbatch.p (input-output table tempbatch,
                         output pBatchID,
                         output pSuccess,
                         output pMsg).

  if not pSuccess then
  do:
    {lib/pbhide.i}
    return.
  end.

  {lib/pbupdate.i "'Retrieving new batch ' + string(pBatchID) + '...'" "integer((1 / tNumTasks) * 100)"}
  
  refreshBatch(pBatchID).

  find batch where batch.batchID = pBatchID. 

  if search(pAttachment) <> ? 
   then
    do:
      {lib/pbupdate.i "'Uploading attachment for batch ' + string(pBatchID) + '...'" "integer((1 / tNumTasks) * 100)"}
       
      run server/uploadarcrepositoryfile.p (tAccountUser, true, "Batch", pBatchID, pReference, search(pAttachment), output table batchitem, output std-lo, output std-ch).
       
      if std-lo
       then
        do:
             run server/linkdocument.p ("Batch",
                                        string(pBatchID),
                                        0,
                                        output std-lo,
                                        output std-ch).
              if std-lo 
               then batch.hasDocument = true.
         end.
        else 
        do:
          session:set-wait-state("").
          MESSAGE std-ch
               VIEW-AS ALERT-BOX error BUTTONS OK title "Unable to Save Attachment".
        end.
   end.

 {lib/pbhide.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewBatchAction) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewBatchAction Procedure 
PROCEDURE NewBatchAction :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pComments as char.
 def input parameter pFlag as char.
 def input parameter pSecure as logical.
 def output parameter pSeq as int.
 def output parameter pBatchChanged as log.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/newbatchaction.p (pBatchID,
                          pComments,
                          pFlag,
                          pSecure,
                          output pSeq,
                          output pBatchChanged,
                          output pSuccess,
                          output pMsg).
 if not pSuccess
  then return.
  
 refreshBatchActions(pBatchID).

 if pBatchChanged
  then do:
    refreshBatch(pBatchID).
    publish "BatchChanged".
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewBatchForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewBatchForm Procedure 
PROCEDURE NewBatchForm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pFormType as char.
 def input-output parameter pFileNumber as char.
 
 def input parameter pPolicyID as int.
 def input parameter pRateCode as char.
 def input-output parameter pFormID as char.
 def input parameter pFormCount as int.
 def input-output parameter pStatCode as char.
 
 def input-output parameter pEffDate as datetime.
 def input parameter pLiabilityAmount as decimal.
 
 def input-output parameter pCountyID as char.
 def input parameter pResidential as logical.

 def input parameter pGrossPremium as decimal.
 def input parameter pNetPremium as decimal.
 def input parameter pZipCode as char.
 
 def output parameter pRevenueType as character.
 def output parameter pSeq as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 def output parameter pRedo as logical.

 def var tAlertMsg as char no-undo.

 def buffer batchform for batchform.
 def buffer tempbatchform for tempbatchform.
 

 /* The 3 values that are visualized by a selection list may need to be blanked */
 if pCountyID = ?
  then pCountyID = "".
 if pStatCode = ? 
  then pStatCode = "".
 if pFormID = ? 
  then pFormID = "".
 
 empty temp-table tempbatchform.
 create tempbatchform.
 assign
   tempbatchform.batchID = pBatchID
   tempbatchform.formType = pFormType
   tempbatchform.fileNumber = pFileNumber
   tempbatchform.policyID = pPolicyID
   tempbatchform.rateCode = pRateCode
   tempbatchform.formID = pFormID
   tempbatchform.statCode = pStatCode
   tempbatchform.effDate = pEffDate
   tempbatchform.liabilityAmount = pLiabilityAmount
   tempbatchform.countyID = pCountyID
   tempbatchform.residential = pResidential
   tempbatchform.grossPremium = pGrossPremium
   tempbatchform.netPremium = pNetPremium
   tempbatchform.zipcode = pZipCode
   tempbatchform.formCount = pFormCount
   .
   
 run server/newbatchform.p (input-output table tempbatchform,
                            output pSeq,
                            output tAlertMsg,
                            output pSuccess,
                            output pMsg).
 if not pSuccess
  then return.                       

 if tAlertMsg <> "" then
 message tAlertMsg view-as alert-box warning.

 refreshBatch(pBatchID).
 
 find first tempbatchform
   where tempbatchform.batchID = pBatchID
     and tempbatchform.seq = pSeq.
 create batchform.
 buffer-copy tempbatchform to batchform.
      
 assign
   pFileNumber = batchform.fileNumber
   pFormID = batchform.formID
   pStatCode = batchform.statCode
   pEffDate = batchform.effDate
   pCountyID = batchform.countyID
   pRedo = batchform.reprocess
   pRevenueType = batchform.revenueType
   .      
                        
 PUBLISH "BatchTotalsChanged".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewDefaultEndorsement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewDefaultEndorsement Procedure 
PROCEDURE NewDefaultEndorsement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pIdType as char.      /* A)gent or S)tate */
 def input parameter pId as char.          /* AgentID or StateID */
 def input parameter pInsuredType as char. /* O)wner or L)ender */
 def input parameter pFormID as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer defaultform for defaultform.
 def var tSeq as int.

 run server/newdefaultform.p (pIdType,
                              pId,
                              pInsuredType,
                              pFormID,
                              output tSeq,
                              output pSuccess,
                              output pMsg).
 if not pSuccess
  then next.                        

 refreshDefaultEndorsements().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewHoldOpenPolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewHoldOpenPolicy Procedure 
PROCEDURE NewHoldOpenPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pAgentID as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 def output parameter pPolicyID as int.
 
 def var tNumTasks as int no-undo.

 tNumTasks = 1.
 
 run sys/progressbar.w persistent ("Creating hold open policy, please wait...").
 publish "pbUpdateStatus" ("Creating new hold open policy...", 0, 0).
 
 run server/newholdopen.p (input pAgentID,
                           output pPolicyID,
                           output pSuccess,
                           output pMsg).

 if not pSuccess then
 do:
  publish "pbCloseWindow".
  return.
 end.

 publish "pbUpdateStatus" ("Hold open creation complete for policy " + string(pPolicyID) + ".", 100, 1).
 publish "pbCloseWindow".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newOrganizationAgent) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newOrganizationAgent Procedure 
PROCEDURE newOrganizationAgent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tOrganization.
  
  for first tOrganization:
    find first organization 
      where organization.OrgId = torganization.orgID no-error.
    if not available organization
     then
      do:
        create organization.
        buffer-copy torganization to organization.
      end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newOrganizationAttorney) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newOrganizationAttorney Procedure 
PROCEDURE newOrganizationAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tcacheOrganization.
  
  for first tcacheOrganization:
    find first organization 
      where organization.orgId = tcacheOrganization.orgID no-error.
    if not available organization
     then
      do:
        create organization.
        buffer-copy tcacheOrganization to organization.
      end.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newPersonAttorney) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newPersonAttorney Procedure 
PROCEDURE newPersonAttorney :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tPerson.
  
  for first tPerson:
    find first tcachePerson 
      where tcachePerson.personId = tperson.personID no-error.
    if not available tcachePerson
     then
      do:
        create tcachePerson.
        buffer-copy tperson to tcachePerson.
      end.
  end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewPolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPolicy Procedure 
PROCEDURE NewPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pPolicyID as int.
 def input parameter pAgentID as char.
 def input parameter pFormID as char.
 def input parameter pFileNumber as char.
 def input parameter pLiability as decimal.
 def input parameter pResidential as logical.
 def input parameter pGrossPremium as decimal.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer temppolicy for temppolicy.
 
 empty temp-table temppolicy.
 create temppolicy.
 assign
   temppolicy.policyID = pPolicyID
   temppolicy.agentID = pAgentID
   temppolicy.formID = pFormID
   temppolicy.fileNumber = pFileNumber
   temppolicy.liability = pLiability
   temppolicy.residential = pResidential
   temppolicy.grossPremium = pGrossPremium
   .
   
 run server/newpolicy.p (input-output table temppolicy,
                         output pSuccess,
                         output pMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewPolicyHoi) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPolicyHoi Procedure 
PROCEDURE NewPolicyHoi :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for temppolicyhoi.
  define output parameter std-lo as log no-undo.

  run server/newpolicyhoi.p (input table temppolicyhoi,
                             output std-lo,
                             output std-ch).

  if not std-lo 
   then message std-ch view-as alert-box error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newRevenue) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newRevenue Procedure 
PROCEDURE newRevenue :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameter Definition */
  define input  parameter table      for ttarrevenue.
  define output parameter oplSuccess as logical   no-undo.
  define output parameter opcMsg     as character no-undo.
  
  define variable dtCreateDate as datetime  no-undo.

  define buffer ttarrevenue for ttarrevenue.

  /* client Server Call */
  run server/newrevenue.p (input table ttarrevenue,
                           output dtCreateDate,
                           output oplSuccess,
                           output opcMsg).
 
  if not oplSuccess 
   then
    return.
 
  /* Caching of data */
  for first ttarrevenue:
    create arrevenue.
    buffer-copy ttarrevenue except ttarrevenue.createddate ttarrevenue.createdby ttarrevenue.cUsername to arrevenue.
    assign
        arrevenue.createddate = dtCreateDate
        arrevenue.createdby   = tAccountUser
        arrevenue.cUsername   = cCurrentUser
        .
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewStatCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewStatCode Procedure 
PROCEDURE NewStatCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStateID as char.
 def input parameter pStatCode as char.
 def input parameter pFormID as char.
 def input parameter pDescription as char.
 def input parameter pRegulation as char.
 def input parameter pType as char.
 def input parameter pActive as logical.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer statcode for statcode.
 def buffer stateform for stateform.

 run server/newstatcode.p (pStateID,
                           pStatCode,
                           pFormID,
                           pDescription,
                           pRegulation,
                           pType,
                           pActive,
                           output pSuccess,
                           output pMsg).
 if not pSuccess
  then return.
                        
 refreshStatCode(pStateID, pStatCode).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewStateForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewStateForm Procedure 
PROCEDURE NewStateForm :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pStateID as char.
 def input parameter pFormID as char.
 def input parameter pDescription as char.
 def input parameter pFormCode as char.
 def input parameter pType as char.
 def input parameter pInsuredType as char.
 def input parameter pActive as logical.
 def input parameter pRateCheck as char.
 def input parameter pRateMin as decimal.
 def input parameter pRateMax as decimal.
 def input parameter pOrgName as char.
 def input parameter pOrgRev as char.
 def input parameter pOrgRel as datetime.
 def input parameter pOrgEff as datetime.
 def input parameter pRevType as character.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 
 def buffer stateform for stateform.

 run server/newstateform.p (pStateID,
                            pFormID,
                            pDescription,
                            pFormCode,
                            pType,
                            pInsuredType,
                            pActive,
                            pRateCheck,
                            pRateMin,
                            pRateMax,
                            pOrgName,
                            pOrgRev,
                            pOrgRel,
                            pOrgEff,
                            pRevType,
                            output pSuccess,
                            output pMsg).
 if not pSuccess
  then return.
                         
 refreshStateForm(pStateID, pFormID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-newSysCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newSysCode Procedure 
PROCEDURE newSysCode :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input  parameter table for ttSysCode.
  
  define output parameter oplSuccess   as logical    no-undo.
  define output parameter opcMsg       as character  no-undo.  

  define buffer sysCode   for sysCode.
  define buffer ttSysCode for ttSysCode.
 
  /* check for availability of record before making the server call. */
  find first ttSysCode no-error.
  if not available ttSysCode
   then
    return.

  /* Server Call */
  run server/newsyscode.p (input table ttSysCode,                               
                           output oplSuccess,
                           output opcMsg).

  if not oplSuccess
   then
    return.
  
  /* Caching of data */
  create sysCode.
  buffer-copy ttSysCode to sysCode.  

  empty temp-table ttSysCode.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-PurgeAllBatchForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PurgeAllBatchForms Procedure 
PROCEDURE PurgeAllBatchForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batch for batch.
 def buffer tempbatch for tempbatch.
 def buffer batchaction for batchaction.
 def buffer tempbatchaction for tempbatchaction.
 
 empty temp-table tempbatch.
 empty temp-table tempbatchaction.
 
 run server/purgeallbatchforms.p (pBatchID,
                                  output pSuccess,
                                  output pMsg).
 if not pSuccess
  then return.

 refreshBatch(pBatchID).
 refreshBatchActions(pBatchID).
 
 PUBLISH "BatchTotalsChanged".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ReassignPolicies) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReassignPolicies Procedure 
PROCEDURE ReassignPolicies :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pComments as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/reassignbatchpolicies.p (pBatchID,
                                     pComments,
                                     output pSuccess,
                                     output pMsg).

 if not pSuccess
  then return.

 refreshBatch(pBatchID). 
 refreshBatchActions(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshRevenues) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshRevenues Procedure 
PROCEDURE refreshRevenues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter std-lo  as logical    no-undo.
  define output parameter std-ch  as character  no-undo.

  define buffer ttarrevenue for ttarrevenue.

  /* Client Server Call */
  run server/getrevenues.p (input  "ALL",  /* RevenueType */
                            output table ttarrevenue,
                            output std-lo,
                            output std-ch).
 
  if not std-lo
   then
    return.

  empty temp-table arrevenue.
 
  for each ttarrevenue:
    create arrevenue.
    buffer-copy ttarrevenue to arrevenue.
  end.

  empty temp-table ttarrevenue.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshSysCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshSysCodes Procedure 
PROCEDURE refreshSysCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define input parameter ipcType     as character  no-undo.  
  
  define output parameter oplSuccess as logical    no-undo.
  define output parameter opcMsg     as character  no-undo.
  
  define buffer sysCode  for sysCode.
  define buffer ttSysCode for ttSysCode.
  
  empty temp-table ttSysCode.

  /* Get the records in temp-table, so that in case any error comes, then the local data instance is not deleted. */
  /* client Server Call */
  run server/getSysCodes.p (input ipcType, 
                            output table ttSysCode,   /* gets this in temp table */
                            output oplSuccess,
                            output opcMsg).
  if not oplSuccess 
   then
    return.
  
  publish "SetCurrentValue" ("RefreshCodes", true).

  empty temp-table sysCode.
  /* Update the local copy of data. */ 
  for each ttSysCode :
    create sysCode.
    buffer-copy ttSysCode to sysCode.
  end.

  empty temp-table ttSysCode.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshAttorneys) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAttorneys Procedure 
PROCEDURE refreshAttorneys :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* Parameters Definition */
  define output parameter oplSuccess as logical    no-undo.
  define output parameter opcMsg     as character  no-undo.
  
  define buffer attorney  for attorney.
  define buffer tAttorney for tAttorney.

  empty temp-table tAttorney.
  
  /* Get the records in temp-table, so that in case any error comes, then the local data instance is not deleted. */
  /* client Server Call */
  run server/getAttorneys.p (input  "" ,   /* stateID */
                             input "" ,  /* Person  */
                             output table tAttorney,
                             output oplSuccess,
                             output opcMsg).
 
  if not oplSuccess 
   then
    return.
  
 /* publish "SetCurrentValue" ("RefreshActions", true). */

  empty temp-table attorney.

  /* Update the local copy of data. */  
  for each tAttorney:
    create attorney.
    buffer-copy tAttorney to attorney.
    assign
             attorney.entity     = if (tAttorney.orgID ne "" and tAttorney.orgID ne ?) then {&OrganizationCode} else 
                                   if (tAttorney.personID ne "" and tAttorney.personID ne ?) then {&PersonCode} else ""
             attorney.entityID   = if (tAttorney.orgID ne "" and tAttorney.orgID ne ?) then tAttorney.orgID  else
                                   if (tAttorney.personID ne "" and tAttorney.personID ne ?) then tAttorney.personID else ""
             attorney.entityName = if (tAttorney.firmname ne "" and tAttorney.firmname ne ?) then tAttorney.firmname   else tAttorney.name1
             .
  end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SearchBatchForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SearchBatchForms Procedure 
PROCEDURE SearchBatchForms :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input param pBatchID as int.
def input param pSearchFile as char.
def input param pSearchPolicy as char.
def input param pSearchErrors as char.
def input param pSearchCondition as char.
def output param pZipCode as char.
def output param table for tempBatchForm.

def buffer batchform for batchform.
def buffer tempBatchForm for tempBatchForm.

def var tSuccess as logical no-undo.
def var tMsg as char no-undo.
 
empty temp-table tempbatchform.

if pBatchID = 0 or pBatchID = ? 
then return.
 
run server/searchbatchforms.p (pBatchID,
                               pSearchFile,
                               pSearchPolicy,
                               pSearchErrors,
                               pSearchCondition,
                               output pZipCode, 
                               output table tempbatchform,
                               output tSuccess,
                               output tMsg).
if not tSuccess then 
do: 
  std-lo = false.
  
  publish "DataError" (input tMsg).
  publish "GetAppDebug" (output std-lo).
  
  if std-lo then 
  message "Error: " tMsg view-as alert-box warning.
  
  return.
end.
 
for each batchform where batchform.batchID = pBatchID:
  delete batchform.
end.

for each tempbatchform where tempbatchform.batchID = pBatchID:
  create batchform.
  buffer-copy tempbatchform to batchform.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SearchPolicyHOI) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SearchPolicyHOI Procedure 
PROCEDURE SearchPolicyHOI :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter tStateID as char.
  def input parameter tBeginPeriod as int.
  def input parameter tEndPeriod as int.
  define output parameter table for policyhoi.

  empty temp-table policyhoi.
  run server/getpolicyhoi.p (0, /* 0 for  All home issued policies */
                             tStateID,
                             tBeginPeriod,
                             tEndPeriod,
                             output table policyhoi,
                             output tLoadedPolicyhoi,
                             output std-ch).


  if not tLoadedPolicyhoi
   then 
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).
      if std-lo 
       then message "SearchPolicyhoi failed: " std-ch view-as alert-box warning.
    end.
   else
    for each policyhoi exclusive-lock:
      publish "GetSysPropDesc" ("OPS", "DIPStatus", "DIPStatus", policyhoi.DIPStatus, output policyhoi.DIPStatusDesc).
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-setAttorneyStatus) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setAttorneyStatus Procedure 
PROCEDURE setAttorneyStatus :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcAttorneyID  as character no-undo.
  define input  parameter ipcStatus      as character no-undo.
  define output parameter lSuccess       as logical   no-undo.

  define variable cMsg         as character no-undo.
  define variable dtStatusDate as datetime no-undo.

  /* client Server Call */
  run server/changeattorneystatus.p (input  ipcAttorneyID,
                                     input  ipcStatus,
                                     output dtStatusDate,
                                     output lSuccess,
                                     output cMsg).
 
  if not lSuccess 
   then 
    message cMsg view-as alert-box info buttons ok.
  else
   for first attorney where attorney.attorneyID = ipcAttorneyID : 
     attorney.stat = ipcStatus.

     case attorney.stat:
       when {&ActiveStat}    then attorney.activeDate    = dtStatusDate.
       when {&ProspectStat}  then attorney.prospectDate  = dtStatusDate.
       when {&ClosedStat}    then attorney.closedDate    = dtStatusDate.
       when {&CancelledStat} then attorney.cancelDate    = dtStatusDate.
       when {&WithdrawnStat} then attorney.withdrawnDate = dtStatusDate.
     end case.
     
     publish "AttorneyChanged" (ipcAttorneyID).   
   end.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLastSync) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLastSync Procedure 
PROCEDURE SetLastSync :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  tLastSync = now.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SplitBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SplitBatch Procedure 
PROCEDURE SplitBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def input parameter pComments as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 def output parameter pNewBatchID as int.

 def buffer batch for batch.
 def buffer tempbatch for tempbatch.
 
 run server/splitbatch.p (pBatchID,
                      pComments,
                      output pNewBatchID,
                      output pSuccess,
                      output pMsg).
 if not pSuccess
  then return.
  
 
 /* Aggregations have changed, so refresh batch */ 
 refreshBatch(pBatchID).
                    
 /* Actions have been added, so refresh actions */
 refreshBatchActions(pBatchID).

/*  /* Add the newly created batch into the local datastore */ */
/*  empty temp-table tempbatch.                                */
/*  run server/getbatch.p (pNewBatchID,                        */
/*                     output table tempbatch,                 */
/*                     output pSuccess,                        */
/*                     output pMsg).                           */
/*                                                             */
/*  if not pSuccess                                            */
/*   then return.                                              */
/*                                                             */
/*  for each tempbatch                                         */
/*    where tempbatch.batchID = pNewBatchID:                   */
/*   create batch.                                             */
/*   buffer-copy tempbatch to batch.                           */
/*  end.                                                       */

 /* Refresh newly created batch */
 refreshBatch(pNewBatchID).

 /* Actions have been added, so refresh actions */
 refreshBatchActions(pNewBatchID).

/*  message "Errors were split to new batch " + string(pNewBatchID)  */
/*    view-as alert-box title "Success".                             */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SwapPolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SwapPolicy Procedure 
PROCEDURE SwapPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pPolicyID as int.
 def input parameter pNewPolicyID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batchform for batchform.

 run server/swappolicy.p (pPolicyID,
                      pNewPolicyID,
                      output pSuccess,
                      output pMsg).
 if not pSuccess
  then return.
                       
 /* one or more batchform may have been changed in another batch in the current period */
 run LoadBatches in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-TIMEREXT) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TIMEREXT Procedure 
PROCEDURE TIMEREXT :
/*------------------------------------------------------------------------------
  Purpose:     Called when the application is idle
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer esbmsg for esbmsg.

 def var tBatchChanged as int no-undo.
 def var tDefaultEndorsementChanged as int no-undo.
 def var tStatCodeChanged as int no-undo.
 def var tStateFormChanged as int no-undo.


 /* Batch */
 for each esbmsg
   where esbmsg.entity = "Batch"
    break by esbmsg.keyID
          by esbmsg.rcvd descending:
  if first-of(esbmsg.keyID) 
   then 
    do:  std-in = integer(esbmsg.keyID) no-error.
         if std-in <= 0 or std-in = ?
          then .
          else
         case esbmsg.action:
          when "created" or when "updated" 
           then 
            do: std-lo = refreshBatch(std-in).
                if std-lo
                 then tBatchChanged = tBatchChanged + 1.
            end.
          when "deleted" 
           then
            do: find batch
                  where batch.batchID = std-in no-error.
                if available batch
                 then 
                  do: delete batch.
                      tBatchChanged = tBatchChanged + 1.
                  end.
            end.
         end case.
    end.
 end.


 /* Batch Import */
 for each esbmsg
    where esbmsg.entity = "BatchAsync"
    break by esbmsg.keyID
          by esbmsg.rcvd descending:
  if first-of(esbmsg.keyID) 
   then 
    do:  std-in = integer(esbmsg.keyID) no-error.
         if std-in <= 0 or std-in = ?
          then .
          else
         case esbmsg.action:
          when "created" or when "updated" 
           then 
            do: std-lo = refreshBatch(std-in) and refreshBatchForms(std-in).
                if std-lo
                 then tBatchChanged = tBatchChanged + 1.
            end.
          when "deleted" 
           then
            do: find batch
                  where batch.batchID = std-in no-error.
                if available batch
                 then 
                  do: delete batch.
                      tBatchChanged = tBatchChanged + 1.
                  end.
            end.
         end case.
    end.
 end.


 /* DefaultEndorsement */
 for each esbmsg
   where esbmsg.entity = "DefaultEndorsement"
    break by esbmsg.rcvd descending:
  std-lo = false.
  if tLoadedDefaultEndorsements and first(esbmsg.rcvd) 
   then 
    do: 
        std-lo = refreshDefaultEndorsements().
        if std-lo
         then tDefaultEndorsementChanged = tDefaultEndorsementChanged + 1.
    end.
 end.


 /* StatCode */
 for each esbmsg
   where esbmsg.entity = "StatCode"
    break by esbmsg.keyID
          by esbmsg.rcvd descending:
  if tLoadedStatCodes and first-of(esbmsg.keyID) 
   then 
    do:  if num-entries(esbmsg.keyID, {&id-dlm}) = 2
          then
         case esbmsg.action:
          when "created" or when "updated" 
           then 
            do: std-lo = refreshStatCode(entry(1, esbmsg.keyID, {&id-dlm}),
                                         entry(2, esbmsg.keyID, {&id-dlm})).
                if std-lo 
                 then tStatCodeChanged = tStatCodeChanged + 1.
            end.
          when "deleted" 
           then
            do: find statCode
                  where statCode.stateID = entry(1, esbmsg.keyID, {&id-dlm})
                    and statCode.formID = entry(2, esbmsg.keyID, {&id-dlm}) no-error.
                if available statCode 
                 then 
                  do: delete statcode.
                      tStatCodeChanged = tStatCodeChanged + 1.
                  end.
            end.
         end case.
    end.
 end.


 /* StateForm */
 for each esbmsg
   where esbmsg.entity = "StateForm"
    break by esbmsg.keyID
          by esbmsg.rcvd descending:
  if tLoadedStateForms and first-of(esbmsg.keyID) 
   then 
    do:  if num-entries(esbmsg.keyID, {&id-dlm}) = 2
          then
         case esbmsg.action:
          when "created" or when "updated" 
           then 
            do: std-lo = refreshStateForm(entry(1, esbmsg.keyID, {&id-dlm}),
                                          entry(2, esbmsg.keyID, {&id-dlm})).
                if std-lo 
                 then tStateFormChanged = tStateFormChanged + 1.
            end.
          when "deleted" 
           then
            do: find stateForm
                  where stateform.stateID = entry(1, esbmsg.keyID, {&id-dlm})
                    and stateform.formID = entry(2, esbmsg.keyID, {&id-dlm}) no-error.
                if available stateform 
                 then 
                  do: delete stateform.
                      tStateFormChanged = tStateFormChanged + 1.
                  end.
            end.
         end case.
    end.
 end.

 /* das */
/*  MESSAGE                                              */
/*       "    Agents: " tAgentChanged skip               */
/*       "   Batches: " tBatchChanged skip               */
/*       "  Counties: " tCountyChanged skip              */
/*       "    States: " tStateChanged skip               */
/*       "  Defaults: " tDefaultEndorsementChanged skip  */
/*       "   Periods: " tPeriodChanged skip              */
/*       "StateForms: " tStateFormChanged                */
/*      VIEW-AS ALERT-BOX INFO BUTTONS OK.               */

 if tBatchChanged > 0
  then publish "BatchDataChanged".
 if tDefaultEndorsementChanged > 0
  then publish "DefaultEndorsementDataChanged".
 if tStatCodeChanged > 0
  then publish "StatCodeDataChanged".
 if tStateFormChanged > 0
  then publish "StateFormDataChanged".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ToProcessBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ToProcessBatch Procedure 
PROCEDURE ToProcessBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def buffer batch for batch.
 
 run server/toprocessbatch.p (pBatchID,
                         output pSuccess,
                         output pMsg).
 
 if not pSuccess
  then return.
  
 refreshBatch(pBatchID).
 refreshBatchActions(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ToReviewBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ToReviewBatch Procedure 
PROCEDURE ToReviewBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 def var tNewStat as char no-undo.
 
 run server/toreviewbatch.p (pBatchID,
                         output pSuccess,
                         output pMsg).
 
 if not pSuccess
  then return.
  
 refreshBatch(pBatchID).
 refreshBatchForms(pBatchID).   /* due to validation */
 refreshBatchActions(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-UnlockBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UnlockBatch Procedure 
PROCEDURE UnlockBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/unlockbatch.p (pBatchID,
                       output pSuccess,
                       output pMsg).
 if not pSuccess
  then return.
  
 refreshBatch(pBatchID).
 refreshBatchActions(pBatchID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-UnlockBatchMgr) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE UnlockBatchMgr Procedure 
PROCEDURE UnlockBatchMgr :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as char.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run unlockBatch in this-procedure (input pBatchID,
                                    output pSuccess,
                                    output pMsg).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ValidateBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ValidateBatch Procedure 
PROCEDURE ValidateBatch :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pBatchID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.
 def output parameter pErrCnt as int.
 def output parameter pWarnCnt as int.
 def output parameter pGoodCnt as int.

 def buffer batch for batch.
 def buffer batchform for batchform.

 def var tChanged as int no-undo.
 def var tFilename as char no-undo.
 def var tActiveBatchID as int no-undo.


 publish "GetCurrentValue" (pBatchID, output std-ch).
 tActiveBatchID = int(std-ch).

 publish "GetReportDir" (output tFilename).
 if tFilename > "" 
  then if lookup(substring(tFilename, length(tFilename), 1), "/,\") = 0
        then tFilename = tFilename + "/".
 tFilename = tFilename + "Validation_" + string(pBatchID) + ".pdf".
 if search(tFilename) <> ? 
  then
   do: 
       os-delete value(tFilename).
       if os-error <> 0
        then
         do:
             pMsg = "Please close the open validation report".
             return.
         end.
   end.

 empty temp-table tempbatchform.

 if pBatchID = tActiveBatchID  
  then std-ch = "C". /* Return changed batchform */
  else std-ch = "".  /* Return no batchform data */

 run server/validatebatch.p (pBatchID,
                             std-ch,
                             output table tempbatchform,
                             output tChanged,
                             output pGoodCnt,
                             output pWarnCnt,
                             output pErrCnt,
                             output pSuccess,
                             output pMsg).

 refreshBatch(pBatchID).
 refreshBatchActions(pBatchID).
 refreshBatchForms(pBatchID).
 
/*  for each tempbatchform                                */
/*    where tempbatchform.batchID = pBatchID:             */
/*   find batchform                                       */
/*     where batchform.batchID = pBatchID                 */
/*       and batchform.seq = tempbatchform.seq no-error.  */
/*   if not available batchform                           */
/*    then create batchform.                              */
/*   buffer-copy tempbatchform to batchform.              */
/*  end.                                                  */

 if pWarnCnt > 0 or pErrCnt > 0 then
 run valrpt-r.p (pBatchID, tFilename).
 
 pSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ValidateBatchForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ValidateBatchForm Procedure 
PROCEDURE ValidateBatchForm :
def input parameter pBatchID as int.
def input parameter pAgentID as char.
def input parameter pFileNumber as char.
def input parameter pResidential as logical.
def input parameter pFormType as char.
def input parameter pPolicyID as int.
def input parameter pRateCode as char.
def input parameter pStatCode as char.
def input parameter pFormID as char.
def input parameter pEffDate as datetime.
def input parameter pLiabilityAmount as decimal.
def input parameter pCountyID as char.
def input parameter pGrossPremium as decimal.
def input parameter pNetPremium as decimal.

def output parameter pValid as char.
def output parameter pValidMsg as char.

def var tSuccess as logical no-undo.
def var tMsg as char no-undo.

 run server/validatebatchform.p (pBatchID,
                                 pAgentID,
                                 pFileNumber,
                                 pResidential,
                                 pFormType,
                                 pPolicyID,
                                 pRateCode,
                                 pStatCode,
                                 pFormID,
                                 pEffDate,
                                 pLiabilityAmount,
                                 pCountyID,
                                 pGrossPremium,
                                 pNetPremium,
                                 output pValid,
                                 output pValidMsg,
                                 output tSuccess,
                                 output tMsg).
 /* Ignore tSuccess and tMsg as the pValid is set appropriately */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-validOrganization) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validOrganization Procedure 
PROCEDURE validOrganization :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcOrgID as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.

  define buffer Organization for Organization.
   
  find first Organization where Organization.OrgID = ipcOrgID no-error.
  if not available Organization
   then
    do:
      message "Invalid Organization ID"  
        view-as alert-box info buttons ok. 
      return.
    end.

  oplSuccess   = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-validPerson) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validPerson Procedure 
PROCEDURE validPerson :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcPersonID as character no-undo.
  define output parameter oplSuccess  as logical   no-undo.

  define buffer tcachePerson for tcachePerson.
   
  find first tcachePerson where tcachePerson.PersonID = ipcPersonID no-error.
  if not available tcachePerson
   then
    do:
      message "Invalid Person ID"  
        view-as alert-box info buttons ok. 
      return.
    end.

  oplSuccess   = true.

end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-VoidPolicy) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE VoidPolicy Procedure 
PROCEDURE VoidPolicy :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pPolicyID as int.
 def output parameter pSuccess as logical init false.
 def output parameter pMsg as char.

 run server/voidpolicy.p (pPolicyID,
                      output pSuccess,
                      output pMsg).
                      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-addFormToBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION addFormToBatch Procedure 
FUNCTION addFormToBatch RETURNS LOGICAL PRIVATE
 (
  input pBatchID as int,
  input pSeq as int,

  input pFormType as char,
  input pFileNumber as char,
 
  input pPolicyID as int,
  input pFormID as char,
  input pStatCode as char,
 
  input pEffDate as datetime,
  input pLiabilityAmount as decimal,
 
  input pCountyID as char,
  input pResidential as logical,

  input pGrossPremium as decimal,
  input pNetPremium as decimal,
 
  input pValidForm as char,
  input pValidMsg as char,

  output pRedo as logical
 ):

 def buffer batch for batch.
 def buffer batchform for batchform.
 def buffer policy for policy.
 def buffer endorsement for endorsement.

 def var tLiabilityDelta as decimal.
 def var tGrossDelta as decimal.
 def var tNetDelta as decimal.
 def var tRetentionDelta as decimal.

 def var tValidPolicy as logical init true no-undo.

 find batch 
   where batch.batchID = pBatchID.

 for first policy
   where policy.policyID = pPolicyID:
  pRedo = (policy.stat = "P").
 end.

 case pFormType:
  when "P" /* Policy */
   then
    do: 
        for first policy
          where policy.policyID = pPolicyID:
         /* Deltas are calculated againt "processed" values and "current" values */
         assign
/*            pRedo = (policy.stat = "P") /* (policy.liabilityAmount > 0) */ */
           tLiabilityDelta = pLiabilityAmount - policy.liabilityAmount
           tGrossDelta = pGrossPremium - policy.grossPremium
           tNetDelta = pNetPremium - policy.netPremium
           tRetentionDelta = (pGrossPremium - pNetPremium) - policy.retention
           .
        end.
    end.
  when "E" /* Endorsement */
   then
    do: assign
          pLiabilityAmount = 0 /* Not set for an endorsement */
          tLiabilityDelta = 0
          pCountyID = ""       /* Not set for an endorsement */
          
          pRedo = false
          tGrossDelta = pGrossPremium
          tNetDelta = pNetPremium
          tRetentionDelta = (pGrossPremium - pNetPremium)
          .
        for first endorsement
          where endorsement.policyID = pPolicyID
            and endorsement.formID = pFormID
            and endorsement.effDate = pEffDate:
         assign
/*            pRedo = (endorsement.grossPremium > 0)  */
           tGrossDelta = pGrossPremium - endorsement.grossPremium
           tNetDelta = pNetPremium - endorsement.netPremium
           tRetentionDelta = (pGrossPremium - pNetPremium) - endorsement.retention 
/*            pRedo = true  */
           .
        end.
    end.
  when "C" then . /* CPL */
  when "S" then . /* Search */
  when "M" then . /* Misc */
 end case.

 /* Set related forms to "E"rrors if this one is the basis for the error */
 if pValidForm = "E"
  then
   do: tValidPolicy = false.
       for each batchform
         where batchform.batchID = pBatchID
           and batchform.policyID = pPolicyID
           and batchform.validPolicy = true:
        batchform.validPolicy = false.
       end.
   end.
 /* If this one is not an "E"rror, see if there is a related form in the
    batch that is an error and set this one if necessary */
  else
   do:
       if can-find(first batchform
                     where batchform.batchID = pBatchID
                       and batchform.policyID = pPolicyID
                       and batchform.validPolicy = false)
        then tValidPolicy = false.
   end.

 create batchform.
 assign
   batchform.batchID = pBatchID
   batchform.seq = pSeq

   batchform.fileNumber = pFileNumber
   batchform.formType = pFormType
   batchform.formID = pFormID

   batchform.policyID = pPolicyID
   batchform.statCode = pStatCode
   batchform.effDate = pEffDate

   batchform.countyID = pCountyID
   batchform.muniID = ""
   batchform.cplID = ""
   
   batchform.liabilityAmount = pLiabilityAmount
   batchform.grossPremium = pGrossPremium
   batchform.netPremium = pNetPremium
   batchform.retentionPremium = (pGrossPremium - pNetPremium)
   
   batchform.insuredType = ""
   batchform.residential = pResidential

   batchform.liabilityDelta = tLiabilityDelta
   batchform.grossDelta = tGrossDelta
   batchform.netDelta = tNetDelta
   batchform.retentionDelta = tRetentionDelta

   batchform.reprocess = pRedo
   
   batchform.validPolicy = tValidPolicy
   batchform.validForm = pValidForm
   batchform.validmsg = pValidMsg
   
   batchform.rateCode = ""
   batchform.createdDate = now
   .

 /* Add new values into aggregations on batch record */
 batch.liabilityAmount = batch.liabilityAmount + batchform.liabilityAmount.
 batch.grossPremiumProcessed = batch.grossPremiumProcessed + batchform.grossPremium.
 batch.netPremiumProcessed = batch.netPremiumProcessed + batchform.netPremium.
 batch.retainedPremiumProcessed = batch.retainedPremiumProcessed + 
   (batchform.grossPremium - batchform.netPremium).

 batch.liabilityDelta = batch.liabilityDelta + batchform.liabilityDelta.
 batch.grossPremiumDelta = batch.grossPremiumDelta + batchform.grossDelta.
 batch.netPremiumDelta = batch.netPremiumDelta + batchform.netDelta.
 batch.retainedPremiumDelta = batch.retainedPremiumDelta + 
   (batchform.grossDelta - batchform.netDelta).

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getNextBatchID) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getNextBatchID Procedure 
FUNCTION getNextBatchID RETURNS CHARACTER PRIVATE
  ( input pMonth as int,
    input pYear as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer batch for batch.
 def buffer period for period.

 def var tNextBatch as int no-undo.

 find period 
   where period.periodMonth = pMonth
     and period.periodYear = pYear no-error.
 if not available period 
  then return "NA".
 if period.active <> yes 
  then return "NA".

 find last batch
   where batch.periodID = period.periodID no-error.
 if available batch
  then tNextBatch = int(batch.batchID) + 1.
  else tNextBatch = (period.periodID * 10000) + 1.

 return string(tNextBatch).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBatch Procedure 
FUNCTION refreshBatch RETURNS LOGICAL PRIVATE
  ( pBatchID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  Create/Update a batch record from the server
    Notes:  
------------------------------------------------------------------------------*/
 def buffer batch for batch.
 def buffer tempbatch for tempbatch.
 def buffer batchaction for batchaction.
 def buffer tempbatchaction for tempbatchaction.

 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.

 empty temp-table tempbatch.
 empty temp-table tempbatchaction.

 if pBatchID = 0 or pBatchID = ? 
  then return false.

 run server/getbatches.p (0, /* periodID, 0=Any */
                          pBatchID,
                          output table tempbatch,
                          output table tempbatchaction,
                          output tSuccess,
                          output tMsg).
 
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.
   end.

 find first tempbatch
   where tempbatch.batchID = pBatchID
   no-error.
 if not avail tempbatch then
 return true.

 /* Verify the tempbatch.periodID = "Current Period" */
 publish "GetCurrentValue" ("PeriodID", output std-ch).
 std-in = integer(std-ch).
 if tempbatch.periodID <> std-in 
  then return false.

 find batch
   where batch.batchID = pBatchID no-error.

 if not available batch
  then create batch.
 buffer-copy tempbatch to batch.

 for each batchaction
   where batchaction.batchID = pBatchID:
  delete batchaction.
 end.
 for each tempbatchaction
   where tempbatchaction.batchID = pBatchID:
  create batchaction.
  buffer-copy tempbatchaction to batchaction.
 end. 

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatchActions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBatchActions Procedure 
FUNCTION refreshBatchActions RETURNS LOGICAL PRIVATE
  ( pBatchID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer batchaction for batchaction.
 def buffer tempbatchaction for tempbatchaction.
 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.

 
 empty temp-table tempbatchaction.
 
 if pBatchID = 0 or pBatchID = ? 
  then return false.

 run server/getbatchactions.p (pBatchID,
                               output table tempbatchaction,
                               output tSuccess,
                               output tMsg).
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.
   end.

 for each batchaction
   where batchaction.batchID = pBatchID:
  delete batchaction.
 end.
 
 for each tempbatchaction
   where tempbatchaction.batchID = pBatchID:
  create batchaction.
  buffer-copy tempbatchaction to batchaction.
 end.
 
 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatchForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBatchForm Procedure 
FUNCTION refreshBatchForm RETURNS LOGICAL PRIVATE
  ( pBatchID as int,
    pSeq as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer batchform for batchform.
 def buffer tempbatchform for tempbatchform.
 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.

 
 empty temp-table tempbatchform.

 if pBatchID = 0 or pBatchID = ? 
    or pSeq = 0 or pSeq = ?
  then return false.
 
 run server/getbatchform.p (pBatchID,
                            pSeq,
                            output table tempbatchform,
                            output tSuccess,
                            output tMsg).
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "RefreshBatchForm Error: " tMsg view-as alert-box warning.
       return false.
   end.
 
 for first batchform
   where batchform.batchID = pBatchID
     and batchform.seq = pSeq:
  delete batchform.
 end.
 for first tempbatchform
   where tempbatchform.batchID = pBatchID
     and tempbatchform.seq = pSeq:
  create batchform.
  buffer-copy tempbatchform to batchform.
 end.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshBatchForms) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBatchForms Procedure 
FUNCTION refreshBatchForms RETURNS LOGICAL PRIVATE
  ( pBatchID as int ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer batchform for batchform.
 def buffer tempbatchform for tempbatchform.
 def var tSuccess as logical no-undo.
 def var tMsg as char no-undo.

 
 empty temp-table tempbatchform.

 if pBatchID = 0 or pBatchID = ? 
  then return false.
 
 run server/getbatchforms.p (pBatchID,
                             0, /* sequence - zero for all */
                             output table tempbatchform,
                             output tSuccess,
                             output tMsg).

 if not tSuccess
  then 
   do: std-lo = false.
       publish "DataError" (input tMsg).
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "Error: " tMsg view-as alert-box warning.
       return false.
   end.
 
 for each batchform
   where batchform.batchID = pBatchID:
  delete batchform.
 end.
 for each tempbatchform
   where tempbatchform.batchID = pBatchID:
  create batchform.
  buffer-copy tempbatchform to batchform.
 end.

 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshDefaultEndorsements) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshDefaultEndorsements Procedure 
FUNCTION refreshDefaultEndorsements RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer defaultform for defaultform.
 def buffer tempdefaultform for tempdefaultform.
 
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
 

 empty temp-table tempdefaultform.

 run server/getdefaultforms.p (output table tempdefaultform,
                               output tSuccess,
                               output tMsg).
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "RefreshDefaultEndorsements Error: " tMsg view-as alert-box warning.
       return false.
   end.
 
 empty temp-table defaultform.

 for each tempdefaultform:
  create defaultform.
  buffer-copy tempdefaultform to defaultform.
 end.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshStatCode) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshStatCode Procedure 
FUNCTION refreshStatCode RETURNS LOGICAL PRIVATE
  ( pStateID as char,
    pStatCode as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempstatcode for tempstatcode.
 def buffer statcode for statcode.
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.


 empty temp-table tempstatcode.

 if pStateID = "" or pStateID = ?
   or pStatCode = "" or pStatCode = ? 
  then return false.

 run server/getstatcodes.p (pStateID,  /* blank=All */
                            pStatCode, /* blank=All */
                            output table tempstatcode,
                            output tSuccess,
                            output tMsg).
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "LoadStatCodes failed: " tMsg view-as alert-box warning.
       return false.
   end.

 find tempstatcode
   where tempstatcode.stateID = pStateID
     and tempstatcode.statCode = pStatCode.
 find first statcode
   where statcode.stateID = pStateID
     and statcode.statCode = pStatCode no-error.
 if not available statcode 
  then create statcode.
 buffer-copy tempstatcode to statcode.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-refreshStateForm) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshStateForm Procedure 
FUNCTION refreshStateForm RETURNS LOGICAL PRIVATE
  ( pStateID as char,
    pFormID as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer tempstateform for tempstateform.
 def buffer stateform for stateform.
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.

 
 empty temp-table tempstateform.
 
 if pStateID = "" or pStateID = ?
   or pFormID = "" or pFormID = ? 
  then return false.

 run server/getstateforms.p (pStateID, /* stateID, blank=All */
                             pFormID,  /* formID, blank=All */
                             output table tempstateform,
                             output tSuccess,
                             output tMsg).
 if not tSuccess
  then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "RefreshStateForm error: " tMsg view-as alert-box warning.
       return false.
   end.


 find first tempstateform
   where tempstateform.stateID = pStateID
     and tempstateform.formID = pFormID.
 
 find stateform
   where stateform.stateID = pStateID 
     and stateform.formID = pFormID no-error.

 if not available stateform
  then create stateform.
 buffer-copy tempstateform to stateform.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-removeFormFromBatch) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeFormFromBatch Procedure 
FUNCTION removeFormFromBatch RETURNS LOGICAL PRIVATE
 (pBatchID as int,
  pSeq as int):

 def buffer batch for batch.
 def buffer batchform for batchform.

 def var tPolicyID as int no-undo.
 def var tValidPolicy as logical no-undo.

 find batch 
   where batch.batchID = pBatchID.

 find batchform 
   where batchform.batchID = pBatchID 
     and batchform.seq = pSeq.

 tPolicyID = batchform.policyID.

 batch.liabilityAmount = batch.liabilityAmount - batchform.liabilityAmount.
 batch.grossPremiumProcessed = batch.grossPremiumProcessed - batchform.grossPremium.
 batch.netPremiumProcessed = batch.netPremiumProcessed - batchform.netPremium.
 batch.retainedPremiumProcessed = batch.retainedPremiumProcessed - 
   (batchform.grossPremium - batchform.netPremium).

 batch.liabilityDelta = batch.liabilityDelta - batchform.liabilityDelta.
 batch.grossPremiumDelta = batch.grossPremiumDelta - batchform.grossDelta.
 batch.netPremiumDelta = batch.netPremiumDelta - batchform.netDelta.
 batch.retainedPremiumDelta = batch.retainedPremiumDelta - 
   (batchform.grossDelta - batchform.netDelta).

 delete batchform.

 if can-find(first batchform 
              where batchform.batchID = pBatchID
                and batchform.policyID = tPolicyID
                and batchform.validForm = "E")
  then tValidPolicy = false.
  else tValidPolicy = true.

 for each batchform
   where batchform.batchID = pBatchID
     and batchform.policyID = tPolicyID:
  batchform.validPolicy = tValidPolicy.
 end.
 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

