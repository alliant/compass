&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/* dialogagentappcancel.w
   Window for adding the new agent application
   Yoke Sam 08.09.2017

 */
 
{tt/policyhoi.i}
{tt/agent.i}
{tt/county.i}
{tt/sysprop.i}
{tt/policy.i}
{tt/batchform.i}
{tt/endorsement.i}


def output parameter pCancel as logical init true.


&scoped-define statehoi "TX"

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSearch tPolicy tFileNumber tAgentID ~
tIssueDate tAgentName tAddr1 tLiability tGrossPremium tAddr2 tCoopAgentID ~
tRequestAgentID bCancel 
&Scoped-Define DISPLAYED-OBJECTS tFileNumber tCounty tAgentID tIssueDate ~
tAgentName tAddr1 tLiability tGrossPremium tAddr2 tDIPStatus tCoopAgentID ~
tRequestAgentID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ResetScreenValues fMain 
FUNCTION ResetScreenValues RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD SetModify fMain 
FUNCTION SetModify RETURNS logical
  ( input pModify as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidateEntry fMain 
FUNCTION ValidateEntry RETURNS logical
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD ValidatePolicy fMain 
FUNCTION ValidatePolicy RETURNS logical
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel 
     LABEL "Close" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bClear  NO-FOCUS
     LABEL "Clear" 
     SIZE 5 BY 1.

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 5 BY 1.

DEFINE VARIABLE tCounty AS CHARACTER FORMAT "X(256)":U INITIAL "S" 
     LABEL "County" 
     VIEW-AS COMBO-BOX INNER-LINES 30
     LIST-ITEM-PAIRS "Select","S"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr1 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE tAddr2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAgentName AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 49 BY 1 NO-UNDO.

DEFINE VARIABLE tCoopAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Cooperating Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "State assigned identifier" NO-UNDO.

DEFINE VARIABLE tFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1 NO-UNDO.

DEFINE VARIABLE tIssueDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Policy Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "$zzz,zzz,zz9.99-":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 22.8 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>>":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter policy number and press <Return> to search"
     FONT 6 NO-UNDO.

DEFINE VARIABLE tRequestAgentID AS CHARACTER FORMAT "X(256)":U 
     LABEL "Requesting Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tDIPStatus AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "MultiCounty (0)", "0",
"Best Evidence (1)", "1",
"Out of County (2)", "2"
     SIZE 21.2 BY 3 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bSearch AT ROW 1.48 COL 32.6 WIDGET-ID 204 NO-TAB-STOP 
     bClear AT ROW 1.48 COL 38.2 WIDGET-ID 208 NO-TAB-STOP 
     tPolicy AT ROW 1.48 COL 16 COLON-ALIGNED WIDGET-ID 2
     tFileNumber AT ROW 3.24 COL 16 COLON-ALIGNED WIDGET-ID 14
     tCounty AT ROW 4.43 COL 16 COLON-ALIGNED WIDGET-ID 190
     tAgentID AT ROW 1.48 COL 68 COLON-ALIGNED WIDGET-ID 16 NO-TAB-STOP 
     tIssueDate AT ROW 5.62 COL 16 COLON-ALIGNED WIDGET-ID 184
     tAgentName AT ROW 1.48 COL 82.8 COLON-ALIGNED NO-LABEL WIDGET-ID 70 NO-TAB-STOP 
     tAddr1 AT ROW 2.62 COL 82.8 COLON-ALIGNED NO-LABEL WIDGET-ID 18 NO-TAB-STOP 
     tLiability AT ROW 6.81 COL 16 COLON-ALIGNED WIDGET-ID 32
     tGrossPremium AT ROW 8 COL 16 COLON-ALIGNED WIDGET-ID 44
     tAddr2 AT ROW 3.76 COL 82.8 COLON-ALIGNED NO-LABEL WIDGET-ID 20 NO-TAB-STOP 
     tDIPStatus AT ROW 9.33 COL 18 NO-LABEL WIDGET-ID 192
     tCoopAgentID AT ROW 6.76 COL 69 COLON-ALIGNED WIDGET-ID 188
     tRequestAgentID AT ROW 5.62 COL 69 COLON-ALIGNED WIDGET-ID 186 NO-TAB-STOP 
     bSave AT ROW 12.67 COL 51
     bCancel AT ROW 12.67 COL 67 NO-TAB-STOP 
     "Status:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 9.57 COL 9 WIDGET-ID 196
     SPACE(118.39) SKIP(4.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Home Issued Policy"
         DEFAULT-BUTTON bSearch CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bClear IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bSave IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAddr1:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAddr2:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAgentID:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tAgentName:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tCoopAgentID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR COMBO-BOX tCounty IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET tDIPStatus IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tFileNumber:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tGrossPremium:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tIssueDate:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolicy IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tRequestAgentID:READ-ONLY IN FRAME fMain        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* New Home Issued Policy */
DO:
   std-lo = yes.
 
    if bSave:sensitive in frame {&frame-name}
       then message "You've not saved yet; cancel?" view-as alert-box buttons yes-no update std-lo.
    
    if std-lo then
     do:
        APPLY "END-ERROR":U TO SELF.
        return no-apply.
     end.
     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCancel fMain
ON CHOOSE OF bCancel IN FRAME fMain /* Close */
DO:
  apply "WINDOW-CLOSE" to frame {&frame-name}.
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear fMain
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  ResetScreenValues().
  setModify(false).
  bSave:sensitive = false.
  bCancel:sensitive = true.
  apply "LEAVE" to bClear.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear fMain
ON LEAVE OF bClear IN FRAME fMain /* Clear */
DO:
  apply "entry":U to tPolicy in frame {&frame-name}.
  return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fMain
ON CHOOSE OF bSave IN FRAME fMain /* Save */
DO:
  run NewPolicyHoi in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch fMain
ON CHOOSE OF bSearch IN FRAME fMain /* Search */
DO:
  apply "return" to tPolicy.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAddr1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr1 fMain
ON ENTRY OF tAddr1 IN FRAME fMain
DO:
  if self:read-only then 
  do:
    apply "LEAVE" to bClear.
     return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAddr2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr2 fMain
ON ENTRY OF tAddr2 IN FRAME fMain
DO:
  if self:read-only then 
  do:
    apply "LEAVE" to bClear.
     return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID fMain
ON ENTRY OF tAgentID IN FRAME fMain /* Agent */
DO:
  if self:read-only then 
  do:
    apply "LEAVE" to bClear.
     return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentName fMain
ON ENTRY OF tAgentName IN FRAME fMain
DO:
  if self:read-only then 
  do:
     apply "LEAVE" to bClear.
     return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCoopAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCoopAgentID fMain
ON ENTRY OF tCoopAgentID IN FRAME fMain /* Cooperating Agent */
DO:
  if tCoopAgentID:read-only then 
  do:
    apply "LEAVE" to bClear.
    return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCounty
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCounty fMain
ON ENTRY OF tCounty IN FRAME fMain /* County */
DO:
  if not tCounty:sensitive then apply "LEAVE" to bClear.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tFileNumber
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tFileNumber fMain
ON ENTRY OF tFileNumber IN FRAME fMain /* File */
DO:
 if tFileNumber:read-only then 
 do:
    apply "LEAVE" to bClear.
     return no-apply.
 end.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tGrossPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tGrossPremium fMain
ON ENTRY OF tGrossPremium IN FRAME fMain /* Gross Premium */
DO:
  if tGrossPremium:read-only then
  do:
    apply "LEAVE" to bClear.
    return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tIssueDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tIssueDate fMain
ON ENTRY OF tIssueDate IN FRAME fMain /* Policy Date */
DO:
  if tIssueDate:read-only then 
  do:
     apply "LEAVE" to bClear.
     return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLiability
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLiability fMain
ON ENTRY OF tLiability IN FRAME fMain /* Liability */
DO:
  if tLiability:read-only then
  do:
    apply "LEAVE" to bClear.
    return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPolicy fMain
ON LEAVE OF tPolicy IN FRAME fMain /* Policy */
DO:
  
/*     apply 'return' to self. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tPolicy fMain
ON RETURN OF tPolicy IN FRAME fMain /* Policy */
DO:
  if not ValidatePolicy()
   then return no-apply.
  SetModify(true).
  run SetScreenValues.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRequestAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRequestAgentID fMain
ON ENTRY OF tRequestAgentID IN FRAME fMain /* Requesting Agent */
DO:
  if self:read-only then 
  do:
    apply "LEAVE" to bClear.
     return no-apply.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

setModify(false).

frame {&frame-name}:title = "New Home Issued Policy".

bClear:load-image("images/s-erase.bmp").
bClear:load-image-insensitive("images/s-erase-i.bmp").
bSearch:load-image("images/s-magnifier.bmp").
bSearch:load-image-insensitive("images/s-magnifier-i.bmp").
bSearch:hidden = false.

std-ch = "".
publish "GetCounties" (output table county).
/* for each county where stateID = {&statehoi} by county.countyID:                                                                     */
/*     std-ch = (if std-ch > "" then std-ch + "," else std-ch) + county.description + " [" + county.countyID + "]," + county.countyID. */
/* end.                                                                                                                                */


tCounty:screen-value = " ".


publish "GetSysProps" (output table sysprop).

tDIPStatus:screen-value = "2".


RUN enable_UI.

MAIN-BLOCK:

repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   WAIT-FOR GO OF FRAME {&FRAME-NAME}.

END.



/* repeat ON ERROR UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK */
/*    ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:  */
/*   WAIT-FOR GO OF FRAME {&FRAME-NAME}.             */
/*                                                   */
/*   if tCode:input-value in frame fMain = ""        */
/*   or tCode:input-value in frame fMain = ? then    */
/*   do:                                             */
/*     message                                       */
/*       "Code cannot be blank"                      */
/*       view-as alert-box error.                    */
/*     next.                                         */
/*   end.                                            */
/*                                                   */
/*   assign                                          */
/*     pCode = tCode:input-value in frame fMain      */
/*     pCancel = false                               */
/*     .                                             */
/*   leave MAIN-BLOCK.                               */
/* END.                                              */
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFileNumber tCounty tAgentID tIssueDate tAgentName tAddr1 tLiability 
          tGrossPremium tAddr2 tDIPStatus tCoopAgentID tRequestAgentID 
      WITH FRAME fMain.
  ENABLE bSearch tPolicy tFileNumber tAgentID tIssueDate tAgentName tAddr1 
         tLiability tGrossPremium tAddr2 tCoopAgentID tRequestAgentID bCancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyAgentApp fMain 
PROCEDURE ModifyAgentApp PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*                                                                                     */
/*    empty temp-table tempAgentApp.                                                   */
/*    do with frame {&frame-name}:                                                     */
/*         bModify:sensitive = false.                                                  */
/*        for each agentapplication by agentapplication.applicationID desc:            */
/*                                                                                     */
/*            assign                                                                   */
/*              agentapplication.agentID = tAgentID:input-value                        */
/*              agentapplication.stat = tStat:input-value                              */
/*              agentapplication.typeofapp = tTypeofApp:input-value                    */
/*              agentapplication.username = replace(tusername:input-value," ","")      */
/*              agentapplication.password = replace(tpassword:input-value," ","")      */
/*              agentapplication.datecreated = tCreatedDate:input-value                */
/*              agentapplication.datestarted = tStartedDate:input-value                */
/*              agentapplication.dateunderreview = tReviewDate:input-value             */
/*              agentapplication.datesubmitted = tSubmittedDate:input-value            */
/*              agentapplication.datestopped = tDeniedDate:input-value                 */
/*              agentapplication.dateapproved = tApprovedDate:input-value              */
/*              agentapplication.datesigned = tCompletedDate:input-value               */
/*              .                                                                      */
/*                                                                                     */
/*        create tempAgentApp.                                                         */
/*                                                                                     */
/*        buffer-copy agentapplication to tempAgentApp.                                */
/*                                                                                     */
/*        publish "ModifyAgentApp" (table tempAgentApp, output std-lo, output std-ch). */
/*                                                                                     */
/*        if not std-lo then                                                           */
/*            message std-ch view-as alert-box error.                                  */
/*                                                                                     */
/*        leave.                                                                       */
/*                                                                                     */
/*        end.                                                                         */
/*                                                                                     */
/*    end.                                                                             */


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewPolicyHoi fMain 
PROCEDURE NewPolicyHoi :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  empty temp-table policyhoi.
  if not(validateEntry()) then return.
  do with frame {&frame-name}:
    create policyhoi.
    assign
        bSave:sensitive = false
        bCancel:sensitive = false
        policyhoi.policyID = int(tPolicy:input-value)
        policyhoi.DIPStatus = tDIPStatus:input-value
        policyhoi.liabilityAmount = decimal(tLiability:input-value)
        policyhoi.grossPremium = decimal(tGrossPremium:input-value)
        policyhoi.policyDate = date(tIssueDate:input-value)
        policyhoi.fileNumber = tFileNumber:input-value
        policyhoi.countyID = tCounty:input-value
        policyhoi.requestAgentID = tRequestAgentID:input-value
        policyhoi.coopAgentID = tCoopAgentID:input-value
        .
      
    publish "NewPolicyHoi" (table policyhoi, output std-lo).

    if std-lo then 
    do:
        apply "CHOOSE" to bClear.
        pCancel = false.
    end.
    else
    do:
        bSave:sensitive = true.
        bCancel:sensitive = true.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetScreenValues fMain 
PROCEDURE SetScreenValues :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

def var tMsgStat as char no-undo.

do with frame {&frame-name}:

  for first policy no-lock
         by policy.policyID:

    publish "GetAgent" (policy.agentID, output table agent).
    for first agent where agent.agentID = policy.agentID no-lock:
        
        {lib/get-county-list.i &combo=tCounty &state=agent.stateID}
        assign
          tAgentID:screen-value = string(policy.agentID)
          tAgentName:screen-value = agent.name
          tAddr1:screen-value = agent.addr1
          tAddr2:screen-value = agent.addr2
          tRequestAgentID:read-only = true
          tRequestAgentID:screen-value = agent.stateUID
          .
        if tRequestAgentID:screen-value = "?"
         then tRequestAgentID:screen-value = "".
    end.
    
    /* get the endorsements */
    std-de = policy.issuedGrossPremium.
    publish "GetEndorsements" (policy.policyID, output table endorsement).
    for each endorsement no-lock:
      std-de = std-de + endorsement.grossPremium.
    end.
    
    assign
      tFileNumber:screen-value = policy.fileNumber
      tIssueDate:screen-value = string(policy.issuedEffDate)
      tLiability:screen-value = string(policy.issuedLiabilityAmount)
      tGrossPremium:screen-value = string(std-de)
      tDIPStatus:screen-value = "2"
      .
  end.
end.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ResetScreenValues fMain 
FUNCTION ResetScreenValues RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
  assign
    tPolicy:screen-value = ""
    tRequestAgentID:screen-value = ""
    tCoopAgentID:screen-value = ""
    tAgentID:screen-value = ""
    tAgentName:screen-value = ""
    tAddr1:screen-value = ""
    tAddr2:screen-value = ""
    tFileNumber:screen-value = ""
    tCounty:screen-value = entry(2, tCounty:list-item-pairs)
    tDIPStatus:screen-value = "2"
    tLiability:screen-value = string(0,"$zzz,zzz,zz9.99-")
    tGrossPremium:screen-value = string(0,"$zzz,zzz,zz9.99-")
    tIssueDate:screen-value = ""
  .

  
end.


  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION SetModify fMain 
FUNCTION SetModify RETURNS logical
  ( input pModify as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:     
    assign 
      tFileNumber:read-only = not pModify
      tCoopAgentID:read-only = not pModify
      tCounty:sensitive = pModify
      tDIPStatus:sensitive = pModify
      tIssueDate:read-only = not pModify
      tLiability:read-only = not pModify
      tGrossPremium:read-only = not pModify
      bSearch:sensitive = not pModify
      bClear:sensitive = pModify
      tPolicy:read-only = pModify
      bSave:sensitive = pModify
      .
  end.

  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidateEntry fMain 
FUNCTION ValidateEntry RETURNS logical
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
std-lo = true.

std-ch = "".

 do with frame {&frame-name}:

     /* Check for File Number */
     if tFileNumber:screen-value = "" then
     do:
        std-ch = std-ch + "File Number cannot be blank!".
     end.

     /* Check for Agent IDs */
     if tCoopAgentID:screen-value = ""  then
         std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "Cooperating Agent ID cannot be blank.".
     /* Check for future date of policy  */
     if date(tIssueDate:screen-value) > today then 
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) +  "Policy Date cannot be in the future.".
    
     if date(tIssueDate:input-value) = ? then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) +  "Policy Date invalid.".

     /* Check for liability and gross premium  */
     if tLiability:input-value <= 0 then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "Liability amount cannot be negative or zero.".

     if tGrossPremium:input-value <= 0 then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "Gross Premium cannot be negative or zero.".

     if tCounty:input-value = "" then
        std-ch = (if std-ch > "" then std-ch + chr(13) else std-ch) + "County cannot be blank.".

 end.

 if std-ch > "" then
   do: 
     message std-ch view-as alert-box.
     std-lo = false.
   end.

  RETURN std-lo.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION ValidatePolicy fMain 
FUNCTION ValidatePolicy RETURNS logical
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
std-lo = false.

do with frame {&frame-name}:
  SetModify(false).
  if tPolicy:screen-value = "" then
  do: return std-lo.
  end.
  else
  do:
  
  empty temp-table policy no-error.
  empty temp-table policyhoi no-error.

  publish "GetPolicy" (tPolicy:screen-value, output table policy, output table batchform).
  find policy no-lock
    where policy.policyID = tPolicy:input-value no-error.
  if  not avail policy then
      do:
      
         message "Policy " + tPolicy:screen-value + " does not exist." 
         view-as alert-box.
      end.

  else
      do:
          publish "GetPolicyHoi" (tPolicy:input-value,output table policyhoi).
          if can-find(first policyhoi) then
          do:
              message "Home Office Issued Policy " + tPolicy:screen-value  + " already exists."
              view-as alert-box.
          end.
          else 
             std-lo = true.
    
      end.
  end.
   
end.          

RETURN std-lo.   /* Function return value. */

            
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

