&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : opsconfig.p
    Purpose     : Manage XML configuration file
    Author(s)   : D.Sinclair
    Created     : Copied from adc/startcfg.p and reorganized
    Notes       :
3.12.2015 D.Sinclair - Added Get/Set Load* to allow datasets to optionally
                       be loaded on startup
  ----------------------------------------------------------------------*/

{lib/commonconfig.i}
{lib/configttfile.i &config="ops.xml"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 17.33
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


subscribe to "GetConfirmInvoice" anywhere.
subscribe to "SetConfirmInvoice" anywhere.

subscribe to "GetStartPeriod" anywhere.
subscribe to "SetStartPeriod" anywhere.

subscribe to "GetLastPeriod" anywhere.
subscribe to "SetLastPeriod" anywhere.

subscribe to "GetSearchStates" anywhere.
subscribe to "SetSearchStates" anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-GetConfirmInvoice) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetConfirmInvoice Procedure 
PROCEDURE GetConfirmInvoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter std-lo as logical.
 std-ch = getOption("ConfirmInvoice").
 if std-ch = "" 
  then std-ch = getSetting("ConfirmInvoice").

 if lookup(std-ch, "no,N,False,F,0") > 0 
  then std-lo = false.
  else std-lo = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetLastPeriod) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetLastPeriod Procedure 
PROCEDURE GetLastPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pMonth as int.
 def output parameter pYear as int.

 pMonth = integer(getOption("LastMonth")) no-error.
 pYear = integer(getOption("LastYear")) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetSearchStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSearchStates Procedure 
PROCEDURE GetSearchStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter opcStates as character no-undo.
 opcStates = getOption("States").
 if opcStates = "" then
   opcStates = getSetting("States").
 if opcStates = "" or num-entries(opcStates) < 2 then
   opcStates = "Colorado,CO". /* Must default for selection list to work */
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStartPeriod) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStartPeriod Procedure 
PROCEDURE GetStartPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter std-ch as char.
 std-ch = getOption("OpenPeriod").
 if std-ch = "" 
  then std-ch = getSetting("OpenPeriod").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetConfirmInvoice) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetConfirmInvoice Procedure 
PROCEDURE SetConfirmInvoice :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter std-lo as logical.
 setOption("ConfirmInvoice", string(std-lo)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetLastPeriod) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetLastPeriod Procedure 
PROCEDURE SetLastPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pMonth as int.
 def input parameter pYear as int.

 if pMonth < 0 or pMonth > 12 
   or pYear < 2012 or pYear > 9999
  then return.

 setOption("LastMonth", string(pMonth)).
 setOption("LastYear", string(pYear)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetSearchStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetSearchStates Procedure 
PROCEDURE SetSearchStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipcStates as character no-undo.
 setOption("States", ipcStates).
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-SetStartPeriod) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetStartPeriod Procedure 
PROCEDURE SetStartPeriod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter std-ch as char.

 if lookup(std-ch, "C,U") = 0 
  then std-ch = "C".
 setOption("OpenPeriod", std-ch).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

