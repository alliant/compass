&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wstatcode.w
   Window of STAT codes
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/statcode.i}
{tt/state.i}

{lib/std-def.i}

def var hData as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwCodes

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES statcode

/* Definitions for BROWSE brwCodes                                      */
&Scoped-define FIELDS-IN-QUERY-brwCodes statcode.stateID statcode.statcode statcode.active statcode.description statcode.regulation statcode.formID   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwCodes   
&Scoped-define SELF-NAME brwCodes
&Scoped-define QUERY-STRING-brwCodes FOR EACH statcode
&Scoped-define OPEN-QUERY-brwCodes OPEN QUERY {&SELF-NAME} FOR EACH statcode.
&Scoped-define TABLES-IN-QUERY-brwCodes statcode
&Scoped-define FIRST-TABLE-IN-QUERY-brwCodes statcode


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwCodes}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwCodes bExport bNew bRefresh bClear ~
bFilter fSearch fState RECT-37 
&Scoped-Define DISPLAYED-OBJECTS fSearch fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bClear 
     LABEL "Clear" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the selected STAT code".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bFilter 
     LABEL "Filter" 
     SIZE 10 BY 1.14.

DEFINE BUTTON bModify  NO-FOCUS
     LABEL "Modify" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the selected STAT code".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Create a new STAT code".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Colorado","CO",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY"
     DROP-DOWN-LIST
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE fSearch AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 65 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36.8 BY 1.91.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 138 BY 1.81.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwCodes FOR 
      statcode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwCodes C-Win _FREEFORM
  QUERY brwCodes DISPLAY
      statcode.stateID label "StateID" width 10
 statcode.statcode column-label "STAT!Code" format "x(10)"
 statcode.active label "Active" format "Yes/No" width 8
 statcode.description label "Description" format "x(255)" width 100
 statcode.regulation label "Regulation" format "x(20)"
 statcode.formID column-label "Default!Form Code" format "x(15)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 176 BY 22.55 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwCodes AT ROW 3.38 COL 2 WIDGET-ID 200
     bDelete AT ROW 1.33 COL 30.8 WIDGET-ID 14 NO-TAB-STOP 
     bModify AT ROW 1.33 COL 23.6 WIDGET-ID 12 NO-TAB-STOP 
     bExport AT ROW 1.33 COL 9.2 WIDGET-ID 2 NO-TAB-STOP 
     bNew AT ROW 1.33 COL 16.4 WIDGET-ID 10 NO-TAB-STOP 
     bRefresh AT ROW 1.33 COL 2 WIDGET-ID 4 NO-TAB-STOP 
     bClear AT ROW 1.62 COL 166.6 WIDGET-ID 68
     bFilter AT ROW 1.62 COL 156.2 WIDGET-ID 64
     fSearch AT ROW 1.71 COL 88.8 COLON-ALIGNED WIDGET-ID 62
     fState AT ROW 1.71 COL 49 COLON-ALIGNED WIDGET-ID 42
     "Filters" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1 COL 41 WIDGET-ID 60
     RECT-2 AT ROW 1.24 COL 1.8 WIDGET-ID 8
     RECT-37 AT ROW 1.33 COL 40 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 178 BY 25.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "STAT Codes"
         HEIGHT             = 25.19
         WIDTH              = 178
         MAX-HEIGHT         = 25.24
         MAX-WIDTH          = 226.4
         VIRTUAL-HEIGHT     = 25.24
         VIRTUAL-WIDTH      = 226.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwCodes 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bModify IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       brwCodes:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwCodes:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwCodes
/* Query rebuild information for BROWSE brwCodes
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH statcode.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwCodes */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* STAT Codes */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* STAT Codes */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* STAT Codes */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bClear C-Win
ON CHOOSE OF bClear IN FRAME fMain /* Clear */
DO:
  fSearch:screen-value = "".
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bFilter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bFilter C-Win
ON CHOOSE OF bFilter IN FRAME fMain /* Filter */
DO:
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModify
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModify C-Win
ON CHOOSE OF bModify IN FRAME fMain /* Modify */
DO:
  run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  run ActionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwCodes
&Scoped-define SELF-NAME brwCodes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON DEFAULT-ACTION OF brwCodes IN FRAME fMain
DO:
  run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON ROW-DISPLAY OF brwCodes IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON START-SEARCH OF brwCodes IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwCodes C-Win
ON VALUE-CHANGED OF brwCodes IN FRAME fMain
DO:
  assign
    bModify:sensitive in frame fMain = (available statcode)
    bDelete:sensitive in frame fMain = (available statcode)
   .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fSearch C-Win
ON RETURN OF fSearch IN FRAME fMain /* Search */
DO:
  apply "choose" to bFilter.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
 assign {&SELF-NAME}.
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("stateID").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bExport:load-image("images/excel.bmp").
bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bNew:load-image("images/new.bmp").
bModify:load-image("images/update.bmp").
bModify:load-image-insensitive("images/update-i.bmp").
bDelete:load-image("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").

{lib/get-state-list.i &combo=fState &addAll=true}
 
run getData in this-procedure.

run windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available statcode 
  then return.

 std-lo = false.
 MESSAGE "STAT Code will be permanently removed.  Continue?"
  VIEW-AS ALERT-BOX question BUTTONS Yes-No update std-lo.
 if not std-lo 
  then return.

 publish "DeleteStatCode" (statcode.stateID,
                           statcode.statcode,
                           output std-lo,
                           output std-ch).
 if not std-lo 
  then 
   do: MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.
 delete statcode.
 browse brwCodes:delete-selected-rows().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionModify C-Win 
PROCEDURE actionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFormID as char no-undo.
 def var tFormType as char no-undo.     
    /* FormType is not stored in DB, only used for default form selection */
 def var tDescription as char no-undo.
 def var tRegulation as char no-undo.
 def var tActive as logical no-undo.

 def var tCancel as logical no-undo.

 if not available statcode 
  then return.

 assign
   tFormID = statcode.formID
   tFormType = statcode.formType
   tDescription = statcode.description
   tRegulation = statcode.regulation
   tActive = statcode.active
   .

 UPDATE-BLOCK:
 repeat:
  run dialogmodifystat.w (input statcode.stateID,
                          input statcode.statcode,
                          input-output tFormID,
                          input-output tFormType,
                          input-output tDescription,
                          input-output tRegulation,
                          input-output tActive,
                          output tCancel).
  if tCancel 
   then leave UPDATE-BLOCK.
  
  publish "ModifyStatCode" (statcode.stateID,
                            statcode.statcode,
                            tFormID,
                            tDescription,
                            tRegulation,
                            tActive,
                            output std-lo,
                            output std-ch).
                            
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        next UPDATE-BLOCK.
    end.
  
  if tFormID = ? 
   then tFormID = "".

  assign
    statcode.formID = tFormID
    statcode.formType = tFormType
    statcode.description = tDescription
    statcode.regulation = tRegulation
    statcode.active = tActive
    .
  display
    statcode.formID
    statcode.description
    statcode.regulation
    statcode.active
   with browse brwCodes.
  leave.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionNew C-Win 
PROCEDURE actionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tStateID as char no-undo.
 def var tStatcode as char no-undo.
 def var tFormID as char no-undo.
 def var tDescription as char no-undo.
 def var tRegulation as char no-undo.
 def var tType as char no-undo.
 def var tActive as logical no-undo.

 def var tCancel as logical no-undo.

 UPDATE-BLOCK:
 repeat:
  run dialognewstat.w (input-output tStateID,
                       input-output tStatcode,
                       input-output tFormID,
                       input-output tDescription,
                       input-output tRegulation,
                       input-output tType,
                       input-output tActive,
                       output tCancel).
  if tCancel 
   then leave UPDATE-BLOCK.
  
  publish "NewStatCode" (tStateID,
                         tStatcode,
                         tFormID,
                         tDescription,
                         tRegulation,
                         tType,
                         tActive,
                         output std-lo,
                         output std-ch).
  if not std-lo 
   then
    do:
        MESSAGE std-ch
         VIEW-AS ALERT-BOX error BUTTONS OK.
        next UPDATE-BLOCK.
    end.
  run getData in this-procedure.
  leave.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fSearch fState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwCodes bExport bNew bRefresh bClear bFilter fSearch fState RECT-37 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.

 if query brwCodes:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).
 
 th = temp-table statcode:handle.
 
 run util/exporttable.p (table-handle th,
                         "statcode",
                         "for each statcode ",
                         "stateID,statcode,active,description,regulation,formID",
                         "State,STAT Code,Active,Description,State Regulation,Default Form",
                         std-ch,
                         "STATcodes.csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("stateID").
 
 apply "VALUE-CHANGED" to brwCodes in frame fMain.
 apply "entry" to browse {&browse-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer statcode for statcode.
 def var tFile as char no-undo.
 
 close query brwCodes.
 empty temp-table statcode.
 
 publish "GetStatCodes" (output table statcode).

 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("stateID").
 
 apply "VALUE-CHANGED" to brwCodes in frame fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tWhereClause as char no-undo.

do with frame {&frame-name}:

  tWhereClause = "where " +
                 (if fState:screen-value <> "ALL" 
                 then "statcode.stateID = '" + fState:screen-value + "' "
                 else "true ") +
                 "and " + 
                 (if fSearch:screen-value <> ""
                 then "statcode.description matches '*" + fSearch:screen-value + "*' "
                 else "true ").
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

