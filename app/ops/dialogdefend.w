&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* dialogdefend.w
   Add DEFault ENDorsements during policy processing
   11.26.2014 D.Sinclair    Added "Zero" button for use with a bundled rate.
   01.08.2015 B.Johnson     Added totals and ability to show all endorsements.
 */

{tt/defaultform.i}
{tt/agent.i}
{tt/stateform.i}

def input-output parameter table for defaultform.
def input parameter table for agent.
def input parameter table for stateform.
def input parameter pStateID as char.
def input parameter pLiability as dec.
def output parameter pSave as logical init false.

{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES defaultform

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData defaultform.seq defaultform.include defaultform.statcode defaultform.formID defaultform.description defaultform.grossPremium defaultform.netPremium   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData include grossPremium netPremium   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH defaultform by defaultform.formID
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH defaultform by defaultform.formID.
&Scoped-define TABLES-IN-QUERY-brwData defaultform
&Scoped-define FIRST-TABLE-IN-QUERY-brwData defaultform


/* Definitions for DIALOG-BOX Dialog-Frame                              */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData tTotGross tTotNet tShowNonDefault ~
tShowOnlySelected Btn_Zero Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tTotGross tTotNet tShowNonDefault ~
tShowOnlySelected 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD calcTotals Dialog-Frame 
FUNCTION calcTotals RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "No Endorsements will be added to the Batch"
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Add the Endorsements marked Yes to the Batch"
     BGCOLOR 8 .

DEFINE BUTTON Btn_Zero 
     LABEL "Zero All" 
     SIZE 15 BY 1.14 TOOLTIP "Zero values such as for a ~"bundled~" rate policy"
     BGCOLOR 8 .

DEFINE VARIABLE tTotGross AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tTotNet AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE tShowNonDefault AS LOGICAL INITIAL no 
     LABEL "Show Non-Default Endorsements" 
     VIEW-AS TOGGLE-BOX
     SIZE 48 BY .81 NO-UNDO.

DEFINE VARIABLE tShowOnlySelected AS LOGICAL INITIAL no 
     LABEL "Show Only Selected Endorsements" 
     VIEW-AS TOGGLE-BOX
     SIZE 48 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      defaultform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData Dialog-Frame _FREEFORM
  QUERY brwData DISPLAY
      defaultform.seq format ">>>9" label "Seq" width 6
 defaultform.include column-label "Add to Batch" view-as toggle-box
 defaultform.statcode label "STAT Code"
 defaultform.formID format "x(15)" label "FormID"
 defaultform.description format "x(100)" label "Description"
 defaultform.grossPremium label "Gross" format "zzz,zz9.99"
 defaultform.netPremium label "Net" format "zzz,zz9.99"

 enable include grossPremium netPremium
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 175.8 BY 15.43 ROW-HEIGHT-CHARS .71.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     brwData AT ROW 1.33 COL 2.2 WIDGET-ID 200
     tTotGross AT ROW 16.95 COL 161.8 RIGHT-ALIGNED NO-LABEL WIDGET-ID 6
     tTotNet AT ROW 16.95 COL 173.6 RIGHT-ALIGNED NO-LABEL WIDGET-ID 4
     tShowNonDefault AT ROW 17.19 COL 3 WIDGET-ID 10
     tShowOnlySelected AT ROW 18.14 COL 3 WIDGET-ID 12
     Btn_Zero AT ROW 18.38 COL 159.8 WIDGET-ID 2
     Btn_OK AT ROW 18.43 COL 72
     Btn_Cancel AT ROW 18.48 COL 92.2
     "Totals:" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 17.19 COL 143 WIDGET-ID 8
     SPACE(29.19) SKIP(2.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Endorsements"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData TEXT-1 Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tTotGross IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       tTotGross:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* SETTINGS FOR FILL-IN tTotNet IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       tTotNet:READ-ONLY IN FRAME Dialog-Frame        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH defaultform by defaultform.formID.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Endorsements */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData Dialog-Frame
ON ROW-DISPLAY OF brwData IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData Dialog-Frame
ON START-SEARCH OF brwData IN FRAME Dialog-Frame
DO:
  if self:current-column:name = "include"
   then
    do:
      std-lo = can-find(first defaultform where not(defaultform.include)). 
      for each defaultform exclusive-lock:
        defaultform.include = std-lo.
      end.
      self:refresh(). 
    end.
   else
    do:
      {lib/brw-startSearch.i}
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Save */
DO:
  apply "go" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Zero
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Zero Dialog-Frame
ON CHOOSE OF Btn_Zero IN FRAME Dialog-Frame /* Zero All */
DO:
  close query brwData.
  for each defaultform:
    defaultform.grossPremium = 0.
    defaultform.netPremium = 0.
  end.
  calcTotals().
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tShowNonDefault
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tShowNonDefault Dialog-Frame
ON VALUE-CHANGED OF tShowNonDefault IN FRAME Dialog-Frame /* Show Non-Default Endorsements */
DO:
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tShowOnlySelected
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tShowOnlySelected Dialog-Frame
ON VALUE-CHANGED OF tShowOnlySelected IN FRAME Dialog-Frame /* Show Only Selected Endorsements */
DO:
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

{lib/brw-main.i}

ON 'value-changed':U OF include in browse {&browse-name}
DO:
  get current {&browse-name}.
  assign
    defaultform.include = defaultform.include:checked in browse {&browse-name}
    defaultform.grossPremium
    defaultform.netPremium.
  run filterData.
  calcTotals().
END.

ON 'leave':U OF grossPremium in browse {&browse-name}
DO:
  find first agent no-error.
  if agent.remitType = "P"
/*     and netPremium:input-value in browse {&browse-name} = 0  */
   then
    do: std-de = round(self:input-value * agent.remitValue, 2).
        netPremium:screen-value in browse {&browse-name} = string(std-de).
    end.

  self:bgcolor = ?.

  find stateform
    where stateform.stateID = pStateID
      and stateform.formID = defaultform.formID no-error.
  if available stateform
    and stateform.rateCheck = "F"
    and (self:input-value < stateform.rateMin
      or self:input-value > stateform.rateMax)
   then self:bgcolor = 14.
  if available stateform
    and stateform.rateCheck = "L"
    and (self:input-value < ((pLiability / 1000) * stateform.rateMin)
      or self:input-value > ((pLiability / 1000) * stateform.rateMax))
   then self:bgcolor = 14.
   
  get current {&browse-name}.
  assign 
    defaultform.grossPremium
    defaultform.netPremium.
  calcTotals().
END.

ON 'leave':U OF netPremium in browse {&browse-name}
DO:
  find first agent no-error.
  if agent.remitType = "P"
   and agent.remitValue > 0
   and grossPremium:input-value in browse {&browse-name} = 0 
  then
  do: std-de = round(self:input-value / agent.remitValue, 2).
      grossPremium:screen-value in browse {&browse-name} = string(std-de).
  end.
  
  get current {&browse-name}.
  assign 
    defaultform.grossPremium
    defaultform.netPremium.
  calcTotals().
END.

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

run getData in this-procedure.
calcTotals().

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  apply 'entry' to browse {&browse-name}.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  pSave = true.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tTotGross tTotNet tShowNonDefault tShowOnlySelected 
      WITH FRAME Dialog-Frame.
  ENABLE brwData tTotGross tTotNet tShowNonDefault tShowOnlySelected Btn_Zero 
         Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData Dialog-Frame 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("seq").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData Dialog-Frame 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

close query {&browse-name}.
dataSortBy = "".
dataSortDesc = no.
run sortData ("seq").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var tWhereClause as char no-undo.

do with frame {&frame-name}:

  tWhereClause = "where " +
                 (if tShowNonDefault:checked = false
                 then "defaultform.type = 'D' "
                 else "true ") +
                 "and " + 
                 (if tShowOnlySelected:checked = true
                 then "defaultform.include = true "
                 else "true ").
  
  {lib/brw-sortData.i &pre-by-clause="tWhereClause +"}
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION calcTotals Dialog-Frame 
FUNCTION calcTotals RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  def buffer xdefaultform for defaultform.

  do with frame {&frame-name}:
    assign
      tTotGross = 0
      tTotNet   = 0.
  
    for each xdefaultform where xdefaultform.include = true:
      assign
        tTotGross = tTotGross + xdefaultform.grossPremium
        tTotNet   = tTotNet + xdefaultform.netPremium.
    end.
    
    tTotGross:screen-value = string(tTotGross).
    tTotNet:screen-value = string(tTotNet).
  end.
  
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

