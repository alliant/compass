&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wperioddetails.w
   Window of batches for a PERIOD
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/state.i}
{tt/defaultform.i}
{tt/stateform.i}

{lib/std-def.i}

def var hData as handle no-undo.

def var tStatus as char no-undo.
{lib/winlaunch.i}

/* This variable needs to be longchar since the agent list can be rather 
   lengthy.  A regular 'char' variable was causing error 12371 (attempt to
   update data exceeding 32000).  See also the 'AgentComboSets' function. */
def var tAgentsList as longchar no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES defaultform

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData defaultform.seq defaultform.formID defaultform.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH defaultform
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH defaultform.
&Scoped-define TABLES-IN-QUERY-brwData defaultform
&Scoped-define FIRST-TABLE-IN-QUERY-brwData defaultform


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fType fState fPolicy fForm brwData 
&Scoped-Define DISPLAYED-OBJECTS fType fState fAgent fPolicy fForm 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getData C-Win 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setButtons C-Win 
FUNCTION setButtons RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setForms C-Win 
FUNCTION setForms RETURNS CHARACTER PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd  NO-FOCUS
     LABEL "Add" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add a new default endorsement".

DEFINE BUTTON bDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete the selected endorsement".

DEFINE BUTTON bDown  NO-FOCUS
     LABEL "Down" 
     SIZE 7.2 BY 1.71 TOOLTIP "Move the selected endorsement down in sequence".

DEFINE BUTTON bUp  NO-FOCUS
     LABEL "Up" 
     SIZE 7.2 BY 1.71 TOOLTIP "Move the selected endorsement up in sequence".

DEFINE VARIABLE fAgent AS CHARACTER 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","N"
     DROP-DOWN AUTO-COMPLETION
     SIZE 66 BY 1 NO-UNDO.

DEFINE VARIABLE fForm AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "None","N"
     DROP-DOWN-LIST
     SIZE 95.4 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Colorado","CO"
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE fPolicy AS CHARACTER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Lender", "L",
"Owner", "O"
     SIZE 22 BY .86 NO-UNDO.

DEFINE VARIABLE fType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "State", "State",
"Agent", "Agent"
     SIZE 10.6 BY 2.52 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88 BY 4.95.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 106.4 BY 4.95.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      defaultform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      defaultform.seq format ">9" label "Seq" width 6
 defaultform.formID format "x(15)" label "FormID"
 defaultform.description format "x(100)" label "Description"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS NO-SCROLLBAR-VERTICAL SIZE 195 BY 10.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     fType AT ROW 1.62 COL 11.8 NO-LABEL WIDGET-ID 28
     bAdd AT ROW 1.76 COL 188 WIDGET-ID 36 NO-TAB-STOP 
     fState AT ROW 1.76 COL 20.8 COLON-ALIGNED NO-LABEL WIDGET-ID 34
     bDelete AT ROW 4.48 COL 91.4 WIDGET-ID 42 NO-TAB-STOP 
     fAgent AT ROW 3.05 COL 20.8 COLON-ALIGNED NO-LABEL WIDGET-ID 72
     bDown AT ROW 4.48 COL 106.2 WIDGET-ID 38 NO-TAB-STOP 
     fPolicy AT ROW 4.67 COL 11.8 NO-LABEL WIDGET-ID 78
     bUp AT ROW 4.48 COL 98.8 WIDGET-ID 40 NO-TAB-STOP 
     fForm AT ROW 2.19 COL 89.6 COLON-ALIGNED NO-LABEL WIDGET-ID 84
     brwData AT ROW 6.43 COL 2 WIDGET-ID 200
     "Select:" VIEW-AS TEXT
          SIZE 6.8 BY .62 AT ROW 1.91 COL 4.2 WIDGET-ID 76
     "Type:" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 4.71 COL 5.4 WIDGET-ID 82
     "View" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.1 COL 3.8 WIDGET-ID 86
          FONT 6
     "Actions" VIEW-AS TEXT
          SIZE 9.2 BY .62 AT ROW 1.14 COL 92.2 WIDGET-ID 90
          FONT 6
     RECT-36 AT ROW 1.43 COL 2 WIDGET-ID 74
     RECT-37 AT ROW 1.43 COL 90.6 WIDGET-ID 88
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 196.8 BY 16.62 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Default Endorsements"
         HEIGHT             = 16.62
         WIDTH              = 196.8
         MAX-HEIGHT         = 16.76
         MAX-WIDTH          = 197.4
         VIRTUAL-HEIGHT     = 16.76
         VIRTUAL-WIDTH      = 197.4
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData fForm fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

/* SETTINGS FOR BUTTON bAdd IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDelete IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDown IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bUp IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fAgent IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-36 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH defaultform.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Default Endorsements */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Default Endorsements */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Default Endorsements */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd C-Win
ON CHOOSE OF bAdd IN FRAME fMain /* Add */
DO:
  run ActionAdd in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete C-Win
ON CHOOSE OF bDelete IN FRAME fMain /* Delete */
DO:
  run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDown
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDown C-Win
ON CHOOSE OF bDown IN FRAME fMain /* Down */
DO:
  run ActionDown in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  setButtons().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUp C-Win
ON CHOOSE OF bUp IN FRAME fMain /* Up */
DO:
  run ActionUp in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAgent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAgent C-Win
ON VALUE-CHANGED OF fAgent IN FRAME fMain
DO:
  getData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fPolicy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fPolicy C-Win
ON VALUE-CHANGED OF fPolicy IN FRAME fMain
DO:
  getData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain
DO:
  getData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fType C-Win
ON VALUE-CHANGED OF fType IN FRAME fMain
DO:
 run AgentComboEnable (self:screen-value = "Agent").
 if self:screen-value = "State" 
  then assign
         fState:sensitive in frame fMain = true
         fAgent:sensitive in frame fMain = false
         .
  else assign 
         fState:sensitive in frame fMain = false
         fAgent:sensitive in frame fMain = true
         .
 getData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = {&window-name}:height-pixels.
{&window-name}:max-width-pixels = {&window-name}:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

status default "" in window {&window-name}.
status input off.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bAdd:load-image("images/add.bmp").
bAdd:load-image-insensitive("images/add-i.bmp").
bDelete:load-image("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").
bUp:load-image("images/up.bmp").
bUp:load-image-insensitive("images/up-i.bmp").
bDown:load-image("images/down.bmp").
bDown:load-image-insensitive("images/down-i.bmp").

publish "GetStateForms" (output table stateform).
fAgent:delimiter = {&msg-dlm}.

{lib/get-state-list.i &combo=fState}
{lib/get-agent-list.i &combo=fAgent}

/* run getData in this-procedure. */

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

apply "VALUE-CHANGED" to fType.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionAdd C-Win 
PROCEDURE actionAdd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 std-lo = false.
 if fForm:screen-value in frame fMain = ? 
  then
   do:
       MESSAGE "Please select a Form"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
       return.
   end.

 if fType:screen-value in frame fMain = "State" 
  then publish "NewDefaultEndorsement" ("S",
                                        fState:screen-value in frame fMain,
                                        fPolicy:screen-value in frame fMain,
                                        fForm:screen-value in frame fMain,
                                        output std-lo,
                                        output std-ch).
  else publish "NewDefaultEndorsement" ("A",
                                        fAgent:screen-value in frame fMain,
                                        fPolicy:screen-value in frame fMain,
                                        fForm:screen-value in frame fMain,
                                        output std-lo,
                                        output std-ch).

 if not std-lo 
  then
   do:
      MESSAGE std-ch
       VIEW-AS ALERT-BOX error BUTTONS OK.
      return.
   end.
 getData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDelete C-Win 
PROCEDURE actionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 if not available defaultForm 
  then 
   do: setButtons().
       return.
   end.

 if fType:screen-value in frame fMain = "State" 
  then publish "DeleteDefaultEndorsement" ("S",
                                           fState:screen-value in frame fMain,
                                           fPolicy:screen-value in frame fMain,
                                           defaultForm.seq,
                                           output std-lo,
                                           output std-ch).
  else publish "DeleteDefaultEndorsement" ("A",
                                           fAgent:screen-value in frame fMain,
                                           fPolicy:screen-value in frame fMain,
                                           defaultForm.seq,
                                           output std-lo,
                                           output std-ch).
 if not std-lo
  then
   do: 
       MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 getData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionDown C-Win 
PROCEDURE actionDown :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available defaultForm 
  then 
   do: setButtons().
       return.
   end.

 if fType:screen-value in frame fMain = "State" 
  then publish "MoveDefaultEndorsement" ("S",
                                         fState:screen-value in frame fMain,
                                         fPolicy:screen-value in frame fMain,
                                         defaultForm.seq,
                                         "DOWN",
                                         output std-lo,
                                         output std-ch).
  else publish "MoveDefaultEndorsement" ("A",
                                         fAgent:screen-value in frame fMain,
                                         fPolicy:screen-value in frame fMain,
                                         defaultForm.seq,
                                         "DOWN",
                                         output std-lo,
                                         output std-ch).
 if not std-lo
  then
   do: 
       MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 getData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionUp C-Win 
PROCEDURE actionUp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if not available defaultForm 
  then 
   do: setButtons().
       return.
   end.

 std-lo = false.
 std-ch = "".
 if fType:screen-value in frame fMain = "State" 
  then publish "MoveDefaultEndorsement" ("S",
                                         fState:screen-value in frame fMain,
                                         fPolicy:screen-value in frame fMain,
                                         defaultForm.seq,
                                         "UP",
                                         output std-lo,
                                         output std-ch).
  else publish "MoveDefaultEndorsement" ("A",
                                         fAgent:screen-value in frame fMain,
                                         fPolicy:screen-value in frame fMain,
                                         defaultForm.seq,
                                         "UP",
                                         output std-lo,
                                         output std-ch).
 if not std-lo
  then
   do: 
       MESSAGE std-ch
        VIEW-AS ALERT-BOX error BUTTONS OK.
       return.
   end.

 getData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fType fState fAgent fPolicy fForm 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE fType fState fPolicy fForm brwData 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwData:width-pixels = frame fmain:width-pixels - 10.
 brwData:height-pixels = frame fMain:height-pixels - 58.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getData C-Win 
FUNCTION getData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwData.
 empty temp-table defaultform.

 if fType:screen-value in frame fMain = "State" 
  then publish "GetDefaultEndorsements" ("",    /* Blank for Agent */
                                         fState:screen-value in frame fMain,
                                         fPolicy:screen-value in frame fMain,
                                         output table defaultform).
  else publish "GetDefaultEndorsements" (fAgent:screen-value in frame fMain,
                                         "",    /* Blank for State */
                                         fPolicy:screen-value in frame fMain,
                                         output table defaultform).

 /* Filter out non-default forms because 'GetDefaultEndorsements' will
    also return non-default forms for use with the processing screen */
 for each defaultform where defaultform.type <> "D":
  delete defaultform.
 end.

/*  open query brwData preselect each defaultform by defaultform.seq. */

 if fType:screen-value = "State" 
  then tStatus = fState:screen-value in frame fMain
                    + "/" + fPolicy:screen-value in frame fMain
                    + "  (" + string(now) + ")".
  else tStatus = fAgent:screen-value in frame fMain
                    + "/" + fPolicy:screen-value in frame fMain
                    + "  (" + string(now) + ")".
 status default tStatus in window {&window-name}.
 status input tStatus in window {&window-name}.

 dataSortBy = "".
 dataSortDesc = no.
 run sortData("seq").

 setButtons().
 setForms().
 RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setButtons C-Win 
FUNCTION setButtons RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 bAdd:sensitive in frame fMain = (query brwData:num-results < 10). 
 bDelete:sensitive in frame fMain = available defaultform.
 bUp:sensitive in frame fMain = (available defaultform and defaultform.seq > 1).
 bDown:sensitive in frame fMain = (available defaultform and defaultform.seq < query brwData:num-results).

 RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setForms C-Win 
FUNCTION setForms RETURNS CHARACTER PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 def buffer stateform for stateform.
 def var tStateID as char no-undo.

 if fType:screen-value in frame fMain = "State" 
  then tStateID = fState:screen-value in frame fMain.
  else
  do: find agent 
        where agent.agentID = fAgent:screen-value in frame fMain no-error.
      if not available agent 
       then tStateID = "".
       else tStateID = agent.stateID.
  end.

 std-ch = "".
 for each stateform 
   where stateform.stateID = tStateID
     and stateform.formType = "E"
     and (stateform.insuredType = fPolicy:screen-value in frame fMain
       or stateform.insuredType = "B")
   by stateform.formID:
  if can-find(defaultForm
                where defaultform.formID = stateform.formID) 
   then next.
  std-ch = std-ch + "," + replace(stateform.formID, ",", " ")
                                + "  :  "
                                + replace(stateform.description, ",", " ")
                                + " (" + stateform.insuredType + ")"
                                + "," + replace(stateform.formID, ",", " ")
                                .
 end.
 std-ch = trim(std-ch, ",").
 if std-ch = "" 
  then std-ch = "Select,None".
 fForm:list-item-pairs in frame fMain = std-ch.
 fForm = entry(2, std-ch).

 RETURN std-ch.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

