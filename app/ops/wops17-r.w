&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wops17-r.w
   Window of Reprocessed Forms report
   Created 2.27.2015 D.Sinclair
*/

CREATE WIDGET-POOL.

{tt/agent.i}
{tt/period.i}
{tt/batchform.i}

{lib/std-def.i}

def var hData as handle no-undo.

def var tStatus as char no-undo.
def var tNumTasks as int no-undo.
def var tCurTask as int no-undo.

def var tGettingData as logical.
def var tKeepGettingData as logical.

def var sPeriodID as int no-undo.



define menu brwpopmenu title "Actions"
 menu-item m_PopViewPolicy label "View Policy"
 menu-item m_PopViewBatch label "View Batch"
 .

ON 'choose':U OF menu-item m_PopViewPolicy in menu brwpopmenu
DO:
 run viewPolicy in this-procedure.
 RETURN.
END.

ON 'choose':U OF menu-item m_PopViewBatch in menu brwpopmenu
DO:
 run viewBatch in this-procedure.
 RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES batchform

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData batchform.batchID batchform.seq batchform.fileNumber batchform.policyID batchform.formType batchform.formID batchform.rateCode batchform.statCode batchform.liabilityDelta batchform.grossDelta batchform.netDelta batchform.retentionDelta   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH batchform
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH batchform.
&Scoped-define TABLES-IN-QUERY-brwData batchform
&Scoped-define FIRST-TABLE-IN-QUERY-brwData batchform


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData tPolCount bRefresh bExport sMonth ~
sYear tPolLiability tPolGross tPolNet tPolRetention tEndCount tEndGross ~
tEndNet tEndRetention tEndLiability RECT-36 RECT-38 
&Scoped-Define DISPLAYED-OBJECTS tPolCount sMonth sYear tEndCount 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setReportButtons C-Win 
FUNCTION setReportButtons RETURNS LOGICAL PRIVATE
  ( input pCmd as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport 
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV file".

DEFINE BUTTON bRefresh 
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE sMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     LIST-ITEM-PAIRS "January",1,
                     "February",2,
                     "March",3,
                     "April",4,
                     "May",5,
                     "June",6,
                     "July",7,
                     "August",8,
                     "September",9,
                     "October",10,
                     "November",11,
                     "December",12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tEndCount AS INTEGER FORMAT "-zzz,zzz,zzz,zz9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndGross AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndLiability AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndNet AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tEndRetention AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolCount AS INTEGER FORMAT "-zzz,zzz,zzz,zz9":U INITIAL 0 
     LABEL "Form Count" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolGross AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolLiability AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolNet AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE tPolRetention AS DECIMAL FORMAT "-zzz,zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Retention Premium" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57 BY 5.71.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 83 BY 5.71.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      batchform SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      batchform.batchID label "Batch ID" format "zzzzzzzzz"
batchform.seq label "Seq"
batchform.fileNumber label "File" format "x(50)" width 20
batchform.policyID label "Policy" format "zzzzzzzzz"
batchform.formType column-label "Form!Type" format "x(7)"
batchform.formID column-label "Form ID" format "x(20)" width 10
batchform.rateCode column-label "Rate!Code" format "x(20)" width 10
batchform.statCode column-label "STAT!Code" format "x(20)" width 10
batchform.liabilityDelta column-label "Liability" format "->>>,>>>,>>>,>>9.99"
batchform.grossDelta column-label "Gross!Premium" format "->>>,>>>,>>>,>>9.99"
batchform.netDelta column-label "Net!Premium" format "->>>,>>>,>>>,>>9.99"
batchform.retentionDelta column-label "Retention!Premium" format "->>>,>>>,>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 181.8 BY 20.57 ROW-HEIGHT-CHARS .81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 7.43 COL 2 WIDGET-ID 200
     tPolCount AT ROW 2.19 COL 82 COLON-ALIGNED WIDGET-ID 86 NO-TAB-STOP 
     bRefresh AT ROW 2.95 COL 42 WIDGET-ID 4
     bExport AT ROW 2.95 COL 49.2 WIDGET-ID 2
     sMonth AT ROW 3.33 COL 10 COLON-ALIGNED WIDGET-ID 64
     sYear AT ROW 3.33 COL 27 COLON-ALIGNED NO-LABEL WIDGET-ID 66
     tPolLiability AT ROW 3.14 COL 82 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tPolGross AT ROW 4.1 COL 82 COLON-ALIGNED WIDGET-ID 88 NO-TAB-STOP 
     tPolNet AT ROW 5.05 COL 82 COLON-ALIGNED WIDGET-ID 90 NO-TAB-STOP 
     tPolRetention AT ROW 6 COL 82 COLON-ALIGNED WIDGET-ID 92 NO-TAB-STOP 
     tEndCount AT ROW 2.19 COL 112 COLON-ALIGNED NO-LABEL WIDGET-ID 94 NO-TAB-STOP 
     tEndGross AT ROW 4.1 COL 112 COLON-ALIGNED NO-LABEL WIDGET-ID 96 NO-TAB-STOP 
     tEndNet AT ROW 5.05 COL 112 COLON-ALIGNED NO-LABEL WIDGET-ID 98 NO-TAB-STOP 
     tEndRetention AT ROW 6 COL 112 COLON-ALIGNED NO-LABEL WIDGET-ID 100 NO-TAB-STOP 
     tEndLiability AT ROW 3.14 COL 112 COLON-ALIGNED NO-LABEL WIDGET-ID 108 NO-TAB-STOP 
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.19 COL 3 WIDGET-ID 56
     "Policies" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.57 COL 94 WIDGET-ID 104
     "Endorsements/Other" VIEW-AS TEXT
          SIZE 21 BY .62 AT ROW 1.57 COL 117.6 WIDGET-ID 106
     "Totals" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.19 COL 61 WIDGET-ID 112
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-38 AT ROW 1.48 COL 60 WIDGET-ID 110
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 183.8 BY 27.24 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Reprocessed Forms"
         HEIGHT             = 27.24
         WIDTH              = 183.8
         MAX-HEIGHT         = 47.48
         MAX-WIDTH          = 384
         VIRTUAL-HEIGHT     = 47.48
         VIRTUAL-WIDTH      = 384
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

ASSIGN 
       tEndCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndNet:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tEndRetention IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tEndRetention:READ-ONLY IN FRAME fMain        = TRUE.

ASSIGN 
       tPolCount:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolGross IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolGross:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolLiability IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolLiability:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolNet IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolNet:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolRetention IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolRetention:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH batchform.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 4.52
       COLUMN          = 12
       HEIGHT          = .48
       WIDTH           = 29
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(brwData:HANDLE IN FRAME fMain).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Reprocessed Forms */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Reprocessed Forms */
DO:
  if tGettingData 
   then return no-apply.

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Reprocessed Forms */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  assign
    sMonth 
    sYear
    sPeriodID = 0
    .
  
  find first period where period.periodMonth = sMonth:input-value
                    and period.periodYear = sYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Starting period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  sPeriodID = period.periodID.
  
  setReportButtons("Disable").
  run getData in this-procedure.
  setReportButtons("Enable").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  run viewPolicy in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sMonth C-Win
ON VALUE-CHANGED OF sMonth IN FRAME fMain /* Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain
DO:
 clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

browse brwData:POPUP-MENU = MENU brwpopmenu:HANDLE.

subscribe to "DataError" anywhere.

publish "GetPeriods" (output table period).
std-ch = "".
for each period
 break by period.periodYear:
 if not first-of(period.periodYear) 
  then next.
 std-ch = std-ch + (if std-ch > "" then "," else "") + string(period.periodYear).
end.
if std-ch = "" 
 then std-ch = string(year(today)).
sYear:list-items in frame fMain = std-ch.
 
std-ch = "".
publish "GetCurrentValue" ("PeriodYear", output std-ch).
if std-ch = "" 
 then std-ch = string(year(today)).
sYear = int(std-ch).

std-ch = "".
publish "GetCurrentValue" ("PeriodMonth", output std-ch).
if std-ch = "" 
 then std-ch = string(month(today)).
sMonth = int(std-ch).


session:immediate-display = yes.

run windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
  clearData().
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wops17-r.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wops17-r.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input param pMsg as char.

  /* Since Progress is single-threaded, this works */
  if not tGettingData 
   then return.

  tKeepGettingData = false.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the systems administrator if the problem persists."
    view-as alert-box error.

  clearData().
  
  setReportButtons("Enable").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY tPolCount sMonth sYear tEndCount 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData tPolCount bRefresh bExport sMonth sYear tPolLiability 
         tPolGross tPolNet tPolRetention tEndCount tEndGross tEndNet 
         tEndRetention tEndLiability RECT-36 RECT-38 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 th = temp-table batchform:handle.

 if query {&browse-name}:num-results = ? 
 or query {&browse-name}:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.
   
 publish "GetReportDir" (output std-ch).

 run util/exporttable.p (table-handle th,
                         "batchform",
                         "for each batchform by batchform.batchID by batchform.seq ",
                         "batchID,seq,fileNumber,policyID,formType,formID,rateCode,statCode,liabilityDelta,grossDelta,netDelta,retentionDelta",
                         "BatchID,Seq,FileNumber,PolicyID,FormType,FormID,RateCode,STATCode,Liability,GrossPremium,NetPremium,RetentionPremium",
                         std-ch,
                         "ReprocessedForms_"
                         + string(sPeriodID)  
                         + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def buffer batchform for batchform.
  
hide message no-pause.
message "Filtering data...".

do with frame {&frame-name}:
  
  assign
    tPolCount = 0
    tPolLiability = 0
    tPolGross = 0
    tPolNet = 0
    tPolRetention = 0
    tEndCount = 0
    tEndLiability = 0
    tEndGross = 0
    tEndNet = 0
    tEndRetention = 0.
  
  for each batchform no-lock:
   
    process events.
    
    if batchform.formType = "P" 
     then assign
              tPolCount = tPolCount + 1
              tPolLiability = tPolLiability + batchform.liabilityDelta
              tPolGross = tPolGross + batchform.grossDelta
              tPolNet = tPolNet + batchform.netDelta
              tPolRetention = tPolRetention + batchform.retentionDelta.
     else assign
              tEndCount = tEndCount + 1
              tEndLiability = tEndLiability + batchform.liabilityDelta
              tEndGross = tEndGross + batchform.grossDelta
              tEndNet = tEndNet + batchform.netDelta
              tEndRetention = tEndRetention + batchform.retentionDelta.
  end.
end.
 
displayTotals().
 
dataSortBy = "".
dataSortDesc = yes.
run sortData ("batchID").

hide message no-pause.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer batchform for batchform.

 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.
 
 close query brwData.
 empty temp-table batchform.
 clearData().

 tNumTasks = 2.
 tCurTask = 1.
 pbMinStatus().

 tGettingData = true.
 tkeepGettingData = true.

 hide message no-pause.
 message "Getting data...".

 GETTING-DATA:
 do with frame {&frame-name}:
   pbUpdateStatus(tCurTask, 0).
   
   run server/getreprocessedforms.p (sPeriodID,
                                     output table batchform,
                                     output tSuccess,
                                     output tMsg).
    if not tSuccess
     then 
      do: std-lo = false.
          publish "GetAppDebug" (output std-lo).
          if std-lo 
           then message "GetReprocessedForms failed: " tMsg view-as alert-box warning.
      end.
  
   process events.
      
   if not tKeepGettingData 
    then leave GETTING-DATA.
   
   pbUpdateStatus(int((tCurTask / tNumTasks) * 100), 0).
 end. /* GETTING-DATA */
 
 if not tKeepGettingData 
  then
   do:
       close query brwData.
       empty temp-table batchform.
       clearData().

       hide message no-pause.
       message "Cancelled (" + string(now,"99/99/99 HH:MM:SS") + ")".
       pbUpdateStatus(100, 1).
       pbMinStatus().
       tGettingData = false.
       return.
   end.
 
 run filterData.

 pbUpdateStatus(100, 1).
 pbMinStatus().
 tGettingData = false. 
 hide message no-pause.
 message "Report Complete for Period " + 
         sMonth:screen-value in frame fMain + "/" +
         sYear:screen-value in frame fMain + "  " +
         "(" + string(now,"99/99/99 HH:MM:SS") + ")".
 message.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i &post-by-clause="+ ' by batchform.seq '" }
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewBatch C-Win 
PROCEDURE viewBatch PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available batchform and batchform.batchID <> 0
   then
    do: 
        publish "SetCurrentValue" ("BatchID", string(batchform.batchID)).
        run wops01-r.w persistent.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewPolicy C-Win 
PROCEDURE viewPolicy PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if available batchform and batchform.policyID <> 0
   then
    do: 
        publish "SetCurrentValue" ("PolicyID", string(batchform.policyID)).
        run wpolicy.w persistent.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    
    close query {&browse-name}.
    hide message no-pause.
    
    assign
      tPolCount = 0
      tPolLiability = 0
      tPolGross = 0
      tPolNet = 0
      tPolRetention = 0
      tEndCount = 0
      tEndLiability = 0
      tEndGross = 0
      tEndNet = 0
      tEndRetention = 0.
      
    display
      tPolCount
      tPolLiability
      tPolGross
      tPolNet
      tPolRetention
      tEndCount
      tEndLiability
      tEndGross
      tEndNet
      tEndRetention
      .
  end.
                  
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION displayTotals C-Win 
FUNCTION displayTotals RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 
do with frame {&frame-name}:
  
  display
    tPolCount
    tPolLiability
    tPolGross
    tPolNet
    tPolRetention
    tEndCount
    tEndLiability
    tEndGross
    tEndNet
    tEndRetention
    .
  
  pause 0.
end.

RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

  {&WINDOW-NAME}:move-to-top().
  
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setReportButtons C-Win 
FUNCTION setReportButtons RETURNS LOGICAL PRIVATE
  ( input pCmd as char ) :

  do with frame {&frame-name}:
    case pCmd:
      when "Enable" then
      assign
        bRefresh:sensitive = yes
        bExport:sensitive  = yes.
      when "Disable" then
      assign
        bRefresh:sensitive = no
        bExport:sensitive  = no.
      otherwise
        .
    end case.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

