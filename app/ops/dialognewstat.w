&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fNew 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
def input-output parameter pStateID as char.
def input-output parameter pStatCode as char.
def input-output parameter pFormID as char.
def input-output parameter pDescription as char.
def input-output parameter pRegulation as char.
def input-output parameter pType as char.
def input-output parameter pActive as logical.

def output parameter pCancel as logical init true.

/* Local Variable Definitions ---                                       */

{lib/std-def.i}
def var tName as char no-undo.

{tt/stateform.i}
{tt/state.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fNew

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tState tStatCode tDescription tType tFormID ~
tRegulation Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tState tStatCode tDescription tType ~
tFormID tRegulation 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFormsCombo fNew 
FUNCTION getFormsCombo RETURNS CHARACTER PRIVATE
 ( )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tFormID AS CHARACTER INITIAL "NONE" 
     LABEL "Default Form" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN AUTO-COMPLETION
     SIZE 115 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "Select","NONE"
     DROP-DOWN
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 115 BY 1 TOOLTIP "STAT Code description" NO-UNDO.

DEFINE VARIABLE tRegulation AS CHARACTER FORMAT "X(256)":U 
     LABEL "Regulation" 
     VIEW-AS FILL-IN 
     SIZE 40.2 BY 1 TOOLTIP "Regulation specifying/requiring the STAT Code" NO-UNDO.

DEFINE VARIABLE tStatCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "STAT Code" 
     VIEW-AS FILL-IN 
     SIZE 23.6 BY 1 NO-UNDO.

DEFINE VARIABLE tType AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Policy", "P",
"Endorsement", "E",
"CPL", "C",
"Commitment", "T"
     SIZE 16 BY 3.57 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 13.4 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fNew
     tState AT ROW 1.48 COL 16.2 COLON-ALIGNED WIDGET-ID 104
     tStatCode AT ROW 1.48 COL 47.4 COLON-ALIGNED WIDGET-ID 102
     tDescription AT ROW 2.67 COL 16.2 COLON-ALIGNED WIDGET-ID 90
     tType AT ROW 4 COL 18.6 NO-LABEL WIDGET-ID 14
     tFormID AT ROW 7.81 COL 16.2 COLON-ALIGNED WIDGET-ID 80
     tActive AT ROW 9.1 COL 18.6 WIDGET-ID 98
     tRegulation AT ROW 3.86 COL 91 COLON-ALIGNED WIDGET-ID 92
     Btn_OK AT ROW 10 COL 53.8
     Btn_Cancel AT ROW 10 COL 71.8
     "Type:" VIEW-AS TEXT
          SIZE 6.2 BY .62 TOOLTIP "Select the type of form for this STAT Code" AT ROW 4.1 COL 12.2 WIDGET-ID 96
     SPACE(117.79) SKIP(7.27)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Create STAT Code"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fNew
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fNew:SCROLLABLE       = FALSE
       FRAME fNew:HIDDEN           = TRUE.

/* SETTINGS FOR TOGGLE-BOX tActive IN FRAME fNew
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tActive:HIDDEN IN FRAME fNew           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fNew fNew
ON WINDOW-CLOSE OF FRAME fNew /* Create STAT Code */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState fNew
ON VALUE-CHANGED OF tState IN FRAME fNew /* State */
DO:
 tFormID:list-item-pairs in frame fNew = getFormsCombo().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tType fNew
ON VALUE-CHANGED OF tType IN FRAME fNew
DO:
 tFormID:list-item-pairs in frame fNew = getFormsCombo().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fNew 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetStateForms" (output table stateform).
{lib/get-state-list.i &combo=tState}
if pStateID = "" 
 then
  do:
      publish "GetCurrentValue" ("ViewState", output std-ch).
      if index(tState:list-item-pairs in frame fNew, std-ch) > 0 
       then pStateID = std-ch.
  end.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
if lookup(pType, "P,E,C,T") = 0 
 then pType = "P".
if pFormID = "" 
 then tFormID = "NONE".
 else tFormID = pFormID.

assign
  tState = pStateID
  tStatCode = pStatCode
  tDescription = pDescription
  tType = pType
  tRegulation = pRegulation
  tActive = pActive
  .

RUN enable_UI.

if lookup(tState, tState:list-item-pairs in frame fNew) > 0
 then tState:screen-value in frame fNew = tState.
tFormID:list-item-pairs in frame fNew = getFormsCombo().
if lookup(tFormID, tFormID:list-item-pairs in frame fNew) > 0
 then tFormID:screen-value in frame fNew = tFormID.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if tStatCode:screen-value in frame fNew = "" 
   then
    do:
        MESSAGE "STAT Code cannot be blank"
         VIEW-AS ALERT-BOX error BUTTONS OK.
        next.
    end.

  if tFormID:screen-value in frame fNew = "NONE" 
   then pFormID = "".
   else pFormID = tFormID:screen-value in frame fNew.

  assign
    pStateID = tState:screen-value in frame fNew
    pStatcode = tStatcode:screen-value in frame fNew
    pDescription = tDescription:screen-value in frame fNew
    pRegulation = tRegulation:screen-value in frame fNew
    pType = tType:screen-value in frame fNew
    pActive = tActive:checked in frame fNew
    pCancel = false
    .
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fNew  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fNew.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fNew  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tState tStatCode tDescription tType tFormID tRegulation 
      WITH FRAME fNew.
  ENABLE tState tStatCode tDescription tType tFormID tRegulation Btn_OK 
         Btn_Cancel 
      WITH FRAME fNew.
  VIEW FRAME fNew.
  {&OPEN-BROWSERS-IN-QUERY-fNew}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFormsCombo fNew 
FUNCTION getFormsCombo RETURNS CHARACTER PRIVATE
 ( ) :
/*------------------------------------------------------------------------------
 Purpose:  
   Notes:  
------------------------------------------------------------------------------*/
 def buffer stateform for stateform.
 
 std-ch = "".
 for each stateform 
   where stateform.stateID = tState:screen-value in frame fNew
     and stateform.formType = tType:screen-value in frame fNew
   by stateform.formID:
  std-ch = std-ch + "," + replace(stateform.formID, ",", " ")
                                + "  :  "
                                + replace(stateform.description, ",", " ")
                                + "," + replace(stateform.formID, ",", " ")
                                .
 end.
 std-ch = trim(std-ch, ",").
 if std-ch = "" 
  then std-ch = "Select,None".
 RETURN std-ch.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

