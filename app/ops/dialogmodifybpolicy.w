&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialogMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialogMain 
/*
 dialogmodifybpolicy.w
 DIALOG to allow MODIFY or delete of a POLICY in a Batch
 4.25.2012 D.Sinclair
 
 Modified: 
 Date          Name             Comments
 02/17/2022    Vignesh Rajan    Increased the tLiability field format
                                from "$-zz,zzz,zz9.99" to "$-zzz,zzz,zz9.99".
 */

{lib/std-def.i}
{tt/agent.i}
{tt/stateform.i}

def input-output parameter pFile as char.
def input-output parameter pPolicy as int.
def input-output parameter pForm as char.
def input-output parameter pRateCode as char.
def input-output parameter pSTAT as char.
def input-output parameter pEffDate as datetime.
def input-output parameter pLiability as dec.
def input-output parameter pCounty as char.
def input-output parameter pResidential as logical.
def input-output parameter pGrossPremium as dec.
def input-output parameter pNetPremium as dec.
def input-output parameter pZipCode as char.

def input parameter pFormType as char.
def input parameter pFormList as char.
def input parameter pStatCodeList as char.
def input parameter pCountyList as char.
def input parameter pErrMsg as char.
def input parameter pStateID as char.

def input parameter table for agent.
def input parameter table for stateform.

def output parameter pAction as char init "UNDO".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialogMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tZipCode tFile tResidential tPolicy ~
tRateCode tStat tForm tEffDate tLiability tCounty tGrossPremium tNetPremium ~
bSave bDelete bCancel 
&Scoped-Define DISPLAYED-OBJECTS tFormType tZipCode tFile tResidential ~
tPolicy tRateCode tStat tForm tEffDate tLiability tCounty tGrossPremium ~
tRetention tNetPremium tErrMsg 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON bDelete AUTO-GO 
     LABEL "Delete" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON bSave AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE tCounty AS CHARACTER 
     LABEL "County" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN AUTO-COMPLETION
     SIZE 32.2 BY 1 NO-UNDO.

DEFINE VARIABLE tForm AS CHARACTER 
     LABEL "Form" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN AUTO-COMPLETION
     SIZE 113.4 BY 1 NO-UNDO.

DEFINE VARIABLE tRateCode AS CHARACTER FORMAT "X(256)":U INITIAL "P" 
     LABEL "Rate Code" 
     VIEW-AS COMBO-BOX INNER-LINES 9
     LIST-ITEM-PAIRS "Purchase","P",
                     "Loan","L",
                     "Loan Simultaneous","S",
                     "2nd Loan Simultaneous","N",
                     "Home Equity","H",
                     "Construction","C",
                     "Guarantee","G",
                     "Leasehold","E",
                     "Foreclosure","F"
     DROP-DOWN-LIST
     SIZE 29.8 BY 1 TOOLTIP "Select the nature of the policy (rate)" NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER 
     LABEL "STAT" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Item 1","Item 1"
     DROP-DOWN AUTO-COMPLETION
     SIZE 113.4 BY 1 NO-UNDO.

DEFINE VARIABLE tEffDate AS DATETIME FORMAT "99/99/99":U 
     LABEL "Effective" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tErrMsg AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 124.6 BY .61 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "File" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tFormType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Form Type" 
     VIEW-AS FILL-IN 
     SIZE 29.8 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tLiability AS DECIMAL FORMAT "$-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Liability" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicy AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Policy" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE tRetention AS DECIMAL FORMAT "$-zzz,zz9.99":U INITIAL 0 
     LABEL "Retention" 
     VIEW-AS FILL-IN 
     SIZE 19 BY 1 NO-UNDO.

DEFINE VARIABLE tZipCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Zip Code" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tResidential AS LOGICAL INITIAL no 
     LABEL "Residential" 
     VIEW-AS TOGGLE-BOX
     SIZE 15.2 BY .83 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialogMain
     tFormType AT ROW 3.13 COL 11.6 COLON-ALIGNED WIDGET-ID 144
     tZipCode AT ROW 1.96 COL 59.8 COLON-ALIGNED WIDGET-ID 142
     tFile AT ROW 1.96 COL 11.6 COLON-ALIGNED WIDGET-ID 2
     tResidential AT ROW 2.04 COL 35.4 WIDGET-ID 38
     tPolicy AT ROW 4.35 COL 11.6 COLON-ALIGNED WIDGET-ID 4
     tRateCode AT ROW 5.52 COL 11.6 COLON-ALIGNED WIDGET-ID 140
     tStat AT ROW 6.7 COL 11.6 COLON-ALIGNED WIDGET-ID 32
     tForm AT ROW 7.91 COL 11.6 COLON-ALIGNED WIDGET-ID 30
     tEffDate AT ROW 3.13 COL 59.8 COLON-ALIGNED WIDGET-ID 10
     tLiability AT ROW 4.35 COL 59.8 COLON-ALIGNED WIDGET-ID 12
     tCounty AT ROW 5.52 COL 60 COLON-ALIGNED WIDGET-ID 34
     tGrossPremium AT ROW 3.13 COL 106 COLON-ALIGNED WIDGET-ID 16
     tRetention AT ROW 5.52 COL 106 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tNetPremium AT ROW 4.35 COL 106 COLON-ALIGNED WIDGET-ID 18
     bSave AT ROW 9.83 COL 36.6
     bDelete AT ROW 9.83 COL 56.6
     bCancel AT ROW 9.83 COL 76.6
     tErrMsg AT ROW 11.35 COL 1 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     SPACE(2.19) SKIP(1.13)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Modify Policy"
         DEFAULT-BUTTON bSave CANCEL-BUTTON bCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialogMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fDialogMain:SCROLLABLE       = FALSE
       FRAME fDialogMain:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN tErrMsg IN FRAME fDialogMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tFormType IN FRAME fDialogMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tRetention IN FRAME fDialogMain
   NO-ENABLE                                                            */
ASSIGN 
       tZipCode:HIDDEN IN FRAME fDialogMain           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialogMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialogMain fDialogMain
ON WINDOW-CLOSE OF FRAME fDialogMain /* Modify Policy */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDelete fDialogMain
ON CHOOSE OF bDelete IN FRAME fDialogMain /* Delete */
DO: /* Call Help Function (or a simple message). */
 pAction = "DELETE".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSave fDialogMain
ON CHOOSE OF bSave IN FRAME fDialogMain /* Save */
DO:
  if tFile:screen-value in frame fDialogMain = "" 
   then
    do:
     MESSAGE "File cannot be blank."
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return no-apply.
    end.
  pAction = "SAVE".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tGrossPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tGrossPremium fDialogMain
ON LEAVE OF tGrossPremium IN FRAME fDialogMain /* Gross Premium */
DO:
  if agent.remitType = "P"
    and tNetPremium:input-value in frame {&frame-name} = 0
   then
    do: std-de = round(self:input-value * agent.remitValue, 2).
        tNetPremium:screen-value in frame {&frame-name} = string(std-de).
    end.
  std-de = self:input-value - tNetPremium:input-value in frame {&frame-name}.
  tRetention:screen-value in frame {&frame-name} = string(std-de).

  self:bgcolor = ?.
  
  find stateform
    where stateform.stateID = pStateID
      and stateform.formID = tForm:input-value no-error.
  if available stateform
    and stateform.rateCheck = "F"
    and (self:input-value < stateform.rateMin
      or self:input-value > stateform.rateMax)
   then self:bgcolor = 14.
  if available stateform
    and stateform.rateCheck = "L"
    and (self:input-value < ((tLiability:input-value in frame {&frame-name} / 1000) * stateform.rateMin)
      or self:input-value > ((tLiability:input-value in frame {&frame-name} / 1000) * stateform.rateMax))
   then self:bgcolor = 14.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNetPremium
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNetPremium fDialogMain
ON LEAVE OF tNetPremium IN FRAME fDialogMain /* Net Premium */
DO:
 if agent.remitType = "P"
   and agent.remitValue > 0
   and tGrossPremium:input-value in frame {&frame-name} = 0 
 then
  do: std-de = round(self:input-value / agent.remitValue, 2).
      tGrossPremium:screen-value in frame {&frame-name} = string(std-de).
  end.
 std-de = tGrossPremium:input-value in frame {&frame-name} - self:input-value.
 tRetention:screen-value in frame {&frame-name} = string(std-de).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialogMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

tCounty:list-item-pairs in frame fDialogMain = pCountyList.
tForm:list-item-pairs in frame fDialogMain = pFormList.
tStat:list-item-pairs in frame fDialogMain = pStatCodeList.

publish "GetBatchFormZipCode" (pStateID, output std-lo).

ASSIGN
  tFile = pFile
  tPolicy = pPolicy
  tForm = pForm
  tRateCode = pRateCode
  tSTAT = pSTAT
  tEffDate = pEffDate
  tLiability = pLiability
  tCounty = pCounty
  tResidential = pResidential
  tGrossPremium = pGrossPremium
  tNetPremium = pNetPremium
  tRetention = tGrossPremium - tNetPremium
  tFormType = pFormType
  tErrMsg = pErrMsg
  tZipCode = pZipCode
  .
  
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  find first agent no-lock.
  RUN enable_UI.
  tZipCode:hidden = not std-lo.
  
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if pAction = "SAVE" 
   then assign
          pFile = tFile:input-value in frame fDialogMain
          pPolicy = tPolicy:input-value in frame fDialogMain
          pForm = tForm:input-value in frame fDialogMain
          pRateCode = tRateCode:input-value in frame fDialogMain
          pSTAT = tSTAT:input-value in frame fDialogMain
          pEffDate = tEffDate:input-value in frame fDialogMain
          pLiability = tLiability:input-value in frame fDialogMain
          pCounty = tCounty:input-value in frame fDialogMain
          pResidential = tResidential:checked in frame fDialogMain
          pGrossPremium = tGrossPremium:input-value in frame fDialogMain
          pNetPremium = tNetPremium:input-value in frame fDialogMain
          .
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialogMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialogMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialogMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tFormType tZipCode tFile tResidential tPolicy tRateCode tStat tForm 
          tEffDate tLiability tCounty tGrossPremium tRetention tNetPremium 
          tErrMsg 
      WITH FRAME fDialogMain.
  ENABLE tZipCode tFile tResidential tPolicy tRateCode tStat tForm tEffDate 
         tLiability tCounty tGrossPremium tNetPremium bSave bDelete bCancel 
      WITH FRAME fDialogMain.
  VIEW FRAME fDialogMain.
  {&OPEN-BROWSERS-IN-QUERY-fDialogMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

