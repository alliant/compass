&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wops12e-r.w
   Window of Agent Data Call Export report.
*/

CREATE WIDGET-POOL.

{tt/period.i}
{tt/agent.i}
{tt/state.i}
{lib/std-def.i}
{lib/winlaunch.i}
{lib/validFilename.i}

def var tNameOK as log no-undo.
def var tBadChars as char no-undo.

def var tStatus as char no-undo.
def var tNumTasks as int no-undo.
def var tCurTask as int no-undo.

def var tGettingData as logical.
def var tKeepGettingData as logical.

def var sPeriodID as int no-undo.
def var ePeriodID as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-36 sMonth sYear eMonth eYear tStateID ~
bRefresh tFile 
&Scoped-Define DISPLAYED-OBJECTS sMonth sYear eMonth eYear tStateID tFile 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setReportButtons C-Win 
FUNCTION setReportButtons RETURNS LOGICAL
  ( input pCmd as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bRefresh 
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get data".

DEFINE VARIABLE eMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Ending Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE eYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE sMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "Starting Period" 
     VIEW-AS COMBO-BOX INNER-LINES 12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE sYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE tFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Output File" 
     VIEW-AS FILL-IN 
     SIZE 70 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 100 BY 8.33.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     sMonth AT ROW 2.19 COL 17 COLON-ALIGNED WIDGET-ID 86
     sYear AT ROW 2.19 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 88
     eMonth AT ROW 3.38 COL 17 COLON-ALIGNED WIDGET-ID 70
     eYear AT ROW 3.38 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 72
     tStateID AT ROW 4.57 COL 17 COLON-ALIGNED WIDGET-ID 82
     bRefresh AT ROW 6.24 COL 92 WIDGET-ID 4
     tFile AT ROW 6.62 COL 17 COLON-ALIGNED WIDGET-ID 68
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 56
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 101.8 BY 9 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "State Data Call Export"
         HEIGHT             = 9
         WIDTH              = 101.8
         MAX-HEIGHT         = 29.52
         MAX-WIDTH          = 281
         VIRTUAL-HEIGHT     = 29.52
         VIRTUAL-WIDTH      = 281
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME fMain:HANDLE
       ROW             = 8.62
       COLUMN          = 19
       HEIGHT          = .48
       WIDTH           = 80
       WIDGET-ID       = 52
       HIDDEN          = no
       SENSITIVE       = yes.
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {35053A22-8589-11D1-B16A-00C0F0283628} type: ProgressBar */
      CtrlFrame:MOVE-AFTER(tFile:HANDLE IN FRAME fMain).

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* State Data Call Export */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* State Data Call Export */
DO:
/* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* State Data Call Export */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  assign
    sMonth 
    sYear
    eMonth 
    eYear
    tStateID
    tFile.
  
  assign
    sPeriodID = 0
    ePeriodID = 0.
  
  find first period where period.periodMonth = sMonth:input-value
                    and period.periodYear = sYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Starting period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  sPeriodID = period.periodID.
  
  find first period where period.periodMonth = eMonth:input-value
                    and period.periodYear = eYear:input-value
                    no-lock no-error.
  if not avail period then
  do:
    message "Ending period is not valid."
      view-as alert-box error.
    return no-apply.
  end.
  else
  ePeriodID = period.periodID.

  if sPeriodID = 0 or ePeriodID = 0 or ePeriodID < sPeriodID then
  do:
    message "Ending period must be equal or later than starting period."
      view-as alert-box error.
    return no-apply.
  end.

  if tStateID:input-value = "" or tStateID:input-value = ? or tStateID:input-value = "Select" then
  do:
    message
      "A state must be selected."
      view-as alert-box.
    return no-apply.
  end.

  if tFile:screen-value = "" or tFile:screen-value = ? then
  do:
    message
      "Filename cannot be blank."
      view-as alert-box error.
      return no-apply.
  end.
  
  tNameOK = validFilename(tFile:screen-value, output tBadChars).
  if not(tNameOK) then
  do:
    message
      "Invalid characters in filename." skip
      "The following characters are not allowed:" skip
      tBadChars
      view-as alert-box error.
      return no-apply.
  end.
  
  setReportButtons("Disable").
  run exportData in this-procedure.
  setReportButtons("Enable").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eMonth C-Win
ON VALUE-CHANGED OF eMonth IN FRAME fMain /* Ending Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eYear C-Win
ON VALUE-CHANGED OF eYear IN FRAME fMain
DO:
 clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sMonth C-Win
ON VALUE-CHANGED OF sMonth IN FRAME fMain /* Starting Period */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME sYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL sYear C-Win
ON VALUE-CHANGED OF sYear IN FRAME fMain
DO:
 clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateID C-Win
ON VALUE-CHANGED OF tStateID IN FRAME fMain /* State */
DO:
  clearData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").

subscribe to "DataError" anywhere.

session:immediate-display = yes.

run windowResized.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=sMonth &yr=sYear}
    {lib/get-period-list.i &mth=eMonth &yr=eYear}
    {lib/get-state-list.i &combo=tStateID}
    /* set the values */
    {lib/set-current-value.i &state=tStateID}
    {lib/set-current-value.i &mth=sMonth &yr=sYear}
    {lib/set-current-value.i &mth=eMonth &yr=eYear}
  end.
  
  clearData().
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load C-Win  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "wops13e-r.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
    CtrlFrame:NAME = "CtrlFrame":U
  .
  RUN initialize-controls IN THIS-PROCEDURE NO-ERROR.
END.
ELSE MESSAGE "wops13e-r.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DataError C-Win 
PROCEDURE DataError :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input param pMsg as char.

  message
    pMsg skip(1)
    "Please close and re-run the report or notify" skip
    "the systems administrator if the problem persists."
    view-as alert-box error.

  clearData().
  
  setReportButtons("Enable").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  RUN control_load.
  DISPLAY sMonth sYear eMonth eYear tStateID tFile 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-36 sMonth sYear eMonth eYear tStateID bRefresh tFile 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tLiabRange as char no-undo init "100000,1000000,5000000,10000000".
 def var tFolderFile as char no-undo.
 def var tSuccess as logical init false no-undo.
 def var tMsg as char no-undo.

 hide message no-pause.
 message "Exporting data...".

 GETTING-DATA:
 do with frame {&frame-name}:
   
   tNumTasks = 1.
   tCurTask = 1.
   pbMinStatus().
  
   tGettingData = true.
   tkeepGettingData = true.
  
   hide message no-pause.
   message "Exporting data...".
   
   pbUpdateStatus(tCurTask, 0).
   
   tFolderFile = tFile:input-value.
 
   run server/expstatedatacall.p (input sPeriodID,
                                  input ePeriodID,
                                  input tStateID:input-value,
                                  input tLiabRange,
                                  input-output tFolderFile,
                                  output tSuccess,
                                  output tMsg).
                                  
   if not tSuccess then 
   do: std-lo = false.
       publish "GetAppDebug" (output std-lo).
       if std-lo 
        then message "ExpStateDataCall failed: " tMsg view-as alert-box warning.
       leave.
   end.
      
   process events.
      
   if not tKeepGettingData 
    then leave GETTING-DATA.
   
   tCurTask = std-in.
   pbUpdateStatus(int((tCurTask / tNumTasks) * 100), 0).
 end. /* GETTING-DATA */

 if not tKeepGettingData 
  then
   do:
       clearData().

       hide message no-pause.
       message "Cancelled (" + string(now,"99/99/99 HH:MM:SS") + ")".
       pbUpdateStatus(100, 1).
       pbMinStatus().
       tGettingData = false.
       return.
   end.
 
 pbUpdateStatus(100, 1).
 pbMinStatus().
 tGettingData = false. 
 
 hide message no-pause.
 
 if tSuccess = yes then
 do:
   message 
      "Report exported to:" skip
      tFolderFile skip(1)
      "Would you like to open the file?"
      view-as alert-box question buttons yes-no update std-lo.
   
   if std-lo = yes then
   run util/openfile.p (tFolderFile).
 end.
         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    hide message no-pause.
    
    assign
      sPeriodID = 0
      ePeriodID = 0.
    
    find first period where period.periodMonth = sMonth:input-value
                        and period.periodYear = sYear:input-value
                        no-lock no-error.
    if avail period then
    sPeriodID = period.periodID.
    
    find first period where period.periodMonth = eMonth:input-value
                        and period.periodYear = eYear:input-value
                        no-lock no-error.
    if avail period then
    ePeriodID = period.periodID.
    
    assign
      tFile:read-only    = no  
      tFile:screen-value = "StateDataCall" + 
                           (if tStateID:input-value <> "" and tStateID:input-value <> "Select" then "_" + tStateID:input-value else "") +
                           (if sPeriodID <> 0 then "_" + string(sPeriodID) else "") +
                           (if ePeriodID <> 0 then "_" + string(ePeriodID) else "") +
                           ".csv".
    
  end.
                  
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbMinStatus C-Win 
FUNCTION pbMinStatus RETURNS LOGICAL
  ( /* parameter-definitions */ ) :

  chCtrlFrame:ProgressBar:VALUE = chCtrlFrame:ProgressBar:MIN.
  
  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION pbUpdateStatus C-Win 
FUNCTION pbUpdateStatus RETURNS LOGICAL
  ( input pPercentage   as int,
    input pPauseSeconds as int ) :

  {&WINDOW-NAME}:move-to-top().
  
  do with frame {&frame-name}:
    if chCtrlFrame:ProgressBar:VALUE <> pPercentage then
    assign
      chCtrlFrame:ProgressBar:VALUE = pPercentage.
      
    if pPauseSeconds > 0 then
    pause pPauseSeconds no-message.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setReportButtons C-Win 
FUNCTION setReportButtons RETURNS LOGICAL
  ( input pCmd as char ) :

  do with frame {&frame-name}:
    case pCmd:
      when "Enable" then
      assign
        bRefresh:sensitive = yes.
      when "Disable" then
      assign
        bRefresh:sensitive = no.
      otherwise
        .
    end case.
  end.

  RETURN true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

