&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fDialog 
/* dialogsavebatch.w
   2.6.2013 D.Sinclair
   */

def output parameter pCancel as logical init true.
def output parameter pStat as char init "P".
def output parameter pComments as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fDialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tBatchStatus tComments Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tBatchStatus 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14 TOOLTIP "Select to continue working on the batch"
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14 TOOLTIP "Unlock the batch and mark it ready for review"
     BGCOLOR 8 .

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 1000 SCROLLBAR-VERTICAL
     SIZE 70.6 BY 4 TOOLTIP "Enter notes regarding any oustanding issues" NO-UNDO.

DEFINE IMAGE iGreenFlag
     FILENAME "adeicon/blank":U
     SIZE 7.2 BY 1.71 TOOLTIP "Select to indicate the batch is complete and to unlock it".

DEFINE IMAGE iRedFlag
     FILENAME "adeicon/blank":U
     SIZE 7.2 BY 1.71 TOOLTIP "Select to indicate the batch is complete with issues and to unlock it".

DEFINE VARIABLE tBatchStatus AS CHARACTER 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "No Issues / Move to Reviewing", "G",
"Unresolved Issue(s) / Remain in Processing", "R"
     SIZE 71.4 BY 3.76 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fDialog
     tBatchStatus AT ROW 1.62 COL 16.6 NO-LABEL WIDGET-ID 2
     tComments AT ROW 5.62 COL 18.6 NO-LABEL WIDGET-ID 12
     Btn_OK AT ROW 10.62 COL 18.6
     Btn_Cancel AT ROW 10.62 COL 37.6
     "Comments:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 5.62 COL 7.6 WIDGET-ID 14
     iGreenFlag AT ROW 1.57 COL 8.6 WIDGET-ID 6
     iRedFlag AT ROW 3.48 COL 8.6 WIDGET-ID 8
     SPACE(79.59) SKIP(7.37)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         BGCOLOR 15 
         TITLE "Close Batch"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fDialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fDialog:SCROLLABLE       = FALSE
       FRAME fDialog:HIDDEN           = TRUE.

/* SETTINGS FOR IMAGE iGreenFlag IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR IMAGE iRedFlag IN FRAME fDialog
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tComments IN FRAME fDialog
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fDialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fDialog fDialog
ON WINDOW-CLOSE OF FRAME fDialog /* Close Batch */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fDialog 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

iGreenFlag:load-image("images/flag_green.bmp").
iRedFlag:load-image("images/flag_red.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
RUN enable_UI.

MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
  if tBatchStatus:input-value in frame fDialog = "R" 
   and tComments:screen-value in frame fDialog = ""
   then
    do:
         MESSAGE "To mark the batch Red, comments are required"
          VIEW-AS ALERT-BOX warning BUTTONS OK.
         next.
    end.

  pStat = tBatchStatus:input-value in frame fDialog.
  pComments = tComments:screen-value in frame fDialog.
  pCancel = false.
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fDialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fDialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fDialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tBatchStatus 
      WITH FRAME fDialog.
  ENABLE tBatchStatus tComments Btn_OK Btn_Cancel 
      WITH FRAME fDialog.
  VIEW FRAME fDialog.
  {&OPEN-BROWSERS-IN-QUERY-fDialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

