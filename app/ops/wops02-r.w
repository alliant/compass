&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wops02-r.w
   Window of batches for a PERIOD
   4.23.2012
   */

CREATE WIDGET-POOL.

{tt/batch.i}
{tt/batch.i &tableAlias=ttData}
{tt/period.i}
{tt/state.i}

{lib/std-def.i}

def var hData as handle no-undo.

def var tStatus as char no-undo.
{lib/winlaunch.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ttData

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData ttData.batchID ttData.stateID ttData.agentID ttData.agentName ttData.stat ttData.fileCount ttData.policyCount ttData.liabilityDelta ttData.grossPremiumDelta ttData.netPremiumDelta ttData.retainedPremiumDelta   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData FOR EACH ttData
&Scoped-define OPEN-QUERY-brwData OPEN QUERY {&SELF-NAME} FOR EACH ttData.
&Scoped-define TABLES-IN-QUERY-brwData ttData
&Scoped-define FIRST-TABLE-IN-QUERY-brwData ttData


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS brwData bExport bPrint bRefresh tAgents ~
tBatches tGrossPremium fMonth fYear tNetPremium tPolicies RECT-36 tFiles ~
fState 
&Scoped-Define DISPLAYED-OBJECTS fMonth fYear fState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to a CSV File".

DEFINE BUTTON bPrint  NO-FOCUS
     LABEL "Print" 
     SIZE 7.2 BY 1.71 TOOLTIP "View period details as PDF".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Go" 
     SIZE 7.2 BY 1.71 TOOLTIP "Get period details".

DEFINE VARIABLE fMonth AS INTEGER FORMAT ">9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 12
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "ALL","A"
     DROP-DOWN-LIST
     SIZE 23.2 BY 1 NO-UNDO.

DEFINE VARIABLE fYear AS INTEGER FORMAT ">>>9":U INITIAL 0 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "2010" 
     DROP-DOWN-LIST
     SIZE 10.8 BY 1 NO-UNDO.

DEFINE VARIABLE tAgents AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Agents" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tBatches AS INTEGER FORMAT "zzz,zz9":U INITIAL 0 
     LABEL "Batches" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE tFiles AS INTEGER FORMAT "-zzz,zz9":U INITIAL 0 
     LABEL "Files" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE tGrossPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Gross Premium" 
     VIEW-AS FILL-IN 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE tNetPremium AS DECIMAL FORMAT "-zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Net Premium" 
     VIEW-AS FILL-IN 
     SIZE 22.6 BY 1 NO-UNDO.

DEFINE VARIABLE tPolicies AS INTEGER FORMAT "-zzz,zz9":U INITIAL 0 
     LABEL "Policies" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 33 BY 2.62.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 41 BY 2.62.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 2.62.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      ttData SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      ttData.batchID column-label "Batch!ID" format ">>>>>>>9"
ttData.stateID label "State" format "x(6)"
ttData.agentID label "AgentID" format "x(12)"
ttData.agentName label "Agent" format "x(250)" width 40
ttData.stat label "Stat"
ttData.fileCount label "Files" format "zz,zz9"
ttData.policyCount label "Policies" format "zzz,zz9"
ttData.liabilityDelta label "Liability" format "-zzz,zzz,zzz,zz9.99"
ttData.grossPremiumDelta column-label "Gross!Premium" format "-zzz,zzz,zzz,zz9.99"
ttData.netPremiumDelta column-label "Net!Premium" format "-zzz,zzz,zzz,zz9.99"
ttData.retainedPremiumDelta label "Retained" format "-zzz,zzz,zzz,zz9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 182 BY 17.57 ROW-HEIGHT-CHARS .81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     brwData AT ROW 4.33 COL 2 WIDGET-ID 200
     bExport AT ROW 1.95 COL 86.4 WIDGET-ID 2 NO-TAB-STOP 
     bPrint AT ROW 1.95 COL 79.4 WIDGET-ID 14 NO-TAB-STOP 
     bRefresh AT ROW 1.95 COL 33.6 WIDGET-ID 4 NO-TAB-STOP 
     tAgents AT ROW 2.57 COL 108 COLON-ALIGNED WIDGET-ID 22 NO-TAB-STOP 
     tBatches AT ROW 1.48 COL 108 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     tGrossPremium AT ROW 1.48 COL 159 COLON-ALIGNED WIDGET-ID 24 NO-TAB-STOP 
     fMonth AT ROW 2.33 COL 2 COLON-ALIGNED NO-LABEL WIDGET-ID 16 NO-TAB-STOP 
     fYear AT ROW 2.33 COL 19.2 COLON-ALIGNED NO-LABEL WIDGET-ID 18 NO-TAB-STOP 
     tNetPremium AT ROW 2.57 COL 159 COLON-ALIGNED WIDGET-ID 26 NO-TAB-STOP 
     tPolicies AT ROW 2.57 COL 129.6 COLON-ALIGNED WIDGET-ID 58 NO-TAB-STOP 
     tFiles AT ROW 1.48 COL 129.6 COLON-ALIGNED WIDGET-ID 60 NO-TAB-STOP 
     fState AT ROW 2.33 COL 49.8 COLON-ALIGNED WIDGET-ID 82
     "Parameters" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 1.14 COL 3 WIDGET-ID 56
     "Filters" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 1.14 COL 45 WIDGET-ID 70
     "Actions" VIEW-AS TEXT
          SIZE 7.8 BY .62 AT ROW 1.14 COL 79 WIDGET-ID 74
     RECT-36 AT ROW 1.48 COL 2 WIDGET-ID 54
     RECT-35 AT ROW 1.48 COL 44 WIDGET-ID 68
     RECT-37 AT ROW 1.48 COL 78 WIDGET-ID 72
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 184 BY 21.14 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Batches for Period"
         HEIGHT             = 21.14
         WIDTH              = 184
         MAX-HEIGHT         = 21.57
         MAX-WIDTH          = 190.4
         VIRTUAL-HEIGHT     = 21.57
         VIRTUAL-WIDTH      = 190.4
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = yes
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwData 1 fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE RECT-35 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-37 IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAgents IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tAgents:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tBatches IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tBatches:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tFiles IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tFiles:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tGrossPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tGrossPremium:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tNetPremium IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tNetPremium:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tPolicies IN FRAME fMain
   NO-DISPLAY                                                           */
ASSIGN 
       tPolicies:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH ttData.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Batches for Period */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Batches for Period */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Batches for Period */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
  run exportData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPrint
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPrint C-Win
ON CHOOSE OF bPrint IN FRAME fMain /* Print */
DO:
  run printData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Go */
DO:
  fState:screen-value in frame {&frame-name} = "ALL".
  run getData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fMonth
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMonth C-Win
ON VALUE-CHANGED OF fMonth IN FRAME fMain
DO:
/*   run getData in this-procedure.  */
/*   apply "ENTRY" to brwData.       */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fState C-Win
ON VALUE-CHANGED OF fState IN FRAME fMain /* State */
DO:
  run filterData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fYear C-Win
ON VALUE-CHANGED OF fYear IN FRAME fMain
DO:
/*   run getData in this-procedure.  */
/*   apply "ENTRY" to brwData.       */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bRefresh:load-image("images/completed.bmp").
bRefresh:load-image-insensitive("images/completed-i.bmp").
bPrint:load-image("images/pdf.bmp").
bPrint:load-image-insensitive("images/pdf-i.bmp").
bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").

run windowResized.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  do with frame {&frame-name}:
    /* create the combos */
    {lib/get-period-list.i &mth=fMonth &yr=fYear}
    {lib/get-state-list.i &combo=fState &addAll=true}
    /* set the values */
    {lib/set-current-value.i &state=fState &mth=fMonth &yr=fYear}
  end.
  
  fState:screen-value in frame {&frame-name} = "ALL".
  run getData in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fMonth fYear fState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE brwData bExport bPrint bRefresh tAgents tBatches tGrossPremium fMonth 
         fYear tNetPremium tPolicies RECT-36 tFiles fState 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var th as handle no-undo.
 th = temp-table ttData:handle.

 if query brwData:num-results = 0 
  then
   do: 
    MESSAGE "There is nothing to export"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 publish "GetReportDir" (output std-ch).

 run util/exporttable.p (table-handle th,
                         "ttData",
                         "for each ttData ",
                         "batchID,stateID,agentID,agentName,stat,fileCount,policyCount,liabilityAmount,grossPremiumProcessed,netPremiumProcessed,retainedPremiumProcessed,liabilityDelta,grossPremiumDelta,netPremiumDelta,retainedPremiumDelta",
                         "Batch ID,State,Agent ID,Agent Name,Status,Num Files,Num Policies,Liability Amount,Gross Premium,Net Premium,Retained,Liability Delta,Gross Delta,Net Delta,Retained Delta",
                         std-ch,
                         "BatchesForPeriod_" 
                          + string(fYear:screen-value in frame fMain,"9999")
                          + string(fMonth:screen-value in frame fMain,"99")
                          + ".csv",
                         true,
                         output std-ch,
                         output std-in).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 empty temp-table ttData no-error.
 
 for each batch
  where (if fState:input-value in frame {&frame-name} = "ALL" 
         then true
         else batch.stateID = fState:input-value in frame {&frame-name}):
  create ttData.
  buffer-copy batch to ttData.
 end.
 
 dataSortBy = "".
 dataSortDesc = no.
 run sortData ("batchID").

 assign
   tBatches = 0
   tAgents = 0
   tFiles = 0
   tPolicies = 0
   tGrossPremium = 0
   tNetPremium = 0
   .
 
 for each ttData
  break by ttData.agentID:
  tBatches = tBatches + 1.
  if first-of(ttData.agentID) 
   then tAgents = tAgents + 1.
  tFiles = tFiles + ttData.fileCount.
  tPolicies = tPolicies + ttData.policyCount.
  tGrossPremium = tGrossPremium + ttData.grossPremiumDelta.
  tNetPremium = tNetPremium + ttData.netPremiumDelta.
 end.

 display
   tBatches
   tAgents
   tFiles
   tPolicies
   tGrossPremium
   tNetPremium
  with frame fMain.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getData C-Win 
PROCEDURE getData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def buffer batch for batch.
 
 close query brwData.
 empty temp-table batch.
 clearData().

 hide message no-pause.
 message "Getting data...".

 session:set-wait-state("general").
 publish "GetBatchesTemp" (input fMonth:input-value in frame fMain,
                           input fYear:input-value in frame fMain,
                           output table batch).
 session:set-wait-state("").

 run filterData.

 pause 0.
 
 hide message no-pause.
 message "Report Complete for Period " + 
         fMonth:screen-value in frame fMain + "/" +
         fYear:screen-value in frame fMain + "  " +
         "(" + string(now,"99/99/99 HH:MM:SS") + ")".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE printData C-Win 
PROCEDURE printData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tTitle as char no-undo.
 def var tFilename as char no-undo.


 if query brwData:num-results = 0
  then
   do: 
    MESSAGE "There is nothing to print"
     VIEW-AS ALERT-BOX warning BUTTONS OK.
    return.
   end.

 tTitle = "Batches in " 
    + getMonthName(fMonth:input-value in frame fMain) 
    + " " + string(fYear:input-value in frame fMain, "9999").

 publish "IsPeriodOpen" (fMonth:input-value in frame fMain,
                         fYear:input-value in frame fMain,
                         output std-lo).

 tTitle = tTitle + " (" + (if std-lo then "Open" else "Closed") + ")".

 tFilename = "BatchesForPeriod_" 
    + string(fYear:input-value in frame fMain)
    + string(fMonth:input-value in frame fMain)
    + ".pdf".

 run ops02-r.p (table ttData,
                tTitle,
                tFilename).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetDataSource C-Win 
PROCEDURE SetDataSource :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter p as handle.

 if valid-handle(p) 
  then hData = p.
 run getData in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{lib/brw-sortData.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* {&frame-name} components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 10.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 5.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION clearData C-Win 
FUNCTION clearData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  close query brwData.
  hide message no-pause.
  clear frame fMain.
  RETURN true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

