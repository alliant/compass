&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialogcopyregion.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
  
  Modified    :
   Date        Name     Comments
   07/01/2019  Sachin   Bug Fixes.  
   12/18/2019  Anubha   Modified to show tooltip on Region combo.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{tt\ratecard.i}
{tt\rateui.i}
/* Parameters Definitions ---                                           */
define input parameter pCardSetID     as integer   no-undo.
define input parameter pCurrentRegion as character no-undo.
define input parameter table for ratecard.

{tt\ratecard.i &tablealias=selectedRateCard}
{tt\ratecard.i &tablealias=availRateCard}
{tt\ratecard.i &tablealias=tempRateCard}  /* Note - During Add row(s) operation, this temp-table will store selected rows temporarily.*/
{lib\std-def.i}
{lib\brw-multi-def.i}

/* Local Variable Definitions ---                                       */
define variable cRegionList   as character no-undo.
define variable cAvailCards   as character no-undo.
define variable rratecardID   as rowid     no-undo.
define variable iNumRows      as integer   no-undo.
define variable iRateCardID   as integer   no-undo.
define variable regionlist    as character no-undo.

define buffer bcard for ratecard.

define variable pregionList   as character no-undo.
define variable activeStateID as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAvailRateCard

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES availRateCard ratecard selectedRateCard

/* Definitions for BROWSE brwAvailRateCard                              */
&Scoped-define FIELDS-IN-QUERY-brwAvailRateCard availRateCard.region availRateCard.cardName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAvailRateCard   
&Scoped-define SELF-NAME brwAvailRateCard
&Scoped-define QUERY-STRING-brwAvailRateCard for each availRateCard     where availRateCard.region <> cbRegions:input-value no-lock by availRateCard.region
&Scoped-define OPEN-QUERY-brwAvailRateCard open query {&SELF-NAME} for each availRateCard     where availRateCard.region <> cbRegions:input-value no-lock by availRateCard.region.
&Scoped-define TABLES-IN-QUERY-brwAvailRateCard availRateCard
&Scoped-define FIRST-TABLE-IN-QUERY-brwAvailRateCard availRateCard


/* Definitions for BROWSE brwExistingRateCard                           */
&Scoped-define FIELDS-IN-QUERY-brwExistingRateCard ratecard.cardName ratecard.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwExistingRateCard   
&Scoped-define SELF-NAME brwExistingRateCard
&Scoped-define QUERY-STRING-brwExistingRateCard for each ratecard     where ratecard.region = cbRegions:input-value no-lock by ratecard.region
&Scoped-define OPEN-QUERY-brwExistingRateCard open query {&SELF-NAME} for each ratecard     where ratecard.region = cbRegions:input-value no-lock by ratecard.region.
&Scoped-define TABLES-IN-QUERY-brwExistingRateCard ratecard
&Scoped-define FIRST-TABLE-IN-QUERY-brwExistingRateCard ratecard


/* Definitions for BROWSE brwSelectedRateCard                           */
&Scoped-define FIELDS-IN-QUERY-brwSelectedRateCard selectedRateCard.region selectedRateCard.cardName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSelectedRateCard   
&Scoped-define SELF-NAME brwSelectedRateCard
&Scoped-define QUERY-STRING-brwSelectedRateCard for each selectedRateCard no-lock by selectedRateCard.region
&Scoped-define OPEN-QUERY-brwSelectedRateCard open query {&SELF-NAME} for each selectedRateCard no-lock by selectedRateCard.region.
&Scoped-define TABLES-IN-QUERY-brwSelectedRateCard selectedRateCard
&Scoped-define FIRST-TABLE-IN-QUERY-brwSelectedRateCard selectedRateCard


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAvailRateCard}~
    ~{&OPEN-QUERY-brwExistingRateCard}~
    ~{&OPEN-QUERY-brwSelectedRateCard}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-67 RECT-68 RECT-69 cbRegions ~
brwExistingRateCard fSearchCard brwAvailRateCard bSearch ~
brwSelectedRateCard 
&Scoped-Define DISPLAYED-OBJECTS cbRegions Existing_Cards fSearchCard ~
Selected_Cards 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd  NO-FOCUS
     LABEL "-->" 
     SIZE 4.4 BY 1.05 TOOLTIP "Add Selected Rate Card to Copy".

DEFINE BUTTON bRemove  NO-FOCUS
     LABEL "<--" 
     SIZE 4.4 BY 1.05 TOOLTIP "Remove Selected Rate Card from Copy".

DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 5 BY 1.1 TOOLTIP "Search".

DEFINE BUTTON Btn_save 
     LABEL "Copy" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbRegions AS CHARACTER FORMAT "X(256)" 
     LABEL "Target Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 26.8 BY 1 NO-UNDO.

DEFINE VARIABLE Existing_Cards AS CHARACTER FORMAT "X(256)":U INITIAL "Existing Cards" 
     VIEW-AS FILL-IN 
     SIZE 41 BY 1
     FONT 6 NO-UNDO.

DEFINE VARIABLE fSearchCard AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 30.6 BY 1 NO-UNDO.

DEFINE VARIABLE Selected_Cards AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 43.2 BY 1
     FONT 6 NO-UNDO.

DEFINE RECTANGLE RECT-67
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 9.8 BY .91.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 8.76.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 98 BY 16.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAvailRateCard FOR 
      availRateCard SCROLLING.

DEFINE QUERY brwExistingRateCard FOR 
      ratecard SCROLLING.

DEFINE QUERY brwSelectedRateCard FOR 
      selectedRateCard SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAvailRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAvailRateCard Dialog-Frame _FREEFORM
  QUERY brwAvailRateCard DISPLAY
      availRateCard.region    label "Region"                   
availRateCard.cardName  label "Name"   width 35   format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 44 BY 11.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwExistingRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwExistingRateCard Dialog-Frame _FREEFORM
  QUERY brwExistingRateCard DISPLAY
      ratecard.cardName  label "Name"          width 35   format "x(50)"
ratecard.description  label "Description"   width 50   format "x(100)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 95 BY 5.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwSelectedRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSelectedRateCard Dialog-Frame _FREEFORM
  QUERY brwSelectedRateCard DISPLAY
      selectedRateCard.region     label "Region"         
selectedRateCard.cardName   label "Name"   width 35  format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 43 BY 9.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bAdd AT ROW 17.95 COL 49.2 WIDGET-ID 80 NO-TAB-STOP 
     cbRegions AT ROW 1.62 COL 16.2 COLON-ALIGNED WIDGET-ID 6
     Existing_Cards AT ROW 2.91 COL 3 NO-LABEL WIDGET-ID 104
     brwExistingRateCard AT ROW 3.76 COL 3.4 WIDGET-ID 600
     fSearchCard AT ROW 11.05 COL 9 COLON-ALIGNED WIDGET-ID 306
     bRemove AT ROW 19.05 COL 49.2 WIDGET-ID 78 NO-TAB-STOP 
     Selected_Cards AT ROW 12.14 COL 54.8 NO-LABEL WIDGET-ID 106
     brwAvailRateCard AT ROW 13.05 COL 3.4 WIDGET-ID 400
     bSearch AT ROW 11 COL 42.4 WIDGET-ID 304 NO-TAB-STOP 
     brwSelectedRateCard AT ROW 13.05 COL 55.2 WIDGET-ID 500
     Btn_save AT ROW 23.81 COL 68.8
     "Available Cards" VIEW-AS TEXT
          SIZE 20 BY .62 AT ROW 12.38 COL 3.4 WIDGET-ID 70
          FONT 6
     RECT-67 AT ROW 8.14 COL 17 WIDGET-ID 96
     RECT-68 AT ROW 1.29 COL 2 WIDGET-ID 98
     RECT-69 AT ROW 10.67 COL 2 WIDGET-ID 100
     SPACE(0.59) SKIP(0.37)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Copy Region Rate Cards" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwExistingRateCard Existing_Cards Dialog-Frame */
/* BROWSE-TAB brwAvailRateCard Selected_Cards Dialog-Frame */
/* BROWSE-TAB brwSelectedRateCard bSearch Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAdd IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bRemove IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       brwAvailRateCard:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAvailRateCard:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

ASSIGN 
       brwExistingRateCard:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwExistingRateCard:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

ASSIGN 
       brwSelectedRateCard:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwSelectedRateCard:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON Btn_save IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Existing_Cards IN FRAME Dialog-Frame
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN Selected_Cards IN FRAME Dialog-Frame
   NO-ENABLE ALIGN-L                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAvailRateCard
/* Query rebuild information for BROWSE brwAvailRateCard
     _START_FREEFORM
open query {&SELF-NAME} for each availRateCard
    where availRateCard.region <> cbRegions:input-value no-lock by availRateCard.region.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAvailRateCard */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwExistingRateCard
/* Query rebuild information for BROWSE brwExistingRateCard
     _START_FREEFORM
open query {&SELF-NAME} for each ratecard
    where ratecard.region = cbRegions:input-value no-lock by ratecard.region.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwExistingRateCard */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSelectedRateCard
/* Query rebuild information for BROWSE brwSelectedRateCard
     _START_FREEFORM
open query {&SELF-NAME} for each selectedRateCard no-lock by selectedRateCard.region.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwSelectedRateCard */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Copy Region Rate Cards */
do:
  run closeWindow in this-procedure.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd Dialog-Frame
ON CHOOSE OF bAdd IN FRAME Dialog-Frame /* --> */
do:
  run addToSelected(output std-lo).
  if not std-lo 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemove Dialog-Frame
ON CHOOSE OF bRemove IN FRAME Dialog-Frame /* <-- */
do:
  run removeFromSelected(output std-lo).
  if not std-lo 
   then
    return no-apply.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailRateCard
&Scoped-define SELF-NAME brwAvailRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailRateCard Dialog-Frame
ON DEFAULT-ACTION OF brwAvailRateCard IN FRAME Dialog-Frame
DO:
  apply "CHOOSE" to bAdd.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailRateCard Dialog-Frame
ON LEFT-MOUSE-UP OF brwAvailRateCard IN FRAME Dialog-Frame
do:
  find current availRateCard no-error.
  if available availRateCard 
   then
    brwAvailRateCard:tooltip = availRateCard.description.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailRateCard Dialog-Frame
ON ROW-DISPLAY OF brwAvailRateCard IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailRateCard Dialog-Frame
ON VALUE-CHANGED OF brwAvailRateCard IN FRAME Dialog-Frame
DO:

  bAdd:sensitive = true.  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwExistingRateCard
&Scoped-define SELF-NAME brwExistingRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwExistingRateCard Dialog-Frame
ON LEFT-MOUSE-UP OF brwExistingRateCard IN FRAME Dialog-Frame
do:
  find current ratecard no-error.
  if available ratecard 
   then
    brwAvailRateCard:tooltip = ratecard.description.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwExistingRateCard Dialog-Frame
ON ROW-DISPLAY OF brwExistingRateCard IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSelectedRateCard
&Scoped-define SELF-NAME brwSelectedRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON DEFAULT-ACTION OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
  apply "CHOOSE" to bRemove.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON LEFT-MOUSE-UP OF brwSelectedRateCard IN FRAME Dialog-Frame
do:
  find current selectedRateCard no-error.
  if available selectedRateCard 
   then
    brwSelectedRateCard:tooltip = selectedRateCard.description.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON ROW-DISPLAY OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON VALUE-CHANGED OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
    bRemove:sensitive = true.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch Dialog-Frame
ON CHOOSE OF bSearch IN FRAME Dialog-Frame /* Search */
DO:
  close query brwAvailRateCard.
  open query brwAvailRateCard for each availRateCard no-lock where 
      ((availRateCard.region      matches (if trim(fSearchCard:input-value) = "" then "*" else ("*" + trim(fSearchCard:input-value) + "*"))) or 
       (availRateCard.cardname    matches (if trim(fSearchCard:input-value) = "" then "*" else ("*" + trim(fSearchCard:input-value) + "*")))    ) and
      (availRateCard.region <> cbRegions:input-value)
        by availRateCard.region by availRateCard.cardname.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_save Dialog-Frame
ON CHOOSE OF Btn_save IN FRAME Dialog-Frame /* Copy */
do:
  run saveRateCards.  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRegions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRegions Dialog-Frame
ON VALUE-CHANGED OF cbRegions IN FRAME Dialog-Frame /* Target Region */
do:  
  if brwSelectedRateCard:num-entries > 0 
   then
    do:
      message  "All changes will be discarded.Are You Sure?"
        view-as alert-box question buttons ok-cancel set std-lo.

      if not std-lo 
       then 
        return no-apply.
    end.
  run setScreen.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailRateCard
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(ACTIVE-WINDOW) and frame {&FRAME-NAME}:PARENT eq ?
then frame {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Add images to buttons. */
bRemove:load-image("images/s-previous.bmp").
bAdd   :load-image("images/s-next.bmp").
bAdd   :load-image-insensitive("images/s-next-i.bmp").
bRemove:load-image("images/s-previous.bmp").
bRemove:load-image-insensitive("images/s-previous-i.bmp").
bSearch:load-image("images/s-magnifier.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
{lib/win-main.i}
{lib/brw-main-multi.i &browse-list="brwExistingRateCard,brwAvailRateCard,brwSelectedRateCard"}
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:

  publish "GetRateStateValue"(input pCardSetID,
                              input "StateID",
                              output activeStateID).
                              
  if activeStateID <> "" and activeStateID <> "?" 
   then
    do:
       publish "GetRegionList" (activeStateID,
                                "Region",
                                output pRegionlist).
                                
       publish "GetRateStateRegions" (activeStateID,
                                      "Region",
                                      output table region).                          
                           
       cbRegions:list-items = "," + pregionList.
             
    end.
  
  run enable_UI.
  run setScreen.
  
  wait-for go of frame {&FRAME-NAME}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addToSelected Dialog-Frame 
PROCEDURE addToSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter loSuccess as logical no-undo.

  do with frame {&frame-name}:
  end.

  empty temp-table tempRateCard.

  if brwAvailRateCard:num-selected-rows = 0 
   then 
    return.

  if brwAvailRateCard:num-selected-rows = 1 
   then 
    do:
      find current availRatecard no-error.
      if available availRatecard 
       then
        iRateCardID = availRateCard.cardID.
    end.

  do iNumRows = 1 to browse brwAvailRateCard:num-selected-rows:

    browse brwAvailRateCard:fetch-selected-row(iNumRows).
    find current availRatecard no-error.

    if can-find(first selectedRateCard where selectedRateCard.cardname = availRateCard.cardname)
        or can-find(first tempRateCard where tempRateCard.cardname = availRateCard.cardname) 
     then  
      do:
        message "Another rate card with same name was already selected. Please remove the selected one or deselect this one."
        view-as alert-box info button ok.
      
        for each tempRateCard:
          create availRateCard.
          buffer-copy tempRateCard to availRateCard.
        end.
      
        open query brwAvailRateCard for each availRateCard 
         where availRateCard.region <> cbRegions:input-value by availRateCard.region.
      
        return.
      end.

    create tempRateCard.
    buffer-copy availRateCard to tempRateCard. 
    delete availRateCard.
  end.

  for each tempRateCard:
    create selectedRateCard.
    buffer-copy tempRateCard to selectedRateCard.
  end.
       
  brwAvailRateCard:delete-selected-rows().

  open query brwSelectedRateCard for each selectedRateCard by selectedRateCard.region.

  if iNumRows = 2 
   then
    do:
      rRateCardID = ?.
      find first selectedRateCard where selectedRateCard.cardID = iRateCardID no-error.
    
      if available selectedRateCard 
       then
        rRateCardID  =  rowid(selectedRateCard).
    
      reposition brwSelectedRateCard to rowid rRateCardID no-error.
      brwSelectedRateCard:select-focused-row().
    end.
  
  if  can-find(first availRateCard where availRateCard.region <> cbRegions:input-value) and 
      brwAvailRateCard:num-selected-rows ne 0
   then
    bAdd:sensitive = true.  
   else
    bAdd:sensitive = false.

  if can-find(first selectedRateCard) and 
      brwSelectedRateCard:num-selected-rows ne 0
   then
     bRemove:sensitive  = true.
  else
     bRemove:sensitive  = false.

  if can-find(first selectedRateCard) 
   then
      btn_save:sensitive = true.
  loSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeWindow Dialog-Frame 
PROCEDURE closeWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if can-find(first selectedRateCard) 
   then
    do:        
      message  "Selected cards have not been copied. Do you really want to close?"
        view-as alert-box question buttons ok-cancel set std-lo.  
      if not std-lo 
       then 
        return no-apply.        
    end.   

  empty temp-table availRateCard.
  empty temp-table ratecard.
  empty temp-table selectedRateCard.
  apply "END-ERROR":U to self.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbRegions Existing_Cards fSearchCard Selected_Cards 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-67 RECT-68 RECT-69 cbRegions brwExistingRateCard fSearchCard 
         brwAvailRateCard bSearch brwSelectedRateCard 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE removeFromSelected Dialog-Frame 
PROCEDURE removeFromSelected :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter loSuccess as logical no-undo.

  do with frame {&frame-name}:
  end.

  if brwSelectedRateCard:num-selected-rows = 0 
   then 
    return.

  if brwSelectedRateCard:num-selected-rows = 1 
   then 
    do:
      find current selectedRateCard no-error.
      iRateCardID = selectedRateCard.cardID.
    end.

  do iNumRows = 1 to browse brwSelectedRateCard:num-selected-rows:  
    browse brwSelectedRateCard:fetch-selected-row(iNumRows).
    find current selectedRateCard no-error.

    create availRateCard.
    buffer-copy selectedRateCard to availRateCard. 
    delete selectedRateCard.
  end.

  brwSelectedRateCard:delete-selected-rows().

  open query brwAvailRateCard for each availRateCard 
   where availRateCard.region <> cbRegions:input-value by availRateCard.region.

  if iNumRows = 2 
   then
    do:
      assign rRateCardID = ?.
      find first availRateCard where availRateCard.cardID = iRateCardID no-error.
      if available availRateCard 
       then
        rRateCardID  =  rowid(availRateCard).   
      reposition brwAvailRateCard to rowid rRateCardID no-error.
      brwAvailRateCard:select-focused-row().
    end.

  if can-find(first selectedRateCard) and 
      brwSelectedRateCard:num-selected-rows ne 0
   then
     bRemove:sensitive  = true.
  else
     bRemove:sensitive  = false.

  btn_save:sensitive = can-find(first selectedRateCard).

  if  can-find(first availRateCard where availRateCard.region <> cbRegions:input-value) and 
      brwAvailRateCard:num-selected-rows ne 0
   then
    bAdd:sensitive = true.
   else
    bAdd:sensitive = false.

  loSuccess = true.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveRateCards Dialog-Frame 
PROCEDURE saveRateCards :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if can-find(first selectedRateCard) 
   then
    publish "CopyRegions"(input cbRegions:input-value, 
                          input table selectedRateCard,
                          output table tempRateCard).
  for each tempRateCard:
    create rateCard.
    buffer-copy tempRateCard to rateCard.
  end.

  run setScreen.  
  publish "refreshScreen"("New",
                          input pCardSetID).

  btn_save:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setScreen Dialog-Frame 
PROCEDURE setScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame Dialog-Frame:
  end.                              
  empty temp-table selectedRateCard.
  empty temp-table availRateCard.
  
  find first region where region.regioncode = cbRegions:input-value no-error.
  
  /* Assigning tooltip for Region combobox */
  cbRegions:tooltip = if available region then region.description else "". 

  for each ratecard :
    if ratecard.region <> (if cbRegions:input-value = ? or  cbRegions:input-value = "?" then "" else cbRegions:INPUT-VALUE)   
        and can-find(first bcard where bcard.region = (if cbRegions:input-value = ? or  cbRegions:input-value = "?" then "" else cbRegions:input-value)
                                   and bcard.cardname = ratecard.cardname) 
     then
      next.
    create availRateCard.
    buffer-copy ratecard to availRateCard.
  end.

  open query brwAvailRateCard for each availRateCard
   where availRateCard.region <> cbRegions:input-value by availRateCard.region.

  open query brwSelectedRateCard for each selectedRateCard
   where selectedRateCard.cardname = "" by selectedRateCard.region .

  open query brwExistingRateCard for each RateCard
   where RateCard.region = cbRegions:input-value by ratecard.region .
              
  if cbRegions:input-value = "" or cbRegions:input-value = ? 
   then
    do:
      Existing_Cards:screen-value = 'Existing Cards in " " Region'.
      selected_Cards:screen-value = 'Staged Cards to Copy to " " Region'.
    end.
   else
   do:
     Existing_Cards:screen-value = "Existing Cards in " + cbRegions:input-value.
     selected_Cards:screen-value = "Staged Cards to Copy to " + cbRegions:input-value.
   end.

  if can-find(first availRateCard where availRateCard.region <> cbRegions:input-value) and 
      brwAvailRateCard:num-selected-rows ne 0
   then
    bAdd:sensitive = true.  
   else
    bAdd:sensitive = false. 

  if can-find(first selectedRateCard) and 
      brwSelectedRateCard:num-selected-rows ne 0
   then
     bRemove:sensitive  = true.
  else
     bRemove:sensitive  = false.

  if can-find(first selectedRateCard) 
   then
      btn_save:sensitive = true.      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

