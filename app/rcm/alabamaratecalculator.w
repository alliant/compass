&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly and Archana

  Created: 
  
  Modified:
  Date        Name     Comments
  07/01/2019  Sachin   Modified for standardisation and bug fixes.
  02/07/2022  Vignesh  Added Logic for error handling.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
{lib\rcm-std-def.i}
{lib\std-def.i}
{lib\winlaunch.i}

/*Temp table to contain endorsement data*/
{tt\rateendorsements.i &tablealias=ownerEndorsement}
{tt\rateendorsements.i &tablealias=loanEndorsement}
{tt\rateendorsements.i &tablealias=secondLoanEndorsement}

/* Temp tables for log */
{tt\ratelog.i}

/*Temp tables to manage dynamic checkbox and editor*/
{tt\rateendorsementui.i &tablealias=tthOwr}
{tt\rateendorsementui.i &tablealias=tthLdr}
{tt\rateendorsementui.i &tablealias=tthScndLoan}

/*Temp tables to show detailed premium of endorsements*/
{tt\rateendorsementpremium.i &tablealias=endorsDetail}


/*Temp tables to handle open endorsements windows*/
define temp-table openEndors
    field hInstance as handle.

/*Temp tables to handle open log windows*/
define temp-table openLog
    field hInstance as handle.
                                                                   


/* variables used in screen resizing*/
define variable endorsPos              as decimal                  no-undo.
define variable resultsPos             as decimal                  no-undo.
define variable fullheight             as decimal                  no-undo.
define variable cutwidth               as decimal                  no-undo.
define variable fullwidth              as decimal                  no-undo.
define variable cutheight              as decimal                  no-undo.
define variable endSelected            as logical   initial false  no-undo. 
define variable multiLoanSelected      as logical   initial false  no-undo. 

/*variables containing key value pair for ui input and output premium*/
define variable pcCalculatedPremium    as character                no-undo.
define variable pcInputValues          as character                no-undo.
define variable pcInputValuesPDF       as character                no-undo.
                                                                 
/* output parameter parsing variables */
define variable premiumOwner           as character                no-undo.
define variable premiumOEndors         as character                no-undo.
define variable premiumOEndorsDetail   as character                no-undo.
define variable premiumLoan            as character                no-undo.
define variable premiumLEndors         as character                no-undo.
define variable premiumLEndorsDetail   as character                no-undo.
define variable premiumScnd            as character                no-undo.
define variable premiumSEndors         as character                no-undo.
define variable premiumSEndorsDetail   as character                no-undo.
define variable premiumLenderCPL       as character                no-undo.
define variable premiumBuyerCPL        as character                no-undo.
define variable premiumSellerCPL       as character                no-undo.
define variable success                as character                no-undo.
define variable cardSetId              as character                no-undo.
define variable effectiveDate          as character                no-undo.
define variable serverErrMsg           as character                no-undo.
define variable grandTotal             as character                no-undo.

define variable ratesCode              as character                no-undo.
define variable ratesMsg               as character                no-undo.

/*type cased variables after parsing output parameter*/
define variable lSuccess               as logical   initial false  no-undo.

/* variables to populate propertytypecombobox */
define variable pcPropertyType         as character                no-undo.

/* variables to handle dynamic widget of endorsements*/
define variable iEndCount              as integer                  no-undo.
define variable iNextEndors            as integer                  no-undo.
define variable flag                   as logical   initial true   no-undo.

/*variable to handle second loan section*/
define variable secondLoanEndorsLoaded as logical   initial false  no-undo. 
define variable secondLoanEndorsList   as logical   initial false  no-undo. 

/*variable to handle simo case*/
define variable tempcoverageAmount     as character                no-undo.
define variable tempLoanAmount         as character                no-undo.

/*variable to launch calculator for multiple versions*/
define variable versionNo              as character                no-undo.

define variable logscreenparameter     as character                no-undo.
define variable pError                 as logical                  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD RemoveItemFromList C-Win 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btendorsement 
     LABEL "v" 
     SIZE 4.8 BY 1.13 TOOLTIP "Show endorsements".

DEFINE BUTTON btSecondLoan 
     LABEL " >" 
     SIZE 4.8 BY 1.13 TOOLTIP "Show".

DEFINE VARIABLE cbLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 41.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbPropType AS CHARACTER FORMAT "X(256)":U INITIAL "R" 
     LABEL "Property Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiCoverageAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Coverage Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanPriorAmt AS DECIMAL FORMAT "zzz,zzz,zzz":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiPriorAmt AS DECIMAL FORMAT "zzz,zzz,zzz":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.35.

DEFINE RECTANGLE RECT-60
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 5.35.

DEFINE VARIABLE tbSimultaneous AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.6 BY .57 NO-UNDO.

DEFINE RECTANGLE RECT-67
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 10.7.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 10.7.

DEFINE BUTTON btCalculate 
     LABEL "&Calculate" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Calculate(Alt + C)".

DEFINE BUTTON btClear 
     LABEL "Clear&x" 
     SIZE 7.2 BY 1.7 TOOLTIP "Clear(Alt + X)".

DEFINE BUTTON btLEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE BUTTON btlog 
     LABEL "Show Calcu&lations" 
     SIZE 7.2 BY 1.7 TOOLTIP "Show Ca&lculations(Alt + L)".

DEFINE BUTTON btOEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE BUTTON btRateSheet 
     LABEL "&PDF" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Print Rates(Alt + P)".

DEFINE VARIABLE fiBuyerCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "Buyer" 
     VIEW-AS FILL-IN 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiCPLAmt AS DECIMAL FORMAT "zz,zz9.99":U INITIAL 0 
     LABEL "CPL Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE figrandTotal AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Grand Total" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "Lender" 
     VIEW-AS FILL-IN 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderEndorsPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderPolicyPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiLendersEndorsList AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loan Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiLendersTotalPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerEndorsList AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerEndorsPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerPolicyPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnersTotalPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiSellerCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "Seller" 
     VIEW-AS FILL-IN 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.17.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 5.17.

DEFINE RECTANGLE RECT-70
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 2.04.

DEFINE BUTTON btScndClear 
     LABEL "SClear" 
     SIZE 4.8 BY 1.13 TOOLTIP "Clear Second Loan to enable hide".

DEFINE VARIABLE cbSecondLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fiSecondLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.35.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 10.83.

DEFINE BUTTON btSEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE VARIABLE fiScndLenderEndorsPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiScndLenderPolicyPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiScndLendersTotalPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiSecondLendersEndorsList AS CHARACTER FORMAT "X(256)":U 
     LABEL "Second Loan Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 45 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.17.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 263.8 BY 26.14 WIDGET-ID 100.

DEFINE FRAME frBasicEndors
     RECT-67 AT ROW 1 COL 2.2 WIDGET-ID 148
     RECT-68 AT ROW 1 COL 89 WIDGET-ID 150
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         NO-UNDERLINE THREE-D 
         AT COL 1 ROW 7.91
         SIZE 176.1 BY 10.85 WIDGET-ID 500.

DEFINE FRAME frLenderEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 90.6 ROW 1.09
         SCROLLABLE SIZE 86.6 BY 100 WIDGET-ID 700.

DEFINE FRAME frOwnerEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 1.09
         SCROLLABLE SIZE 85.6 BY 100 WIDGET-ID 600.

DEFINE FRAME frScndLoan
     btScndClear AT ROW 2.65 COL 83 WIDGET-ID 176 NO-TAB-STOP 
     fiSecondLoanAmt AT ROW 3.04 COL 39 COLON-ALIGNED WIDGET-ID 168
     cbSecondLoanRateType AT ROW 4.13 COL 39 COLON-ALIGNED WIDGET-ID 170
     "Second Loan" VIEW-AS TEXT
          SIZE 15.4 BY .61 AT ROW 2.35 COL 2.6 WIDGET-ID 174
          FONT 6
     RECT-61 AT ROW 2.57 COL 1 WIDGET-ID 172
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 176.4 ROW 1.1
         SIZE 87.6 BY 6.91 WIDGET-ID 800.

DEFINE FRAME frScndLoanResults
     fiSecondLendersEndorsList AT ROW 1.52 COL 39 COLON-ALIGNED WIDGET-ID 166 NO-TAB-STOP 
     fiScndLenderEndorsPremium AT ROW 2.57 COL 39 COLON-ALIGNED WIDGET-ID 160 NO-TAB-STOP 
     btSEndors AT ROW 2.57 COL 60.8 WIDGET-ID 168
     fiScndLenderPolicyPremium AT ROW 3.61 COL 39 COLON-ALIGNED WIDGET-ID 162 NO-TAB-STOP 
     fiScndLendersTotalPremium AT ROW 4.65 COL 39 COLON-ALIGNED WIDGET-ID 164 NO-TAB-STOP 
     RECT-63 AT ROW 1 COL 1 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 176.4 ROW 18.62
         SIZE 87.6 BY 8.52 WIDGET-ID 1300.

DEFINE FRAME frBasicResults
     btOEndors AT ROW 2.57 COL 52.6 WIDGET-ID 172
     btLEndors AT ROW 2.57 COL 140.2 WIDGET-ID 176
     fiLenderCPL AT ROW 7.39 COL 10.2 COLON-ALIGNED WIDGET-ID 156
     fiBuyerCPL AT ROW 7.39 COL 25.4 COLON-ALIGNED WIDGET-ID 162
     fiOwnerEndorsList AT ROW 1.52 COL 30.8 COLON-ALIGNED WIDGET-ID 140 NO-TAB-STOP 
     fiSellerCPL AT ROW 7.39 COL 41 COLON-ALIGNED WIDGET-ID 164
     btCalculate AT ROW 7 COL 147.8 WIDGET-ID 34
     btClear AT ROW 7 COL 155 WIDGET-ID 104
     btRateSheet AT ROW 7 COL 162.2 WIDGET-ID 152
     btlog AT ROW 7 COL 169.4 WIDGET-ID 146
     fiLendersEndorsList AT ROW 1.52 COL 118.4 COLON-ALIGNED WIDGET-ID 142 NO-TAB-STOP 
     fiOwnerEndorsPremium AT ROW 2.57 COL 30.8 COLON-ALIGNED WIDGET-ID 100 NO-TAB-STOP 
     fiLenderEndorsPremium AT ROW 2.57 COL 118.4 COLON-ALIGNED WIDGET-ID 48 NO-TAB-STOP 
     fiOwnerPolicyPremium AT ROW 3.61 COL 30.8 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     fiLenderPolicyPremium AT ROW 3.61 COL 118.4 COLON-ALIGNED WIDGET-ID 36 NO-TAB-STOP 
     fiOwnersTotalPremium AT ROW 4.65 COL 30.8 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     fiLendersTotalPremium AT ROW 4.65 COL 118.4 COLON-ALIGNED WIDGET-ID 68 NO-TAB-STOP 
     figrandTotal AT ROW 7.35 COL 118.4 COLON-ALIGNED WIDGET-ID 108 NO-TAB-STOP 
     fiCPLAmt AT ROW 7.39 COL 69.2 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     "Closing Protection Letters" VIEW-AS TEXT
          SIZE 29.6 BY .61 AT ROW 6.52 COL 3.8 WIDGET-ID 58
          FONT 6
     RECT-58 AT ROW 1 COL 2.2 WIDGET-ID 2
     RECT-62 AT ROW 1 COL 89 WIDGET-ID 148
     RECT-70 AT ROW 6.87 COL 2.2 WIDGET-ID 166
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 18.57
         SIZE 175.8 BY 8.52
         DEFAULT-BUTTON btCalculate WIDGET-ID 400.

DEFINE FRAME frBasic
     cbPropType AT ROW 1.26 COL 30.8 COLON-ALIGNED WIDGET-ID 60
     fiCoverageAmt AT ROW 3.04 COL 30.8 COLON-ALIGNED WIDGET-ID 16
     cbRateType AT ROW 4.13 COL 30.8 COLON-ALIGNED WIDGET-ID 12
     fiPriorAmt AT ROW 5.26 COL 30.8 COLON-ALIGNED WIDGET-ID 38
     fiVersion AT ROW 1.3 COL 167.4 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     fiLoanAmt AT ROW 3.04 COL 118.4 COLON-ALIGNED WIDGET-ID 46
     cbLoanRateType AT ROW 4.13 COL 118.4 COLON-ALIGNED WIDGET-ID 44
     fiLoanPriorAmt AT ROW 5.26 COL 118.4 COLON-ALIGNED WIDGET-ID 88
     tbSimultaneous AT ROW 3.3 COL 141.2 WIDGET-ID 116
     btSecondLoan AT ROW 6.57 COL 171.2 WIDGET-ID 158
     btendorsement AT ROW 6.57 COL 2.8 WIDGET-ID 150
     "Simultaneous" VIEW-AS TEXT
          SIZE 13 BY .61 AT ROW 3.17 COL 144 WIDGET-ID 162
     "Owner" VIEW-AS TEXT
          SIZE 7.4 BY .61 AT ROW 2.26 COL 3.8 WIDGET-ID 58
          FONT 6
     "Loan" VIEW-AS TEXT
          SIZE 5.8 BY .61 AT ROW 2.26 COL 90.6 WIDGET-ID 66
          FONT 6
     "Second Loan" VIEW-AS TEXT
          SIZE 15.6 BY .57 AT ROW 6.83 COL 155.6 WIDGET-ID 160
          FONT 6
     "Endorsements" VIEW-AS TEXT
          SIZE 16.8 BY .61 AT ROW 6.83 COL 8 WIDGET-ID 170
          FONT 6
     RECT-56 AT ROW 2.57 COL 2.2 WIDGET-ID 2
     RECT-60 AT ROW 2.57 COL 89 WIDGET-ID 64
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.1
         SIZE 176.2 BY 6.91 WIDGET-ID 200.

DEFINE FRAME frScndLoanEndors
     RECT-69 AT ROW 1.04 COL 1 WIDGET-ID 158
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 176.4 ROW 7.91
         SIZE 87.6 BY 10.86 WIDGET-ID 900.

DEFINE FRAME frScndLenderEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.8 ROW 1.1
         SCROLLABLE SIZE 85.6 BY 100 WIDGET-ID 1200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Alabama Rate Calculator"
         HEIGHT             = 26.39
         WIDTH              = 264.2
         MAX-HEIGHT         = 35.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 35.52
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frBasic:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicEndors:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicResults:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frLenderEndors:FRAME = FRAME frBasicEndors:HANDLE
       FRAME frOwnerEndors:FRAME = FRAME frBasicEndors:HANDLE
       FRAME frScndLenderEndors:FRAME = FRAME frScndLoanEndors:HANDLE
       FRAME frScndLoan:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frScndLoanEndors:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frScndLoanResults:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME frBasicResults:MOVE-BEFORE-TAB-ITEM (FRAME frBasic:HANDLE)
       XXTABVALXX = FRAME frScndLoanResults:MOVE-BEFORE-TAB-ITEM (FRAME frBasicResults:HANDLE)
       XXTABVALXX = FRAME frScndLoanEndors:MOVE-BEFORE-TAB-ITEM (FRAME frScndLoanResults:HANDLE)
       XXTABVALXX = FRAME frBasicEndors:MOVE-BEFORE-TAB-ITEM (FRAME frScndLoanEndors:HANDLE)
       XXTABVALXX = FRAME frScndLoan:MOVE-BEFORE-TAB-ITEM (FRAME frBasicEndors:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasic
   Custom                                                               */
/* SETTINGS FOR BUTTON btSecondLoan IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiVersion IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tbSimultaneous IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frBasicEndors
   Custom                                                               */
ASSIGN XXTABVALXX = FRAME frOwnerEndors:MOVE-BEFORE-TAB-ITEM (FRAME frLenderEndors:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasicResults
   NOT-VISIBLE Custom                                                   */
/* SETTINGS FOR BUTTON btlog IN FRAME frBasicResults
   NO-ENABLE                                                            */
ASSIGN 
       fiCPLAmt:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       figrandTotal:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderEndorsPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLendersEndorsList:HIDDEN IN FRAME frBasicResults           = TRUE
       fiLendersEndorsList:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLendersTotalPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerEndorsList:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerEndorsPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnersTotalPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

/* SETTINGS FOR FRAME frLenderEndors
                                                                        */
ASSIGN 
       FRAME frLenderEndors:HEIGHT           = 10.33
       FRAME frLenderEndors:WIDTH            = 85.6.

/* SETTINGS FOR FRAME frOwnerEndors
                                                                        */
ASSIGN 
       FRAME frOwnerEndors:HEIGHT           = 10.33
       FRAME frOwnerEndors:WIDTH            = 85.6.

/* SETTINGS FOR FRAME frScndLenderEndors
                                                                        */
ASSIGN 
       FRAME frScndLenderEndors:HEIGHT           = 10.33
       FRAME frScndLenderEndors:WIDTH            = 85.6.

/* SETTINGS FOR FRAME frScndLoan
                                                                        */
/* SETTINGS FOR BUTTON btScndClear IN FRAME frScndLoan
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frScndLoanEndors
                                                                        */
/* SETTINGS FOR FRAME frScndLoanResults
                                                                        */
ASSIGN 
       fiScndLenderEndorsPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

ASSIGN 
       fiScndLenderPolicyPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

ASSIGN 
       fiScndLendersTotalPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

ASSIGN 
       fiSecondLendersEndorsList:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasic
/* Query rebuild information for FRAME frBasic
     _Query            is NOT OPENED
*/  /* FRAME frBasic */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicEndors
/* Query rebuild information for FRAME frBasicEndors
     _Query            is NOT OPENED
*/  /* FRAME frBasicEndors */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicResults
/* Query rebuild information for FRAME frBasicResults
     _Query            is NOT OPENED
*/  /* FRAME frBasicResults */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frScndLoanEndors
/* Query rebuild information for FRAME frScndLoanEndors
     _Query            is NOT OPENED
*/  /* FRAME frScndLoanEndors */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frScndLoanResults
/* Query rebuild information for FRAME frScndLoanResults
     _Query            is NOT OPENED
*/  /* FRAME frScndLoanResults */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Alabama Rate Calculator */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, ust ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Alabama Rate Calculator */
DO:
  /* This event will close the window and terminate the procedure.  */
  for each openLog:
    if valid-handle(openLog.hInstance) then
      delete object openLog.hInstance.
  end.
  empty temp-table openLog.
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btCalculate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCalculate C-Win
ON CHOOSE OF btCalculate IN FRAME frBasicResults /* Calculate */
DO:
  run CalculatePremium in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME frBasicResults /* Clearx */
DO:
  run ClearAll in this-procedure no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME btendorsement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btendorsement C-Win
ON CHOOSE OF btendorsement IN FRAME frBasic /* v */
DO:
  if endSelected = false then
  do:
    assign
      endSelected = true.
      btEndorsement:load-image("images/s-up2.bmp") in frame frBasic. 
      btEndorsement:tooltip in frame frBasic = "Hide endorsements".
  end.
  else
  do:
    assign
      endSelected = false.
      btEndorsement:load-image("images/s-down2.bmp") in frame frBasic.
      btEndorsement:tooltip in frame frBasic = "Show endorsements".
  end.
  run setPosition  in this-procedure no-error.
  run LoadSecondLoanEndorsements in this-procedure no-error.
   if cbPropType:screen-value  eq "C" then
   run SetWidgeState in this-procedure (false) no-error .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btLEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btLEndors C-Win
ON CHOOSE OF btLEndors IN FRAME frBasicResults /* View Details */
DO:
  run endorsPremiumDetail in this-procedure(input premiumLEndorsDetail,
                                            input {&Lenders},
                                            output table endorsDetail)  no-error. 
  create openEndors.
  run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                                           input {&Lenders}) no-error.     
  empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btlog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btlog C-Win
ON CHOOSE OF btlog IN FRAME frBasicResults /* Show Calculations */
DO:
  create openLog.
  run wlogs.w persistent set openLog.hInstance (input table rateLog,
                                                input logscreenparameter) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btOEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOEndors C-Win
ON CHOOSE OF btOEndors IN FRAME frBasicResults /* View Details */
DO:
  run endorsPremiumDetail in this-procedure(input premiumOEndorsDetail,
                                            input {&Owners},
                                            output table endorsDetail) no-error. 
  create openEndors.
  run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                                           input {&Owners})no-error.  
  empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRateSheet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRateSheet C-Win
ON CHOOSE OF btRateSheet IN FRAME frBasicResults /* PDF */
DO:
  run GeneratePDF.w ("Alabama",
                    replace (pcCalculatedPremium,"=","^") ,
                    pcInputValuesPDF) no-error.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME btScndClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btScndClear C-Win
ON CHOOSE OF btScndClear IN FRAME frScndLoan /* SClear */
DO:
  run ClearSecondLoanSection in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME btSecondLoan
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSecondLoan C-Win
ON CHOOSE OF btSecondLoan IN FRAME frBasic /*  > */
DO:
  if multiLoanSelected = false then
  do:
    assign
      multiLoanSelected                     = true
      btSecondLoan:tooltip in frame frBasic = "Hide".

      btSecondLoan:load-image-insensitive("images/s-previous-i.bmp") in frame frBasic no-error.
      btSecondLoan:load-image("images/s-previous.bmp") in frame frBasic no-error.
  end.                                                                        
  else                                                                        
  do:
    assign
      multiLoanSelected                     = false
      btSecondLoan:tooltip in frame frBasic = "Show".

      btSecondLoan:load-image("images/s-next.bmp") in frame frBasic no-error.
      btSecondLoan:load-image-insensitive("images/s-next-i.bmp") in frame frBasic no-error.
  end.

  cbSecondLoanRateType:screen-value in frame frScndLoan = {&None}.
  run setPosition in this-procedure no-error.
  run LoadSecondLoanEndorsements in this-procedure no-error.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoanResults
&Scoped-define SELF-NAME btSEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSEndors C-Win
ON CHOOSE OF btSEndors IN FRAME frScndLoanResults /* View Details */
DO:
  run endorsPremiumDetail in this-procedure (input premiumSEndorsDetail,
                                             input {&Lenders},
                                             output table endorsDetail) no-error.  
  create openEndors.
  run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                  input {&SecondLoan})no-error.  
   empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME cbLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbLoanRateType C-Win
ON VALUE-CHANGED OF cbLoanRateType IN FRAME frBasic /* Rate to Apply */
DO:
  run setLoanPriorInfoState in this-procedure no-error.
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPropType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPropType C-Win
ON VALUE-CHANGED OF cbPropType IN FRAME frBasic /* Property Type */
DO:
  run PropertyTypeValuechange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRateType C-Win
ON VALUE-CHANGED OF cbRateType IN FRAME frBasic /* Rate to Apply */
DO:
  run setOwnerPriorInfoState in this-procedure no-error. 
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME cbSecondLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSecondLoanRateType C-Win
ON VALUE-CHANGED OF cbSecondLoanRateType IN FRAME frScndLoan /* Rate to Apply */
DO:
  if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0"                     or 
     cbSecondLoanRateType:screen-value in frame frScndLoan ne {&None} or
     secondLoanEndorsList eq true then
  assign
    btSecondLoan:sensitive in frame frBasic = false
    tbSimultaneous:checked in frame frBasic = true
    btScndClear:sensitive in frame frScndLoan = true.

  else
  assign
    btSecondLoan:sensitive in frame frBasic = true
    btScndClear:sensitive in frame frScndLoan = false.
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiBuyerCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiBuyerCPL C-Win
ON VALUE-CHANGED OF fiBuyerCPL IN FRAME frBasicResults /* Buyer */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME fiCoverageAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON LEAVE OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
  tempcoverageAmount = fiCoverageAmt:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON VALUE-CHANGED OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
  run CoverageAmountValueChange  in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiLenderCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLenderCPL C-Win
ON VALUE-CHANGED OF fiLenderCPL IN FRAME frBasicResults /* Lender */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME fiLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON LEAVE OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
  tempLoanAmount = fiLoanAmt:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON VALUE-CHANGED OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
  run LoanAmountValueChange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiLoanPriorAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanPriorAmt C-Win
ON VALUE-CHANGED OF fiLoanPriorAmt IN FRAME frBasic /* Prior Policy Amount */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiPriorAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiPriorAmt C-Win
ON VALUE-CHANGED OF fiPriorAmt IN FRAME frBasic /* Prior Policy Amount */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoanResults
&Scoped-define SELF-NAME fiSecondLendersEndorsList
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSecondLendersEndorsList C-Win
ON ENTRY OF fiSecondLendersEndorsList IN FRAME frScndLoanResults /* Second Loan Endorsements */
DO:
  if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0" 
    or cbSecondLoanRateType:screen-value in frame frScndLoan ne {&None} 
    or fiSecondLendersEndorsList:screen-value in frame frScndLoanResults ne "" then
      btSecondLoan:sensitive in frame frBasic  = false.
  else
      btSecondLoan:sensitive in frame frBasic = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSecondLendersEndorsList C-Win
ON VALUE-CHANGED OF fiSecondLendersEndorsList IN FRAME frScndLoanResults /* Second Loan Endorsements */
DO:
  if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0" 
    or cbSecondLoanRateType:screen-value in frame frScndLoan ne {&None} 
    or fiSecondLendersEndorsList:screen-value in frame frScndLoanResults ne "" then
      btSecondLoan:sensitive in frame frBasic  = false.
  else
      btSecondLoan:sensitive in frame frBasic = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME fiSecondLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSecondLoanAmt C-Win
ON VALUE-CHANGED OF fiSecondLoanAmt IN FRAME frScndLoan /* Loan Amount */
DO:
 if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0"                     or
    cbSecondLoanRateType:screen-value in frame frScndLoan ne {&None} or
    secondLoanEndorsList eq true then
   assign
     btSecondLoan  :sensitive in frame frBasic    = false
     tbSimultaneous:checked   in frame frBasic    = true
     btScndClear   :sensitive in frame frScndLoan = true.

 else
   assign
     btSecondLoan:sensitive in frame frBasic    = true
     btScndClear :sensitive in frame frScndLoan = false.

 run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiSellerCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSellerCPL C-Win
ON VALUE-CHANGED OF fiSellerCPL IN FRAME frBasicResults /* Seller */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME tbSimultaneous
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbSimultaneous C-Win
ON VALUE-CHANGED OF tbSimultaneous IN FRAME frBasic
DO:
  if tbSimultaneous:checked = true  then
    assign
      cbLoanRateType:screen-value               = cbLoanRateType:entry(1)
      fiLoanPriorAmt:screen-value               = "0"
      fiLoanPriorAmt:sensitive                  = false
      btSecondLoan  :sensitive in frame frBasic = true.
  else
  do:
    assign
      cbLoanRateType:screen-value                      = cbLoanRateType:entry(1)
      fiSecondLoanAmt:screen-value in frame frScndLoan = "0" 
      btSecondLoan  :sensitive in frame frBasic = false.
    if multiLoanSelected = true then
      apply 'choose' to btSecondLoan .
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
 /* file reads json for ui setup and maintain data*/


 publish "GetALPropertyType" (output pcPropertyType).

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

btClear:load-image("images/erase.bmp")                       in frame frBasicResults    no-error.
btClear:load-image-insensitive("images/erase-i.bmp")         in frame frBasicResults    no-error.
btCalculate:load-image("images/calc.bmp")                    in frame frBasicResults    no-error.
btCalculate:load-image-insensitive("images/calc-i.bmp")      in frame frBasicResults    no-error.
btLog:load-image("images/log.bmp")                           in frame frBasicResults    no-error.
btLog:load-image-insensitive("images/log-i.bmp")             in frame frBasicResults    no-error.
btEndorsement:load-image("images/s-down2.bmp")               in frame frBasic           no-error.
btEndorsement:load-image-insensitive("images/s-down2-i.bmp") in frame frBasic           no-error.
btSecondLoan:load-image("images/s-next.bmp")                 in frame frBasic           no-error.
btSecondLoan:load-image-insensitive("images/s-next-i.bmp")   in frame frBasic           no-error.
btScndClear:load-image("images/s-erase.bmp")                 in frame frScndLoan        no-error.
btScndClear:load-image-insensitive("images/s-erase-i.bmp")   in frame frScndLoan        no-error.
btRateSheet:load-image("images/pdf.bmp")                     in frame frBasicResults    no-error. 
btRateSheet:load-image-insensitive("images/pdf-i.bmp")       in frame frBasicResults    no-error.
btOEndors:load-image("images/s-lookup.bmp")                  in frame frBasicResults    no-error.
btOEndors:load-image-insensitive("images/s-lookup-i.bmp")    in frame frBasicResults    no-error.
btLEndors:load-image("images/s-lookup.bmp")                  in frame frBasicResults    no-error.
btLEndors:load-image-insensitive("images/s-lookup-i.bmp")    in frame frBasicResults    no-error.
btSEndors:load-image("images/s-lookup.bmp")                  in frame frScndLoanResults no-error.
btSEndors:load-image-insensitive("images/s-lookup-i.bmp")    in frame frScndLoanResults no-error. 
 

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

{lib/winshowscrollbars.i}
cbPropType:list-item-pairs = pcPropertyType.
cbPropType:screen-value = {&DefaultPropertyType}.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
{lib/win-main.i}
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

/* ui resize for initial setup*/

  run ShowScrollBars(frame frBasic:handle, no,no).
  run ShowScrollBars(frame frBasicResults:handle, no, no).
  run ShowScrollBars(frame frBasicEndors:handle, no, no).

  endorsPos  = frame frBasicEndors:row.
  resultsPos = frame frBasicResults:row .
  fullheight = frame frBasic:height +  frame frBasicEndors:height  +  frame frBasicResults:height.
  fullwidth  = frame frBasic:width  +  frame frScndLoan:width.
  cutheight  = frame frBasic:height +  frame frBasicResults:height.
  cutwidth   = frame frBasic:width  +  0.5.

  run SetPosition in this-procedure no-error.

  apply 'value-changed' to cbPropType.

/*publish to get the current selected version if launched by maintenance screen else use active version*/
  publish "GetCurrentValue" ("ALVersion", output versionNo).

  if versionNo ne "" then
    fiVersion:screen-value = versionNo.
  else 
    assign 
      fiVersion:hidden  = true
      fiVersion:visible = false.

  subscribe to "ApplyEntry" anywhere.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApplyEntry C-Win 
PROCEDURE ApplyEntry :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "entry" to  fiCoverageAmt in frame frBasic.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CalculatePremium C-Win 
PROCEDURE CalculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  pcInputValues:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable errorStatus        as logical   no-undo.      
  define variable errorMsg           as character no-undo.

  /* variable contain list of selected endorsement */                                   
  define variable ownerEndors        as character no-undo.    
  define variable loanEndors         as character no-undo.    
  define variable secondLoanEndors   as character no-undo.

  /* variable contain list of selected endorsement with proposed rate*/                                   
  define variable ownerEndorsPR      as character no-undo.    
  define variable loanEndorsPR       as character no-undo.    
  define variable secondLoanEndorsPR as character no-undo.

  define variable proposedRate       as decimal   no-undo.

  define variable iCountOEndors      as integer   no-undo.
  define variable iCountLEndors      as integer   no-undo.
  define variable iCountSEndors      as integer   no-undo.

  define variable pSuccess           as logical   no-undo.
  define variable pMsg               as character no-undo.   
  
  pcCalculatedPremium = "".                                                                      
  do with frame {&frame-name}:                                                
  end.                                                                        
  /*-------------------Client Validations------------------------*/
  run ClientValidation in this-procedure(output errorStatus,
                                         output errorMsg) no-error.
  
  if errorStatus = yes then
  do:
    message errorMsg
      view-as alert-box info buttons ok.
    btRateSheet:sensitive in frame frBasicResults = false.
    return no-apply.
  end.

  do with frame {&frame-name}:                                                
  end.  
  /*--------------------------------------Get Endorsement Lists--------------------------------------*/
  ownerEndors = fiOwnerEndorsList:screen-value in frame frBasicResults.
  ownerEndors = replace(ownerEndors, ",", "|").

  loanEndors = fiLendersEndorsList:screen-value in frame frBasicResults.
  loanEndors = replace(loanEndors, ",", "|").

  secondLoanEndors = fiSecondLendersEndorsList:screen-value in frame frScndLoanResults.
  secondLoanEndors = replace(secondLoanEndors, ",", "|").
  /*--------------------------Embed proposed rate in the endorsement list-----------------------------*/
  do iCountOEndors = 1 to num-entries(ownerEndors,"|"):
    publish "GetALProposedRate"(input entry(iCountOEndors,ownerEndors,"|"),
                              output proposedRate).
    ownerEndorsPR =  ownerEndorsPR + entry(iCountOEndors,ownerEndors,"|") + "-" + (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.
  end.
  
  do iCountLEndors = 1 to num-entries(loanEndors,"|"):
    publish "GetALProposedRate"(input entry(iCountLEndors,loanEndors,"|"),
                              output proposedRate).
    loanEndorsPR =  loanEndorsPR + entry(iCountLEndors,loanEndors,"|") + "-" + (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.
  end.
  
  do iCountSEndors = 1 to num-entries(secondLoanEndors,"|"):
    publish "GetALProposedRate"(input entry(iCountSEndors,secondLoanEndors,"|"),
                              output proposedRate).
    secondLoanEndorsPR =  secondLoanEndorsPR + entry(iCountSEndors,secondLoanEndors,"|") + "-" + (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.
  end.

  ownerEndorsPR = trim(ownerEndorsPR,"|").
  loanEndorsPR = trim(loanEndorsPR,"|").
  secondLoanEndorsPR = trim(secondLoanEndorsPR,"|").

  do with frame frBasic:
  end.
  /*-------------------------Input variable-------------------------------*/
  pcInputValues = "ratetype^"                   + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else cbRateType:screen-value +
                  ",coverageAmount^"            + string(int(fiCoverageAmt:screen-value))  + 
                  ",reissueCoverageAmount^"     + string(int(fiPriorAmt:screen-value))     + 
                  ",loanCoverageAmount^"        + string(int(fiLoanAmt:screen-value))      +  
                  ",loanRateType^"              + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else cbLoanRateType:screen-value +
                  ",loanReissueCoverageAmount^" + string(int(fiLoanPriorAmt:screen-value)) + 
                  ",ownerEndors^"               + ownerEndorsPR                            + 
                  ",loanEndors^"                + loanEndorsPR                             + 
                  ",simultaneous^"              + string(tbSimultaneous:screen-value)      +
                  ",propertyType^"              + string(cbPropType:screen-value)          +
                  ",Version^"                   + fiVersion:screen-value                   +
                  ",region^"                    + ""                                       +
                  ",lenderCPL^"                 + string(fiLenderCPL:screen-value)         +
                  ",buyerCPL^"                  + string(fiBuyerCPL:screen-value)          +
                  ",sellerCPL^"                 + string(fiSellerCPL:screen-value)         .

  pcInputValuesPDF = "ratetype^"                   + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else entry((lookup(cbRateType:screen-value, cbRateType:list-item-pairs,",") - 1),cbRateType:list-item-pairs, ",") +
                     ",coverageAmount^"            + string(int(fiCoverageAmt:screen-value))  + 
                     ",reissueCoverageAmount^"     + string(int(fiPriorAmt:screen-value))     + 
                     ",loanCoverageAmount^"        + string(int(fiLoanAmt:screen-value))      +  
                     ",loanRateType^"              + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else entry((lookup(cbLoanRateType:screen-value, cbLoanRateType:list-item-pairs,",") - 1),cbLoanRateType:list-item-pairs, ",") +
                     ",loanReissueCoverageAmount^" + string(int(fiLoanPriorAmt:screen-value)) + 
                     ",ownerEndors^"               + ownerEndorsPR                            + 
                     ",loanEndors^"                + loanEndorsPR                             + 
                     ",simultaneous^"              + string(tbSimultaneous:screen-value)      +
                     ",propertyType^"              + string(cbPropType:screen-value)          +
                     ",Version^"                   + fiVersion:screen-value                   +
                     ",region^"                    + ""                                       +
                     ",lenderCPL^"                 + string(fiLenderCPL:screen-value)         +
                     ",buyerCPL^"                  + string(fiBuyerCPL:screen-value)          +
                     ",sellerCPL^"                 + string(fiSellerCPL:screen-value)         +
                     ",ratetypecode^"               + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else cbRateType:screen-value +
                     ",loanRateTypecode^"          + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else cbLoanRateType:screen-value .

                                                                                                                                         

  if multiLoanSelected = true then
  do with frame frScndLoan:
    pcInputValues = pcInputValues + 
                    ",secondloanRateType^"        + if cbSecondLoanRateType:screen-value  = ? or cbSecondLoanRateType:screen-value  = "?" then "" else   cbSecondLoanRateType:screen-value +
                    ",secondloanCoverageAmount^"  + string(int(fiSecondLoanAmt:screen-value)) +
                    ",secondLoanEndors^"          + secondLoanEndorsPR.
    pcInputValuesPDF = pcInputValuesPDF + 
                       ",secondloanRateType^"        + if cbSecondLoanRateType:screen-value  = ? or cbSecondLoanRateType:screen-value  = "?" then "" else   entry((lookup(cbSecondLoanRateType:screen-value, cbSecondLoanRateType:list-item-pairs,",") - 1),cbSecondLoanRateType:list-item-pairs, ",") +
                       ",secondloanCoverageAmount^"  + string(int(fiSecondLoanAmt:screen-value)) +
                       ",secondLoanEndors^"          + secondLoanEndorsPR                        + 
                       ",secondloanRateTypecode^"    + if cbsecondLoanRateType:screen-value  = ? or cbsecondLoanRateType:screen-value  = "?" then "" else cbsecondLoanRateType:screen-value .
  end. 
                            
  publish "ALCalculatePremium" (input pcInputValues,
                                input {&Alabama},
                                input integer(versionNo),
                                output pcCalculatedPremium,
                                output table rateLog).

  /*--------------------Extracting output parameter and set premium on screen--------------------------------------*/ 
  run ExtractOutputParameters.

  logscreenparameter =  pcInputValues + ",cardSetId^" + cardSetId
                                      + ",StateID^" + {&Alabama}
                                      + ",manualEffectiveDate^" + (if effectiveDate eq "" then "" else string(date(effectiveDate), "99/99/99")).  
  btLog:sensitive = true.

  if lSuccess = false  or serverErrMsg ne "serverErrMsg" then
  do:
    message serverErrMsg
      view-as alert-box info buttons ok.
    btRateSheet:sensitive in frame frBasicResults = false.
    return no-apply.
  end.
  else if ratesCode >= '3000' and ratesMsg <> "" then
  do:
    message ratesMsg
      view-as alert-box info buttons ok.
  end.
  btRateSheet:sensitive in frame frBasicResults = true.
  
/*--------setup endorsement premium detail button-----------*/  
  if fiOwnerEndorsList:screen-value ne "" then
    btOEndors:sensitive = true.
  if fiLendersEndorsList:screen-value ne "" then
    btLEndors:sensitive = true.
  if fiSecondLendersEndorsList:screen-value ne "" then
    btSEndors:sensitive = true.

  assign
    fiOwnerEndorsPremium     :screen-value = premiumOEndors
    fiOwnerPolicyPremium     :screen-value = premiumOwner
    fiLenderEndorsPremium    :screen-value = premiumLEndors
    fiLenderPolicyPremium    :screen-value = premiumLoan
    fiScndLenderEndorsPremium:screen-value = premiumSEndors
    fiScndLenderPolicyPremium:screen-value = premiumScnd 
    fiOwnersTotalPremium     :screen-value = string(decimal(premiumOEndors)   + decimal(premiumOwner))
    fiLendersTotalPremium    :screen-value = string(decimal(premiumLEndors)   + decimal(premiumLoan))
    fiScndLendersTotalPremium:screen-value = string(decimal(premiumScnd)      + decimal(premiumSEndors))
    fiCPLAmt                 :screen-value = string(decimal(premiumLenderCPL) + decimal(premiumBuyerCPL)
                                                                              + decimal(premiumSellerCPL))
    fiGrandTotal:screen-value              = string(decimal(premiumOEndors) + decimal(premiumOwner)
                                           + decimal(premiumLEndors)        + decimal(premiumLoan)
                                           + decimal(premiumScnd)           + decimal(premiumSEndors)
                                           + decimal(premiumLenderCPL)      + decimal(premiumBuyerCPL)
                                           + decimal(premiumSellerCPL)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearAll C-Win 
PROCEDURE ClearAll :
/*------------------------------------------------------------------------------
  Purpose: reset screen for selected ratetype    
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasic:
  end.

  assign
    fiCoverageAmt       :screen-value                     = ""
    fiLoanAmt           :screen-value                     = ""
    fiPriorAmt          :screen-value                     = ""
    fiLoanPriorAmt      :screen-value                     = ""
    cbRateType          :screen-value                     = {&None}
    cbLoanRateType      :screen-value                     = {&None}
    fiSecondLoanAmt     :screen-value in frame frScndLoan = "0"
    cbSecondLoanRateType:screen-value in frame frScndLoan = {&None}.

  do with frame frBasicResults:
  end.

  assign
    fiLenderCPL:screen-value   = ""
    fiBuyerCPL  :screen-value  = ""
    fiSellerCPL :screen-value  = "".

  run RefreshEndors in this-procedure ("") no-error.

  apply 'leave'         to fiCoverageAmt.
  apply 'leave'         to fiLoanAmt.
  apply 'value-changed' to fiCoverageAmt.
  apply 'value-changed' to fiLoanAmt.
  apply 'value-changed' to cbRateType.
  apply 'value-changed' to cbLoanRateType.
  apply 'value-changed' to cbPropType.

  pcInputValues = "".

  run ClearResults in this-procedure no-error. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearResults C-Win 
PROCEDURE ClearResults :
/*------------------------------------------------------------------------------
  Purpose: for any change in user input reset results 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame frBasicResults:
 end.
 assign
   fiOwnerEndorsPremium:screen-value   = ""
   fiOwnerPolicyPremium:screen-value   = ""
   fiOwnersTotalPremium:screen-value   = ""
   fiLenderEndorsPremium:screen-value  = ""
   fiLenderPolicyPremium:screen-value  = ""
   fiLendersTotalPremium:screen-value  = ""
   fiGrandTotal         :screen-value  = ""
   fiCPLAmt             :screen-value  = "".
 
 do with frame frScndLoanResults:
 end.
 assign
   fiScndLenderEndorsPremium:screen-value                         = ""
   fiScndLenderPolicyPremium:screen-value                         = ""
   fiScndLendersTotalPremium:screen-value                         = ""
   btRateSheet              :sensitive in frame frBasicResults    = false
   btlog                    :sensitive in frame frBasicResults    = false
   btOEndors                :sensitive in frame frBasicResults    = false
   btLEndors                :sensitive in frame frBasicResults    = false
   btSEndors                :sensitive in frame frScndLoanResults = false.

 for each openEndors:      
    if valid-handle(openEndors.hInstance) then
      delete object openEndors.hInstance no-error.
  end.
  empty temp-table openEndors no-error. 

 pcCalculatedPremium = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearSecondLoanSection C-Win 
PROCEDURE ClearSecondLoanSection :
/*------------------------------------------------------------------------------
  Purpose: clear out only second loan section  
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frScndLoan:
  end.
  assign 
    fiSecondLoanAmt:screen-value      = "0"
    cbSecondLoanRateType:screen-value = {&None}.

  run RefreshEndors in this-procedure ({&SecondLoan}) no-error.
  
  btSecondLoan:sensitive in frame frBasic     = true.
  btScndClear :sensitive  in frame frScndLoan = false.
  
  run ClearResults  in this-procedure no-error. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClientValidation C-Win 
PROCEDURE ClientValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
  
------------------------------------------------------------------------------*/

 define output parameter errorStatus  as logical   no-undo. 
 define output parameter errorMsg     as character no-undo. 
 
 do with frame frBasic:
 end.
            
 if ((fiCoverageAmt:screen-value ne "" and fiCoverageAmt:screen-value ne "0") or cbRateType:screen-value = {&NaUI}) and
     cbRateType:screen-value ne  {&None} and 
     cbLoanRateType:screen-value = {&NaUI} then
 do:
   errorStatus = false.
   return.
 end.
 
if ((fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0") or cbLoanRateType:screen-value = {&NaUI}) and
     cbLoanRateType:screen-value ne  {&None} and 
     cbRateType:screen-value = {&NaUI} then
do:
  errorStatus = false.
  return.
end.

 if fiCoverageAmt:screen-value = "" or fiCoverageAmt:screen-value = "0" then
 do:
   if can-find(first tthowr where tthowr.hChecked = true) and cbRateType:screen-value ne {&NaUI} then
   do:
       errorMsg =  "Please enter coverage amount.".
       errorStatus = true.
       return.
   end.
   if fiLoanAmt:screen-value = "" or fiLoanAmt:screen-value = "0" then
   do: 
     if can-find(first tthLdr where tthLdr.hChecked = true) and cbLoanRateType:screen-value ne {&NaUI} then
     do:
       errorMsg =  "Please enter loan amount.".
       errorStatus = true.
       return.
     end.
     if (cbRateType:screen-value = {&NaUI} and cbLoanRateType:screen-value = {&None}) or 
         (cbLoanRateType:screen-value = {&NaUI} and cbRateType:screen-value = {&None}) then
     do:         
       errorStatus = false.
       return.
     end.
     if (cbRateType:screen-value ne {&None} and cbRateType:screen-value ne {&NaUI}) and cbLoanRateType:screen-value ne {&None}  then
     do:
       errorMsg =  "Please enter coverage amount.".
       errorStatus = true.
       return.
     end.
     if (cbLoanRateType:screen-value ne {&None} and cbLoanRateType:screen-value ne {&NaUI}) then
     do:
       errorMsg =  "Please enter loan amount.".
       errorStatus = true.
       return.
     end.

     errorMsg = "Please enter coverage amount.".
     errorStatus = true.
     return.
   end. /* if fiLoanAmt */  

   else if cbLoanRateType:screen-value ne {&None}  then
   do:
     if cbRateType:screen-value ne {&None} then
     do:
       errorMsg =  "Please enter coverage amount.".
       errorStatus = true.
       return.
     end.      
   end.

   else if cbLoanRateType:screen-value = {&None} then
   do:
      errorMsg =  "Please select loan rate type.".
      errorStatus = true.
      return.
   end.

   if fiLoanPriorAmt:sensitive and (fiLoanPriorAmt:screen-value eq "" or fiLoanPriorAmt:screen-value eq "0" ) then
   do:         
     errorMsg =  "Please enter prior loan amount.".
     errorStatus = true.
     return.
   end.

 end. /* if fiCoverageAmt */
 
 else if cbRateType:screen-value = {&None}  then
 do:
   errorMsg =  "Please select rate type.".
   errorStatus = true.
   return.
 end.

 else if fiPriorAmt:sensitive and (fiPriorAmt:screen-value eq "" or fiPriorAmt:screen-value eq "0" ) then
 do:
   errorMsg =  "Please enter Prior Owner amount.".
   errorStatus = true.
   return.
 end.
 
 else if (fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0" ) and
       (cbLoanRateType:screen-value = {&None}) then
 do:
   errorMsg =  "Please select loan rate type.".
   errorStatus = true.
   return.
 end.
 
 else if (fiLoanAmt:screen-value eq "" or fiLoanAmt:screen-value eq "0" ) and
       (cbLoanRateType:screen-value ne {&None}) then
 do:
   errorMsg =  "Please enter loan amount.".
   errorStatus = true.
   return.
 end.

 else if (fiLoanAmt:screen-value eq "" or fiLoanAmt:screen-value eq "0" ) and
       (cbLoanRateType:screen-value eq {&None}) 
     and can-find(first tthLdr where tthLdr.hChecked = true) then
 do:
   errorMsg =  "Please enter loan amount.".
   errorStatus = true.
   return.
 end. 

 else if fiLoanPriorAmt:sensitive and (fiLoanPriorAmt:screen-value eq "" or fiLoanPriorAmt:screen-value eq "0" ) then
 do:
   errorMsg =  "Please enter Prior Loan amount.".
   errorStatus = true.
   return.
 end.

 do with frame frScndLoan:
 end.
 if multiLoanSelected = true then
 do:
   if (fiSecondLoanAmt:screen-value = "" or fiSecondLoanAmt:screen-value = "0") and (cbSecondLoanRateType:screen-value eq {&None}) and 
       can-find(first tthScndLoan where tthScndLoan.hChecked = true)  then
   do:
     errorMsg =  "Please enter second loan amount.".
     errorStatus = true.
     return.
   end.
   if (fiSecondLoanAmt:screen-value = "" or fiSecondLoanAmt:screen-value = "0") and (cbSecondLoanRateType:screen-value ne {&None})  then
   do:
     errorMsg =  "Please enter second loan amount.".
     errorStatus = true.
     return.
   end.
   else if (cbSecondLoanRateType:screen-value = {&None}) and (fiSecondLoanAmt:screen-value ne "" and fiSecondLoanAmt:screen-value ne "0")  then
   do:
     errorMsg =  "Please select second loan rate type.".
     errorStatus = true.
     return.
   end.
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CoverageAmountValueChange C-Win 
PROCEDURE CoverageAmountValueChange :
/*------------------------------------------------------------------------------
  Purpose:     Handle simultaneous case (if user enters amount in both owner 
               and loan section then it is considered as simultaneous case
               and action to be taken
               - update ratetype list
               - handle simo checkbox
               - handle second loan button)
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable pclistOwner               as character no-undo.
 define variable pclistLoan                as character no-undo.
 define variable pclistScndLoan            as character no-undo.
 define variable cRatetype                 as character no-undo.
 define variable cLoanRatetype             as character no-undo.
 define variable cSecondLoanRatetype       as character no-undo.

 do with frame frBasic:
 end.
 
 assign
   cRatetype           = cbRateType          :screen-value
   cLoanRatetype       = cbLoanRateType      :screen-value
   cSecondLoanRatetype = cbSecondLoanRateType:screen-value in frame frScndLoan.

 if fiCoverageAmt:screen-value ne "0" and tempLoanAmount ne "" and tempLoanAmount ne "0" then
 do:
   assign
     tbSimultaneous:checked = true.
   if cbPropType:screen-value = "R" or cbPropType:screen-value = "M" then
     assign
       btSecondLoan:sensitive in frame frBasic = true
       btSecondLoan:sensitive                  = true.
   publish "GetALSimoComboList"(cbPropType:screen-value,
                              yes,
                              output pclistOwner,  
                              output pclistLoan,  
                              output pclistScndLoan).
 end. /* if fiCoverageAmt */

 else 
 do:
   assign
     tbSimultaneous:checked = false.
   if cbPropType:screen-value ne "R" and cbPropType:screen-value ne "M" then
   do:
     btSecondLoan:sensitive = false.
     apply 'value-changed' to btSecondLoan.
   end.
   publish "GetALComboLists"(cbPropType:screen-value,
                           output pclistOwner,  
                           output pclistLoan,  
                           output pclistScndLoan).
 end.

 run ClearResults.

 apply 'value-changed' to tbSimultaneous.
 cRatetype = cbRateType:screen-value.

 assign
   cbRateType          :list-item-pairs                     =  trim(pclistOwner, ",")
   cbLoanRateType      :list-item-pairs                     =  trim(pclistLoan, ",")
   cbSecondLoanRateType:list-item-pairs in frame frScndLoan =  trim(pclistScndLoan, ",").

 if lookup(cRatetype,cbRateType:list-item-pairs) = 0 
  then
   cbRateType:screen-value     = entry(2,cbRateType:list-item-pairs,",").
 else 
   cbRateType:screen-value     = cRatetype.

 if lookup(cLoanRatetype,cbLoanRateType:list-item-pairs) = 0 
  then
   cbLoanRateType:screen-value     = entry(2,cbLoanRateType:list-item-pairs,",").
 else 
   cbLoanRateType:screen-value     = cLoanRatetype.

 if lookup(cSecondLoanRatetype,cbSecondLoanRateType:list-item-pairs) = 0 
  then
   cbSecondLoanRateType:screen-value     = entry(2,cbSecondLoanRateType:list-item-pairs,",").
 else 
   cbSecondLoanRateType:screen-value     = cSecondLoanRatetype.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DoCheckbox C-Win 
PROCEDURE DoCheckbox :
/*------------------------------------------------------------------------------
  Purpose: update the list on ui for selected checkbox of owner loan and second
           loan 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                                                                                                                                                                       
find first tthOwr where tthowr.hToggle = self no-error.
if available tthowr then
do:
  if tthowr.hChecked = false then
    do:
      assign tthowr.hChecked        = true
             tthowr.hEditor:fgcolor = 9.
      find first ownerEndorsement where ownerEndorsement.endorsementCode = tthowr.hseq no-error.
      if available ownerEndorsement then
        fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:screen-value in frame frBasicResults + "," + ownerEndorsement.endorsementCode , ",").

    end.
  else
    do:
      assign tthowr.hChecked        = false
             tthowr.hEditor:fgcolor = 0.
      find first ownerEndorsement where ownerEndorsement.endorsementCode = tthowr.hseq no-error.
      if available ownerEndorsement then
        fiOwnerEndorsList:screen-value in frame frBasicResults = RemoveItemFromList(fiOwnerEndorsList:screen-value in frame frBasicResults , string(ownerEndorsement.endorsementCode) , ",").
    end.
end.

find first tthLdr where tthLdr.hToggle = self no-error.
if available tthLdr then
do:
  if tthLdr.hChecked = false then
    do:
      assign tthLdr.hChecked        = true
             tthLdr.hEditor:fgcolor = 9.
      find first loanEndorsement where loanEndorsement.endorsementCode = tthLdr.hseq no-error.
      if available loanEndorsement then
        fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:screen-value in frame frBasicResults + "," + loanEndorsement.endorsementCode , ",").
    end.
  else
    do:
      assign tthLdr.hChecked        = FALSE
             tthLdr.hEditor:fgcolor = 0.
      find first loanEndorsement where loanEndorsement.endorsementCode = tthLdr.hseq no-error.
      if available loanEndorsement then
        fiLendersEndorsList:screen-value in frame frBasicResults = RemoveItemFromList(fiLendersEndorsList:screen-value in frame frBasicResults , string(loanEndorsement.endorsementCode) , ",").
    end.
end.

find first tthScndLoan where tthScndLoan.hToggle = self no-error.
if available tthScndLoan then
do:
  if tthScndLoan.hChecked = false then
    do:
      assign tthScndLoan.hChecked        = true
             tthScndLoan.hEditor:fgcolor = 9.
      find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = tthScndLoan.hseq no-error.
      if available secondLoanEndorsement then
        fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = trim(fiSecondLendersEndorsList:screen-value in frame frScndLoanResults + "," + secondLoanEndorsement.endorsementCode , ",").
    end.
  else
    do:
      assign tthScndLoan.hChecked        = FALSE
             tthScndLoan.hEditor:fgcolor = 0.
      find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = tthScndLoan.hseq no-error.
      if available secondLoanEndorsement then
        fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = RemoveItemFromList(fiSecondLendersEndorsList:screen-value in frame frScndLoanResults , string(secondLoanEndorsement.endorsementCode) , ",").
    end.
  if fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = "" then
    secondLoanEndorsList = false.
  else 
    secondLoanEndorsList = true.

  if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0" 
    or cbSecondLoanRateType:screen-value in frame frScndLoan ne {&None} 
    or secondLoanEndorsList eq true then
      assign
        btSecondLoan:sensitive in frame frBasic   = false
        tbSimultaneous:checked in frame frBasic   = true
        btScndClear:sensitive in frame frScndLoan = true.
  else
      assign
        btSecondLoan:sensitive in frame frBasic   = true
        btScndClear:sensitive in frame frScndLoan = false.
end.
run ClearResults in this-procedure no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DoEditor C-Win 
PROCEDURE DoEditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter cbSeq as handle      no-undo.
 
 if cbSeq:checked = false then
 do:
   apply 'value-changed' to cbSeq.
   cbSeq:checked = true.
   self:fgcolor  = 9.
   apply 'entry' to cbSeq.
 end.
 
 else if cbSeq:checked = true then
 do:
   apply 'value-changed' to cbSeq.
   cbSeq:checked = false.
   self:fgcolor  = 0.
   apply 'entry' to cbSeq.
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW FRAME frOwnerEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frOwnerEndors}
  VIEW FRAME frLenderEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frLenderEndors}
  DISPLAY cbPropType fiCoverageAmt cbRateType fiPriorAmt fiVersion fiLoanAmt 
          cbLoanRateType fiLoanPriorAmt tbSimultaneous 
      WITH FRAME frBasic IN WINDOW C-Win.
  ENABLE cbPropType fiCoverageAmt cbRateType fiPriorAmt fiLoanAmt 
         cbLoanRateType fiLoanPriorAmt btendorsement RECT-56 RECT-60 
      WITH FRAME frBasic IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasic}
  VIEW FRAME frScndLenderEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLenderEndors}
  DISPLAY fiSecondLoanAmt cbSecondLoanRateType 
      WITH FRAME frScndLoan IN WINDOW C-Win.
  ENABLE RECT-61 fiSecondLoanAmt cbSecondLoanRateType 
      WITH FRAME frScndLoan IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoan}
  ENABLE RECT-67 RECT-68 
      WITH FRAME frBasicEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicEndors}
  ENABLE RECT-69 
      WITH FRAME frScndLoanEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoanEndors}
  DISPLAY fiLenderCPL fiBuyerCPL fiOwnerEndorsList fiSellerCPL 
          fiLendersEndorsList fiOwnerEndorsPremium fiLenderEndorsPremium 
          fiOwnerPolicyPremium fiLenderPolicyPremium fiOwnersTotalPremium 
          fiLendersTotalPremium figrandTotal fiCPLAmt 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  ENABLE btOEndors btLEndors fiLenderCPL fiBuyerCPL fiOwnerEndorsList 
         fiSellerCPL btCalculate btClear btRateSheet fiLendersEndorsList 
         fiOwnerEndorsPremium fiLenderEndorsPremium fiOwnerPolicyPremium 
         fiLenderPolicyPremium fiOwnersTotalPremium fiLendersTotalPremium 
         figrandTotal fiCPLAmt RECT-58 RECT-62 RECT-70 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicResults}
  DISPLAY fiSecondLendersEndorsList fiScndLenderEndorsPremium 
          fiScndLenderPolicyPremium fiScndLendersTotalPremium 
      WITH FRAME frScndLoanResults IN WINDOW C-Win.
  ENABLE RECT-63 fiSecondLendersEndorsList fiScndLenderEndorsPremium btSEndors 
         fiScndLenderPolicyPremium fiScndLendersTotalPremium 
      WITH FRAME frScndLoanResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoanResults}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE endorsPremiumDetail C-Win 
PROCEDURE endorsPremiumDetail :
/*------------------------------------------------------------------------------
  Purpose: create endorsement temp table for detailed premium to show on UI     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter ipPremiumEndorsDetail as character.
define input parameter ipEndorsType          as character.
define output parameter table for endorsDetail.

define variable iCount as integer  no-undo.
if ipEndorsType = {&Owners} or ipEndorsType = "" then
  do iCount = 1 to num-entries(ipPremiumEndorsDetail,"|"):    
    find first ownerEndorsement where ownerEndorsement.endorsementCode = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-") and (ownerEndorsement.endorsementType = ipEndorsType or ownerEndorsement.endorsementType = "") no-error.
    if available ownerEndorsement then
    do:        
        create endorsDetail.
        assign
          endorsDetail.endorsCode    = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-")
          endorsDetail.endorsPremium = decimal(entry(2,entry(iCount,ipPremiumEndorsDetail,"|"),"-"))
          endorsDetail.endorsDesc    = ownerEndorsement.description.

    end.    
  end.
else if ipEndorsType = {&Lenders} or ipEndorsType = "" then
  do iCount = 1 to num-entries(ipPremiumEndorsDetail,"|"):    
    find first loanEndorsement where loanEndorsement.endorsementCode = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-") and (loanEndorsement.endorsementType = ipEndorsType or loanEndorsement.endorsementType = "") no-error.
    if available loanEndorsement then
    do:        
        create endorsDetail.
        assign
          endorsDetail.endorsCode    = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-")
          endorsDetail.endorsPremium = decimal(entry(2,entry(iCount,ipPremiumEndorsDetail,"|"),"-"))
          endorsDetail.endorsDesc    = loanEndorsement.description.

    end.    
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExtractOutputParameters C-Win 
PROCEDURE ExtractOutputParameters :
/*------------------------------------------------------------------------------
  Purpose: parse output parameter of calculated premium 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable iCount  as integer no-undo.

assign
  premiumOwner         = "premiumOwner"
  premiumOEndors       = "premiumOEndors"
  premiumOEndorsdetail = "premiumOEndorsdetail"
  premiumLoan          = "premiumLoan"
  premiumLEndors       = "premiumLEndors"
  premiumLEndorsdetail = "premiumLEndorsdetail"
  premiumScnd          = "premiumScnd"
  premiumSEndors       = "premiumSEndors"
  premiumSEndorsdetail = "premiumSEndorsdetail"
  success              = "success"
  serverErrMsg         = "serverErrMsg"
  grandTotal           = "grandTotal"
  premiumLenderCPL     = "premiumLenderCPL"
  premiumBuyerCPL      = "premiumBuyerCPL"
  premiumSellerCPL     = "premiumSellerCPL"
  cardSetId            = "cardSetId"
  effectiveDate        = "manualEffectiveDate"
  ratesCode            = "RatesCode"
  ratesMsg             = "RatesMsg".

do iCount = 1 to num-entries(pcCalculatedPremium,","):

case entry(1,entry(iCount,pcCalculatedPremium,","),"="):
  
    when premiumOwner  then
         premiumOwner                  = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumOEndors  then
         premiumOEndors                = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumOEndorsDetail  then
         premiumOEndorsDetail          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumLoan then
         premiumLoan                   = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumLEndors  then
         premiumLEndors                = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumLEndorsDetail  then
         premiumLEndorsDetail          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumScnd  then
         premiumScnd                   = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumSEndors  then
         premiumSEndors                = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumSEndorsDetail  then
         premiumSEndorsDetail          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when success then
         success                       = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when serverErrMsg then
         serverErrMsg                  = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumLenderCPL  then
         premiumLenderCPL              = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumBuyerCPL  then
         premiumBuyerCPL               = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumSellerCPL  then
         premiumSellerCPL              = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when cardSetId  then                            
         cardSetId                     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                        
                                                                                                     
    when effectiveDate  then
         effectiveDate                 = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.

    when ratesCode then                                                                                                                               
         ratesCode                     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error. 
           
    when ratesMsg then                                                                                                                               
         ratesMsg                      = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.         
    
  end case.  
end.
lSuccess = logical(success) no-error.

 if  premiumOwner = "premiumOwner" then
     premiumOwner = "0".
 if  premiumOEndors = "premiumOEndors" then
     premiumOEndors = "0".
 if  premiumLoan = "premiumLoan" then
     premiumLoan = "0".
 if  premiumLEndors = "premiumLEndors" then
     premiumLEndors = "0".
 if  premiumScnd = "premiumScnd" then
     premiumScnd = "0".
 if  premiumSEndors = "premiumSEndors" then
     premiumSEndors = "0".
 if  premiumLenderCPL = "premiumLenderCPL" then
     premiumLenderCPL = "0".
 if  premiumBuyerCPL = "premiumBuyerCPL" then
     premiumBuyerCPL = "0".
 if  premiumSellerCPL = "premiumSellerCPL" then
     premiumSellerCPL = "0".       
  if ratesCode = "RateCode" then
     ratesCode = "".          
  if ratesMsg = "RatesMsg" then
     ratesMsg = "".        
             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InitializeFrameLEndors C-Win 
PROCEDURE InitializeFrameLEndors :
/*------------------------------------------------------------------------------
  Purpose: initialize endosement frame for lender create check box and editor
           for description for each endorsement  
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name} :
end.
iEndCount = 0.

{lib\cleanCheckBox.i &hTemp = "tthldr"}.

empty temp-table tthldr no-error.

assign iNextEndors = 2.

fiLendersEndorsList:screen-value in frame frBasicResults = "".

for each loanEndorsement:
  iEndCount = iEndCount + 1.
  {lib\dynamicCheckBox.i &tableName = "loanEndorsement" , &seq = iEndCount , &frameName = "frLenderEndors" , &hTemp = "tthldr"}.
  iNextEndors = iNextEndors + 2.
  if iNextEndors ge 12 then frame frLenderEndors:virtual-height = frame frLenderEndors:virtual-height + 2.
end. 

if iEndCount > 0 then
  frame frLenderEndors:virtual-height = iEndCount * 2.5.

run ShowScrollBars(frame frLenderEndors:handle, no, yes).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InitializeFrameOEndors C-Win 
PROCEDURE InitializeFrameOEndors :
/*------------------------------------------------------------------------------
  Purpose: initialize endorsement frame for owner create check box and editor
           for description for each endorsement    
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name} :
end.
iEndCount = 0.

{lib\cleanCheckBox.i &hTemp = "tthOwr"}.

empty temp-table tthOwr no-error.
assign iNextEndors = 2.

fiOwnerEndorsList:screen-value in frame frBasicResults = "".

 for each ownerEndorsement:
  iEndCount = iEndCount + 1.
  {lib\dynamicCheckBox.i &tableName = "ownerEndorsement" &seq = iEndCount , &frameName = "frOwnerEndors" , &hTemp = "tthOwr"}.
  if iNextEndors ge 12 then frame frOwnerEndors:virtual-height = frame frOwnerEndors:virtual-height + 2.
  iNextEndors = iNextEndors + 2.  
end. 

if iEndCount > 0 then
  frame frOwnerEndors:virtual-height = iEndCount * 2.5.

run ShowScrollBars(frame frOwnerEndors:handle, no, yes).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InitializeFrameSEndors C-Win 
PROCEDURE InitializeFrameSEndors :
/*------------------------------------------------------------------------------
  Purpose: initialize endorsement frame for second loan create check box and editor
           for description for each endorsement 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name} :
end.
iEndCount = 0.

{lib\cleanCheckBox.i &hTemp = "tthScndLoan"}.

empty temp-table tthScndLoan no-error.
assign iNextEndors = 2.

fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = "".

 for each secondLoanEndorsement:
  iEndCount = iEndCount + 1.
  {lib\dynamicCheckBox.i &tableName = "secondLoanEndorsement" &seq = iEndCount , &frameName = "frScndLenderEndors" , &hTemp = "tthScndLoan"}.
  if iNextEndors ge 12 then frame frScndLenderEndors:virtual-height = frame frScndLenderEndors:virtual-height + 2.
  iNextEndors = iNextEndors + 2.  
end. 

if iEndCount > 0 then
  frame frScndLenderEndors:virtual-height = iEndCount * 2.5.

run ShowScrollBars(frame frScndLenderEndors:handle, no, yes).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSecondLoanEndorsements C-Win 
PROCEDURE LoadSecondLoanEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if (endSelected) and (multiLoanSelected)  and (not secondLoanEndorsLoaded) then
do:
  run InitializeFrameSEndors  in this-procedure no-error.
  secondLoanEndorsLoaded = true.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoanAmountValueChange C-Win 
PROCEDURE LoanAmountValueChange :
/*------------------------------------------------------------------------------
  Purpose:     Handle simultaneous case (if user enters amount in both owner 
               and loan section then it is considered as simultaneous case
               and action to be taken
               - update ratetype list
               - handle simo checkbox
               - handle second loan button)
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable pclistOwner               as character no-undo.
 define variable pclistLoan                as character no-undo.
 define variable pclistScndLoan            as character no-undo.
 define variable cRatetype                 as character no-undo.
 define variable cLoanRatetype             as character no-undo.
 define variable cSecondLoanRatetype       as character no-undo.

 do with frame frBasic:
 end.
 
 assign
   cRatetype           = cbRateType:screen-value
   cLoanRatetype       = cbLoanRateType:screen-value
   cSecondLoanRatetype = cbSecondLoanRateType:screen-value in frame frScndLoan.
 
 if tempcoverageAmount ne "0" and fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0"  then
 do:
   assign
     tbSimultaneous:checked = true.
   if cbPropType:screen-value = "R" or cbPropType:screen-value = "M" then
     assign
       btSecondLoan:sensitive in frame frBasic = true
       btSecondLoan:sensitive = true.
   publish "GetALSimoComboList"(cbPropType:screen-value,
                              yes,
                              output pclistOwner,  
                              output pclistLoan,  
                              output pclistScndLoan).
 end. /* if tempcoverageAmount */
 else 
 do:
   assign
     tbSimultaneous:checked = false.
     if cbPropType:screen-value ne "R" and cbPropType:screen-value ne "M" then
   do:
     btSecondLoan:sensitive in frame frBasic = false.
     btSecondLoan:sensitive = false.
     apply 'value-changed' to btSecondLoan.
   end.
   publish "GetALComboLists"(cbPropType:screen-value,
                           output pclistOwner,  
                           output pclistLoan,  
                           output pclistScndLoan).
 end.

 run ClearResults  in this-procedure no-error.
 apply 'value-changed' to tbSimultaneous.
 cRatetype = cbRateType:screen-value.

 assign
   cbRateType          :list-item-pairs                     =  trim(pclistOwner, ",")
   cbLoanRateType      :list-item-pairs                     =  trim(pclistLoan, ",")
   cbSecondLoanRateType:list-item-pairs in frame frScndLoan =  trim(pclistScndLoan, ",").

 if lookup(cRatetype,cbRateType:list-item-pairs) = 0 
  then
   cbRateType:screen-value     = entry(2,cbRateType:list-item-pairs,",").
 else 
   cbRateType:screen-value     = cRatetype.

 if lookup(cLoanRatetype,cbLoanRateType:list-item-pairs) = 0 
  then
   cbLoanRateType:screen-value     = entry(2,cbLoanRateType:list-item-pairs,",").
 else 
   cbLoanRateType:screen-value     = cLoanRatetype.

 if lookup(cSecondLoanRatetype,cbSecondLoanRateType:list-item-pairs) = 0 
  then
   cbSecondLoanRateType:screen-value     = entry(2,cbSecondLoanRateType:list-item-pairs,",").
 else 
   cbSecondLoanRateType:screen-value     = cSecondLoanRatetype.

 run setOwnerPriorInfoState  in this-procedure no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PropertyTypeValueChange C-Win 
PROCEDURE PropertyTypeValueChange :
/*------------------------------------------------------------------------------
  Purpose: handle UI state and rate type list for selected property type     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame frBasic:
 end.
 
 define variable pclistOwner       as character no-undo.
 define variable pclistLoan        as character no-undo.
 define variable pclistScndLoan    as character no-undo.

 define variable trackRateType           as character no-undo.
 define variable trackLoanRateType       as character no-undo.
 define variable trackSecondLoanRateType as character no-undo.



  if endselected 
  then
   do:                                                        
      run reloadEndors  in this-procedure ({&Owners}) no-error .   
      run reloadEndors  in this-procedure ({&Lenders}) no-error .        
      if multiloanselected 
       then
      run reloadEndors  in this-procedure ({&SecondLoan}) no-error .                                               
   end.

 publish "GetALComboLists"  (cbPropType:screen-value,
                             output pclistOwner,  
                             output pclistLoan,  
                             output pclistScndLoan).

 trackRateType = cbRateType:screen-value.
 trackLoanRateType = cbLoanRateType:screen-value.
 trackSecondLoanRateType = cbSecondLoanRateType:screen-value in frame frScndLoan. 

 assign
   cbRateType          :list-item-pairs                     =  trim(pclistOwner , ",")
   cbLoanRateType      :list-item-pairs                     =  trim(pclistLoan , ",")
   cbSecondLoanRateType:list-item-pairs in frame frScndLoan =  trim(pclistScndLoan , ","). 

 if cbPropType:screen-value  eq "C" then
 do:
   btSecondLoan:sensitive            = false.
   tbSimultaneous:checked            = false.
 
   run RefreshEndors in this-procedure ({&Owners})  no-error.
   run RefreshEndors in this-procedure ({&SecondLoan})  no-error.
   run SetWidgeState(false).
   apply 'value-changed' to tbSimultaneous.
   if multiloanselected = true then
     apply 'choose'        to btSecondLoan.
 end.
 
 else if cbPropType:screen-value  eq "R" then 
 do:
   run SetWidgeState in this-procedure (true) no-error.
 end.

 else if cbPropType:screen-value  eq "M" then 
 do:
   run SetWidgeState in this-procedure(true) no-error.
 end.

 if can-do(cbRateType:list-item-pairs,trackRateType) then
   cbRateType          :screen-value  = trackRateType.
 else
   cbRateType          :screen-value  = {&None}.

 if can-do(cbLoanRateType:list-item-pairs,trackLoanRateType) then
   cbLoanRateType          :screen-value  = trackLoanRateType.
 else
   cbLoanRateType          :screen-value  = {&None}.

 if can-do(cbSecondLoanRateType:list-item-pairs,trackSecondLoanRateType) then
   cbSecondLoanRateType          :screen-value  = trackSecondLoanRateType.
 else
   cbSecondLoanRateType          :screen-value  = {&None}.
 run SetPosition  in this-procedure no-error.
 apply 'value-changed' to cbRateType.
 apply 'value-changed' to cbLoanRateType.
 apply 'leave'         to fiSecondLoanAmt.
 apply 'leave'         to fiCoverageAmt.
 apply 'value-changed' to fiSecondLoanAmt.
 apply 'value-changed' to fiCoverageAmt.
 apply 'value-changed' to fiLoanAmt.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshEndors C-Win 
PROCEDURE RefreshEndors :
/*------------------------------------------------------------------------------
  Purpose: empty endorsement list     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 define input parameter pCase as character no-undo.
 
 case pCase:
   when {&Owners} then
     for each tthOwr where tthOwr.hChecked = true:
       apply "value-changed":U to tthOwr.hToggle.
       tthOwr.hToggle:checked = false.
     end.
   when {&Lenders} then
     for each tthLdr where tthldr.hChecked = true:
       apply "value-changed":U to tthldr.hToggle.
       tthldr.hToggle:checked = false.
     end.
   when {&SecondLoan} then
     for each tthScndLoan where tthScndLoan.hChecked = true:
       apply "value-changed":U to tthScndLoan.hToggle.
       tthScndLoan.hToggle:checked = false.
     end.
   otherwise
     do:
       for each tthOwr where tthOwr.hChecked = true:
         apply "value-changed":U to tthOwr.hToggle.
         tthOwr.hToggle:checked = false.
       end.
       for each tthLdr where tthldr.hChecked = true:
         apply "value-changed":U to tthldr.hToggle.
         tthldr.hToggle:checked = false.
       end.
       for each tthScndLoan where tthScndLoan.hChecked = true:
         apply "value-changed":U to tthScndLoan.hToggle.
         tthScndLoan.hToggle:checked = false.
       end.
     end.
 end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reloadEndors C-Win 
PROCEDURE reloadEndors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pcCaseType as character no-undo.

define variable chOwnerEndorsementList       as character no-undo.
define variable chLoanEndorsementList        as character no-undo.
define variable chSecondLoanEndorsementList  as character no-undo.
define variable iCountLEndors                as integer   no-undo.


do with frame frBasicResults:
end.
assign
    chOwnerEndorsementList = fiOwnerEndorsList:screen-value
    chLoanEndorsementList  = fiLendersEndorsList:screen-value
  chSecondLoanEndorsementList = fiSecondLendersEndorsList:screen-value in frame frScndLoanResults.

do with frame frBasic:
end.

flag = true.

case pcCaseType:
  when {&Owners} then
  do:
    empty temp-table ownerEndorsement no-error.
    publish "GetALEndors"(input pcCaseType,input cbPropType:screen-value,input cbRateType:screen-value in frame frBasic,output table ownerEndorsement).
    run InitializeFrameOEndors in this-procedure no-error.
    do iCountLEndors = 1 to num-entries(chOwnerEndorsementList,","):    
      find first ownerEndorsement where ownerEndorsement.endorsementCode = entry(iCountLEndors,chOwnerEndorsementList,",") no-error.
      if available ownerEndorsement then
      do:
        find first tthowr where tthowr.hseq = ownerEndorsement.endorsementCode no-error.
        if available tthowr then
          assign fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:screen-value in frame frBasicResults + "," + ownerEndorsement.endorsementCode , ",")
                 tthowr.hToggle:checked = true
                 tthowr.hChecked        = true
                 tthowr.hEditor:fgcolor = 9.      
      end.
    end.
  end.
  when {&Lenders} then
  do:
    empty temp-table loanEndorsement no-error.
    publish "GetALEndors"(input pcCaseType,input cbPropType:screen-value,input cbLoanRateType:screen-value in frame frBasic,output table loanEndorsement).
    run InitializeFrameLEndors in this-procedure no-error.
    do iCountLEndors = 1 to num-entries(chLoanEndorsementList,","):    
      find first loanEndorsement where loanEndorsement.endorsementCode = entry(iCountLEndors,chLoanEndorsementList,",") no-error.
      if available loanEndorsement then
      do:
        find first tthldr where tthldr.hseq = loanEndorsement.endorsementCode no-error.
        if available tthldr then
          assign fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:screen-value in frame frBasicResults + "," + loanEndorsement.endorsementCode , ",")
                 tthldr.hToggle:checked = true
                 tthldr.hChecked        = true
                 tthldr.hEditor:fgcolor = 9.      
      end.
    end.
  end.
  when {&SecondLoan} then
  do:
    empty temp-table secondLoanEndorsement no-error.
    publish "GetALEndors"(input {&Lenders},input cbPropType:screen-value,input cbSecondLoanRateType:screen-value in frame frScndLoan,output table secondLoanEndorsement).
    run InitializeFrameSEndors in this-procedure no-error.
    do iCountLEndors = 1 to num-entries(chSecondLoanEndorsementList,","):    
      find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = entry(iCountLEndors,chSecondLoanEndorsementList,",") no-error.
      if available secondLoanEndorsement then
      do:
        find first tthScndLoan where tthScndLoan.hseq = secondLoanEndorsement.endorsementCode no-error.
        if available tthScndLoan then
          assign fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = trim(fiSecondLendersEndorsList:screen-value in frame frScndLoanResults + "," + secondLoanEndorsement.endorsementCode , ",")
                 tthScndLoan.hToggle:checked = true
                 tthScndLoan.hChecked        = true
                 tthScndLoan.hEditor:fgcolor = 9.      
      end.
    end.
  end.
  otherwise
  do:
    empty temp-table loanEndorsement no-error.
    empty temp-table ownerEndorsement no-error.
    empty temp-table secondLoanEndorsement no-error.
    publish "GetALEndors"(input {&Lenders},input cbPropType:screen-value,input cbRateType:screen-value in frame frBasic,output table loanEndorsement).
    publish "GetALEndors"(input {&Owners},input cbPropType:screen-value,input cbLoanRateType:screen-value in frame frBasic,output table ownerEndorsement).
    publish "GetALEndors"(input {&Lenders},input cbPropType:screen-value,input cbSecondLoanRateType:screen-value in frame frScndLoan,output table SecondLoanEndorsement).
    run InitializeFrameLEndors in this-procedure no-error.
    run InitializeFrameOEndors in this-procedure no-error.
    run InitializeFrameSEndors in this-procedure no-error.

    do iCountLEndors = 1 to num-entries(chOwnerEndorsementList,","):    
      find first ownerEndorsement where ownerEndorsement.endorsementCode = entry(iCountLEndors,chOwnerEndorsementList,",") no-error.
      if available ownerEndorsement then
      do:
        find first tthowr where tthowr.hseq = ownerEndorsement.endorsementCode no-error.
        if available tthowr then
          assign fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:screen-value in frame frBasicResults + "," + ownerEndorsement.endorsementCode , ",")
                 tthowr.hToggle:checked = true
                 tthowr.hChecked        = true
                 tthowr.hEditor:fgcolor = 9.      
      end.
    end.

    do iCountLEndors = 1 to num-entries(chLoanEndorsementList,","):    
      find first loanEndorsement where loanEndorsement.endorsementCode = entry(iCountLEndors,chLoanEndorsementList,",") no-error.
      if available loanEndorsement then
      do:
        find first tthldr where tthldr.hseq = loanEndorsement.endorsementCode no-error.
        if available tthldr then
          assign fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:screen-value in frame frBasicResults + "," + loanEndorsement.endorsementCode , ",")
                 tthldr.hToggle:checked = true
                 tthldr.hChecked        = true
                 tthldr.hEditor:fgcolor = 9.      
      end.
    end.

    do iCountLEndors = 1 to num-entries(chSecondLoanEndorsementList,","):    
      find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = entry(iCountLEndors,chSecondLoanEndorsementList,",") no-error.
      if available secondLoanEndorsement then
      do:
        find first tthScndLoan where tthScndLoan.hseq = secondLoanEndorsement.endorsementCode no-error.
        if available tthScndLoan then
          assign fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = trim(fiSecondLendersEndorsList:screen-value in frame frScndLoanResults + "," + secondLoanEndorsement.endorsementCode , ",")
                 tthScndLoan.hToggle:checked = true
                 tthScndLoan.hChecked        = true
                 tthScndLoan.hEditor:fgcolor = 9.      
      end.
    end.

  end.

end case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setLoanPriorInfoState C-Win 
PROCEDURE setLoanPriorInfoState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable loShowPriorPolicyAmount as logical no-undo.

 publish "GetALPriorPolicyAmountConfig" (input cbPropType:screen-value in frame frBasic,
                                         input {&Lenders},
                                         input cbLoanRateType:screen-value,
                                         output loShowPriorPolicyAmount).  
 
 if not loShowPriorPolicyAmount then fiLoanPriorAmt:screen-value = "0".
 fiLoanPriorAmt:sensitive = loShowPriorPolicyAmount.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setOwnerPriorInfoState C-Win 
PROCEDURE setOwnerPriorInfoState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable loShowPriorPolicyAmount as logical no-undo.

  publish "GetALPriorPolicyAmountConfig" (input cbPropType:screen-value in frame frBasic,
                                         input {&Owners},
                                         input cbRateType:screen-value,
                                         output loShowPriorPolicyAmount).

  if (not loShowPriorPolicyAmount) or (cbRateType:screen-value = {&None}) 
   then 
    do:    
      assign 
        fiPriorAmt:screen-value = "0"
        fiPriorAmt:sensitive    = false.
    end.
  else
    fiPriorAmt:sensitive = loShowPriorPolicyAmount. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetPosition C-Win 
PROCEDURE SetPosition :
/*------------------------------------------------------------------------------
  Purpose: resize ui based on endorsements are shown or hidden or second
           loan is shown or hidden  
  Parameters:  <none>
  Notes:       
  ------------------------------------------------------------------------------*/

  if  endselected = false  and multiloanselected = false then
  do:
    frame frScndLoan       :visible = false.
    frame frScndLoanEndors :visible = false.
    frame frScndLoanResults:visible = false.
    frame frBasicEndors    :visible = false.
    c-win:height = cutheight.
    c-win:width  = cutwidth.
    frame frBasicResults:row = endorsPos.
  end.

  else if endselected = true and multiloanselected = false  then
  do: 
    frame frScndLoanResults:visible = false.
    frame frScndLoan       :visible = false.
    frame frScndLoanEndors :visible = false.
    frame frBasicEndors    :visible = true.
    c-win:height = fullheight.
    c-win:width  = cutwidth.
    frame frBasicEndors:row  = endorsPos.
    frame frBasicResults:row = resultsPos.

    run ShowScrollBars(frame frBasic:handle, no,no).
    run ShowScrollBars(frame frBasicResults:handle, no, no).
    run ShowScrollBars(frame frBasicEndors:handle, no, no).
  end.

  else if multiloanselected = true and endselected = true  then
  do:
    frame frBasicResults  :visible  = true.
    frame frScndLoan      :visible  = true.
    frame frBasicEndors   :visible  = true.
    frame frScndLoanEndors:visible  = true.
    frame frScndLoanResults:visible = true.
    c-win:height = fullheight.
    c-win:width  = fullwidth.
    frame frScndLoanResults:row = resultsPos. 
    frame frBasicResults   :row = resultsPos.
    frame frBasicEndors    :row = endorsPos . 
    frame frScndLoanEndors :row = endorsPos. 


    run ShowScrollBars(frame frBasic:handle, no,no).
    run ShowScrollBars(frame frBasicResults:handle, no, no).
    run ShowScrollBars(frame frBasicEndors:handle, no, no).
    run ShowScrollBars(frame frScndLoan:handle, no, no).
    run ShowScrollBars(frame frScndLoanResults:handle, no, no).
    run ShowScrollBars(frame frScndLoanEndors:handle, no, no).
    run ShowScrollBars(frame {&frame-name}:handle, no,no).
 end.

 else if multiloanselected = true and endselected = false  then
 do:
   frame frScndLoan       :visible = true.
   frame frScndLoanResults:visible = true.
   frame frScndLoanEndors :visible = false.
   frame frBasicEndors    :visible = false.
   c-win:height = cutheight.
   c-win:width  = fullwidth.
   frame frBasicResults   :row = endorsPos.
   frame frScndLoanResults:row = endorsPos.

   run ShowScrollBars(frame frBasic:handle, no,no).
   run ShowScrollBars(frame frBasicResults:handle, no, no).
   run ShowScrollBars(frame frBasicEndors:handle, no, no).
   run ShowScrollBars(frame frScndLoan:handle, no, no).
   run ShowScrollBars(frame frScndLoanResults:handle, no, no).
   run ShowScrollBars(frame frScndLoanEndors:handle, no, no).
   run ShowScrollBars(frame {&frame-name}:handle, no,no).
 end.
/*--------------------------for endors trigger--------------------------------*/
  if endselected = true then
  do:
    if flag = true then
        run reloadEndors in this-procedure ("") no-error.
    flag = false.
  end.
/*------------------------------------------------------------------*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetWidgeState C-Win 
PROCEDURE SetWidgeState :
/*------------------------------------------------------------------------------
  Purpose: set widgets conditionally based on property type    
  pcInputValues:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pState as logical.

do with frame frBasic:
end.

if not pState then
  assign
    fiCoverageAmt:screen-value   in frame frBasic    = "0"
    fiSecondLoanAmt:screen-value in frame frScndLoan = "0".
assign
  fiCoverageAmt       :sensitive   = pState
  fiSecondLoanAmt     :sensitive   = pState
  cbRateType          :sensitive   = pState
  cbSecondLoanRateType:sensitive   = pState
  .

if tbSimultaneous:checked in frame frBasic = true  then
  btSecondLoan:sensitive in frame frBasic = pState.

for each tthOwr:
   if valid-handle(tthOwr.hToggle) 
    then
     tthOwr.hToggle:sensitive = pState.
   if valid-handle(tthOwr.hEditor) 
    then
     tthOwr.hEditor:sensitive = pState.
end.
for each tthScndLoan:
   if valid-handle(tthScndLoan.hToggle) 
    then
     tthScndLoan.hToggle:sensitive = pState.
   if valid-handle(tthScndLoan.hEditor) 
    then
     tthScndLoan.hEditor:sensitive = pState.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION RemoveItemFromList C-Win 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character):
 
define variable lipos as integer no-undo.

lipos = lookup(pcremoveitem,pclist,pcdelimiter).

if lipos > 0 then
do:
  assign entry(lipos,pclist,pcdelimiter) = "".
  if lipos = 1 then
    pclist = substring(pclist,2).
  else if lipos = num-entries(pclist,pcdelimiter) then
    pclist = substring(pclist,1,length(pclist) - 1).
  else
    pclist = replace(pclist,pcdelimiter + pcdelimiter,pcdelimiter).
end.

return pclist.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

