&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*
 dialogcodercm.w
 UI for a system ROLE
 D.Sinclair
 8.10.2012
 */
 
 
{lib/std-def.i}
define input parameter pNew                as logical   no-undo.                                          
define input        parameter pTypelist    as character no-undo.
define input-output parameter pCodeType    as character no-undo.
define input-output parameter pType        as character no-undo.
define input-output parameter pCode        as character no-undo.
define input-output parameter pDescription as character no-undo.
define input-output parameter pComments    as character no-undo.
define output       parameter pSave        as logical   no-undo initial false.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbCodeType tCode tDescription tComments ~
Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cbCodeType cbType tCode tDescription ~
tComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbCodeType AS CHARACTER FORMAT "X(256)" 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 60.2 BY 2.38 NO-UNDO.

DEFINE VARIABLE cbType AS CHARACTER FORMAT "X(256)":U INITIAL "Region" 
     LABEL "Type" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tCode AS CHARACTER FORMAT "X(256)":U 
     LABEL "Code" 
     VIEW-AS FILL-IN 
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE tDescription AS CHARACTER FORMAT "X(256)":U 
     LABEL "Description" 
     VIEW-AS FILL-IN 
     SIZE 60.2 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     cbCodeType AT ROW 1.29 COL 12.8 COLON-ALIGNED WIDGET-ID 48
     cbType AT ROW 2.48 COL 12.8 COLON-ALIGNED WIDGET-ID 158
     tCode AT ROW 3.67 COL 12.8 COLON-ALIGNED WIDGET-ID 156
     tDescription AT ROW 4.86 COL 12.8 COLON-ALIGNED WIDGET-ID 4
     tComments AT ROW 6.05 COL 14.8 NO-LABEL WIDGET-ID 44
     Btn_OK AT ROW 8.62 COL 23.8 WIDGET-ID 152
     Btn_Cancel AT ROW 8.62 COL 40.8 WIDGET-ID 150
     "Comments:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 6.05 COL 3.4 WIDGET-ID 46
     SPACE(61.79) SKIP(3.32)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "System Code" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME                                                           */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN cbType IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX fMain
/* Query rebuild information for DIALOG-BOX fMain
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* System Code */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCode
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCode fMain
ON VALUE-CHANGED OF tCode IN FRAME fMain /* Code */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tComments fMain
ON VALUE-CHANGED OF tComments IN FRAME fMain
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDescription fMain
ON VALUE-CHANGED OF tDescription IN FRAME fMain /* Description */
DO:
  run enableDisableSave in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */
if pNew 
 then 
  assign
    frame fMain:title = "New Region".
 else 
  assign
    tCode                = pCode
    tDescription         = pDescription
    tComments            = pComments
    frame fMain:title    = "Modify Region"
    cbType               = pType
    .

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

  publish "GetSearchStates" (output std-ch).

  if std-ch = ?
   then std-ch = "".
  if std-ch > ""
   then std-ch = "," + std-ch.

  cbCodeType:list-item-pairs  = trim(std-ch,",").

  if pNew 
   then
    cbCodeType:screen-value = cbCodeType:entry(1).
  else
    cbCodeType:screen-value = pCodeType.

RUN enable_UI.

 
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
repeat ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  run enableDisableSave in this-procedure.

  assign
    cbCodeType:sensitive = pNew
    tCode:sensitive      = pNew
    .

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
 
  assign
    pCodeType    = cbCodeType:screen-value
    pType        = cbType:screen-value 
    pCode        = tCode:screen-value  
    pDescription = tDescription:screen-value 
    pComments    = tComments:screen-value    
    pSave        = true
    .
  leave MAIN-BLOCK.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableSave fMain 
PROCEDURE enableDisableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if pNew
   then
    Btn_OK:sensitive = not (tCode:input-value  = "" 
                            or
                            pCode = tCode:input-value
                            )no-error.
   else                
    Btn_OK:sensitive = not ((tCode:input-value  = "")
                               or
                            (pCode        = tCode:input-value        and
                             pDescription = tDescription:input-value and
                             pComments    = tComments:input-value
                             )
                            ) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbCodeType cbType tCode tDescription tComments 
      WITH FRAME fMain.
  ENABLE cbCodeType tCode tDescription tComments Btn_OK Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

