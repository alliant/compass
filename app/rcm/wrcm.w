&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: rcm/wrcm.w

  Description: Main window of RCM application.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 
  @Modified    :
  Date        Name    Comments
  07/01/2019  Anjly   Modified for Region Maintainance.
  11/06/2019  Sachin  Modified to add ratescenarios and ratescenario cards.
  12/18/2019  Anubha  Added multiple browse for showing Rate Scenarios along
                      with cards used in that scenario.
  11/08/2021  Vignesh Fixed sorting issue. 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratescenario.i}
{tt\ratescenario.i &tablealias = tratescenario}
{tt\ratescenariocard.i}
{tt\ratescenariocard.i &tablealias = tratescenariocard}
{tt\ratestatescenario.i}
{tt\rateTable.i}
{tt\rateattr.i}
{tt\raterule.i}
{tt\ratelog.i}
{lib\std-def.i}
{lib\rcm-std-def.i}
{lib\winlaunch.i}
{lib\brw-multi-def.i}
{tt\rateui.i}
define variable tOK             as logical.
define variable activeCardSetID as integer.
define temp-table openVersions
  field cardSetId as integer
  field hInstance as handle.

define temp-table openDocs
  field cardSetID as integer
  field hInstance as handle.
    
define temp-table openCards no-undo
 field cardID    as integer
 field hInstance as handle.


define variable lStatCond    as logical   no-undo.
define variable pLastState   as character no-undo.
define variable tConfirmExit as logical   no-undo.
define variable hDocWindow   as handle.

define variable activeRateStateID as rowid   no-undo.
define variable incardset         as integer no-undo.
define variable activeCardID      as integer no-undo.
define variable activeRateScenarioID as rowid   no-undo.
define variable cCardsList        as character no-undo.

define variable lShowScenario     as logical   no-undo.
define variable lrefreshScenario  as logical   no-undo.

/* resize func*/
define variable drsH           as decimal  no-undo. 
define variable dsW            as decimal  no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwRateState

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ratestate ratescenario ratescenariocard

/* Definitions for BROWSE brwRateState                                  */
&Scoped-define FIELDS-IN-QUERY-brwRateState ratestate.active ratestate.version date(ratestate.effectiveDate) date(ratestate.lastModifiedDate) if ratestate.description = ? or ratestate.description = "?" then "" else ratestate.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwRateState   
&Scoped-define SELF-NAME brwRateState
&Scoped-define QUERY-STRING-brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate
&Scoped-define OPEN-QUERY-brwRateState open query {&SELF-NAME} for each ratestate by ratestate.version by ratestate.lastModifiedDate.
&Scoped-define TABLES-IN-QUERY-brwRateState ratestate
&Scoped-define FIRST-TABLE-IN-QUERY-brwRateState ratestate


/* Definitions for BROWSE brwScenario                                   */
&Scoped-define FIELDS-IN-QUERY-brwScenario ratescenario.region ratescenario.property ratescenario.scenario   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwScenario   
&Scoped-define SELF-NAME brwScenario
&Scoped-define QUERY-STRING-brwScenario for each ratescenario where ratescenario.cardSetID = activeCardSetID
&Scoped-define OPEN-QUERY-brwScenario OPEN QUERY {&SELF-NAME} for each ratescenario where ratescenario.cardSetID = activeCardSetID.
&Scoped-define TABLES-IN-QUERY-brwScenario ratescenario
&Scoped-define FIRST-TABLE-IN-QUERY-brwScenario ratescenario


/* Definitions for BROWSE brwScenarioCard                               */
&Scoped-define FIELDS-IN-QUERY-brwScenarioCard rateScenariocard.cardName rateScenariocard.attribute   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwScenarioCard   
&Scoped-define SELF-NAME brwScenarioCard
&Scoped-define QUERY-STRING-brwScenarioCard for each ratescenariocard where ratescenariocard.scenarioID = ratescenario.scenarioID
&Scoped-define OPEN-QUERY-brwScenarioCard open query {&SELF-NAME} for each ratescenariocard where ratescenariocard.scenarioID = ratescenario.scenarioID.
&Scoped-define TABLES-IN-QUERY-brwScenarioCard ratescenariocard
&Scoped-define FIRST-TABLE-IN-QUERY-brwScenarioCard ratescenariocard


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwRateState}~
    ~{&OPEN-QUERY-brwScenario}~
    ~{&OPEN-QUERY-brwScenarioCard}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-54 btMaintenance RECT-68 RECT-69 ~
RECT-70 btEdit imAlert edCalc btRefresh cbState brwRateState btExport ~
brwScenario brwScenarioCard bDocuments btActivate btCopy btDelate btNew ~
btRateCalculator 
&Scoped-Define DISPLAYED-OBJECTS edCalc cbState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD RemoveItemFromList C-Win 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setScreenHeader C-Win 
FUNCTION setScreenHeader RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Moudule 
       MENU-ITEM m_Configure    LABEL "Configure"     
       MENU-ITEM m_About        LABEL "About"         
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_References 
       MENU-ITEM m_States       LABEL "States"        
       MENU-ITEM m_Codes        LABEL "Region"        .

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_Export_all_cards LABEL "Export all cards"
              DISABLED
       MENU-ITEM m_Compare_Versions LABEL "Compare Versions"
              DISABLED.

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Moudule      LABEL "Module"        
       SUB-MENU  m_References   LABEL "References"    
       SUB-MENU  m_Reports      LABEL "Reports"       .

DEFINE MENU POPUP-MENU-brwScenario 
       MENU-ITEM m_Delete_Scenario LABEL "Delete Selected Scenario".

DEFINE MENU POPUP-MENU-brwScenCards 
       MENU-ITEM m_View_Detail  LABEL "View Detail"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bDocuments  NO-FOCUS
     LABEL "Documents" 
     SIZE 7.2 BY 1.7 TOOLTIP "Documents".

DEFINE BUTTON btActivate  NO-FOCUS
     LABEL "Activate" 
     SIZE 7.2 BY 1.7 TOOLTIP "Activate selected version".

DEFINE BUTTON btCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.7 TOOLTIP "Copy selected version".

DEFINE BUTTON btDelate  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.7 TOOLTIP "Remove selected version".

DEFINE BUTTON btEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.7 TOOLTIP "Edit selected version".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.7 TOOLTIP "Export".

DEFINE BUTTON btMaintenance  NO-FOCUS
     LABEL "Maintenance" 
     SIZE 7.2 BY 1.7 TOOLTIP "Edit card set up".

DEFINE BUTTON btNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.7 TOOLTIP "Add a new version".

DEFINE BUTTON btRateCalculator  NO-FOCUS
     LABEL "RateCalculator" 
     SIZE 7.2 BY 1.7 TOOLTIP "View rate calculator".

DEFINE BUTTON btRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.7 TOOLTIP "Refresh".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 26.8 BY 1 NO-UNDO.

DEFINE VARIABLE edCalc AS CHARACTER 
     VIEW-AS EDITOR NO-BOX
     SIZE 40.4 BY 1.48 NO-UNDO.

DEFINE IMAGE imAlert TRANSPARENT
     SIZE 4 BY .96.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37 BY 1.87.

DEFINE RECTANGLE RECT-54
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29.6 BY 1.87.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23 BY 1.87.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 8.2 BY 1.87.

DEFINE RECTANGLE RECT-70
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16 BY 1.87.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwRateState FOR 
      ratestate SCROLLING.

DEFINE QUERY brwScenario FOR 
      ratescenario SCROLLING.

DEFINE QUERY brwScenarioCard FOR 
      ratescenariocard SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwRateState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwRateState C-Win _FREEFORM
  QUERY brwRateState DISPLAY
      ratestate.active              column-label "Active" view-as toggle-box 
ratestate.version                          label "Version"                       width 7
date(ratestate.effectiveDate)              label "Manual Effective"                     width 17
date(ratestate.lastModifiedDate)           label "Last Modified"                 width 17
if ratestate.description  = ? or  ratestate.description  = "?" then "" else ratestate.description label "Description" format "X(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 159.6 BY 7.74
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Versions" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwScenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwScenario C-Win _FREEFORM
  QUERY brwScenario DISPLAY
      ratescenario.region   label "Region"   format "X(50)"  width 10
ratescenario.property label "Property" format "X(50)"  width 15   
ratescenario.scenario label "Scenario" format "X(100)" width 45
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 108.6 BY 7.74
         TITLE "Scenarios" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwScenarioCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwScenarioCard C-Win _FREEFORM
  QUERY brwScenarioCard DISPLAY
      rateScenariocard.cardName         label "Card Name"       width 25    format "x(50)"
rateScenariocard.attribute        label "Attribute"      width 10     format "x(30)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 49.6 BY 7.74
         BGCOLOR 15 
         TITLE BGCOLOR 15 "Primary cards used by selected scenario" ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     btMaintenance AT ROW 1.3 COL 62.2 WIDGET-ID 8 NO-TAB-STOP 
     btEdit AT ROW 1.3 COL 98.8 WIDGET-ID 258 NO-TAB-STOP 
     edCalc AT ROW 1.52 COL 121.6 NO-LABEL WIDGET-ID 284 NO-TAB-STOP 
     btRefresh AT ROW 1.3 COL 47.2 WIDGET-ID 278 NO-TAB-STOP 
     cbState AT ROW 1.61 COL 8.4 COLON-ALIGNED WIDGET-ID 6
     brwRateState AT ROW 3.35 COL 2.4 WIDGET-ID 400
     btExport AT ROW 1.3 COL 54.4 WIDGET-ID 280 NO-TAB-STOP 
     brwScenario AT ROW 11.39 COL 2.4 WIDGET-ID 500
     brwScenarioCard AT ROW 11.39 COL 112.4 WIDGET-ID 600
     bDocuments AT ROW 1.3 COL 76.6 WIDGET-ID 188 NO-TAB-STOP 
     btActivate AT ROW 1.3 COL 69.4 WIDGET-ID 262 NO-TAB-STOP 
     btCopy AT ROW 1.3 COL 106 WIDGET-ID 260 NO-TAB-STOP 
     btDelate AT ROW 1.3 COL 91.6 WIDGET-ID 172 NO-TAB-STOP 
     btNew AT ROW 1.3 COL 84.4 WIDGET-ID 170 NO-TAB-STOP 
     btRateCalculator AT ROW 1.3 COL 39.4 WIDGET-ID 272 NO-TAB-STOP 
     RECT-1 AT ROW 1.26 COL 2.4 WIDGET-ID 264
     RECT-54 AT ROW 1.26 COL 84.2 WIDGET-ID 266
     RECT-68 AT ROW 1.26 COL 61.4 WIDGET-ID 270
     RECT-69 AT ROW 1.26 COL 39 WIDGET-ID 274
     RECT-70 AT ROW 1.26 COL 46.2 WIDGET-ID 276
     imAlert AT ROW 1.7 COL 117.4 WIDGET-ID 282
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 162.4 BY 18.48 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 18.48
         WIDTH              = 162.4
         MAX-HEIGHT         = 33.61
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.61
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwRateState cbState DEFAULT-FRAME */
/* BROWSE-TAB brwScenario btExport DEFAULT-FRAME */
/* BROWSE-TAB brwScenarioCard brwScenario DEFAULT-FRAME */
ASSIGN 
       brwRateState:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwRateState:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       brwScenario:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwScenario:HANDLE
       brwScenario:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       brwScenarioCard:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwScenCards:HANDLE
       brwScenarioCard:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

ASSIGN 
       edCalc:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE
       edCalc:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

ASSIGN 
       imAlert:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwRateState
/* Query rebuild information for BROWSE brwRateState
     _START_FREEFORM
open query {&SELF-NAME} for each ratestate by ratestate.version by ratestate.lastModifiedDate.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwRateState */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwScenario
/* Query rebuild information for BROWSE brwScenario
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} for each ratescenario where ratescenario.cardSetID = activeCardSetID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwScenario */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwScenarioCard
/* Query rebuild information for BROWSE brwScenarioCard
     _START_FREEFORM
open query {&SELF-NAME} for each ratescenariocard where ratescenariocard.scenarioID = ratescenario.scenarioID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwScenarioCard */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
 /* This event will close the window and terminate the procedure.  */
  do with frame {&frame-name}:
  end.
  publish "SetCurrentValue" ("RateStateID", "").
  publish "SetCurrentValue" ("RateVersionNo", "").

  publish "SetLastState" (input cbState:screen-value).

  if tConfirmExit then
  do:
    message "Are you sure you want to exit?"
      view-as alert-box question buttons ok-cancel update lChoice as logical.
  
    if not lChoice then
      return no-apply.
  end.
  
  if activeCardID <> 0 and activeCardID <> ?
   then
    publish "emptyDetailWindow"(input activeCardID).
  
  publish "ExitApplication".
  return no-apply. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win
do:
    run windowresized in this-procedure.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDocuments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDocuments C-Win
ON CHOOSE OF bDocuments IN FRAME DEFAULT-FRAME /* Documents */
DO:
  define variable dochandle as handle.
  define variable openDoc   as logical.
  do with frame default-frame:
  end.
  find current ratestate no-error.
  if available ratestate then
    publish "checkOpenDocuments" (input ratestate.version,
                                  output openDoc,
                                  output dochandle ).
  if not openDoc
   then
    do:
      run documents.w persistent set hDocWindow
        ("RCM",
         "/rcm/" + string(ratestate.stateID) + "/" + string(ratestate.version) + "/",
         "Rate Calculator Maintenance " + string(date(ratestate.effectivedate)),
         "new,delete,open,modify,send,request",
         "ratestate",
         string(activeCardSetID),
         "") no-error.
    end.
  else
    do:
      run showWindow in openDocs.hInstance no-error.
    end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRateState
&Scoped-define SELF-NAME brwRateState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateState C-Win
ON DEFAULT-ACTION OF brwRateState IN FRAME DEFAULT-FRAME /* Versions */
DO:
  run doSelect.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateState C-Win
ON ROW-DISPLAY OF brwRateState IN FRAME DEFAULT-FRAME /* Versions */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateState C-Win
ON VALUE-CHANGED OF brwRateState IN FRAME DEFAULT-FRAME /* Versions */
DO:
   find current ratestate no-error.
   
   if available ratestate then
   do:
       
      
      activeCardSetID = ratestate.cardsetid.
      btRateCalculator:load-image("images/calc.bmp").
      
      if ratestate.active = true 
       then    
        assign 
            btDelate  :sensitive = false 
            btActivate:sensitive = false.       
       else 
        assign
            btDelate  :sensitive = true 
            btActivate:sensitive = true. 
      
      /* Set Version corresponding to any selected state from combo.*/
      if cbstate:input-value <> ? and cbstate:input-value <> ""
       then
        publish "SetCurrentValue" (cbstate:input-value + "Version", string(ratestate.version)). 
    
    
      /* Refreshing the scenario related browse */
      if lShowScenario
       then   
        do:         
           run getScenarios(input activeCardSetID).
           apply "value-changed" to brwScenario.  
           apply "left-mouse-up" to brwScenario.  /* update tooltip in Scenario browse */ 
        end.  /* if lShowScenario */
     
     
  end.  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwScenario
&Scoped-define SELF-NAME brwScenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenario C-Win
ON LEFT-MOUSE-UP OF brwScenario IN FRAME DEFAULT-FRAME /* Scenarios */
DO:
  find current ratescenario no-error.

  brwScenario:tooltip = if available ratescenario then ratescenario.description else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenario C-Win
ON ROW-DISPLAY OF brwScenario IN FRAME DEFAULT-FRAME /* Scenarios */
DO:
    {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenario C-Win
ON START-SEARCH OF brwScenario IN FRAME DEFAULT-FRAME /* Scenarios */
DO:
  {lib/brw-startSearch-multi.i &browseName="brwScenario"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenario C-Win
ON VALUE-CHANGED OF brwScenario IN FRAME DEFAULT-FRAME /* Scenarios */
DO:                                      
  /***********Render the scenarios on the browse*****************/
  find current ratescenario no-error.
  
  close query brwScenarioCard. 
  open  query brwScenarioCard for each ratescenariocard where ratescenariocard.scenarioID = ratescenario.scenarioID.
    
  if can-find(first ratescenariocard)
   then   
    do:
       brwScenariocard:sensitive = true.
       brwScenarioCard:set-repositioned-row(1,"ALWAYS").
    end.
   else    
    brwScenariocard:sensitive = false.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwScenarioCard
&Scoped-define SELF-NAME brwScenarioCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenarioCard C-Win
ON DEFAULT-ACTION OF brwScenarioCard IN FRAME DEFAULT-FRAME /* Primary cards used by selected scenario */
DO:
   run viewScenarioCard in this-procedure no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenarioCard C-Win
ON ROW-DISPLAY OF brwScenarioCard IN FRAME DEFAULT-FRAME /* Primary cards used by selected scenario */
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenarioCard C-Win
ON START-SEARCH OF brwScenarioCard IN FRAME DEFAULT-FRAME /* Primary cards used by selected scenario */
DO:
  define variable tWhereClause as character no-undo.
  
  tWhereClause = "where ratescenariocard.scenarioID = " + string(ratescenario.scenarioID).
  
  {lib/brw-startSearch-multi.i &browseName="brwScenarioCard" &whereClause="tWhereClause"}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwScenarioCard C-Win
ON VALUE-CHANGED OF brwScenarioCard IN FRAME DEFAULT-FRAME /* Primary cards used by selected scenario */
DO:
   find current rateScenarioCard no-error.
   
   if available rateScenarioCard
    then
     activeCardID = rateScenarioCard.cardID.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btActivate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btActivate C-Win
ON CHOOSE OF btActivate IN FRAME DEFAULT-FRAME /* Activate */
DO:
  run activateVersion.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCopy C-Win
ON CHOOSE OF btCopy IN FRAME DEFAULT-FRAME /* Copy */
DO:
  run copyRateState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btDelate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btDelate C-Win
ON CHOOSE OF btDelate IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deleteRateState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btEdit C-Win
ON CHOOSE OF btEdit IN FRAME DEFAULT-FRAME /* Edit */
DO:
  run editRateState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btMaintenance
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btMaintenance C-Win
ON CHOOSE OF btMaintenance IN FRAME DEFAULT-FRAME /* Maintenance */
DO:
  run doSelect.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNew C-Win
ON CHOOSE OF btNew IN FRAME DEFAULT-FRAME /* New */
DO: 
  run addRateState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRateCalculator
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRateCalculator C-Win
ON CHOOSE OF btRateCalculator IN FRAME DEFAULT-FRAME /* RateCalculator */
DO:
  run ViewCalculator.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefresh C-Win
ON CHOOSE OF btRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  if can-find(first ratestate) 
   then
    do:
      empty temp-table ratestate.
      empty temp-table ratescenario.
      empty temp-table ratescenariocard.
    end.

  publish "GetRateStates" (input cbState:screen-value,
                           yes,/*refresh to server call */
                           output table ratestate).                          
                           
  open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
  
/* After data refresh, the highlighted row will be the very first record of browse.*/  
 find first ratestate no-error.
 if available ratestate then
   activeRateStateID  =  rowid(ratestate).
 
 reposition brwRateState to rowid activeRateStateID no-error.
 apply "value-changed" to brwRateState in frame DEFAULT-FRAME.
 setScreenHeader().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME DEFAULT-FRAME /* State */
DO:
  define variable cStatus as character no-undo.

  if can-find(first ratestate) then
  do:
    empty temp-table ratestate.
    empty temp-table ratescenario.
    empty temp-table ratescenariocard.
  end.
    
  if cbState:input-value <> ""    and   
     cbState:input-value <> "ALL" 
   then
    do:
       /* Assigning a logical flag to denote if the selected state has scenario based implementation or not. */
       lShowScenario = lookup(cbstate:input-value,{&StateWithScenarios}) > 0 no-error.
     
       run windowresized. 
       brwScenario:visible     = lShowScenario.
       brwScenarioCard:visible = lShowScenario.
  
       publish "GetRateStates" (input cbState:screen-value,
                                no,  /* fetch from cache data in datamodel if exist */
                                output table ratestate).
    end.
    else
    do:
       assign
           brwScenario:visible     = false
           brwScenarioCard:visible = false
           lShowScenario           = false
           .
       run windowresized. 
    end.


  setScreenHeader().

  run refreshstatusbar.
  open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
  apply 'value-changed' to brwRateState.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Codes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Codes C-Win
ON CHOOSE OF MENU-ITEM m_Codes /* Region */
DO:
  run wcodesrcm.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure */
DO:
  run ActionConfig in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Delete_Scenario
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Delete_Scenario C-Win
ON CHOOSE OF MENU-ITEM m_Delete_Scenario /* Delete Selected Scenario */
DO:
   run deleteScenario in this-procedure no-error.                                       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_States
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_States C-Win
ON CHOOSE OF MENU-ITEM m_States /* States */
DO:
  run wstate.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail C-Win
ON CHOOSE OF MENU-ITEM m_View_Detail /* View Detail */
DO:
   run viewScenarioCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRateState
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

subscribe to "OpenMaintainVersions"        anywhere.
subscribe to "checkOpenVersions"           anywhere.
subscribe to "OpenMaintainDocuments"       anywhere.
subscribe to "checkOpenDocuments"          anywhere.
subscribe to "RefreshState"                anywhere.
subscribe to "DeleteMaintainVersion"       anywhere.

subscribe to "refreshScenarios"            anywhere.

subscribe to "openMaintainCards"           anywhere.
subscribe to "checkOpenCards"              anywhere.
subscribe to "DeleteMaintainCards"         anywhere.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

{lib/win-main.i} 
{lib/brw-main-multi.i &browse-list="brwRateState,brwScenario,brwScenarioCard"}

bDocuments      :load-image("images/attach.bmp").
bDocuments      :load-image-insensitive("images/attach-i.bmp").
btNew           :load-image("images/new.bmp").
btNew           :load-image-insensitive("images/new-i.bmp").
btDelate        :load-image("images/delete.bmp").
btDelate        :load-image-insensitive("images/delete-i.bmp").
btEdit          :load-image("images/update.bmp").
btEdit          :load-image-insensitive("images/update-i.bmp").
btCopy          :load-image("images/split.bmp").
btCopy          :load-image-insensitive("images/split-i.bmp").
btMaintenance  :load-image("images/blank-modify.bmp").
btMaintenance  :load-image-insensitive("images/blank-modify-i.bmp").
btActivate      :load-image-insensitive("images/check-i.bmp").
btActivate      :load-image("images/check.bmp").
btRateCalculator:load-image-insensitive("images/calc-i.bmp").
btRateCalculator:load-image("images/calc.bmp").
btRefresh       :load-image-insensitive("images/refresh-i.bmp").
btRefresh       :load-image("images/refresh.bmp").
btExport        :load-image-insensitive("images/excel-i.bmp").
btExport        :load-image("images/excel.bmp").

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.

  publish "GetSearchStates" (output std-ch).
  if std-ch = ?
   then std-ch = "".
  if std-ch > ""
   then std-ch = "," + std-ch.
  cbState:list-item-pairs in frame {&frame-name} = "Select Any State,ALL" + std-ch.
  
  publish "GetStateCondition" (output lStatCond).
  if lStatCond = true 
   then 
    do:
      publish "GetLastState" (output pLastState).
      cbState:screen-value = pLastState.
    end.
  else cbState:screen-value = "ALL".
 
  /* Store dimensions of various components for resizing. */
  assign
      drsH  = brwratestate:height
      dsW   = brwScenario:width
      .
  apply 'value-changed' to cbState.

  publish "GetConfirmExit"(output tConfirmExit).

  setScreenHeader().

  run windowresized.

  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE actionConfig C-Win 
PROCEDURE actionConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter pError  as logical   no-undo initial true.
 define variable tSelectedStates as character no-undo.
 define variable cCurrentState   as character no-undo.
 
 cCurrentState = cbState:screen-value in frame {&frame-name}.
 run dialogconfig.w.
 
 if return-value = "no" then
     return.
 
 publish "GetSearchStates" (output std-ch).
 
 if std-ch = ?
  then std-ch = "".
 if std-ch > ""
  then std-ch = "," + std-ch.

 cbState:list-item-pairs in frame {&frame-name} = "Select Any State,ALL" + std-ch.
 
 if lookup(cCurrentState,cbState:list-item-pairs) > 0
  then 
   cbState:screen-value = cCurrentState.
 else 
 do:
   cbState:screen-value = "ALL".
   empty temp-table ratestate.
 end.
 
 open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
  
 publish "GetConfirmExit"(output tConfirmExit).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE activateVersion C-Win 
PROCEDURE activateVersion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame DEFAULT-FRAME:
 end.

 run windowresized.
 
 find current ratestate no-error.
 if available ratestate and 
     (ratestate.execBusinessLogic   = ?   or ratestate.execBusinessLogic   = "?"   or  ratestate.execBusinessLogic   = "" or
      ratestate.configUserInterface = ?   or ratestate.configUserInterface = "?"   or  ratestate.configUserInterface = ""
      ) 
  then
   do:
     define variable cMsgBody    as character   no-undo.
     define variable iNumEntries as integer     no-undo.
     define variable cFront      as character   no-undo.
     define variable cBack       as character   no-undo.
     define variable iCount      as integer     no-undo.

     cMsgBody = (if ratestate.configUserInterface eq "" then "UI Configuration File," else "")    + 
                  (if ratestate.execBusinessLogic eq ""  then " Business Layer Program," else "")
  .
      cMsgBody = trim(cMsgBody, ",").

      iNumEntries = num-entries(cMsgBody).
      if iNumEntries gt 1 then do:
        do iCount = 1 to (iNumEntries - 1):
          cFront = cFront + entry(iCount,cMsgBody) + ', ' no-error.
        end.
        cFront = trim(cFront).
        cFront = trim(cFront,",").
      end.
      cBack = entry(iNumEntries,cMsgBody) no-error.

      if iNumEntries gt 1 then
        message cFront 'and' cBack ' cannot be blank.'
            view-as alert-box info buttons ok.
      else if iNumEntries eq 1 then
        message cMsgBody ' cannot be blank.'
            view-as alert-box info buttons ok.
     return error.
   end.
   
 message "Highlighted version will be activated. Previously active version will be deactivated. Are you sure?"
   view-as alert-box question buttons ok-cancel set tOK.
 if not tOK 
   then return error.
 else
   find current ratestate no-error. 

 incardset = ratestate.cardsetid.

 publish "ActivateRateState"(incardset).
 if can-find(first ratestate) then
   empty temp-table ratestate.

 publish "refreshRateStateData" (input cbState:screen-value,
                                 output table ratestate).

 close query brwRateState.
 open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.

 apply 'value-changed' to brwRateState in frame default-frame.

 find first ratestate where ratestate.cardsetid = incardset no-error.
 if available ratestate then
   activeRateStateID  =  rowid(ratestate).

 reposition brwRateState to rowid activeRateStateID no-error.
 apply "value-changed" to brwRateState in FRAME DEFAULT-FRAME.
 setScreenHeader().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addRateState C-Win 
PROCEDURE addRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufratestate for ratestate.
  define variable lSuccess as logical no-undo.

  do with frame DEFAULT-FRAME:
  end.

  run windowresized.
  
  run dialognewstate.w  (cbState:screen-value in frame default-frame,
                         0,
                         0,
                         "Add New Version",
                         input table ratestate,
                         output lSuccess) no-error. 

  if lSuccess then do:
    empty temp-table ratestate no-error.
    publish "refreshRateStateData" (input cbState:screen-value,
                                    output table ratestate).

    close query brwRateState.
    open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
    run refreshstatusbar.

    for last bufratestate by bufratestate.version descending:
      activeRateStateID  =  rowid(bufratestate).
    end.

    open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
    brwRateState:set-repositioned-row(11,"ALWAYS").      
    reposition brwRateState to rowid activeRateStateID no-error.

  end.
  apply 'value-changed' to brwRateState IN FRAME default-frame.
  setScreenHeader().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenCards C-Win 
PROCEDURE checkOpenCards :
/*------------------------------------------------------------------------------
  Purpose:     Verify if the ratecard window is already opened or not 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input  parameter ipiCardID    as integer no-undo.
define output parameter oplCardOpen   as logical no-undo.
define output parameter ophaCardHandle as handle  no-undo.

find first openCards
 where openCards.cardID = ipiCardID no-error.

if available openCards and valid-handle(openCards.hInstance)
 then
  do:
     oplCardOpen    = yes.
     ophaCardHandle = openCards.hInstance.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenDocuments C-Win 
PROCEDURE checkOpenDocuments :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter pCardSetID as character no-undo.
 define output parameter docOpen    as logical   no-undo.
 define output parameter docHandle  as handle    no-undo.

 find first opendocs
  where opendocs.cardSetID = integer(pCardSetID) no-error.
 if available opendocs and valid-handle(opendocs.hInstance)
  then
   do:
     docOpen   = yes.
     docHandle = opendocs.hInstance.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkOpenVersions C-Win 
PROCEDURE checkOpenVersions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter  pCardSetID    as character no-undo.
 define output parameter  versionOpen   as logical   no-undo.
 define output parameter  versionHandle as handle    no-undo.

 find first openVersions
  where openVersions.cardSetID = integer(pCardSetID) no-error.
 if available openVersions and valid-handle(openVersions.hInstance)
  then
   do:
     versionOpen   = yes.
     versionHandle = openVersions.hInstance.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyRateState C-Win 
PROCEDURE copyRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer   bufRateState for ratestate.
  define variable copiedRateStateID as rowid   no-undo.
  define variable lSuccess          as logical no-undo.

  run windowresized.
  
  find current ratestate no-error.
  if avail ratestate then do:
    run dialogcopystate.w  (cbState:screen-value in frame DEFAULT-FRAME,
                            ratestate.version,
                            ratestate.cardsetid,
                            input table ratestate,
                            output lSuccess) no-error.
    if lSuccess then do:
      if can-find(first ratestate) then
      empty temp-table ratestate no-error.
      publish "refreshRateStateData" (input cbState:screen-value,
                                      output table ratestate).
      open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
      run refreshstatusbar.
      for last bufRateState by bufratestate.version descending:
        copiedRateStateID  =  rowid(bufratestate) no-error.
      end.
      open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
      brwRateState:set-repositioned-row(11,"ALWAYS").      
      reposition brwRateState to rowid copiedRateStateID no-error.
    end.
  end.

  apply 'value-changed' to brwRateState in frame DEFAULT-FRAME.  
  setScreenHeader().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteMaintainCards C-Win 
PROCEDURE deleteMaintainCards :
/*------------------------------------------------------------------------------
  Purpose:     Closes the card window if already opened
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiCardID as integer no-undo.
    
  find openCards 
   where openCards.cardID = ipiCardID no-error.
  
  if available openCards 
   then
    delete openCards.

  publish "CloseDetailWindow" (input ipiCardID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteMaintainVersion C-Win 
PROCEDURE deleteMaintainVersion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pCardSetID as integer no-undo.

 find openVersions 
  where openVersions.cardSetID = pCardSetID no-error.
 if available openVersions 
  then
   delete openVersions.

 publish "CloseRateCardWindow" (input pCardSetID ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRateState C-Win 
PROCEDURE deleteRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable versionhandle as handle  no-undo.
 define variable openVersion   as logical no-undo.

 do with frame {&frame-name}:
 end.

 message "Highlighted version will be deleted. Are you sure?"
   view-as alert-box question buttons ok-cancel set tOK.
 if not tOK 
   then return error.

 find current ratestate no-error. 
 if available ratestate then
   publish "DeleteRateState"(ratestate.cardSetID).

 publish "checkOpenVersions" (input activeCardSetID,
                              output openVersion,
                              output versionhandle ).
 if openVersion then
   publish "DeleteMaintainVersion"(input activeCardSetID). 
 if can-find(first ratestate) then
   empty temp-table ratestate no-error.
 publish "refreshRateStateData" (input cbState:screen-value,
                                 output table ratestate).
 close query brwRateState.
 open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.

 run refreshstatusbar.

 find first ratestate no-error.
 if available ratestate then
   activeRateStateID  =  rowid(ratestate).

 apply "value-changed" to brwRateState in FRAME DEFAULT-FRAME.

 setScreenHeader().

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteScenario C-Win 
PROCEDURE deleteScenario :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lDeleteSuccess as logical.
  
  if available ratescenario
   then
    do:
       publish "DeleteRateScenario"  (input ratescenario.scenarioID,
                                      output lDeleteSuccess).            
                                                                  
       if not lDeleteSuccess                         
        then
         return.
                                              
       apply "value-changed" to brwRateState in frame {&frame-name}. 
    end.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doSelect C-Win 
PROCEDURE doSelect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable versionhandle as handle      no-undo.
 define variable openVersion   as logical     no-undo.
 define variable cStateList    as character   no-undo.
 define variable cState        as character   no-undo.
 
 do with frame default-frame:
 end.
 
 publish "checkOpenVersions" (input activeCardSetID,
                              output openVersion,
                              output versionhandle ).
 if not openVersion
  then
   do:
     cStateList = cbState:list-item-pairs.
     cState = entry((lookup(cbstate:screen-value,cStateList) - 1),cStateList) no-error.
     run wratecardmaintain.w persistent (input cState,
                                         input ratestate.version,
                                         input ratestate.cardsetid,
                                         input table ratestate) no-error.
   end.
 else
   run ViewWindow in versionhandle no-error.   
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EditRateState C-Win 
PROCEDURE EditRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable lSuccess as logical no-undo.
 define variable iCount   as integer no-undo.

 run windowresized.
 
 find current ratestate no-error.
 if available ratestate then
   assign 
     incardset = ratestate.cardSetID.

 run dialognewstate.w  (cbState:screen-value in frame DEFAULT-FRAME,
                        ratestate.version,
                        ratestate.cardsetid,
                        "Edit",
                        input table ratestate,
                        output lSuccess) no-error.

 if can-find(first ratestate) then
   empty temp-table ratestate no-error.
   
 publish "refreshRateStateData" (input cbState:screen-value,
                                 output table ratestate).

 do iCount = 1 TO brwRateState:num-iterations:
   if brwRateState:is-row-selected(iCount) then leave.
 end.

 close query brwRateState.
 open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
 
 find first ratestate where ratestate.cardsetid = incardset no-error.
 if available ratestate then
   activeRateStateID  =  rowid(ratestate).
 
 open query brwRateState for each ratestate by ratestate.version by ratestate.lastModifiedDate.
 brwRateState:set-repositioned-row(iCount,"ALWAYS").
 reposition brwRateState to rowid activeRateStateID no-error.
 apply 'value-changed' to brwRateState in frame DEFAULT-FRAME.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY edCalc cbState 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-54 btMaintenance RECT-68 RECT-69 RECT-70 btEdit imAlert 
         edCalc btRefresh cbState brwRateState btExport brwScenario 
         brwScenarioCard bDocuments btActivate btCopy btDelate btNew 
         btRateCalculator 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tHandle as handle    no-undo.
 define variable rptDir  as character no-undo.
 define variable std-in  as integer   no-undo.
 define variable cFields as character no-undo.
 define variable cHeader as character no-undo.

 if query brwRateState:num-results = 0
  then
   do:
     btExport:sensitive in frame DEFAULT-FRAME = true.
     message "There is nothing to export"
      view-as alert-box warning buttons ok.
     return.
   end.
   
 empty temp-table ratestatescenario.
 
 for first ratestate where ratestate.cardsetid = activecardsetid: 
 
   if lShowScenario
    then
     do: 
        assign
            cFields = "active,version,description,lastModifiedDate,effectiveDate,createdBy,approvedBy,dateApproved,dateStateApproved,filingMethod,scenario,scenariodescription,region,property,cardname,attribute,scenariocreatedDate,scenariocardcreatedDate"
            cHeader = "Active,Version,Description,Last Modified,Effective,Created By,Approved By,Approved,State Approved,Filing Method,Scenario Name,Description,Region,Property,CardName,Attribute, Scenario Created Date, Scenario Card Created Date"
            .
     
        if not can-find(first tratescenario where tratescenario.cardsetid = ratestate.cardsetid) 
         then
          do:
             create ratestatescenario.
             buffer-copy ratestate to ratestatescenario.    
          end.
         else
          for each tratescenario where tratescenario.cardsetid = ratestate.cardsetid:
             
            for each tratescenariocard where tratescenariocard.scenarioid = tratescenario.scenarioid:
             
              create ratestatescenario.
              buffer-copy ratestate to ratestatescenario.
              
              assign
                  ratestatescenario.scenario                = tratescenario.scenario
                  ratestatescenario.scenariodescription     = tratescenario.description
                  ratestatescenario.region                  = tratescenario.region
                  ratestatescenario.property                = tratescenario.property              
                  ratestatescenario.scenariocreatedDate     = tratescenario.createddate
                  ratestatescenario.scenariocardcreatedDate = tratescenariocard.createddate
                  ratestatescenario.cardname                = tratescenariocard.cardname
                  ratestatescenario.attribute               = tratescenariocard.attribute
                  .
            end.   
          end. 
     end.
    else
     do:
        assign
            cFields = "active,version,description,lastModifiedDate,effectiveDate,createdBy,approvedBy,dateApproved,dateStateApproved,filingMethod"
            cHeader = "Active,Version,Description,Last Modified,Effective,Created By,Approved By,Approved,State Approved,Filing Method"
            .
            
        create ratestatescenario.
        buffer-copy ratestate to ratestatescenario.   
     end.
 end.
 
 publish "GetReportDir" (output rptDir).
 tHandle = temp-table ratestatescenario:handle.
  
 run util/exporttable.p (table-handle tHandle,
                         "ratestatescenario",
                         "for each ratestatescenario by ratestatescenario.version by ratestatescenario.lastModifiedDate. ",
                         cFields,
                         cHeader,
                         rptDir,
                         "rateState-" + "-" + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                         true,
                         output rptDir,
                         output std-in) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getScenarios C-Win 
PROCEDURE getScenarios :
/*------------------------------------------------------------------------------
  Purpose:     Get the list of ratesscenarios for the selected state version.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipRateCardSetID as integer.

  define variable iCnt  as integer no-undo.
  define variable iAttribCnt  as integer no-undo.   
  /* Reset all temp-tables.*/
  empty temp-table ratescenario.
  empty temp-table ratescenariocard.
  empty temp-table tratescenario.
  empty temp-table tratescenariocard.
  
  publish "getratescenarios" (input ipRateCardSetID,
                              input lrefreshScenario,
                              output table tratescenario,
                              output table tratescenariocard).    
  
  lrefreshScenario = false.
  
  for each tratescenario:
   
        create ratescenario.
     buffer-copy tratescenario except property to ratescenario.
     do iCnt = 1 to num-entries(tratescenario.property,","):
       assign ratescenario.property = ratescenario.property + "," + entry(lookup(entry(iCnt,tratescenario.property),{&PropTypes}) + 1, {&PropTypes}) no-error.
      end.
     
     ratescenario.property = trim(ratescenario.property, ",").
  end.

  /* If any card involves multiple attributes, then show them all as an individual ratescenariocard record*/
  for each tratescenariocard:
    if num-entries(tratescenariocard.attribute,",") > 1 
     then
      do iAttribCnt = 1 to num-entries(tratescenariocard.attribute,","):
        create ratescenariocard.
        buffer-copy tratescenariocard to ratescenariocard.
        assign  ratescenariocard.attribute = entry(iAttribCnt,tratescenariocard.attribute,",").
      end.
     else
     do:
        create ratescenariocard.
        buffer-copy tratescenariocard to ratescenariocard.
     end.
  end.

  do with frame default-frame:
  
  end.
 
  brwScenario:sensitive     = can-find(first tratescenario).
  brwScenariocard:sensitive = can-find(first tratescenario).



  close query brwScenario.
  open query brwScenario for each ratescenario where ratescenario.cardsetID = ipRateCardSetID.

  find first ratescenario where ratescenario.cardsetID = ipRateCardSetID no-error.    
  if available ratescenario 
   then
    do:
       activeRateScenarioID  =  rowid(ratescenario).
       reposition brwScenario to rowid activeRateScenarioID no-error. 
    end.    
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openMaintainCards C-Win 
PROCEDURE openMaintainCards :
/*------------------------------------------------------------------------------
  Purpose:     Maintains the list of currently opened card window. 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter ipiCardID  as integer  no-undo.
 define input parameter iphaWindow as handle   no-undo.
 
 find openCards 
   where openCards.cardID = ipiCardID no-error.
  
 if not available openCards 
  then
   do:
      create openCards.
      openCards.cardID = ipiCardID.
   end.
  
  openCards.hInstance = iphaWindow. 
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openMaintainDocuments C-Win 
PROCEDURE openMaintainDocuments :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pCardSetID as integer no-undo.
 define input parameter pWindow    as handle  no-undo.

 find openDocs 
  where openDocs.cardSetID = pCardSetID no-error.
 if not available openDocs 
  then
   do:
     create openDocs.
     openDocs.cardSetID = pCardSetID.
   end.

 openDocs.hInstance = pWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openMaintainVersions C-Win 
PROCEDURE openMaintainVersions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pCardSetID as integer no-undo.
 define input parameter pWindow    as handle  no-undo.
 
 find openVersions
   where openVersions.cardSetID = pCardSetID no-error.
 if not available openVersions
  then
   do:
     create openVersions.
     openVersions.cardSetID = activeCardSetID.
   end.

 openVersions.hInstance = pWindow.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshScenarios C-Win 
PROCEDURE refreshScenarios :
/*------------------------------------------------------------------------------
  Purpose:     Refreshes the scenario browse and repositioning.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/   
  lrefreshScenario = true.
  
  apply "value-changed" to brwRateState in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshState C-Win 
PROCEDURE refreshState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable iCount      as integer no-undo.
  
 do with frame {&frame-name}:
 end.

 run windowresized.
 
 find current ratestate no-error.
 
 if available ratestate 
  then
   incardset = ratestate.cardSetID.

 publish "refreshRateStateData"(input cbState:screen-value,
                                output table ratestate). 
  
 open query brwRateState for each rateState.
 
 find first ratestate where ratestate.cardsetid = incardset no-error.
 if available ratestate 
  then
   activeRateStateID  =  rowid(ratestate). 

 reposition brwRateState to rowid activeRateStateID no-error.
 
 apply 'value-changed' to brwRateState in frame default-frame.

 run refreshstatusbar.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshStatusBar C-Win 
PROCEDURE refreshStatusBar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cStatus as character no-undo.
  std-in = 0.

  for each ratestate:
    std-in = std-in + 1.
  end.

  cStatus = string(std-in) + " " + if std-in = 1 then "record found" else "record(s) found as of" + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss").

  status default cStatus in window {&window-name}.
  status input   cStatus in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortData-multi.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewCalculator C-Win 
PROCEDURE ViewCalculator :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable perror              as logical no-undo.
  define variable perrormsg           as character no-undo.
  define variable datamodelRFile      as character no-undo.
  define variable userInterfaceRFile  as character no-undo.

  do with frame {&frame-name}:

  end.
  
  run windowresized.
  
  apply 'value-changed' to brwRateState.
  find current ratestate no-error. 
  if available ratestate then
  do:
  
   /* Sets the state ID of calculator in state data model */
    publish "setStateID" (input ratestate.stateID).
    
     if ratestate.execDataModel <> "" then
     do:
        datamodelRFile = removeItemFromList(ratestate.execDataModel,entry(num-entries(ratestate.execDataModel,"."),ratestate.execDataModel,"."),"."). 
        datamodelRFile = datamodelRFile + ".r".
    
    userInterfaceRFile = removeItemFromList(ratestate.execUserInterface,entry(num-entries(ratestate.execUserInterface,"."),ratestate.execUserInterface,"."),".").
    userInterfaceRFile = userInterfaceRFile + ".r".

    if search(ratestate.execDataModel) ne ?  then
    do:
      run value(ratestate.execDataModel) persistent (input ratestate.configUserInterface,
                                                     output perror,
                                                     output perrormsg) no-error.
      if error-status:error or error-status:num-messages > 0 then
      do:
        message "Data Model not found"
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
      else if perror then
      do:
        message perrormsg
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
    end.
    else if search(datamodelRFile) ne ? then
    do:
           run value(datamodelRFile) persistent (input ratestate.configUserInterface,
                                                 output perror,
                                                     output perrormsg) no-error.
      if error-status:error or error-status:num-messages > 0 then
      do:
        message "Data Model not found"
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
      else if perror then
      do:
        message perrormsg
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
    end.
    else 
    do:
      message "Data Model not found"
        view-as alert-box warning buttons ok.
      return no-apply.
    end.

    if search(ratestate.execUserInterface) ne ?  then
    do:
      run value(ratestate.execUserInterface) persistent  no-error.
      if error-status:error or error-status:num-messages > 0 then
      do:
        message  "User Interface not found"
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
      else if perror then
      do:
        message perrormsg
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
    end.
    else if search(userInterfaceRFile)   ne ? then
    do:
      run value(userInterfaceRFile) persistent no-error.
      if error-status:error or error-status:num-messages > 0 then
      do:
        message "User Interface not found"
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
      else if perror then
      do:
        message perrormsg
          view-as alert-box warning buttons ok.
        return no-apply.
      end.
    end.
    else 
    do:
      message  "User Interface not found"
        view-as alert-box warning buttons ok.
      return no-apply.
    end.

        publish "SetCurrentValue" (cbState:screen-value + "Version", string(ratestate.version)).
     end.
     else
     do:      
       find current ratestate no-error.
       if available ratestate then
       do:
          publish "SetCurrentValue" ("RateStateID", input ratestate.stateID).
          publish "SetCurrentValue" ("RateVersionNo", input string(ratestate.version)).
         run ratecalculator.w persistent no-error.
       end.
     end.
  end.
  publish "ApplyEntry".


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewScenarioCard C-Win 
PROCEDURE viewScenarioCard :
/*------------------------------------------------------------------------------
  Purpose: Opens the ratecard for the card selected in browse brwScenarioCard  
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lopenCard    as logical   no-undo.
  define variable ophlWindow   as handle    no-undo.
 
  find first ratestate 
    where ratestate.cardsetid = activeCardSetID no-error. 
    
  find current rateScenarioCard no-error.
  
   if available rateScenarioCard
    then
     activeCardID = rateScenarioCard.cardID.
     
  if available ratestate and activeCardID ne 0 and activeCardID ne ?
   then
    do:  
       publish "checkOpenCards" (input  activeCardID,
                                  output lopenCard,
                                  output ophlWindow ).
                          
       if not lopenCard
        then
         run wratedetailmaintain.w persistent (input ratestate.cardsetID,
                                               input activeCardID,
                                               input "Edit",
                                               input ratestate.stateID,
                                               input ratestate.version,
                                               input ratestate.effectivedate,
                                               input ratestate.description) no-error.
        else
         run ViewWindow in ophlWindow no-error.
         
    end.   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowresized C-Win 
PROCEDURE windowresized :
/*------------------------------------------------------------------------------
  Purpose:     Setting the window & frame dimensions & size during window resizing or maximizing.
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  do with frame {&frame-name}:
  end.
  
   /* set frame height and width */
  frame {&frame-name}:width-pixels = {&window-name}:width-pixels.             
  frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.   
  frame {&frame-name}:height-pixels = {&window-name}:height-pixels.         
  frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels. 
  
  if not lshowscenario then
  do:
     brwratestate:height = c-win:height - 2.7.
     brwratestate:width  = c-win:width  - 2.9.
  end.
  else
  do:
     brwratestate:height = drsH.
     brwratestate:width  = c-win:width - 2.9.

     brwScenario:width   =  dsw.
     brwScenario:height  =  c-win:height - (drsH + 2.9).

     brwScenariocard:width   =  c-win:width  - (dsw + 4).
     brwScenariocard:height  =  c-win:height - (drsH + 2.9).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION RemoveItemFromList C-Win 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character):
 
define variable lipos as integer no-undo.

lipos = lookup(pcremoveitem,pclist,pcdelimiter).

if lipos > 0 then
do:
  assign entry(lipos,pclist,pcdelimiter) = "".
  if lipos = 1 then
    pclist = substring(pclist,2).
  else if lipos = num-entries(pclist,pcdelimiter) then
    pclist = substring(pclist,1,length(pclist) - 1).
  else
    pclist = replace(pclist,pcdelimiter + pcdelimiter,pcdelimiter).
end.

return pclist.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setScreenHeader C-Win 
FUNCTION setScreenHeader RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.

if cbState:screen-value = "" or 
   cbState:screen-value = "ALL" then
do:
   assign
     btRateCalculator:sensitive = false
     btRefresh:sensitive        = false
     btExport:sensitive         = false
     btMaintenance:sensitive   = false
     btActivate:sensitive       = false
     bDocuments:sensitive       = false
     btNew:sensitive            = false
     btDelate:sensitive         = false
     btEdit:sensitive           = false
     btCopy:sensitive           = false
     imalert:hidden             = true
     edCalc:hidden              = true.
end.

else if not can-find(first ratestate) then
do:
   assign
     btRateCalculator:sensitive = false
     btRefresh:sensitive        = true
     btExport:sensitive         = false
     btMaintenance:sensitive   = false
     btActivate:sensitive       = false
     bDocuments:sensitive       = false
     btNew:sensitive            = true
     btDelate:sensitive         = false
     btEdit:sensitive           = false
     btCopy:sensitive           = false
     .
    assign 
      imalert:hidden = false  
      edCalc:hidden  = false.  
      imalert:load-image("images/exclamation.gif") no-error.
      edCalc:screen-value  = "Rate Calculator is not active for selected state.".
end.

else if can-find(first ratestate) and not can-find(first ratestate where ratestate.active = true) then
do:
  assign
    btRateCalculator:sensitive = true
    btRefresh:sensitive        = true
    btExport:sensitive         = true
    btMaintenance:sensitive   = true
    btActivate:sensitive       = true
    bDocuments:sensitive       = true
    btNew:sensitive            = true
    btDelate:sensitive         = true
    btEdit:sensitive           = true
    btCopy:sensitive           = true
    imalert:hidden             = false  
    edCalc:hidden              = false.  
    imalert:load-image("images/exclamation.gif").
    edCalc:screen-value  = "Rate Calculator is not active for selected state.".

end.

else if can-find(first ratestate) and can-find(first ratestate where ratestate.active = true) then
do:
  assign
    btRateCalculator:sensitive = true
    btRefresh:sensitive        = true
    btExport:sensitive         = true
    btMaintenance:sensitive   = true
    btActivate:sensitive       = true
    bDocuments:sensitive       = true
    btNew:sensitive            = true
    btDelate:sensitive         = true
    btEdit:sensitive           = true
    btCopy:sensitive           = true
    imalert:hidden             = true
    edCalc:hidden              = true.
                    
  find current ratestate no-error.
  if available ratestate then
  do:
    if ratestate.active = true then
      assign
        btDelate:sensitive   = false
        btActivate:sensitive = false.
  end.

end.
return false.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

