&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* temp-table definitions                                               */
{tt\ratelog.i   &tablealias=tempRateLog}
{tt\raterule.i}
{lib\std-def.i}
/* Parameters Definitions ---                                           */
define input parameter pCardSetID     as integer   no-undo.
define input parameter pCardID        as integer   no-undo.
define input parameter pVersion       as integer   no-undo.
define input parameter pVersionDesc   as character no-undo.   
define input parameter pStateID       as character no-undo.
define input parameter pEffDate       as datetime  no-undo. 
define input parameter cAttributeList as character no-undo.
define input parameter pCardName      as character no-undo.
define input parameter pRegion        as character no-undo.
define input parameter table for raterule.
/* Local Variable Definitions ---                                       */
define variable pcinputvalues as character no-undo.
define variable ppremiumerror as character no-undo.
define variable ppremium      as decimal   no-undo.

{lib/winlaunch.i}

define temp-table ratelogwin
    field hratelog as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fCovrageAmnt 
&Scoped-Define DISPLAYED-OBJECTS fCovrageAmnt fAttr fPremium 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCalculation  NO-FOCUS
     LABEL "calculation" 
     SIZE 7.2 BY 1.71 TOOLTIP "Show calculation".

DEFINE BUTTON bTest  NO-FOCUS
     LABEL "Test" 
     SIZE 7.2 BY 1.71 TOOLTIP "Test rate card".

DEFINE VARIABLE fAttr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Attribute" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE fCovrageAmnt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Coverage Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.2 BY 1 NO-UNDO.

DEFINE VARIABLE fPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Premium Amount" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 17.2 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bTest AT ROW 1.29 COL 48.2 WIDGET-ID 326 NO-TAB-STOP 
     fCovrageAmnt AT ROW 1.33 COL 24 COLON-ALIGNED WIDGET-ID 330 NO-TAB-STOP 
     fAttr AT ROW 2.57 COL 24 COLON-ALIGNED WIDGET-ID 328 NO-TAB-STOP 
     fPremium AT ROW 3.81 COL 24 COLON-ALIGNED WIDGET-ID 332 NO-TAB-STOP 
     bCalculation AT ROW 3.05 COL 48.4 WIDGET-ID 324 NO-TAB-STOP 
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 62 BY 4.14
         DEFAULT-BUTTON bTest WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Test Card"
         HEIGHT             = 4.14
         WIDTH              = 62
         MAX-HEIGHT         = 33.62
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.62
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON bCalculation IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bTest IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX fAttr IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fPremium IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Test Card */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Test Card */
DO:

  run emptyRateLogWin(pCardSetID,
                      pCardID).
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCalculation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCalculation C-Win
ON CHOOSE OF bCalculation IN FRAME DEFAULT-FRAME /* calculation */
DO:
  pcInputValues = "CoverageAmount^"   + string(int(fCovrageAmnt:screen-value)) + 
                  ",Attribute^"       + (if fAttr:screen-value = ? or fAttr:screen-value = "?" then "" else string(fAttr:screen-value)) +  
                  ",CardSetID^"       + string(pCardSetID) + 
                  ",Version^"         + string(pVersion)   + 
                  ",VersionDesc^"     + pVersionDesc       +  
                  ",manualEffectiveDate^"   + string(pEffDate) no-error.
  create ratelogwin.
  run wlogs.w persistent set ratelogwin.hratelog(input table tempRateLog,
                                                 input pcInputValues) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTest C-Win
ON CHOOSE OF bTest IN FRAME DEFAULT-FRAME /* Test */
DO:

  publish "TestRateCardPremium" (input pCardSetID,
                                 input pCardName,
                                 input pRegion,
                                 input fCovrageAmnt:screen-value,
                                 input (if (fAttr:screen-value = "?" or fAttr:screen-value = ?) then "" else fAttr:screen-value),
                                 output ppremiumerror,
                                 output table tempRateLog).
 
  ppremium = decimal(ppremiumerror) no-error.
  
  if not (error-status:error) then
  do:
    assign
      bCalculation:sensitive = true.
      fPremium:screen-value = string(ppremium).
  end.
  else
    MESSAGE ppremiumerror
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fAttr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fAttr C-Win
ON VALUE-CHANGED OF fAttr IN FRAME DEFAULT-FRAME /* Attribute */
DO:
  assign
    fPremium:screen-value = "0.00"
    bCalculation:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCovrageAmnt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCovrageAmnt C-Win
ON VALUE-CHANGED OF fCovrageAmnt IN FRAME DEFAULT-FRAME /* Coverage Amount */
DO:
  assign
    fPremium:screen-value = "0.00"
    bCalculation:sensitive = false.
  if fCovrageAmnt:screen-value <> "0" then
    assign
      bTest:sensitive          = true
      fAttr:sensitive          = true
      .
  else
    assign 
      bTest:sensitive          = false
      bCalculation:sensitive   = false
      fAttr:sensitive          = false
        .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

publish "OpenMaintainTestWin"  (input pCardSetID, input pCardID, input this-procedure).
subscribe to "ApplyEntry"      anywhere.
subscribe to "ApplyClose"      anywhere.
subscribe to "emptyRateLogWin" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

bTest:load-image("images/calc.bmp").
bTest:load-image-insensitive("images/calc-i.bmp").

bCalculation:load-image("images/log.bmp").
bCalculation:load-image-insensitive("images/log-i.bmp").

{lib/win-main.i}
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
fAttr:list-items   = cAttributeList.
fAttr:screen-value = fAttr:entry(1) no-error.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApplyEntry C-Win 
PROCEDURE ApplyEntry :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter cardID as integer.

if pCardID = cardId then
  apply 'entry' to fCovrageAmnt in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyRateLogWin C-Win 
PROCEDURE emptyRateLogWin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter cardsetid as integer.
define input parameter cardid    as integer.

if cardsetid = pCardSetID  and
  cardid = pCardID   then
do:
  publish "emptyViewCards"(pCardSetID,
                           pCardID).
  for each ratelogwin:
    delete procedure ratelogwin.hRateLog no-error.
  end.
  empty temp-table ratelogwin.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fCovrageAmnt fAttr fPremium 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE fCovrageAmnt 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

