&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS fMain 
/*------------------------------------------------------------------------
 File: dialogconfig.w

 Description: Create a configuration file to select states at beginning
 
 Input Parameters:
     <none>
 
 Output Parameters:
     <none>
 
 Author: Anjly Chanana
 
 Created:
  
 Modified    :
  Date        Name     Comments
  07/01/2019  Sachin   Bug Fixes.

------------------------------------------------------------------------*/

{lib/std-def.i}

&global-define defaultState     "Alabama,AL"

/* local variables definitions  */
define variable availableStates as character no-undo.
define variable selectedStates  as character no-undo.
define variable cStatesList     as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bAddAll bRemoveAll tShowState tOtherStates ~
tConfirmExit tStates tReportDir bReportsSearch Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS tShowState tOtherStates tConfirmExit ~
tStates tReportDir 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddAll  NO-FOCUS
     LABEL "ALL-->" 
     SIZE 4.4 BY 1.05 TOOLTIP "Add all States to the Search option".

DEFINE BUTTON bAddState  NO-FOCUS
     LABEL "-->" 
     SIZE 4.4 BY 1.05 TOOLTIP "Add a State to the Search options".

DEFINE BUTTON bDeleteState  NO-FOCUS
     LABEL "<--" 
     SIZE 4.4 BY 1.05 TOOLTIP "Remove a State from the Search options".

DEFINE BUTTON bRemoveAll  NO-FOCUS
     LABEL "<-- ALL" 
     SIZE 4.4 BY 1.05 TOOLTIP "Remove all States from the Search option".

DEFINE BUTTON bReportsSearch 
     LABEL "..." 
     SIZE 4 BY 1.

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tReportDir AS CHARACTER FORMAT "X(200)":U 
     LABEL "Reports Directory" 
     VIEW-AS FILL-IN 
     SIZE 55 BY 1 TOOLTIP "Enter the default directory to save report PDF documents and CSV export files" NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 81.4 BY 15.29.

DEFINE VARIABLE tOtherStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     SIZE 24 BY 9.48 NO-UNDO.

DEFINE VARIABLE tStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SORT SCROLLBAR-VERTICAL 
     SIZE 24 BY 9.48 NO-UNDO.

DEFINE VARIABLE tConfirmExit AS LOGICAL INITIAL no 
     LABEL "Confirm Application Exit" 
     VIEW-AS TOGGLE-BOX
     SIZE 25.8 BY .81 NO-UNDO.

DEFINE VARIABLE tShowState AS LOGICAL INITIAL no 
     LABEL "Open in Last State Viewed" 
     VIEW-AS TOGGLE-BOX
     SIZE 29 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bAddAll AT ROW 4.52 COL 40.8 WIDGET-ID 90 NO-TAB-STOP 
     bAddState AT ROW 5.86 COL 40.8 WIDGET-ID 78 NO-TAB-STOP 
     bDeleteState AT ROW 7.19 COL 40.8 WIDGET-ID 80 NO-TAB-STOP 
     bRemoveAll AT ROW 8.57 COL 40.8 WIDGET-ID 88 NO-TAB-STOP 
     tShowState AT ROW 12.33 COL 21.4 WIDGET-ID 92
     tOtherStates AT ROW 2.33 COL 15 NO-LABEL WIDGET-ID 82 NO-TAB-STOP 
     tConfirmExit AT ROW 13.57 COL 21.4 WIDGET-ID 132
     tStates AT ROW 2.33 COL 47 NO-LABEL WIDGET-ID 74 NO-TAB-STOP 
     tReportDir AT ROW 14.91 COL 19.4 COLON-ALIGNED WIDGET-ID 2
     bReportsSearch AT ROW 14.91 COL 77.8 WIDGET-ID 30
     Btn_OK AT ROW 17.14 COL 26.6
     Btn_Cancel AT ROW 17.14 COL 44.2
     "Selected States" VIEW-AS TEXT
          SIZE 18.6 BY .62 AT ROW 1.67 COL 49.4 WIDGET-ID 162
          FONT 6
     "Available States" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 1.71 COL 17.4 WIDGET-ID 160
          FONT 6
     "Options" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 1.05 COL 3.8 WIDGET-ID 70
     RECT-31 AT ROW 1.33 COL 2.4 WIDGET-ID 68
     SPACE(0.39) SKIP(2.04)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX fMain
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE
       FRAME fMain:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME fMain
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fMain
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fMain fMain
ON WINDOW-CLOSE OF FRAME fMain /* Configuration */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAll fMain
ON CHOOSE OF bAddAll IN FRAME fMain /* ALL--> */
DO:
  assign
    availableStates               = if tOtherStates:list-item-pairs = ? or tOtherStates:list-item-pairs = "?" then "" else tOtherStates:list-item-pairs
    selectedStates                = if tStates:list-item-pairs = ? or tStates:list-item-pairs = "?" then "" else tStates:list-item-pairs
    tStates:list-item-pairs       = trim(availableStates + "," + selectedStates,",")
    tOtherStates:list-item-pairs  = ","
    bAddAll:sensitive             = false
    bRemoveAll:sensitive          = true
    bAddState:sensitive           = false
    availableStates               = ""
    selectedStates                = "" no-error
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddState fMain
ON CHOOSE OF bAddState IN FRAME fMain /* --> */
DO:
  run addState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteState fMain
ON CHOOSE OF bDeleteState IN FRAME fMain /* <-- */
DO:
  run deleteState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll fMain
ON CHOOSE OF bRemoveAll IN FRAME fMain /* <-- ALL */
DO:
  define variable remSelectedState as character no-undo. /* variable to store the default remaining state when 'removeAll' is selected 
                                                            (top most state in this case) */

  assign
    availableStates               = if tOtherStates:list-item-pairs = ? or tOtherStates:list-item-pairs = "?" then "" else tOtherStates:list-item-pairs
    selectedStates                = if tStates:list-item-pairs = ? or tStates:list-item-pairs = "?" then "" else tStates:list-item-pairs 
    remSelectedState              = entry(1,selectedStates) + "," + entry(2,selectedStates)
    tOtherStates:list-item-pairs  = trim(availableStates + "," + selectedStates,",")
    tStates:list-item-pairs       = ","
    bRemoveAll:sensitive          = false
    bAddAll:sensitive             = true
    bDeleteState:sensitive        = false
    availableStates               = ""
    selectedStates                = "" 
    tStates:list-item-pairs       = trim((tStates:list-item-pairs + "," + remSelectedState),",")
    .

  /* Delete states occuring in 'Selected states' from 'Available States' */
  do std-in = 2 to num-entries(tStates:list-item-pairs) by 2:
    tOtherStates:delete(entry(std-in, tStates:list-item-pairs)).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReportsSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReportsSearch fMain
ON CHOOSE OF bReportsSearch IN FRAME fMain /* ... */
DO:
  std-ch = tReportDir:input-value.
  if tReportDir:input-value > ""
   then 
    system-dialog get-dir std-ch
     initial-dir std-ch.
   else 
    system-dialog get-dir std-ch.
  
  if std-ch > "" and std-ch <> ?
   then 
    tReportDir:screen-value = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel fMain
ON CHOOSE OF Btn_Cancel IN FRAME fMain /* Cancel */
DO:
  std-lo = no. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK fMain
ON CHOOSE OF Btn_OK IN FRAME fMain /* Save */
DO:
  std-lo = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOtherStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fMain
ON DEFAULT-ACTION OF tOtherStates IN FRAME fMain
DO:
  apply "CHOOSE" to bAddState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates fMain
ON VALUE-CHANGED OF tOtherStates IN FRAME fMain
DO:
  assign
    bAddState:sensitive    = tOtherStates:input-value <> ""
    bDeleteState:sensitive = false
    .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fMain
ON DEFAULT-ACTION OF tStates IN FRAME fMain
DO:
  apply "CHOOSE" to bDeleteState.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates fMain
ON VALUE-CHANGED OF tStates IN FRAME fMain
DO:
  if num-entries(tStates:list-item-pairs,",") ne 2    and
     tStates:input-value <> ""
   then
    bDeleteState:sensitive = true.

  bAddState:sensitive = false.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK fMain 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.
 bAddState   :load-image("images/s-next.bmp") no-error.
 bDeleteState:load-image("images/s-previous.bmp") no-error.
 bAddAll     :load-image("images/s-nextpg.bmp") no-error.
 bRemoveAll  :load-image("images/s-previouspg.bmp") no-error.

 bAddState:load-image-insensitive("images/s-next-i.bmp").
 bDeleteState:load-image-insensitive("images/s-previous-i.bmp").
 bAddAll:load-image-insensitive("images/s-nextpg-i.bmp").
 bRemoveAll:load-image-insensitive("images/s-previouspg-i.bmp").
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.

  publish "GetSearchStates" (output std-ch).
  publish "GetStateList" (output cStatesList). 
  tOtherStates:list-item-pairs = cStatesList. 

  /* Delete states occuring in 'Selected states' from 'Available States' */
  do std-in = 2 to num-entries(std-ch) by 2:
    tOtherStates:delete(entry(std-in, std-ch)).
  end.

  tStates:list-item-pairs = std-ch.
   
  /* Assign a default selected state, if no state is selected initially */
  if tStates:list-item-pairs = ","
   then
    do:
      tStates:list-item-pairs = {&defaultState}.
      tOtherStates:delete({&defaultState}).
    end.


  publish "GetConfirmExit" (output tConfirmExit).
  publish "GetReportDir" (output tReportDir).    
  publish "GetStateCondition" (output tShowState).
  
  /* At least one state must be present in list of selected states */
  if num-entries(tStates:list-item-pairs,",") = 2
   then
    bRemoveAll:sensitive = false.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if tReportDir:input-value > ""
   then
    do:
      publish "SetReportDir" (tReportDir:input-value).
      if error-status:error
       then
        do:
          message "Invalid Directory"
             view-as alert-box error buttons OK.
          undo MAIN-BLOCK, retry MAIN-BLOCK.
        end.
    end.
   else publish "SetReportDir" ("").

  publish "SetSearchStates" (tStates:list-item-pairs). 
  publish "SetStateCondition" (tShowState:checked).
  publish "SetReportDir" (tReportDir:input-value).
  publish "SetConfirmExit" (tConfirmExit:checked).
END.
               
run disable_UI.

return string(std-lo).

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addState fMain 
PROCEDURE addState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable icount as integer     no-undo.
  define variable cTemp  as character   no-undo.
  
  do with frame {&frame-name}:
  end.

  if tOtherStates:input-value = ?
   then 
    return.

  tStates:add-last(entry(lookup(tOtherStates:input-value, tOtherStates:list-item-pairs) - 1, tOtherStates:list-item-pairs), 
                   tOtherStates:input-value) no-error.
  tStates:list-item-pairs = trim(tStates:list-item-pairs,",") no-error.

  tOtherStates:delete(tOtherStates:input-value) no-error.

  assign
    bAddState:sensitive   = false
    bRemoveAll:sensitive  = num-entries(tstates:list-item-pairs,",") > 2
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteState fMain 
PROCEDURE deleteState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
      
  if tStates:input-value = ? or num-entries(tStates:list-item-pairs,",") = 2
   then 
    return.

  tOtherStates:add-last(entry(lookup(tStates:input-value, tStates:list-item-pairs) - 1, tStates:list-item-pairs), 
                        tStates:input-value) no-error.

  tOtherStates:list-item-pairs = trim(tOtherStates:list-item-pairs,",") no-error.
  tStates:delete(tStates:input-value) no-error.

  assign
    bDeleteState:sensitive  = false
    bAddAll:sensitive       = true
    .

  if num-entries(tStates:list-item-pairs,",") = 2
   then
    bRemoveAll:sensitive = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI fMain  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME fMain.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI fMain  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tShowState tOtherStates tConfirmExit tStates tReportDir 
      WITH FRAME fMain.
  ENABLE bAddAll bRemoveAll tShowState tOtherStates tConfirmExit tStates 
         tReportDir bReportsSearch Btn_OK Btn_Cancel 
      WITH FRAME fMain.
  VIEW FRAME fMain.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

