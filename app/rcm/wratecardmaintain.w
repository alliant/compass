&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: rcm/wratecardmaintain.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 
    @Modified    :
    Date        Name    Comments
    07/01/2019  Anjly   Testing bug fix.
    12/18/2019  Anubha  Fixed Flickering issue.
    12/18/2019  Anubha  Modified to show region description on browse
                        If region is not there then show ratecard description.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
/* Temp-table definitions                                                */
{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratecard.i &tablealias=tempRateCard}
{tt\rateui.i}
{lib\std-def.i}

/* Parameter definitions                                                 */
define input parameter pStateID  as character  no-undo.
define input parameter pVersion  as integer    no-undo.
define input parameter pCardSetID as integer   no-undo.
define input parameter table for ratestate.

/* Local variable definitions                                           */
define variable activeCardID       as integer   no-undo.
define variable activeCardSetID    as integer   no-undo.
define variable lApplySearchString as logical   no-undo.
define variable cLastSearchString  as character no-undo.

{lib/winlaunch.i}

define temp-table openCards no-undo
 field cardid    as integer
 field hInstance as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwRateCard

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ratecard

/* Definitions for BROWSE brwRateCard                                   */
&Scoped-define FIELDS-IN-QUERY-brwRateCard ratecard.region ratecard.cardName ratecard.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwRateCard   
&Scoped-define SELF-NAME brwRateCard
&Scoped-define QUERY-STRING-brwRateCard for each ratecard no-lock by ratecard.region by ratecard.cardname
&Scoped-define OPEN-QUERY-brwRateCard open query {&SELF-NAME} for each ratecard no-lock by ratecard.region by ratecard.cardname.
&Scoped-define TABLES-IN-QUERY-brwRateCard ratecard
&Scoped-define FIRST-TABLE-IN-QUERY-brwRateCard ratecard


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwRateCard}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bSearch RECT-68 RECT-69 fSearchCard ~
brwRateCard btCopy btCopyRegion btNew btPDF btDelete btEdit btExport ~
btRefresh 
&Scoped-Define DISPLAYED-OBJECTS fStateID fiVersion fEffDate tbActive ~
fSearchCard 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQAR 
       MENU-ITEM m_View_Detail  LABEL "View Detail"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bSearch  NO-FOCUS
     LABEL "Search" 
     SIZE 5 BY 1.1 TOOLTIP "Search".

DEFINE BUTTON btCopy  NO-FOCUS
     LABEL "Copy" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy existing card".

DEFINE BUTTON btCopyRegion  NO-FOCUS
     LABEL "Region" 
     SIZE 7.2 BY 1.71 TOOLTIP "Copy region rate cards".

DEFINE BUTTON btDelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove selected card".

DEFINE BUTTON btEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify selected card".

DEFINE BUTTON btExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE BUTTON btNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "Add new card".

DEFINE BUTTON btPDF  NO-FOCUS
     LABEL "PDF" 
     SIZE 7.2 BY 1.71 TOOLTIP "Print rate card".

DEFINE BUTTON btRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh".

DEFINE VARIABLE fEffDate AS DATE FORMAT "99/99/99":U 
     LABEL "Manual Effective" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15.8 BY 1 NO-UNDO.

DEFINE VARIABLE fiVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE fSearchCard AS CHARACTER FORMAT "X(256)":U 
     LABEL "Search" 
     VIEW-AS FILL-IN 
     SIZE 70.2 BY 1 NO-UNDO.

DEFINE VARIABLE fStateID AS CHARACTER FORMAT "X(256)":U 
     LABEL "State ID" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 15.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29.6 BY 1.86.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29.8 BY 1.86.

DEFINE VARIABLE tbActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwRateCard FOR 
      ratecard SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwRateCard C-Win _FREEFORM
  QUERY brwRateCard DISPLAY
      ratecard.region             label "Region"     width 30     format "x(30)" 
      ratecard.cardName           label "Name"             width 35     format "x(50)"  
ratecard.description              label "Description"      width 40     format "x(150)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 183 BY 14.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bSearch AT ROW 2.52 COL 149.6 WIDGET-ID 304 NO-TAB-STOP 
     fStateID AT ROW 1.38 COL 77 COLON-ALIGNED WIDGET-ID 302
     fiVersion AT ROW 1.38 COL 105 COLON-ALIGNED WIDGET-ID 270
     fEffDate AT ROW 1.38 COL 131.4 COLON-ALIGNED WIDGET-ID 212
     tbActive AT ROW 1.48 COL 150.4 WIDGET-ID 292
     fSearchCard AT ROW 2.57 COL 77 COLON-ALIGNED WIDGET-ID 306
     brwRateCard AT ROW 3.95 COL 2 WIDGET-ID 400
     btCopy AT ROW 1.57 COL 53.2 WIDGET-ID 260 NO-TAB-STOP 
     btCopyRegion AT ROW 1.57 COL 23.8 WIDGET-ID 296 NO-TAB-STOP 
     btNew AT ROW 1.57 COL 31.6 WIDGET-ID 170 NO-TAB-STOP 
     btPDF AT ROW 1.57 COL 16.6 WIDGET-ID 294 NO-TAB-STOP 
     btDelete AT ROW 1.57 COL 38.8 WIDGET-ID 172 NO-TAB-STOP 
     btEdit AT ROW 1.57 COL 46 WIDGET-ID 258 NO-TAB-STOP 
     btExport AT ROW 1.57 COL 9.4 WIDGET-ID 10 NO-TAB-STOP 
     btRefresh AT ROW 1.57 COL 2.2 WIDGET-ID 278 NO-TAB-STOP 
     RECT-68 AT ROW 1.52 COL 2 WIDGET-ID 298
     RECT-69 AT ROW 1.52 COL 31.2 WIDGET-ID 300
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 185 BY 17.86
         DEFAULT-BUTTON bSearch WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Rate Cards"
         HEIGHT             = 17.86
         WIDTH              = 185
         MAX-HEIGHT         = 33.62
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.62
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = 32
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB brwRateCard fSearchCard DEFAULT-FRAME */
ASSIGN 
       brwRateCard:POPUP-MENU IN FRAME DEFAULT-FRAME             = MENU POPUP-MENU-brwQAR:HANDLE
       brwRateCard:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brwRateCard:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR FILL-IN fEffDate IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiVersion IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fStateID IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tbActive IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwRateCard
/* Query rebuild information for BROWSE brwRateCard
     _START_FREEFORM
open query {&SELF-NAME} for each ratecard no-lock by ratecard.region by ratecard.cardname.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwRateCard */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Rate Cards */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Rate Cards */
DO:
  publish "emptyDetailWindow"(input activeCardSetID).
  run emptytemp-tables in this-procedure no-error.
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRateCard
&Scoped-define SELF-NAME brwRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateCard C-Win
ON DEFAULT-ACTION OF brwRateCard IN FRAME DEFAULT-FRAME
DO:
  run EditRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateCard C-Win
ON LEFT-MOUSE-UP OF brwRateCard IN FRAME DEFAULT-FRAME
DO:
   find current ratecard no-error.
   if available ratecard 
    then
     do:
        find first region where region.regioncode = ratecard.region no-error.   
        brwRateCard:tooltip = if available region then region.description else ratecard.description.      /* show ratecard description if region is not there. */
     end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateCard C-Win
ON ROW-DISPLAY OF brwRateCard IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateCard C-Win
ON START-SEARCH OF brwRateCard IN FRAME DEFAULT-FRAME
DO:
   {lib/brw-startSearch.i} 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRateCard C-Win
ON VALUE-CHANGED OF brwRateCard IN FRAME DEFAULT-FRAME
DO:
 find current ratecard no-error.
 if available ratecard 
  then
   do:
     assign
       activeCardID           = ratecard.cardID
       btExport:sensitive     = true
       btPDF:sensitive        = true
       btCopyRegion:sensitive = true
       btDelete:sensitive     = true
       btEdit:sensitive       = true
       btCopy:sensitive       = true
       .
     
     find first region where region.regioncode = ratecard.region no-error.    

     brwRateCard:tooltip = if available region then region.description else ratecard.description.  /* show ratecard description if region is not there. */
   end.
  else 
    assign
      btExport:sensitive     = false
      btPDF:sensitive        = false
      btCopyRegion:sensitive = false
      btDelete:sensitive     = false
      btEdit:sensitive       = false
      btCopy:sensitive       = false
      brwRateCard:tooltip    = ""
      .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSearch
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSearch C-Win
ON CHOOSE OF bSearch IN FRAME DEFAULT-FRAME /* Search */
DO:
   /* if search button is clicked or return key is hit, 
     then 'cLastSearchString' stores the last string searched until user again hits the search */
  assign
      lApplySearchString = true  
      cLastSearchString  = fSearchCard:input-value
      .
  run filterData in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btCopy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCopy C-Win
ON CHOOSE OF btCopy IN FRAME DEFAULT-FRAME /* Copy */
DO:
  run CopyRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btCopyRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCopyRegion C-Win
ON CHOOSE OF btCopyRegion IN FRAME DEFAULT-FRAME /* Region */
DO:
  define variable selectedregion as character.
  
  find current ratecard no-error.
  if available ratecard 
   then
    selectedregion = ratecard.region.
  
  run dialogcopyregion.w(input pCardSetID,
                         input selectedregion,
                         input table tempRateCard) no-error.
  
  publish "refreshRateCardData" (output table tempRateCard).

  run filterData in this-procedure.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btDelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btDelete C-Win
ON CHOOSE OF btDelete IN FRAME DEFAULT-FRAME /* Delete */
DO: 
  run DeleteRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btEdit C-Win
ON CHOOSE OF btEdit IN FRAME DEFAULT-FRAME /* Edit */
DO:
  run EditRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btExport C-Win
ON CHOOSE OF btExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportData in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNew C-Win
ON CHOOSE OF btNew IN FRAME DEFAULT-FRAME /* New */
DO: 
  run AddRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btPDF
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btPDF C-Win
ON CHOOSE OF btPDF IN FRAME DEFAULT-FRAME /* PDF */
DO:
  find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
  if available ratestate 
   then
    run ratecardpdfmanual.p(input pCardSetID,
                            input 0,
                            input pStateID,
                            input pVersion,
                            input rateState.description,
                            input fEffDate:input-value) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRefresh C-Win
ON CHOOSE OF btRefresh IN FRAME DEFAULT-FRAME /* Refresh */
DO:
  publish "GetRateCards" (input pCardSetID,
                          input yes, /* refresh ratecards from server */
                          output table tempRateCard). 

  fSearchCard:screen-value = cLastSearchString.
  run filterData       in this-procedure.
  run refreshStatusBar in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail C-Win
ON CHOOSE OF MENU-ITEM m_View_Detail /* View Detail */
DO:
  run EditRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main.i}  
run emptytemp-tables in this-procedure no-error.
activeCardSetID = pCardSetID.

publish "GetRateCards" (input pCardSetID,
                        no, /*fetch ratecards from datamodel if exist*/
                        output table tempRateCard).                         

subscribe to "OpenMaintainCards"   anywhere.
subscribe to "DeleteMaintainCards" anywhere.
subscribe to "checkOpenCards"      anywhere.
subscribe to "refreshScreen"       anywhere.
subscribe to "CloseRateCardWindow" anywhere.

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

btNew       :load-image("images/new.bmp").
btDelete    :load-image("images/delete.bmp").
btDelete    :load-image-insensitive("images/delete-i.bmp").
btEdit      :load-image("images/update.bmp").
btEdit      :load-image-insensitive("images/update-i.bmp").
btCopy      :load-image("images/split.bmp").
btCopy      :load-image-insensitive("images/split-i.bmp").
btRefresh   :load-image-insensitive("images/refresh-i.bmp").
btRefresh   :load-image("images/refresh.bmp").
btExport    :load-image("images/excel.bmp").
btExport    :load-image-insensitive("images/excel-i.bmp").
btPDF       :load-image ("images/pdf.bmp") .
btPDF       :load-image-insensitive("images/pdf-i.bmp") .
btCopyRegion:load-image ("images/copy.bmp") .
btCopyRegion:load-image-insensitive("images/copy-i.bmp") .
bSearch     :load-image("images/s-magnifier.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  run enable_UI.
  find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
  if available ratestate 
   then
    do:  
      publish "OpenMaintainVersions" (string(ratestate.cardsetid), this-procedure) .
      assign 
          fStateID:screen-value    = if ratestate.stateID = ? or ratestate.stateID = "?" then ""    else  rateState.stateID               
          fiVersion:screen-value   = if ratestate.version = ?                            then ""    else  string(ratestate.version)              
          fEffDate:screen-value    = if ratestate.effectivedate = ?                      then ""    else  string(date(ratestate.effectivedate))   
          tbActive:checked         = if ratestate.active = ?                             then false else  rateState.active
          .
      c-win:title = c-win:title + if ratestate.description = ? or ratestate.description = "?" then "" else (" - " + rateState.description).
      
      publish "GetRateStateRegions" (ratestate.stateID,
                                     "Region",
                                     output table region).

    end.

  else
    c-win:title = "".

  cLastSearchString = fSearchCard:input-value.
  run filterData       in this-procedure.
  run refreshStatusBar in this-procedure no-error.

  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addRateCard C-Win 
PROCEDURE addRateCard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable reposRowID as rowid no-undo.

 find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
 if available ratestate 
  then
   run wratedetailmaintain.w persistent(input pCardSetID,
                                        input 0,
                                        input "New",
                                        input pStateID,
                                        input pVersion,
                                        input ratestate.effectivedate, 
                                        input ratestate.description) no-error .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeRateCardWindow C-Win 
PROCEDURE closeRateCardWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pCardSetID as integer no-undo.
 if pCardSetID = activeCardSetID 
  then
   do:
     run emptytemp-tables in this-procedure no-error.
     /* This event will close the window and terminate the procedure.  */
     apply "CLOSE":U to this-procedure.
     return no-apply.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE copyRateCard C-Win 
PROCEDURE copyRateCard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 find current ratecard no-error.
 if available ratecard 
  then
   activeCardID = ratecard.cardID.

 find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
 if available ratestate 
  then
   run wratedetailmaintain.w persistent(input pCardSetID,
                                        input activeCardID,
                                        input "Copy",
                                        input pStateID,
                                        input pVersion,
                                        input ratestate.effectivedate, 
                                        input ratestate.description) no-error .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRateCard C-Win 
PROCEDURE deleteRateCard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pSuccess   as logical no-undo.
  define variable cardhandle as handle  no-undo.
  define variable openCard   as logical no-undo.

  message "Highlighted rate card will be deleted. Are you sure?"
    view-as alert-box question buttons ok-cancel set std-lo.
  
  if not std-lo 
   then 
    return error.
   else 
    do:
      find current ratecard no-error.
      if available ratecard 
       then
        activeCardID           = ratecard.cardID.
      publish "DeleteRateCardDetail"(input activeCardID,
                                     output pSuccess).
    end.

  if pSuccess 
   then
    do:
      publish "checkOpenCards" (input activeCardID,
                              output openCard,
                              output cardhandle ).
      if openCard
       then  
        publish "DeleteMaintainCards" (input activeCardID).
    
      for first tempRateCard where tempRateCard.cardid = activeCardID:
        delete tempRateCard.
      end.
      
      run filterData in this-procedure.
    
      run refreshStatusBar in this-procedure no-error.
      publish "refreshState".
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE editRateCard C-Win 
PROCEDURE editRateCard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable cardhandle  as handle  no-undo.
 define variable lopenCard   as logical no-undo.

 publish "checkOpenCards" (input activeCardID,
                           output lopenCard,
                           output cardhandle ).
 if not lopenCard
  then
   do:
     find current ratecard no-error.
     if available ratecard 
      then
       activeCardID = ratecard.cardID.

     find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
     if available ratestate 
      then
       run wratedetailmaintain.w persistent(input pCardSetID,
                                            input activeCardID,
                                            input "Edit",
                                            input pStateID,
                                            input pVersion,
                                            input ratestate.effectivedate, 
                                            input ratestate.description) no-error.
   end.
  else
   run ViewWindow in cardhandle no-error.

   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptytemp-tables C-Win 
PROCEDURE emptytemp-tables :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 empty temp-table ratecard     no-error.
 empty temp-table opencards    no-error.
 empty temp-table tempRateCard no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fStateID fiVersion fEffDate tbActive fSearchCard 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bSearch RECT-68 RECT-69 fSearchCard brwRateCard btCopy btCopyRegion 
         btNew btPDF btDelete btEdit btExport btRefresh 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable ttHand     as handle    no-undo.
 define variable rptDir     as character no-undo.
 define variable std-in     as integer   no-undo.
 
 if query brwRateCard:num-results = 0
  then
   do:
     btExport:sensitive in frame DEFAULT-FRAME = true.
     message "There is nothing to export"
      view-as alert-box warning buttons ok.
     return.
   end.

 publish "GetReportDir" (output rptDir).
 ttHand = temp-table rateCard:handle.
 
 run util/exporttable.p (table-handle ttHand,
                          "rateCard",
                          "for each rateCard no-lock by rateCard.region by rateCard.cardname.",
                          "region,cardName,description",
                          "Region,Name,Description",
                          rptDir,
                          "rateCard-" + string(pCardSetID) + "-" + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          output rptDir,
                          output std-in) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  close query brwRateCard.
  empty temp-table rateCard.

  if lApplySearchString /* if search filter is also applied */
   then
    for each tempRateCard no-lock where 
     (tempRateCard.region      matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or 
     (tempRateCard.cardname    matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or
     (tempRateCard.description matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")))  
        by tempRateCard.region by tempRateCard.cardname:
      std-in = std-in + 1.
      create rateCard.
      buffer-copy tempRateCard to rateCard.
    end.  
   else
    for each tempRateCard:
      create rateCard.
      buffer-copy tempRateCard to rateCard.
    end.

  open query brwRateCard preselect each rateCard by ratecard.region by ratecard.cardname.

  setStatusCount(query brwRateCard:num-results).
  apply "value-changed" to brwRateCard.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshScreen C-Win 
PROCEDURE refreshScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pTitle     as character no-undo.
 define input parameter pCardSetID as integer   no-undo.
 define variable reposRowID        as rowid     no-undo.
 define variable iCount            as integer   no-undo.

 do with frame DEFAULT-FRAME:
 end.

 if activeCardSetID = pCardSetID 
  then
   do:
     empty temp-table tempRateCard no-error.
     publish "refreshRateCardData"(input pCardSetID,
                                   output table  tempRateCard ).
                                   
     publish "GetRegionList" (string(ratestate.cardsetid),
                              "Region",
                              output table region).
   
     run filterData in this-procedure.
   
     do iCount = 1 TO brwRateCard:num-iterations:
       if brwRateCard:is-row-selected(iCount) then leave.
     end.         
   
     close query brwRateCard.
   
     case pTitle : 
       when "Edit" then
       do:  
         for first rateCard where rateCard.cardID = activeCardID:
           reposRowID = rowid(rateCard).
         end. 
         open query brwRateCard for each rateCard where 
          (ratecard.region      matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or 
          (ratecard.cardname    matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or
          (ratecard.description matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")))   
            by rateCard.region by rateCard.cardName.
         brwRateCard:set-repositioned-row(iCount,"ALWAYS") no-error.      
         reposition brwRateCard to rowid reposRowID no-error.
       end. 
       otherwise
       do:
         for last rateCard by rateCard.cardID descending:
           reposRowID = rowid(rateCard).
         end.
         open query brwRateCard for each rateCard where 
          (ratecard.region      matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or 
          (ratecard.cardname    matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or
          (ratecard.description matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")))   
            by rateCard.region by rateCard.cardName.
         brwRateCard:set-repositioned-row(13,"ALWAYS") no-error.      
         reposition brwRateCard to rowid reposRowID no-error.
       end.  
     end case.
   
     apply 'value-changed' to brwRateCard in frame DEFAULT-FRAME.
     run refreshstatusbar in this-procedure no-error.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshStatusBar C-Win 
PROCEDURE refreshStatusBar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer ttRateCard for ratecard. 
 std-in = 0.
 do with frame {&frame-name}:
 end.
 for each ttRateCard no-lock where
  (ttRateCard.region      matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or 
  (ttRateCard.cardname    matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*"))) or
  (ttRateCard.description matches (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")))   
  by ttRateCard.region by ttRateCard.cardname:
   std-in = std-in + 1.
 end.

 std-ch = string(std-in) + " " + (if std-in = 1 then "record found" else "record(s) found") + " as of " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss").
 status default std-ch in window {&window-name}.
 status input   std-ch in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pName as char.

{&pre-code}

hQueryHandle = browse {&browse-name}:query.
hQueryHandle:query-close().

browse {&browse-name}:clear-sort-arrows().

if pName = dataSortBy 
  then dataSortDesc = not dataSortDesc.

do with frame {&frame-name}:
end.
{&pre-querystring}

tQueryString = "preselect each ratecard no-lock where " + 
                "rateCard.cardname    matches " + '"' + (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")) +  '"' + " or " +
                    "rateCard.region    matches " + '"' + (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")) +  '"' + " or " +
                        "rateCard.description    matches " + '"' + (if trim(cLastSearchString) = "" then "*" else ("*" + trim(cLastSearchString) + "*")) +  '"' + 
               (if pName <> ""
                then " by " + pName + (if dataSortDesc then " descending" else "")
                else "")
               {&post-by-clause}.

{&post-querystring}

do std-in = 1 to browse {&browse-name}:num-columns:
  std-ha = browse {&browse-name}:get-browse-column(std-in).
  if std-ha:name = pName 
    then browse {&browse-name}:set-sort-arrow(std-in, not dataSortDesc).
end.
dataSortBy = pName.

&if defined(ExcludeOpenQuery) = 0 &then 
hQueryHandle:query-prepare(tQueryString).
hQueryHandle:query-open().  
&endif

apply 'entry' to browse {&browse-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewWindow C-Win 
PROCEDURE viewWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply 'entry' to btNew in frame DEFAULT-FRAME.

  if {&window-name}:window-state eq window-minimized  then
     {&window-name}:window-state = window-normal .
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

