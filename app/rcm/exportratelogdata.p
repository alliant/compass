&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* exportratelogdata.p

   EXPORT a temp-TABLE and string content to csv and launch associated program to view the results
   Limited to tables with 100 fields (or max 100 fields defined for export)
   
   INPUT
   -----
   pTableHandle Handle of temp-table (pass parameter as:  table-handle <var>
   pBufferName  Name to be assigned to the table (referenced in the predicate
   pPredicate   Query clause such as "for each customer "
                The named buffer needs to be the same as the pBufferName parameter
   pFieldNames  Comma-delimited list of fields from buffer to export
                The order listed is the order exported.
                If blank, all fields are exported.
   pFieldLabels Comma-delimited list of labels exported in first row
                (blank = none)
   pDirectory   Default directory when requesting filename
   pFilename    If set, must be .csv and does not prompt user for filename.
                If blank, user is prompted to select/enter filename.
   pOpen        True = launch associated application linked to .csv files
   pLabel       Label for Input string content to be exported to csv file
   pInfo        Input string content to be exported to csv file
   
   OUTPUT
   ------     
   pMsg         Non-blank output = error condition.
   pRecordCount Number of rows exported.
   
   */
{lib/std-def.i}
{lib/winlaunch.i}

/* Parameters Definitions ---                                           */
define input parameter table-handle xTable.
define input parameter  pBufferName  as character no-undo.
define input parameter  pPredicate   as character no-undo.
define input parameter  pFieldNames  as character no-undo.
define input parameter  pFieldLabels as character no-undo.
                        
define input parameter  pDirectory   as character no-undo.
define input parameter  pFilename    as character no-undo.
define input parameter  pOpen        as logical   no-undo.
define input parameter  pLabel       as character no-undo.
define input parameter  pInfo        as character no-undo.
 
define output parameter pMsg         as character no-undo.
define output parameter pRecordCount as integer   no-undo.
  
/* Local Variable Definitions ---                                       */
define variable bh as handle no-undo.
define variable qh as handle no-undo.
define variable fh as handle no-undo.
 
define variable fieldData as character no-undo extent 100.
define variable fieldCnt  as integer   no-undo.

create widget-pool.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&IF DEFINED(EXCLUDE-closeQuery) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD closeQuery Procedure 
FUNCTION closeQuery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-exportRateLogData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD exportRateLogData Procedure 
FUNCTION exportRateLogData RETURNS INTEGER PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getFilename Procedure 
FUNCTION getFilename RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-isNumeric) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD isNumeric Procedure 
FUNCTION isNumeric RETURNS LOGICAL
  ( input pValue as character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-openFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openFile Procedure 
FUNCTION openFile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-openQuery) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openQuery Procedure 
FUNCTION openQuery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-validateParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD validateParameters Procedure 
FUNCTION validateParameters RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

if not validateParameters() 
 then return.

if not openQuery() 
 then return.

if not getFilename() 
 then
  do: closeQuery().
      return.
  end.

pRecordCount = exportRateLogData().

closeQuery().

if pOpen
 then openFile().

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Implementations ***************** */

&IF DEFINED(EXCLUDE-closeQuery) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION closeQuery Procedure 
FUNCTION closeQuery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 qh:query-close.
 
 delete object qh.
 delete object bh.
 
 return true.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-exportRateLogData) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION exportRateLogData Procedure 
FUNCTION exportRateLogData RETURNS INTEGER PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 define variable pCnt   as integer            no-undo.
 define variable tmpvar as character extent 2 no-undo.
 define variable icnt   as integer            no-undo.
 
 output to value(pFilename) page-size 0.

 if pLabel > "" then
   put unformatted pLabel skip.

 if pInfo > "" then
 do icnt = 1 to num-entries(pinfo,chr(10)):

   assign tmpvar[1] = entry(1,entry(icnt,pinfo,chr(10)),"=")
          tmpvar[2] = entry(2,entry(icnt,pinfo,chr(10)),"=").

   export delimiter "," tmpvar[1]  tmpvar[2].

 end.

 put unformatted skip(1).  /* Added a blank line in string content and content exported from temp-table. */


 if pFieldLabels > ""
  then put unformatted pFieldLabels skip.
 
 EXPORT-BLOCK:
 repeat:
   qh:get-next.
   if qh:query-off-end 
    then leave EXPORT-BLOCK.
   
   fieldCnt = 1.
   /* Capture field values in common array variable */
   CAPTURE-BLOCK:
   do std-in = 1 to bh:num-fields:
     fh = bh:buffer-field(std-in).
     
     if pFieldNames > "" /* blank for all */
      then
       do: fieldCnt = lookup(fh:name, pFieldNames).
           if fieldCnt = 0 
            then next. /* Not in export field list */
       end.
     
     if fh:data-type = "datetime" then
      fieldData[fieldCnt] = string(fh:buffer-value,"99/99/9999").
     else
     /* Some fields (like AgentID) can contain leading zeros or can contain all 
        numeric data in a character field.  In order to preserve the leading 
        zeros and maintain left justification for all numeric character data 
        when the .CSV is opened in Excel, the value must be represented as a 
        string formula:  ="string value" */
      if fh:data-type = "character" and isNumeric(fh:buffer-value) then
       fieldData[fieldCnt] = '="' + string(fh:buffer-value) + '"'.  
     else 
      fieldData[fieldCnt] = string(fh:buffer-value).
     
     /* Convert any nulls or question marks to blank or zero */
     if fieldData[fieldCnt] = ? or fieldData[fieldCnt] = "?" then
     do:
       if fh:data-type begins "int" or fh:data-type begins "dec" 
        then fieldData[fieldCnt] = "0".
       else fieldData[fieldCnt] = "".
     end.
     
     if pFieldNames > ""
      then next. /* Sequence is handled above */
     
     /* Output in sequence */
     fieldCnt = fieldCnt + 1.
     if fieldCnt > extent(fieldData) 
      then leave CAPTURE-BLOCK.
   end.
   
   export delimiter ","
     fieldData.
   pCnt = pCnt + 1.
 end.
 output close.
 
 publish "AddTempFile" ("ExportData: " + pFilename, pFilename).

 return pCnt.   /* Function return value. */
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-getFilename) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getFilename Procedure 
FUNCTION getFilename RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 define variable doSave as logical no-undo.

 if pFilename > "" 
  then return true.

 system-dialog get-file pFilename
  filters "CSV Files" "*.csv"
  initial-dir pDirectory
  ask-overwrite
  create-test-file
  default-extension ".csv"
  use-filename
  save-as
 update doSave.

 if not doSave 
  then 
   do: pMsg = "Cancelled by user".
       return false.
   end.

 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-isNumeric) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION isNumeric Procedure 
FUNCTION isNumeric RETURNS LOGICAL
  ( input pValue as character ) :

  define variable iLength as integer no-undo.

  do iLength = 1 to length(pValue):
    if index("0123456789.,", substring(pValue,iLength,1)) = 0 then
     return false.
  end.

  return true.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-openFile) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openFile Procedure 
FUNCTION openFile RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if search(pFilename) = ? 
  then
   do: pMsg = "Export file was not created".
       return false.
   end.

 run ShellExecuteA in this-procedure (0,
                             "open",
                             pFilename,
                             "",
                             "",
                             1,
                             OUTPUT std-in).

 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-openQuery) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openQuery Procedure 
FUNCTION openQuery RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 create buffer bh for table xTable buffer-name pBufferName no-error.
 if error-status:error 
  then
   do: pMsg = "Export configuration error: Invalid table".
       return false.
   end.
 
 create query qh.
 
 qh:set-buffers(bh).
 std-lo = qh:query-prepare(pPredicate) no-error.
 if not std-lo 
  then
   do: pMsg = "Export configuration error: Invalid query".
       delete object qh.
       delete object bh.
       return false.
   end.
 
 std-lo = qh:query-open no-error.
 if not std-lo 
  then
   do: pMsg = "Export configuration error: Problematic query".
       delete object qh.
       delete object bh.
       return false.
   end.

 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-validateParameters) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION validateParameters Procedure 
FUNCTION validateParameters RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 if pBufferName = "" 
  or index(pPredicate, " " + pBufferName) = 0
   then
    do: pMsg = "Export configuration error: Table undefined".
        return false.
    end.
 
 if pFieldLabels = "table-field-names" 
  then pFieldLabels = pFieldNames.
 
 if pFieldLabels > ""
    and num-entries(pFieldLabels) <> num-entries(pFieldNames) 
  then
   do: pMsg = "Export configuration error: Label and Field count mismatch".
       return false.
   end.
 
 if pFilename > "" and index(pFilename, ".csv") = 0 
  then
   do: pMsg = "Export configuration error: Filename is not a CSV".
       return false.
   end.

 return true.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

