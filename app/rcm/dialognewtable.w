&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: rcm/dialognewtable.w

  Description: creates new record for step 1 of card setup

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
/*  Temp-table definitions                                             */
{tt\rateTable.i &tablealias=tempRateTable}

/* Parameters definitions ---                                           */
define input parameter pCardName   as character no-undo.
define input parameter pRowNo      as integer   no-undo.
define input parameter pLastMaxAmt as decimal   no-undo.
define output parameter  table for tempRateTable.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fMaxAmount tStack fRoundTo fCalcBy fRate ~
fPer fFixed eComments Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS fCardName fRow fMinAmount fMaxAmount ~
tStack fRoundTo fCalcBy fRate fPer fFixed eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 78.6 BY 2.05 NO-UNDO.

DEFINE VARIABLE fCardName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Card Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 62.2 BY 1 NO-UNDO.

DEFINE VARIABLE fFixed AS DECIMAL FORMAT "z,zz9.99":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 12.8 BY 1 NO-UNDO.

DEFINE VARIABLE fMaxAmount AS DECIMAL FORMAT "zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Maximum" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 TOOLTIP "Enter the upper limit of the coverage range" NO-UNDO.

DEFINE VARIABLE fMinAmount AS DECIMAL FORMAT "zzz,zzz,zz9.99":U INITIAL 0 
     LABEL "Minimum" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 TOOLTIP "Enter the lower limit of coverage range" NO-UNDO.

DEFINE VARIABLE fPer AS INTEGER FORMAT "zzz,zzz":U INITIAL 0 
     LABEL "Per Unit" 
     VIEW-AS FILL-IN 
     SIZE 11.4 BY 1 NO-UNDO.

DEFINE VARIABLE fRate AS DECIMAL FORMAT "z,zz9.99999":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16.8 BY 1 NO-UNDO.

DEFINE VARIABLE fRoundTo AS INTEGER FORMAT "zzz,zzz":U INITIAL 0 
     LABEL "Round To" 
     VIEW-AS FILL-IN 
     SIZE 11.4 BY 1 TOOLTIP "Round liability up to next" NO-UNDO.

DEFINE VARIABLE fRow AS CHARACTER FORMAT "X(256)":U 
     LABEL "Row" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE fCalcBy AS CHARACTER INITIAL "R" 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Rate:", "R",
"Fixed:", "F"
     SIZE 8.8 BY 2.14 NO-UNDO.

DEFINE VARIABLE tStack AS LOGICAL INITIAL no 
     LABEL "Stack" 
     VIEW-AS TOGGLE-BOX
     SIZE 10 BY .81 TOOLTIP "Accumulate with previous range" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fCardName AT ROW 1.38 COL 12.8 COLON-ALIGNED WIDGET-ID 6 NO-TAB-STOP 
     fRow AT ROW 1.38 COL 87.2 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     fMinAmount AT ROW 2.52 COL 12.8 COLON-ALIGNED WIDGET-ID 14 NO-TAB-STOP 
     fMaxAmount AT ROW 2.52 COL 54 COLON-ALIGNED WIDGET-ID 40
     tStack AT ROW 2.52 COL 84.6 WIDGET-ID 270
     fRoundTo AT ROW 3.67 COL 12.8 COLON-ALIGNED WIDGET-ID 22
     fCalcBy AT ROW 4.81 COL 15.2 NO-LABEL WIDGET-ID 248
     fRate AT ROW 4.81 COL 22.2 COLON-ALIGNED NO-LABEL WIDGET-ID 274
     fPer AT ROW 4.81 COL 54 COLON-ALIGNED WIDGET-ID 276
     fFixed AT ROW 5.95 COL 22.2 COLON-ALIGNED NO-LABEL WIDGET-ID 272
     eComments AT ROW 7.1 COL 14.8 NO-LABEL WIDGET-ID 36
     Btn_OK AT ROW 9.52 COL 32
     Btn_Cancel AT ROW 9.52 COL 49
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 6.95 COL 3.8 WIDGET-ID 38
     "Calculation:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 5.05 COL 3.2 WIDGET-ID 266
     SPACE(80.59) SKIP(5.18)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add Rate Table"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN fCardName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fMinAmount IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRow IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Add Rate Table */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  run saveTable in this-procedure no-error.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fCalcBy
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fCalcBy Dialog-Frame
ON VALUE-CHANGED OF fCalcBy IN FRAME Dialog-Frame
DO:
 if fCalcBy:screen-value = "R" 
   then
    assign
      fRate :screen-value  = "0.00"
      fRate :visible       = true
      fFixed:screen-value  = "0.00"
      fFixed:visible       = false
      fPer  :screen-value  = "0"
      fPer  :visible       = true.

  else if fCalcBy:screen-value = "F" 
   then
    assign
      fFixed:screen-value = "0.00"
      fFixed:visible      = true
      fRate :screen-value = "0.00"
      fRate :visible      = false
      fPer  :screen-value = "0"
      fPer  :visible      = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  assign
    fCardName :screen-value  = if pCardName = "?" or pCardName = ? then "" else  pCardName
    fRow      :screen-value  = string(pRowNo)
    fMinAmount:screen-value  = string(pLastMaxAmt) .
  apply 'value-changed' to fCalcBy.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fCardName fRow fMinAmount fMaxAmount tStack fRoundTo fCalcBy fRate 
          fPer fFixed eComments 
      WITH FRAME Dialog-Frame.
  ENABLE fMaxAmount tStack fRoundTo fCalcBy fRate fPer fFixed eComments Btn_OK 
         Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveTable Dialog-Frame 
PROCEDURE saveTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.

  do with frame Dialog-Frame:
  end.

  run validateData in this-procedure (output lSuccess,
                                      output cMsg) no-error.
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons ok.
      return no-apply.
    end.

  else
  do:
    Btn_OK:auto-go = true.
    create tempRateTable.
    assign    
      tempRateTable.seq           =  int(fRow:screen-value)  
      tempRateTable.minAmount     =  decimal(fMinAmount:screen-value)             
      tempRateTable.maxAmount     =  decimal(fMaxAmount:screen-value)     
      tempRateTable.roundTo       =  decimal(fRoundTo:screen-value)              
      tempRateTable.calcType      =  fCalcBy:screen-value          
      tempRateTable.stack         =  tStack:checked        
      tempRateTable.rate          =  decimal(fRate:screen-value) 
      tempRateTable.per           =  decimal(fPer:screen-value)                
      tempRateTable.fixed         =  decimal(fFixed:screen-value) 
      tempRateTable.comments      =  eComments:screen-value  .           
      .
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateData Dialog-Frame 
PROCEDURE validateData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define output parameter oplSuccess as logical   no-undo.
 define output parameter opcMsg     as character no-undo.

 do with frame Dialog-Frame:
 end.

 if fMaxAmount:screen-value = "" or fMaxAmount:screen-value = "0.00"  
  then
   do:
     opcMsg = "Enter maximum amount.".
     oplSuccess = no.
     return .
   end.
 else if decimal(fMaxAmount:screen-value) < decimal(fMinAmount:screen-value) 
  then
   do:
     opcMsg = "Maximum amount should be greater than minimum amount.".
     oplSuccess = no.
     return .
   end.
 
 if fRoundTo:screen-value = ""  
  then
   do:
     opcMsg = "Enter round to value.".
     oplSuccess = no.
     return .
   end.
 if fCalcBy:screen-value = "R"  
  then
   do:
     if fRate:screen-value = "" or fRate:screen-value = "0.00000"  
      then
       do:
         opcMsg = "Enter rate.".
         oplSuccess = no.
         return .
       end.
     if fPer:screen-value = ""  
      then
       do:
         opcMsg = "Enter per unit value.".
         oplSuccess = no.
         return .
       end.
   end.

 oplSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

