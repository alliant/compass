&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: rcm/dialognewstate.w

  Description: Creates new rateState record

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 
  Modified    :
   Date        Name     Comments
   07/01/2019  Sachin   Modified for standardisation.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/*  Temp-table definitions                                             */
{tt\ratestate.i}
{tt\ratestate.i &tablealias=tempRateState}

/* Parameters definitions ---                                           */
define input  parameter pStateID    as character no-undo.
define input  parameter pVersion    as integer   no-undo.
define input  parameter pCardSetID  as integer   no-undo.
define input  parameter pTitle      as character no-undo.
define input  parameter table for ratestate.
define output parameter lSuccess  as logical     no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS eVersionDesc cbFillingMethod fEffectiveDate ~
fApprovedBy fDateFilled fDateApproved fDateStateApproved fConfigUI fExecDM ~
fExecUI fExecBL eComments Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS eVersionDesc cbFillingMethod ~
fEffectiveDate fApprovedBy fDateFilled fDateApproved fDateStateApproved ~
fConfigUI fExecDM fExecUI fExecBL eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbFillingMethod AS CHARACTER FORMAT "X(256)":U INITIAL "Manual" 
     LABEL "Filing Method" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Electronic","Manual" 
     DROP-DOWN-LIST
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 63.4 BY 2.71 NO-UNDO.

DEFINE VARIABLE eVersionDesc AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 63.4 BY 1.91 NO-UNDO.

DEFINE VARIABLE fApprovedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Approved By" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fConfigUI AS CHARACTER FORMAT "X(100)":U 
     LABEL "UI Configuration File" 
     VIEW-AS FILL-IN 
     SIZE 63.4 BY 1 NO-UNDO.

DEFINE VARIABLE fDateApproved AS DATE FORMAT "99/99/99":U 
     LABEL "Approved" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDateFilled AS DATE FORMAT "99/99/99":U 
     LABEL "Filed" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDateStateApproved AS DATE FORMAT "99/99/99":U 
     LABEL "State Approved" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fEffectiveDate AS DATE FORMAT "99/99/99":U 
     LABEL "Manual Effective" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fExecBL AS CHARACTER FORMAT "X(100)":U 
     LABEL "Business Layer Program" 
     VIEW-AS FILL-IN 
     SIZE 63.4 BY 1 NO-UNDO.

DEFINE VARIABLE fExecDM AS CHARACTER FORMAT "X(100)":U 
     LABEL "Data Model Program" 
     VIEW-AS FILL-IN 
     SIZE 63.4 BY 1 NO-UNDO.

DEFINE VARIABLE fExecUI AS CHARACTER FORMAT "X(100)":U 
     LABEL "User Interface Program" 
     VIEW-AS FILL-IN 
     SIZE 63.4 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     eVersionDesc AT ROW 1.33 COL 26 NO-LABEL WIDGET-ID 42
     cbFillingMethod AT ROW 3.43 COL 24 COLON-ALIGNED WIDGET-ID 40
     fEffectiveDate AT ROW 3.43 COL 69.6 COLON-ALIGNED WIDGET-ID 14
     fApprovedBy AT ROW 4.62 COL 24 COLON-ALIGNED WIDGET-ID 20
     fDateFilled AT ROW 4.62 COL 69.6 COLON-ALIGNED WIDGET-ID 18
     fDateApproved AT ROW 5.81 COL 24 COLON-ALIGNED WIDGET-ID 46
     fDateStateApproved AT ROW 5.81 COL 69.6 COLON-ALIGNED WIDGET-ID 24
     fConfigUI AT ROW 7 COL 24 COLON-ALIGNED WIDGET-ID 48
     fExecDM AT ROW 8.19 COL 24 COLON-ALIGNED WIDGET-ID 52
     fExecUI AT ROW 9.38 COL 24 COLON-ALIGNED WIDGET-ID 54
     fExecBL AT ROW 10.57 COL 24 COLON-ALIGNED WIDGET-ID 50
     eComments AT ROW 11.76 COL 26 NO-LABEL WIDGET-ID 36
     Btn_OK AT ROW 14.76 COL 30.2
     Btn_Cancel AT ROW 14.76 COL 47.2
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 11.91 COL 15.2 WIDGET-ID 38
     "Version:" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 1.62 COL 18 WIDGET-ID 44
     SPACE(63.99) SKIP(13.75)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add New Version"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Add New Version */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  create tempratestate.
  assign
    tempratestate.version             =  pVersion                              
    tempratestate.stateid             =  pStateID                              
    tempratestate.cardsetid           =  pCardSetID                            
    tempratestate.description         =  eVersionDesc:screen-value             
    tempratestate.effectiveDate       =  date(fEffectiveDate:screen-value)     
    tempratestate.approvedBy          =  fApprovedBy:screen-value
    tempratestate.dateApproved        =  date(fDateApproved:screen-value)
    tempratestate.filingMethod        =  cbFillingMethod:screen-value          
    tempratestate.dateFiled           =  date(fDateFilled:screen-value)        
    tempratestate.dateStateApproved   =  date(fDateStateApproved:screen-value) 
    tempratestate.comment             =  eComments:screen-value           
    tempratestate.active              =  false
    tempratestate.execBusinessLogic   =  fExecBL:screen-value
    tempratestate.configUserInterface =  fConfigUI:screen-value
    tempratestate.execDataModel       =  fExecDM:screen-value
    tempratestate.execUserInterface   =  fExecUI:screen-value
    . 

  if pTitle = "Edit" then 
  do:
    find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
    if available ratestate then
    do:
      assign tempratestate.active =  ratestate.active. 
      publish "ModifyRateState"(input table tempratestate,
                                output lSuccess).
    end.
  end.
  else
    publish "AddRateState"(input table tempratestate,
                           output lSuccess).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eVersionDesc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eVersionDesc Dialog-Frame
ON VALUE-CHANGED OF eVersionDesc IN FRAME Dialog-Frame
DO:
  run enableOk in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
  if available ratestate 
   then
    do:
      assign
        eVersionDesc      :screen-value  = if ratestate.description = ? or ratestate.description = "?"   then "" else string(ratestate.description)                
        fEffectiveDate    :screen-value  = if ratestate.effectiveDate = ?                                then "" else string(ratestate.effectiveDate)                 
        fApprovedBy       :screen-value  = if ratestate.approvedBy = ? or ratestate.approvedBy = "?"     then "" else string(rateState.approvedBy)                   
        cbFillingMethod   :screen-value  = if ratestate.filingMethod = ? or ratestate.filingMethod = "?" then "" else rateState.filingMethod                     
        fDateFilled       :screen-value  = if ratestate.dateFiled = ?                                    then "" else string(rateState.dateFiled)                  
        fDateApproved     :screen-value  = if ratestate.dateApproved = ?                                 then "" else string(rateState.dateApproved) 
        fDateStateApproved:screen-value  = if ratestate.dateStateApproved = ?                            then "" else string(rateState.dateStateApproved)  
        eComments         :screen-value  = if ratestate.comment = ? or ratestate.comment = "?"           then "" else rateState.comment   
        fExecBL           :screen-value  = if ratestate.execBusinessLogic = ? or ratestate.execBusinessLogic = "?"      then "" else rateState.execBusinessLogic
        fConfigUI         :screen-value  = if ratestate.configUserInterface = ? or ratestate.configUserInterface = "?"  then "" else rateState.configUserInterface
        fExecDM           :screen-value  = if ratestate.execDataModel = ? or ratestate.execDataModel = "?"              then "" else rateState.execDataModel
        fExecUI           :screen-value  = if ratestate.execUserInterface = ? or ratestate.execUserInterface = "?"      then "" else rateState.execUserInterface
        .
      frame Dialog-Frame:title = ( if pTitle = "Edit" then pTitle + " Version - " + string(eVersionDesc:screen-value) else pTitle).
    end.

  apply "value-changed":U to eVersionDesc.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.                                         
END.                                                                                       
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableOk Dialog-Frame 
PROCEDURE enableOk :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if (eVersionDesc:screen-value in frame Dialog-Frame ne "") then
   enable btn_OK with frame Dialog-Frame.
 else
   disable btn_OK with frame Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY eVersionDesc cbFillingMethod fEffectiveDate fApprovedBy fDateFilled 
          fDateApproved fDateStateApproved fConfigUI fExecDM fExecUI fExecBL 
          eComments 
      WITH FRAME Dialog-Frame.
  ENABLE eVersionDesc cbFillingMethod fEffectiveDate fApprovedBy fDateFilled 
         fDateApproved fDateStateApproved fConfigUI fExecDM fExecUI fExecBL 
         eComments Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

