&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Archana and Anjly

  Created: 
  
  Modified    :
   Date        Name     Comments
   07/01/2019  Sachin   Modified as per UAT comments.
   02/07/2022  Vignesh  Added Logic for error handling.

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{lib\rcm-std-def.i}
{lib\std-def.i}
{lib\winlaunch.i}

/*Temp table to contain endorsement data*/
{tt\rateui.i}

/*Temp tables for log*/
{tt\ratelog.i}

/*Temp tables to handle open log windows*/
define temp-table openLog
    field hInstance as handle.


/* variables used in screen resizing*/
define variable endorsPos              as decimal                  no-undo.
define variable resultsPos             as decimal                  no-undo.
define variable fullheight             as decimal                  no-undo.
define variable cutwidth               as decimal                  no-undo.
define variable fullwidth              as decimal                  no-undo.
define variable cutheight              as decimal                  no-undo.
define variable endSelected            as logical   initial false  no-undo. 
define variable multiLoanSelected      as logical   initial false  no-undo.

/*variable containing key value pair for ui input and output premium*/
define variable pcCalculatedPremium    as character                no-undo.
define variable pcInputValues          as character                no-undo.
define variable pcInputValuesPDF       as character                no-undo.

/* output parameter parsing variables */
define variable effectiveDate          as character                no-undo.
define variable premiumOwner           as character                no-undo.
define variable premiumSimo            as character                no-undo.
define variable premiumLoan            as character                no-undo.
define variable premiumBasic           as character                no-undo.
define variable premiumScndLoan        as character                no-undo.
define variable premiumCPL             as character                no-undo.
define variable success                as character                no-undo.
define variable serverErrMsg           as character                no-undo.
define variable cardSetID              as character                no-undo.

define variable ratesCode              as character                no-undo.
define variable ratesMsg               as character                no-undo.

/*type cased variables after parsing output parameter*/
define variable lSuccess               as logical   initial false  no-undo.

/* variables to populate propertytypecombobox */
define variable pcPropertyType         as character                no-undo.

/* variables to handle dynamic widget of endorsements*/
define variable iEndCount              as integer                  no-undo.
define variable iNextEndors            as integer                  no-undo.
define variable flag                   as logical   initial true   no-undo.

/*variable to handle second loan section*/
define variable secondLoanEndorsLoaded as logical   initial false  no-undo. 
define variable secondLoanEndorsList   as logical   initial false  no-undo. 

/*variable to handle simo case*/
define variable tempcoverageAmount     as character                no-undo.
define variable tempLoanAmount         as character                no-undo.


/*variable to launch calculator for multiple versions*/
define variable versionNo              as character                no-undo.

define variable logscreenparameter     as character                no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btSecondLoan 
     LABEL " >" 
     SIZE 4.8 BY 1.13 TOOLTIP "Show".

DEFINE VARIABLE cbLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 37.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbPropType AS CHARACTER FORMAT "X(256)":U INITIAL "R" 
     LABEL "Property Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 38.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 38.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiCoverageAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Coverage Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 3.91.

DEFINE RECTANGLE RECT-60
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 3.91.

DEFINE VARIABLE tbSimultaneous AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.6 BY .57 NO-UNDO.

DEFINE BUTTON btCalculate 
     LABEL "&Calculate" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Calculate(Alt + C)".

DEFINE BUTTON btClear 
     LABEL "Clear&x" 
     SIZE 7.2 BY 1.7 TOOLTIP "Clear(Alt + X)".

DEFINE BUTTON btlog 
     LABEL "Show Ca&lculations" 
     SIZE 7.2 BY 1.7 TOOLTIP "Show Ca&lculations(Alt + L)".

DEFINE BUTTON btRateSheet 
     LABEL "&PDF" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Print Rates(Alt + P)".

DEFINE VARIABLE fiCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "CPL" 
     VIEW-AS FILL-IN 
     SIZE 6.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiCPLTotal AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "CPL Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE figrandTotal AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Grand Total" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderPolicyPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerPolicyPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 1.91.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 1.91.

DEFINE RECTANGLE RECT-71
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 2.13.

DEFINE BUTTON btScndClear 
     LABEL "SClear" 
     SIZE 4.8 BY 1.13 TOOLTIP "Clear Second Loan to enable hide".

DEFINE VARIABLE cbSecondLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 30 BY 1 NO-UNDO.

DEFINE VARIABLE fiSecondLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 3.91.

DEFINE VARIABLE fiScndLenderPolicyPremium AS DECIMAL FORMAT "zz,zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 19.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 74.8 BY 1.91.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 225.2 BY 11 WIDGET-ID 100.

DEFINE FRAME frScndLoan
     btScndClear AT ROW 2.65 COL 70.4 WIDGET-ID 176 NO-TAB-STOP 
     fiSecondLoanAmt AT ROW 3.13 COL 33 COLON-ALIGNED WIDGET-ID 168
     cbSecondLoanRateType AT ROW 4.26 COL 33 COLON-ALIGNED WIDGET-ID 170
     "Second Loan" VIEW-AS TEXT
          SIZE 15.4 BY .61 AT ROW 2.26 COL 2.6 WIDGET-ID 174
          FONT 6
     RECT-61 AT ROW 2.57 COL 1 WIDGET-ID 172
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 150.8 ROW 1
         SIZE 75.2 BY 5.48 WIDGET-ID 800.

DEFINE FRAME frBasicResults
     fiCPL AT ROW 4.17 COL 13.4 COLON-ALIGNED WIDGET-ID 154
     btCalculate AT ROW 3.87 COL 122.2 WIDGET-ID 34
     btClear AT ROW 3.87 COL 129.4 WIDGET-ID 104
     btRateSheet AT ROW 3.87 COL 136.6 WIDGET-ID 152
     btlog AT ROW 3.87 COL 143.8 WIDGET-ID 146
     fiLenderPolicyPremium AT ROW 1.44 COL 100 COLON-ALIGNED WIDGET-ID 36 NO-TAB-STOP 
     fiOwnerPolicyPremium AT ROW 1.48 COL 28.4 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     fiCPLTotal AT ROW 4.17 COL 51 COLON-ALIGNED WIDGET-ID 158 NO-TAB-STOP 
     figrandTotal AT ROW 4.17 COL 100 COLON-ALIGNED WIDGET-ID 108 NO-TAB-STOP 
     "Closing Protection Letters" VIEW-AS TEXT
          SIZE 29 BY .61 AT ROW 3.35 COL 3.6 WIDGET-ID 58
          FONT 6
     RECT-58 AT ROW 1 COL 2 WIDGET-ID 2
     RECT-62 AT ROW 1 COL 76.4 WIDGET-ID 148
     RECT-71 AT ROW 3.65 COL 2 WIDGET-ID 156
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 6.38
         SIZE 150.6 BY 5.57
         DEFAULT-BUTTON btCalculate WIDGET-ID 400.

DEFINE FRAME frBasic
     cbPropType AT ROW 1.3 COL 28.4 COLON-ALIGNED WIDGET-ID 60
     fiCoverageAmt AT ROW 3.13 COL 28.4 COLON-ALIGNED WIDGET-ID 16
     cbRateType AT ROW 4.26 COL 28.4 COLON-ALIGNED WIDGET-ID 12
     fiLoanAmt AT ROW 3.13 COL 100 COLON-ALIGNED WIDGET-ID 46
     tbSimultaneous AT ROW 3.39 COL 121.6 WIDGET-ID 116
     cbLoanRateType AT ROW 4.26 COL 100 COLON-ALIGNED WIDGET-ID 44
     fiVersion AT ROW 1.26 COL 141.8 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     btSecondLoan AT ROW 5.17 COL 145.8 WIDGET-ID 158
     "Second Loan" VIEW-AS TEXT
          SIZE 15.4 BY .57 AT ROW 5.39 COL 130.4 WIDGET-ID 160
          FONT 6
     "Loan" VIEW-AS TEXT
          SIZE 6 BY .61 AT ROW 2.26 COL 78 WIDGET-ID 66
          FONT 6
     "Owner" VIEW-AS TEXT
          SIZE 7.4 BY .61 AT ROW 2.26 COL 3.6 WIDGET-ID 58
          FONT 6
     "Simultaneous" VIEW-AS TEXT
          SIZE 13 BY .61 AT ROW 3.35 COL 124.4 WIDGET-ID 162
     RECT-56 AT ROW 2.57 COL 2 WIDGET-ID 2
     RECT-60 AT ROW 2.57 COL 76.4 WIDGET-ID 64
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 150.6 BY 5.48 WIDGET-ID 200.

DEFINE FRAME frScndLoanResults
     fiScndLenderPolicyPremium AT ROW 1.44 COL 33 COLON-ALIGNED WIDGET-ID 162 NO-TAB-STOP 
     RECT-63 AT ROW 1 COL 1 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 150.8 ROW 6.38
         SIZE 75.2 BY 3.91 WIDGET-ID 1300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Georgia Rate Calculator"
         HEIGHT             = 10.91
         WIDTH              = 225
         MAX-HEIGHT         = 33.61
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.61
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frBasic:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicResults:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frScndLoan:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frScndLoanResults:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME frBasicResults:MOVE-BEFORE-TAB-ITEM (FRAME frScndLoanResults:HANDLE)
       XXTABVALXX = FRAME frScndLoan:MOVE-BEFORE-TAB-ITEM (FRAME frBasicResults:HANDLE)
       XXTABVALXX = FRAME frBasic:MOVE-BEFORE-TAB-ITEM (FRAME frScndLoan:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasic
   Custom                                                               */
/* SETTINGS FOR BUTTON btSecondLoan IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiVersion IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tbSimultaneous IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frBasicResults
   NOT-VISIBLE Custom                                                   */
/* SETTINGS FOR BUTTON btlog IN FRAME frBasicResults
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRateSheet IN FRAME frBasicResults
   NO-ENABLE                                                            */
ASSIGN 
       fiCPLTotal:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       figrandTotal:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

/* SETTINGS FOR FRAME frScndLoan
                                                                        */
/* SETTINGS FOR BUTTON btScndClear IN FRAME frScndLoan
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frScndLoanResults
                                                                        */
ASSIGN 
       fiScndLenderPolicyPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasic
/* Query rebuild information for FRAME frBasic
     _Query            is NOT OPENED
*/  /* FRAME frBasic */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicResults
/* Query rebuild information for FRAME frBasicResults
     _Query            is NOT OPENED
*/  /* FRAME frBasicResults */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frScndLoanResults
/* Query rebuild information for FRAME frScndLoanResults
     _Query            is NOT OPENED
*/  /* FRAME frScndLoanResults */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Georgia Rate Calculator */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Georgia Rate Calculator */
DO:
  /* This event will close the window and terminate the procedure.  */
  for each openLog:
    if valid-handle(openLog.hInstance) then
      delete object openLog.hInstance.
  end.
  empty temp-table openLog.
  run ClearAll.
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btCalculate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCalculate C-Win
ON CHOOSE OF btCalculate IN FRAME frBasicResults /* Calculate */
DO:
  run calculatePremium in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME frBasicResults /* Clearx */
DO:
  run clearAll in this-procedure no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btlog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btlog C-Win
ON CHOOSE OF btlog IN FRAME frBasicResults /* Show Calculations */
DO:

  create openLog.
  run wlogs.w persistent set openLog.hInstance (input table rateLog,
                                                input logscreenparameter) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRateSheet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRateSheet C-Win
ON CHOOSE OF btRateSheet IN FRAME frBasicResults /* PDF */
DO:
  run GeneratePDF.w ("Georgia",
                     replace (pcCalculatedPremium,"=","^") ,
                     pcInputValuesPDF) no-error.       
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME btScndClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btScndClear C-Win
ON CHOOSE OF btScndClear IN FRAME frScndLoan /* SClear */
DO:
   run ClearSecondLoanSection in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME btSecondLoan
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSecondLoan C-Win
ON CHOOSE OF btSecondLoan IN FRAME frBasic /*  > */
DO:
  run setPosition in this-procedure no-error.
  if multiLoanSelected = false 
   then
    do:
       assign
            multiLoanSelected                     = true
            btSecondLoan:tooltip in frame frBasic = "Show"
            .
         btSecondLoan:load-image("images/s-next.bmp")               in frame frBasic no-error.
         btSecondLoan:load-image-insensitive("images/s-next-i.bmp") in frame frBasic no-error.

    end.                                                                        
   else                                                                        
    do:
       assign
            multiLoanSelected                     = false
            btSecondLoan:tooltip in frame frBasic = "Hide"
            .
       btSecondLoan:load-image-insensitive("images/s-previous-i.bmp") in frame frBasic no-error.
       btSecondLoan:load-image("images/s-previous.bmp")               in frame frBasic no-error.
       run ClearSecondLoanSection in this-procedure no-error.
    end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbLoanRateType C-Win
ON VALUE-CHANGED OF cbLoanRateType IN FRAME frBasic /* Rate to Apply */
DO:
  run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPropType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPropType C-Win
ON VALUE-CHANGED OF cbPropType IN FRAME frBasic /* Property Type */
DO:
  run propertyTypeValueChange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRateType C-Win
ON VALUE-CHANGED OF cbRateType IN FRAME frBasic /* Rate to Apply */
DO:
  run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME cbSecondLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSecondLoanRateType C-Win
ON VALUE-CHANGED OF cbSecondLoanRateType IN FRAME frScndLoan /* Rate to Apply */
DO:
  if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0"           or 
     cbSecondLoanRateType:screen-value in frame frScndLoan ne {&none}  or
     secondLoanEndorsList eq true 
    then
     assign
          btSecondLoan:sensitive in frame frBasic    = false
          tbSimultaneous:checked in frame frBasic    = true
          btScndClear:sensitive  in frame frScndLoan = true
          .
    else
     assign
          btSecondLoan:sensitive in frame frBasic    = true
          btScndClear:sensitive  in frame frScndLoan = false
          .
  run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME fiCoverageAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON LEAVE OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
  tempcoverageAmount = fiCoverageAmt:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON VALUE-CHANGED OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
  run coverageAmntValueChanged in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCPL C-Win
ON VALUE-CHANGED OF fiCPL IN FRAME frBasicResults /* CPL */
DO:
  run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME fiLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON LEAVE OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
  tempLoanAmount = fiLoanAmt:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON VALUE-CHANGED OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
  run loanAmntValueChanged in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME fiSecondLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSecondLoanAmt C-Win
ON VALUE-CHANGED OF fiSecondLoanAmt IN FRAME frScndLoan /* Loan Amount */
DO:
  if fiSecondLoanAmt:screen-value in frame frScndLoan ne "0"           or
     cbSecondLoanRateType:screen-value in frame frScndLoan ne {&none}  or
     secondLoanEndorsList eq true 
    then
     assign
          btSecondLoan:sensitive in frame frBasic    = false
          tbSimultaneous:checked in frame frBasic    = true
          btScndClear:sensitive  in frame frScndLoan = true
          .
    else
     assign
         btSecondLoan:sensitive in frame frBasic    = true
         btScndClear:sensitive  in frame frScndLoan = false
         .
  run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME tbSimultaneous
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbSimultaneous C-Win
ON VALUE-CHANGED OF tbSimultaneous IN FRAME frBasic
DO:

 define variable pclistOwner      as character no-undo.
 define variable pclistLoan       as character no-undo.
 define variable pclistScndLoan   as character no-undo.

 publish "GetGASimoComboList"(cbPropType:screen-value,
                              yes,
                              output pclistOwner,  
                              output pclistLoan,  
                              output pclistScndLoan).

 if tbSimultaneous:checked = true and entry(num-entries(pclistScndLoan,","), pclistScndLoan, ",") ne {&none} 
  then
   btSecondLoan:sensitive in frame frBasic = true.
  else
   do:
      if multiLoanSelected = true 
       then
        multiLoanSelected = false.
      run ClearSecondLoanSection in this-procedure no-error.
      apply 'choose' to btSecondLoan .
      btSecondLoan:sensitive in frame frBasic = false.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
/*  run georgiacalculatordata.p persistent. */

 publish "GetGAPropertyType" (output pcPropertyType).


/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

btClear     :load-image("images/erase.bmp")                 in frame frBasicResults no-error.
btClear:load-image-insensitive("images/erase-i.bmp")        in frame frBasicResults no-error.
btCalculate :load-image("images/calc.bmp")                  in frame frBasicResults no-error.
btCalculate:load-image-insensitive("images/calc-i.bmp")     in frame frBasicResults no-error.
btLog       :load-image("images/log.bmp")                   in frame frBasicResults no-error.
btLog       :load-image-insensitive("images/log-i.bmp")     in frame frBasicResults no-error.
btSecondLoan:load-image-insensitive("images/s-next-i.bmp")  in frame frBasic no-error.
btSecondLoan:load-image ("images/s-next.bmp")               in frame frBasic no-error.
btScndClear :load-image-insensitive("images/s-erase-i.bmp") in frame frScndLoan no-error.
btScndClear :load-image ("images/s-erase.bmp")              in frame frScndLoan no-error.
btRateSheet :load-image ("images/pdf.bmp")                  in frame frBasicResults no-error.
btRateSheet :load-image-insensitive("images/pdf-i.bmp")     in frame frBasicResults no-error.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
{lib/winshowscrollbars.i}
cbPropType:list-item-pairs = pcPropertyType.
cbPropType:screen-value = {&DefaultPropertyType}.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
{lib/win-main.i}
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  cbRateType:screen-value = cbRateType:entry(1).

  run ShowScrollBars(frame frBasic       :handle, no, no).
  run ShowScrollBars(frame frBasicResults:handle, no, no).

  fullwidth  = frame frBasic:width  +  frame frScndLoan:width.
  cutwidth   = frame frBasic:width  +  0.5.

  run setPosition in this-procedure no-error.

  apply 'value-changed' to cbPropType.
  apply 'value-changed' to cbRateType.
  apply 'value-changed' to cbLoanRateType.
  apply 'value-changed' to tbSimultaneous.
  apply 'leave'         to fiCoverageAmt.

  cbRateType          :screen-value = {&none}.
  cbLoanRateType      :screen-value = {&none}.
  cbSecondLoanRateType:screen-value = {&none}.

  publish "GetCurrentValue" ("GEVersion", output versionNo).

  if versionNo ne "" 
   then
    fiVersion:screen-value = versionNo.
   else 
    assign 
        fiVersion:hidden  = true
        fiVersion:visible = false
        .

  subscribe to "ApplyEntry" anywhere.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApplyEntry C-Win 
PROCEDURE ApplyEntry :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "entry" to  fiCoverageAmt in frame frBasic.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculatePremium C-Win 
PROCEDURE calculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  pcInputValues:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable errorStatus      as logical   no-undo.      
  define variable errorMsg         as character no-undo.
  define variable ownerEndors      as character no-undo.    
  define variable loanEndors       as character no-undo.    
  define variable secondLoanEndors as character no-undo.
  
  pcCalculatedPremium = "".                                                                      
  do with frame {&frame-name}:                                                
  end.  

  /*-------------------Client Validations------------------------*/
  run clientValidation in this-procedure (output errorStatus,
                                          output errorMsg) no-error.
  
  if errorStatus = yes 
   then
    do:
       message errorMsg
         view-as alert-box info buttons ok.
       btRateSheet:sensitive in frame frBasicResults = false.
       return no-apply.
    end.

  do with frame frBasic:
  end.

  /*-------------------------Input variable-------------------------------*/
  pcInputValues = "ratetype^"                   + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else cbRateType:screen-value +
                  ",coverageAmount^"            + string(int(fiCoverageAmt:screen-value))  + 
                  ",loanCoverageAmount^"        + string(int(fiLoanAmt:screen-value))      +  
                  ",loanRateType^"              + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else cbLoanRateType:screen-value +
                  ",simultaneous^"              + string(tbSimultaneous:screen-value)      +
                  ",propertyType^"              + string(cbPropType:screen-value)          +
                  ",Version^"                   + fiVersion:screen-value                   +
                  ",region^"                    + ""                                       +
                  ",CPL^"                       + string(fiCPL:screen-value).

  pcInputValuesPDF = "ratetype^"                + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else entry((lookup(cbRateType:screen-value, cbRateType:list-item-pairs,",") - 1),cbRateType:list-item-pairs, ",") +
                     ",coverageAmount^"         + string(int(fiCoverageAmt:screen-value))  + 
                     ",loanCoverageAmount^"     + string(int(fiLoanAmt:screen-value))      +  
                     ",loanRateType^"           + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else entry((lookup(cbLoanRateType:screen-value, cbLoanRateType:list-item-pairs,",") - 1),cbLoanRateType:list-item-pairs, ",") +
                     ",simultaneous^"           + string(tbSimultaneous:screen-value)      +
                     ",propertyType^"           + string(cbPropType:screen-value)          +
                     ",Version^"                + fiVersion:screen-value                   +
                     ",region^"                 + ""                                       +
                     ",CPL^"                    + string(fiCPL:screen-value)+
                     ",ratetypecode^"           + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else cbRateType:screen-value +
                     ",loanRateTypecode^"       + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else cbLoanRateType:screen-value .

  do with frame frScndLoan:
    pcInputValues = pcInputValues + 
                    ",secondloanRateType^"        + if cbSecondLoanRateType:screen-value  = ? or cbSecondLoanRateType:screen-value  = "?" then "" else cbSecondLoanRateType:screen-value +
                    ",secondloanCoverageAmount^"  + string(int(fiSecondLoanAmt:screen-value))     .

    pcInputValuesPDF = pcInputValuesPDF + 
                       ",secondloanRateType^"        + if cbSecondLoanRateType:screen-value  = ? or cbSecondLoanRateType:screen-value  = "?" then "" else   entry((lookup(cbSecondLoanRateType:screen-value, cbSecondLoanRateType:list-item-pairs,",") - 1),cbSecondLoanRateType:list-item-pairs, ",") +
                       ",secondloanCoverageAmount^"  + string(int(fiSecondLoanAmt:screen-value)) + 
                       ",secondloanRateTypecode^"    + if cbsecondLoanRateType:screen-value  = ? or cbsecondLoanRateType:screen-value  = "?" then "" else cbsecondLoanRateType:screen-value .
  end.    
  publish "GACalculatePremium" (input pcInputValues,
                                input "GA",
                                input integer(versionNo),
                                output pcCalculatedPremium,
                                output table rateLog).
  btLog:sensitive = true.

 /*--------------------Extracting output parameter and set premium on screen--------------------------------------*/ 
  run extractOutputParameters in this-procedure no-error.

  logscreenparameter =  pcInputValues + ",cardSetId^" + cardSetId
                                      + ",StateID^" + "GA"
                                      + ",manualEffectiveDate^" + (if effectiveDate eq "" then "" else string(date(effectiveDate), "99/99/99")).  
  if lSuccess = false  or serverErrMsg ne "serverErrMsg" 
   then
    do:
       message serverErrMsg
         view-as alert-box info buttons ok.
       btRateSheet:sensitive in frame frBasicResults = false.
       return no-apply.
    end.
   else if ratesCode >= '3000' and ratesMsg <> "" 
    then
     do:
       message ratesMsg
         view-as alert-box info buttons ok.
     end.           

  btRateSheet:sensitive in frame frBasicResults = true.

  assign
       fiOwnerPolicyPremium     :screen-value                            = premiumOwner
       fiLenderPolicyPremium    :screen-value                            = premiumLoan
       fiScndLenderPolicyPremium:screen-value in frame frScndLoanResults = premiumScndLoan 
       .

  fiCPLTotal  :screen-value   = premiumCPL.
  fiGrandTotal:screen-value   = string( decimal(fiOwnerPolicyPremium:screen-value) + 
                                        decimal(fiLenderPolicyPremium:screen-value) + 
                                        decimal(fiScndLenderPolicyPremium:screen-value in frame frScndLoanResults) +
                                        decimal(fiCPLTotal:screen-value)).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearAll C-Win 
PROCEDURE clearAll :
/*------------------------------------------------------------------------------
  Purpose:    reset screen for selected ratetype   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasic:
  end.

  assign 
    fiCoverageAmt  :screen-value                          = ""
    fiLoanAmt      :screen-value                          = ""
    fiSecondLoanAmt:screen-value in frame frScndLoan      = ""
    fiCPL          :screen-value in frame frBasicResults  = ""
    cbRateType     :screen-value                          = {&None}
    cbLoanRateType :screen-value                          = {&None}
    tbSimultaneous :checked                               = false
    multiLoanSelected                                     = false.
  apply 'leave'         to fiCoverageAmt.
  apply 'leave'         to fiLoanAmt.
  apply 'value-changed' to fiCoverageAmt.
  apply 'value-changed' to fiLoanAmt.
  apply 'value-changed' to tbSimultaneous.
  apply 'value-changed' to cbRateType.
  apply 'value-changed' to cbLoanRateType.
  apply 'value-changed' to cbPropType.

  pcInputValues = "".
  run clearResults in this-procedure no-error.  

  btRateSheet:sensitive in frame frBasicResults = false.
  btlog      :sensitive in frame frBasicResults = false.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearResults C-Win 
PROCEDURE clearResults :
/*------------------------------------------------------------------------------
  Purpose:     for any change in user input reset results
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame frBasicResults:
 end.
 assign
     fiOwnerPolicyPremium :screen-value   = ""
     fiLenderPolicyPremium:screen-value   = ""
     fiGrandTotal         :screen-value   = ""
     fiCPLTotal           :screen-value   = "" 
     .
 do with frame frScndLoanResults:
 end.
 assign
     fiScndLenderPolicyPremium:screen-value                      = ""
     btRateSheet              :sensitive in frame frBasicResults = false
     btlog                    :sensitive in frame frBasicResults = false
     .
 pcCalculatedPremium = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearSecondLoanSection C-Win 
PROCEDURE ClearSecondLoanSection :
/*------------------------------------------------------------------------------
  Purpose:     clear out only second loan section
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frScndLoan:
  end.
  assign 
      fiSecondLoanAmt:screen-value            = ""
      cbSecondLoanRateType:screen-value       = {&None}
      pcInputValues                           = ""
      btSecondLoan:sensitive in frame frBasic = true
      btScndClear :sensitive in frame frScndLoan = false
      .
  run clearResults.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clientValidation C-Win 
PROCEDURE clientValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
  
------------------------------------------------------------------------------*/
define output parameter errorStatus as logical   no-undo. 
define output parameter errorMsg    as character no-undo. 
 
 do with frame frBasic:
 end.
 /*---------Coverage amount is not mendatory for NA---------*/                    
 if ((fiCoverageAmt:screen-value ne "" and fiCoverageAmt:screen-value ne "0") or cbRateType:screen-value = {&NaUI}) and
     cbRateType:screen-value ne  {&none} and 
     cbLoanRateType:screen-value = {&NaUI} 
  then
   do:
      errorStatus = false.
      return.
   end.
 /*---------Loan Coverage amount is not mendatory for NA---------*/  
 if ((fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0") or cbLoanRateType:screen-value = {&NaUI}) and
     cbLoanRateType:screen-value ne  {&none} and 
     cbRateType:screen-value = {&NaUI} 
   then
    do:
       errorStatus = false.
       return.
    end.

 if fiCoverageAmt:screen-value = "" or fiCoverageAmt:screen-value = "0"
  then
   do:
      if fiLoanAmt:screen-value = "" or fiLoanAmt:screen-value = "0" 
       then
        do:     
           if (cbRateType:screen-value = {&NaUI} and cbLoanRateType:screen-value = {&none}) or 
              (cbLoanRateType:screen-value = {&NaUI} and cbRateType:screen-value = {&none}) 
            then
             do:         
                errorStatus = false.
                return.
             end.
           if (cbRateType:screen-value ne {&none} and cbRateType:screen-value ne {&NaUI}) and cbLoanRateType:screen-value ne {&none}  
            then
             do:
                errorMsg =  "Please enter coverage amount.".
                errorStatus = true.
                return.
             end.
           if (cbLoanRateType:screen-value ne {&none} and cbLoanRateType:screen-value ne {&NaUI}) 
            then
             do:         
                errorMsg =  "Please enter loan amount.".
                errorStatus = true.
                return.
             end.
           errorMsg =  "Please enter coverage amount.".
           errorStatus = true.
           return.
        end.     
       else if cbLoanRateType:screen-value ne {&none}  
             then
              do:
                 if cbRateType:screen-value ne {&none} 
                  then
                   do:
                      errorMsg =  "Please enter coverage amount.".
                      errorStatus = true.
                      return.
                   end.      
             end.
       else if cbLoanRateType:screen-value = {&none}  
        then
         do:
            errorMsg =  "Please select loan rate type.".
            errorStatus = true.
            return.
         end.  
   end.
  else if cbRateType:screen-value = {&none}  
        then
         do:
            errorMsg =  "Please select rate type.".
            errorStatus = true.
            return.
         end.
 
  else if (fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0" ) and
          (cbLoanRateType:screen-value = {&none}) 
       then
        do:
           errorMsg =  "Please select loan rate type.".
           errorStatus = true.
           return.
        end.
       
  else if (fiLoanAmt:screen-value eq "" or fiLoanAmt:screen-value eq "0" ) and
          (cbLoanRateType:screen-value ne {&none}) 
       then
        do:
           errorMsg =  "Please enter loan amount.".
           errorStatus = true.
           return.
        end. 

 do with frame frScndLoan:
 end.

 if tbSimultaneous:checked = true 
 then
  do:
     if (fiSecondLoanAmt:screen-value = "" or fiSecondLoanAmt:screen-value = "0" or fiSecondLoanAmt:screen-value = "?") and (cbSecondLoanRateType:screen-value ne {&none})  
      then
       do:
          errorMsg =  "Please enter second loan amount.".
          errorStatus = true.
          return.
       end.
     else if (cbSecondLoanRateType:screen-value = {&none}) and (fiSecondLoanAmt:screen-value ne "" and fiSecondLoanAmt:screen-value ne "0" and fiSecondLoanAmt:screen-value ne "?")  
           then
            do:
              errorMsg =  "Please select second loan rate type.".
              errorStatus = true.
              return.
            end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE coverageAmntValueChanged C-Win 
PROCEDURE coverageAmntValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     Handle simultaneous case (if user enters amount in both owner 
               and loan section then it is considered as simultaneous case
               and action to be taken
               - update ratetype list
               - handle simo checkbox
               - handle second loan button)
 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable pclistOwner               as character no-undo.
 define variable pclistLoan                as character no-undo.
 define variable pclistScndLoan            as character no-undo.
 define variable cRatetype                 as character no-undo.
 define variable cLoanRatetype             as character no-undo.
 define variable cSecondLoanRatetype       as character no-undo.
 
 do with frame frBasic:
 end.
 
 assign 
   cRatetype           = cbRateType          :screen-value
   cLoanRatetype       = cbLoanRateType      :screen-value
   cSecondLoanRatetype = cbSecondLoanRateType:screen-value in frame frScndLoan.

 if fiCoverageAmt:screen-value ne "0" and tempLoanAmount ne "" and tempLoanAmount ne "0" then
 do:
   assign
     tbSimultaneous:checked = true.
   if cbPropType:screen-value = "R" or cbPropType:screen-value = "M" then
   do:
     btSecondLoan:sensitive in frame frBasic = true.
   end.
   else
     btSecondLoan:sensitive in frame frBasic = false.

   publish "GetGASimoComboList"(cbPropType:screen-value,
                              yes,
                              output pclistOwner,  
                              output pclistLoan,  
                              output pclistScndLoan).
 end.
 else 
 do:
   assign
     tbSimultaneous:checked = false.
     btSecondLoan:sensitive in frame frBasic = false.
     multiLoanSelected = false.
     publish "GetGAComboLists"(cbPropType:screen-value,
                             output pclistOwner,  
                             output pclistLoan,  
                             output pclistScndLoan).
 end.
 run clearResults.

 apply 'value-changed' to tbSimultaneous.
 cRatetype = cbRateType:screen-value.
 assign 
   cbRateType          :list-item-pairs                     =  trim(pclistOwner, ",").
   cbLoanRateType      :list-item-pairs                     =  trim(pclistLoan, ",").
   cbSecondLoanRateType:list-item-pairs in frame frScndLoan =  trim(pclistScndLoan, ",").


 if lookup(cRatetype,cbRateType:list-item-pairs) = 0 
  then
   cbRateType:screen-value     = entry(2,cbRateType:list-item-pairs,",") no-error.
 else 
   cbRateType:screen-value     = cRatetype.

 if lookup(cLoanRatetype,cbLoanRateType:list-item-pairs) = 0 
  then
   cbLoanRateType:screen-value     = entry(2,cbLoanRateType:list-item-pairs,",") no-error.
 else
   cbLoanRateType:screen-value     = cLoanRatetype.

 if lookup(cSecondLoanRatetype,cbSecondLoanRateType:list-item-pairs) = 0 
  then
   cbSecondLoanRateType:screen-value     = entry(2,cbSecondLoanRateType:list-item-pairs,",") no-error.
 else 
   cbSecondLoanRateType:screen-value     = cSecondLoanRatetype.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  DISPLAY cbPropType fiCoverageAmt cbRateType fiLoanAmt tbSimultaneous 
          cbLoanRateType fiVersion 
      WITH FRAME frBasic IN WINDOW C-Win.
  ENABLE cbPropType fiCoverageAmt cbRateType fiLoanAmt cbLoanRateType RECT-56 
         RECT-60 
      WITH FRAME frBasic IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasic}
  DISPLAY fiSecondLoanAmt cbSecondLoanRateType 
      WITH FRAME frScndLoan IN WINDOW C-Win.
  ENABLE RECT-61 fiSecondLoanAmt cbSecondLoanRateType 
      WITH FRAME frScndLoan IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoan}
  DISPLAY fiCPL fiLenderPolicyPremium fiOwnerPolicyPremium fiCPLTotal 
          figrandTotal 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  ENABLE fiCPL btCalculate btClear fiLenderPolicyPremium fiOwnerPolicyPremium 
         fiCPLTotal figrandTotal RECT-58 RECT-62 RECT-71 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicResults}
  DISPLAY fiScndLenderPolicyPremium 
      WITH FRAME frScndLoanResults IN WINDOW C-Win.
  ENABLE RECT-63 fiScndLenderPolicyPremium 
      WITH FRAME frScndLoanResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoanResults}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractOutputParameters C-Win 
PROCEDURE extractOutputParameters :
/*------------------------------------------------------------------------------
  Purpose:  parse output parameter of calculated premium 
   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable iCount as integer no-undo.
assign
  premiumOwner    = "premiumOwner"       
  premiumLoan     = "premiumLoan"       
  premiumSimo     = "premiumSimo"       
  premiumBasic    = "premiumBasic"    
  premiumScndLoan = "premiumScnd"
  success         = "success"            
  serverErrMsg    = "serverErrMsg"
  cardSetID       = "cardSetID"
  premiumCPL      = "premiumCPL"
  effectiveDate   = "manualEffectiveDate"
  ratesCode       = "RatesCode"
  ratesMsg        = "RatesMsg".


do iCount = 1 to num-entries(pcCalculatedPremium,","):

case entry(1,entry(iCount,pcCalculatedPremium,","),"="):
  
    when premiumOwner  then
         premiumOwner                  = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumLoan then
         premiumLoan                   = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumScndLoan  then
         premiumScndLoan               = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when success then
         success                       = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when serverErrMsg then
         serverErrMsg                  = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when premiumCPL  then
         premiumCPL                    = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when cardSetId  then                            
         cardSetId                     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                        
                                                                                                     
    when effectiveDate  then
         effectiveDate                 = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
    when ratesCode then                                                                                                                               
         ratesCode                     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error. 
           
    when ratesMsg then                                                                                                                               
         ratesMsg                      = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.  
         
  end case.  
end.

lSuccess = logical(success) no-error.

 if  premiumOwner = "premiumOwner" then
     premiumOwner = "0".
 if  premiumLoan = "premiumLoan" then
     premiumLoan = "0".
 if  premiumScndLoan = "premiumScnd" then
     premiumScndLoan = "0".
 if  premiumSimo = "premiumSimo" then
     premiumSimo = "0".
 if  premiumBasic = "premiumBasic" then
     premiumBasic = "0".
 if  premiumCPL = "premiumCPL" then
     premiumCPL = "0".
 if  ratesCode = "RateCode" then
     ratesCode = "".
 if  ratesMsg = "RatesMsg" then
     ratesMsg = "".    
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loanAmntValueChanged C-Win 
PROCEDURE loanAmntValueChanged :
/*------------------------------------------------------------------------------
  Purpose:     Handle simultaneous case (if user enters amount in both owner 
               and loan section then it is considered as simultaneous case
               and action to be taken
               - update ratetype list
               - handle simo checkbox
               - handle second loan button)

  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable pclistOwner          as character no-undo.
 define variable pclistLoan           as character no-undo.
 define variable pclistScndLoan       as character no-undo.
 define variable cRatetype            as character no-undo.
 define variable cLoanRatetype        as character no-undo.
 define variable cSecondLoanRatetype  as character no-undo.
 do with frame frBasic:
 end.
 
 assign
   cRatetype           = cbRateType          :screen-value
   cLoanRatetype       = cbLoanRateType      :screen-value
   cSecondLoanRatetype = cbSecondLoanRateType:screen-value in frame frScndLoan.

 if tempcoverageAmount ne "0" and fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0"  then
 do:
   assign
     tbSimultaneous:checked = true.
   if cbPropType:screen-value = "R" or cbPropType:screen-value = "M" then
     assign
       btSecondLoan:sensitive in frame frBasic = true
       multiLoanSelected = true.
   else
   do:
     btSecondLoan:sensitive in frame frBasic = false.
     apply 'value-changed' to btSecondLoan.
   end.
   publish "GetGASimoComboList"(cbPropType:screen-value,
                              yes,
                              output pclistOwner,  
                              output pclistLoan,  
                              output pclistScndLoan).
 end.
 else 
 do:
   assign
     tbSimultaneous:checked                    = false
     btSecondLoan  :sensitive in frame frBasic = false
     multiLoanSelected                         = false.
   publish "GetGAComboLists"(cbPropType:screen-value,
                           output pclistOwner,  
                           output pclistLoan,  
                           output pclistScndLoan).
 end.                     
 run clearResults in this-procedure no-error.
 apply 'value-changed' to tbSimultaneous.
 cRatetype = cbRateType:screen-value.

 assign
   cbRateType          :list-item-pairs                     =  trim(pclistOwner, ",")
   cbLoanRateType      :list-item-pairs                     =  trim(pclistLoan, ",")
   cbSecondLoanRateType:list-item-pairs in frame frScndLoan =  trim(pclistScndLoan, ",").

 if lookup(cRatetype,cbRateType:list-item-pairs) = 0 
  then
   cbRateType:screen-value     = entry(2,cbRateType:list-item-pairs,",") no-error.
 else 
   cbRateType:screen-value     = cRatetype.

 if lookup(cLoanRatetype,cbLoanRateType:list-item-pairs) = 0 
  then
   cbLoanRateType:screen-value     = entry(2,cbLoanRateType:list-item-pairs,",") no-error.
 else 
   cbLoanRateType:screen-value     = cLoanRatetype.

 if lookup(cSecondLoanRatetype,cbSecondLoanRateType:list-item-pairs) = 0 
  then
   cbSecondLoanRateType:screen-value     = entry(2,cbSecondLoanRateType:list-item-pairs,",") no-error.
 else 
   cbSecondLoanRateType:screen-value     = cSecondLoanRatetype.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE propertyTypeValueChange C-Win 
PROCEDURE propertyTypeValueChange :
/*------------------------------------------------------------------------------
  Purpose:    handle UI state and rate type list for selected property type      
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame frBasic:
 end.   
 
 define variable pclistOwner      as character no-undo.
 define variable pclistLoan       as character no-undo.
 define variable pclistScndLoan   as character no-undo.

 define variable trackRateType           as character no-undo.
 define variable trackLoanRateType       as character no-undo.
 define variable trackSecondLoanRateType as character no-undo.

 publish "GetGAComboLists" (cbPropType:screen-value,
                            output pclistOwner,  
                            output pclistLoan,  
                            output pclistScndLoan).

 trackRateType = cbRateType:screen-value.
 trackLoanRateType = cbLoanRateType:screen-value.
 trackSecondLoanRateType = cbSecondLoanRateType:screen-value in frame frScndLoan.

 assign                                                 
     cbRateType          :list-item-pairs                     =  trim(pclistOwner , ",")
     cbLoanRateType      :list-item-pairs                     =  trim(pclistLoan , ",")
     cbSecondLoanRateType:list-item-pairs in frame frScndLoan =  trim(pclistScndLoan , ",")
     .

 if can-do(cbRateType:list-item-pairs,trackRateType) 
  then
   cbRateType:screen-value = trackRateType.
  else
   cbRateType:screen-value = {&none}.

 if can-do(cbLoanRateType:list-item-pairs,trackLoanRateType) 
  then
   cbLoanRateType:screen-value = trackLoanRateType.
  else
   cbLoanRateType:screen-value = {&none}.

 if can-do(cbSecondLoanRateType:list-item-pairs,trackSecondLoanRateType) 
  then
   cbSecondLoanRateType:screen-value = trackSecondLoanRateType.
  else
   cbSecondLoanRateType:screen-value = {&none}.
 
 apply 'value-changed' to cbRateType.
 apply 'value-changed' to cbLoanRateType.
 apply 'value-changed' to fiSecondLoanAmt.
 apply 'value-changed' to fiCoverageAmt.
 apply 'value-changed' to fiLoanAmt.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setPosition C-Win 
PROCEDURE setPosition :
/*------------------------------------------------------------------------------
  Purpose: resize ui based on endorsements are shown or hidden or second
           loan is shown or hidden  
   
  Parameters:  <none>
  Notes:       
  ------------------------------------------------------------------------------*/
 if  multiloanselected = false  then
 do: 
   frame frScndLoanResults:visible = false.
   frame frScndLoan       :visible = false.
   c-win:width = cutwidth.

   run ShowScrollBars(frame frBasic:handle,        no, no).
   run ShowScrollBars(frame frBasicResults:handle, no, no).
 end.
 else if multiloanselected = true then
 do:
   assign
     frame frBasicResults   :visible = true
     frame frScndLoan       :visible = true
     frame frScndLoanResults:visible = true.
   c-win:width = fullwidth.
   run ShowScrollBars(frame frBasic:handle, no,no).
   run ShowScrollBars(frame frBasicResults:handle, no, no).
   run ShowScrollBars(frame frScndLoan:handle, no, no).
   run ShowScrollBars(frame frScndLoanResults:handle, no, no).
   run ShowScrollBars(frame {&frame-name}:handle, no,no).
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

