&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File:     rcm/dialogcopystate.w

  Description: Copy cards of a version of a state into another version.

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Archana Gupta

  Created: 
  
  Modified    :
   Date        Name     Comments
   07/01/2019  Sachin   Modified for standardisation.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{lib\std-def.i}
{lib\brw-multi-def.i}

/*  Temp-table definitions                                             */
{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratetable.i}
{tt\rateattr.i}
{tt\raterule.i}
{tt\ratestate.i &tablealias=tempRateState}
{tt\ratecard.i &tablealias=tempRateCard}

/* Parameters definitions ---                                           */
define input  parameter  pStateID     as character no-undo.
define input  parameter  pVersion     as integer   no-undo.
define input  parameter  pCardSetID   as integer   no-undo.
define input  parameter  table for ratestate.
define output parameter lSuccess     as logical no-undo.

/* Local Variable definitions ---                                       */
define variable rateCardsList     as character no-undo.
define variable availableCards    as character no-undo.
define variable selectedCards     as character no-undo.
define variable activeCardID      as integer   no-undo.
define variable iRateCardID       as integer   no-undo. 
define variable rRateCardID       as rowid     no-undo.
define variable iNumRows          as integer   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAvailableRateCard

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ratecard tempratecard

/* Definitions for BROWSE brwAvailableRateCard                          */
&Scoped-define FIELDS-IN-QUERY-brwAvailableRateCard ratecard.region ratecard.cardName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAvailableRateCard   
&Scoped-define SELF-NAME brwAvailableRateCard
&Scoped-define QUERY-STRING-brwAvailableRateCard for each ratecard no-lock
&Scoped-define OPEN-QUERY-brwAvailableRateCard open query {&SELF-NAME} for each ratecard no-lock.
&Scoped-define TABLES-IN-QUERY-brwAvailableRateCard ratecard
&Scoped-define FIRST-TABLE-IN-QUERY-brwAvailableRateCard ratecard


/* Definitions for BROWSE brwSelectedRateCard                           */
&Scoped-define FIELDS-IN-QUERY-brwSelectedRateCard tempratecard.region tempratecard.cardName   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwSelectedRateCard   
&Scoped-define SELF-NAME brwSelectedRateCard
&Scoped-define QUERY-STRING-brwSelectedRateCard for each tempratecard no-lock
&Scoped-define OPEN-QUERY-brwSelectedRateCard open query {&SELF-NAME} for each tempratecard no-lock.
&Scoped-define TABLES-IN-QUERY-brwSelectedRateCard tempratecard
&Scoped-define FIRST-TABLE-IN-QUERY-brwSelectedRateCard tempratecard


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAvailableRateCard}~
    ~{&OPEN-QUERY-brwSelectedRateCard}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS eVersionDesc fApprovedBy fEffectiveDate ~
cbFillingMethod fDateFilled fDateStateApproved fConfigUI fExecDM fExecUI ~
fExecBL bAddAll bAdd bRemove bRemoveAll eComments Btn_OK ~
brwAvailableRateCard Btn_Cancel brwSelectedRateCard 
&Scoped-Define DISPLAYED-OBJECTS eVersionDesc fApprovedBy fEffectiveDate ~
cbFillingMethod fDateFilled fDateStateApproved fConfigUI fExecDM fExecUI ~
fExecBL eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-brwQAR 
       MENU-ITEM m_View_Detail  LABEL "View Detail"   .

DEFINE MENU POPUP-MENU-brwQAR-2 
       MENU-ITEM m_View_Detail-2 LABEL "View Detail"   .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bAdd 
     LABEL "-->" 
     SIZE 5.6 BY 1 TOOLTIP "Add Selected Rate Card to Copy".

DEFINE BUTTON bAddAll 
     LABEL "ALL-->" 
     SIZE 5.6 BY 1 TOOLTIP "Add All Rate Cards to Copy".

DEFINE BUTTON bRemove 
     LABEL "<--" 
     SIZE 5.6 BY 1 TOOLTIP "Remove Selected Rate Card from Copy".

DEFINE BUTTON bRemoveAll 
     LABEL "<-- ALL" 
     SIZE 5.6 BY 1 TOOLTIP "Remove All Rate Cards from Copy".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbFillingMethod AS CHARACTER FORMAT "X(256)":U INITIAL "Manual" 
     LABEL "Filing Method" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEMS "Electronic","Manual" 
     DROP-DOWN-LIST
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 93.8 BY 2.71 NO-UNDO.

DEFINE VARIABLE eVersionDesc AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 70.2 BY 2.14 NO-UNDO.

DEFINE VARIABLE fApprovedBy AS CHARACTER FORMAT "X(256)":U 
     LABEL "Approved By" 
     VIEW-AS FILL-IN 
     SIZE 25.2 BY 1 NO-UNDO.

DEFINE VARIABLE fConfigUI AS CHARACTER FORMAT "X(100)":U 
     LABEL "UI Configuration File" 
     VIEW-AS FILL-IN 
     SIZE 70.2 BY 1 NO-UNDO.

DEFINE VARIABLE fDateFilled AS DATE FORMAT "99/99/99":U 
     LABEL "Filed" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fDateStateApproved AS DATE FORMAT "99/99/99":U 
     LABEL "State Approved" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fEffectiveDate AS DATE FORMAT "99/99/99":U 
     LABEL "Manual Effective" 
     VIEW-AS FILL-IN 
     SIZE 17.8 BY 1 NO-UNDO.

DEFINE VARIABLE fExecBL AS CHARACTER FORMAT "X(100)":U 
     LABEL "Business Layer Program" 
     VIEW-AS FILL-IN 
     SIZE 70.2 BY 1 NO-UNDO.

DEFINE VARIABLE fExecDM AS CHARACTER FORMAT "X(100)":U 
     LABEL "Data Model Program" 
     VIEW-AS FILL-IN 
     SIZE 70.2 BY 1 NO-UNDO.

DEFINE VARIABLE fExecUI AS CHARACTER FORMAT "X(100)":U 
     LABEL "User Interface Program" 
     VIEW-AS FILL-IN 
     SIZE 70.2 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAvailableRateCard FOR 
      ratecard SCROLLING.

DEFINE QUERY brwSelectedRateCard FOR 
      tempratecard SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAvailableRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAvailableRateCard Dialog-Frame _FREEFORM
  QUERY brwAvailableRateCard DISPLAY
      ratecard.region  label "Region"                   
 ratecard.cardName     label "Name"             width 35     format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE NO-TAB-STOP SIZE 43 BY 12.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwSelectedRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwSelectedRateCard Dialog-Frame _FREEFORM
  QUERY brwSelectedRateCard DISPLAY
      tempratecard.region       label "Region"                   
      tempratecard.cardName     label "Name"             width 35     format "x(50)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE NO-TAB-STOP SIZE 43 BY 12.91
         BGCOLOR 15  ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     eVersionDesc AT ROW 1.29 COL 26.4 NO-LABEL WIDGET-ID 42
     fApprovedBy AT ROW 3.57 COL 24.2 COLON-ALIGNED WIDGET-ID 20
     fEffectiveDate AT ROW 3.57 COL 76.6 COLON-ALIGNED WIDGET-ID 14
     cbFillingMethod AT ROW 4.71 COL 24.2 COLON-ALIGNED WIDGET-ID 40
     fDateFilled AT ROW 4.71 COL 76.6 COLON-ALIGNED WIDGET-ID 18
     fDateStateApproved AT ROW 5.86 COL 76.6 COLON-ALIGNED WIDGET-ID 24
     fConfigUI AT ROW 7 COL 24.2 COLON-ALIGNED WIDGET-ID 92
     fExecDM AT ROW 8.14 COL 24.2 COLON-ALIGNED WIDGET-ID 96
     fExecUI AT ROW 9.29 COL 24.2 COLON-ALIGNED WIDGET-ID 98
     fExecBL AT ROW 10.43 COL 24.2 COLON-ALIGNED WIDGET-ID 94
     bAddAll AT ROW 15.76 COL 46.6 WIDGET-ID 90
     bAdd AT ROW 16.86 COL 46.6 WIDGET-ID 80
     bRemove AT ROW 18 COL 46.6 WIDGET-ID 78
     bRemoveAll AT ROW 19.1 COL 46.6 WIDGET-ID 88
     eComments AT ROW 26.38 COL 2.6 NO-LABEL WIDGET-ID 36
     Btn_OK AT ROW 29.38 COL 33.4
     brwAvailableRateCard AT ROW 12.48 COL 2.6 WIDGET-ID 400
     Btn_Cancel AT ROW 29.38 COL 50.4
     brwSelectedRateCard AT ROW 12.48 COL 53.4 WIDGET-ID 500
     "Selected Cards" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 11.81 COL 66 WIDGET-ID 84
     "Available Cards" VIEW-AS TEXT
          SIZE 16 BY .62 AT ROW 11.86 COL 15.2 WIDGET-ID 70
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 25.76 COL 2.6 WIDGET-ID 38
     "Description:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 1.38 COL 14.4 WIDGET-ID 46
     SPACE(71.19) SKIP(28.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE ""
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwAvailableRateCard Btn_OK Dialog-Frame */
/* BROWSE-TAB brwSelectedRateCard Btn_Cancel Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

ASSIGN 
       brwAvailableRateCard:POPUP-MENU IN FRAME Dialog-Frame             = MENU POPUP-MENU-brwQAR:HANDLE
       brwAvailableRateCard:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAvailableRateCard:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

ASSIGN 
       brwSelectedRateCard:POPUP-MENU IN FRAME Dialog-Frame             = MENU POPUP-MENU-brwQAR-2:HANDLE
       brwSelectedRateCard:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwSelectedRateCard:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAvailableRateCard
/* Query rebuild information for BROWSE brwAvailableRateCard
     _START_FREEFORM
open query {&SELF-NAME} for each ratecard no-lock.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAvailableRateCard */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwSelectedRateCard
/* Query rebuild information for BROWSE brwSelectedRateCard
     _START_FREEFORM
open query {&SELF-NAME} for each tempratecard no-lock.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwSelectedRateCard */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAdd
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAdd Dialog-Frame
ON CHOOSE OF bAdd IN FRAME Dialog-Frame /* --> */
DO:
  if brwAvailableRateCard:num-selected-rows = 0 
   then return.
  
  if brwAvailableRateCard:num-selected-rows = 1  then do:
    find current ratecard no-error.
    assign iRateCardID = ratecard.cardID.
  end.
 
  do iNumRows = 1 to browse brwAvailableRateCard:num-selected-rows:
    brwAvailableRateCard:fetch-selected-row(iNumRows) no-error.
    get current brwAvailableRateCard exclusive-lock.
    create tempratecard.
    buffer-copy ratecard to tempratecard no-error.
    delete ratecard.
  end.
  
  brwAvailableRateCard:delete-selected-rows() no-error.
  
  close query brwSelectedRateCard.
  open query brwSelectedRateCard for each tempratecard by tempratecard.region.
  
  if iNumRows = 2 
   then
    do: 
      rRateCardID = ?.
      find first tempratecard where tempratecard.cardID = iRateCardID no-error.
      if available tempratecard then
        rRateCardID  =  rowid(tempratecard) no-error.
      reposition brwSelectedRateCard to rowid rRateCardID no-error.
    end.
  
 if not can-find(first ratecard) then
   assign bAdd   :sensitive = false
          bAddAll:sensitive = false.

 if can-find(first tempratecard) then
   assign bRemove   :sensitive = true
          bRemoveAll:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddAll Dialog-Frame
ON CHOOSE OF bAddAll IN FRAME Dialog-Frame /* ALL--> */
DO:
  for each ratecard:
    create tempratecard.
    buffer-copy ratecard to tempratecard no-error. 
    delete ratecard.
  end.
   
  close query brwSelectedRateCard.
  open query brwSelectedRateCard for each tempratecard by tempratecard.region.

  close query brwAvailableRateCard.
  open query brwAvailableRateCard for each ratecard by ratecard.region.

  assign bAdd      :sensitive = false
         bAddAll   :sensitive = false
         bRemove   :sensitive = true
         bRemoveAll:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemove
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemove Dialog-Frame
ON CHOOSE OF bRemove IN FRAME Dialog-Frame /* <-- */
DO:
  if brwSelectedRateCard:num-selected-rows = 0 
   then return.

  if brwSelectedRateCard:num-selected-rows = 1 
   then
    do:
      find current tempRateCard no-error.
      assign iRateCardID = tempRateCard.cardID.
    end.

  do iNumRows = 1 to browse brwSelectedRateCard:num-selected-rows:
    brwSelectedRateCard:fetch-selected-row(iNumRows) no-error.
    get current brwSelectedRateCard exclusive-lock.

    create ratecard.
    buffer-copy tempratecard to ratecard no-error.     
    delete tempratecard.
  end.
    
  brwSelectedRateCard:delete-selected-rows() no-error.
  
  if iNumRows = 2 
   then
    do:
      rRateCardID = ?.
      find first ratecard where ratecard.cardID = iRateCardID no-error.
      if available ratecard then
        rRateCardID  =  rowid(ratecard) no-error.
      reposition brwAvailableRateCard to rowid rRateCardID no-error.
    end.
    
  close query brwAvailableRateCard.
  open query brwAvailableRateCard for each ratecard by ratecard.region.
  
  if can-find(first ratecard) then
    assign bAdd   :sensitive = true
           bAddAll:sensitive = true.

  if not can-find(first tempratecard) then
    assign bRemove   :sensitive = false
           bRemoveAll:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRemoveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRemoveAll Dialog-Frame
ON CHOOSE OF bRemoveAll IN FRAME Dialog-Frame /* <-- ALL */
DO:
  for each tempratecard:
    create ratecard.
    buffer-copy tempratecard to ratecard no-error.   
    delete tempratecard.
  end.

  close query brwAvailableRateCard.
  open  query brwAvailableRateCard for each ratecard by ratecard.region.

  close query brwSelectedRateCard.
  open  query brwSelectedRateCard for each tempratecard by tempratecard.region.

  assign bAdd      :sensitive = true
         bAddAll   :sensitive = true
         bRemove   :sensitive = false
         bRemoveAll:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailableRateCard
&Scoped-define SELF-NAME brwAvailableRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableRateCard Dialog-Frame
ON DEFAULT-ACTION OF brwAvailableRateCard IN FRAME Dialog-Frame
DO:
  apply "choose" to bAdd.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableRateCard Dialog-Frame
ON LEFT-MOUSE-UP OF brwAvailableRateCard IN FRAME Dialog-Frame
DO:
  find current ratecard no-error.
  if available ratecard then
    brwAvailableRateCard:tooltip = ratecard.description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableRateCard Dialog-Frame
ON ROW-DISPLAY OF brwAvailableRateCard IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAvailableRateCard Dialog-Frame
ON VALUE-CHANGED OF brwAvailableRateCard IN FRAME Dialog-Frame
DO:
  find current ratecard no-error.
  if available ratecard then
    activeCardID = ratecard.cardID.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwSelectedRateCard
&Scoped-define SELF-NAME brwSelectedRateCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON DEFAULT-ACTION OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
  apply "choose" to bRemove.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON LEFT-MOUSE-UP OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
  find current tempratecard no-error.
  if available tempratecard then
    brwSelectedRateCard:tooltip = tempratecard.description.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON ROW-DISPLAY OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwSelectedRateCard Dialog-Frame
ON VALUE-CHANGED OF brwSelectedRateCard IN FRAME Dialog-Frame
DO:
  find current tempratecard no-error.
  if available tempratecard then
    activeCardID = tempratecard.cardID.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  create tempratestate.
  assign
    tempratestate.version             =  pVersion                              
    tempratestate.stateid             =  pStateID                              
    tempratestate.cardsetid           =  pCardSetID                            
    tempratestate.description         =  eVersionDesc:screen-value             
    tempratestate.effectiveDate       =  date(fEffectiveDate:screen-value)     
    tempratestate.approvedBy          =  fApprovedBy:screen-value              
    tempratestate.filingMethod        =  cbFillingMethod:screen-value          
    tempratestate.dateFiled           =  date(fDateFilled:screen-value)        
    tempratestate.dateStateApproved   =  date(fDateStateApproved:screen-value) 
    tempratestate.comment             =  eComments:screen-value
    tempratestate.configUserInterface =  fConfigUI:screen-value
    tempratestate.execBusinessLogic   =  fExecBL:screen-value
    tempratestate.execDataModel       =  fExecDM:screen-value
    tempratestate.execUserInterface   =  fExecUI:screen-value
    tempratestate.active              =  false.

  publish "CopyRateState"(input table tempratestate, 
                          input table tempratecard,
                          output lSuccess).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail Dialog-Frame
ON CHOOSE OF MENU-ITEM m_View_Detail /* View Detail */
DO:
  run EditRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_View_Detail-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_View_Detail-2 Dialog-Frame
ON CHOOSE OF MENU-ITEM m_View_Detail-2 /* View Detail */
DO:
  run EditRateCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAvailableRateCard
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetRateCards" (input pCardSetID,
                        no, /*fetch ratecards from datamodel if exist*/
                        output table ratecard). 

bRemoveAll:load-image("images/s-previouspg.bmp") no-error.
bRemove   :load-image("images/s-previous.bmp") no-error.
bAdd      :load-image("images/s-next.bmp") no-error.
bAddAll   :load-image("images/s-nextpg.bmp") no-error.

bRemoveAll:load-image-insensitive("images/s-previouspg-i.bmp") no-error.
bRemove   :load-image-insensitive("images/s-previous-i.bmp") no-error.
bAdd      :load-image-insensitive("images/s-next-i.bmp") no-error.
bAddAll   :load-image-insensitive("images/s-nextpg-i.bmp") no-error.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

{lib/brw-main-multi.i &browse-list="brwAvailableRateCard,brwSelectedRateCard"} 

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

  bRemoveAll:sensitive = false.
  bRemove   :sensitive = false.
  bAdd      :sensitive = false.
  bAddAll   :sensitive = false.

  find first ratestate where ratestate.cardsetid = pCardSetID  no-error.
  if available ratestate 
   then
    do:
      assign
        eVersionDesc      :screen-value = if ratestate.description = ? or ratestate.description = "?"                  then "" else string(ratestate.description)               
        fEffectiveDate    :screen-value = if ratestate.effectiveDate = ?                                               then "" else string(ratestate.effectiveDate)                       
        fApprovedBy       :screen-value = if ratestate.approvedBy = ? or ratestate.approvedBy = "?"                    then "" else rateState.approvedBy                         
        cbFillingMethod   :screen-value = if ratestate.filingMethod = ? or ratestate.filingMethod = "?"                then "" else rateState.filingMethod                   
        fDateFilled       :screen-value = if ratestate.dateFiled = ?                                                   then "" else string(rateState.dateFiled)                  
        fDateStateApproved:screen-value = if ratestate.dateStateApproved = ?                                           then "" else string(rateState.dateStateApproved)           
        eComments         :screen-value = if ratestate.comment = ? or ratestate.comment = "?"                          then "" else rateState.comment 
        fExecBL           :screen-value = if ratestate.execBusinessLogic = ? or ratestate.execBusinessLogic = "?"      then "" else rateState.execBusinessLogic
        fConfigUI         :screen-value = if ratestate.configUserInterface = ? or ratestate.configUserInterface = "?"  then "" else rateState.configUserInterface
        fExecDM           :screen-value = if ratestate.execDataModel = ? or ratestate.execDataModel = "?"              then "" else rateState.execDataModel
        fExecUI           :screen-value = if ratestate.execUserInterface = ? or ratestate.execUserInterface = "?"      then "" else rateState.execUserInterface
        bAdd              :sensitive    = true
        bAddAll           :sensitive    = true.
        .    
      frame Dialog-Frame:title = ("Copy Version - " + ratestate.description).
    end.
    
  wait-for go of frame {&frame-name}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY eVersionDesc fApprovedBy fEffectiveDate cbFillingMethod fDateFilled 
          fDateStateApproved fConfigUI fExecDM fExecUI fExecBL eComments 
      WITH FRAME Dialog-Frame.
  ENABLE eVersionDesc fApprovedBy fEffectiveDate cbFillingMethod fDateFilled 
         fDateStateApproved fConfigUI fExecDM fExecUI fExecBL bAddAll bAdd 
         bRemove bRemoveAll eComments Btn_OK brwAvailableRateCard Btn_Cancel 
         brwSelectedRateCard 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

