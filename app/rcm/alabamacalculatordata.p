&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        :  alabamacalculatordata.p
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Anjly and Archana
    Created     :
    Notes       :
    
    @Modified   :    
    Date        Name         Comments
    07/01/2019  Vikas Jain   Modified for standardisation.          
  ----------------------------------------------------------------------*/  
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/
{lib\std-def.i}
{lib\rcm-std-def.i}
/* ***************************  Definitions  ************************** */
/* Parameter definitions         */
define input  parameter pUIConfigFile as character no-undo.
define output parameter perror        as logical   no-undo.
define output parameter perrorMsg     as character no-undo.

/* Temp-table definitions       */
{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratetable.i}
{tt\raterule.i}
{tt\ratelog.i}
{tt\rateUI.i}
define temp-table endorsementData like endorsement.

{tt\ratecard.i  &tablealias=tempRateCard}
{tt\ratetable.i &tablealias=tempRateTable}
{tt\raterule.i  &tablealias=tempRateRule}

/* Local variable definitions  */
define variable hServer as handle no-undo.

/* Dataset definitions         */
define dataset RCUI
 for developerComments, proposedRateConfig, region, propertyType, loanRateType, ownerRateType,scndLoanRateType,endorsement
    data-relation for propertyType, loanRateType relation-fields (propertyTypecode, propertyTypecode)     nested   foreign-key-hidden        
    data-relation for propertyType, ownerRateType relation-fields (propertyTypecode, propertyTypecode)    nested   foreign-key-hidden        
    data-relation for propertyType, scndLoanRateType relation-fields (propertyTypecode, propertyTypecode) nested   foreign-key-hidden .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 6.62
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
if search(pUIConfigFile) = ? 
 then
  do:
     perrormsg = "UI can't be initialized for selected state. Contact the System Administrator.".
     perror = true.
     apply 'close' to this-procedure.
     return.
  end.
std-lo = dataset RCUI:read-json("file", search(pUIConfigFile), "empty").

run ratedatasrv.p persistent set hServer.

subscribe to "GetALComboLists"               anywhere.
subscribe to "GetALPropertyType"             anywhere.
subscribe to "GetALSimoComboList"            anywhere.
subscribe to "GetALEndors"                   anywhere.
subscribe to "GetALPriorPolicyAmountConfig"  anywhere.
subscribe to "GetALPriorPolicyDateConfig"    anywhere.
subscribe to "GetALProposedRate"             anywhere.
subscribe to "ALCalculatePremium"            anywhere.
subscribe to "GetALRateCardDetail"           anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-ALCalculatePremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ALCalculatePremium Procedure 
PROCEDURE ALCalculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcUiInput           as character no-undo.
  define input  parameter ipcStateID           as character no-undo.
  define input  parameter ipiVersion           as integer   no-undo.
  define output parameter opcCalculatedPremium as character no-undo.
  define output parameter table for rateLog.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  run calculatePremiumService in hserver(input ipcUiInput,
                                         input ipcStateID,
                                         input ipiVersion,
                                         output opcCalculatedPremium,
                                         output table rateLog,
                                         output lSuccess,
                                         output cMsg
                                         ).
  if not lSuccess 
   then
    do:
      message cMsg
          view-as alert-box info buttons ok.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALComboLists) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALComboLists Procedure 
PROCEDURE GetALComboLists :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcPropertyTypeCode  as character no-undo.
  
  define output parameter opcListOwner         as character no-undo.
  define output parameter opcListLoan          as character no-undo.
  define output parameter opcListScndLoan      as character no-undo.
  
  define buffer ownerRateType    for ownerRateType.
  define buffer loanRateType     for loanRateType.
  define buffer scndLoanRateType for scndLoanRateType.

  for each ownerRateType where ownerRateType.propertyTypecode = ipcPropertyTypeCode and ownerRateType.simo <> ?:
    opcListOwner = opcListOwner + "," + ownerRateType.description + "," + ownerRateType.ownerRateTypecode .
  end.
  opcListOwner = trim({&SelectType} + "," + {&None} + "," + trim(opcListOwner, ","), ",").
   
  for each loanRateType where loanRateType.propertyTypecode = ipcPropertyTypeCode and loanRateType.simo <> ?:
    opcListLoan = opcListLoan + "," + loanRateType.description + "," + loanRateType.loanRateTypecode .
  end.
  opcListLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListLoan, ","), ",").
   
  for each scndLoanRateType where scndLoanRateType.propertyTypecode = ipcPropertyTypeCode and scndLoanRateType.simo <> ? :
    opcListScndLoan = opcListScndLoan + "," + scndLoanRateType.description + "," + scndLoanRateType.scndLoanRateTypecode .
  end.
  opcListScndLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListScndLoan, ","), ",").                                                                      
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALEndors Procedure 
PROCEDURE GetALEndors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcCaseType as character no-undo.
  define input  parameter ipcPropType as character no-undo.
  define input  parameter ipcRateType as character no-undo.
  define output parameter table for endorsementdata.
  
  define buffer endorsement for endorsement.

  empty temp-table endorsementdata.
  
  for each endorsement where (endorsement.endorsementType = ipcCaseType        or endorsement.endorsementType = "")
                         and (can-do(endorsement.propertyTypeList,ipcPropType) or endorsement.propertyTypeList = "")
                         and (can-do(endorsement.rateTypeList,(if ipcRateType = {&None} then endorsement.rateTypeList else ipcRateType)) or endorsement.rateTypeList = "") :
    create endorsementdata.
    buffer-copy endorsement to endorsementdata.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALPriorPolicyAmountConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALPriorPolicyAmountConfig Procedure 
PROCEDURE GetALPriorPolicyAmountConfig :
  /*------------------------------------------------------------------------------
    Purpose:     
    Parameters:  <none>
    Notes:       
  ------------------------------------------------------------------------------*/
  define input  parameter ipcPropType   as character no-undo.
  define input  parameter ipcCaseType   as character no-undo.
  define input  parameter ipcRateType   as character no-undo.
  define output parameter oplShowPrior  as logical   no-undo.
  
  if ipcCaseType = {&Owners} 
   then
    do:
      find first ownerRateType 
       where ownerRateType.propertyTypecode = ipcPropType 
        and ownerRateType.ownerRateTypeCode = ipcRateType no-error.
      if available ownerRateType 
       then
        oplShowPrior = ownerRateType.priorPolicyAmountFlag.    
    end.
 
  if ipcCaseType = {&Lenders} 
   then
    do:
      find first loanRateType 
       where loanRateType.propertyTypecode = ipcPropType 
        and loanRateType.LoanRateTypeCode = ipcRateType no-error.
      if available loanRateType 
       then
        oplShowPrior = loanRateType.priorPolicyAmountFlag.
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALPropertyType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALPropertyType Procedure 
PROCEDURE GetALPropertyType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter pcPropertyType as character no-undo.
  
  define buffer propertyType for propertyType.
  
  for each propertyType:     
    pcPropertyType = pcPropertyType + "," + propertyType.description + "," + propertyType.propertyTypecode .
  end.
  
  pcPropertyType = trim(pcPropertyType, ",").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALProposedRate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALProposedRate Procedure 
PROCEDURE GetALProposedRate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcEndorsementCode  as character no-undo.
  define output parameter opdeProposedRate    as decimal   no-undo. 
  
  find first endorsement where endorsement.endorsementCode = ipcEndorsementCode no-error.
  if available endorsement 
   then
    opdeProposedRate = endorsement.proposedRate.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALRateCardDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALRateCardDetail Procedure 
PROCEDURE GetALRateCardDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter piCardSetID as integer no-undo.
  define input parameter pCardID     as integer no-undo.
  
  define output parameter table for tempRateCard.
  define output parameter table for tempRateTable.
  define output parameter table for tempRateRule.
  
  define buffer tempRateCard  for tempRateCard.
  define buffer tempRateTable for tempRateTable.
  define buffer tempRateRule  for tempRateRule.
  
  empty temp-table tempRateCard .   
  empty temp-table tempRateTable.      
  empty temp-table tempRateRule .     
  
  run GetRateCardDetail in hServer(input piCardSetID,  
                                   input pCardID,
                                   output table tempRateCard, 
                                   output table tempRateTable,                               
                                   output table tempRateRule).                                                                                               
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetALSimoComboList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetALSimoComboList Procedure 
PROCEDURE GetALSimoComboList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcPropertyTypeCode as character no-undo.
  define input  parameter iplSimo             as logical   no-undo.
  
  define output parameter opcListOwner        as character no-undo.
  define output parameter opcListLoan         as character no-undo.
  define output parameter opcListScndLoan     as character no-undo.

  define buffer ownerRateType    for ownerRateType.
  define buffer loanRateType     for loanRateType.
  define buffer scndLoanRateType for scndLoanRateType.
  
  for each ownerRateType where ownerRateType.propertyTypecode = ipcPropertyTypeCode and (ownerRateType.simo = iplSimo or ownerRateType.simo = ?) :
    opcListOwner = opcListOwner + "," + ownerRateType.description + "," + ownerRateType.ownerRateTypecode .
  end.
  opcListOwner = trim({&SelectType} + "," + {&None} + "," + trim(opcListOwner, ","), ",").
   
  for each loanRateType where loanRateType.propertyTypecode = ipcPropertyTypeCode and (loanRateType.simo = iplSimo or loanRateType.simo = ?):
    opcListLoan = opcListLoan + "," + loanRateType.description + "," + loanRateType.loanRateTypecode .
  end.
  opcListLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListLoan, ","), ",").
   
  for each scndLoanRateType where scndLoanRateType.propertyTypecode = ipcPropertyTypeCode and (scndLoanRateType.simo = iplSimo or scndLoanRateType.simo = ?):
    opcListScndLoan = opcListScndLoan + "," + scndLoanRateType.description + "," + scndLoanRateType.scndLoanRateTypecode .
  end.
  opcListScndLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListScndLoan, ","), ",").   


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

