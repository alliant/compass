&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: rcm/dialognewrule.w

  Description: create new record for step 3 of card setup

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 
  Modified    :
   Date        Name     Comments
   07/01/2019  Sachin   Modified as per UAT comments.
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{tt\raterule.i &tablealias=tempRateRule}
{lib\rcm-std-def.i}
{lib\std-def.i}
define input parameter pRegion       as character.
define input parameter pCardSetID    as character.
define input parameter pCardName     as character.
define input parameter pRowNo        as integer.
define input parameter pAttribList   as character.
define input parameter cRateCardList as character. 
define input-output parameter table for tempRateRule.

/* Local Variable Definitions ---                                       */
define variable inRowNum            as integer   no-undo.
define variable cRefCardName        as character no-undo.
define variable pAttrIDList         as character no-undo.
define variable lResetRefCardName   as logical   no-undo.
define variable lResetRefCardAttrib as logical   no-undo.
define variable initialAttr         as character no-undo.
define variable initialRule         as character no-undo.
define variable attrList            as character no-undo.
define variable ruleList            as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cbAttrID eDescription cbRule tActive ~
eComments Btn_OK Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cbAttrID eDescription cbRule fCardName ~
tActive cbRefName fRow cbRefAttrib fValue eComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convBlank Dialog-Frame 
FUNCTION convBlank RETURNS CHARACTER
  (pcString as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD RemoveItemFromList Dialog-Frame 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cbAttrID AS CHARACTER 
     LABEL "Attribute ID" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 10
     DROP-DOWN
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cbRefAttrib AS CHARACTER FORMAT "X(256)":U INITIAL "--Select Attribute--" 
     LABEL "Referenced Card  Attribute" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "--Select Attribute--" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cbRefName AS CHARACTER FORMAT "X(256)":U INITIAL "--Select Rate Card--" 
     LABEL "Referenced Card Name" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "--Select Rate Card--" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE cbRule AS CHARACTER FORMAT "X(256)":U INITIAL "--Select Rule--" 
     LABEL "Rule" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEMS "--Select Rule--","Add","CardRange","CardRules","Divide","Fixed","Maximum","Minimum","Multiply","Percent","Round","RoundDown","RoundUp","ShowComments","Subtract" 
     DROP-DOWN-LIST
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 56.8 BY 2.71 NO-UNDO.

DEFINE VARIABLE eDescription AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 56.8 BY 1.67 NO-UNDO.

DEFINE VARIABLE fCardName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Card Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fRow AS CHARACTER FORMAT "X(256)":U 
     LABEL "Row" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 4.4 BY 1 NO-UNDO.

DEFINE VARIABLE fValue AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Value" 
     VIEW-AS FILL-IN 
     SIZE 15.2 BY 1 NO-UNDO.

DEFINE VARIABLE tActive AS LOGICAL INITIAL no 
     LABEL "Active" 
     VIEW-AS TOGGLE-BOX
     SIZE 11 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     cbAttrID AT ROW 2.52 COL 27 COLON-ALIGNED WIDGET-ID 298
     eDescription AT ROW 3.71 COL 29 NO-LABEL WIDGET-ID 286
     cbRule AT ROW 5.57 COL 27 COLON-ALIGNED WIDGET-ID 282
     fCardName AT ROW 1.29 COL 27 COLON-ALIGNED WIDGET-ID 6 NO-TAB-STOP 
     tActive AT ROW 5.67 COL 62.4 WIDGET-ID 34
     cbRefName AT ROW 6.76 COL 27 COLON-ALIGNED WIDGET-ID 268
     fRow AT ROW 1.29 COL 79.2 COLON-ALIGNED WIDGET-ID 20 NO-TAB-STOP 
     cbRefAttrib AT ROW 7.95 COL 27 COLON-ALIGNED WIDGET-ID 302
     fValue AT ROW 9.14 COL 27 COLON-ALIGNED WIDGET-ID 280
     eComments AT ROW 10.33 COL 29 NO-LABEL WIDGET-ID 36
     Btn_OK AT ROW 13.52 COL 28
     Btn_Cancel AT ROW 13.52 COL 45
     "Comments:" VIEW-AS TEXT
          SIZE 10.6 BY .62 AT ROW 10.48 COL 18.2 WIDGET-ID 38
     "Description:" VIEW-AS TEXT
          SIZE 11.6 BY .62 AT ROW 3.86 COL 17.4 WIDGET-ID 290
     SPACE(57.39) SKIP(10.51)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Add Rate Rule"
         CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME Custom                                                    */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX cbRefAttrib IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbRefName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fCardName IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fRow IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fValue IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Add Rate Rule */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  define variable validateStatus as logical initial true.
  define variable validateMsg as character.

  if cbRule:screen-value = "--Select Rule--" 
   then
    do:
      message "Select any Rule."
          view-as alert-box info buttons ok.
      return no-apply.
    end.

  else if cbRule:screen-value = {&CardRules} and cbRefName:screen-value = "--Select Rate Card--" 
   then
    do:
      message "Select any Rate Card."
          view-as alert-box info buttons ok.
      return no-apply.
    end.

  else if cbRefAttrib:sensitive = true and cbRefAttrib:screen-value = "--Select Attribute--" then
  do:
    message "You must select a Referenced Card Attribute if a Referenced Card is Selected."
          view-as alert-box info buttons ok.
    return no-apply.
  end.

  else
  do:
    Btn_OK:auto-go = true.

    apply 'leave' to cbAttrID.

    find first tempRateRule no-error.
    if not available tempRateRule then
      create tempRateRule.
    assign    
      tempRateRule.seq              =  int(fRow:screen-value)
      tempRateRule.attrid           =  convBlank(cbAttrID:screen-value)
      tempRateRule.ruleName         =  if cbRule:screen-value <> "--Select Rule--"  then cbRule:screen-value else ""             
      tempRateRule.ruleValue        =  decimal(fValue:screen-value)     
      tempRateRule.active           =  tActive:checked   
      tempRateRule.comments         =  eComments:screen-value          
      tempRateRule.description      =  eDescription:screen-value
      tempRateRule.refCardName      =  if (cbRefName:screen-value = "--Select Rate Card--" or cbRefName:screen-value = "" or cbRefName:screen-value = "?" or cbRefName:screen-value = ?) then "" else cbRefName:screen-value 
      tempRateRule.refAttrName      =  if (cbRefAttrib:screen-value = "Use Parent" or cbRefAttrib:screen-value = "--Select Attribute--") then "" else cbRefAttrib:screen-value      
      tempRateRule.refUseParentAttr =  if cbRefAttrib:screen-value = "Use Parent" then true else false  
      .
  end.

  find first tempRateRule no-error.
  if available tempRateRule and tempRateRule.seq <> inRowNum then
  do:
    publish "ValidateRateruleData" (input pCardName,
                                    input table tempRateRule,
                                    output validateStatus,
                                    output validateMsg).

    if validateStatus = false then
    do:
      message validateMsg
        view-as alert-box info buttons ok.
      empty temp-table tempRateRule.
      return no-apply.
    end.
  end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbAttrID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAttrID Dialog-Frame
ON LEAVE OF cbAttrID IN FRAME Dialog-Frame /* Attribute ID */
DO:
  if inRowNum = 0 then
  do:
    publish "GetNextAttribSeq"(input  convBlank(cbAttrID:screen-value),
                               output pRowNo).

    fRow:screen-value = string(pRowNo).
  end.

  initialAttr =  cbAttrID:screen-value.
  initialRule =  cbRule:screen-value.

   if cbRule:list-items = ? or  cbRule:list-items = "?" then 
     cbRule:list-items = "".

   ruleList = cbRule:list-items.
   
   if cbAttrID:screen-value = "preall" or 
       cbAttrID:screen-value = "postall" then
   do:
     ruleList = RemoveItemFromList( ruleList ,"default" , ",").
   end.
   else
   do:
     if lookup("default",ruleList,",") <= 0 then
       ruleList = ruleList + ",Default".
   end.

   cbAttrID:screen-value = initialAttr.
   cbRule:list-items     = ruleList.
   cbRule:screen-value   = if lookup(initialRule,cbRule:list-items,",") > 0 then initialRule else "--Select Rule--".                                                                                            
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRefName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRefName Dialog-Frame
ON LEAVE OF cbRefName IN FRAME Dialog-Frame /* Referenced Card Name */
DO:
  define variable existingListAttr    as character no-undo.
  define variable existingScreenValue as character no-undo.
  define variable lUseParent          as logical   no-undo.

  existingListAttr    = cbRefAttrib:list-items.
  existingScreenValue = cbRefAttrib:screen-value.

  do with frame {&frame-name}:
  end.

  if (cbRefName:screen-value <> "--Select Rate Card--" and cbRefName:screen-value <> "" and cbRefName:screen-value <> "?" and cbRefName:screen-value <> ?) and
     (cRefCardName ne "--Select Rate Card--" and (if cRefCardName = "" then cRefCardName ne cbRefName:screen-value else true) ) and
      cbRule:screen-value = {&CardRules}
   then
    do:
      publish "GetAttributes"(input cbRefName:screen-value,
                              input pCardSetID ,
                              input pRegion,
                              output pAttrIDList).

      /* Add "Use Parent" in the list of Referenced Card Attributes only if all attributes of parent are present in attributes of referenced card */
      do std-in = 1 to num-entries(pAttribList):
        if not can-do(pAttrIDList,entry(std-in,pAttribList)) 
         then
          leave.
        if std-in = num-entries(pAttribList) 
         then
          lUseParent = true.
      end.

      cbRefAttrib:list-items   = "--Select Attribute--," + (if lUseParent then "Use Parent," else "") + pAttrIDList.
      cbRefAttrib:screen-value = if (lookup(existingScreenValue,cbRefAttrib:list-items) eq 0) then "--Select Attribute--" else existingScreenValue .
    end.
  else
   assign
     cbRefAttrib:list-items   = existingListAttr
     cbRefAttrib:screen-value = existingScreenValue
     .
  assign cRefCardName = cbRefName:screen-value. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRefName Dialog-Frame
ON VALUE-CHANGED OF cbRefName IN FRAME Dialog-Frame /* Referenced Card Name */
DO:
    assign cbRefAttrib:sensitive    = if  cbRule:screen-value ne {&CardRules} then false else true 
           cbRefAttrib:screen-value = if  cbRule:screen-value ne {&CardRules} then "--Select Attribute--" else cbRefAttrib:screen-value  .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRule Dialog-Frame
ON LEAVE OF cbRule IN FRAME Dialog-Frame /* Rule */
DO:
  initialAttr = cbAttrID:screen-value.
  initialRule = cbRule:screen-value.

  if cbAttrID:list-items = ?  or  cbAttrID:list-items = "?" then
  cbAttrID:list-items = " ".

  attrList = cbAttrID:list-items.

  if cbRule:screen-value = "default" then
  do:
     attrList = RemoveItemFromList( attrList ,"preall" , ",").
     attrList = RemoveItemFromList( attrList ,"postall" , ",").
  end.
  else
  do:
    if lookup("PreAll",attrList,",") <= 0 then
      attrList = trim(attrList  + ",PreAll" , ",").
    if lookup("PostAll",attrList,",") <= 0 then
     attrList = trim(attrList  + ",PostAll" , ",").
  end.

  cbAttrID:list-items    = attrList.
  cbAttrID:screen-value  = initialAttr.
  cbRule:screen-value    = if lookup(initialRule,cbRule:list-items,",") > 0 then initialRule else "--Select Rule--".

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRule Dialog-Frame
ON VALUE-CHANGED OF cbRule IN FRAME Dialog-Frame /* Rule */
DO:


  if cbRule:screen-value = "CardRange" or cbRule:screen-value = {&CardRules} 
   then
    do:
      assign
        cbRefName  :sensitive = true
        fValue:sensitive      = false
        fValue:screen-value   = "0.00"  
        fValue:visible        = false
        .
    end.
   else
    do:
      assign
        cbRefName:sensitive    = false
        cbRefName:screen-value = "--Select Rate Card--"
        fValue:sensitive       = if cbRule:screen-value ne "--Select Rule--"  then true else false
        fValue:visible         = if cbRule:screen-value ne "--Select Rule--"  then true else false.
    end.
   apply "value-changed" to cbRefName.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

if cRateCardList <> "" then
  cbRefName :list-items in frame {&frame-name} = cbRefName:list-items in frame {&frame-name} + "," + cRateCardList.
     

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  assign
    fCardName:screen-value = convBlank(pCardName)
    fRow:screen-value      = string(pRowNo)
    inRowNum               = pRowNo
    .

  find first tempRateRule no-error.                            
  if available tempRateRule 
   then    
    do:
      publish "GetAttributes"(input tempRateRule.refCardName,
                              input pCardSetID ,
                              input pRegion,
                              output pAttrIDList).
      
      cbRefAttrib:list-items   = "--Select Attribute--," + "Use Parent," + pAttrIDList.

      assign
        cbAttrID:list-items       = convBlank(tempRateRule.attrid)
        cbAttrID:screen-value     = convBlank(tempRateRule.attrid)
        cbRule:screen-value       = if tempRateRule.ruleName = "" then cbRule:entry(1) else tempRateRule.ruleName        
        fValue:screen-value       = string(tempRateRule.ruleValue)
        tActive:checked           = tempRateRule.active
        eDescription:screen-value = tempRateRule.description                                
        eComments:screen-value    = tempRateRule.comments
        cbAttrID:sensitive        = false
        cbRefName:screen-value    = if (tempRateRule.refCardName = "" or tempRateRule.refCardName = "?" or tempRateRule.refCardName = ?) then "--Select Rate Card--" else  tempRateRule.refCardName
        cbRefAttrib:screen-value  = if (tempRateRule.refAttrName = "" and tempRateRule.refUseParentAttr) then "Use Parent" 
                                    else if (tempRateRule.refAttrName = "" and tempRateRule.refUseParentAttr = false) then ? else tempRateRule.refAttrName
        . 
    end.
    else
    do:
      if trim(pAttribList) <> "?" and trim(pAttribList) <> "" then
        cbAttrID:list-items  = pAttribList.
      if lookup({&PostAll},pAttribList) = 0 then
        cbAttrID:add-last({&PostAll}).
      if lookup({&PreAll},pAttribList) = 0 then
        cbAttrID:add-last({&PreAll}).
    end.
   
    apply "value-changed" to cbRule.
    apply "value-changed" to cbRefName.

  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbAttrID eDescription cbRule fCardName tActive cbRefName fRow 
          cbRefAttrib fValue eComments 
      WITH FRAME Dialog-Frame.
  ENABLE cbAttrID eDescription cbRule tActive eComments Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convBlank Dialog-Frame 
FUNCTION convBlank RETURNS CHARACTER
  (pcString as character):
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
define variable chVal as character no-undo. 

if pcString = "BLANK" then 
  chVal = "".
else if pcString = "" then 
  chVal = "BLANK".
else if pcString = "?" or pcString = ? then 
  chVal = "".
else 
  chVal = pcString.

RETURN chVal.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION RemoveItemFromList Dialog-Frame 
FUNCTION RemoveItemFromList returns character
    (pclist as character,
     pcremoveitem as character,
     pcdelimiter as character):
 
define variable lipos as integer no-undo.

lipos = lookup(pcremoveitem,pclist,pcdelimiter).

if lipos > 0 then
do:
  assign entry(lipos,pclist,pcdelimiter) = "".
  if lipos = 1 then
    pclist = substring(pclist,2).
  else if lipos = num-entries(pclist,pcdelimiter) then
    pclist = substring(pclist,1,length(pclist) - 1).
  else
    pclist = replace(pclist,pcdelimiter + pcdelimiter,pcdelimiter).
end.

return trim(pclist,pcdelimiter) .

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

