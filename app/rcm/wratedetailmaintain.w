&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        :  wratedetailmaintain.p
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Archana Gupta
    Created     :
    Notes       :
    @Modified    :
    Date        Name    Comments
    07/01/2019  Anjly   Modified for Region Maintainance.
    12/18/2019  Anubha  Fixed Flickering issue.
    12/18/2019  Anubha  Modified to show region description on combo
    01/24/2021  Vignesh Fixed the invalid handle issue.
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/
{lib\std-def.i}
{lib\rcm-std-def.i}    
{lib\brw-multi-def.i}

/* Temp-table definitions                                                */
{tt\ratecard.i}
{tt\rateTable.i}
{tt\raterule.i}
{tt\rateui.i}

{tt\ratecard.i  &tablealias=tempRateCard}
{tt\rateTable.i &tablealias=tempRateTable}
{tt\raterule.i  &tablealias=tempRateRule}
{tt\ratelog.i   &tablealias=tempRateLog}

{tt\ratecard.i  &tablealias=tempOutRateCard}
{tt\rateTable.i &tablealias=tempOutRateTable}
{tt\raterule.i  &tablealias=tempOutRateRule}
{tt\ratestate.i &tablealias=tempOutRateState}

{tt\ratecard.i  &tablealias=undoTempRateCard}
{tt\rateTable.i &tablealias=undoTempRateTable}
{tt\raterule.i  &tablealias=undoTempRateRule}

/* Parameter definitions                                                */
define input parameter pCardSetID   as integer   no-undo.
define input parameter pCardID      as integer   no-undo.
define input parameter pTitle       as character no-undo.
define input parameter pStateID     as character no-undo.
define input parameter pVersion     as integer   no-undo.
define input parameter pEffDate     as datetime  no-undo.
define input parameter pVersionDesc as character no-undo.

/* Local variable definitions                                           */
define variable selectedRateTable   as integer   no-undo.
define variable selectedRateRule    as integer   no-undo.
define variable selectedRuleAttr    as character no-undo.
define variable lastRateTable       as integer   no-undo.
define variable lastRateRule        as integer   no-undo.
define variable listRefAttrib       as character no-undo.
                                    
define variable pAttrIDList         as character no-undo.
define variable pRefCardID          as integer   no-undo.                                   
                                    
define variable activeCardID        as integer no-undo.
define variable copyCardID          as integer no-undo.
define variable activeCardSetID     as integer no-undo.
define variable activeStateID       as character no-undo.
define variable activeHandle        as handle  no-undo.
define variable lWhetherClose       as logical no-undo.

define variable cRefCardName        as character no-undo.
define variable cRegionList         as character no-undo.
define variable rateCardList        as character no-undo.
define variable regRateCardList     as character no-undo.
define variable pRegionList         as character no-undo.

define buffer bufrateRule           for rateRule.
                                    
define variable hRateCard           as handle.
define variable fCardID             as integer no-undo.
                                    
define variable ppremium            as decimal no-undo.
define variable pError              as logical no-undo.

define variable pcInputValues       as character no-undo.

/*------get original values -----*/
define variable cbCardNameOrig   as character no-undo.
define variable cbRegionOrig     as character no-undo.
define variable eDescriptionOrig as character no-undo.
define variable eCommentsOrig    as character no-undo.
define variable diffData         as logical   no-undo.
define variable cCardName        as character no-undo.
define variable cAttributeList   as character no-undo.
define variable cWinTitle        as character no-undo.

define temp-table openTestWin no-undo
 field cardSetID as integer
 field cardID    as integer 
 field hInstance as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwRule

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES raterule ratetable

/* Definitions for BROWSE brwRule                                       */
&Scoped-define FIELDS-IN-QUERY-brwRule raterule.active raterule.attrid raterule.seq raterule.ruleName if (raterule.ruleName = "cardRange" or raterule.ruleName = "cardRules") then raterule.refCardName else string(raterule.ruleValue) @ raterule.refCardName if (raterule.refCardName ne "" and (raterule.refAttrName = ?)) then "" else (if raterule.refUseParentAttr = true then "Use Parent" else raterule.refAttrName) @ raterule.refAttrName raterule.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwRule   
&Scoped-define SELF-NAME brwRule
&Scoped-define QUERY-STRING-brwRule for each raterule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq
&Scoped-define OPEN-QUERY-brwRule open query {&SELF-NAME} for each raterule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq .
&Scoped-define TABLES-IN-QUERY-brwRule raterule
&Scoped-define FIRST-TABLE-IN-QUERY-brwRule raterule


/* Definitions for BROWSE brwTable                                      */
&Scoped-define FIELDS-IN-QUERY-brwTable ratetable.stack ratetable.minAmount ratetable.maxAmount ratetable.roundTo if ratetable.calcType = "R" then "Rate" else "Fixed" @ ratetable.calcType if ratetable.calcType = "R" then ratetable.rate else ratetable.fixed @ ratetable.fixed ratetable.per ratetable.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTable   
&Scoped-define SELF-NAME brwTable
&Scoped-define QUERY-STRING-brwTable for each ratetable
&Scoped-define OPEN-QUERY-brwTable open query {&SELF-NAME} for each ratetable .
&Scoped-define TABLES-IN-QUERY-brwTable ratetable
&Scoped-define FIRST-TABLE-IN-QUERY-brwTable ratetable


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwRule}~
    ~{&OPEN-QUERY-brwTable}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bTestCard bPdf cbCardName cbRegion ~
eDescription eComments cbShowAttrib brwRule brwTable bDeleteRule bDownRule ~
bModifyRule bNewRule bNewTable bUpRule RECT-66 
&Scoped-Define DISPLAYED-OBJECTS cbCardName cbRegion eDescription eComments ~
cbShowAttrib 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD convBlank C-Win 
FUNCTION convBlank RETURNS CHARACTER
  (pcString as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetAttribList C-Win 
FUNCTION GetAttribList returns character
  (piCardID as integer) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bDeleteRule  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.13 TOOLTIP "Remove selected rule".

DEFINE BUTTON bDeleteTable  NO-FOCUS
     LABEL "Delete" 
     SIZE 4.6 BY 1.13 TOOLTIP "Remove selected range row".

DEFINE BUTTON bDownRule  NO-FOCUS
     LABEL "Down" 
     SIZE 4.6 BY 1.13 TOOLTIP "Move selected rule down".

DEFINE BUTTON bModifyRule  NO-FOCUS
     LABEL "Modify" 
     SIZE 4.6 BY 1.13 TOOLTIP "Modify selected rule".

DEFINE BUTTON bNewRule  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.13 TOOLTIP "Add a rule".

DEFINE BUTTON bNewTable  NO-FOCUS
     LABEL "New" 
     SIZE 4.6 BY 1.13 TOOLTIP "Add a range row".

DEFINE BUTTON bPdf  NO-FOCUS
     LABEL "PDF" 
     SIZE 7.2 BY 1.7 TOOLTIP "Print Rate Card".

DEFINE BUTTON bSaveAll  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.7 TOOLTIP "Save all changes".

DEFINE BUTTON bTestCard  NO-FOCUS
     LABEL "Test" 
     SIZE 7.2 BY 1.7 TOOLTIP "Test Rate Card".

DEFINE BUTTON bUndo  NO-FOCUS
     LABEL "Undo" 
     SIZE 7.2 BY 1.7 TOOLTIP "Undo all changes".

DEFINE BUTTON bUpRule  NO-FOCUS
     LABEL "Up" 
     SIZE 4.6 BY 1.13 TOOLTIP "Move selected rule up".

DEFINE VARIABLE cbCardName AS CHARACTER 
     LABEL "Name" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN
     SIZE 55.4 BY 1 TOOLTIP "Name of this card when referenced by another card" NO-UNDO.

DEFINE VARIABLE cbRegion AS CHARACTER FORMAT "X(256)" 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE cbShowAttrib AS CHARACTER FORMAT "X(256)":U 
     LABEL "Show Attribute" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 116.8 BY 1.65 NO-UNDO.

DEFINE VARIABLE eDescription AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 116.8 BY 1.65 NO-UNDO.

DEFINE RECTANGLE RECT-66
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30 BY 1.96.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwRule FOR 
      raterule SCROLLING.

DEFINE QUERY brwTable FOR 
      ratetable SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwRule C-Win _FREEFORM
  QUERY brwRule DISPLAY
      raterule.active        column-label "Active"             format "y/n" width 7  view-as toggle-box
raterule.attrid            label "Attribute"                   format "x(20)"      width 19
raterule.seq               label "Seq"                         format "zz9"        width 20
raterule.ruleName          label "Rule"                        format "x(20)"      width 20
if (raterule.ruleName = "cardRange" or raterule.ruleName = "cardRules") then raterule.refCardName else string(raterule.ruleValue) @ raterule.refCardName label "Value" format "x(20)" width 20
if (raterule.refCardName ne "" and (raterule.refAttrName = ?)) then "" else (if raterule.refUseParentAttr = true then "Use Parent" else raterule.refAttrName) @ raterule.refAttrName      label "Reference Attribute"  width 20 format "x(45)"
raterule.description       label "Description"                 format "x(200)"     width 39
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 180.6 BY 7.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwTable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTable C-Win _FREEFORM
  QUERY brwTable DISPLAY
      ratetable.stack                                                           column-label "Stack with!Previous Row"    width 14 view-as toggle-box
ratetable.minAmount                                                             column-label "Coverage!Minimum"           width 28 format ">>>,>>>,>>>,>>9.99" 
ratetable.maxAmount                                                             column-label "Coverage!Maximum"           width 28 format ">>>,>>>,>>>,>>9.99" 
ratetable.roundTo                                                               column-label "Round up!to next"           width 18 format "zzz,zzz" 
if ratetable.calcType = "R" then "Rate" else "Fixed" @ ratetable.calcType              label "Calculation"                width 18 
if ratetable.calcType = "R" then ratetable.rate else ratetable.fixed @ ratetable.fixed label "Value"                      width 13 format "zz,zz9.99999"
ratetable.per                                                                          label "Per"                        width 13 format "zzz,zzz" 
ratetable.comments                                                                     label "Comments"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 180.6 BY 8.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bTestCard AT ROW 1.44 COL 181.4 WIDGET-ID 322 NO-TAB-STOP 
     bPdf AT ROW 1.44 COL 174.2 WIDGET-ID 310 NO-TAB-STOP 
     bUndo AT ROW 1.44 COL 167 WIDGET-ID 278 NO-TAB-STOP 
     cbCardName AT ROW 1.35 COL 13.4 COLON-ALIGNED WIDGET-ID 316
     cbRegion AT ROW 1.35 COL 109.8 COLON-ALIGNED WIDGET-ID 196
     eDescription AT ROW 2.52 COL 15.2 NO-LABEL WIDGET-ID 262
     eComments AT ROW 4.39 COL 15.2 NO-LABEL WIDGET-ID 290
     bSaveAll AT ROW 1.44 COL 159.8 WIDGET-ID 276 NO-TAB-STOP 
     cbShowAttrib AT ROW 6.48 COL 41.6 COLON-ALIGNED WIDGET-ID 320
     brwRule AT ROW 7.65 COL 8.6 WIDGET-ID 600
     brwTable AT ROW 17.17 COL 8.6 WIDGET-ID 400
     bDeleteRule AT ROW 8.7 COL 3.8 WIDGET-ID 184 NO-TAB-STOP 
     bDeleteTable AT ROW 18.26 COL 3.8 WIDGET-ID 172 NO-TAB-STOP 
     bDownRule AT ROW 12 COL 3.8 WIDGET-ID 192 NO-TAB-STOP 
     bModifyRule AT ROW 9.83 COL 3.8 WIDGET-ID 188 NO-TAB-STOP 
     bNewRule AT ROW 7.61 COL 3.8 WIDGET-ID 186 NO-TAB-STOP 
     bNewTable AT ROW 17.13 COL 3.8 WIDGET-ID 170 NO-TAB-STOP 
     bUpRule AT ROW 10.91 COL 3.8 WIDGET-ID 190 NO-TAB-STOP 
     "Description:" VIEW-AS TEXT
          SIZE 11 BY .61 AT ROW 2.65 COL 3.6 WIDGET-ID 264
     "Range" VIEW-AS TEXT
          SIZE 8.2 BY .61 AT ROW 16.17 COL 8.8 WIDGET-ID 200
          FONT 6
     "Comments:" VIEW-AS TEXT
          SIZE 10.2 BY .61 AT ROW 4.52 COL 4.4 WIDGET-ID 292
     "Rules" VIEW-AS TEXT
          SIZE 7 BY .61 AT ROW 6.65 COL 9 WIDGET-ID 204
          FONT 6
     RECT-66 AT ROW 1.35 COL 159.4 WIDGET-ID 274
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.05
         SIZE 190.8 BY 25.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Rate Card"
         HEIGHT             = 25.13
         WIDTH              = 190.8
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwRule cbShowAttrib DEFAULT-FRAME */
/* BROWSE-TAB brwTable brwRule DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bDeleteTable IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwRule:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwRule:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwTable:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwTable:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR BUTTON bSaveAll IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bUndo IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwRule
/* Query rebuild information for BROWSE brwRule
     _START_FREEFORM
open query {&SELF-NAME} for each raterule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwRule */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTable
/* Query rebuild information for BROWSE brwTable
     _START_FREEFORM
open query {&SELF-NAME} for each ratetable .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwTable */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Rate Card */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Rate Card */
DO:
 define variable tOK       as logical.
 define variable ldiffdata as logical.
 run compareData in this-procedure (output ldiffdata) no-error.
  if ldiffdata  then
  do:
    message  "Rate Card has been modified. All changes will be discarded."
      view-as alert-box question buttons ok-cancel set tOK.  
    if not tOK 
      then return no-apply.
    else
    do:
      publish "refreshScreen".
      
      if activeCardID <> 0
       then
        publish "deleteMaintainCards" (input activeCardID).
        
      run emptyDetailWindow in this-procedure (pCardSetID) no-error.
      run EmptyTempTable in this-procedure no-error.           
      /* This event will close the window and terminate the procedure.  */
      apply "CLOSE":U to this-procedure.      
      return no-apply.
    end.
  end.
  else
  do:
    publish "refreshScreen".
    
    if activeCardID <> 0
     then
      publish "deleteMaintainCards" (input activeCardID).
      
    run emptyDetailWindow in this-procedure (pCardSetID) no-error.
    run EmptyTempTable in this-procedure no-error.    
    /* This event will close the window and terminate the procedure.  */
    apply "CLOSE":U to this-procedure.
    return no-apply.
  end.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Rate Card */
DO:
  run windowresized in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteRule C-Win
ON CHOOSE OF bDeleteRule IN FRAME DEFAULT-FRAME /* Delete */
DO:
  run deleteRateRule in this-procedure no-error.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteTable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteTable C-Win
ON CHOOSE OF bDeleteTable IN FRAME DEFAULT-FRAME /* Delete */
DO: 
  run deleteRateTable in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDownRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDownRule C-Win
ON CHOOSE OF bDownRule IN FRAME DEFAULT-FRAME /* Down */
DO:
  run moveRateRuleDown in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bModifyRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bModifyRule C-Win
ON CHOOSE OF bModifyRule IN FRAME DEFAULT-FRAME /* Modify */
DO:
  run modifyRateRule in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewRule C-Win
ON CHOOSE OF bNewRule IN FRAME DEFAULT-FRAME /* New */
DO:  
  run addRateRule in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNewTable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNewTable C-Win
ON CHOOSE OF bNewTable IN FRAME DEFAULT-FRAME /* New */
DO:
  run addRateTable in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPdf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPdf C-Win
ON CHOOSE OF bPdf IN FRAME DEFAULT-FRAME /* PDF */
DO:
  empty temp-table tempRateCard no-error.
  create tempRateCard.
  assign
    tempRateCard.cardSetID        = activeCardSetID  
    tempRateCard.cardID           = activeCardID 
    tempRateCard.cardName         = cbCardName:screen-value  
    tempRateCard.region           = if cbRegion:screen-value = "?" or cbRegion:screen-value = ? then "" else cbRegion:screen-value 
    tempRateCard.description      = eDescription:screen-value     
    tempRateCard.comments         = eComments:screen-value                                                                            
    .  
  run rateCardPdf.p(input activeCardID,
                    input pStateID,
                    input pVersion,
                    input pVersionDesc,
                    input pEffDate, 
                    input table tempRateCard,                        
                    input table Ratetable,                                                     
                    input table RateRule) no-error.
  empty temp-table tempRateCard no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRule
&Scoped-define SELF-NAME brwRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRule C-Win
ON LEFT-MOUSE-UP OF brwRule IN FRAME DEFAULT-FRAME
DO:
  find current raterule no-error.
  if available raterule then
    brwRule:tooltip = raterule.comment.
  else 
    brwRule:tooltip = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRule C-Win
ON ROW-DISPLAY OF brwRule IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRule C-Win
ON VALUE-CHANGED OF brwRule IN FRAME DEFAULT-FRAME
DO:
 define variable ldiffdata as logical.

 run setNavigation in this-procedure no-error.
 run compareData in this-procedure (output ldiffdata) no-error.
 if ldiffdata then
 assign
   bSaveAll:sensitive in frame default-frame = true
   bUndo   :sensitive in frame default-frame = true.

 find current rateRule no-error.
 if available rateRule then
  assign
     selectedRuleAttr = rateRule.attrid 
     selectedRateRule = rateRule.seq.


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTable
&Scoped-define SELF-NAME brwTable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTable C-Win
ON ROW-DISPLAY OF brwTable IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTable C-Win
ON VALUE-CHANGED OF brwTable IN FRAME DEFAULT-FRAME
DO:
  define variable ldiffdata as logical.
  find current ratetable no-error.

  if not available ratetable then
    bDeleteTable:sensitive = false.    
  else
  do:
    selectedRateTable = ratetable.seq.
    if selectedRateTable ne lastRateTable then
      bDeleteTable:sensitive = false.
    else
      bDeleteTable:sensitive = true.
  end.

  run compareData in this-procedure(output ldiffdata) no-error.
  if ldiffdata  then
  assign
    bSaveAll:sensitive in frame default-frame = true
    bUndo   :sensitive in frame default-frame = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSaveAll
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSaveAll C-Win
ON CHOOSE OF bSaveAll IN FRAME DEFAULT-FRAME /* Save */
DO:            
  run saveCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bTestCard
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bTestCard C-Win
ON CHOOSE OF bTestCard IN FRAME DEFAULT-FRAME /* Test */
DO:
  run testCard in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUndo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUndo C-Win
ON CHOOSE OF bUndo IN FRAME DEFAULT-FRAME /* Undo */
DO:
  if pTitle ne "New" then
    run setScreen in this-procedure(input-output activeCardSetID,
                                    input-output activeCardID,
                                    input pTitle,
                                    input pStateID,
                                    input pVersion,
                                    input pEffDate,
                                    input pVersionDesc,
                                    input table undoTempRateCard,
                                    input table undoTempRateTable,
                                    input table undoTempRateRule) no-error.
  else
    run cleanScreen in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bUpRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bUpRule C-Win
ON CHOOSE OF bUpRule IN FRAME DEFAULT-FRAME /* Up */
DO:  
  run moveRateRuleUp in this-procedure no-error.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbCardName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbCardName C-Win
ON VALUE-CHANGED OF cbCardName IN FRAME DEFAULT-FRAME /* Name */
DO:
  define variable ldiffdata as logical.
  run compareData in this-procedure (output ldiffdata) no-error.
  if ldiffdata  then
  assign
    bSaveAll:sensitive in frame default-frame = true
    bUndo   :sensitive in frame default-frame = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRegion C-Win
ON VALUE-CHANGED OF cbRegion IN FRAME DEFAULT-FRAME /* Region */
DO:
  cCardName = cbCardName:screen-value.
  
  find first region where region.regioncode = cbRegion:input-value no-error.
  
  cbRegion:tooltip = if available region then region.description else "".
  
  lWhetherClose = false.

  if not (cbRegion:screen-value = "" or cbRegion:screen-value = "?" or cbRegion:screen-value = ?) 
   then
    do:
      publish "GetRateCardList" (input  cbRegion:screen-value,
                                 input  if pTitle = "Edit" then activeCardID else 0,
                                 input  if pTitle = "Edit" then activeCardSetID else 0,
                                 output regRateCardList).
    
      if pTitle <> "Edit" then
        assign 
          cbCardName:list-items   = regRateCardList
          cbCardName:screen-value = cCardName.
    end.
  else
  do:
    if pTitle <> "Edit" then
      assign 
        cbCardName:list-items   = rateCardList
        cbCardName:screen-value = cCardName.
  end.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbShowAttrib
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbShowAttrib C-Win
ON VALUE-CHANGED OF cbShowAttrib IN FRAME DEFAULT-FRAME /* Show Attribute */
DO:
  if cbShowAttrib:screen-value = "Show All" then
    open query brwRule for each rateRule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq.
  else
    open query brwRule for each rateRule where raterule.cardid = activecardid and (rateRule.attrid = (if cbShowAttrib:screen-value = "?" or cbShowAttrib:screen-value = ? then "" else cbShowAttrib:screen-value)
      or rateRule.attrid = {&PreAll} or rateRule.attrid = {&PostAll}) by rateRule.attrid by rateRule.seq.

  apply 'value-changed' to brwRule.
  apply 'left-mouse-up' to brwRule.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eComments C-Win
ON VALUE-CHANGED OF eComments IN FRAME DEFAULT-FRAME
DO:
  define variable ldiffdata as logical.
  run compareData in this-procedure (output ldiffdata) no-error.
  if ldiffdata  then
  assign
    bSaveAll:sensitive in frame default-frame = true
    bUndo   :sensitive in frame default-frame = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME eDescription
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL eDescription C-Win
ON VALUE-CHANGED OF eDescription IN FRAME DEFAULT-FRAME
DO:
  define variable ldiffdata as logical.
  run compareData in this-procedure (output ldiffdata) no-error.
  if ldiffdata  then
  assign
    bSaveAll:sensitive in frame default-frame = true
    bUndo   :sensitive in frame default-frame = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRule
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main-multi.i &browse-list="brwTable,brwRule"}

subscribe to "CloseDetailWindow"    anywhere.
subscribe to "GetNextAttribSeq"     anywhere.
subscribe to "validateRateRuleData" anywhere.
subscribe to "OpenMaintainTestWin"  anywhere.
subscribe to "emptyDetailWindow"    anywhere.
subscribe to "delMaintTestWin"      anywhere.

do with frame {&frame-name}:
end.

run EmptyTempTable in this-procedure no-error. 

activeCardID     = if pTitle = "Edit" then pCardID else 0.
activeCardSetID  = pCardSetID. 
activeHandle     = this-procedure.
copycardid       = pCardID.

if pTitle ne "new" then
 publish "GetRateCardDetail" (input activeCardSetID,
                              input if pTitle = "Edit" then activeCardID else copycardid,
                              output table rateCard,
                              output table ratetable,
                              output table rateRule). 
if pTitle eq "Edit" then
  publish "OpenMaintainCards" (activeCardID, this-procedure) .

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.

run setButtonImages in this-procedure no-error.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

for each bufrateRule where bufrateRule.cardID = activeCardID by bufrateRule.seq:
  lastRateRule = bufrateRule.seq.
end.

cWinTitle = {&window-name}:title.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.

  publish "GetRateCardList" (input "",
                             input if pTitle = "Edit" then activeCardID else 0,
                             input activeCardSetID,
                             output ratecardlist).
  
  cbCardName:list-items in frame {&frame-name} = rateCardList.
  publish "GetRateStateValue"(input activeCardSetID,
                           input "StateID",
                           output activeStateID).
                           
  if activeStateID <> "" and activeStateID <> "?" 
   then
    do:
       publish "GetRegionList" (activeStateID,
                                "Region",
                                output pRegionlist).
                                
       publish "GetRateStateRegions" (activeStateID,
                                      "Region",
                                      output table region).                          
                               
       cbRegion:list-items in frame {&frame-name} = pregionList.
    end.
  
  for each rateCard:
    create undoTempRateCard.
    buffer-copy rateCard to undoTempRateCard.
  end.
  for each rateTable:
    create undoTempRateTable.
    buffer-copy rateTable to undoTempRateTable.
  end.
  for each rateRule:
    create undoTempRateRule.
    buffer-copy rateRule to undoTempRateRule.
  end.

  run setScreen in this-procedure(input-output activeCardSetID,
                                  input-output activeCardID,
                                  input pTitle,
                                  input pStateID,
                                  input pVersion,
                                  input pEffDate,
                                  input pVersionDesc,
                                  input table ratecard,
                                  input table rateTable,
                                  input table rateRule) no-error.

  assign
    cbCardNameOrig    = cbCardName:screen-value           in frame {&frame-name}                            
    cbRegionOrig      = cbRegion:screen-value             in frame {&frame-name}                              
    eDescriptionOrig  = eDescription:screen-value         in frame {&frame-name}                          
    eCommentsOrig     = eComments:screen-value            in frame {&frame-name}.  

  on 'value-changed':U anywhere 
  do:
    run compareData in this-procedure (output diffData) no-error.
    if diffData then
      assign
        bSaveAll:sensitive in frame DEFAULT-FRAME = true
        bUndo:sensitive in frame DEFAULT-FRAME = true.
    else 
    assign
      bSaveAll:sensitive in frame DEFAULT-FRAME = false
      bUndo:sensitive in frame DEFAULT-FRAME = false.
  end.

  {&window-name}:window-state = 3.
  
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addRateRule C-Win 
PROCEDURE addRateRule :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define buffer bufrateRule for rateRule.
 define variable rateRuleRowId as rowid     no-undo.
 define variable cAttrList     as character no-undo.
 define variable cSetAttr      as character no-undo.
 define variable loNewAttr     as logical   no-undo.

 do with frame {&frame-name}:
 end.
 
 empty temp-table tempRateRule no-error.

 find current rateRule no-error.
 if available rateRule then
   rateRuleRowId = rowid(rateRule).
 
 for last bufrateRule 
   where bufrateRule.cardID = activeCardID by bufrateRule.seq:
   lastRateRule = bufrateRule.seq.
 end.

 cAttrList = getAttribList(activeCardID) no-error.

 run dialognewrule.w(input cbRegion:screen-value,
                     input pCardSetID,
                     input cbCardName:screen-value,
                     input 0,
                     input if can-find(first rateRule where rateRule.cardId = activeCardID and rateRule.attrID = {&PreAll}) then cAttrList + ",PreALL" 
                           else if can-find(first rateRule where rateRule.cardId = activeCardID and rateRule.attrID = {&PostAll}) then cAttrList + ",PostALL"
                           else cAttrList, 
                     input ratecardlist, 
                     input-output table tempRateRule) no-error. 
 
 find first tempRateRule no-error.
 if available tempRateRule 
  then
   do:
     if not can-find(first rateRule where rateRule.cardID = activeCardID)
         or ((tempRateRule.attrid <> {&PreAll} and tempRateRule.attrid <> {&PostAll})
             and not can-find(first rateRule where rateRule.cardID = activeCardID
                           and rateRule.attrid = tempRateRule.attrid)) then
       loNewAttr = true.

     create rateRule.
     buffer-copy tempRateRule except cardID to rateRule.
     rateRule.cardID  = activeCardID.
   end.
   else
     return.

 if loNewAttr then
 do:
   cSetAttr = cbShowAttrib:screen-value.
 
   run refreshAttrDrpDn in this-procedure no-error.
   cbShowAttrib:screen-value = cSetAttr. 
 end.
 
 if cbShowAttrib:screen-value = "Show All" then
   open query brwRule for each rateRule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq.
 else
    open query brwRule for each rateRule where raterule.cardid = activecardid and (rateRule.attrid = (if cbShowAttrib:screen-value = "?" or cbShowAttrib:screen-value = ? then "" else cbShowAttrib:screen-value)
     or rateRule.attrid = {&PreAll} or rateRule.attrid = {&PostAll}) by rateRule.attrid by rateRule.seq.
 
  if rateRuleRowId <> ? then
    reposition brwRule to rowid rateRuleRowId no-error.
 
 apply 'value-changed' to brwRule.
 apply 'left-mouse-up' to brwRule.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE addRateTable C-Win 
PROCEDURE addRateTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable rateTableRowId as rowid   no-undo.
 define variable lastMaxAmnt    as decimal no-undo.

 do with frame {&frame-name}:
 end.
 
 for last ratetable where ratetable.cardID = activeCardID by ratetable.seq:
   lastRateTable = ratetable.seq.
   lastMaxAmnt = ratetable.MaxAmount.
 end.
 
 if lastMaxAmnt ne 0.0  then
   run dialognewtable.w(input cbCardName:screen-value,
                        input lastRateTable + 1,
                        input lastMaxAmnt + 0.01,
                        output table tempRateTable) no-error.  
 else
   run dialognewtable.w(input cbCardName:screen-value,
                input 1,
                input 0.0,
                output table tempRateTable) no-error.  
  
 find first tempRateTable no-error.
 if available tempRateTable 
  then
   do:
     create ratetable.
     buffer-copy tempRateTable to ratetable.
     ratetable.cardID  = activeCardID.
     rateTableRowID = rowid(ratetable) no-error.
   end.

 open query brwTable for each ratetable.
 reposition brwTable to rowid rateTableRowID no-error.
 
 for last ratetable where ratetable.cardID = activeCardID by ratetable.seq:
   lastRateTable = ratetable.seq.
 end.

 apply 'value-changed' to brwTable.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cleanScreen C-Win 
PROCEDURE cleanScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.

 assign
   cbCardName:screen-value   = ""
   cbRegion:screen-value     = ?
   eDescription:screen-value = ""
   eComments:screen-value    = "".

 empty temp-table ratetable no-error.
 empty temp-table raterule no-error.
 
 open query brwTable    for each ratetable.

 run refreshAttrDrpDn in this-procedure no-error.  
 apply 'value-changed' to cbShowAttrib.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clientValidations C-Win 
PROCEDURE clientValidations :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter errorStatus as logical   no-undo. 
  define output parameter errorMsg    as character no-undo. 

  define buffer bufRateRule for raterule.

  do with frame {&frame-name}:
  end.

  if cbCardName:screen-value = ""  or cbCardName:screen-value = "?" or cbCardName:screen-value = ? 
   then 
    do:
     errorMsg    = "Please Select a Card Name.".
     errorStatus = true.
     return.
    end.

  if can-find(first RateRule where RateRule.cardID =  activecardid and RateRule.refCardName = cbCardName:screen-value) 
   then 
    do:
     errorMsg    = "Card cannot be Self-Referenced.".
     errorStatus = true.
     return.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeDetailWindow C-Win 
PROCEDURE closeDetailWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pCardID as integer no-undo.

 if activeCardID = pCardID 
  then
   do:
     run EmptyTempTable in this-procedure no-error.
     /* This event will close the window and terminate the procedure.  */
     apply "CLOSE":U to this-procedure.
     return no-apply.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE compareData C-Win 
PROCEDURE compareData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter pDiffData        as logical no-undo.

define variable inumrecordsratetable     as integer no-undo.
define variable inumrecordsundoratetable as integer no-undo.
define variable inumrecordsraterule      as integer no-undo.
define variable inumrecordsundoraterule  as integer no-undo.

do with frame {&frame-name}:
end.

if cbCardNameOrig ne cbCardName:screen-value then
  pDiffData = true.
if cbRegionOrig ne cbRegion:screen-value then
  pDiffData = true.
if eDescriptionOrig ne eDescription:screen-value then
  pDiffData = true.
if eCommentsOrig ne eComments:screen-value then
  pDiffData = true.

for each ratetable:
  inumrecordsratetable = inumrecordsratetable + 1.
  for each undotempRateTable where RateTable.ratetableID   eq  undotempRateTable.ratetableID :
     if  (RateTable.seq           ne   undotempRateTable.seq          or
          RateTable.minAmount     ne   undotempRateTable.minAmount    or
          RateTable.maxAmount     ne   undotempRateTable.maxAmount    or
          RateTable.roundTo       ne   undotempRateTable.roundTo      or
          RateTable.calcType      ne   undotempRateTable.calcType     or
          RateTable.stack         ne   undotempRateTable.stack        or
          RateTable.rate          ne   undotempRateTable.rate         or
          RateTable.per           ne   undotempRateTable.per          or
          RateTable.fixed         ne   undotempRateTable.fixed        or
          RateTable.comments      ne   undotempRateTable.comments       )
          then
    pDiffData = true.
  end.
  if RateTable.ratetableID = 0 and pTitle ne "copy"  then
    pDiffData = true.
end.

for each undotempRateTable:
  inumrecordsundoratetable = inumrecordsundoratetable + 1.
end.

for each raterule:
  inumrecordsraterule = inumrecordsraterule + 1.
  for each undotempRateRule where RateRule.RateRuleID eq undotempRateRule.RateRuleID :
    if (RateRule.RateRuleID          eq undotempRateRule.RateRuleID        and
         (RateRule.seq                ne undotempRateRule.seq               or
          RateRule.ruleName           ne undotempRateRule.ruleName          or
          RateRule.ruleValue          ne undotempRateRule.ruleValue         or
          RateRule.active             ne undotempRateRule.active            or    
          RateRule.refCardName        ne undotempRateRule.refCardName       or
          RateRule.refUseParentAttr   ne undotempRateRule.refUseParentAttr  or
          RateRule.refAttrName        ne undotempRateRule.refAttrName       or
          RateRule.description        ne undotempRateRule.description       or
          RateRule.comments           ne undotempRateRule.comments)           )  
         then
      pDiffData = true.
  end.
  if RateRule.RateRuleID = 0 and pTitle ne "copy" then
    pDiffData = true.
end.

for each undotempRateRule:
  inumrecordsundoraterule = inumrecordsundoraterule + 1.
end.
if inumrecordsraterule ne inumrecordsundoraterule then
   pDiffData = true.

if inumrecordsratetable ne inumrecordsundoratetable then
   pDiffData = true.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRateRule C-Win 
PROCEDURE deleteRateRule :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.

 define variable tOK                 as  logical   no-undo.
 define variable initialcbattrvalue  as  character no-undo.
 define buffer   bufrateRule for rateRule.

 message "Highlighted function will be deleted. Are you sure? "
   view-as alert-box question buttons ok-cancel set tOK.

 if not tOK 
   then return error.
 else
   find rateRule where rateRule.cardid = activeCardID and rateRule.attrID = selectedRuleAttr and rateRule.seq = selectedRateRule no-error.
 if available rateRule then
   delete rateRule.
 initialcbattrvalue = cbShowAttrib:screen-value.
 for each rateRule where rateRule.cardid = activeCardID 
     and rateRule.attrID = selectedRuleAttr and
      rateRule.seq gt selectedRateRule :
   rateRule.seq = rateRule.seq - 1.
 end.

run refreshAttrDrpDn in this-procedure no-error.  
cbShowAttrib:screen-value  = if lookup(initialcbattrvalue, cbShowAttrib:list-items) ne 0 then initialcbattrvalue else "Show All" no-error.

 apply 'value-changed' to cbShowAttrib.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE deleteRateTable C-Win 
PROCEDURE deleteRateTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 do with frame {&frame-name}:
 end.
 define variable tOK as logical no-undo.
 
 message "Highlighted range will be deleted. Are you sure?"
   view-as alert-box question buttons ok-cancel set tOK.
 
 if not tOK 
   then return error.
 else
   find ratetable where ratetable.cardid = activeCardID and ratetable.seq = selectedRateTable no-error.
   if available ratetable then
     delete ratetable.
 
 open query brwTable for each ratetable.

 for last ratetable where ratetable.cardID = activeCardID by ratetable.seq:
   lastRateTable = ratetable.seq.
 end.
 
 apply 'value-changed' to brwTable.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE delMaintainTestWin C-Win 
PROCEDURE delMaintainTestWin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pCardSetID as integer no-undo.
define input parameter pCardID    as integer no-undo.

for each openTestWin where openTestWin.cardSetID = pCardSetID and openTestWin.cardid = pCardID:
  delete openTestWin.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyDetailWindow C-Win 
PROCEDURE emptyDetailWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter ipCardSetID as integer no-undo.

if ipCardSetID  = pCardSetID  
then
do:
  publish "emptyRateLogWin" (input pCardSetID ,
                             input pCardID). 
                             
  publish "deleteMaintainCards" (input pCardID).
  
  for each openTestWin where cardSetID = ipCardSetID and
                             cardID    = pCardID :
    if valid-handle(openTestWin.hInstance) then
    delete object openTestWin.hInstance no-error.
  end.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyTempTable C-Win 
PROCEDURE emptyTempTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
empty temp-table ratecard no-error.
empty temp-table ratetable no-error.
empty temp-table raterule no-error.

empty temp-table undoTempRateCard no-error.    
empty temp-table undoTempRateTable no-error.    
empty temp-table undoTempRateRule no-error.    

empty temp-table tempRateCard no-error.    
empty temp-table tempRateTable no-error.     
empty temp-table tempRateRule no-error.  

empty temp-table tempOutRateCard no-error.    
empty temp-table tempOutRateTable no-error.    
empty temp-table tempOutRateRule no-error.  

empty temp-table tempRateLog no-error. 

for each openTestWin where openTestWin.cardSetID = pCardSetID and openTestWin.cardid = pcardID:
  if valid-handle(openTestWin.hInstance) then
    delete object openTestWin.hInstance no-error.
end.
empty temp-table openTestWin no-error.
  
                      
assign
  selectedRateTable = 0
  selectedRateRule  = 0
  lastRateTable     = 0
  lastRateRule      = 0
  pRefCardID        = 0
  activeCardID      = 0
  copyCardID        = 0
  activeCardSetID   = 0
  fCardID           = 0
  listRefAttrib     = ""
  pAttrIDList       = ""
  cRefCardName      = ""
  cRegionList       = ""
  rateCardList      = ""
  regRateCardList   = ""
  ppremium          = 0
  pError            = false
  hRateCard         = ?
  activeHandle      = ?
  cRefCardName      = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbCardName cbRegion eDescription eComments cbShowAttrib 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bTestCard bPdf cbCardName cbRegion eDescription eComments cbShowAttrib 
         brwRule brwTable bDeleteRule bDownRule bModifyRule bNewRule bNewTable 
         bUpRule RECT-66 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetNextAttribSeq C-Win 
PROCEDURE GetNextAttribSeq :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter pAttrName   as character no-undo.
 define output parameter pRowNo      as integer   no-undo.
 
 define buffer bufrateRule for rateRule.
 define variable iAttrSeq as integer no-undo.
 
 for each bufrateRule
   where bufrateRule.cardID = activeCardID 
     and bufrateRule.attrID = pAttrName by bufrateRule.seq:
   pRowNo = bufrateRule.seq.
 end.
 
 pRowNo = pRowNo + 1.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyRateRule C-Win 
PROCEDURE modifyRateRule :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable rateRuleRowId as rowid no-undo.

do with frame {&frame-name}:
end.

empty temp-table tempRateRule no-error.

find first rateRule where rateRule.cardID = activeCardID and rateRule.attrid = selectedRuleAttr and rateRule.seq = selectedRateRule no-error. 
if available rateRule then
  buffer-copy rateRule to tempRateRule no-error.

find first tempRateRule no-error.
if available tempRateRule then
  run dialognewrule.w(input cbRegion:screen-value,
                      input pCardSetID,
                      input cbCardName:screen-value,   
                      input selectedRateRule,
                      input tempRateRule.attrid,
                      input rateCardList,
                      input-output table tempRateRule) no-error.   

find first tempRateRule no-error.
if available tempRateRule then
  find first rateRule 
    where rateRule.cardID = activeCardID 
      and rateRule.attrid = selectedRuleAttr
      and rateRule.seq = selectedRateRule no-error.

if available rateRule then
do:         
  buffer-copy tempRateRule except seq to rateRule no-error.
  rateRuleRowId = rowid(rateRule) no-error.
end.


if cbShowAttrib:screen-value = "Show All" then
  open query brwRule for each rateRule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq.
else
  open query brwRule for each rateRule where raterule.cardid = activecardid and (rateRule.attrid = (if cbShowAttrib:screen-value = "?" or cbShowAttrib:screen-value = ? then "" else cbShowAttrib:screen-value)
    or rateRule.attrid = {&PreAll} or rateRule.attrid = {&PostAll}) by rateRule.attrid by rateRule.seq.

if rateRuleRowId <> ? then
  reposition brwRule to rowid rateRuleRowId no-error.

apply 'value-changed' to brwRule.
apply 'left-mouse-up' to brwRule.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE moveRateRuleDown C-Win 
PROCEDURE moveRateRuleDown :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufRateRule for RateRule.
  define variable rRuleID   as rowid no-undo.

  do with frame {&frame-name}:
  end.

  for each bufRateRule
    where bufRateRule.cardId = activeCardID 
      and bufRateRule.attrId = selectedRuleAttr by bufRateRule.seq:
    lastRateRule = bufRateRule.seq.
  end. 

  /* Return if the highest sequence row of an attribute is currently selected, cannot move this record down.*/
  if lastRateRule = selectedRateRule then return.

  /* updating selected record with seq 99999.*/
  find bufRateRule where bufRateRule.cardid = activeCardID 
      and bufRateRule.attrId = selectedRuleAttr 
      and bufRateRule.seq = selectedRateRule no-error.
  if available bufRateRule then
    assign bufRateRule.seq = 99999.

   /* Modifying the record that is in next row of selected record and decrease its sequence by 1*/
  find RateRule where RateRule.cardid = activeCardID 
      and RateRule.attrId = selectedRuleAttr
      and RateRule.seq    = selectedRateRule + 1 no-error.
  if available RateRule then
      assign RateRule.seq = selectedRateRule.

  /* Modifying the selected record and increase its sequence by 1*/
  find RateRule where RateRule.cardid = activeCardID and RateRule.seq = 99999 no-error.
  if available RateRule then
    assign RateRule.seq = selectedRateRule + 1
           rRuleId      = rowid(RateRule) no-error.

  if cbShowAttrib:screen-value = "Show All" then
    open query brwRule for each rateRule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq.
  else
    open query brwRule for each rateRule where raterule.cardid = activecardid and (rateRule.attrid = (if cbShowAttrib:screen-value = "?" or cbShowAttrib:screen-value = ? then "" else cbShowAttrib:screen-value)
    or rateRule.attrid = {&PreAll} or rateRule.attrid = {&PostAll}) by rateRule.attrid by rateRule.seq.
  if brwRule:GET-REPOSITIONED-ROW( )  = 1 then
    brwRule:set-repositioned-row(7,"ALWAYS").

  reposition brwRule to rowid rRuleId no-error.
  apply 'value-changed' to brwRule.
  apply 'left-mouse-up' to brwRule.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE moveRateRuleUp C-Win 
PROCEDURE moveRateRuleUp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer bufRateRule for RateRule.
  define variable rRuleID   as rowid no-undo.

  do with frame {&frame-name}:
  end.

  /* If first sequence of attribute is selected, it cannot be moved above further. */
  if selectedRateRule = 1 then
    return.

  /* updating selected record with seq 100000.*/
  find bufRateRule 
      where bufRateRule.cardid = activeCardID 
        and bufRateRule.attrId = selectedRuleAttr 
        and bufRateRule.seq    = selectedRateRule no-error.
  if available bufRateRule then
    assign bufRateRule.seq = 100000.

  /* Modifying the record that is in prev row of selected record and increase its sequence by 1*/
  find RateRule
    where RateRule.cardid = activeCardID
      and RateRule.attrId = selectedRuleAttr 
      and RateRule.seq    = selectedRateRule - 1 no-error.
  if available RateRule then
    assign RateRule.seq = selectedRateRule.

  /* Modifying the selected record and decrease its sequence by 1*/
  find RateRule 
    where RateRule.cardid = activeCardID 
      and RateRule.attrId = selectedRuleAttr
      and RateRule.seq = 100000 no-error.
  if available RateRule then
    assign RateRule.seq = selectedRateRule - 1
           rRuleId      = rowid(RateRule) no-error.

  if cbShowAttrib:screen-value = "Show All" then
    open query brwRule for each rateRule where raterule.cardid = activecardid by rateRule.attrid by rateRule.seq.
  else
    open query brwRule for each rateRule where raterule.cardid = activecardid and (rateRule.attrid = (if cbShowAttrib:screen-value = "?" or cbShowAttrib:screen-value = ? then "" else cbShowAttrib:screen-value)
      or rateRule.attrid = {&PreAll} or rateRule.attrid = {&PostAll}) by rateRule.attrid by rateRule.seq.

  if brwRule:GET-REPOSITIONED-ROW( )  = 7 then
    brwRule:set-repositioned-row(6,"ALWAYS").

  reposition brwRule to rowid rRuleId no-error.
  apply 'value-changed' to brwRule.
  apply 'left-mouse-up' to brwRule.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE OpenMaintainTestWin C-Win 
PROCEDURE OpenMaintainTestWin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter pCardSetID as integer  no-undo.
 define input parameter pCardID    as integer  no-undo.
 define input parameter pWindow    as handle   no-undo.

 find first openTestWin where openTestWin.cardSetID = pCardSetID and 
                              openTestWin.cardID = pcardID       and 
                              openTestWin.hInstance = pWindow    no-error.
 if not available openTestWin 
  then
   do:
     create openTestWin.
     assign
       openTestWin.cardSetID = pCardSetID
       openTestWin.cardID    = pCardID
       openTestWin.hInstance = pWindow.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAttrDrpDn C-Win 
PROCEDURE refreshAttrDrpDn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  cAttributeList = "".
 
  do with frame {&frame-name}:
  end.
 
  cAttributeList = getAttribList(activeCardID) no-error.

  if cAttributeList <> "" 
    or can-find(first rateRule
                where rateRule.cardId = activeCardID
                 and (rateRule.attrID = {&PreAll} 
                  or rateRule.attrID = {&PostAll}  
                  or rateRule.attrID = ""))  then
    assign 
      cAttributeList            = "Show All," + cAttributeList
      cbShowAttrib:list-items   = cAttributeList
      cbShowAttrib:sensitive    = true.
  else
    assign 
      cAttributeList            = "Show All"
      cbShowAttrib:list-items   = cAttributeList
      cbShowAttrib:sensitive    = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveCard C-Win 
PROCEDURE saveCard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define variable errorMsg    as character no-undo.
define variable errorStatus as logical   no-undo.
define variable pSuccess    as logical   no-undo.
    
do with frame {&frame-name}:
end.

  
run ClientValidations in this-procedure(output errorStatus,
                                        output errorMsg) no-error.

if errorStatus = yes 
 then
  do:
    message errorMsg
      view-as alert-box info buttons ok.
    assign
      bSaveAll:sensitive = false.
    return no-apply.
  end.

empty temp-table tempRateCard  no-error.
empty temp-table tempRatetable no-error.
empty temp-table tempRateRule  no-error.

do with frame DEFAULT-FRAME:
end.

if pTitle eq "Copy" then 
  activeCardID = 0.

create tempRateCard.
assign
  tempRateCard.cardSetID        = activeCardSetID  
  tempRateCard.cardID           = activeCardID 
  tempRateCard.cardName         = cbCardName:screen-value  
  tempRateCard.region           = if cbRegion:screen-value = "?" or cbRegion:screen-value = ? then ""    else cbRegion:screen-value 
  tempRateCard.description      = eDescription:screen-value                                                                                         
  tempRateCard.comments         = eComments:screen-value                                                                            
  .                                                              
                                                                                                      
for each ratetable where ratetable.cardID = activecardid :                                     
  create tempRatetable.
  buffer-copy ratetable except cardID to tempRatetable no-error.
  assign tempRatetable.cardID = activeCardID.
end.

for each RateRule where RateRule.cardID =  activecardid:
  create tempRateRule.
  buffer-copy RateRule except cardID to tempRateRule no-error.
  assign tempRateRule.cardID = activeCardID.
end.

if pTitle = "Edit" then
  publish "ModifyRateCardDetail"(input  table tempRateCard,                        
                                 input  table tempRatetable,                                                      
                                 input  table tempRateRule,
                                 output table tempoutrateCard,  
                                 output table tempoutratetable,
                                 output table tempoutrateRule,
                                 output table tempoutratestate,
                                 output pSuccess).
else
  publish "AddRateCardDetail"(input  table tempRateCard,                        
                              input  table tempRatetable,                                                      
                              input  table tempRateRule,
                              output table tempoutrateCard,  
                              output table tempoutratetable,
                              output table tempoutrateRule,
                              output table tempoutratestate,
                              output pSuccess).
                              
if not pSuccess then
  return.

empty temp-table undoTempRateCard no-error.
empty temp-table undoTempRateTable no-error.
empty temp-table undoTempRateRule no-error.

for each tempoutrateCard:
  create undoTempRateCard.
  buffer-copy tempoutrateCard to undoTempRateCard no-error.
end.

for each tempoutratetable:
  create undoTempRateTable.
  buffer-copy tempoutratetable to undoTempRateTable no-error.
end.

for each tempoutrateRule:
  create undoTempRateRule.
  buffer-copy tempoutrateRule to undoTempRateRule no-error.
end.

assign
  cbCardNameOrig    = cbCardName:screen-value in frame {&frame-name}                            
  cbRegionOrig      = cbRegion:screen-value in frame {&frame-name}                              
  eDescriptionOrig  = eDescription:screen-value in frame {&frame-name}                          
  eCommentsOrig     = eComments:screen-value in frame {&frame-name}. 

for first tempoutrateCard:
  activeCardID = tempoutratecard.cardID.
end.

run setScreen  in this-procedure(input-output activeCardSetID,
                                 input-output activeCardID,
                                 input pTitle,
                                 input pStateID,
                                 input pVersion,
                                 input pEffDate,
                                 input pVersionDesc,
                                 input table tempoutrateCard,
                                 input table tempoutratetable,
                                 input table tempoutrateRule) no-error.

publish "refreshScreen"(pTitle,
                        input activeCardSetID).

if pTitle = "New" or pTitle = "Copy" 
 then
  do:
    publish "OpenMaintainCards" (activeCardID, activeHandle) .
    cbCardName:sensitive = false.
    cbRegion  :sensitive = false.
    pTitle = "Edit".
    bTestCard:sensitive = true. 
  end.

publish "refreshState".

empty temp-table tempRateCard no-error.
empty temp-table tempRatetable no-error.
empty temp-table tempRateRule no-error.

apply 'value-changed' to brwTable.
apply 'value-changed' to brwRule.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setButtonImages C-Win 
PROCEDURE setButtonImages :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.

bTestCard:load-image("images/calc.bmp").
bTestCard:load-image-insensitive("images/calc-i.bmp").

bNewTable:load-image("images/s-add.bmp").
bNewTable:load-image-insensitive("images/s-add-i.bmp").

bNewRule:load-image("images/s-add.bmp").
bNewRule:load-image-insensitive("images/s-add-i.bmp").

bDeleteTable:load-image("images/s-delete.bmp").
bDeleteTable:load-image-insensitive("images/s-delete-i.bmp").

bDeleteRule:load-image("images/s-delete.bmp").
bDeleteRule:load-image-insensitive("images/s-delete-i.bmp").

bModifyRule:load-image("images/s-update.bmp").
bModifyRule:load-image-insensitive("images/s-update-i.bmp").

bUpRule:load-image("images/s-up.bmp").
bUpRule:load-image-insensitive("images/s-up-i.bmp").

bDownRule:load-image("images/s-down.bmp").
bDownRule:load-image-insensitive("images/s-down-i.bmp").

bSaveAll:load-image("images/save.bmp").
bSaveAll:load-image-insensitive("images/save-i.bmp").

bUndo:load-image("images/erase.bmp").
bUndo:load-image-insensitive("images/erase-i.bmp").

bPdf:load-image ("images/pdf.bmp") .
bPdf:load-image-insensitive("images/pdf-i.bmp").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setNavigation C-Win 
PROCEDURE setNavigation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
do with frame {&frame-name}:
end.
define buffer bRateRule for RateRule.
find current rateRule no-error.
if available raterule 
 then
  do:
    selectedRateRule = raterule.seq.
    selectedRuleAttr = raterule.attrid.
    bDeleteRule:sensitive = true.      
    bModifyRule:sensitive = true.
    if can-find(first bRateRule where bRateRule.attrid = rateRule.attrid
      and bRateRule.seq < rateRule.seq) then
      bUpRule:sensitive = true.
    else 
      bUpRule:sensitive = false.
    
    if  can-find(first bRateRule where bRateRule.attrid = rateRule.attrid
      and bRateRule.seq > rateRule.seq) then
      bDownRule:sensitive = true.
    else 
      bDownRule:sensitive = false.
  end.
else
do:
  bDeleteRule:sensitive = false.
  bModifyRule:sensitive = false.
  bUpRule:sensitive     = false.
  bDownRule:sensitive   = false.
  lastRateRule = 0.
end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setScreen C-Win 
PROCEDURE setScreen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input-output parameter activeCardSetID as integer.
  define input-output parameter activeCardID    as integer.
  
  define input parameter pTitle       as character no-undo.
  define input parameter pStateID     as character no-undo.
  define input parameter pVersion     as integer   no-undo.
  define input parameter pEffDate     as datetime  no-undo.
  define input parameter pVersionDesc as character no-undo.
  
  define input parameter table for tempoutrateCard.
  define input parameter table for tempoutratetable.                                                                                    
  define input parameter table for tempoutrateRule.  
  
  define variable cAttrList as character no-undo.
  
  empty temp-table ratecard no-error.
  empty temp-table ratetable no-error.
  empty temp-table rateRule no-error.
  
  do with frame {&frame-name}:
  end.
  
  for each tempoutrateCard:
    create ratecard.
    buffer-copy tempoutrateCard to ratecard no-error.
  end.
  for each tempoutratetable:
    create ratetable.
    buffer-copy tempoutratetable to ratetable no-error.
  end.
  for each tempoutrateRule:
    create rateRule.
    buffer-copy tempoutrateRule to rateRule no-error.
  end.
  
  status default "Card ID = " + string(activeCardID) in window {&window-name}.
  status input "Card ID   = " + string(activeCardID) in window {&window-name}.
  
  if pTitle = "New" then
   bTestCard:sensitive = false.
  
  if pTitle = "copy" 
   then
    do:
      bTestCard:sensitive = false.
      for each ratecard where ratecard.cardID = copycardid:
        ratecard.cardid = 0.
      end.
      for each ratetable where ratetable.cardID = copycardid:
        ratetable.cardid      = 0.
        ratetable.ratetableid = 0.
      end.
      for each raterule where raterule.cardID = copycardid:
        raterule.cardid     = 0.
        raterule.rateruleid = 0.
      end.
    end.

  find first ratecard no-error.
  if available ratecard
   then
    do:
      assign
        activeCardSetID             = ratecard.cardsetID
        activeCardID                = ratecard.cardID
        eDescription:screen-value   = ratecard.description
        ecomments:screen-value      = ratecard.comments.
        
      /* If ratecard's region is not setup in syscodes then reset the region widget and show appropriate information message to user.*/
      if ratecard.region <> "" and ratecard.region <> "?" and 
         (lookup(ratecard.region,cbRegion:list-items) = 0 or (pregionList = "?" or pregionList = "")) 
       then
          message "Region '" ratecard.region "' is not setup in the system. Please contact the System Administrator." 
            view-as alert-box information buttons ok. 
       else
         cbRegion:screen-value = if (ratecard.region = "?" or ratecard.region = "") then ? else ratecard.region no-error.
          

      publish "GetRateCardList" (input if cbRegion:screen-value in frame {&frame-name} = "?" or cbRegion:screen-value in frame {&frame-name} = ? or cbRegion:screen-value in frame {&frame-name} = "ALL" then "" else cbRegion:screen-value  in frame {&frame-name},
                                 input if pTitle = "Edit" then activeCardID else 0,
                                 input activeCardSetID,
                                 output ratecardlist).
      
      cbCardName:list-items in frame {&frame-name} = rateCardList.

      assign cRefCardName = ratecard.refCardName.

      apply 'value-changed' to cbRegion.
  
      assign cbCardName:screen-value = ratecard.cardName.
    
        publish "GetRefCardID" (input ratecard.refCardName,
                                input ratecard.cardSetID,
                                input ratecard.region,
                                output pRefCardID).

        listRefAttrib = getAttribList(pRefCardID) no-error.

      assign
      {&window-name}:title      = cWinTitle + " - " + ratecard.cardName + " - Version - " + string(pVersion).
  
      status default "Card ID: " + string(ratecard.cardID) in window {&window-name}.
      status input "Card ID: " + string(ratecard.cardID) in window {&window-name}.
  
  
    end.
  apply 'value-changed' to brwTable.

  for last ratetable where ratetable.cardID = activeCardID by ratetable.seq:
    lastRateTable = ratetable.seq.
  end.

  case pTitle:
    when "New" then 
      assign 
        bPdf:sensitive = not bPdf:sensitive.
    
    when "Copy" 
     then 
      do:
        assign 
          bPdf:sensitive = true
          cbRegion  :sensitive = false.
        open query brwTable    for each ratetable.
        open query brwRule     for each rateRule where raterule.cardid = activecardid by RateRule.attrid by rateRule.seq .
      end.
    
    
    when "Edit" 
     then
      do:
        assign 
          bPdf      :sensitive = true
          cbCardName:sensitive = false
          cbRegion  :sensitive = false.
         bTestCard:sensitive   = true.
        open query brwTable    for each ratetable .
        open query brwRule     for each rateRule where raterule.cardid = activecardid by RateRule.attrid by rateRule.seq .
      end.

  end case.

  run refreshAttrDrpDn in this-procedure no-error.

  if cbShowAttrib:sensitive then
    cbShowAttrib:screen-value = cbShowAttrib:entry(1).
  else
    assign
      cbShowAttrib:list-items   = "Show All"
      cbShowAttrib:screen-value = "Show All".
  
  apply 'value-changed' to brwTable.
  apply 'value-changed' to cbShowAttrib.
  
  bSaveAll:sensitive in frame default-frame = false.
  bUndo   :sensitive in frame default-frame = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE testCard C-Win 
PROCEDURE testCard :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

do with frame {&frame-name}:
end.

 cAttributeList = GetAttribList(input activeCardID) no-error. 
 run wtestcard.w persistent  (input activecardsetid,
                              input activeCardID,
                              input pVersion,
                              input pVersionDesc,
                              input pStateID,
                              input pEffDate,
                              input cAttributeList,
                              input (if (cbCardName:screen-value = "?" or cbCardName:screen-value = ?) then "" else cbCardName:screen-value),
                              input (if (cbRegion:screen-value = "?" or cbRegion:screen-value = ? ) then "" else cbRegion:screen-value),                            
                              input table raterule) no-error.
publish "ApplyEntry"(activeCardID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE validateRateRuleData C-Win 
PROCEDURE validateRateRuleData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pCardName as character no-undo.
define input parameter table for tempRateRule.
define output parameter validateStatus as logical no-undo initial true.
define output parameter validateMsg as character  no-undo .  

if  pCardName = cbCardName:screen-value in frame {&frame-name} then
  for each tempRateRule:
    if can-find(first rateRule where rateRule.cardID  = activeCardID
                                    and rateRule.attrid =  tempRateRule.attrid
                                    and rateRule.ruleName =  tempRateRule.ruleName
                                    and rateRule.seq ne  tempRateRule.seq) then
    do:
      validateMsg     = "The combination of Attribute ID and Rule Name should be Unique.".
      validateStatus  = false.
      return.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE viewWindow C-Win 
PROCEDURE viewWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply 'entry' to cbCardName in frame DEFAULT-FRAME.

  if {&window-name}:window-state eq window-minimized  then
    {&window-name}:window-state = window-normal .
  {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowresized C-Win 
PROCEDURE windowresized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame default-frame:width-pixels = c-Win:width-pixels.
 frame default-frame:virtual-width-pixels = c-Win:width-pixels.
 frame default-frame:height-pixels = c-Win:height-pixels.
 frame default-frame:virtual-height-pixels = c-Win:height-pixels.

 browse brwRule:width-pixels = frame default-frame:width-pixels -  51. 
 browse brwTable:width-pixels = frame default-frame:width-pixels - 51.

 if {&window-name}:width-pixels > frame default-frame:width-pixels 
  then
   do: 
     frame default-frame:width-pixels = {&window-name}:width-pixels.
     frame default-frame:virtual-width-pixels = {&window-name}:width-pixels.                
   end.
 else
   do:
     frame default-frame:virtual-width-pixels = {&window-name}:width-pixels.
     frame default-frame:width-pixels = {&window-name}:width-pixels.
   end.

 browse brwTable:height-pixels = frame default-frame:height-pixels - 351. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION convBlank C-Win 
FUNCTION convBlank RETURNS CHARACTER
  (pcString as character):
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 define variable chVal as character no-undo. 
 
 if pcString = "BLANK" then 
   chVal = "".
 else 
   chVal = pcString.
 
 return chVal.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetAttribList C-Win 
FUNCTION GetAttribList returns character
  (piCardID as integer):

define buffer buffRateRule for rateRule.
define variable chAttrList as character no-undo.
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 for each buffRateRule 
   where  buffRateRule.cardID = piCardID 
     and  (buffRateRule.attrid <> {&PreAll} and buffRateRule.attrid <> {&PostAll})
     and  buffRateRule.attrid <> "?" 
     and  buffRateRule.attrid <> ? 
     and  buffRateRule.attrid <> "" 
     break by buffRateRule.attrid: 
 
   if first-of(buffRateRule.attrid) then
     chAttrList = chAttrList + "," + buffRateRule.attrid.
 end.
 
 chAttrList = trim(chAttrList,",").    
 
 if not can-do(chAttrList,"") and can-find(first buffRateRule where buffRateRule.cardID = piCardID and buffRateRule.attrid = "") then
   chAttrList = chAttrList + "," .
 
 return chAttrList.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

