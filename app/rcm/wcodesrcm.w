&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wcodesrcm.w
   Window of system CODES
   Archana Gupta
   04.16.2018
   
   Modified    :
   Date        Name     Comments   
   12/18/2019  Anubha   Modified to show region description as tooltip in 
                        browse
*/                        

{tt/syscode.i}
{tt/syscode.i &tablealias=ttsyscode}
{tt/syscode.i &tablealias=tempsyscode}

define variable typeList as character no-undo .
define variable cStateID as character no-undo .
 
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwData

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES syscode

/* Definitions for BROWSE brwData                                       */
&Scoped-define FIELDS-IN-QUERY-brwData syscode.codeType syscode.type syscode.code syscode.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwData   
&Scoped-define SELF-NAME brwData
&Scoped-define QUERY-STRING-brwData for each syscode by syscode.codeType by syscode.code
&Scoped-define OPEN-QUERY-brwData open query {&SELF-NAME} for each syscode by syscode.codeType by syscode.code.
&Scoped-define TABLES-IN-QUERY-brwData syscode
&Scoped-define FIRST-TABLE-IN-QUERY-brwData syscode


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwData}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bdelete RECT-1 RECT-2 RECT-3 bEdit cbState ~
brwData bNew bExport bRefresh 
&Scoped-Define DISPLAYED-OBJECTS cbState 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD UpdateTypeList C-Win 
FUNCTION UpdateTypeList RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bdelete  NO-FOCUS
     LABEL "Delete" 
     SIZE 7.2 BY 1.71 TOOLTIP "Delete".

DEFINE BUTTON bEdit  NO-FOCUS
     LABEL "Edit" 
     SIZE 7.2 BY 1.71 TOOLTIP "Edit".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export to Excel".

DEFINE BUTTON bNew  NO-FOCUS
     LABEL "New" 
     SIZE 7.2 BY 1.71 TOOLTIP "New".

DEFINE BUTTON bRefresh  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Reload data".

DEFINE VARIABLE cbState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 26.8 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 16.6 BY 2.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.2 BY 2.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 2.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwData FOR 
      syscode SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwData C-Win _FREEFORM
  QUERY brwData DISPLAY
      syscode.codeType    label "CodeType"    format "x(15)"  width 20
 syscode.type       label "Type"        format "x(25)"  width 15
syscode.code        label "Code"        format "x(15)"  width 20
syscode.description label "Description" format "x(255)" width 40
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 109 BY 12.86 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bdelete AT ROW 1.24 COL 26.6 WIDGET-ID 10 NO-TAB-STOP 
     bEdit AT ROW 1.24 COL 34.2 WIDGET-ID 8 NO-TAB-STOP 
     cbState AT ROW 1.57 COL 48.2 COLON-ALIGNED WIDGET-ID 56
     brwData AT ROW 3.38 COL 2 WIDGET-ID 200
     bNew AT ROW 1.24 COL 19 WIDGET-ID 6 NO-TAB-STOP 
     bExport AT ROW 1.24 COL 10.4 WIDGET-ID 2 NO-TAB-STOP 
     bRefresh AT ROW 1.24 COL 2.8 WIDGET-ID 4 NO-TAB-STOP 
     RECT-1 AT ROW 1.14 COL 2 WIDGET-ID 50
     RECT-2 AT ROW 1.14 COL 18.2 WIDGET-ID 52
     RECT-3 AT ROW 1.14 COL 42 WIDGET-ID 54
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 111 BY 15.33 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Region"
         HEIGHT             = 15.33
         WIDTH              = 111
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         SHOW-IN-TASKBAR    = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwData cbState fMain */
ASSIGN 
       FRAME fMain:RESIZABLE        = TRUE.

ASSIGN 
       brwData:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwData:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwData
/* Query rebuild information for BROWSE brwData
     _START_FREEFORM
open query {&SELF-NAME} for each syscode by syscode.codeType by syscode.code.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwData */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Region */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Region */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Region */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bdelete
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bdelete C-Win
ON CHOOSE OF bdelete IN FRAME fMain /* Delete */
DO:
  run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bEdit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bEdit C-Win
ON CHOOSE OF bEdit IN FRAME fMain /* Edit */
DO:
  run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME fMain /* Export */
DO:
   run exportData in this-procedure. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bNew
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bNew C-Win
ON CHOOSE OF bNew IN FRAME fMain /* New */
DO:
  run ActionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  publish "GetCodes" (input "",
                      output table tempsyscode).  
  run filterData in this-procedure.
  updateTypeList().
  open query brwData for each syscode where syscode.codetype = cbState:screen-value by syscode.codeType by syscode.code.
  run refreshStatusBar in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwData
&Scoped-define SELF-NAME brwData
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON DEFAULT-ACTION OF brwData IN FRAME fMain
DO:
  if syscode.isSecure = no 
   then
    run ActionModify in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON ROW-DISPLAY OF brwData IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON START-SEARCH OF brwData IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwData C-Win
ON VALUE-CHANGED OF brwData IN FRAME fMain
DO:
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON LEAVE OF cbState IN FRAME fMain /* State */
DO:
  cStateID = cbState:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbState C-Win
ON VALUE-CHANGED OF cbState IN FRAME fMain /* State */
DO:
  run filterData in this-procedure.
  updateTypeList().
  open query brwData for each syscode where syscode.codetype = cbState:screen-value by syscode.codeType by syscode.code.
  run setWidgetState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/win-status.i}
{lib/brw-main.i}

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

status input " ".

bExport:load-image("images/excel.bmp").
bExport:load-image-insensitive("images/excel-i.bmp").
bRefresh:load-image("images/sync.bmp").
bRefresh:load-image-insensitive("images/refresh-i.bmp").
bnew:load-image("images/add.bmp").
bEdit:load-image("images/update.bmp").
bEdit:load-image-insensitive("images/update-i.bmp").
bDelete:load-image("images/delete.bmp").
bDelete:load-image-insensitive("images/delete-i.bmp").

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  publish "GetSearchStates" (output std-ch).
  if std-ch = ?
   then std-ch = "".
  if std-ch > ""
   then std-ch = "," + std-ch.
  cbState:list-item-pairs in frame {&frame-name} = trim(std-ch,",").
  cbState:screen-value  = cbState:entry(1).
  RUN enable_UI.

  publish "GetCodes" (input "",
                      output table tempsyscode).
  run filterData in this-procedure.
  updateTypeList().
  open query brwData for each syscode by syscode.codeType by syscode.code.
  run refreshStatusBar in this-procedure.

  IF NOT THIS-PROCEDURE:PERSISTENT 
   THEN WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete C-Win 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  if not available syscode 
   then 
    return.
  
  std-lo = false.
  
  message "Highlighted region Code will be deleted. Are you sure?"
    view-as alert-box question buttons Yes-No update std-lo.
  if not std-lo 
   then 
    return.
  
  publish "DeleteSysCode" (syscode.codetype,
                           syscode.code,
                           output std-lo,
                           output std-ch).
  if not std-lo 
   then
    do:
       message std-ch
         view-as alert-box info buttons OK.
       return.
    end.
  
  
  publish "refreshcodes" (output table tempsyscode).
  run filterData in this-procedure. 
  updateTypeList().
  open query brwData for each syscode where syscode.codetype = cbState:screen-value by syscode.codeType by syscode.code.
  run refreshStatusBar in this-procedure.
  run setWidgetState   in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionModify C-Win 
PROCEDURE ActionModify :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable tCodeType    as character no-undo.
  define variable tType        as character no-undo.
  define variable tCode        as character no-undo.
  define variable tDescription as character no-undo.
  define variable tComments    as character no-undo. 

  define variable tseq         as character no-undo.
  
  empty temp-table ttsyscode.
  define variable pSave        as logical   no-undo initial false.
  
  if not available syscode
   then 
    return.
  
  assign
    tCodeType    = syscode.codeType
    tCode        = syscode.code
    tType        = syscode.type
    tDescription = syscode.description
    tComments    = syscode.comments
    .
  
  LOOP:
  do:
    run dialogcodercm.w
      (input false,
       input typeList,
       input-output tCodeType,
       input-output tType,
       input-output tCode,
       input-output tDescription,
       input-output tcomments,
       output pSave).
    
    if not pSave
     then
      leave LOOP.
    
    create ttsyscode.
    assign 
      ttsyscode.codeType    = tCodeType
      ttsyscode.code        = tCode
      ttsyscode.type        = tType
      ttsyscode.description = tDescription
      ttsyscode.comments    = tComments
      ttsyscode.isSecure    = false
      .
    
    publish "modifySyscode" (input table ttsyscode,
                             output std-lo,
                             output std-ch).
    
    if not std-lo
     then
      do:
        message std-ch
         view-as alert-box error buttons OK.
        return.
      end.
    
    publish "refreshcodes" (output table tempsyscode).
    updateTypeList().
    run filterData in this-procedure. 
    open query brwData for each syscode by syscode.codeType by syscode.code.

    find first syscode where 
               syscode.codeType  = tCodeType   and 
               syscode.code      = tCode       no-error.
  
    if available syscode
     then
      reposition brwData to rowid rowid(syscode) no-error.
    run refreshStatusBar in this-procedure.
    run setWidgetState   in this-procedure.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew C-Win 
PROCEDURE ActionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.
  
  define variable tCodeType    as character no-undo.
  define variable tType        as character no-undo.
  define variable tCode        as character no-undo.
  define variable tDescription as character no-undo.
  define variable tComments    as character no-undo.
  
  define variable pSave        as logical no-undo initial false.
  empty temp-table ttsyscode.
  
  find current syscode no-error.
  if available syscode
   then 
    tCodeType    = syscode.codeType.
  
  LOOP:
  do:
    run dialogcodercm.w(
        input true,
        input typeList,
        input-output tCodeType,
        input-output tType,
        input-output tCode,
        input-output tDescription,
        input-output tcomments,
        output pSave).
    
    if not pSave
     then 
      leave LOOP.
    
    create ttsyscode.
    assign 
      ttsyscode.codeType    = tCodeType
      ttsyscode.code        = tCode
      ttsyscode.type        = tType
      ttsyscode.description = tDescription
      ttsyscode.comments    = tComments
      ttsyscode.isSecure    = false
      .

    publish "NewSyscode" (input table ttsyscode,
                          output std-lo,
                          output std-ch).
    
    if not std-lo
     then
      do:
        message std-ch
         view-as alert-box error buttons OK.
        return.
      end.
    
    publish "refreshcodes" (output table tempsyscode).
    run filterData in this-procedure.
    updateTypeList().
    open query brwData preselect each syscode by syscode.codeType by syscode.code.

    if cbState:input-value = tCodeType
     then
      find first syscode where 
                 syscode.codeType = tCodeType  and 
                 syscode.code     = tCode      no-error.
     else
      find first syscode where
                 syscode.codeType = cbState:input-value no-error.
     
    if available syscode
     then
      reposition brwData to rowid rowid(syscode) no-error.
    
    run refreshStatusBar in this-procedure.
    run setWidgetState   in this-procedure.
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbState 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bdelete RECT-1 RECT-2 RECT-3 bEdit cbState brwData bNew bExport 
         bRefresh 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportData C-Win 
PROCEDURE exportData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define variable tHandle as handle    no-undo.
  define variable tQuery  as character  no-undo.
  
  if query brwData:num-results = 0
   then
    do:
      message "There is nothing to export"
       view-as alert-box warning buttons ok.
      return.
    end.
  
  tQuery = 'for each syscode where syscode.codeType = "' + cbState:screen-value in frame {&frame-name} + '"'.
  
  publish "GetReportDir" (output std-ch).
  tHandle = temp-table syscode:handle.
  
  run util/exporttable.p (table-handle tHandle,
                           "syscode",
                           tQuery,
                           "codeType,type,code,description,comments,isSecure",
                           "Code Type,Type,Code,Description,Comments,Is Secure",
                           std-ch,
                           "Region-" + "-" + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                           true,
                           output std-ch,
                           output std-in) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  close query brwData.
  empty temp-table sysCode.

  for each tempsyscode where tempsyscode.codeType = cbState:input-value in frame {&frame-name}:
    create sysCode.
    buffer-copy tempSysCode to sysCode.
  end.
  
  open query brwData preselect each sysCode by sysCode.codeType by sysCode.code.

  setStatusCount(query brwData:num-results).
  run setWidgetState in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshStatusBar C-Win 
PROCEDURE refreshStatusBar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer ttSysCode for syscode. 
  std-in = 0.

  for each ttSysCode :
    std-in = std-in + 1.
  end.
  
  std-ch = string(std-in) + " " + (if std-in = 1 then "record found" else "record(s) found") + " as of " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss").
  status default std-ch in window {&window-name}.
  status input   std-ch in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgetState C-Win 
PROCEDURE setWidgetState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
  end.

  if available syscode
   then
    do:
      if syscode.isSecure then 
       assign 
         bEdit:sensitive   = false
         bdelete:sensitive = false
         .
      else
       assign 
         bEdit:sensitive   = true
         bdelete:sensitive = true
         .

      assign 
        bExport:sensitive = true.
        brwData:tooltip   = syscode.description.
    end.

   else
    assign 
      bEdit:sensitive   = false
      bdelete:sensitive = false
      bExport:sensitive = false
      brwData:tooltip   = "".
      .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  {lib/brw-sortdata.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 brwData:width-pixels = frame fmain:width-pixels - 10.
 brwData:height-pixels = frame fMain:height-pixels - 50.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION UpdateTypeList C-Win 
FUNCTION UpdateTypeList RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

 for each syscode:
   typeList = typeList + "," + syscode.type.
 end.

 typeList = trim(typeList,",").
   return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

