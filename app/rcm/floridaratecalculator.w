&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: floridaratecalculator.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Vikas and Archana

  Created:
  
  Modified    :
   Date        Name     Comments
   07/01/2019  Sachin   Modified for standardisation. 
   02/07/2022  Vignesh  Added Logic for error handling.

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{lib\std-def.i}
{lib\rcm-std-def.i}
{lib\winlaunch.i}

/*Temp table to contain endorsement data*/
{tt\rateendorsements.i &tablealias=endorsement}
/*Temp tables for log*/
{tt\ratelog.i}

/*Temp tables to manage dynamic checkbox and editor*/
{tt\rateendorsementui.i &tablealias=tthOwr}
{tt\rateendorsementui.i &tablealias=tthLdr}

/*Temp tables to show detailed premium of endorsements*/
{tt\rateendorsementpremium.i &tablealias=endorsDetail}


/*Temp tables to handle open endorsements windows*/
define temp-table openEndors
  field hInstance as handle.

/*Temp tables to handle open log windows*/
define temp-table openLog
  field hInstance as handle.
                                                                   



/* variables used in screen resizing*/
define variable endorsPos              as decimal                  no-undo.
define variable resultsPos             as decimal                  no-undo.
define variable fullheight             as decimal                  no-undo.
define variable cutwidth               as decimal                  no-undo.
define variable fullwidth              as decimal                  no-undo.
define variable cutheight              as decimal                  no-undo.
define variable endSelected            as logical   initial false  no-undo. 

/*variables containing key value pair for ui input and output premium*/
define variable pcCalculatedPremium    as character                no-undo.
define variable pcInputValues          as character                no-undo.
define variable pcInputValuesPDF       as character                no-undo.

                                                                 
/* output parameter parsing variables */
define variable premiumOwner           as character                no-undo.
define variable premiumOEndors         as character                no-undo.
define variable premiumOEndorsDetail   as character                no-undo.
define variable premiumLoan            as character                no-undo.
define variable premiumLEndors         as character                no-undo.
define variable premiumLEndorsDetail   as character                no-undo.
define variable success                as character                no-undo.
define variable cardSetId              as character                no-undo.
define variable effectiveDate          as character                no-undo.
define variable serverErrMsg           as character                no-undo.
define variable grandTotal             as character                no-undo.

define variable ratesCode              as character                no-undo.
define variable ratesMsg               as character                no-undo.

/*type cased variables after parsing output parameter*/
define variable lSuccess               as logical   initial false  no-undo.


/* variables to populate propertytypecombobox */
define variable pcPropertyType         as character                no-undo.

/* variables to handle dynamic widget of endorsements*/
define variable iEndCount              as integer                  no-undo.
define variable iNextEndors            as integer                  no-undo.
define variable flag                   as logical   initial true   no-undo.


/*variable to handle simo case*/
define variable tempcoverageAmount     as character                no-undo.
define variable tempLoanAmount         as character                no-undo.

/*variable to launch calculator for multiple versions*/
define variable versionNo              as character                no-undo.

define variable logscreenparameter     as character                no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD removeItemFromList C-Win 
FUNCTION removeItemFromList returns character
    (pclist       as character,
     pcremoveitem as character,
     pcdelimiter  as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btendorsement 
     LABEL "v" 
     SIZE 4.8 BY 1.13 TOOLTIP "Show endorsements".

DEFINE VARIABLE cbLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 46.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbPropType AS CHARACTER FORMAT "X(256)":U INITIAL "R" 
     LABEL "Property Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiCoverageAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Coverage Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiEffectiveDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanPriorAmt AS DECIMAL FORMAT "zzz,zzz,zzz":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiPriorAmt AS DECIMAL FORMAT "zzz,zzz,zzz":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.52.

DEFINE RECTANGLE RECT-60
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 5.52.

DEFINE VARIABLE tbSimultaneous AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.6 BY .57 NO-UNDO.

DEFINE RECTANGLE RECT-67
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 10.7.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 10.7.

DEFINE BUTTON btCalculate 
     LABEL "&Calculate" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Calculate(Alt + C)".

DEFINE BUTTON btClear 
     LABEL "Clear&x" 
     SIZE 7.2 BY 1.7 TOOLTIP "Clear(Alt + X)".

DEFINE BUTTON btLEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE BUTTON btlog 
     LABEL "Show Calcu&lations" 
     SIZE 7.2 BY 1.7 TOOLTIP "Show Ca&lculations(Alt + L)".

DEFINE BUTTON btOEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE BUTTON btRateSheet 
     LABEL "&PDF" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Print Rates(Alt + P)".

DEFINE VARIABLE figrandTotal AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Grand Total" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderEndorsPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderPolicyPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiLendersEndorsList AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loan Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.8 BY 1 NO-UNDO.

DEFINE VARIABLE fiLendersTotalPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerEndorsList AS CHARACTER FORMAT "X(256)":U 
     LABEL "Owner Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerEndorsPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerPolicyPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnersTotalPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.26.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 5.26.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 176.2 BY 26.52 WIDGET-ID 100.

DEFINE FRAME frBasic
     cbPropType AT ROW 1.3 COL 30.8 COLON-ALIGNED WIDGET-ID 60
     fiCoverageAmt AT ROW 3.17 COL 30.8 COLON-ALIGNED WIDGET-ID 16
     cbRateType AT ROW 4.26 COL 30.8 COLON-ALIGNED WIDGET-ID 12
     fiPriorAmt AT ROW 5.3 COL 30.8 COLON-ALIGNED WIDGET-ID 38
     fiLoanAmt AT ROW 3.17 COL 118.2 COLON-ALIGNED WIDGET-ID 46
     cbLoanRateType AT ROW 4.26 COL 118.2 COLON-ALIGNED WIDGET-ID 44
     tbSimultaneous AT ROW 3.39 COL 141.8 WIDGET-ID 116 NO-TAB-STOP 
     fiLoanPriorAmt AT ROW 5.3 COL 118.2 COLON-ALIGNED WIDGET-ID 88
     fiEffectiveDate AT ROW 6.35 COL 118.2 COLON-ALIGNED WIDGET-ID 170
     btendorsement AT ROW 6.74 COL 2.6 WIDGET-ID 150
     fiVersion AT ROW 1.3 COL 167.6 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     "Simultaneous" VIEW-AS TEXT
          SIZE 13 BY .61 AT ROW 3.35 COL 144.8 WIDGET-ID 162
     "Owner" VIEW-AS TEXT
          SIZE 7.4 BY .61 AT ROW 2.26 COL 3.6 WIDGET-ID 58
          FONT 6
     "Loan" VIEW-AS TEXT
          SIZE 6.4 BY .61 AT ROW 2.35 COL 90.6 WIDGET-ID 66
          FONT 6
     "Endorsements" VIEW-AS TEXT
          SIZE 16.6 BY .61 AT ROW 6.96 COL 8 WIDGET-ID 172
          FONT 6
     RECT-56 AT ROW 2.57 COL 2 WIDGET-ID 2
     RECT-60 AT ROW 2.57 COL 89 WIDGET-ID 64
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 176.08 BY 7.14 WIDGET-ID 200.

DEFINE FRAME frBasicEndors
     RECT-67 AT ROW 1 COL 2 WIDGET-ID 148
     RECT-68 AT ROW 1 COL 89 WIDGET-ID 150
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         NO-UNDERLINE THREE-D 
         AT COL 1 ROW 8
         SIZE 176.08 BY 10.86 WIDGET-ID 500.

DEFINE FRAME frLenderEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 90.6 ROW 1.09
         SCROLLABLE SIZE 85.6 BY 100 WIDGET-ID 700.

DEFINE FRAME frOwnerEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 1.09
         SCROLLABLE SIZE 85.6 BY 100 WIDGET-ID 600.

DEFINE FRAME frBasicResults
     btOEndors AT ROW 2.57 COL 49 WIDGET-ID 154
     btLEndors AT ROW 2.57 COL 136.4 WIDGET-ID 156
     btCalculate AT ROW 7 COL 148 WIDGET-ID 34
     btClear AT ROW 7 COL 155.2 WIDGET-ID 104
     btRateSheet AT ROW 7 COL 162.4 WIDGET-ID 152
     btlog AT ROW 7 COL 169.6 WIDGET-ID 146
     fiOwnerEndorsList AT ROW 1.52 COL 30.8 COLON-ALIGNED WIDGET-ID 140 NO-TAB-STOP 
     fiLendersEndorsList AT ROW 1.52 COL 118.2 COLON-ALIGNED WIDGET-ID 142 NO-TAB-STOP 
     fiOwnerEndorsPremium AT ROW 2.57 COL 30.8 COLON-ALIGNED WIDGET-ID 100 NO-TAB-STOP 
     fiLenderEndorsPremium AT ROW 2.57 COL 118.2 COLON-ALIGNED WIDGET-ID 48 NO-TAB-STOP 
     fiOwnerPolicyPremium AT ROW 3.61 COL 30.8 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     fiLenderPolicyPremium AT ROW 3.61 COL 118.2 COLON-ALIGNED WIDGET-ID 36 NO-TAB-STOP 
     fiOwnersTotalPremium AT ROW 4.65 COL 30.8 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     fiLendersTotalPremium AT ROW 4.65 COL 118.2 COLON-ALIGNED WIDGET-ID 68 NO-TAB-STOP 
     figrandTotal AT ROW 7.3 COL 118.2 COLON-ALIGNED WIDGET-ID 108 NO-TAB-STOP 
     RECT-58 AT ROW 1 COL 2 WIDGET-ID 2
     RECT-62 AT ROW 1 COL 89 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 18.81
         SIZE 176.08 BY 8.33
         DEFAULT-BUTTON btCalculate WIDGET-ID 400.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Florida Rate Calculator"
         HEIGHT             = 26.52
         WIDTH              = 176.2
         MAX-HEIGHT         = 35.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 35.52
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frBasic:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicEndors:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicResults:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frLenderEndors:FRAME = FRAME frBasicEndors:HANDLE
       FRAME frOwnerEndors:FRAME = FRAME frBasicEndors:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME frBasicResults:MOVE-BEFORE-TAB-ITEM (FRAME frBasic:HANDLE)
       XXTABVALXX = FRAME frBasicEndors:MOVE-BEFORE-TAB-ITEM (FRAME frBasicResults:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasic
   Custom                                                               */
/* SETTINGS FOR FILL-IN fiVersion IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX tbSimultaneous IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frBasicEndors
   Custom                                                               */
ASSIGN XXTABVALXX = FRAME frOwnerEndors:MOVE-BEFORE-TAB-ITEM (FRAME frLenderEndors:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasicResults
   NOT-VISIBLE Custom                                                   */
/* SETTINGS FOR BUTTON btlog IN FRAME frBasicResults
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON btRateSheet IN FRAME frBasicResults
   NO-ENABLE                                                            */
ASSIGN 
       figrandTotal:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderEndorsPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLendersEndorsList:HIDDEN IN FRAME frBasicResults           = TRUE
       fiLendersEndorsList:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLendersTotalPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerEndorsList:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerEndorsPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnersTotalPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

/* SETTINGS FOR FRAME frLenderEndors
                                                                        */
ASSIGN 
       FRAME frLenderEndors:HEIGHT           = 10.33
       FRAME frLenderEndors:WIDTH            = 85.5.

/* SETTINGS FOR FRAME frOwnerEndors
                                                                        */
ASSIGN 
       FRAME frOwnerEndors:HEIGHT           = 10.33
       FRAME frOwnerEndors:WIDTH            = 85.6.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasic
/* Query rebuild information for FRAME frBasic
     _Query            is NOT OPENED
*/  /* FRAME frBasic */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicEndors
/* Query rebuild information for FRAME frBasicEndors
     _Query            is NOT OPENED
*/  /* FRAME frBasicEndors */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicResults
/* Query rebuild information for FRAME frBasicResults
     _Query            is NOT OPENED
*/  /* FRAME frBasicResults */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Florida Rate Calculator */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Florida Rate Calculator */
DO:
   /* This event will close the window and terminate the procedure.  */
   for each openLog:      
     if valid-handle(openLog.hInstance) then
       delete object openLog.hInstance no-error.
   end.
   empty temp-table openLog no-error.
   run clearAll in this-procedure no-error.
  
   apply "close":U to this-procedure.
   return no-apply.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btCalculate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCalculate C-Win
ON CHOOSE OF btCalculate IN FRAME frBasicResults /* Calculate */
DO:
   run calculatePremium in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME frBasicResults /* Clearx */
DO:
   run clearAll in this-procedure no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME btendorsement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btendorsement C-Win
ON CHOOSE OF btendorsement IN FRAME frBasic /* v */
DO:
  if endSelected = false then
  do:
      endSelected = true.
      btEndorsement:load-image("images/s-up2.bmp") in frame frBasic no-error. 
      btEndorsement:tooltip in frame frBasic = "Hide endorsements".
  end.
  else
  do:
      endSelected = false.
      btEndorsement:load-image("images/s-down2.bmp") in frame frBasic no-error.
      btEndorsement:tooltip in frame frBasic = "Show endorsements".
  end.

  run setPosition in this-procedure no-error.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btLEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btLEndors C-Win
ON CHOOSE OF btLEndors IN FRAME frBasicResults /* View Details */
DO:
   run endorsPremiumDetail in this-procedure (input premiumLEndorsDetail,
                                              input {&Lenders},
                                              output table endorsDetail) no-error.  
   create openEndors.
   run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                                            input {&Lenders}) no-error.
   empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btlog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btlog C-Win
ON CHOOSE OF btlog IN FRAME frBasicResults /* Show Calculations */
DO:    
   create openLog.
   run wlogs.w persistent set openLog.hInstance (input table rateLog,
                                                 input logscreenparameter) no-error.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btOEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOEndors C-Win
ON CHOOSE OF btOEndors IN FRAME frBasicResults /* View Details */
DO:
   run endorsPremiumDetail in this-procedure(input premiumOEndorsDetail,
                                             input {&Owners},
                                             output table endorsDetail) no-error. 
  
   create openEndors.
   run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                                            input {&Owners}) no-error.
   empty temp-table endorsDetail no-error. 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRateSheet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRateSheet C-Win
ON CHOOSE OF btRateSheet IN FRAME frBasicResults /* PDF */
DO: 
   run generatepdf.w ("Florida",
                      replace (pcCalculatedPremium,"=","^") ,
                      pcInputValuesPDF) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME cbLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbLoanRateType C-Win
ON VALUE-CHANGED OF cbLoanRateType IN FRAME frBasic /* Rate to Apply */
DO:  
   run setLoanPriorInfoState in this-procedure no-error.
   run clearResults          in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPropType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPropType C-Win
ON VALUE-CHANGED OF cbPropType IN FRAME frBasic /* Property Type */
DO:
   run propertyTypeValuechange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRateType C-Win
ON VALUE-CHANGED OF cbRateType IN FRAME frBasic /* Rate to Apply */
DO:
   run setOwnerPriorInfoState in this-procedure no-error.
   run clearResults           in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiCoverageAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON LEAVE OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
   tempcoverageAmount = fiCoverageAmt:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON VALUE-CHANGED OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
   run coverageAmountValueChange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiEffectiveDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiEffectiveDate C-Win
ON VALUE-CHANGED OF fiEffectiveDate IN FRAME frBasic /* Effective Date */
DO:
   run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON LEAVE OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
   tempLoanAmount = fiLoanAmt:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON VALUE-CHANGED OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
   run loanAmountValueChange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiLoanPriorAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanPriorAmt C-Win
ON VALUE-CHANGED OF fiLoanPriorAmt IN FRAME frBasic /* Prior Policy Amount */
DO:
   run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiPriorAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiPriorAmt C-Win
ON VALUE-CHANGED OF fiPriorAmt IN FRAME frBasic /* Prior Policy Amount */
DO:
   run clearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
/*-----------------------------STATE SPECIFIC CODE---------------------------------------*/

 publish "GetFLEndors"       (output table endorsement).
 publish "GetFLPropertyType" (output pcPropertyType).

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

btEndorsement:load-image("images/s-down2.bmp")           in frame frBasic        no-error.
btCalculate  :load-image("images/calc.bmp")              in frame frBasicResults no-error.
btClear      :load-image("images/erase.bmp")             in frame frBasicResults no-error.
btRateSheet  :load-image ("images/pdf.bmp")              in frame frBasicResults no-error.
btRateSheet  :load-image-insensitive("images/pdf-i.bmp") in frame frBasicResults no-error.
btLog        :load-image("images/log.bmp")               in frame frBasicResults no-error.
btLog        :load-image-insensitive("images/log-i.bmp") in frame frBasicResults no-error.

btOEndors:load-image("images/s-lookup.bmp")                  in frame frBasicResults no-error.
btOEndors:load-image-insensitive("images/s-lookup-i.bmp")    in frame frBasicResults no-error.

btLEndors:load-image("images/s-lookup.bmp")                  in frame frBasicResults no-error.
btLEndors:load-image-insensitive("images/s-lookup-i.bmp")    in frame frBasicResults no-error.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.
{lib/winshowscrollbars.i}
assign 
  cbPropType:list-item-pairs = pcPropertyType
  cbPropType:screen-value    = {&DefaultPropertyType}.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
{lib/win-main.i}
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.

/* ui resize for initial setup*/
  run ShowScrollBars(frame frBasic:handle, no,no) no-error.
  run ShowScrollBars(frame frBasicResults:handle, no, no) no-error.
  run ShowScrollBars(frame frBasicEndors:handle, no, no) no-error.  

  assign
    endorsPos  = frame frBasicEndors:row
    resultsPos = frame frBasicResults:row 
    fullheight = frame frBasic:height +  frame frBasicEndors:height  +  frame frBasicResults:height  
    cutheight  = frame frBasic:height +  frame frBasicResults:height
    cutwidth   = frame frBasic:width  +  0.5.

  run SetPosition in this-procedure no-error.

  apply 'value-changed' to cbPropType. 

/*publish to get the current selected version if launched by maintenance screen else use active version*/
/*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
  publish "GetCurrentValue" ("FLVersion", output versionNo).

  if versionNo ne "" then
    fiVersion:screen-value = versionNo.
  else 
    assign 
      fiVersion:hidden = true
      fiVersion:visible = false.

  subscribe to "ApplyEntry" anywhere.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE applyEntry C-Win 
PROCEDURE applyEntry :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "entry" to  fiCoverageAmt in frame frBasic.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculatePremium C-Win 
PROCEDURE calculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  pcInputValues:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable errorStatus      as logical   no-undo.      
  define variable errorMsg         as character no-undo.
  /* variable contain list of selected endorsement */                                   
  define variable ownerEndors      as character no-undo.    
  define variable loanEndors       as character no-undo.    
  /* variable contain list of selected endorsement with proposed rate*/                                   
  define variable ownerEndorsPR    as character no-undo.    
  define variable loanEndorsPR     as character no-undo.

  define variable proposedRate     as decimal   no-undo.
  define variable iCountOEndors    as integer   no-undo.
  define variable iCountLEndors    as integer   no-undo.
  
  pcCalculatedPremium = "".                                                                      
                                                                        
  /*-------------------Client Validations------------------------*/
  run clientValidation in this-procedure(output errorStatus,
                                         output errorMsg) no-error.

  do with frame frBasicResults:                                                
  end. 
  if errorStatus = yes then
  do:
    message errorMsg
      view-as alert-box info buttons ok.
    btRateSheet:sensitive = false.
    return no-apply.
  end.

  /*--------------------------------------Get Endorsement Lists--------------------------------------*/
  ownerEndors = fiOwnerEndorsList:screen-value.
  ownerEndors = replace(ownerEndors, ",", "|").

  loanEndors = fiLendersEndorsList:screen-value.
  loanEndors = replace(loanEndors, ",", "|").  

  /*--------------------------Embed proposed rate in the endorsement list-----------------------------*/
  do iCountOEndors = 1 to num-entries(ownerEndors,"|"):
  /*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
    publish "GetFLProposedRate"(input entry(iCountOEndors,ownerEndors,"|"),
                                output proposedRate).
    ownerEndorsPR =  ownerEndorsPR + entry(iCountOEndors,ownerEndors,"|") + "-" + (if proposedRate ne ? then string(proposedRate) else "null")  + "|" no-error.
  end.
  
  do iCountLEndors = 1 to num-entries(loanEndors,"|"):
    /*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
    publish "GetFLProposedRate"(input entry(iCountLEndors,loanEndors,"|"),
                                output proposedRate).
    loanEndorsPR =  loanEndorsPR + entry(iCountLEndors,loanEndors,"|") + "-" + (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.
  end.

  ownerEndorsPR = trim(ownerEndorsPR,"|").
  loanEndorsPR  = trim(loanEndorsPR,"|").
  
  do with frame frBasic:
  end.
  /*-------------------------Input variable-------------------------------*/
  pcInputValues    = "ratetype^"                   + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else cbRateType:screen-value +
                     ",coverageAmount^"            + string(fiCoverageAmt:input-value)        + 
                     ",reissueCoverageAmount^"     + string(fiPriorAmt:input-value)           + 
                     ",loanCoverageAmount^"        + string(fiLoanAmt:input-value)            +  
                     ",loanRateType^"              + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else cbLoanRateType:screen-value +
                     ",loanReissueCoverageAmount^" + string(fiLoanPriorAmt:input-value)       + 
                     ",ownerEndors^"               + ownerEndorsPR                            + 
                     ",loanEndors^"                + loanEndorsPR                             + 
                     ",loanpriorEffectiveDate^"    + fiEffectiveDate:screen-value             +  
                     ",simultaneous^"              + tbSimultaneous:screen-value              +
                     ",propertyType^"              + cbPropType:screen-value                  +
                     ",Version^"                   + fiVersion:screen-value                   +
                     ",region^"                    + "".
                   
  pcInputValuesPDF = "ratetype^"                   + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else entry((lookup(cbRateType:screen-value, cbRateType:list-item-pairs,",") - 1),cbRateType:list-item-pairs, ",") +
                     ",coverageAmount^"            + string(fiCoverageAmt:input-value)        + 
                     ",reissueCoverageAmount^"     + string(fiPriorAmt:input-value)           + 
                     ",loanCoverageAmount^"        + string(fiLoanAmt:input-value)            +  
                     ",loanRateType^"              + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else entry((lookup(cbLoanRateType:screen-value, cbLoanRateType:list-item-pairs,",") - 1),cbLoanRateType:list-item-pairs, ",") +
                     ",loanReissueCoverageAmount^" + string(fiLoanPriorAmt:input-value)       + 
                     ",ownerEndors^"               + ownerEndorsPR                            + 
                     ",loanEndors^"                + loanEndorsPR                             +
                     ",loanpriorEffectiveDate^"    + fiEffectiveDate:screen-value             + 
                     ",simultaneous^"              + tbSimultaneous:screen-value              +
                     ",propertyType^"              + cbPropType:screen-value                  +
                     ",Version^"                   + fiVersion:screen-value                   +
                     ",region^"                    + ""                                       +
                     ",ratetypecode^"              + if cbRateType:screen-value  = ? or cbRateType:screen-value  = "?" then "" else cbRateType:screen-value +
                     ",loanRateTypecode^"          + if cbLoanRateType:screen-value  = ? or cbLoanRateType:screen-value  = "?" then "" else cbLoanRateType:screen-value .

  publish "FLcalculatePremium" (input pcInputValues,
                                input {&Florida},
                                input integer(versionNo),
                                output pcCalculatedPremium,
                                output table rateLog).

  /*--------------------Extracting output parameter and set premium on screen--------------------------------------*/ 
  run ExtractOutputParameters in this-procedure no-error.
  logscreenparameter =  pcInputValues + ",cardSetId^"     + cardSetId
                                      + ",StateID^"       + {&Florida}
                                      + ",manualEffectiveDate^" + (if effectiveDate eq "" then "" else string(date(effectiveDate), "99/99/99")).  

  do with frame frBasicResults:                                                
  end. 

  btLog:sensitive = true.
  if lSuccess = false  or serverErrMsg ne "serverErrMsg" then
  do:
     message serverErrMsg
       view-as alert-box info buttons ok.
     btRateSheet:sensitive = false.
     return no-apply.
  end.  
  else if ratesCode >= '3000' and ratesMsg <> "" then
  do:
    message ratesMsg
      view-as alert-box info buttons ok.
  end.  
  btRateSheet:sensitive = true.
  /*--------setup endorsement premium detail button-----------*/  
  if fiOwnerEndorsList:screen-value ne "" then
    btOEndors:sensitive = true.
  if fiLendersEndorsList:screen-value ne "" then
    btLEndors:sensitive = true.

  /*btRateSheet:sensitive in frame frBasicResults = true.*/
  assign
    fiOwnerEndorsPremium:screen-value      = premiumOEndors
    fiOwnerPolicyPremium:screen-value      = premiumOwner
    fiLenderEndorsPremium:screen-value     = premiumLEndors
    fiLenderPolicyPremium:screen-value     = premiumLoan    
    fiOwnersTotalPremium:screen-value      = string(decimal(premiumOEndors) + decimal(premiumOwner))
    fiLendersTotalPremium:screen-value     = string(decimal(premiumLEndors) + decimal(premiumLoan))
    fiGrandTotal:screen-value              = string(decimal(premiumOEndors) + decimal(premiumOwner)
                                                  + decimal(premiumLEndors) + decimal(premiumLoan)) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearAll C-Win 
PROCEDURE clearAll :
/*------------------------------------------------------------------------------
  Purpose:     reset screen for selected ratetype 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasic:
  end.

  assign 
    fiCoverageAmt:screen-value    = ""
    fiLoanAmt:screen-value        = ""
    fiPriorAmt:screen-value       = ""
    fiLoanPriorAmt:screen-value   = ""
    fiEffectiveDate:screen-value  = ? 
    cbRateType:screen-value       = cbRateType:entry(1)
    cbLoanRateType:screen-value   = cbLoanRateType:entry(1) no-error.

  run RefreshEndors in this-procedure("") no-error.

  apply 'leave'         to fiCoverageAmt.
  apply 'leave'         to fiLoanAmt.
  apply 'value-changed' to fiCoverageAmt.
  apply 'value-changed' to fiLoanAmt.
  apply 'value-changed' to cbRateType.
  apply 'value-changed' to cbLoanRateType.
  apply 'value-changed' to cbPropType.

  pcInputValues = "".

  run clearResults in this-procedure no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clearResults C-Win 
PROCEDURE clearResults :
/*------------------------------------------------------------------------------
  Purpose:      for any change in user input reset results 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasicResults:
  end.

  assign
    fiOwnerEndorsPremium:screen-value    = ""
    fiOwnerPolicyPremium:screen-value    = ""
    fiOwnersTotalPremium:screen-value    = ""
    fiLenderEndorsPremium:screen-value   = ""
    fiLenderPolicyPremium:screen-value   = ""
    fiLendersTotalPremium:screen-value   = ""
    btOEndors:sensitive                  = false
    btLEndors:sensitive                  = false  
    btLog:sensitive                      = false  
    fiGrandTotal:screen-value            = "" 
    btRateSheet:sensitive                = false.

  for each openEndors:      
    if valid-handle(openEndors.hInstance) then
      delete object openEndors.hInstance no-error.
  end.

  empty temp-table openEndors no-error.

  pcCalculatedPremium = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE clientValidation C-Win 
PROCEDURE clientValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
  
------------------------------------------------------------------------------*/
  define output parameter errorStatus as logical   no-undo. 
  define output parameter errorMsg    as character no-undo. 
   
  do with frame frBasic:
  end. 

  if ((fiCoverageAmt:screen-value ne "" and fiCoverageAmt:screen-value ne "0") or cbRateType:screen-value = {&NaUI}) and
     ((fiPriorAmt:sensitive and fiPriorAmt:screen-value ne "" and fiPriorAmt:screen-value ne "0" ) or not fiPriorAmt:sensitive) and
     cbRateType:screen-value ne  {&None} and 
     cbLoanRateType:screen-value = {&NaUI} then
  do:    
    errorStatus = false.
    return.
  end.
   
  if ((fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0") or cbLoanRateType:screen-value = {&NaUI}) and
     ((fiLoanPriorAmt:sensitive and fiLoanPriorAmt:screen-value ne "" and fiLoanPriorAmt:screen-value ne "0" ) or not fiLoanPriorAmt:sensitive) and
     ((fiEffectiveDate:sensitive and ((fiEffectiveDate:screen-value <> "") and (trim(replace(fiEffectiveDate:screen-value," ","")) <> "//"))) or not fiEffectiveDate:sensitive) and
     cbLoanRateType:screen-value ne {&None} and 
     cbRateType:screen-value = {&NaUI} then
  do:    
    errorStatus = false.
    return.
  end.
       
  if fiCoverageAmt:screen-value = "" or fiCoverageAmt:screen-value = "0" then
  do:
    if can-find(first tthowr where tthowr.hChecked = true) and cbRateType:screen-value ne {&NaUI} then
    do:
        errorMsg =  "Please enter coverage amount.".
        errorStatus = true.
        return.
    end.
    if fiLoanAmt:screen-value = "" or fiLoanAmt:screen-value = "0" then
    do:     
      if can-find(first tthLdr where tthLdr.hChecked = true) and cbLoanRateType:screen-value ne {&NaUI} then
      do:
        errorMsg =  "Please enter loan amount.".
        errorStatus = true.
        return.
      end.
      if (cbRateType:screen-value = {&NaUI} and cbLoanRateType:screen-value = {&None}) or 
        (cbLoanRateType:screen-value = {&NaUI} and cbRateType:screen-value = {&None})
        and ((fiPriorAmt:sensitive and fiPriorAmt:screen-value ne "" and fiPriorAmt:screen-value ne "0" ) or not fiPriorAmt:sensitive) 
        and ((fiLoanPriorAmt:sensitive and fiLoanPriorAmt:screen-value ne "" and fiLoanPriorAmt:screen-value ne "0" ) or not fiLoanPriorAmt:sensitive)
        and ((fiEffectiveDate:sensitive and ((fiEffectiveDate:screen-value <> "") and (trim(replace(fiEffectiveDate:screen-value," ","")) <> "//"))) or not fiEffectiveDate:sensitive) then
      do:         
        errorStatus = false.
        return.
      end.
      if (cbRateType:screen-value ne {&None} and cbRateType:screen-value ne {&NaUI}) and cbLoanRateType:screen-value ne {&None}  then
      do:
        errorMsg = "Please enter coverage amount.".
        errorStatus = true.
        return.
      end.
      if (cbLoanRateType:screen-value ne {&None} and cbLoanRateType:screen-value ne {&NaUI}) then
      do:
        errorMsg = "Please enter loan amount.".
        errorStatus = true.
        return.
      end.
      errorMsg = "Please enter coverage amount.".
      errorStatus = true.
      return.
    end.     
    else if cbLoanRateType:screen-value ne {&None}  then
    do:
      if cbRateType:screen-value ne {&None} then
      do:
        errorMsg = "Please enter coverage amount.".
        errorStatus = true.
        return.
      end.      
    end.
    else if cbLoanRateType:screen-value = {&None}  then
    do:
      errorMsg = "Please select loan rate type.".
      errorStatus = true.
      return.
    end.  
    if fiLoanPriorAmt:sensitive and (fiLoanPriorAmt:screen-value eq "" or fiLoanPriorAmt:screen-value eq "0" ) then
    do:         
      errorMsg =  "Please enter prior loan amount.".
      errorStatus = true.
      return.
    end.
    if fiEffectiveDate:sensitive and ((fiEffectiveDate:screen-value = "") or (trim(replace(fiEffectiveDate:screen-value," ","")) = "//")) then
    do:
      errorMsg = "Please enter prior policy date.".
      errorStatus = true.
      return.
    end.
  end.

  else if cbRateType:screen-value = {&None}  then
  do:
    errorMsg = "Please select rate type.".
    errorStatus = true.
    return.
  end.
  else if fiPriorAmt:sensitive and (fiPriorAmt:screen-value eq "" or fiPriorAmt:screen-value eq "0" ) then
  do:
    errorMsg =  "Please enter Prior Owner amount.".
    errorStatus = true.
    return.
  end.
  else if (fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0" ) and
        (cbLoanRateType:screen-value = {&None}) then
  do:
    errorMsg = "Please select loan rate type.".
    errorStatus = true.
    return.
  end.
  
  else if (fiLoanAmt:screen-value eq "" or fiLoanAmt:screen-value eq "0" ) and
        (cbLoanRateType:screen-value ne {&None}) then
  do:
    errorMsg = "Please enter loan amount.".
    errorStatus = true.
    return.
  end.

  else if (fiLoanAmt:screen-value eq "" or fiLoanAmt:screen-value eq "0" ) and
       (cbLoanRateType:screen-value eq {&None}) 
     and can-find(first tthLdr where tthLdr.hChecked = true) then
  do:
    errorMsg = "Please enter loan amount.".
    errorStatus = true.
    return.
  end. 
  else if fiLoanPriorAmt:sensitive and (fiLoanPriorAmt:screen-value eq "" or fiLoanPriorAmt:screen-value eq "0" ) then
  do:         
    errorMsg =  "Please enter prior loan amount.".
    errorStatus = true.
    return.
  end.
  else if fiEffectiveDate:sensitive and cbRateType:screen-value = {&None} and ((fiEffectiveDate:screen-value = "") or (trim(replace(fiEffectiveDate:screen-value," ","")) = "//")) then
  do:
    errorMsg = "Please enter prior policy date.".
    errorStatus = true.
    return.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE coverageAmountValueChange C-Win 
PROCEDURE coverageAmountValueChange :
/*------------------------------------------------------------------------------
  Purpose:     Handle simultaneous case (if user enters amount in both owner 
               and loan section then it is considered as simultaneous case
               and action to be taken
               - update ratetype list
               - handle simo checkbox) 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pclistOwner               as character no-undo.
  define variable pclistLoan                as character no-undo.
  define variable pclistScndLoan            as character no-undo.
  define variable cRatetype                 as character no-undo.
  define variable cLoanRatetype             as character no-undo.
  
  do with frame frBasic:
  end.
  
  assign
    cRatetype     = cbRateType:screen-value
    cLoanRatetype = cbLoanRateType:screen-value.
  
  if fiCoverageAmt:screen-value ne "0" and tempLoanAmount ne "" and tempLoanAmount ne "0" then
  do:
    assign
      tbSimultaneous:checked = true.
    /*-----------------------------STATE SPECIFIC CODE---------------------------------------*/    
    publish "GetFLSimoComboList"(cbPropType:screen-value,
                                 yes,
                                 output pclistOwner,  
                                 output pclistLoan).
  end.
  else 
  do:
    assign
      tbSimultaneous:checked = false. 
    /*-----------------------------STATE SPECIFIC CODE---------------------------------------*/      
    publish "GetFLComboLists"(cbPropType:screen-value,
                              output pclistOwner,  
                              output pclistLoan).
  
  end.
  
  run clearResults in this-procedure no-error.
  apply 'value-changed' to tbSimultaneous.
  assign
    cRatetype                      =  cbRateType:screen-value
    cbRateType:list-item-pairs     =  trim(pclistOwner, ",")
    cbLoanRateType:list-item-pairs =  trim(pclistLoan, ","). 
 
  if lookup(cRatetype,cbRateType:list-item-pairs) = 0 then
    assign
       cbRateType:screen-value = {&None}
       fiPriorAmt:screen-value = ""
       fiPriorAmt:sensitive    = false. 
  else cbRateType:screen-value   = cRatetype.    
 
  if lookup(cLoanRatetype,cbLoanRateType:list-item-pairs) = 0 then
    assign
      cbLoanRateType:screen-value   = {&None}
      fiLoanPriorAmt:screen-value   = ""
      fiLoanPriorAmt:sensitive      = false
      fiEffectiveDate:screen-value  = ?
      fiEffectiveDate:sensitive     = false.
  else cbLoanRateType:screen-value  = cLoanRatetype.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose: update the list on ui for selected checkbox of owner and loan
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                                                                                                                                                                       
  find first tthOwr where tthowr.hToggle = self no-error.
  if available tthowr then
  do:
    if tthowr.hChecked = false then
    do:
      assign tthowr.hChecked        = true
             tthowr.hEditor:fgcolor = 9.
      find first endorsement where endorsement.endorsementCode = tthowr.hseq no-error.
      if available endorsement then
        fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:screen-value in frame frBasicResults + "," + endorsement.endorsementCode , ",").
    end.
    else
    do:
      assign tthowr.hChecked        = false
             tthowr.hEditor:fgcolor = 0.
      find first endorsement where endorsement.endorsementCode = tthowr.hseq no-error.
      if available endorsement then
        fiOwnerEndorsList:screen-value in frame frBasicResults = RemoveItemFromList(fiOwnerEndorsList:screen-value in frame frBasicResults , string(endorsement.endorsementCode) , ",") no-error.
    end.
  end.
  
  find first tthLdr where tthLdr.hToggle = self no-error.
  if available tthLdr then
  do:
    if tthLdr.hChecked = false then
    do:
      assign tthLdr.hChecked        = true
             tthLdr.hEditor:fgcolor = 9.
      find first endorsement where endorsement.endorsementCode = tthLdr.hseq no-error.
      if available endorsement then
        fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:screen-value in frame frBasicResults + "," + endorsement.endorsementCode , ",").
    end.
    else
    do:
      assign tthLdr.hChecked        = false
             tthLdr.hEditor:fgcolor = 0.
      find first endorsement where endorsement.endorsementCode = tthLdr.hseq no-error.
      if available endorsement then
        fiLendersEndorsList:screen-value in frame frBasicResults = RemoveItemFromList(fiLendersEndorsList:screen-value in frame frBasicResults , string(endorsement.endorsementCode) , ",") no-error.
    end.
  end.

  run clearResults in this-procedure no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doEditor C-Win 
PROCEDURE doEditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cbSeq as handle no-undo.
  
  apply 'value-changed' to cbSeq. 

  if cbSeq:checked then
   assign 
    cbSeq:checked = false
    self:fgcolor  = 0.
  else
   assign
    cbSeq:checked = true
    self:fgcolor  = 9.

  apply 'entry' to cbSeq.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  DISPLAY cbPropType fiCoverageAmt cbRateType fiPriorAmt fiLoanAmt 
          cbLoanRateType tbSimultaneous fiLoanPriorAmt fiEffectiveDate fiVersion 
      WITH FRAME frBasic IN WINDOW C-Win.
  ENABLE cbPropType fiCoverageAmt cbRateType fiPriorAmt fiLoanAmt 
         cbLoanRateType fiLoanPriorAmt fiEffectiveDate btendorsement RECT-56 
         RECT-60 
      WITH FRAME frBasic IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasic}
  VIEW FRAME frOwnerEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frOwnerEndors}
  VIEW FRAME frLenderEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frLenderEndors}
  ENABLE RECT-67 RECT-68 
      WITH FRAME frBasicEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicEndors}
  DISPLAY fiOwnerEndorsList fiLendersEndorsList fiOwnerEndorsPremium 
          fiLenderEndorsPremium fiOwnerPolicyPremium fiLenderPolicyPremium 
          fiOwnersTotalPremium fiLendersTotalPremium figrandTotal 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  ENABLE btOEndors btLEndors btCalculate btClear fiOwnerEndorsList 
         fiLendersEndorsList fiOwnerEndorsPremium fiLenderEndorsPremium 
         fiOwnerPolicyPremium fiLenderPolicyPremium fiOwnersTotalPremium 
         fiLendersTotalPremium figrandTotal RECT-58 RECT-62 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicResults}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE endorsPremiumDetail C-Win 
PROCEDURE endorsPremiumDetail :
/*------------------------------------------------------------------------------
  Purpose:     create endorsement temp table for detailed premium to show on UI     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipPremiumEndorsDetail as character no-undo.
  define input parameter ipEndorsType          as character no-undo.
  define output parameter  table for endorsDetail.
  
  define variable iCount as integer  no-undo.
  
  do iCount = 1 to num-entries(ipPremiumEndorsDetail,"|"):
    find first endorsement where endorsement.endorsementCode = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-") and (endorsement.endorsementType = ipEndorsType or endorsement.endorsementType = "") no-error.
    if available endorsement then
    do:        
      create endorsDetail.
      assign
        endorsDetail.endorsCode    = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-")
        endorsDetail.endorsPremium = decimal(entry(2,entry(iCount,ipPremiumEndorsDetail,"|"),"-"))
        endorsDetail.endorsDesc    = endorsement.description no-error.
    end.    
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractOutputParameters C-Win 
PROCEDURE extractOutputParameters :
/*------------------------------------------------------------------------------
  Purpose:     parse output parameter of calculated premium 

  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount as integer no-undo.
  
  assign
    premiumOwner         = "premiumOwner"
    premiumOEndors       = "premiumOEndors"
    premiumOEndorsdetail = "premiumOEndorsdetail"
    premiumLoan          = "premiumLoan"
    premiumLEndors       = "premiumLEndors"
    premiumLEndorsdetail = "premiumLEndorsdetail"
    success              = "success"
    serverErrMsg         = "serverErrMsg"
    grandTotal           = "grandTotal"
    cardSetId            = "cardSetId"
    effectiveDate        = "manualEffectiveDate"
    ratesCode            = "RatesCode"
    ratesMsg             = "RatesMsg".    
  
  do iCount = 1 to num-entries(pcCalculatedPremium,","):
  
    case entry(1,entry(iCount,pcCalculatedPremium,","),"="):
  
      when premiumOwner  then
           premiumOwner                  = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
      
      when premiumOEndors  then
           premiumOEndors                = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
      
      when premiumOEndorsDetail  then
           premiumOEndorsDetail          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
      
      when premiumLoan then
           premiumLoan                   = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
      when premiumLEndors  then
           premiumLEndors                = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
    
      when premiumLEndorsDetail  then
           premiumLEndorsDetail          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
      
      when success then
           success                       = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
      
      when serverErrMsg then
           serverErrMsg                  = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
      
      
      when cardSetId  then                            
           cardSetId                     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                        
                                                                                                       
      when effectiveDate  then
           effectiveDate                 = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.
           
      when ratesCode then                                                                                                                               
           ratesCode                     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error. 
           
      when ratesMsg then                                                                                                                               
           ratesMsg                      = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                
    
    end case.
  end.
  
  lSuccess = logical(success) no-error.
  
  if  premiumOwner = "premiumOwner" then
      premiumOwner = "0".
  if  premiumOEndors = "premiumOEndors" then
      premiumOEndors = "0".
  if  premiumLoan = "premiumLoan" then
      premiumLoan = "0".
  if  premiumLEndors = "premiumLEndors" then
      premiumLEndors = "0". 
  if  ratesCode = "RateCode" then
      ratesCode = "". 
  if ratesMsg = "RatesMsg" then
     ratesMsg = "".    
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrameLEndors C-Win 
PROCEDURE initializeFrameLEndors :
/*------------------------------------------------------------------------------
  Purpose: Initialize endorsement frame for lender create check box and editor
           for description for each endorsement  

  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
  
  assign iNextEndors = 2.
  
  for each endorsement where endorsement.endorsementType = {&Lenders} or endorsement.endorsementType = ""  :
    iEndCount = iEndCount + 1.
    {lib\dynamicCheckBox.i  &tableName = "endorsement" , &seq = iEndCount , &frameName = "frLenderEndors" , &hTemp = "tthldr"}.
    iNextEndors = iNextEndors + 2.
    if iNextEndors ge 12 then frame frLenderEndors:virtual-height = frame frLenderEndors:virtual-height + 2.
  end. 
  
  run ShowScrollBars(frame frLenderEndors:handle, no, yes) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrameOEndors C-Win 
PROCEDURE initializeFrameOEndors :
/*------------------------------------------------------------------------------
  Purpose: Initialize endorsement frame for owner create check box and editor
           for description for each endorsement   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.
  
  assign iNextEndors = 2.
  
  for each endorsement where endorsement.endorsementType = {&Owners} or endorsement.endorsementType = ""   :
    iEndCount = iEndCount + 1.
    {lib\dynamicCheckBox.i  &tableName = "endorsement" , &seq = iEndCount , &frameName = "frOwnerEndors" , &hTemp = "tthOwr"}.
    if iNextEndors ge 12 then frame frOwnerEndors:virtual-height = frame frOwnerEndors:virtual-height + 2.
    iNextEndors = iNextEndors + 2.  
  end. 
  
  run ShowScrollBars(frame frOwnerEndors:handle, no, yes) no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE loanAmountValueChange C-Win 
PROCEDURE loanAmountValueChange :
/*------------------------------------------------------------------------------
  Purpose:  Handle simultaneous case (if user enters amount in both owner 
               and loan section then it is considered as simultaneous case
               and action to be taken
               - update ratetype list
               - handle simo checkbox)   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable pclistOwner   as character no-undo.
  define variable pclistLoan    as character no-undo.
  define variable cRatetype     as character no-undo.
  define variable cLoanRatetype as character no-undo.
  
  do with frame frBasic:
  end.
  
  assign 
    cRatetype     = cbRateType:screen-value
    cLoanRatetype = cbLoanRateType:screen-value.

  if tempcoverageAmount ne "0" and fiLoanAmt:screen-value ne "" and fiLoanAmt:screen-value ne "0"  then
  do:
    assign
      tbSimultaneous:checked = true.  
    publish "GetFLSimoComboList"(cbPropType:screen-value,
                                 yes,
                                 output pclistOwner,  
                                 output pclistLoan).
  end.
  else 
  do:
    assign
      tbSimultaneous:checked = false.
    publish "GetFLComboLists"(cbPropType:screen-value,
                              output pclistOwner,  
                              output pclistLoan).
  end.


  run clearResults in this-procedure no-error.
  apply 'value-changed' to tbSimultaneous.
  cRatetype = cbRateType:screen-value.

  assign 
    cbRateType:list-item-pairs     =  trim(pclistOwner, ",")
    cbLoanRateType:list-item-pairs =  trim(pclistLoan, ",").  

  if lookup(cRatetype,cbRateType:list-item-pairs) = 0 
   then
    assign
      cbRateType:screen-value  = {&None}
      fiPriorAmt:screen-value  = ""
      fiPriorAmt:sensitive     = false. 
  else cbRateType:screen-value = cRatetype.

  if lookup(cLoanRatetype,cbLoanRateType:list-item-pairs) = 0 
   then
    assign
      cbLoanRateType:screen-value    = {&None}
      fiLoanPriorAmt:screen-value    = ""
      fiLoanPriorAmt:sensitive       = false
      fiEffectiveDate:screen-value   = ?
      fiEffectiveDate:sensitive      = false.
  else cbLoanRateType:screen-value   = cLoanRatetype.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE propertyTypeValueChange C-Win 
PROCEDURE propertyTypeValueChange :
/*------------------------------------------------------------------------------
  Purpose:  handle UI state and rate type list for selected property type       
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasic:
  end.
  
  define variable pclistOwner            as character no-undo.
  define variable pclistLoan             as character no-undo.
  
  define variable trackRateType          as character no-undo.
  define variable trackLoanRateType      as character no-undo.
  
  publish "GetFLComboLists"  (cbPropType:screen-value,
                              output pclistOwner,  
                              output pclistLoan).
  
  assign
    trackRateType     = cbRateType:screen-value
    trackLoanRateType = cbLoanRateType:screen-value.
  
  cbRateType:list-item-pairs     =  trim(pclistOwner , ",").
  cbLoanRateType:list-item-pairs =  trim(pclistLoan , ",").  
   
  run setWidgeState in this-procedure(true) no-error.
  
  if can-do(cbRateType:list-item-pairs,trackRateType) then
    cbRateType:screen-value  = trackRateType.
  else
    cbRateType:screen-value  = {&None}.
  
  if can-do(cbLoanRateType:list-item-pairs,trackLoanRateType) then
    cbLoanRateType:screen-value  = trackLoanRateType.
  else
    cbLoanRateType:screen-value  = {&None}.
  
  apply 'value-changed' to cbRateType.
  apply 'value-changed' to cbLoanRateType.
  apply 'leave'         to fiCoverageAmt.
  apply 'value-changed' to fiCoverageAmt.
  apply 'value-changed' to fiLoanAmt.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshEndors C-Win 
PROCEDURE refreshEndors :
/*------------------------------------------------------------------------------
  Purpose:    empty endorsement list   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input parameter pCase as character no-undo.
  
  case pCase:
    when {&Owners} then
      for each tthOwr where tthOwr.hChecked = true:
        apply "value-changed":U to tthOwr.hToggle.
        tthOwr.hToggle:checked = false.
      end.
    when {&Lenders} then
      for each tthLdr where tthldr.hChecked = true:
        apply "value-changed":U to tthldr.hToggle.
        tthldr.hToggle:checked = false.
      end.
    otherwise
      do:
        for each tthOwr where tthOwr.hChecked = true:
          apply "value-changed":U to tthOwr.hToggle.
          tthOwr.hToggle:checked = false.
        end.
        for each tthLdr where tthldr.hChecked = true:
          apply "value-changed":U to tthldr.hToggle.
          tthldr.hToggle:checked = false.
        end.
      end.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setLoanPriorInfoState C-Win 
PROCEDURE setLoanPriorInfoState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable loShowPriorPolicyAmount as logical no-undo.
  define variable loShowPriorPolicyDate   as logical no-undo.

  /*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
  publish "GetFLPriorPolicyAmountConfig" (input cbPropType:screen-value in frame frBasic,
                                          input {&Lenders},
                                          input cbLoanRateType:screen-value,                                      
                                          output loShowPriorPolicyAmount). 

  publish "GetFLPriorPolicyDateConfig" (input cbPropType:screen-value in frame frBasic,
                                        input {&Lenders},
                                        input cbLoanRateType:screen-value,                                      
                                        output loShowPriorPolicyDate).  

  if not loShowPriorPolicyAmount then fiLoanPriorAmt:screen-value = "0".
  fiLoanPriorAmt:sensitive = loShowPriorPolicyAmount.  
  
  if not loShowPriorPolicyDate then fiEffectiveDate:screen-value = ?.
  fiEffectiveDate:sensitive = loShowPriorPolicyDate.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setOwnerPriorInfoState C-Win 
PROCEDURE setOwnerPriorInfoState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable loShowPriorPolicyAmount as logical no-undo. 

  do with frame {&frame-name}:
  end.
  /*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
  publish "GetFLPriorPolicyAmountConfig" (input cbPropType:screen-value in frame frBasic,
                                          input {&Owners},
                                          input cbRateType:screen-value,
                                          output loShowPriorPolicyAmount).
 
  if not loShowPriorPolicyAmount then fiPriorAmt:screen-value = "0".
  fiPriorAmt:sensitive = loShowPriorPolicyAmount. 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setPosition C-Win 
PROCEDURE setPosition :
/*------------------------------------------------------------------------------
  Purpose:   resize ui based on endorsements are shown or hidden  
  Parameters:  <none>
  Notes:       
  ------------------------------------------------------------------------------*/
  if  endselected = false then
  do:          
    frame frBasicEndors:visible = false.
    c-win:height = cutheight.
    c-win:width  = cutwidth.
    frame frBasicResults:row = endorsPos.
  end.
  
  else 
  do:
    frame frBasicResults:visible = true.    
    frame frBasicEndors:visible = true.    
    c-win:height = fullheight.
    c-win:width  = cutwidth.    
    frame frBasicResults:row = resultsPos.
    frame frBasicEndors:row = endorsPos .     

    run ShowScrollBars(frame frBasic:handle, no,no) no-error.
    run ShowScrollBars(frame frBasicResults:handle, no, no) no-error.
    run ShowScrollBars(frame frBasicEndors:handle, no, no) no-error.    
    run ShowScrollBars(frame {&frame-name}:handle, no,no) no-error.
  end. 
  /*--------------------------for endors trigger--------------------------------*/
  if endselected = true then
  do:
    if flag = true then
    do:
      run InitializeFrameOEndors in this-procedure no-error.
      run InitializeFrameLEndors in this-procedure no-error.
    end.
    flag = false.
  end.
/*------------------------------------------------------------------*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setWidgeState C-Win 
PROCEDURE setWidgeState :
/*------------------------------------------------------------------------------
  Purpose: set widgets conditionally based on property type         
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pState as logical no-undo.

  do with frame frBasic:
  end.
  
  if not pState then
    assign
      fiCoverageAmt:screen-value    = "0"
      fiPriorAmt:screen-value       = "0" . 
  
  assign
    fiCoverageAmt:sensitive  = pState  
    cbRateType:sensitive     = pState  
    fiPriorAmt:sensitive     = pState.
  
  for each tthOwr:
    assign
      tthOwr.hToggle:sensitive = pState
      tthOwr.hEditor:sensitive = pState.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION removeItemFromList C-Win 
FUNCTION removeItemFromList returns character
    (pclist       as character,
     pcremoveitem as character,
     pcdelimiter  as character):
 
  define variable liPos as integer no-undo.
  
  liPos = lookup(pcremoveitem,pclist,pcdelimiter).
  
  if liPos > 0 then
  do:
    assign entry(liPos,pclist,pcdelimiter) = "" no-error.

    if liPos = 1 then
      pclist = substring(pclist,2).
    else if liPos = num-entries(pclist,pcdelimiter) then
      pclist = substring(pclist,1,length(pclist) - 1).
    else
      pclist = replace(pclist,pcdelimiter + pcdelimiter,pcdelimiter).
  end.

  return pclist.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

