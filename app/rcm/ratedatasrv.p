&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : rcm/ratedatasrv.p
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Archana Gupta
    Created     :
    Notes       : 
    Modified    :
    
    Date          Name             Comments
    07/01/2019    Archana Gupta    Modified for implementing Region 
                                   related changes
    12/18/2019    Anubha Jain      Added implementation for ratescenarios
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib\std-def.i}

/* Temp-table Definitions ---                                           */
{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratetable.i}
{tt\raterule.i}
{tt\ratelog.i}

{tt\ratestate.i &tablealias=tempRateState}
{tt\ratecard.i  &tablealias=tempRateCard}
{tt\rateTable.i &tablealias=tempRateTable}
{tt\raterule.i  &tablealias=tempRateRule}

{tt\ratestate.i &tablealias=ttOutRateState}
{tt\ratecard.i  &tablealias=ttOutRateCard}
{tt\rateTable.i &tablealias=ttOutRateTable}
{tt\raterule.i  &tablealias=ttOutRateRule}

{tt\ratelog.i &tablealias=tempRateLog}
{tt\state.i} 
{tt\syscode.i}
{tt\syscode.i &tablealias=tempSyscode}

{tt\ratescenario.i     &tablealias=tempratescenario}
{tt\ratescenariocard.i &tablealias=tempratescenariocard}

/* Local Variable Definitions ---                                       */
define variable tLoadedStates as logical initial false no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15.05
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-ActivateRateState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActivateRateState Procedure 
PROCEDURE ActivateRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCardSetID as integer no-undo.
  define output parameter table for rateState.
  define output parameter table for tempRateState.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/activateratestate.p (input pCardSetID,
                                  output table rateState,
                                  output table tempRateState,
                                  output lSuccess,
                                  output cMsg) no-error.
  
  session:set-wait-state("").
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddRateCardDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddRateCardDetail Procedure 
PROCEDURE AddRateCardDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for rateCard.
  define input  parameter table for rateTable.
  define input  parameter table for rateRule.
  
  define output parameter table for tempratecard.
  define output parameter table for tempratetable.
  define output parameter table for tempraterule.
  define output parameter table for tempratestate.
  
  define output parameter oplSuccess as logical.
  
  define variable cMsg     as character.
  
  session:set-wait-state("general").
  run server/newratecarddetail.p (input  table rateCard,
                                  input  table rateTable,
                                  input  table rateRule,
                                  output table tempratecard,
                                  output table tempratetable,
                                  output table tempraterule,
                                  output table tempratestate,
                                  output oplSuccess,
                                  output cMsg) no-error.
  session:set-wait-state("").
  if not oplSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-AddRateState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AddRateState Procedure 
PROCEDURE AddRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tempratestate.
  define output parameter table for tempratestate.
  define output parameter oplSuccess as logical no-undo.
  
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/addratestate.p (input table tempratestate,
                             output table tempratestate,
                             output oplSuccess,
                             output cMsg).
  session:set-wait-state("").
  
  if not oplSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-calculatePremiumService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calculatePremiumService Procedure 
PROCEDURE calculatePremiumService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pcUiInput           as character   no-undo.
  define input  parameter pcStateID           as character   no-undo.
  define input  parameter piVersion           as integer     no-undo.
  define output parameter pcCalculatedPremium as character   no-undo.
  define output parameter table for rateLog.
  define output parameter oplSuccess          as logical     no-undo.
  define output parameter opcMsg              as character   no-undo.
  
  session:set-wait-state("general").
  run server\calculatepremium.p(input pcUiInput,        
                                input pcStateID, 
                                input piVersion,
                                output pcCalculatedPremium, 
                                output table rateLog,       
                                output oplSuccess,            
                                output opcMsg).  
  
  session:set-wait-state("").
  
  if not oplSuccess 
   then
    do:
      message opcMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CopyRateState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopyRateState Procedure 
PROCEDURE CopyRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tempRateState.
  define input  parameter table for tempRateCard.
  define output parameter table for ttOutRatestate.
  define output parameter oplSuccess as logical no-undo.
  
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/copyratestate.p (input  table tempratestate,
                              input  table tempratecard,
                              output table ttOutRateState,
                              output oplSuccess,
                              output cMsg).
  session:set-wait-state("").
  if not oplSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CopyRegions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopyRegions Procedure 
PROCEDURE CopyRegions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipchRegion as character no-undo.
  define input  parameter table for tempRateCard.
  define output parameter table for ttOutRateCard.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/copyregion.p (input ipchRegion,
                              input  table tempratecard,                           
                              output table ttOutRateCard,                           
                              output lSuccess,
                              output cMsg).
  session:set-wait-state("").
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteRateCardDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteRateCardDetail Procedure 
PROCEDURE DeleteRateCardDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCardID    as integer     no-undo.
  define output parameter table for tempRateState.  
  define output parameter oplSuccess as logical     no-undo.
  define output parameter opcMsg     as character   no-undo.
  
  
  session:set-wait-state("general").
  run server/deleteratecard.p (input pCardID,  
                                     output table tempratestate,
                                     output oplSuccess,
                                     output opcMsg).
  session:set-wait-state("").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteRateScenario) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteRateScenario Procedure 
PROCEDURE DeleteRateScenario :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pScenarioID  as integer no-undo.
  define output parameter oplDelete    as logical no-undo.
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/deleteratescenario.p ( input pScenarioID,
                                    output lSuccess,
                                    output cMsg).
  session:set-wait-state("").
  oplDelete = lSuccess.
  
  if not lSuccess 
   then
    do:
       message cMsg
         view-as alert-box info buttons OK.
       return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteRateState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteRateState Procedure 
PROCEDURE DeleteRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCardSetID   as integer no-undo.
  define output parameter oplDelete    as logical no-undo.
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/deleteratestate.p ( input pCardSetID,
                                 output lSuccess,
                                 output cMsg).
  session:set-wait-state("").
  oplDelete = lSuccess.
  
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-DeleteSyscodeService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DeleteSyscodeService Procedure 
PROCEDURE DeleteSyscodeService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCodeType as character no-undo.
  define input  parameter pcode     as character no-undo.
  define output parameter pSuccess  as logical   no-undo.
  define output parameter pMsg      as character no-undo.
  
  run server/deletesyscode.p (input  pCodeType,
                              input pCode,
                              output pSuccess,
                              output pMsg).
  
  session:set-wait-state("").
  if not pSuccess 
   then
    message pMsg
     view-as alert-box info buttons ok.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetAttributes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetAttributes Procedure 
PROCEDURE GetAttributes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

define input  parameter piCardName   as character no-undo.
define input  parameter piCardSetID  as integer   no-undo.
define input  parameter piRegion     as character no-undo.
define output parameter poAttrIDList as character no-undo.

define variable lSuccess as logical   no-undo.
define variable cMsg     as character no-undo.

session:set-wait-state("general").
run server/getattributes.p (input piCardName,
                            input piCardSetID,
                            input piRegion,
                            output poAttrIDList,
                            output lSuccess,
                            output cMsg).
session:set-wait-state("").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCardNamesList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCardNamesList Procedure 
PROCEDURE GetCardNamesList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCardSetID    as integer   no-undo.
  define output parameter pCardNameList as character no-undo.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/getratecardnames.p ( input  pCardSetID,
                                  output pCardNameList,
                                  output lSuccess,
                                  output cMsg).
  
  session:set-wait-state("").
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCodesService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCodesService Procedure 
PROCEDURE GetCodesService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCodeType as character no-undo.
  define output parameter table for tempsyscode.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  run server/getsyscodes.p (input pcodetype, 
                            output table tempsyscode,
                            output lSuccess,
                            output cMsg).
  
  session:set-wait-state("").
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetRateCardDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRateCardDetail Procedure 
PROCEDURE GetRateCardDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCardSetID as integer no-undo.
  define input  parameter pCardID    as integer no-undo.
  define output parameter table for tempRateCard.
  define output parameter table for tempRateTable.
  define output parameter table for tempRateRule.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  empty temp-table tempRateCard.
  empty temp-table tempRateTable.
  empty temp-table tempRateRule.
  
  session:set-wait-state("general").
  run server/getratecarddetail.p (input pCardSetID,
                                  input pCardID,
                                  output table tempRateCard,      
                                  output table tempRateTable,        
                                  output table tempRateRule,  
                                  output lSuccess,
                                  output cMsg).
  session:set-wait-state("").
  
  if not lSuccess
   then
    do:
      message cMsg 
       view-as alert-box error buttons ok.
      return error .
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetRateCards) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRateCards Procedure 
PROCEDURE GetRateCards :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pCardSetID as integer no-undo.
  define output parameter table for tempRateCard.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  empty temp-table tempRateCard.
  
  session:set-wait-state("general").
  run server/getratecards.p ( input pCardSetID,
                              output table tempRateCard,
                              output lSuccess,
                              output cMsg).
  
  session:set-wait-state("").
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetRateScenarios) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRateScenarios Procedure 
PROCEDURE GetRateScenarios :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter piCardSetID as integer no-undo.
  define input  parameter plCardsReq  as logical no-undo.
  define output parameter table for tempratescenario.
  define output parameter table for tempratescenariocard.

  define variable lSuccess as logical     no-undo.
  define variable cMsg     as character   no-undo.
    
  empty temp-table tempratescenario.
  empty temp-table tempratescenariocard.
  
  session:set-wait-state("general").
  run server/getratescenarios.p (input piCardSetID,
                                 input plCardsReq,   /* to indicate that rateScenarioCards needs to be refreshed or not */ 
                                 output table tempratescenario,
                                 output table tempratescenariocard,
                                 output lSuccess,
                                 output cMsg).
  session:set-wait-state("").
  
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetRateStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRateStates Procedure 
PROCEDURE GetRateStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter pState as character no-undo.
  define output parameter table for rateState.

  define variable lSuccess as logical         no-undo.
  define variable cMsg     as character       no-undo.
  
  session:set-wait-state("general").
  run server/getratestates.p (input pState,
                              output table rateState,
                              output lSuccess,
                              output cMsg).
  session:set-wait-state("").
  if not lSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStateName) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateName Procedure 
PROCEDURE GetStateName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcStateId as character no-undo.
  define output parameter opcStateName as character no-undo.
  
  if not tLoadedStates 
   then 
    run LoadStates in this-procedure.
    
  find first state where state.stateId = ipcStateId no-error.
  
  if available state then
     opcStateName = state.description.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStates Procedure 
PROCEDURE GetStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for state.
  
  if not tLoadedStates 
   then 
    run LoadStates in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadStates) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadStates Procedure 
PROCEDURE LoadStates :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define buffer state for state.

  define variable cMsg as character no-undo.
  
  empty temp-table state.
  
  run server/getstates.p (input "",
                          output table state,
                          output tLoadedStates,
                          output cMsg).
  if not tLoadedStates
   then 
    do: 
      std-lo = false.
      publish "GetAppDebug" (output std-lo).

      if std-lo 
       then 
        message "LoadStates failed: " cMsg 
         view-as alert-box warning.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-LoadSyscodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSyscodes Procedure 
PROCEDURE LoadSyscodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pCodeType as character no-undo.

  define buffer syscode     for syscode.
  define buffer tempsyscode for tempsyscode.

  define variable tLoadedSysCodes as logical no-undo.

  empty temp-table syscode.

  run server/getsyscodes.p (pCodeType, 
                            output table tempsyscode,
                            output std-lo,
                            output std-ch).
  if std-lo 
   then
     for each tempsyscode:
       create syscode.
       buffer-copy tempsyscode to syscode.
     end.

  tLoadedSysCodes = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyRateCardDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyRateCardDetail Procedure 
PROCEDURE ModifyRateCardDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for rateCard.
  define input  parameter table for rateTable.
  define input  parameter table for rateRule.
  
  define output parameter table for tempratecard.
  define output parameter table for tempratetable.
  define output parameter table for tempraterule.
  define output parameter table for tempratestate.
  
  define output parameter oplSuccess as logical.
  
  define variable cMsg     as character.
  
  session:set-wait-state("general").
  run server/modifyratecarddetail.p (input  table rateCard,
                                  input  table rateTable,
                                  input  table rateRule,
                                  output table tempratecard,
                                  output table tempratetable,
                                  output table tempraterule,
                                  output table tempratestate,
                                  output oplSuccess,
                                  output cMsg) no-error.
  session:set-wait-state("").
  if not oplSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifyRateState) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifyRateState Procedure 
PROCEDURE ModifyRateState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter table for tempratestate.
  define output parameter table for tempratestate.
  define output parameter oplSuccess as logical no-undo.
  
  define variable cMsg     as character no-undo.
  
  session:set-wait-state("general").
  run server/modifyratestate.p (input table tempratestate,
                               output table tempratestate,
                               output oplSuccess,
                               output cMsg).
  session:set-wait-state("").
  
  if not oplSuccess 
   then
    do:
      message cMsg
        view-as alert-box info buttons OK.
      return error.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-ModifySyscodeService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ModifySyscodeService Procedure 
PROCEDURE ModifySyscodeService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tempsyscode.
  define output parameter pSuccess as logical initial false no-undo.
  define output parameter pMsg     as character             no-undo.
  
  run server/modifysyscode.p (input table tempsyscode,
                              output pSuccess,
                              output pMsg).
  session:set-wait-state("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-NewSyscodeService) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE NewSyscodeService Procedure 
PROCEDURE NewSyscodeService :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter table for tempsyscode.
  define output parameter pSuccess as logical   initial false no-undo.
  define output parameter pMsg     as character               no-undo.
  
  run server/newsyscode.p (input table tempsyscode,
                           output pSuccess,
                           output pMsg).
  session:set-wait-state("").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-TestRateCardPremiumsrv) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TestRateCardPremiumsrv Procedure 
PROCEDURE TestRateCardPremiumsrv :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input  parameter pCardSetID         as integer no-undo.
define input  parameter pCardName          as character no-undo.
define input  parameter pRegion            as character no-undo.
define input  parameter pCoverage          as decimal   no-undo.
define input  parameter pAttr              as character   no-undo.
define output parameter pCalculatedPremium as character no-undo.
define output parameter table for tempRateLog.
define output parameter pSuccess          as logical no-undo.
define output parameter pMsg              as character no-undo.

  run server/testratecard.p (input pCardSetID,
                             input pCardName,
                             input pRegion,
                             input pCoverage,
                             input pAttr,
                             output pCalculatedPremium,
                             output table tempRateLog,
                             output pSuccess,
                             output pMsg).
  
  if not pSuccess 
   then
    do:
      message pMsg
        view-as alert-box info buttons OK.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

