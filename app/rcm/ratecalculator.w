&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
 
  Modified    :
  Date        Name     Comments   
  12/18/2019  Anubha   Modified a show region description in tooltip of region combo.
  01/24/2022  Vignesh  Added Logic for error handling.
  09/16/2022  SChandu  Added logic for entering parcels in Nevada state.
  09/14/2023  SChandu  Added Commitment logic for NewMexico state.
  07/16/2024  Vignesh  Dummy commit to revert the NM changes(rounding the total value)
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
{lib\rcm-std-def.i}
{lib\std-def.i}
{lib\winlaunch.i}
 {lib\encrypt.i}
 
/*Temp table to contain endorsement data*/
/* {tt\rateendorsements.i &tablealias=endorsement} */

/* Temp tables for log */
{tt\ratelog.i}

/* Contains temp-table to store region descriptions */
{tt\rateui.i}
/*Temp tables to manage dynamic checkbox and editor*/
{tt\rateendorsementui.i &tablealias=tthOwr}
{tt\rateendorsementui.i &tablealias=tthLdr}
{tt\rateendorsementui.i &tablealias=tthScndLoan}

/*Temp tables to show detailed premium of endorsements*/
{tt\rateendorsementpremium.i &tablealias=endorsDetail}
{tt\rateendorsements.i &tablealias=ownerEndorsement}
{tt\rateendorsements.i &tablealias=loanEndorsement}
{tt\rateendorsements.i &tablealias=secondLoanEndorsement}

/*Temp tables to handle open endorsements windows*/
define temp-table openEndors
    field hInstance as handle.

/*Temp tables to handle open log windows*/
define temp-table openLog
    field hInstance as handle.
                                                                   
/* Preprocessor used to define the status of the input and based on these status we form the validation */
&scoped-define Blank         "Blank" 
&scoped-define ValidInput    "ValidInput"
&scoped-define InvalidInput  "InvalidInput"

/* variables used in screen resizing*/
define variable endorsPos              as decimal                  no-undo.
define variable resultsPos             as decimal                  no-undo.
define variable fullheight             as decimal                  no-undo.
define variable cutwidth               as decimal                  no-undo.
define variable fullwidth              as decimal                  no-undo.
define variable cutheight              as decimal                  no-undo.
define variable endSelected            as logical   initial false  no-undo. 
define variable multiLoanSelected      as logical   initial false  no-undo. 

/*variables containing key value pair for ui input and output premium*/
define variable pcCalculatedPremium    as character                no-undo.
define variable pcInputValues          as character                no-undo.
define variable pcInputValuesPDF       as character                no-undo.
define variable logscreenparameter     as character                no-undo.

/* output parameter parsing variables */
define variable premiumOwner           as character                no-undo. 
define variable premiumOEndors         as character                no-undo. 
define variable premiumOEndorsDetail   as character                no-undo. 
define variable premiumLoan            as character                no-undo. 
define variable premiumLEndors         as character                no-undo. 
define variable premiumLEndorsDetail   as character                no-undo. 
define variable premiumScnd            as character                no-undo. 
define variable premiumSEndors         as character                no-undo. 
define variable premiumSEndorsDetail   as character                no-undo. 
define variable premiumLenderCPL       as character                no-undo. 
define variable premiumBuyerCPL        as character                no-undo. 
define variable premiumSellerCPL       as character                no-undo. 
define variable success                as character                no-undo. 
define variable cardSetId              as character                no-undo. 
define variable effectiveDate          as character                no-undo. 
define variable serverErrMsg           as character                no-undo. 

define variable ratesCode              as character                no-undo.
define variable ratesMsg               as character                no-undo.
                                                                             
/*type cased variables after parsing output parameter*/                      
define variable lSuccess               as logical                 no-undo  initial false.

/* variables to populate regioncombobox */
define variable chRegionList           as character                no-undo.

/* variables to populate propertytypecombobox */
define variable pcPropertyType         as character                no-undo.

/* variables to handle dynamic widget of endorsements*/
define variable iEndCount              as integer                  no-undo.
define variable iNextEndors            as integer                  no-undo.
define variable flag                   as logical                  no-undo initial true.

/* variable to handle second loan section */
define variable secondLoanEndorsLoaded as logical                  no-undo initial false. 
define variable secondLoanEndorsList   as logical                  no-undo initial false. 

/* variables to avoid reloading of endorsements when there is no change to Property type and Rate type */
define variable cLastORateType         as character                no-undo.
define variable cLastLRateType         as character                no-undo.
define variable cLastSLRateType        as character                no-undo.
define variable cLastPropType          as character                no-undo.
define variable cStateName             as character                no-undo.  
define variable cLastRegion            as character                no-undo.

/* variable to launch calculator for multiple versions */
define variable cStateID              as character                no-undo.
define variable CVersion              as character                no-undo.
define variable lHasEndors             as logical                  no-undo.
define variable iVersion              as integer                  no-undo.
define variable hState                as handle                   no-undo.
define variable cWinTitle              as character                no-undo.


  define variable ownerEndorsPR        as character   no-undo.
  define variable loanEndorsPR         as character   no-undo.
  define variable secondLoanEndorsPR   as character   no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD RemoveItemFromList C-Win 
FUNCTION RemoveItemFromList returns character
        (pclist as character,
         pcremoveitem as character,
         pcdelimiter as character) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btendorsement 
     LABEL "v" 
     SIZE 4.8 BY 1.13 TOOLTIP "Show endorsements".

DEFINE BUTTON btSecondLoan 
     LABEL " >" 
     SIZE 4.8 BY 1.13 TOOLTIP "Show".

DEFINE VARIABLE cbLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 41.6 BY 1 NO-UNDO.

DEFINE VARIABLE cbPropType AS CHARACTER FORMAT "X(256)":U INITIAL "R" 
     LABEL "Property Type" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 44.2 BY 1 NO-UNDO.

DEFINE VARIABLE cbRegion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Region" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE VARIABLE fiCoverageAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Coverage Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiEffectiveDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanEffectiveDate AS DATE FORMAT "99/99/99":U 
     LABEL "Effective Date" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiLoanPriorAmt AS DECIMAL FORMAT "zzz,zzz,zzz":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiPriorAmt AS DECIMAL FORMAT "zzz,zzz,zzz":U INITIAL 0 
     LABEL "Prior Policy Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE fiVersion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Version" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-56
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 6.35.

DEFINE RECTANGLE RECT-60
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 6.35.

DEFINE VARIABLE tbSimultaneous AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.6 BY .57 NO-UNDO.

DEFINE RECTANGLE RECT-67
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 10.7.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 10.7.

DEFINE BUTTON btCalculate  NO-FOCUS
     LABEL "&Calculate" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Calculate(Alt + C)".

DEFINE BUTTON btClear  NO-FOCUS
     LABEL "Clear&x" 
     SIZE 7.2 BY 1.7 TOOLTIP "Clear(Alt + X)".

DEFINE BUTTON btLEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE BUTTON btlog  NO-FOCUS
     LABEL "Show Calcu&lations" 
     SIZE 7.2 BY 1.7 TOOLTIP "Show Ca&lculations(Alt + L)".

DEFINE BUTTON btOEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE BUTTON btRateSheet  NO-FOCUS
     LABEL "&PDF" 
     SIZE 7.2 BY 1.7 TOOLTIP "&Print Rates(Alt + P)".

DEFINE BUTTON btUrl  NO-FOCUS
     LABEL "URL" 
     SIZE 7.2 BY 1.7 TOOLTIP "Copy URL".

DEFINE VARIABLE fiBuyerCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "Buyer" 
     VIEW-AS FILL-IN 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiCPLAmt AS DECIMAL FORMAT "zz,zz9.99":U INITIAL 0 
     LABEL "CPL Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE figrandTotal AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Grand Total" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "Lender" 
     VIEW-AS FILL-IN 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderEndorsPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiLenderPolicyPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiLendersEndorsList AS CHARACTER FORMAT "X(1024)":U 
     LABEL "Loan Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiLendersTotalPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Loan Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerEndorsList AS CHARACTER FORMAT "X(1024)":U 
     LABEL "Owner Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 53.2 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerEndorsPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnerPolicyPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiOwnersTotalPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Owner Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiSellerCPL AS INTEGER FORMAT "9":U INITIAL 0 
     LABEL "Seller" 
     VIEW-AS FILL-IN 
     SIZE 4.2 BY 1 NO-UNDO.

DEFINE VARIABLE ftext AS CHARACTER FORMAT "X(256)":U INITIAL "Closing Protection Letters" 
      VIEW-AS TEXT 
     SIZE 29.6 BY .62
     FONT 6 NO-UNDO.

DEFINE RECTANGLE RECT-58
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.17.

DEFINE RECTANGLE RECT-62
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.8 BY 5.17.

DEFINE RECTANGLE RECT-70
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 2.04.

DEFINE BUTTON btScndClear 
     LABEL "SClear" 
     SIZE 4.8 BY 1.13 TOOLTIP "Clear Second Loan to enable hide".

DEFINE VARIABLE cbSecondLoanRateType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Rate to Apply" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "--Select Type--","--"
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE fiSecondLoanAmt AS DECIMAL FORMAT "zzz,zzz,zz9":U INITIAL 0 
     LABEL "Loan Amount" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-61
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 6.35.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 10.83.

DEFINE BUTTON btSEndors 
     LABEL "View Details" 
     SIZE 4.8 BY 1.13 TOOLTIP "View endorsements premium detail".

DEFINE VARIABLE fiScndLenderEndorsPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Endorsement Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiScndLenderPolicyPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Premium Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiScndLendersTotalPremium AS DECIMAL FORMAT "zzz,zz9.99":U INITIAL 0 
     LABEL "Second Loan Total Amount" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE fiSecondLendersEndorsList AS CHARACTER FORMAT "X(1024)":U 
     LABEL "Second Loan Endorsements" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 45 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-63
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.4 BY 5.17.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 263.8 BY 27.23 WIDGET-ID 100.

DEFINE FRAME frScndLoan
     btScndClear AT ROW 2.67 COL 83 WIDGET-ID 176 NO-TAB-STOP 
     fiSecondLoanAmt AT ROW 3.05 COL 39 COLON-ALIGNED WIDGET-ID 168
     cbSecondLoanRateType AT ROW 4.14 COL 39 COLON-ALIGNED WIDGET-ID 170
     "Second Loan" VIEW-AS TEXT
          SIZE 15.4 BY .62 AT ROW 2.33 COL 2.6 WIDGET-ID 174
          FONT 6
     RECT-61 AT ROW 2.57 COL 1 WIDGET-ID 172
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 176.4 ROW 1.19
         SIZE 87.6 BY 7.91 WIDGET-ID 800.

DEFINE FRAME frBasic
     cbRegion AT ROW 1.19 COL 8.6 COLON-ALIGNED WIDGET-ID 172
     cbPropType AT ROW 1.24 COL 42.8 COLON-ALIGNED WIDGET-ID 60
     tbSimultaneous AT ROW 1.48 COL 92.8 WIDGET-ID 116
     fiVersion AT ROW 1.29 COL 167.4 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     fiCoverageAmt AT ROW 3.05 COL 30.8 COLON-ALIGNED WIDGET-ID 16
     cbRateType AT ROW 4.14 COL 30.8 COLON-ALIGNED WIDGET-ID 12
     fiPriorAmt AT ROW 5.29 COL 30.8 COLON-ALIGNED WIDGET-ID 180
     fiEffectiveDate AT ROW 6.38 COL 30.8 COLON-ALIGNED WIDGET-ID 174
     fiLoanAmt AT ROW 3.05 COL 118.4 COLON-ALIGNED WIDGET-ID 46
     cbLoanRateType AT ROW 4.14 COL 118.4 COLON-ALIGNED WIDGET-ID 44
     fiLoanPriorAmt AT ROW 5.29 COL 118.4 COLON-ALIGNED WIDGET-ID 182
     fiLoanEffectiveDate AT ROW 6.38 COL 118.4 COLON-ALIGNED WIDGET-ID 184
     btSecondLoan AT ROW 7.57 COL 171.2 WIDGET-ID 158
     btendorsement AT ROW 7.57 COL 2.8 WIDGET-ID 150
     "Simultaneous" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 1.43 COL 95.6 WIDGET-ID 162
     "Owner" VIEW-AS TEXT
          SIZE 7.4 BY .62 AT ROW 2.24 COL 3.8 WIDGET-ID 58
          FONT 6
     "Loan" VIEW-AS TEXT
          SIZE 5.8 BY .62 AT ROW 2.24 COL 90.6 WIDGET-ID 66
          FONT 6
     "Second Loan" VIEW-AS TEXT
          SIZE 15.6 BY .62 AT ROW 7.81 COL 155.6 WIDGET-ID 160
          FONT 6
     "Endorsements" VIEW-AS TEXT
          SIZE 16.8 BY .62 AT ROW 7.81 COL 8.4 WIDGET-ID 170
          FONT 6
     RECT-56 AT ROW 2.57 COL 2.2 WIDGET-ID 2
     RECT-60 AT ROW 2.57 COL 89 WIDGET-ID 64
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.19
         SIZE 176.2 BY 7.91 WIDGET-ID 200.

DEFINE FRAME frScndLoanEndors
     RECT-69 AT ROW 1 COL 1 WIDGET-ID 158
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 176.4 ROW 9
         SIZE 87.6 BY 10.85 WIDGET-ID 900.

DEFINE FRAME frScndLenderEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.6 ROW 1.05
         SCROLLABLE SIZE 85.6 BY 100 WIDGET-ID 1200.

DEFINE FRAME frBasicEndors
     RECT-67 AT ROW 1 COL 2.2 WIDGET-ID 148
     RECT-68 AT ROW 1 COL 89 WIDGET-ID 150
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         NO-UNDERLINE THREE-D 
         AT COL 1 ROW 9
         SIZE 176.1 BY 10.85 WIDGET-ID 500.

DEFINE FRAME frLenderEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 90.6 ROW 1.1
         SCROLLABLE SIZE 86.6 BY 100 WIDGET-ID 700.

DEFINE FRAME frOwnerEndors
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.6 ROW 1.1
         SCROLLABLE SIZE 85.6 BY 100 WIDGET-ID 600.

DEFINE FRAME frBasicResults
     ftext AT ROW 6.52 COL 4 COLON-ALIGNED NO-LABEL WIDGET-ID 182 NO-TAB-STOP 
     btCalculate AT ROW 6.95 COL 138.8 WIDGET-ID 34 NO-TAB-STOP 
     btClear AT ROW 6.95 COL 146 WIDGET-ID 104 NO-TAB-STOP 
     btlog AT ROW 6.95 COL 160.4 WIDGET-ID 146 NO-TAB-STOP 
     btRateSheet AT ROW 6.95 COL 153.2 WIDGET-ID 152 NO-TAB-STOP 
     btUrl AT ROW 6.95 COL 167.6 WIDGET-ID 180 NO-TAB-STOP 
     btOEndors AT ROW 2.57 COL 49 WIDGET-ID 172
     btLEndors AT ROW 2.57 COL 136.6 WIDGET-ID 176
     fiOwnerEndorsList AT ROW 1.52 COL 30.8 COLON-ALIGNED WIDGET-ID 140 NO-TAB-STOP 
     fiLenderCPL AT ROW 7.29 COL 10.8 COLON-ALIGNED WIDGET-ID 156
     fiBuyerCPL AT ROW 7.29 COL 26 COLON-ALIGNED WIDGET-ID 162
     fiSellerCPL AT ROW 7.29 COL 41.6 COLON-ALIGNED WIDGET-ID 164
     fiLendersEndorsList AT ROW 1.52 COL 118.4 COLON-ALIGNED WIDGET-ID 142 NO-TAB-STOP 
     fiOwnerEndorsPremium AT ROW 2.57 COL 30.8 COLON-ALIGNED WIDGET-ID 100 NO-TAB-STOP 
     fiLenderEndorsPremium AT ROW 2.57 COL 118.4 COLON-ALIGNED WIDGET-ID 48 NO-TAB-STOP 
     fiOwnerPolicyPremium AT ROW 3.62 COL 30.8 COLON-ALIGNED WIDGET-ID 98 NO-TAB-STOP 
     fiLenderPolicyPremium AT ROW 3.62 COL 118.4 COLON-ALIGNED WIDGET-ID 36 NO-TAB-STOP 
     fiOwnersTotalPremium AT ROW 4.67 COL 30.8 COLON-ALIGNED WIDGET-ID 102 NO-TAB-STOP 
     fiLendersTotalPremium AT ROW 4.67 COL 118.4 COLON-ALIGNED WIDGET-ID 68 NO-TAB-STOP 
     figrandTotal AT ROW 7.29 COL 118.4 COLON-ALIGNED WIDGET-ID 108 NO-TAB-STOP 
     fiCPLAmt AT ROW 7.29 COL 69.8 COLON-ALIGNED WIDGET-ID 168 NO-TAB-STOP 
     RECT-58 AT ROW 1 COL 2.2 WIDGET-ID 2
     RECT-62 AT ROW 1 COL 89 WIDGET-ID 148
     RECT-70 AT ROW 6.81 COL 2.2 WIDGET-ID 166
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 19.71
         SIZE 175.8 BY 8.52
         DEFAULT-BUTTON btCalculate WIDGET-ID 400.

DEFINE FRAME frScndLoanResults
     fiSecondLendersEndorsList AT ROW 1.52 COL 39 COLON-ALIGNED WIDGET-ID 166 NO-TAB-STOP 
     fiScndLenderEndorsPremium AT ROW 2.57 COL 39 COLON-ALIGNED WIDGET-ID 160 NO-TAB-STOP 
     btSEndors AT ROW 2.57 COL 57.2 WIDGET-ID 168
     fiScndLenderPolicyPremium AT ROW 3.62 COL 39 COLON-ALIGNED WIDGET-ID 162 NO-TAB-STOP 
     fiScndLendersTotalPremium AT ROW 4.67 COL 39 COLON-ALIGNED WIDGET-ID 164 NO-TAB-STOP 
     RECT-63 AT ROW 1 COL 1 WIDGET-ID 148
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 176.4 ROW 19.71
         SIZE 87.6 BY 8.52 WIDGET-ID 1300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 28.13
         WIDTH              = 264.2
         MAX-HEIGHT         = 35.52
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 35.52
         VIRTUAL-WIDTH      = 273.2
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME frBasic:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicEndors:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frBasicResults:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frLenderEndors:FRAME = FRAME frBasicEndors:HANDLE
       FRAME frOwnerEndors:FRAME = FRAME frBasicEndors:HANDLE
       FRAME frScndLenderEndors:FRAME = FRAME frScndLoanEndors:HANDLE
       FRAME frScndLoan:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frScndLoanEndors:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME frScndLoanResults:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME frBasicResults:MOVE-BEFORE-TAB-ITEM (FRAME frBasic:HANDLE)
       XXTABVALXX = FRAME frScndLoanResults:MOVE-BEFORE-TAB-ITEM (FRAME frBasicResults:HANDLE)
       XXTABVALXX = FRAME frScndLoanEndors:MOVE-BEFORE-TAB-ITEM (FRAME frScndLoanResults:HANDLE)
       XXTABVALXX = FRAME frBasicEndors:MOVE-BEFORE-TAB-ITEM (FRAME frScndLoanEndors:HANDLE)
       XXTABVALXX = FRAME frScndLoan:MOVE-BEFORE-TAB-ITEM (FRAME frBasicEndors:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasic
   Custom                                                               */
/* SETTINGS FOR BUTTON btSecondLoan IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbRegion IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fiEffectiveDate IN FRAME frBasic
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN fiLoanEffectiveDate IN FRAME frBasic
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN fiLoanPriorAmt IN FRAME frBasic
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN fiPriorAmt IN FRAME frBasic
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN fiVersion IN FRAME frBasic
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frBasicEndors
   Custom                                                               */
ASSIGN XXTABVALXX = FRAME frOwnerEndors:MOVE-BEFORE-TAB-ITEM (FRAME frLenderEndors:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME frBasicResults
   NOT-VISIBLE Custom                                                   */
/* SETTINGS FOR BUTTON btlog IN FRAME frBasicResults
   NO-ENABLE                                                            */
ASSIGN 
       fiCPLAmt:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       figrandTotal:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderEndorsPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLenderPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLendersEndorsList:HIDDEN IN FRAME frBasicResults           = TRUE
       fiLendersEndorsList:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiLendersTotalPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerEndorsList:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerEndorsPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnerPolicyPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       fiOwnersTotalPremium:READ-ONLY IN FRAME frBasicResults        = TRUE.

ASSIGN 
       ftext:READ-ONLY IN FRAME frBasicResults        = TRUE.

/* SETTINGS FOR FRAME frLenderEndors
                                                                        */
ASSIGN 
       FRAME frLenderEndors:HEIGHT           = 10.33
       FRAME frLenderEndors:WIDTH            = 85.6.

/* SETTINGS FOR FRAME frOwnerEndors
                                                                        */
ASSIGN 
       FRAME frOwnerEndors:HEIGHT           = 10.33
       FRAME frOwnerEndors:WIDTH            = 85.6.

/* SETTINGS FOR FRAME frScndLenderEndors
                                                                        */
ASSIGN 
       FRAME frScndLenderEndors:HEIGHT           = 10.33
       FRAME frScndLenderEndors:WIDTH            = 85.6.

/* SETTINGS FOR FRAME frScndLoan
                                                                        */
/* SETTINGS FOR BUTTON btScndClear IN FRAME frScndLoan
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME frScndLoanEndors
                                                                        */
/* SETTINGS FOR FRAME frScndLoanResults
                                                                        */
ASSIGN 
       fiScndLenderEndorsPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

ASSIGN 
       fiScndLenderPolicyPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

ASSIGN 
       fiScndLendersTotalPremium:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

ASSIGN 
       fiSecondLendersEndorsList:READ-ONLY IN FRAME frScndLoanResults        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasic
/* Query rebuild information for FRAME frBasic
     _Query            is NOT OPENED
*/  /* FRAME frBasic */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicEndors
/* Query rebuild information for FRAME frBasicEndors
     _Query            is NOT OPENED
*/  /* FRAME frBasicEndors */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frBasicResults
/* Query rebuild information for FRAME frBasicResults
     _Query            is NOT OPENED
*/  /* FRAME frBasicResults */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frScndLoanEndors
/* Query rebuild information for FRAME frScndLoanEndors
     _Query            is NOT OPENED
*/  /* FRAME frScndLoanEndors */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME frScndLoanResults
/* Query rebuild information for FRAME frScndLoanResults
     _Query            is NOT OPENED
*/  /* FRAME frScndLoanResults */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, ust ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
  publish "SetCurrentValue" ("RateStateID", "").
  publish "SetCurrentValue" ("RateVersionNo", "").
  /* This event will close the window and terminate the procedure.  */
  for each openLog:
    if valid-handle(openLog.hInstance) 
     then
      delete object openLog.hInstance.
  end.
  empty temp-table openLog.  
  if valid-handle(hState) 
     then
      hState = ?.
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btCalculate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCalculate C-Win
ON CHOOSE OF btCalculate IN FRAME frBasicResults /* Calculate */
DO:
  run CalculatePremium in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btClear C-Win
ON CHOOSE OF btClear IN FRAME frBasicResults /* Clearx */
DO:
  run ClearAll in this-procedure no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME btendorsement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btendorsement C-Win
ON CHOOSE OF btendorsement IN FRAME frBasic /* v */
DO:
  if not(endSelected)
   then
    do:
      assign
        endSelected = true.
        btEndorsement:load-image("images/s-up2.bmp") in frame frBasic. 
        btEndorsement:tooltip in frame frBasic = "Hide endorsements"
        .
    end.
  else
    do:
      assign
        endSelected = false.
        btEndorsement:load-image("images/s-down2.bmp") in frame frBasic.
        btEndorsement:tooltip in frame frBasic = "Show endorsements"
        .
    end.

  run setPosition  in this-procedure no-error.
  run LoadSecondLoanEndorsements in this-procedure. 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btLEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btLEndors C-Win
ON CHOOSE OF btLEndors IN FRAME frBasicResults /* View Details */
DO:
  run endorsPremiumDetail in this-procedure(input premiumLEndorsDetail,
                                            input {&Lenders},
                                            output table endorsDetail) no-error. 

  create openEndors.
  run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                                           input {&Lenders}) no-error.     
  empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btlog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btlog C-Win
ON CHOOSE OF btlog IN FRAME frBasicResults /* Show Calculations */
DO:
  create openLog.
  run wlogs.w persistent set openLog.hInstance (input table rateLog,
                                                input logscreenparameter) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btOEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOEndors C-Win
ON CHOOSE OF btOEndors IN FRAME frBasicResults /* View Details */
DO:
  run endorsPremiumDetail in this-procedure(input premiumOEndorsDetail,
                                            input {&Owners},
                                            output table endorsDetail) no-error. 
  create openEndors.
  run wEndorsDetail.w persistent set openEndors.hInstance(input table endorsDetail,
                                                          input {&Owners})no-error.  
  empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btRateSheet
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btRateSheet C-Win
ON CHOOSE OF btRateSheet IN FRAME frBasicResults /* PDF */
DO:

  run generatepdf.w (cStateName,
                     replace (pcCalculatedPremium,"=","^") ,
                     pcInputValuesPDF) no-error.      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME btScndClear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btScndClear C-Win
ON CHOOSE OF btScndClear IN FRAME frScndLoan /* SClear */
DO:
  run ClearSecondLoanSection in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME btSecondLoan
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSecondLoan C-Win
ON CHOOSE OF btSecondLoan IN FRAME frBasic /*  > */
DO:    
  if not(multiLoanSelected)
   then
    do:
      assign
        multiLoanSelected                     = true
        btSecondLoan:tooltip in frame frBasic = "Hide"
        .    
      btSecondLoan:load-image("images/s-previous.bmp") in frame frBasic no-error.
      btSecondLoan:load-image-insensitive("images/s-previous-i.bmp") in frame frBasic no-error. 
    end.                                                                        
  else                                                                        
    do:
      assign
        multiLoanSelected                     = false
        btSecondLoan:tooltip in frame frBasic = "Show"
        .    
      btSecondLoan:load-image-insensitive("images/s-next-i.bmp") in frame frBasic no-error.
      btSecondLoan:load-image("images/s-next.bmp") in frame frBasic no-error.      
    end.
    
  run setPosition in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoanResults
&Scoped-define SELF-NAME btSEndors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSEndors C-Win
ON CHOOSE OF btSEndors IN FRAME frScndLoanResults /* View Details */
DO:
  run endorsPremiumDetail in this-procedure (input premiumSEndorsDetail,
                                             input {&SecondLoan},
                                             output table endorsDetail) no-error.  
  create openEndors.
  run wEndorsDetail.w persistent set openEndors.hInstance (input table endorsDetail,
                                                           input {&SecondLoan})no-error.  
  empty temp-table endorsDetail no-error. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME btUrl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btUrl C-Win
ON CHOOSE OF btUrl IN FRAME frBasicResults /* URL */
DO:
  run generateURL in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME cbLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbLoanRateType C-Win
ON VALUE-CHANGED OF cbLoanRateType IN FRAME frBasic /* Rate to Apply */
DO:  
  run setLoanPriorInfoState in this-procedure no-error.

  if endSelected 
   then
    do:
      run reloadEndors in this-procedure (input {&Lenders}).
      cLastLRateType  = cbLoanRateType:Input-value.
    end.         

  run ClearResults in this-procedure no-error.    
  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbPropType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbPropType C-Win
ON VALUE-CHANGED OF cbPropType IN FRAME frBasic /* Property Type */
DO:
  run PropertyTypeValuechange in this-procedure.    
  cLastPropType = cbPropType:input-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRateType C-Win
ON VALUE-CHANGED OF cbRateType IN FRAME frBasic /* Rate to Apply */
DO:
  run setOwnerPriorInfoState in this-procedure no-error. 

  if endSelected 
   then
  do:
    run reloadEndors in this-procedure (input {&Owners}). 
    cLastORateType = cbRateType:Input-value.
  end.
      
  

  run ClearResults in this-procedure no-error.  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbRegion
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbRegion C-Win
ON VALUE-CHANGED OF cbRegion IN FRAME frBasic /* Region */
DO:
  run regionValueChange in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME cbSecondLoanRateType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbSecondLoanRateType C-Win
ON VALUE-CHANGED OF cbSecondLoanRateType IN FRAME frScndLoan /* Rate to Apply */
DO:
  if fiSecondLoanAmt:input-value in frame frScndLoan ne "0"          or 
     cbSecondLoanRateType:input-value in frame frScndLoan ne {&None} or
     secondLoanEndorsList eq true 
   then
    assign
      btSecondLoan:sensitive in frame frBasic   = false
      btScndClear:sensitive in frame frScndLoan = true
      .

  else
    assign 
      btSecondLoan:sensitive in frame frBasic   = true
      btScndClear:sensitive in frame frScndLoan = false
      .

  if endSelected 
   then
    do:
      run reloadEndors in this-procedure (input {&SecondLoan}) no-error.
      cLastSLRateType = cbSecondLoanRateType:input-value.
    end.         

  run ClearResults in this-procedure no-error.  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiBuyerCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiBuyerCPL C-Win
ON VALUE-CHANGED OF fiBuyerCPL IN FRAME frBasicResults /* Buyer */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME fiCoverageAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiCoverageAmt C-Win
ON VALUE-CHANGED OF fiCoverageAmt IN FRAME frBasic /* Coverage Amount */
DO:
  run ClearResults in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiEffectiveDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiEffectiveDate C-Win
ON VALUE-CHANGED OF fiEffectiveDate IN FRAME frBasic /* Effective Date */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiLenderCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLenderCPL C-Win
ON VALUE-CHANGED OF fiLenderCPL IN FRAME frBasicResults /* Lender */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME fiLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanAmt C-Win
ON VALUE-CHANGED OF fiLoanAmt IN FRAME frBasic /* Loan Amount */
DO:
  run ClearResults in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiLoanEffectiveDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanEffectiveDate C-Win
ON VALUE-CHANGED OF fiLoanEffectiveDate IN FRAME frBasic /* Effective Date */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiLoanPriorAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiLoanPriorAmt C-Win
ON VALUE-CHANGED OF fiLoanPriorAmt IN FRAME frBasic /* Prior Policy Amount */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fiPriorAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiPriorAmt C-Win
ON VALUE-CHANGED OF fiPriorAmt IN FRAME frBasic /* Prior Policy Amount */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frScndLoan
&Scoped-define SELF-NAME fiSecondLoanAmt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSecondLoanAmt C-Win
ON VALUE-CHANGED OF fiSecondLoanAmt IN FRAME frScndLoan /* Loan Amount */
DO:
  if fiSecondLoanAmt:input-value in frame frScndLoan ne "0"          or
     cbSecondLoanRateType:input-value in frame frScndLoan ne {&None} or
     secondLoanEndorsList eq true 
   then
    assign
      btSecondLoan  :sensitive in frame frBasic    = false
      btScndClear   :sensitive in frame frScndLoan = true
      .
 
  else
    assign
      btSecondLoan:sensitive in frame frBasic    = true     
      btScndClear :sensitive in frame frScndLoan = false
      .
 
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasicResults
&Scoped-define SELF-NAME fiSellerCPL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fiSellerCPL C-Win
ON VALUE-CHANGED OF fiSellerCPL IN FRAME frBasicResults /* Seller */
DO:
  run ClearResults in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME frBasic
&Scoped-define SELF-NAME tbSimultaneous
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tbSimultaneous C-Win
ON VALUE-CHANGED OF tbSimultaneous IN FRAME frBasic
DO:
  if tbSimultaneous:checked 
   then   
    btSecondLoan  :sensitive in frame frBasic = true.
  else
    do:
      if multiLoanSelected 
       then
        do:
          apply 'choose' to btSecondLoan .
          run ClearSecondLoanSection in this-procedure.
        end.        
      btSecondLoan  :sensitive in frame frBasic = false.
    end.

  apply 'value-changed' to cbPropType.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
 
  publish "GetCurrentValue" ("RateStateID",output cStateID).
  publish "GetCurrentValue" ("RateVersionNo",output CVersion).
  
  
  if cStateID = "" or cStateID = "?" 
  or cVersion = "" or cVersion = "?" or cVersion = "0"  then
  do:
     message "UI can't be initialized. Contact the System Administrator."
     view-as alert-box information buttons ok.
     return.
  end.

  iVersion = integer(cVersion) no-error.

  run ratecalculatordata.p persistent set hState(input cStateID, input iVersion, output std-ch, output std-lo) .  
/* file reads json for ui setup and maintain data*/
run GetRegions in hState (output table region).
            std-lo = false.
for each region:
    std-lo = true.
   chRegionList = chRegionList + "," + region.regionCode. 
end.
if std-lo  then
do:
assign
chRegionList = trim(chRegionList, ",")
cbRegion:sensitive = true.
run GetPropertyType in hState  (input entry(1, chRegionList, ","), output pcPropertyType).
end.
else
run GetPropertyType in hState (input chRegionList, output pcPropertyType).


run checkEndorsement in hState (output lHasEndors).

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames. */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

btEndorsement:load-image("images/s-down2.bmp")                in frame frBasic           no-error.
btSecondLoan :load-image-insensitive("images/s-next-i.bmp")   in frame frBasic           no-error.
btSecondLoan :load-image ("images/s-next.bmp")                in frame frBasic           no-error.
btScndClear  :load-image-insensitive("images/s-erase-i.bmp")  in frame frScndLoan        no-error.
btScndClear  :load-image ("images/s-erase.bmp")               in frame frScndLoan        no-error.
btOEndors    :load-image("images/s-lookup.bmp")               in frame frBasicResults    no-error.
btOEndors    :load-image-insensitive("images/s-lookup-i.bmp") in frame frBasicResults    no-error.
btLEndors    :load-image("images/s-lookup.bmp")               in frame frBasicResults    no-error.
btLEndors    :load-image-insensitive("images/s-lookup-i.bmp") in frame frBasicResults    no-error.
btSEndors    :load-image("images/s-lookup.bmp")               in frame frScndLoanResults no-error.
btSEndors    :load-image-insensitive("images/s-lookup-i.bmp") in frame frScndLoanResults no-error. 
btCalculate  :load-image("images/calc.bmp")                   in frame frBasicResults    no-error.
btCalculate  :load-image-insensitive("images/calc-i.bmp")     in frame frBasicResults    no-error. 
btClear      :load-image("images/erase.bmp")                  in frame frBasicResults    no-error.
btRateSheet  :load-image ("images/pdf.bmp")                   in frame frBasicResults    no-error. 
btRateSheet  :load-image-insensitive("images/pdf-i.bmp")      in frame frBasicResults    no-error.
btLog        :load-image("images/log.bmp")                    in frame frBasicResults    no-error.
btLog        :load-image-insensitive("images/log-i.bmp")      in frame frBasicResults    no-error.
btUrl        :load-image("images/task.bmp")                   in frame frBasicResults    no-error.
btUrl        :load-image-insensitive("images/task-i.bmp")     in frame frBasicResults    no-error.
/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

{lib/winshowscrollbars.i}
if cbRegion:sensitive then 
 assign
  cbRegion:list-items        = chRegionList 
  cbRegion:screen-value = cbRegion:entry(1) no-error.
  
assign 
  cbPropType:list-item-pairs = pcPropertyType 
  cbPropType:screen-value    = {&DefaultPropertyType}.
   

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
{lib/win-main.i}
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.

  cwintitle = replace({&window-name}:title, "rates","").
  run getStateName in hState(output cStateName) no-error.
  c-win:title = cwintitle + cStateName + " Rate Calculator".
  
  btEndorsement:sensitive = lHasEndors.
  
  /* ui resize for initial setup*/
  run ShowScrollBars(frame frBasic:handle, no,no).
  run ShowScrollBars(frame frBasicResults:handle, no, no).
  run ShowScrollBars(frame frBasicEndors:handle, no, no).

  assign
    endorsPos  = frame frBasicEndors:row
    resultsPos = frame frBasicResults:row
    fullheight = frame frBasic:height +  frame frBasicEndors:height  +  frame frBasicResults:height
    fullwidth  = frame frBasic:width  +  frame frScndLoan:width
    cutheight  = frame frBasic:height +  frame frBasicResults:height
    cutwidth   = frame frBasic:width  +  0.5
    .
  
  assign
    cLastRegion = cbRegion:input-value
    cLastPropType = cbPropType:input-value
    .
  
  /* making disable */
  if cStateID = 'NM'
   then
    cbPropType:sensitive = false.
  

  run setPosition in this-procedure.  
  run regionValueChange in this-procedure.  
/*
/*publish to get the current selected version if launched by maintenance screen else use active version*/
  publish "GetCurrentValue" ("COVersion", output versionNo).
           */
  if CVersion ne "" 
   then
    fiVersion:screen-value = CVersion.
  else 
    assign 
      fiVersion:hidden  = true
      fiVersion:visible = false
      .

  {&window-name}:window-state = 3.
  subscribe to "ApplyEntry" anywhere.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ApplyEntry C-Win 
PROCEDURE ApplyEntry :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  apply "entry" to  fiCoverageAmt in frame frBasic.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CalculatePremium C-Win 
PROCEDURE CalculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  pcInputValues:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable errorStatus        as logical   no-undo.      
  define variable errorMsg           as character no-undo.

  
  pcCalculatedPremium = "". 

  do with frame {&frame-name}:                                                
  end.                                                                        
  /*-------------------Client Validations------------------------*/
  run ClientValidation in this-procedure(output errorStatus,
                                         output errorMsg) no-error.  
  if errorStatus 
   then
    do:
      message errorMsg
        view-as alert-box info buttons ok.
      btRateSheet:sensitive in frame frBasicResults = false.
      return no-apply.
    end.
  
  run setRateCalculateParam in this-procedure.
  
  do with frame frBasic:
  end.

  /*-------------------------Input variable-------------------------------*/
  pcInputValues = "region^"                     + cbRegion:input-value +
                  ",propertyType^"              + cbPropType:input-value +
                  ",simultaneous^"              + tbSimultaneous:input-value +                  
                  ",Version^"                   + fiVersion:input-value +
                  ",coverageAmount^"            + fiCoverageAmt:input-value + 
                  ",ratetype^"                  + if cbRateType:input-value  = ? or cbRateType:input-value  = "?" then "" else cbRateType:input-value +                  
                  ",reissueCoverageAmount^"     + fiPriorAmt:input-value +
                  ",priorEffectiveDate^"        + (if fiEffectiveDate:input-value = ? then "" else fiEffectiveDate:input-value) +  
                  ",ownerEndors^"               + ownerEndorsPR + 
                  ",loanCoverageAmount^"        + fiLoanAmt:input-value +
                  ",loanRateType^"              + if cbLoanRateType:input-value = ? or cbLoanRateType:input-value = "?" then "" else cbLoanRateType:input-value +
                  ",loanReissueCoverageAmount^" + fiLoanPriorAmt:input-value +
                  ",loanPriorEffectiveDate^"    + (if fiLoanEffectiveDate:input-value = ? then "" else fiLoanEffectiveDate:input-value) +  
                  ",loanEndors^"                + loanEndorsPR +                                                      
                  ",lenderCPL^"                 + fiLenderCPL:input-value +
                  ",buyerCPL^"                  + fiBuyerCPL:input-value +
                  ",sellerCPL^"                 + fiSellerCPL:input-value         
                  .

  pcInputValuesPDF = "region^"                     + cbRegion:input-value +
                     ",propertyType^"              + cbPropType:input-value +
                     ",simultaneous^"              + tbSimultaneous:input-value +
                     ",Version^"                   + fiVersion:input-value +
                     ",coverageAmount^"            + fiCoverageAmt:input-value + 
                     ",ratetype^"                  + if cbRateType:input-value  = ? or cbRateType:input-value  = "?" then "" else entry((lookup(cbRateType:input-value, cbRateType:list-item-pairs,",") - 1),cbRateType:list-item-pairs, ",") +                     
                     ",reissueCoverageAmount^"     + fiPriorAmt:input-value +
                     ",PriorEffectiveDate^"        + (if fiEffectiveDate:input-value = ? then "" else fiEffectiveDate:input-value) +                       
                     ",ownerEndors^"               + ownerEndorsPR + 
                     ",loanCoverageAmount^"        + fiLoanAmt:input-value +
                     ",loanRateType^"              + if cbLoanRateType:input-value  = ? or cbLoanRateType:input-value  = "?" then "" else entry((lookup(cbLoanRateType:input-value, cbLoanRateType:list-item-pairs,",") - 1),cbLoanRateType:list-item-pairs, ",") +
                     ",loanReissueCoverageAmount^" + fiLoanPriorAmt:input-value +
                     ",loanPriorEffectiveDate^"    + (if fiLoanEffectiveDate:input-value = ? then "" else fiLoanEffectiveDate:input-value) +                       
                     ",loanEndors^"                + loanEndorsPR +                                           
                     ",lenderCPL^"                 + fiLenderCPL:input-value +
                     ",buyerCPL^"                  + fiBuyerCPL:input-value +
                     ",sellerCPL^"                 + fiSellerCPL:input-value +
                     ",ratetypecode^"              + if cbRateType:input-value = ? or cbRateType:input-value  = "?" then "" else cbRateType:input-value +
                     ",loanRateTypecode^"          + if cbLoanRateType:input-value  = ? or cbLoanRateType:input-value  = "?" then "" else cbLoanRateType:input-value 
                     .

  if tbSimultaneous:checked 
   then
    do with frame frScndLoan:
      pcInputValues = pcInputValues + 
                      ",secondloanRateType^"       + if cbSecondLoanRateType:input-value  = ? or cbSecondLoanRateType:input-value  = "?" then "" else cbSecondLoanRateType:input-value +
                      ",secondloanCoverageAmount^" + fiSecondLoanAmt:input-value +
                      ",secondLoanEndors^"         + secondLoanEndorsPR
                      .
      pcInputValuesPDF = pcInputValuesPDF + 
                         ",secondloanRateType^"        + if cbSecondLoanRateType:input-value  = ? or cbSecondLoanRateType:input-value  = "?" then "" else entry((lookup(cbSecondLoanRateType:input-value, cbSecondLoanRateType:list-item-pairs,",") - 1),cbSecondLoanRateType:list-item-pairs, ",") +
                         ",secondloanCoverageAmount^"  + fiSecondLoanAmt:input-value +
                         ",secondLoanEndors^"          + secondLoanEndorsPR + 
                         ",secondloanRateTypecode^"    + if cbsecondLoanRateType:input-value  = ? or cbsecondLoanRateType:input-value  = "?" then "" else cbsecondLoanRateType:input-value
                         .
    end. 
                            
  run CalculatePremium in hState(input pcInputValues,
                                output pcCalculatedPremium,
                                output table rateLog). 
                  

  /*--------------------Extracting output parameter and set premium on screen--------------------------------------*/ 
  run ExtractOutputParameters.

  logscreenparameter =  pcInputValues + ",cardSetId^"     + cardSetId
                                      + ",StateID^"       + cStateID
                                      + ",manualEffectiveDate^" + (if effectiveDate eq "" then "" else string(date(effectiveDate), "99/99/99")).  

    
  do with frame frBasicResults:
  end.                                     
                                      
  if not(lSuccess) or 
     serverErrMsg ne "serverErrMsg" 
   then
    do:
      message serverErrMsg
        view-as alert-box info buttons ok.
      btRateSheet:sensitive in frame frBasicResults = false.
      btLog:sensitive = false.
      return no-apply.
    end.
   else if ratesCode >= '3000' and ratesMsg <> "" 
    then
     do:
       message ratesMsg
         view-as alert-box info buttons ok.
     end.

  assign
    btLog:sensitive = true.
    btRateSheet:sensitive in frame frBasicResults = true
    .
  
/*--------setup endorsement premium detail button-----------*/  
  if fiOwnerEndorsList:input-value ne "" 
   then
    btOEndors:sensitive = true.
  if fiLendersEndorsList:input-value ne ""
   then
    btLEndors:sensitive = true.
  if fiSecondLendersEndorsList:input-value in frame frScndLoanResults ne "" 
   then
    btSEndors:sensitive = true.

  assign
    fiOwnerEndorsPremium     :screen-value = premiumOEndors
    fiOwnerPolicyPremium     :screen-value = premiumOwner
    fiOwnersTotalPremium     :screen-value = string(decimal(premiumOEndors)   + decimal(premiumOwner))
    fiLenderEndorsPremium    :screen-value = premiumLEndors
    fiLenderPolicyPremium    :screen-value = premiumLoan
    fiLendersTotalPremium    :screen-value = string(decimal(premiumLEndors)   + decimal(premiumLoan))
    fiScndLenderEndorsPremium:screen-value = premiumSEndors
    fiScndLenderPolicyPremium:screen-value = premiumScnd         
    fiScndLendersTotalPremium:screen-value = string(decimal(premiumScnd)      + decimal(premiumSEndors))
    fiCPLAmt                 :screen-value = string(decimal(premiumLenderCPL) + decimal(premiumBuyerCPL) + decimal(premiumSellerCPL))
    fiGrandTotal:screen-value              = string(decimal(premiumOEndors)   + decimal(premiumOwner)
                                                  + decimal(premiumLEndors)   + decimal(premiumLoan)
                                                  + decimal(premiumScnd)      + decimal(premiumSEndors)
                                                  + decimal(premiumLenderCPL) + decimal(premiumBuyerCPL)
                                                  + decimal(premiumSellerCPL))
                                       .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkValidation C-Win 
PROCEDURE checkValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter caseType  as character   no-undo. 
  define output parameter opiStatus as character   no-undo. 
  define output parameter errorMsg  as character   no-undo. 
  
  do with frame frBasic:
  end.
 
  case caseType:
    when {&Owners} 
     then
      do:
      /*---------check if section is blank---------------*/
        if fiCoverageAmt:input-value = 0    and
           cbRateType:input-value = {&none} and
           fiPriorAmt:input-value = 0       and
           fiEffectiveDate:input-value = ?  and
           not can-find(first tthowr where tthowr.hChecked = true)
         then
          do:
            opiStatus = {&Blank}.
            return.
          end.        
       /*---------check if section have valid input---------------*/        
        else if (cbRateType:input-value = {&NaUI}     or
                 (fiCoverageAmt:input-value <> 0      and
                  cbRateType:input-value <> {&none}   and
                  (fiPriorAmt:sensitive = false or 
                   fiPriorAmt:input-value <> 0)       and
                  (fiEffectiveDate:sensitive = false or 
                   fiEffectiveDate:input-value <> ?)))
         then
          do:
            opiStatus = {&ValidInput}.
            return.
          end.
      /*---------check if section is invalid input---------------*/
        else if fiCoverageAmt:input-value = 0
         then
          do:
            errorMsg =  "Please enter coverage amount.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      
        else if cbRateType:input-value = {&none}         
         then
          do:
            errorMsg =  "Please select rate type.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      
        else if (fiPriorAmt:sensitive = true and 
                 fiPriorAmt:input-value = 0)
         then
          do:
            errorMsg =  "Please enter prior policy amount for owner.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      
        else if (fiEffectiveDate:sensitive = true and 
                 fiEffectiveDate:input-value = ?)
         then
          do:
            errorMsg =  "Please enter prior policy effective date for owner.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      end.
   
    when {&Lenders} 
     then
      do:
      /*---------check if section is blank---------------*/
        if fiLoanAmt:input-value = 0            and
           cbLoanRateType:input-value = {&none} and
           fiLoanPriorAmt:input-value = 0       and
           fiLoanEffectiveDate:input-value = ?  and
           not can-find(first tthLdr where tthLdr.hChecked = true)
         then
          do:
            opiStatus = {&Blank}.
            return.
          end.        
      /*---------check if section have valid input---------------*/ 
        else if (cbLoanRateType:input-value = {&NaUI} or
                 (fiLoanAmt:input-value <> 0                and
                  cbLoanRateType:input-value <> {&none}     and
                  (fiLoanPriorAmt:sensitive = false or      
                   fiLoanPriorAmt:input-value <> 0)         and
                  (fiLoanEffectiveDate:sensitive = false or 
                   fiLoanEffectiveDate:input-value <> ?)))
         then
          do:
            opiStatus = {&ValidInput}.
            return.
          end.
      /*---------check if section is invalid input---------------*/
        else if fiLoanAmt:input-value = 0
         then
          do:
            errorMsg =  "Please enter loan amount.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      
        else if cbLoanRateType:input-value = {&none}         
         then
          do:
            errorMsg =  "Please select loan rate type.".
            opiStatus = {&InvalidInput}.
            return.
          end. 

        else if (fiLoanPriorAmt:sensitive = true and 
                 fiLoanPriorAmt:input-value = 0)
         then
          do:
            if cStateID = "NV" and cbLoanRateType:input-value = "B"
             then
              do:
                errorMsg =  "Please enter number of parcels.".
                opiStatus = {&InvalidInput}.
                return.
              end.
            else
             do:
               errorMsg =  "Please enter prior policy amount for loan.".
               opiStatus = {&InvalidInput}.
               return.
             end.
          end.
      
        else if (fiLoanEffectiveDate:sensitive = true and 
                 fiLoanEffectiveDate:input-value = ?)
         then
          do:
            errorMsg =  "Please enter prior policy effective date for loan.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      end.
   
    when {&SecondLoan} 
     then
      do:
        do with frame frScndLoan:
        end.
       /*---------check if section is blank---------------*/
        if fiSecondLoanAmt:input-value = 0            and
           cbSecondLoanRateType:input-value = {&none} and
           not can-find(first tthScndLoan where tthScndLoan.hChecked = true)
         then
          do:
            opiStatus = {&Blank}.
            return.
          end.        
      /*---------check if section have valid input---------------*/ 
        else if (fiSecondLoanAmt:input-value = {&NaUI} or
                 (fiSecondLoanAmt:input-value <> 0  and
                 cbSecondLoanRateType:input-value <> {&none}))
         then
          do:
            opiStatus = {&ValidInput}.
            return.
          end.
      /*---------check if section is invalid input---------------*/
        else if fiSecondLoanAmt:input-value = 0
         then
          do:
            errorMsg =  "Please enter second loan amount.".
            opiStatus = {&InvalidInput}.
            return.
          end.
      
        else if cbSecondLoanRateType:input-value = {&none}         
         then
          do:
            errorMsg =  "Please select second loan rate type.".
            opiStatus = {&InvalidInput}.
            return.
          end.      
      end.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearAll C-Win 
PROCEDURE ClearAll :
/*------------------------------------------------------------------------------
  Purpose: reset screen for selected ratetype    
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasic:
  end.

  assign
    fiCoverageAmt       :screen-value                      = ""
    cbRateType          :screen-value                      = {&None}
    fiPriorAmt          :screen-value                      = ""
    fiEffectiveDate     :screen-value                      = ""
    fiLoanPriorAmt      :screen-value                      = ""
    fiLoanEffectiveDate :screen-value                      = ""
    fiLoanAmt           :screen-value                      = ""    
    cbLoanRateType      :screen-value                      = {&None}
    fiSecondLoanAmt     :screen-value in frame frScndLoan  = "0"
    cbSecondLoanRateType:screen-value in frame frScndLoan  = {&None}
    fiPriorAmt          :sensitive                         = false
    fiEffectiveDate     :sensitive                         = false
    fiLoanPriorAmt      :sensitive                         = false
    fiLoanEffectiveDate :sensitive                         = false
    tbSimultaneous      :checked                           = false
    btCalculate         :sensitive in frame frBasicResults = true    
    .

  apply 'value-changed' to tbSimultaneous.

  do with frame frBasicResults:
  end.

  assign
    fiLenderCPL:screen-value = ""
    fiBuyerCPL :screen-value = ""
    fiSellerCPL:screen-value = ""
    fiLenderCPL:sensitive    = true 
    fiBuyerCPL :sensitive    = true 
    fiSellerCPL:sensitive    = true 
    .

  run ClearResults in this-procedure no-error.

  run RefreshEndors in this-procedure ("") no-error. 

  pcInputValues = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearResults C-Win 
PROCEDURE ClearResults :
/*------------------------------------------------------------------------------
  Purpose: for any change in user input reset results 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasicResults:
  end.
  assign
    fiOwnerEndorsPremium :screen-value = ""
    fiOwnerPolicyPremium :screen-value = ""
    fiOwnersTotalPremium :screen-value = ""
    fiLenderEndorsPremium:screen-value = ""
    fiLenderPolicyPremium:screen-value = ""
    fiLendersTotalPremium:screen-value = ""
    fiGrandTotal         :screen-value = ""
    fiCPLAmt             :screen-value = ""
    btRateSheet          :sensitive    = false
    btlog                :sensitive    = false
    btOEndors            :sensitive    = false
    btLEndors            :sensitive    = false
    .
  
  do with frame frScndLoanResults:
  end.
  assign
    fiScndLenderEndorsPremium:screen-value = ""
    fiScndLenderPolicyPremium:screen-value = ""
    fiScndLendersTotalPremium:screen-value = ""  
    btSEndors                :sensitive    = false
    .
 
  for each openEndors:      
     if valid-handle(openEndors.hInstance) then
       delete object openEndors.hInstance no-error.
  end.
  empty temp-table openEndors no-error. 
 
  pcCalculatedPremium = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClearSecondLoanSection C-Win 
PROCEDURE ClearSecondLoanSection :
/*------------------------------------------------------------------------------
  Purpose: clear out only second loan section  
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frScndLoan:
  end.

  assign 
    fiSecondLoanAmt:screen-value      = "0"
    cbSecondLoanRateType:screen-value = {&None}
    .

  run RefreshEndors in this-procedure ({&SecondLoan}) no-error.
    
  assign
    btSecondLoan:sensitive in frame frBasic    = true
    btScndClear:sensitive  in frame frScndLoan = false
    .
  
  run ClearResults  in this-procedure no-error. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ClientValidation C-Win 
PROCEDURE ClientValidation :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
  
------------------------------------------------------------------------------*/              
  define output parameter errorStatus as logical   no-undo. 
  define output parameter errorMsg    as character no-undo. 
  
  define variable iOStatus     as character no-undo.
  define variable iLStatus     as character no-undo.
  define variable iSLStatus    as character no-undo.
  define variable chOErrorMsg  as character no-undo.
  define variable chLErrorMsg  as character no-undo.
  define variable chSLErrorMsg as character no-undo.
 
  do with frame frBasic:
  end.
 /*-------------validation for owner section---------------*/            
  run checkValidation(input {&Owners},
                      output iOStatus,                         
                      output chOErrorMsg).

  if iOStatus = {&InvalidInput}
   then 
    do:
      assign
        errorStatus = true
        errormsg = chOErrormsg
        .
      return.
    end.
  /*-------------validation for loan section---------------*/   
  run checkValidation(input {&Lenders},
                      output iLStatus,                         
                      output chLErrorMsg).

  if iLStatus = {&InvalidInput} 
   then
    do:
      assign
        errorStatus = true
        errormsg = chLErrormsg
        .
      return.
    end.
  /*-------------validation for second loan section---------------*/   
  run checkValidation(input {&SecondLoan},
                      output iSLStatus,
                      output chSLErrorMsg).  
   
  if iSLStatus = {&InvalidInput} 
   then
    do:
      assign
        errorStatus = true
        errormsg = chSLErrormsg
        .
      return.
    end. 

/*------------------case of valid input with simo check------------------------*/

  if tbSimultaneous:checked 
   then
    do: 
      if (iOStatus =  {&Blank} and iLStatus  = {&Blank}) or
         (iLStatus =  {&Blank} and iSLStatus = {&Blank}) or
         (iSLStatus = {&Blank} and iOStatus  = {&Blank})
       then
        do:
          assign
            errorStatus = true
            errormsg = "At least two amounts must be entered for Simultaneous."
            .
          return.
        end.
     end.
   else
    do: 
      if (iOStatus =  {&ValidInput} and iLStatus  = {&ValidInput}) or
         (iLStatus =  {&ValidInput} and iSLStatus = {&ValidInput}) or
         (iSLStatus = {&ValidInput} and iOStatus  = {&ValidInput})
       then
        do:
          assign
            errorStatus = true
            errormsg = "Simultaneous must be selected for more than one amount."
            .
          return.
        end.
     end.

/*----------------case when user hit calculat for all 3 blank sections---------------*/
  if iOStatus = {&Blank} and 
     iLStatus = {&Blank} 
   then
    do:
      if fiCoverageAmt:sensitive 
       then
        do:
          assign
            errorStatus = true
            errormsg = "Please enter the coverage amount."
            .
          return.
        end.          
      else if fiLoanAmt:sensitive 
       then
        do:
          assign
            errorStatus = true
            errormsg = "Please enter the loan amount."
            .
          return.
        end. 
    end.


/*
 /*------------------vaidation for non simo case if amount is in 2 sections---------------*/
  if not tbSimultaneous:checked 
   then
    do:        
 
      if iOStatus = {&Blank} and 
         iLStatus = {&Blank} 
       then
        do:
          if fiCoverageAmt:sensitive 
           then
            do:
              assign
                errorStatus = true
                errormsg = "Please enter the coverage amount."
                .
              return.
            end.          
          else if fiLoanAmt:sensitive 
           then
            do:
              assign
                errorStatus = true
                errormsg = "Please enter the loan amount."
                .
              return.
            end.          
        end.
 
      if (iOStatus = {&Blank} and iLStatus = {&ValidInput}) or
         (iOStatus = {&ValidInput} and iLStatus = {&Blank})
       then
        do:
          errorStatus = false.         
          return.
        end.
 
      if (iOStatus = {&ValidInput} and iLStatus = {&ValidInput}) 
       then
        do:
          assign
            errorStatus = true
            errormsg = "Simultaneous must be selected for more than one amount."
            .
          return.
        end.
    end.
 
  else
 /*-------------validation for simo case section---------------*/   
    do:
      if (iOStatus =  {&Blank} and
          iLStatus =  {&Blank} and
          iSLStatus = {&Blank})
       then
        do:
          assign
            errorStatus = true
            errormsg = "Please enter the coverage amount."
            .
          return.          
        end.
      if (iOStatus =  {&Blank} and iLStatus =  {&Blank}) or
         (iLStatus =  {&Blank} and iSLStatus = {&Blank}) or
         (iSLStatus = {&Blank} and iOStatus = {&Blank})
       then
        do:
          assign
            errorStatus = true
            errormsg = "At least two amounts must be entered for Simultaneous."
            .
          return.
        end.
      else
        do:
          errorStatus = false.         
          return.
        end.
    end. 
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CoverageAmountValueChange C-Win 
PROCEDURE CoverageAmountValueChange :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DoCheckbox C-Win 
PROCEDURE DoCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     update the list on ui for selected checkbox of owner loan and second
           loan 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/                        
  find first tthOwr where tthowr.hToggle = self no-error.
  if available tthowr 
   then
    do:
      if tthowr.hChecked = false 
       then
        do:
          assign 
            tthowr.hChecked        = true
            tthowr.hEditor:fgcolor = 9
            .                             
          find first ownerEndorsement where ownerEndorsement.endorsementCode = tthowr.hseq no-error.          
          if available ownerEndorsement 
           then
            fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:input-value in frame frBasicResults + "," + ownerEndorsement.endorsementCode , ",").
        end.
      else
        do:
          assign 
            tthowr.hChecked        = false
            tthowr.hEditor:fgcolor = 0
            .
          find first ownerEndorsement where ownerEndorsement.endorsementCode = tthowr.hseq no-error.
          if available ownerEndorsement 
           then
            fiOwnerEndorsList:screen-value in frame frBasicResults = RemoveItemFromList(fiOwnerEndorsList:input-value in frame frBasicResults , string(ownerEndorsement.endorsementCode) , ",").
        end.
    end.
  
  find first tthLdr where tthLdr.hToggle = self no-error.
  if available tthLdr 
   then
    do:
      if tthLdr.hChecked = false 
       then
        do:
          assign 
            tthLdr.hChecked        = true
            tthLdr.hEditor:fgcolor = 9
            .
          find first loanEndorsement where loanEndorsement.endorsementCode = tthLdr.hseq no-error.
          if available loanEndorsement 
           then
            fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:input-value in frame frBasicResults + "," + loanEndorsement.endorsementCode , ",").
        end.
      else
        do:
          assign 
            tthLdr.hChecked        = false
            tthLdr.hEditor:fgcolor = 0
            .
          find first loanEndorsement where loanEndorsement.endorsementCode = tthLdr.hseq no-error.
          if available loanEndorsement 
           then
            fiLendersEndorsList:screen-value in frame frBasicResults = RemoveItemFromList(fiLendersEndorsList:input-value in frame frBasicResults , string(loanEndorsement.endorsementCode) , ",").
        end.
    end.
    
  find first tthScndLoan where tthScndLoan.hToggle = self no-error.
  if available tthScndLoan 
   then
    do:
      if tthScndLoan.hChecked = false 
       then
        do:
          assign 
            tthScndLoan.hChecked        = true
            tthScndLoan.hEditor:fgcolor = 9
            .
          find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = tthScndLoan.hseq no-error.
          if available secondLoanEndorsement 
           then
            fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = trim(fiSecondLendersEndorsList:input-value in frame frScndLoanResults + "," + secondLoanEndorsement.endorsementCode , ",").
        end.
      else
        do:
          assign 
            tthScndLoan.hChecked        = FALSE
            tthScndLoan.hEditor:fgcolor = 0
            .
          find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = tthScndLoan.hseq no-error.
          if available secondLoanEndorsement 
           then
            fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = RemoveItemFromList(fiSecondLendersEndorsList:input-value in frame frScndLoanResults , string(secondLoanEndorsement.endorsementCode) , ",").
        end.
/*       if fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = "" */
/*        then                                                                     */
/*         secondLoanEndorsList = false.                                           */
/*       else                                                                      */
/*         secondLoanEndorsList = true.                                            */
    
      if fiSecondLoanAmt:input-value in frame frScndLoan ne "0"                 or 
         cbSecondLoanRateType:input-value in frame frScndLoan ne {&None}        or 
         fiSecondLendersEndorsList:input-value in frame frScndLoanResults ne ""
       then
         assign
           btSecondLoan:sensitive in frame frBasic   = false
           btScndClear:sensitive in frame frScndLoan = true
           .
      else
        assign
          btSecondLoan:sensitive in frame frBasic   = true
          btScndClear:sensitive in frame frScndLoan = false
          .
    end.

  run ClearResults in this-procedure no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DoEditor C-Win 
PROCEDURE DoEditor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter cbSeq as handle no-undo.
  
  if not(cbSeq:checked)
   then
    do:
      apply 'value-changed' to cbSeq.
      cbSeq:checked = true.
      self:fgcolor  = 9.
      apply 'entry' to cbSeq.
    end.
  
  else
    do:
      apply 'value-changed' to cbSeq.
      cbSeq:checked = false.
      self:fgcolor  = 0.
      apply 'entry' to cbSeq.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableDisableCaseType C-Win 
PROCEDURE enableDisableCaseType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipchCase  as character no-undo.
  define input parameter iploState as logical   no-undo.
  
  do with frame frBasic:
  end. 
  case ipchCase:
    when "Owner" 
     then
      do:
        assign
          fiCoverageAmt      :sensitive = iploState
          cbRateType         :sensitive = iploState
/*           fiPriorAmt         :sensitive = iploState */
/*           fiEffectiveDate    :sensitive = iploState */
          frame frOwnerEndors:sensitive = iploState
          .
        if not iploState 
         then
          do:
            assign  
              fiCoverageAmt  :screen-value = ""
              fiPriorAmt     :screen-value = ""
              fiEffectiveDate:screen-value = ""
              .
            run refreshEndors in this-procedure({&Owners}).
          end. 
          apply 'value-changed' to cbRateType.
      end.    
  
    when "Loan"
     then
      do:
        assign
          fiLoanAmt           :sensitive = iploState
          cbLoanRateType      :sensitive = iploState
/*           fiLoanPriorAmt      :sensitive = iploState */
/*           fiLoanEffectiveDate :sensitive = iploState */
          frame frLenderEndors:sensitive = iploState
          .
        if not iploState 
         then
          do:
            assign
              fiLoanAmt          :screen-value = ""
              fiLoanPriorAmt     :screen-value = ""
              fiLoanEffectiveDate:screen-value = ""
              .
            run refreshEndors in this-procedure({&Lenders}). 
            apply 'value-changed' to cbLoanRateType.
          end.          
      end.
  
    when "SecondLoan"
     then
      do:
        assign
          fiSecondLoanAmt         :sensitive in frame frScndLoan = iploState
          cbSecondLoanRateType    :sensitive                     = iploState        
          frame frScndLenderEndors:sensitive                     = iploState
          .
        if not iploState 
         then
          do:
            fiSecondLoanAmt:screen-value in frame frScndLoan = "".
            run refreshEndors in this-procedure({&SecondLoan}). 
          end.                                                
      end. 
    otherwise.
  end case.    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enableFooter C-Win 
PROCEDURE enableFooter :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define input parameter iplEnable as logical no-undo.
  
  /* local variables */
  define variable lEnable as logical no-undo initial false.
 
  do with frame frBasicResults:    
  end.

  run CommitmentEnabled in hState( output lEnable ).    
  if lEnable
   then
    do:
      assign
          ftext:screen-value = 'Commitment'
          ftext:column = 3.80
          ftext:width = 14
          fiCPLAmt:label = 'Total Amount'
          fiCPLAmt:x  = fiOwnersTotalPremium:x  /* to get the x-axis */
          fiCPLAmt:side-label-handle:x = fiOwnersTotalPremium:x - fiCPLAmt:side-label-handle:width-pixels 
          /* label to equal to owners total amount colon */
          fiBuyerCPL:hidden = true
          fiSellerCPL:hidden = true. 
          fiSellerCPL:side-label-handle:visible = false.
          fiLenderCPL:side-label-handle:visible = false.
          fiBuyerCPL:side-label-handle:visible  = false.
    end.  
    
  assign
    btCalculate:sensitive = iplEnable                                                     
    fiLenderCPL:sensitive = iplEnable
    fiBuyerCPL:sensitive  = iplEnable
    fiSellerCPL:sensitive = iplEnable
    .
  if not iplEnable 
   then
    assign
      fiLenderCPL:screen-value = ""
      fiBuyerCPL:screen-value  = ""
      fiSellerCPL:screen-value = ""
      .
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW FRAME frScndLenderEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLenderEndors}
  VIEW FRAME frOwnerEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frOwnerEndors}
  VIEW FRAME frLenderEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frLenderEndors}
  DISPLAY cbRegion cbPropType tbSimultaneous fiVersion fiCoverageAmt cbRateType 
          fiLoanAmt cbLoanRateType 
      WITH FRAME frBasic IN WINDOW C-Win.
  ENABLE cbPropType tbSimultaneous fiCoverageAmt cbRateType fiLoanAmt 
         cbLoanRateType btendorsement RECT-56 RECT-60 
      WITH FRAME frBasic IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasic}
  DISPLAY fiSecondLoanAmt cbSecondLoanRateType 
      WITH FRAME frScndLoan IN WINDOW C-Win.
  ENABLE RECT-61 fiSecondLoanAmt cbSecondLoanRateType 
      WITH FRAME frScndLoan IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoan}
  ENABLE RECT-67 RECT-68 
      WITH FRAME frBasicEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicEndors}
  ENABLE RECT-69 
      WITH FRAME frScndLoanEndors IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoanEndors}
  DISPLAY ftext fiOwnerEndorsList fiLenderCPL fiBuyerCPL fiSellerCPL 
          fiLendersEndorsList fiOwnerEndorsPremium fiLenderEndorsPremium 
          fiOwnerPolicyPremium fiLenderPolicyPremium fiOwnersTotalPremium 
          fiLendersTotalPremium figrandTotal fiCPLAmt 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  ENABLE ftext btCalculate btClear btRateSheet btUrl btOEndors btLEndors 
         fiOwnerEndorsList fiLenderCPL fiBuyerCPL fiSellerCPL 
         fiLendersEndorsList fiOwnerEndorsPremium fiLenderEndorsPremium 
         fiOwnerPolicyPremium fiLenderPolicyPremium fiOwnersTotalPremium 
         fiLendersTotalPremium figrandTotal fiCPLAmt RECT-58 RECT-62 RECT-70 
      WITH FRAME frBasicResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frBasicResults}
  DISPLAY fiSecondLendersEndorsList fiScndLenderEndorsPremium 
          fiScndLenderPolicyPremium fiScndLendersTotalPremium 
      WITH FRAME frScndLoanResults IN WINDOW C-Win.
  ENABLE RECT-63 fiSecondLendersEndorsList fiScndLenderEndorsPremium btSEndors 
         fiScndLenderPolicyPremium fiScndLendersTotalPremium 
      WITH FRAME frScndLoanResults IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-frScndLoanResults}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE endorsPremiumDetail C-Win 
PROCEDURE endorsPremiumDetail :
/*------------------------------------------------------------------------------
  Purpose: create endorsement temp table for detailed premium to show on UI     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter           ipPremiumEndorsDetail as character.
  define input  parameter           ipEndorsType          as character.
  define output parameter table for endorsDetail.
  
  define variable iCount as integer  no-undo.
 
  if ipEndorsType = {&Owners} or ipEndorsType = "" 
   then
    do iCount = 1 to num-entries(ipPremiumEndorsDetail,"|"):    
      find first ownerEndorsement where ownerEndorsement.endorsementCode = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-") and 
                 (ownerEndorsement.endorsementType = ipEndorsType or ownerEndorsement.endorsementType = "") no-error.
      if available ownerEndorsement 
       then
        do:        
          create endorsDetail.
          assign
            endorsDetail.endorsCode    = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-")
            endorsDetail.endorsPremium = decimal(entry(2,entry(iCount,ipPremiumEndorsDetail,"|"),"-"))
            endorsDetail.endorsDesc    = ownerEndorsement.description
            .        
        end.    
    end.
  else if ipEndorsType = {&Lenders} or ipEndorsType = "" 
   then
    do iCount = 1 to num-entries(ipPremiumEndorsDetail,"|"):    
      find first loanEndorsement where loanEndorsement.endorsementCode = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-") and 
                 (loanEndorsement.endorsementType = ipEndorsType or loanEndorsement.endorsementType = "") no-error.
      if available loanEndorsement 
       then
        do:        
          create endorsDetail.
          assign
            endorsDetail.endorsCode    = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-")
            endorsDetail.endorsPremium = decimal(entry(2,entry(iCount,ipPremiumEndorsDetail,"|"),"-"))
            endorsDetail.endorsDesc    = loanEndorsement.description
            .
        end.    
    end.
 
 else if ipEndorsType = {&SecondLoan} or ipEndorsType = "" then
do iCount = 1 to num-entries(ipPremiumEndorsDetail,"|"):
    find first secondloanEndorsement where secondloanEndorsement.endorsementCode = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-") and (secondloanEndorsement.endorsementType = {&Lenders} or secondloanEndorsement.endorsementType = "") no-error.
    if available secondloanEndorsement then
    do:
        create endorsDetail.
        assign
          endorsDetail.endorsCode    = entry(1,entry(iCount,ipPremiumEndorsDetail,"|"),"-")
          endorsDetail.endorsPremium = decimal(entry(2,entry(iCount,ipPremiumEndorsDetail,"|"),"-"))
          endorsDetail.endorsDesc    = secondloanEndorsement.description.
    end.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ExtractOutputParameters C-Win 
PROCEDURE ExtractOutputParameters :
/*------------------------------------------------------------------------------
  Purpose: parse output parameter of calculated premium 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable iCount        as integer no-undo
  .  
  define variable iCountOEndors as integer no-undo.
  define variable iCountLEndors as integer no-undo.
  define variable iCountSEndors as integer no-undo.
  
  define variable deOEndors     as decimal no-undo.
  define variable deLEndors     as decimal no-undo.
  define variable deSEndors     as decimal no-undo.
  assign
    premiumOwner         = "premiumOwner"
    premiumOEndors       = "premiumOEndors"
    premiumOEndorsdetail = "premiumOEndorsdetail"
    premiumLoan          = "premiumLoan"
    premiumLEndors       = "premiumLEndors"
    premiumLEndorsdetail = "premiumLEndorsdetail"
    premiumScnd          = "premiumScnd"
    premiumSEndors       = "premiumSEndors"
    premiumSEndorsdetail = "premiumSEndorsdetail"
    success              = "success"
    serverErrMsg         = "serverErrMsg"
    premiumLenderCPL     = "premiumLenderCPL"
    premiumBuyerCPL      = "premiumBuyerCPL"
    premiumSellerCPL     = "premiumSellerCPL"
    cardSetId            = "cardSetId"
    effectiveDate        = "manualEffectiveDate"
    ratesCode            = "RatesCode"
    ratesMsg             = "RatesMsg".
  
  do iCount = 1 to num-entries(pcCalculatedPremium,","):
  
    case entry(1,entry(iCount,pcCalculatedPremium,","),"="):
    
     when premiumOwner                                                                                                                                                               
      then                                                                                                                                                                                                                              
       premiumOwner         = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                     
                                                                                                                                   
     when premiumOEndors                                                                                                           
      then                                                                                                                         
       premiumOEndors       = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                     
                                                                                                                                   
     when premiumOEndorsDetail                                                                                                    
      then                                                                                                                         
       premiumOEndorsDetail = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                     
                                                                                                                                   
     when premiumLoan                                                                                                             
      then                                                                                                                                                                                                                              
       premiumLoan          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                
                                                                                                                              
     when premiumLEndors                                                                                                  
      then                                                                                                                    
       premiumLEndors       = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                
                                                                                                                              
     when premiumLEndorsDetail                                                                                           
      then                                                                                                                    
       premiumLEndorsDetail = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                
                                                                                                                              
     when premiumScnd                                                                                                  
      then                                                                                                                                                                                                                              
       premiumScnd          = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                                 
                                                                                                                                               
     when premiumSEndors                                                                                                             
      then                                                                                                                                     
       premiumSEndors       = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                                 
                                                                                                                                               
     when premiumSEndorsDetail                                                                                                           
      then                                                                                                                      
       premiumSEndorsDetail = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                  
                                                                                                                                
     when success                                                                                                            
      then                                                                                                                                                                                                                              
       success              = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                              
                                                                                                                                            
     when serverErrMsg                                                                                                                  
      then                                                                                                                                  
       serverErrMsg         = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                              
                                                                                                                                         
     when premiumLenderCPL                                                                                                                   
      then                                                                                                                               
       premiumLenderCPL     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                          
                                                                                                                                        
     when premiumBuyerCPL                                                                                                                       
      then                                                                                                                              
       premiumBuyerCPL      = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                          
                                                                                                                                        
     when premiumSellerCPL                                                                                                                  
      then                                                                                                                                                                                                                              
       premiumSellerCPL     = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                           
                                                                                                                                         
     when cardSetId                                                                                                                         
      then                                                                                                                               
       cardSetId            = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.                                           
                                                                                                                                         
     when effectiveDate                                                                                                                      
      then                                                                                                                               
       effectiveDate        = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.  
           
     when ratesCode                                                                                                                      
      then                                                                                                                               
       ratesCode            = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error. 
           
     when ratesMsg                                                                                                                      
      then                                                                                                                               
       ratesMsg             = entry(2,entry(iCount,pcCalculatedPremium,","),"=") no-error.         

    end case.  
  end.

  lSuccess = logical(success) no-error.
  
  if premiumOwner = "premiumOwner" 
   then
    premiumOwner = "0".

  if premiumOEndors = "premiumOEndors" 
   then
    premiumOEndors = "0".

  if premiumLoan = "premiumLoan" 
   then
    premiumLoan = "0".

  if premiumLEndors = "premiumLEndors" 
   then
    premiumLEndors = "0".

  if premiumScnd = "premiumScnd" 
   then
    premiumScnd = "0".

  if premiumSEndors = "premiumSEndors" 
   then
    premiumSEndors = "0".

  if premiumLenderCPL = "premiumLenderCPL" 
   then
    premiumLenderCPL = "0".

  if premiumBuyerCPL = "premiumBuyerCPL" 
   then
    premiumBuyerCPL = "0".

  if premiumSellerCPL = "premiumSellerCPL" 
   then
    premiumSellerCPL = "0".

  if effectiveDate = "manualEffectiveDate" 
   then
    effectiveDate = "". 

  if ratesCode = "RateCode" 
   then
    ratesCode = "".

  if ratesMsg = "RatesMsg" 
   then
    ratesMsg = "".              
        
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generateURL C-Win 
PROCEDURE generateURL :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable cRateURL       as character no-undo.
  define variable errorStatus    as logical   no-undo.      
  define variable errorMsg       as character no-undo. 
  define variable cUrl           as character no-undo.
  define variable cUID           as character no-undo.
  define variable cPass          as character no-undo.
  define variable cBaseURL       as character no-undo.
  
if cUrl = "" or cUrl = "?" then
 publish "GetServiceAddress" (output cURL). 
     
if cUID = "" or cUID = "?" then    
 publish "GetCredentialsID" (output cUID).
  
if cPass = "" or cPass = "?" then
 publish "GetCredentialsPassword" (output cPass). 

                                                                      
  /*-------------------Client Validations------------------------*/
  run ClientValidation in this-procedure(output errorStatus,
                                         output errorMsg) no-error.  
  if errorStatus 
   then
    do:
      message errorMsg
        view-as alert-box info buttons ok.
      btRateSheet:sensitive in frame frBasicResults = false.
      return no-apply.
    end.
    
    
  run setRateCalculateParam in this-procedure.
  
  if cBaseURL = "" or cBaseURL = "?" then
   cBaseURL =   cURL + "?I0=JSON&I1=" + cUID + "&I2=" + encrypt(cPass) + "&I3=rateCalculate&stateID=".

  cRateURL =  cBaseURL  + cStateID + "&UiInput=" + pcInputValues + "&version=" + CVersion +
             "&Log=true&Debug=true".
             
       
CLIPBOARD:VALUE = cRateURL.

END PROCEDURE.


 /*    SAMPLE URL:-
https://compass.alliantnational.com:8118/do/action/WService=dev/get?
I0=JSON&I1=ratecalc@alliantnational.com&I2=cndtPWNhS1JORG5DSmxjY1RaUWh4WGlpIXhrcU9aMDEwMWhYcW5xb3BFdlJZTGRUQmNzYmFwR0hjVFJsQlFOZGV0THZseUlGbG1vWnZvQXd5T0RlbVdKaEJ4YnZzblFTUGptRnRvbHJCTXVRS1NmeHV5SVNjWFVtQ3lsYmV1YkZlRG9q
&I3=rateCalculate&stateID=CO&UiInput=ratetype^O,ratetypecode^Original,coverageAmount^23243534,reissueCoverageAmount^0,ownerEndors^GE1-30|100.3-50,loanRateType^E,loanRateTypecode^Expanded%20Coverage,loanCoverageAmount^43543,loanReissueCoverageAmount^0,loanEndors^GE1-30|100.1-25,lenderCPL^1,buyerCPL^0,sellerCPL^0,propertyType^R,Version^1,region^Area1,effectiveDate^2/25/20,simultaneous^yes&version=1
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InitializeFrameLEndors C-Win 
PROCEDURE InitializeFrameLEndors :
/*------------------------------------------------------------------------------
  Purpose: initialize endosement frame for lender create check box and editor
           for description for each endorsement  
  Parameters:  <none>
  Notes:       
  ------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  iEndCount = 0.
  
  {lib\cleanCheckBox.i &hTemp = "tthldr"}.

  empty temp-table tthldr no-error.
  
  iNextEndors = 2.
  
  fiLendersEndorsList:screen-value in frame frBasicResults = "".

  for each loanEndorsement:

    iEndCount = iEndCount + 1.

    {lib\dynamicCheckBox.i &tableName = "loanEndorsement" , &seq = iEndCount , &frameName = "frLenderEndors" , &hTemp = "tthldr"}.

    iNextEndors = iNextEndors + 2.

    if iNextEndors ge 12 
     then 
      frame frLenderEndors:virtual-height = frame frLenderEndors:virtual-height + 2.
  end. 
  
  if iEndCount > 0 
   then
    frame frLenderEndors:virtual-height = iEndCount * 2.5.
  
  run ShowScrollBars(frame frLenderEndors:handle, no, yes).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InitializeFrameOEndors C-Win 
PROCEDURE InitializeFrameOEndors :
/*------------------------------------------------------------------------------
  Purpose: initialize endorsement frame for owner create check box and editor
           for description for each endorsement    
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  do with frame {&frame-name} :
  end.
  
  iEndCount = 0.
  
  {lib\cleanCheckBox.i &hTemp = "tthOwr"}.
  
  empty temp-table tthOwr no-error.

  iNextEndors = 2.
  
  fiOwnerEndorsList:screen-value in frame frBasicResults = "".
  
  for each ownerEndorsement:

    iEndCount = iEndCount + 1.

    {lib\dynamicCheckBox.i &tableName = "ownerEndorsement" &seq = iEndCount , &frameName = "frOwnerEndors" , &hTemp = "tthOwr"}.

    if iNextEndors ge 12 
     then 
      frame frOwnerEndors:virtual-height = frame frOwnerEndors:virtual-height + 2.

    iNextEndors = iNextEndors + 2.  
  end. 
  
  if iEndCount > 0 
   then
    frame frOwnerEndors:virtual-height = iEndCount * 2.5.
  
  run ShowScrollBars(frame frOwnerEndors:handle, no, yes).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InitializeFrameSEndors C-Win 
PROCEDURE InitializeFrameSEndors :
/*------------------------------------------------------------------------------
  Purpose: initialize endorsement frame for second loan create check box and editor
           for description for each endorsement 
  Parameters:  <none>
  Notes:
------------------------------------------------------------------------------*/
  do with frame {&frame-name} :
  end.

  iEndCount = 0.
  
  {lib\cleanCheckBox.i &hTemp = "tthScndLoan"}.
  
  empty temp-table tthScndLoan no-error.

  iNextEndors = 2.
  
  fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = "".
  
  for each secondLoanEndorsement:

    iEndCount = iEndCount + 1.

    {lib\dynamicCheckBox.i &tableName = "secondLoanEndorsement" &seq = iEndCount , &frameName = "frScndLenderEndors" , &hTemp = "tthScndLoan"}.

    if iNextEndors ge 12 
     then 
      frame frScndLenderEndors:virtual-height = frame frScndLenderEndors:virtual-height + 2.

    iNextEndors = iNextEndors + 2.  
  end. 
  
  if iEndCount > 0 
   then
    frame frScndLenderEndors:virtual-height = iEndCount * 2.5.
  
  run ShowScrollBars(frame frScndLenderEndors:handle, no, yes).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoadSecondLoanEndorsements C-Win 
PROCEDURE LoadSecondLoanEndorsements :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if (endSelected)       and
     (multiLoanSelected) and
      not(secondLoanEndorsLoaded) 
   then
    do:
      run InitializeFrameSEndors  in this-procedure no-error.
      secondLoanEndorsLoaded = true.
    end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LoanAmountValueChange C-Win 
PROCEDURE LoanAmountValueChange :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PropertyTypeValueChange C-Win 
PROCEDURE PropertyTypeValueChange :
/*------------------------------------------------------------------------------
  Purpose: handle UI state and rate type list for selected property type     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  do with frame frBasic:
  end. 

  define variable pclistOwner    as character no-undo.
  define variable pclistLoan     as character no-undo.
  define variable pclistScndLoan as character no-undo.
  
  define variable trackRateType           as character no-undo.
  define variable trackLoanRateType       as character no-undo.
  define variable trackSecondLoanRateType as character no-undo.
    
  if tbSimultaneous:checked
   then 
    run GetSimoComboList in hState(cbRegion:input-value,
                                 cbPropType:input-value,
                                 yes,
                                 output pclistOwner,  
                                 output pclistLoan,  
                                 output pclistScndLoan).
  else
   run GetComboLists in hState (cbRegion:input-value,
                                cbPropType:input-value,
                                output pclistOwner,  
                                output pclistLoan,  
                                output pclistScndLoan). 
  
  assign
    trackRateType           = cbRateType:input-value
    trackLoanRateType       = cbLoanRateType:input-value
    trackSecondLoanRateType = cbSecondLoanRateType:input-value in frame frScndLoan
    . 
  
  if entry(num-entries(pclistOwner,","),pclistOwner,",") = {&None} 
   then
    run enableDisableCaseType("Owner",false).
  else
    run enableDisableCaseType("Owner",true).
  
  if entry(num-entries(pclistLoan,","),pclistLoan,",") = {&None} 
   then
    run enableDisableCaseType("loan",false).
  else
    run enableDisableCaseType("loan",true).

  if entry(num-entries(pclistScndLoan,","),pclistScndLoan,",") = {&None} 
   then
    run enableDisableCaseType("SecondLoan",false).
  else
    run enableDisableCaseType("SecondLoan",true).  

  run enableFooter (fiCoverageAmt   :sensitive or 
                    fiLoanAmt       :sensitive or
                    fiSecondLoanAmt :sensitive).
  assign
    cbRateType          :list-item-pairs                     = trim(pclistOwner , ",")
    cbLoanRateType      :list-item-pairs                     = trim(pclistLoan , ",")
    cbSecondLoanRateType:list-item-pairs in frame frScndLoan = trim(pclistScndLoan , ",")
    .

  if can-do(cbRateType:list-item-pairs,trackRateType) 
   then
    cbRateType:screen-value = trackRateType.
  else
    cbRateType:screen-value = {&None}.
  
  if can-do(cbLoanRateType:list-item-pairs,trackLoanRateType)
   then
    cbLoanRateType:screen-value = trackLoanRateType.
  else
    cbLoanRateType:screen-value = {&None}.
  
  if can-do(cbSecondLoanRateType:list-item-pairs,trackSecondLoanRateType) 
   then
    cbSecondLoanRateType:screen-value = trackSecondLoanRateType.
  else
    cbSecondLoanRateType:screen-value = {&None}.

 if endSelected then
   run reloadEndors in this-procedure ("") no-error.
 apply 'value-changed' to cbRateType.
 apply 'value-changed' to cbLoanRateType.      

  cLastPropType   = cbPropType:input-value.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE RefreshEndors C-Win 
PROCEDURE RefreshEndors :
/*------------------------------------------------------------------------------
  Purpose: empty endorsement list     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pCase as character no-undo.
  
  case pCase:
    when {&Owners} 
     then
      for each tthOwr where tthOwr.hChecked:
        apply "value-changed":U to tthOwr.hToggle.
        tthOwr.hToggle:checked = false.
      end.
    when {&Lenders} 
     then
      for each tthLdr where tthldr.hChecked:
        apply "value-changed":U to tthldr.hToggle.
        tthldr.hToggle:checked = false.
      end.
    when {&SecondLoan} 
     then
      for each tthScndLoan where tthScndLoan.hChecked:
        apply "value-changed":U to tthScndLoan.hToggle.
        tthScndLoan.hToggle:checked = false.
      end.
    otherwise
      do:
        for each tthOwr where tthOwr.hChecked:
          apply "value-changed":U to tthOwr.hToggle.
          tthOwr.hToggle:checked = false.
        end.
        for each tthLdr where tthldr.hChecked:
          apply "value-changed":U to tthldr.hToggle.
          tthldr.hToggle:checked = false.
        end.
        for each tthScndLoan where tthScndLoan.hChecked:
          apply "value-changed":U to tthScndLoan.hToggle.
          tthScndLoan.hToggle:checked = false.
        end.
      end.
  end case.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE regionValueChange C-Win 
PROCEDURE regionValueChange :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable trackPropType as character no-undo.
 
  trackPropType = cbPropType:input-value in frame frBasic.
 
  run GetPropertyType in hState  (input cbRegion:input-value, 
                               output pcPropertyType).
 
  cbPropType:list-item-pairs = pcPropertyType.
 
  if can-do(pcPropertyType,trackPropType) 
   then
    cbPropType:screen-value = trackPropType .
  else
    cbPropType:screen-value = entry(2,pcPropertyType,",") .  

/*  if endselected                                               */
/*    then                                                       */
/*       run reloadEndors in this-procedure (input "") no-error. */
 
  run PropertyTypeValuechange in this-procedure.  

  cLastRegion = cbRegion:Input-value.   
  
  for first region where region.regioncode = cLastRegion:
    cbRegion:tooltip = region.description.
  end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ReloadEndors C-Win 
PROCEDURE ReloadEndors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcCaseType as character no-undo.
  
  define variable chOwnerEndorsementList      as character no-undo.
  define variable chLoanEndorsementList       as character no-undo.
  define variable chSecondLoanEndorsementList as character no-undo.
  define variable iCountLEndors               as integer   no-undo.
  
  do with frame frBasicResults:
  end.
  
  assign
    chOwnerEndorsementList      = fiOwnerEndorsList        :input-value
    chLoanEndorsementList       = fiLendersEndorsList      :input-value
    chSecondLoanEndorsementList = fiSecondLendersEndorsList:input-value in frame frScndLoanResults
    .
 
  do with frame frBasic:
  end.  

  case ipcCaseType:
    when {&Owners} 
     then
      ownersBlk:
      do:
        if (cLastRegion   = cbRegion:input-value   and
           cLastPropType  = cbPropType:input-value and 
           cLastORateType = cbRateType:input-value)
         then
          leave ownersBlk.              

        empty temp-table ownerEndorsement no-error.
        run GetEndors in hState (input ipcCaseType, input cbRegion:input-value, input cbPropType:input-value,input cbRateType:input-value,output table ownerEndorsement).
        run InitializeFrameOEndors in this-procedure.
        do iCountLEndors = 1 to num-entries(chOwnerEndorsementList,","):    
          find first ownerEndorsement where ownerEndorsement.endorsementCode = entry(iCountLEndors,chOwnerEndorsementList,",") no-error.
          if available ownerEndorsement
           then
            do:
              find first tthowr where tthowr.hseq = ownerEndorsement.endorsementCode no-error.
              if available tthowr 
               then
                assign 
                  fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:input-value in frame frBasicResults + "," + ownerEndorsement.endorsementCode , ",")
                  tthowr.hToggle:checked = true
                  tthowr.hChecked        = true
                  tthowr.hEditor:fgcolor = 9
                  .      
            end.
        end.
      end.
    when {&Lenders} 
     then
      lendersBlk:
      do:
        if (cLastRegion    = cbRegion:input-value  and
           cLastPropType  = cbPropType:input-value and 
           cLastLRateType = cbLoanRateType:input-value)
         then
          leave lendersBlk.

        empty temp-table loanEndorsement no-error.
        run GetEndors in hState (input ipcCaseType,input cbRegion:input-value, input cbPropType:input-value,input cbLoanRateType:input-value,output table loanEndorsement).
        run InitializeFrameLEndors in this-procedure no-error.
        do iCountLEndors = 1 to num-entries(chLoanEndorsementList,","):    
          find first loanEndorsement where loanEndorsement.endorsementCode = entry(iCountLEndors,chLoanEndorsementList,",") no-error.
          if available loanEndorsement 
           then
            do:
              find first tthldr where tthldr.hseq = loanEndorsement.endorsementCode no-error.
              if available tthldr 
               then
                assign 
                  fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:input-value in frame frBasicResults + "," + loanEndorsement.endorsementCode , ",")
                  tthldr.hToggle:checked = true
                  tthldr.hChecked        = true
                  tthldr.hEditor:fgcolor = 9
                  .      
            end.
        end.
      end.
    when {&SecondLoan}      
     then
      secondLoanBlk:
      do:
        if (cLastRegion    = cbRegion:input-value   and
           cLastPropType   = cbPropType:input-value and 
           cLastSLRateType = cbSecondLoanRateType:input-value in frame frScndLoan)
         then
          leave secondLoanBlk.
        empty temp-table secondLoanEndorsement no-error.
        run GetEndors in hState (input {&Lenders}, input cbRegion:input-value, input cbPropType:input-value, input cbSecondLoanRateType:input-value,output table secondLoanEndorsement).
        run InitializeFrameSEndors in this-procedure no-error.
        do iCountLEndors = 1 to num-entries(chSecondLoanEndorsementList,","):    
          find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = entry(iCountLEndors,chSecondLoanEndorsementList,",") no-error.
          if available secondLoanEndorsement 
           then
            do:
              find first tthScndLoan where tthScndLoan.hseq = secondLoanEndorsement.endorsementCode no-error.
              if available tthScndLoan 
               then
                assign 
                  fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = trim(fiSecondLendersEndorsList:input-value in frame frScndLoanResults + "," + secondLoanEndorsement.endorsementCode , ",")
                  tthScndLoan.hToggle:checked = true
                  tthScndLoan.hChecked        = true
                  tthScndLoan.hEditor:fgcolor = 9
                  .      
            end.
        end.
      end.
    otherwise
      do:
         allOwnerBlk:
         do:
            if cLastRegion     = cbRegion            :Screen-value     and
               cLastPropType   = cbPropType          :Screen-value     and 
               cLastORateType  = cbRateType          :Screen-value     
             then
              leave allOwnerBlk.
            else 
             do:
                empty temp-table ownerEndorsement no-error.
                run GetEndors in hState  (input {&Owners},input cbRegion:input-value, input cbPropType:input-value,input cbRateType:input-value,output table ownerEndorsement).
                run InitializeFrameOEndors in this-procedure no-error.
                do iCountLEndors = 1 to num-entries(chOwnerEndorsementList,","):    
                   find first ownerEndorsement where ownerEndorsement.endorsementCode = entry(iCountLEndors,chOwnerEndorsementList,",") no-error.
                   if available ownerEndorsement 
                    then
                     do:
                       find first tthowr where tthowr.hseq = ownerEndorsement.endorsementCode no-error.
                       if available tthowr 
                        then
                         assign 
                           fiOwnerEndorsList:screen-value in frame frBasicResults = trim(fiOwnerEndorsList:input-value in frame frBasicResults + "," + ownerEndorsement.endorsementCode , ",")
                           tthowr.hToggle:checked = true
                           tthowr.hChecked        = true
                           tthowr.hEditor:fgcolor = 9
                           .      
                     end.
                end.
              end.
         end.

      allLenderBlk:
      do:
         if cLastRegion     = cbRegion            :Screen-value     and
            cLastPropType   = cbPropType          :Screen-value     and 
            cLastLRateType  = cbLoanRateType      :Screen-value     
          then
           leave allLenderBlk.
         else 
          do:
             empty temp-table loanEndorsement no-error.
             run GetEndors in hState (input {&Lenders},input cbRegion:input-value, input cbPropType:input-value,input cbLoanRateType:input-value,output table loanEndorsement).
             run InitializeFrameLEndors in this-procedure no-error.
             do iCountLEndors = 1 to num-entries(chLoanEndorsementList,","):    
                find first loanEndorsement where loanEndorsement.endorsementCode = entry(iCountLEndors,chLoanEndorsementList,",") no-error.
                if available loanEndorsement 
                 then
                  do:
                    find first tthldr where tthldr.hseq = loanEndorsement.endorsementCode no-error.
                    if available tthldr 
                     then
                      assign 
                        fiLendersEndorsList:screen-value in frame frBasicResults = trim(fiLendersEndorsList:input-value in frame frBasicResults + "," + loanEndorsement.endorsementCode , ",")
                        tthldr.hToggle:checked = true
                        tthldr.hChecked        = true
                        tthldr.hEditor:fgcolor = 9
                        .      
                  end.
             end.
         end.
     end.

     allSecondLenderBlk:
     do:    
        if cLastRegion     = cbRegion            :Screen-value     and
           cLastPropType   = cbPropType          :Screen-value     and 
           cLastSLRateType = cbSecondLoanRateType:Screen-value in frame frScndLoan
         then
          leave allSecondLenderBlk.
        else 
        do:
           empty temp-table secondLoanEndorsement no-error.
           run GetEndors in hState (input {&Lenders}, input cbRegion:input-value, input cbPropType:input-value, input cbSecondLoanRateType:input-value,output table secondLoanEndorsement).
           run InitializeFrameSEndors in this-procedure no-error.
           do iCountLEndors = 1 to num-entries(chSecondLoanEndorsementList,","):    
              find first secondLoanEndorsement where secondLoanEndorsement.endorsementCode = entry(iCountLEndors,chSecondLoanEndorsementList,",") no-error.
              if available secondLoanEndorsement 
               then
                do:
                  find first tthScndLoan where tthScndLoan.hseq = secondLoanEndorsement.endorsementCode no-error.
                  if available tthScndLoan 
                   then
                    assign 
                      fiSecondLendersEndorsList:screen-value in frame frScndLoanResults = trim(fiSecondLendersEndorsList:input-value in frame frScndLoanResults + "," + secondLoanEndorsement.endorsementCode , ",")
                      tthScndLoan.hToggle:checked = true
                      tthScndLoan.hChecked        = true
                      tthScndLoan.hEditor:fgcolor = 9
                      .      
                end.
           end.
        end.
     end.
  end.  
end case.    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setLoanPriorInfoState C-Win 
PROCEDURE setLoanPriorInfoState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable loShowPriorPolicyAmount as logical no-undo. 
  define variable loShowPriorPolicyDate   as logical no-undo.

  do with frame {&frame-name}:
  end.

/*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
  run GetPriorPolicyAmountConfig in hState  (input  cbRegion:input-value in frame frBasic,
                                          input  cbPropType:input-value in frame frBasic,
                                          input  {&Lenders},
                                          input  cbLoanRateType:input-value,
                                          output loShowPriorPolicyAmount).
  
  run GetPriorPolicyDateConfig in hState  (input cbRegion:input-value in frame frBasic,
                                        input cbPropType:input-value in frame frBasic,
                                        input {&Lenders},
                                        input cbLoanRateType:input-value,                                      
                                        output loShowPriorPolicyDate).
   /* specific to nevada state*/                                     
  if cStateID = "NV" and cbLoanRateType:input-value = "B" and loShowPriorPolicyAmount = true 
   then
    assign
        fiLoanPriorAmt:sensitive = loShowPriorPolicyAmount
        fiLoanPriorAmt:label     = "Number of Parcels"
        fiLoanPriorAmt:tooltip   = "Number of parcels cannot be less than 2"
        .
  else 
   fiLoanPriorAmt:label   = "Prior Policy Amount". 
     
     
  fiLoanPriorAmt:sensitive = loShowPriorPolicyAmount.
  fiLoanEffectiveDate:sensitive = loShowPriorPolicyDate.

  if not loShowPriorPolicyAmount
   then
    fiLoanPriorAmt:screen-value = "".

  if not loShowPriorPolicyDate 
   then 
    fiLoanEffectiveDate:screen-value = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setOwnerPriorInfoState C-Win 
PROCEDURE setOwnerPriorInfoState :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable loShowPriorPolicyAmount as logical no-undo. 
  define variable loShowPriorPolicyDate   as logical no-undo.

  do with frame {&frame-name}:
  end.

/*-----------------------------STATE SPECIFIC CODE---------------------------------------*/
  run GetPriorPolicyAmountConfig in hState  (input  cbRegion:input-value in frame frBasic,
                                          input  cbPropType:input-value in frame frBasic,
                                          input  {&Owners},
                                          input  cbRateType:input-value,
                                          output loShowPriorPolicyAmount).
  
  run GetPriorPolicyDateConfig in hState  (input cbRegion:input-value in frame frBasic,
                                        input cbPropType:input-value in frame frBasic,
                                        input {&Owners},
                                        input cbRateType:input-value,                                      
                                        output loShowPriorPolicyDate).

 fiPriorAmt:sensitive = loShowPriorPolicyAmount.
 fiEffectiveDate:sensitive = loShowPriorPolicyDate.

  if not loShowPriorPolicyAmount 
   then 
    fiPriorAmt:screen-value = "".

  if not loShowPriorPolicyDate 
   then 
    fiEffectiveDate:screen-value = ?.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetPosition C-Win 
PROCEDURE SetPosition :
/*------------------------------------------------------------------------------
  Purpose: resize ui based on endorsements are shown or hidden or second
           loan is shown or hidden  
  Parameters:  <none>
  Notes:       
  ------------------------------------------------------------------------------*/
  if not(endselected) and 
     not(multiloanselected) 
   then
    assign
      frame frScndLoan       :visible = false
      frame frScndLoanEndors :visible = false
      frame frScndLoanResults:visible = false
      frame frBasicEndors    :visible = false
      c-win:height                    = cutheight
      c-win:width                     = cutwidth
      frame frBasicResults:row        = endorsPos
      .

  else if endselected and 
       not(multiloanselected) 
   then
    do:
      assign 
        frame frScndLoanResults:visible = false
        frame frScndLoan       :visible = false
        frame frScndLoanEndors :visible = false
        frame frBasicEndors    :visible = true
        c-win:height                    = fullheight
        c-win:width                     = cutwidth
        frame frBasicEndors:row         = endorsPos
        frame frBasicResults:row        = resultsPos
        .
      run ShowScrollBars(frame frBasic:handle, no,no).
      run ShowScrollBars(frame frBasicResults:handle, no, no).
      run ShowScrollBars(frame frBasicEndors:handle, no, no).  
    end.    

  else if multiloanselected and 
          endselected 
   then
    do:
      assign
        frame frBasicResults  :visible  = true
        frame frScndLoan      :visible  = true
        frame frBasicEndors   :visible  = true
        frame frScndLoanEndors:visible  = true
        frame frScndLoanResults:visible = true
        c-win:height                    = fullheight
        c-win:width                     = fullwidth
        frame frScndLoanResults:row     = resultsPos 
        frame frBasicResults   :row     = resultsPos
        frame frBasicEndors    :row     = endorsPos  
        frame frScndLoanEndors :row     = endorsPos
        .       
      run ShowScrollBars(frame frBasic:handle, no,no).
      run ShowScrollBars(frame frBasicResults:handle, no, no).
      run ShowScrollBars(frame frBasicEndors:handle, no, no).
      run ShowScrollBars(frame frScndLoan:handle, no, no).
      run ShowScrollBars(frame frScndLoanResults:handle, no, no).
      run ShowScrollBars(frame frScndLoanEndors:handle, no, no).
      run ShowScrollBars(frame {&frame-name}:handle, no,no).
    end.

  else if multiloanselected and 
          not(endselected) 
   then
    do:
      assign
        frame frScndLoan       :visible = true
        frame frScndLoanResults:visible = true
        frame frScndLoanEndors :visible = false
        frame frBasicEndors    :visible = false
        c-win:height                    = cutheight
        c-win:width                     = fullwidth
        frame frBasicResults   :row     = endorsPos
        frame frScndLoanResults:row     = endorsPos
        .
    
      run ShowScrollBars(frame frBasic:handle, no,no).
      run ShowScrollBars(frame frBasicResults:handle, no, no).
      run ShowScrollBars(frame frBasicEndors:handle, no, no).
      run ShowScrollBars(frame frScndLoan:handle, no, no).
      run ShowScrollBars(frame frScndLoanResults:handle, no, no).
      run ShowScrollBars(frame frScndLoanEndors:handle, no, no).
      run ShowScrollBars(frame {&frame-name}:handle, no,no).
    end.
/*--------------------------for endors trigger--------------------------------*/
  if endselected = true then
  do:
    if flag = true then
    do:
        run reloadEndors in this-procedure ("") no-error.
    end.
    flag = false.
  end.
/*------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE setRateCalculateParam C-Win 
PROCEDURE setRateCalculateParam :
/*------------------------------------------------------------------------------
  Purpose:     
  pcInputValues:  <none>
  Notes:       
------------------------------------------------------------------------------*/                          
  define variable proposedRate       as decimal   no-undo.
  
  /* variable contain list of selected endorsement */                                   
  define variable ownerEndors        as character no-undo.    
  define variable loanEndors         as character no-undo.    
  define variable secondLoanEndors   as character no-undo.

  define variable iCountOEndors      as integer   no-undo.
  define variable iCountLEndors      as integer   no-undo.
  define variable iCountSEndors      as integer   no-undo.
  

  do with frame {&frame-name}:                                                
  end.  
  
  ownerEndorsPR       = "".       
  loanEndorsPR        = "".      
  secondLoanEndorsPR  = "".  
  pcInputValues       = "".
  
  /*--------------------------------------Get Endorsement Lists--------------------------------------*/
  ownerEndors = fiOwnerEndorsList:input-value in frame frBasicResults.
  ownerEndors = replace(ownerEndors, ",", "|").

  loanEndors = fiLendersEndorsList:input-value in frame frBasicResults.
  loanEndors = replace(loanEndors, ",", "|").

  secondLoanEndors = fiSecondLendersEndorsList:input-value in frame frScndLoanResults.
  secondLoanEndors = replace(secondLoanEndors, ",", "|").
  
  
  /*--------------------------Embed proposed rate in the endorsement list-----------------------------*/
  do iCountOEndors = 1 to num-entries(ownerEndors,"|"):

    run GetProposedRate in hState (input entry(iCountOEndors,ownerEndors,"|"),
                                   output proposedRate) no-error.
    ownerEndorsPR = ownerEndorsPR + 
                    entry(iCountOEndors,ownerEndors,"|") + "-" + 
                    (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.

  end.
  
  do iCountLEndors = 1 to num-entries(loanEndors,"|"):

    run GetProposedRate in hState (input entry(iCountLEndors,loanEndors,"|"),
                                   output proposedRate) no-error.

    loanEndorsPR = loanEndorsPR + 
                   entry(iCountLEndors,loanEndors,"|") + "-" + 
                   (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.

  end.
  
  do iCountSEndors = 1 to num-entries(secondLoanEndors,"|"):

    run GetProposedRate in hState (input entry(iCountSEndors,secondLoanEndors,"|"),
                                   output proposedRate) no-error.

    secondLoanEndorsPR = secondLoanEndorsPR + 
                         entry(iCountSEndors,secondLoanEndors,"|") + "-" + 
                         (if proposedRate ne ? then string(proposedRate) else "null") + "|" no-error.

  end.

  assign
    ownerEndorsPR = trim(ownerEndorsPR,"|")
    loanEndorsPR = trim(loanEndorsPR,"|")
    secondLoanEndorsPR = trim(secondLoanEndorsPR,"|")
    .
  
  do with frame frBasic:
  end.
  
  /*-------------------------Input variable-------------------------------*/
  pcInputValues = "region^"                     + cbRegion:input-value +
                  ",propertyType^"              + cbPropType:input-value +
                  ",simultaneous^"              + tbSimultaneous:input-value +                  
                  ",Version^"                   + cVersion +
                  ",coverageAmount^"            + fiCoverageAmt:input-value + 
                  ",ratetype^"                  + if cbRateType:input-value  = ? or cbRateType:input-value  = "?" then "" else cbRateType:input-value +                  
                  ",reissueCoverageAmount^"     + fiPriorAmt:input-value +
                  ",priorEffectiveDate^"        + (if fiEffectiveDate:input-value = ? then "" else fiEffectiveDate:input-value) +  
                  ",ownerEndors^"               + ownerEndorsPR + 
                  ",loanCoverageAmount^"        + fiLoanAmt:input-value +
                  ",loanRateType^"              + if cbLoanRateType:input-value = ? or cbLoanRateType:input-value = "?" then "" else cbLoanRateType:input-value +
                  ",loanReissueCoverageAmount^" + fiLoanPriorAmt:input-value +
                  ",loanPriorEffectiveDate^"    + (if fiLoanEffectiveDate:input-value = ? then "" else fiLoanEffectiveDate:input-value) +  
                  ",loanEndors^"                + loanEndorsPR +                                                      
                  ",lenderCPL^"                 + fiLenderCPL:input-value +
                  ",buyerCPL^"                  + fiBuyerCPL:input-value +
                  ",sellerCPL^"                 + fiSellerCPL:input-value         
                  .
 
 if tbSimultaneous:checked 
   then
    do with frame frScndLoan:
      pcInputValues = pcInputValues + 
                      ",secondloanRateType^"       + if cbSecondLoanRateType:input-value  = ? or cbSecondLoanRateType:input-value  = "?" then "" else cbSecondLoanRateType:input-value +
                      ",secondloanCoverageAmount^" + fiSecondLoanAmt:input-value +
                      ",secondLoanEndors^"         + secondLoanEndorsPR
                      .
    end. 
  end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION RemoveItemFromList C-Win 
FUNCTION RemoveItemFromList returns character
        (pclist as character,
         pcremoveitem as character,
         pcdelimiter as character):
     
  define variable lipos as integer no-undo.
  
  lipos = lookup(pcremoveitem,pclist,pcdelimiter).
  
  if lipos > 0 
   then
    do:
      assign entry(lipos,pclist,pcdelimiter) = "".
      if lipos = 1 
       then
        pclist = substring(pclist,2).
      else if lipos = num-entries(pclist,pcdelimiter) 
       then
        pclist = substring(pclist,1,length(pclist) - 1).
      else
        pclist = replace(pclist,pcdelimiter + pcdelimiter,pcdelimiter).
    end.
  
  return pclist.
  
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

