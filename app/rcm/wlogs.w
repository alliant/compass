&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: rcm/wlogs.w

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 
  @Modified    :
    Date        Name    Comments
    07/01/2019  Anjly   Modified for UAT comments.

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

{lib\std-def.i}
{lib\rcm-std-def.i}
{lib\winlaunch.i}

/* Temp-table Definitions ---                                           */
{tt\ratelog.i}

/* Parameters Definitions ---                                           */
define input parameter table for rateLog.
define input parameter parameters as character no-undo.

/* Local Variable Definitions ---                                       */
define variable iCount              as integer   no-undo.
define variable ownerEndors         as character no-undo initial "ownerEndors".
define variable loanEndors          as character no-undo initial "loanEndors".
define variable secondLoanEndors    as character no-undo initial "secondLoanEndors".
define variable propertyType        as character no-undo initial "propertyType".
define variable cbTypeAmountList    as character no-undo.
define variable cbCaseTypeList      as character no-undo.
define variable cbCalculationOnList as character no-undo.
define variable chAttrib            as character no-undo.
define variable flag                as logical   no-undo initial true.
define variable ibgcolor            as integer   no-undo.

define variable cCardSetID        as character no-undo.
define variable cVersion          as character no-undo.
define variable cVersionDesc      as character no-undo.
define variable cStateID          as character no-undo.
define variable cEffectDate       as character no-undo.
define variable hColumnHandle     as handle    no-undo.

define variable iownerendors      as integer no-undo.
define variable iloanrendors      as integer no-undo.
define variable iscndendors       as integer no-undo.
define variable pStateID as character.

define temp-table ttWCards
    field hWCard as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwLog

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES rateLog

/* Definitions for BROWSE brwLog                                        */
&Scoped-define FIELDS-IN-QUERY-brwLog rateLog.calculationFor rateLog.action string(fill(" ",20 - length (rateLog.actionCalculation )) + rateLog.actionCalculation ) @ rateLog.actionCalculation rateLog.calculationOn   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwLog   
&Scoped-define SELF-NAME brwLog
&Scoped-define QUERY-STRING-brwLog for each rateLog by rateLog.seq
&Scoped-define OPEN-QUERY-brwLog open query {&SELF-NAME} for each rateLog by rateLog.seq.
&Scoped-define TABLES-IN-QUERY-brwLog rateLog
&Scoped-define FIRST-TABLE-IN-QUERY-brwLog rateLog


/* Definitions for FRAME DEFAULT-FRAME                                  */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bExport cbTypeAmount cbCaseType eParameters ~
cbCalculationOn RECT-68 brwLog RECT-69 
&Scoped-Define DISPLAYED-OBJECTS cbTypeAmount cbCaseType eParameters ~
cbCalculationOn 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCardDetail  NO-FOCUS
     LABEL "View Cards Detail" 
     SIZE 7.2 BY 1.71 TOOLTIP "View Cards Details".

DEFINE BUTTON bExport  NO-FOCUS
     LABEL "Export" 
     SIZE 7.2 BY 1.71 TOOLTIP "Export".

DEFINE VARIABLE cbCalculationOn AS CHARACTER FORMAT "X(256)":U 
     LABEL "Calculation on" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE cbCaseType AS CHARACTER FORMAT "X(256)":U 
     LABEL "Calculation for" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 20.8 BY 1 NO-UNDO.

DEFINE VARIABLE cbTypeAmount AS CHARACTER FORMAT "X(256)":U 
     LABEL "Calculation of" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEMS "Item 1" 
     DROP-DOWN-LIST
     SIZE 38.8 BY 1 NO-UNDO.

DEFINE VARIABLE eParameters AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 44.4 BY 18.76 NO-UNDO.

DEFINE RECTANGLE RECT-68
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 135.2 BY 2.14.

DEFINE RECTANGLE RECT-69
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwLog FOR 
      rateLog SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwLog C-Win _FREEFORM
  QUERY brwLog DISPLAY
      rateLog.calculationFor label "Calculation For" format "x(30)"  width 25
rateLog.action               label "Action"          format "x(150)" width 110
string(fill(" ",20 - length (rateLog.actionCalculation  )) + rateLog.actionCalculation  )   @ rateLog.actionCalculation    label "Results"    format "x(20)"     width 29
rateLog.calculationOn        label "Calculation On"  format "x(20)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 190 BY 16.81 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bExport AT ROW 1.48 COL 184 WIDGET-ID 10 NO-TAB-STOP 
     bCardDetail AT ROW 1.48 COL 191.6 WIDGET-ID 2 NO-TAB-STOP 
     cbTypeAmount AT ROW 1.81 COL 61.2 COLON-ALIGNED WIDGET-ID 6
     cbCaseType AT ROW 1.81 COL 115.8 COLON-ALIGNED WIDGET-ID 8
     eParameters AT ROW 1.67 COL 2.6 NO-LABEL WIDGET-ID 4
     cbCalculationOn AT ROW 1.81 COL 153 COLON-ALIGNED WIDGET-ID 20
     brwLog AT ROW 3.62 COL 48 WIDGET-ID 400
     "Parameters" VIEW-AS TEXT
          SIZE 11.8 BY .62 AT ROW 1.05 COL 3.4 WIDGET-ID 12
     "Filters" VIEW-AS TEXT
          SIZE 5.6 BY .62 AT ROW 1.05 COL 48.6 WIDGET-ID 18
     RECT-68 AT ROW 1.29 COL 48 WIDGET-ID 14
     RECT-69 AT ROW 1.29 COL 183 WIDGET-ID 16
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 238.4 BY 19.52 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Rate Calculation Logs"
         HEIGHT             = 19.52
         WIDTH              = 238.4
         MAX-HEIGHT         = 33.62
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 33.62
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwLog RECT-68 DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bCardDetail IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       brwLog:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwLog:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       eParameters:READ-ONLY IN FRAME DEFAULT-FRAME        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwLog
/* Query rebuild information for BROWSE brwLog
     _START_FREEFORM
open query {&SELF-NAME} for each rateLog by rateLog.seq.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwLog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Rate Calculation Logs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Rate Calculation Logs */
DO:
  for each ttWCards:
   delete procedure ttWCards.hWCard no-error.
  end.
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Rate Calculation Logs */
DO:
  run windowResized in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCardDetail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCardDetail C-Win
ON CHOOSE OF bCardDetail IN FRAME DEFAULT-FRAME /* View Cards Detail */
DO:

  if integer(self:private-data) ne 0 and integer(self:private-data) ne ? then
  do:
       publish "GetCurrentValue" ("RateStateID",output pStateID).
       if pStateID = "" then
       do:
       
          create ttWCards.
          run wcard.w persistent set ttWCards.hWCard (input cCardSetID,         
                                                      input cVersion,          
                                                      input cVersionDesc,          
                                                      input cStateID,
                                                      input cEffectDate,
                                                      integer(self:private-data)) no-error. 
       end.
       else 
       do:
   
          create ttWCards.
          run wcard-view.w persistent set ttWCards.hWCard (input cCardSetID,         
                                                             input cVersion,          
                                                             input cVersionDesc,          
                                                             input cStateID,
                                                             input cEffectDate,
                                                             integer(self:private-data)) no-error. 

       end.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bExport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bExport C-Win
ON CHOOSE OF bExport IN FRAME DEFAULT-FRAME /* Export */
DO:
  run exportRateLogData in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwLog
&Scoped-define SELF-NAME brwLog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLog C-Win
ON DEFAULT-ACTION OF brwLog IN FRAME DEFAULT-FRAME
DO:
  apply 'choose' to bCardDetail.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLog C-Win
ON ROW-DISPLAY OF brwLog IN FRAME DEFAULT-FRAME
DO:
  hColumnHandle:font = 0.

  if rateLog.groupID mod 2 = 0  then
    assign
      rateLog.calculationFor   :bgcolor in browse brwLog   = 32
      rateLog.calculationOn    :bgcolor in browse brwLog   = 32
      rateLog.action           :bgcolor in browse brwLog   = 32
      rateLog.actionCalculation:bgcolor in browse brwLog   = 32.
  else
    assign
     rateLog.calculationFor   :bgcolor in browse brwLog    = ?
     rateLog.calculationOn    :bgcolor in browse brwLog    = ?
     rateLog.action           :bgcolor in browse brwLog    = ?
     rateLog.actionCalculation:bgcolor in browse brwLog    = ?.

  if ratelog.calculationFor = "Grand Total" then
    assign
      rateLog.calculationFor   :bgcolor in browse brwLog   = 33
      rateLog.calculationOn    :bgcolor in browse brwLog   = 33
      rateLog.action           :bgcolor in browse brwLog   = 33
      rateLog.actionCalculation:bgcolor in browse brwLog   = 33.
  if ratelog.calculationFor = {&Scenario} then
    assign
      rateLog.calculationFor   :bgcolor in browse brwLog   = 18
      rateLog.calculationOn    :bgcolor in browse brwLog   = 18
      rateLog.action           :bgcolor in browse brwLog   = 18
      rateLog.actionCalculation:bgcolor in browse brwLog   = 18.

    if lookup(trim({&GroupError}),ratelog.calculationOn, " ") > 0 then
    assign
      rateLog.calculationFor   :bgcolor in browse brwLog   = 19
      rateLog.calculationOn    :bgcolor in browse brwLog   = 19
      rateLog.action           :bgcolor in browse brwLog   = 19
      rateLog.actionCalculation:bgcolor in browse brwLog   = 19.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwLog C-Win
ON VALUE-CHANGED OF brwLog IN FRAME DEFAULT-FRAME
DO:
  find current ratelog no-error.
  if available ratelog 
   then
    do:
       if  rateLog.cardID = 0 
        then
         assign
              bCardDetail:label     = "View Card Detail"
              bCardDetail:tooltip   = "View Card Detail"
              bCardDetail:sensitive = false
              bCardDetail:private-data = string(rateLog.cardID)
              .
    else
      assign
        bCardDetail:sensitive    = true
        bCardDetail:label        = "View Card " + string(rateLog.cardID)
        bCardDetail:tooltip      = "View Card " + string(rateLog.cardID)
        bCardDetail:private-data = string(rateLog.cardID).
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbCalculationOn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbCalculationOn C-Win
ON VALUE-CHANGED OF cbCalculationOn IN FRAME DEFAULT-FRAME /* Calculation on */
DO:
   run filterData(cbTypeAmount:screen-value,
                  cbCaseType:screen-value,
                  cbCalculationOn:screen-value).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbCaseType
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbCaseType C-Win
ON VALUE-CHANGED OF cbCaseType IN FRAME DEFAULT-FRAME /* Calculation for */
DO:
   run filterData(cbTypeAmount:screen-value,
                  cbCaseType:screen-value,
                  cbCalculationOn:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbTypeAmount
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbTypeAmount C-Win
ON VALUE-CHANGED OF cbTypeAmount IN FRAME DEFAULT-FRAME /* Calculation of */
DO:
   run filterData(cbTypeAmount:screen-value,
                  cbCaseType:screen-value,
                  cbCalculationOn:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

subscribe to "emptyViewCards" anywhere.
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels  = {&window-name}:width-pixels.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

bCardDetail:load-image ("images/open.bmp")               no-error.
bCardDetail:load-image-insensitive ("images/open-i.bmp") no-error.
bExport    :load-image("images/excel.bmp")               no-error.
bExport    :load-image-insensitive("images/excel-i.bmp") no-error.


for each rateLog break by rateLog.typeOfAmount:
  if first-of(rateLog.typeOfAmount) then
    cbTypeAmountList =  cbTypeAmountList + "," + rateLog.typeOfAmount.
end.

assign
    cbTypeAmountList          = trim(cbTypeAmountList,",")
    cbTypeAmountList          = {&all} + "," + cbTypeAmountList
    cbTypeAmount:list-items   = trim(cbTypeAmountList,",")
    cbTypeAmount:screen-value = {&all}
    .

for each rateLog break by rateLog.calculationFor:
  if first-of(rateLog.calculationFor) then
    cbCaseTypeList =  cbCaseTypeList + "," + rateLog.calculationFor .
end.

assign 
    cbCaseTypeList          = trim(cbCaseTypeList,",")
    cbCaseTypeList          = {&all} + "," + cbCaseTypeList
    cbCaseType:list-items   = trim(cbCaseTypeList,",")
    cbCaseType:screen-value = {&all}
    .
for each rateLog break by rateLog.calculationOn:
  if first-of(rateLog.calculationOn) then
    cbCalculationOnList =  cbCalculationOnList + "," + rateLog.calculationOn .
end.

assign 
    cbCalculationOnList          = trim(cbCalculationOnList,",")
    cbCalculationOnList          = {&all} + "," + cbCalculationOnList
    cbCalculationOn:list-items   = trim(cbCalculationOnList,",")
    cbCalculationOn:screen-value = {&all}
    .




run extractParams.

{lib/win-main.i}

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
   
  parameters = replace(parameters,"^", "=").
  do iCount = 1 to num-entries(parameters,","):
    
    if ownerEndors =  entry(1,entry(iCount,parameters,","),"=")
     then
      iownerendors = num-entries(entry(2,entry(iCount,parameters,","),"="),"|").

    if loanEndors =  entry(1,entry(iCount,parameters,","),"=") 
     then
      iloanrendors = num-entries(entry(2,entry(iCount,parameters,","),"="),"|").

    if secondLoanEndors =  entry(1,entry(iCount,parameters,","),"=") 
     then
      iscndendors = num-entries(entry(2,entry(iCount,parameters,","),"="),"|").
  end.
  do iCount = 1 to num-entries(parameters,","):
    
    if ownerEndors =  entry(1,entry(iCount,parameters,","),"=") 
     then
      parameters =  replace(parameters, entry(2,entry(iCount,parameters,","),"="), string(iownerendors)) no-error.

    if loanEndors =  entry(1,entry(iCount,parameters,","),"=") 
     then
      parameters =  replace(parameters, entry(2,entry(iCount,parameters,","),"="), string(iloanrendors)) no-error.

    if secondLoanEndors =  entry(1,entry(iCount,parameters,","),"=") 
     then
      parameters =  replace(parameters, entry(2,entry(iCount,parameters,","),"="), string(iscndendors)) no-error.
  end.
 
  parameters = replace(parameters,"secondloanRateType", "Second Loan Rate Type").
  parameters = replace(parameters,"secondLoancoverageAmount", "Second Loan Coverage Amount").
  parameters = replace(parameters,"secondLoanEndors", "No. of Second Loan's Endorsements").
  
  parameters = replace(parameters,"loanRateType", "Loan Rate Type").
  parameters = replace(parameters,"loancoverageAmount", "Loan Coverage Amount").
  parameters = replace(parameters,"loanreissuecoverageAmount", "Prior Loan Coverage Amount").
  parameters = replace(parameters,"loanEndors", "No. of Loan's Endorsements").
  
  parameters = replace(parameters,"rateType", "Rate Type").
  parameters = replace(parameters,"coverageAmount", "Coverage Amount").
  parameters = replace(parameters,"reissuecoverageAmount", "Prior Coverage Amount").
  parameters = replace(parameters,"ownerEndors", "No. of Owner's Endorsements").
  
  parameters = replace(parameters,"cardSetID", "Card SET ID").
  parameters = replace(parameters,"propertyType", "Property Type").
  parameters = replace(parameters,"effectiveDate", "Effective Date").
  parameters = replace(parameters,"manual", "Manual"). 
  parameters = replace(parameters,"caseType", "Case Type").
  parameters = replace(parameters,"Version", "Version").
  parameters = replace(parameters,"simultaneous", "Simultaneous").
  
  parameters = replace(parameters,"rate", "Rate").
  parameters = replace(parameters,"reissue", "Reissue").
  parameters = replace(parameters,"loan", "Loan").
  parameters = replace(parameters,"region", "Region").
  parameters = replace(parameters,"--", "").
  
  chAttrib = replace(replace(parameters, "," , chr(10)), "=" , " = ").
  eParameters:screen-value = chAttrib.

  hColumnHandle = brwLog:get-browse-column(3).
  
  find first ratelog no-error.
  if available ratelog 
   then
    open query brwLog for each rateLog.

  apply 'value-changed' to brwLog.
  apply 'value-changed' to cbTypeAmount.
  apply 'value-changed' to cbCaseType.
  apply 'value-changed' to brwLog.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyViewCards C-Win 
PROCEDURE emptyViewCards :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter cardsetid as integer no-undo.
define input parameter cardid    as integer no-undo.

find current ratelog no-error.
if available ratelog 
 then
  if cCardSetID = string(cardsetid) and
     cardid =  rateLog.cardid 
   then
     do:
        for each ttWCards:
          delete procedure ttWCards.hWCard no-error.
        end.
        empty temp-table ttWCards no-error.
     end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbTypeAmount cbCaseType eParameters cbCalculationOn 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bExport cbTypeAmount cbCaseType eParameters cbCalculationOn RECT-68 
         brwLog RECT-69 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportRateLogData C-Win 
PROCEDURE exportRateLogData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable handttRateLog     as handle    no-undo.
 define variable rptDir            as character no-undo.
 define variable std-in            as integer   no-undo.
 if query brwLog:num-results = 0
  then
   do:
     bExport:sensitive in frame DEFAULT-FRAME = true.
     message "There is nothing to export"
      view-as alert-box warning buttons ok.
     return.
   end.

 publish "GetReportDir" (output rptDir).
 handttRateLog = temp-table rateLog:handle.

 run exportRateLogData.p (table-handle handttRateLog,
                          "rateLog",
                          "for each rateLog ",
                          "seq,calculationFor,action,actionCalculation,calculationOn",
                          "Sequence,Calculation For,Action,Results,Calculation On",
                          rptDir,
                          "rateLog-" + cStateID + "-" + replace(string(now,"99-99-99"),"-","") +  replace(string(time,"HH:MM:SS"),":","") + ".csv",
                          true,
                          "",
                          chAttrib,
                          output rptDir,
                          output std-in) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE extractParams C-Win 
PROCEDURE extractParams :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define variable iCount as integer no-undo.
  assign
      cCardSetID   = "CardSetID" 
      cVersion     = "Version" 
      cVersionDesc = "VersionDesc" 
      cStateID     = "StateID" 
      cEffectDate  = "manualEffectiveDate"
      .

  do iCount = 1 to num-entries(parameters,","):
    case entry(1,entry(iCount,parameters,","),"^"):
    
      when cCardSetID  then
           cCardSetID    = entry(2,entry(iCount,parameters,","),"^") no-error.
      
      when cVersion then
           cVersion      = entry(2,entry(iCount,parameters,","),"^") no-error.
      
      when cVersionDesc  then
           cVersionDesc  = entry(2,entry(iCount,parameters,","),"^") no-error.
      
      when cStateID then
           cStateID      = entry(2,entry(iCount,parameters,","),"^") no-error.
      
      when cEffectDate then
           cEffectDate   = entry(2,entry(iCount,parameters,","),"^") no-error.
      
    end case.  
  end.
  if  cCardSetID   = "CardSetID"   
   then
    cCardSetID   = "".
  if  cVersion     = "Version"     
   then
    cVersion     = "".
  if  cVersionDesc = "VersionDesc" 
   then
    cVersionDesc = "".
  if  cStateID     = "StateID"     
   then
    cStateID     = "".
  if  cEffectDate  = "EffectDate"  
   then
    cEffectDate  = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE filterData C-Win 
PROCEDURE filterData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pcCalcOfAmtType   as character no-undo. 
  define input parameter pcCalcForCaseType as character no-undo. 
  define input parameter pcCalcOn          as character no-undo. 


  if pcCalcOfAmtType eq {&all} and pcCalcForCaseType eq {&all} and pcCalcOn eq {&all} 
   then
    open query brwLog for each rateLog .
  else if pcCalcForCaseType eq {&all} 
   then
    open query brwLog for each rateLog where rateLog.typeOfAmount  =  (if pcCalcOfAmtType  eq {&all} then rateLog.typeOfAmount else pcCalcOfAmtType) and  
                                             rateLog.calculationOn = (if pcCalcOn eq {&all} then rateLog.calculationOn else pcCalcOn).
  else if pcCalcOfAmtType eq {&all} 
   then
    open query brwLog for each rateLog where rateLog.calculationFor = (if pcCalcForCaseType eq {&all} then rateLog.calculationFor else pcCalcForCaseType) and 
                                             rateLog.calculationOn  = (if pcCalcOn eq {&all} then rateLog.calculationOn else pcCalcOn).
  else if pcCalcOn eq {&all} 
   then
    open query brwLog for each rateLog where rateLog.calculationFor = (if pcCalcForCaseType eq {&all} then rateLog.calculationFor else pcCalcForCaseType) and  
                                             rateLog.typeOfAmount   =  (if pcCalcOfAmtType  eq {&all} then rateLog.typeOfAmount else pcCalcOfAmtType).
  else if pcCalcOfAmtType  eq {&all} and  pcCalcOn eq {&all} 
   then
    open query brwLog for each rateLog where rateLog.typeOfAmount =  (if pcCalcOfAmtType  eq {&all} then rateLog.typeOfAmount else pcCalcOfAmtType)  .
  else if pcCalcOfAmtType eq {&all} and pcCalcForCaseType eq {&all}  
   then
    open query brwLog for each rateLog where rateLog.calculationOn = (if pcCalcOn eq {&all} then rateLog.calculationOn else pcCalcOn).
  else if pcCalcOn eq {&all}  and pcCalcForCaseType eq {&all} 
   then
    open query brwLog for each rateLog where rateLog.calculationFor = (if pcCalcForCaseType eq {&all} then rateLog.calculationFor else pcCalcForCaseType) .
  else
    open query brwLog for each rateLog where rateLog.calculationFor = (if pcCalcForCaseType eq {&all} then rateLog.calculationFor else pcCalcForCaseType) and 
                                             rateLog.typeOfAmount = (if pcCalcOfAmtType  eq {&all} then rateLog.typeOfAmount else pcCalcOfAmtType)        and  
                                             rateLog.calculationOn = (if pcCalcOn eq {&all} then rateLog.calculationOn else pcCalcOn).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
 frame {&frame-name}:height-pixels = {&window-name}:height-pixels.
 frame {&frame-name}:virtual-height-pixels = {&window-name}:height-pixels.

 /* fSearch components */
 browse {&browse-name}:width-pixels = frame {&frame-name}:width-pixels - 240.

 if {&window-name}:width-pixels > frame {&frame-name}:width-pixels 
  then
   do: 
     frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
     frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
   end.
   else
   do:
     frame {&frame-name}:virtual-width-pixels = {&window-name}:width-pixels.
     frame {&frame-name}:width-pixels = {&window-name}:width-pixels.
   end.
 
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y - 10 no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

