&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME DialogGenPDF
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS DialogGenPDF 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */


define input parameter pState   as character no-undo. 
define input parameter pResulta as character no-undo. 
define input parameter pUiInfo  as character no-undo.  

define variable chInputParamaters as character no-undo.
define variable pcheck            as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DialogGenPDF

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fFileNumber fSeller fBuyer fReference ~
fProperty fState fCounty Btn_Gen Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS fFileNumber fSeller fBuyer fReference ~
fProperty fState fCounty 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_Gen AUTO-GO 
     LABEL "Generate" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE fBuyer AS CHARACTER FORMAT "X(256)":U 
     LABEL "Buyer" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fCounty AS CHARACTER FORMAT "X(256)":U 
     LABEL "County" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fFileNumber AS CHARACTER FORMAT "X(256)":U 
     LABEL "File Number" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fProperty AS CHARACTER FORMAT "X(256)":U 
     LABEL "Property" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fReference AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reference" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fSeller AS CHARACTER FORMAT "X(256)":U 
     LABEL "Seller" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.

DEFINE VARIABLE fState AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS FILL-IN 
     SIZE 32 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DialogGenPDF
     fFileNumber AT ROW 1.38 COL 14.4 COLON-ALIGNED WIDGET-ID 2
     fSeller AT ROW 2.48 COL 14.4 COLON-ALIGNED WIDGET-ID 4
     fBuyer AT ROW 3.57 COL 14.4 COLON-ALIGNED WIDGET-ID 6
     fReference AT ROW 4.67 COL 14.4 COLON-ALIGNED WIDGET-ID 8
     fProperty AT ROW 5.76 COL 14.4 COLON-ALIGNED WIDGET-ID 10
     fState AT ROW 6.86 COL 14.4 COLON-ALIGNED WIDGET-ID 12
     fCounty AT ROW 7.95 COL 14.4 COLON-ALIGNED WIDGET-ID 14
     Btn_Gen AT ROW 9.1 COL 16.4
     Btn_Cancel AT ROW 9.1 COL 33.4
     SPACE(4.39) SKIP(0.37)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Premium Rate Quote"
         DEFAULT-BUTTON Btn_Gen CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX DialogGenPDF
   FRAME-NAME                                                           */
ASSIGN 
       FRAME DialogGenPDF:SCROLLABLE       = FALSE
       FRAME DialogGenPDF:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME DialogGenPDF
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL DialogGenPDF DialogGenPDF
ON WINDOW-CLOSE OF FRAME DialogGenPDF /* Premium Rate Quote */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Gen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Gen DialogGenPDF
ON CHOOSE OF Btn_Gen IN FRAME DialogGenPDF /* Generate */
DO:
  chInputParamaters = "FileNumber^"    + fFileNumber:screen-value + "," +
                      "Seller^"        + fSeller:screen-value + "," +
                      "Buyer^"         + fBuyer:screen-value + "," +
                      "Reference^"     + fReference:screen-value + "," +
                      "Property^"      + fProperty:screen-value + "," +
                      "State^"         + fState:screen-value + "," +
                      "County^"        + fCounty:screen-value  + "," +
                      "batchProcess^"  + "false".

  run util\rateSheet.p(input-output pcheck,
                       input pState,  
                       input pResulta,
                       input pUiInfo,
                       input chInputParamaters). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK DialogGenPDF 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI DialogGenPDF  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME DialogGenPDF.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI DialogGenPDF  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fFileNumber fSeller fBuyer fReference fProperty fState fCounty 
      WITH FRAME DialogGenPDF.
  ENABLE fFileNumber fSeller fBuyer fReference fProperty fState fCounty Btn_Gen 
         Btn_Cancel 
      WITH FRAME DialogGenPDF.
  VIEW FRAME DialogGenPDF.
  {&OPEN-BROWSERS-IN-QUERY-DialogGenPDF}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

