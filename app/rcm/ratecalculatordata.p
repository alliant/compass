&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        :  ratecalculatordata.p
    Purpose     :

    Syntax      :

    Description :


    Author(s)   : Anjly and Archana
    Created     :
    Notes       :
    Modified    :
    Date        Name     Comments   
    12/18/2019  Anubha   Modified a procedure to send region table containing
                         both region code and description as output.
    09/14/2023  SChandu  Added Commitment procedure logic for NewMexico state.
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/
{lib\std-def.i}
{lib\rcm-std-def.i}
/* ***************************  Definitions  ************************** */

/* Temp-table definitions       */
{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratetable.i}
{tt\raterule.i}
{tt\ratelog.i}
{tt\rateUI.i}
{tt\syscode.i   &tablealias=tempsyscode}
  
define temp-table endorsementData like endorsement.

{tt\ratecard.i  &tablealias=tempRateCard}
{tt\ratetable.i &tablealias=tempRateTable}
{tt\raterule.i  &tablealias=tempRateRule}

define temp-table tempRegion like region.


define input parameter ipcStateID as character no-undo.
define input parameter ipiVersion as integer   no-undo.
define output parameter opcErrMsg as character no-undo.
define output parameter oplStat   as logical   no-undo.
/* Local variable definitions  */
define variable hServer as handle no-undo.


/* Dataset definitions         */
define dataset RCUI
 for developerComments, proposedRateConfig, region, propertyType, loanRateType, ownerRateType,scndLoanRateType,endorsement
    data-relation for propertyType, loanRateType relation-fields (propertyTypecode, propertyTypecode)     nested   foreign-key-hidden        
    data-relation for propertyType, ownerRateType relation-fields (propertyTypecode, propertyTypecode)    nested   foreign-key-hidden        
    data-relation for propertyType, scndLoanRateType relation-fields (propertyTypecode, propertyTypecode) nested   foreign-key-hidden .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 6.62
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
run ratedatasrv.p persistent set hServer.
/*
subscribe to "GetComboLists"              anywhere.
subscribe to "GetPropertyType"            anywhere.
subscribe to "GetSimoComboList"           anywhere.
subscribe to "GetEndors"                  anywhere.
subscribe to "GetPriorPolicyAmountConfig" anywhere.
subscribe to "GetPriorPolicyDateConfig"   anywhere.
subscribe to "GetProposedRate"            anywhere.
subscribe to "CalculatePremium"           anywhere.
subscribe to "GetCodes"                   anywhere.
subscribe to "GetRegions"                 anywhere.
subscribe to "GetStateName"               anywhere.
     
  */
  subscribe to "GetRateCardDetailLog"          anywhere.

  run GetRateStates in hServer (input ipcStateID,
                               output table ratestate) no-error.   
                      
                      
if not can-find(first ratestate where ratestate.stateID = ipcStateID and ratestate.version = ipiVersion) then
do:
    opcErrMsg = "UI can't be initialized for selected state. Contact the System Administrator.".
    oplStat   = false.    
    return.
end.

for first ratestate where ratestate.version = ipiVersion:                       
                           

  if search(ratestate.configUserInterface) = ? 
   then
    do:
       opcErrMsg = "UI can't be initialized for selected state. Contact the System Administrator.".
       oplStat   = false.
    end.
   else
    oplStat = dataset RCUI:read-json("file", search(ratestate.configUserInterface), "empty").

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-CalculatePremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CalculatePremium Procedure 
PROCEDURE CalculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcUiInput           as character no-undo.
  define output parameter opcCalculatedPremium as character no-undo.
  define output parameter table for rateLog.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  run calculatePremiumService in hserver(input ipcUiInput,
                                         input ipcStateID,
                                         input ipiVersion,
                                         output opcCalculatedPremium,
                                         output table rateLog,
                                         output lSuccess,
                                         output cMsg
                                         ).
  if not lSuccess 
   then
    do:
      message cMsg
          view-as alert-box info buttons ok.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-checkEndorsement) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE checkEndorsement Procedure 
PROCEDURE checkEndorsement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter oplHasEndors as logical.
 oplHasEndors = can-find(first endorsement) no-error.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-CommitmentEnabled) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CommitmentEnabled Procedure 
PROCEDURE CommitmentEnabled :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output  parameter lEnable    as logical   no-undo.
  
  if lookup(ipcStateID,{&StateWithCommitments}) > 0
   then
    lEnable = true.
  else
   lEnable = false.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetComboLists) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetComboLists Procedure 
PROCEDURE GetComboLists :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define input parameter ipcRegionCode     as character no-undo. 
  define input parameter ipcPropType       as character no-undo.
  
  define output parameter opcListOwner     as character no-undo.
  define output parameter opcListLoan      as character no-undo.
  define output parameter opcListScndLoan  as character no-undo.
  
  define buffer ownerRateType    for ownerRateType.
  define buffer loanRateType     for loanRateType.
  define buffer scndLoanRateType for scndLoanRateType.
  
  for each ownerRateType where (ownerRateType.regionList = "" or 
                                can-do(ownerRateType.regionList,ipcRegionCode)) and 
                               ownerRateType.propertyTypecode = ipcPropType:
    if (ownerRateType.simo = ?) 
     then
      next.
    opcListOwner = opcListOwner + "," + ownerRateType.description + "," + ownerRateType.ownerRateTypecode .
  end.
  opcListOwner = trim({&SelectType} + "," + {&None} + "," + trim(opcListOwner, ","), ",").
  

  for each loanRateType where (loanRateType.regionList = "" or 
                               can-do(loanRateType.regionList,ipcRegionCode)) and 
                              loanRateType.propertyTypecode = ipcPropType :
    if (loanRateType.simo = ?) 
     then
      next.
    opcListLoan = opcListLoan + "," + loanRateType.description + "," + loanRateType.loanRateTypecode .
  end.
  opcListLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListLoan, ","), ",").
  

  for each scndLoanRateType where (scndLoanRateType.regionList = "" or 
                                   can-do(scndLoanRateType.regionList,ipcRegionCode)) and 
                                  scndLoanRateType.propertyTypecode = ipcPropType:
    if (scndLoanRateType.simo = ?) 
     then
      next.
    opcListScndLoan = opcListScndLoan + "," + scndLoanRateType.description + "," + scndLoanRateType.scndLoanRateTypecode .
  end.
  opcListScndLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListScndLoan, ","), ",").                                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetEndors Procedure 
PROCEDURE GetEndors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcCaseType as character no-undo.
  define input parameter ipcRegion   as character no-undo.
  define input parameter ipcPropType as character no-undo.
  define input parameter ipcRateType as character no-undo.
  
  define output parameter table for endorsementdata.

  define buffer endorsement for endorsement.
  empty temp-table endorsementdata.
  

  for each endorsement where (endorsement.endorsementType = ipcCaseType        or endorsement.endorsementType = "")
                         and (can-do(endorsement.regionList,ipcRegion)         or endorsement.regionList = "")
                         and (can-do(endorsement.propertyTypeList,ipcPropType) or endorsement.propertyTypeList = "")
                         and (can-do(endorsement.rateTypeList,(if ipcRateType = "none" then endorsement.rateTypeList else ipcRateType)) or endorsement.rateTypeList = "") :
    create endorsementdata.
    buffer-copy endorsement to endorsementdata.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPriorPolicyAmountConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPriorPolicyAmountConfig Procedure 
PROCEDURE GetPriorPolicyAmountConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter ipcRegion     as character no-undo.
 define input  parameter ipcPropType   as character no-undo.
 define input  parameter ipcCaseType   as character no-undo.
 define input  parameter ipcRateType   as character no-undo.
 
 define output parameter oplShowPrior  as logical   no-undo.
  
 if ipcCaseType = {&Owners} 
  then
   do:
     find first ownerRateType 
      where (ownerRateType.regionList = "" or can-do(ownerRateType.regionList,ipcRegion))
       and ownerRateType.propertyTypecode = ipcPropType 
       and ownerRateType.ownerRateTypeCode = ipcRateType no-error.
     if available ownerRateType 
      then
       oplShowPrior = ownerRateType.priorPolicyAmountFlag.    
   end.

 if ipcCaseType = {&Lenders} 
  then
   do:
     find first loanRateType 
      where (loanRateType.regionList = "" or can-do(loanRateType.regionList,ipcRegion))
       and loanRateType.propertyTypecode = ipcPropType 
       and loanRateType.LoanRateTypeCode = ipcRateType no-error.
     if available loanRateType 
      then
       oplShowPrior = loanRateType.priorPolicyAmountFlag.
   end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPriorPolicyDateConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPriorPolicyDateConfig Procedure 
PROCEDURE GetPriorPolicyDateConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
  define input  parameter ipcRegion         as character no-undo.
  define input  parameter ipcPropType       as character no-undo.
  define input  parameter ipcCaseTypeCode   as character no-undo.
  define input  parameter ipcRateType       as character no-undo.
 
  define output parameter loShowPriorDate   as logical   no-undo.
  
  if ipcCaseTypeCode = {&Owners} 
   then
    do:
      find first ownerRateType 
       where (ownerRateType.regionList = "" or can-do(ownerRateType.regionList, ipcRegion ))
         and ownerRateType.propertyTypecode = ipcPropType 
         and ownerRateType.ownerRateTypeCode = ipcRateType no-error.
      if available ownerRateType 
       then
        loShowPriorDate = ownerRateType.priorPolicyDateFlag.
    end.   
  
  if ipcCaseTypeCode = {&Lenders} 
   then
    do:
      find first loanRateType 
       where (loanRateType.regionList = "" or can-do(loanRateType.regionList, ipcRegion))
         and loanRateType.propertyTypecode = ipcPropType 
         and loanRateType.LoanRateTypeCode = ipcRateType no-error.
      if available loanRateType 
       then
        loShowPriorDate = loanRateType.priorPolicyDateFlag.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetPropertyType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetPropertyType Procedure 
PROCEDURE GetPropertyType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  define input parameter ipcRegion        as character no-undo.
 
  define output parameter opcPropertyType as character no-undo.
  
  define buffer propertyType for propertyType.

  for each propertyType where propertyType.regionList = "" 
                           or can-do(propertyType.regionList,ipcRegion) :
    opcPropertyType = opcPropertyType + "," + propertyType.description + "," + propertyType.propertyTypecode .
  end.
  
  opcPropertyType = trim(opcPropertyType, ",").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetProposedRate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetProposedRate Procedure 
PROCEDURE GetProposedRate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcEndorsementCode  as character no-undo.
 
  define output parameter opdeProposedRate    as decimal   no-undo. 
  
  find first endorsement where endorsement.endorsementCode = ipcEndorsementCode.
  if available endorsement 
   then
    opdeProposedRate = endorsement.proposedRate.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetRateCardDetailLog) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRateCardDetailLog Procedure 
PROCEDURE GetRateCardDetailLog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiCardSetID  as integer no-undo.
  define input parameter ipiCardID     as integer no-undo.
  
  define output parameter table for tempRateCard.
  define output parameter table for tempRateTable.
  define output parameter table for tempRateRule.
  
  define buffer tempRateCard  for tempRateCard.
  define buffer tempRateTable for tempRateTable.
  define buffer tempRateRule  for tempRateRule.
  
  empty temp-table  tempRateCard .   
  empty temp-table  tempRateTable.      
  empty temp-table  tempRateRule .     
  
  run GetRateCardDetail in hServer(input ipiCardSetID,  
                                   input ipiCardID,
                                   output table tempRateCard, 
                                   output table tempRateTable,                               
                                   output table tempRateRule).                                                                                             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetRegions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRegions Procedure 
PROCEDURE GetRegions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for tempRegion.
  
  define buffer region for region.
  
  empty temp-table tempRegion.
  
  for each region: 
    create tempRegion.
    buffer-copy region to tempRegion.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetSimoComboList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetSimoComboList Procedure 
PROCEDURE GetSimoComboList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcRegionCode   as character no-undo.
  define input parameter  ipcPropType     as character no-undo.
  define input parameter  iplSimo         as logical   no-undo.
  
  define output parameter opcListOwner    as character no-undo.
  define output parameter opcListLoan     as character no-undo.
  define output parameter opcListScndLoan as character no-undo.
  
  define buffer ownerRateType    for ownerRateType.
  define buffer loanRateType     for loanRateType.
  define buffer scndLoanRateType for scndLoanRateType.
   
  for each ownerRateType where (ownerRateType.regionList = "" or can-do(ownerRateType.regionList,ipcRegionCode))
                         and ownerRateType.propertyTypecode = ipcPropType
                         and (ownerRateType.simo = iplSimo or ownerRateType.simo = ?) :
    opcListOwner = opcListOwner + "," + ownerRateType.description + "," + ownerRateType.ownerRateTypecode .                                                                                                              
  end.
  opcListOwner = trim({&SelectType} + "," + {&None} + "," + trim(opcListOwner, ","), ",").
  
  for each loanRateType where (loanRateType.regionList = "" or can-do(loanRateType.regionList,ipcRegionCode))
                          and loanRateType.propertyTypecode = ipcPropType
                          and (loanRateType.simo = iplSimo or loanRateType.simo = ?):
    
    opcListLoan = opcListLoan + "," + loanRateType.description + "," + loanRateType.loanRateTypecode .                                                                                                        
  end.
  opcListLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListLoan, ","), ",").
  
  for each scndLoanRateType where (scndLoanRateType.regionList = "" or can-do(scndLoanRateType.regionList,ipcRegionCode))
                              and scndLoanRateType.propertyTypecode = ipcPropType
                              and (scndLoanRateType.simo = iplSimo or scndLoanRateType.simo = ?):   
    opcListScndLoan = opcListScndLoan + "," + scndLoanRateType.description + "," + scndLoanRateType.scndLoanRateTypecode .   
  end.
  opcListScndLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListScndLoan, ","), ",").   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetStateName) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetStateName Procedure 
PROCEDURE GetStateName :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter opcStateName  as character no-undo.
  
  run GetStateName in hserver(input ipcStateID,
                              output opcStateName).
                              
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

