&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        :  coloradocalculatordata.p
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Anjly and Archana
    Created     :
    Notes       :
    Modified    :
    Date        Name     Comments   
    12/18/2019  Anubha   Modified a procedure to send region table containing
                         both region code and description as output.
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/
{lib\std-def.i}
{lib\rcm-std-def.i}
/* ***************************  Definitions  ************************** */
/* Parameter definitions         */
define input  parameter ipcUIConfigFile as character no-undo.
define output parameter oplError        as logical   no-undo.
define output parameter opcErrorMsg     as character no-undo.

/* Temp-table definitions       */
{tt\ratestate.i}
{tt\ratecard.i}
{tt\ratetable.i}
{tt\raterule.i}
{tt\ratelog.i}
{tt\rateUI.i}
{tt\syscode.i   &tablealias=tempsyscode}
  
define temp-table endorsementData like endorsement.

{tt\ratecard.i  &tablealias=tempRateCard}
{tt\ratetable.i &tablealias=tempRateTable}
{tt\raterule.i  &tablealias=tempRateRule}

define temp-table tempRegion like region.

/* Local variable definitions  */
define variable hServer as handle no-undo.

/* Dataset definitions         */
define dataset RCUI
 for developerComments, proposedRateConfig, region, propertyType, loanRateType, ownerRateType,scndLoanRateType,endorsement
    data-relation for propertyType, loanRateType relation-fields (propertyTypecode, propertyTypecode)     nested   foreign-key-hidden        
    data-relation for propertyType, ownerRateType relation-fields (propertyTypecode, propertyTypecode)    nested   foreign-key-hidden        
    data-relation for propertyType, scndLoanRateType relation-fields (propertyTypecode, propertyTypecode) nested   foreign-key-hidden .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 6.62
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */
if search(ipcUIConfigFile) = ? 
 then
  do:
    opcErrorMsg = "UI can't be initialized for selected state. Contact the System Administrator.".
    oplError = true.
    apply 'close' to this-procedure.
    return.
  end.
std-lo = dataset RCUI:read-json("file", search(ipcUIConfigFile), "empty").

run ratedatasrv.p persistent set hServer.

subscribe to "GetCOComboLists"              anywhere.
subscribe to "GetCOPropertyType"            anywhere.
subscribe to "GetCOSimoComboList"           anywhere.
subscribe to "GetCOEndors"                  anywhere.
subscribe to "GetCOPriorPolicyAmountConfig" anywhere.
subscribe to "GetCOPriorPolicyDateConfig"   anywhere.
subscribe to "GetCOProposedRate"            anywhere.
subscribe to "COCalculatePremium"           anywhere.
subscribe to "GetCORateCardDetail"          anywhere.
subscribe to "GetCOCodes"                   anywhere.
subscribe to "GetCORegions"                 anywhere.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-COCalculatePremium) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE COCalculatePremium Procedure 
PROCEDURE COCalculatePremium :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcUiInput           as character no-undo.
  define input  parameter ipcStateID           as character no-undo.
  define input  parameter ipiVersion           as integer   no-undo.
  define output parameter opcCalculatedPremium as character no-undo.
  define output parameter table for rateLog.
  
  define variable lSuccess as logical   no-undo.
  define variable cMsg     as character no-undo.
  
  run calculatePremiumService in hserver(input ipcUiInput,
                                         input ipcStateID,
                                         input ipiVersion,
                                         output opcCalculatedPremium,
                                         output table rateLog,
                                         output lSuccess,
                                         output cMsg
                                         ).
  if not lSuccess 
   then
    do:
      message cMsg
          view-as alert-box info buttons ok.
      return error.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOCodes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOCodes Procedure 
PROCEDURE GetCOCodes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter ipcCodeType as character no-undo.
 define output parameter table for tempsyscode.

 empty temp-table tempsyscode.

 run GetCodesService in hServer(input ipcCodeType,
                                output table tempsyscode
                                ).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOComboLists) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOComboLists Procedure 
PROCEDURE GetCOComboLists :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcRegionCode     as character no-undo. 
  define input parameter ipcPropType       as character no-undo.
  
  define output parameter opcListOwner     as character no-undo.
  define output parameter opcListLoan      as character no-undo.
  define output parameter opcListScndLoan  as character no-undo.
  
  define buffer ownerRateType    for ownerRateType.
  define buffer loanRateType     for loanRateType.
  define buffer scndLoanRateType for scndLoanRateType.
  
  for each ownerRateType where (ownerRateType.regionList = "" or 
                                can-do(ownerRateType.regionList,ipcRegionCode)) and 
                               ownerRateType.propertyTypecode = ipcPropType:
    if (ownerRateType.simo = ?) 
     then
      next.
    opcListOwner = opcListOwner + "," + ownerRateType.description + "," + ownerRateType.ownerRateTypecode .
  end.
  opcListOwner = trim({&SelectType} + "," + {&None} + "," + trim(opcListOwner, ","), ",").
  

  for each loanRateType where (loanRateType.regionList = "" or 
                               can-do(loanRateType.regionList,ipcRegionCode)) and 
                              loanRateType.propertyTypecode = ipcPropType :
    if (loanRateType.simo = ?) 
     then
      next.
    opcListLoan = opcListLoan + "," + loanRateType.description + "," + loanRateType.loanRateTypecode .
  end.
  opcListLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListLoan, ","), ",").
  

  for each scndLoanRateType where (scndLoanRateType.regionList = "" or 
                                   can-do(scndLoanRateType.regionList,ipcRegionCode)) and 
                                  scndLoanRateType.propertyTypecode = ipcPropType:
    if (scndLoanRateType.simo = ?) 
     then
      next.
    opcListScndLoan = opcListScndLoan + "," + scndLoanRateType.description + "," + scndLoanRateType.scndLoanRateTypecode .
  end.
  opcListScndLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListScndLoan, ","), ",").                                   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOEndors) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOEndors Procedure 
PROCEDURE GetCOEndors :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcCaseType as character no-undo.
  define input parameter ipcRegion   as character no-undo.
  define input parameter ipcPropType as character no-undo.
  define input parameter ipcRateType as character no-undo.
  
  define output parameter table for endorsementdata.

  define buffer endorsement for endorsement.
  empty temp-table endorsementdata.

  for each endorsement where (endorsement.endorsementType = ipcCaseType        or endorsement.endorsementType = "")
                         and (can-do(endorsement.regionList,ipcRegion)         or endorsement.regionList = "")
                         and (can-do(endorsement.propertyTypeList,ipcPropType) or endorsement.propertyTypeList = "")
                         and (can-do(endorsement.rateTypeList,(if ipcRateType = "none" then endorsement.rateTypeList else ipcRateType)) or endorsement.rateTypeList = "") :
    create endorsementdata.
    buffer-copy endorsement to endorsementdata.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOPriorPolicyAmountConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOPriorPolicyAmountConfig Procedure 
PROCEDURE GetCOPriorPolicyAmountConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input  parameter ipcRegion     as character no-undo.
 define input  parameter ipcPropType   as character no-undo.
 define input  parameter ipcCaseType   as character no-undo.
 define input  parameter ipcRateType   as character no-undo.
 define output parameter oplShowPrior  as logical   no-undo.
 
 if ipcCaseType = {&Owners} 
  then
   do:
     find first ownerRateType 
      where (ownerRateType.regionList = "" or can-do(ownerRateType.regionList,ipcRegion))
       and ownerRateType.propertyTypecode = ipcPropType 
       and ownerRateType.ownerRateTypeCode = ipcRateType no-error.
     if available ownerRateType 
      then
       oplShowPrior = ownerRateType.priorPolicyAmountFlag.    
   end.

 if ipcCaseType = {&Lenders} 
  then
   do:
     find first loanRateType 
      where (loanRateType.regionList = "" or can-do(loanRateType.regionList,ipcRegion))
       and loanRateType.propertyTypecode = ipcPropType 
       and loanRateType.LoanRateTypeCode = ipcRateType no-error.
     if available loanRateType 
      then
       oplShowPrior = loanRateType.priorPolicyAmountFlag.
   end.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOPriorPolicyDateConfig) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOPriorPolicyDateConfig Procedure 
PROCEDURE GetCOPriorPolicyDateConfig :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  define input  parameter ipcRegion         as character no-undo.
  define input  parameter ipcPropType       as character no-undo.
  define input  parameter ipcCaseTypeCode   as character no-undo.
  define input  parameter ipcRateType       as character no-undo.
  define output parameter loShowPriorDate   as logical   no-undo.
  
  if ipcCaseTypeCode = {&Owners} 
   then
    do:
      find first ownerRateType 
       where (ownerRateType.regionList = "" or can-do(ownerRateType.regionList, ipcRegion ))
         and ownerRateType.propertyTypecode = ipcPropType 
         and ownerRateType.ownerRateTypeCode = ipcRateType no-error.
      if available ownerRateType 
       then
        loShowPriorDate = ownerRateType.priorPolicyDateFlag.
    end.   
  
  if ipcCaseTypeCode = {&Lenders} 
   then
    do:
      find first loanRateType 
       where (loanRateType.regionList = "" or can-do(loanRateType.regionList, ipcRegion))
         and loanRateType.propertyTypecode = ipcPropType 
         and loanRateType.LoanRateTypeCode = ipcRateType no-error.
      if available loanRateType 
       then
        loShowPriorDate = loanRateType.priorPolicyDateFlag.
    end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOPropertyType) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOPropertyType Procedure 
PROCEDURE GetCOPropertyType :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipcRegion        as character no-undo.
  define output parameter opcPropertyType as character no-undo.
  
  define buffer propertyType for propertyType.

  for each propertyType where propertyType.regionList = "" 
                           or can-do(propertyType.regionList,ipcRegion) :
    opcPropertyType = opcPropertyType + "," + propertyType.description + "," + propertyType.propertyTypecode .
  end.
  
  opcPropertyType = trim(opcPropertyType, ",").

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOProposedRate) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOProposedRate Procedure 
PROCEDURE GetCOProposedRate :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input  parameter ipcEndorsementCode  as character no-undo.
  define output parameter opdeProposedRate    as decimal   no-undo. 
  
  find first endorsement where endorsement.endorsementCode = ipcEndorsementCode.
  if available endorsement 
   then
    opdeProposedRate = endorsement.proposedRate.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCORateCardDetail) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCORateCardDetail Procedure 
PROCEDURE GetCORateCardDetail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter ipiCardSetID  as integer no-undo.
  define input parameter ipiCardID     as integer no-undo.
  
  define output parameter table for tempRateCard.
  define output parameter table for tempRateTable.
  define output parameter table for tempRateRule.
  
  define buffer tempRateCard  for tempRateCard.
  define buffer tempRateTable for tempRateTable.
  define buffer tempRateRule  for tempRateRule.
  
  empty temp-table  tempRateCard .   
  empty temp-table  tempRateTable.      
  empty temp-table  tempRateRule .     
  
  run GetRateCardDetail in hServer(input ipiCardSetID,  
                                   input ipiCardID,
                                   output table tempRateCard, 
                                   output table tempRateTable,                               
                                   output table tempRateRule).                                                                                             
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCORegions) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCORegions Procedure 
PROCEDURE GetCORegions :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define output parameter table for tempRegion.
  
  define buffer region for region.
  
  empty temp-table tempRegion.
  
  for each region: 
    create tempRegion.
    buffer-copy region to tempRegion.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetCOSimoComboList) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetCOSimoComboList Procedure 
PROCEDURE GetCOSimoComboList :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter  ipcRegionCode   as character no-undo.
  define input parameter  ipcPropType     as character no-undo.
  define input parameter  iplSimo         as logical   no-undo.
  
  define output parameter opcListOwner    as character no-undo.
  define output parameter opcListLoan     as character no-undo.
  define output parameter opcListScndLoan as character no-undo.
  
  define buffer ownerRateType    for ownerRateType.
  define buffer loanRateType     for loanRateType.
  define buffer scndLoanRateType for scndLoanRateType.
   
  for each ownerRateType where (ownerRateType.regionList = "" or can-do(ownerRateType.regionList,ipcRegionCode))
                         and ownerRateType.propertyTypecode = ipcPropType
                         and (ownerRateType.simo = iplSimo or ownerRateType.simo = ?) :
    opcListOwner = opcListOwner + "," + ownerRateType.description + "," + ownerRateType.ownerRateTypecode .                                                                                                              
  end.
  opcListOwner = trim({&SelectType} + "," + {&None} + "," + trim(opcListOwner, ","), ",").
  
  for each loanRateType where (loanRateType.regionList = "" or can-do(loanRateType.regionList,ipcRegionCode))
                          and loanRateType.propertyTypecode = ipcPropType
                          and (loanRateType.simo = iplSimo or loanRateType.simo = ?):
    
    opcListLoan = opcListLoan + "," + loanRateType.description + "," + loanRateType.loanRateTypecode .                                                                                                        
  end.
  opcListLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListLoan, ","), ",").
  
  for each scndLoanRateType where (scndLoanRateType.regionList = "" or can-do(scndLoanRateType.regionList,ipcRegionCode))
                              and scndLoanRateType.propertyTypecode = ipcPropType
                              and (scndLoanRateType.simo = iplSimo or scndLoanRateType.simo = ?):   
    opcListScndLoan = opcListScndLoan + "," + scndLoanRateType.description + "," + scndLoanRateType.scndLoanRateTypecode .   
  end.
  opcListScndLoan = trim({&SelectType} + "," + {&None} + "," + trim(opcListScndLoan, ","), ",").   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

