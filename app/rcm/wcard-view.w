&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        :  wcard-view.w
    Purpose     :

    Syntax      :
    Description :

    Author(s)   : Archana Gupta
    Created     :
    Notes       :
    @Modified    :
    Date        Name    Comments
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/
{lib\std-def.i}
{lib\brw-multi-def.i}
{lib\std-def.i}
{lib\winlaunch.i}

/* Temp-table definitions                                                */
{tt\ratecard.i}
{tt\rateTable.i}
{tt\raterule.i}

{tt\ratecard.i  &tablealias=tempRateCard}

/* Parameter definitions                                                */
define input parameter pCardSetID   as integer   no-undo.
define input parameter pVersion     as integer   no-undo.
define input parameter pVersionDesc as character no-undo.
define input parameter pStateID     as character no-undo.
define input parameter pEffDate     as character  no-undo.
define input parameter pCardID      as integer   no-undo.

/* Local variable definitions                                           */
define variable pAttrIDList         as character no-undo.
define variable pRefCardID          as integer   no-undo.                                   
define variable hRateCard           as handle    no-undo.
define variable fCardID             as integer   no-undo.
define variable activeCardSetID     as integer   no-undo.  
define variable cAttrList           as character no-undo.
define variable cAttributeList      as character no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brwRule

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES raterule ratetable

/* Definitions for BROWSE brwRule                                       */
&Scoped-define FIELDS-IN-QUERY-brwRule raterule.active raterule.attrid raterule.seq raterule.ruleName if (raterule.ruleName = "cardRange" or raterule.ruleName = "cardRules") then raterule.refCardName else string(raterule.ruleValue) @ raterule.refCardName if (raterule.refCardName ne "" and (raterule.refAttrName = ?)) then "" else (if raterule.refUseParentAttr = true then "Use Parent" else raterule.refAttrName) @ raterule.refAttrName raterule.description   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwRule   
&Scoped-define SELF-NAME brwRule
&Scoped-define QUERY-STRING-brwRule for each raterule where raterule.cardid = pcardid by rateRule.attrid by rateRule.seq
&Scoped-define OPEN-QUERY-brwRule open query {&SELF-NAME} for each raterule where raterule.cardid = pcardid by rateRule.attrid by rateRule.seq .
&Scoped-define TABLES-IN-QUERY-brwRule raterule
&Scoped-define FIRST-TABLE-IN-QUERY-brwRule raterule


/* Definitions for BROWSE brwTable                                      */
&Scoped-define FIELDS-IN-QUERY-brwTable ratetable.stack ratetable.minAmount ratetable.maxAmount ratetable.roundTo if ratetable.calcType = "R" then "Rate" else "Fixed" @ ratetable.calcType "Calculation" if ratetable.calcType = "R" then ratetable.rate else ratetable.fixed @ ratetable.fixed "Value" ratetable.per "Per" ratetable.comments "Comments"   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwTable   
&Scoped-define SELF-NAME brwTable
&Scoped-define QUERY-STRING-brwTable for each ratetable
&Scoped-define OPEN-QUERY-brwTable open query {&SELF-NAME} for each ratetable .
&Scoped-define TABLES-IN-QUERY-brwTable ratetable
&Scoped-define FIRST-TABLE-IN-QUERY-brwTable ratetable


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brwRule}~
    ~{&OPEN-QUERY-brwTable}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bPdf cbShowAttrib brwRule brwTable 
&Scoped-Define DISPLAYED-OBJECTS cbCardName cbRegion eDescription eComments ~
cbShowAttrib 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetAttribList C-Win 
FUNCTION GetAttribList returns character
  (piCardID as integer) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bPdf  NO-FOCUS
     LABEL "PDF" 
     SIZE 7.2 BY 1.71 TOOLTIP "Print Rate Card".

DEFINE VARIABLE cbShowAttrib AS CHARACTER FORMAT "X(256)":U 
     LABEL "Show Attribute" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     DROP-DOWN-LIST
     SIZE 29 BY 1 NO-UNDO.

DEFINE VARIABLE eComments AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 116.8 BY 1.67 NO-UNDO.

DEFINE VARIABLE eDescription AS CHARACTER 
     VIEW-AS EDITOR
     SIZE 116.8 BY 1.67 NO-UNDO.

DEFINE VARIABLE cbCardName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Card Name" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 73.4 BY 1 NO-UNDO.

DEFINE VARIABLE cbRegion AS CHARACTER FORMAT "X(256)":U 
     LABEL "Region" 
     VIEW-AS FILL-IN NATIVE 
     SIZE 29.4 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwRule FOR 
      raterule SCROLLING.

DEFINE QUERY brwTable FOR 
      ratetable SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwRule C-Win _FREEFORM
  QUERY brwRule DISPLAY
      raterule.active        column-label "Active"             format "y/n" width 7  view-as toggle-box
raterule.attrid            label "Attribute"                   format "x(20)"      width 19
raterule.seq               label "Seq"                         format "zz9"        width 20
raterule.ruleName          label "Rule"                        format "x(20)"      width 20
if (raterule.ruleName = "cardRange" or raterule.ruleName = "cardRules") then raterule.refCardName else string(raterule.ruleValue) @ raterule.refCardName label "Value" format "x(20)" width 20
if (raterule.refCardName ne "" and (raterule.refAttrName = ?)) then "" else (if raterule.refUseParentAttr = true then "Use Parent" else raterule.refAttrName) @ raterule.refAttrName      label "Reference Attribute"  width 20 format "x(45)"
raterule.description       label "Description"                 format "x(200)"     width 39
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 180.6 BY 7.91 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwTable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwTable C-Win _FREEFORM
  QUERY brwTable DISPLAY
      ratetable.stack                                                     column-label "Stack with!Previous Row"    width 14 view-as toggle-box
ratetable.minAmount                                                       column-label "Coverage!Minimum"           width 28 format ">>>,>>>,>>>,>>9.99" 
ratetable.maxAmount                                                       column-label "Coverage!Maximum"           width 28 format ">>>,>>>,>>>,>>9.99" 
ratetable.roundTo                                                         column-label "Round up!to next"           width 18 format "zzz,zzz" 
if ratetable.calcType = "R" then "Rate" else "Fixed" @ ratetable.calcType label        "Calculation"                width 16 
if ratetable.calcType = "R" then ratetable.rate else ratetable.fixed @ ratetable.fixed label        "Value"         width 16 format "zz,zz9.99999"
ratetable.per                                                             label        "Per"                        width 13 format "zzz,zzz" 
ratetable.comments  label        "Comments"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 180.6 BY 8.57 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     cbCardName AT ROW 1.33 COL 13.4 COLON-ALIGNED WIDGET-ID 322
     cbRegion AT ROW 1.33 COL 100.6 COLON-ALIGNED WIDGET-ID 324
     bPdf AT ROW 1.43 COL 182 WIDGET-ID 310 NO-TAB-STOP 
     eDescription AT ROW 2.52 COL 15.2 NO-LABEL WIDGET-ID 262
     eComments AT ROW 4.38 COL 15.2 NO-LABEL WIDGET-ID 290
     cbShowAttrib AT ROW 6.48 COL 41.6 COLON-ALIGNED WIDGET-ID 320
     brwRule AT ROW 7.67 COL 8.6 WIDGET-ID 600
     brwTable AT ROW 17.19 COL 8.6 WIDGET-ID 400
     "Description:" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 2.67 COL 3.6 WIDGET-ID 264
     "Range" VIEW-AS TEXT
          SIZE 8.2 BY .62 AT ROW 16.19 COL 8.8 WIDGET-ID 200
          FONT 6
     "Comments:" VIEW-AS TEXT
          SIZE 10.2 BY .62 AT ROW 4.52 COL 4.4 WIDGET-ID 292
     "Rules" VIEW-AS TEXT
          SIZE 7 BY .62 AT ROW 6.67 COL 9 WIDGET-ID 204
          FONT 6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.05
         SIZE 190.8 BY 25.19 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Rate Card"
         HEIGHT             = 25.24
         WIDTH              = 190.8
         MAX-HEIGHT         = 32.57
         MAX-WIDTH          = 273.2
         VIRTUAL-HEIGHT     = 32.57
         VIRTUAL-WIDTH      = 273.2
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brwRule cbShowAttrib DEFAULT-FRAME */
/* BROWSE-TAB brwTable brwRule DEFAULT-FRAME */
ASSIGN 
       brwRule:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwRule:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

ASSIGN 
       brwTable:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE
       brwTable:COLUMN-MOVABLE IN FRAME DEFAULT-FRAME         = TRUE.

/* SETTINGS FOR FILL-IN cbCardName IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN cbRegion IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR eComments IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR eDescription IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwRule
/* Query rebuild information for BROWSE brwRule
     _START_FREEFORM
open query {&SELF-NAME} for each raterule where raterule.cardid = pcardid by rateRule.attrid by rateRule.seq .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwRule */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwTable
/* Query rebuild information for BROWSE brwTable
     _START_FREEFORM
open query {&SELF-NAME} for each ratetable .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwTable */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Rate Card */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Rate Card */
DO:
    run EmptyTempTable.
    /* This event will close the window and terminate the procedure.  */

    apply "CLOSE":U to this-procedure.
    return no-apply.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Rate Card */
DO:
  run windowresized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bPdf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bPdf C-Win
ON CHOOSE OF bPdf IN FRAME DEFAULT-FRAME /* PDF */
DO:
  run rateCardPdf.p(input pCardID,
                    input pStateID,
                    input pVersion,
                    input pVersionDesc,
                    input if pEffDate = "" then ? else datetime(pEffDate), 
                    input table RateCard,                        
                    input table Ratetable,                                                     
                    input table RateRule) no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRule
&Scoped-define SELF-NAME brwRule
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRule C-Win
ON LEFT-MOUSE-UP OF brwRule IN FRAME DEFAULT-FRAME
DO:
  find current raterule no-error.
  if available raterule then
    brwRule:tooltip = raterule.comment.
  else 
    brwRule:tooltip = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwRule C-Win
ON ROW-DISPLAY OF brwRule IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwTable
&Scoped-define SELF-NAME brwTable
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwTable C-Win
ON ROW-DISPLAY OF brwTable IN FRAME DEFAULT-FRAME
DO:
  {lib/brw-rowDisplay-multi.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbShowAttrib
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbShowAttrib C-Win
ON VALUE-CHANGED OF cbShowAttrib IN FRAME DEFAULT-FRAME /* Show Attribute */
DO:
  if cbShowAttrib:screen-value = "Show All" then
    open query brwRule for each rateRule where raterule.cardid = pCardID by rateRule.attrid by rateRule.seq.
  else
    open query brwRule for each rateRule where raterule.cardid = pCardID and (rateRule.attrid = (if cbShowAttrib:screen-value = "?" or cbShowAttrib:screen-value = ? then "" else cbShowAttrib:screen-value)
      or rateRule.attrid = "ALL") by rateRule.attrid by rateRule.seq.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwRule
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
{lib/win-main.i}
{lib/brw-main-multi.i &browse-list="brwTable,brwRule"}
subscribe to "CloseDetailWindow" anywhere.
do with frame {&frame-name}:
end.

run EmptyTempTable no-error.
 publish "GetCurrentValue" ("RateStateID",output pStateID).
if pStateID = "" then
   publish "GetRateCardDetail" (input pCardSetID,
                                input pCardID,
                                output table rateCard,
                                output table ratetable,
                                output table rateRule). 
else
 publish "GetRateCardDetailLog" (input pCardSetID,
                                               input pCardID,
                                               output table rateCard,
                                               output table ratetable,
                                               output table rateRule).

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.


/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */

bPdf:load-image ("images/pdf.bmp")  in frame default-frame no-error.
bPdf:load-image-insensitive("images/pdf-i.bmp")  in frame default-frame no-error.

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   
  RUN enable_UI.


  find first ratecard no-error.
  if available ratecard
   then
    do :
      assign
        cbCardName:screen-value in frame DEFAULT-FRAME   = ratecard.cardName
        cbRegion:screen-value in frame DEFAULT-FRAME     = ratecard.region
        eDescription:screen-value in frame DEFAULT-FRAME = ratecard.description
        . 
    end.

  run refreshAttrDrpDn in this-procedure no-error.
  cbShowAttrib:screen-value in frame default-frame = "Show All".
  open query brwTable    for each ratetable .

  apply 'value-changed' to cbShowAttrib in frame DEFAULT-FRAME.
 
  status default "Card ID: " + string(pCardID) in window {&window-name}.
  status input "Card ID: " + string(pCardID) in window {&window-name}.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyTempTable C-Win 
PROCEDURE emptyTempTable :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
empty temp-table ratecard no-error.
empty temp-table ratetable no-error.
empty temp-table raterule no-error.
                    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cbCardName cbRegion eDescription eComments cbShowAttrib 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bPdf cbShowAttrib brwRule brwTable 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE refreshAttrDrpDn C-Win 
PROCEDURE refreshAttrDrpDn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  cAttributeList = "".
 
  do with frame {&frame-name}:
  end.
 
  cAttributeList = getAttribList(pCardID).
 
  if cAttributeList <> ""  then
      cbShowAttrib:sensitive = true.
  else
      cbShowAttrib:sensitive = false.

  cbShowAttrib:list-items   = trim("Show All," + cAttributeList , ",").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowresized C-Win 
PROCEDURE windowresized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame default-frame:width-pixels = c-Win:width-pixels.
 frame default-frame:virtual-width-pixels = c-Win:width-pixels.
 frame default-frame:height-pixels = c-Win:height-pixels.
 frame default-frame:virtual-height-pixels = c-Win:height-pixels.

 browse brwRule:width-pixels = frame default-frame:width-pixels -  51. 
 browse brwTable:width-pixels = frame default-frame:width-pixels - 51.

 if {&window-name}:width-pixels > frame default-frame:width-pixels 
  then
   do: 
         frame default-frame:width-pixels = {&window-name}:width-pixels.
         frame default-frame:virtual-width-pixels = {&window-name}:width-pixels.                
   end.
 else
   do:
         frame default-frame:virtual-width-pixels = {&window-name}:width-pixels.
         frame default-frame:width-pixels = {&window-name}:width-pixels.
   end.

 browse brwTable:height-pixels = frame default-frame:height-pixels - 351. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetAttribList C-Win 
FUNCTION GetAttribList returns character
  (piCardID as integer):

define buffer buffRateRule for rateRule.
define variable chAttrList as character no-undo.
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 for each buffRateRule 
   where  buffRateRule.cardID = piCardID 
     and  buffRateRule.attrid <> "ALL" 
     and  buffRateRule.attrid <> "?" 
     and  buffRateRule.attrid <> ? 
     and  buffRateRule.attrid <> "" 
     break by buffRateRule.attrid: 
 
   if first-of(buffRateRule.attrid) then
     chAttrList = chAttrList + "," + buffRateRule.attrid.
 end.
 
 chAttrList = trim(chAttrList,",").    
 
 if can-find(first buffRateRule where buffRateRule.cardID = piCardID and buffRateRule.attrid = "") then
   chAttrList = chAttrList + "," .
 
 return chAttrList.

end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

