&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wqur.w
   main QUR window
   Rahul Sharma 11.01.2017
 */
 /*------------------------------------------------------------------------
    File        :   wqar.w
    Modification:
    Date          Name      Description
    11/01/2017    RS        Create New
    01/05/2018    RS        Changed to save General Observations
    01/05/2018    RS        Changed to enable inspect option
    01/05/2018    RS        Removed Questionnaire report and added its content
                            to internal review report.    
  
  ----------------------------------------------------------------------*/

create widget-pool.
{tt/doc.i &tableAlias="localqurFileInfo"}
{tt/qarauditquestion.i}
{tt/qarrecentaudit.i}
{tt/qarauditfinding.i}   /* finding */
{tt/qarauditbp.i}        /* bestPractice */
{tt/qarauditaction.i}    /* action */
{tt/qaraudit.i}          /* audit     */
{tt/qaraccount.i}        /* account */
{tt/openwindow.i}

def var pAgentFileNumbers     as char no-undo.
def var pAgentFileIDs         as char no-undo.
def var activeAgentFileID     as int  no-undo.
def var activeAgentFileNumber as char no-undo.
def var activeAuditID         as int  no-undo .
define variable tAutoSave     as logical   no-undo.
def var tclicksave            as logical initial true.

/* handles to auxiliary windows */
def var wUnanswered as handle.
def var wAnswered   as handle.
def var wScoring    as handle.
def var wFindings   as handle.
def var wEscrow     as handle.
def var wBkgNoc     as handle.
def var wBkgAuditor as handle.
def var wBkgAgent   as handle.
def var wReferences as handle no-undo.
def var hDocWindow  as handle no-undo.
def var hAgent      as handle no-undo.
def var wCalculator as handle no-undo.
def var wCalendar   as handle no-undo.
def var wSection    as handle extent 11 no-undo.
def var hFindbp     as handle extent 200 no-undo .
def var tLockOn     as logical no-undo init false.

{lib/std-def.i}
{lib/winlaunch.i}
{lib/winshowscrollbars.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-27 RECT-30 RECT-31 RECT-32 
&Scoped-Define DISPLAYED-OBJECTS tAgentID tName tAddr tCity tState tZip ~
tMainOffice tNumOffices tStat tNumEmployees tNumUnderwriters tContactName ~
tContactPhone tContactFax tContactOther tContactEmail tContactPosition ~
tParentName tParentAddr tParentCity tParentState tParentZip tDeliveredTo ~
tAuditDate tAuditor tRanking tServices tQarID 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD hideReview C-Win 
FUNCTION hideReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD saveCurrentField C-Win 
FUNCTION saveCurrentField RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD setActionWidgetState C-Win 
FUNCTION setActionWidgetState RETURNS LOGICAL PRIVATE
  ( pIsOpen as logical )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD showReview C-Win 
FUNCTION showReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU m_Module 
       MENU-ITEM m_Configure    LABEL "Configure..."  
       MENU-ITEM m_About        LABEL "About..."      
       RULE
       MENU-ITEM m_Exit         LABEL "Exit"          .

DEFINE SUB-MENU m_Review 
       MENU-ITEM m_Start        LABEL "Start..."      
       MENU-ITEM m_Restart      LABEL "Restart..."    
       RULE
       MENU-ITEM m_Open         LABEL "Open..."       
       MENU-ITEM m_Close        LABEL "Close..."      
       MENU-ITEM m_Save         LABEL "Save"          
       RULE
       MENU-ITEM m_Finish       LABEL "Finish..."     .

DEFINE SUB-MENU m_Section 
       MENU-ITEM m_1            LABEL "1 - Evidence of Search" ACCELERATOR "ALT-1"
       MENU-ITEM m_2            LABEL "2 - Title Commitment (Schedule A)" ACCELERATOR "ALT-2"
       MENU-ITEM m_3            LABEL "3 - Title Commitment (Exceptions)" ACCELERATOR "ALT-3"
       MENU-ITEM m_4            LABEL "4 - Title Commitment (Requirements)" ACCELERATOR "ALT-4"
       MENU-ITEM m_5            LABEL "5 - Signed Final Commitment" ACCELERATOR "ALT-5"
       MENU-ITEM m_6            LABEL "6 - Compliance with Requirements" ACCELERATOR "ALT-6"
       MENU-ITEM m_7            LABEL "7 - Closing Instructions" ACCELERATOR "ALT-7"
       MENU-ITEM m_8            LABEL "8 - Policies"   ACCELERATOR "ALT-8"
       MENU-ITEM m_9            LABEL "9 - Underwriting Authority Limits" ACCELERATOR "ALT-9"
       MENU-ITEM m_10           LABEL "10 - Indemnities and Escrows" ACCELERATOR "ALT-A"
       MENU-ITEM m_11           LABEL "11 - Extra-hazardous Risks" ACCELERATOR "ALT-B".

DEFINE SUB-MENU m_View 
       MENU-ITEM m_Documents    LABEL "Documents"     
       MENU-ITEM m_Agent_Files  LABEL "Agent Files"   
       RULE
       MENU-ITEM m_Background_Operations LABEL "Background - Operations".

DEFINE SUB-MENU m_Progress 
       MENU-ITEM m_Unanswered_Questions LABEL "Unanswered Questions"
       MENU-ITEM m_Answered_Questions LABEL "Answered Questions"
       MENU-ITEM m_Findings_and_Best_Practices LABEL "Findings and Best Practices"
       RULE
       MENU-ITEM m_Results      LABEL "Results"       .

DEFINE SUB-MENU m_Tools 
       MENU-ITEM m_Gap_Calendar LABEL "Gap Calendar"  
       MENU-ITEM m_References   LABEL "References"    
       MENU-ITEM m_Agents       LABEL "Agents"        .

DEFINE SUB-MENU m_Reports 
       MENU-ITEM m_Preliminary_Report LABEL "Preliminary Report"
       RULE
       MENU-ITEM m_Questionnaire LABEL "Questionnaire" 
       MENU-ITEM m_Internal_Review LABEL "Internal Review".

DEFINE SUB-MENU m_Actions 
       MENU-ITEM m_Cancel       LABEL "Cancel..."     
       RULE
       MENU-ITEM m_Import       LABEL "Import..."     
       MENU-ITEM m_Export       LABEL "Export..."     
       RULE
       MENU-ITEM m_Inspect      LABEL "Inspect..."    .

DEFINE SUB-MENU m_Informal 
       MENU-ITEM m_Informal_New LABEL "New"           
       MENU-ITEM m_Informal_Open LABEL "Open"          
       MENU-ITEM m_Informal_Close LABEL "Close"         
       MENU-ITEM m_Informal_Save LABEL "Save"          .

DEFINE MENU MENU-BAR-C-Win MENUBAR
       SUB-MENU  m_Module       LABEL "Module"        
       SUB-MENU  m_Review       LABEL "Review"        
       SUB-MENU  m_Section      LABEL "Section"       
       SUB-MENU  m_View         LABEL "View"          
       SUB-MENU  m_Progress     LABEL "Progress"      
       SUB-MENU  m_Tools        LABEL "Tools"         
       SUB-MENU  m_Reports      LABEL "Reports"       
       SUB-MENU  m_Actions      LABEL "Actions"       
       SUB-MENU  m_Informal     LABEL "Informal"      .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bBtnCalendar  NO-FOCUS
     LABEL "Cal" 
     SIZE 7.2 BY 1.71 TOOLTIP "Gap Calendar".

DEFINE BUTTON bBtnClose  NO-FOCUS
     LABEL "Close" 
     SIZE 7.2 BY 1.71 TOOLTIP "Close".

DEFINE BUTTON bBtnFiles  NO-FOCUS
     LABEL "Files" 
     SIZE 7.2 BY 1.71 TOOLTIP "Agent files".

DEFINE BUTTON bBtnOpen  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open".

DEFINE BUTTON bBtnPrelimRpt  NO-FOCUS
     LABEL "Prelim" 
     SIZE 7.2 BY 1.71 TOOLTIP "Produce a preliminary summary report".

DEFINE BUTTON bBtnReferences  NO-FOCUS
     LABEL "Refs" 
     SIZE 7.2 BY 1.71 TOOLTIP "References".

DEFINE BUTTON bBtnSave  NO-FOCUS
     LABEL "Save" 
     SIZE 7.2 BY 1.71 TOOLTIP "Save".

DEFINE BUTTON bBtnSpellCheck  NO-FOCUS
     LABEL "Spl" 
     SIZE 7.2 BY 1.71 TOOLTIP "Check Spelling".

DEFINE BUTTON bBtnStart  NO-FOCUS
     LABEL "Start" 
     SIZE 7.2 BY 1.71 TOOLTIP "Start a queued review".

DEFINE BUTTON bBtnSubmit  NO-FOCUS
     LABEL "Finish" 
     SIZE 7.2 BY 1.71 TOOLTIP "Finalize the review (no further changes allowed)".

DEFINE VARIABLE cbAuditScore AS CHARACTER FORMAT "X(256)":U 
     LABEL "Score" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Select One","0",
                     "Green (1)","1",
                     "Yellow (2)","2",
                     "Red (3)","3"
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE tActiveFile AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "1234567890123456789012345","Item 1"
     DROP-DOWN-LIST
     SIZE 35.4 BY 1 TOOLTIP "Select the agency file that is being reviewed at this time"
     BGCOLOR 15  NO-UNDO.

DEFINE RECTANGLE RECT-bfile
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 30.4 BY 1.86.

DEFINE RECTANGLE RECT-bfile-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 50 BY 1.86.

DEFINE RECTANGLE RECT-bfile-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 23.4 BY 1.86.

DEFINE RECTANGLE RECT-bfile-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 15.8 BY 1.86.

DEFINE RECTANGLE RECT-bfile-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 25.4 BY 1.86.

DEFINE BUTTON bSection-1  NO-FOCUS
     LABEL "Section-1" 
     SIZE 13 BY 1.14 TOOLTIP "Search".

DEFINE BUTTON bsection-10  NO-FOCUS
     LABEL "Section-10" 
     SIZE 13 BY 1.14 TOOLTIP "-Indemnities".

DEFINE BUTTON bsection-11  NO-FOCUS
     LABEL "Section-11" 
     SIZE 13 BY 1.14 TOOLTIP "Risks".

DEFINE BUTTON bSection-2  NO-FOCUS
     LABEL "Section-2" 
     SIZE 13 BY 1.14 TOOLTIP "Schedule A".

DEFINE BUTTON bSection-3  NO-FOCUS
     LABEL "Section-3" 
     SIZE 13 BY 1.14 TOOLTIP "Exceptions".

DEFINE BUTTON bSection-4  NO-FOCUS
     LABEL "Section-4" 
     SIZE 13 BY 1.14 TOOLTIP "Requirements".

DEFINE BUTTON bSection-5  NO-FOCUS
     LABEL "Section-5" 
     SIZE 13 BY 1.14 TOOLTIP "Commitment".

DEFINE BUTTON bSection-6  NO-FOCUS
     LABEL "Section-6" 
     SIZE 13 BY 1.14 TOOLTIP "Requirements".

DEFINE BUTTON bSection-7  NO-FOCUS
     LABEL "Section-7" 
     SIZE 13 BY 1.14 TOOLTIP "Closing".

DEFINE BUTTON bSection-8  NO-FOCUS
     LABEL "Section-8" 
     SIZE 13 BY 1.14 TOOLTIP "Policies".

DEFINE BUTTON bsection-9  NO-FOCUS
     LABEL "Section-9" 
     SIZE 13 BY 1.14 TOOLTIP "Limits".

DEFINE BUTTON tAgentLookup 
     LABEL "..." 
     SIZE 4.4 BY 1.1.

DEFINE VARIABLE tServices AS CHARACTER FORMAT "X(30)":U 
     LABEL "Services" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     LIST-ITEM-PAIRS "Title and Escrow","Both",
                     "Title only","Title",
                     "Escrow only","Escrow"
     DROP-DOWN-LIST
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE tDeliveredTo AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 50.4 BY 2 TOOLTIP "List the recipients of the preliminary and final reports" NO-UNDO.

DEFINE VARIABLE tNotes AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 141 BY 4.48 TOOLTIP "Miscellaneous Notes" NO-UNDO.

DEFINE VARIABLE tAddr AS CHARACTER FORMAT "X(40)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 129.6 BY 1 TOOLTIP "Enter the agency's street address" NO-UNDO.

DEFINE VARIABLE tAgentID AS CHARACTER FORMAT "X(10)":U 
     LABEL "Agent" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the agency's identifier assigned by Alliant" NO-UNDO.

DEFINE VARIABLE tAuditDate AS DATE FORMAT "99/99/99":U 
     LABEL "Review Date" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tAuditor AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reviewer(s)" 
     VIEW-AS FILL-IN 
     SIZE 50.4 BY 1 NO-UNDO.

DEFINE VARIABLE tCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN 
     SIZE 34.8 BY 1 TOOLTIP "Enter the city" NO-UNDO.

DEFINE VARIABLE tContactEmail AS CHARACTER FORMAT "x(40)":U 
     LABEL "Email" 
     VIEW-AS FILL-IN 
     SIZE 58.8 BY 1 TOOLTIP "Enter the contact's primary email address" NO-UNDO.

DEFINE VARIABLE tContactFax AS CHARACTER FORMAT "x(14)":U 
     LABEL "Fax" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE tContactName AS CHARACTER FORMAT "X(60)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 58.8 BY 1 TOOLTIP "Enter the contact's full name" NO-UNDO.

DEFINE VARIABLE tContactOther AS CHARACTER FORMAT "x(14)":U 
     LABEL "Other" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 TOOLTIP "Enter the contact's secondary phone number" NO-UNDO.

DEFINE VARIABLE tContactPhone AS CHARACTER FORMAT "x(14)":U 
     LABEL "Phone" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 TOOLTIP "Enter the contact's primary phone number" NO-UNDO.

DEFINE VARIABLE tContactPosition AS CHARACTER FORMAT "X(256)":U 
     LABEL "Job Title" 
     VIEW-AS FILL-IN 
     SIZE 58.8 BY 1 NO-UNDO.

DEFINE VARIABLE tLastRevenue AS DECIMAL FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "Net Premium $" 
     VIEW-AS FILL-IN 
     SIZE 20.8 BY 1 TOOLTIP "Prior rolling premium of 12 months." NO-UNDO.

DEFINE VARIABLE tName AS CHARACTER FORMAT "X(150)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 129.6 BY 1 TOOLTIP "Enter the full business name of the agency" NO-UNDO.

DEFINE VARIABLE tNumEmployees AS INTEGER FORMAT ">>,>>>":U INITIAL 0 
     LABEL "Employees" 
     VIEW-AS FILL-IN 
     SIZE 20.8 BY 1 TOOLTIP "Enter the total number of employees for the agency" NO-UNDO.

DEFINE VARIABLE tNumOffices AS INTEGER FORMAT ">>>":U INITIAL 0 
     LABEL "Offices" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "Enter the total number of locations for this agency" NO-UNDO.

DEFINE VARIABLE tNumUnderwriters AS INTEGER FORMAT ">>":U INITIAL 0 
     LABEL "Underwriters" 
     VIEW-AS FILL-IN 
     SIZE 20.8 BY 1 TOOLTIP "Enter the total number of title insurance underwriters working with this agency" NO-UNDO.

DEFINE VARIABLE tParentAddr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Address" 
     VIEW-AS FILL-IN 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE tParentCity AS CHARACTER FORMAT "x(40)":U 
     VIEW-AS FILL-IN 
     SIZE 34.8 BY 1 TOOLTIP "Enter the city" NO-UNDO.

DEFINE VARIABLE tParentName AS CHARACTER FORMAT "X(256)":U 
     LABEL "Name" 
     VIEW-AS FILL-IN 
     SIZE 55.8 BY 1 NO-UNDO.

DEFINE VARIABLE tParentState AS CHARACTER FORMAT "X(2)":U 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE tParentZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN 
     SIZE 14.8 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE VARIABLE tQarID AS CHARACTER FORMAT "X(10)":U 
     LABEL "QUR" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "Enter the agency's identifier assigned by Alliant" NO-UNDO.

DEFINE VARIABLE tRanking AS INTEGER FORMAT ">>,>>>":U INITIAL 0 
     LABEL "Ranking" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE tStat AS CHARACTER FORMAT "X(256)":U 
     LABEL "Status" 
     VIEW-AS FILL-IN 
     SIZE 22.2 BY 1 NO-UNDO.

DEFINE VARIABLE tState AS CHARACTER FORMAT "X(2)":U 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE tZip AS CHARACTER FORMAT "x(10)":U 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 TOOLTIP "Enter the Zipcode" NO-UNDO.

DEFINE RECTANGLE RECT-27
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71.8 BY 8.1.

DEFINE RECTANGLE RECT-30
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 67.8 BY 4.52.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71.8 BY 4.52.

DEFINE RECTANGLE RECT-32
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 67.8 BY 8.1.

DEFINE VARIABLE tMainOffice AS LOGICAL INITIAL yes 
     LABEL "Main Office" 
     VIEW-AS TOGGLE-BOX
     SIZE 15 BY .81 TOOLTIP "Select if the location you are auditing is the main office" NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 145.4 BY 28 WIDGET-ID 100.

DEFINE FRAME fMain
     bSection-1 AT ROW 1 COL 1.8 WIDGET-ID 92
     bsection-10 AT ROW 1 COL 118.8 WIDGET-ID 124
     bsection-11 AT ROW 1 COL 131.8 WIDGET-ID 126
     bSection-2 AT ROW 1 COL 14.8 WIDGET-ID 94
     bSection-3 AT ROW 1 COL 27.8 WIDGET-ID 96
     bSection-4 AT ROW 1 COL 40.8 WIDGET-ID 98
     bSection-5 AT ROW 1 COL 53.8 WIDGET-ID 100
     bSection-6 AT ROW 1 COL 66.8 WIDGET-ID 102
     bSection-7 AT ROW 1 COL 79.8 WIDGET-ID 104
     bSection-8 AT ROW 1 COL 92.8 WIDGET-ID 106
     bsection-9 AT ROW 1 COL 105.8 WIDGET-ID 122
     tAgentID AT ROW 3.76 COL 12.4 COLON-ALIGNED WIDGET-ID 2
     tName AT ROW 4.86 COL 12.4 COLON-ALIGNED WIDGET-ID 4
     tAddr AT ROW 5.95 COL 12.4 COLON-ALIGNED WIDGET-ID 6
     tCity AT ROW 7.05 COL 12.4 COLON-ALIGNED NO-LABEL WIDGET-ID 82
     tState AT ROW 7.05 COL 48.4 COLON-ALIGNED NO-LABEL WIDGET-ID 90
     tZip AT ROW 7.05 COL 54.4 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     tMainOffice AT ROW 9.38 COL 6.2 WIDGET-ID 48
     tNumOffices AT ROW 10.57 COL 11.2 COLON-ALIGNED WIDGET-ID 36
     tStat AT ROW 2.67 COL 119.6 COLON-ALIGNED WIDGET-ID 118 NO-TAB-STOP 
     tNumEmployees AT ROW 9.14 COL 50.2 COLON-ALIGNED WIDGET-ID 38
     tNumUnderwriters AT ROW 10.33 COL 50.2 COLON-ALIGNED WIDGET-ID 40
     tLastRevenue AT ROW 11.52 COL 50.2 COLON-ALIGNED WIDGET-ID 44
     tContactName AT ROW 13.67 COL 12.2 COLON-ALIGNED WIDGET-ID 84
     tContactPhone AT ROW 14.86 COL 12.2 COLON-ALIGNED WIDGET-ID 18
     tContactFax AT ROW 16.05 COL 12.2 COLON-ALIGNED WIDGET-ID 20
     tContactOther AT ROW 17.24 COL 12.2 COLON-ALIGNED WIDGET-ID 22
     tContactEmail AT ROW 18.43 COL 12.2 COLON-ALIGNED WIDGET-ID 24
     tContactPosition AT ROW 19.62 COL 12.2 COLON-ALIGNED WIDGET-ID 66
     tParentName AT ROW 9.1 COL 84.2 COLON-ALIGNED WIDGET-ID 52
     tParentAddr AT ROW 10.29 COL 84.2 COLON-ALIGNED WIDGET-ID 54
     tAgentLookup AT ROW 3.76 COL 28.8 WIDGET-ID 50 NO-TAB-STOP 
     tParentCity AT ROW 11.48 COL 84.4 COLON-ALIGNED NO-LABEL WIDGET-ID 56
     tParentState AT ROW 11.48 COL 119.6 COLON-ALIGNED NO-LABEL WIDGET-ID 58
     tParentZip AT ROW 11.48 COL 125 COLON-ALIGNED NO-LABEL WIDGET-ID 60
     tDeliveredTo AT ROW 13.67 COL 91.6 NO-LABEL WIDGET-ID 46
     tAuditDate AT ROW 16.05 COL 89.6 COLON-ALIGNED WIDGET-ID 42
     tAuditor AT ROW 17.24 COL 89.6 COLON-ALIGNED WIDGET-ID 80
     tRanking AT ROW 18.43 COL 89.6 COLON-ALIGNED WIDGET-ID 88
     tServices AT ROW 19.62 COL 89.6 COLON-ALIGNED WIDGET-ID 76
     tNotes AT ROW 22.24 COL 3 NO-LABEL WIDGET-ID 114
     tQarID AT ROW 2.67 COL 12.4 COLON-ALIGNED WIDGET-ID 112 NO-TAB-STOP 
     "Location" VIEW-AS TEXT
          SIZE 11 BY .62 AT ROW 8.19 COL 4 WIDGET-ID 70
          FONT 6
     "General" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 12.95 COL 77.2 WIDGET-ID 74
          FONT 6
     "General Observations" VIEW-AS TEXT
          SIZE 21 BY .62 AT ROW 21.52 COL 3 WIDGET-ID 116
     "Contact" VIEW-AS TEXT
          SIZE 10 BY .62 AT ROW 12.95 COL 4 WIDGET-ID 86
          FONT 6
     "Parent Company" VIEW-AS TEXT
          SIZE 19 BY .62 AT ROW 8.19 COL 77.2 WIDGET-ID 64
          FONT 6
     "Delivered To:" VIEW-AS TEXT
          SIZE 13 BY .62 AT ROW 13.86 COL 78.4 WIDGET-ID 120
     RECT-27 AT ROW 13.19 COL 3.2 WIDGET-ID 78
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 2.95 SCROLLABLE  WIDGET-ID 200.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME fMain
     RECT-30 AT ROW 8.43 COL 76.2 WIDGET-ID 62
     RECT-31 AT ROW 8.43 COL 3.2 WIDGET-ID 68
     RECT-32 AT ROW 13.19 COL 76.2 WIDGET-ID 72
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.2 ROW 2.95 SCROLLABLE  WIDGET-ID 200.

DEFINE FRAME fButtons
     bBtnFiles AT ROW 1.05 COL 111.8 WIDGET-ID 34 NO-TAB-STOP 
     bBtnSpellCheck AT ROW 1.05 COL 62 WIDGET-ID 120 NO-TAB-STOP 
     bBtnClose AT ROW 1.05 COL 16.2 WIDGET-ID 8 NO-TAB-STOP 
     bBtnStart AT ROW 1.05 COL 1.8 WIDGET-ID 24 NO-TAB-STOP 
     bBtnSubmit AT ROW 1.05 COL 38.8 WIDGET-ID 22 NO-TAB-STOP 
     bBtnCalendar AT ROW 1.05 COL 54.6 WIDGET-ID 40 NO-TAB-STOP 
     tActiveFile AT ROW 1.38 COL 73.8 COLON-ALIGNED WIDGET-ID 28
     cbAuditScore AT ROW 1.43 COL 125.8 COLON-ALIGNED WIDGET-ID 126
     bBtnOpen AT ROW 1.05 COL 9 WIDGET-ID 6 NO-TAB-STOP 
     bBtnPrelimRpt AT ROW 1.05 COL 31.6 WIDGET-ID 14 NO-TAB-STOP 
     bBtnReferences AT ROW 1.05 COL 47.2 WIDGET-ID 118 NO-TAB-STOP 
     bBtnSave AT ROW 1.05 COL 23.4 WIDGET-ID 10 NO-TAB-STOP 
     "Score:" VIEW-AS TEXT
          SIZE 6 BY .62 AT ROW 1.57 COL 120.8 WIDGET-ID 130
     "File:" VIEW-AS TEXT
          SIZE 4.8 BY .62 AT ROW 1.57 COL 70.6 WIDGET-ID 44
     RECT-bfile AT ROW 1 COL 1 WIDGET-ID 4
     RECT-bfile-2 AT ROW 1 COL 69.8 WIDGET-ID 46
     RECT-bfile-3 AT ROW 1 COL 46.6 WIDGET-ID 48
     RECT-bfile-5 AT ROW 1 COL 31 WIDGET-ID 52
     RECT-bfile-6 AT ROW 1 COL 119.4 WIDGET-ID 128
    WITH 1 DOWN NO-BOX NO-HIDE KEEP-TAB-ORDER NO-HELP 
         NO-LABELS SIDE-LABELS NO-UNDERLINE NO-VALIDATE THREE-D NO-AUTO-VALIDATE 
         AT COL 1 ROW 1
         SIZE 144 BY 1.86 WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 28
         WIDTH              = 145.4
         MAX-HEIGHT         = 39.86
         MAX-WIDTH          = 288
         VIRTUAL-HEIGHT     = 39.86
         VIRTUAL-WIDTH      = 288
         MAX-BUTTON         = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU MENU-BAR-C-Win:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fButtons:FRAME = FRAME DEFAULT-FRAME:HANDLE
       FRAME fMain:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */

DEFINE VARIABLE XXTABVALXX AS LOGICAL NO-UNDO.

ASSIGN XXTABVALXX = FRAME fButtons:MOVE-BEFORE-TAB-ITEM (FRAME fMain:HANDLE)
/* END-ASSIGN-TABS */.

/* SETTINGS FOR FRAME fButtons
                                                                        */
/* SETTINGS FOR BUTTON bBtnClose IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnFiles IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnPrelimRpt IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnSave IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnSpellCheck IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bBtnSubmit IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX cbAuditScore IN FRAME fButtons
   NO-ENABLE LABEL "Score:"                                             */
/* SETTINGS FOR RECTANGLE RECT-bfile IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-2 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-3 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-bfile-5 IN FRAME fButtons
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tActiveFile IN FRAME fButtons
   NO-DISPLAY NO-ENABLE LABEL "Agent:"                                  */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME Size-to-Fit Custom                                        */
ASSIGN 
       FRAME fMain:SCROLLABLE       = FALSE.

/* SETTINGS FOR BUTTON bSection-1 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-1:PRIVATE-DATA IN FRAME fMain     = 
                "1".

/* SETTINGS FOR BUTTON bsection-10 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bsection-10:PRIVATE-DATA IN FRAME fMain     = 
                "10".

/* SETTINGS FOR BUTTON bsection-11 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bsection-11:PRIVATE-DATA IN FRAME fMain     = 
                "11".

/* SETTINGS FOR BUTTON bSection-2 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-2:PRIVATE-DATA IN FRAME fMain     = 
                "2".

/* SETTINGS FOR BUTTON bSection-3 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-3:PRIVATE-DATA IN FRAME fMain     = 
                "3".

/* SETTINGS FOR BUTTON bSection-4 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-4:PRIVATE-DATA IN FRAME fMain     = 
                "4".

/* SETTINGS FOR BUTTON bSection-5 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-5:PRIVATE-DATA IN FRAME fMain     = 
                "5".

/* SETTINGS FOR BUTTON bSection-6 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-6:PRIVATE-DATA IN FRAME fMain     = 
                "6".

/* SETTINGS FOR BUTTON bSection-7 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-7:PRIVATE-DATA IN FRAME fMain     = 
                "7".

/* SETTINGS FOR BUTTON bSection-8 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bSection-8:PRIVATE-DATA IN FRAME fMain     = 
                "8".

/* SETTINGS FOR BUTTON bsection-9 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       bsection-9:PRIVATE-DATA IN FRAME fMain     = 
                "9".

/* SETTINGS FOR FILL-IN tAddr IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAddr:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tAgentID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAgentID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR BUTTON tAgentLookup IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tAgentLookup:HIDDEN IN FRAME fMain           = TRUE.

/* SETTINGS FOR FILL-IN tAuditDate IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tAuditor IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tCity IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tCity:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tContactEmail IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactFax IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactName IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactOther IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactPhone IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tContactPosition IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR EDITOR tDeliveredTo IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tLastRevenue IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       tLastRevenue:HIDDEN IN FRAME fMain           = TRUE.

/* SETTINGS FOR TOGGLE-BOX tMainOffice IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tName IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tName:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR EDITOR tNotes IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN tNumEmployees IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNumOffices IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tNumUnderwriters IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentAddr IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentCity IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentName IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentState IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tParentZip IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tQarID IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tQarID:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tRanking IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tServices IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN tStat IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tStat:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tState IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tState:READ-ONLY IN FRAME fMain        = TRUE.

/* SETTINGS FOR FILL-IN tZip IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       tZip:READ-ONLY IN FRAME fMain        = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fButtons
/* Query rebuild information for FRAME fButtons
     _Query            is NOT OPENED
*/  /* FRAME fButtons */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fMain
/* Query rebuild information for FRAME fMain
     _Query            is NOT OPENED
*/  /* FRAME fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win
DO:
  if tLockOn 
   then
    do: 
      MESSAGE "Please complete the Finding or Best Practice." VIEW-AS ALERT-BOX warning BUTTONS OK.
      return no-apply.
    end.
  
  run closeAudit in this-procedure no-error.
  if error-status:error 
   then return no-apply.

  publish "SetRecentAudits" (input table recentAudit).
  publish "ExitApplication".

  apply "CLOSE":U to this-procedure.
  return.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME bBtnCalendar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnCalendar C-Win
ON CHOOSE OF bBtnCalendar IN FRAME fButtons /* Cal */
DO:
  run showCalendar in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnClose
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnClose C-Win
ON CHOOSE OF bBtnClose IN FRAME fButtons /* Close */
DO: 
  run closeAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnFiles
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnFiles C-Win
ON CHOOSE OF bBtnFiles IN FRAME fButtons /* Files */
DO:
  def var tFileList     as char.
  def var tActiveFileID as int.
  
  tActiveFileID = activeAgentFileID.
  run dialogfiles.w (input-output tActiveFileID,
                     output tFileList).
  
  /* tActiveFile is the combo-box */
  tActiveFile:list-item-pairs in frame fButtons = tFileList.
  if tFileList = "|0"
    then tActiveFile:sensitive = false.
  else tActiveFile:sensitive = true.
  tActiveFile:screen-value = string(tActiveFileID).
  
  /* Always publish as though "changed" as attributes may have been updated */
  activeAgentFileID = tActiveFileID.
  publish "ActiveFileChanged" (activeAgentFileID).  
  publish "IsNew" (output std-lo).
  publish "AuditChanged" (std-lo).
  publish "ShowTitle" ( entry( lookup(tActiveFile:screen-value,tActiveFile:list-item-pairs, "|") - 1, tActiveFile:list-item-pairs, "|")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnOpen C-Win
ON CHOOSE OF bBtnOpen IN FRAME fButtons /* Open */
DO:  
  run openAudit in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnPrelimRpt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnPrelimRpt C-Win
ON CHOOSE OF bBtnPrelimRpt IN FRAME fButtons /* Prelim */
DO: 
  apply "CHOOSE" to menu-item m_Preliminary_Report in menu m_Reports.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnReferences
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnReferences C-Win
ON CHOOSE OF bBtnReferences IN FRAME fButtons /* Refs */
DO:
  run showReferences in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSave
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSave C-Win
ON CHOOSE OF bBtnSave IN FRAME fButtons /* Save */
DO:
  run saveAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSpellCheck
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSpellCheck C-Win
ON CHOOSE OF bBtnSpellCheck IN FRAME fButtons /* Spl */
DO:
  run SpellChecker in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnStart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnStart C-Win
ON CHOOSE OF bBtnStart IN FRAME fButtons /* Start */
DO:
  run startAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bBtnSubmit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bBtnSubmit C-Win
ON CHOOSE OF bBtnSubmit IN FRAME fButtons /* Finish */
DO:
  if cbAuditScore:screen-value = "0" 
   then
    do:
      MESSAGE "Please select audit score."
          VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return no-apply.
    end.
  run finishAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME bSection-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSection-1 C-Win
ON CHOOSE OF bSection-1 IN FRAME fMain /* Section-1 */
or 'choose' of bSection-2
 or 'choose' of bSection-3
 or 'choose' of bSection-4
 or 'choose' of bSection-5
 or 'choose' of bSection-6
 or 'choose' of bSection-7
 or 'choose' of bSection-8
 or 'choose' of bSection-9
 or 'choose' of bSection-10
 or 'choose' of bSection-11
DO:
  if valid-handle(wSection[integer(self:private-data)]) 
   then
    do:
      run ShowWindow IN wSection[integer(self:private-data)].
      publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE in frame fButtons,tActiveFile:LIST-ITEM-PAIRS in frame fButtons, "|") - 1, tActiveFile:LIST-ITEM-PAIRS in frame fButtons, "|")).
    end.
  else
    if not valid-handle(wSection[integer(self:private-data)]) then 
      run ViewSection (int(self:private-data)).
  return.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fButtons
&Scoped-define SELF-NAME cbAuditScore
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbAuditScore C-Win
ON VALUE-CHANGED OF cbAuditScore IN FRAME fButtons /* Score */
DO:
  if self:modified 
   then
    do:
      {lib/setattrch.i auditScore self:screen-value}
      bBtnSave:sensitive in frame fButtons = true.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_1 C-Win
ON CHOOSE OF MENU-ITEM m_1 /* 1 - Evidence of Search */
do:
  apply "choose" to bSection-1 in frame fMain.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_10 C-Win
ON CHOOSE OF MENU-ITEM m_10 /* 10 - Indemnities and Escrows */
DO:
  apply "choose" to bSection-10 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_11 C-Win
ON CHOOSE OF MENU-ITEM m_11 /* 11 - Extra-hazardous Risks */
DO:
  apply "choose" to bSection-11 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_2 C-Win
ON CHOOSE OF MENU-ITEM m_2 /* 2 - Title Commitment (Schedule A) */
DO:
  apply "choose" to bSection-2 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_3 C-Win
ON CHOOSE OF MENU-ITEM m_3 /* 3 - Title Commitment (Exceptions) */
DO:
  apply "choose" to bSection-3 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_4 C-Win
ON CHOOSE OF MENU-ITEM m_4 /* 4 - Title Commitment (Requirements) */
DO:
  apply "choose" to bSection-4 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_5 C-Win
ON CHOOSE OF MENU-ITEM m_5 /* 5 - Signed Final Commitment */
DO:
  apply "choose" to bSection-5 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_6 C-Win
ON CHOOSE OF MENU-ITEM m_6 /* 6 - Compliance with Requirements */
DO:
  apply "choose" to bSection-6 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_7 C-Win
ON CHOOSE OF MENU-ITEM m_7 /* 7 - Closing Instructions */
DO:
  apply "choose" to bSection-7 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_8 C-Win
ON CHOOSE OF MENU-ITEM m_8 /* 8 - Policies */
DO:
  apply "choose" to bSection-8 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_9 C-Win
ON CHOOSE OF MENU-ITEM m_9 /* 9 - Underwriting Authority Limits */
DO:
  apply "choose" to bSection-9 in frame fMain.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_About
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_About C-Win
ON CHOOSE OF MENU-ITEM m_About /* About... */
DO:
  publish "AboutApplication".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agents C-Win
ON CHOOSE OF MENU-ITEM m_Agents /* Agents */
DO:
  if valid-handle(hAgent)
   then run ShowWindow in hAgent no-error.
   else run referenceagent.w persistent set hAgent.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Agent_Files
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Agent_Files C-Win
ON CHOOSE OF MENU-ITEM m_Agent_Files /* Agent Files */
DO:
  apply "CHOOSE" to bBtnFiles in frame fButtons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Answered_Questions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Answered_Questions C-Win
ON CHOOSE OF MENU-ITEM m_Answered_Questions /* Answered Questions */
DO:
  if valid-handle(wAnswered)
    then run showWindow in wAnswered.
  else run wanswered.w persistent set wAnswered. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Background_Operations
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Background_Operations C-Win
ON CHOOSE OF MENU-ITEM m_Background_Operations /* Background - Operations */
DO:
  if valid-handle(wBkgNoc)
   then
     run ShowWindow IN wBkgNoc.
  else run wbkgnoc.w persistent set wBkgNoc.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Cancel C-Win
ON CHOOSE OF MENU-ITEM m_Cancel /* Cancel... */
DO:
  run cancelAudit in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Close C-Win
ON CHOOSE OF MENU-ITEM m_Close /* Close... */
DO:
  run closeAudit in this-procedure no-error.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Configure
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Configure C-Win
ON CHOOSE OF MENU-ITEM m_Configure /* Configure... */
DO:
  run dialogoptions.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Documents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Documents C-Win
ON CHOOSE OF MENU-ITEM m_Documents /* Documents */
DO:
  publish "IsActive" (output std-lo).
  if not std-lo
   then return.

  if not valid-handle(hDocWindow)
   then run repository.w persistent set hDocWindow ("Audit", string(activeAuditID)).
   else run ShowWindow in hDocWindow.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Exit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Exit C-Win
ON CHOOSE OF MENU-ITEM m_Exit /* Exit */
DO:
  apply "WINDOW-CLOSE" to {&window-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Export
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Export C-Win
ON CHOOSE OF MENU-ITEM m_Export /* Export... */
DO:
  run exportAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Findings_and_Best_Practices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Findings_and_Best_Practices C-Win
ON CHOOSE OF MENU-ITEM m_Findings_and_Best_Practices /* Findings and Best Practices */
DO:
  if valid-handle(wFindings)
    then run showWindow in wFindings.
  else run wfindings.w persistent set wFindings.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Finish
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Finish C-Win
ON CHOOSE OF MENU-ITEM m_Finish /* Finish... */
DO:
  apply "choose" to bBtnSubmit in frame fbuttons.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Gap_Calendar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Gap_Calendar C-Win
ON CHOOSE OF MENU-ITEM m_Gap_Calendar /* Gap Calendar */
DO:
  run showCalendar in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Import
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Import C-Win
ON CHOOSE OF MENU-ITEM m_Import /* Import... */
DO:
  run importAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_Close
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_Close C-Win
ON CHOOSE OF MENU-ITEM m_Informal_Close /* Close */
DO:
  run closeInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_New
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_New C-Win
ON CHOOSE OF MENU-ITEM m_Informal_New /* New */
DO:
  run newInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_Open C-Win
ON CHOOSE OF MENU-ITEM m_Informal_Open /* Open */
DO:
  run openInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Informal_Save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Informal_Save C-Win
ON CHOOSE OF MENU-ITEM m_Informal_Save /* Save */
DO:
  run saveInformal in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Inspect
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Inspect C-Win
ON CHOOSE OF MENU-ITEM m_Inspect /* Inspect... */
DO:
  run serverInspect in this-procedure.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Internal_Review
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Internal_Review C-Win
ON CHOOSE OF MENU-ITEM m_Internal_Review /* Internal Review */
DO:
  run generateReport in this-procedure ("InternalReport").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Module
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Module C-Win
ON MENU-DROP OF MENU m_Module /* Module */
or menu-drop of menu m_Review 
    or menu-drop of menu m_Section
    or menu-drop of menu m_View
    or menu-drop of menu m_Progress
    or menu-drop of menu m_Tools
    or menu-drop of menu m_Reports
    or menu-drop of menu m_Actions
DO:
/*     saveCurrentField(). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Open
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Open C-Win
ON CHOOSE OF MENU-ITEM m_Open /* Open... */
DO:
  run openAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Preliminary_Report
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Preliminary_Report C-Win
ON CHOOSE OF MENU-ITEM m_Preliminary_Report /* Preliminary Report */
DO:
  run generateReport in this-procedure ("PreliminaryReport").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Questionnaire
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Questionnaire C-Win
ON CHOOSE OF MENU-ITEM m_Questionnaire /* Questionnaire */
DO:
  run generateReport in this-procedure ("QuestionnaireReport").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_References
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_References C-Win
ON CHOOSE OF MENU-ITEM m_References /* References */
DO:
  run showReferences in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Restart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Restart C-Win
ON CHOOSE OF MENU-ITEM m_Restart /* Restart... */
DO:
  run restartAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Results
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Results C-Win
ON CHOOSE OF MENU-ITEM m_Results /* Results */
DO:
  if valid-handle(wScoring)
    then run showWindow in wScoring.
  else run wscoring.w persistent set wScoring.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Save
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Save C-Win
ON CHOOSE OF MENU-ITEM m_Save /* Save */
DO:
  run saveAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Start
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Start C-Win
ON CHOOSE OF MENU-ITEM m_Start /* Start... */
DO:
  run startAudit in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Unanswered_Questions
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Unanswered_Questions C-Win
ON CHOOSE OF MENU-ITEM m_Unanswered_Questions /* Unanswered Questions */
DO:
  if valid-handle(wUnanswered)
    then run showWindow in wUnanswered.
  else run wunanswered.w persistent set wUnanswered.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tActiveFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tActiveFile C-Win
ON VALUE-CHANGED OF tActiveFile IN FRAME fButtons /* Agent */
DO:
  do with frame fButtons:
  end.
  activeAgentFileID = int(self:screen-value).
  publish "ActiveFileChanged" (activeAgentFileID).
  publish "ShowTitle" ( entry( lookup(tActiveFile:screen-value,tActiveFile:list-item-pairs, "|") - 1, tActiveFile:list-item-pairs, "|")).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fMain
&Scoped-define SELF-NAME tAddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAddr C-Win
ON LEAVE OF tAddr IN FRAME fMain /* Address */
DO:
 if self:modified 
  then
   {lib/setattrch.i addr self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAgentID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAgentID C-Win
ON LEAVE OF tAgentID IN FRAME fMain /* Agent */
DO:
 if self:modified 
  then
   {lib/setattrch.i agentID self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditDate C-Win
ON LEAVE OF tAuditDate IN FRAME fMain /* Review Date */
DO:
 if self:modified 
  then
   do:
     {lib/setattrda.i auditDate self:input-value}
   end.
  else do:
    publish "GetReviewAuditor" (output std-da , output std-ch).
    date(tAuditdate:screen-value) = std-da.
  end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuditor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuditor C-Win
ON LEAVE OF tAuditor IN FRAME fMain /* Reviewer(s) */
DO:
 if self:modified 
  then
   {lib/setattrch.i auditor self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tCity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tCity C-Win
ON LEAVE OF tCity IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i city self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactEmail
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactEmail C-Win
ON LEAVE OF tContactEmail IN FRAME fMain /* Email */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactEmail self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactFax
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactFax C-Win
ON LEAVE OF tContactFax IN FRAME fMain /* Fax */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactFax self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactName C-Win
ON LEAVE OF tContactName IN FRAME fMain /* Name */
DO:
 if self:modified 
  then
   do:
    {lib/setattrch.i contactName self:screen-value}
    if tDeliveredTo:screen-value = "" 
    then 
    do: tDeliveredTo:screen-value = self:screen-value.
        {lib/setattrch.i deliveredTo self:screen-value}
    end.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactOther
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactOther C-Win
ON LEAVE OF tContactOther IN FRAME fMain /* Other */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactOther self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactPhone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactPhone C-Win
ON LEAVE OF tContactPhone IN FRAME fMain /* Phone */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactPhone self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tContactPosition
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tContactPosition C-Win
ON LEAVE OF tContactPosition IN FRAME fMain /* Job Title */
DO:
 if self:modified 
  then
   {lib/setattrch.i contactPosition self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tDeliveredTo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tDeliveredTo C-Win
ON LEAVE OF tDeliveredTo IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i deliveredTo self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tLastRevenue
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tLastRevenue C-Win
ON LEAVE OF tLastRevenue IN FRAME fMain /* Net Premium $ */
DO:
 if self:modified 
  then
   {lib/setattrde.i lastRevenue self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tMainOffice
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tMainOffice C-Win
ON VALUE-CHANGED OF tMainOffice IN FRAME fMain /* Main Office */
DO:
  if self:modified 
   then
    do:
      {lib/setattrlo.i mainOffice self:checked}
      run EnableSave.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tName C-Win
ON LEAVE OF tName IN FRAME fMain /* Name */
DO:
 if self:modified 
  then
   {lib/setattrch.i name self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNotes
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNotes C-Win
ON LEAVE OF tNotes IN FRAME fMain
DO:
  if self:modified 
   then publish "SetQuestionAnswer" (1000, input self:screen-value).
  publish "SaveAfterEveryAnswer".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNumEmployees
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNumEmployees C-Win
ON LEAVE OF tNumEmployees IN FRAME fMain /* Employees */
DO:
 if self:modified 
  then
  {lib/setattrin.i numEmployees self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNumOffices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNumOffices C-Win
ON LEAVE OF tNumOffices IN FRAME fMain /* Offices */
DO:
 if self:modified 
  then
   {lib/setattrin.i numOffices self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tNumUnderwriters
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tNumUnderwriters C-Win
ON LEAVE OF tNumUnderwriters IN FRAME fMain /* Underwriters */
DO:
 if self:modified 
  then
   {lib/setattrin.i numUnderwriters self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentAddr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentAddr C-Win
ON LEAVE OF tParentAddr IN FRAME fMain /* Address */
DO:
 if self:modified 
  then
   {lib/setattrch.i parentAddr self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentCity
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentCity C-Win
ON LEAVE OF tParentCity IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i parentCity self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentName
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentName C-Win
ON LEAVE OF tParentName IN FRAME fMain /* Name */
DO:
 if self:modified 
  then
   {lib/setattrch.i parentName self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentState C-Win
ON LEAVE OF tParentState IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i parentState self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tParentZip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tParentZip C-Win
ON LEAVE OF tParentZip IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i parentZip self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tRanking
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tRanking C-Win
ON LEAVE OF tRanking IN FRAME fMain /* Ranking */
DO:
 if self:modified 
  then
   {lib/setattrin.i ranking self:input-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tServices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tServices C-Win
ON VALUE-CHANGED OF tServices IN FRAME fMain /* Services */
DO:
  if self:modified 
   then
    do:
      {lib/setattrch.i services self:screen-value}
      run EnableSave.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tState C-Win
ON LEAVE OF tState IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i state self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tZip
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tZip C-Win
ON LEAVE OF tZip IN FRAME fMain
DO:
 if self:modified 
  then
   {lib/setattrch.i zip self:screen-value}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

{lib/win-main.i}

subscribe to "startAudit" anywhere.
subscribe to "AuditChanged" anywhere.
subscribe to "ViewFinding" anywhere.
subscribe to "SelectAuditPage" anywhere.
subscribe to "DisplayScore" anywhere.
subscribe to "WindowClosed" anywhere.
subscribe to "WindowOpened" anywhere.
subscribe to "SetClickSave" anywhere.

subscribe to "LockOn" anywhere.
subscribe to "LockOff" anywhere.
subscribe to "SaveAfterEveryAnswer" anywhere .
subscribe to "StartNewAudit" anywhere.
subscribe to "EnableSave" anywhere.
subscribe to "LeaveAll" anywhere.
subscribe to "ActionWindowForAgent" anywhere.

publish "SetCurrentValue" ("Report","").
publish "GetAutoSave" (output tAutoSave ).
{&window-name}:window-state = 2.

bBtnOpen:load-image("images/open.bmp") in frame fButtons.
bBtnOpen:load-image-insensitive("images/open-i.bmp") in frame fButtons.
bBtnClose:load-image("images/close.bmp") in frame fButtons.
bBtnClose:load-image-insensitive("images/close-i.bmp") in frame fButtons.
bBtnSave:load-image("images/save.bmp") in frame fButtons.
bBtnSave:load-image-insensitive("images/save-i.bmp") in frame fButtons.
bBtnPrelimRpt:load-image("images/report.bmp") in frame fButtons.
bBtnPrelimRpt:load-image-insensitive("images/report-i.bmp") in frame fButtons.
bBtnSubmit:load-image("images/upload.bmp") in frame fButtons.
bBtnSubmit:load-image-insensitive("images/upload-i.bmp") in frame fButtons.
bBtnStart:load-image("images/download.bmp") in frame fButtons.
bBtnStart:load-image-insensitive("images/download-i.bmp") in frame fButtons.
bBtnFiles:load-image("images/docs.bmp") in frame fButtons.
bBtnFiles:load-image-insensitive("images/docs-i.bmp") in frame fButtons.
bBtnReferences:load-image("images/book.bmp") in frame fButtons.
bBtnReferences:load-image-insensitive("images/book-i.bmp") in frame fButtons.
bBtnCalendar:load-image("images/cal.bmp") in frame fButtons.
bBtnSpellCheck:load-image("images/spellcheck.bmp") in frame fButtons.
bBtnSpellCheck:load-image-insensitive("images/spellcheck-i.bmp") in frame fButtons.
tActiveFile:delimiter in frame fButtons = "|".

hideReview().
setActionWidgetState(false).
frame fMain:sensitive = false.

status input " " in window {&window-name}. 

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN enable_UI.
  

  on 'value-changed' of frame fMain anywhere 
  do:
    run EnableSave.
    tclicksave = false.
  end.
   tlastrevenue:visible = true.

  /* Sets the maximum width and height to that of the current
     Windows Resolution*/
  assign current-window:max-width  = 145.00
         current-window:max-height = 28
         current-window:min-width  = 145.00
         .
  {&window-name}:window-state = 3.

  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionWindowForAgent C-Win 
PROCEDURE ActionWindowForAgent :
/*------------------------------------------------------------------------------
@description Opens a window for an agent
------------------------------------------------------------------------------*/
  define input parameter pAgentID as character no-undo.
  define input parameter pType as character no-undo.
  define input parameter pWindow as character no-undo.
  
  openWindowForAgent(pAgentID, pType, pWindow).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditChanged C-Win 
PROCEDURE AuditChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pNew as logical.

 if not pNew
  then 
   assign
     menu-item m_Save:sensitive in menu m_Review = true
     bBtnSave:sensitive in frame fButtons = true
     .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancelAudit C-Win 
PROCEDURE cancelAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pReason as char .
 def var pCancel as logical.
 def var pSuccess as logical.
 run dialogcancel.w (input tQarID:screen-value in frame fMain,
                     input tAgentID:screen-value in frame fMain,
                     input tName:screen-value in frame fMain, 
                     output pReason,
                     output pCancel).
 if pCancel then
   return.
 publish "cancelAudit" (input integer(activeauditid) ,
                        input preason,
                        output pSuccess).
 if not pSuccess then
   return.
 publish "AuditClosed".
 publish "ActiveFileChanged" (0).
 hideReview().
 setActionWidgetState(false).
 frame fMain:sensitive = false.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeAudit C-Win 
PROCEDURE closeAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOK as logical.
 define variable pFnBPChanged as character no-undo.
 define variable pCorrectCase as character no-undo.
 define variable tCloseAll as logical      no-undo.
 define variable pAuditStatus as character no-undo.
 define variable iCount       as integer   no-undo.
 define variable sectionscore as character no-undo .
 define variable tInvalidSec  as character initial "f,f,f,f,f,f,f,f" no-undo.

 /* Informal */
 {lib/getattrch.i auditType std-ch}
 if std-ch = ""
  then
   do: run closeInformal in this-procedure.
       return.
   end.

 publish "IsActive" (output tOk).
 if not tOk 
  then return.

 publish "SetCurrentValue" ("Report", "").
 publish "GetCloseSections" (output tCloseAll) .
 publish "GetAuditStatus" (output pAuditStatus).
 status default "" in window {&window-name}.
 status input "" in window {&window-name}.

 if tAutoSave then
   publish "LeaveAll" .

 tCloseAll = false.
 if not tAutoSave 
  then
   do:
     publish "IsChanged" (output tOk).
     if tOk 
      then
       do:
         publish "IsNew" (output tOk).
         if not tOk
          then 
           assign
             menu-item m_Save:sensitive in menu m_Review = true
             bBtnSave:sensitive in frame fButtons = true
              .
   
           MESSAGE "Audit has been modified. All changes will be discarded."
            VIEW-AS ALERT-BOX question BUTTONS ok-cancel set tOK.
         if not tOK 
          then return error.
         else
          run closewindows .
       end.
       else 
         run closewindows .
   end.
 else 
   run closewindows .

 pFnBPChanged = "" . 
 pCorrectCase = "".
 publish "IsModifiedFindBp" (input "auditclose",
                            output pFnBPChanged,
                            output pCorrectCase).
 if tCloseAll = false 
  then
   do:
     if (pFnBPChanged = "auditclose" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "auditclose" )then 
     do:
       MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
               "Do you want to continue?"
         VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
       if lChoice = true then
       publish "CloseFindBp" ("auditclose").
       else 
       do:
         run EnableSave .
         return error.
       end.
       pFnBPChanged = "".
     end.
     else
       publish "CloseFindBp" ("auditclose").
   end.
 session:set-wait-state("general").
 run WindowClosed ("audit").
 publish "CloseAudit".
 session:set-wait-state("").
 cbAuditScore:screen-value in frame fbuttons = "0".
 hideReview().
 setActionWidgetState(false).
 frame fMain:sensitive = false.
 
 publish "AuditClosed".
 publish "ActiveFileChanged" (0).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closeInformal C-Win 
PROCEDURE closeInformal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOK as logical.
/*  define variable tAutoSave    as logical   no-undo. */
 define variable pFnBPChanged as character no-undo.
 define variable pCorrectCase as character no-undo.
 define variable tCloseAll as logical      no-undo.
 define variable pAuditStatus as character no-undo.
 define variable iCount       as integer   no-undo.
 define variable sectionscore as character no-undo .
 define variable tInvalidSec  as character initial "f,f,f,f,f,f,f,f" no-undo.

 publish "IsActive" (output tOk).
 if not tOk 
   then return.

 publish "SetCurrentValue" ("Report", "").
 publish "GetAuditStatus" (output pAuditStatus).
 status default "" in window {&window-name}.
 status input "" in window {&window-name}.

 tCloseAll = false.
 publish "IsChanged" (output tOk).
 if tOk 
  then
   do: 
       MESSAGE "Audit has been modified. All changes will be discarded."
         VIEW-AS ALERT-BOX question BUTTONS ok-cancel set tOK.
       if not tOK 
        then return error.
   end.

 run closeWindows in this-procedure.

 pFnBPChanged = "" . 
 pCorrectCase = "".
 publish "IsModifiedFindBp" (input "auditclose",
                            output pFnBPChanged,
                            output pCorrectCase).
 if tCloseAll = false 
  then
   do:
   if (pFnBPChanged = "auditclose" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "auditclose" )then 
   do:
     MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
             "Do you want to continue?"
       VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
     if lChoice = true then
     publish "CloseFindBp" ("auditclose").
     else 
     do:
       run EnableSave .
       return error.
     end.
     pFnBPChanged = "".
   end.
   else
     publish "CloseFindBp" ("auditclose").
 end.

 session:set-wait-state("general").
 run WindowClosed ("audit").
 publish "CloseAudit".
 session:set-wait-state("").

 hideReview().
 assign
   tAgentID:read-only in frame {&frame-name} = true
   tName:read-only in frame {&frame-name} = true
   tAddr:read-only in frame {&frame-name} = true
   tCity:read-only in frame {&frame-name} = true
   tState:read-only in frame {&frame-name} = true
   tZip:read-only in frame {&frame-name} = true
   .

 setActionWidgetState(false).

 frame fMain:sensitive = false.

 publish "AuditClosed".
 publish "ActiveFileChanged" (0).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE closewindows C-Win 
PROCEDURE closewindows :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define var tCloseAll as logical .
  def var pActive as logical .
  define variable pFnBPChanged as character.
  define variable pCorrectCase as character.
  tCloseAll = false.
  pFnBPChanged = "".
  pCorrectCase = "".

  publish "GetCloseSections" (output tCloseAll) .
  publish "IsModifiedFindBp" (input "autoclose",
                              output pFnBPChanged,
                              output pCorrectCase).
  
  if tCloseAll 
   then
    do:
      publish "isActive" (output pActive).
      if pActive = true then
        if (pFnBPChanged = "autoclose" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "autoclose" ) 
         then 
          do:
            MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
                   "Do you want to continue?"
              VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO UPDATE lChoice AS LOGICAL.
            if lChoice = true 
             then
              do:
                publish "CloseFindBp" ("autoclose").
                publish "Close" .                
              end.
            else return error.
            pFnBPChanged = "" .
          end.
      else
      do:
        publish "CloseFindBp" ("autoclose").
        publish "Close" .                                    
      end.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE displayHeader C-Win 
PROCEDURE displayHeader :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var pQarID as char.
 def var pStat as char.
 def var pAgentID as char.
 def var pName as char.
 def var pAddr as char.
 def var pCity as char.
 def var pState as char.
 def var pZip as char.

 {lib/getattrch.i qarID pQarID}
 {lib/getattrch.i stat pStat}
 {lib/getattrch.i agentID pAgentID}
 {lib/getattrch.i name pName}
 {lib/getattrch.i addr pAddr}
 {lib/getattrch.i city pCity}
 {lib/getattrch.i state pState}
 {lib/getattrch.i zip pZip}
 
 if pStat = "A" then 
   pStat = "Active" .
 else if pStat = "C" then 
   pStat = "Completed".

 assign
   tQarID:screen-value in frame fMain   = pQarID
   tStat:screen-value in frame fMain    = pStat
   tAgentID:screen-value in frame fMain = pAgentID
   tName:screen-value in frame fMain    =  pName
   tAddr:screen-value in frame fMain    = pAddr
   tCity:screen-value in frame fMain    = pCity
   tState:screen-value in frame fMain   = pState
   tZip:screen-value in frame fMain     = pZip 
   .

 run DisplayScore.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DisplayScore C-Win 
PROCEDURE DisplayScore :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 publish "SetAuditAnswer" ("Score", cbAuditScore:screen-value in frame fButtons).
 END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE EnableSave C-Win 
PROCEDURE EnableSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if tStat:screen-value in frame fMain = "Completed" then 
    return.
  assign
    menu-item m_Save:sensitive in menu m_Review = true
    bBtnSave:sensitive in frame fButtons = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  DISPLAY cbAuditScore 
      WITH FRAME fButtons IN WINDOW C-Win.
  ENABLE bBtnStart RECT-bfile-6 bBtnCalendar bBtnOpen bBtnReferences 
      WITH FRAME fButtons IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fButtons}
  DISPLAY tAgentID tName tAddr tCity tState tZip tMainOffice tNumOffices tStat 
          tNumEmployees tNumUnderwriters tContactName tContactPhone tContactFax 
          tContactOther tContactEmail tContactPosition tParentName tParentAddr 
          tParentCity tParentState tParentZip tDeliveredTo tAuditDate tAuditor 
          tRanking tServices tQarID 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE RECT-27 RECT-30 RECT-31 RECT-32 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exportAudit C-Win 
PROCEDURE exportAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  def var tOK as logical.

  publish "IsActive" (output tOk).
  if not tOk 
   then return.

  publish "IsChanged" (output tOk).
  if tOk 
  then
   do:
     MESSAGE "Audit has been modified. All changes will be saved." view-as alert-box question buttons ok-cancel set tOK.
     if not tOK 
     then return error.
   end.


  def var tFile as char.
  def var doSave as logical.

  publish "GetAuditDir" (output std-ch).
  publish "GetFilenameFormatted" ("audit", output tFile).

  system-dialog get-file tFile
    filters "QAR Files" "*.qarl"
    initial-dir std-ch
    ask-overwrite
    create-test-file
    default-extension ".qarl"
    save-as
   update doSave.

  if not doSave 
   then return.

  session:set-wait-state("general").
  publish "SaveAudit".
  publish "ExportAudit" (tFile).
  session:set-wait-state("").

  hideReview().
  setActionWidgetState(false).
  frame fMain:sensitive = false.

  publish "AuditClosed".
  publish "ActiveFileChanged" (0).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE finishAudit C-Win 
PROCEDURE finishAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tID as char no-undo.
 def var pSuccess as logical no-undo.

 {lib/getattrin.i unansweredCount std-in}
 if std-in > 0 
  then
   MESSAGE "There are still " std-in " unanswered questions." skip
            "Do you want to continue and submit the audit?"
      VIEW-AS ALERT-BOX warning BUTTONS yes-no update std-lo.
  else MESSAGE "Audit will be submitted." skip(1)
         "Do you want to continue?"
        VIEW-AS ALERT-BOX INFO BUTTONS yes-no update std-lo.
 if not std-lo 
  then return.

 run saveAudit in this-procedure.

 publish "GetActiveFilename" (output std-ch).
 std-ch = search(std-ch).
 if std-ch = ? 
  then return.

 session:set-wait-state("general").
 publish "FinishAudit" (input tQarID:screen-value in frame fMain,
                        input std-ch,
                        output pSuccess,
                        output std-ch).
 session:set-wait-state("").

 if not pSuccess 
  then
   do:
     MESSAGE "Audit submission failed." skip(1)
             std-ch
      VIEW-AS ALERT-BOX error BUTTONS OK.
     return.
   end.
 
 session:set-wait-state("general").
 run closeAudit in this-procedure.
 session:set-wait-state("").

 message "Audit successfully finished."
   view-as alert-box info buttons ok.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE generateReport C-Win 
PROCEDURE generateReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define input parameter pReportFormat as character no-undo.

  def var doSave as logical init true.
  def var tReportDir as char.
  
  std-ch = "".
  publish "GetReportDir" (output tReportDir).
  publish "GetFilenameFormatted" (pReportFormat, output std-ch).
    
  system-dialog get-file std-ch
   filters "PDF Files" "*.pdf"
   initial-dir tReportDir
   ask-overwrite
   create-test-file
   default-extension ".pdf"
   use-filename
   save-as
  update doSave.
  
  if not doSave 
   then return.
   
  publish "RunReport" (pReportFormat, std-ch).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importAudit C-Win 
PROCEDURE importAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 publish "GetAuditDir" (output std-ch).
 system-dialog get-file tFile
    filters "QAR Files" "*.qarl"
    default-extension ".qarl"
    initial-dir std-ch
   update tOK.

 if not tOK
  then return.

 tFile = search(tFile).
 if tFile = ?
  then return.

 session:set-wait-state("general").
 publish "ImportAudit" (tFile).
 session:set-wait-state("").

 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tActiveFile:screen-value in frame fButtons,tActiveFile:list-item-pairs in frame fButtons, "|") - 1, tActiveFile:list-item-pairs in frame fButtons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
apply "leave" to tNotes in frame {&frame-name}.
apply "leave" to tAgentID in frame {&frame-name}.
apply "leave" to tName in frame {&frame-name}.
apply "leave" to tAddr in frame {&frame-name}.
apply "leave" to tCity in frame {&frame-name}.
apply "leave" to tState in frame {&frame-name}.
apply "leave" to tZip in frame {&frame-name}.
apply "leave" to tNumEmployees in frame {&frame-name}.
apply "leave" to tNumUnderwriters in frame {&frame-name}.
apply "leave" to tLastRevenue in frame {&frame-name}.
apply "leave" to tNumOffices in frame {&frame-name}.
apply "leave" to tContactName in frame {&frame-name}.
apply "leave" to tContactPhone in frame {&frame-name}.
apply "leave" to tContactFax in frame {&frame-name}.
apply "leave" to tContactOther in frame {&frame-name}.
apply "leave" to tContactEmail in frame {&frame-name}.
apply "leave" to tContactPosition in frame {&frame-name}.
apply "leave" to tDeliveredTo in frame {&frame-name}.
apply "leave" to tAuditDate in frame {&frame-name}.
apply "leave" to tAuditor in frame {&frame-name}.
apply "leave" to tRanking in frame {&frame-name}.
apply "leave" to tParentName in frame {&frame-name}.
apply "leave" to tParentAddr in frame {&frame-name}.
apply "leave" to tParentCity in frame {&frame-name}.
apply "leave" to tParentState in frame {&frame-name}.
apply "leave" to tParentZip in frame {&frame-name}.
apply "leave" to cbAuditScore in frame fbuttons.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockOff C-Win 
PROCEDURE LockOff :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
assign
  tLockOn = false
  frame fMain:sensitive = true
  frame fButtons:sensitive = true
  menu m_Reports:sensitive = true
 /* menu-item m_Escrow_Reconciliation:sensitive in menu m_View = true */
  menu-item m_References:sensitive in menu m_Tools = true
  .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LockOn C-Win 
PROCEDURE LockOn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  if valid-handle(wEscrow)
   then run hideWindow in wEscrow.

  assign
    tLockOn = true
    frame fMain:sensitive = false
    frame fButtons:sensitive = false
    menu m_Review:sensitive = false
    menu m_Reports:sensitive = false
    /* menu-item m_Escrow_Reconciliation:sensitive in menu m_View = false */ 
    menu-item m_References:sensitive in menu m_Tools = false
    .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifyAudit C-Win 
PROCEDURE modifyAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var pCancelled as logical.

 def var pAgentID as char.
 def var pName as char.
 def var pAddr as char.
 def var pCity as char.
 def var pState as char.
 def var pZip as char.
 def var pContactName as char.
 def var pContactPhone as char.
 def var pContactFax as char.
 def var pContactOther as char.
 def var pContactEmail as char.
 def var pContactPosition as char.
 def var pParentName as char.
 def var pParentAddr as char.
 def var pParentCity as char.
 def var pParentState as char.
 def var pParentZip as char.
 def var pAuditDate as date.
 def var pAuditScore as int.
 def var pMainOffice as logical.
 def var pNumOffices as int.
 def var pNumEmployees as int.
 def var pNumUnderwriters as int.
 def var pLastRevenue as decimal.
 def var pAuditor as char.
 def var pRanking as int.
 def var pDeliveredTo as char.
 def var pServices as char no-undo.
  
 {lib/getattrch.i agentID pAgentID}
 {lib/getattrch.i name pName}
 {lib/getattrch.i addr pAddr}
 {lib/getattrch.i city pCity}
 {lib/getattrch.i state pState}
 {lib/getattrch.i zip pZip}
 {lib/getattrch.i contactName pContactName}
 {lib/getattrch.i contactPhone pContactPhone}
 {lib/getattrch.i contactFax pContactFax}
 {lib/getattrch.i contactOther pContactOther}
 {lib/getattrch.i contactEmail pContactEmail}
 {lib/getattrch.i contactPosition pContactPosition}
 {lib/getattrch.i parentName pParentName}
 {lib/getattrch.i parentAddr pParentAddr}
 {lib/getattrch.i parentCity pParentCity}
 {lib/getattrch.i parentState pParentState}
 {lib/getattrch.i parentZip pParentZip}
 {lib/getattrda.i auditDate pAuditDate}
 {lib/getattrlo.i mainOffice pMainOffice}
 {lib/getattrin.i numOffices pNumOffices}
 {lib/getattrin.i numEmployees pNumEmployees}
 {lib/getattrin.i numUnderwriters pNumUnderwriters}
 {lib/getattrde.i lastRevenue pLastRevenue}
 {lib/getattrin.i ranking pRanking}
 {lib/getattrch.i auditor pAuditor}
 {lib/getattrch.i deliveredTo pDeliveredTo}
 {lib/getattrch.i services pServices}

 run dialognew.w
    ("Modify Review",
     "AZ,CO,FL,KS,MO,SC,TX",
     input-output pAgentID,
     input-output pName,
     input-output pAddr,
     input-output pCity,
     input-output pState,
     input-output pZip,
     input-output pContactName,
     input-output pContactPhone,
     input-output pContactFax,
     input-output pContactOther,
     input-output pContactEmail,
     input-output pContactPosition,
     input-output pParentName,
     input-output pParentAddr,
     input-output pParentCity,
     input-output pParentState,
     input-output pParentZip,
     input-output pMainOffice,
     input-output pNumOffices,
     input-output pNumEmployees,
     input-output pNumUnderwriters,
     input-output pLastRevenue,
     input-output pAuditor,
     input-output pAuditDate,
     input-output pRanking,
     input-output pDeliveredTo,
     input-output pServices,
     output pCancelled).
 if pCancelled 
  then return.

 {lib/setattrch.i agentID pAgentID}
 {lib/setattrch.i name pName}
 {lib/setattrch.i addr pAddr}
 {lib/setattrch.i city pCity}
 {lib/setattrch.i state pState}
 {lib/setattrch.i zip pZip}
 {lib/setattrch.i contactName pContactName}
 {lib/setattrch.i contactPhone pContactPhone}
 {lib/setattrch.i contactFax pContactFax}
 {lib/setattrch.i contactOther pContactOther}
 {lib/setattrch.i contactEmail pContactEmail}
 {lib/setattrch.i contactPosition pContactPosition}
 {lib/setattrch.i parentName pParentName}
 {lib/setattrch.i parentAddr pParentAddr}
 {lib/setattrch.i parentCity pParentCity}
 {lib/setattrch.i parentState pParentState}
 {lib/setattrch.i parentZip pParentZip}
 {lib/setattrda.i auditDate pAuditDate}
 {lib/setattrlo.i mainOffice pMainOffice}
 {lib/setattrin.i numOffices pNumOffices}
 {lib/setattrin.i numEmployees pNumEmployees}
 {lib/setattrin.i numUnderwriters pNumUnderwriters}
 {lib/setattrde.i lastRevenue pLastRevenue}
 {lib/setattrin.i ranking pRanking}
 {lib/setattrch.i auditor pAuditor}
 {lib/setattrch.i deliveredTo pDeliveredTo}
 {lib/setattrch.i services pServices}

 run displayHeader in this-procedure.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE newInformal C-Win 
PROCEDURE newInformal PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tActive as logical.
 def var pAuditor as char.
publish "setcurrentvalue" ("Informal" , "I").
 publish "IsActive" (output tActive).
 if tActive
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 publish "NewAudit".
 publish "SetInformal".
   
 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}.
 {lib/setattrch.i services 'Both'}

 publish "GetCredentialsName" (output pAuditor).
 {lib/setattrch.i auditor pAuditor}

 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain = string(today)
   tServices:screen-value in frame fMain = "Both"
   tAuditor:screen-value in frame fMain = pAuditor
   cbAuditScore:screen-value in frame fbuttons = "0"
   .

 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   tAuditor
   tRanking
   tServices
   tNotes
  with frame fMain.
 enable cbAuditScore with frame fbuttons.
 assign
   tAgentID:read-only in frame {&frame-name} = false
   tName:read-only in frame {&frame-name} = false
   tAddr:read-only in frame {&frame-name} = false
   tCity:read-only in frame {&frame-name} = false
   tState:read-only in frame {&frame-name} = false
   tZip:read-only in frame {&frame-name} = false
   .

 activeAgentFileID = 0.
 run displayHeader in this-procedure.

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openAudit C-Win 
PROCEDURE openAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 run dialogopenqur.w (output std-lo, output tFile).
 if not std-lo /* Cancelled */
  then return.
 publish "SetCurrentValue" ("Informal" , "").
 session:set-wait-state("general").
 publish "OpenAudit" (tFile).
 session:set-wait-state("").

 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tactivefile:screen-value in frame fbuttons,tactivefile:list-item-pairs in frame fbuttons, "|") - 1, tactivefile:list-item-pairs in frame fbuttons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 
 activeAuditID = integer(tQarID:screen-value) no-error.

 publish "GetlocalqurFiles" (output table localqurFileInfo).
 find localqurFileInfo where localqurFileInfo.identifier = string(activeAuditID) no-error.
 if available (localqurFileInfo) 
  then
   do: 
     status default "Last saved:" + "  " + string(localqurFileInfo.modifieddate) in window {&window-name}. 
     status input "Last saved:" + "  " + string(date(localqurFileInfo.modifieddate)) + "  " +  string(time,"hh:mm:ss AM") in window {&window-name}. 
   end.
 else
  do:
   status default "" in window {&window-name}.
   status input "" in window {&window-name}.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE openInformal C-Win 
PROCEDURE openInformal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOk as logical.
 def var tFile as char.
 define variable tInitDir as character no-undo.
 publish "setcurrentvalue" ("Informal" , "I").
 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.
  
 tInitDir = os-getenv("userprofile") + "\Documents\".
 if search(tInitDir) = ?
  then os-create-dir value(tInitDir).

 system-dialog get-file tFile
   title "Select an Informal Audit"
   initial-dir tInitDir
   filters "QUR Version L" "*.qurl"
   must-exist
   update std-lo.

 if not std-lo /* Cancelled */
  then return.
  
 session:set-wait-state("general").
 publish "OpenAudit" (tFile).
 session:set-wait-state("").

 publish "IsActive" (output tOk).
 if not tOk /* Error opening audit */
  then return.
  
 publish "IsInformal" (output tOk).
 if not tOk /* not informal */
  then
   do:
    message "Please select an Informal Audit file" view-as alert-box information buttons ok.
    publish "CloseAudit".
    return.
   end.
  
 showReview().
 assign
   tAgentID:read-only in frame {&frame-name} = false
   tName:read-only in frame {&frame-name} = false
   tAddr:read-only in frame {&frame-name} = false
   tCity:read-only in frame {&frame-name} = false
   tState:read-only in frame {&frame-name} = false
   tZip:read-only in frame {&frame-name} = false
   .

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ShowTitle" ( entry( lookup(tactivefile:screen-value in frame fbuttons,tactivefile:list-item-pairs in frame fbuttons, "|") - 1, tactivefile:list-item-pairs in frame fbuttons, "|")).
 publish "ActiveFileChanged" (activeAgentFileID).
 
 activeAuditID = integer(tQarID:screen-value) no-error.

 status default "" in window {&window-name}.
 status input "" in window {&window-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PreliminaryReport C-Win 
PROCEDURE PreliminaryReport :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  run summaryrpt.p ("P").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recallAudit C-Win 
PROCEDURE recallAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*-----------not-used---------------*/
/* def var tOk as logical.
 def var tFile as char.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 run dialogrecall.w (output std-lo).
 if not std-lo /* Cancel */
  then return.

 showReview().
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
 publish "ActiveFileChanged" (activeAgentFileID).*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE restartAudit C-Win 
PROCEDURE restartAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var lChoice as logical.
 def var tFile as char.
 def var pAuditID as char.
 def var pAuditor as char.
 def var pSuccess as logical.
 def var pSelected as logical.
 def var pFullFile as char.
 def var tOk as logical.

 publish "IsActive" (output tOk).
 if tOk
  then
   do:
     MESSAGE "Please close the open Audit"
       VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

 run dialogrestartqur.w (output pSelected,
                         output pAuditID,
                         output pFullFile).
 if pSelected 
  then
   do:
    publish "RestartAudit"(input pAuditId,
                           input pFullFile,
                           output pSuccess,
                           output lChoice) .
    if not pSuccess or not lChoice  then
      return.
   end.
 else return.
 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}
 {lib/setattrch.i services 'Both'}

 publish "GetCredentialsName" (output pAuditor).
 {lib/setattrch.i auditor pAuditor}
 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain = string(today)
   tServices:screen-value in frame fMain = "title"
   tAuditor:screen-value in frame fMain = pAuditor
   .
 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   tAuditor
   tRanking   
   tNotes
   with frame fMain.

 enable cbAuditScore with frame fbuttons.
 cbAuditScore:screen-value = "0".
 activeAgentFileID = 0.
 run displayHeader in this-procedure.
 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SaveAfterEveryAnswer C-Win 
PROCEDURE SaveAfterEveryAnswer :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define variable tAutoSave as logical no-undo.
 define variable tActive as logical no-undo .
 define variable tChanged as logical no-undo .
 
 /* Informal review */
 {lib/getattrch.i auditType std-ch}
 if std-ch = ""
  then return.

 publish "GetAutoSave" (output tAutoSave).
 if tAutoSave 
  then
   do:
     publish "isActive" (output tActive).
     publish "isChanged" (output tChanged).
     if tActive = true and tChanged = true 
      then
       do:
         activeAuditID = integer(tQarID:screen-value in frame fmain).
         publish "SaveAudit" (input activeAuditID).
         showReview().
       end. 
     status default  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") in window {&window-name}.
     status input  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss am") in window {&window-name}.
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveAudit C-Win 
PROCEDURE saveAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tOk as logical.
 
 /* Informal */
 if tQarID:screen-value in frame fMain = "0"
  then
   do: run saveInformal in this-procedure.
       showReview().
       return.
   end.
   
 publish "LeaveAll" .
 session:set-wait-state("general").
 activeAuditID = integer(tQarID:screen-value in frame fmain ).
 
 publish "SaveAudit" (input integer(activeAuditID)).
 session:set-wait-state("").
 status default  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") in window {&window-name}.
 status input  "Last saved:"  + "  " + string(today,"99/99/9999") + " " + string(time,"hh:mm:ss AM") in window {&window-name}.
 assign
   menu-item m_Save:sensitive in menu m_Review = false
   bBtnSave:sensitive in frame fButtons = false
   .
 tclicksave = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saveInformal C-Win 
PROCEDURE saveInformal :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOk as logical.
 def var tFile as char no-undo.
 define variable tInitDir as character no-undo.
 
 publish "GetActiveFilename" (output tFile).

 publish "LeaveAll" .

 std-lo = false.
 file-info:file-name = tFile.
 if index(file-info:file-type, "F") > 0 
  then std-lo = true.
  
 tInitDir = os-getenv("userprofile") + "\Documents\".
 if search(tInitDir) = ?
  then os-create-dir value(tInitDir).

 if not std-lo
  then system-dialog get-file tFile
             save-as
             initial-dir tInitDir
             default-extension ".qurl"
             ask-overwrite
             filters "QUR Version L" "*.qurl"
             update std-lo.

 if not std-lo /* Cancelled */
  then return.

 session:set-wait-state("general").
 publish "SaveAuditInformal" (0, tFile).
 session:set-wait-state("").

 assign
   menu-item m_Informal_Save:sensitive in sub-menu m_Informal = false
   bBtnSave:sensitive in frame fButtons = false
   .
 tclicksave = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SelectAuditPage C-Win 
PROCEDURE SelectAuditPage :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pPage as int.
 def input parameter pfileno as char.
 activeAgentFileID = int(entry (2,pfileno,"-")).
 publish "IsActive" (output std-lo).
 if not std-lo 
  then return.

 if pPage < 1 or pPage > 11
  then return.
 if valid-handle(wSection[pPage]) then
 do: 
   publish "ShowTitle" (input entry (1,pfileno,"-")).
   run ShowWindow IN wSection[pPage].
    publish "ActiveFileChanged" (activeAgentFileID).
    tActivefile:screen-value in frame fbuttons = entry (2,pfileno,"-").
 end.
else
 do:
   run ViewSection (pPage).
   publish "ShowTitle" (input entry (1,pfileno,"-")).
   publish "ActiveFileChanged" (activeAgentFileID).
    tActivefile:screen-value in frame fbuttons = entry (2,pfileno,"-").
 end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE serverInspect C-Win 
PROCEDURE serverInspect :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tOk as logical.
 def var tFile as char.

 run dialogserverinspectqur.w(output  std-lo).
 if not std-lo /* Cancelled */
  then return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SetClickSave C-Win 
PROCEDURE SetClickSave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  tclicksave = false .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showCalendar C-Win 
PROCEDURE showCalendar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
if not valid-handle(wCalendar) 
 then run calendar.w persistent set wCalendar.
 else wCalendar:current-window:move-to-top().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showReferences C-Win 
PROCEDURE showReferences :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 if valid-handle(wReferences)
  then run showWindow in wReferences.
  else run wreferences.w persistent set wReferences.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SpellChecker C-Win 
PROCEDURE SpellChecker :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 define variable pcomment  as char no-undo.
 define variable pscore    as char no-undo.
 define variable checktext as char no-undo.
 define variable checktextnew as char    no-undo.
 define variable iCount       as int     no-undo.
 define variable actstart     as integer no-undo.
 define variable acctstart    as integer no-undo.
 define variable bpstart      as integer no-undo.
 define variable totalentries as integer no-undo.
 define variable panswer      as char    no-undO.
 define variable currentAction as char   no-undo.
 define variable widgetlist as char initial "22000,21020,21030,21040,21060,21070,21105,20045,20110" no-undo.
 

 do with frame  fMain:
 end.

 publish "GetFindBpAction" (output table finding,
                            output table bestpractice,
                            output table action,
                            output table account ).

 assign tNotes.
 checktext = tNotes.
 
 do iCount = 1 to 8:
   publish "GetSectionAnswer" (input string(iCount),
                               output pscore,
                               output pcomment).
   checktext = checktext + "|" + pcomment.
 end.

 do iCount = 1 to 9:
   publish "GetBackgroundAnswer"(input integer(entry(iCount ,widgetlist, "," )),
                                 output panswer).
   checktext = checktext + "|" + panswer                                                                                                                                                                                                                                                               .
 end.
 
 publish "GetQuestionAnswer" (1000, output panswer).
 checktext = checktext + "|" + panswer.

 iCount = 20.
 for each finding  by finding.questionSeq : 
   checktext = checktext + "|" +  replace( finding.comments, "&br;" , chr(10)).
   checktext = checktext + "|" +  replace( finding.reference, "&br;" , chr(10)).
   iCount = iCount + 2. 
 end.

 bpstart = iCount .

 for each bestpractice  by bestpractice.questionSeq : 
   checktext = checktext + "|" +  replace( bestpractice.comments, "&br;" , chr(10)).
   iCount = iCount + 1 .
 end.

 actstart = iCount . 

 for each action by  action.questionSeq :
   checktext = checktext + "|" +  replace( action.description, "&br;" , chr(10)).
   checktext = checktext + "|" + replace( action.note, "&br;" , chr(10)).
   iCount = iCount + 2 .
 end.

 acctstart = iCount.

 for each account by  account.acctNumber :
   checktext = checktext + "|" +  replace( account.comments, "&br;" , chr(10)).
   iCount = iCount + 1.
 end.

 totalentries = iCount.

 run util/spellcheck.p ({&window-name}:handle, 
                        input-output checktext, 
                        output std-lo).


 publish "SetAuditAnswer" ("Comments",  trim(entry(1,checktext, chr(7)), chr(13))).
 tNotes =  trim(entry(1,checktext, chr(7)), chr(13)).
 display tNotes with frame fmain .

 do iCount = 1 to 8:
   publish "SetSectionAnswer" (input string(iCount),
                               input "Comments",
                               input trim(entry(iCount + 1 ,checktext, chr(7)),chr(13))).
 end.

 do iCount = 1 to 9:
   publish "SetBackgroundAnswer"(input integer(entry(iCount,widgetlist, "," )),
                                 input trim(entry(iCount + 9,checktext, chr(7)),chr(13))).
 end.


 publish "SetQuestionAnswer" (1000,
                              input trim(entry(19,checktext, chr(7)),chr(13))).

 iCount = 20.

 for each finding  by finding.questionSeq : 
   finding.comments = trim( replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1.
   finding.reference =  trim(replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1. 
 end.

 iCount = bpstart .

 for each bestpractice  by bestpractice.questionSeq : 
   bestpractice.comments = trim( replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1 .
 end.

 iCount = actstart . 

 for each action by  action.questionSeq :
   action.description = trim(replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1 .
   action.note =  trim(replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1 .
 end.

 iCount = acctstart.

 for each account by  account.acctNumber :
   account.comments = trim( replace( entry(iCount,checktext, chr(7)), chr(10), "&br;"),chr(13)).
   iCount = iCount + 1.
 end.

 publish "SetFindBpAction" (input table finding,
                            input table bestpractice,
                            input table action,
                            input table account ).
 publish "SetAuditChanged"(true).

 run EnableSave.
 publish "SaveAfterEveryAnswer".
 checktext = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE startAudit C-Win 
PROCEDURE startAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tActive  as logical.
 def var pAuditor as char.

 publish "IsActive" (output tActive).
 if tActive
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.
 run dialogbeginaudit.w (output std-lo). 
publish "SetCurrentValue" ("Informal" , "").
 if std-lo = false then
   return.
   
 publish "SetDefaults".
   
 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}.
 {lib/setattrch.i services 'Both'}

 publish "GetCredentialsName" (output pAuditor).
 {lib/setattrch.i auditor pAuditor}

 assign
   tMainOffice:checked in frame fMain = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain = string(today)
   tServices:screen-value in frame fMain = "title"
   tAuditor:screen-value in frame fMain = pAuditor
   .

 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   tAuditor
   tRanking   
   tNotes
  with frame fMain.

 enable cbAuditScore with frame fbuttons.
 cbAuditScore:screen-value = "0".
 activeAgentFileID = 0.
 run displayHeader in this-procedure.

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StartNewAudit C-Win 
PROCEDURE StartNewAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tActive     as logical.
 def var pAuditor    as char.
 def var pReviewDate as date.

 publish "IsActive" (output tActive).
 if tActive
  then
   do:
     MESSAGE "Please close the open Audit"
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
     return.
   end.

  publish "NewAudit". 
  run dialognewaudit.w (output std-lo,output pReviewdate,output pAuditor). 

 if std-lo = false then
   return.
   

 {lib/setattrlo.i mainOffice true}
 {lib/setattrin.i numOffices 1}
 {lib/setattrda.i auditDate today}.
 {lib/setattrch.i services 'Both'}


 assign
   tMainOffice:checked in frame fMain      = true
   tNumOffices:screen-value in frame fMain = "1"
   tAuditDate:screen-value in frame fMain  = string(pReviewdate)
   tServices:screen-value in frame fMain   = "title"
   tAuditor:screen-value in frame fMain    = pAuditor
   .

 enable
   tAgentID
   tName
   tAddr
   tCity
   tState
   tZip
   tMainOffice
   tNumOffices
   tNumEmployees
   tNumUnderwriters
   tLastRevenue
   tParentName
   tParentAddr
   tParentCity
   tParentState
   tParentZip
   tContactName
   tContactPhone
   tContactFax
   tContactOther
   tContactEmail
   tContactPosition
   tDeliveredTo
   tAuditDate
   tAuditor
   tRanking   
   tNotes
  with frame fMain.

 activeAgentFileID = 0.
 run displayHeader in this-procedure.

 setActionWidgetState(true).
 frame fMain:sensitive = true.

 publish "AuditOpened".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewFinding C-Win 
PROCEDURE ViewFinding :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def input parameter pQuestionSeq as int.
 def input parameter pParentWindow as char.
 def output parameter pHasFinding as logical.

 def var pQuestionID as char.
 def var pDesc as char.
 def var pPriority as int.
 def var pCanChangePriority as logical.
 def var hValue as int.

 publish "GetQuestionAttributes" (input pQuestionSeq,
                                  output pQuestionID, 
                                  output pDesc,
                                  output pPriority,
                                  output pCanChangePriority,
                                  output std-ch,
                                  output std-ch,
                                  output std-ch).

 publish "GetSectionAttribute" (input substring(pQuestionID, 1, 1), 
                                input "UsesFiles",
                                output std-ch).
 if pDesc <> ""
  then 
   do:
     hValue = integer((pQuestionSeq / 5) - 199 ).
     if  not valid-handle(hFindbp[hValue]) then
     run wfindbp.w persistent set hFindbp[hValue]
                          (pQuestionSeq,
                           pQuestionID,
                           pDesc,
                           pPriority,
                           std-ch = "Y",
                           pCanChangePriority,
                           focus,
                           substring(pQuestionID, 1, 1),
                           pParentWindow).
     else 
       publish "SetFocusFindBp" (input pQuestionSeq).
   end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ViewSection C-Win 
PROCEDURE ViewSection :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pSection as int.
 def var pAuditStatus as char no-undo.
 
 if not tclicksave then
   publish "LeaveAll" .
 if valid-handle(wSection[pSection]) 
  then
   do: run MoveToTop in wSection[pSection] no-error.
       return.
   end.

 case pSection:
  when 1 
   then
    do: run wsection1.w persistent set wSection[1].
        subscribe procedure wSection[1] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[1] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[1].
    end.
  when 2 
   then
    do: run wsection2.w persistent set wSection[2].
        subscribe procedure wSection[2] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[2] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[2].
    end.
  when 3 
   then
    do: run wsection3.w persistent set wSection[3].
        subscribe procedure wSection[3] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[3] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[3].
    end.
  when 4 
   then
    do: run wsection4.w persistent set wSection[4].
        subscribe procedure wSection[4] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[4] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[4].
    end.
  when 5 
   then
    do: run wsection5.w persistent set wSection[5].
        subscribe procedure wSection[5] to "ActiveFileChanged" in this-procedure.
        if valid-handle(wSection[7]) 
         then subscribe procedure wSection[5] to "ActiveFileChanged" in wSection[7].
        subscribe procedure wSection[5] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[5].
    end.
  when 6 
   then
    do: run wsection6.w persistent set wSection[6].
        subscribe procedure wSection[6] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[6] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[6].
    end.
  when 7 
   then
    do: run wsection7.w persistent set wSection[7].
        subscribe procedure wSection[7] to "ActiveFileChanged" in this-procedure.
        if valid-handle(wSection[5]) 
         then subscribe procedure wSection[7] to "ActiveFileChanged" in wSection[5].
        subscribe procedure wSection[7] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[7].
    end.

  when 8 
   then
    do: run wsection8.w persistent set wSection[8].
        subscribe procedure wSection[8] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[8] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[8].
    end.

  when 9 
   then
    do: run wsection9.w persistent set wSection[9].
        subscribe procedure wSection[9] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[9] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[9].
    end.

  when 10 
   then
    do: run wsection10.w persistent set wSection[10].
        subscribe procedure wSection[10] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[10] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[10].
    end.

  when 11 
   then
    do: run wsection11.w persistent set wSection[11].
        subscribe procedure wSection[11] to "ActiveFileChanged" in this-procedure.
        subscribe procedure wSection[11] to "AuditClosed" in this-procedure.
        run ShowWindow IN wSection[11].
    end.
 end case.
 do WITH FRAME fButtons:
 end.
 publish "IsActive" (output std-lo).
 if std-lo 
  then run AuditOpened in wSection[pSection] no-error.
 publish "ActiveFileChanged" (activeAgentFileID).
 publish "ShowTitle" ( ENTRY( LOOKUP(tActiveFile:SCREEN-VALUE,tActiveFile:LIST-ITEM-PAIRS, "|") - 1, tActiveFile:LIST-ITEM-PAIRS, "|")).
 publish "GetAuditStatus" (output pAuditStatus).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE WindowClosed C-Win 
PROCEDURE WindowClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter pWinName as character no-undo.

case pWinName: 
  when "Escrow"     then if valid-handle(wEscrow) then     delete procedure wEscrow.
  when "Answered"   then if valid-handle(wAnswered)then    delete procedure wAnswered.
  when "Scoring"    then if valid-handle(wScoring)then     delete procedure wScoring.
  when "Findings"   then if valid-handle(wFindings)then    delete procedure wFindings.
  when "References" then if valid-handle(wReferences)then  delete procedure wReferences.
  when "DOCUMENTS"  then if valid-handle(hDocWindow)then   delete procedure hDocWindow.
  when "audit"      
   then
    do:
      if valid-handle(wEscrow) then     delete procedure wEscrow.     
      if valid-handle(wAnswered)then    delete procedure wAnswered.   
      if valid-handle(wScoring)then     delete procedure wScoring.    
      if valid-handle(wFindings)then    delete procedure wFindings.   
      if valid-handle(wReferences)then  delete procedure wReferences. 
      if valid-handle(hDocWindow)then   delete procedure hDocWindow.  
    end.
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION hideReview C-Win 
FUNCTION hideReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  clear frame fMain no-pause.
  clear frame fButtons no-pause.
  disable cbAuditScore   with frame fButtons.
 
  clear frame {&frame-name}.
  pause 0.
  
  disable
    tAgentID
    tName
    tAddr
    tCity
    tState
    tZip
    tMainOffice
    tNumOffices
    tNumEmployees
    tNumUnderwriters
    tLastRevenue
    tParentName
    tParentAddr
    tParentCity
    tParentState
    tParentZip
    tContactName
    tContactPhone
    tContactFax
    tContactOther
    tContactEmail
    tContactPosition
    tDeliveredTo
    tAuditDate
    tAuditor
    tRanking    
    tNotes
   with frame {&frame-name}.

  tDeliveredTo:screen-value = "".
  tNotes:screen-value = "" .
  tActiveFile:list-item-pairs in frame fButtons = "|0".
  disable cbAuditScore with  frame fbuttons.

  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION openWindowForAgent C-Win 
FUNCTION openWindowForAgent RETURNS HANDLE
  ( input pAgentID as character,
    input pType as character,
    input pFile as character) :
/*------------------------------------------------------------------------------
@description Opens the window if not open and calls the procedure ShowWindow
@note The second parameter is the handle to the agentdatasrv.p
------------------------------------------------------------------------------*/
  define variable hWindow as handle no-undo.
  define variable hFileDataSrv as handle no-undo.
  define variable cAgentWindow as character no-undo.
  define buffer openwindow for openwindow.

  for each openwindow:
    if not valid-handle(openwindow.procHandle) 
     then delete openwindow.
  end.
  assign
    hWindow = ?
    hFileDataSrv = ?
    cAgentWindow = pAgentID + " " + pType
    .

  if pAgentID <> "" then
    publish "OpenAgent" (pAgentID, output hFileDataSrv).

  for first openwindow no-lock
      where openwindow.procFile = cAgentWindow:
      
    hWindow = openwindow.procHandle.
  end.
  
  if not valid-handle(hWindow) then
  do:
    run value(pFile) persistent set hWindow (hFileDataSrv).

    create openwindow.
    assign openwindow.procFile   = cAgentWindow
           openwindow.procHandle = hWindow.
  end.

  run ShowWindow in hWindow no-error.
  return hWindow.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION saveCurrentField C-Win 
FUNCTION saveCurrentField RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  if valid-handle(focus)   /* maybe not if the window was just closed */
    and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
    and focus:modified     /* inconsistently set... */
  then apply "leave" to focus.

  return true. 
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION setActionWidgetState C-Win 
FUNCTION setActionWidgetState RETURNS LOGICAL PRIVATE
  ( pIsOpen as logical ) :
/*------------------------------------------------------------------------------
  Purpose:  Set the sensitive state of the menus/buttons that allow for a
            review to be opened.

    Notes:  pIsOpen = true when a review file is active
------------------------------------------------------------------------------*/
 /* Review */
  assign   
    menu-item m_Start:sensitive in menu m_Review = not pIsOpen
    menu-item m_Restart:sensitive in menu m_Review = not pIsOpen
    menu-item m_Open:sensitive in menu m_Review = not pIsOpen
    menu-item m_Close:sensitive in menu m_Review = pIsOpen
    menu-item m_Save:sensitive in menu m_Review = pIsOpen
    menu-item m_Finish:sensitive in menu m_Review = pIsOpen
  
    bBtnStart:sensitive in frame fButtons = not pIsOpen
    bBtnOpen:sensitive = not pIsOpen
    bBtnClose:sensitive = pIsOpen
    bBtnSave:sensitive = pIsOpen
    bBtnSubmit:sensitive = pIsOpen
    bBtnSpellCheck:sensitive = pIsOpen
    .
  
  /* Section */
  assign
    menu-item m_1:sensitive  in menu m_section = pIsOpen
    menu-item m_2:sensitive  in menu m_section = pIsOpen
    menu-item m_3:sensitive  in menu m_section = pIsOpen
    menu-item m_4:sensitive  in menu m_section = pIsOpen
    menu-item m_5:sensitive  in menu m_section = pIsOpen
    menu-item m_6:sensitive  in menu m_section = pIsOpen
    menu-item m_7:sensitive  in menu m_section = pIsOpen
    menu-item m_8:sensitive  in menu m_section = pIsOpen
    menu-item m_9:sensitive  in menu m_section = pIsOpen
    menu-item m_10:sensitive in menu m_section = pIsOpen
    menu-item m_11:sensitive in menu m_section = pIsOpen
  
    bSection-1:sensitive in frame  fMain = pIsOpen
    bSection-2:sensitive in frame  fMain = pIsOpen
    bSection-3:sensitive in frame  fMain = pIsOpen
    bSection-4:sensitive in frame  fMain = pIsOpen
    bSection-5:sensitive in frame  fMain = pIsOpen
    bSection-6:sensitive in frame  fMain = pIsOpen
    bSection-7:sensitive in frame  fMain = pIsOpen
    bSection-8:sensitive in frame  fMain = pIsOpen
    bSection-9:sensitive in frame  fMain = pIsOpen
    bSection-10:sensitive in frame fMain = pIsOpen
    bSection-11:sensitive in frame fMain = pIsOpen
    .
  
  /* View */
  assign   
    menu-item m_Documents:sensitive             in menu m_View = pIsOpen
    menu-item m_Agent_Files:sensitive           in menu m_View = pIsOpen
    menu-item m_background_operations:sensitive in menu m_View = pIsOpen
    bBtnFiles:sensitive = pIsOpen
    .
  
  /* Progress */
  assign   
    menu-item m_Unanswered_questions:sensitive        in menu m_Progress = pIsOpen
    menu-item m_Answered_questions:sensitive          in menu m_Progress = pIsOpen
    menu-item m_Findings_and_Best_Practices:sensitive in menu m_Progress = pIsOpen
    menu-item m_Results:sensitive                     in menu m_Progress = pIsOpen
    .
  
  /* Reports */
  assign
    menu-item m_Preliminary_Report:sensitive in menu m_Reports = pIsOpen
    menu-item m_Questionnaire:sensitive      in menu m_Reports = pIsOpen  
    menu-item m_Internal_Review:sensitive    in menu m_Reports = pIsOpen
    bBtnPrelimRpt:sensitive = pIsOpen
    .
  
  /* Actions */
  assign
    menu-item m_Cancel:sensitive in menu m_Actions = pIsOpen
    menu-item m_Import:sensitive in menu m_Actions = not pIsOpen
    menu-item m_Export:sensitive in menu m_Actions = pIsOpen
    menu-item m_Inspect:sensitive in menu m_Actions = not pIsOpen
    .
    menu-item m_Inspect:sensitive in menu m_Actions = true.
    
  /* Informal Audit */
  define variable lIsInformal as logical no-undo initial false.
  define variable cAuditType as character no-undo.
  publish "IsInformal" (output lIsInformal).
  {lib/getattrch.i auditType cAuditType}
  if cAuditType = "" or cAuditType = ?
   then
    assign
      sub-menu m_Review:sensitive = not pIsOpen
      sub-menu m_Informal:sensitive = true
      menu-item m_Cancel:sensitive in menu m_Actions = not pIsOpen and lIsInformal
      menu-item m_Import:sensitive in menu m_Actions = not pIsOpen and lIsInformal
      menu-item m_Export:sensitive in menu m_Actions = pIsOpen and lIsInformal
/*       menu-item m_Inspect:sensitive in menu m_Actions = not pIsOpen and lIsInformal */
      menu-item m_Documents:sensitive in menu m_View = not pIsOpen
      menu-item m_Informal_New:sensitive in sub-menu m_Informal = not pIsOpen
      menu-item m_Informal_Open:sensitive in sub-menu m_Informal = not pIsOpen
      menu-item m_Informal_Close:sensitive in sub-menu m_Informal = pIsOpen
      menu-item m_Informal_Save:sensitive in sub-menu m_Informal = pIsOpen
      bBtnSubmit:sensitive = not pIsOpen
      bBtnPrelimRpt:sensitive = not pIsOpen
      .
   else sub-menu m_Informal:sensitive = false.
return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION showReview C-Win 
FUNCTION showReview RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  do with frame {&frame-name}:
    {lib/getattrch.i agentID std-ch}
    tAgentID:screen-value = std-ch.
    {lib/getattrch.i name std-ch}
    tName:screen-value = std-ch.
    {lib/getattrch.i addr std-ch}
    tAddr:screen-value = std-ch.
    {lib/getattrch.i city std-ch}
    tCity:screen-value = std-ch.
    {lib/getattrch.i state std-ch}
    tState:screen-value = std-ch.
    {lib/getattrch.i zip std-ch}
    tZip:screen-value = std-ch.

    {lib/getattrch.i contactName std-ch}
    tContactName:screen-value = std-ch.
    {lib/getattrch.i contactPhone std-ch}
    tContactPhone:screen-value = std-ch.
    {lib/getattrch.i contactFax std-ch}
    tContactFax:screen-value = std-ch.
    {lib/getattrch.i contactOther std-ch}
    tContactOther:screen-value = std-ch.
    {lib/getattrch.i contactEmail std-ch}
    tContactEmail:screen-value = std-ch.
    {lib/getattrch.i contactPosition std-ch}
    tContactPosition:screen-value = std-ch.

    {lib/getattrch.i parentName std-ch}
    tParentName:screen-value = std-ch.
    {lib/getattrch.i parentAddr std-ch}
    tParentAddr:screen-value = std-ch.
    {lib/getattrch.i parentCity std-ch}
    tParentCity:screen-value = std-ch.
    {lib/getattrch.i parentState std-ch}
    tParentState:screen-value = std-ch.
    {lib/getattrch.i parentZip std-ch}
    tParentZip:screen-value = std-ch.

    {lib/getattrch.i deliveredTo std-ch}
    tDeliveredTo:screen-value = std-ch.
    {lib/getattrda.i auditDate std-da}
    tAuditDate:screen-value = string(std-da).
    {lib/getattrch.i auditor std-ch}
    tAuditor:screen-value = std-ch.
    {lib/getattrin.i ranking std-in}
    tRanking:screen-value = string(std-in).

    {lib/getattrin.i numOffices std-in}
    tNumOffices:screen-value = string(std-in).
    {lib/getattrin.i numEmployees std-in}
    tNumEmployees:screen-value = string(std-in).
    {lib/getattrin.i numUnderwriters std-in}
    tNumUnderwriters:screen-value = string(std-in).

    {lib/getattrch.i auditScore std-ch}
    if std-ch > ""
     then cbAuditScore:screen-value in frame fbuttons = string(std-ch).
     else cbAuditScore:screen-value in frame fbuttons  = "0".

    publish "GetBackgroundAnswer" (20000, output std-ch).
    tLastRevenue:screen-value = std-ch.

    {lib/getattrlo.i mainOffice std-lo}
    tMainOffice:checked = std-lo.

    {lib/getattrch.i services std-ch}
    tServices:screen-value = std-ch.

    publish "GetQuestionAnswer" (1000, output tNotes).
    tNotes:screen-value = tNotes.

    enable
      tAgentID
      tName
      tAddr
      tCity
      tState
      tZip
      tMainOffice
      tNumOffices
      tNumEmployees
      tNumUnderwriters
      tParentName
      tParentAddr
      tParentCity
      tParentState
      tParentZip
      tContactName
      tContactPhone
      tContactFax
      tContactOther
      tContactEmail
      tContactPosition
      tDeliveredTo
      tAuditDate
      tAuditor
      tRanking      
      tNotes
    with frame {&frame-name}.
    enable cbAuditScore with  frame fbuttons.

    publish "GetAgentFileList" (output std-ch).
    activeAgentFileID = int(entry(2, std-ch, "|")).
    tActiveFile:list-item-pairs in frame fButtons = std-ch.
    if std-ch = "|0"
      then tActiveFile:sensitive = false.
    else tActiveFile:sensitive = true.
    tActiveFile:screen-value = string(activeAgentFileID).
  end.

  run displayHeader in this-procedure.
  return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

