&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/* dialogoptions.w
------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
    File        : dialogoptions.w
    Description : Provide options to configue setiings
    Modification:
    Date          Name      Description
    01/03/2018    AG        Added new inforrmal audit directory option
  ----------------------------------------------------------------------*/
{lib/std-def.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bReportDir-2 tCloseSections tAutoSave ~
tStates tOtherStates tExportDir tReportDir tInformalDir bReportDir Btn_OK ~
Btn_Cancel bSharingDir 
&Scoped-Define DISPLAYED-OBJECTS tCloseSections tAutoSave tStates ~
tOtherStates tExportDir tReportDir tInformalDir 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bAddState 
     LABEL "<--" 
     SIZE 8 BY .95 TOOLTIP "Add a State to the Search options".

DEFINE BUTTON bDeleteState 
     LABEL "-->" 
     SIZE 8 BY .95 TOOLTIP "Remove a State from the Search options".

DEFINE BUTTON bReportDir  NO-FOCUS
     LABEL "Select" 
     SIZE 4.8 BY 1.1 TOOLTIP "Select a reports directory".

DEFINE BUTTON bReportDir-2  NO-FOCUS
     LABEL "Select" 
     SIZE 4.8 BY 1.1 TOOLTIP "Select a Informal Audit reports directory".

DEFINE BUTTON bSharingDir  NO-FOCUS
     LABEL "Select" 
     SIZE 4.8 BY 1.1 TOOLTIP "Select a default sharing directory".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Save" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE tExportDir AS CHARACTER FORMAT "X(256)":U 
     LABEL "Sharing Directory" 
     VIEW-AS FILL-IN 
     SIZE 61 BY 1 TOOLTIP "Select a default directory to import/export" NO-UNDO.

DEFINE VARIABLE tInformalDir AS CHARACTER FORMAT "X(256)":U 
     LABEL "Informal Audit Directory" 
     VIEW-AS FILL-IN 
     SIZE 61 BY 1 TOOLTIP "Select the directory where reports of informal audits are saved" NO-UNDO.

DEFINE VARIABLE tReportDir AS CHARACTER FORMAT "X(256)":U 
     LABEL "Reports Directory" 
     VIEW-AS FILL-IN 
     SIZE 61 BY 1 TOOLTIP "Select the directory where reports are saved" NO-UNDO.

DEFINE RECTANGLE RECT-31
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 84 BY 6.91.

DEFINE RECTANGLE RECT-40
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 91.6 BY 17.38.

DEFINE VARIABLE tOtherStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Alabama","AL",
                     "Alaska","AK",
                     "Arizona","AZ",
                     "Arkansas","AR",
                     "California","CA",
                     "Connecticut","CT",
                     "Delaware","DE",
                     "Florida","FL",
                     "Georgia","GA",
                     "Hawaii","HI",
                     "Idaho","ID",
                     "Illinois","IL",
                     "Indiana","IN",
                     "Iowa","IA",
                     "Kansas","KS",
                     "Kentucky","KY",
                     "Louisiana","LA",
                     "Maine","ME",
                     "Maryland","MD",
                     "Massachusetts","MA",
                     "Michigan","MI",
                     "Minnesota","MN",
                     "Mississippi","MS",
                     "Missouri","MO",
                     "Montana","MT",
                     "Nebraska","NE",
                     "Nevada","NV",
                     "New Hampshire","NH",
                     "New Jersey","NJ",
                     "New Mexico","NM",
                     "New York","NY",
                     "North Carolina","NC",
                     "North Dakota","ND",
                     "Ohio","OH",
                     "Oklahoma","OK",
                     "Oregon","OR",
                     "Pennsylvania","PA",
                     "Rhode Island","RI",
                     "South Carolina","SC",
                     "South Dakota","SD",
                     "Tennessee","TN",
                     "Texas","TX",
                     "Utah","UT",
                     "Vermont","VT",
                     "Virginia","VA",
                     "Washington","WA",
                     "West Virginia","WV",
                     "Wisconsin","WI",
                     "Wyoming","WY" 
     SIZE 24 BY 5.95 NO-UNDO.

DEFINE VARIABLE tStates AS CHARACTER 
     VIEW-AS SELECTION-LIST SINGLE NO-DRAG SORT SCROLLBAR-VERTICAL 
     LIST-ITEM-PAIRS "Colorado","CO" 
     SIZE 25 BY 5.95 NO-UNDO.

DEFINE VARIABLE tAutoSave AS LOGICAL INITIAL no 
     LABEL "Automatically save after every answer" 
     VIEW-AS TOGGLE-BOX
     SIZE 44 BY .81 NO-UNDO.

DEFINE VARIABLE tCloseSections AS LOGICAL INITIAL no 
     LABEL "Close section windows when the review is closed" 
     VIEW-AS TOGGLE-BOX
     SIZE 54 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bReportDir-2 AT ROW 15.1 COL 88.2 WIDGET-ID 122 NO-TAB-STOP 
     tCloseSections AT ROW 2.57 COL 9 WIDGET-ID 62
     tAutoSave AT ROW 3.52 COL 9 WIDGET-ID 90
     tStates AT ROW 5.62 COL 18.2 NO-LABEL WIDGET-ID 74
     tOtherStates AT ROW 5.62 COL 53.2 NO-LABEL WIDGET-ID 82
     bAddState AT ROW 7.52 COL 44.2 WIDGET-ID 78
     bDeleteState AT ROW 8.48 COL 44.2 WIDGET-ID 80
     tExportDir AT ROW 12.95 COL 25 COLON-ALIGNED WIDGET-ID 94
     tReportDir AT ROW 14.05 COL 25 COLON-ALIGNED WIDGET-ID 104
     tInformalDir AT ROW 15.14 COL 25 COLON-ALIGNED WIDGET-ID 124
     bReportDir AT ROW 14.05 COL 88 WIDGET-ID 102 NO-TAB-STOP 
     Btn_OK AT ROW 16.95 COL 30
     Btn_Cancel AT ROW 16.95 COL 48
     bSharingDir AT ROW 12.95 COL 88.2 WIDGET-ID 100 NO-TAB-STOP 
     "General" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 1.19 COL 4.2 WIDGET-ID 66
          FONT 6
     "States" VIEW-AS TEXT
          SIZE 8 BY .62 AT ROW 4.81 COL 8.2 WIDGET-ID 120
          FONT 6
     RECT-40 AT ROW 1.48 COL 2.4 WIDGET-ID 64
     RECT-31 AT ROW 5.14 COL 6 WIDGET-ID 68
     SPACE(5.19) SKIP(7.18)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Configuration"
         DEFAULT-BUTTON Btn_Cancel CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bAddState IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON bDeleteState IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-31 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-40 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Configuration */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bAddState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bAddState Dialog-Frame
ON CHOOSE OF bAddState IN FRAME Dialog-Frame /* <-- */
DO:
  if tOtherStates:input-value = ?
   then return.
  tStates:add-last(entry(lookup(tOtherStates:input-value, tOtherStates:list-item-pairs) - 1, tOtherStates:list-item-pairs), 
                   tOtherStates:input-value).
  tOtherStates:delete(tOtherStates:input-value).
  bAddState:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bDeleteState
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bDeleteState Dialog-Frame
ON CHOOSE OF bDeleteState IN FRAME Dialog-Frame /* --> */
DO:
 if tStates:input-value = ?
  then return.
 tOtherStates:add-last(entry(lookup(tStates:input-value, tStates:list-item-pairs) - 1, tStates:list-item-pairs), 
                  tStates:input-value).
 tStates:delete(tStates:input-value).
 bDeleteState:sensitive = false.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReportDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReportDir Dialog-Frame
ON CHOOSE OF bReportDir IN FRAME Dialog-Frame /* Select */
DO:
  std-ch = "".
  system-dialog get-dir std-ch title "Select Reports Directory".
  if std-ch <> "" 
   then tReportDir:screen-value in frame {&frame-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReportDir-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReportDir-2 Dialog-Frame
ON CHOOSE OF bReportDir-2 IN FRAME Dialog-Frame /* Select */
DO:
  std-ch = "".
  system-dialog get-dir std-ch title "Select Reports Directory".
  if std-ch <> ""  
   then 
  do:
   if std-ch begins (os-getenv("userprofile") + "\AppData") then
   do:
     MESSAGE "The selected directory is invalid. It will be changed to user's documents folder."
         VIEW-AS ALERT-BOX Warning BUTTONS OK.
     std-ch = os-getenv("userprofile") + "\Documents\".
  end.
     tInformalDir:screen-value in frame {&frame-name} = std-ch.
  end.
     
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bSharingDir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bSharingDir Dialog-Frame
ON CHOOSE OF bSharingDir IN FRAME Dialog-Frame /* Select */
DO:
  std-ch = "".
  system-dialog get-dir std-ch title "Select Sharing Directory".
  if std-ch <> "" 
   then tExportDir:screen-value in frame {&frame-name} = std-ch.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tOtherStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates Dialog-Frame
ON DEFAULT-ACTION OF tOtherStates IN FRAME Dialog-Frame
DO:
  apply "CHOOSE" to bAddState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tOtherStates Dialog-Frame
ON VALUE-CHANGED OF tOtherStates IN FRAME Dialog-Frame
DO:
  bAddState:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStates
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates Dialog-Frame
ON DEFAULT-ACTION OF tStates IN FRAME Dialog-Frame
DO:
 apply "CHOOSE" to bDeleteState.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStates Dialog-Frame
ON VALUE-CHANGED OF tStates IN FRAME Dialog-Frame
DO:
  bDeleteState:sensitive = true.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

publish "GetCloseSections" (output tCloseSections).
publish "GetAutoSave" (output tAutoSave).
publish "GetReportDir" (output tReportDir).
publish "GetAuditDir" (output tExportDir).
publish "GetReportDirInformal" (output tInformalDir).
publish "GetSearchStates" (output std-ch).

if lookup("CO", tOtherStates:list-item-pairs) = 0 
   and lookup("CO", std-ch) = 0
 then tOtherStates:add-last("Colorado", "CO").
do std-in = 2 to num-entries(std-ch) by 2:
 tOtherStates:delete(entry(std-in, std-ch)).
end.
tStates:list-item-pairs = std-ch.


bSharingDir:load-image("images/s-tempfiles.bmp").
bReportDir:load-image("images/s-tempfiles.bmp").
bReportDir-2:load-image("images/s-tempfiles.bmp").
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.

  if tReportDir:screen-value > ""
  then do:
    publish "SetReportDir" (tReportDir:screen-value).
    if error-status:error 
    then do: 
      message "Invalid Directory."
          view-as alert-box error buttons ok.
      undo MAIN-BLOCK, retry MAIN-BLOCK.
    end.
  end.
  if tInformalDir:screen-value > ""
  then do:
    publish "SetReportDirInformal" (tInformalDir:screen-value).
    if error-status:error 
    then do: 
      message "Invalid Directory"
          view-as alert-box error buttons ok.
      undo MAIN-BLOCK, retry MAIN-BLOCK.
    end.
  end.

  if tExportDir:screen-value > ""
  then do:
    publish "SetAuditDir" (tExportDir:screen-value).
    if error-status:error 
    then do: 
      message "Invalid Directory"
        view-as alert-box error buttons ok.
      undo MAIN-BLOCK, retry MAIN-BLOCK.
    end.
  end.
  publish "SetAutoSave" (tAutoSave:checked).
  publish "SetCloseSections" (tCloseSections:checked).
  publish "SetSearchStates" (tStates:list-item-pairs).
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCloseSections tAutoSave tStates tOtherStates tExportDir tReportDir 
          tInformalDir 
      WITH FRAME Dialog-Frame.
  ENABLE bReportDir-2 tCloseSections tAutoSave tStates tOtherStates tExportDir 
         tReportDir tInformalDir bReportDir Btn_OK Btn_Cancel bSharingDir 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

