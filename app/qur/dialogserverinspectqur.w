&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------
  File: dialogserverinspectqur.w
  Description: Inspect audit form server
  Author: AG
  Created: 02.21.2017
  Modified:
    Date          Name      Description
    03/20/2017    AG        Filters Added.
    04/05/2017    AG        Modified filters
    09/11/2017    AG        Alternate color code in browse.
------------------------------------------------------------------------*/
def output parameter pOpen as logical init false.

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/qaraudit.i &tableAlias="ttallcompletedaudit"}
{tt/qaraudit.i}          /* audit     */
def temp-table allcompletedaudit like ttallcompletedaudit.
{tt/agent.i}
{tt/state.i}

def var agentpair as char no-undo.
def var tYear as char no-undo.
def var pState as char no-undo.
def var tAgent as char no-undo.
def var iBgcolor as int no-undo.
def var openqar as logical.
def var qarsummaryhandle as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME brwAudit

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES allcompletedaudit

/* Definitions for BROWSE brwAudit                                      */
&Scoped-define FIELDS-IN-QUERY-brwAudit allcompletedaudit.cqarID allcompletedaudit.audittype allcompletedaudit.agentId allcompletedaudit.name allcompletedaudit.StateId allcompletedaudit.auditStartDate allcompletedaudit.auditFinishDate   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwAudit   
&Scoped-define SELF-NAME brwAudit
&Scoped-define QUERY-STRING-brwAudit for each allcompletedaudit where allcompletedaudit.stat = "C" by allcompletedaudit.auditStartDate descending                                        by allcompletedaudit.agentID
&Scoped-define OPEN-QUERY-brwAudit open query {&SELF-NAME} for each allcompletedaudit where allcompletedaudit.stat = "C" by allcompletedaudit.auditStartDate descending                                        by allcompletedaudit.agentID.
&Scoped-define TABLES-IN-QUERY-brwAudit allcompletedaudit
&Scoped-define FIRST-TABLE-IN-QUERY-brwAudit allcompletedaudit


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-brwAudit}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bReset RECT-33 cYear tStateId Agents ~
brwAudit Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS cYear tStateId Agents 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getaudittype Dialog-Frame 
FUNCTION getaudittype RETURNS CHARACTER
  ( ptype as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse Dialog-Frame 
FUNCTION refreshBrowse RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bReset  NO-FOCUS
     LABEL "Reset" 
     SIZE 4.8 BY 1.14 TOOLTIP "Reset local active files list".

DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Close" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Open" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE Agents AS CHARACTER 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN AUTO-COMPLETION
     SIZE 66 BY 1 NO-UNDO.

DEFINE VARIABLE cYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 11.4 BY 1 NO-UNDO.

DEFINE VARIABLE tStateId AS CHARACTER FORMAT "X(256)":U 
     LABEL "State" 
     VIEW-AS COMBO-BOX INNER-LINES 15
     DROP-DOWN-LIST
     SIZE 17.6 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 122.4 BY 2.14.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwAudit FOR 
      allcompletedaudit SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwAudit Dialog-Frame _FREEFORM
  QUERY brwAudit DISPLAY
      allcompletedaudit.cqarID          label "QUR ID"      format "x(20)"     width 10
allcompletedaudit.audittype       label "Audit Type"  format "x(10)"     width 12
allcompletedaudit.agentId         label "Agent ID"    format "x(15)"     
allcompletedaudit.name            label "Agent Name"  format "x(55)"     width 38  
allcompletedaudit.StateId         label "State"
allcompletedaudit.auditStartDate  label "Start Date"  format "9999/99/99" width 15
allcompletedaudit.auditFinishDate label "Finish Date" format "9999/99/99" width 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 122 BY 9 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     bReset AT ROW 1.81 COL 119.2 WIDGET-ID 14 NO-TAB-STOP 
     cYear AT ROW 1.91 COL 7.2 COLON-ALIGNED WIDGET-ID 4
     tStateId AT ROW 1.91 COL 21 WIDGET-ID 42
     Agents AT ROW 1.91 COL 45.8 WIDGET-ID 46
     brwAudit AT ROW 3.71 COL 123.6 RIGHT-ALIGNED WIDGET-ID 200
     Btn_OK AT ROW 13 COL 44
     Btn_Cancel AT ROW 13 COL 65
     RECT-33 AT ROW 1.33 COL 2.6 WIDGET-ID 2
     SPACE(0.59) SKIP(10.91)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Inspect Completed Audit"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
/* BROWSE-TAB brwAudit Agents Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR COMBO-BOX Agents IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* SETTINGS FOR BROWSE brwAudit IN FRAME Dialog-Frame
   ALIGN-R                                                              */
ASSIGN 
       brwAudit:ALLOW-COLUMN-SEARCHING IN FRAME Dialog-Frame = TRUE
       brwAudit:COLUMN-RESIZABLE IN FRAME Dialog-Frame       = TRUE.

/* SETTINGS FOR BUTTON Btn_OK IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR COMBO-BOX tStateId IN FRAME Dialog-Frame
   ALIGN-L                                                              */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwAudit
/* Query rebuild information for BROWSE brwAudit
     _START_FREEFORM
open query {&SELF-NAME} for each allcompletedaudit where allcompletedaudit.stat = "C" by allcompletedaudit.auditStartDate descending
                                       by allcompletedaudit.agentID.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwAudit */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Inspect Completed Audit */
DO:
  apply "END-ERROR":U to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Agents Dialog-Frame
ON VALUE-CHANGED OF Agents IN FRAME Dialog-Frame /* Agent */
DO:
  if Agents:screen-value = tAgent then.
  else
   refreshBrowse().
   tAgent = Agents:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bReset
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bReset Dialog-Frame
ON CHOOSE OF bReset IN FRAME Dialog-Frame /* Reset */
DO:
  if cYear:screen-value = ? 
   then
    do:
      MESSAGE "Please enter valid year."
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
      return no-apply.
    end.

  publish "LoadCompletedAudits" (integer(cYear:screen-value) ,
                                 tStateId:screen-value,
                                 Agents:screen-value,
                                 output table ttallcompletedaudit).
  for each ttallcompletedaudit:
    create allcompletedaudit.
    buffer-copy ttallcompletedaudit to allcompletedaudit.
    assign
      allcompletedaudit.cqarID     = string(ttallcompletedaudit.qarID)
      allcompletedaudit.audittype  = getaudittype(ttallcompletedaudit.audittype)  .
  end.

  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
  if can-find(first allcompletedaudit) then 
    Btn_OK:sensitive in frame {&frame-name} = true.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwAudit
&Scoped-define SELF-NAME brwAudit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON DEFAULT-ACTION OF brwAudit IN FRAME Dialog-Frame
DO: 
  apply "choose":U to btn_OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON ROW-DISPLAY OF brwAudit IN FRAME Dialog-Frame
DO:
  if current-result-row("brwAudit") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.

  allcompletedaudit.cqarID            :bgcolor in browse brwAudit = iBgcolor.
  allcompletedaudit.agentId          :bgcolor in browse brwAudit = iBgcolor.
  allcompletedaudit.audittype        :bgcolor in browse brwAudit = iBgcolor.      
  allcompletedaudit.name             :bgcolor in browse brwAudit = iBgcolor.
  allcompletedaudit.auditStartDate   :bgcolor in browse brwAudit = iBgcolor.
  allcompletedaudit.auditFinishDate  :bgcolor in browse brwAudit = iBgcolor.
  allcompletedaudit.StateId          :bgcolor in browse brwAudit = iBgcolor.

end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON START-SEARCH OF brwAudit IN FRAME Dialog-Frame
DO:
 {lib/brw-startSearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwAudit Dialog-Frame
ON VALUE-CHANGED OF brwAudit IN FRAME Dialog-Frame
DO:
  enable Btn_OK with frame Dialog-Frame.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Open */
DO:
  if available allcompletedaudit then
    run doOpen (allcompletedaudit.qarid).
      
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cYear
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cYear Dialog-Frame
ON VALUE-CHANGED OF cYear IN FRAME Dialog-Frame /* Year */
DO:
 if cYear:screen-value = tYear then.
 else
  refreshBrowse().
  tYear = cYear:screen-value.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tStateId
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tStateId Dialog-Frame
ON VALUE-CHANGED OF tStateId IN FRAME Dialog-Frame /* State */
DO:
  if tStateId:screen-value <> pState
   then refreshBrowse().
  pState = tStateId:screen-value.
  assign {&SELF-NAME}.
  dataSortBy = "".
  dataSortDesc = no.
  run sortData ("stateID").

  run AgentComboState in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

bReset:load-image("images/s-refresh.bmp").

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  {lib/get-state-list.i &combo=tStateId &addAll=true}
  {lib/get-agent-list.i &combo=Agents &state=tStateID}

  publish "GetAgents" (output table agent).
  Agents:delimiter = {&msg-dlm}.
 
  cYear:add-first(string(year(today) + 1)).
  do std-in = 1 to 10:
    cYear:add-last(string(year(today) + 1 - std-in)).
  end.
  run enable_UI.
  {lib/set-current-value.i &state=tStateId}
  
  cYear:screen-value = string(year(now) + 1).
  tAgent = Agents:screen-value.
  tYear = cYear:screen-value.
  pState = tStateId:screen-value.

  cYear:inner-lines = 10.

  if can-find(first allcompletedaudit) then 
    Btn_OK:sensitive in frame {&frame-name} = true.

  wait-for go of frame {&frame-name}.
END.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doOpen Dialog-Frame 
PROCEDURE doOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def input parameter pID as char.

  find first allcompletedaudit 
   where allcompletedaudit.qarid = integer(pid) 
   exclusive-lock no-error.
  if available(allcompletedaudit) 
   then
    do:
      create audit . 
      buffer-copy allcompletedaudit to audit.
      publish "checkOpenQur" (input audit.QarId,
                           output openqar,
                           output qarsummaryhandle ).
      if openqar
       then
        do:
          run ViewWindow in qarsummaryhandle no-error.
          empty temp-table audit.
          return.
        end.
      else
       do:
         {lib/pbshow.i "''"}
         {lib/pbupdate.i "'Fetching Audit, please wait...'" 0}
         {lib/pbupdate.i "'Fetching Audit, please wait...'" 20}
         run QURSummary.w persistent ( input table audit ).
         run ViewWindow in qarsummaryhandle no-error.   
         {lib/pbupdate.i "'Fetching Audit, please wait...'" 100}
         {lib/pbhide.i}
         empty temp-table audit.
       end.
      pOpen = true.
    end.
  else
  do:
    MESSAGE "Unable to start audit " pID "." skip(1)
            std-ch
      VIEW-AS ALERT-BOX INFO BUTTONS OK.
  
    apply "window-close" to frame dialog-frame. /* Leave this procedure */ 
  end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cYear tStateId Agents 
      WITH FRAME Dialog-Frame.
  ENABLE bReset RECT-33 cYear tStateId Agents brwAudit Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData Dialog-Frame 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData.i } 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getaudittype Dialog-Frame 
FUNCTION getaudittype RETURNS CHARACTER
  ( ptype as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  if ptype = "Q" then
  return "QAR".   /* Function return value. */
  if ptype = "T" then
  return "TOR".   /* Function return value. */
  else if ptype = "E" then
  return "ERR".   /* Function return value. */
  else if ptype = "U" then
  return "UW".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse Dialog-Frame 
FUNCTION refreshBrowse RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 empty temp-table allcompletedaudit.
 close query brwAudit.
 open query brwAudit for each allcompletedaudit where allcompletedaudit.stat = "C" by allcompletedaudit.auditStartDate descending
                                   by allcompletedaudit.agentID.
 Btn_OK:sensitive in frame {&frame-name} = false.
 return "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

