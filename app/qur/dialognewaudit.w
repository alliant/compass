&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: dialognewaudit.w

  Description: Create and start new audit .

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: Anjly Chanana

  Created: 03-27-2017
  
  Modified:
    Date          Name      Description
    03/29/2017    AC        Minor screen changes.
    04/03/2017    AG        Added agents for selected states only
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

def output parameter pOpen as logical init false.
def output parameter pReviewdate as date.
def output parameter pAuditor as char.

def var states as char.
 
/* Local Variable Definitions ---                                       */

{lib/std-def.i}
{tt/qaraudit.i &tableAlias="allqueuedaudit"}
{tt/qaraudit.i}          /* audit     */

{tt/auditor.i}
{tt/agent.i}

def var agentpair as char.
def var auditorpair as char.
def var RevierDate as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-41 fYear Agents cAuditors fRevierDate ~
bCreate BtnCancel 
&Scoped-Define DISPLAYED-OBJECTS fYear Agents cAuditors fRevierDate 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bCreate AUTO-GO 
     LABEL "Create" 
     SIZE 15 BY 1.14.

DEFINE BUTTON BtnCancel AUTO-END-KEY DEFAULT 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE Agents AS CHARACTER FORMAT "X(256)":U 
     LABEL "Agent" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 98.4 BY 1 NO-UNDO.

DEFINE VARIABLE cAuditors AS CHARACTER FORMAT "X(256)":U 
     LABEL "Auditor" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     LIST-ITEM-PAIRS "a","a"
     DROP-DOWN-LIST
     SIZE 42 BY 1 NO-UNDO.

DEFINE VARIABLE fRevierDate AS DATE FORMAT "99/99/99":U 
     LABEL "Target Review Date" 
     VIEW-AS FILL-IN 
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE VARIABLE fYear AS CHARACTER FORMAT "X(256)":U 
     LABEL "Year" 
     VIEW-AS FILL-IN 
     SIZE 14.4 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-41
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 120 BY 5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     fYear AT ROW 1.76 COL 21.6 COLON-ALIGNED WIDGET-ID 6
     Agents AT ROW 2.81 COL 21.6 COLON-ALIGNED WIDGET-ID 22
     cAuditors AT ROW 3.91 COL 21.6 COLON-ALIGNED WIDGET-ID 14
     fRevierDate AT ROW 5 COL 21.6 COLON-ALIGNED WIDGET-ID 4
     bCreate AT ROW 6.86 COL 44.4 WIDGET-ID 10
     BtnCancel AT ROW 6.86 COL 63.4 WIDGET-ID 24
     RECT-41 AT ROW 1.48 COL 3 WIDGET-ID 12
     SPACE(1.39) SKIP(1.84)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "New Audit"
         CANCEL-BUTTON BtnCancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* New Audit */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Agents
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Agents Dialog-Frame
ON VALUE-CHANGED OF Agents IN FRAME Dialog-Frame /* Agent */
DO:
    /*   Agents:tooltip = ENTRY( LOOKUP(Agents:SCREEN-VALUE,Agents:LIST-ITEM-PAIRS, "|") - 1, Agents:LIST-ITEM-PAIRS, "|"). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bCreate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bCreate Dialog-Frame
ON CHOOSE OF bCreate IN FRAME Dialog-Frame /* Create */
DO:
  run createaudit .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnCancel Dialog-Frame
ON CHOOSE OF BtnCancel IN FRAME Dialog-Frame /* Cancel */
DO:
  pOpen = false.
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cAuditors
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cAuditors Dialog-Frame
ON VALUE-CHANGED OF cAuditors IN FRAME Dialog-Frame /* Auditor */
DO:
    pAuditor =  ENTRY( LOOKUP(cAuditors:screen-value in frame {&frame-name},cAuditors:LIST-ITEM-PAIRS, "|") - 1, cAuditors:LIST-ITEM-PAIRS, "|").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fRevierDate
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fRevierDate Dialog-Frame
ON LEAVE OF fRevierDate IN FRAME Dialog-Frame /* Target Review Date */
DO:
   pReviewdate = date(fRevierDate:screen-value).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  
  publish "GetAuditors"(output table auditor).

  cAuditors:delimiter = "|" .
  for each auditor:
    auditorpair  = auditorpair + "|" + string(auditor.name) + "|" + string(auditor.UID) .
  end.
  auditorpair = trim(auditorpair , " |" ).
  cAuditors:list-item-pairs = auditorpair.

  publish "GetAgents" (output table agent).

  Agents:delimiter = {&msg-dlm}.
  {lib/get-agent-list.i &combo=Agents}
  cAuditors:inner-lines = 10.
  RUN enable_UI.

  fYear:screen-value = string(year(now)).
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CreateAudit Dialog-Frame 
PROCEDURE CreateAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var pAuditId as int.
def var pSuccess as logical.
def var pMsg as char.
do with frame Dialog-Frame:
end.
assign
  cAuditors = cAuditors:screen-value.
  fRevierDate = datetime(fRevierDate:screen-value).
  fYear = fYear:screen-value.
  Agents = Agents:screen-value.

run server/newaudit.p( input integer(fYear),
                       input Agents,
                       input fRevierDate,
                       input cAuditors,
                       output pAuditId,
                       output pSuccess,
                       output pMsg).
if not pSuccess then
do:
  MESSAGE pMsg
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
  return .
end.


run server/queueaudit.p(input pAuditId,
                        input cAuditors,
                        output pSuccess,
                        output pMsg).
if not pSuccess then
do:
  MESSAGE pMsg
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
  return .
end.

find first audit no-error.
if not available audit then
  create audit.
assign
  audit.qarID   = pAuditId
  audit.agentID = Agents
  audit.stat    = "Q".

find agent where agent.agentID = Agents no-error.
if available agent then
  assign
    audit.name  = agent.name 
    audit.addr  = agent.addr1 
    audit.city  = agent.city 
    audit.state = agent.state
    audit.zip   = agent.zip.

 publish "StartAudit" (input pAuditId,
                       input table audit,
                       output pSuccess).
 pOpen = pSuccess.
if not pSuccess then
do:
  MESSAGE "Unable to start audit " pAuditId "." skip(1)
          std-ch
    VIEW-AS ALERT-BOX INFO BUTTONS OK. 
  apply "window-close" to frame dialog-frame. /* Leave this procedure */
end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fYear Agents cAuditors fRevierDate 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-41 fYear Agents cAuditors fRevierDate bCreate BtnCancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

