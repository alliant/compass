&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
{lib/std-def.i}
{tt/qaraudit.i &tableAlias="qarauditgot" }          /* audit     */
{tt/qarauditsection.i &tableAlias="sectiongot"}   /* section   */
{tt/qarquestiond.i &tableAlias="questiongot"}  /* question  */
{tt/qarauditanswer.i &tableAlias="answergot"}  /* question  */
{tt/qarauditfinding.i &tableAlias="findinggot"}   /* finding */
{tt/qarauditbp.i &tableAlias="bestpracticegot"}        /* bestPractice */
{tt/qarauditfile.i &tableAlias="agentFilegot"}      /* agentFile */
{tt/qarauditaction.i &tableAlias="qaractiongot" }    /* action */
{tt/qaraccount.i  &tableAlias="escrowaccounts"}
{tt/qarnote.i &tableAlias="qarnotes"}
{tt/sysuser.i}
def var hServer as handle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-emptyDataset) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE emptyDataset Procedure 
PROCEDURE emptyDataset :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 empty temp-table qarauditgot.      
 empty temp-table sectiongot.  
 empty temp-table questiongot.
 empty temp-table findinggot.  
 empty temp-table bestpracticegot.  
 empty temp-table agentFilegot.  
 empty temp-table qaractiongot.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-GetQarNotes) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetQarNotes Procedure 
PROCEDURE GetQarNotes :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def input parameter pAuditId as int no-undo.
def output parameter table for qarnotes.

def var pSuccess as logical no-undo.
def var pMsg as character no-undo.

run server/getauditnotes.p (input pAuditId,
                            output table qarnotes,
                            output pSuccess,
                            output pMsg).

if not pSuccess
then
 do:
   message pmsg view-as alert-box error buttons ok.
   return error .
 end.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-InspectAudit) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE InspectAudit Procedure 
PROCEDURE InspectAudit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pauditID as char.
 def output parameter table for qarauditgot.    
 def output parameter table for sectiongot.     
 def output parameter table for questiongot.
 def output parameter table for answergot.
 def output parameter table for agentFilegot.    
 def output parameter table for findinggot.      
 def output parameter table for qaractiongot.    
 def output parameter table for bestPracticegot.   
 def output parameter table for escrowaccounts. 
 def output parameter table for qarnotes.   
 def output parameter table for sysuser. 
 def output parameter pSuccess as logical init false.

 session:set-wait-state("general").
 run server/inspectaudit.p (input integer(pAuditID),
                            output table qarauditgot,     
                            output table sectiongot,      
                            output table questiongot,   
                            output table answergot, 
                            output table agentFilegot,    
                            output table findinggot,      
                            output table qaractiongot,    
                            output table bestPracticegot, 
                            output table escrowaccounts,
                            output table qarnotes,
                            output table sysuser,
                            output pSuccess,
                            output std-ch).
 session:set-wait-state("").

 if not pSuccess
  then
   do:
     message std-ch view-as alert-box error buttons ok.
     return error .
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

