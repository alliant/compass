&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------
    File        : wsection3.w
    Description : Show questions of section3
    Modification:
    Date          Name      Description
    01/04/2018    AG        Changed to show app icon in window title
  ----------------------------------------------------------------------*/

CREATE WIDGET-POOL.

define variable pFnBPChanged as character no-undo .
define variable pCorrectCase as character no-undo .
define variable pValidScore as logical no-undo .

{lib/std-def.i}
{lib/winshowscrollbars.i}

{lib/questiondef.i &seq=1065}
{lib/questiondef.i &seq=1070}
{lib/questiondef.i &seq=1075}
{lib/questiondef.i &seq=1080}
{lib/questiondef.i &seq=1085}
{lib/questiondef.i &seq=1090}
{lib/questiondef.i &seq=1095}
{lib/questiondef.i &seq=1100}
{lib/questiondef.i &seq=1105}
{lib/questiondef.i &seq=1110}
{lib/questiondef.i &seq=1115}
def var activeFileID as int.
def var score as char.
def var chComments as char.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tComments 
&Scoped-Define DISPLAYED-OBJECTS tComments 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE tComments AS CHARACTER 
     VIEW-AS EDITOR SCROLLBAR-VERTICAL
     SIZE 134.4 BY 3
     BGCOLOR 15 FONT 18 NO-UNDO.

DEFINE VARIABLE title1 AS CHARACTER FORMAT "X(256)":U INITIAL "title1" 
      VIEW-AS TEXT 
     SIZE 149.6 BY 1
     FONT 10 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     tComments AT ROW 4 COL 20 NO-LABEL WIDGET-ID 26
     title1 AT ROW 1.48 COL 2.4 COLON-ALIGNED NO-LABEL WIDGET-ID 28 NO-TAB-STOP 
     "Comments:" VIEW-AS TEXT
          SIZE 15 BY .62 AT ROW 4.57 COL 4.4 WIDGET-ID 24
          FONT 18
     "Section" VIEW-AS TEXT
          SIZE 10.2 BY .62 AT ROW 3.86 COL 4.4 WIDGET-ID 22
          FONT 18
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158 BY 26.29 WIDGET-ID 100.

DEFINE FRAME fSection
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 7.29
         SCROLLABLE SIZE 158 BY 20 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Section 3"
         HEIGHT             = 26.29
         WIDTH              = 158
         MAX-HEIGHT         = 26.29
         MAX-WIDTH          = 158
         VIRTUAL-HEIGHT     = 26.29
         VIRTUAL-WIDTH      = 158
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         PRIVATE-DATA       = "3"
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME fSection:FRAME = FRAME fMain:HANDLE.

/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR FILL-IN title1 IN FRAME fMain
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FRAME fSection
                                                                        */
ASSIGN 
       FRAME fSection:HEIGHT           = 20
       FRAME fSection:WIDTH            = 158.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Section 3 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON LEAVE OF C-Win /* Section 3 */
DO:
  run LeaveAll in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Section 3 */
DO:
 pFnBPChanged = "".
 pCorrectCase = "".
 if valid-handle(focus) /* maybe not if the window was just closed */
  and (focus:type = "fill-in" or focus:type = "editor") /* all other types ok */
  and focus:modified /* inconsistently set... */
  then apply "leave" to focus.

 publish "IsModifiedFindBp" (input self:private-data,
                            output pFnBPChanged,
                            output pCorrectCase).
 if (pFnBPChanged = "3" and pCorrectCase = "") or (pCorrectCase = "Not" and pFnBPChanged = "3" ) 
  then 
   do:
     MESSAGE "Information on Finding and Best Practices has been modified. All changes will be discarded." skip 
             "Do you want to continue?"
       view-as alert-box question buttons yes-no update lChoice as logical.
     if lChoice = true 
      then
       do:
         publish "CloseFindBp" (self:private-data).
         publish "SetFnBPChanged" (input "").
       end.
     else return.
      pFnBPChanged = "".
      pCorrectCase = "".
   end.
 else
   publish "CloseFindBp" (self:private-data).
   publish "SetFnBPChanged" (input "").
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Section 3 */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tComments
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tComments C-Win
ON LEAVE OF tComments IN FRAME fMain
DO:
  if not self:modified then return.
  publish "SetSectionAnswer" ("3", "Comments", self:screen-value).
  publish "SaveAfterEveryAnswer" .
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.
{lib/win-main.i}
subscribe to "ShowTitle" anywhere.
{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

subscribe to "Close" anywhere.  
subscribe to "LeaveAll" anywhere .
subscribe to "CheckSectionScore" anywhere.
run initializeFrame in this-procedure.
on 'value-changed':u anywhere
do:
  publish "EnableSave".
  publish "SetClickSave".
end.
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActiveFileChanged C-Win 
PROCEDURE ActiveFileChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pID as int.
 
 activeFileID = pID.
 if activeFileID = 0
  then run auditClosed in this-procedure.
  else run auditOpened in this-procedure.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 
 {lib/unsetquestion.i &seq=1065}
 {lib/unsetquestion.i &seq=1070}
 {lib/unsetquestion.i &seq=1075}
 {lib/unsetquestion.i &seq=1080}
 {lib/unsetquestion.i &seq=1085}
 {lib/unsetquestion.i &seq=1090}
 {lib/unsetquestion.i &seq=1095}
 {lib/unsetquestion.i &seq=1100}
 {lib/unsetquestion.i &seq=1105}
 {lib/unsetquestion.i &seq=1110}
 {lib/unsetquestion.i &seq=1115} 
 assign
   tComments:screen-value in frame fMain = ""
   tComments:sensitive in frame fMain = false
   frame fSection:sensitive = false
   {&WINDOW-NAME}:title = "Section 3"
   .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 if activeFileID = 0
  then return.
 
 {lib/dispfileanswer.i &seq=1065}
 {lib/dispfileanswer.i &seq=1070}
 {lib/dispfileanswer.i &seq=1075}
 {lib/dispfileanswer.i &seq=1080}
 {lib/dispfileanswer.i &seq=1085}
 {lib/dispfileanswer.i &seq=1090}
 {lib/dispfileanswer.i &seq=1095}
 {lib/dispfileanswer.i &seq=1100}
 {lib/dispfileanswer.i &seq=1105}
 {lib/dispfileanswer.i &seq=1110}
 {lib/dispfileanswer.i &seq=1115}

 publish "getSectionAnswer" (input "3", 
                            output score,
                            output chComments).
                                                        

 assign
   tComments:screen-value in frame fMain = chComments
   tComments:sensitive in frame fMain = true
   frame fSection:sensitive = true
   .
 publish "GetAuditStatus" (output pAuditStatus).
 
 if pAuditStatus = "C" then
   run AuditReadOnly.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditReadOnly C-Win 
PROCEDURE AuditReadOnly :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*------------not used-------------*/
/* do with frame {&frame-name}:
 end.
 assign
   tComments:sensitive = false
   tScore:sensitive    = false. */ 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Close C-Win 
PROCEDURE Close :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  define variable tautosave as logical no-undo.
  publish "GetAutosave" (output tautosave).
  if tautosave then                                          
    publish "SaveAfterEveryAnswer".
  apply 'CLOSE' to this-procedure .
  return no-apply.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doButton C-Win 
PROCEDURE doButton :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dobutton.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doCheckbox C-Win 
PROCEDURE doCheckbox :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 {lib/docheckboxf.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE doFillin C-Win 
PROCEDURE doFillin :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/dofillin.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tComments 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tComments 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW FRAME fSection IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fSection}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE initializeFrame C-Win 
PROCEDURE initializeFrame PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  {lib/question.i &seq=1065 &r="1.72"  &ex-y=true &ex-u=true &ex-a=true &ex-b=true &ex-n=true}
  {lib/question.i &seq=1070 &r="1.72 + 2.5" &ex-u=true}
  {lib/question.i &seq=1075 &r="1.72 + 5.0" &ex-u=true}
  {lib/question.i &seq=1080 &r="1.72 + 7.5" &ex-u=true}
  {lib/question.i &seq=1085 &r="1.72 + 10.0" &ex-u=true}  
  {lib/question.i &seq=1090 &r="1.72 + 12.5" &ex-u=true}
  {lib/question.i &seq=1095 &r="1.72 + 15.0" &ex-u=true}
  {lib/question.i &seq=1100 &r="1.72 + 17.5" &ex-u=true}
  {lib/question.i &seq=1105 &r="1.72 + 20.0" &ex-u=true}
  {lib/question.i &seq=1110 &r="1.72 + 22.5" &ex-u=true}
  {lib/question.i &seq=1115 &r="1.72 + 25" &ex-u=true}
  
  {lib/dispsectiont.i &id="3"}
  
  run ShowScrollBars(frame fSection:handle, no, yes).

  run AuditOpened in this-procedure.    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LeaveAll C-Win 
PROCEDURE LeaveAll :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 apply "leave" to tComments in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowWindow C-Win 
PROCEDURE ShowWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  def var tFlag as logical.
  tFlag = tComments:sensitive in frame fMain.
  tComments:sensitive in frame fMain  = true.
  apply "ENTRY" to tComments in frame fMain.
  tComments:sensitive in frame fMain = tFlag.
  if {&window-name}:window-state eq window-minimized  then
   {&window-name}:window-state = window-normal .

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ShowTitle C-Win 
PROCEDURE ShowTitle :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 define input parameter fname as character .
 if fname = ? and activeFileID = 0 then
   assign {&window-name}:title = "Section 3 " .
 else
   assign {&window-name}:title = "Section 3 File - " + fname .
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.

 title1:width-pixels = frame fMain:width-pixels - 40.
 tComments:width-pixels = frame fMain:width-pixels - 115.

 if {&window-name}:width-pixels > frame fSection:width-pixels 
  then
   do:
     frame fSection:width-pixels = {&window-name}:width-pixels.
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
   end.
  else
   do:
     frame fSection:virtual-width-pixels = {&window-name}:width-pixels.
     frame fSection:width-pixels = {&window-name}:width-pixels.
         /* das: For some reason, shrinking a window size MAY cause the horizontal
            scroll bar.  The above sequence of widget setting should resolve it,
            but it doesn't every time.  So... */
     run ShowScrollBars(frame fSection:handle, no, yes).
   end.

 frame fSection:height-pixels = {&window-name}:height-pixels - 132.
 
 {lib/resizequestion.i &seq=1065}
 {lib/resizequestion.i &seq=1070}
 {lib/resizequestion.i &seq=1075}
 {lib/resizequestion.i &seq=1080}
 {lib/resizequestion.i &seq=1085}
 {lib/resizequestion.i &seq=1090}
 {lib/resizequestion.i &seq=1095}
 {lib/resizequestion.i &seq=1100}
 {lib/resizequestion.i &seq=1105}
 {lib/resizequestion.i &seq=1110}
 {lib/resizequestion.i &seq=1115}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

