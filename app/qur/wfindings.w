&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/* wfindings.w
    Modification:
    Date          Name      Description
    02/03/2017    AG        Changed to implement sorting in browse.
    09/11/2017    AG        Alternate color code in browse.
------------------------------------------------------------------------*/

create widget-pool.
{lib/std-def.i}

{tt/qarrptfinding.i}
{tt/qarrptbp.i}
{lib/brw-multi-def.i}

def var hasfinding as logical.
def var iBgcolor as int no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwBestPractices

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES rptBestPractice rptFinding

/* Definitions for BROWSE brwBestPractices                              */
&Scoped-define FIELDS-IN-QUERY-brwBestPractices rptBestPractice.questionID rptBestPractice.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwBestPractices   
&Scoped-define SELF-NAME brwBestPractices
&Scoped-define QUERY-STRING-brwBestPractices preselect EACH rptBestPractice by rptBestPractice.questionSeq
&Scoped-define OPEN-QUERY-brwBestPractices open query {&SELF-NAME}  preselect EACH rptBestPractice by rptBestPractice.questionSeq.
&Scoped-define TABLES-IN-QUERY-brwBestPractices rptBestPractice
&Scoped-define FIRST-TABLE-IN-QUERY-brwBestPractices rptBestPractice


/* Definitions for BROWSE brwFindings                                   */
&Scoped-define FIELDS-IN-QUERY-brwFindings rptFinding.priorityDesc rptFinding.questionID rptFinding.files rptFinding.comments   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwFindings   
&Scoped-define SELF-NAME brwFindings
&Scoped-define QUERY-STRING-brwFindings preselect each rptFinding  by rptFinding.questionPriority descending  by rptFinding.questionSeq
&Scoped-define OPEN-QUERY-brwFindings open query {&SELF-NAME} preselect each rptFinding  by rptFinding.questionPriority descending  by rptFinding.questionSeq.
&Scoped-define TABLES-IN-QUERY-brwFindings rptFinding
&Scoped-define FIRST-TABLE-IN-QUERY-brwFindings rptFinding


/* Definitions for FRAME fMain                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fMain ~
    ~{&OPEN-QUERY-brwBestPractices}~
    ~{&OPEN-QUERY-brwFindings}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bRefresh tAuto brwFindings brwBestPractices 
&Scoped-Define DISPLAYED-OBJECTS tAuto tFindingsLabel tBestPracticesLabel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bRefresh 
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the findings and best practices".

DEFINE VARIABLE tBestPracticesLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Best Practices" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tFindingsLabel AS CHARACTER FORMAT "X(256)":U INITIAL "Findings" 
      VIEW-AS TEXT 
     SIZE 14 BY .62 NO-UNDO.

DEFINE VARIABLE tAuto AS LOGICAL INITIAL no 
     LABEL "Auto Refresh" 
     VIEW-AS TOGGLE-BOX
     SIZE 17 BY .81 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwBestPractices FOR 
      rptBestPractice SCROLLING.

DEFINE QUERY brwFindings FOR 
      rptFinding SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwBestPractices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwBestPractices C-Win _FREEFORM
  QUERY brwBestPractices DISPLAY
      rptBestPractice.questionID label "Question" format "x(15)"
 rptBestPractice.comments label "Comments" format "x(150)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104 BY 5.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.

DEFINE BROWSE brwFindings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwFindings C-Win _FREEFORM
  QUERY brwFindings DISPLAY
      rptFinding.priorityDesc label "Priority" format "x(12)"     
 rptFinding.questionID label "Question" format "x(15)"
 rptFinding.files label "Files" format "x(30)"
 rptFinding.comments label "Comments" format "x(150)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104 BY 5.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     bRefresh AT ROW 1.24 COL 2 WIDGET-ID 4
     tAuto AT ROW 1.71 COL 10 WIDGET-ID 2
     brwFindings AT ROW 3.86 COL 2 WIDGET-ID 200
     brwBestPractices AT ROW 10.76 COL 2 WIDGET-ID 300
     tFindingsLabel AT ROW 3.14 COL 2.2 NO-LABEL WIDGET-ID 14
     tBestPracticesLabel AT ROW 10.05 COL 2.2 NO-LABEL WIDGET-ID 16
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 106 BY 15.95 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Findings and Best Practices"
         HEIGHT             = 15.95
         WIDTH              = 106
         MAX-HEIGHT         = 22.33
         MAX-WIDTH          = 112
         VIRTUAL-HEIGHT     = 22.33
         VIRTUAL-WIDTH      = 112
         SHOW-IN-TASKBAR    = no
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwFindings tAuto fMain */
/* BROWSE-TAB brwBestPractices brwFindings fMain */
ASSIGN 
       brwBestPractices:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwBestPractices:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

ASSIGN 
       brwFindings:ALLOW-COLUMN-SEARCHING IN FRAME fMain = TRUE
       brwFindings:COLUMN-RESIZABLE IN FRAME fMain       = TRUE.

/* SETTINGS FOR FILL-IN tBestPracticesLabel IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
/* SETTINGS FOR FILL-IN tFindingsLabel IN FRAME fMain
   NO-ENABLE ALIGN-L                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwBestPractices
/* Query rebuild information for BROWSE brwBestPractices
     _START_FREEFORM
open query {&SELF-NAME}
 preselect EACH rptBestPractice by rptBestPractice.questionSeq.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwBestPractices */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwFindings
/* Query rebuild information for BROWSE brwFindings
     _START_FREEFORM
open query {&SELF-NAME} preselect each rptFinding
 by rptFinding.questionPriority descending
 by rptFinding.questionSeq.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brwFindings */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Findings and Best Practices */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Findings and Best Practices */
DO:
  /* This event will close the window and terminate the procedure.  */
  run hideWindow.
/*   APPLY "CLOSE":U TO THIS-PROCEDURE. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Findings and Best Practices */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bRefresh
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bRefresh C-Win
ON CHOOSE OF bRefresh IN FRAME fMain /* Refresh */
DO:
  refreshBrowse().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBestPractices
&Scoped-define SELF-NAME brwBestPractices
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBestPractices C-Win
ON DEFAULT-ACTION OF brwBestPractices IN FRAME fMain
DO:  
  if available rptBestPractice
  then publish "ViewFinding" (input rptBestPractice.questionSeq, "WFindings", output hasfinding).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBestPractices C-Win
ON ROW-DISPLAY OF brwBestPractices IN FRAME fMain
DO:
  if current-result-row("brwBestPractices") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  rptBestPractice.questionID:bgcolor in browse brwBestPractices = iBgColor.
  rptBestPractice.comments:bgcolor in browse brwBestPractices = iBgColor.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwBestPractices C-Win
ON START-SEARCH OF brwBestPractices IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwBestPractices }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwFindings
&Scoped-define SELF-NAME brwFindings
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON DEFAULT-ACTION OF brwFindings IN FRAME fMain
DO:
  if available rptFinding
   then publish "ViewFinding" (input rptFinding.questionSeq, "WFindings", output hasfinding).  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON ROW-DISPLAY OF brwFindings IN FRAME fMain
DO:
  if current-result-row("brwFindings") modulo 2 = 0 then
    iBgColor = 17.
  else
    iBgColor = 15.
  rptFinding.priorityDesc :bgcolor in browse brwFindings = iBgColor.
  rptFinding.questionID  :bgcolor in browse brwFindings = iBgColor.
  rptFinding.files  :bgcolor in browse brwFindings = iBgColor.
  rptFinding.comments  :bgcolor in browse brwFindings = iBgColor.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwFindings C-Win
ON START-SEARCH OF brwFindings IN FRAME fMain
DO:
  {lib/brw-startSearch-multi.i &browse-name = brwFindings}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tAuto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tAuto C-Win
ON VALUE-CHANGED OF tAuto IN FRAME fMain /* Auto Refresh */
DO:
  if self:checked 
   then 
    do: subscribe to "AuditChanged" anywhere.
        bRefresh:sensitive in frame fMain = false.
    end.
   else 
    do: unsubscribe to "AuditChanged".
        bRefresh:sensitive in frame fMain = true.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwBestPractices
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

bRefresh:load-image("images/refresh.bmp").
bRefresh:load-image-insensitive("images/refresh-i.bmp").

subscribe to "AuditOpened" anywhere.
subscribe to "AuditClosed" anywhere.
refreshBrowse().

MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditChanged C-Win 
PROCEDURE AuditChanged :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pNew as logical.

 refreshBrowse().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditClosed C-Win 
PROCEDURE AuditClosed :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AuditOpened C-Win 
PROCEDURE AuditOpened :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tAuto tFindingsLabel tBestPracticesLabel 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE bRefresh tAuto brwFindings brwBestPractices 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE hideWindow C-Win 
PROCEDURE hideWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {&window-name}:visible = false.
 publish "WindowClosed" (input "Findings").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE showWindow C-Win 
PROCEDURE showWindow :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 refreshBrowse().
 {&window-name}:visible = true.
 {&window-name}:move-to-top().
 publish "WindowOpened" (input "Findings").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortData-multi.i }
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /* Window is shrinking... */
 if frame fMain:height-pixels > {&window-name}:height-pixels
  then
   do:
     brwFindings:width-pixels = {&window-name}:width-pixels - 10.
     brwBestPractices:width-pixels = {&window-name}:width-pixels - 10.
     
     std-in = round(({&window-name}:height-pixels - 85) / 2, 0).
     
     brwFindings:height-pixels = std-in.
     brwBestPractices:height-pixels = std-in.
     brwBestPractices:y = {&window-name}:height-pixels - std-in - 5.
     
     tBestPracticesLabel:y = browse brwBestPractices:y - 15.
     
     frame fMain:width-pixels = {&window-name}:width-pixels.
     frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
     frame fMain:height-pixels = {&window-name}:height-pixels.
     frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

   end.
  else
   do:

     frame fMain:width-pixels = {&window-name}:width-pixels.
     frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
     frame fMain:height-pixels = {&window-name}:height-pixels.
     frame fMain:virtual-height-pixels = {&window-name}:height-pixels.
     
     brwFindings:width-pixels = frame fmain:width-pixels - 10.
     brwBestPractices:width-pixels = frame fMain:width-pixels - 10.
     
     std-in = round((frame fMain:height-pixels - 85) / 2, 0).
     
     brwFindings:height-pixels = std-in.
     brwBestPractices:height-pixels = std-in.
     brwBestPractices:y = frame fMain:height-pixels - std-in - 5.
     
     tBestPracticesLabel:y = browse brwBestPractices:y - 15.
   end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshBrowse C-Win 
FUNCTION refreshBrowse RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 close query brwFindings.
 close query brwBestPractices.

 publish "GetFindings" (output table rptFinding).
 publish "GetBestPractices" (output table rptBestPractice).

 for each rptFinding:
  rptFinding.comments = replace(rptFinding.comments, "&br;", " ").
 end.
 for each rptBestPractice:
  rptBestPractice.comments = replace(rptBestPractice.comments, "&br;", " ").
 end.

 open query brwFindings 
  preselect each rptFinding 
   by rptFinding.questionPriority descending
   by rptFinding.questionID.
 open query brwBestPractices 
  preselect each rptBestPractice by rptBestPractice.questionID.

 return true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

