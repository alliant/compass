&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/

def input parameter pApp as char no-undo. /* Application ID */
def input parameter pServerPath as char no-undo.
def input parameter pDesc as char no-undo. /* UI Transaction description */
def input parameter pValidActions as char no-undo. /* new,delete,open,modify,email */

/* Used to call Compass server to hold reference that docs exist */
def input parameter pEntityType as char no-undo.
def input parameter pEntityID as char no-undo.
def input parameter pEntitySeq as int no-undo.


CREATE WIDGET-POOL.

{lib/std-def.i}

{tt/doc.i}
{tt/doc.i &tableAlias="tempdoc"}

{lib/urlencode.i}
{lib/winlaunch.i}

 DEFINE MENU docpopmenu TITLE "Actions"
  MENU-ITEM m_PopOpen LABEL "Open"
  RULE
  menu-item m_PopDelete label "Delete..."
  menu-item m_PopModify label "Categorize..."
  RULE
  menu-item m_PopNew label "Upload..."
  .

 ON 'choose':U OF menu-item m_PopOpen in menu docpopmenu
 DO:
  run ActionOpen in this-procedure (output std-lo).
  RETURN.
 END.

 ON 'choose':U OF menu-item m_PopDelete in menu docpopmenu
 DO:
  run ActionDelete in this-procedure.
  RETURN.
 END.

 ON 'choose':U OF menu-item m_PopModify in menu docpopmenu
 DO:
  run ActionCategorize in this-procedure.
  RETURN.
 END.

 ON 'choose':U OF menu-item m_PopNew in menu docpopmenu
 DO:
  run ActionNew in this-procedure.
  RETURN.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain
&Scoped-define BROWSE-NAME brwDocs

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES document

/* Definitions for BROWSE brwDocs                                       */
&Scoped-define FIELDS-IN-QUERY-brwDocs document.displayName document.details document.createdDate document.fileSizeDesc document.createdBy   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brwDocs   
&Scoped-define SELF-NAME brwDocs
&Scoped-define QUERY-STRING-brwDocs FOR EACH document by document.displayName
&Scoped-define OPEN-QUERY-brwDocs OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
&Scoped-define TABLES-IN-QUERY-brwDocs document
&Scoped-define FIRST-TABLE-IN-QUERY-brwDocs document


/* Definitions for FRAME fMain                                          */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS tCategory brwDocs B4 
&Scoped-Define DISPLAYED-OBJECTS tCategory 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD refreshData C-Win 
FUNCTION refreshData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as char,
    output pMsg as char )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON B1  NO-FOCUS
     LABEL "Open" 
     SIZE 7.2 BY 1.71 TOOLTIP "Open the selected document(s)".

DEFINE BUTTON B2  NO-FOCUS
     LABEL "Upload" 
     SIZE 7.2 BY 1.71 TOOLTIP "Upload a new document".

DEFINE BUTTON B3  NO-FOCUS
     LABEL "Del" 
     SIZE 7.2 BY 1.71 TOOLTIP "Remove the previously saved document(s)".

DEFINE BUTTON B4  NO-FOCUS
     LABEL "Refresh" 
     SIZE 7.2 BY 1.71 TOOLTIP "Refresh the list of documents".

DEFINE BUTTON B5  NO-FOCUS
     LABEL "Categorize" 
     SIZE 7.2 BY 1.71 TOOLTIP "Modify the category of the selected document(s)".

DEFINE VARIABLE tCategory AS CHARACTER FORMAT "X(256)":U 
     LABEL "Default Category" 
     VIEW-AS FILL-IN 
     SIZE 38 BY 1 TOOLTIP "Category associated to newly added documents" NO-UNDO.

DEFINE RECTANGLE rButtons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36.6 BY 2.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brwDocs FOR 
      document SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brwDocs C-Win _FREEFORM
  QUERY brwDocs DISPLAY
      document.displayName format "x(256)" width 50 label "File"
 document.details format "x(200)" width 25 label "Category"
 document.createdDate format "x(25)" label "Date Saved"
 document.fileSizeDesc format "x(15)" width 10 label "Size"
       document.createdBy format "x(25)" label "Saved By"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE DROP-TARGET SIZE 147.6 BY 13.95 ROW-HEIGHT-CHARS .81 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     B1 AT ROW 1.14 COL 1.6 WIDGET-ID 24
     tCategory AT ROW 1.57 COL 54.6 COLON-ALIGNED WIDGET-ID 40
     brwDocs AT ROW 3.05 COL 1 WIDGET-ID 200
     B5 AT ROW 1.14 COL 22.8 WIDGET-ID 36
     B4 AT ROW 1.14 COL 29.8 WIDGET-ID 32
     B2 AT ROW 1.14 COL 8.6 WIDGET-ID 4
     B3 AT ROW 1.14 COL 15.6 WIDGET-ID 6
     rButtons AT ROW 1 COL 1 WIDGET-ID 22
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 147.6 BY 16 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Documents"
         HEIGHT             = 16
         WIDTH              = 147.6
         MAX-HEIGHT         = 19.05
         MAX-WIDTH          = 149.6
         VIRTUAL-HEIGHT     = 19.05
         VIRTUAL-WIDTH      = 149.6
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* BROWSE-TAB brwDocs tCategory fMain */
/* SETTINGS FOR BUTTON B1 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       B1:PRIVATE-DATA IN FRAME fMain     = 
                "Open".

/* SETTINGS FOR BUTTON B2 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       B2:PRIVATE-DATA IN FRAME fMain     = 
                "New".

/* SETTINGS FOR BUTTON B3 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       B3:PRIVATE-DATA IN FRAME fMain     = 
                "Delete".

ASSIGN 
       B4:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

/* SETTINGS FOR BUTTON B5 IN FRAME fMain
   NO-ENABLE                                                            */
ASSIGN 
       B5:PRIVATE-DATA IN FRAME fMain     = 
                "Email".

ASSIGN 
       brwDocs:COLUMN-RESIZABLE IN FRAME fMain       = TRUE
       brwDocs:COLUMN-MOVABLE IN FRAME fMain         = TRUE.

/* SETTINGS FOR RECTANGLE rButtons IN FRAME fMain
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brwDocs
/* Query rebuild information for BROWSE brwDocs
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH document by document.displayName.
     _END_FREEFORM
     _Query            is NOT OPENED
*/  /* BROWSE brwDocs */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Documents */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Documents */
DO:
  publish "WindowClosed" ("DOCUMENTS").

  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-RESIZED OF C-Win /* Documents */
DO:
  run windowResized in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B1 C-Win
ON CHOOSE OF B1 IN FRAME fMain /* Open */
DO:
   run ActionOpen in this-procedure (output std-lo).
   if std-lo 
    then apply "WINDOW-CLOSE" to frame {&frame-name}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B2 C-Win
ON CHOOSE OF B2 IN FRAME fMain /* Upload */
DO:
  run ActionNew in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B3 C-Win
ON CHOOSE OF B3 IN FRAME fMain /* Del */
DO:
  run ActionDelete in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B4 C-Win
ON CHOOSE OF B4 IN FRAME fMain /* Refresh */
DO:
  refreshData().
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B5 C-Win
ON CHOOSE OF B5 IN FRAME fMain /* Categorize */
DO:
  run ActionCategorize in this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brwDocs
&Scoped-define SELF-NAME brwDocs
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DEFAULT-ACTION OF brwDocs IN FRAME fMain
DO:
   run ActionOpen in this-procedure (output std-lo).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON DROP-FILE-NOTIFY OF brwDocs IN FRAME fMain
DO:
  run ActionFilesDropped in this-procedure (self:handle).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON ROW-DISPLAY OF brwDocs IN FRAME fMain
DO:
  {lib/brw-rowdisplay.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON START-SEARCH OF brwDocs IN FRAME fMain
DO:
  {lib/brw-startsearch.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brwDocs C-Win
ON VALUE-CHANGED OF brwDocs IN FRAME fMain
DO:
  if brwDocs:num-selected-rows > 0 
   then assign
          b1:sensitive = lookup("open", pValidActions) > 0
          b3:sensitive = lookup("delete", pValidActions) > 0
          b5:sensitive = lookup("modify", pValidActions) > 0 
          menu-item m_PopOpen:sensitive in menu docpopmenu = b1:sensitive
          menu-item m_PopDelete:sensitive in menu docpopmenu = b3:sensitive
          menu-item m_PopModify:sensitive in menu docpopmenu = b5:sensitive
          .
   else assign
          b1:sensitive = false
          b3:sensitive = false
          b5:sensitive = false
          menu-item m_PopOpen:sensitive in menu docpopmenu = false
          menu-item m_PopDelete:sensitive in menu docpopmenu = false
          menu-item m_PopModify:sensitive in menu docpopmenu = false
          .
  b2:sensitive in frame {&frame-name} = lookup("new", pValidActions) > 0.
  menu-item m_PopNew:sensitive in menu docpopmenu = b2:sensitive.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

{lib/win-main.i}
{lib/brw-main.i}

if pDesc > "" 
 then {&window-name}:title = {&window-name}:title + " for " + pDesc.

{&window-name}:min-height-pixels = {&window-name}:height-pixels.
{&window-name}:min-width-pixels = {&window-name}:width-pixels.
{&window-name}:max-height-pixels = session:height-pixels.
{&window-name}:max-width-pixels = session:width-pixels.

b1:load-image-up("images/download.bmp").
b1:load-image-insensitive("images/download-i.bmp").
b2:load-image-up("images/upload.bmp").
b2:load-image-insensitive("images/upload-i.bmp").
b3:load-image-up("images/erase.bmp").
b3:load-image-insensitive("images/erase-i.bmp").
b4:load-image-up("images/sync.bmp").
b4:load-image-insensitive("images/sync-i.bmp").
b5:load-image-up("images/update.bmp").
b5:load-image-insensitive("images/update-i.bmp").


browse brwDocs:POPUP-MENU = MENU docpopmenu:HANDLE.

/* Tell the parent window we are open even though we haven't pulled 
   directory data yet */
publish "WindowOpened" ("DOCUMENTS").

b2:sensitive = lookup("new", pValidActions) > 0.
menu-item m_PopNew:sensitive in menu docpopmenu = lookup("new", pValidActions) > 0.

b5:sensitive = lookup("modify", pValidActions) > 0.
menu-item m_PopModify:sensitive in menu docpopmenu = lookup("modify", pValidActions) > 0.


/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* Prepend root folder based on config setting (dev versus prod) */
std-ch = "".
publish "GetSfRootFolder" (output std-ch).

std-ch = std-ch + pServerPath 
  + "  (" 
  + (if lookup("new", pValidActions) > 0 then "C" else "")
  + (if lookup("open", pValidActions) > 0 then "R" else "")
  + (if lookup("modify", pValidActions) > 0 then "U" else "")
  + (if lookup("delete", pValidActions) > 0 then "D" else "")
  + ")".
status input std-ch in window {&window-name}.
status default std-ch in window {&window-name}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  refreshData().
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionCategorize C-Win 
PROCEDURE ActionCategorize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 def var tFileCnt as int no-undo.
 def var tGoodCnt as int no-undo.
 def var tMsg as char no-undo.
 def var tNewCategory as char no-undo.

 if lookup("modify", pValidActions) = 0
  then return.

 if browse brwDocs:num-selected-rows = 0
  then return.

 run sharefile/sfcatdialog.w (output tNewCategory,
                              output std-lo).
 if not std-lo
  then return.


 do tFileCnt = 1 to browse brwDocs:num-selected-rows:
  browse brwDocs:fetch-selected-row(tFileCnt).

  if not available document
   then next.

  if document.identifier = ""
   then
    do: tMsg = tMsg + (if tMsg > "" then chr(19) else "")
           + document.fullpath + " is invalid.".
        next.
    end.

  session:set-wait-state("general").
  run sharefile/sfFileEdit.p
    (pApp,
     document.identifier,
     tNewCategory,
     "",
     output std-lo,
     output std-ch).
  session:set-wait-state("").

  if not std-lo
   then tMsg = tMsg + (if tMsg > "" then chr(19) else "")
          + "Unable to edit " + document.name + " .".
   else tGoodCnt = tGoodCnt + 1.
 end.

 if tMsg > ""
   or tGoodCnt <> browse brwDocs:num-selected-rows
  then
   MESSAGE "Failed to edit " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1)
            tMsg skip(2)
           "Contact the System Administrator."
    VIEW-AS ALERT-BOX warning BUTTONS OK.
 
 if tGoodCnt > 0
  then refreshData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionDelete C-Win 
PROCEDURE ActionDelete :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFileCnt as int no-undo.
 def var tMsg as char no-undo.
 def var tGoodCnt as int no-undo.

 if lookup("delete", pValidActions) = 0
  then return.

 if browse brwDocs:num-selected-rows = 0
  then 
   do: b3:sensitive in frame {&frame-name} = false.
       menu-item m_PopDelete:sensitive in menu docpopmenu = false.
       return.
   end.

 MESSAGE "Document(s) will be permanently removed.  Continue?"
  VIEW-AS ALERT-BOX question BUTTONS OK-Cancel update std-lo.
 if not std-lo 
  then return.

 do tFileCnt = 1 to browse brwDocs:num-selected-rows:
  browse brwDocs:fetch-selected-row(tFileCnt).

  if not available document 
   then next.

  if document.identifier = ""
   then
    do: tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
           + document.fullpath + " is invalid.".
        next.
    end.

  session:set-wait-state("general").
  run sharefile/sfFileDelete.p (pApp,
                                document.identifier,
                                output std-lo,
                                output std-ch).
  session:set-wait-state("").

  if not std-lo 
   then tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
          + "Unable to remove " + document.name + " .".
   else tGoodCnt = tGoodCnt + 1.
 end.

 if tMsg > ""
   or tGoodCnt <> browse brwDocs:num-selected-rows
  then
   MESSAGE "Failed to remove " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1)
            tMsg skip(2)
           "Contact the System Administrator."
    VIEW-AS ALERT-BOX warning BUTTONS OK.
  else refreshData().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionEmail C-Win 
PROCEDURE ActionEmail :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 def var tTo as char no-undo.
 def var tSubject as char no-undo.
 def var tMsg as char no-undo.
 def var tExpiration as int init 30 no-undo.
 def var tNotify as logical init true no-undo.
 def var tDownloads as int init 2 no-undo.

 if lookup("email", pValidActions) = 0
  then return.

 if browse brwDocs:num-selected-rows = 0
  then return.

 if document.identifier = ""
  then
   do: 
    MESSAGE "Document record is corrupted.  Please contact the System Administrator."
     VIEW-AS ALERT-BOX INFO BUTTONS OK.
   end.

 MAIL-BLOCK:
 repeat:
  /* Prompt for email attributes */
  run sharefile/sfemaildialog.w (input document.displayName,
                                 input-output tTo,
                                 input-output tSubject,
                                 input-output tMsg,
                                 input-output tExpiration,
                                 input-output tNotify,
                                 input-output tDownloads,
                                 output std-lo).
  if not std-lo 
   then leave MAIL-BLOCK.
 
/*   session:set-wait-state("general"). */
  run sharefile/sfFileSend.p (pApp,
                              document.identifier,
                              tTo,
                              tSubject,
                              tMsg,
                              tExpiration,
                              tNotify,
                              tDownloads,
                              output std-lo,
                              output std-ch).
/*   session:set-wait-state("").  */
 
  if std-lo 
   then 
    do: publish "AddContact" (tTo).
        leave MAIL-BLOCK.
    end.

  MESSAGE std-ch
   VIEW-AS ALERT-BOX warning BUTTONS OK.
 end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionFilesDropped C-Win 
PROCEDURE ActionFilesDropped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def input parameter pWidget as handle.

 def var tFileCnt as int no-undo.
 def var tMsg as char no-undo.

 if not valid-handle(pWidget) 
  then return.

 if lookup("new", pValidActions) = 0
  then
   do:  pWidget:end-file-drop().
        return.
   end.

 FILE-LOOP:
 do tFileCnt = 1 to pWidget:num-dropped-files:
  std-ch = pWidget:get-dropped-file(tFileCnt).
  std-lo = uploadFile(std-ch, output tMsg).
  if not std-lo 
   then
    do:
        MESSAGE tMsg skip(2)
                "File uploads interrupted."
         VIEW-AS ALERT-BOX error BUTTONS OK.
        leave FILE-LOOP.
    end.
 end.
 pWidget:end-file-drop().

 refreshData().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionNew C-Win 
PROCEDURE ActionNew :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def var tFile as char no-undo.

 if lookup("new", pValidActions) = 0
  then return.

 system-dialog get-file tFile
  must-exist 
  title "Document to Upload"
  update std-lo.
 if not std-lo 
  then return.
 
 publish "GetConfirmFileUpload" (output std-lo).
 if std-lo 
  then
   do:
       MESSAGE tFile " will be uploaded.  Continue?"
        VIEW-AS ALERT-BOX question BUTTONS YES-NO title "Confirmation" update std-lo.
       if not std-lo 
        then return.
   end.

 std-lo = uploadFile(tFile, output std-ch).
 if not std-lo 
  then
   do:
       MESSAGE std-ch
        VIEW-AS ALERT-BOX warning BUTTONS OK.
       return.
   end.

 refreshData().
 if can-find(first document) 
  then apply "value-changed" to brwDocs in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ActionOpen C-Win 
PROCEDURE ActionOpen :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 def output parameter pSuccess as logical init false no-undo.
 
 def var tLocalPath as char no-undo.
 def var tMsg as char no-undo.
 def var tFile as char no-undo.
 def var tFileCnt as int no-undo.
 def var tGoodCnt as int no-undo.

 if lookup("open", pValidActions) = 0
  then return.

 if browse brwDocs:num-selected-rows = 0
  then return.
 
 /* das:ActionOpen - handle multiple */

 publish "GetTempDir" (output tLocalPath).

 do tFileCnt = 1 to browse brwDocs:num-selected-rows:
  browse brwDocs:fetch-selected-row(tFileCnt).

  if not available document 
   then next.

  if document.identifier = ""
   then
    do: tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
           + document.fullpath + " is invalid.".
        next.
    end.

  tFile = tLocalPath + document.name.
  if search(tFile) <> ? 
   then os-delete value(tFile).

  session:set-wait-state("general").
  run sharefile/sfFileDownload.p (pApp,
                                  document.fullpath,
                                  tLocalPath,
                                  output std-lo,
                                  output std-ch).
  session:set-wait-state("").

  if not std-lo 
   then
    do: tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
           + "Unable to download " + document.name + " .".
        next.
    end.
 
  if search(tFile) = ? 
   then
    do: tMsg = tMsg + (if tMsg > "" then chr(19) else "") 
           + document.name + " failed to download.".
        next.
    end.
 
  publish "AddTempFile" (document.name,
                         tFile).
 
  RUN ShellExecuteA in this-procedure (0,
                              "open",
                              tFile,
                              "",
                              "",
                              1,
                              OUTPUT std-in).
  tGoodCnt = tGoodCnt + 1.
 end.
 if tMsg > ""
   or tGoodCnt <> browse brwDocs:num-selected-rows
  then
   MESSAGE "Failed to download " (browse brwDocs:num-selected-rows - tGoodCnt) " files." skip(1)
            tMsg skip(2)
           "Contact the System Administrator."
    VIEW-AS ALERT-BOX warning BUTTONS OK.
  else pSuccess = true.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tCategory 
      WITH FRAME fMain IN WINDOW C-Win.
  ENABLE tCategory brwDocs B4 
      WITH FRAME fMain IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE sortData C-Win 
PROCEDURE sortData PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 {lib/brw-sortdata.i}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE windowResized C-Win 
PROCEDURE windowResized :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 frame fMain:width-pixels = {&window-name}:width-pixels.
 frame fMain:virtual-width-pixels = {&window-name}:width-pixels.
 frame fMain:height-pixels = {&window-name}:height-pixels.
 frame fMain:virtual-height-pixels = {&window-name}:height-pixels.

 /* fMain components */
 {&browse-name}:width-pixels = frame {&frame-name}:width-pixels.
 {&browse-name}:height-pixels = frame {&frame-name}:height-pixels - {&browse-name}:y. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION refreshData C-Win 
FUNCTION refreshData RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def buffer document for document.

 close query brwDocs.
 empty temp-table document.
 empty temp-table tempdoc.

 run sharefile/sfFolderList.p (input pApp,
                               input pServerPath,
                               output std-lo,
                               output std-ch,
                               output table document).

 if not std-lo 
  then
   do:
       message std-ch
         view-as alert-box warning.
       return false.
   end.

 dataSortBy = "".
 dataSortDesc = false.
 run sortData in this-procedure ("displayName").
 apply "value-changed" to brwDocs in frame {&frame-name}.

 if not can-find(first document) 
  then run server/unlinkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).

 RETURN true.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION uploadFile C-Win 
FUNCTION uploadFile RETURNS LOGICAL PRIVATE
  ( input pFile as char,
    output pMsg as char ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
 def var tFileID as char no-undo.

 file-info:file-name = pFile.
 if file-info:full-pathname = ?
   or index(file-info:file-type, "F") = 0
   or index(file-info:file-type, "R") = 0
  then 
   do:
       pMsg = pFile + " is not accessible".
       return false.
   end.

 run sys/progressbar.w persistent ("").
 publish "pbUpdateStatus" ("Uploading document...", 10, 0).

 run sharefile/sfFileUpload.p 
   (pApp,
    pFile,
    pServerPath,
    output std-lo,
    output pMsg,
    output tFileID).

 if not std-lo then
 do:
  publish "pbCloseWindow".
  return false.
 end.
 
 publish "pbUpdateStatus" ("Uploading document...", 80, 0).

 run server/linkdocument.p (pEntityType, pEntityID, pEntitySeq, output std-lo, output std-ch).
 
 publish "pbUpdateStatus" ("Upload complete.", 100, 1).
 publish "pbCloseWindow".
 
 /* Set the file attributes for sorting */
 if tCategory:screen-value in frame {&frame-name} = ""
  then return std-lo.

 session:set-wait-state("general").
 run sharefile/sfFileEdit.p
   (pApp,
    tFileID,
    tCategory:screen-value,
    "",
    output std-lo,
    output pMsg).
 session:set-wait-state("").
 
 RETURN std-lo.
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

