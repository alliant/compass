"1" "Search" "Is there evidence of Search in File" yes 0.5
"2" "Schedule A" "Is Schedule A of the Title Commitment completed properly?" yes 2.0
"3" "Exceptions" "Are exceptions and any affirmative coverages appropriate and properly worded in the Title Commitment?" yes 1.0
"4" "Requirements" "Were appropriate and necessary requirements made in the Title Commitment?" yes 2.0
"5" "Commitment" " Is the final Commitment in file, with down-date, tax search, survey[, and T-47 for Texas only]?" yes 3.0
"6" "Requirements" "Were all requirements properly met, and properly documented?" yes 3.0
"7" "Closing" "Are all Closing documents properly drafted, executed, acknowledged & recorded in accordance with underwriting execution guidelines?" yes 2.0
"8" "Policies" "Were Policies and Endorsements properly issued, and the proper rates charged?" yes 1.5
"9" "Limits" "Was Policy Authorization Request approval obtained, retained in file, and its requirements followed?" yes 1.5
"10" "Indemnities" "Were any indemnities or post closing escrow properly handled and approved by the Underwriter?" yes 1.5
"11" "Risks" "Were extra-hazardous risks properly identified, approved by the Underwriter, and documented in the file?" yes 1.5
.
